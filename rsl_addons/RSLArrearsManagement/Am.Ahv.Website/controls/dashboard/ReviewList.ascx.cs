﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.utitility_classes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using Am.Ahv.Utilities.constants;
using System.IO;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Entities;
namespace Am.Ahv.Website.controls.dashboard
{
    public partial class ReviewList :UserControlBase
    {
        #region"Attributes"

        Status statusManager = null;
        Am.Ahv.BusinessManager.Dashboard.ReviewList reviewListManager = null;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool overDue;

        public bool OverDue
        {
            get { return overDue; }
            set { overDue = value; }
        }

        int statusId;

        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
     
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
       
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        #endregion


        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["PrevRowTenant"] = "";
            if (!IsPostBack)
            {
               /*  Commented by Adnan to speed up Dashboard 
                SetDefaultPagingAttributes();
                ViewState["SortDirection"] = "ASC";
                ViewState["SortExpression"] = "AM_Case.ACTIONREVIEWDATE";
                InitLookups();
              //  PopulateGridView(2, false, DbIndex, ApplicationConstants.pagingPageSizeReviewList);
              //  SetCurrentRecordsCount(2, false);                
              //  SetPagingLabels();
               
              */   
            }
            ResetMessage();
        }

        #endregion

        #region"DDL Status Selected Index Changed"

        protected void ddlReviewListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlReviewListStatus.SelectedValue != "-1")
            {
                SetDefaultPagingAttributes();
                PopulateGridView(Convert.ToInt32(ddlReviewListStatus.SelectedValue), chkBoxOverDue.Checked, DbIndex, ApplicationConstants.pagingPageSizeReviewList, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetCurrentRecordsCount(Convert.ToInt32(ddlReviewListStatus.SelectedValue), chkBoxOverDue.Checked);
            }
            GetViewStateValues();
            SetPagingLabels();
            updpnlReviewList.Update();
        }

        #endregion

        #region"CHK Box Over Due Checked Changed"

        protected void chkBoxOverDue_CheckedChanged(object sender, EventArgs e)
        {
           // if (ddlReviewListStatus.SelectedValue != "-1")
           // {
            SetDefaultPagingAttributes();
            PopulateGridView(Convert.ToInt32(ddlReviewListStatus.SelectedValue), chkBoxOverDue.Checked, DbIndex, ApplicationConstants.pagingPageSizeReviewList, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetCurrentRecordsCount(Convert.ToInt32(ddlReviewListStatus.SelectedValue), chkBoxOverDue.Checked);
            //}
            GetViewStateValues(); 
            SetPagingLabels();
            updpnlReviewList.Update();
        }

        #endregion

        #region"LBtn Next"

        protected void lBtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            increasePaging();
        }

        #endregion

        #region"LBtn Previous"

        protected void lBtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            reducePaging();
        }

        #endregion

        #region"Img Case Click"

        protected void imgCase_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((ImageButton)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            Label lbl = new Label();
            Label lblCaseHistory = new Label();
            Label lblCustomerId = new Label();
            ImageButton imgBtn = new ImageButton();
            imgBtn = (ImageButton)gvReviewList.Rows[index].FindControl("imgCase");
            lbl = (Label)gvReviewList.Rows[index].FindControl("lblTenantId");
         //   lblCaseHistory = (Label)gvReviewList.Rows[index].FindControl("lblCaseHistoryId");
            lblCustomerId = (Label)gvReviewList.Rows[index].FindControl("lblCustomerId");

            Session.Remove(SessionConstants.CaseId);
            Session.Remove(SessionConstants.TenantId);
            Session.Remove(SessionConstants.CustomerId);
            Session[SessionConstants.TenantId] = lbl.Text;
            Session[SessionConstants.CaseId] = imgBtn.CommandArgument;
            Session[SessionConstants.CustomerId] = lblCustomerId.Text;
            PageBase page = new PageBase();
            page.RemovePaymentPlanSession();
            Response.Redirect(PathConstants.casehistory, false);
            //Response.Redirect(PathConstants.casedetails + "?CaseHistoryId=" + lblCaseHistory.Text, false);
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Init Lookups"

        public void InitLookups()
        {
            try
            {
                statusManager = new Status();
                ddlReviewListStatus.DataSource = statusManager.GetAllStatus();
                ddlReviewListStatus.DataTextField = "Title";
                ddlReviewListStatus.DataValueField = "StatusId";
                ddlReviewListStatus.DataBind();
                if (ddlReviewListStatus.Items.Count == 0)
                {
                    ddlReviewListStatus.Items.Add(new ListItem("Please Select", "-1"));
                    ddlReviewListStatus.SelectedValue = "-1";
                }
                else
                {
                    ddlReviewListStatus.Items.Insert(0,new ListItem("All", "-1"));
                    //ddlReviewListStatus.SelectedValue = "-1";
                    ddlReviewListStatus.SelectedIndex = 0;
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingLookupsReviewList, true);
                }
            }
        }

        #endregion

        #region"Populate Grid View"

        public void PopulateGridView(int statusId, bool overdueStatus, int dbIndex, int pageSize, string sortExpression, string sortDirection)
        {
            try
            {   
                Label lbl = this.Parent.FindControl("lblCaseOfficer") as Label;         
                reviewListManager = new BusinessManager.Dashboard.ReviewList();
                gvReviewList.DataSource = reviewListManager.GetReviewList(int.Parse(lbl.Text), statusId, overdueStatus, dbIndex, pageSize, sortExpression, sortDirection);
                                        //=reviewListManager.GetReviewList(Convert.ToInt32(Session[SessionConstants.CaseOwnedBy]), statusId, overdueStatus);
                
                gvReviewList.DataBind();
                SetViewStateValues(Convert.ToString(statusId), overdueStatus);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridViewReviewList, true);
                }
            }
        }

        #endregion

        

        //protected void review_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
            
        //     if (e.Row.RowType == DataControlRowType.DataRow)
        //     {
        //        Label lbl = (Label)e.Row.FindControl("lblJointTenacy");
        //        Label lbl2 = (Label)e.Row.FindControl("lblTenantId");
        //        if (lbl.Text == "2" && lbl2.Text == Session["PrevRowTenant"].ToString())
        //        {
        //            e.Row.Visible = false;
                    
        //        }
        //        else
        //        {
        //            e.Row.Visible =true;
        //            lbl.Visible = false;
                    
        //        }
  
        //         Session["PrevRowTenant"] = lbl2.Text;
                 

        //        //}
        //        //else
        //        //{
        //        //    e.Row.Visible = true;
        //        //}
        //    }
        //}

        #endregion

        #region"Funtions"

        #region"Reload Control"

        public void ReloadControl()
        {
            
             
            GetViewStateValues();
            //GetSearchIndexes();
            if (ddlReviewListStatus.SelectedValue == null || ddlReviewListStatus.SelectedValue.Equals(string.Empty))
            {
                StatusId = -1;
            }
            else
            {
                StatusId = Convert.ToInt32(ddlReviewListStatus.SelectedValue);
            }
            OverDue = chkBoxOverDue.Checked;
            PopulateGridView(StatusId, OverDue, DbIndex, ApplicationConstants.pagingPageSizeReviewList, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
            SetCurrentRecordsCount(StatusId, OverDue);
            SetPagingLabels();
            updpnlReviewList.Update();
             
        }


        #endregion

        #region"Get Due Date"

        public string GetDueDate(string nextActionAlert, string recordedActionDate, string IsOverdue, string isNextStatus, string statusRecordedDate)
        {
            DateTime date = DateTime.Now;
            string[] strArray = nextActionAlert.Split(';');
            if (isNextStatus == "False")
            {
                Am.Ahv.BusinessManager.statusmgmt.Action actionManager = new BusinessManager.statusmgmt.Action();
                date = DateOperations.AddDate((int.Parse(strArray[0])), strArray[1], DateOperations.ConvertStringToDate(recordedActionDate));
                return date.ToString("dd/MM/yyyy");
            }
            else
            {
                Am.Ahv.BusinessManager.statusmgmt.Action actionManager = new BusinessManager.statusmgmt.Action();
                date = DateOperations.AddDate((int.Parse(strArray[0])), strArray[1], DateOperations.ConvertStringToDate(statusRecordedDate));
                return date.ToString("dd/MM/yyyy");
            }
        }

        #endregion

        #region"Reduce Paging"

        public void reducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                GetSearchIndexes();
                PopulateGridView(StatusId, OverDue, DbIndex * ApplicationConstants.pagingPageSizeReviewList, ApplicationConstants.pagingPageSizeReviewList, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void increasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                GetSearchIndexes();
                PopulateGridView(StatusId, OverDue, DbIndex * ApplicationConstants.pagingPageSizeReviewList, ApplicationConstants.pagingPageSizeReviewList, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion               

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = ApplicationConstants.pagingPageSizeReviewList;
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            //RowIndex = -1;
            //RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageReviewList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexReviewList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesReviewList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsReviewList] = TotalRecords;           
            ViewState[ViewStateConstants.currentRecordsReviewList] = CurrentRecords;

        }

        public void SetViewStateValues(string _statusId, bool _overDue)
        {
            ViewState[ViewStateConstants.statusIdReviewList] = _statusId;
            ViewState[ViewStateConstants.overDueReviewList] = _overDue;            
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {
              //  GetViewStateValues();
                int count = TotalRecords;

                if (count > ApplicationConstants.pagingPageSizeReviewList)
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblResultOnPage.Text = (CurrentRecords).ToString();
                    lblResultTotal.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblResultOnPage.Text = gvReviewList.Rows.Count.ToString();
                    lblResultTotal.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                SetViewStateValues();
            }
            catch (FileLoadException flException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPagingLabelsReviewList, true);
                }
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % ApplicationConstants.pagingPageSizeReviewList > 0)
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeReviewList) + 1;
                }
                else
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeReviewList);
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPagesReviewList, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (gvReviewList.Rows.Count == ApplicationConstants.pagingPageSizeReviewList)
                {
                    CurrentRecords = CurrentPage * gvReviewList.Rows.Count;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += gvReviewList.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecordsReviewList, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"

        public void SetCurrentRecordsCount(int statusId, bool overdueStatus)
        {
            try
            {
                Label lbl = this.Parent.FindControl("lblCaseOfficer") as Label;
                reviewListManager = new BusinessManager.Dashboard.ReviewList();
                TotalRecords = reviewListManager.GetReviewListCount(int.Parse(lbl.Text), statusId, overdueStatus);
                ViewState[ViewStateConstants.totalRecordsReviewList] = TotalRecords;
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalRecordsReviewList, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageReviewList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexReviewList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesReviewList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsReviewList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsReviewList]);            
        }

        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            StatusId = Convert.ToInt32(ViewState[ViewStateConstants.statusIdReviewList]);            
            OverDue = Convert.ToBoolean(ViewState[ViewStateConstants.overDueReviewList]);            
        }

        #endregion

        #endregion

        
  

    #region"sorting"

   protected void pgvReviewList_Sorting(object sender, GridViewSortEventArgs e)
   {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }

            SetDefaultPagingAttributes();
            GetViewStateValues();
            GetSearchIndexes();

            PopulateGridView(StatusId, OverDue, DbIndex, ApplicationConstants.pagingPageSizeReviewList, e.SortExpression, GetSortDirection(e.SortExpression).ToString());
            SetCurrentRecordsCount(Convert.ToInt32(ddlReviewListStatus.SelectedValue), chkBoxOverDue.Checked);

            SetPagingLabels();
   }

  
    public void SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e, SortDirection sortDirection)
    {
        e.SortDirection = sortDirection;
        IEnumerable<T>set= pagebase.PageBase.SortGrid<T>(dataSource, e);
        gvReviewList.DataSource = set;
        gvReviewList.DataBind();
    }

   private string GetSortDirection(string column)
   {
      // By default, set the sort direction to ascending.
      // SortDirection sortDirection = SortDirection.Ascending;

      // Retrieve the last column that was sorted.
      string sortExpression = Convert.ToString(ViewState["SortExpression"]);

      if (sortExpression != null)
      {
        // Check if the same column is being sorted.
        // Otherwise, the default value can be returned.
        if (sortExpression.ToUpper() == column.ToUpper())
        {
          string lastDirection = Convert.ToString(ViewState["SortDirection"]);
          if ((lastDirection != null) && (lastDirection == "ASC"))
          {
             ViewState["SortDirection"] = "DESC";
             //sortDirection = SortDirection.Descending;
          }
          else
          {
            ViewState["SortDirection"] = "ASC";
          }
        }
        else
        {
          ViewState["SortDirection"] = "DESC";
        }
     }

      // Save new values in ViewState.
      //ViewState["SortDirection"] = sortDirection;
      ViewState["SortExpression"] = column;

      return ViewState["SortDirection"].ToString();
    }
    #endregion
 }
}