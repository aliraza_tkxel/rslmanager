﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardSearch.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.DashboardSearch" %>
<asp:UpdatePanel ID="updpnlDashBoardSearch" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table border="0" cellpadding="0" style="border-collapse: collapse" width="380">
            <tr>
                <td height="7" class="dashboardSearchBoxCell" colspan="2">
                </td>
            </tr>
            <tr>
                <td height="30" class="dashboardSearchBoxCell">
                    Postcode:
                </td>
                <td height="30" class="dashboardSearchBoxCell">
                    <asp:TextBox ID="txtPostCode" runat="server" Width="195px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td height="30" class="dashboardSearchBoxCell">
                    Cases owned by:
                </td>
                <td height="30" class="dashboardSearchBoxCell">
                    <asp:DropDownList ID="ddlCaseOwnedBy" runat="server" Width="200px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlCaseOwnedBy_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td height="30" class="dashboardSearchBoxCell">
                    Cases within:
                </td>
                <td height="30" class="dashboardSearchBoxCell">
                    <asp:DropDownList ID="ddlCaseRegion" runat="server" Width="200px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlCaseRegion_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td height="30" class="dashboardSearchBoxCell">
                    &nbsp;
                </td>
                <td height="30" class="dashboardSearchBoxCell">
                    <asp:DropDownList ID="ddlCaseSuburb" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td height="30" class="dashboardSearchBoxCell">
                    &nbsp;
                </td>
                <td height="30" class="dashboardSearchBoxCell">
                    <p style="margin-left: 5px; margin-right: 7px">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="dashboradSearchBoxButton"
                            OnClick="btnSearch_Click" />
                        <%--<input type="submit" value="Search" name="B1" class="dashboradSearchBoxButton" />--%>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>