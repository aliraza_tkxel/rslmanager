﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewList.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.ReviewList" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:UpdatePanel ID="updpnlReviewList" UpdateMode="Conditional" runat="server">
    <ContentTemplate>    
  
  <div style="clear:both;">
  <table width="402" cellpadding="0" style="border-collapse: collapse">
                        <tr>
                            <td height="39" background="style/images/dashboard_17.jpg" width="12">
                                &nbsp;
                            </td>
                            <td height="39" background="style/images/dashboard_09.jpg">
                                <div class="dashboardHeading" style="float:left; padding-top:5px;" >
                                    Action Review List:
                                </div>
                                <div style="float:left">
                                <font class="dashboardHeading">                                                                        
                                    <asp:DropDownList ID="ddlReviewListStatus" runat="server" Width="140px" OnSelectedIndexChanged="ddlReviewListStatus_SelectedIndexChanged"
                                                       AutoPostBack="true">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:CheckBox ID="chkBoxOverDue" runat="server" oncheckedchanged="chkBoxOverDue_CheckedChanged" AutoPostBack="true" />
                                    Overdue</font>
                                    </div>
                            </td>
                            <td height="39" background="style/images/dashboard_18.jpg" width="21">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td background="style/images/dashboard_11.jpg">
                                &nbsp;
                            </td>
                            <td bgcolor="#DFDFDF">

                                 <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    </asp:Panel>
                                <div align="center" style="min-height:485px;height:auto !important;height:485px;height:auto;">
                                  <cc1:PagingGridView ID="gvReviewList" runat="server" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                                  EmptyDataRowStyle-HorizontalAlign="Center" RowStyle-Wrap="true" Height="50px" Width="100%" AllowPaging="false" GridLines="None" AutoGenerateColumns="false"
                                                  OnSorting="pgvReviewList_Sorting">
                                          <Columns>
                                            <asp:TemplateField HeaderText="Stage :" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="Status" HeaderStyle-ForeColor="black">
                                                    <ItemStyle Width="20%" Height="25px" HorizontalAlign="Left" />
                                                    <ItemTemplate>                 
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") + ":" %> ' ForeColor="Black"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Action" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="LastAction" HeaderStyle-ForeColor="black">
                                                    <ItemStyle Width="20%" Height="25px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <!--<asp:Label ID="lblNextAction" runat="server" Text='<%#Eval("NextAction") %>'></asp:Label>-->
                                                        <asp:Label ID="lblLastAction" runat="server" Text='<%#Eval("LastAction") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Customer" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                                                    <ItemStyle Width="30%" HorizontalAlign="Left" Height="25px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblJointTenacy" runat="server" Text='<%#Eval("JointTenancyCount") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCustomer" runat="server" Text='<%#Eval("JointTenancyCount").ToString()=="2"?(Eval ("CustomerName")+" & " +Eval("CustomerName2")):Eval ("CustomerName") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Owned" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="Owned" HeaderStyle-ForeColor="black">
                                                    <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                                                    <ItemTemplate>                                
                                                        <asp:Label ID="lblOwned" runat="server" Text='<%#Eval("Owned") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Due" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="ActionReviewDate" HeaderStyle-ForeColor="black">
                                                    <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblDue" runat="server" Text='<%# this.GetDueDate(Eval("nextActionAlert").ToString(), Eval("ActionRecordedDate").ToString(), Eval("IsOverdue").ToString(), Eval("IsNextStatus").ToString(), Eval("StatusRecordedDate").ToString()) %>'></asp:Label>--%>
                                                        <asp:Label ID="lblDue" runat="server" Text='<%# Eval("ActionReviewDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemStyle Width="5%" HorizontalAlign="Left" Height="25px" />
                                                    <ItemTemplate>                                
                                                        <asp:Label ID="lblTenantId" runat="server" Text='<%# Eval("TenantId") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCustomerId" runat="server" Text='<%# Eval("CUSTOMERID") %>' Visible="false"></asp:Label>
                                                       
                                                        <asp:ImageButton ID="imgCase" ImageUrl="~/style/images/i.jpg" OnClick="imgCase_Click" CommandArgument='<%#Eval("Caseid") %>' runat="server" />                                
                                                    </ItemTemplate>
                                                </asp:TemplateField>                        
                                        </Columns>
                                    </cc1:PagingGridView>
                                </div>
                                <hr width="100%" />
                                 <table width="100%" >
                                    <tr>
                                        <td valign="top" width="100px">
                                            <asp:Label ID="lblResults" runat="server" Text="Results"></asp:Label><br />
                                            <asp:Label ID="lblResultOnPage" runat="server"></asp:Label>
                                            &nbsp;<asp:Label ID="lblResultsOf" runat="server" Text="of"></asp:Label>
                                            &nbsp;<asp:Label ID="lblResultTotal" runat="server"></asp:Label>
                                        </td>
                                        <td valign="top" width="100px">
                                            <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label><br />
                                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                                            &nbsp;<asp:Label ID="lblPageOf" runat="server" Text="of"></asp:Label>
                                            &nbsp;<asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                                        </td>
                                        <td valign="top" width="70px">
                                            <asp:LinkButton ID="lbtnPrevious" runat="server" CausesValidation="false" 
                                                OnClick="lBtnPrevious_Click" Text="< Previous"></asp:LinkButton>
                                        </td>
                                        <td valign="top" width="50px">
                                            <asp:LinkButton ID="lbtnNext" runat="server" CausesValidation="false" 
                                                OnClick="lBtnNext_Click" Text="Next >"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td background="style/images/dashboard_13.jpg">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="26" width="12" background="style/images/dashboard_14.jpg">
                                &nbsp;
                            </td>
                            <td height="26" background="style/images/dashboard_15.jpg">
                                &nbsp;
                            </td>
                            <td height="26" width="21" background="style/images/dashboard_16.jpg">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    </div>
   </ContentTemplate>
</asp:UpdatePanel>
<%--<asp:Panel ID="pnlReviewList" Width="390px" GroupingText=" " runat="server">
<table width="100%">
    <tr style="background-color:Red; height:20px; margin-right:10px; margin-left:10px;">
        <td>
            <asp:Label ID="lblReviewList" Font-Bold="true" ForeColor="White" Font-Size="Medium" runat="server" Text="Review List:"></asp:Label>
            &nbsp;&nbsp;
            <asp:DropDownList ID="ddlStatus" Width="150px" runat="server" 
                onselectedindexchanged="ddlStatus_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;
            <asp:CheckBox ID="chkBoxOverdue" runat="server" Text="Overdue" Font-Bold="true" ForeColor="White" Font-Size="Medium" />
        </td>        
    </tr>
    <tr>
        <td>
            <asp:GridView ID="gvReviewList" runat="server" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                          EmptyDataRowStyle-HorizontalAlign="Center" RowStyle-Wrap="true" Width="100%" AllowPaging="false" GridLines="None" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Status :" HeaderStyle-HorizontalAlign="Left" ShowHeader="true">
                            <ItemStyle Width="15%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>                 
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") + ":" %> ' ForeColor="Black"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Next Action" HeaderStyle-HorizontalAlign="Left" ShowHeader="true">
                            <ItemStyle Width="25%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblNextAction" runat="server" Text='<%#Eval("NextAction") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Customer" ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>
                                <asp:Label ID="lblCustomer" runat="server" Text='<%#Eval("Customer") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Owned" ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblOwned" runat="server" Text='<%#Eval("Owned") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due" ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>
                                <asp:Label ID="lblDue" runat="server" Text='<%# this.GetDueDate( Eval("ActionRecordedDate").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>                                
                                <asp:ImageButton ID="imgCase" ImageUrl="~/style/images/i.jpg" CommandArgument='<%#Eval("Caseid") %>' runat="server" />                                
                            </ItemTemplate>
                        </asp:TemplateField>                        
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
        <hr width="100%" />
             <table width="100%" >
                <tr>
                    <td valign="top" width="100px">
                        <asp:Label ID="lblResults" runat="server" Text="Results"></asp:Label><br />
                        <asp:Label ID="lblResultOnPage" runat="server"></asp:Label>
                        &nbsp;<asp:Label ID="lblResultsOf" runat="server" Text="of"></asp:Label>
                        &nbsp;<asp:Label ID="lblResultTotal" runat="server"></asp:Label>
                    </td>
                    <td valign="top" width="100px">
                        <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label><br />
                        <asp:Label ID="lblPageCurrent" runat="server"></asp:Label>
                        &nbsp;<asp:Label ID="lblPageOf" runat="server" Text="of"></asp:Label>
                        &nbsp;<asp:Label ID="lblPageTotal" runat="server"></asp:Label>
                    </td>
                    <td valign="top" width="70px">
                        <asp:LinkButton ID="BtnPrevious" runat="server" CausesValidation="false" 
                            OnClick="lBtnPrevious_Click" Text="&lt; Previous"></asp:LinkButton>
                    </td>
                    <td valign="top" width="50px">
                        <asp:LinkButton ID="BtnNext" runat="server" CausesValidation="false" 
                            OnClick="lBtnNext_Click" Text="Next &gt;"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Panel>--%>