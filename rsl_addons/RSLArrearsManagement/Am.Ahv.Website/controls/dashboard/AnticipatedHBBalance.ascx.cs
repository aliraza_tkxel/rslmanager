﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using Am.Ahv.BusinessManager.lookup;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.Entities;

namespace Am.Ahv.Website.controls.dashboard
{
    public partial class AnticipatedHBBalance : System.Web.UI.UserControl
    {
        #region"Attributes"
        bool isError;
        bool isException;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion
        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //if (GetUserType(base.GetCurrentLoggedinUser()) == ApplicationConstants.CaseOfficer)
                    //{
                    //    showFirstDetectionCount(0, 0);
                    //}
                    //else
                    //{
                    //    showFirstDetectionListAlertCount(-1, -1);
                    //}
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.PageLoadError, true);
                }
            }

        }
        #endregion

        #endregion
        #region "Functions"

        #region "show  Anticipated HB Balance"
        public void showAnticipatedHBBalanceBalance(string postCode, int regionId, int suburbId, int resourceId)
        {
            try
            {

                BalanceList balanceList = new BalanceList();
                AM_SP_GetAnticipatedHBBalance_Result result = new AM_SP_GetAnticipatedHBBalance_Result();
                result = balanceList.GetAnticipatedHBBalance(postCode, regionId, suburbId, resourceId);
                double hb = Convert.ToDouble(result.HB.ToString());
           
                lblNetAnticipatedHBBalance.Text = "£" + Math.Round(hb, 0).ToString();
                if (result.NetRentBalance == null )
                {
                    result.NetRentBalance = 0;
                }
                if (result.GrossRentBalance == null)
                {
                    result.GrossRentBalance = 0;
                }
                if (result.HB == null)
                {
                    result.HB = 0;
                }
                Session[SessionConstants.netRentBalance] = result.NetRentBalance;
                Session[SessionConstants.grossRentBalance ] = result.GrossRentBalance;
                Session[SessionConstants.anticipatedHBBalance] = result.HB;   

                updateAnticipatedHBBalanceBalance.Update();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CountingErrorFirstDetection, true);
                }
            }
        }
        #endregion

        #endregion
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Get User Type"

        private string GetUserType(int userId)
        {
            Lookup lookupManager = new Lookup();
            return lookupManager.GetUserType(userId);

        }

        #endregion
    }
}