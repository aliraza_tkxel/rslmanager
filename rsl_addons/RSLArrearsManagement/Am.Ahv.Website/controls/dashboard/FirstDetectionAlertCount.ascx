﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FirstDetectionAlertCount.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.FirstDetectionAlertCount" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateFirstDetectionsAlertCount" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
    <div>      
        <asp:Label ID="lblFirstDetectionAlertCount" runat="server" CssClass="dashboardBigNumbersUP" ></asp:Label>        
    </div>
    </ContentTemplate>

</asp:UpdatePanel>
