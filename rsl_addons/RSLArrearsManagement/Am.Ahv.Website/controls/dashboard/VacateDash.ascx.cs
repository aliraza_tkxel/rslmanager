﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Dashboard;
namespace Am.Ahv.Website.controls.dashboard
{
    public partial class vacatedash : UserControlBase
    {
        #region Attributes
        vacteoverdue Manager = null;
        bool isError { set; get; }
        bool isException { set; get; }
        int ResourceId { set; get; }
        #endregion
        #region Events
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ResourceId = 5;
                //LoadVcateCaseCount(ResourceId);
            }
        }
        #endregion
        #endregion
        #region Utilities
        #region Load Vacate
        private void LoadVcateCaseCount(int RerourceID)
        {
            try
            {
                int ID = RerourceID;
                Manager = new vacteoverdue();
                lblvacatecount.Text = Manager.GetVacateCount(ID).ToString();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.DBvacate, true);
                }
            }
        }
        #endregion
        #region ReloadControl
        public void ReloadControl(int ResourceId)
        {
            LoadVcateCaseCount(ResourceId);
            upvacat.Update();
        }
        #endregion
        #endregion
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
      
    }
}