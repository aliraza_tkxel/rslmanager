﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;


namespace Am.Ahv.Website.controls.dashboard
{
    public partial class MyCases : UserControlBase
    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException;}
            set { isException = value;}
        }

        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            resetMessage();
            try
            {
                if (!IsPostBack)
                {
                    ShowMyCasesCount(Session[SessionConstants.PostCodeSearch].ToString(),base.GetCurrentLoggedinUser(),-1,-1);
                }
            }
            catch(Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex,"Exception Poicy");
            }
            finally
            {
                if (isException) 
                {
                    setMessage(UserMessageConstants.PageLoadError,true);
                }
            }
        }
        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Show MyCases Count"
        public void ShowMyCasesCount(string postCode,int caseOfficerId,int regionId,int suburbid)
        {
            try
            {
                CaseDetails mycases = new CaseDetails();
                lblMyCasesCount.Text = mycases.CountMyCases(postCode,caseOfficerId,regionId,suburbid).ToString();
                updateMyCasesCount.Update();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ErrorMyCasesCount, true);
                }
            }
        }
        #endregion

        



    }
}