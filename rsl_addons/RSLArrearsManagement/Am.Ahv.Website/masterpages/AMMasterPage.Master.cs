﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using System.Globalization;

namespace Am.Ahv.Website.masterpages
{
    public partial class AMMasterPage : System.Web.UI.MasterPage
    {
        public static string message = "";
        public String messageClass = "topmessagesuccess";
        private string backgroundProperty = "background-color";
        private string backgroundColor = "#e6e6e6";
        PageBase pageBase = null;


        #region " Page Load "
        protected void Page_Load(object sender, EventArgs e)
        {
            pageBase = new PageBase();
            if (!pageBase.CheckSession())
            {
                RedirectToLoginPage();
            }

            this.highLightMenuItems();
            if (!IsPostBack)
            {               
                {

                    if (HttpContext.Current.Session[SessionConstants.LoggedInUserId] == null)
                    {
                       // HttpContext.Current.Response.Redirect("~/BHAIntranet/Login.aspx");
                    }
                }
               // else
                {

                    SetLoggedInUser();
                    SetCurrentDate();
                    SetLoggedInDate();
                }
            }
        } 
        #endregion

        #region " Page PreRender "
        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblMessage.Text = message;
            updPnlMessage.Update();
        } 
        #endregion


        #region "high Light Menu Items "
        private void highLightMenuItems()
        {
            FileOperations fOper = new FileOperations();

            string fName = fOper.getCurUrlFileName();

            if (fName == PathConstants.dashboardFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightDashboardTwoUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
            }
            else if (fName == PathConstants.statusFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkStatusActions.Style.Add(backgroundProperty, backgroundColor);

            }
            else if (fName == PathConstants.outComeFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkOutcomes.Style.Add(backgroundProperty, backgroundColor);

            }

            else if (fName == PathConstants.activityFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkActivities.Style.Add(backgroundProperty, backgroundColor);

            }


            else if (fName == PathConstants.userFileName )
            {

                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkUser.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.letterResourcetFileName || fName == PathConstants.createLetterFileName || fName == PathConstants.deleteLetterFileName || fName == PathConstants.previewLetterFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkLetters.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.resourceFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightResourceUrl();
                this.hideCasesUpdatePanel();
                this.hideReportsUpdatePanel();
            }            
            else if (fName == PathConstants.firstDetectionListFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightCaseMgtUrl();
                this.hideResourcesUpdatePanel();
                this.hideReportsUpdatePanel();
                if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "previoustenants")
                    {
                        this.lnkPreviousTenants.Style.Add(backgroundProperty, backgroundColor);
                    }
                    else
                    {
                        this.lnkFirstDetList.Style.Add(backgroundProperty, backgroundColor);
                    }
                }
                else
                {
                    this.lnkFirstDetList.Style.Add(backgroundProperty, backgroundColor);
                }

            }
            else if (fName == PathConstants.openCaseFileName || fName == PathConstants.caseListFileName || fName == PathConstants.caseHistoryFileName || fName == PathConstants.caseDetailsFileName || fName == PathConstants.addUpdateCaseFileName || fName == PathConstants.addCaseActivityFileName || fName == PathConstants.viewActivitiesFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightCaseMgtUrl();
                this.hideResourcesUpdatePanel();
                this.hideReportsUpdatePanel();
                this.lnkMyCases.Style.Add(backgroundProperty, backgroundColor);

            }
            else if (fName == PathConstants.suppressedCaseFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkSupCase.Style.Add(backgroundProperty, backgroundColor);

            }
            else if (fName == PathConstants.statusActionReportFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkStageActions.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.balanceListReportFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkBalList.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.historicalBalListReportFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkHistBalList.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.noticeToVctReportFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkNoticeToVctReport.Style.Add(backgroundProperty, backgroundColor);
            }
            else if (fName == PathConstants.paymentPlanReportFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkBtnPaymentPlan.Style.Add(backgroundProperty, backgroundColor);
            }

            else if (fName == PathConstants.CloseCaseListFileName)
            {
                this.highLightDashboardOneUrl();
                this.highLightReportsUrl();
                this.hideResourcesUpdatePanel();
                this.hideCasesUpdatePanel();
                this.lnkCloseCaseList.Style.Add(backgroundProperty, backgroundColor);
            }
            
            

        } 
        #endregion

        #region " Highlight Url Functions "

        #region "high Light Dashboard One Url "
        private void highLightDashboardOneUrl()
        {
           // this.lnkDashboardOne.Style.Add(backgroundProperty, backgroundColor);
        }
        #endregion

        #region "high Light Dashboard Two Url"
        private void highLightDashboardTwoUrl()
        {
            this.lnkDashboardTwo.Style.Add(backgroundProperty, backgroundColor);
        } 
        #endregion

        #region "high Light Resource Url "
        private void highLightResourceUrl()
        {
            this.lnkResource.Style.Add(backgroundProperty, backgroundColor);
        } 
        #endregion

        #region "high Light Case Mgt Url "
        private void highLightCaseMgtUrl()
        {
            this.lnkCaseMgt.Style.Add(backgroundProperty, backgroundColor);
        } 
        #endregion

        #region "high Light Reports Url "
        private void highLightReportsUrl()
        {
            this.lnkReports.Style.Add(backgroundProperty, backgroundColor);
        } 
        #endregion
       
        #endregion 

        #region "Link Button - Events"

        #region "Dashboard - Link - Events"
        //protected void lnkBtnDashBoradOne_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect(PathConstants.dashboardPath);
        //}

        protected void lnkBtnDashBoradTwo_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.dashboardPath);
        }

        protected void lnkBtnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.myWhiteBoardPath);
        }

        #endregion

        #region "Resource Area - Link Button - Events"
        protected void lnkBtnStatusAction_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addStatusPath);
        }

        protected void lnkBtnActivities_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.activityPath);
        }

        protected void lnkBtnOutcomes_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.outComePath);
        }

        protected void lnkBtnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.userPath);            
        }

        protected void lnkBtnResources_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.resourcePath);
        }

        protected void lnkBtnLetters_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.lettersResourceAreaPathCached + "?cmd=new");
        }
        #endregion

        #region "Case Management - Link Button - Events"

        protected void lnkBtnCaseMgt_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.mycaselistef);
        }

        protected void lnkBtnFirstDetection_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.firstDetectionList);
        }

        protected void lnkBtnMyCases_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.mycaselistef);
        }

        protected void lbtnPreviousTenants_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.firstDetectionList + "?cmd=previoustenants");
        }
       

        #endregion 

        #region "Reports - Link Button - Events"

        protected void lnkBtnStageActions_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.statusActionReportPath);
        }

        protected void lnkBtnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.statusActionReportPath);
        }

        #region "lnk Btn SupCase Click "
        protected void lnkBtnSupCase_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.suppressedCasePath);
        }
        #endregion

        protected void lnkBtnBalanceList_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.balanceListPath, false);
        }

        protected void lnkBtnHistBalList_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.historicalBalanceListReportPath);
        }

        protected void lnkBtnNoticeToVctReport_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.noticeToVcateReportPath);
        }

        protected void lnkBtnPaymentPlan_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.paymentPlanReportPath); 
        }

        protected void lnkBtnCloseCaseList_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.CloseCaseListPath); 
        }

        #endregion        


        #endregion

        #region " Hide Update Panels "
        private void hideResourcesUpdatePanel()
        {
            this.updPanelResources.Visible = false;
            this.updPanelResources.Update();
        }

        private void hideCasesUpdatePanel()
        {
            this.updPanelCases.Visible = false;
            this.updPanelCases.Update();
        }

        private void hideReportsUpdatePanel()
        {
            this.updPanelReports.Visible = false;
            this.updPanelReports.Update();
        }
        #endregion 

        #region"Set Logged In User"

        public void SetLoggedInUser()
        {
            pageBase = new PageBase();
            lblLoggedInUserName.Text = pageBase.GetLoggedInUserName();
        }

        #endregion

        #region"Set Last Logged In Date"

        public void SetLoggedInDate()
        {
            pageBase = new PageBase();
            string date = pageBase.GetLastLoggedInDate();
            if (date == string.Empty)
            {
                lblLastLoggedInDate.Text = "";
            }
            else
            {
                lblLastLoggedInDate.Text = ": Last Logged In " + date; ;
            }
        }

        #endregion

        #region"Set Current Date"

        public void SetCurrentDate()
        {
            CultureInfo culture = new CultureInfo("en-GB");            
            lblDate.Text = DateTime.Now.DayOfWeek.ToString() +", "+ DateTime.Now.ToString("F", culture);
        }

        #endregion

        #region"LBTN Sign Out Click"

        protected void lbtnSignOut_Click(object sender, EventArgs e)
        {
            this.RemoveSessionValues();
            Response.Redirect(PathConstants.LoginPath);
        }

        #endregion

        #region"reset Logged In Labels"

        private void ResetLoggedInLabels()
        {
            lblLoggedInUserName.Text = string.Empty;
            lblDate.Text = string.Empty;
        }

        #endregion      

        #region"Redirect To Login Page"

        public void RedirectToLoginPage()
        {
            Response.Redirect(PathConstants.BridgePath);
        }

        #endregion

        #region"Remove Session Values"

        public void RemoveSessionValues()
        {
            Session.RemoveAll();
        }
        #endregion
    }
}
