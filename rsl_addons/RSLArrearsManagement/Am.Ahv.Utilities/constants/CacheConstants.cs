﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
  public  class CacheConstants
    {
        public const string ActionId = "LRAActionId";
        public const string StatusId = "LRAStatusId";
        public const string Code = "Code";
        public const string Title = "Title";
        public const string SearchValue = "SearchValue";
        public const string PageCommand = "PageCommand";
        public const string StatusCreateLetter = "StatusCreateLetter";
        public const string ActionCreateLetter = "ActionCreateLetter";
        public const string TitleCreateLetter = "TitleCreateLetter";
        public const string CodeCreateLetter = "CodeCreateLetter";
        public const string PersonalizationCreateLetter = "PersonalizationCreateLetter";
    }
}
