
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class PathConstants
    {

        #region "File Name Constants For Menu"

        public const string dashboardFileName = "Default.aspx";

        #region "Resources Area Files Name"
        public const string statusFileName = "Status.aspx";
        public const string activityFileName = "Activity.aspx";
        public const string outComeFileName = "Outcome.aspx";
        public const string userFileName = "User.aspx";
        public const string resourceFileName = "Resource.aspx";
        public const string firstDetectionListFileName = "FirstDetectionList.aspx";
        public const string letterResourcetFileName = "LettersResourceArea.aspx";
        public const string createLetterFileName = "CreateLetter.aspx";
        public const string deleteLetterFileName = "DeleteLetter.aspx";
        public const string previewLetterFileName = "LetterPreview.aspx";


        #endregion

        #region "Case Management Area File Names"

        public const string openCaseFileName = "OpenNewCase.aspx";
        public const string caseListFileName = "CaseList.aspx";
        public const string caseHistoryFileName = "CaseHistory.aspx";
        public const string caseDetailsFileName = "CaseDetails.aspx";
        public const string suppressedCaseFileName = "SuppressedCases.aspx";        
        public const string addCaseActivityFileName = "AddCaseActivity.aspx";
        public const string addUpdateCaseFileName = "UpdateCase.aspx";
        public const string viewActivitiesFileName = "ViewActivities.aspx";
       // public const string CRM = "Customer/CRM.asp";
        

        #endregion

        #region "Report Area File Names"
        public const string statusActionReportFileName = "StatusAndActionReport.aspx";
        public const string balanceListReportFileName = "BalanceListReport.aspx";
        public const string historicalBalListReportFileName = "HistoricalBalanceListReport.aspx";
        public const string noticeToVctReportFileName = "NoticeToVacateCasesList.aspx";
        public const string paymentPlanReportFileName = "PaymentPlanMonitoring.aspx";
        public const string CloseCaseListFileName = "CloseCaseList.aspx";
        
        #endregion

        #endregion

        #region "File Path Constants"

        #region"Login Paths"

        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string BridgePath = "~/Bridge.aspx";

        #endregion

        #region "Resource Area File Paths"
        public const string myWhiteBoardPath = "~/../BHAIntranet/CustomPages/MyWhiteBoard.aspx";
        public const string dashboardPath = "~/Default.aspx";
        public const string resourcePath = "~/secure/resources/Resource.aspx";       
        public const string addCaseWorkerPath = "~/secure/resources/User.aspx";
        public const string outComePath = "~/secure/resources/Outcome.aspx";
        public const string addNewActivityPath = "~/secure/resources/Activity.aspx?cmd=New";
        public const string editActivityPath = "~/secure/resources/Activity.aspx?cmd=Edit";
        public const string userPath = "~/secure/resources/User.aspx";
        public const string addStatusPath = "~/secure/resources/Status.aspx";
        public const string addMoreStatusPath = "~/secure/resources/Status.aspx?cmd=New";
        public const string editStatusPath = "~/secure/resources/Status.aspx?cmd=Edit";
        public const string lettersResourceAreaPathCached = "~/secure/resources/LettersResourceArea.aspx";
        public const string previewLetter = "LetterPreview.aspx?cmd=PreviewLetter";
        public const string previewTemplate = "LetterPreview.aspx?cmd=PreviewTemplate";
        public const string editLetterTemplate = "~/secure/resources/CreateLetter.aspx?cmd=cedit&";
        public const string activityLetterTemplateEdit = "~/secure/resources/CreateLetter.aspx?cmd=activityedit&";
        public const string delLetterTemplate = "~/secure/resources/DeleteLetter.aspx";
        public const string previewLetterTemplate = "~/secure/resources/LetterPreview.aspx";
        public const string activityPath = "~/secure/resources/Activity.aspx";
        #endregion

        #region "Case Management File Paths"

        public const string openNewCase = "~/secure/casemgt/OpenNewCase.aspx";
        public const string openNewCaseBalanceListReport = "~/secure/casemgt/OpenNewCase.aspx";
        public const string CaseDetailsBalanceListReport = "~/secure/casemgt/CaseHistory.aspx";
        public const string firstDetectionList = "~/secure/casemgt/FirstDetectionList.aspx";
        public const string mycaseList = "~/secure/casemgt/CaseList.aspx?cmd=Error";
        public const string caseList = "~/secure/casemgt/CaseList.aspx";
        public const string casehistory = "~/secure/casemgt/CaseHistory.aspx";
        public const string javaScriptCasehistory = "~/secure/casemgt/CaseHistory.aspx?cid=";
        public const string casedetails = "~/secure/casemgt/CaseDetails.aspx";
        public const string mycaselistef = "~/secure/casemgt/CaseList.aspx";
        public const string updatecase = "~/secure/casemgt/UpdateCase.aspx";
        public const string addCaseActivityPath = "~/secure/casemgt/AddCaseActivity.aspx";
        public const string viewActivitiesPath = "~/secure/casemgt/ViewActivities.aspx";
        public const string updateCaseRedirectPath = "~/secure/casemgt/CaseHistory.aspx?cmd=Success";
        public const string viewActivityPath = "ViewCaseActivity.aspx?activityid=";
        public const string downloadFile = "../../secure/casemgt/Download.aspx";
        public const string AddCaseActivity = "~/secure/casemgt/AddCaseActivity.aspx";

        #endregion

        #region "Report File Paths"
        public const string statusActionReportPath = "~/secure/reports/StatusAndActionReport.aspx";
        public const string suppressedCasePath = "~/secure/reports/SuppressedCases.aspx";               
        public const string flexiblePaymentReport = "~/secure/reports/FlexiblePaymentPlan.aspx";
        public const string flexiblePaymentReportPopup = "../../secure/reports/FlexiblePaymentPlan.aspx";
        public const string regularPaymentReport = "~/secure/reports/RegularPaymentPlan.aspx";
        public const string caseDetailPrintOutPath = "~/secure/reports/RegularPaymentPlan.aspx";
        public const string caseActivityPrintOutPath = "~/secure/reports/RegularPaymentPlan.aspx";
        public const string balanceListReportPath = "~/secure/reports/BalanceList.aspx";
        public const string balanceListPath = "~/secure/reports/BalanceListReport.aspx";        
        public const string historicalBalanceListReportPath = "~/secure/reports/HistoricalBalanceListReport.aspx";
        public const string noticeToVcateReportPath = "~/secure/reports/NoticeToVacateCasesList.aspx";
        public const string caseDetailsReportPath = "~/secure/reports/CaseDetailsReport.aspx";
        public const string caseDetailsReportPathPopup = "../../secure/reports/CaseDetailsReport.aspx";
        public const string caseActivitiesReportPath = "~/secure/reports/CaseActivities.aspx";
        public const string paymentPlanReportPath = "~/secure/reports/PaymentPlanMonitoring.aspx";
        public const string CloseCaseListPath = "~/secure/reports/CloseCaseList.aspx";


        #endregion

        #region Letters Area
        public const string LRAMovetoEdit = "~/secure/resources/CreateLetter.aspx?cmd=edit&Id=";
        public const string LRAMovetopreview = "~/secure/resources/LetterPreview.aspx?cmd=search";
        public const string CHMovetopreview = "../../secure/resources/LetterPreview.aspx?cmd=search&Id=";
        public const string CHRedirecttopreview = "../../secure/resources/LetterPreview.aspx?cmd=search";
        public const string LRAmovetoselect = "~/secure/resources/Status.aspx?cmd=search&statusid=";
        public const string LRAmovetoselectError = "~/secure/resources/Status.aspx?cmd=error";
        public const string LetterTemplatesArea = "~/secure/resources/LettersResourceArea.aspx";
        public const string LetterCreateTemplate = "~/secure/resources/CreateLetter.aspx";
        #endregion

        #region"Letter Template Images"

        public const string LetterTemplateImages = "~/style/images/userfiles";

        #endregion

        #endregion

    }
}

