using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class SessionConstants
    {
        #region"User Credentials"

        public const string BridgedUserId = "BridgedUserId";
        public const string RslUserId = "RslUserId";
        public const string LoggedInUserId = "LoggedInUserId";
        public const string LoggedInUserName = "LoggedInUserName";
        public const string LastLoggedInDate = "LastLoggedInDate";

        #endregion

        #region"Resources"

        #region"Letter/Template Management"

        public const string GlobalCaseHistoryList = "GlobalCaseHistoryList";
        public const string StandardLetterIdCreateLetter = "StandardLetterId";
        public const string StandardLetterHistoryIdCreateLetter = "StandardLetterHistoryId";
        public const string IsStandardLetterHistoryCreateLetter = "IsStandardLetterHistory";
        public const string CachedActionObject = "CacheAction";

        public const string isSavedPDFCreateLetter = "IsSavedPDF";

        #endregion

        #region"Case Worker"

        public const string Employee = "Employee";
        public const string RegionList = "RegionList";
        public const string SuburbList = "SuburbList";
        public const string StreetList = "StreetList";
        public const string EditResourceId = "EditResourceId";

        #endregion

        #region"Outcomes"

        public const string EditOutcomeId = "EditOutcomeId";
        public const string CurrentPageOutcomes = "CurrentPage";
        public const string DbIndexOutcomes = "dbIndex";
        public const string TotalPagesOutcomes = "TotalPages";
        public const string TotalRecordsOutcomes = "TotalRecords";

        #endregion

        #region"Activities"

        public const string EditActivityId = "EditActivityId";
        public const string ActivityListingsId = "activityListingsId";

        #endregion

        #region"Status/Stage and Actions"

        public const string ActionId = "ActionId";
        public const string StatusID = "StatusID";
        public const string StatusName = "StatusName";
        public const string StatusType = "Status";

        #endregion

        #endregion

        #region"Dashboard"

        public const string CaseOwnedByDashboardSearch = "CaseOwnedByDashboardSearch";
        public const string RegionDashboardSearch = "RegionDashboardSearch";
        public const string SuburbDashboardSearch = "SuburbDashboardSearch";
        public const string PostCodeSearch = "PostCodeSearch";
        public const string monthArrearsKPI = "monthArrearsKPI";
        public const string yearArrearsKPI = "yearArrearsKPI ";
        public const string netRentBalance = "NetRentBalance";
        public const string grossRentBalance = "GrossRentBalance";
        public const string anticipatedHBBalance = "AnticipatedHBBalance";

        #endregion

        #region"Case Management"

        #region"Payment Plan"

        public const string paymentPlanId = "PaymentPlanId";
        public const string UpdateFlagPaymentPlan = "UpdateFlagPaymentPlan";
        public const string PaymentPlanTypePaymentPlan = "PaymentPlanType";
        public const string CreateDatePaymentPlan = "PaymentPlanCreateDate";
        public const string OpenCaseFlag = "OpenCaseFlag";
        public const string PaymentPlanBackPage = "PaymentPlanBackPage";
        public const string lastPayment = "lastPayment";
        public const string lastPaymentDate = "lastPaymentDate";
        public const string paymentPLanSummary = "PaymentPlanSummary";

        #endregion

        #region"Case History/Summary"

        public const string NextStatusIdCaseSummary = "NextStatusIdCaseSummary";
        public const string NextActionIdCaseSummary = "NextActionIdCaseSummary";
        public const string visitedCases = "VisitedCases";

        public const string CaseId = "CaseId";
        public const string CaseHistoryId = "CaseHistoryId";
        public const string Rentbalance = "RentBalance";
        public const string WeeklyRentAmount = "WeeklyRentAmount";
        public const string HBPayment = "HBPayment";
        public const string TenantId = "TenantId";
        public const string CaseOwnedBy = "CaseOwnedBy";
        public const string CustomerId = "CustomerId";
        public const string ViewCaseActivityId = "ViewCaseActivityId";
        public const string HistoryIdList = "CaseHistoryIds";

        public const string OwedTBHA = "OwedToBHA";
        public const string LastMonthNetArrears = "LastMonthNetArrears";
        public const string HBCyclePayment = "HBCyclePayment";

        #endregion

        #region"Open Case"

        public const string SavedLetterHistoryListOpenCase = "SavedLetterHistoryListOpenCase";

        #endregion

        #region"Update Case"

        public const string SavedLetterHistoryListUpdateCase = "SavedLetterHistoryListUpdateCase";
        public const string NextActionUpdateCase = "NextActionUpdateCase";
        public const string ActionRecordedDateUpdateCase = "ActionRecordedDateUpdateCase";
        public const string StandardLetterListUpdateCase = "letterListUpdateCase";
        public const string SavedStandardLetterListUpdateCase = "SavedLettersUpdateCase";
        public const string DocumentsArrayListUpdateCase = "documentsArrayListUpdateCase";

        #endregion

        #endregion

    }
}
