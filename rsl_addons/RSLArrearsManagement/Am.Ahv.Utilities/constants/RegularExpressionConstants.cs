﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class RegularExpressionConstants
    {
        //public static string AmountExp = @"^[+-]?(?:\d+\.?\d*|\d*\.?\d+)[\r\n]*$";
        public static string AmountExp = @"^\d+(\.\d+)?$";
        public static string numbersExp = "^[0-9.]+$";
        public static string emailExp =  @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$";
        public static string stringLengthExp = @"^[\s\S]{0,30}$";

    }
}
