using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class ApplicationConstants
    {
        public static string regionId = "PATCHID";
        public static string regionname = "LOCATION";
        public static string suburbId = "DEVELOPMENTID";
        public static string suburbname = "DEVELOPMENTNAME";
        public static string schememname = "SCHEMENAME";
        public static string defaulvalue = "-1";
        public static string streetId = "ADDRESS1";
        public static string streetName = "ADDRESS1";
        public static string schemeId = "SCHEMEID";
        public static string codeId = "lookupCodeId";
        public static string codename = "CodeName";
        public static string teamname = "TEAMNAME";
        public static string teamId = "TEAMID";
        public static string EmployeId = "EMPLOYEEID";
        public static string EmloyeeName = "EmployeeName";
        public static string usertype = "UserType";
        public static string EMPLOYEENAME = "LASTNAME";
        public static string rentcount = "RentCount";
        public static string reviewperiod = "ReviewPeriod";
        public static string activityType = "Activity";
        public static string outcome = "Outcome";
        public static string referralType = "ReferralType";
        public static string balanceListArrearsReport = "BalanceList";
        public static string StatusId = "StatusId";
        public static string ActionId = "ActionId";
        public static string Title = "Title";
        public static string orgId = "ORGID";
        public static string orgName = "NAME";

        public static string ContactName = "Name";


        public const string AKPIPatch = "Months";
        public const string AKPITotalRent = "TotalRent";
        public static int pagingPageSize = 10;
        public static int pagingPageSizeOutcomes = 10;
        public static int stringLength = 150;
        public static int pagingPageSizeCaseList = 10;
        public static int pagingPageSizeFirstDetection = 10;
        public static int pagingPageSizeSuppressedCases = 10;
        public static int pagingPageSizeReviewList = 25;
        public static int pagingPageSizeBalanceList = 50;
        public static int pagingPageSizeHistoricalBalanceList = 50;

        public static int[] recordsPerPageCaseList = { 10, 20, 30, 40, 50};
        public static int[] recordsPerPageFirstDetection = {10, 20, 30, 40, 50};
        public static int[] recordsPerPageSuppressedCases = {10, 20, 30, 40, 50};

        public static int[] emptyDataSource = { };

        public static string CaseOfficer = "Case Officer";

        public static string NoticeToVacateDS = "NoticeToVacateCasesList";
        public static string HistoricalBalanceListDS =  "HistoricalBalanceListDataSet";

        public static string UserSummaryDS = "DataSet1";
        public static string PaymentPlanDS = "PaymentPlanSummaryDataSet";
        public static string PaymentScheduleDS = "test";

        public static string CaseActivitiesDS = "CaseActivitiesDataSet";

        public static string ReferralType_Internal = "Internal";
        public static string ReferralType_External = "External";
        public static string ReferralType_SupportWorker = "Support Worker";

        public static string SelectTeam = "Select Team";
        public static string SelectOutCome = "Select Outcome";
        public static string SelectName = "Select Name";
        public static string SelectActivity = "Select Activity";
        public static string ActivityHeading = "Add Activity";
        public static string ReferHeading = "Refer Case";
        public static string Refer = "Refer";
        public static string ActivityUploadPath = "ActivityUploadPath";

        public static string SelectContact = "Select Contact";

        public static string ResourceId = "ResourceId";

        public static string ActivityId =  "activityid";

        public static string SelectOrganisation = "Select Organisation";

        public const string javaScriptWindowAttributes = "'_blank','directories=no,height=700,width=1048,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no')";

        public const string PreviewLetter = "PreviewLetter";

        public const string PreviewTemplate = "PreviewTemplate";

        public const string cmd = "cmd";

        public const string search = "search";

        public const string Id = "Id";

        public const string LetterLogo = "LetterLogo";
        public const string OrgName = "OrganisationName";
        public const string OrgAddress = "OrganisationAddress";
        public const string OrgTel = "OrganisationPhone";
        public const string OrgEmail = "OrganisationEmail";

        public const string OrgNameVal = "Organisation Name";
        public const string OrgAddressVal = "Organisation Address";
        public const string OrgTelVal = "Organisation Telephone Number";
        public const string OrgEmailVal = "Organisation Email";


        public const string CustomerContactAdd = "CustomerContactAdd";
        public const string IsManualAddress = "IsManualAddress";
        public const string ManualContactName = "ManualContactName";
        public const string ManualAddress1 = "ManualAddress1";
        public const string ManualAddress2 = "ManualAddress2";
        public const string ManualAddress3 = "ManualAddress3";
        public const string ManualCityTown = "ManualCityTown";
        public const string ManualPostCode = "ManualPostCode";
        public const string IsInvidualLetterReq = "IsInvidualLetterReq";


        public const string Letter =  "Letter";
        public const string SignOff = "SignOff";
        public const string From = "From";
        public const string Team ="Team";

        public const string StandardLetter = "StandardLetter";
        public const string StandardLetterHisotry = "StandardLetterHistory";  
    }
}
