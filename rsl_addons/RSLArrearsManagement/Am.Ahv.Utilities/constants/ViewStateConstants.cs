﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class ViewStateConstants
    {
        #region"Case List"

        public static string dbIndexCaseList = "DbIndexCaseList";
        public static string currentPageCaseList = "CurrentPageCaseList";
        public static string totalPagesCaseList = "TotalPagesCaseList";
        public static string totalRecordsCaseList = "TotalRecordsCaseList";
        public static string rowIndexCaseList = "RowIndexCaseList";
        public static string recordsPerPageCaseList = "RecordsPerPageCaseList";
        public static string regionIdCaseList = "RegionIdCaseList";
        public static string caseOwnerIdCaseList = "caseOwnerIdCaseList";
        public static string suburbIdCaseList = "suburbIdCaseList";
        public static string allRegionFlagCaseList = "allRegionFlagCaseList";
        public static string allCaseOwnerFlagCaseList = "allCaseOwnerFlagCaseList";
        public static string allSuburbFlagCaseList = "allSuburbFlagCaseList";
        public static string currentRecordsCaseList = "currentRecordsCaseList";
        public static string CaseHistoryPageIndex = "CaseHistoryPageIndex";
        public static string StatusTitleCaseList = "StatusTitleCaseList";
        public static string IsNISPCaseList = "IsNISPCaseList";
        public static string IsOverdueCheckList = "IsOverdueCheckList";
        public static string isFirstDetectionCaseList = "IsFirstDetectionCaseList";
        public static string SurnameCaseList = "SurnameCaseList";
        public static string CaseLetterId = "CaseLetterId";


        #endregion

        #region Activity List
        public static string LetterActivityId = "LetterActivityId";
        #endregion 

        #region "Standard Letter"
        public static string CompleteLetterObject = "CompleteLetterObject";
        public static string CompleteLetterObjectType = "CompleteLetterObjectType";
        #endregion

        #region"Initial Case Monitoring List"

        public static string dbIndexFirstDetectionList = "DbIndexFirstDetectionList";
        public static string currentPageFirstDetectionList = "CurrentPageFirstDetectionList";
        public static string totalPagesFirstDetectionList = "TotalPagesFirstDetectionList";
        public static string totalRecordsFirstDetectionList = "TotalRecordsFirstDetectionList";
        public static string rowIndexFirstDetectionList = "RowIndexFirstDetectionList";
        public static string recordsPerPageFirstDetectionList = "RecordsPerPageFirstDetectionList";
        public static string regionIdFirstDetectionList = "RegionIdFirstDetectionList";
        public static string suburbIdFirstDetectionList = "suburbIdFirstDetectionList";
        public static string allRegionFlagFirstDetectionList = "allRegionFlagFirstDetectionList";
        public static string allSuburbFlagFirstDetectionList = "allSuburbFlagFirstDetectionList";
        public static string currentRecordsFirstDetectionList = "currentRecordsFirstDetectionList";
        public static string surnameFirstDetectionList = "surnameFirstDetectionList";
        public static string address1FirstDetectionList = "address1FirstDetectionList";
        public static string tenancyidFirstDetectionList = "tenancyidFirstDetectionList";
        public static string previoustenantsFirstDetectionList = "previoustenantsFirstDetectionList";
        #endregion

        #region"Suppressed Cases"

        public static string dbIndexSuppressedCases = "DbIndexSuppressedCases";
        public static string currentPageSuppressedCases = "CurrentPageSuppressedCases";
        public static string totalPagesSuppressedCases = "TotalPagesSuppressedCases";
        public static string totalRecordsSuppressedCases = "TotalRecordsSuppressedCases";
        public static string rowIndexSuppressedCases = "RowIndexSuppressedCases";
        public static string recordsPerPageSuppressedCases = "RecordsPerPageSuppressedCases";
        public static string regionIdSuppressedCases = "RegionIdSuppressedCases";
        public static string assignedToSuppressedCases = "assignedToSuppressedCases";
        public static string suburbIdSuppressedCases = "suburbIdSuppressedCases";
        public static string allRegionFlagSuppressedCases = "allRegionFlagSuppressedCases";
        public static string allAssignedToFlagSuppressedCases = "allAssignedToFlagSuppressedCases";
        public static string allSuburbFlagSuppressedCases = "allSuburbFlagSuppressedCases";
        public static string currentRecordsSuppressedCases = "currentRecordsSuppressedCases";
        public static string CaseHistoryPageIndexSuppressedCases = "CaseHistoryPageIndexSuppressedCases";
        public static string hblAssetType = "hblAssetType";
        public static string hblCustomerStatus = "hblCustomerStatus";
        public static string rentBalanceFrom = "RentBalancefrom";
        public static string rentBalanceTo = "RentBalanceTo";

        #endregion

        #region"Action/Status"

        public static string ActionstatusId = "StatusID";
        public static string StatusRank = "StatusRank";

        #endregion

        #region"Review List"

        public static string dbIndexReviewList = "DbIndexReviewList";
        public static string currentPageReviewList = "CurrentPageReviewList";
        public static string totalPagesReviewList = "TotalPagesReviewList";
        public static string totalRecordsReviewList = "TotalRecordsReviewList";
        public static string rowIndexReviewList = "RowIndexReviewList";
        public static string currentRecordsReviewList = "currentRecordsReviewList";
        public static string statusIdReviewList = "statusIdReviewList";
        public static string overDueReviewList = "overDueReviewList";

        #endregion

        #region"Default"

        public static string caseOfficerIdDashBoard = "caseOfficerIdDashBoard";

        #endregion

        #region"Update Case"

        public static string DocumentsAttachedUpdateCase = "DocumentsAttached";
        public static string DocumentsAttachedOpenCase = "DocumentsAttachedOpenCase";
        public static string SavedLettersUpdateCase = "SavedLettersUpdateCase";
        public static string PageUrl = "PageUrl";
        public static string DataTablerecordUpdateCase = "DataTablerecord";
        public static string RecordedActionRankingUpdateCase = "ActionRanking";
        public static string RecordedActionUpdateCase = "RecordedAction";
        public static string ModalPopupPageUpdateCase = "ModalPopupPage";
        public static string CurrentActionRankUpdateCase = "CurrentActionRank";
        public static string ActionDataArrayListUpdateCase = "ActionDataArrayList";
        public static string RecordedStatusUpdateCase = "RecordedStatus";
        public static string ActionListCounterUpdateCase = "ActionListCounter";
        public static string ActionRankingIterationUpdateCase = "ActionRankingIteration";
        public static string NewStatusFlagUpdateCase = "NewStatusFlag";
        public static string RecordedStatusRankingUpdateCase = "RecordedStatusRanking";
        public static string CurrentStatusRankingUpdateCase = "CurrentStatusRanking";
        public static string StatusRankingIterationUpdateCase = "StatusRankingIteration";
        public static string IgnoredStatusUpdateCase = "IgnoredStatus";
        public static string ignoredActionIdUpdateCase = "IgnoredActionId";
        public static string NewStandardLettersAction = "NewStandardLettersAction";
        public static string AddedStandardLetterListOpenNewCase = "NewStandardLetterOpenNewCase";
        public static string StatusIdUpdateCase = "StatusIdUpdateCase";
        public static string StatusRecordedDateUpdateCase = "StatusRecordedDateUpdateCase";
        public static string LetterIdUpdateCase = "LetterId";

        #endregion

        #region"Balance List"

        public static string dbIndexBalanceList = "DbIndexBalanceList";
        public static string currentPageBalanceList = "CurrentPageBalanceList";
        public static string totalPagesBalanceList = "TotalPagesBalanceList";
        public static string totalRecordsBalanceList = "TotalRecordsBalanceList";
        public static string rowIndexBalanceList = "RowIndexBalanceList";
        public static string recordsPerPageBalanceList = "RecordsPerPageBalanceList";
        public static string currentRecordsBalanceList = "currentRecordsBalanceList";
        public static string ListCountBalanceList = "ListCountBalanceList";

        #endregion

        #region"Historical Balance List"

        public static string dbIndexHistoricalBalanceList = "DbIndexHistoricalBalanceList";
        public static string currentPageHistoricalBalanceList = "CurrentPageHistoricalBalanceList";
        public static string totalPagesHistoricalBalanceList = "TotalPagesHistoricalBalanceList";
        public static string totalRecordsHistoricalBalanceList = "TotalRecordsHistoricalBalanceList";
        public static string rowIndexHistoricalBalanceList = "RowIndexHistoricalBalanceList";
        public static string recordsPerPageHistoricalBalanceList = "RecordsPerPageHistoricalBalanceList";
        public static string currentRecordsHistoricalBalanceList = "currentRecordsHistoricalBalanceList";
        public static string ListCountHistoricalBalanceList = "ListCountHistoricalBalanceList";

        #endregion

        #region"Payment Plan"

        public static string isPaymentPlanIgnoredPaymentPlan = "IsPaymentPlanIgnored";
        public static string IgnoreReasonPaymentPlan = "IgnoreReasonPaymentPlan";
        public static string FlexiblePaymentMonths = "FlexiblePaymentMonths";
        #endregion
    }
}
