using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.constants
{
    public class UserMessageConstants
    {
        #region"Schedule Job"
        
        #region "Find Initial Case Monitoring Customers"

        public const string FindFirstDetectionCustomersScheduleJobInProgress = "Schedule job is in progress...";
        public const string GetCustomersRentParametersJobInProgress = "Cannot Initiate 'Find Initial Case Monitoring' job. 'Get Customers Rent Parameters Schedule Job' is in progress.";        
        public const string successFindFirstDetectionCustomers = "Initial Case Monitoring schedule job ran successfully. Please check the FirstDetctionList for updates.";
        public const string ErrorFindFirstDetectionCustomers = "Problem occured in Initial Case Monitoring schedule job. Please check the error log.";
        public const string ErrorInEmailFindFirstDetectionCustomers = "Problem occured in sending email notification. Please check the error log.";
        public const string EmailSubjectFindFirstDetectionCustomers = "Initial Case Monitoring schedule job has been completed.";
        public const string EmailFailureSubjectFindFirstDetectionCustomers = "Initial Case Monitoring schedule job has not been completed.";
        public const string FailureEmailBodyFindFirstDetectionCustomers = "It is to notify you that Initial Case Monitoring schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                      "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";       

        #endregion

        #region "Payment Plan Review Notification"

        public const string emailSubjectPaymentPlanNotification = "Payment Plan Review Notification.";
        public const string emailBodyPaymentPlanNotification = "This is Email Body.";

        public const string successPaymentPlanReviewNotification = "Payment plan review notification schedule job ran successfully.";
        public const string ErrorPaymentPlanReviewNotification = "Problem occured in Payment plan review notification schedule job. Please check the error log.";
        public const string EmailSubjectPaymentPlanReviewNotification = "Payment plan review notification schedule job has been completed.";
        public const string EmailFailureSubjectPaymentPlanReviewNotification = "Payment plan review notification schedule job has not been completed.";
        public const string FailureEmailBodyPaymentPlanReviewNotification = "It is to notify you that payment plan review notification schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                     "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
        #endregion

        #region "NegativeRentBalance.aspx"

        public const string successNegativeRentBalance = "Negative rent balance schedule job ran successfully.";
        public const string ErrorNegativeRentBalance = "Problem occured in negative rent balance schedule job. Please check the error log.";
        public const string EmailSubjectNegativeRentBalance = "Negative rent balance schedule job has been completed.";
        public const string EmailFailureSubjectNegativeRentBalance = "Negative rent balance schedule job has not been completed.";
        public const string FailureEmailBodyNegativeRentBalance = "It is to notify you that negative rent balance schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                     "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
        #endregion

        #region "Historical Balance List"

        public const string successFindHistoricalBalanceList = "Find historical balance list schedule job ran successfully.";
        public const string ErrorFindHistoricalBalanceList = "Problem occured in find historical balance list schedule job. Please check the error log.";
        public const string EmailSubjectFindHistoricalBalanceList = "Find historical balance list schedule job has been completed.";
        public const string EmailFailureSubjectFindHistoricalBalanceList = "Find historical balance list schedule job has not been completed.";
        public const string FailureEmailBodyFindHistoricalBalanceList = "It is to notify you that find historical balance list schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                     "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
        #endregion

        #region"Find Missed Payments"

        public const string FindMissedPaymentsScheduleJobInProgress = "'Find Missed Payments' schedule job is in progress...";
        public const string FindMissedPaymentsScheduleJobSuccess = "'Find Missed Payments' schedule job ran successfully.";
        public const string FindMissedPaymentsScheduleJobFailure = "Problem occured in 'Find Missed Payments' schedule job. Please check the error log.";
        public const string EmailSubjectFindMissedPayments = "Find Missed Payments schedule job has been completed.";
        public const string EmailFailureSubjectFindMissedPayments = "Find Missed Payments schedule job has not been completed.";
        public const string FailureEmailBodyFindMissedPayments = "It is to notify you that Find Missed Payments schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                      "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
        public const string ErrorInEmailFindMissedPayments = "Problem occured in sending email notification. Please check the error log.";
        
        #endregion

        #region"Find Over Due Action Cases"

        public const string FindOverDueActionCasesScheduleJobInProgress = "'Find OverDue Action Cases' schedule job is in progress...";
        public const string FindOverDueActionCasesScheduleJobSuccess = "'Find OverDue Action Cases' schedule job ran successfully.";
        public const string FindOverDueActionCasesScheduleJobFailure = "Problem occured in 'Find OverDue Action Cases' schedule job. Please check the error log.";
        public const string EmailSubjectFindOverDueActionCases = "Find OverDue Action Cases schedule job has been completed.";
        public const string EmailFailureSubjectFindOverDueActionCases = "Find OverDue Action Cases schedule job has not been completed.";
        public const string FailureEmailBodyFindOverDueActionCases = "It is to notify you that Find OverDue Action Cases schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                      "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
        public const string ErrorInEmailFindOverDueActionCases = "Problem occured in sending email notification. Please check the error log.";

        #endregion

        #region"Customer Rent Parameters"

        public const string ErrorInEmailCustomerRentParameters = "Problem occured in sending email notification. Please check the error log.";
        public const string ErrorCustomerRentParameters = "Problem occured in Get customers rent parameters schedule job. Please check the error log.";
        public const string successFindCustomerRentParameters = "Get customers rent parameters schedule job ran successfully.";
        public const string JobInProgressCustomerRentParameters = "Cannot Initiate new job. A job is already in progress. Please wait for the previous job to finish.";
        public const string EmailSuccessSubjectCustomerRentParameters = "Get customers rent parameters schedule job has been completed.";
        public const string EmailFailureSubjectCustomerRentParameters = "Get customers rent parameters schedule job has not been completed.";
        public const string FailureEmailBodyCustomerRentParameters = "It is to notify you that Get customers rent parameters schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                      "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";

        #endregion

        #region"Customer Rent Parameters History"

        public const string ErrorInEmailCustomerRentParametersHistory = "Problem occured in sending email notification. Please check the error log.";
        public const string ErrorCustomerRentParametersHistory = "Problem occured in Get customers rent parameters history schedule job. Please check the error log.";
        public const string successFindCustomerRentParametersHistory = "Get customers rent parameters history schedule job ran successfully.";
        public const string JobInProgressCustomerRentParametersHistory = "Cannot Initiate new job. A job is already in progress. Please wait for the previous job to finish.";
        public const string EmailSuccessSubjectCustomerRentParametersHistory = "Get customers rent parameters history schedule job has been completed.";
        public const string EmailFailureSubjectCustomerRentParametersHistory = "Get customers rent parameters history schedule job has not been completed.";
        public const string FailureEmailBodyCustomerRentParametersHistory = "It is to notify you that Get customers rent parameters history schedule job has not been completed successfully. Please check the error log file to identify the problems." +
                                      "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";

        #endregion

        #endregion

        #region"General"

        public const string topMessageDefaultSuccessMessage = "Operation performed successfully";
        public const string topMessageDefaultFailureMessage = "There is some error occured";
        public const string selectActionOpenCase = "Please select Action.";
        public const string topMessageSuccessClass = "topmessagesuccess";
        public const string topMessageFailureClass = "topmessagefailure";
        public const string NoCustomerInfoPreviewLetter = "Customer information is not available.";
        public const string InputActivity = "Please enter Activity.";
        public const string InputOutcome = "Please enter Outcome.";
        // public const string selectActionOpenCase = "Please select Action.";
        public const string stringLengthMessage = "Please enter within 150 characters.";
        public const string cachecleared = "Cache have been cleared. Please re enter all the data";
        public const string requiredFieldMessage = "Please fill the required fields.";
        public const string requiredDropDownMessage = "Please select the required fields";

        public const string updateSuccess = "Update Successful.";
        public const string InsertSuccess = "Add Successful.";

        public const string invalidSelection = "Rent Amount and Rent Parameter cannot be selected at once. Please choose one.";
        public const string CacheExpire = "The data has been expired from cache. Please re-enter the data.";

        #endregion

        #region"Dashboard"

        public const string PageLoadError = "Error has occured during page loading ";
        public const string CountingErrorFirstDetection = "Error occured in Counting Initial Case Monitorings";
        public const string CountingErrorOverDueActionAlertCount = "Error occured in Counting Overdue Actions";

        public const string ErrorMyCasesCount = "Error occured in showing count of MyCases";

        public const string DBoverdue = "Error Loading Overdue Count.";
        public const string DBvacate = "Error Loading VCate Count.";
        public const string AStenantcount = "Error Loading Tenant Count.";
        public const string ASpaymentplan = "Error Loading Payment plan.";
        public const string ASarrearsvalue = "Error Loading Arrears Value.";
        public const string ASpercentageplan = "Error Loading % plan against cases.";

        public const string AKpichart = "Error Loading Arrears KPI Chart.";

        public const string exceptionPagingLabelsReviewList = "problem in setting paging labels.";
        public const string exceptionTotalRecordsReviewList = "Problem calculating total records.";
        public const string exceptionCurrentRecordsReviewList = "Problem calculating current records.";
        public const string exceptionTotalPagesReviewList = "Problem calculating total pages.";
        public const string exceptionLoadingLookupsReviewList = "Problem in loading lookups.";
        public const string exceptionLoadingGridViewReviewList = "Problem in loading Grid View.";

        public const string LoadingLookupError = "Error occured in loading lookups.";
        public const string LoadingPageError = "Error occured in loading Page.";
        public const string LoadingRegionError = "Error occured in Loading Patch.";
        public const string LoadingSuburbError = "Error occured in Loading Scheme.";

        public const string LoadingNoticeToVacateCasesList = "Error occured in loading notice to vacate cases list.";

        #endregion

        #region"Resource"

        #region"Stages and Actions"

        public const string errorSavingAction = "Unable to save Action.";
        public const string exceptionloadingLookupsAction = "Problem in loading lookups.";
        public const string exceptionSettingStatusAction = "Problem in setting stage.";
        public const string exceptionLoadingAction = "Problem in loading action.";
        public const string exceptionSavingAction = "Problem in saving action.";
        public const string exceptionRegeneratingRanking = "Problem in regenerating ranking.";
        public const string selectstandardLetter = "Please select any Letter to remove.";        
        public const string ActionTitleLimit = "Title should be less than 30 characters.";

        public const string MultipleRankMessage = "Cannot save multiple stage with the same rank";
        public const string MultipleRankUpdateMessage = "Cannot update the stage with the different rank";
        public const string FirstDetectionMessage = "Multiple Initial Case Monitoring stage not allowed";
        public const string MultipleActionRankMessage = "Cannot save multiple actions with the samer rank";
        public const string StatusAlreadyAssignedMessage = "Cannot update the rank of the assigned stage.";
        public const string StatusUnAssignedRankMessage = "Cannot update the stage with unassigned rank.";

        public const string exceptionLoadingLookupsStatus = "Problem in loading lookups.";
        public const string exceptionSavingStatus = "Problem in saving stage.";
        public const string exceptionLoadingStatus = "Problem in loading stage.";
        public const string exceptionResetPageStatus = "Problem in resetting page.";
        public const string exceptionEditStatus = "Problem in editing stage.";
        public const string exceptionGettingListStatus = "Problem in getting stage list.";

        public const string exceptionLoadingLookupsFirstDetection = "Problem in loading lookups.";
        public const string exceptionLoadingFirstDetection = "Problem in loading Initial Case Monitoring.";
        public const string exceptionAddDetection = "Problem in adding Initial Case Monitoring.";
        public const string AAInvalidRank = "Cannot update action with different Rank.";
        public const string AAInvalidUpdate = "Error updating action";
        public const string AAInvalidaction = "Cannot save action against unassigned rank";
        public const string assignedActionUpdateError = "Cannot update the rank of assigned action.";
        public const string ASInvalidUpdate = "Error updating stage";

        #endregion

        #region"Activity"

        public const string exceptionActivityTitle = "Problem in getting activity information.";
        public const string exceptionloadingActivityList = "Problem in loading activity list.";
        public const string exceptionloadingActivity = "Problem in loading activities.";
        public const string exceptionSavingActivity = "Problem in saving activity.";

        #endregion

        #region"Add Case Worker"

        public const string resourceAdded = "Resource added successfully.";
        public const string resourceUpdated = "Resource updated successfully.";
        public const string selectRegion = "Please select a 'Patch'.";
        public const string selectSuburb = "Please select a 'Scheme'.";
        public const string selectStreet = "Please select a 'Street'.";

        public const string ACPageload = "Problem Loading Add Case Worker.";
        public const string ACRegionddl = "Problem Loading Region Drop Down.";
        public const string ACSuburbddl = "Problem Loading Suburb Drop Down.";
        public const string ACStreetddl = "Problem Loading Street Drop Down.";
        public const string ACRegionAddbtn = "Problem Adding Patch.";
        public const string ACSuburbAddbtn = "Problem Adding Scheme.";
        public const string ACStreetAddbtn = "Problem Adding Street.";
        public const string ACSuburbRemovebtn = "Problem Removing Scheme.";
        public const string ACStreetRemovebtn = "Problem Removing Street.";
        public const string ACRegionRemovebtn = "Problem Removing Patch.";
        public const string ACFindbtn = "Problem finding user.";
        public const string ACTeamddl = "Problem Loading Team DDL.";
        public const string ACSavebtn = "Problem Saving Case Worker.";
        public const string ACPopulate = "Problem Populating the Case Worker.";
        public const string ACclearsession = "Problem in Clearing Session Values.";
        public const string ACConvertList = "Problem Converting List.";
        public const string ACaddresource = "problem Adding Resource.";
        public const string ACresetpage = "Problem Reseting Page.";
        public const string ACinitlookup = "Problem Loading Lookups.";
        public const string ACIspresent = "Problem Adding Patch Scheme.";

        public const string regionSuburbAlreadyAddedCaseWorker = "The Item is alreay added.";

        #endregion

        #region"Load Users"

        public const string exceptionEditingCaseWorker = "Problem in editing case worker.";
        public const string exceptionUpdatingCaseWorker = "Problem in updating case worker.";
        public const string exceptionSettingPageLabelsCaseWorker = "Problem in setting page labels.";
        public const string exceptionDataCaseWorker = "Problem in loading case workers.";
        public const string exceptionUserTypeCaseWorker = "Problem in loading case worker types.";
        public const string exceptionUpdateResourceCaseWorker = "Problem in updating resource.";
        public const string exceptionrecordCountCaseWorker = "Problem in getting records count.";

        #endregion

        #region"Outcomes"

        public const string exceptionloadingOutcomes = "Problem in loading 'Outcomes'.";
        public const string exceptionSettingTotalPagesOutcomes = "Problem in setting total pages.";
        public const string exceptionSettingCurrentRecordsOutcomes = "Problem in setting current records.";
        public const string exceptionSavingOutcome = "Problem in saving outcome.";
        public const string exceptionSettingOutcome = "Problem in setting outcome.";

        #endregion

        #region"Letters/Templates"

        public const string errorActionDropDownCreateLetter = "Problem in populating Action Dropdown.";
        public const string errorFromDropDownCreateLetter = "Problem in populating From Dropdown.";
        public const string lettersaveSuccessCreateLetter = "Letter saved successfully.";
        public const string selectStatusCreateLetter = "Please select 'Stage'.";
        public const string selectActionCreateLetter = "Please select 'Action'.";
        public const string enterTitleCreateLetter = "Please enter 'Title'.";
        public const string enterCodeCreateLetter = "Please enter 'Code'.";
        public const string enterThirdPartyContact = "Please enter Third Party Contact Address Details.";
        public const string errorSavingLetterCreateLetter = "Problem in saving letter.";
        public const string selectSignOffCreateLetter = "Please select 'SignOff'.";
        public const string selectTeamCreateLetter = "Please select 'Team'.";
        public const string selectFromCreateLetter = "Please select 'From'.";
        public const string errorEditLetterCreateLetter = "Problem in editing letter.";
        public const string updateLetterCreateLetter = "Letter updated successfully.";

        public const string LoadingLettersError = "Erorr Occured in Loading Letters.";
        public const string MoreTemplateError = "Erorr Occured in Adding More.";
        public const string EditingError = "Erorr Occured in Editing.";

        public const string previewError = "Error occured in previweing letter";

        #endregion

        #region"Page Base / Bridge"
                
        public const string exceptionSettingSession = "problem in setting user session values.";
        public const string InvalidUserMessage = "User must be case officer or case manager to enter into arrears management system";

        #endregion

        #endregion

        #region"Case Management"

        #region"Active Case List"

        public const string noUsersFound = "No Case Managers or Case Officers found.";
        public const string exceptionSuburbAndCaseOwnedBy = "Problem loading data against 'Patch'.";
        public const string exceptionAgainstSuburb = "Problem loading data against 'Scheme'.";
        public const string exceptionButtonSearch = "Problem loading data against Search.";
        public const string exceptionRecordsPerPage = "Problem loading data with different page size.";
        public const string exceptionLoadingGridView = "Problem loading data.";
        public const string exceptionLoadingRegion = "Problem loading Patch.";
        public const string exceptionTotalPages = "Problem calculating total pages.";
        public const string exceptionCurrentRecords = "Problem calculating current records.";
        public const string exceptionTotalRecords = "Problem calculating total records.";
        public const string exceptionCheckBoolean = "Problem checking boolean values.";
        public const string exceptionDashboardSearchCriteria = "Problem in loading dashboard search criteria.";

        public const string CLquery = "Invalid Case";

        #endregion

        #region"Initial Case Monitoring List"

        public const string noUsersFoundFirstDetection = "No Case Managers or Case Officers found.";
        public const string exceptionAgainstSuburbFirstDetection = "Problem loading data against 'Scheme'.";
        public const string exceptionButtonSearchFirstDetection = "Problem loading data against Search.";
        public const string exceptionRecordsPerPageFirstDetection = "Problem loading data with different page size.";
        public const string exceptionLoadingGridViewFirstDetection = "Problem loading data.";
        public const string exceptionLoadingRegionFirstDetection = "Problem loading Patch.";
        public const string exceptionTotalPagesFirstDetection = "Problem calculating total pages.";
        public const string exceptionCurrentRecordsFirstDetection = "Problem calculating current records.";
        public const string exceptionTotalRecordsFirstDetection = "Problem calculating total records.";
        public const string exceptionGetDaysFirstDetection = "Problem in retrieving days information.";
        public const string PreviousTenantsFirstDetection = "Previous Tenants List";

        #endregion

        #region"Payment Plan"
        public const string currencyIsNotInCorrectFormat = "Please enter a valid";
        public const string exceptingSavingPaymentInstallments = "Problem saving Payment Installments.";
        public const string paymentPlanSuccess = "Payment Plan created successfully.";
        public const string paymentPlanStartDate = "Please provide Start Date.";
        public const string paymentPlanAmountToBeCollected = "Please provide Agreement Amount.";
        public const string paymentPlanAmountToBeCollectedgreater = "Please provide Agreement Amount less than or equal to Owed to BHA.";
        public const string paymentPlanAmountToBeCollectednegative = "Please provide Valid Agreement Amount .";
        public const string paymentPlanValidAmount = "Please provide valid amount.";
        public const string paymentPlanTypeSelection = "Please select 'Payment Plan Type'.";
        public const string paymentPlanReviewDate = "Please provide the review date.";
        public const string paymentPlanFirstCollection = "Please provide the first collection date.";
        public const string paymentPlanFirstCollectionFuture = "Please provide first collection date in future.";
        public const string paymentPlanFirstCollectionDateBeforeStartDate = "'First Collection Date' should come after or equal to the 'Start Date'.";
        public const string paymentPlanValidFirstCollectionDate = "Please provide valid 'First Collection Date'.";
        public const string paymentPlandatePayment = "Please provide payment date in future.";
        public const string paymentPlanStartDateFuture = "Start date cannot be choosed from past.";
        public const string paymentPlanValidStartDate = "Please provide valid 'Start Date'.";
        public const string paymentPlanReviewDateFuture = "Please provide 'Review Date' in future.";
        public const string paymentPlanValidReviewDate = "Please provide valid 'Review Date'.";
        public const string paymentPlanReviewDateBeforeStartDate = "'Review Date' should come after or equal to the 'First Collection Date'.";
        public const string paymentPlanAmountGreaterThanrentBalance = "'Agreement Amount' is greater than 'Rent Balance'.";
        public const string paymentPlanAmountEqualRentBalance = "'Agreement Amount' should be equal to 'Rent Balance'.";
        public const string paymentPlanDifferentPaymentDate = "Please provide different payment dates.";
        public const string paymentPlanValidDate = "Please provide valid date.";
        public const string paymentPlanAlreadyExist = "An unfinished payment plan already exist against the tenant.";
        public const string paymentPlanWeeklyRentAmount = "Please provide valid weekly rent amount.";
        public const string paymentValidAmount = "Please enter valid amount.";
        public const string paymentAmountgretaterThanZero = "should greater than '0'.";
        public const string paymentProvideFrequency = "Please select 'Frequency'.";
        public const string paymentArrearsBalance = "Please provide Arrears Balance.";
        public const string paymentRentPayable = "Please provide Rent Payable.";
        public const string paymentInvalidPayment1Date = "Please enter valid date for 1st Payment.";
        public const string paymentInvalidFlexiblePayments = "Please enter valid flexible payments.";
        public const string CheckPaymentPlanPrintPayments = "Problem in checking previous payment plan.";
        public const string exceptingSavingPaymentPlan = "Problem saving Payment Plan.";
        public const string exceptingInInstallments = "Problem creating Payment Plan installments.";
        public const string exceptingInInstallmentsDate = "Problem creating Payment Plan installments date.";
        public const string UpdateSuccessfulPaymentPlan = "Update Successfull.";
        public const string problemInUpdatePaymentPlan = "Problem in payment plan update.";
        public const string problemLoadingPaymentPlan = "Problem in loading payment plan.";
        public const string EnterIgnoreReasonPaymentPlan = "Please enter a reason.";
        public const string CaseNotOpenPaymentPlan = "A case must be opened before creating a payment plan.";
        public const string InvalidArrearsBalancePaymentPlan = "Invalid Arrears Balance. Please enter valid Arrears Balance.";

        public const string paymentPlanFirstPayment = "Please provide the 1st payment first.";
        public const string paymentPlanOverFlexibleAmounts = "Flexible payments exceeds Rent Balance.";
        public const string paymentPlanAmountGreaterThanRemainingRentBalance = "'Agreement Amount' is greater than remaining 'Rent Balance'.";
        public const string paymentPlanAmountEqualRemainingRentBalance = "'Agreement Amount' should be equal to remaining 'Rent Balance'.";
        public const string paymentPlanReviewDateBefore1stPayment = "'Review Date' should come after or equal to the '1st Payment'.";
        public const string paymentPlanUnableToCreate = "Unable to create 'Payment Plan'.";

        public const string RPPusersummary = "Problem Loading User Summary.";
        public const string RPPpaymentplan = "There is no Payment Plan against this tenant.";
        public const string RPPpaymentschedule = "There is no Payment Schedule against this tenant.";

        public const string ACWresourceexist = "Resource with the same employeeId already exists.";
        public const string ACWaddregion = "Please add a Patch.";

        #endregion

        #region"Add Case Activity"

        public const string noActivityTypeSelected = "Please Select the Actitvity Type";
        public const string noTitleEntered = "Please enter the title";
        public const string noNotesEntered = "Please enter the Notes";
        public const string noDocumentSelected = "Please choose the document";
        public const string noOutcomeSelected = "Please select the outcome";
        public const string noReasonEntered = "Please enter the Reason";
        public const string invalidImageType = "Only MS Word,Excel or PDF are allowed";
        public const string ActivitySavedMassage = "Activity has been saved successfully";
        public const string noReferralDateSelected = "Please select the Date of Referral";
        public const string noNameSelected = "Please select the Name";
        public const string noFollowupdateSelected = "Please select the Followup Date";
        public const string noTeamSelected = "Please Select the Team";
        public const string noOrgSelected = "Please select the organisation";
        public const string noContactSelected = "Please select the contact";
        public const string noEmail = "Please enter the email";

        public const string InvalidReferralDate = "Date of Referral,Invalid value or Incorrect Date Format";
        public const string InvalidFollowupDate = "Followup Date,Invalid value or Incorrect Date Format";
        public const string errorPageLoading = "Error occured in loading page";
        public const string errorSavingCaseActivity = "Error occured in Saving Activity";
        public const string CaseActivityReferralMsg = "Referral has been saved successfully";
        public const string CaseActivityReferralSavingError = "Error occured in saving Referral";
        public const string ReferralViewError = "Error occured in showing Referral View";
        public const string ErrorActionStatus = "Error occured in showing action and stage of current opened case";
        public const string ErrorLoadingLookups = "Error occured in loading lookups";
        public const string ErrorLoadingNames = "Error occured in loading Names";
        public const string ErrorLoadingContacts = "Error occured in loading contacts";
        public const string EmailSubjectExternalReferral = "Confirmation of Referral (DO NOT REPLY)";
        public const string EmailError = "Please enter email in correct format like example@example.com";
        public const string ActivityFileSizeMessage = "More than 2MB file size is not allowed";
        public const string ErrorSendingEmailAddCaseActivity = "Problem in sending email due to which 'External Referral' could not be saved.";

        public const string EmailFormat = "&lt;div id=&quot;refHTML&quot;&gt;This is a notification of a referral that has been made to you in RSLmanager.&lt;br /&gt;"
+ "&lt;br /&gt;"
+ "Please login now and access the clients case within the Arrears Management area.&lt;br /&gt;"
+ "&lt;br /&gt;"
+ "Date of Referral: [ReferralDate]   &lt;br /&gt;"
+ "  Follow Up Date: [FollowUpDate]  &lt;br /&gt;"
+ " Customer Number: [CustomerNumber] &lt;br /&gt;"
+ "           Notes: [Notes]           &lt;br /&gt;"
+ "&lt;br /&gt;"
+ "The customer is aware of the referral&lt;br /&gt;"
+ "&lt;br /&gt;"
+ "This email and any files transmitted with it are confidential and intended solely for the use of the &lt;br /&gt;"
+ "individual or entity to whom they are addressed. If you have received this email in error please &lt;br /&gt;"
+ "notify the system manager. Please note that any views or opinions presented in this email are &lt;br /&gt;"
+ "solely those of the author and do not necessarily represent those of the company. Finally, the &lt;br /&gt;"
+ "recipient should check this email and any attachments for the presence of viruses. The company &lt;br /&gt;"
+ "accepts no liability for any damage caused by any virus transmitted by this email.&lt;/div&gt;"
+ "&lt;input type=&quot;hidden&quot; id=&quot;gwProxy&quot;&gt;&lt;!--Session data--&gt;&lt;/input&gt;&lt;input type=&quot;hidden&quot; id=&quot;jsProxy&quot; onclick=&quot;if(typeof(jsCall)==&#39;function&#39;){jsCall();}else{setTimeout(&#39;jsCall()&#39;,500);}&quot; /&gt;"
+ "&lt;div id=&quot;refHTML&quot;&gt;&amp;nbsp;&lt;/div&gt;";

        #endregion

        #region"View Activities"

        public const string exceptionloadingViewActivities = "problem in loading 'Activities'.";
        public const string ActivityLoadError = "Activity couldn't be shown.";

        #endregion

        #region"User Info Summary"

        public const string exceptingGettingUserInfo = "Problem retrieving customer information.";
        public const string exceptiongettinglastPayment = "Problem retrieving customer last CPay record.";
        public const string NoHBRecordLastPayment = "There is no HB record.";

        public const string RetrievingUserSummary = "No User Summary Available.";
        public const string LoadingUserSummary = "Error Loading User Summary.";

        public const string NoCPAYRecordLastPayment = "No CPay Record";

        #endregion

        #region"Open Case"

        public const string exceptingSavingCase = "Problem in opening a case.";
        public const string exceptingSavingDocuments = "Problem in saving Documents.";
        public const string caseOpenSuccess = "Case opened successfully.";
        public const string selectCaseManagerCaseOpen = "Please Select 'Case Manager'.";
        public const string selectCaseOfficerCaseOpen = "Please Select 'Case Officer'.";
        public const string futureActionDateCaseOpen = "Please Select 'Action Review' in future.";
        public const string futureStatusDateCaseOpen = "Please Select 'Stage Review' in future.";
        public const string fileSizeCaseOpen = "Please select file less than 10MB.";
        public const string exceptionUploadFileCaseOpen = "Problem in uploading a file.";
        public const string AlreadyExistCaseOpen = "An unfinished case is already exist against the tenant.";
        public const string exceptionActionReview = "Problem in populating 'Action Review'.";
        public const string exceptionStatusReview = "Problem in populating 'Stage Review'.";
        public const string exceptionPopulatingLookups = "Problem in populating lookups.";
        public const string exceptionPopulatingDocuments = "Problem in populating documents.";
        public const string selectOptionRemoveDocuments = "Please select an item to remove.";
        public const string selectOptionEditDocuments = "Please select an item to edit.";
        public const string selectStandardLettersEditDocuments = "Please select 'Standard Letter' to edit.";
        public const string selectDocuments = "Please Add Standard Letters.";
        public const string uploadDocuments = "Problem in Document Upload.";
        public const string SessionExpiredOpenCase = "StandardLetter session has been expired.";

        #endregion

        #region"Update Case"

        public const string futureDateUpdateCase = "Please select date in future ";
        public const string ValidDateUpdateCase = "Please provide valid date ";
        public const string selectCaseManagerUpdateCase = "Please select 'Case Manager'.";
        public const string selectCaseOfficerUpdateCase = "Please select 'Case Officer'.";
        public const string enterRecoveryAmountUpdateCase = "Please enter 'Recovery Amount'.";
        public const string enterNotesUpdateCase = "Please enter 'Notes'.";
        public const string txtNotesUpdateCase = "Notes length should be equal to or less than 1000.";
        public const string enterStatusReviewUpdateCase = "Please enter 'Stage Review'.";
        public const string enterActionReviewUpdateCase = "Please enter 'Action Review'.";
        public const string enterNoticeIssueUpdateCase = "Please enter 'Notice Issue Date'.";
        public const string enterNoticeExpiryUpdateCase = "Please enter 'Notice Expiry Date'.";
        public const string selectOutcomeUpdateCase = "Please select 'Outcome'.";
        public const string successUpdateCase = "Case updated successfully.";
        public const string suppressSuccessUpdateCase = "Case suppressed successfully.";
        public const string SuppressReviewDateUpdateCase = "Please Select date within three months.";
        public const string SuppressReviewInvalidDateUpdateCase = "Enter Valid Date";
        public const string SuppressCaseRequiredFieldsUpdateCase = "Please Enter Reason and Select a date within three months.";
        public const string exceptionLoadUpdateCase = "problem in loading case.";
        public const string exceptionInUpdateCase = "problem in updating case.";
        public const string documentAttachedUpdateCase = "A document is already attached. Please remove the attached document.";
        public const string exceptionIsSuppressUpdateCase = "Problem in Is Suppress Check.";
        public const string SessionExpiredUpdateCase = "StandardLetter session has been expired. Please update the case again.";
        public const string ActionRecordedTitleUpdateCase = "Recorded Action Detail";
        public const string ActionIgnoredTitleUpdateCase = "Ignored Action Reason";
        public const string ActionModalPopupErrorUpdateCase = "Please enter detail.";
        public const string ActionModalPopupIgnoreErrorUpdateCase = "Please enter reason.";
        public const string PreviousActionUpdateCase = "Previous 'Action' cannot be recorded.";
        public const string proceduralActionLeftUpdateCase = "A procedural action is not recorded yet in the last stage. Please record the procedural action.";

        #endregion

        #region"Case Details"

        public const string NoCaseDetail = "No More Case Details";
        public const string CaseDetail = "No Case Details Avialable";

        public const string CDlbprevious = "Problem Populating Prvious Data";
        public const string CDpageload = "Problem Loading Page";
        public const string CDlbnext = "Problem Populating Next Data";
        public const string CDLbtn = "Problem Populating Data";

        public const string CDcasehistory = "Problem Loading Case History";
        public const string CDsethistorylist = "Problem Setting History Values";
        public const string CDenabledisable = "Problem Number Navigations";
        public const string CDpopulatecase = "Problem Populating Case Details";
        public const string CDsetpageindex = "Problem Setting Page Index";

        #endregion

        #region"Case History/Summary"
                
        public const string CHpageload = "Problem Loading Page";
        public const string CHimagebutton = "Problem Loading Docs";
        public const string CHselectbutton = "Problem Loading Record";
        public const string CHignoresavebtn = "Problem Ignoring Case";
        public const string CHsupressbtn = "Problem Supressing Case";
        
        public const string CHloadgrid = "Problem Loading Grid";
        public const string CHdatacomparison = "Problem Comparing Data";

        public const string ModelPopupTitleTriggerCaseHistory = "The next Stage and Action details are outlined below: ";
        public const string ModelPopupNoNextActionCaseHistory = "No next action.";
        public const string ModelPopupTitleFollowUpCaseHistory = "The follow up action details are outlined below: ";

        public const string CHexceptionactioncount = "Problem Updating Action count.";
        public const string CHexceptionignoreactioncount = "Problem Updating Ignore Action Count.";

        #endregion

        #endregion

        #region"Reports"

        #region"Historical Balance List"

        public const string LoadingHistoricalBalanceList = "Error occured in loading historical balance list.";
        public const string exceptionLoadingArrearsBalanceReport = "Problem in loading 'Arrears Balance Report'.";

        #endregion

        #region"Payment Plan"

        public const string RetrievingPaymentschedule = "No Payment Schedule Available against this tenant.";
        public const string LoadingPaymentSchedule = "Error occured while loading payment schedule.";

        public const string RetrievingPaymentPlan = "There is no Payment Plan against this tenant.";
        public const string LoadingPaymentPlan = "Error occured while loading payment plan.";

        #endregion

        #region"Case Details"

        public const string RetrievingCaseActivities = "No Case Activities Available.";
        public const string LoadingCaseActivities = "Error Loading in Case Activities.";

        #endregion

        #endregion          
    }
}
