using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.Utilities.utitility_classes
{
    public class FileOperations
    {
        #region Get Cur Url Absolute Path

        public string getCurUrlAbsolutePath()
        {     
            
            string absoulteUrlPath = HttpContext.Current.Request.Url.AbsolutePath;            
            return absoulteUrlPath;
        }

        #endregion

        #region"Create Url"

        public static string CreateUrl(string tenant, bool isCaseOpen, string caseId)
        {
            string path = string.Empty;
            string[] arr = HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"].Split('/');
            string vDir = arr[1];
            if (isCaseOpen)
            {
                path = ConfigurationManager.AppSettings["httpPath"] + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/" + vDir + "/" + PathConstants.CaseDetailsBalanceListReport + "?cmd=scase&cid=" + caseId + "&tid=" + tenant;
            }
            else
            {
                path = ConfigurationManager.AppSettings["httpPath"] + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/" + vDir + "/" + PathConstants.openNewCaseBalanceListReport + "?cmd=open&id=" + tenant;
            }
            return path;
        }

        #endregion

        #region Get Url File Info
        public System.IO.FileInfo GetUrlFileInfo(string absoulteUrlPath)
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(absoulteUrlPath);
            return fileInfo;
        }
        #endregion

        #region Get File Name
        public string getFileName(System.IO.FileInfo fileInfo)
        {
            string fName = fileInfo.Name;
            return fName;
        }
        #endregion

        #region Get Cur Url File Name
        public string getCurUrlFileName()
        {
            string absoluteUrlPath = getCurUrlAbsolutePath();
            System.IO.FileInfo fileInfo = GetUrlFileInfo(absoluteUrlPath);
            string fName = this.getFileName(fileInfo);

            return fName;

        }
        #endregion 
        #region Get  Url File Name
        public string GetUrlFileName(string URL)
        {

            System.IO.FileInfo fileInfo = GetUrlFileInfo(URL);
            string fName = this.getFileName(fileInfo);

            return fName;

        }
        #endregion 

        #region"Change File Name"

        public static string ChangeFileName(string name, int id)
        {
            //string[] split;
            //split = name.Split('.');
            string fullName = Path.GetFileNameWithoutExtension(name) + "_" + id.ToString() + Path.GetExtension(name);
            return fullName;
            
        }

        #endregion        

        #region"Check File Extension"

        public static bool CheckFileExtension(string fileName)
        {
            string fileExt = Path.GetExtension(fileName);
            if  (!fileExt.Equals(".pdf") && !fileExt.Equals(".doc") && !fileExt.Equals(".docx") &&
                !fileExt.Equals(".xls") && !fileExt.Equals(".xlsx") && !fileExt.Equals(".png") &&
                !fileExt.Equals(".jpg"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion       

        #region"Upload File"

        public static void UploadFile(string name, int id)
        {
            string destinationPath = string.Empty;
            destinationPath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"];
            string newFile = string.Empty;

            //   FlUpload.SaveAs(destinationPath);
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }
            newFile = destinationPath + ChangeFileName(name, id);
            File.Move(destinationPath + name, newFile);
            File.Delete(destinationPath + name);
        }

        #endregion
    }
}
