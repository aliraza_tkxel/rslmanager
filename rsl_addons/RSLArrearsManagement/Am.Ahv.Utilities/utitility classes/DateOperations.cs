﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Am.Ahv.Utilities.utitility_classes
{
    public class DateOperations
    {
        #region"Add Date"

        /// <summary>
        /// This function is used to add a period of time 
        /// in the date and returns a new date.
        /// </summary>
        /// <param name="Value"> Total number of period.</param>
        /// <param name="Duration"> Weeks, Days, Months, Years.</param>
        /// <param name="DateToAdd">Date in which the duration and value will be added.</param>
        /// <returns> New Date </returns>
        public static DateTime AddDate(int Value, String Duration, DateTime DateToAdd)
        {
            try
            {
                DateTime returndate = DateTime.MinValue;
                switch (Duration)
                {
                    case "Days":
                        returndate = DateToAdd.AddDays(Value);
                        break;
                    case "Weeks":
                        returndate = DateToAdd.AddDays(Value * 7);
                        break;
                    case "Months":
                        returndate = DateToAdd.AddMonths(Value);
                        break;
                    case "Years":
                        returndate = DateToAdd.AddYears(Value);
                        break;

                }
                return returndate;
            }
            catch (ArgumentOutOfRangeException argumentor)
            {
                throw argumentor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Overload of above function. 
        /// </summary>
        /// <param name="period"></param>
        /// <param name="duration"></param>
        /// <param name="firstCollection"></param>
        /// <returns></returns>
        public static DateTime AddDate(double period, string duration, DateTime firstCollection)
        {
            try
            {
                DateTime returnDate = new DateTime();
                switch (duration)
                {
                    case "Weekly":
                        returnDate = firstCollection.AddDays((period) * 7);
                        break;

                    case "Daily":
                        returnDate = firstCollection.AddDays((period));
                        break;

                    case "Fortnightly":
                        returnDate = firstCollection.AddDays((period) * 14);
                        break;

                    case "Monthly":
                        returnDate = firstCollection.AddMonths(Convert.ToInt32((period)));
                        break;
                    case "Full Amount":
                        returnDate = firstCollection;
                        break;
                }
                return returnDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// It performs the same functionality as the above two functions provide.
        /// The only difference is that it includes the current date. 
        /// for example:
        /// if we have to add a period of 9 months in anydate inclusive todays date then we will
        /// use this function.
        /// </summary>
        /// <param name="period"></param>
        /// <param name="duration"></param>
        /// <param name="firstCollection"></param>
        /// <returns></returns>
        public static DateTime AddRegularDate(double period, string duration, DateTime firstCollection)
        {
            try
            {
                DateTime returnDate = new DateTime();
                switch (duration)
                {
                    case "Weekly":
                        returnDate = firstCollection.AddDays((period - 1) * 7);
                        break;

                    case "Daily":
                        returnDate = firstCollection.AddDays((period - 1));
                        break;

                    case "Fortnightly":
                        returnDate = firstCollection.AddDays((period - 1) * 14);
                        break;

                    case "Monthly":
                        returnDate = firstCollection.AddMonths(Convert.ToInt32((period - 1)));
                        break;
                    case "Full Amount":
                        returnDate = firstCollection;
                        break;
                }
                return returnDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Date Difference "
        /// <summary>
        /// Determines whether the two dates are different or not.
        /// </summary>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        public bool DateDifference(string startDate, string endDate)
        {

            if (int.Parse(endDate.Substring(6, 4)) > int.Parse(startDate.Substring(6, 4)))
            {
                return true;
            }
            else if (int.Parse(endDate.Substring(6, 4)) >= int.Parse(startDate.Substring(6, 4)))
            {
                if (int.Parse(endDate.Substring(3, 2)) > int.Parse(startDate.Substring(3, 2)))
                {
                    return true;
                }
                else if (int.Parse(endDate.Substring(3, 2)) >= int.Parse(startDate.Substring(3, 2)))
                {
                    if (int.Parse(endDate.Substring(0, 2)) >= int.Parse(startDate.Substring(0, 2)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region "Is Valid Uk Date "

        /// <summary>
        /// Determines whether the date is in valid UK format or not.
        /// </summary>
        /// <param name="date"> Date to be checked.</param>
        /// <returns> bool</returns>

        public static bool isValidUkDate(string date)
        {
            try
            {
                if (date.Length >= 10 && date.Length <= 10)
                {
                    if ((date.Substring(2, 1) == "/") && (date.Substring(5, 1) == "/"))
                    {
                        if (date.Substring(3, 2) == "09" || date.Substring(3, 2) == "04" || date.Substring(3, 2) == "06" || date.Substring(3, 2) == "11")
                        {
                            if (int.Parse(date.Substring(0, 2)) > 0 && int.Parse(date.Substring(0, 2)) < 31)
                            {
                                if (int.Parse(date.Substring(6, 4)) > 1753)
                                {
                                    return true;
                                }
                            }
                        }
                        else if (date.Substring(3, 2) == "02")
                        {
                            if (int.Parse(date.Substring(6, 4)) > 1753)
                            {
                                if (int.Parse(date.Substring(6, 4)) % 4 == 0)
                                {
                                    if (int.Parse(date.Substring(0, 2)) > 0 && int.Parse(date.Substring(0, 2)) < 30)
                                    {
                                        return true;
                                    }
                                    else
                                    {
                                        if (int.Parse(date.Substring(0, 2)) > 0 && int.Parse(date.Substring(0, 2)) < 29)
                                        {
                                            return true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (int.Parse(date.Substring(0, 2)) > 0 && int.Parse(date.Substring(0, 2)) < 29)
                                    {
                                        return true;
                                    }
                                }

                            }
                        }
                        else if (date.Substring(3, 2) == "01" || date.Substring(3, 2) == "03" || date.Substring(3, 2) == "05" || date.Substring(3, 2) == "07" || date.Substring(3, 2) == "08" || date.Substring(3, 2) == "10" || date.Substring(3, 2) == "12")
                        {
                            if (int.Parse(date.Substring(6, 4)) > 1753)
                            {
                                if (int.Parse(date.Substring(0, 2)) > 0 && int.Parse(date.Substring(0, 2)) < 32)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region"Convert String To Date"

        /// <summary>
        /// Converts any date in string data type to its valid DateTime data type.
        /// </summary>
        /// <param name="date">date to be converted.</param>
        /// <returns>DateTime</returns>

        public static DateTime ConvertStringToDate(string date)
        {
            int day = Convert.ToInt32(date.Substring(0, 2));
            int month = Convert.ToInt32(date.Substring(3, 2));
            int year = Convert.ToInt32(date.Substring(6, 4));
            DateTime dateResult = new DateTime(year, month, day);
            return dateResult;
        }

        #endregion

        #region"Future Date Validation"

        /// <summary>
        /// Determines whether the date is in future or not.
        /// </summary>
        /// <param name="date">date to be checked.</param>
        /// <returns>bool</returns>

        public static bool FutureDateValidation(DateTime date)
        {
            string currentDate = DateTime.Now.ToShortDateString();
            DateTime date2 = Convert.ToDateTime(currentDate);
            int result = date.CompareTo(date2);// DateTime.Compare(date, currentDate);
            if (result < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"Future Date Validation"

        /// <summary>
        /// Determines whether the date is in future or not.
        /// </summary>
        /// <param name="date">date to be checked.</param>
        /// <returns>bool</returns>

        public static bool todayAndFutureDateValidation(DateTime date)
        {
            string currentDate = DateTime.Now.ToShortDateString();
            DateTime date2 = Convert.ToDateTime(currentDate);
            int result = date.CompareTo(date2);// DateTime.Compare(date, currentDate);
            if (result < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"get Cultured Date Time"

        /// <summary>
        /// Returns the US cultured date time.
        /// </summary>
        /// <param name="date">date to be converted.</param>
        /// <returns>US Cultured DateTime</returns>

        public static DateTime GetCulturedDateTime(string date)
        {
            CultureInfo info = new CultureInfo("en-GB");
            CultureInfo infoUS = new CultureInfo("en-US");
            DateTime dt = Convert.ToDateTime(date, info);
            return Convert.ToDateTime(dt, infoUS);
        }

        /// <summary>
        /// Converts UK Cultured Date Time.
        /// </summary>
        /// <param name="date">date to be converted.</param>
        /// <returns>UK Cultured DateTime</returns>

        public static DateTime GetUKCulturedDateTime(string date)
        {
            CultureInfo info = new CultureInfo("en-GB");
            CultureInfo infoUS = new CultureInfo("en-US");
            DateTime uk = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dtUS = Convert.ToDateTime(date, infoUS);
            DateTime dt = Convert.ToDateTime(date, info);
            return dt;
        }

        #endregion

        #region"Compare Dates"

        /// <summary>
        /// Compares whether the two dates are equal or not.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns>bool</returns>

        public static bool CompareDates(DateTime date1, DateTime date2)
        {
            if (date1.Day.Equals(date2.Day) && date1.Month.Equals(date2.Month) && date1.Year.Equals(date2.Year))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

      #region"Is Past date"

      public static bool IsPastDate(string value)
      {
          DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
          DateTime date = DateOperations.ConvertStringToDate(value);

          TimeSpan res = dt.Subtract(date);
          if (res.Days < 0)
          {
              return false;
          }
          else
          {
              return true;
          }
      }

      #endregion

      #region"Subtract Two Dates"

      public static int SubtractTwoDates(DateTime _dateToSubtract, DateTime _dateFromSubtract)
      {
          DateTime dateToSubtract = new DateTime(_dateToSubtract.Year, _dateToSubtract.Month, _dateToSubtract.Day);
          DateTime dateFromSubtract = new DateTime(_dateFromSubtract.Year, _dateFromSubtract.Month, _dateFromSubtract.Day);
          TimeSpan res = dateFromSubtract.Subtract(dateToSubtract);
          return res.Days;
      }

      #endregion

        #region"Get Month Name"

      public static string GetMonthName(int Id)
      {
          string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;
          return names[Id];
      }

        #endregion

      public static bool DateDifference(char p, char p_2)
      {
          throw new NotImplementedException();
      }
    }
}
