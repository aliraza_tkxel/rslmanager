﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Utilities.constants;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.Caching;
using System.Globalization;

namespace Am.Ahv.Utilities.utitility_classes
{
    public class Validation
    {

        #region "Validate the Input type of Currency"
        // Validates a positive currency amount only.

        public bool Validate_Currency_Text_Box(string Value)
        {
            if (!Regex.IsMatch(Value, RegularExpressionConstants.AmountExp))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion


        public bool emptyString(string value)
        {
            if (value.Equals(string.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool lengthString(string value)
        {            
            if (value.Length > ApplicationConstants.stringLength)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckIntegerValue(string value)
        {
            Regex rgx = new Regex(RegularExpressionConstants.numbersExp);
            if (!rgx.IsMatch(value))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool CheckStringLength(string value)
        {
            Regex rgx = new Regex(RegularExpressionConstants.stringLengthExp);
            if (!rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #region"Set Bracs & Color for amount only"
        //fuction for resolving ticket #9127
        public string SetBracsAndCurrency(string value)
        {
            string returnValue = "N/A";
            if (!value.Equals(string.Empty))
            {
                if (Convert.ToDouble(value) > 0.00)
                {
                    returnValue = String.Format(new CultureInfo("en-GB"), "{0:c}", Math.Round(Math.Abs(Convert.ToDouble(value)), 2));
                }
                else if (Convert.ToDouble(value) == 0.00)
                {
             
                    returnValue = "£0.00 ";
                }
                else
                {
                    returnValue = String.Format(new CultureInfo("en-GB"), "{0:c}", Math.Round(Math.Abs(Convert.ToDouble(value)), 2));
                }
            }
            
            return returnValue;
        }

        //End fuction for resolving ticket #9127

        public string SetBracs(string value)
        {
            if (!value.Equals(string.Empty))
            {
                if (Convert.ToDouble(value) > 0.00)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(value)), 2).ToString("0.00"); 
                }
                else if (Convert.ToDouble(value) == 0.00)
                {
                    return "£0.00 ";
                }
                else
                {
                    return "(£ " + Math.Round(Math.Abs(Convert.ToDouble(value)), 2).ToString("0.00") + ")";
                }
            }
            else
            {
                return "N/A";
            }
        }


        public System.Drawing.Color SetForeColor(string value)
        {

            if (!value.Equals(string.Empty))
            {
                if (Convert.ToDouble(value) >= 0.00)
                {

                    return System.Drawing.Color.Black;
                }
                else
                {
                    return System.Drawing.Color.Red;
                }
            }
            else
            {
                return System.Drawing.Color.Black;
            }
        }

        #endregion

        #region"Set Bracs & Color for Date and amount both"

        public string SetBracs(string date, string amount)
        {

            if (!date.Equals(string.Empty) && !amount.Equals(string.Empty))
            {
                if (Convert.ToDouble(amount) > 0.0)
                {

                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(amount)), 2).ToString() + " " + date;
                }
                else
                {

                    return "(£" + Math.Round(Math.Abs(Convert.ToDouble(amount)), 2).ToString() + ") " + date;

                }
            }
            else
            {
                return "-";
            }
        }

        public System.Drawing.Color SetForeColor(string date, string amount)
        {

            if (!date.Equals(string.Empty) && !amount.Equals(string.Empty))
            {
                if (Convert.ToDouble(amount) > 0.0)
                {

                    return System.Drawing.Color.Black;
                }
                else
                {

                    return System.Drawing.Color.Red;

                }
            }
            else
            {
                return System.Drawing.Color.Black;
            }
        }

        #endregion

       


    }
}
