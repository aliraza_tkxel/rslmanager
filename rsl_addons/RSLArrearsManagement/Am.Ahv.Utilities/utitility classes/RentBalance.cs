﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.Utilities.utitility_classes
{
    public class RentBalance
    {

        #region"Calculate Rent Balance"

        public static string CalculateRentBalance(double rentBalance, double rentAmount)
        {
            DayOfWeek dw = DateTime.Now.DayOfWeek;
            double balance = 0.0;
            if (rentAmount != 0.0)
            {
                balance = Math.Round(rentBalance - ((rentAmount / 7) * Convert.ToInt32(dw)), 2);
            }
            else
            {
                balance = rentBalance;
            }
            return balance.ToString();
        }

        #endregion

        #region"Calculate Recover Amount"

        public static double CalculateRecoveryAmount(double rentBalance, double monthlyRent)
        {
            //double monthlyRent = weeklyRent;
            int totalDays = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            double perDayAmount = monthlyRent / totalDays;
            int dayOfMonth = DateTime.Now.Day;           
            double totalBalance = Math.Round(rentBalance + (perDayAmount * dayOfMonth), 2);
            return totalBalance;       
        }

        #endregion

        #region"Calculate HB Cycle Payment"

        public static double CalculateHBCyclePayment(double nextHB)
        { 
            //double val = Math.Round(nextHB * (Math.Round(Convert.ToDouble(13.0/365.0), 3)*30.42), 2);
            //updated by:umair
            //update date:26 08 2011
            //double val = Math.Round(nextHB * (Convert.ToDouble(13.0 / 365.0) * 30.42),2);
            double val =Math.Round(Convert.ToDouble((nextHB * 13)/12),2);
            //end update
            return val;
        }

        #endregion
    }
}
