﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.resourcebo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.resource_bo;
using System.Data;
namespace Am.Ahv.BusinessObject.resource_bo
{
    public class ResourcePatchDevelopmentBo : BaseBO, IResourcePatchDevelopment
    {
        public ResourcePatchDevelopmentBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }


        #region IResourcePatchDevelopment Members

        public AM_ResourcePatchDevelopment AddResource(List<int> PatchIDs, List<int> ResourceIDs, List<int> RegionIDs)
        {
            //AM_ResourcePatchDevelopment Addee;
            //foreach(int item in RegionIDs)
            //{
            //    Addee = new AM_ResourcePatchDevelopment();
            //    Addee.AM_ResourceReference.EntityKey=new System.Data.EntityKey(""
            //}
            throw new NotImplementedException();
        }

        #endregion

        #region IBaseBO<AM_ResourcePatchDevelopment,int> Members

        public AM_ResourcePatchDevelopment GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<AM_ResourcePatchDevelopment> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_ResourcePatchDevelopment AddNew(AM_ResourcePatchDevelopment obj)
        {
            this.AMDBMgr.AddToAM_ResourcePatchDevelopment(obj);
            this.AMDBMgr.SaveChanges();
            return obj;
        }

        public AM_ResourcePatchDevelopment Update(AM_ResourcePatchDevelopment obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_ResourcePatchDevelopment obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            try
            {
                IQueryable<AM_ResourcePatchDevelopment> resource = from p in this.AMDBMgr.AM_ResourcePatchDevelopment
                                                                   where p.ResourceId == id &&
                                                                   p.IsActive == true
                                                                   select p;

                List<AM_ResourcePatchDevelopment> arealist = resource.ToList();
                if (arealist != null && arealist.Count > 0)
                {
                    //using (this.AMDBMgr = new TKXEL_RSLManagerEntities())
                    //{
                    foreach (AM_ResourcePatchDevelopment item in arealist)
                    {
                        item.IsActive = false;
                        this.AMDBMgr.SaveChanges();
                    }
                    // }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        #region IResourcePatchDevelopment Members


        public List<AM_ResourcePatchDevelopment> GetAreasByResourceId(int ResourceID)
        {
            try
            {
                IQueryable<AM_ResourcePatchDevelopment> resourcequery = from r in this.AMDBMgr.AM_ResourcePatchDevelopment
                                                                        where r.ResourceId == ResourceID && r.IsActive == true
                                                                        select r;
                List<AM_ResourcePatchDevelopment> areaslist = resourcequery.ToList();
                if (areaslist != null && areaslist.Count > 0)
                {
                    return areaslist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

       
    }
}
