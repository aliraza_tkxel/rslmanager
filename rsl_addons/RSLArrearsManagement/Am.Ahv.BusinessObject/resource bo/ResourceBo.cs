﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.resourcebo;
using System.Globalization;

namespace Am.Ahv.BusinessObject.resource_bo
{
    public class ResourceBO : BaseBO, IResourceBO
    {
        public ResourceBO()
        {

            AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region IBaseBO<AM_Resource,int> Members
        /// <summary>
        /// Persisting the new resource into the DB
        /// </summary>
        /// <param name="obj">Resource Object</param>
        /// <returns>Newly inserted object</returns>
        public Am.Ahv.Entities.AM_Resource AddNew(Am.Ahv.Entities.AM_Resource obj)
        {

            AMDBMgr.AddToAM_Resource(obj);
            SaveChanges();
            return obj;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Am.Ahv.Entities.AM_Resource obj)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get the List of all the active resources
        /// </summary>
        /// <returns></returns>
        public List<Am.Ahv.Entities.AM_Resource> GetAll()
        {
            List<AM_Resource> resources = AMDBMgr.AM_Resource.Where(r => r.IsActive == true).ToList();
            return resources;
        }

        public Am.Ahv.Entities.AM_Resource GetById(int id)
        {
            IQueryable<AM_Resource> rquery = from resource in AMDBMgr.AM_Resource where resource.ResourceId == id select resource;
            List<AM_Resource> resourcelist = rquery.ToList();
            if (resourcelist != null && resourcelist.Count > 0)
            {
                return resourcelist.First();
            }
            else
                return null;
        }

        public Am.Ahv.Entities.AM_Resource Update(Am.Ahv.Entities.AM_Resource obj)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Case Person"

        public IQueryable getCasePerson(String psersonType)
        {
            try
            {
                var caseOfficers = (from t in AMDBMgr.E__EMPLOYEE
                                    join t0 in AMDBMgr.AM_Resource on new { EMPLOYEEID = t.EMPLOYEEID } equals new { EMPLOYEEID = t0.EmployeeId }
                                    join t1 in AMDBMgr.AM_LookupCode on t0.AM_LookupCode.LookupCodeId equals t1.LookupCodeId
                                    where
                                    t1.CodeName == psersonType && t1.IsActive == true
                                    select new
                                    {
                                        EMPLOYEEID = (Int32?)t.EMPLOYEEID,
                                        name = (t.FIRSTNAME + " " + (t.LASTNAME ?? ""))
                                    });


                return caseOfficers;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Resource Details"

        public IQueryable getResourceDetails(int dbIndex, int pageSize)
        {
            try
            {
                var pagedData = (from t in AMDBMgr.AM_Resource
                                 join t0 in AMDBMgr.E__EMPLOYEE on t.EmployeeId equals t0.EMPLOYEEID
                                 join t1 in AMDBMgr.E_JOBDETAILS on t0.EMPLOYEEID equals t1.EMPLOYEEID
                                 join t2 in AMDBMgr.AM_LookupCode on t.AM_LookupCode.LookupCodeId equals t2.LookupCodeId
                                 where
                                   t.IsActive == true && t2.IsActive == true
                                 orderby
                                   t.ResourceId
                                 select new
                                 {
                                     t.ResourceId,
                                     t.IsActive,
                                     t.AM_LookupCode.LookupCodeId,
                                     EmployeeId = (Int32?)t.EmployeeId,
                                     EmployeeName = ((t0.FIRSTNAME ?? "") + " " + (t0.LASTNAME ?? "")),
                                     JOBTITLE = (t1.JOBTITLE ?? ""),
                                     t2.CodeName
                                 }).Skip(dbIndex * pageSize).Take(pageSize);
                return pagedData;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Record Count"

        public int getRecordsCount()
        {
            try
            {
                int count = (from t in AMDBMgr.AM_Resource
                             join t0 in AMDBMgr.E__EMPLOYEE on t.EmployeeId equals t0.EMPLOYEEID
                             join t1 in AMDBMgr.E_JOBDETAILS on t0.EMPLOYEEID equals t1.EMPLOYEEID
                             join t2 in AMDBMgr.AM_LookupCode on t.AM_LookupCode.LookupCodeId equals t2.LookupCodeId
                             where
                               t.IsActive == true && t2.IsActive == true
                             orderby
                               t.ResourceId
                             select new
                             {
                                 t.ResourceId,
                                 t.IsActive,
                                 t.AM_LookupCode.LookupCodeId,
                                 EmployeeId = (Int32?)t.EmployeeId,
                                 EmployeeName = ((t0.FIRSTNAME ?? "") + " " + (t0.LASTNAME ?? "")),
                                 JOBTITLE = (t1.JOBTITLE ?? ""),
                                 t2.CodeName
                             }).Count();
                return count;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Update Resource Type"

        public void UpdateResourceType(int resourceId, AM_Resource resourceObj)
        {
            try
            {
                AM_Resource resource = new AM_Resource();
                IQueryable<AM_Resource> data = from t in AMDBMgr.AM_Resource where t.ResourceId == resourceId select t;
                resource = data.ToList()[0];
                resource.AM_LookupCodeReference.Load();
                if (resourceObj.IsActive)
                {
                    resource.LookupCodeId = resourceObj.LookupCodeId;
                }
                //if (value > 0)
                //{
                //    resource.AM_LookupCodeReference.EntityKey = new System.Data.EntityKey("TKXEL_RSLManagerEntities.AM_LookupCode", "LookupCodeId", value);
                //}
                resource.IsActive = resourceObj.IsActive;
                resource.ModifiedDate = resourceObj.ModifiedDate;
                resource.ModifiedBy = resourceObj.ModifiedBy;
                AMDBMgr.SaveChanges();
            }
            catch (ArgumentOutOfRangeException argumentException)
            {
                throw argumentException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Active Persons"

        public IQueryable GetActivePersons(string designation)
        {

            try
            {
                var persons = (from t in AMDBMgr.E__EMPLOYEE
                               join t0 in AMDBMgr.AM_Resource on new { EMPLOYEEID = t.EMPLOYEEID } equals new { EMPLOYEEID = t0.EmployeeId }
                               join t1 in AMDBMgr.AM_LookupCode on t0.AM_LookupCode.LookupCodeId equals t1.LookupCodeId
                               where
                               t1.CodeName == designation && t0.IsActive == true && t1.IsActive == true
                               select new
                               {
                                   t0.ResourceId,
                                   EMPLOYEEID = (Int32?)t.EMPLOYEEID,
                                   name = (t.FIRSTNAME + " " + (t.LASTNAME ?? ""))
                               });


                return persons;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Resource Id"

        public int GetResourceId(int userId)
        {
            try
            {
                IQueryable<int> id = (from ds in AMDBMgr.AM_Resource
                                      where ds.EmployeeId == userId && ds.IsActive == true
                                      select ds.ResourceId);
                List<int> list = id.ToList();
                if (list.Count() == 0 || list.Count() == null)
                {
                    return -1;
                }
                else
                {
                    return list[0];
                }
            }
            
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IResourceBO Members


        IEnumerable<AM_Resource> IResourceBO.GetAllResources()
        {
            return AMDBMgr.AM_Resource.Where(r => r.IsActive == true).AsEnumerable<AM_Resource>();
        }

        #endregion

        #region"Get Resource Name"

        public string GetResourceName(int userId)
        {
            try
            {
                var result = (from ds in AMDBMgr.E__EMPLOYEE
                              where ds.EMPLOYEEID == userId
                              select new
                              {
                                  EmployeeName = ((ds.FIRSTNAME ?? "") + " " + (ds.LASTNAME ?? ""))
                              });
                string name = string.Empty;
                foreach (var t in result)
                {
                    name = t.EmployeeName;
                }
                return name;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IResourceBO Members


        public bool IsResourceExist(int EmployeeId)
        {
            try
            {
                IQueryable<AM_Resource> resourcequery = from r in this.AMDBMgr.AM_Resource where r.EmployeeId == EmployeeId && r.IsActive == true select r;
                List<AM_Resource> resourcelist = resourcequery.ToList();
                if (resourcelist.Count > 0 && resourcelist != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion

        #region IResourceBO Members


        public bool UpdateResource(AM_Resource Resource)
        {
            IQueryable<AM_Resource> rquery = from res in this.AMDBMgr.AM_Resource where res.ResourceId == Resource.ResourceId select res;
            List<AM_Resource> rlist = rquery.ToList();
            if (rlist != null && rlist.Count > 0)
            {
                AM_Resource upres = rlist.First();
                upres.ResourceId = Resource.ResourceId;
                upres.IsActive = Resource.IsActive;
                upres.LookupCodeId = Resource.LookupCodeId;
                upres.ModifiedBy = Resource.ModifiedBy;
                upres.ModifiedDate = Resource.ModifiedDate;
                upres.EmployeeId = Resource.EmployeeId;
                upres.CreatedDate = Resource.CreatedDate;
                this.AMDBMgr.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Get Last Logged In Date"

        public string GetLastLoggedInDate(int UserId)
        {
            try
            {
                string strDate = string.Empty;
                List<AC_LOGINS> listLogin = (from ds in AMDBMgr.AC_LOGINS
                                             where ds.EMPLOYEEID == UserId
                                             orderby ds.DATEMODIFIED descending
                                             select ds).ToList();
                if (listLogin != null)
                {
                    if (listLogin.Count > 0)
                    {
                        string userName = listLogin[0].LOGIN;
                        List<AC_LOGIN_HISTORY> listHistory = (from history in AMDBMgr.AC_LOGIN_HISTORY
                                                              where history.UserName == userName
                                                              orderby history.LoginDate descending
                                                              select history).ToList();
                        if (listHistory != null)
                        {
                            if (listHistory.Count > 0)
                            {
                                DateTime loginDate = listHistory[0].LoginDate.Value;
                                CultureInfo culture = new CultureInfo("en-GB");
                                strDate = loginDate.ToString("F", culture);
                            }
                        }
                    }
                }
                return strDate;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        public string getJobTitle(int employeeId)
        {
            var query = (from e in AMDBMgr.E_JOBDETAILS
                        //join resource in AMDBMgr.AM_Resource on e.EMPLOYEEID equals resource.EmployeeId
                        where e.EMPLOYEEID == employeeId
                        select e.JOBTITLE);
            if (query != null)
            {
                if (query.Count() != 0)
                {
                    List<string> list = query.ToList();
                    return list[0];
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public string getWorkEmail(int employeeId)
        {
            var query = (from e in AMDBMgr.E_CONTACT
                         //join resource in AMDBMgr.AM_Resource on e.EMPLOYEEID equals resource.EmployeeId
                         where e.EMPLOYEEID == employeeId
                         select e.WORKEMAIL);

                if (query != null)
                {
                    if (query.Count() != 0)
                    {
                        List<string> list = query.ToList();
                        return list[0];
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
        }
        //param EmployeeId
        public string getWorkDirectDial(int employeeId)
        {
            var query = (from e in AMDBMgr.E_CONTACT
                         //join resource in AMDBMgr.AM_Resource on e.EMPLOYEEID equals resource.EmployeeId
                         where e.EMPLOYEEID == employeeId
                         select e.WORKDD);

            if (query != null)
            {
                if (query.Count() != 0)
                {
                    List<string> list = query.ToList();
                    return list[0];
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
           
        }
    }
}
