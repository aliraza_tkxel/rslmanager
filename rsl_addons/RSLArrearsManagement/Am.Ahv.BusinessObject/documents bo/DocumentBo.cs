﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.documents_bo;
using Am.Ahv.BusinessObject.base_bo;
namespace Am.Ahv.BusinessObject.documents_bo
{
    class DocumentBo:BaseBO,IDocumentBo
    {
        public DocumentBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }
        #region IDocumentBo Members

        public List<AM_Documents> GetDocumentByCaseId(int CaseId)
        {
            try
            {
                IQueryable<AM_Documents> docquery = from doc in this.AMDBMgr.AM_Documents where doc.AM_Case.CaseId == CaseId select doc;
                List<AM_Documents> DocList = docquery.ToList();
                if (DocList.Count > 0)
                {
                    return DocList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAttachedByCaseId(int CaseId)
        {
            try
            {
                List<AM_Documents> Doclist = GetDocumentByCaseId(CaseId);
                if (Doclist.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IBaseBO<AM_Documents,int> Members

        public AM_Documents GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<AM_Documents> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_Documents AddNew(AM_Documents obj)
        {
            try
            {
                AMDBMgr.AddToAM_Documents(obj);
                AMDBMgr.SaveChanges();
                return obj;
            }

            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AM_Documents Update(AM_Documents obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_Documents obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDocumentBo Members


        public List<AM_Documents> GetDocumentsByCaseHistoryId(int CaseHistoryId)
        {
            try
            {
                IQueryable<AM_Documents> docquery = from doc in this.AMDBMgr.AM_Documents where doc.AM_CaseHistory.CaseHistoryId== CaseHistoryId select doc;
                List<AM_Documents> DocList = docquery.ToList();
                if (DocList.Count > 0)
                {
                    return DocList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        #region IDocumentBo Members


        public int GetDoucmentCountByCaseId(int CaseId)
        {
            try
            {
                var count = (from p in this.AMDBMgr.AM_Documents where p.CaseId == CaseId select p).Count();
                return count;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IDocumentBo Members


        public int GetDocumentCountByActivity(int AcitvityId)
        {
            try
            {
                var count = (from p in this.AMDBMgr.AM_Documents where p.ActivityId == AcitvityId select p).Count();
                return count;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IDocumentBo Members


        public int GetDocumentCountByCaseHistoryId(int CaseHistoryId)
        {
            try
            {
                int count = (from p in this.AMDBMgr.AM_Documents where p.CaseHistoryId == CaseHistoryId select p).Count();
                return count;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public List<AM_Documents> GetDocumentsByActivityId(int ActivityId)
        {
            try
            {
                IQueryable<AM_Documents> docquery = from doc in this.AMDBMgr.AM_Documents where doc.ActivityId == ActivityId select doc;
                List<AM_Documents> DocList = docquery.ToList();
                if (DocList.Count > 0)
                {
                    return DocList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
