﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.tenantbo;
using Am.Ahv.BusinessObject.base_bo;
namespace Am.Ahv.BusinessObject.tenant_bo
{
    class TenantBo:BaseBO,ITenantBo
    {
        public TenantBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }


        #region ITenantBo Members

        public string GetCustomerContactByTenantID(int TenantId)
        {
            try
            {
                IQueryable<C_CUSTOMERTENANCY> customerId = from cid in this.AMDBMgr.C_CUSTOMERTENANCY where cid.TENANCYID == TenantId select cid;
                List<C_CUSTOMERTENANCY> listCustomer = customerId.ToList();
                if (listCustomer != null && listCustomer.Count > 0)
                {
                    int customerid = customerId.First().CUSTOMERID;
                    IQueryable<C_ADDRESS> query = from p in this.AMDBMgr.C_ADDRESS where p.CUSTOMERID == customerid select p;
                    List<C_ADDRESS> list = query.ToList();
                    if (list != null && list.Count > 0)
                    {
                        return list.First().TEL;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
