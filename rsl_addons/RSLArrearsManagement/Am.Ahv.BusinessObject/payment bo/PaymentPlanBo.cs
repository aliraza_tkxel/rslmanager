﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BoInterface.basebo;
using System.Data;
using System.Data.Objects;
using System.Transactions;
using Am.Ahv.Utilities.utitility_classes;



namespace Am.Ahv.BusinessObject.paymentbo
{
    public class PaymentPlanBo:BaseBO,IPaymentPlanBo
    {
        public PaymentPlanBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region IBaseBO<AM_Payment,int> Members

        public AM_PaymentPlan GetById(int id)
        {
            throw new NotImplementedException();

        }

        public List<AM_PaymentPlan> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_PaymentPlan AddNew(AM_PaymentPlan obj)
        {
            throw new NotImplementedException();
        }

        public AM_PaymentPlan Update(AM_PaymentPlan obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_PaymentPlan obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Add Payment Plan"

        public int AddPaymentPlan(AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayment)
        {
            try
            {
                bool success = false;
                using (TransactionScope tran = new TransactionScope())
                {
                    this.AMDBMgr.AddToAM_PaymentPlan(paymentPlan);
                    this.AMDBMgr.SaveChanges();
                    paymentPlanHistory.PaymentPlanId = paymentPlan.PaymentPlanId;

                    this.AMDBMgr.AddToAM_PaymentPlanHistory(paymentPlanHistory);
                    this.AMDBMgr.SaveChanges();

                    AM_Payment payment = new AM_Payment();
                    for (int i = 0; i < listPayment.Count; i++)
                    {
                        payment.Date = listPayment[i].Date;
                        payment.Payment = listPayment[i].Payment;
                        payment.IsFlexible = listPayment[i].IsFlexible;
                        payment.PaymentPlanId = paymentPlan.PaymentPlanId;
                        payment.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                        this.AMDBMgr.AddToAM_Payment(payment);
                        AMDBMgr.SaveChanges();
                        AMDBMgr.Detach(payment);
                    }

                    List<AM_CaseHistory> listHistory = (from ds in AMDBMgr.AM_CaseHistory
                                                        where ds.TennantId == paymentPlan.TennantId && ds.IsActive == true
                                                        orderby ds.CaseHistoryId descending
                                                        select ds).ToList();

                    if (listHistory != null)
                    {
                        AM_CaseHistory paymentCaseHistory = new AM_CaseHistory();
                        AM_CaseHistory history = listHistory.First();
                        paymentCaseHistory.ActionId = history.ActionId;
                        paymentCaseHistory.ActionHistoryId = history.ActionHistoryId;
                        paymentCaseHistory.StatusId = history.StatusId;
                        paymentCaseHistory.StatusHistoryId = history.StatusHistoryId;
                        paymentCaseHistory.ActionIgnoreCount = history.ActionIgnoreCount;
                        paymentCaseHistory.ActionIgnoreReason = history.ActionIgnoreReason;
                        paymentCaseHistory.ActionRecordedCount = history.ActionRecordedCount;
                        paymentCaseHistory.ActionRecordedDate = paymentPlan.CreatedDate;///history.ActionRecordedDate;
                        paymentCaseHistory.ActionReviewDate = paymentPlan.ReviewDate; //history.ActionReviewDate;
                        paymentCaseHistory.CaseId = history.CaseId;
                        paymentCaseHistory.CaseManager = history.CaseManager;
                        paymentCaseHistory.CaseOfficer = history.CaseOfficer;
                        paymentCaseHistory.CreatedDate = paymentPlan.CreatedDate;//history.CreatedDate;
                        paymentCaseHistory.InitiatedById = history.InitiatedById;
                        paymentCaseHistory.IsActionIgnored = history.IsActionIgnored;
                        paymentCaseHistory.IsActionPostponed = history.IsActionPostponed;
                        paymentCaseHistory.IsActionRecorded = history.IsActionRecorded;
                        paymentCaseHistory.IsActive = history.IsActive;
                        paymentCaseHistory.IsDocumentAttached = false;
                        paymentCaseHistory.IsDocumentUpload = false;
                        paymentCaseHistory.IsSuppressed = history.IsSuppressed;
                        paymentCaseHistory.ModifiedBy = paymentPlan.ModifiedBy;///history.ModifiedBy;
                        paymentCaseHistory.ModifiedDate = paymentPlan.ModifiedDate;///history.ModifiedDate;
                        paymentCaseHistory.Notes = "Payment Plan";//history.Notes;
                        paymentCaseHistory.NoticeExpiryDate = history.NoticeExpiryDate;
                        paymentCaseHistory.NoticeIssueDate = history.NoticeIssueDate;
                        paymentCaseHistory.OutcomeLookupCodeId = history.OutcomeLookupCodeId;
                        paymentCaseHistory.Reason = history.Reason;
                        paymentCaseHistory.RecoveryAmount = history.RecoveryAmount;
                        paymentCaseHistory.StatusReview = history.StatusReview;
                        paymentCaseHistory.SuppressedBy = history.SuppressedBy;
                        paymentCaseHistory.SuppressedDate = history.SuppressedDate;
                        paymentCaseHistory.SuppressedReason = history.SuppressedReason;
                        paymentCaseHistory.TennantId = history.TennantId;
                        paymentCaseHistory.IsPaymentPlanIgnored = false;
                        paymentCaseHistory.IsPaymentPlan = true;
                        paymentCaseHistory.IsPaymentPlanUpdated = false;
                        paymentCaseHistory.PaymentPlanId = paymentPlan.PaymentPlanId;
                        paymentCaseHistory.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                        paymentCaseHistory.IsCaseUpdated = false;
                        paymentCaseHistory.StatusRecordedDate = history.StatusRecordedDate;
                        this.AMDBMgr.AddToAM_CaseHistory(paymentCaseHistory);
                        AMDBMgr.SaveChanges();

                        AM_Case paymentCase = new AM_Case();
                        List<AM_Case> listCase = (from d in AMDBMgr.AM_Case
                                                  where d.TenancyId == paymentPlan.TennantId && d.IsActive == true
                                                  orderby d.CaseId descending
                                                  select d).ToList();
                        if (listCase != null)
                        {
                            paymentCase = listCase.First();
                            paymentCase.IsPaymentPlanIgnored = false;
                            paymentCase.IsPaymentPlan = true;
                            paymentCase.IsPaymentPlanUpdated = false;
                            paymentCase.PaymentPlanId = paymentPlan.PaymentPlanId;
                            paymentCase.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                            paymentCase.IsCaseUpdated = false;
                            AMDBMgr.SaveChanges();
                        }
                    }

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                if (success)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Payment Plan History"

        public int AddPaymentPlanHistory(AM_PaymentPlanHistory paymentHistory)
        {
            try
            {
                this.AMDBMgr.AddToAM_PaymentPlanHistory(paymentHistory);
                this.AMDBMgr.SaveChanges();
                return paymentHistory.PaymentPlanHistoryId;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Frequency Data"
        /// <summary>
        /// AuthorName:Nouman
        /// Revision:1.0
        /// Summary:I guess this should be in LookupBo not here... Please clarify
        /// </summary>
        /// <returns></returns>
        public IQueryable GetFrequencyData()
        {
            try
            {
                var data = (from ds in AMDBMgr.AM_LookupCode
                            join t in AMDBMgr.AM_LookupType on ds.LookupTypeId equals t.LookupTypeId
                            where t.TypeName.Equals("PaymentPlan") && ds.LookupCodeId != 87 && ds.IsActive == true
                            select new
                            {
                                ds.LookupCodeId,
                                ds.CodeName
                            });
                return data;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Payment Plan Type"

        public IQueryable GetPaymentPlanType()
        {
            try
            {
                var data = (from ds in AMDBMgr.AM_LookupCode
                            join t in AMDBMgr.AM_LookupType on ds.LookupTypeId equals t.LookupTypeId
                            where t.TypeName.Equals("PaymentPlanType") && ds.IsActive == true
                            select new
                            {
                                ds.LookupCodeId,
                                ds.CodeName
                            });
                return data;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Weekly Rent"

        public double GetWeeklyRent(int tenantId)
        {
            try
            {
                var rent = (from ds in AMDBMgr.C_TENANCY
                            join t in AMDBMgr.P__PROPERTY on ds.PROPERTYID equals t.PROPERTYID
                            join t1 in AMDBMgr.P_FINANCIAL on t.PROPERTYID equals t1.PROPERTYID
                            where ds.TENANCYID == tenantId
                            select new
                            {
                                t1.TOTALRENT
                            });

                double totalRent = 0.0;
                foreach (var t in rent)
                {
                    totalRent = Convert.ToDouble(t.TOTALRENT);
                }
                return totalRent;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Payment Plan"
        /// <summary>
        /// Author:Nouman
        /// Revision:1.0
        /// Summary: Please simplify this code. It can be done with lesser Lines of code. 
        /// </summary>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        public bool CheckPaymentPlan(int tenantId)
        {
            try
            {
                var result = from ds in AMDBMgr.AM_PaymentPlan
                             where ds.TennantId == tenantId && (ds.IsActive == null || ds.IsActive == true) 
                             select new
                             {
                                 ds.IsCreated
                             };
                bool isExist = false;
                foreach (var t in result)
                {
                    if (t.IsCreated)
                    {
                        isExist = true;
                        break;
                    }
                    else
                        isExist = false;
                }
                return isExist;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Set Case Payment Plan"

        public bool SetCasePaymentPlan(int tenantId, int modifiedBy)
        {
            try
            {
                bool flagBit = false;
                AM_Case updateCase = null;
                AM_CaseHistory caseHistory = null;

                IQueryable<AM_Case> query = (from ds in AMDBMgr.AM_Case
                                             where ds.TenancyId == tenantId && ds.IsActive == true
                                             select ds);
                IQueryable<AM_CaseHistory> queryHistory = (from history in AMDBMgr.AM_CaseHistory
                                                           where history.TennantId == tenantId && history.IsActive == true
                                                           select history);

                List<AM_Case> listCase = query.ToList<AM_Case>();
                List<AM_CaseHistory> listHistory = queryHistory.ToList<AM_CaseHistory>();

                if (listCase.Count > 0)
                {
                    updateCase = listCase.First();
                    updateCase.IsPaymentPlan = true;
                    updateCase.ModifiedDate = DateTime.Now;
                    updateCase.ModifiedBy = modifiedBy;
                    AMDBMgr.SaveChanges();

                    if (listHistory.Count > 0)
                    {
                        caseHistory = listHistory.First();
                        caseHistory.IsPaymentPlan = true;
                        caseHistory.ModifiedDate = DateTime.Now;
                        caseHistory.ModifiedBy = modifiedBy;
                        AMDBMgr.SaveChanges();
                        flagBit = true;
                    }
                    if (flagBit)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IPaymentPlanBo Members


        public int GetPaymentPlanCount()
        {
            try
            {
                var query = (from pp in this.AMDBMgr.AM_PaymentPlan where pp.IsCreated == true select pp.PaymentPlanId).Count();
                int count = query;
                return query;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<AM_PaymentPlan> GetPaymentPlanSummary(int tennantId)
        {
            try
            {
                try
                {
                    IQueryable<AM_PaymentPlan> Paymentplanquery = from p in this.AMDBMgr.AM_PaymentPlan where p.TennantId == tennantId && p.IsCreated == true select p;
                    List<AM_PaymentPlan> PaymentList = Paymentplanquery.ToList();
                    if (PaymentList.Count > 0 && PaymentList != null)
                    {
                        return LoadReference(PaymentList);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (EntityException entityexception)
                {
                    throw entityexception;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private List<AM_PaymentPlan> LoadReference(List<AM_PaymentPlan> PlanList)
        {
            foreach (AM_PaymentPlan item in PlanList)
            {
                item.AM_LookupCodeReference.Load();
                item.AM_LookupCode1Reference.Load();
                item.AM_Resource1Reference.Load();
                item.AM_ResourceReference.Load();

            }
            return PlanList;

        }

        #endregion

        #region"Get Payment Plan Review"
        public DataTable GetPaymentPlanReview()
        {
            try
            {
                var query = (from payment in AMDBMgr.AM_PaymentPlan
                             join resource in AMDBMgr.AM_Resource on payment.CreatedBy equals resource.ResourceId
                             join emp in AMDBMgr.E__EMPLOYEE on resource.EmployeeId equals emp.EMPLOYEEID
                             join contact in AMDBMgr.E_CONTACT on emp.EMPLOYEEID equals contact.EMPLOYEEID
                             join ten in AMDBMgr.C_TENANCY on payment.TennantId equals ten.TENANCYID
                             join custen in AMDBMgr.C_CUSTOMERTENANCY on ten.TENANCYID equals custen.TENANCYID 
                             join cus in AMDBMgr.C__CUSTOMER on custen.CUSTOMERID equals cus.CUSTOMERID 
                             join prop in AMDBMgr.P__PROPERTY on ten.PROPERTYID equals prop.PROPERTYID
                             
                             where payment.IsCreated == true
                             select new
                             {
                                 contact.WORKEMAIL,
                                 payment.IsCreated,
                                 payment.ReviewDate,
                                 cus.FIRSTNAME,
                                 cus.LASTNAME,
                                 prop.PROPERTYID,
                                 prop.ADDRESS1,
                                 prop.TOWNCITY,
                                 prop.POSTCODE

                             });
                DataTable dt = new DataTable();
                dt.Columns.Add("Email");
                dt.Columns.Add("IsCreated");
                dt.Columns.Add("ReviewDate");
                dt.Columns.Add("FIRSTNAME");
                dt.Columns.Add("LASTNAME");
                dt.Columns.Add("PROPERTYID");
                dt.Columns.Add("ADDRESS1");
                dt.Columns.Add("TOWNCITY");
                dt.Columns.Add("POSTCODE");
                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["Email"] = t.WORKEMAIL;
                    dr["IsCreated"] = t.IsCreated.ToString();
                    dr["ReviewDate"] = t.ReviewDate.ToString("dd/MM/yyyy");
                    dr["FIRSTNAME"] = t.FIRSTNAME;
                    dr["LASTNAME"] = t.LASTNAME;
                    dr["PROPERTYID"] = t.PROPERTYID;
                    dr["ADDRESS1"] = t.ADDRESS1;
                    dr["TOWNCITY"] = t.TOWNCITY;
                    dr["POSTCODE"] = t.POSTCODE;
                   
                    dt.Rows.Add(dr);
                }

                return dt;
            }
            catch (EntityException eex)
            {
                throw eex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Check Payment Plan Type"

        public string GetPaymentPlanType(int tenantId)
        {
            try
            {
                List<AM_PaymentPlan> result = (from ds in AMDBMgr.AM_PaymentPlan
                                               where ds.TennantId == tenantId && ds.IsCreated == true
                                               select ds).ToList();
                result[0].AM_LookupCode1Reference.Load();
                string str = result[0].AM_LookupCode1.CodeName;
                return str;
            }
            catch (EntityException eex)
            {
                throw eex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion       

        #region"Get Payment Plan Monitoring"

        public List<AM_SP_GetPaymentPlanMonitoring_Result> GetPaymentPlanMonitoring(string _postCode,int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string missedCheck, string sortExpression, string sortDir)
        {
            try
            {
                List<AM_SP_GetPaymentPlanMonitoring_Result> list = AMDBMgr.AM_SP_GetPaymentPlanMonitoring(_postCode,_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize,missedCheck, sortExpression, sortDir).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment List"

        public List<AM_Payment> GetPaymentList(int paymentPlanId)
        {
            try
            {
                List<AM_PaymentPlanHistory> res = (from his in AMDBMgr.AM_PaymentPlanHistory
                                                    where his.PaymentPlanId == paymentPlanId
                                                    orderby his.PaymentPlanHistoryId descending
                                                    select his).ToList();
                if (res != null)
                {
                    if (res.Count > 0)
                    {
                        int paymentPlanHistoryId = res[0].PaymentPlanHistoryId;
                        List<AM_Payment> list = (from ds in AMDBMgr.AM_Payment
                                                 where ds.PaymentPlanHistoryId == paymentPlanHistoryId
                                                 select ds).ToList();
                        return list;
                    }
                }

                return null;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public List<AM_Payment> GetPaymentLists(int paymentPlanId)
        //{
        //    try
        //    {
        //        List<AM_PaymentPlanHistory> res = (from his in AMDBMgr.AM_PaymentPlanHistory
        //                                           where his.PaymentPlanId == paymentPlanId
        //                                           orderby his.PaymentPlanHistoryId descending
        //                                           select his).ToList();
        //        if (res != null)
        //        {
        //            if (res.Count > 0)
        //            {
        //                int paymentPlanHistoryId = res[0].PaymentPlanHistoryId;
        //                List<AM_Payment> list = (from ds in AMDBMgr.AM_Payment
        //                                         where ds.PaymentPlanHistoryId == paymentPlanHistoryId && ds.Date<=DateTime.Today
        //                                         select ds).ToList();
        //                if (list != null)
        //                {
        //                    return list;
        //                }
                        
        //            }
        //        }

        //        return null;
        //    }
        //    catch (EntityException ee)
        //    {
        //        throw ee;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion

        #region"Get Customer Rent List"

        public List<F_RENTJOURNAL> GetCustomerRentList(int tenantId, DateTime firstPaymentdate)
        {
            try
            {
                DateTime date = firstPaymentdate.AddDays(-2.0);
                List<F_RENTJOURNAL> list = (from ds in AMDBMgr.F_RENTJOURNAL
                                                where ds.TENANCYID == tenantId && ds.ACCOUNTTIMESTAMP != null &&
                                                      ds.ACCOUNTTIMESTAMP.Value >= date
                                                orderby ds.ACCOUNTTIMESTAMP ascending
                                                select ds).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<F_RENTJOURNAL> GetCustomerRentList(int? tenantId, DateTime firstPaymentdate)
        {
            try
            {
                DateTime date = firstPaymentdate.AddDays(-2.0);
                List<F_RENTJOURNAL> list = (from ds in AMDBMgr.F_RENTJOURNAL
                                            where ds.TENANCYID == tenantId && ds.ACCOUNTTIMESTAMP != null &&
                                                  ds.ACCOUNTTIMESTAMP.Value >= date
                                            orderby ds.ACCOUNTTIMESTAMP ascending
                                            select ds).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<F_RENTJOURNAL> GetCustomerRentLists(int tenantId, DateTime PeriodStartDate, DateTime PeriodEndDate)
        {
            try
            {
                int[] paymenttypeid = new int[] { 2, 3, 5, 8, 13, 92, 93 }; //payment type Direct Debit, Standing Order,Cash,Cheque,BACS,Payment Card,,Credit Card and Debit Card only

                List<F_RENTJOURNAL> list = (from ds in AMDBMgr.F_RENTJOURNAL
                                            where ds.TENANCYID == tenantId && ds.ACCOUNTTIMESTAMP != null &&
                                                  ds.TRANSACTIONDATE.Value >= PeriodStartDate.Date &&
                                                  ds.TRANSACTIONDATE.Value <= PeriodEndDate.Date &&
                                                  paymenttypeid.Contains(ds.PAYMENTTYPE.Value) && ds.ITEMTYPE.Value.Equals(1) //item rent only
                                            orderby ds.ACCOUNTTIMESTAMP ascending
                                            select ds).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment Plan Monitoring Count"

        public int GetPaymentPlanMonitoringCount(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, string missedCheck)
        {
            try
            {
                var result = AMDBMgr.AM_SP_GetPaymentPlanMonitoringCount(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, missedCheck);
           
                int count = 0;
                foreach (var t in result)
                {
                    count = t.Value;
                }
                return count;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Get Payment Plan Total Rent Balance"

        public string GetPaymentPlanTotalRentBalance(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            var result = AMDBMgr.AM_SP_GetPaymentPlanRentBalanceSum(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            string balance = "";
            foreach (var t in result)
            {
                balance = RentBalance.CalculateRentBalance(Convert.ToDouble(t.RentBalance), Convert.ToDouble(t.TotalRent));
            }
            return balance;
        }

        #endregion

        #region"Get Payment Plan"

        public AM_PaymentPlan GetPaymentPlan(int tenantId)
        {
            try
            {
                List<AM_PaymentPlan> listPaymentPlan = (from ds in AMDBMgr.AM_PaymentPlan
                                                        where ds.TennantId == tenantId && ds.IsCreated == true && (ds.IsActive == null || ds.IsActive == true) 
                                                        select ds).ToList();
                if (listPaymentPlan != null)
                {
                    return listPaymentPlan[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Load Flexible Payments"

        public List<AM_Payment> LoadFlexiblePayments(int paymentPlanId)
        {
            List<AM_PaymentPlanHistory> PaymentPlanhistory = (from history in AMDBMgr.AM_PaymentPlanHistory
                                                              where history.PaymentPlanId == paymentPlanId
                                                              orderby history.PaymentPlanHistoryId descending
                                                              select history).ToList();
            int paymentPlanHistoryId = PaymentPlanhistory[0].PaymentPlanHistoryId;
            List<AM_Payment> listpayments = (from ds in AMDBMgr.AM_Payment
                                             where ds.PaymentPlanId == paymentPlanId && ds.IsFlexible == true && ds.PaymentPlanHistoryId == paymentPlanHistoryId
                                             select ds).ToList();
            if (listpayments != null)
            {
                return listpayments;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region"Get Payment Plan History"

        public AM_PaymentPlanHistory GetPaymentPlanHistory(int paymentPlanId)
        {
            try
            {
                List<AM_PaymentPlanHistory> listPaymentPlanHistory = (from ds in AMDBMgr.AM_PaymentPlanHistory
                                                                      where ds.PaymentPlanId == paymentPlanId
                                                                      orderby ds.PaymentPlanHistoryId descending
                                                                      select ds).ToList();
                if (listPaymentPlanHistory != null)
                {
                    return listPaymentPlanHistory[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Payment Plan"

        public bool UpdatePaymentPlan(int paymentPlanId, AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments)
        {
            try
            {
                bool success = false;
                using (TransactionScope ts = new TransactionScope())
                {
                    AM_PaymentPlan paymentPlanObj = new AM_PaymentPlan();
                    List<AM_PaymentPlan> paymentPlanList = (from ds in AMDBMgr.AM_PaymentPlan
                                                            where ds.PaymentPlanId == paymentPlanId
                                                            select ds).ToList();
                    paymentPlanObj = paymentPlanList.First();
                    paymentPlanObj.PaymentPlanType = paymentPlan.PaymentPlanType;
                    paymentPlanObj.ReviewDate = paymentPlan.ReviewDate;
                    paymentPlanObj.StartDate = paymentPlan.StartDate;
                    paymentPlanObj.WeeklyRentAmount = paymentPlan.WeeklyRentAmount;
                    paymentPlanObj.ModifiedDate = paymentPlan.ModifiedDate;
                    paymentPlanObj.ModifiedBy = paymentPlan.ModifiedBy;
                    paymentPlanObj.IsCreated = paymentPlan.IsCreated;
                    paymentPlanObj.FrequencyLookupCodeId = paymentPlan.FrequencyLookupCodeId;
                    paymentPlanObj.FirstCollectionDate = paymentPlan.FirstCollectionDate;
                    paymentPlanObj.EndDate = paymentPlan.EndDate;
                    paymentPlanObj.AmountToBeCollected = paymentPlan.AmountToBeCollected;
                    paymentPlanObj.TennantId = paymentPlan.TennantId;
                    paymentPlanObj.RentBalance = paymentPlan.RentBalance;
                    paymentPlanObj.LastPayment = paymentPlan.LastPayment;
                    paymentPlanObj.LastPaymentDate = paymentPlan.LastPaymentDate;
                    paymentPlanObj.RentPayable = paymentPlan.RentPayable;
                    paymentPlanObj.ArrearsCollection = paymentPlan.ArrearsCollection;
                    paymentPlanObj.IsActive = paymentPlan.IsActive;////////////added by umair

                    this.AMDBMgr.SaveChanges();

                    paymentPlanHistory.PaymentPlanId = paymentPlanId;
                    this.AMDBMgr.AddToAM_PaymentPlanHistory(paymentPlanHistory);
                    this.AMDBMgr.SaveChanges();

                    for (int i = 0; i < listPayments.Count; i++)
                    {
                        AM_Payment payment = new AM_Payment();
                        payment.Date = listPayments[i].Date;
                        payment.Payment = listPayments[i].Payment;
                        payment.PaymentPlanId = paymentPlanId;
                        payment.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                        payment.IsFlexible = listPayments[i].IsFlexible;
                        this.AMDBMgr.AddToAM_Payment(payment);
                        AMDBMgr.SaveChanges();
                    }

                    //AM_CaseHistory history = new AM_CaseHistory();
                    List<AM_CaseHistory> listHistory = (from ds in AMDBMgr.AM_CaseHistory
                                                        where ds.TennantId == paymentPlan.TennantId && ds.IsActive == true
                                                        orderby ds.CaseHistoryId descending
                                                        select ds).ToList();

                    if (listHistory != null)
                    {
                        AM_CaseHistory paymentCaseHistory = new AM_CaseHistory();
                        AM_CaseHistory history = listHistory.First();
                        paymentCaseHistory.ActionId = history.ActionId;
                        paymentCaseHistory.ActionHistoryId = history.ActionHistoryId;
                        paymentCaseHistory.StatusId = history.StatusId;
                        paymentCaseHistory.StatusHistoryId = history.StatusHistoryId;
                        paymentCaseHistory.ActionIgnoreCount = history.ActionIgnoreCount;
                        paymentCaseHistory.ActionIgnoreReason = history.ActionIgnoreReason;
                        paymentCaseHistory.ActionRecordedCount = history.ActionRecordedCount;
                        paymentCaseHistory.ActionRecordedDate = paymentPlan.CreatedDate;//history.ActionRecordedDate;
                        paymentCaseHistory.ActionReviewDate = paymentPlan.ReviewDate;//history.ActionReviewDate;
                        paymentCaseHistory.CaseId = history.CaseId;
                        paymentCaseHistory.CaseManager = history.CaseManager;
                        paymentCaseHistory.CaseOfficer = history.CaseOfficer;
                        paymentCaseHistory.CreatedDate = paymentPlan.CreatedDate;//history.CreatedDate;
                        paymentCaseHistory.InitiatedById = history.InitiatedById;
                        paymentCaseHistory.IsActionIgnored = history.IsActionIgnored;
                        paymentCaseHistory.IsActionPostponed = history.IsActionPostponed;
                        paymentCaseHistory.IsActionRecorded = history.IsActionRecorded;
                        paymentCaseHistory.IsActive = history.IsActive;
                        paymentCaseHistory.IsDocumentAttached = false;
                        paymentCaseHistory.IsDocumentUpload = false;
                        paymentCaseHistory.IsSuppressed = history.IsSuppressed;
                        paymentCaseHistory.ModifiedBy = paymentPlan.ModifiedBy; //history.ModifiedBy;
                        paymentCaseHistory.ModifiedDate = paymentPlan.ModifiedDate;//history.ModifiedDate;
                        paymentCaseHistory.Notes = "Payment Plan"; //history.Notes;
                        paymentCaseHistory.NoticeExpiryDate = history.NoticeExpiryDate;
                        paymentCaseHistory.NoticeIssueDate = history.NoticeIssueDate;
                        paymentCaseHistory.OutcomeLookupCodeId = history.OutcomeLookupCodeId;
                        paymentCaseHistory.Reason = history.Reason;
                        paymentCaseHistory.RecoveryAmount = history.RecoveryAmount;
                        paymentCaseHistory.StatusReview = history.StatusReview;
                        paymentCaseHistory.SuppressedBy = history.SuppressedBy;
                        paymentCaseHistory.SuppressedDate = history.SuppressedDate;
                        paymentCaseHistory.SuppressedReason = history.SuppressedReason;
                        paymentCaseHistory.TennantId = history.TennantId;
                        paymentCaseHistory.IsPaymentPlanIgnored = false;

                        paymentCaseHistory.IsCaseUpdated = false;
                        paymentCaseHistory.StatusRecordedDate = paymentPlan.CreatedDate;//history.StatusRecordedDate;
                        if (paymentPlanHistory.IsActive == false)
                        {
                            paymentCaseHistory.IsPaymentPlan = false;
                            paymentCaseHistory.IsPaymentPlanUpdated = false;
                            paymentCaseHistory.PaymentPlanId = null;
                            paymentCaseHistory.PaymentPlanHistoryId = null;
                            paymentCaseHistory.IsPaymentPlanClose = true;
                        }
                        else
                        {
                            paymentCaseHistory.IsPaymentPlan = true;
                            paymentCaseHistory.IsPaymentPlanUpdated = true;
                            paymentCaseHistory.PaymentPlanId = paymentPlanId;
                            paymentCaseHistory.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                            paymentCaseHistory.IsPaymentPlanClose = false;     
                        }

                        this.AMDBMgr.AddToAM_CaseHistory(paymentCaseHistory);
                        AMDBMgr.SaveChanges();

                        AM_Case paymentCase = new AM_Case();
                        List<AM_Case> listCase = (from d in AMDBMgr.AM_Case
                                                  where d.TenancyId == paymentPlan.TennantId && d.IsActive == true
                                                  orderby d.CaseId descending
                                                  select d).ToList();
                        if (listCase != null)
                        {
                            paymentCase = listCase.First();
                            paymentCase.IsCaseUpdated = false;
                            if (paymentPlan.IsActive == false)
                            {
                                paymentCase.IsPaymentPlanIgnored = false;
                                paymentCase.IsPaymentPlan = false;
                                paymentCase.IsPaymentPlanUpdated = false;
                                paymentCase.PaymentPlanId = null;
                                paymentCase.PaymentPlanHistoryId = null;
                                paymentCase.IsPaymentPlanClose = true;
                            }
                            else
                            {
                                paymentCase.IsPaymentPlanIgnored = false;
                                paymentCase.IsPaymentPlan = true;
                                paymentCase.IsPaymentPlanUpdated = true;
                                paymentCase.PaymentPlanId = paymentPlanId;
                                paymentCase.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                                paymentCase.IsPaymentPlanClose = false;
                            }

                            AMDBMgr.SaveChanges();
                        }
                    }
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    ts.Complete();
                    success = true;
                }
                return success;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Get Payment Plan Summary History"
        public List<AM_PaymentPlanHistory> GetPaymentPlanSummaryHistory(int PaymentPlanHistoryId)
        {
            try
            {
                try
                {
                    IQueryable<AM_PaymentPlanHistory> Paymentplanquery = from p in this.AMDBMgr.AM_PaymentPlanHistory where p.PaymentPlanHistoryId == PaymentPlanHistoryId select p;
                    List<AM_PaymentPlanHistory> PaymentList = Paymentplanquery.ToList();
                    if (PaymentList.Count > 0 && PaymentList != null)
                    {
                        return LoadReferencePlanHistory(PaymentList);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (EntityException entityexception)
                {
                    throw entityexception;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region"Load Reference Plan History"
        private List<AM_PaymentPlanHistory> LoadReferencePlanHistory(List<AM_PaymentPlanHistory> PlanList)
        {
            foreach (AM_PaymentPlanHistory item in PlanList)
            {
                item.AM_LookupCodeReference.Load();
                // item.AM_LookupCode1Reference.Load();
                item.AM_Resource1Reference.Load();
                item.AM_ResourceReference.Load();

            }
            return PlanList;

        }
        #endregion

        public bool getLatestMissedDetectionDate(int newPayment)
        {
            try
            {
                 var CheckPayment = (from hm in AMDBMgr.AM_MissedPayments where (hm.PaymentId == newPayment) select hm.PaymentId);

                if (CheckPayment.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "Delete Zero Missed Payments"
        //Updated by:Noor
        //Update Date:30 08 2011
        public void DeleteZeorMissedPayments()
        {
            List<AM_MissedPaymentsZero> list = (from ds in AMDBMgr.AM_MissedPaymentsZero
                                              
                                               select ds).ToList();

            foreach (var ds in list)
            {
                AMDBMgr.AM_MissedPaymentsZero.DeleteObject(ds);
            }

            try
            {
                AMDBMgr.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Update End
        #endregion
        
        #region "Delete Old Missed Payments"
        //Updated by:Noor
        //Update Date:29 08 2011
        public bool DeleteOldMissedPayments(int tenancyId)
        {
            bool success = false;
            List<AM_MissedPayments> list = (from ds in AMDBMgr.AM_MissedPayments
                                            where ds.TenantId == tenancyId 
                                            select ds).ToList();

            foreach (var ds in list)
            {
                AMDBMgr.AM_MissedPayments.DeleteObject(ds);
            }

            try
            {
                AMDBMgr.SaveChanges();
                success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }
        //Update End
        #endregion

        public List<AM_Payment> GetPaymentList(int? paymentPlanId)
        {
            try
            {
                List<AM_PaymentPlanHistory> res = (from his in AMDBMgr.AM_PaymentPlanHistory
                                                   where his.PaymentPlanId == paymentPlanId
                                                   orderby his.PaymentPlanHistoryId descending
                                                   select his).ToList();
                if (res != null)
                {
                    if (res.Count > 0)
                    {
                        int paymentPlanHistoryId = res[0].PaymentPlanHistoryId;
                        List<AM_Payment> list = (from ds in AMDBMgr.AM_Payment
                                                 where ds.PaymentPlanHistoryId == paymentPlanHistoryId
                                                 select ds).ToList();
                        return list;
                    }
                }

                return null;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<F_RENTJOURNAL> GetCustomerRentLists(int? tenantId, DateTime PeriodStartDate, DateTime PeriodEndDate)
        {
            try
            {
                int[] paymenttypeid = new int[] { 2, 3, 5, 8, 13, 92, 93 }; //payment type Direct Debit, Standing Order,Cash,Cheque,BACS,Payment Card,,Credit Card and Debit Card only

                List<F_RENTJOURNAL> list = (from ds in AMDBMgr.F_RENTJOURNAL
                                            where ds.TENANCYID == tenantId && ds.ACCOUNTTIMESTAMP != null &&
                                                  ds.TRANSACTIONDATE.Value >= PeriodStartDate.Date &&
                                                  ds.TRANSACTIONDATE.Value <= PeriodEndDate.Date &&
                                                  paymenttypeid.Contains(ds.PAYMENTTYPE.Value) && ds.ITEMTYPE.Value.Equals(1) //item rent only
                                            orderby ds.ACCOUNTTIMESTAMP ascending
                                            select ds).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get Zero Missed Payment Tenancies
        public List<AM_SP_GetZeroMissedPaymentTenancies_Result> GetZeroMissedPaymentTenancies()
        {
            try
            {
                List<AM_SP_GetZeroMissedPaymentTenancies_Result> zeroMissedList = new List<AM_SP_GetZeroMissedPaymentTenancies_Result>();
                var tenancyList = AMDBMgr.AM_SP_GetZeroMissedPaymentTenancies().ToList();
                zeroMissedList = tenancyList;

                return zeroMissedList;

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        


    }
}
