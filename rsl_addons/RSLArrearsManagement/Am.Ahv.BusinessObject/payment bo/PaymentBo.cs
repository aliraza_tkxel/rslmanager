﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BoInterface.basebo;
using System.Data;



namespace Am.Ahv.BusinessObject.paymentbo
{
    public class PaymentBo :BaseBO,IPayment
    {

        public PaymentBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region IBaseBO<AM_Payment,int> Members

        public AM_Payment GetById(int id)
        {
            throw new NotImplementedException();

        }

        public List<AM_Payment> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_Payment AddNew(AM_Payment obj)
        {
            try
            {
                this.AMDBMgr.AddToAM_Payment(obj);
                this.AMDBMgr.SaveChanges();
                AMDBMgr.Detach(obj);
                return obj;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            //throw new NotImplementedException();
            

        }

        public AM_Payment Update(AM_Payment obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_Payment obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion



        #region IPayment Members

        public List<AM_Payment> GetPaymentDetailsByTenantId(int TenantId)
        {
            try
            {
                List<AM_PaymentPlanHistory> list = (from ds in AMDBMgr.AM_PaymentPlanHistory
                                                    where ds.TennantId == TenantId && ds.IsCreated == true
                                                    orderby ds.PaymentPlanHistoryId descending
                                                    select ds).ToList();
                int paymentPlanHistoryId = 0;
                if (list != null)
                {
                    if (list.Count > 0)
                    {
                        paymentPlanHistoryId = list[0].PaymentPlanHistoryId;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

                //IQueryable<AM_Payment> PaymentQuery = from p in this.AMDBMgr.AM_Payment where p.AM_PaymentPlan.TennantId == TenantId && p.AM_PaymentPlan.IsCreated == true select p;
                IQueryable<AM_Payment> PaymentQuery = from p in this.AMDBMgr.AM_Payment
                                                      where p.PaymentPlanHistoryId == paymentPlanHistoryId
                                                      select p;
                List<AM_Payment> PaymentList = PaymentQuery.ToList();
                if (PaymentList.Count > 0 && PaymentList != null)
                {
                    return PaymentList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_Payment> GetPaymentDetailsByPaymentPlanHistoryId(int PaymentPlanHistoryId)
        {
            try
            {
                IQueryable<AM_Payment> PaymentQuery = from p in this.AMDBMgr.AM_Payment where p.PaymentPlanHistoryId == PaymentPlanHistoryId && p.AM_PaymentPlan.IsCreated == true select p;
                List<AM_Payment> PaymentList = PaymentQuery.ToList();
                if (PaymentList.Count > 0 && PaymentList != null)
                {
                    return PaymentList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        public int GetMissedPaymentsAlertCount()
        {
            try
            {
                int result = (from e in AMDBMgr.AM_MissedPayments
                              select e).Count();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #region"Get Missed Payment Alert Count"

        public int GetMissedPaymentsAlertCount(string postCode, int regionId, int suburbId, int resourceId)
 
        {

            try
            {
                //int result = 0;

                //if (postCode == "")
                //{
                //    if (resourceId != -1)
                //    {
                //        result = (from e in AMDBMgr.AM_MissedPayments
                //                  join paymentplan in AMDBMgr.AM_PaymentPlan on e.TenantId equals paymentplan.TennantId
                //                  join cse in AMDBMgr.AM_Case on paymentplan.PaymentPlanId equals cse.PaymentPlanId
                //                  where cse.IsActive == true && paymentplan.IsCreated == true && (cse.CaseManager == resourceId || cse.CaseOfficer == resourceId)
                                  


                //                  select e).Count();
                //    }
                //    else
                //    {
                //        result = (from e in AMDBMgr.AM_MissedPayments
                //                  join paymentplan in AMDBMgr.AM_PaymentPlan on e.TenantId equals paymentplan.TennantId
                //                  join cse in AMDBMgr.AM_Case on paymentplan.PaymentPlanId equals cse.PaymentPlanId
                //                  where cse.IsActive == true && paymentplan.IsCreated == true
                                  

                //                  select e).Count();
                //    }
                //}
                //else
                //{
                //    if (resourceId != -1)
                //    {
                //        result = (from e in AMDBMgr.AM_MissedPayments
                //                  join paymentplan in AMDBMgr.AM_PaymentPlan on e.TenantId equals paymentplan.TennantId
                //                  join cse in AMDBMgr.AM_Case on paymentplan.PaymentPlanId equals cse.PaymentPlanId
                //                  join caddress in AMDBMgr.C_ADDRESS on e.CustomerId equals caddress.CUSTOMERID
                //                  where cse.IsActive == true && paymentplan.IsCreated == true && (cse.CaseManager == resourceId || cse.CaseOfficer == resourceId)
                //                  && caddress.POSTCODE == postCode


                //                  select e).Count();
                //    }
                //    else
                //    {
                //        result = (from e in AMDBMgr.AM_MissedPayments
                //                  join paymentplan in AMDBMgr.AM_PaymentPlan on e.TenantId equals paymentplan.TennantId
                //                  join cse in AMDBMgr.AM_Case on paymentplan.PaymentPlanId equals cse.PaymentPlanId
                //                  join caddress in AMDBMgr.C_ADDRESS on e.CustomerId equals caddress.CUSTOMERID
                //                  where cse.IsActive == true && paymentplan.IsCreated == true
                //                  && caddress.POSTCODE == postCode

                //                  select e).Count();
                //    }
                //}

                var total = AMDBMgr.AM_SP_GetMissedPaymentAlerts(postCode, resourceId, regionId, suburbId);
                int recordCount = 0;
                foreach (var it in total)
                {
                    recordCount = it.Value;
                }
                return recordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region IBaseBO<AM_Payment,int> Members


       

        #endregion
        #region"Get Payment List"

        public List<AM_Payment> GetPaymentList(int? paymentPlanId)
        {
            try
            {
                List<AM_PaymentPlanHistory> res = (from his in AMDBMgr.AM_PaymentPlanHistory
                                                   where his.PaymentPlanId == paymentPlanId
                                                   orderby his.PaymentPlanHistoryId descending
                                                   select his).ToList();
                if (res != null)
                {
                    if (res.Count > 0)
                    {
                        int paymentPlanHistoryId = res[0].PaymentPlanHistoryId;
                        List<AM_Payment> list = (from ds in AMDBMgr.AM_Payment
                                                 where ds.PaymentPlanHistoryId == paymentPlanHistoryId
                                                 select ds).ToList();
                        return list;
                    }
                }

                return null;
    }
            catch (EntityException ee)
            {
                throw ee;
}
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        public List<F_RENTJOURNAL> GetCustomerRentLists(int? tenantId, DateTime PeriodStartDate, DateTime PeriodEndDate)
        {
            try
            {
                int[] paymenttypeid = new int[] { 2, 3, 5, 8, 13, 92, 93 }; //payment type Direct Debit, Standing Order,Cash,Cheque,BACS,Payment Card,,Credit Card and Debit Card only

                List<F_RENTJOURNAL> list = (from ds in AMDBMgr.F_RENTJOURNAL
                                            where ds.TENANCYID == tenantId && ds.ACCOUNTTIMESTAMP != null &&
                                                  ds.TRANSACTIONDATE.Value >= PeriodStartDate.Date &&
                                                  ds.TRANSACTIONDATE.Value <= PeriodEndDate.Date &&
                                                  paymenttypeid.Contains(ds.PAYMENTTYPE.Value) && ds.ITEMTYPE.Value.Equals(1) //item rent only
                                            orderby ds.ACCOUNTTIMESTAMP ascending
                                            select ds).ToList();
                return list;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
    

