﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.team_bo;
using Am.Ahv.Entities;
using System.Data.Entity;
namespace Am.Ahv.BusinessObject.team_bo
{
    class TeamBo:BaseBO,ITeamBo
    {
        public TeamBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #region IBaseBO<E_TEAM,int> Members

        public E_TEAM GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<E_TEAM> GetAll()
        {
            try
            {
                List<E_TEAM> teamlist = this.AMDBMgr.E_TEAM.Where(et => et.ACTIVE == 1).ToList();
                if (teamlist.Count > 0)
                {
                    return teamlist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public E_TEAM AddNew(E_TEAM obj)
        {
            throw new NotImplementedException();
        }

        public E_TEAM Update(E_TEAM obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(E_TEAM obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Team by Employee Id"

        public E_TEAM GetTeamByEmployeeId(int empId)
        {
            try
            {
                List<E_TEAM> listTeam = (from ds in AMDBMgr.E_JOBDETAILS
                                         join t in AMDBMgr.E_TEAM on ds.TEAM equals t.TEAMID
                                         where ds.EMPLOYEEID == empId
                                         orderby t.TEAMID
                                         select t).ToList();
                if (listTeam != null)
                {
                    if (listTeam.Count > 0)
                    {
                        return listTeam[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

      
    }
}
