﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;
using System.Transactions;
using Am.Ahv.BoInterface.actionbo;
using System.Data;
using Am.Ahv.BusinessObject.letters_bo;
namespace Am.Ahv.BusinessObject.action_bo
{
    public class ActionBO : BaseBO, IActionBO
    {
        
        
        public ActionBO()
        {
            try
            {
                AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region IBaseBO<AM_Action,int> Members

        AM_Action Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.GetById(int id)
        {
            try
            {
                IQueryable<Am.Ahv.Entities.AM_Action> aquery = from ac in AMDBMgr.AM_Action where ac.ActionId == id select ac;
                List<Am.Ahv.Entities.AM_Action> actionlist = aquery.ToList();
                if (actionlist.Count > 0)
                {

                    return actionlist[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //List<AM_Action> Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.GetAllResources()
        //{

        //}

        AM_Action Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.AddNew(AM_Action obj)
        {
            try
            {
                AMDBMgr.AddToAM_Action(obj);
                SaveChanges();
                return obj;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        AM_Action Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.Update(AM_Action obj)
        {
            throw new NotImplementedException();
            //Am.Ahv.Entities.AM_Action action = 
            //action.ModifiedDate = DateTime.Now;
            //AMDBMgr.SaveChanges();
            //return action;
        }

        bool Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.Delete(AM_Action obj)
        {
            throw new NotImplementedException();
        }

        bool Am.Ahv.BoInterface.basebo.IBaseBO<AM_Action, int>.Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IActionBO Members

        #region "Add Action"

        AM_Action IActionBO.AddAction(AM_Action action, AM_ActionHistory actionhistory, List<int> LetterIds)
        {
            try
            {
                bool success = false;
                

                using (TransactionScope trans = new TransactionScope())
                {
                    {
                        this.AMDBMgr.AddToAM_Action(action);
                        this.AMDBMgr.SaveChanges();
                        actionhistory.ActionId = action.ActionId;
                        action.AM_ActionHistory.Add(actionhistory);
                       

                        AMDBMgr.SaveChanges();
                        AM_ActionAndStandardLetters Sl = null;
                        foreach (int item in LetterIds.Distinct())
                        {
                            Sl = new AM_ActionAndStandardLetters();
                            Sl.ActionId = action.ActionId;
                            Sl.ActionHistoryId = actionhistory.ActionHistoryId;
                            Sl.StandardLetterId = item;
                            Sl.IsActive = true;
                            //Sl.StandardLetterHistoryId = letters.GetStandardLetterHistoryByStutusIdandStandardLetter(item,action.StatusId);
                            
                            
                            action.AM_ActionAndStandardLetters.Add(Sl);
                            this.AMDBMgr.SaveChanges();
                        }


                        
                        trans.Complete();
                        success = true;
                    }
                }
                if (success)
                {
                    return action;
                }
                else
                {
                    return null;
                }

            }

            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region "Get Action Count"

        public int getActionCount(int statusId)
        {
            try
            {
                int count = (from ds in AMDBMgr.AM_Action select ds).Count();
                return count;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Get Status"
        /// <summary>
        /// This method Should be inside StatusBO not here
        /// Code Revision:1.0
        /// Author: Nouman
        /// </summary>
        /// <param name="statusId"></param>
        /// <returns></returns>
        public string getStatus(int statusId)
        {
            try
            {
                var status = (from s in AMDBMgr.AM_Status
                              where s.StatusId == statusId
                              select s.Title);

                return status.First();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "get Action"

        public AM_Action getAction(int actionId)
        {
            try
            {
                IQueryable<AM_Action> action = (from act in AMDBMgr.AM_Action
                                                where act.ActionId == actionId
                                                select act
                                                    );

                AM_Action a = new AM_Action();
                a = loadReference(action.ToList().First());
                return a;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Load Reference"

        private AM_Action loadReference(AM_Action action)
        {
            action.AM_LookupCode1Reference.Load();
            action.AM_LookupCodeReference.Load();
            return action;
        }

        private List<AM_Action> loadReference(List<AM_Action> action)
        {
            foreach (AM_Action a in action)
            {
                a.AM_LookupCode1Reference.Load();
                a.AM_LookupCodeReference.Load();
            }
            return action;
        }

        #endregion

        #region"Update Action"

        public bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistory, List<int> LetterIds)
        {
            try
            {
                AM_Action actionObj = null;
                IQueryable<AM_Action> query = (from act in AMDBMgr.AM_Action
                                               where act.ActionId == actionId
                                               select act);
                List<AM_Action> actionlist = query.ToList();
                bool success = false;

                using (TransactionScope trans = new TransactionScope())
                {
                    {
                        if (actionlist.Count > 0 && actionlist != null)
                        {
                            actionObj = actionlist.First();
                            actionObj.Title = action.Title;
                            actionObj.RecommendedFollowupPeriod = action.RecommendedFollowupPeriod;
                            actionObj.RecommendedFollowupPeriodFrequencyLookup = action.RecommendedFollowupPeriodFrequencyLookup;
                            actionObj.Ranking = action.Ranking;
                            actionObj.NextActionAlert = action.NextActionAlert;
                            actionObj.NextActionAlertFrequencyLookup = action.NextActionAlertFrequencyLookup;
                            actionObj.NextActionDetails = action.NextActionDetails;
                            actionObj.IsProceduralAction = action.IsProceduralAction;
                            actionObj.IsPaymentPlanMandotry = action.IsPaymentPlanMandotry;
                            actionObj.ModifiedDate = System.DateTime.Now;
                            actionObj.ModifiedBy = action.ModifiedBy;
                            actionHistory.CreatedDate = System.DateTime.Now;
                            actionHistory.CreatedBy = action.CreatedBy;
                            actionHistory.ModifiedDate = System.DateTime.Now;
                            actionHistory.ModifiedBy = actionHistory.ModifiedBy;
                            actionObj.AM_ActionHistory.Load();
                            actionObj.AM_ActionHistory.Add(actionHistory);
                            this.AMDBMgr.SaveChanges();
                            AM_ActionAndStandardLetters actionstandard = null;
                            //LettersBo letters = new LettersBo();
                            foreach (int item in LetterIds.Distinct())
                            {
                                if (!IsLetter(item, actionId))
                                {
                                    actionstandard = new AM_ActionAndStandardLetters();
                                    actionstandard.ActionHistoryId = actionHistory.ActionHistoryId;
                                    actionstandard.ActionId = actionObj.ActionId;
                                    actionstandard.StandardLetterId = item;
                                    actionstandard.IsActive = true;
                                   // actionstandard.StandardLetterHistoryId = letters.GetStandardLetterHistoryByStutusIdandStandardLetter(item, action.StatusId);
                                    actionObj.AM_ActionAndStandardLetters.Add(actionstandard);
                                }
                                this.AMDBMgr.SaveChanges();  
                            }

                            
                            trans.Complete();
                            success = true;
                        }
                    }
                }
                if (success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool UpdateLetter(int ActionId, int ActionHistoryId, int StandardLetterId)
        {
            try
            {
                AM_ActionAndStandardLetters actionstandard = new AM_ActionAndStandardLetters();
                actionstandard.ActionId = ActionId;
                actionstandard.ActionHistoryId = ActionHistoryId;
                actionstandard.StandardLetterId = StandardLetterId;
                this.AMDBMgr.AddToAM_ActionAndStandardLetters(actionstandard);
                this.AMDBMgr.SaveChanges();
                return true;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region IActionBO Members


        public bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistroy, List<int> LettterIds, int ActionId2, int Rank2)
        {
            try
            {
                AM_Action actionObj = null;
                IQueryable<AM_Action> query = (from act in AMDBMgr.AM_Action
                                               where act.ActionId == actionId
                                               select act);
                List<AM_Action> actionlist = query.ToList();
                bool success = false;
                using (TransactionScope trans = new TransactionScope())
                {
                    {

                        if (actionlist.Count > 0 && actionlist != null)
                        {
                            actionObj = actionlist.First();
                            actionObj.Title = action.Title;
                            actionObj.RecommendedFollowupPeriod = action.RecommendedFollowupPeriod;
                            actionObj.RecommendedFollowupPeriodFrequencyLookup = action.RecommendedFollowupPeriodFrequencyLookup;
                            actionObj.Ranking = action.Ranking;
                            actionObj.NextActionAlert = action.NextActionAlert;
                            actionObj.NextActionAlertFrequencyLookup = action.NextActionAlertFrequencyLookup;
                            actionObj.NextActionDetails = action.NextActionDetails;
                            actionObj.IsProceduralAction = action.IsProceduralAction;
                            actionObj.IsPaymentPlanMandotry = action.IsPaymentPlanMandotry;
                            actionObj.ModifiedDate = System.DateTime.Now;
                            actionObj.ModifiedBy = action.ModifiedBy;
                            actionHistroy.CreatedDate = System.DateTime.Now;
                            actionHistroy.CreatedBy = action.CreatedBy;
                            actionHistroy.ModifiedDate = System.DateTime.Now;
                            actionHistroy.ModifiedBy = actionHistroy.ModifiedBy;
                            actionObj.AM_ActionHistory.Load();
                            actionObj.AM_ActionHistory.Add(actionHistroy);
                            this.AMDBMgr.SaveChanges();
                            AM_ActionAndStandardLetters actionstandard = null;
                            foreach (int item in LettterIds)
                            {
                                if (!IsLetter(item, actionId))
                                {
                                    actionstandard = new AM_ActionAndStandardLetters();
                                    actionstandard.ActionHistoryId = actionHistroy.ActionHistoryId;
                                    actionstandard.ActionId = actionObj.ActionId;
                                    actionstandard.StandardLetterId = item;
                                    actionstandard.IsActive = true;
                                    actionObj.AM_ActionAndStandardLetters.Add(actionstandard);
                                }
                            }

                            this.AMDBMgr.SaveChanges();
                            success = true;
                            bool flag = updateAction2(ActionId2, Rank2);
                            trans.Complete();
                            if (flag && success)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
                return success;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool updateAction2(int ActionId2, int Rank2)
        {
            try
            {
                IQueryable<AM_Action> actionquery = from ac in this.AMDBMgr.AM_Action where ac.ActionId == ActionId2 select ac;
                if (actionquery.ToList() != null && actionquery.ToList().Count > 0)
                {
                    AM_Action ac = actionquery.ToList().First();
                    ac.Ranking = Rank2;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IsLetterAttached
        private bool IsLetter(int item, int actionId)
        {
            try
            {
                IQueryable<AM_ActionAndStandardLetters> acquery = from ac in this.AMDBMgr.AM_ActionAndStandardLetters
                                                                  where ac.StandardLetterId == item && ac.IsActive == true && ac.ActionId == actionId
                                                                  select ac;
                /* Commented By Zunair Minhas on 22 June 2010
                IQueryable<AM_ActionAndStandardLetters> acquery = from ac in this.AMDBMgr.AM_ActionAndStandardLetters where ac.StandardLetterId == item select ac;
                 * */
                List<AM_ActionAndStandardLetters> actionstandardLetter = acquery.ToList();
                if (actionstandardLetter != null && actionstandardLetter.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion

        #region"Add Action History"

        private void addActionHistory(AM_ActionHistory history)
        {
            try
            {
                AMDBMgr.AddToAM_ActionHistory(history);
                AMDBMgr.SaveChanges();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Action By Status"

        public List<AM_Action> getActionByStatus(int statusId)
        {
            try
            {
                IQueryable<AM_Action> actionList = (from action in AMDBMgr.AM_Action
                                                    where action.StatusId == statusId && action.Title != "Transferred"  
                                                    orderby action.Ranking
                                                    select action);


                List<AM_Action> actionllister = actionList.ToList();
                if (actionllister.Count > 0)
                {
                    actionllister = loadReference(actionllister);
                    return actionllister;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public IQueryable GetActionListByStatusId(int statusId)
        {
            try
            {
                IQueryable<AM_Action> actionList = (from action in AMDBMgr.AM_Action
                                                    where action.StatusId == statusId
                                                    orderby action.Ranking
                                                    select action);


                //List<AM_Action> actionllister = actionList.ToList();
                //if (actionllister.Count > 0)
                //{
                //    actionllister = loadReference(actionllister);
                //    return actionList;
                //}
                //else
                //{
                //    return null;
                //}
                return actionList;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region"Disable Action Document"

        public void DisableActionDocument(int docId, int actionId)
        {
            try
            {
                AM_ActionAndStandardLetters actionLetter = new AM_ActionAndStandardLetters();
                List<AM_ActionAndStandardLetters> letterList = (from ds in AMDBMgr.AM_ActionAndStandardLetters
                                                                where ds.ActionId == actionId && ds.StandardLetterId == docId && ds.IsActive == true
                                                                select ds).ToList<AM_ActionAndStandardLetters>();
                actionLetter = letterList.First();
                actionLetter.IsActive = false;
                AMDBMgr.SaveChanges();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Next Action"

        public AM_Action GetNextAction(int actionId, int statusId)
        {
            AM_Action action = GetActionRanking(actionId);
            int ranking = action.Ranking;
            List<AM_Action> ListAction = (from ds in AMDBMgr.AM_Action
                                          where ds.Ranking == (ranking + 1) && ds.StatusId == statusId
                                          select ds).ToList();
            if (ListAction != null && ListAction.Count > 0)
            {
                return ListAction[0];
            }
            else
                return null;
        }

        #endregion

        #endregion

        #region Get Action By Rank

        public AM_Action GetActionByRank(int Rank)
        {
            try
            {
                IQueryable<AM_Action> actionquery = from ac in this.AMDBMgr.AM_Action where ac.Ranking == Rank select ac;
                if (actionquery.ToList() != null && actionquery.ToList().Count > 0)
                {
                    return actionquery.ToList().First();
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IActionBO Members

        public int GetActionRankByStatusId(int StatusId)
        {
            try
            {
                IQueryable<AM_Action> query = from a in this.AMDBMgr.AM_Action where a.AM_Status.StatusId == StatusId select a;
                List<AM_Action> actionlist = query.ToList();
                if (actionlist.Count > 0 && actionlist != null)
                {
                    return actionlist.Count;
                }

                else
                    if (actionlist.Count == 0)
                    {
                        return -2;
                    }
                    else
                    {
                        return -1;
                    }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IActionBO Members

        public AM_Action GetActionByRankStatus(int StatusId, int Rank)
        {
            try
            {
                IQueryable<AM_Action> aq = from a in this.AMDBMgr.AM_Action where a.AM_Status.StatusId == StatusId && a.Ranking == Rank select a;
                List<AM_Action> ActionList = aq.ToList();
                if (ActionList.Count > 0)
                {
                    return ActionList.First();
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Action History By Action Id"

        public int GetActionHistoryByActionId(int actionId)
        {
            try
            {
                var query = (from ds in AMDBMgr.AM_ActionHistory
                             where ds.ActionId == actionId
                             orderby ds.ActionHistoryId
                             select new
                             {
                                 ds.ActionHistoryId
                             });


                //var  query1 = from p in this.AMDBMgr.AM_ActionHistory where p.ActionId == actionId orderby p.ActionHistoryId select p;
                //return query1.ToList().Last().ActionHistoryId;
                int id = 0;

                foreach (var t in query)
                {
                    id = t.ActionHistoryId;
                }
                return id;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IBaseBO<AM_Action,int> Members


        public List<AM_Action> GetAll()
        {
            try
            {
                List<Am.Ahv.Entities.AM_Action> action = AMDBMgr.AM_Action.ToList();
                if (action.Count > 0)
                {
                    return action;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Action Ranking"

        public AM_Action GetActionRanking(int actionId)
        {
            try
            {
                List<AM_Action> rank = (from r in AMDBMgr.AM_Action
                                        where r.ActionId == actionId
                                        select r).ToList();
                if (rank != null && rank.Count > 0)
                {
                    return rank.First();
                }
                else
                    return null;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Check Payment Plan Mandatory"

        public bool CheckPaymentPlanMandatory(int actionId)
        {
            try
            {
                List<AM_Action> listAction = (from ds in AMDBMgr.AM_Action
                                              where ds.ActionId == actionId
                                              select ds).ToList();
                bool value = false;
                if (listAction[0].IsPaymentPlanMandotry == null)
                {
                    value = false;
                }
                else
                {
                    value = listAction[0].IsPaymentPlanMandotry.Value;
                }
                return value;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IActionBO Members

        public bool IsActionAsigned(int ActionId)
        {
            try
            {
                IQueryable<AM_CaseHistory> casequery = from ca in this.AMDBMgr.AM_CaseHistory where ca.ActionId == ActionId select ca;
                //     IQueryable<AM_CaseActionIgnoredAndRecorded> ignored = from icase in this.AMDBMgr.AM_CaseActionIgnoredAndRecorded where icase.ActionId == ActionId select icase;
                //if (casequery.ToList() != null && casequery.ToList().Count > 0 && ignored.ToList() != null && ignored.ToList().Count > 0)
                if (casequery.ToList() != null && casequery.ToList().Count > 0)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public bool DeleteAction(int ActionId)
        {
            try
            {
                bool success = false;
                using (TransactionScope trans = new TransactionScope())
                {
                    List<AM_ActionHistory> actionHistory = (from AHistory in AMDBMgr.AM_ActionHistory where AHistory.ActionId == ActionId select AHistory).ToList<AM_ActionHistory>();
                    if (actionHistory != null && actionHistory.Count > 0)
                    {
                        foreach (AM_ActionHistory Ah in actionHistory)
                        {
                            List<AM_ActionAndStandardLetters> letter = (from ALetters in AMDBMgr.AM_ActionAndStandardLetters where ALetters.ActionHistoryId == Ah.ActionHistoryId select ALetters).ToList<AM_ActionAndStandardLetters>();
                            if (letter != null && letter.Count > 0)
                            {
                                foreach (AM_ActionAndStandardLetters ASL in letter)
                                {
                                    AMDBMgr.AM_ActionAndStandardLetters.DeleteObject(ASL);
                                    AMDBMgr.SaveChanges();
                                }
                            }

                        }
                        foreach (AM_ActionHistory Ah in actionHistory)
                        {
                            AMDBMgr.AM_ActionHistory.DeleteObject(Ah);
                            AMDBMgr.SaveChanges();
                        }
                    }

                    List<AM_Action> Action = (from ActionOjb in AMDBMgr.AM_Action where ActionOjb.ActionId == ActionId select ActionOjb).ToList<AM_Action>();
                    if (Action != null && Action.Count > 0)
                    {
                        AMDBMgr.AM_Action.DeleteObject(Action[0]);
                        AMDBMgr.SaveChanges();


                    }
                    trans.Complete();
                    success = true;
                }
                return success;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IActionBO Members
        
        public bool UpdateActionRanking(int ActiondId, int Rank)
        {
            try
            {
                IQueryable<AM_Action> actionquery = from ac in this.AMDBMgr.AM_Action where ac.ActionId == ActiondId select ac;
                if (actionquery.ToList() != null && actionquery.ToList().Count > 0)
                {
                    AM_Action obj = actionquery.First();
                    obj.Ranking = Rank;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Follow Up Period Consecutive"

        public bool isFollowUpPeriodConsecutive(int statusId,int periodVal, string period,int ranking)
        {
           List<AM_Action> Action = (from ActionOjb in AMDBMgr.AM_Action orderby ActionOjb.StatusId ascending select ActionOjb).ToList<AM_Action>();
           int i = 0,j=0;
           int startLimit = 0, endLimit = 0; int value =0;
           for (j = 0; j < Action.Count; j++)
           {
               if (Action[j].StatusId == statusId)
               {
                   break;
               }
           }
           for (i = j; i < Action.Count; i++)
           {
             
                   if (Action[i].StatusId != statusId)
                   {
                       break;
                   }
           }
            if (i - 1 >= 0)
            {
                if (Action[i - 1].AM_LookupCode1.CodeName == "Weeks")
                {
                    startLimit = Action[i-1].RecommendedFollowupPeriod * 7;
                }
                else if(Action[i - 1].AM_LookupCode1.CodeName == "Months")
                {
                     startLimit = Action[i-1].RecommendedFollowupPeriod * 7 * 4;
                }
                else if (Action[i - 1].AM_LookupCode1.CodeName == "Years")
                {
                    startLimit = Action[i - 1].RecommendedFollowupPeriod * 7 * 4 * 12;
                }
                else if (Action[i - 1].AM_LookupCode1.CodeName == "Days")
                {
                    startLimit = Action[i - 1].RecommendedFollowupPeriod;
                }
            }

            if (i < Action.Count)
            {
                if (Action[i].AM_LookupCode1.CodeName == "Weeks")
                {
                    endLimit = Action[i].RecommendedFollowupPeriod * 7;
                }
                else if (Action[i].AM_LookupCode1.CodeName == "Months")
                {
                    endLimit = Action[i].RecommendedFollowupPeriod * 7 * 4;
                }
                else if (Action[i].AM_LookupCode1.CodeName == "Years")
                {
                    endLimit = Action[i].RecommendedFollowupPeriod * 7 * 4 * 12;
                }
                else if (Action[i].AM_LookupCode1.CodeName == "Days")
                {
                    endLimit = Action[i].RecommendedFollowupPeriod;
                }
            }

            if (period == "Weeks")
            {
                value = periodVal * 7;
            }
            else if (period == "Months")
            {
                value = periodVal * 7*4;
            }
            else if (period == "Years")
            {
                value = periodVal * 7*4*12;
            }
            else if (period == "Days")
            {
                value = periodVal;
            }

            if (endLimit == 0)
            {
                if (value > startLimit)
                {
                    return true;
                }
            }
            else if (startLimit > 0 && endLimit > 0)
            {
                if (value > startLimit && value < endLimit)
                {
                    return true;
                }
            }
            return false;                
        }

        #endregion

        #region"Is Update Follow Up Period Valid"

        public bool isUpdatedFollowUpPeriodValid(int actionId,int periodVal, string period)
        {
            int i = 0;
            int startLimit = 0, endLimit = 0; int value = 0;
            List<AM_Action> Action = (from ActionOjb in AMDBMgr.AM_Action orderby ActionOjb.StatusId ascending select ActionOjb).ToList<AM_Action>();
            if (Action.Count == 1)
            {
                return true;
            }
            else
            {
                for (i = 0; i < Action.Count; i++)
                {
                    if (Action[i].ActionId == actionId)
                    {
                        break;
                    }
                }
                if (i - 1 >= 0)
                {
                    if (Action[i - 1].AM_LookupCode1.CodeName == "Weeks")
                    {
                        startLimit = Action[i - 1].RecommendedFollowupPeriod * 7;
                    }
                    else if (Action[i - 1].AM_LookupCode1.CodeName == "Months")
                    {
                        startLimit = Action[i - 1].RecommendedFollowupPeriod * 7 * 4;
                    }
                    else if (Action[i - 1].AM_LookupCode1.CodeName == "Years")
                    {
                        startLimit = Action[i - 1].RecommendedFollowupPeriod * 7 * 4 * 12;
                    }
                    else if (Action[i - 1].AM_LookupCode1.CodeName == "Days")
                    {
                        startLimit = Action[i - 1].RecommendedFollowupPeriod;
                    }
                }

                if (i + 1 <  Action.Count)
                {
                    if (Action[i + 1].AM_LookupCode1.CodeName == "Weeks")
                    {
                        endLimit = Action[i + 1].RecommendedFollowupPeriod * 7;
                    }
                    else if (Action[i + 1].AM_LookupCode1.CodeName == "Months")
                    {
                        endLimit = Action[i + 1].RecommendedFollowupPeriod * 7 * 4;
                    }
                    else if (Action[i + 1].AM_LookupCode1.CodeName == "Years")
                    {
                        endLimit = Action[i + 1].RecommendedFollowupPeriod * 7 * 4 * 12;
                    }
                    else if (Action[i + 1].AM_LookupCode1.CodeName == "Days")
                    {
                        endLimit = Action[i + 1].RecommendedFollowupPeriod;
                    }
                }

                if (period == "Weeks")
                {
                    value = periodVal * 7;
                }
                else if (period == "Months")
                {
                    value = periodVal * 7 * 4;
                }
                else if (period == "Years")
                {
                    value = periodVal * 7 * 4 * 12;
                }
                else if (period == "Days")
                {
                    value = periodVal;
                }

                if (endLimit == 0)
                {
                    if (value > startLimit)
                    {
                        return true;
                    }
                }
                else if (startLimit > 0 && endLimit > 0)
                {
                    if (value > startLimit && value < endLimit)
                    {
                        return true;
                    }
                }

            }
            return false;

        }

        #endregion

        #region"Is Ignored Procedural Action Remaining"

        public bool IsIgnoredProceduralActionRemaining(int statusId, int rankId, int currentActionId, int currentStatusId, int previousIgnoredActionId, int isFirstIteration)
        {
            try
            {
                List<AM_Status> status = (from st in AMDBMgr.AM_Status
                                          where st.StatusId == currentStatusId
                                          select st).ToList();
              /*  if (status[0].Ranking > 1)
                {
                    rankId -= 1;
                }*/
                List<AM_Action> listActon = null;
                if (isFirstIteration == -1)
                {
                    listActon = (from ds in AMDBMgr.AM_Action
                                 where ds.StatusId >= statusId && ds.Ranking >= rankId &&
                                       ds.ActionId < currentActionId && ds.StatusId < currentStatusId &&
                                       ds.IsProceduralAction == true
                                 select ds).ToList();
                }
                else
                {
                    listActon = (from ds in AMDBMgr.AM_Action
                                 where ds.StatusId >= statusId && ds.ActionId > previousIgnoredActionId &&
                                       ds.ActionId < currentActionId && ds.StatusId < currentStatusId &&
                                       ds.IsProceduralAction == true
                                 select ds).ToList();
                }
                if (listActon != null)
                {
                    if (listActon.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Preocedural Action Left With in Stage"

        public bool IsProceduralActionLeftWithinStage(int stageId, int caseId)
        {
            try
            {
                int actionRecordedCount = (from action in AMDBMgr.AM_Action
                                           join caseHistory in AMDBMgr.AM_CaseHistory on action.ActionId equals caseHistory.ActionId
                                           where action.IsProceduralAction == true && action.StatusId == stageId && caseHistory.IsActionRecorded == true &&
                                           caseHistory.CaseId == caseId
                                           select action.ActionId).Count();

                int proceduralActionCount = (from paction in AMDBMgr.AM_Action
                                             where paction.StatusId == stageId && paction.IsProceduralAction == true
                                             select paction.ActionId).Count();
                if (proceduralActionCount == null)
                {
                    return false;
                }
                if (proceduralActionCount == 0)
                {
                    return false;
                }
                else if (proceduralActionCount > 0)
                {
                    if (actionRecordedCount == null)
                    {
                        return true;
                    }
                    else if (actionRecordedCount == 0)
                    {
                        return true;
                    }
                    if (actionRecordedCount == proceduralActionCount)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Procedural Action Left With In Stage Range"

        public bool IsProceduralActionLeftWithInStageRange(int currentStageRank)
        {
            try
            {
                int proceduralCount = (from ds in AMDBMgr.AM_Action
                                       join status in AMDBMgr.AM_Status on ds.StatusId equals status.StatusId
                                       where status.Ranking >= 1 && status.Ranking < currentStageRank && ds.IsProceduralAction == true
                                       select ds).Count();
                if (proceduralCount == null)
                {
                    return false;
                }
                else if (proceduralCount == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get First Procedural Ignored Action"

        public AM_Action GetFirstProceduralIgnoredAction(int statusId, int caseId)
        {
            try
            {
                List<AM_Action> list = (from ds in AMDBMgr.AM_Action
                                            where ds.StatusId == statusId && ds.IsProceduralAction == true
                                            select ds).ToList();
                if(list == null)
                {
                    return null;
                }
                else if(list.Count == 0)
                {
                    return null;
                }
                else
                {
                    int count = 0;
                    for(int i =0; i < list.Count;i++)
                    {
                        int id = list[i].ActionId;
                        List<AM_CaseHistory> listHistory = (from history in AMDBMgr.AM_CaseHistory
                                                                where history.ActionId == id && history.IsActionRecorded == true && history.CaseId == caseId
                                                                select history).ToList();
                        if(listHistory == null)
                        {
                            break;
                        }
                        else if(listHistory.Count == 0)
                        {
                            break;
                        }
                        count++;
                    }
                    count = (count > 0) ? count - 1 : count;

                    return list[count];
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Procedural Action Recorded"

        public bool IsProceduralActionRecorded(int statusId, int ranking, int caseId)
        {
            try
            {
                List<AM_CaseHistory> list = (from ds in AMDBMgr.AM_CaseHistory
                                             join action in AMDBMgr.AM_Action on ds.ActionId equals action.ActionId
                                             where ds.StatusId == statusId && ds.CaseId == caseId && ds.IsActionRecorded == true && action.Ranking == ranking
                                             select ds).ToList();
                if (list == null)
                {
                    return false;
                }
                else if (list.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"is Action Already Recorded"

        public bool IsActionAlreadyRecorded(int actionId, int caseId)
        {
            int count = (from ds in AMDBMgr.AM_CaseHistory
                         where ds.ActionId == actionId && ds.CaseId == caseId && ds.IsActionRecorded == true
                         select ds).Count();
            
            if (count == 0 || count == null)
            {
                return false;
            }
            else if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        #endregion

    }
}
