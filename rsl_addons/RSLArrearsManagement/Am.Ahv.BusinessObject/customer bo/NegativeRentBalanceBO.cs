﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.customerbo;
using Am.Ahv.BusinessObject.base_bo;
using System.Data;
namespace Am.Ahv.BusinessObject.Customer_bo
{
    class NegativeRentBalanceBO : BaseBO, INegativeRentBalance
    {
        public NegativeRentBalanceBO()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }


        #region INegativeRentBalance Members

        public int GetNegativeRentBalance(int timer)
        {
            try
            {
                this.AMDBMgr.CommandTimeout = timer;
                var result = this.AMDBMgr.AM_SP_GetNegativeTenancyBalanceCount();
                int count = 0;
                if (result != null)
                {
                    foreach (var t in result)
                    {
                        count = t.Value;
                    }
                }
                return count;
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetNegativeBalanceTenantsCount()
        {
            try
            {
                List<AM_NegativeRentBalanceCount> negativeList = this.AMDBMgr.AM_NegativeRentBalanceCount.ToList();
                if (negativeList != null && negativeList.Count > 0)
                {
                    return negativeList.First().NegativeRentCount;
                }
                else
                {
                    return 0;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int TotalTenancies()
        {
            try
            {
                int totaltenantcount = this.AMDBMgr.C_TENANCY.Where(c => c.ENDDATE == null).Count();
                if (totaltenantcount > 0)
                {
                    return totaltenantcount;
                }
                else
                {
                    return 0;
                }
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int LetPropertiesCount()
        {
            try
            {
                int count = this.AMDBMgr.P__PROPERTY.Where(p => p.STATUS == 2).Count();
                if (count > 0)
                {
                    return count;
                }
                else
                {
                    return 0;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion



       
    }
}
