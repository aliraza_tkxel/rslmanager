﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Am.Ahv.BoInterface;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.customerbo;
using Am.Ahv.BusinessObject.base_bo;
using System.Data.Objects;



namespace Am.Ahv.BusinessObject.Customer_bo
{
    public class CustomerBo:BaseBO,ICustomerBo
    {
        public CustomerBo()
         {
             this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
             
         }


        #region IBaseBO<C__CUSTOMER,int> Members

        public C__CUSTOMER GetById(int id)
        {
            throw new NotImplementedException();

        }

        public List<C__CUSTOMER> GetAll()
        {
            throw new NotImplementedException();
        }

        public C__CUSTOMER AddNew(C__CUSTOMER obj)
        {
            throw new NotImplementedException();
        }

        public C__CUSTOMER Update(C__CUSTOMER obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(C__CUSTOMER obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Customer Details By Tennat Id"

        public C__CUSTOMER getCustomerDetailsByTenantId(int tenantId)
        {
            IQueryable<C__CUSTOMER> customer = (from t in AMDBMgr.C_TENANCY
                                   join t0 in AMDBMgr.C_CUSTOMERTENANCY on t.TENANCYID equals t0.TENANCYID  
                                   join t1 in AMDBMgr.C__CUSTOMER on t0.CUSTOMERID equals t1.CUSTOMERID
                                   where
                                     t.TENANCYID == 1 && t0.ENDDATE== null
                                   select t1
                                   );      
             C__CUSTOMER cust= customer.First<C__CUSTOMER>();
             return cust;           
        }

        #endregion

        #region"Get Address By Customer Id"

        public C_ADDRESS getAddressByCustomerId(int customerId)
        {
            IQueryable<C_ADDRESS> customerAddress = (from t in AMDBMgr.C_ADDRESS
                                                     where t.CUSTOMERID == customerId
                                                     select t
                                                    );
            if (customerAddress.Count<C_ADDRESS>() > 0)
            {
                C_ADDRESS address = customerAddress.First<C_ADDRESS>();
                return address;
            }
            else
            {
                return null;
            }
        }

        #endregion



        #region"Get Address By Customer Id"

        public IQueryable getAddressAndTypeByCustomerId(int customerId)
        {

            var customerAddress = (from t in AMDBMgr.C_ADDRESS
                                                     where t.CUSTOMERID == customerId
                                                     join t0 in AMDBMgr.C_ADDRESSTYPE on t.ADDRESSTYPE equals t0.ADDRESSTYPEID
                                                     select new
                                                     {
                                                         t.ADDRESSID,
                                                         t0.DESCRIPTION
                                                     });


            return customerAddress.AsQueryable();

            //if (customerAddress.Count<C_ADDRESS>() > 0)
            //{
            //    C_ADDRESS address = customerAddress.First<C_ADDRESS>();
            //    return address;
            //}
            //else
            //{
            //    return null;
            //}
        }

        #endregion

        #region"Customer Latest Rent"

        public F_RENTJOURNAL customersLatestRent(int tenantId)
        {
            F_RENTJOURNAL rentJournal=new F_RENTJOURNAL();
            return rentJournal;            
        }

        #endregion

        #region"Get Customer Tenancy By Tenant Id"

        public C_TENANCY getCustomersTenancyByTenantId(int tenantId)
        {
            IQueryable<C_TENANCY> customerTenancy = (from t in AMDBMgr.C_TENANCY                                                
                                                where
                                                  t.TENANCYID == 1
                                                select t);
           C_TENANCY tenancy= customerTenancy.First<C_TENANCY>();            
           return tenancy;
        }

        #endregion

        #region"Get Customer Information"

        public AM_SP_GetCustomerInformation_Result GetCustomerInformation(int tenantId, int customerId)
        {
            try
            {
//              var temp = AMDBMgr.AM_SP_GetCustomerInformation(tenantId) ;
                List<AM_SP_GetCustomerInformation_Result> customerlist = AMDBMgr.AM_SP_GetCustomerInformation(tenantId, customerId).ToList();
                if (customerlist != null && customerlist.Count > 0)
                {
                    return customerlist.First();
                }
                else
                {
                    return null;
                }
                
                //AM_SP_GetCustomerInformation_Result customer = new AM_SP_GetCustomerInformation_Result();
                /*foreach (var value in temp)
                {
                    customer.rentAmount = value.rentAmount;
                    customer.RentBalance = value.RentBalance;
                    customer.suburb = value.suburb;
                    customer.TENANCYID = value.TENANCYID;
                    customer.lastPayment = value.lastPayment;
                    customer.customerName = value.customerName;
                    customer.customerAddress = value.customerAddress;
                    customer.AMOUNT = value.AMOUNT;
                }*/
                //return customer;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Customer Last Payment"

        public AM_SP_GetCustomerLastPayment_Result GetCustomerLastPayment(int tenantId, int customerId)
        {
            try
            {
                var result = AMDBMgr.AM_SP_GetCustomerLastPayment(tenantId, customerId);

                AM_SP_GetCustomerLastPayment_Result lastPayment = new AM_SP_GetCustomerLastPayment_Result();
                foreach (var t in result)
                {
                    //lastPayment.CRNNumber = t.CRNNumber;
                    lastPayment.lastPayment = t.lastPayment;
                    lastPayment.EstimatedHBDue = t.EstimatedHBDue;
                    lastPayment.NextHB = t.NextHB;
                    lastPayment.Startdate = t.Startdate;
                    //lastPayment.TenancyId = t.TenancyId;
                    lastPayment.TransactionDate = t.TransactionDate;
                    lastPayment.HBID = t.HBID;
                }

                return lastPayment;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        #endregion

        #region "Get Customer Status"
        public IQueryable GetCustomerStatus()
        {
            try
            {
                int[] ids = {  2, 3};
                var customerStatus = (from t in AMDBMgr.C_CUSTOMERTYPE
                                     
                                      orderby t.CUSTOMERTYPEID
                                      where ids.Contains(t.CUSTOMERTYPEID)
                                    select new
                                    {
                                        t.CUSTOMERTYPEID,
                                        CustomerStatusId = (Int32?)t.CUSTOMERTYPEID,
                                        CustomerStatusDescription = t.DESCRIPTION
                                    });
                return customerStatus;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion

        #region"Register Customer Rent Parameters Schedule Job Status"

        public bool RegisterCustomerRentParametersScheduleJobStatus()
        {
            try
            {
                if (!IsScheduleJobInProgress())
                {
                    AM_Schedule_Job_Log log = new AM_Schedule_Job_Log();
                    log.ScheduleJobType = "CustomerRentParameters";
                    log.Status = "Running";
                    log.StartTime = DateTime.Now;
                    log.Message = "Job in progress.";
                    AMDBMgr.AddToAM_Schedule_Job_Log(log);
                    AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #endregion

        #region"Check Schedule Job Status"

        public bool IsScheduleJobInProgress()
        {
            try
            {
                List<AM_Schedule_Job_Log> jobLog = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                                    where ds.ScheduleJobType == "CustomerRentParameters" && ds.StartTime != null
                                                    orderby ds.Id descending
                                                    select ds).ToList();

                if (jobLog == null)
                {
                    return false;
                }
                else if (jobLog.Count == 0)
                {
                    return false;
                }
                else
                {
                    AM_Schedule_Job_Log job = jobLog.First();
                    if (job.Status == "Running")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Remove Customer Rent Records"

        public void RemoveCustomerRentRecords()
        {
            try
            {                
                AMDBMgr.AM_SP_RemoveCustomerRentParameters();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
 
        }

        #endregion

        #region"Get Customers Rent Parameters"

        public void GetCustomersRentParameters(int timer)
        {
            try
            {
                RemoveCustomerRentRecords();
                AMDBMgr.CommandTimeout = timer;
                AMDBMgr.AM_SP_GetCustomerRentParameters();
               // AMDBMgr.Connection.Close();
                //UpdateEstimatedDueHBPayment(timer);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"End Customer Rent Parameters Schedule Job"

        public bool EndCustomerRentParametersScheduleJob()
        {
            try
            {
                List<AM_Schedule_Job_Log> log = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                           where ds.ScheduleJobType == "CustomerRentParameters" && ds.StartTime != null && ds.Status == "Running"
                                           orderby ds.Id descending
                                           select ds).ToList();

                if (log != null)
                {
                    if (log.Count > 0)
                    {
                        AM_Schedule_Job_Log job = new AM_Schedule_Job_Log();
                        job.ScheduleJobType = log[0].ScheduleJobType;
                        job.StartTime = log[0].StartTime;
                        job.Status = "Completed";
                        job.EndTime = DateTime.Now;
                        job.Message = "Job Completed Successfully";
                        AMDBMgr.AddToAM_Schedule_Job_Log(job);
                        AMDBMgr.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Error Customer Rent Parameters Schedule Job"

        public bool ErrorCustomerRentParametersScheduleJob(string errorMessage)
        {
            try
            {
                List<AM_Schedule_Job_Log> log = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                                 where ds.ScheduleJobType == "CustomerRentParameters" && ds.StartTime != null && ds.Status == "Running"
                                                 orderby ds.Id descending
                                                 select ds).ToList();

                if (log != null)
                {
                    if (log.Count > 0)
                    {
                        AM_Schedule_Job_Log job = new AM_Schedule_Job_Log();
                        job.ScheduleJobType = log[0].ScheduleJobType;
                        job.StartTime = log[0].StartTime;
                        job.Status = "Error";
                        job.EndTime = DateTime.Now;
                        job.Message = errorMessage;
                        AMDBMgr.AddToAM_Schedule_Job_Log(job);
                        AMDBMgr.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Estimated Due HB Payment"

        public void UpdateEstimatedDueHBPayment(int timer)
        {
            try
            {
                AMDBMgr.CommandTimeout = timer;
                List<int> listCustomers = (from ds in AMDBMgr.AM_Customer_Rent_Parameters
                                                                   select ds.TenancyId.Value).ToList();
                if (listCustomers != null)
                {
                    if (listCustomers.Count > 0)
                    {
                        for (int i = 0; i < listCustomers.Count; i++)
                        {
                            int id = listCustomers[i];
                            double HB = 0.0;
                            var  estHb = AMDBMgr.AM_SP_Get_Estimated_Due_HB_Payment(id);
                            foreach (var t in estHb)
                            {
                                HB = Convert.ToDouble(t.ESTHB);
                            }
                            List<AM_Customer_Rent_Parameters> list = (from cust in AMDBMgr.AM_Customer_Rent_Parameters
                                                                      where cust.TenancyId == id
                                                                      select cust).ToList();
                            for (int j = 0; j < list.Count; j++)
                            {
                                AM_Customer_Rent_Parameters rent = new AM_Customer_Rent_Parameters();
                                rent.TenancyId = list[j].TenancyId;
                                rent.Title = list[j].Title;
                                rent.SalesLedgerBalance = list[j].SalesLedgerBalance;
                                rent.RentBalance = list[j].RentBalance;
                                rent.NextHB = list[j].NextHB;
                                rent.LastPaymentDate = list[j].LastPaymentDate;
                                rent.LastPayment = list[j].LastPayment;
                                rent.LastName = list[j].LastName;
                                rent.ID = list[j].ID;
                                rent.FirstName = list[j].FirstName;
                                rent.CustomerId = list[j].CustomerId;
                                rent.CustomerAddress = list[j].CustomerAddress;
                                rent.EstimatedHBDue = HB;
                                AMDBMgr.SaveChanges();
                            }

                        }
                    }
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Register Customer Rent Parameters History Schedule Job Status"

        public bool RegisterCustomerRentParametersHistoryScheduleJobStatus()
        {
            try
            {
                if (!IsScheduleHistoryJobInProgress())
                {
                    AM_Schedule_Job_Log log = new AM_Schedule_Job_Log();
                    log.ScheduleJobType = "CustomerRentParametersHistory";
                    log.Status = "Running";
                    log.StartTime = DateTime.Now;
                    log.Message = "Job in progress.";
                    AMDBMgr.AddToAM_Schedule_Job_Log(log);
                    AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Check Schedule History Job Status"

        public bool IsScheduleHistoryJobInProgress()
        {
            try
            {
                List<AM_Schedule_Job_Log> jobLog = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                                    where ds.ScheduleJobType == "CustomerRentParametersHistory" && ds.StartTime != null
                                                    orderby ds.Id descending
                                                    select ds).ToList();

                if (jobLog == null)
                {
                    return false;
                }
                else if (jobLog.Count == 0)
                {
                    return false;
                }
                else
                {
                    AM_Schedule_Job_Log job = jobLog.First();
                    if (job.Status == "Running")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Customers Rent Parameters History"

        public void GetCustomersRentParametersHistory(int timer)
        {
            try
            {
                //RemoveCustomerRentRecords();
                AMDBMgr.CommandTimeout = timer;
                AMDBMgr.AM_SP_GetCustomerRentParametersHistory();
                // AMDBMgr.Connection.Close();
                //UpdateEstimatedDueHBPayment(timer);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"End Customer Rent Parameters History Schedule Job"

        public bool EndCustomerRentParametersHistoryScheduleJob()
        {
            try
            {
                List<AM_Schedule_Job_Log> log = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                                 where ds.ScheduleJobType == "CustomerRentParametersHistory" && ds.StartTime != null && ds.Status == "Running"
                                                 orderby ds.Id descending
                                                 select ds).ToList();

                if (log != null)
                {
                    if (log.Count > 0)
                    {
                        AM_Schedule_Job_Log job = new AM_Schedule_Job_Log();
                        job.ScheduleJobType = log[0].ScheduleJobType;
                        job.StartTime = log[0].StartTime;
                        job.Status = "Completed";
                        job.EndTime = DateTime.Now;
                        job.Message = "Job Completed Successfully";
                        AMDBMgr.AddToAM_Schedule_Job_Log(job);
                        AMDBMgr.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Error Customer Rent Parameters History Schedule Job"

        public bool ErrorCustomerRentParameterHistorysScheduleJob(string errorMessage)
        {
            try
            {
                List<AM_Schedule_Job_Log> log = (from ds in AMDBMgr.AM_Schedule_Job_Log
                                                 where ds.ScheduleJobType == "CustomerRentParametersHistory" && ds.StartTime != null && ds.Status == "Running"
                                                 orderby ds.Id descending
                                                 select ds).ToList();

                if (log != null)
                {
                    if (log.Count > 0)
                    {
                        AM_Schedule_Job_Log job = new AM_Schedule_Job_Log();
                        job.ScheduleJobType = log[0].ScheduleJobType;
                        job.StartTime = log[0].StartTime;
                        job.Status = "Error";
                        job.EndTime = DateTime.Now;
                        job.Message = errorMessage;
                        AMDBMgr.AddToAM_Schedule_Job_Log(job);
                        AMDBMgr.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
