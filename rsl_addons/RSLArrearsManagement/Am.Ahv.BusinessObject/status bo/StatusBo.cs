﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.status_bo;
using Am.Ahv.Entities;
using System.Transactions;
using Am.Ahv.BusinessObject.base_bo;
using System.Data;
using System.Data.Objects;
namespace Am.Ahv.BusinessObject.status_bo
{
    class StatusBo : BaseBO, IStatusBo
    {
        public StatusBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Add Status

        public AM_Status AddStatus(AM_Status status, AM_StatusHistory statushistory)
        {

            bool success = false;
            try
            {
                 using (TransactionScope trans = new TransactionScope())
                {
                    this.AMDBMgr.AddToAM_Status(status);

                    this.AMDBMgr.SaveChanges();
                    statushistory.StatusId = status.StatusId;
                    this.AMDBMgr.AddToAM_StatusHistory(statushistory);
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                     //AMDBMgr.SaveChanges(false);
                    trans.Complete();
                    success = true;
                }
                 if (success)
                 {
                     return status;
                 }
                 else

                 {
                     return null;
                 }
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region IBaseBO<AM_Status,int> Members

        public AM_Status GetById(int id)
        {
            try
            {
                IQueryable<AM_Status> squery = from st in this.AMDBMgr.AM_Status where st.StatusId == id select st;
                List<AM_Status> slist = squery.ToList();
                if (slist.Count > 0)
                {
                    return slist[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_Status> GetAll()
        {
            try
            {
                var query = (from s in AMDBMgr.AM_Status
                             orderby s.Ranking
                             select s);
                List<AM_Status> statuslist = query.ToList();
                //AM_Status  statusAll=new AM_Status();
                //statusAll.StatusId=0;
                //statusAll.Title="All";
                //statuslist.Add(statusAll);

                if (statuslist.Count > 0)
                {
                    return statuslist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public AM_Status AddNew(AM_Status obj)
        {
            throw new NotImplementedException();
        }

        public AM_Status Update(AM_Status obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_Status obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Get Status Count


        public int GetStatusCount()
        {
            try
            {
                int i = this.AMDBMgr.AM_Status.ToList().Count;
                if (i > 0)
                {
                    return i;
                }
                else
                {
                    return -1;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Update Status"

        public void updateStatus(int statusId, AM_Status status, AM_StatusHistory statusHistory)
        {
            try
            {
                AM_Status statusObj = new AM_Status();
                IQueryable<AM_Status> query = (from s in AMDBMgr.AM_Status
                                               where s.StatusId == statusId
                                               select s);
                statusObj = query.ToList().First();
                statusObj.Title = status.Title;
                if (status.IsRentAmount == true)
                {
                    statusObj.RentAmount = status.RentAmount;
                    statusObj.IsRentAmount = status.IsRentAmount;
                    statusObj.IsRentParameter = false;
                }
                if (status.IsRentParameter == true)
                {
                    statusObj.RentParameter = status.RentParameter;
                    statusObj.RentParameterFrequencyLookupCodeId = status.RentParameterFrequencyLookupCodeId;
                    statusObj.IsRentParameter = status.IsRentParameter;
                    statusObj.IsRentAmount = false;
                }
                statusObj.Ranking = status.Ranking;
                statusObj.ReviewPeriod = status.ReviewPeriod;
                statusObj.ReviewPeriodFrequencyLookupCodeId = status.ReviewPeriodFrequencyLookupCodeId;
                
                statusObj.NextStatusAlert = status.NextStatusAlert;                
                
                statusObj.NextStatusAlertFrequencyLookupCodeId = status.NextStatusAlertFrequencyLookupCodeId;
                statusObj.NextStatusDetail = status.NextStatusDetail;
                statusObj.IsNoticeParameterRequired = status.IsNoticeParameterRequired;
                statusObj.IsRecoveryAmount = status.IsRecoveryAmount;
                statusObj.IsNoticeIssueDate = status.IsNoticeIssueDate;
                statusObj.IsNoticeExpiryDate = status.IsNoticeExpiryDate;
                statusObj.IsHearingDate = status.IsHearingDate;
                statusObj.IsWarrantExpiryDate = status.IsWarrantExpiryDate;
                statusObj.IsDocumentUpload = status.IsDocumentUpload;
                statusObj.ModifiedBy = status.ModifiedBy;
                statusObj.CreatedBy = status.CreatedBy;
                
                statusObj.ModifiedDate = System.DateTime.Now;
                statusHistory.CreatedDate = System.DateTime.Now;
                statusHistory.CreatedBy = statusHistory.CreatedBy;
                statusHistory.ModifiedDate = System.DateTime.Now;
                statusHistory.ModifiedBy = statusHistory.ModifiedBy;

                AMDBMgr.SaveChanges();
                statusHistory.StatusId = statusObj.StatusId;
                
                addStatusHistory(statusHistory);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add Status History"

        public void addStatusHistory(AM_StatusHistory statusHistory)
        {
            try
            {
                AMDBMgr.AddToAM_StatusHistory(statusHistory);
                AMDBMgr.SaveChanges();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IStatusBo Members


        public AM_Status GetStatusByRank(int Rank)
        {
            try
            {
                IQueryable<AM_Status> statusquery = from sq in this.AMDBMgr.AM_Status where sq.Ranking == Rank select sq;
                List<AM_Status> statusList = statusquery.ToList();
                if (statusList.Count > 0)
                {
                    return statusList.First();
                }
                else
                {
                    return null;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region IStatusBo Members


        public int GetStatusRankCount()
        {
            try
            {
                List<AM_Status> statuslist = this.AMDBMgr.AM_Status.ToList();
                if (statuslist.Count > 0 && statuslist != null)
                {
                    return statuslist.Count;
                }
                else
                {
                    return -1;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #endregion

        #region"Get Status History Id By Status"

        public int GetStatusHistoryIdByStatus(int statusId)
        {
            var result = (from ds in AMDBMgr.AM_StatusHistory
                           where ds.StatusId == statusId
                            orderby ds.StatusHistoryId
                            select new
                            {
                                ds.StatusHistoryId
                            });
            int id = 0;
            foreach (var t in result)
            {
                id = t.StatusHistoryId; 
            }
            return id;
        }

        #endregion

        #region IStatusBo Members

        /// <summary>
        /// For setting the history ID inside the Creation of Action
        /// </summary>
        /// <param name="StatusId"></param>
        /// <returns></returns>
        public int GetStatusHistoryByStatusId(int StatusId)
        {
            try
            {
                IQueryable<AM_StatusHistory> query = from sh in this.AMDBMgr.AM_StatusHistory where sh.StatusId == StatusId orderby sh.StatusId select sh;
                List<AM_StatusHistory> Slist = query.ToList();
                if (Slist.Count > 0 && Slist != null)
                {
                    return Slist.First().StatusHistoryId;
                }
                else
                {
                    return -1;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Check Document Upload"

        public bool CheckDocumentUpload(int statusId)
        {
            try
            {
                var temp = (from ds in AMDBMgr.AM_Status
                            where ds.StatusId == statusId
                            select ds.IsDocumentUpload);
                bool value = false;

                foreach (var t in temp)
                {
                    value = t.Value;
                }

                return value;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        #endregion

        #region"Get Next Status"

        public AM_Status GetNextStatus(int statusId)
        {
            try
            {
                var rank = (from r in AMDBMgr.AM_Status
                            where r.StatusId == statusId
                            select new
                            {
                                r.Ranking
                            });
                int ranking = 0;
                foreach (var t in rank)
                {
                    ranking = t.Ranking;
                }
                List<AM_Status> ListStatus = (from ds in AMDBMgr.AM_Status
                                              where ds.Ranking == (ranking + 1)
                                              select ds).ToList();
                if (ListStatus != null && ListStatus.Count > 0)
                {
                    return ListStatus[0];
                }
                else
                    return null;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status Ranking"

        public AM_Status GetStatusRanking(int statusId)
        {
            try
            {
                List<AM_Status> listStatus = (from ds in AMDBMgr.AM_Status
                                              where ds.StatusId == statusId
                                              select ds).ToList();
                if (listStatus != null)
                {
                    return listStatus.First();
                }
                else
                {
                    return null;
                }

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Status With Rank"

        public bool UpdateStatusWithRank(int statusid, AM_Status status, AM_StatusHistory statusHistory, int nextStatusId, int rank)
        {
            try
            {
                bool success = false;
                using (TransactionScope trans = new TransactionScope())
                {
                    List<AM_Status> listStatus = (from ds in AMDBMgr.AM_Status
                                                  where ds.StatusId == statusid
                                                  select ds).ToList();
                    AM_Status statusObj = new AM_Status();
                    if (listStatus != null)
                    {
                        if (listStatus.Count > 0)
                        {
                            statusObj = listStatus.First();
                            statusObj = SetStatus(statusObj, status);
                            AMDBMgr.SaveChanges();
                        }
                    }

                    statusHistory.StatusId = statusid;
                    addStatusHistory(statusHistory);

                    UpdateLastStatusAndStatusHistory(nextStatusId, rank);

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                return success;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Set Status"

        public AM_Status SetStatus(AM_Status statusObj, AM_Status status)
        {
            statusObj.CreatedBy = status.CreatedBy;
            statusObj.CreatedDate = status.CreatedDate;
            statusObj.IsDocumentUpload = status.IsDocumentUpload;
            statusObj.IsHearingDate = status.IsHearingDate;
            statusObj.IsNoticeExpiryDate = status.IsNoticeExpiryDate;
            statusObj.IsNoticeIssueDate = status.IsNoticeIssueDate;
            statusObj.IsNoticeParameterRequired = status.IsNoticeParameterRequired;
            statusObj.IsRecoveryAmount = status.IsRecoveryAmount;
            statusObj.IsRentAmount = status.IsRentAmount;
            statusObj.IsRentParameter = status.IsRentParameter;
            statusObj.IsWarrantExpiryDate = status.IsWarrantExpiryDate;
            statusObj.ModifiedBy = status.ModifiedBy;
            statusObj.ModifiedDate = status.ModifiedDate;
            statusObj.NextStatusAlert = status.NextStatusAlert;
            statusObj.NextStatusAlertFrequencyLookupCodeId = status.NextStatusAlertFrequencyLookupCodeId;
            statusObj.NextStatusDetail = status.NextStatusDetail;
            statusObj.Ranking = status.Ranking;
            statusObj.RentAmount = status.RentAmount;
            statusObj.RentParameter = status.RentParameter;
            statusObj.RentParameterFrequencyLookupCodeId = status.RentParameterFrequencyLookupCodeId;
            statusObj.ReviewPeriod = status.ReviewPeriod;
            statusObj.ReviewPeriodFrequencyLookupCodeId = status.ReviewPeriodFrequencyLookupCodeId;
            statusObj.Title = status.Title;

            return statusObj;
        }

        #endregion

        #region"Set Status History"

        public AM_StatusHistory SetStatusHistory(AM_StatusHistory statusHistoryObj, AM_StatusHistory statusHistory)
        {
            statusHistoryObj.CreatedBy = statusHistory.CreatedBy;
            statusHistoryObj.CreatedDate = statusHistory.CreatedDate;
            statusHistoryObj.IsDocumentUpload = statusHistory.IsDocumentUpload;
            statusHistoryObj.IsHearingDate = statusHistory.IsHearingDate;
            statusHistoryObj.IsNoticeExpiryDate = statusHistory.IsNoticeExpiryDate;
            statusHistoryObj.IsNoticeIssueDate = statusHistory.IsNoticeIssueDate;
            statusHistoryObj.IsNoticeParameterRequired = statusHistory.IsNoticeParameterRequired;
            statusHistoryObj.IsRecoveryAmount = statusHistory.IsRecoveryAmount;
            statusHistoryObj.IsRentAmount = statusHistory.IsRentAmount;
            statusHistoryObj.IsRentParameter = statusHistory.IsRentParameter;
            statusHistoryObj.IsWarrantExpiryDate = statusHistory.IsWarrantExpiryDate;
            statusHistoryObj.ModifiedBy = statusHistory.ModifiedBy;
            statusHistoryObj.ModifiedDate = statusHistory.ModifiedDate;
            statusHistoryObj.NextStatusAlert = statusHistory.NextStatusAlert;
            statusHistoryObj.NextStatusAlertFrequencyLookupCodeId = statusHistory.NextStatusAlertFrequencyLookupCodeId;
            statusHistoryObj.NextStatusDetail = statusHistory.NextStatusDetail;
            statusHistoryObj.Ranking = statusHistory.Ranking;
            statusHistoryObj.RentAmount = statusHistory.RentAmount;
            statusHistoryObj.RentParameter = statusHistory.RentParameter;
            statusHistoryObj.RentParameterFrequencyLookupCodeId = statusHistory.RentParameterFrequencyLookupCodeId;
            statusHistoryObj.ReviewPeriod = statusHistory.ReviewPeriod;
            statusHistoryObj.ReviewPeriodFrequencyLookupCodeId = statusHistory.ReviewPeriodFrequencyLookupCodeId;
            statusHistoryObj.Title = statusHistory.Title;
            statusHistoryObj.StatusId = statusHistory.StatusId;

            return statusHistoryObj;
        }

        #endregion

        #region"Is Status Assigned"

        public bool IsStatusAssigned(int statusId)
        {
            try
            {
                List<AM_CaseHistory> listCaseHistory = (from ds in AMDBMgr.AM_CaseHistory
                                                        where ds.StatusId == statusId
                                                        select ds).ToList();
                if (listCaseHistory == null)
                {
                    return true;
                }
                else if (listCaseHistory.Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Last Status And Status History"

        public void UpdateLastStatusAndStatusHistory(int statusId, int updatedRank)
        {
            AM_Status status = GetById(statusId);
            if (status != null)
            {
                status.ModifiedDate = DateTime.Now;
                status.Ranking = updatedRank;
            }
            AMDBMgr.SaveChanges();
            AM_StatusHistory statusHistory = GetlastStatusHistoryByStatusId(status.StatusId);
            if (statusHistory != null)
            {
                statusHistory.Ranking = updatedRank;
                statusHistory.ModifiedDate = DateTime.Now;
            }
            AMDBMgr.SaveChanges();
        }

        #endregion

        #region"Get Last Status History By Status Id"

        public AM_StatusHistory GetlastStatusHistoryByStatusId(int statusId)
        {
            try
            {
                List<AM_StatusHistory> listHistory = (from ds in AMDBMgr.AM_StatusHistory
                                                      where ds.StatusId == statusId
                                                      orderby ds.StatusHistoryId descending
                                                      select ds).ToList();
                if (listHistory != null)
                {
                    if (listHistory.Count > 0)
                    {
                        return listHistory.First();
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
