﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BusinessObject.base_bo;
using System.Data.Entity;
using Am.Ahv.Entities;
using System.Data;
namespace Am.Ahv.BusinessObject.Lookup_bo
{
    public class LookupBo : BaseBO, ILookupBo
    {
        public LookupBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region ILookupBo Members

        public List<AM_LookupCode> GetLookupByName(string lookuptype)
        {
            try
            {
                IQueryable<AM_LookupCode> lookupquery = from lq in this.AMDBMgr.AM_LookupCode where lq.AM_LookupType.TypeName == lookuptype && lq.IsActive == true select lq;
                List<AM_LookupCode> Llist = lookupquery.ToList();
                return Llist;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<AM_LookupCode> GetLookupByName(string lookuptype, int DbIndex, int pageSize)
        {
            try
            {
                IQueryable<AM_LookupCode> lookupquery = (from lq in this.AMDBMgr.AM_LookupCode
                                                         where lq.AM_LookupType.TypeName == lookuptype && lq.IsActive == true
                                                         orderby lq.LookupCodeId
                                                         select lq).Skip(DbIndex * pageSize).Take(pageSize);
                List<AM_LookupCode> Llist = lookupquery.ToList();
                return Llist;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IBaseBO<AM_LookupCode,int> Members

        public AM_LookupCode GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<AM_LookupCode> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_LookupCode AddNew(AM_LookupCode obj)
        {
            throw new NotImplementedException();
        }

        public AM_LookupCode Update(AM_LookupCode obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_LookupCode obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ILookupBo Members

        public int getLookupCount(string typeName)
        {
            try
            {
                int count = (from lq in this.AMDBMgr.AM_LookupCode where lq.AM_LookupType.TypeName == typeName && lq.IsActive == true select lq).Count();
                return count;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Author:Nouman
        /// Revision:1.0
        /// Summary: User Update Method of IBaseBO rather reinventing the wheel and pass object insted of long parameters inside a function for uniformity.
        /// </summary>
        /// <param name="lookupCodeId"></param>
        /// <param name="value"></param>
        /// <param name="modifiedBy"></param>
        public void updateLookup(int lookupCodeId, string value, int modifiedBy)
        {
            try
            {
                AM_LookupCode code = new AM_LookupCode();
                IQueryable<AM_LookupCode> temp = from t in AMDBMgr.AM_LookupCode where t.LookupCodeId == lookupCodeId && t.IsActive == true select t;
                code = temp.ToList()[0];
                code.CodeName = value;
                code.ModifiedDate = System.DateTime.Now;
                code.ModifiedBy = modifiedBy;
                code.IsActive = true;
                AMDBMgr.SaveChanges();
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (IndexOutOfRangeException ie)
            {
                throw ie;
            }
            catch (UpdateException u)
            {
                throw u;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Author:Nouman
        /// Revision:1.0
        /// Summary: User AddNew Method of IBaseBO rather reinventing the wheel and pass object insted of long parameters inside a function for uniformity.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>

        public void addLookup(string value, string type, int createdBy)
        {
            try
            {
                AM_LookupType ltype = new AM_LookupType();
                AM_LookupCode code = new AM_LookupCode();
                IQueryable<AM_LookupType> lookUpType = from t in AMDBMgr.AM_LookupType where t.TypeName == type select t;
                ltype = lookUpType.ToList()[0];
                code.AM_LookupType = ltype;
                code.CodeName = value;
                code.IsActive = true;
                code.CreatedDate = System.DateTime.Now;
                code.CreatedBy = createdBy;
                AMDBMgr.AddToAM_LookupCode(code);
                AMDBMgr.SaveChanges();
            }
            catch (ArgumentOutOfRangeException ie)
            {
                throw ie;
            }
            catch (EntitySqlException ee)
            {
                throw ee;
            }
            catch (EntityException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region"Get Look Up"

        public string getLookup(int lookupcodeid)
        {
            try
            {
                AM_LookupCode code = new AM_LookupCode();
                string temp = (from ds in AMDBMgr.AM_LookupCode where ds.LookupCodeId == lookupcodeid select ds.CodeName).First();
                return temp;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion
        /// <summary>
        /// Author:Nouman
        /// Revision:1.0
        /// Summary: There was no need to write this method with Stored Procedure. We can run the queries on the basis of this thing using Linq. 
        /// Check out the Commented Query. Secondly this Method Should be in IResourceBO & ResourceBo.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserType(int userId)
        {
            try
            {
                //var lookupquery = from r in this.AMDBMgr.AM_Resource where r.ResourceId == userId select r.AM_LookupCode.CodeName;
                var userType = AMDBMgr.AM_SP_GetUserType(userId);
                return userType.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_LookupCode> GetOutComeLookupByName(string lookuptype)
        {
            try
            {
                IQueryable<AM_LookupCode> lookupquery = from lq in this.AMDBMgr.AM_LookupCode where lq.AM_LookupType.TypeName == lookuptype && lq.IsActive == true && lq.ParentId == null select lq;
                List<AM_LookupCode> Llist = lookupquery.ToList();
                return Llist;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool DeleteOutComeLookUp(int OutComeId)
        {
            try
            {
                List<AM_LookupCode> LookUp = (from LookupCode in AMDBMgr.AM_LookupCode where LookupCode.LookupCodeId == OutComeId select LookupCode).ToList<AM_LookupCode>();
                if (LookUp != null && LookUp.Count > 0)
                {
                    AMDBMgr.AM_LookupCode.DeleteObject(LookUp[0]);
                    if (AMDBMgr.SaveChanges() > 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteLookup(int lookupCodeId, int modifiedBy)
        {
            try
            {
                AM_LookupCode code = new AM_LookupCode();
                IQueryable<AM_LookupCode> temp = from t in AMDBMgr.AM_LookupCode where t.LookupCodeId == lookupCodeId && t.IsActive == true select t;
                code = temp.ToList()[0];
                code.ModifiedDate = System.DateTime.Now;
                code.ModifiedBy = modifiedBy;
                code.IsActive = false;
                AMDBMgr.SaveChanges();
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (IndexOutOfRangeException ie)
            {
                throw ie;
            }
            catch (UpdateException u)
            {
                throw u;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
