﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;
using System.Data;
namespace Am.Ahv.BusinessObject.case_bo
{
    class CaseStandardLetterBo:BaseBO,ICaseStandardLetter
    {
        public CaseStandardLetterBo()
        {
            this.AMDBMgr = new Entities.TKXEL_RSLManager_UKEntities();
        }

        #region ICaseStandardLetter Members

        public List<AM_CaseAndStandardLetter> GetCaseStandardLetterByCaseHistoryId(int CaseHistoryId)
        {
            try 
            {
                IQueryable<AM_CaseAndStandardLetter> CaseStandardLetters = from csl in this.AMDBMgr.AM_CaseAndStandardLetter 
                                                                           where csl.CaseHistoryId == CaseHistoryId 
                                                                           select csl;
                List<AM_CaseAndStandardLetter> cslist = CaseStandardLetters.ToList();
                if (cslist != null && cslist.Count > 0)
                {
                    return cslist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region ICaseStandardLetter Members

        public AM_CaseAndStandardLetter GetFirstCaseStandardLetter(int CaseHistoryId, int LetterId)
        {
            try
            {
                IQueryable<AM_CaseAndStandardLetter> CaseStandardLetters = from csl in this.AMDBMgr.AM_CaseAndStandardLetter
                                                                           where csl.CaseHistoryId == CaseHistoryId && csl.StandardLetterId == LetterId
                                                                           select csl;
                List<AM_CaseAndStandardLetter> cslist = CaseStandardLetters.ToList();
                if (cslist != null && cslist.Count > 0)
                {
                    return cslist.First();
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
    }
}
