﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;

namespace Am.Ahv.BusinessObject.case_bo
{
    public class FirstDetectionBO: BaseBO, IFirstDetectionBO
    {

        
        public FirstDetectionBO()
        {
            AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }


        #region"get First Detection List"

        public IQueryable<AM_SP_getFirstDetectionList_Result> GetFirstDetectionList(string postCode,int caseOwnedId,int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId)
        {
            try
            {
                AMDBMgr.CommandTimeout = 900;
                var firstDetection = AMDBMgr.AM_SP_getFirstDetectionList(postCode,caseOwnedId,regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize, sortExpression, sortDirection,address1,tenancyId);
                return firstDetection.AsQueryable<AM_SP_getFirstDetectionList_Result>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Customer Rent List"

        public List<AM_SP_GetCustomerRentList_Result> GetCustomerRentList()
        {
            List<AM_SP_GetCustomerRentList_Result> list2 = new List<AM_SP_GetCustomerRentList_Result>();
            list2 = AMDBMgr.AM_SP_GetCustomerRentList().ToList();
            return list2;
        }

        #endregion

        #region"Get Status Parameters"

        public AM_SP_GetStatusParameters_Result GetStatusParameters()
        {
            List<AM_SP_GetStatusParameters_Result> statusParameters = AMDBMgr.AM_SP_GetStatusParameters().ToList();
            AM_SP_GetStatusParameters_Result statusParam = new AM_SP_GetStatusParameters_Result();
            if (statusParameters != null)
            {
                if (statusParameters.Count > 0)
                {
                    statusParam.CodeName = statusParameters[0].CodeName;
                    statusParam.IsRentParameter = statusParameters[0].IsRentParameter;
                    statusParam.IsRentAmount = statusParameters[0].IsRentAmount;
                    statusParam.RentAmount = statusParameters[0].RentAmount;
                    statusParam.StatusHistoryId = statusParameters[0].StatusHistoryId;
                    statusParam.StatusModifiedDate = statusParameters[0].StatusModifiedDate;
                    statusParam.RentParameter = statusParameters[0].RentParameter;
                }
            }
            //foreach (var item in statusParameters)
            //{
            //    if (item.CodeName != null)
            //    {
            //        statusParam.CodeName = item.CodeName;
            //    }
            //    if (item.IsRentParameter != null)
            //    {
            //        statusParam.IsRentParameter = item.IsRentParameter;
            //    }
            //    if (item.RentAmount != null)
            //    {
            //        statusParam.RentAmount = item.RentAmount;
            //    }
            //    if (item.RentParameter != null)
            //    {
            //        statusParam.RentParameter = item.RentParameter;
            //    }
            //    if (item.StatusHistoryId != null)
            //    {
            //        statusParam.StatusHistoryId = item.StatusHistoryId;
            //    }
            //    if (item.StatusModifiedDate != null)
            //    {
            //        statusParam.StatusModifiedDate = item.StatusModifiedDate;
            //    }
            //    if (item.IsRentAmount != null)
            //    {
            //        statusParam.IsRentAmount = item.IsRentAmount;
            //    }
            //}

            return statusParam;
        }

        #endregion

        #region"Add First Detection"
        /// <summary>
        /// Revision:1.0
        /// Author:Nouman
        /// Summary: You should Pass the object rather long signature to the BO Layer.
        /// </summary>
        /// <param name="_customerId"></param>
        /// <param name="_tenancyId"></param>        
        /// <param name="_date"></param>
        /// <param name="_isDefaulter"></param>
        public void AddFirstDetection(int _customerId, int _tenancyId, DateTime _date, bool _isDefaulter)
        {
            //update by: Umair
            //Update Date: 28 11 2011
            List<AM_FirstDetecionList_History> firstDetectionHistory = (from ds in AMDBMgr.AM_FirstDetecionList_History
                                                                  where ds.IsDefaulter == _isDefaulter &
                                                                        ds.CustomerId == _customerId &
                                                                        ds.TenancyId == _tenancyId
                                                                  select ds).ToList();

            AM_FirstDetecionList firstDetection = new AM_FirstDetecionList();
            firstDetection.CustomerId = _customerId;
            firstDetection.TenancyId = _tenancyId;
            //firstDetection.FirstDetectionDate = _date;
            firstDetection.IsDefaulter = _isDefaulter;
            //firstDetection.CreatedDate = DateTime.Now;

            if (firstDetectionHistory.Count() ==0)
            {
                firstDetection.FirstDetectionDate = _date;
                firstDetection.CreatedDate = DateTime.Now;
            }
            else
            {
                firstDetection.FirstDetectionDate = firstDetectionHistory.First().FirstDetectionDate.HasValue ? firstDetectionHistory.First().FirstDetectionDate : _date;
                firstDetection.CreatedDate = firstDetectionHistory.First().CreatedDate.HasValue ? firstDetectionHistory.First().CreatedDate.Value : DateTime.Now;
            }
            //update end

            AMDBMgr.AddToAM_FirstDetecionList(firstDetection);
            AMDBMgr.SaveChanges();
        }

        #endregion

        #region"Add First Detection History"
        /// <summary>
        /// Revision:1.0
        /// Author:Umair
        /// Summary: This funtion will save the history of the prvious month first detection list
        /// </summary>
        /// <param name="_customerId"></param>
        /// <param name="_tenancyId"></param>        
        /// <param name="_date"></param>
        /// <param name="_isDefaulter"></param>
        public void AddFirstDetectionHistory(bool _isDefaulter)
        {
            
            AM_FirstDetecionList_History firstDetection; 

            List<AM_FirstDetecionList> FirstDetectionList = (from ds in AMDBMgr.AM_FirstDetecionList
                                                             where ds.IsDefaulter == _isDefaulter
                                                             select ds).ToList();
            try
            {
    
                foreach (var ds in FirstDetectionList)
                {
                    firstDetection = new AM_FirstDetecionList_History();

                    firstDetection.CustomerId = ds.CustomerId;
                    firstDetection.TenancyId = ds.TenancyId;
                    firstDetection.FirstDetectionDate = ds.FirstDetectionDate;
                    firstDetection.IsDefaulter = ds.IsDefaulter;
                    firstDetection.CreatedDate = ds.CreatedDate;

                    AMDBMgr.AddToAM_FirstDetecionList_History(firstDetection);
                }

                AMDBMgr.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region "Delete First Detection"
        //Updated by:Umair
        //Update Date:25 08 2011
        public void DeleteFirstDetection(bool _isDefaulter)
        {
            List<AM_FirstDetecionList> list = (from ds in AMDBMgr.AM_FirstDetecionList
                                               where ds.IsDefaulter == _isDefaulter
                                               select ds).ToList();

            foreach (var ds in list)
            {
                AMDBMgr.AM_FirstDetecionList.DeleteObject(ds);
            }

            try
            {
                AMDBMgr.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Update End
        #endregion

        #region "Delete First Detection History"
        //Updated by:Umair
        //Update Date:25 08 2011
        public void DeleteFirstDetectionHistory()
        {
            List<AM_FirstDetecionList_History> list = (from ds in AMDBMgr.AM_FirstDetecionList_History
                                               select ds).ToList();

            foreach (var ds in list)
            {
                
                AMDBMgr.AM_FirstDetecionList_History.DeleteObject(ds);
            }

            try
            {
                AMDBMgr.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Update End
        #endregion

        #region"Get First Detection Records Count"

        public int GetFirstDetectionRecordsCount(string postCode,int caseOwnedBy,int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1, string tenancyId)
        {
            var recordsCount = AMDBMgr.AM_SP_GetFirstDetectionListCount(postCode,caseOwnedBy,regionId, suburbId, allRegionFlag, allSuburbFlag, surname,address1,tenancyId);
            int i = 0;
            foreach (var t in recordsCount)
            {
                i = t.Value;
            }
            return i;
        }

        #endregion

        #region "Count First Detections"
        public int CountFirstDetections(int caseOfficerId, int regionId, int suburbId)
        {

            try
            {
                if (caseOfficerId <= 0 && regionId <= 0 && suburbId <= 0)
                {
                    int total1 = (from p in this.AMDBMgr.AM_Case where p.AM_Status.Title == "First Detection" & p.IsActive == true select p.CaseId).Count();
                    //int total = (from ds in AMDBMgr.AM_Case
                    //             join t in AMDBMgr.AM_Status on ds.StatusId equals t.StatusId
                    //             where t.Title == "First Detection"
                    //             select new
                    //             {
                    //                 ds
                    //             }).Count();
                    return total1;
                }
                else
                {
                    int total = (AMDBMgr.AM_SP_CountFirstDetections(caseOfficerId, regionId, suburbId)).Count();
                    return total;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Rent List"

        public List<AM_SP_GetRentList_Result> GetRentList(int dbIndex, int pageSize)
        {
            this.AMDBMgr.CommandTimeout = 900;
            List<AM_SP_GetRentList_Result> result = AMDBMgr.AM_SP_GetRentList(dbIndex, pageSize).ToList();
            this.AMDBMgr.Connection.Close();
            return result;
        }

        #endregion

        public int GetRentListCount()
        {
            var temp = AMDBMgr.AM_SP_GetRentListCount();

            int count = 0;

            foreach (var t in temp)
            {
                count = t.Value;
            }
            return count;
        }

        public bool CheckDefaulterTenant(int customerId)
        {
            //Update by:Umair
            //Update Date:25 08 2011
            //List<AM_FirstDetecionList> list = (from ds in AMDBMgr.AM_FirstDetecionList
            //                                   where ds.CustomerId == customerId && ds.IsDefaulter == true
            //                                   select ds).ToList();
            List<AM_FirstDetecionList> list = (from ds in AMDBMgr.AM_FirstDetecionList
                                               join cs in AMDBMgr.AM_Case on ds.TenancyId equals cs.TenancyId
                                               where ds.CustomerId == customerId && ds.IsDefaulter == false && cs.IsActive==true
                                               select ds).ToList();
            //bool flag = false;
            bool flag = true;
            //end update
            if (list != null)
            {
                if (list.Count > 0)
                {
                    flag = list[0].IsDefaulter.Value;
                }
            }
            return flag;
        }

        public int GetFirstDetectionListAlterCount(string postCode,int regionId, int suburbId,int resourceId)
        {
            try
            {
                int result = 0;
                var data = AMDBMgr.AM_SP_GetFirstDetectionListCount(postCode,resourceId, regionId, suburbId, true, true, string.Empty,string.Empty,string.Empty);
                foreach (var t in data)
                {
                    if (t != null)
                    {
                        result = t.Value;
                    }
                }
                return result;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region"Get Latest First Detection List Alert Count"

        public int GetLatestFirstDetectionListAlterCount()
        {
            // int result = 0;
            try
            {
                

                int result = (from e in AMDBMgr.AM_FirstDetecionList
                              where (e.IsDefaulter == true)
                              select e).Count();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion        

        #region"Get Latest First Detection List Alert Count"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="suburbId"></param>
        /// <returns></returns>
        public int GetLatestFirstDetectionListAlterCount(int regionId, int suburbId,int resourceId)
        {

            try
            {
                var recordsCount = AMDBMgr.AM_SP_GetPreviousTenantsCount(resourceId, regionId, suburbId, true, true, string.Empty, 0, 0,string.Empty,string.Empty);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Previous Tenants"

        public IQueryable<AM_SP_GetPreviousTenantsList_Result> GetPreviousTenants(int caseOwnedId, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId)
        {
            try
            {
                var firstDetection = AMDBMgr.AM_SP_GetPreviousTenantsList(caseOwnedId, regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize, sortExpression, sortDirection,address1,tenancyId);
                return firstDetection.AsQueryable<AM_SP_GetPreviousTenantsList_Result>();
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region"Get Previous Tenants Records Count"

        public int GetPreviousTenantsRecordsCount(int caseOwnedBy, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1,string tenancyId)
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_GetPreviousTenantsCount(caseOwnedBy, regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize,address1,tenancyId);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion
    }
}