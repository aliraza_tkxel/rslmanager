﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.base_bo;
using System.Data;
using Am.Ahv.Entities;
using System.Collections;
using System.Transactions;
using System.Data.Objects;
using Am.Ahv.BusinessObject.action_bo;
using Am.Ahv.BusinessObject.status_bo;
using Am.Ahv.BusinessObject.letters_bo;
using System.IO;
using Am.Ahv.Utilities.utitility_classes;

namespace Am.Ahv.BusinessObject.case_bo
{
    public class OpenCaseBO : BaseBO, IOpenCaseBO
    {
        public OpenCaseBO()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region "Open Case"

        public int OpenCase(AM_Case newCase)
        {
            try
            {
                AMDBMgr.AddToAM_Case(newCase);
                AMDBMgr.SaveChanges();
                return newCase.CaseId;                
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Case History and Documents"

        public int AddCaseHistoryandDocuments(AM_Case newCase, AM_CaseHistory caseHistory, ArrayList listDocuments, ArrayList listLetters, int tenantId, int modifiedBy, DataTable ActionData, bool isPaymentPlanAdded, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                bool success = false;
                int caseId = 0;
                using (TransactionScope tran = new TransactionScope())
                {
                    caseId = OpenCase(newCase);
                    caseHistory.CaseId = caseId;

                    AM_CaseHistory temp = new AM_CaseHistory();
                    temp.ActionId = caseHistory.ActionId;
                    temp.ActionHistoryId = caseHistory.ActionHistoryId;
                    temp.StatusId = caseHistory.StatusId;
                    temp.StatusHistoryId = caseHistory.StatusHistoryId;
                    temp.IsActionIgnored = caseHistory.IsActionIgnored;
                    temp.IsActionRecorded = caseHistory.IsActionRecorded;
                    temp.IsPaymentPlan = caseHistory.IsPaymentPlan;
                   
                    int caseHistoryId = 0;
                    if (ActionData != null)
                    {
                        ActionBO actionBo = new ActionBO();
                        StatusBo statusBo = new StatusBo();
                        for (int i = 0; i < ActionData.Rows.Count; i++)
                        {
                            //if (i == -1)
                            //{
                            //    caseHistory.CreatedDate = DateTime.Now;
                            //    caseHistory.IsActionRecorded = true;
                            //    caseHistory.IsActionIgnored = false;
                            //    caseHistory.ActionRecordedCount = 1;
                            //    caseHistory.ActionIgnoreCount = 0;
                            //    //if (!isPaymentPlanAdded)
                            //    {
                            //        caseHistory.IsPaymentPlan = false;
                            //    }
                            //    AMDBMgr.AddToAM_CaseHistory(caseHistory);
                            //    AMDBMgr.SaveChanges();
                            //    caseHistoryId = caseHistory.CaseHistoryId;
                            //    AMDBMgr.Detach(caseHistory);
                            //}
                            //else
                            {
                                caseHistory.CreatedDate = DateTime.Now;
                                caseHistory.IsActionIgnored = true;
                                caseHistory.IsActionRecorded = false;
                                caseHistory.ActionIgnoreCount = 1;
                                caseHistory.ActionRecordedCount = 0;
                                caseHistory.ActionIgnoreReason = ActionData.Rows[i]["IgnoreReason"].ToString();
                                caseHistory.ActionId = int.Parse(ActionData.Rows[i]["ActionId"].ToString());
                                caseHistory.StatusId = int.Parse(ActionData.Rows[i]["StatusId"].ToString());
                                caseHistory.ActionHistoryId = actionBo.GetActionHistoryByActionId(caseHistory.ActionId);
                                caseHistory.StatusHistoryId = statusBo.GetStatusHistoryByStatusId(caseHistory.StatusId);
                                caseHistory.IsPaymentPlan = false;
                                caseHistory.IsPaymentPlanIgnored = false;
                                caseHistory.PaymentPlanIgnoreReason = string.Empty;
                                AMDBMgr.AddToAM_CaseHistory(caseHistory);
                                AMDBMgr.SaveChanges();
                                AMDBMgr.Detach(caseHistory);
                            }
                        }
                    }
                  //  else
                    {
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.IsActionRecorded = true;
                        caseHistory.ActionRecordedCount = 1;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.IsPaymentPlan = false;
                        caseHistory.ActionHistoryId = temp.ActionHistoryId;
                        caseHistory.ActionId = temp.ActionId;
                        caseHistory.StatusHistoryId = temp.StatusHistoryId;
                        caseHistory.StatusId = temp.StatusId;                        
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();
                        AMDBMgr.Detach(caseHistory);
                        if (caseHistoryId == 0)
                        {
                            caseHistoryId = caseHistory.CaseHistoryId;
                        }
                    }

                    if (isPaymentPlanAdded)
                    {
                        // only for payment plan.
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.ActionRecordedDate = DateTime.Now;
                        caseHistory.IsActionRecorded = false;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.ActionRecordedCount = 0;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.IsDocumentUpload = false;
                        caseHistory.IsDocumentAttached = false;
                        caseHistory.IsPaymentPlan = true;
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();                     
                    }

                    AM_Documents doc = new AM_Documents();
                    for (int i = 0; i < listDocuments.Count; i++)
                    {
                        FileOperations.UploadFile(listDocuments[i].ToString(), caseHistory.CaseId.Value);
                        if (caseHistoryId != 0)
                        {
                            doc.CaseHistoryId = caseHistoryId;
                        }
                        else
                        {
                            doc.CaseHistoryId = caseHistory.CaseHistoryId;
                        }
                        doc.CaseId = caseHistory.CaseId;
                        doc.DocumentName = FileOperations.ChangeFileName(listDocuments[i].ToString(), caseHistory.CaseId.Value);
                        doc.IsActive = true;
                        AMDBMgr.AddToAM_Documents(doc);
                        AMDBMgr.SaveChanges();
                        AMDBMgr.Detach(doc);
                    }


                    AM_CaseAndStandardLetter caseLetters = new AM_CaseAndStandardLetter();
                    case_bo.CaseBo caseBo = new CaseBo();
                    for (int j = 0; j < listLetters.Count; j++)
                    {
                        string[] str = listLetters[j].ToString().Split(';');
                        if (!caseBo.IsLetter(int.Parse(str[0]), int.Parse(caseHistory.CaseId.ToString())))
                        {
                            if (caseHistoryId != 0)
                            {
                                caseLetters.CaseHistoryId = caseHistoryId;
                            }
                            else
                            {
                                caseLetters.CaseHistoryId = caseHistory.CaseHistoryId;
                            }
                            
                            caseLetters.RentBalance = rentBalance;
                            caseLetters.MarketRent = marketRent;
                            caseLetters.CreateDate = todayDate;
                            caseLetters.RentAmount = rentAmount;


                            caseLetters.CaseId = int.Parse(caseHistory.CaseId.ToString());
                            caseLetters.StandardLetterId = new LettersBo().GetSLetterByHistoryID( int.Parse(str[0])).StandardLetterId;
                            caseLetters.IsActive = true;
                            //if (str[1] != "%")
                            //{
                            //if (str.Length > 2)
                            //{
                            //    caseLetters.StandardLetterHistoryId = int.Parse(str[1]);   
                            //}
                            caseLetters.StandardLetterHistoryId = int.Parse(str[0]); 
                            //}
                            AMDBMgr.AddToAM_CaseAndStandardLetter(caseLetters);
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(caseLetters);
                        }
                    }

                    AM_FirstDetecionList table = new AM_FirstDetecionList();
                    IQueryable<AM_FirstDetecionList> query = (from ds in AMDBMgr.AM_FirstDetecionList
                                                              where ds.TenancyId == tenantId && ds.IsDefaulter == true
                                                              select ds);
                    List<AM_FirstDetecionList> list = query.ToList();
                    if (list.Count > 0)
                    {
                        table = list.First();
                        table.IsDefaulter = false;
                        table.ModifiedBy = modifiedBy;
                        table.ModifiedDate = DateTime.Now;
                        AMDBMgr.SaveChanges();
                    }                   

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                //if (success == false)
                //{
                //    DisableOpenedCase(caseHistory.CaseId.Value);
                //}
                if (success)
                {
                    return caseId;
                }
                else
                {
                    return -1;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {

                throw entitySqlException;
            }
            catch (EntityException entityException)
            {

                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Documents"

        public void AddDocuments(AM_Documents doc)
        {
            try
            {
                AMDBMgr.AddToAM_Documents(doc);
                AMDBMgr.SaveChanges();
                AMDBMgr.Detach(doc);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IBaseBO<AM_Case,int> Members

        public Entities.AM_Case GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Entities.AM_Case> GetAll()
        {
            throw new NotImplementedException();
        }

        public Entities.AM_Case AddNew(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public Entities.AM_Case Update(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Check Case"

        public bool CheckCase(int tenantId)
        {
            try
            {
                var result = (from ds in AMDBMgr.AM_Case
                              where ds.TenancyId == tenantId
                              select new { ds.IsActive });

                bool isExist = false;
                foreach (var t in result)
                {
                    if (t.IsActive == true)
                    {
                        isExist = true;
                        break;
                    }
                    else
                        isExist = false;
                }
                return isExist;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan(int tenantId)
        {
            try
            {
                var result = (from ds in AMDBMgr.AM_PaymentPlan
                              where ds.TennantId == tenantId && (ds.IsActive==null || ds.IsActive ==true) 
                              select new { ds.IsCreated });
                bool isExist = false;
                foreach (var t in result)
                {
                    if (t.IsCreated == true)
                    {
                        isExist = true;
                        break;
                    }
                    else
                        isExist = false;
                }
                return isExist;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        #region"Set Defaulter Bit"
        public bool SetDefaulterBit(int tenantId, int modifiedBy)
        {
            try
            {
                AM_FirstDetecionList table = null;
                IQueryable<AM_FirstDetecionList> query = (from ds in AMDBMgr.AM_FirstDetecionList
                                                          where ds.TenancyId == tenantId && ds.IsDefaulter == true
                                                          select ds);
                List<AM_FirstDetecionList> list = query.ToList();
                if (list.Count > 0)
                {
                    table = list.First();
                    table.IsDefaulter = false;
                    table.ModifiedBy = modifiedBy;
                    table.ModifiedDate = DateTime.Now;
                    AMDBMgr.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }
        #endregion

        #region"Disable Opened Case"

        public void DisableOpenedCase(int caseId)
        {
            List<AM_Case> list = (from ds in AMDBMgr.AM_Case
                                  where ds.CaseId == caseId && ds.IsActive == true
                                  select ds).ToList();
            AM_Case caseObj = new AM_Case();
            if (list != null && list.Count > 0)
            {
                caseObj = list.First();
                caseObj.IsActive = false;
                AMDBMgr.SaveChanges();
            }
        }

        #endregion

        #region"Save Payment Plan And Open New Case"

        public int SavePaymentPlanAndOpenNewCase(AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments, AM_Case amCase, AM_CaseHistory caseHistory,
                                                    ArrayList listDocuments, ArrayList listLetters, int tenantId, int modifiedBy, DataTable ActionData)
        {
            try
            {
                bool success = false;
                int caseId = 0;
                using (TransactionScope tran = new TransactionScope())
                {
                    this.AMDBMgr.AddToAM_PaymentPlan(paymentPlan);
                    this.AMDBMgr.SaveChanges();
                    paymentPlanHistory.PaymentPlanId = paymentPlan.PaymentPlanId;

                    this.AMDBMgr.AddToAM_PaymentPlanHistory(paymentPlanHistory);
                    this.AMDBMgr.SaveChanges();

                    AM_Payment payment = new AM_Payment();
                    for (int i = 0; i < listPayments.Count; i++)
                    {
                        payment.Date = listPayments[i].Date;
                        payment.Payment = listPayments[i].Payment;
                        payment.IsFlexible = listPayments[i].IsFlexible;
                        payment.PaymentPlanId = paymentPlan.PaymentPlanId;
                        payment.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                        this.AMDBMgr.AddToAM_Payment(payment);
                        AMDBMgr.SaveChanges();
                        AMDBMgr.Detach(payment);
                    }

                    amCase.IsPaymentPlan = true;
                    caseHistory.IsPaymentPlan = true;

                    amCase.IsPaymentPlanUpdated = false;
                    caseHistory.IsPaymentPlanUpdated = false;

                    amCase.PaymentPlanId = paymentPlan.PaymentPlanId;
                    caseHistory.PaymentPlanId = paymentPlan.PaymentPlanId;

                    amCase.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                    caseHistory.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;

                    amCase.IsPaymentPlanIgnored = false;
                    caseHistory.IsPaymentPlanIgnored = false;

                    #region"Commented Because of the New Update (Didn't Deleted the Code for future use"
                    //caseId = AddCaseHistoryandDocuments(amCase, caseHistory, listDocuments, listLetters, tenantId, modifiedBy, ActionData, true);
                    #endregion

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                if (success)
                {
                    return caseId;
                }
                else
                {
                    return -1;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion        

    }
}
