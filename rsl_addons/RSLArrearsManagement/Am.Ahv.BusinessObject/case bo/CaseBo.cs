using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using System.Data;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.Utilities.utitility_classes;
using System.Transactions;
using System.Data.Objects;
using System.Collections;
using Am.Ahv.BusinessObject.action_bo;
using Am.Ahv.BusinessObject.status_bo;

namespace Am.Ahv.BusinessObject.case_bo
{
    class CaseBo : BaseBO, ICaseBo
    {
        public CaseBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }
        //int i=0;

        #region IBaseBO<AM_Case,int> Members

        public AM_Case GetById(int id)
        {
            try
            {
                IQueryable<AM_Case> Casequery = from cq in this.AMDBMgr.AM_Case where cq.CaseId == id select cq;
                List<AM_Case> Clist = Casequery.ToList();
                if (Clist.Count > 0)
                {
                    return LoadReference(Clist.First());
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_Case> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_Case AddNew(AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public AM_Case Update(AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Lazy Loading
        private List<AM_Case> LoadReferences(List<AM_Case> Caslist)
        {
            foreach (AM_Case Item in Caslist)
            {
                Item.AM_Resource3Reference.Load();
                Item.AM_Resource2Reference.Load();
                Item.AM_Resource1Reference.Load();
                Item.AM_LookupCodeReference.Load();
                Item.AM_ActionReference.Load();

                Item.AM_StatusReference.Load();

                Item.AM_StatusReference.Load();

                Item.AM_Documents.Load();
            }
            return Caslist;
        }
        private AM_Case LoadReference(AM_Case Item)
        {
            try
            {
                Item.AM_Resource3Reference.Load();
                Item.AM_Resource2Reference.Load();
                Item.AM_Resource1Reference.Load();
                Item.AM_LookupCodeReference.Load();
                Item.AM_ActionReference.Load();
                Item.AM_StatusReference.Load();
                Item.AM_Documents.Load();
                return Item;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Case By Case History Id"

        public AM_Case GetCaseByCaseHistoryId(int CaseHistoryId)
        {
            var query = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseHistoryId == CaseHistoryId select ch;
            List<AM_CaseHistory> ChList = query.ToList();
            if (ChList.Count > 0)
            {
                return SetCase(ChList.First());
            }
            else
            {
                return null;
            }
        }


        #endregion

        #region"Set Case"

        private AM_Case SetCase(AM_CaseHistory CH)
        {
            try
            {
                AM_Case casedetails = new AM_Case();
                casedetails.CaseId = (int)CH.CaseId;
                ///Setting the resources
                //casedetails.AM_ResourceReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Resource", "ResourceId", CH.InitiatedById);
                //casedetails.AM_Resource2Reference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Resource", "ResourceId", CH.CaseManager);
                //casedetails.AM_Resource3Reference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Resource", "ResourceId", CH.CaseOfficer);
                //casedetails.AM_Resource1Reference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Resource", "ResourceId", CH.ModifiedBy);
                ///Setting the Action

                //casedetails.AM_ActionReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Action", "ActionId", CH.ActionId);
                casedetails.ActionId = CH.ActionId;
                casedetails.InitiatedBy = CH.InitiatedById;
                casedetails.CaseManager = CH.CaseManager;
                casedetails.CaseOfficer = CH.CaseOfficer;
                casedetails.ModifiedBy = CH.ModifiedBy;
                ///Setting the Status
                //casedetails.AM_StatusReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Status", "StatusId", CH.StatusId);
                casedetails.StatusId = CH.StatusId;
                //setting the dates
                casedetails.CreatedDate = CH.CreatedDate;
                casedetails.ModifiedDate = CH.ModifiedDate;
                casedetails.NoticeExpiryDate = CH.NoticeExpiryDate;
                casedetails.NoticeIssueDate = CH.NoticeIssueDate;
                casedetails.ActionReviewDate = CH.ActionReviewDate;
                casedetails.StatusReview = CH.StatusReview;
                /// setting other required parameters
                casedetails.RecoveryAmount = CH.RecoveryAmount;
                casedetails.Notes = CH.Notes;
                casedetails.Reason = CH.Notes;
                casedetails.TenancyId = CH.TennantId;
                casedetails.OutcomeLookupCodeId = CH.OutcomeLookupCodeId;
                casedetails.IsSuppressed = CH.IsSuppressed;
                casedetails.IsActive = false;
                this.AMDBMgr.AddToAM_Case(casedetails);
                this.AMDBMgr.SaveChanges();
                return LoadReference(casedetails);
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Ignore Case"

        public bool IgnoreCase(int CaseId, string Reason, int ignoredActionStatusId, int IgnoredActionId, bool isModalPopUp)
        {
            try
            {
                ActionBO actionBo = new ActionBO();
                StatusBo statusBo = new StatusBo();
                IQueryable<AM_Case> casequery = from c in this.AMDBMgr.AM_Case where c.CaseId == CaseId select c;
                List<AM_Case> caselist = casequery.ToList();
                AM_Case UpdatedCase = null;
                if (caselist.Count > 0)
                {
                    if (!isModalPopUp)
                    {
                        UpdatedCase = caselist.First();
                        UpdatedCase.ActionIgnoreReason = Reason;
                        UpdatedCase.IsActionIgnored = true;
                        UpdatedCase.ActionId = IgnoredActionId;
                        UpdatedCase.ActionHistoryId = actionBo.GetActionHistoryByActionId(IgnoredActionId);
                        UpdatedCase.StatusId = ignoredActionStatusId;
                        UpdatedCase.StatusHistoryId = statusBo.GetStatusHistoryIdByStatus(ignoredActionStatusId);
                        this.AMDBMgr.SaveChanges();
                    }
                    List<AM_CaseHistory> list = (from ds in AMDBMgr.AM_CaseHistory
                                                 where ds.CaseId == CaseId && ds.IsActive == true
                                                 orderby ds.CaseHistoryId descending
                                                 select ds).ToList();
                    if (list != null)
                    {
                        if (list.Count > 0)
                        {
                            AM_CaseHistory caseHistory = new AM_CaseHistory();
                            caseHistory.ActionHistoryId = actionBo.GetActionHistoryByActionId(IgnoredActionId);
                            caseHistory.ActionId = IgnoredActionId;
                            caseHistory.ActionIgnoreCount = 1;
                            caseHistory.ActionIgnoreReason = Reason;
                            caseHistory.ActionRecordedCount = 0;
                            caseHistory.ActionRecordedDate = list[0].ActionRecordedDate;
                            caseHistory.ActionReviewDate = list[0].ActionReviewDate;
                            caseHistory.CaseHistoryId = list[0].CaseHistoryId;
                            caseHistory.CaseId = list[0].CaseId;
                            caseHistory.CaseManager = list[0].CaseManager;
                            caseHistory.CaseOfficer = list[0].CaseOfficer;
                            caseHistory.CreatedDate = list[0].CreatedDate;
                            caseHistory.InitiatedById = list[0].InitiatedById;
                            caseHistory.IsActionIgnored = true;
                            caseHistory.IsActionPostponed = list[0].IsActionPostponed;
                            caseHistory.IsActionRecorded = false;
                            caseHistory.IsActive = list[0].IsActive;
                            caseHistory.IsDocumentAttached = false;
                            caseHistory.IsDocumentUpload = false;
                            caseHistory.IsPaymentPlan = false;
                            caseHistory.IsPaymentPlanIgnored = false;
                            caseHistory.IsSuppressed = list[0].IsSuppressed;
                            caseHistory.ModifiedBy = list[0].ModifiedBy;
                            caseHistory.ModifiedDate = list[0].ModifiedDate;
                            caseHistory.Notes = list[0].Notes;
                            caseHistory.NoticeExpiryDate = list[0].NoticeExpiryDate;
                            caseHistory.NoticeIssueDate = list[0].NoticeIssueDate;
                            caseHistory.OutcomeLookupCodeId = list[0].OutcomeLookupCodeId;
                            caseHistory.PaymentPlanHistoryId = list[0].PaymentPlanHistoryId;
                            caseHistory.PaymentPlanId = list[0].PaymentPlanId;
                            caseHistory.PaymentPlanIgnoreReason = list[0].PaymentPlanIgnoreReason;
                            caseHistory.Reason = list[0].Reason;
                            caseHistory.RecordedActionDetails = list[0].RecordedActionDetails;
                            caseHistory.RecoveryAmount = list[0].RecoveryAmount;
                            caseHistory.StatusHistoryId = statusBo.GetStatusHistoryIdByStatus(ignoredActionStatusId);
                            caseHistory.StatusId = ignoredActionStatusId;
                            caseHistory.StatusReview = list[0].StatusReview;
                            caseHistory.SuppressedBy = list[0].SuppressedBy;
                            caseHistory.SuppressedDate = list[0].SuppressedDate;
                            caseHistory.SuppressedReason = list[0].SuppressedReason;
                            caseHistory.TennantId = list[0].TennantId;

                            AMDBMgr.AddToAM_CaseHistory(caseHistory);
                            AMDBMgr.SaveChanges();
                        }
                    }


                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Supress Case"

        public bool SupressCase(int CaseId, string Reason, DateTime supressdate, int supressedBy)
        {
            try
            {
                IQueryable<AM_Case> casequery = from c in this.AMDBMgr.AM_Case where c.CaseId == CaseId select c;
                List<AM_Case> caselist = casequery.ToList();
                AM_Case UpdatedCase = null;
                if (caselist.Count > 0)
                {
                    UpdatedCase = caselist.First();
                    UpdatedCase.SuppressedReason = Reason;
                    UpdatedCase.IsSuppressed = true;
                    UpdatedCase.SuppressedDate = supressdate;
                    UpdatedCase.SuppressedBy = supressedBy;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Count Mycases"

        public int CountMyCases(string postCode, int caseOfficerId, int regionId, int suburbid)
        {

            try
            {
                //this need to be discussed.
                var total = AMDBMgr.AM_SP_GetMyCasesCount(postCode, caseOfficerId, regionId, suburbid).Count();
                if (total != null && total > 0)
                {
                    return total;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Overdue Case Count"

        public int OverdueCaseCount(int ResourceId)
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_OverdueStagesAlertCount(ResourceId);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i;

                //if (ResourceId != -1)
                //{
                //    IQueryable<AM_Case> statusquery = from overdue in this.AMDBMgr.AM_Case where overdue.AM_Resource3.ResourceId == ResourceId && overdue.IsActive == true select overdue;
                //    List<AM_Case> Caselist = statusquery.ToList();

                //    if (Caselist.Count > 0 && Caselist != null)
                //    {
                //        int count = 0;
                //        DateTime datecal = new DateTime();
                //        DateTime dateaction = new DateTime();
                //        int i;
                //        int j;
                //        foreach (AM_Case item in Caselist)
                //        {

                //            datecal = DateOperations.AddDate(item.AM_Status.NextStatusAlert, item.AM_Status.AM_LookupCode1.CodeName, item.StatusReview);
                //            i = datecal.CompareTo(DateTime.Now);
                //            if (i < 0)
                //            {
                //                count++;
                //            }
                //            //dateaction = DateOperations.AddDate(item.AM_Action.NextActionAlert, item.AM_Action.AM_LookupCode.CodeName, item.ActionReviewDate);
                //            //j = dateaction.CompareTo(DateTime.Now);
                //            //if (j < 0)
                //            //{
                //            //    count++;
                //            //}
                //        }
                //        return count;
                //    }
                //    else
                //    {
                //        return 0;
                //    }
                //}
                //else
                //{
                //    IQueryable<AM_Case> statusquery = from overdue in this.AMDBMgr.AM_Case where overdue.IsActive == true select overdue;
                //    List<AM_Case> Caselist = statusquery.ToList();
                //    if (Caselist.Count > 0 && Caselist != null)
                //    {
                //        int count = 0;
                //        DateTime datecal = new DateTime();
                //        DateTime dateaction = new DateTime();
                //        int i;
                //        int j;
                //        foreach (AM_Case item in Caselist)
                //        {
                //            datecal = DateOperations.AddDate(item.AM_Status.NextStatusAlert, item.AM_Status.AM_LookupCode1.CodeName, item.StatusReview);
                //            i = datecal.CompareTo(DateTime.Now);
                //            if (i < 0)
                //            {
                //                count++;
                //            }
                //            //dateaction = DateOperations.AddDate(item.AM_Action.NextActionAlert, item.AM_Action.AM_LookupCode.CodeName, item.ActionReviewDate);
                //            //j = dateaction.CompareTo(DateTime.Now);
                //            //if (j < 0)
                //            //{
                //            //    count++;
                //            //}
                //        }
                //        return count;
            }


            catch (EntityException entityexception)
            {
                throw entityexception;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Vcat Case Count"

        public int VcatCaseCount(int ResourceId)
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_NISPdueexpireAlertCount(ResourceId);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i;
                //int count = 0;
                //if (ResourceId != -1)
                //{
                //    List<AM_Case> listCase = (from amCase in AMDBMgr.AM_Case
                //                              where amCase.AM_Resource3.ResourceId == ResourceId && amCase.NoticeExpiryDate != null && amCase.IsActive == true
                //                              select amCase).ToList();
                //    if (listCase != null)
                //    {
                //        if (listCase.Count > 0)
                //        {
                //            for(int i = 0; i < listCase.Count; i++)
                //            {
                //                DateTime calculatedDate = DateOperations.AddDate(4, "Weeks", DateTime.Now);
                //                int days = DateOperations.SubtractTwoDates(listCase[i].NoticeExpiryDate.Value, calculatedDate);
                //                if (days <= 28)
                //                {
                //                    count++;
                //                }
                //            }
                //        }
                //    }
                //    return count;                    
                //}
                //else
                //{
                //    List<AM_Case> listCase = (from amCase in AMDBMgr.AM_Case
                //                              where amCase.NoticeExpiryDate != null && amCase.IsActive == true
                //                              select amCase).ToList();
                //    if (listCase != null)
                //    {
                //        if (listCase.Count > 0)
                //        {
                //            for (int i = 0; i < listCase.Count; i++)
                //            {
                //                DateTime calculatedDate = DateOperations.AddDate(4, "Weeks", DateTime.Now);
                //                int days = Math.Abs(DateOperations.SubtractTwoDates(listCase[i].NoticeExpiryDate.Value, calculatedDate));
                //                if (days <= 28)
                //                {
                //                    count++;
                //                }
                //            }
                //        }
                //    }
                //    return count;
                //}
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Review List"

        public List<AM_SP_GetReviewList_Result> GetReviewList(int caseOfficer, int statusId, bool overdue, int dbIndex, int pageSize, string sortExpression, string sortDirection)
        {
            List<AM_SP_GetReviewList_Result> query = this.AMDBMgr.AM_SP_GetReviewList(caseOfficer, statusId, overdue, dbIndex, pageSize, sortExpression, sortDirection).ToList();

            //List<AM_SP_GetReviewList_Result> listitem = new List<AM_SP_GetReviewList_Result>();
            //AM_SP_GetReviewList_Result result = new AM_SP_GetReviewList_Result();
            //if (query != null)
            //{
            //    if (query.Count > 0)
            //    {
            //        int dtenancy = 0;
            //        for (int i = 0; i < query.Count(); i++)
            //        {
            //            if (query[i].JointTenancyCount > 1)
            //            {
            //                i += (query[i].JointTenancyCount.Value - 1);
            //            }
            //            result.Caseid = query[i].Caseid;
            //            result.ActionRecordedDate = query[i].ActionRecordedDate;
            //            result.ActionReviewDate = query[i].ActionReviewDate;
            //            result.CaseHistoryid = query[i].CaseHistoryid;
            //            //result.Caseid = query[i].Caseid;
            //            result.CUSTOMERID = query[i].CUSTOMERID;
            //            result.CustomerName = query[i].CustomerName;
            //            result.CustomerName2 = query[i].CustomerName2;
            //            result.IsNextStatus = query[i].IsNextStatus;
            //            result.IsOverdue = query[i].IsOverdue;
            //            result.JointTenancyCount = query[i].JointTenancyCount;
            //            result.NextAction = query[i].NextAction;
            //            result.nextActionAlert = query[i].nextActionAlert;
            //            result.Owned = query[i].Owned;
            //            result.Status = query[i].Status;
            //            result.StatusRecordedDate = query[i].StatusRecordedDate;
            //            result.TenantId = query[i].TenantId;
            //            listitem.Add(result);
            //            dtenancy += 1;
            //        }
            //    }
            //}
            return query;

        }

        #endregion

        #region"Get Review List Count"

        public int GetReviewListCount(int caseOfficer, int statusId, bool overdue)
        {
            var count = this.AMDBMgr.AM_SP_GetReviewListCount(caseOfficer, statusId, overdue);
            int i = 0;
            foreach (var t in count)
            {
                i = t.Value;
            }
            return i;
        }

        #endregion

        #region"Tenancies Count"

        public int TenanciesCount()
        {
            try
            {
                var query = (from t in this.AMDBMgr.AM_Case where t.IsActive == true select t.TenancyId).Count();
                int firstdetectionCount = (from ds in AMDBMgr.AM_FirstDetecionList where ds.IsDefaulter == true select ds.TenancyId).Count();
                int count = query;
                return count + firstdetectionCount;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Get Arrears Count"

        public decimal GetArrearsCount()
        {
            try
            {

                //  var data = this.AMDBMgr.AM_SP_ArrearsSum();
                var data = (from ds in AMDBMgr.AM_Case
                            where ds.IsActive == true
                            select ds.RentBalance).Sum();
                decimal value = 0;
                if (data != null)
                {
                    value = Convert.ToDecimal(data.Value);
                }

                return value;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Open Case Count"

        public int GetOpenCaseCount()
        {
            try
            {
                var query = (from c in this.AMDBMgr.AM_Case where c.IsActive == true select c.CaseId).Count();
                int count = query;
                return count;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Action Count"

        public bool UpdateActionCount(int CaseId)
        {
            try
            {
                IQueryable<AM_Case> casequery = from c in this.AMDBMgr.AM_Case where c.CaseId == CaseId select c;
                List<AM_Case> caselist = casequery.ToList();
                AM_Case UpdatedCase = null;
                if (caselist.Count > 0)
                {
                    UpdatedCase = caselist.First();
                    UpdatedCase.ActionRecordedCount = +1;
                    UpdatedCase.IsActionRecorded = true;
                    UpdatedCase.ActionRecordedDate = DateTime.Now;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Ignore Action Count"

        public bool UpdateIgnoreActionCount(int CaseId)
        {
            try
            {
                IQueryable<AM_Case> casequery = from c in this.AMDBMgr.AM_Case where c.CaseId == CaseId select c;
                List<AM_Case> caselist = casequery.ToList();
                AM_Case UpdatedCase = null;
                if (caselist.Count > 0)
                {
                    UpdatedCase = caselist.First();
                    UpdatedCase.ActionIgnoreCount = +1;
                    UpdatedCase.IsActionIgnored = true;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Case Documents"

        public IQueryable GetCaseDocuments(int caseId)
        {
            try
            {
                var docs = from ds in AMDBMgr.AM_Documents
                           where ds.CaseId == caseId && ds.IsActive == true
                           select new
                           {
                               ds.DocumentName,
                               ds.DocumentId
                           };
                return docs;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Initiated By Name"

        public string GetInitiatedByName(int employeeId)
        {
            try
            {
                var result = (from ds in AMDBMgr.E__EMPLOYEE
                              where ds.EMPLOYEEID == employeeId
                              select new
                              {
                                  EmployeeName = ((ds.FIRSTNAME ?? "") + " " + (ds.LASTNAME ?? ""))
                              });

                string name = string.Empty;
                foreach (var t in result)
                {
                    name = t.EmployeeName;
                }
                return name;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Disable Document"

        public void DisableDocument(int docId)
        {
            try
            {
                AM_Documents doc = new AM_Documents();
                var result = from ds in AMDBMgr.AM_Documents
                             where ds.DocumentId == docId
                             select ds;
                List<AM_Documents> list = result.ToList();
                doc = list.First();
                doc.IsActive = false;
                AMDBMgr.SaveChanges();

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Case"

        public bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            bool success = false;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AM_Case caseObj = new AM_Case();
                    List<AM_Case> list = (from ds in AMDBMgr.AM_Case
                                          where ds.CaseId == caseId
                                          select ds).ToList<AM_Case>();
                    caseObj = list.First();

                    caseObj.CaseOfficer = amCase.CaseOfficer;
                    caseObj.CaseManager = amCase.CaseManager;
                    caseObj.StatusId = amCase.StatusId;
                    caseObj.StatusHistoryId = amCase.StatusHistoryId;
                    caseObj.StatusReview = amCase.StatusReview;
                    caseObj.ActionId = amCase.ActionId;
                    caseObj.ActionHistoryId = amCase.ActionHistoryId;
                    caseObj.ActionReviewDate = amCase.ActionReviewDate;
                    caseObj.RecoveryAmount = amCase.RecoveryAmount;
                    caseObj.NoticeIssueDate = amCase.NoticeIssueDate;
                    caseObj.NoticeExpiryDate = amCase.NoticeExpiryDate;
                    caseObj.WarrantExpiryDate = amCase.WarrantExpiryDate;
                    caseObj.HearingDate = amCase.HearingDate;
                    caseObj.Notes = amCase.Notes;
                    caseObj.OutcomeLookupCodeId = amCase.OutcomeLookupCodeId;
                    caseObj.Reason = amCase.Reason;
                    caseObj.ActionRecordedDate = amCase.ActionRecordedDate;
                    caseObj.ModifiedBy = amCase.ModifiedBy;
                    caseObj.ModifiedDate = amCase.ModifiedDate;
                    caseObj.IsDocumentAttached = amCase.IsDocumentAttached;
                    caseObj.IsDocumentUpload = amCase.IsDocumentUpload;
                    caseObj.IsActive = amCase.IsActive;
                    caseObj.IsCaseUpdated = true;
                    caseObj.StatusRecordedDate = amCase.StatusRecordedDate;
                    caseObj.IsSuppressed = amCase.IsSuppressed;

                    if (amCase.IsPaymentPlanIgnored != null)
                    {
                        caseObj.IsPaymentPlanIgnored = amCase.IsPaymentPlanIgnored;
                    }
                    if (amCase.PaymentPlanIgnoreReason != null)
                    {
                        caseObj.PaymentPlanIgnoreReason = amCase.PaymentPlanIgnoreReason;
                    }
                    if (amCase.IsPaymentPlan != null)
                    {
                        caseObj.IsPaymentPlan = amCase.IsPaymentPlan;
                        if (caseObj.IsActive == false)
                        {
                            caseObj.IsPaymentPlan = false;
                        }
                       
                    }
                    if (amCase.PaymentPlanId != null)
                    {
                        caseObj.PaymentPlanId = amCase.PaymentPlanId;
                    }
                    if (amCase.PaymentPlanHistoryId != null)
                    {
                        caseObj.PaymentPlanHistoryId = amCase.PaymentPlanHistoryId;
                    }
                    caseObj.RentBalance = amCase.RentBalance;
                    AMDBMgr.SaveChanges();

                    if (amCase.IsActive == false)
                    {
                        List<AM_PaymentPlan> listPayment = (from ds in AMDBMgr.AM_PaymentPlan
                                                            where ds.TennantId == amCase.TenancyId && ds.IsCreated == true
                                                            orderby ds.PaymentPlanId descending
                                                            select ds).ToList();
                        if (listPayment != null && listPayment.Count > 0)
                        {
                            AM_PaymentPlan paymentPlan = listPayment.First();

                            paymentPlan.IsCreated = false;
                            paymentPlan.ModifiedBy = amCase.ModifiedBy.Value;
                            paymentPlan.ModifiedDate = amCase.ModifiedDate;
                            paymentPlan.IsActive = false; 
                            AMDBMgr.SaveChanges();

                            List<AM_PaymentPlanHistory> listHistory = (from history in AMDBMgr.AM_PaymentPlanHistory
                                                                       where history.PaymentPlanId == paymentPlan.PaymentPlanId && history.IsCreated == true
                                                                       orderby history.PaymentPlanHistoryId descending
                                                                       select history).ToList();
                            AM_PaymentPlanHistory paymentPlanHistory = listHistory.First();

                            paymentPlanHistory.IsCreated = false;
                            paymentPlanHistory.ModifiedBy = amCase.ModifiedBy;
                            paymentPlanHistory.ModifiedDate = amCase.ModifiedDate;
                            paymentPlanHistory.IsActive = false;
                            AMDBMgr.SaveChanges();
                        }

                       
                    }

                    int caseHistoryId = 0;
                    if (actionData != null)
                    {
                        ActionBO actionBo = new ActionBO();
                        StatusBo statusBo = new StatusBo();

                        AM_CaseHistory temp = new AM_CaseHistory();
                        temp.ActionId = caseHistory.ActionId;
                        temp.ActionHistoryId = caseHistory.ActionHistoryId;
                        temp.StatusId = caseHistory.StatusId;
                        temp.StatusHistoryId = caseHistory.StatusHistoryId;
                        temp.IsActionRecorded = caseHistory.IsActionRecorded;
                        temp.IsDocumentAttached = caseHistory.IsDocumentAttached;
                        temp.IsDocumentUpload = caseHistory.IsDocumentUpload;

                        for (int i = 0; actionData.Rows.Count > i; i++)
                        {
                            caseHistory.CaseId = caseId;
                            caseHistory.CreatedDate = DateTime.Now;
                            caseHistory.IsActionIgnored = true;
                            caseHistory.IsActionRecorded = false;
                            caseHistory.ActionRecordedCount = 0;
                            caseHistory.ActionIgnoreCount = 1;
                            caseHistory.ActionIgnoreReason = actionData.Rows[i]["IgnoreReason"].ToString();
                            caseHistory.ActionId = int.Parse(actionData.Rows[i]["ActionId"].ToString());
                            caseHistory.StatusId = int.Parse(actionData.Rows[i]["StatusId"].ToString());
                            caseHistory.ActionHistoryId = actionBo.GetActionHistoryByActionId(caseHistory.ActionId);
                            caseHistory.StatusHistoryId = statusBo.GetStatusHistoryByStatusId(caseHistory.StatusId);
                            caseHistory.IsPaymentPlan = false;
                            caseHistory.IsPaymentPlanIgnored = false;
                            caseHistory.PaymentPlanIgnoreReason = string.Empty;
                            AMDBMgr.AddToAM_CaseHistory(caseHistory);
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(caseHistory);
                        }
                        caseHistory.CaseId = caseId;
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.IsActionRecorded = true;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.ActionRecordedCount = 1;
                        caseHistory.IsPaymentPlan = false;
                        caseHistory.ActionId = temp.ActionId;
                        caseHistory.ActionHistoryId = temp.ActionHistoryId;
                        caseHistory.StatusId = temp.StatusId;
                        caseHistory.StatusHistoryId = temp.StatusHistoryId;
                        caseHistory.IsDocumentUpload = temp.IsDocumentUpload;
                        caseHistory.IsDocumentAttached = temp.IsDocumentAttached;
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();
                        caseHistoryId = caseHistory.CaseHistoryId;
                        AMDBMgr.Detach(caseHistory);
                    }
                    else
                    {
                        caseHistory.CaseId = caseId;
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.IsActionRecorded = true;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.ActionRecordedCount = 1;
                        caseHistory.IsPaymentPlan = false;
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();
                        caseHistoryId = caseHistory.CaseHistoryId;
                    }

                    AM_CaseAndStandardLetter letter = new AM_CaseAndStandardLetter();
                    for (int i = 0; i < st.Count; i++)
                    {
                        // if (!IsLetter(st[i].StandardLetterId, caseId))
                        {
                            letter.CaseId = caseId;
                            if (caseHistoryId != 0)
                            {
                                letter.CaseHistoryId = caseHistoryId;
                            }
                            else
                            {
                                letter.CaseHistoryId = caseHistory.CaseHistoryId;
                            }
                            if (st[i].StatusId != -1)
                            {
                                // standard letter history id has been saved in the free field of the object to save the computation.
                                letter.StandardLetterHistoryId = st[i].StatusId;
                            }
                            letter.StandardLetterId = st[i].StandardLetterId;
                            letter.IsActive = true;
                            letter.MarketRent = marketRent;
                            letter.RentBalance = rentBalance;
                            letter.CreateDate = todayDate;
                            letter.RentAmount = rentAmount;
                            AMDBMgr.AddToAM_CaseAndStandardLetter(letter);
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(letter);
                        }
                    }

                    if (oldSL != null)
                    {
                        for (int i = 0; i < oldSL.Count; i++)
                        {
                            int id = oldSL[i].StandardLetterId;
                            List<AM_CaseAndStandardLetter> listCaseSL = (from ds in AMDBMgr.AM_CaseAndStandardLetter
                                                                         where ds.StandardLetterHistoryId == id
                                                                         select ds).ToList();
                            if (listCaseSL != null)
                            {
                                AM_CaseAndStandardLetter letterSL = new AM_CaseAndStandardLetter();
                                // standard letter history id has been saved in the free field of the object to save the computation.
                                //letter.StandardLetterHistoryId = oldSL[i].StatusId; //old code

                                letterSL.CaseId = caseId;
                                if (caseHistoryId != 0)
                                {
                                    letterSL.CaseHistoryId = caseHistoryId;
                                }
                                else
                                {
                                    letterSL.CaseHistoryId = caseHistory.CaseHistoryId;
                                }
                                if (oldSL[i].StatusId != -1)
                                {
                                    // standard letter history id has been saved in the free field of the object to save the computation.
                                    letterSL.StandardLetterHistoryId = oldSL[i].StatusId;
                                }
                                letterSL.StandardLetterId = new letters_bo.LettersBo().GetSLetterByHistoryID(oldSL[i].StandardLetterId).StandardLetterId;

                                letterSL.MarketRent = marketRent;
                                letterSL.RentBalance = rentBalance;
                                letterSL.CreateDate = todayDate;
                                letterSL.RentAmount = rentAmount;
                                //letterSL.IsActive = oldSL[i].IsActive;
                                letterSL.IsActive = true;

                                AMDBMgr.AddToAM_CaseAndStandardLetter(letterSL);
                                AMDBMgr.SaveChanges();
                            }
                        }
                    }

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                return success;
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region"Update Case"

        public bool UpdateCaseContactInfo(int caseId, AM_Case amCase, bool isCustomerEditView)
        {
            bool success = false;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AM_Case caseObj = new AM_Case();
                    List<AM_Case> list = (from ds in AMDBMgr.AM_Case
                                                 where ds.CaseId == caseId
                                                 select ds).ToList<AM_Case>();
                    if (list.ToList().Count > 0)
                    {
                        caseObj = list.First();

                        caseObj.JointTenantLetterRequired = amCase.JointTenantLetterRequired;
                        if (isCustomerEditView)
                        {
                            caseObj.AdditionalContactID = amCase.AdditionalContactID;
                            caseObj.ThirdPartyContactID = amCase.ThirdPartyContactID;
                        }
                        AMDBMgr.SaveChanges();
                        AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        tran.Complete();
                        success = true;
                    }
                }
                return success;
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion"Update Case"

        public bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, ArrayList documentList, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            bool success = false;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AM_Case caseObj = new AM_Case();
                    List<AM_Case> list = (from ds in AMDBMgr.AM_Case
                                          where ds.CaseId == caseId
                                          select ds).ToList<AM_Case>();
                    caseObj = list.First();

                    caseObj.CaseOfficer = amCase.CaseOfficer;
                    caseObj.CaseManager = amCase.CaseManager;
                    caseObj.StatusId = amCase.StatusId;
                    caseObj.StatusHistoryId = amCase.StatusHistoryId;
                    caseObj.StatusReview = amCase.StatusReview;
                    caseObj.ActionId = amCase.ActionId;
                    caseObj.ActionHistoryId = amCase.ActionHistoryId;
                    caseObj.ActionReviewDate = amCase.ActionReviewDate;
                    caseObj.RecoveryAmount = amCase.RecoveryAmount;
                    caseObj.NoticeIssueDate = amCase.NoticeIssueDate;
                    caseObj.NoticeExpiryDate = amCase.NoticeExpiryDate;
                    caseObj.WarrantExpiryDate = amCase.WarrantExpiryDate;
                    caseObj.HearingDate = amCase.HearingDate;
                    caseObj.Notes = amCase.Notes;
                    caseObj.OutcomeLookupCodeId = amCase.OutcomeLookupCodeId;
                    caseObj.Reason = amCase.Reason;
                    caseObj.ModifiedBy = amCase.ModifiedBy;
                    caseObj.ModifiedDate = amCase.ModifiedDate;
                    caseObj.IsDocumentAttached = amCase.IsDocumentAttached;
                    caseObj.IsDocumentUpload = amCase.IsDocumentUpload;
                    caseObj.IsActive = amCase.IsActive;
                    caseObj.IsCaseUpdated = true;
                    caseObj.StatusRecordedDate = amCase.StatusRecordedDate;
                    if (amCase.IsPaymentPlanIgnored != null)
                    {
                        caseObj.IsPaymentPlanIgnored = amCase.IsPaymentPlanIgnored;
                    }
                    if (amCase.PaymentPlanIgnoreReason != null)
                    {
                        caseObj.PaymentPlanIgnoreReason = amCase.PaymentPlanIgnoreReason;
                    }
                    if (amCase.IsPaymentPlan != null)
                    {
                        caseObj.IsPaymentPlan = amCase.IsPaymentPlan;
                    }
                    if (amCase.PaymentPlanId != null)
                    {
                        caseObj.PaymentPlanId = amCase.PaymentPlanId;
                    }
                    if (amCase.PaymentPlanHistoryId != null)
                    {
                        caseObj.PaymentPlanHistoryId = amCase.PaymentPlanHistoryId;
                    }
                    caseObj.RentBalance = amCase.RentBalance;
                    AMDBMgr.SaveChanges();

                    int caseHistoryId = 0;
                    if (actionData != null)
                    {
                        ActionBO actionBo = new ActionBO();
                        StatusBo statusBo = new StatusBo();
                        //int actionId = int.Parse(actionData.Rows[0]["ActionId"].ToString());
                        //List<AM_CaseHistory> listHistory = (from his in AMDBMgr.AM_CaseHistory
                        //                                    where his.CaseId == caseId && his.IsActive == true && his.ActionId == actionId
                        //                                    orderby his.CaseHistoryId ascending
                        //                                    select his).ToList();

                        //if (listHistory != null)
                        //{
                        //    for (int i = 0; i < listHistory.Count; i++)
                        //    {
                        //        listHistory[i].CaseId = caseId;
                        //        listHistory[i].IsActionRecorded = true;
                        //        listHistory[i].RecordedActionDetails = actionData.Rows[0]["IgnoreReason"].ToString();
                        //        listHistory[i].IsActionIgnored = false;
                        //        listHistory[i].ActionIgnoreCount = 0;
                        //        listHistory[i].ActionRecordedCount = +1;
                        //        //listHistory[i].IsPaymentPlan = false;
                        //        AMDBMgr.SaveChanges();
                        //    }
                        //}

                        AM_CaseHistory temp = new AM_CaseHistory();
                        temp.ActionId = caseHistory.ActionId;
                        temp.ActionHistoryId = caseHistory.ActionHistoryId;
                        temp.StatusId = caseHistory.StatusId;
                        temp.StatusHistoryId = caseHistory.StatusHistoryId;
                        temp.IsActionRecorded = caseHistory.IsActionRecorded;
                        temp.IsDocumentAttached = caseHistory.IsDocumentAttached;
                        temp.IsDocumentUpload = caseHistory.IsDocumentUpload;
                        //AM_CaseActionIgnoredAndRecorded caseAction = new AM_CaseActionIgnoredAndRecorded();
                        for (int i = 0; actionData.Rows.Count > i; i++)
                        {
                            caseHistory.CaseId = caseId;
                            caseHistory.CreatedDate = DateTime.Now;
                            caseHistory.IsActionIgnored = true;
                            caseHistory.IsActionRecorded = false;
                            caseHistory.ActionRecordedCount = 0;
                            caseHistory.ActionIgnoreCount = 1;
                            caseHistory.ActionIgnoreReason = actionData.Rows[i]["IgnoreReason"].ToString();
                            caseHistory.ActionId = int.Parse(actionData.Rows[i]["ActionId"].ToString());
                            caseHistory.StatusId = int.Parse(actionData.Rows[i]["StatusId"].ToString());
                            caseHistory.ActionHistoryId = actionBo.GetActionHistoryByActionId(caseHistory.ActionId);
                            caseHistory.StatusHistoryId = statusBo.GetStatusHistoryByStatusId(caseHistory.StatusId);
                            caseHistory.IsPaymentPlan = false;
                            caseHistory.IsPaymentPlanIgnored = false;
                            caseHistory.PaymentPlanIgnoreReason = string.Empty;

                            AMDBMgr.AddToAM_CaseHistory(caseHistory);
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(caseHistory);
                        }
                        caseHistory.CaseId = caseId;
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.IsActionRecorded = true;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.ActionRecordedCount = 1;
                        caseHistory.IsPaymentPlan = false;
                        caseHistory.ActionId = temp.ActionId;
                        caseHistory.ActionHistoryId = temp.ActionHistoryId;
                        caseHistory.StatusId = temp.StatusId;
                        caseHistory.StatusHistoryId = temp.StatusHistoryId;
                        caseHistory.IsDocumentUpload = temp.IsDocumentUpload;
                        caseHistory.IsDocumentAttached = temp.IsDocumentAttached;
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();
                        caseHistoryId = caseHistory.CaseHistoryId;
                        AMDBMgr.Detach(caseHistory);
                    }
                    else
                    {
                        caseHistory.CaseId = caseId;
                        caseHistory.CreatedDate = DateTime.Now;
                        caseHistory.IsActionRecorded = true;
                        caseHistory.IsActionIgnored = false;
                        caseHistory.ActionIgnoreCount = 0;
                        caseHistory.ActionRecordedCount = 1;
                        caseHistory.IsPaymentPlan = false;
                        AMDBMgr.AddToAM_CaseHistory(caseHistory);
                        AMDBMgr.SaveChanges();
                        caseHistoryId = caseHistory.CaseHistoryId;
                    }

                    AM_CaseAndStandardLetter letter = new AM_CaseAndStandardLetter();
                    for (int i = 0; i < st.Count; i++)
                    {
                        //if (!IsLetter(st[i].StandardLetterId, caseId))
                        {
                            letter.CaseId = caseId;
                            if (caseHistoryId != 0)
                            {
                                letter.CaseHistoryId = caseHistoryId;
                            }
                            else
                            {
                                letter.CaseHistoryId = caseHistory.CaseHistoryId;
                            }
                            if (st[i].StatusId != -1)
                            {
                                // standard letter history id has been saved in the free field of the object to save the computation.
                                letter.StandardLetterHistoryId = st[i].StatusId;
                            }
                            letter.StandardLetterId = st[i].StandardLetterId;
                            letter.IsActive = true;
                            letter.MarketRent = marketRent;
                            letter.RentBalance = rentBalance;
                            letter.CreateDate = todayDate;
                            letter.RentAmount = rentAmount;
                            AMDBMgr.AddToAM_CaseAndStandardLetter(letter);
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(letter);
                        }
                    }

                    if (oldSL != null)
                    {
                        for (int i = 0; i < oldSL.Count; i++)
                        {
                            int id = oldSL[i].StandardLetterId;
                            List<AM_CaseAndStandardLetter> listCaseSL = (from ds in AMDBMgr.AM_CaseAndStandardLetter
                                                                         where ds.StandardLetterId == id
                                                                         select ds).ToList();
                            if (listCaseSL != null)
                            {
                                AM_CaseAndStandardLetter letterSL = new AM_CaseAndStandardLetter();
                                // standard letter history id has been saved in the free field of the object to save the computation.
                                letter.MarketRent = marketRent;
                                letter.RentBalance = rentBalance;
                                letter.CreateDate = todayDate;
                                letter.RentAmount = rentAmount;
                                letter.StandardLetterHistoryId = oldSL[i].StatusId;
                                AMDBMgr.SaveChanges();
                            }
                        }
                    }

                    AM_Documents doc = null;
                    for (int i = 0; i < documentList.Count; i++)
                    {
                        doc = new AM_Documents();
                        doc.CaseId = caseId;
                        doc.DocumentName = documentList[i].ToString();
                        if (caseHistoryId != 0)
                        {
                            doc.CaseHistoryId = caseHistoryId;
                        }
                        else
                        {
                            doc.CaseHistoryId = caseHistory.CaseHistoryId;
                        }
                        doc.IsActive = true;
                        AMDBMgr.AddToAM_Documents(doc);
                        AMDBMgr.SaveChanges();
                        AMDBMgr.Detach(doc);
                    }

                    if (amCase.IsActive == false)
                    {
                        List<AM_PaymentPlan> listPayment = (from ds in AMDBMgr.AM_PaymentPlan
                                                            where ds.TennantId == amCase.TenancyId && ds.IsCreated == true
                                                            select ds).ToList();
                        if (listPayment != null && listPayment.Count > 0)
                        {
                            AM_PaymentPlan paymentPlan = listPayment.First();

                            paymentPlan.IsCreated = false;
                            paymentPlan.ModifiedBy = amCase.ModifiedBy.Value;
                            paymentPlan.ModifiedDate = amCase.ModifiedDate;

                            AMDBMgr.SaveChanges();

                            List<AM_PaymentPlanHistory> listHistory = (from history in AMDBMgr.AM_PaymentPlanHistory
                                                                       where history.PaymentPlanId == paymentPlan.PaymentPlanId && history.IsCreated == true
                                                                       orderby history.PaymentPlanHistoryId descending
                                                                       select history).ToList();
                            AM_PaymentPlanHistory paymentPlanHistory = listHistory.First();

                            paymentPlanHistory.IsCreated = false;
                            paymentPlanHistory.ModifiedBy = amCase.ModifiedBy;
                            paymentPlanHistory.ModifiedDate = amCase.ModifiedDate;

                            AMDBMgr.SaveChanges();
                        }
                    }

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                return success;
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Check Suppress"

        public bool CheckSuppress(int caseId)
        {
            try
            {
                var returnValue = (from ds in AMDBMgr.AM_Case
                                   where ds.CaseId == caseId
                                   select new { ds.IsSuppressed });
                bool value = false;
                foreach (var i in returnValue)
                {
                    if (i.IsSuppressed)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
                return value;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Case Standard Letters"

        public DataTable GetCaseStandardLetters(int caseId)
        {
            try
            {
                var query = from ds in AMDBMgr.AM_CaseAndStandardLetter
                            join t in AMDBMgr.AM_StandardLetters on ds.StandardLetterId equals t.StandardLetterId
                            where ds.CaseId == caseId && ds.IsActive == true
                            select t;

                DataTable dt = new DataTable();
                dt.Columns.Add("StandardLetterId");
                dt.Columns.Add("StandardLetter");


                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["StandardLetterId"] = t.StandardLetterId;
                    dr["StandardLetter"] = t.Title;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetCaseStandardLetterHistory(int caseId)
        {
            try
            {
                var query = from ds in AMDBMgr.AM_CaseAndStandardLetter
                            join t in AMDBMgr.AM_StandardLetterHistory on ds.StandardLetterHistoryId equals t.StandardLetterHistoryId
                            where ds.CaseId == caseId && ds.IsActive == true
                            select t;

                DataTable dt = new DataTable();
                dt.Columns.Add("StandardLetterHistoryId");
                dt.Columns.Add("StandardLetter");


                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["StandardLetterHistoryId"] = t.StandardLetterHistoryId;
                    dr["StandardLetter"] = t.Title;

                    dt.Rows.Add(dr);
                }

                return dt;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Disbale Case Standard Letter"

        public void DisableCaseStandardLetter(int letterId, int caseId)
        {
            try
            {
                AM_CaseAndStandardLetter caseLetter = new AM_CaseAndStandardLetter();
                var result = from ds in AMDBMgr.AM_CaseAndStandardLetter
                             where ds.StandardLetterHistoryId == letterId && ds.CaseId == caseId && ds.IsActive == true
                             select ds;
                List<AM_CaseAndStandardLetter> list = result.ToList();
                caseLetter = list.First();
                caseLetter.IsActive = false;
                AMDBMgr.SaveChanges();

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IsLetterAttached
        public bool IsLetter(int item, int caseId)
        {
            try
            {
                IQueryable<AM_CaseAndStandardLetter> acquery = from ac in this.AMDBMgr.AM_CaseAndStandardLetter
                                                               where ac.StandardLetterId == item && ac.IsActive == true && ac.CaseId == caseId
                                                               select ac;
                /* Commented By Zunair Minhas on 22 June 2010
                IQueryable<AM_ActionAndStandardLetters> acquery = from ac in this.AMDBMgr.AM_ActionAndStandardLetters where ac.StandardLetterId == item select ac;
                 * */
                List<AM_CaseAndStandardLetter> actionstandardLetter = acquery.ToList();
                if (actionstandardLetter != null && actionstandardLetter.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"is Outcome Exists"

        public bool IsOutComeExists(int OutCome)
        {
            try
            {
                List<AM_Case> CaseList = (from Case in AMDBMgr.AM_Case where Case.OutcomeLookupCodeId == OutCome select Case).ToList<AM_Case>();
                if (CaseList != null && CaseList.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Check Recovery Amount"

        public bool CheckRecoveryAmount(int statusId)
        {
            try
            {
                var result = from status in AMDBMgr.AM_Status where status.StatusId == statusId select status.IsRecoveryAmount;

                bool isAmount = false;
                foreach (var t in result)
                {
                    if (t != null)
                    {
                        isAmount = t.Value;
                    }
                }
                return isAmount;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Get Patch By Case Owned By"

        public IQueryable getRegionList(int CaseOwnedById)
        {
            try
            {
                if (CaseOwnedById != -1)
                {
                    var regionList = (from ds in AMDBMgr.E_PATCH
                                      join mid in AMDBMgr.AM_ResourcePatchDevelopment on ds.PATCHID equals mid.PatchId
                                      join resource in AMDBMgr.AM_Resource on mid.ResourceId equals resource.ResourceId
                                      where mid.ResourceId == CaseOwnedById && resource.IsActive == true && mid.IsActive == true
                                      orderby ds.PATCHID
                                      select new
                                      {
                                          PatchId = ds.PATCHID,
                                          Patch = ds.LOCATION
                                      }).Distinct();
                    return regionList;
                }
                else
                {
                    var regionList = (from ds in AMDBMgr.E_PATCH
                                      join mid in AMDBMgr.AM_ResourcePatchDevelopment on ds.PATCHID equals mid.PatchId
                                      join resource in AMDBMgr.AM_Resource on mid.ResourceId equals resource.ResourceId
                                      orderby ds.PATCHID
                                      select new
                                      {
                                          PatchId = ds.PATCHID,
                                          Patch = ds.LOCATION
                                      }).Distinct();
                    return regionList;
                }

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        #region "Get Suburb By Case Owned By"

        public IQueryable getSuburbList(int RegionId, int CaseOwnedById)
        {
            try
            {
                var regionList = (from ds in AMDBMgr.P_SCHEME
                                  join mid in AMDBMgr.AM_ResourcePatchDevelopment on ds.SCHEMEID equals mid.SCHEMEID
                                  join resource in AMDBMgr.AM_Resource on mid.ResourceId equals resource.ResourceId
                                  where mid.ResourceId == CaseOwnedById && mid.PatchId == RegionId
                                        && resource.IsActive == true && mid.IsActive == true
                                  orderby ds.SCHEMEID
                                  select new
                                  {
                                      DevelopmentId = ds.SCHEMEID,
                                      DevelopmentName = ds.SCHEMENAME
                                  }).Distinct();

                return regionList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        public int GetOverdueActionsAlertCount(int ResourceId, int regionid, int suburbid)
        {

            var recordsCount = AMDBMgr.AM_SP_OverdueActionsAlertCount(ResourceId, regionid, suburbid);
            int i = 0;
            foreach (var t in recordsCount)
            {
                i = t.Value;
            }
            return i;
        }

        #region Find OverDue Action Cases
        public List<AM_SP_FindOverDueActionCases_Result> FindOverDueActionCases()
        {
            try
            {
                List<AM_SP_FindOverDueActionCases_Result> overDueCaselist = new List<AM_SP_FindOverDueActionCases_Result>();
                var list = AMDBMgr.AM_SP_FindOverDueActionCases();
                overDueCaselist = list.ToList();
                return overDueCaselist;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SaveOverDueActionCaseRecord
        public void SaveOverDueActionCaseRecord(AM_FindOverDueActionCase overDueActionCase)
        {
            try
            {
                AMDBMgr.AddToAM_FindOverDueActionCase(overDueActionCase);
                AMDBMgr.SaveChanges();
                AMDBMgr.Detach(overDueActionCase);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SaveOverDueActionTransaction
        public void SaveOverDueActionTransaction()
        {
            try
            {
                AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Delete OverDue Action Cases"
        //Updated by:Noor
        //Update Date:30 08 2011
        public void DeleteOverDueActionCases()
        {
            List<AM_FindOverDueActionCase> list = (from ds in AMDBMgr.AM_FindOverDueActionCase

                                                   select ds).ToList();

            foreach (var ds in list)
            {
                AMDBMgr.AM_FindOverDueActionCase.DeleteObject(ds);
            }

            try
            {
                AMDBMgr.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Update End
        #endregion

    }
}
