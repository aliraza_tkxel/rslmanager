﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.case_bo;
using System.Data;
using Am.Ahv.Entities;

namespace Am.Ahv.BusinessObject.case_bo
{
    public class SuppressedCaseBO : BaseBO, ISuppressedCaseBO
    {
        public SuppressedCaseBO()
        {

            AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region "Get Assigned To List"

        public IQueryable GetAssignedToList()
        {
            try
            {
                var assignedToList = (from t in AMDBMgr.AM_Resource
                                      join t0 in AMDBMgr.E__EMPLOYEE on t.EmployeeId equals t0.EMPLOYEEID                                      
                                      where
                                        t.IsActive == true 
                                      orderby
                                        t0.FIRSTNAME 
                                      select new
                                      {
                                          t.ResourceId,
                                          EmployeeId = (Int32?)t.EmployeeId,
                                          EmployeeName = ((t0.FIRSTNAME ?? "") + " " + (t0.LASTNAME ?? ""))                                          
                                      });
                return assignedToList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            
        }

        #endregion

        #region"Get Region List"

        public IQueryable GetRegionList(int resourceID)
        {
            try
            {
                var regionList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                  join t in AMDBMgr.E_PATCH on ds.PatchId equals t.PATCHID
                                  where ds.ResourceId == resourceID && ds.IsActive == true
                                  orderby t.PATCHID
                                  select new
                                  {
                                      t.PATCHID,
                                      t.LOCATION
                                  }).Distinct();
                return regionList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Suppressed Cases"

        public IQueryable GetSuppressedCases(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string sortExpression, string sortDir)
        {
            try
            {
                var suppressedCases = (AMDBMgr.AM_SP_GetSuppressedCaseList(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, sortExpression, sortDir));
                return suppressedCases.AsQueryable();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        #endregion

        #region"Get Suppressed Cases Count"

        public int GetSuppressedCasesCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                var count = (AMDBMgr.AM_SP_GetSuppressedCaseListCount(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag));
                int i = 0;
                foreach (var t in count)
                {
                    i = t.Value;
                }
                return i;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region IBaseBO<AM_Case,int> Members

        public Entities.AM_Case GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Entities.AM_Case> GetAll()
        {
            throw new NotImplementedException();
        }

        public Entities.AM_Case AddNew(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public Entities.AM_Case Update(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Entities.AM_Case obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region "Cases Owned By"
        public IQueryable CasesOwnedBy()
        {
            try
            {
                var casesOwnedBy = (from t in AMDBMgr.AM_Resource
                                      join t0 in AMDBMgr.E__EMPLOYEE on t.EmployeeId equals t0.EMPLOYEEID
                                      join t1 in AMDBMgr.E_JOBDETAILS on t0.EMPLOYEEID equals t1.EMPLOYEEID
                                      join t2 in AMDBMgr.AM_LookupCode on t.AM_LookupCode.LookupCodeId equals t2.LookupCodeId
                                      where
                                        t.IsActive == true && t2.CodeName == "Case Officer" && t2.IsActive == true
                                      orderby
                                        t0.FIRSTNAME
                                      select new
                                      {
                                          t.ResourceId,
                                          EmployeeId = (Int32?)t.EmployeeId,
                                          EmployeeName = ((t0.FIRSTNAME ?? "") + " " + (t0.LASTNAME ?? "")),
                                          t2.CodeName
                                      });
                return casesOwnedBy;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion

        #region IBaseBO<AM_Case,int> Members


        public List<AM_Case> GetAllResources()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
