using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.case_bo;

using Am.Ahv.BusinessObject.base_bo;
namespace Am.Ahv.BusinessObject.case_bo
{
    class CaseHistoryBo : BaseBO, ICaseHistoryBo
    {
        public CaseHistoryBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }
        #region ICaseHistoryBo Members

        public IQueryable GetCaseHistoryByCaseId(int CaseID)
        {
            try
            {
                var query = from t in AMDBMgr.AM_Action
                            join t0 in AMDBMgr.AM_Case on t.ActionId equals t0.ActionId
                            join t1 in AMDBMgr.AM_CaseHistory on t0.CaseId equals t1.CaseId
                            join t2 in AMDBMgr.AM_Status on t0.StatusId equals t2.StatusId
                            where
                              t1.AM_Case.CaseId == CaseID
                            select new
                            {
                                ActionTitle = t.Title,
                                date = t1.CreatedDate,
                                Review = t1.ActionReviewDate,
                                StatusTitle = t2.Title,
                                CaseId = t1.AM_Case.CaseId,
                                IsDoc = t1.IsDocumentUpload,
                                CaseHistoryId = t1.CaseHistoryId,

                            };



                return query;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (ObjectNotFoundException objectnotfound)
            {
                throw objectnotfound;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IBaseBO<AM_CaseHistory,int> Members

        public AM_CaseHistory GetById(int id)
        {
            try
            {
                IQueryable<AM_CaseHistory> ch = from casehistory in this.AMDBMgr.AM_CaseHistory where casehistory.CaseHistoryId == id select casehistory;
                List<AM_CaseHistory> clist = ch.ToList();
                if (clist.Count > 0)
                {

                    return LoadRefernces(clist.First());

                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_CaseHistory> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_CaseHistory AddNew(AM_CaseHistory obj)
        {
            throw new NotImplementedException();
        }

        public AM_CaseHistory Update(AM_CaseHistory obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AM_CaseHistory obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICaseHistoryBo Members


        public List<AM_CaseHistory> GetCasehistory(int CaseID)
        {
            try
            {
                IQueryable<AM_CaseHistory> casequery = from c in this.AMDBMgr.AM_CaseHistory where c.AM_Case.CaseId == CaseID orderby c.CaseHistoryId descending select c;
                List<AM_CaseHistory> caselist = casequery.ToList();
                if (caselist.Count > 0)
                {
                    return LoadRefernces(caselist);
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<AM_CaseHistory> LoadRefernces(List<AM_CaseHistory> CHList)
        {
            try
            {
                foreach (AM_CaseHistory item in CHList)
                {
                    item.AM_CaseReference.Load();
                    item.AM_Case.AM_ActionReference.Load();
                    item.AM_Case.AM_StatusReference.Load();
                    item.AM_Activity.Load();
                    item.AM_LookupCodeReference.Load();
                }
                return CHList;
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private AM_CaseHistory LoadRefernces(AM_CaseHistory item)
        {
            //item.AM_CaseReference.Load();
            //item.AM_Case.AM_ActionReference.Load();
            //item.AM_Case.AM_StatusReference.Load();
            //item.AM_LookupCodeReference.Load();
            //item.AM_Resource1Reference.Load();


            item.AM_ResourceReference.Load();
            item.AM_Resource2Reference.Load();
            item.AM_Resource1Reference.Load();
            item.AM_LookupCodeReference.Load();
            item.AM_ActionReference.Load();

            item.AM_StatusReference.Load();

            item.AM_StatusReference.Load();

            item.AM_Documents.Load();

            return item;
        }
        #endregion

        #region ICaseHistoryBo Members


        public IQueryable getCaseList()
        {
            //var caseList = AMDBMgr.AM_SP_GetCaseList3(0, 0, true, true);
            //return caseList.AsQueryable();
            throw new NotImplementedException();
        }

        #endregion

        #region ICaseHistoryBo Members


        public IQueryable getCloseCaseList()
        {
            //var caseList = AMDBMgr.AM_SP_GetCaseList3(0, 0, true, true);
            //return caseList.AsQueryable();
            throw new NotImplementedException();
        }

        #endregion

        #region ICaseHistoryBo Members
        /// <summary>
        /// Author:Nouman
        /// Revision:1.0
        /// Summary: This method is alreay implemented. So no need to reinvent the wheel.
        /// </summary>
        /// <param name="CaseID"></param>
        /// <returns></returns>

        //public List<AM_CaseHistory> GetCaseHisotyByCaseId(int CaseID)
        //{
        //    try
        //    {
        //        IQueryable<AM_CaseHistory> Casequery = from ch in this.AMDBMgr.AM_CaseHistory where ch.AM_Case.CaseId == CaseID select ch;
        //        List<AM_CaseHistory> caselist = Casequery.ToList();
        //        if (caselist.Count > 0)
        //        {

        //            return caselist;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion

        #region ICaseHistoryBo Members


        public List<AM_CaseHistory> GetCaseHistoryByID(int CaseHistoryId)
        {
            try
            {
                IQueryable<AM_CaseHistory> ch = from p in this.AMDBMgr.AM_CaseHistory where p.CaseHistoryId == CaseHistoryId select p;
                List<AM_CaseHistory> clist = ch.ToList();
                if (clist.Count > 0)
                {
                    return clist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ICaseHistoryBo Members


        public List<int> GetCaseHistoryCount(int CaseID)
        {
            try
            {
                IQueryable<AM_CaseHistory> HLQ = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseID select ch;
                List<AM_CaseHistory> Clist = HLQ.ToList();
                if (Clist.Count > 0)
                {
                    return GetHistoryIDList(Clist);
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Utility
        private List<int> GetHistoryIDList(List<AM_CaseHistory> HistoryList)
        {
            List<int> HistoryIDList = new List<int>();
            foreach (AM_CaseHistory item in HistoryList)
            {
                HistoryIDList.Add(item.CaseHistoryId);
            }
            return HistoryIDList;
        }


        #region "Get Case List"

        public IQueryable getCaseList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck)
        {
            try
            {                                
                var caseList = (AMDBMgr.AM_SP_GetCaseList(postCode, caseOwnerId, regionId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, dbIndex, pageSize, surname, sortExpression, sortDir, isNisp, OverdueCheck));
                return caseList.AsQueryable();
                
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region Get OverDue Action Case List
        public List<AM_SP_GetOverDueList_Result> getOverDueActionCaseList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck)
        {
            try
            {
                
                List<AM_SP_GetOverDueList_Result> caseList = AMDBMgr.AM_SP_GetOverDueList(postCode, caseOwnerId, regionId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, dbIndex, pageSize, surname, sortExpression, sortDir).ToList();

                return caseList;
                
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion 

        #region "Get Close Case List"

        public IQueryable getCloseCaseList(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir)
        {
            try
            {
                var caseList = (AMDBMgr.AM_SP_GetCloseCaseList(caseOwnerId, regionId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, dbIndex, pageSize, surname, sortExpression, sortDir));
                return caseList.AsQueryable();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion



        #region"Get Case Owner List"

        public IQueryable getCaseOwnerList()
        {
            try
            {
                var caseOwnerList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                     join resource in AMDBMgr.AM_Resource on ds.ResourceId equals resource.ResourceId
                                     join employee in AMDBMgr.E__EMPLOYEE on resource.EmployeeId equals employee.EMPLOYEEID
                                     where resource.IsActive == true && ds.IsActive == true
                                     orderby ds.AM_Resource.ResourceId
                                     select new
                                     {
                                         ResourceId = ds.AM_Resource.ResourceId,
                                         EmployeeName = (employee.FIRSTNAME ?? "") + " " + (employee.LASTNAME ?? "")
                                     }).Distinct();

                return caseOwnerList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IQueryable getCaseOwnerList(int regionId)
        {
            try
            {
                var caseOwnerList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                     join resource in AMDBMgr.AM_Resource on ds.ResourceId equals resource.ResourceId
                                     join employee in AMDBMgr.E__EMPLOYEE on resource.EmployeeId equals employee.EMPLOYEEID
                                     where ds.PatchId == regionId && resource.IsActive == true && ds.IsActive == true
                                     orderby ds.AM_Resource.ResourceId
                                     select new
                                     {
                                         ResourceId = ds.AM_Resource.ResourceId,
                                         EmployeeName = (employee.FIRSTNAME ?? "") + " " + (employee.LASTNAME ?? "")
                                     }).Distinct();

                return caseOwnerList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IQueryable getCaseOwnerList(int regionId, int suburbId)
        {
            try
            {
                var caseOwnerList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                     join resource in AMDBMgr.AM_Resource on ds.ResourceId equals resource.ResourceId
                                     join employee in AMDBMgr.E__EMPLOYEE on resource.EmployeeId equals employee.EMPLOYEEID
                                     where ds.PatchId == regionId && ds.SCHEMEID == suburbId && ds.IsActive == true
                                     orderby ds.AM_Resource.ResourceId
                                     select new
                                     {
                                         ResourceId = ds.AM_Resource.ResourceId,
                                         EmployeeName = (employee.FIRSTNAME ?? "") + " " + (employee.LASTNAME ?? "")
                                     }).Distinct();

                return caseOwnerList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion


        public IQueryable getRegionList(int CaseOwnedById)
        {
            try
            {
                var regionList = (from ds in AMDBMgr.E_PATCH
                                  join mid in AMDBMgr.AM_ResourcePatchDevelopment on ds.PATCHID equals mid.PatchId
                                  join resource in AMDBMgr.AM_Resource on mid.ResourceId equals resource.ResourceId
                                  where mid.ResourceId == CaseOwnedById && resource.IsActive == true && mid.IsActive == true
                                  orderby mid.AM_Resource.ResourceId
                                  select new
                                  {
                                      ResourceId = mid.AM_Resource.ResourceId,
                                      Patch = ds.LOCATION
                                  }).Distinct();

                return regionList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #region"Get Records Count"

        public int getRecordsCount(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle, string surname, bool isNisp, string OverdueCheck)
        {
            try
            {
                var caseListCount = (AMDBMgr.AM_SP_GetCaseListCount(postCode, caseOwnerId, regionId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, surname, isNisp, OverdueCheck));
                int i = 0;
                foreach (var t in caseListCount)
                {
                    i = t.Value;
                }
                return i;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Close Records Count"

        public int getCloseRecordsCount(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle, string surname)
        {
            try
            {
                var caseListCount = (AMDBMgr.AM_SP_GetCloseCaseListCount(caseOwnerId, regionId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, surname));
                int i = 0;
                foreach (var t in caseListCount)
                {
                    i = t.Value;
                }
                return i;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #endregion

        #region ICaseHistoryBo Members


        public bool UpdateHistoryIgnoreByStatusAction(int StatusId, int ActionId, int CaseId)
        {
            try
            {
                IQueryable<AM_CaseHistory> casehistoryquery = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseId && ch.StatusId == StatusId && ch.ActionId == ActionId select ch;
                List<AM_CaseHistory> chlist = casehistoryquery.ToList();
                if (chlist.Count > 0)
                {
                    AM_CaseHistory History = chlist.First();
                    History.ActionIgnoreCount = +1;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ICaseHistoryBo Members


        public bool UpdateHistoryRecordStatusAction(int StatusId, int ActionId, int CaseId)
        {
            try
            {
                IQueryable<AM_CaseHistory> casehistoryquery = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseId && ch.StatusId == StatusId && ch.ActionId == ActionId select ch;
                List<AM_CaseHistory> chlist = casehistoryquery.ToList();
                if (chlist.Count > 0)
                {
                    AM_CaseHistory History = chlist.First();
                    History.ActionRecordedCount = +1;
                    this.AMDBMgr.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ICaseHistoryBo Members


        public bool UpdateHistorySupressedValues(int CaseId, string Reason, DateTime Date, int supressedBy)
        {
            try
            {
                IQueryable<AM_CaseHistory> cquery = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseId && ch.IsActionRecorded == true select ch;
                AM_CaseHistory clist = cquery.ToList().Last();
                clist.SuppressedReason = Reason;
                clist.SuppressedDate = Date;
                clist.SuppressedBy = supressedBy;
                clist.IsSuppressed = true;
                this.AMDBMgr.SaveChanges();
                return true;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ICaseHistoryBo Members


        public bool UpdateActionCount(int CaseId)
        {
            try
            {
                IQueryable<AM_CaseHistory> cquery = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseId select ch;
                AM_CaseHistory clist = cquery.ToList().Last();
                clist.ActionRecordedCount = +1;
                clist.IsActionRecorded = true;
                clist.ActionRecordedDate = DateTime.Now;
                this.AMDBMgr.SaveChanges();
                return true;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ICaseHistoryBo Members


        public bool UpdateActionIgnoreCount(int CaseId)
        {
            try
            {
                IQueryable<AM_CaseHistory> cquery = from ch in this.AMDBMgr.AM_CaseHistory where ch.CaseId == CaseId select ch;
                AM_CaseHistory clist = cquery.ToList().Last();
                clist.ActionIgnoreCount = +1;
                clist.IsActionIgnored = true;
                this.AMDBMgr.SaveChanges();
                return true;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region ICaseHistoryBo Members GetLatest.CaseHistory


        public AM_CaseHistory GetLatestCaseHistory(int CaseId)
        {
            IQueryable<AM_CaseHistory> CaseQuery = from Ch in this.AMDBMgr.AM_CaseHistory where Ch.CaseId == CaseId orderby Ch.CaseHistoryId select Ch;
            List<AM_CaseHistory> Clist = CaseQuery.ToList();
            if (Clist != null && Clist.Count > 0)
            {
                return Clist.Last();
            }
            else
            {
                return null;
            }
        }

        public int GetLatestCaseHistoryId(int CaseId)
        {
            IQueryable<AM_CaseHistory> CaseQuery = from Ch in this.AMDBMgr.AM_CaseHistory where Ch.CaseId == CaseId orderby Ch.CaseHistoryId select Ch;
            List<AM_CaseHistory> Clist = CaseQuery.ToList();
            if (Clist != null && Clist.Count > 0)
            {
                return Clist.Last().CaseHistoryId;
            }
            else
            {
                return -1;
            }
        }

        #endregion
    }
}
