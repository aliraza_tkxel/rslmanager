using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BoInterface.resourcebo;
using Am.Ahv.BoInterface.customerbo;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.team_bo;
using Am.Ahv.BoInterface.status_bo;
using Am.Ahv.BoInterface.resource_bo;
using Am.Ahv.BusinessObject.status_bo;
using Am.Ahv.BoInterface.actionbo;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.Customer_bo;
using Am.Ahv.BoInterface.documents_bo;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BoInterface.activity_bo;
using Am.Ahv.BoInterface.tenantbo;
using Am.Ahv.BusinessObject.region_suburb_bo;
using Am.Ahv.BusinessObject.resource_bo;
using Am.Ahv.BusinessObject.user_bo;
using Am.Ahv.BusinessObject.Lookup_bo;
using Am.Ahv.BusinessObject.team_bo;
using Am.Ahv.BusinessObject.action_bo;
using Am.Ahv.BusinessObject.activity_bo;
using Am.Ahv.BusinessObject.case_bo;
using Am.Ahv.BusinessObject.documents_bo;
using Am.Ahv.BusinessObject.paymentbo;
using Am.Ahv.BusinessObject.letters_bo;
using Am.Ahv.BoInterface.letters_bo;
using Am.Ahv.BoInterface.referral_bo;
using Am.Ahv.BusinessObject.referral_bo;
using Am.Ahv.BoInterface.reports_bo;
using Am.Ahv.BusinessObject.reports_bo;

namespace Am.Ahv.BusinessObject.factory_bo
{
    public class FactoryBO
    {
        public static IResourceBO CreateResourceBO()
        {
            return new ResourceBO();
        }

        public static CustomerBo CreateCustomerBO()
        {
            return new CustomerBo();
        }

        public static IUserBo CreateUserBO()
        {
            return new UserBo();
        }
        public static IRegionBo CreateRegionBO()
        {
            return new RegionBo();
        }
        public static ISuburbBo CreateSuburbBO()
        {
            return new SuburbBo();
        }

        public static IStreetBo CreateStreetBO()
        {
            return new StreetBo();
        }

        public static IAssetTypeBo CreateAssetTypeBO()
        {
            return new AssetTypeBo();
        }

        public static ILookupBo CreateLookupBO()
        {
            return new LookupBo();
        }
        public static ITeamBo CreateTeamBO()
        {
            return new TeamBo();
        }
        public static IResourcePatchDevelopment CreateJuntcionBO()
        {
            return new ResourcePatchDevelopmentBo();
        }
        public static IStatusBo CreateStatusBO()
        {
            return new StatusBo();
        }
        public static IActionBO CreateActionBO()
        {
            return new ActionBO();
        }

        public static IActivityBO CreateActivityBO()
        {
            return new ActivityBO();
        }
        public static ICaseHistoryBo CreateCaseHistoryBO()
        {
            return new CaseHistoryBo();
        }
        public static IDocumentBo CreateDocumentBO()
        {
            return new DocumentBo();
        }
        public static ICaseBo CreateCaseBO()
        {
            return new CaseBo();
        }
        public static ITenantBo CreateTenantBO()
        {
            return new Am.Ahv.BusinessObject.tenant_bo.TenantBo();
        }
        public static IFirstDetectionBO createFirstDetectionBO()
        {
            return new FirstDetectionBO();
        }

        public static IPayment CreatePaymentBO()
        {
            return new PaymentBo();
        }

        public static IReferralBO CreateReferralBO()
        {
            return new ReferralBO();
        }
        //public static IPaymentPlanBo CreatePaymentPlanBo();
        //{
        //    return new Am.Ahv.BusinessObject.paymentbo.PaymentPlanBo();
        //}
        public static IPaymentPlanBo CreatePaymentPlanBo()
        {
            return new PaymentPlanBo();
        }
        

        public static IPayment CreatePaymentBo()
        {
            return new PaymentBo();
        }


        //public static IPayment CreatePaymentBo()
        //{
        //     return new
        //}

        public static ICustomerBo CreateCustomerBo()
        {
            return new CustomerBo();
        }
        public static ISuppressedCaseBO CreateSuppressedCaseBO()
        {
            return new SuppressedCaseBO();
        }

        public static IOpenCaseBO createOpenCaseBO()
        {
            return new OpenCaseBO();
        }

        public static IStatusAndActionReportBO CreateStatusAndActionReportBO()
        {
            return new StatusAndActionReportBO();
        }

        public static IBalanceListBO CreateBalalnceListBO()
        {
            return new BalanceListBO();
        }
        public static ILettersBo CreateLettersBO()
        {
            return new LettersBo();
        }
        public static ICaseStandardLetter CreateCaseStandardLetter()
        {
            return new CaseStandardLetterBo();
        }
        public static INegativeRentBalance CreateNegativeRentBalance()
        {
            return new NegativeRentBalanceBO();
        }
    }
}
