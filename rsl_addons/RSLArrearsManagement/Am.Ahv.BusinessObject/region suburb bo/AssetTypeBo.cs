﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.Entities;
using System.Data;

namespace Am.Ahv.BusinessObject.region_suburb_bo
{
    class AssetTypeBo:base_bo.BaseBO,IAssetTypeBo
    {
        public AssetTypeBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region "Get Assigned To List"

        public IQueryable GetAssetType()
        {
            try
            {
                var assetType = (from t in AMDBMgr.P_ASSETTYPE
                                      orderby
                                        t.ASSETTYPEID
                                      select new
                                      {
                                          t.ASSETTYPEID,
                                          AssetTypeId = (Int32?)t.ASSETTYPEID,
                                          AssetTypeDescription = t.DESCRIPTION
                                      });
                return assetType;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        #endregion

        #region IBaseBO<P_ASSETTYPE,int> Members

        public P_ASSETTYPE GetById(int id)
        {
            throw new NotImplementedException();

        }

        public List<P_ASSETTYPE> GetAll()
        {
            throw new NotImplementedException();
        }

        public P_ASSETTYPE AddNew(P_ASSETTYPE obj)
        {
            throw new NotImplementedException();
        }

        public P_ASSETTYPE Update(P_ASSETTYPE obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(P_ASSETTYPE obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
