﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.region_suburb_bo;
using System.Data;
using Am.Ahv.Entities;
namespace Am.Ahv.BusinessObject.region_suburb_bo
{
    class SuburbBo : base_bo.BaseBO, ISuburbBo
    {
        public SuburbBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region IBaseBO<P_DEVELOPMENT,int> Members

        public Am.Ahv.Entities.P_SCHEME GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Am.Ahv.Entities.P_SCHEME> GetAll()
        {
            return this.AMDBMgr.P_SCHEME.ToList();
        }

        public Am.Ahv.Entities.P_SCHEME AddNew(Am.Ahv.Entities.P_SCHEME obj)
        {
            throw new NotImplementedException();
        }

        public Am.Ahv.Entities.P_SCHEME Update(Am.Ahv.Entities.P_SCHEME obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Am.Ahv.Entities.P_SCHEME obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region get Suburb By Region

        public List<Am.Ahv.Entities.P_SCHEME> GetSuburbByRegion(int RegionID)
        {
            try
            {
                IQueryable<Am.Ahv.Entities.P_SCHEME> pquery = from sch in this.AMDBMgr.P_SCHEME
                                                              join dev in AMDBMgr.PDR_DEVELOPMENT on sch.DEVELOPMENTID equals dev.DEVELOPMENTID
                                                              where dev.PATCHID == RegionID
                                                              orderby sch.SCHEMENAME ascending
                                                              select sch;
                List<Am.Ahv.Entities.P_SCHEME> suburblist = pquery.ToList();
                if (suburblist.Count > 0)
                {
                    return suburblist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get All Suburbs"

        public List<SchemeBo> GetAllSuburbs()
        {
            try
            {
                List<SchemeBo> listSuburb = (from ds in AMDBMgr.P_SCHEME
                                             join t in AMDBMgr.PDR_DEVELOPMENT on ds.DEVELOPMENTID equals t.DEVELOPMENTID
                                             orderby ds.SCHEMENAME ascending
                                             select new SchemeBo
                                             {
                                                 DevelopmentId = ds.DEVELOPMENTID,
                                                 DevelopmentName = t.DEVELOPMENTNAME,
                                                 SchemeId = ds.SCHEMEID,
                                                 SchemeName = ds.SCHEMENAME,
                                                 PatchId = t.PATCHID
                                             }).ToList();
                return listSuburb;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ISuburbBo Members


        public List<Entities.P_SCHEME> GetSuburbByIds(List<Entities.AM_ResourcePatchDevelopment> AreasList)
        {
            try
            {
                List<int> SuburbList = new List<int>();
                List<int> PatchList = new List<int>();
                foreach (AM_ResourcePatchDevelopment item in AreasList)
                {
                    if (item.SCHEMEID != null && item.IsActive == true)
                    {
                        SuburbList.Add(item.SCHEMEID.Value);
                    }
                    if (item.PatchId != null && item.IsActive == true)
                    {
                        PatchList.Add(item.PatchId.Value);
                    }
                }
                using (this.AMDBMgr = new TKXEL_RSLManager_UKEntities())
                {
                    IQueryable<P_SCHEME> pquery = (from suburb in this.AMDBMgr.P_SCHEME
                                                   join d in this.AMDBMgr.PDR_DEVELOPMENT on suburb.DEVELOPMENTID equals d.DEVELOPMENTID
                                                   join p in this.AMDBMgr.E_PATCH on d.PATCHID equals p.PATCHID
                                                   where SuburbList.Contains(suburb.SCHEMEID) 
                                                   //&& PatchList.Contains(p.PATCHID)
                                                   select suburb);
                    List<P_SCHEME> suburblist = pquery.ToList();
                    if (suburblist != null && suburblist.Count > 0)
                    {
                        return suburblist;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion

        public IQueryable GetSuburbsList(int resourceid)
        {
            try
            {
                var suburbList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                  join sch in AMDBMgr.P_SCHEME on ds.SCHEMEID equals sch.SCHEMEID
                                  join t in AMDBMgr.PDR_DEVELOPMENT on sch.DEVELOPMENTID equals t.DEVELOPMENTID
                                  where ds.ResourceId == resourceid && ds.IsActive == true
                                  orderby t.DEVELOPMENTID
                                  select new
                                  {
                                      t.DEVELOPMENTID,
                                      t.DEVELOPMENTNAME,
                                      sch.SCHEMENAME
                                  }).Distinct();

                return suburbList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public IQueryable GetSuburbsListByRegion(string region, int userId)
        {
            try
            {
                var suburbList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
                                  join sch in AMDBMgr.P_SCHEME on ds.SCHEMEID equals sch.SCHEMEID
                                  join t in AMDBMgr.PDR_DEVELOPMENT on sch.DEVELOPMENTID equals t.DEVELOPMENTID
                                  join p in AMDBMgr.E_PATCH on t.PATCHID equals p.PATCHID
                                  where p.LOCATION == region && ds.IsActive == true && ds.ResourceId == userId

                                  select new
                                  {
                                      t.DEVELOPMENTID,
                                      sch.SCHEMEID,
                                      sch.SCHEMENAME
                                  }).Distinct();

                return suburbList;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }



        #region get Suburb By USER

        public List<Am.Ahv.Entities.P_SCHEME> GetSuburbByUser(int RegionID, int UserId)
        {
            try
            {
                IQueryable<Am.Ahv.Entities.P_SCHEME> pquery = from ds in AMDBMgr.P_SCHEME
                                                              join dl in AMDBMgr.AM_ResourcePatchDevelopment on
                                                               ds.SCHEMEID equals dl.SCHEMEID
                                                              join dn in AMDBMgr.AM_Resource on
                                                               dl.ResourceId equals dn.ResourceId
                                                              join t in AMDBMgr.PDR_DEVELOPMENT on ds.DEVELOPMENTID equals t.DEVELOPMENTID
                                                              where dn.ResourceId == UserId && dl.IsActive == true && t.PATCHID == RegionID
                                                              select ds;
                List<Am.Ahv.Entities.P_SCHEME> suburblist = pquery.ToList();
                return suburblist;

                //if (suburblist.Count > 0)
                //{
                //    return suburblist;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region get Suburb By Only USER

        public List<Am.Ahv.Entities.P_SCHEME> GetSuburbByUser(int UserId)
        {
            try
            {
                IQueryable<Am.Ahv.Entities.P_SCHEME> pquery = from ds in AMDBMgr.P_SCHEME
                                                              join dl in AMDBMgr.AM_ResourcePatchDevelopment on
                                                               ds.SCHEMEID equals dl.SCHEMEID
                                                              join dn in AMDBMgr.AM_Resource on
                                                               dl.ResourceId equals dn.ResourceId
                                                              join t in AMDBMgr.PDR_DEVELOPMENT on ds.DEVELOPMENTID equals t.DEVELOPMENTID
                                                              where dn.ResourceId == UserId && dl.IsActive == true
                                                              select ds;
                List<Am.Ahv.Entities.P_SCHEME> suburblist = pquery.ToList();
                return suburblist;

                //if (suburblist.Count > 0)
                //{
                //    return suburblist;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion





    }
}
