﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.region_suburb_bo;
using System.Data;
using Am.Ahv.Entities;
namespace Am.Ahv.BusinessObject.region_suburb_bo
{
    class RegionBo:BaseBO,IRegionBo
    {
        public RegionBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

    

        


        #region IBaseBO<E_PATCH,int> Members

        public Am.Ahv.Entities.E_PATCH GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Am.Ahv.Entities.E_PATCH> GetAll()
        {
            try
            {
                List<Am.Ahv.Entities.E_PATCH> regionlist = this.AMDBMgr.E_PATCH.ToList();
               
                if (regionlist.Count > 0)
                {
                    return regionlist;
                }
                else
                {
                    return null;
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Am.Ahv.Entities.E_PATCH AddNew(Am.Ahv.Entities.E_PATCH obj)
        {
            throw new NotImplementedException();
        }

        public Am.Ahv.Entities.E_PATCH Update(Am.Ahv.Entities.E_PATCH obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Am.Ahv.Entities.E_PATCH obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Region Report"

        public List<Entities.E_PATCH> GetRegionReport()
        {
            IQueryable<Entities.E_PATCH> pquery= from region in this.AMDBMgr.E_PATCH  orderby region.PATCHID select region;
            List<Entities.E_PATCH> regionlist = pquery.ToList();
            return regionlist;
        }

        #endregion

        #region"Get Total Rent By Region"

        public List<Am.Ahv.Entities.AM_SP_Region_Amount_Result> GetTotalRentByRegion(int ResourceId)
        {
            try
            {
                var data = this.AMDBMgr.AM_SP_Region_Amount(ResourceId);
                return data.ToList();
            }
            catch (ArgumentException arg)
            {
                throw arg;
            }
            catch (EntityException entitexcepption)
            {
                throw entitexcepption;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IRegionBo Members


        public IQueryable GetRegionCount()
        {
            throw new NotImplementedException();
        }

        #endregion        
        
        #region Get User Region
        public List<E_PATCH> GetUserRegion(int UserId)
        {


            IQueryable<E_PATCH> query = (from ds in AMDBMgr.AM_Resource
                                         join dl in AMDBMgr.AM_ResourcePatchDevelopment on
                                         ds.ResourceId equals dl.ResourceId
                                         join dn in AMDBMgr.E_PATCH on
                                         dl.PatchId equals dn.PATCHID
                                         where ds.ResourceId == UserId && dl.IsActive == true
                                         select dn);
            List<E_PATCH> regionList = query.ToList();
            //List<IQueryable> regionlist = query;

            return regionList;
        }
        #endregion

        #region Get Scheme Regions
        public List<E_PATCH> GetSchemeRegions(List<int?> suburbIds)
        {
            //if (suburbIds.Count == 1)
                
            //{
            //    int? SubrubValue= suburbIds[0];
            //    IQueryable<E_PATCH> query = (from dn in AMDBMgr.E_PATCH
            //                                 join dl in AMDBMgr.P_DEVELOPMENT on
            //                                 dn.PATCHID equals dl.PATCHID


            //                                 where SubrubValue == dl.DEVELOPMENTID
            //                                 select dn);
            //    List<E_PATCH> regionList = query.ToList();

            //    return regionList;
            //}
            //else
            //{

                IQueryable<E_PATCH> query = (from dn in AMDBMgr.E_PATCH
                                             join dl in AMDBMgr.PDR_DEVELOPMENT on
                                             dn.PATCHID equals dl.PATCHID


                                             where suburbIds.Contains(dl.DEVELOPMENTID)
                                             select dn).Distinct();
                List<E_PATCH> regionList = query.ToList();

                return regionList;
          //  }
        }
        #endregion
    }


    
        
}
