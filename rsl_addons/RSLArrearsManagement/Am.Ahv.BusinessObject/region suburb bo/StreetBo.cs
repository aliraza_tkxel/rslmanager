﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;
namespace Am.Ahv.BusinessObject.region_suburb_bo
{
    class StreetBo:base_bo.BaseBO,IStreetBo
    {
        public StreetBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<String> GetStreetBySuburb(int SuburbID)
        {
            List<String> listSuburb = (from ds in AMDBMgr.P__PROPERTY
                                            where ds.DEVELOPMENTID == SuburbID
                                       orderby ds.ADDRESS1 ascending
                                       select ds.ADDRESS1).Distinct().ToList();
            listSuburb.Sort();
            return listSuburb;
        }

        public List<String> GetAllStreets()
        {
            try
            {
                List<String> listSuburb = (from ds in AMDBMgr.P__PROPERTY
                                                orderby ds.ADDRESS1 ascending
                                                select ds.ADDRESS1).Distinct().ToList();
                listSuburb.Sort();
                return listSuburb;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable GetStreetList(int resourceId)
        {
            throw new NotImplementedException();
        }

        P__PROPERTY BoInterface.basebo.IBaseBO<P__PROPERTY, int>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        List<P__PROPERTY> BoInterface.basebo.IBaseBO<P__PROPERTY, int>.GetAll()
        {
            throw new NotImplementedException();
        }

        public P__PROPERTY AddNew(P__PROPERTY obj)
        {
            throw new NotImplementedException();
        }

        public P__PROPERTY Update(P__PROPERTY obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(P__PROPERTY obj)
        {
            throw new NotImplementedException();
        }


        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        

        //#region get Suburb By Region

        //public List<Am.Ahv.Entities.P_DEVELOPMENT> GetSuburbByRegion(int RegionID)
        //{
        //    try
        //    {
        //        IQueryable<Am.Ahv.Entities.P_DEVELOPMENT> pquery = from pq in this.AMDBMgr.P_DEVELOPMENT 
        //                                                           where pq.PATCHID == RegionID 
        //                                                           orderby pq.DEVELOPMENTNAME ascending
        //                                                           select pq;
        //        List<Am.Ahv.Entities.P_DEVELOPMENT> suburblist = pquery.ToList();
        //        if (suburblist.Count > 0)
        //        {
        //            return suburblist;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion

        //#region"Get All Suburbs"

        //public List<P_DEVELOPMENT> GetAllSuburbs()
        //{
        //    try
        //    {
        //        List<P_DEVELOPMENT> listSuburb = (from ds in AMDBMgr.P_DEVELOPMENT
        //                                          orderby ds.DEVELOPMENTNAME ascending
        //                                          select ds).ToList();
        //        return listSuburb;
        //    }
        //    catch (EntityException ee)
        //    {
        //        throw ee;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion

        //#region ISuburbBo Members


        //public List<Entities.P_DEVELOPMENT> GetSuburbByIds(List<Entities.AM_ResourcePatchDevelopment> AreasList)
        //{
        //    try
        //    {
        //        List<int> SuburbList = new List<int>();
        //        foreach (AM_ResourcePatchDevelopment item in AreasList)
        //        {
        //            if (item.DevelopmentId != null)
        //            {
        //                SuburbList.Add(item.DevelopmentId.Value);
        //            }
        //        }
        //        using (this.AMDBMgr = new TKXEL_RSLManager_UKEntities())
        //        {
        //            IQueryable<P_DEVELOPMENT> pquery = from suburb in this.AMDBMgr.P_DEVELOPMENT where SuburbList.Contains(suburb.DEVELOPMENTID) select suburb;
        //            List<P_DEVELOPMENT> suburblist = pquery.ToList();
        //            if (suburblist != null && suburblist.Count > 0)
        //            {
        //                return suburblist;
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }


        //}

        //#endregion

        //public IQueryable GetSuburbsList(int resourceid)
        //{
        //    try
        //    {
        //        var suburbList = (from ds in AMDBMgr.AM_ResourcePatchDevelopment
        //                          join t in AMDBMgr.P_DEVELOPMENT on ds.DevelopmentId equals t.DEVELOPMENTID
        //                          where ds.ResourceId == resourceid && ds.IsActive == true
        //                          orderby t.DEVELOPMENTID
        //                          select new
        //                          {
        //                              t.DEVELOPMENTID,
        //                              t.DEVELOPMENTNAME
        //                          }).Distinct();

        //        return suburbList;
        //    }
        //    catch (NullReferenceException nullException)
        //    {
        //        throw nullException;
        //    }
        //    catch (EntitySqlException entitySqlException)
        //    {
        //        throw entitySqlException;
        //    }
        //    catch (EntityException entityException)
        //    {
        //        throw entityException;
        //    }
        //    catch (Exception exception)
        //    {
        //        throw exception;
        //    }
        //}


       
    }
}
