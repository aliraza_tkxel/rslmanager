﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.BoInterface;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BusinessObject.base_bo;
namespace Am.Ahv.BusinessObject.user_bo
{
    public class UserBo : BaseBO, IUserBo
    {
        public UserBo()
        {
            try
            {
                this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #region IBaseBO<E__EMPLOYEE,int> Members

        public E__EMPLOYEE GetById(int id)
        {
            IQueryable<E__EMPLOYEE> Employee = from emp in this.AMDBMgr.E__EMPLOYEE where emp.EMPLOYEEID == id select emp;
            List<E__EMPLOYEE> Emplist = Employee.ToList();
            if (Emplist.Count > 0 && Emplist != null)
            {
                return Emplist.First();
            }
            else
            {
                return null;
            }

        }

        public List<E__EMPLOYEE> GetAll()
        {
            throw new NotImplementedException();
        }

        public E__EMPLOYEE AddNew(E__EMPLOYEE obj)
        {
            throw new NotImplementedException();
        }

        public E__EMPLOYEE Update(E__EMPLOYEE obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(E__EMPLOYEE obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Employee By Name"

        public E__EMPLOYEE GetEmployeeByName(string Name)
        {
            try
            {
                IQueryable<E__EMPLOYEE> empquer = from emp in AMDBMgr.E__EMPLOYEE where emp.FIRSTNAME == Name select emp;
                List<E__EMPLOYEE> emplist = empquer.ToList();
                if (emplist.Count > 0)
                {
                    return emplist[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region"get Employee List"

        List<E__EMPLOYEE> IUserBo.GetEmployeeList(string Name)
        {
            try
            {
                List<E__EMPLOYEE> emplist = null;
                if (Name.Contains(' '))
                {
                    string[] str = Name.Split(' ');

                    //// Start - Changes By Aamir Waheed May 21,2013
                    //// To handle names with multiple spaces.
                    //int count = str.Count();
                    //if (count > 2)
                    //{
                    //    str[1] = Name.Substring(Name.IndexOf(' ') + 1);
                    //}

                    //if (str.Count() >= 2)
                    // End - Changes By Aamir Waheed May 21,2013

                    if (str.Count() == 2)
                    
                    {
                        string str1 = str[0];
                        string str2 = str[1];
                        IQueryable<E__EMPLOYEE> empquer = from emp in AMDBMgr.E__EMPLOYEE
                                                          join ac in AMDBMgr.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                                          where (emp.FIRSTNAME.Contains(str1) || emp.LASTNAME.Contains(str2)) && ac.ACTIVE == 1
                                                          select emp;
                        emplist = empquer.ToList();
                    }
                    else
                    {
                        string str1 = str[0];
                        IQueryable<E__EMPLOYEE> empquer = from emp in AMDBMgr.E__EMPLOYEE
                                                          join ac in AMDBMgr.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                                          where (emp.FIRSTNAME.Contains(str1) || emp.LASTNAME.Contains(str1)) && ac.ACTIVE == 1
                                                          select emp;
                        emplist = empquer.ToList();
                    }


                }
                else
                {
                    IQueryable<E__EMPLOYEE> empquer = from emp in AMDBMgr.E__EMPLOYEE
                                                      join ac in AMDBMgr.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                                      where (emp.FIRSTNAME.Contains(Name) || emp.LASTNAME.Contains(Name)) && ac.ACTIVE == 1
                                                      select emp;
                    emplist = empquer.ToList();
                }
                if (emplist.Count > 0)
                {
                    return emplist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"get user By Team Id"

        public IQueryable GetUserByTeamID(int TeamID)
        {
            try
            {
                var query = from e in this.AMDBMgr.E__EMPLOYEE
                            join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                            join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                            where t.TEAMID == TeamID
                            select new
                            {
                                EMPLOYEEID = (Int32?)e.EMPLOYEEID,
                                FIRSTName = e.FIRSTNAME
                            };
                return query;
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region"get Employee BY Exact Name"

        public E__EMPLOYEE GetEmployeeByExactName(string Name)
        {
            try
            {
                List<E__EMPLOYEE> lq = null;
                if (Name.Contains(' '))
                {
                    string[] str = Name.Split(' ');

                    // Start - Changes By Aamir Waheed May 21,2013
                    // To handle names with multiple spaces.
                    int count = str.Count();
                    if (count > 2)
                    {
                        str[1] = Name.Substring(Name.IndexOf(' ') + 1);
                    }
                                        
                    // if (str.Count() == 2)
                    if (str.Count() >= 2)
                    // End - Changes By Aamir Waheed May 21,2013                    
                    {
                        string str1 = str[0];
                        string str2 = str[1];

                        // Start - Changes By Aamir Waheed May 21,2013
                        // IQueryable<E__EMPLOYEE> equery = from emp in this.AMDBMgr.E__EMPLOYEE
                        //                                 where emp.FIRSTNAME == str1 && emp.LASTNAME == str2
                        //                                 select emp;

                        IQueryable<E__EMPLOYEE> equery = from emp in this.AMDBMgr.E__EMPLOYEE
                                                         join ac in this.AMDBMgr.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                                         where emp.FIRSTNAME == str1 && emp.LASTNAME == str2 && ac.ACTIVE == 1
                                                         select emp;
                        // End - Changes By Aamir Waheed May 21,2013

                        lq = equery.ToList();
                    }
                    else if (str.Count() == 1)
                    {
                        string str1 = str[0];

                        // Start - Changes By Aamir Waheed May 21,2013
                        // IQueryable<E__EMPLOYEE> equery = from emp in this.AMDBMgr.E__EMPLOYEE
                        //                                 where emp.FIRSTNAME == str1
                        //                                 select emp;

                        IQueryable<E__EMPLOYEE> equery = from emp in this.AMDBMgr.E__EMPLOYEE
                                                         join ac in this.AMDBMgr.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                                         where emp.FIRSTNAME == str1 && ac.ACTIVE == 1
                                                         select emp;

                        // End - Changes By Aamir Waheed May 21,2013

                        lq = equery.ToList();
                    }
                }
                else
                {
                    IQueryable<E__EMPLOYEE> equery = from emp in this.AMDBMgr.E__EMPLOYEE
                                                     where emp.FIRSTNAME == Name
                                                     select emp;
                    lq = equery.ToList();
                }

                if (lq == null)
                {
                    return null;
                }
                if (lq.Count > 0)
                {
                    return lq[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"get User By Team Name"

        E__EMPLOYEE IUserBo.GetUserByTeamName(string Name, int TeamID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> equery = from e in this.AMDBMgr.E__EMPLOYEE
                                                 join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                                                 join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                                                 where t.TEAMID == TeamID && e.FIRSTNAME == Name
                                                 select e;
                List<E__EMPLOYEE> elist = equery.ToList();
                if (elist.Count > 0)
                {
                    return elist.First();
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region"Get Full Narrowed Search"

        public E__EMPLOYEE GetFullNarrowedSearch(string Name, int Type, int TeamID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> equery = from e in this.AMDBMgr.E__EMPLOYEE
                                                 join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                                                 join r in AMDBMgr.AM_Resource on e.EMPLOYEEID equals r.EmployeeId
                                                 join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                                                 where t.TEAMID == TeamID && e.FIRSTNAME == Name && r.AM_LookupCode.LookupCodeId == Type
                                                 select e;
                List<E__EMPLOYEE> elist = equery.ToList();
                if (elist.Count > 0)
                {
                    return elist[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region"Get Team Members"

        public List<E__EMPLOYEE> GetTeamMembers(int TeamID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> Empquery = from e in this.AMDBMgr.E__EMPLOYEE
                                                   join ac in this.AMDBMgr.AC_LOGINS on e.EMPLOYEEID equals ac.EMPLOYEEID
                                                   join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                                                   join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                                                   where t.TEAMID == TeamID && ac.ACTIVE == 1
                                                   select e;

                List<E__EMPLOYEE> Emploist = Empquery.ToList();
                if (Emploist.Count > 0)
                {
                    return Emploist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region"Get Support Workers"

        public List<E__EMPLOYEE> GetSupportWorkers(int TeamID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> Empquery = from e in this.AMDBMgr.E__EMPLOYEE
                                                   join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                                                   join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                                                   where t.TEAMID == TeamID && j.JOBTITLE.Contains("Support Worker")
                                                   select e;

                List<E__EMPLOYEE> Emploist = Empquery.ToList();
                if (Emploist.Count > 0)
                {
                    return Emploist;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region"Get Name By Id"

        public string GetNameById(int userID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> empwuery = from emp in this.AMDBMgr.E__EMPLOYEE where emp.EMPLOYEEID == userID select emp;
                List<E__EMPLOYEE> Emplist = empwuery.ToList();
                if (Emplist.Count > 0)
                {
                    return Emplist[0].FIRSTNAME + " " + Emplist[0].LASTNAME;

                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region""

        public string GetEmployeeNameByUserId(int userID)
        {
            try
            {
                IQueryable<E__EMPLOYEE> empwuery = from emp in this.AMDBMgr.E__EMPLOYEE where emp.EMPLOYEEID == userID select emp;
                List<E__EMPLOYEE> Emplist = empwuery.ToList();
                if (Emplist.Count > 0)
                {
                    return Emplist[0].FIRSTNAME + "," + Emplist[0].LASTNAME;

                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Single Employee"

        public IQueryable GetSingleEmployee(int Id)
        {
            try
            {
                var query = from ds in AMDBMgr.AM_Resource
                            join t in AMDBMgr.E__EMPLOYEE on ds.EmployeeId equals t.EMPLOYEEID
                            where ds.ResourceId == Id && ds.IsActive == true
                            select new
                            {
                                ds.ResourceId,
                                EmployeeName = (t.FIRSTNAME ?? "") + " " + (t.LASTNAME ?? "")
                            };
                return query.AsQueryable();
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Employee Name and Team"

        public string GetEmployeeNameAndTeam(int empId)
        {
            try
            {
                var resultSet = from emp in AMDBMgr.E__EMPLOYEE
                                join jd in AMDBMgr.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                                join t in AMDBMgr.E_TEAM on jd.TEAM equals t.TEAMID
                                where emp.EMPLOYEEID == empId
                                orderby emp.EMPLOYEEID ascending
                                select new
                                {
                                    EmployeeName = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    t.TEAMID
                                };
                string str = string.Empty;
                if (resultSet != null)
                {
                    foreach (var t in resultSet)
                    {
                        str = t.EmployeeName + ";" + t.TEAMID;
                    }
                }
                return str;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
