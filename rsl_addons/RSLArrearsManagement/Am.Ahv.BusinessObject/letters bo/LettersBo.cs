using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.letters_bo;
using Am.Ahv.Entities;
using System.Data;
using System.Linq.Expressions;
using LinqKit;
using System.Transactions;
using Am.Ahv.BusinessObject.resource_bo;
using Am.Ahv.BusinessObject.action_bo;
using System.Data.Objects;

namespace Am.Ahv.BusinessObject.letters_bo
{
    class LettersBo : BaseBO, ILettersBo
    {
        public LettersBo()
        {
            this.AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }
        #region"Search Standard Letters"

        public List<AM_StandardLetters> SearchStandardLetters(int StatusId, int ActionId, string Title, string Code)
        {
            try
            {
                using (this.AMDBMgr = new TKXEL_RSLManager_UKEntities())
                {
                    Expression<Func<AM_StandardLetters, bool>> predicate = PredicateBuilder.True<AM_StandardLetters>();
                    if (!string.IsNullOrEmpty(Title))
                    {
                       // predicate = predicate.And(sl => sl.Title.Equals(Title));
                        predicate = predicate.And(sl => sl.Title.Contains(Title));
                    }
                    if (!string.IsNullOrEmpty(Code))
                    {
                        predicate = predicate.And(sl => sl.Code.Equals(Code));
                    }
                    if (CheckDefault(ActionId))
                    {
                        predicate = predicate.And(sl => sl.ActionId == ActionId);
                    }
                    if (CheckDefault(StatusId))
                    {
                        predicate = predicate.And(sl => sl.StatusId == StatusId);
                    }
                    predicate = predicate.And(sl => sl.IsActive == true);
                    List<AM_StandardLetters> StandardLetterList = new List<AM_StandardLetters>();
                    StandardLetterList = this.AMDBMgr.AM_StandardLetters.AsExpandable().Where(predicate).ToList();
                    return LoadReference(StandardLetterList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region IBaseBO<AM_StandardLetters,int> Members

        public Entities.AM_StandardLetters GetById(int id)
        {
            try
            {
                IQueryable<AM_StandardLetters> query = from ds in AMDBMgr.AM_StandardLetters
                                                       where ds.StandardLetterId == id
                                                       select ds;
                List<AM_StandardLetters> list = query.ToList();
                if (list != null && list.Count > 0)
                {
                    return list.First();

                }
                return null;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Entities.AM_StandardLetters> GetAll()
        {
            throw new NotImplementedException();
        }

        public Entities.AM_StandardLetters AddNew(AM_StandardLetters obj)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AMDBMgr.AddToAM_StandardLetters(obj);
                    AMDBMgr.SaveChanges();

                    AM_ActionAndStandardLetters actionLetters = new AM_ActionAndStandardLetters();
                    ActionBO actionBo = new ActionBO();
                    actionLetters.ActionId = obj.ActionId;
                    actionLetters.StandardLetterId = obj.StandardLetterId;
                    actionLetters.ActionHistoryId = actionBo.GetActionHistoryByActionId(obj.ActionId);
                    actionLetters.IsActive = true;
                    AMDBMgr.AddToAM_ActionAndStandardLetters(actionLetters);
                    AMDBMgr.SaveChanges();

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                }
                return obj;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Entities.AM_StandardLetters Update(AM_StandardLetters obj)
        {
            try
            {
                IQueryable<AM_StandardLetters> query = from ds in AMDBMgr.AM_StandardLetters
                                                       where ds.StandardLetterId == obj.StandardLetterId
                                                       select ds;
                AM_StandardLetters sl = query.ToList().First();
                if (sl != null)
                {
                    sl.ActionId = obj.ActionId;
                    sl.Body = obj.Body;
                    sl.Code = obj.Code;
                    sl.ModifiedBy = obj.ModifiedBy;
                    sl.ModifiedDate = obj.ModifiedDate;
                    sl.StatusId = obj.StatusId;
                    sl.Title = obj.Title;
                    sl.IsPrinted = obj.IsPrinted;
                    if (obj.SignOffLookupCode != null)
                    {
                        sl.SignOffLookupCode = obj.SignOffLookupCode;
                    }
                    else
                    {
                        sl.SignOffLookupCode = null;
                    }
                    if (obj.TeamId != null)
                    {
                        sl.TeamId = obj.TeamId;
                    }
                    else
                    {
                        sl.TeamId = null;
                    }
                    if (obj.FromResourceId != null)
                    {
                        sl.FromResourceId = obj.FromResourceId.Value;
                    }
                    else
                    {
                        sl.FromResourceId = null;
                    }
                    sl.IsActive = obj.IsActive;
                    if (obj.PersonalizationLookupCode != null)
                    {
                        sl.PersonalizationLookupCode = obj.PersonalizationLookupCode;
                    }
                    AMDBMgr.SaveChanges();
                    return obj;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public Entities.AM_StandardLetters_ThirdPartyContacts getThirdPartyContactDetails(int thirdPartyContactId)
        {
            try
            {
                IQueryable<AM_StandardLetters_ThirdPartyContacts> query = from ds in AMDBMgr.AM_StandardLetters_ThirdPartyContacts
                                                       where ds.ThirdPartyContactId == thirdPartyContactId
                                                       select ds;
                if (query.ToList().Count > 0)
                {
                    AM_StandardLetters_ThirdPartyContacts sl = query.ToList().First();
                    return sl;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(Entities.AM_StandardLetters obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            try
            {
                bool result = false;
                using (TransactionScope tran = new TransactionScope())
                {
                    IQueryable<AM_StandardLetters> slquery = from sl in this.AMDBMgr.AM_StandardLetters where sl.StandardLetterId == id select sl;
                    AM_StandardLetters Sletter = slquery.ToList().First();
                    if (Sletter != null)
                    {
                        Sletter.IsActive = false;
                        this.AMDBMgr.SaveChanges();
                        result = true;
                    }

                    List<AM_ActionAndStandardLetters> listActions = (from ds in AMDBMgr.AM_ActionAndStandardLetters
                                                                     where ds.StandardLetterId == id && ds.IsActive == true
                                                                     select ds).ToList();
                    if (listActions != null || listActions.Count == 0)
                    {
                        for (int i = 0; i < listActions.Count; i++)
                        {
                            listActions[i].IsActive = false;
                            AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(listActions[i]);
                        }
                    }
                    result = true;
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                }
                return result;
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Personalization"

        public List<AM_LookupCode> GetPersonalization()
        {
            try
            {
                List<AM_LookupCode> code = (from ds in AMDBMgr.AM_LookupCode
                                            join t1 in AMDBMgr.AM_LookupType on ds.LookupTypeId equals t1.LookupTypeId
                                            where t1.TypeName.Equals("Personalization") && ds.IsActive == true
                                            select ds).ToList<AM_LookupCode>();

                //IQueryable<AM_LookupCode> lquery = from lq in this.AMDBMgr.AM_LookupCode where lq.AM_LookupType.TypeName.Equals("Personalization") select lq;
                //List<AM_LookupCode> code = lquery.ToList();

                return code;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Sign Off"

        public List<AM_LookupCode> GetSignOff()
        {
            try
            {
                List<AM_LookupCode> code = (from ds in AMDBMgr.AM_LookupCode
                                            join t1 in AMDBMgr.AM_LookupType on ds.LookupTypeId equals t1.LookupTypeId
                                            where t1.TypeName == "SignOff" && ds.IsActive == true
                                            select ds).ToList<AM_LookupCode>();
                return code;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Default"

        private bool CheckDefault(int Value)
        {
            if (Value == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"Load Reference"

        private List<AM_StandardLetters> LoadReference(List<AM_StandardLetters> StandardLetterList)
        {
            foreach (AM_StandardLetters item in StandardLetterList)
            {
                item.AM_ActionReference.Load();
                item.AM_StatusReference.Load();
                item.AM_Resource1Reference.Load();
                item.AM_ResourceReference.Load();
            }
            return StandardLetterList;
        }
        #endregion

        #region"Get Team Members"

        public DataTable GetTeamMembers(int teamId)
        {
            try
            {
                var query = from e in this.AMDBMgr.E__EMPLOYEE
                            join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                            join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                            join resource in AMDBMgr.AM_Resource on e.EMPLOYEEID equals resource.EmployeeId
                            where t.TEAMID == teamId && resource.IsActive == true
                            select new
                            {
                                EmployeeName = ((e.FIRSTNAME ?? "") + " " + (e.LASTNAME ?? "")),
                                ResourceId = resource.ResourceId
                            };
                DataTable dt = new DataTable();
                dt.Columns.Add("EmployeeName");
                dt.Columns.Add("ResourceId");
                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["EmployeeName"] = t.EmployeeName;
                    dr["ResourceId"] = t.ResourceId;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public DataTable GetTeamEmployees(int teamId)
        {
            try
            {
                var query = from e in this.AMDBMgr.E__EMPLOYEE
                            join ac in AMDBMgr.AC_LOGINS on e.EMPLOYEEID equals ac.EMPLOYEEID
                            join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                            join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                            // join resource in AMDBMgr.AM_Resource on e.EMPLOYEEID equals resource.EmployeeId
                            where t.TEAMID == teamId && ac.ACTIVE == 1
                            select new
                            {
                                EmployeeName = ((e.FIRSTNAME ?? "") + " " + (e.LASTNAME ?? "")),
                                ResourceId = e.EMPLOYEEID // resource.ResourceId
                            };
                DataTable dt = new DataTable();
                dt.Columns.Add("EmployeeName");
                dt.Columns.Add("ResourceId");
                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["EmployeeName"] = t.EmployeeName;
                    dr["ResourceId"] = t.ResourceId;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get All Team Members"

        public DataTable GetAllTeamMembers(int teamId)
        {
            try
            {
                var query = from e in this.AMDBMgr.E__EMPLOYEE
                            join ac in AMDBMgr.AC_LOGINS on e.EMPLOYEEID equals ac.EMPLOYEEID
                            join j in this.AMDBMgr.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                            join t in this.AMDBMgr.E_TEAM on new { TEAMID = (Int32)j.TEAM } equals new { TEAMID = t.TEAMID }
                            
                            where t.TEAMID == teamId && ac.ACTIVE == 1
                            select new
                            {
                                EmployeeName = ((e.FIRSTNAME ?? "") + " " + (e.LASTNAME ?? "")),
                                EmployeeId =e.EMPLOYEEID
                            };
                DataTable dt = new DataTable();
                dt.Columns.Add("EmployeeName");
                dt.Columns.Add("EmployeeId");
                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["EmployeeName"] = t.EmployeeName;
                    dr["EmployeeId"] = t.EmployeeId;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Customer Info"

        public AM_SP_GetCustomerInfoForLetter_Result GetCustomerInfo(int tenantId, int customerId, int contactId = -1)
        {
            try
            {
                var res = AMDBMgr.AM_SP_GetCustomerInfoForLetter(tenantId, customerId, contactId);
                AM_SP_GetCustomerInfoForLetter_Result result = new AM_SP_GetCustomerInfoForLetter_Result();
                foreach (var val in res)
                {
                    result.CustomerName = val.CustomerName;
                    result.CustomerName2 = val.CustomerName2;
                    result.JointTenancyCount = val.JointTenancyCount;
                    result.HOUSENUMBER = val.HOUSENUMBER;
                    result.ADDRESS1 = val.ADDRESS1;
                    result.ADDRESS2 = val.ADDRESS2;
                    result.ADDRESS3 = val.ADDRESS3;
                    result.TOWNCITY = val.TOWNCITY;
                    result.COUNTY = val.COUNTY;
                    result.POSTCODE = val.POSTCODE;

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Employee Name"
        public string GetEmployeeName(int? ResourceId)
        {
            try
            {
                var emp = from Emp in this.AMDBMgr.E__EMPLOYEE
                          join t0 in this.AMDBMgr.AM_Resource on Emp.EMPLOYEEID equals t0.EmployeeId
                          where t0.ResourceId == ResourceId
                          select new
                          {
                              Name = ((Emp.FIRSTNAME ?? "") + " " + (Emp.LASTNAME ?? ""))
                          };
                string EmpName = string.Empty;
                foreach (var t in emp)
                {
                    EmpName = t.Name;
                }

                return EmpName;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
        #region"Activity Checker"
        public string ActivityChecker(string ActionId, string StatusId, string CaseId, string CaseHistoryId)
        {
            try
            {
                int _actionId = Convert.ToInt32(ActionId);
                int _caseId = Convert.ToInt32(CaseId);
                int _statusId = Convert.ToInt32(StatusId);
                int _caseHistory = Convert.ToInt32(CaseHistoryId);
                var actChker = from ds in AMDBMgr.AM_Activity
                               where ds.ActionId == _actionId && ds.CaseId == _caseId
                               && ds.StatusId == _statusId && ds.CaseHistoryId == _caseHistory
                               select ds;
                if (actChker.Count() == 0)
                {
                    return "false";
                }
                else
                {
                    return "true";
                }
            } 
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Team Name"
        public string GetTeamName(int? TeamId)
        {
            try
            {
                IQueryable<string> TeamName = (from Team in this.AMDBMgr.E_TEAM
                                               where Team.TEAMID == TeamId
                                               select Team.TEAMNAME);
                List<string> teamlist = TeamName.ToList();
                if (teamlist.Count > 0)
                {
                    return teamlist[0];
                }
                else
                    return null;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Get Letters By Action Id"

        public List<AM_StandardLetterHistory> GetLettersByActionId(int actionId)
        {
            try
            {
                IQueryable<AM_StandardLetters> query = (from ds in AMDBMgr.AM_ActionAndStandardLetters
                                                        join t in AMDBMgr.AM_StandardLetters on ds.StandardLetterId equals t.StandardLetterId
                                                        where ds.ActionId == actionId && ds.IsActive == true
                                                        select t);
                if (query != null)
                {

                    List<AM_StandardLetters> list = query.ToList<AM_StandardLetters>();
                    List<AM_StandardLetterHistory> historyList = new List<AM_StandardLetterHistory>();
                    foreach (AM_StandardLetters sl in list)
                    {
                        historyList.Add(this.GetSLetterHistoryBySLetterID(sl.StandardLetterId));   
                    }
                    return historyList;
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetLettersByActionIdDataTable(int actionId)
        {
            try
            {
                var query = (from ds in AMDBMgr.AM_ActionAndStandardLetters
                             join t in AMDBMgr.AM_StandardLetters on ds.StandardLetterId equals t.StandardLetterId
                             where ds.ActionId == actionId && ds.IsActive == true
                             select t);
                DataTable dt = new System.Data.DataTable();
                dt.Columns.Add("StandardletterId");
                dt.Columns.Add("Title");
                foreach (var t in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["StandardletterId"] = t.StandardLetterId;
                    dr["Title"] = t.Title;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region ILettersBo Members

        public string GetLettersNameByAction(int ActionId)
        {
            string lettertitle = string.Empty;
            IQueryable<AM_StandardLetters> slquery = from sl in this.AMDBMgr.AM_StandardLetters where sl.ActionId == ActionId select sl;
            List<AM_StandardLetters> slist = slquery.ToList();
            if (slist != null && slist.Count > 0)
            {

                foreach (AM_StandardLetters item in slist)
                {
                    lettertitle = "" + item.Title;
                }
                return lettertitle;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region"Get All Letters"
        public List<AM_StandardLetters> GetAllLetters()
        {
            try
            {
                List<AM_StandardLetters> letters = (from Letters in this.AMDBMgr.AM_StandardLetters where Letters.IsActive==true select Letters).ToList<AM_StandardLetters>();
                return letters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ILettersBo Members


        public double GetMarketRent(int TenantId)
        {
            try
            {
                decimal value = 0;
                var omv = from tenant in this.AMDBMgr.C_TENANCY
                          join financial in this.AMDBMgr.P_FINANCIAL on tenant.PROPERTYID equals financial.PROPERTYID
                          where tenant.TENANCYID == TenantId && tenant.ENDDATE == null
                          select financial.OMV;
                foreach (var item in omv)
                {
                    if (item != null)
                    {
                        value = item.Value;
                    }
                    else
                    {
                        return 0.0;
                    }
                }
                return Convert.ToDouble(value);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion

        #region"Get Standard Letter By Activity Id"

        public List<AM_ActivityAndStandardLetters> GetStandardLetterByActivityId(int ActivityId)
        {
            try
            {
                List<AM_ActivityAndStandardLetters> letters = (from Letters in this.AMDBMgr.AM_ActivityAndStandardLetters where Letters.ActivityId == ActivityId select Letters).ToList<AM_ActivityAndStandardLetters>();
                return letters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add New Standard Letter"

        public void AddNewStandardLetter(AM_StandardLetters standardLetter, AM_StandardLetterHistory standardLetterHistory)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AMDBMgr.AddToAM_StandardLetters(standardLetter);
                    AMDBMgr.SaveChanges();

                    standardLetterHistory.StandardLetterId = standardLetter.StandardLetterId;
                    AMDBMgr.AddToAM_StandardLetterHistory(standardLetterHistory);
                    AMDBMgr.SaveChanges();

                    AM_ActionAndStandardLetters actionLetters = new AM_ActionAndStandardLetters();
                    ActionBO actionBo = new ActionBO();
                    actionLetters.ActionId = standardLetter.ActionId;
                    actionLetters.StandardLetterId = standardLetter.StandardLetterId;
                    actionLetters.ActionHistoryId = actionBo.GetActionHistoryByActionId(standardLetter.ActionId);
                    actionLetters.StandardLetterHistoryId = standardLetterHistory.StandardLetterHistoryId;
                    actionLetters.IsActive = true;
                    AMDBMgr.AddToAM_ActionAndStandardLetters(actionLetters);
                    AMDBMgr.SaveChanges();

                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                }             
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Standard Letter"

        public void UpdateStandardLetter(AM_StandardLetters standardLetter, AM_StandardLetterHistory standardLetterHistory)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    IQueryable<AM_StandardLetters> query = from ds in AMDBMgr.AM_StandardLetters
                                                           where ds.StandardLetterId == standardLetter.StandardLetterId
                                                           select ds;
                    if (query.ToList().Count > 0)
                    {
                        AM_StandardLetters sl = query.ToList().First();
                        sl.ActionId = standardLetter.ActionId;
                        sl.Body = standardLetter.Body;
                        sl.Code = standardLetter.Code;
                        sl.ModifiedBy = standardLetter.ModifiedBy;
                        sl.ModifiedDate = standardLetter.ModifiedDate;
                        sl.StatusId = standardLetter.StatusId;
                        sl.Title = standardLetter.Title;
                        sl.IsPrinted = standardLetter.IsPrinted;
                        //sl.JointTenantLetterRequired = standardLetter.JointTenantLetterRequired;
                        //if (isCustomerEditView)
                        //{
                        //    sl.AdditionalContactID = standardLetter.AdditionalContactID;
                        //    sl.ThirdPartyContactID = standardLetter.ThirdPartyContactID;
                        //}
                        if (standardLetter.SignOffLookupCode != null)
                        {
                            sl.SignOffLookupCode = standardLetter.SignOffLookupCode;
                        }
                        else
                        {
                            sl.SignOffLookupCode = null;
                        }
                        if (standardLetter.TeamId != null)
                        {
                            sl.TeamId = standardLetter.TeamId;
                        }
                        else
                        {
                            sl.TeamId = null;
                        }
                        if (standardLetter.FromResourceId != null)
                        {
                            sl.FromResourceId = standardLetter.FromResourceId.Value;
                        }
                        else
                        {
                            sl.FromResourceId = null;
                        }
                        sl.IsActive = standardLetter.IsActive;
                        if (standardLetter.PersonalizationLookupCode != null)
                        {
                            sl.PersonalizationLookupCode = standardLetter.PersonalizationLookupCode;
                        }
                        AMDBMgr.SaveChanges();

                        standardLetterHistory.StandardLetterId = standardLetter.StandardLetterId;
                        AMDBMgr.AddToAM_StandardLetterHistory(standardLetterHistory);
                        AMDBMgr.SaveChanges();

                        List<AM_ActionAndStandardLetters> actionSLList = (from ds in AMDBMgr.AM_ActionAndStandardLetters
                                                                              where ds.StandardLetterId == standardLetter.StandardLetterId && ds.StandardLetterHistoryId != null
                                                                              orderby ds.ActionAndStandardLettersId descending
                                                                              select ds).ToList();
                        if (actionSLList != null)
                        {
                            if (actionSLList.Count > 0)
                            {
                                AM_ActionAndStandardLetters actionSL = actionSLList.First();
                                actionSL.ActionId = standardLetter.ActionId;
                                actionSL.StandardLetterHistoryId = standardLetterHistory.StandardLetterHistoryId;
                                AMDBMgr.SaveChanges();
                            }
                        }
                    }
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                }                
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Update Third Party Contact"

        public int UpdateThirdPartyContact(AM_StandardLetters_ThirdPartyContacts contact)
        {
            try
            {
                if (contact.ThirdPartyContactId > 0)
                {
                    using (TransactionScope tran = new TransactionScope())
                    {
                        IQueryable<AM_StandardLetters_ThirdPartyContacts> query = from ds in AMDBMgr.AM_StandardLetters_ThirdPartyContacts
                                                                                  where ds.ThirdPartyContactId == contact.ThirdPartyContactId
                                                                                  select ds;
                        if (query.ToList().Count > 0)
                        {
                            AM_StandardLetters_ThirdPartyContacts sl = query.ToList().First();
                            sl.ContactName = contact.ContactName;
                            sl.Address1 = contact.Address1;
                            sl.Address2 = contact.Address2;
                            sl.Address3 = contact.Address3;
                            sl.TownCity = contact.TownCity;
                            sl.PostCode = contact.PostCode;
                            AMDBMgr.SaveChanges();
                            AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                            tran.Complete();
                        }
                    }
                }
                else
                { 
                    contact.ThirdPartyContactId = AddNewThirdPartyContact(contact); 
                }
                return contact.ThirdPartyContactId;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Add NEW Third Party Contact"

        public int AddNewThirdPartyContact(AM_StandardLetters_ThirdPartyContacts contact)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    AMDBMgr.AddToAM_StandardLetters_ThirdPartyContacts(contact);
                    AMDBMgr.SaveChanges();
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                  }
                return AMDBMgr.AM_StandardLetters_ThirdPartyContacts.Max(u => u.ThirdPartyContactId);
                //return AMDBMgr.AM_StandardLetters_ThirdPartyContacts.Where(ContactDetail => ContactDetail.ContactName == contact.ContactName).Max(ContactDetail => ContactDetail.ThirdPartyContactId);
                
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Latest Standard Letter History"

        public AM_StandardLetterHistory GetLatestStandardLetterHistory(int standardLetterId)
        {
            try
            {
                List<AM_StandardLetterHistory> listHistory = (from ds in AMDBMgr.AM_StandardLetterHistory
                                                                  where ds.StandardLetterId == standardLetterId
                                                                  orderby ds.StandardLetterHistoryId descending                
                                                                  select ds).ToList();

                if (listHistory != null)
                {
                    return listHistory.First();
                }
                else
                {
                    return null;
                }

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add Edited Standard Letter"

        public int AddEditedStandardLetter(AM_StandardLetterHistory slHistory)
        {
            try
            {
                AMDBMgr.AddToAM_StandardLetterHistory(slHistory);
                AMDBMgr.SaveChanges();

                return slHistory.StandardLetterHistoryId;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Standard Letter By History Id"

        public AM_StandardLetterHistory GetStandardLetterHistoryById(int Id)
        {
            try
            {
                List<AM_StandardLetterHistory> slH = (from ds in AMDBMgr.AM_StandardLetterHistory
                                                where ds.StandardLetterHistoryId == Id
                                                select ds).ToList();

                if (slH != null)
                {
                    if (slH.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return slH.First();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Standard Letter Latest History By Letter Id"

        public AM_StandardLetterHistory GetSLetterHistoryBySLetterID(int SLetterID)
        {
            try
            {
                List<AM_StandardLetterHistory> slH = (from ds in AMDBMgr.AM_StandardLetterHistory
                                                      join sl in AMDBMgr.AM_StandardLetters on ds.StandardLetterId equals sl.StandardLetterId
                                                      where ds.StandardLetterId == SLetterID && ds.Body == sl.Body
                                                      select ds).ToList();

                if (slH != null)
                {
                    if (slH.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return slH.First();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get GetStandard Letter By HistoryID"

        public AM_StandardLetters GetSLetterByHistoryID(int LHistoryID)
        {
            try
            {
                var sl = (from ds in AMDBMgr.AM_StandardLetters
                                         join lh in AMDBMgr.AM_StandardLetterHistory on ds.StandardLetterId equals lh.StandardLetterId
                                                      where lh.StandardLetterHistoryId == LHistoryID
                                                      select ds);

                if (sl != null)
                {
                    if (sl.Count() == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return sl.First();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Case Standard Letter History Id"

        public int GetCaseStandardLetterHistoryId(int stlId, int caseId)
        {
            try
            {
                List<AM_CaseAndStandardLetter> list = (from ds in AMDBMgr.AM_CaseAndStandardLetter
                                                       where ds.StandardLetterHistoryId == stlId && ds.CaseId == caseId
                                                       orderby ds.StandardLetterHistoryId descending
                                                       select ds
                                                           ).ToList();
                if (list != null && list[0].StandardLetterHistoryId != null)
                {
                    if (list.Count > 0)
                    {
                        AM_CaseAndStandardLetter letter = list.First();
                        return letter.StandardLetterHistoryId.Value;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        
    }
}
