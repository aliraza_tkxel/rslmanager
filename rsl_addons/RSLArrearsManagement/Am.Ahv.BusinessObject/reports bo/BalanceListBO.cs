﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using System.Data;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.reports_bo;

namespace Am.Ahv.BusinessObject.reports_bo
{
    public class BalanceListBO: BaseBO, IBalanceListBO
    {
        public BalanceListBO()
        {
            AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region"Get Balance List"

        public List<AM_SP_GETRENTBALANCELIST_Result> GetBalanceList(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, int index, int pageSize, string sortExpression, string sortDir,string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                List<AM_SP_GETRENTBALANCELIST_Result> list = new List<AM_SP_GETRENTBALANCELIST_Result>();

                list = AMDBMgr.AM_SP_GETRENTBALANCELIST(_caseOwnedBy, _customerStatus, _suburbId, sortExpression + " " + sortDir, _regionId, _assetType, rentBalanceFrom, rentBalanceTo, pageSize, index).ToList(); 
               //     (index, pageSize, _caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, sortExpression, sortDir).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Balance List Count"

        public int GetBalanceListCount(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_GetBalanceListCount(_caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, rentBalanceFrom, rentBalanceTo);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Historical Balance List"

        public List<AM_SP_GetHistoricalBalanceList_Result> GetHistoricalBalanceList(int caseOwnedBy,int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir)
        {
            try
            {
                List<AM_SP_GetHistoricalBalanceList_Result> list = new List<AM_SP_GetHistoricalBalanceList_Result>();
                list = AMDBMgr.AM_SP_GetHistoricalBalanceList(startIndex, endIndex, caseOwnedBy, region, suburb, customerStatus, assetType, months, years, sortExpression, sortDir).ToList();
                return list;
                //List<AM_SP_GetHistoricalBalanceList_Result> List = AMDBMgr.AM_SP_GetHistoricalBalanceList(startIndex, endIndex, caseOwnedBy,region, suburb,customerStatus,assetType,months,years, sortExpression, sortDir).ToList<AM_SP_GetHistoricalBalanceList_Result>();
                //return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Historical Balance List Count"

        public int GetHistoricalBalanceListCount(int caseOwnedBy,int region,int suburb,int customerStatus,int assetStatus,int months, int years)
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_GetHistoricalBalanceListCount(caseOwnedBy,region,suburb,customerStatus,assetStatus,months, years);
                int i = 0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Notice To Vacate Cases List"

        public List<AM_SP_GetNoticeToVacateCases_Result> GetNoticeToVacateCasesList()
        {
            try
            {
                List<AM_SP_GetNoticeToVacateCases_Result> List = AMDBMgr.AM_SP_GetNoticeToVacateCases().ToList<AM_SP_GetNoticeToVacateCases_Result>();
                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Find Historical Balance List"

        public List<AM_SP_FindHistoricalBalanceList_Result> FindHistoricalBalanceList(int DbIndex,int PageSize)
        {
            try
            {
                List<AM_SP_FindHistoricalBalanceList_Result> List = AMDBMgr.AM_SP_FindHistoricalBalanceList(DbIndex,PageSize).ToList<AM_SP_FindHistoricalBalanceList_Result>();
                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Find Historical Balance List Count"

        public int FindHistoricalBalanceListCount()
        {
            try
            {
                var recordsCount = AMDBMgr.AM_SP_FindHistoricalBalanceListCount();
                int i=0;
                foreach (var t in recordsCount)
                {
                    i = t.Value;
                }
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public bool CheckHistoricalTable(int CustomerId, int TenancyId, string month, int year)
        {
            try
            {
                var result = (from ds in AMDBMgr.AM_HistoricalBalanceList
                              where (ds.CustomerId == CustomerId) && (ds.TenancyId == TenancyId) && (ds.month == month && ds.year == year)
                              select ds);

                if (result.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public AM_HistoricalBalanceList AddHistoricalBalanceList(AM_HistoricalBalanceList obj)
        {
            try
            {
                this.AMDBMgr.AddToAM_HistoricalBalanceList(obj);
                this.AMDBMgr.SaveChanges();
                AMDBMgr.Detach(obj);
                return obj;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            //throw new NotImplementedException();


        }

        #region"Get Balance List Export to excel"

        public List<AM_SP_GetBalanceListExportToExcel_Result > GetBalanceListExportToExcel(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string sortExpression, string sortDir)
        {
            try
            {
                List<AM_SP_GetBalanceListExportToExcel_Result> list = new List<AM_SP_GetBalanceListExportToExcel_Result>();
                list = AMDBMgr.AM_SP_GetBalanceListExportToExcel(_customerStatus, _suburbId, sortExpression + " " + sortDir, _regionId, _assetType).ToList(); 
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Historical Balance List"

        public List<AM_SP_GetHistoricalBalanceListExportToExcel_Result> GetHistoricalBalanceListExportToExcel(int caseOwnedBy, int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir)
        {
            try
            {
                List<AM_SP_GetHistoricalBalanceListExportToExcel_Result> list = new List<AM_SP_GetHistoricalBalanceListExportToExcel_Result>();
                list = AMDBMgr.AM_SP_GetHistoricalBalanceListExportToExcel(startIndex, endIndex, caseOwnedBy, region, suburb, customerStatus, assetType, months, years, sortExpression, sortDir).ToList();
                return list;
                //List<AM_SP_GetHistoricalBalanceList_Result> List = AMDBMgr.AM_SP_GetHistoricalBalanceList(startIndex, endIndex, caseOwnedBy,region, suburb,customerStatus,assetType,months,years, sortExpression, sortDir).ToList<AM_SP_GetHistoricalBalanceList_Result>();
                //return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Get Anticipated HB Balance "

        public AM_SP_GetAnticipatedHBBalance_Result GetAnticipatedHBBalance(string postcode, int caseOwnedBy, int region, int suburb)
        {
            try
            {
                 
                List<AM_SP_GetAnticipatedHBBalance_Result> balanceResult = new List<AM_SP_GetAnticipatedHBBalance_Result>();
                balanceResult = AMDBMgr.AM_SP_GetAnticipatedHBBalance(caseOwnedBy, region, suburb, postcode).ToList();
                return balanceResult.First();

                //List<AM_SP_GetHistoricalBalanceList_Result> List = AMDBMgr.AM_SP_GetHistoricalBalanceList(startIndex, endIndex, caseOwnedBy,region, suburb,customerStatus,assetType,months,years, sortExpression, sortDir).ToList<AM_SP_GetHistoricalBalanceList_Result>();
                //return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
