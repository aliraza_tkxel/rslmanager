﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.BoInterface.reports_bo;
using Am.Ahv.Entities;
using System.Data;

namespace Am.Ahv.BusinessObject.reports_bo
{
    public class StatusAndActionReportBO : BaseBO, IStatusAndActionReportBO
    {
        public StatusAndActionReportBO()
        {
            AMDBMgr = new TKXEL_RSLManager_UKEntities();
        }

        #region"Get Status And Action Report"

        public IQueryable GetStatusAndActionReport(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize)
        {
            try
            {
                var query = AMDBMgr.AM_SP_GetStatusActionReport(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize);
                return query.AsQueryable();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Status And Action Report Count"

        public int GetStatusAndActionReportCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                int count = AMDBMgr.AM_SP_GetStatusActionReportCount(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag).Count();
                
                return count;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
    }
}
