﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.activity_bo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;
using System.Data;
using System.Transactions;
using Am.Ahv.Utilities.utitility_classes;
using System.Configuration.Assemblies;
using System.IO;
using Am.Ahv.Utilities.constants;
using System.Data.Objects;

namespace Am.Ahv.BusinessObject.activity_bo
{
    public class ActivityBO : BaseBO, IActivityBO
    {
        public ActivityBO()
        {
            try
            {
                AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region IBaseBO<AM_Activity,int> Members

        public AM_Activity GetById(int id)
        {
            try
            {
                IQueryable<AM_Activity> aquery = from ac in AMDBMgr.AM_Activity where ac.ActivityId == id select ac;
                List<AM_Activity> activityObj = aquery.ToList();
                if (activityObj.Count > 0)
                {

                    return LoadReference(activityObj)[0];
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<AM_Activity> GetAll()
        {
            throw new NotImplementedException();
        }

        public AM_Activity AddNew(AM_Activity obj)
        {
            try
            {
              AMDBMgr.AddToAM_Activity(obj);
              AMDBMgr.SaveChanges();
              return obj;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AM_Activity Update(AM_Activity obj)
        {
            IQueryable<AM_Activity> aquery = from ac in AMDBMgr.AM_Activity where ac.ActivityId == obj.ActivityId select ac;
            AM_Activity activitylist = aquery.ToList().First();
            activitylist.Title = obj.Title;
            activitylist.Notes = obj.Notes;
            activitylist.Reason = obj.Reason;
            activitylist.OutComeLookupId = obj.OutComeLookupId;
            activitylist.ActivityLookupId = obj.ActivityLookupId;
            AMDBMgr.SaveChanges();
            return activitylist;

        }

        public bool Delete(AM_Activity obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Get Case Activities"

        public IQueryable GetCaseActivities(int caseId)
        {
            try
            {
                var result = AMDBMgr.AM_SP_ViewActivities(caseId);
                return result.AsQueryable();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_Activity> GetActivitiesReport(int caseId)
        {
            try
            {
                IQueryable<AM_Activity> activityReportQuery = from p in this.AMDBMgr.AM_Activity where p.CaseId == caseId select p;
                List<AM_Activity> ActivityList = activityReportQuery.ToList();
                if (ActivityList.Count > 0 && ActivityList != null)
                {
                    return LoadReference(ActivityList);
                }
                else
                {
                    return ActivityList;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private List<AM_Activity> LoadReference(List<AM_Activity> ActivityList)
        {
            try
            {
                foreach (AM_Activity item in ActivityList)
                {
                    item.AM_LookupCodeReference.Load();
                    item.AM_LookupCode1Reference.Load();
                    item.AM_StatusReference.Load();
                    item.AM_ResourceReference.Load();
                }
                return ActivityList;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }


        #endregion

        #region IActivityBO Members


        public List<AM_Activity> GetActivitiesByCaseId(int CaseId)
        {
            try
            {
                IQueryable<AM_Activity> activityquery = from ac in this.AMDBMgr.AM_Activity where ac.CaseId == CaseId orderby ac.RecordedDate descending select ac;
                List<AM_Activity> ActivityList = activityquery.ToList();
                if (ActivityList.Count > 0 && ActivityList != null)
                {
                   // ActivityList[0].Title;
                    //ActivityList[0].Notes;
                   
                    return LoadReferralReferences(ActivityList);
                }
                else
                {
                    return null;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Load Reference
        private List<AM_Activity> LoadReferences(List<AM_Activity> activitylist)
        {
            foreach (AM_Activity item in activitylist)
            {
                item.AM_LookupCode1Reference.Load();
                item.AM_LookupCodeReference.Load();
            }
            return activitylist;
        }

        private List<AM_Activity> LoadReferralReferences(List<AM_Activity> activitylist)
        {
            foreach (AM_Activity item in activitylist)
            {
                item.AM_ReferralReference.Load();
                item.AM_LookupCode1Reference.Load();
                item.AM_LookupCodeReference.Load();
                item.AM_ActionReference.Load();
                item.AM_StatusReference.Load();
            }
            return activitylist;
        }

        #endregion

        #region"Get Organisations"
        public IQueryable GetOrganisations()
        {
            try
            {
                var orgs = from org in this.AMDBMgr.S_ORGANISATION select new { org.ORGID, org.NAME };
                return orgs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Contacts"
        public IQueryable GetContacts(int OrgId)
        {
            var contacts = from Emp in this.AMDBMgr.E__EMPLOYEE
                           where Emp.ORGID == OrgId
                           select new
                           {
                               EMPLOYEEID = (Int32?)Emp.EMPLOYEEID,
                               Name = ((Emp.FIRSTNAME ?? "") + " "+ (Emp.LASTNAME ?? ""))
                           };
            return contacts;
        }
        #endregion

        #region"Get Contact Email"

        public string GetContactEmail(int EmployeeId)
        {
            try
            {
                E_CONTACT con = (from contact in AMDBMgr.E_CONTACT where contact.EMPLOYEEID == EmployeeId select contact).First<E_CONTACT>();
                return con.WORKEMAIL;
            }
            catch (NullReferenceException excep)
            {
                throw excep;
            }
            catch (EntityException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get External Agency Organisations"

        public IQueryable GetExternalAgencyOrganisations()
        {
            try
            {
                var orgs = (from scope in AMDBMgr.S_SCOPE
                            join area in AMDBMgr.S_AREAOFWORK on scope.AREAOFWORK equals area.AREAOFWORKID
                            join org in AMDBMgr.S_ORGANISATION on scope.ORGID equals org.ORGID
                            where area.DESCRIPTION == "External Agency"
                            orderby org.ORGID ascending
                            select org).Distinct();
                return orgs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add Activity"

        public int AddActivity(AM_Activity activity, List<string> docs, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                bool success = false;
                using (TransactionScope tran = new TransactionScope())
                {
                    this.AMDBMgr.AddToAM_Activity(activity);
                    this.AMDBMgr.SaveChanges();
                    AM_Documents ActivityDoc = new AM_Documents();
                    AM_ActivityAndStandardLetters activityLetters = new AM_ActivityAndStandardLetters();
                    for (int i = 0; i < docs.Count; i++)
                    {
                        if (!FileOperations.CheckFileExtension(docs[i]))
                        {
                            string[] str = docs[i].Split(';');
                            activityLetters.StandardLettersId = Int32.Parse(str[0]);
                            //This condition would exectue when there 'll be more than one standard letter & anyone of those is edited
                            if (str.Count() > 1)
                            {
                                if (str[1] != "%")
                                {
                                    activityLetters.StandardLetterHistoryId = int.Parse(str[1]);
                                }
                            }
                          
                            activityLetters.ActivityId = activity.ActivityId;
                            activityLetters.RentBalance = rentBalance;
                            activityLetters.MarketRent = marketRent;
                            activityLetters.CreatedDate = todayDate;
                            activityLetters.RentAmount = rentAmount;
                            

                            this.AMDBMgr.AddToAM_ActivityAndStandardLetters(activityLetters);
                            this.AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(activityLetters);
                        }
                        else
                        {      
                          //  ActivityDoc.DocumentName = Path.GetFileNameWithoutExtension(docs[i]) + "_" +activity.ActivityId.ToString() + Path.GetExtension(docs[i]);
                            ActivityDoc.DocumentName = docs[i];//Path.GetFileNameWithoutExtension(docs[i]) + Path.GetExtension(docs[i]);
                            ActivityDoc.ActivityId = activity.ActivityId;
                            this.AMDBMgr.AddToAM_Documents(ActivityDoc);
                            this.AMDBMgr.SaveChanges();
                            AMDBMgr.Detach(ActivityDoc);
                        }
                    }
                    AMDBMgr.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    tran.Complete();
                    success = true;
                }
                if (success)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Case Activity Documents"

        //#region"Get Case Activity Documents"

        //public List<AM_Documents> GetCaseActivityDocuments(int activityId)
        //{
        //    try
        //    {
        //        IQueryable<AM_Documents> query = from ds in AMDBMgr.AM_Documents
        //                           where ds.ActivityId == activityId
        //                           select ds;

        //        List<AM_Documents> list = new List<AM_Documents>();
        //        list = query.ToList();
        //        return list;
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion

        #endregion

        #region"Get Customer Id"

        public int GetCustomerId(int tennantId)
        {
            try
            {
                C_CUSTOMERTENANCY C = (from customerTenancy in AMDBMgr.C_CUSTOMERTENANCY where customerTenancy.TENANCYID == tennantId select customerTenancy).First<C_CUSTOMERTENANCY>();
                return C.CUSTOMERID;
            }
            catch (NullReferenceException excep)
            {
                throw excep;
            }
            catch (EntityException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
