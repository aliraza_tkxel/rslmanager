﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.Entities;
using System.Transactions;
using System.Data;
namespace Am.Ahv.BusinessObject.base_bo
{
    public class BaseBO
    {

        protected TKXEL_RSLManager_UKEntities AMDBMgr = null;
        public bool SaveChanges()
        {
            try
            {
                bool success = false;
                using (TransactionScope trans = new TransactionScope())
                {
                    AMDBMgr.SaveChanges();
                    trans.Complete();
                    success = true;
                }
                return success;
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (System.Data.OptimisticConcurrencyException optimistic)
            {
                throw optimistic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(object entity)
        {
            try
            {
                AMDBMgr.DeleteObject(entity);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
