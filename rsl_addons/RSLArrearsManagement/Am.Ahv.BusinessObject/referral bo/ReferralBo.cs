﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.referral_bo;
using Am.Ahv.BusinessObject.base_bo;
using Am.Ahv.Entities;
using System.Data;
namespace Am.Ahv.BusinessObject.referral_bo
{
    public class ReferralBO : BaseBO , IReferralBO

    {
        public ReferralBO()
        {
            try
            {
                AMDBMgr = new TKXEL_RSLManager_UKEntities();
            }
            catch (System.ArgumentException argumentexception)
            {
                throw argumentexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region IBaseBO<AM_Referral,int> Members

        AM_Referral BoInterface.basebo.IBaseBO<AM_Referral, int>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        List<AM_Referral> BoInterface.basebo.IBaseBO<AM_Referral, int>.GetAll()
        {
            throw new NotImplementedException();
        }

        AM_Referral BoInterface.basebo.IBaseBO<AM_Referral, int>.AddNew(AM_Referral obj)
        {
            try
            {
                AMDBMgr.AddToAM_Referral(obj);
                AMDBMgr.SaveChanges();
                return obj;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        AM_Referral BoInterface.basebo.IBaseBO<AM_Referral, int>.Update(AM_Referral obj)
        {
            throw new NotImplementedException();
        }

        bool BoInterface.basebo.IBaseBO<AM_Referral, int>.Delete(AM_Referral obj)
        {
            throw new NotImplementedException();
        }

        bool BoInterface.basebo.IBaseBO<AM_Referral, int>.Delete(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IBaseBO<AM_Referral,int> Members


        public List<AM_Referral> GetAll()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
