﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Paythru.Core

'''<summary>
'''This is a test class for UrlProcessorTest and is intended
'''to contain all UrlProcessorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class UrlProcessorTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    <TestMethod()> _
    <Description("Integration")>
    Public Sub ProcessTest_Integration_Valid()

        Assert.Inconclusive("This test should not be used on live environment as this is an intergration test and will insert test data in the database. Please check your connection string and see comments in the test method on how to run this test.")
        '*******************************************************************************
        ' If you need to test end to end intergration, please uncomment the below.
        ' Ensure that the database connection is not pointing to live in app.config
        '*******************************************************************************

        'Dim target As UrlProcessor = New UrlProcessor()
        'Dim tenancyId As String = 977472
        'Dim customerId As String = 19582
        'Dim employeeId As String = Nothing
        'Dim applet As String = "website"

        'Dim actual As String

        'actual = target.Process(tenancyId, customerId, employeeId, applet)

        'Dim redemptionUri As Uri = Nothing
        'Assert.IsTrue(Uri.TryCreate(actual, UriKind.RelativeOrAbsolute, redemptionUri))
        'Debug.WriteLine(redemptionUri.ToString())

    End Sub

End Class
