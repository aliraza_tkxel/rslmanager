﻿
Imports Paythru.Data
Imports System
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Paythru.Core

'''<summary>
'''This is a test class for UrlHelperTest and is intended
'''to contain all UrlHelperTest Unit Tests
'''</summary>
<TestClass()> _
Public Class UrlHelperTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub PostTest_WebsiteStatus_EqualsOK()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111"
        Dim expected As New PostResponse With {.Status = "OK"}
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.AreEqual(expected.Status, actual.Status)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_WebsiteContent_IsNotNull()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111"
        Dim expected As New PostResponse With {.Status = "OK"}
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.AreEqual(expected.Status, actual.Status)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_WebsiteContent_HasValue()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111"
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.IsTrue(actual.Content.Length > 0)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_WebsiteContent_HasRedemptionUrl()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111"

        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.IsTrue(actual.GetRedemptionUrl.Length > 0)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_WebsiteContent_RedemptionUrlIsValid()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111"

        Dim actual As PostResponse
        actual = target.Post(url, payload)
        Dim redemptionUri As Uri = Nothing

        Assert.IsTrue(Uri.TryCreate(actual.GetRedemptionUrl, UriKind.RelativeOrAbsolute, redemptionUri))

    End Sub

    <TestMethod()> _
    Public Sub PostTest_StaffStatus_EqualsOK()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander"
        Dim expected As New PostResponse With {.Status = "OK"}
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.AreEqual(expected.Status, actual.Status)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_StaffContent_IsNotNull()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander"
        Dim expected As New PostResponse With {.Status = "OK"}
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.AreEqual(expected.Status, actual.Status)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_StaffContent_HasValue()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander"
        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.IsTrue(actual.Content.Length > 0)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_StaffContent_HasRedemptionUrl()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander"

        Dim actual As PostResponse
        actual = target.Post(url, payload)

        Assert.IsTrue(actual.GetRedemptionUrl.Length > 0)

    End Sub

    <TestMethod()> _
    Public Sub PostTest_StaffContent_RedemptionUrlIsValid()
        Dim target As UrlHelper = New UrlHelper()
        Dim url As String = "https://api.demo.paythru.com/gettoken"
        Dim payload As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander"

        Dim actual As PostResponse
        actual = target.Post(url, payload)
        Dim redemptionUri As Uri = Nothing

        Assert.IsTrue(Uri.TryCreate(actual.GetRedemptionUrl, UriKind.RelativeOrAbsolute, redemptionUri))

    End Sub

    <TestMethod()> _
    Public Sub PopulateResponseLog_PopulateData_IsValid()

        Dim target As UrlHelper = New UrlHelper()
        Dim postData As PostResponse = New PostResponse() With {.Content = "<?xml version=""1.0"" encoding=""utf-8"" ?>  <paythruResponse>  <token>  <merchant>Broadland</merchant>  <applet>website</applet>  <tokenid>nyRzz8do</tokenid>  <redemption_url>https://broadland.demo.paythru.com/t/nyRzz8do</redemption_url>  <token_start>2013-10-22T10:17:04Z</token_start>  <token_expiry>2013-10-29T10:17:04Z</token_expiry>  <uses>1</uses>  <redemptions>-1</redemptions>  <success_url>https://testcrm.broadlandhousinggroup.org/</success_url>  <cancel_url>https://tenantsonline.broadlandhousing.org/</cancel_url>  <callback_url>https://testcrm.broadlandhousinggroup.org/PaythruWeb/Paythru.svc/notify</callback_url>  <trans_reference />  <trans_type>auth</trans_type>  <trans_cat>purchase</trans_cat>  <currency>GBP</currency>  <creation>2013-10-22T10:17:04Z</creation>  </token>  <user>  <first_name>Nataliya</first_name>  <surname>Alexander</surname>  <email>nataliya.alexander@broadlandgroup.org</email>  </user>  <items>  <item>  <name />  <price />  <quantity>1</quantity>  </item>  </items>  </paythruResponse>", .Status = "OK"}
        Dim transactionId As Guid = New Guid()

        Dim actual As F_PAYTHRU_GET_TOKEN_RESPONSE_LOG
        Dim expected As New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {
                                                                    .TransactionId = transactionId,
                                                                    .Payload = XDocument.Parse("<?xml version=""1.0"" encoding=""utf-8"" ?>  <paythruResponse>  <token>  <merchant>Broadland</merchant>  <applet>website</applet>  <tokenid>nyRzz8do</tokenid>  <redemption_url>https://broadland.demo.paythru.com/t/nyRzz8do</redemption_url>  <token_start>2013-10-22T10:17:04Z</token_start>  <token_expiry>2013-10-29T10:17:04Z</token_expiry>  <uses>1</uses>  <redemptions>-1</redemptions>  <success_url>https://testcrm.broadlandhousinggroup.org/</success_url>  <cancel_url>https://tenantsonline.broadlandhousing.org/</cancel_url>  <callback_url>https://testcrm.broadlandhousinggroup.org/PaythruWeb/Paythru.svc/notify</callback_url>  <trans_reference />  <trans_type>auth</trans_type>  <trans_cat>purchase</trans_cat>  <currency>GBP</currency>  <creation>2013-10-22T10:17:04Z</creation>  </token>  <user>  <first_name>Nataliya</first_name>  <surname>Alexander</surname>  <email>nataliya.alexander@broadlandgroup.org</email>  </user>  <items>  <item>  <name />  <price />  <quantity>1</quantity>  </item>  </items>  </paythruResponse>").ToString,
                                                                    .DateReceived = DateTime.Now,
                                                                    .CreatedBy = Environment.UserName,
                                                                    .DateCreated = DateTime.Now,
                                                                    .RedemptionUrl = "https://broadland.demo.paythru.com/t/nyRzz8do"
                                                                 }

        actual = target.PopulateResponseLog(postData, transactionId)
        Assert.AreEqual(expected.TransactionId, actual.TransactionId)
        Assert.AreEqual(expected.Payload, actual.Payload)
        Assert.IsTrue(DateTime.Parse(expected.DateReceived) < DateTime.Now)
        Assert.AreEqual(expected.CreatedBy, actual.CreatedBy)
        Assert.AreEqual(expected.RedemptionUrl, actual.RedemptionUrl)
        Assert.IsTrue(DateTime.Parse(expected.DateCreated) < DateTime.Now)

    End Sub

    <TestMethod()> _
    Public Sub PopulatePayload_PopulateDataWebsite_IsValid()

        Dim target As UrlHelper = New UrlHelper()
        Dim config As List(Of F_PAYTHRU_CONFIG) = Nothing
        Dim customer As C__CUSTOMER = Nothing
        Dim tenancy As C_TENANCY = Nothing
        Dim prop As P__PROPERTY = Nothing
        Dim applet As String = "website"

        Dim actual As Payload = Nothing

        Dim expected As New Payload With {
                                        .SuccessUrl = "https://tenantsonline.broadlandhousing.org/successtest",
                                        .CancelUrl = "https://tenantsonline.broadlandhousing.org/canceltest",
                                        .CallbackUrl = "https://tenantsonline.broadlandhousing.org/callbacktest",
                                        .FirstName = "Nataliya",
                                        .LastName = "Alexander",
                                        .Email = "nataliya.alexander@broadlandgroup.org",
                                        .TenancyId = 123,
                                        .CustomerId = 456,
                                        .TokenUrl = "https://api.demo.paythru.com/gettoken",
                                        .PropertyId = 101112,
                                        .TokenPayload = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland",
                                        .Address1 = "The Street",
                                        .Address2 = "The village",
                                        .Address3 = Nothing,
                                        .County = "Norfolk",
                                        .Housenumber = "1",
                                        .Postcode = "NR1 1BA",
                                        .TownCity = "Norwich",
                                        .Applet = "website",
                                        .DefaultEmail = "enterprisedev@broadlandgroup.org"
                                       }

        expected.EmployeeId = 0
        expected.EmployeeFirstName = String.Empty
        expected.EmployeeLastName = String.Empty

        config = New List(Of F_PAYTHRU_CONFIG)
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "SuccessUrl_website", .SettingValue = "https://tenantsonline.broadlandhousing.org/successtest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "CancelUrl_website", .SettingValue = "https://tenantsonline.broadlandhousing.org/canceltest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "CallbackUrl_website", .SettingValue = "https://tenantsonline.broadlandhousing.org/callbacktest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "TokenUrl_website", .SettingValue = "https://api.demo.paythru.com/gettoken"})

        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "TokenPayload_website", .SettingValue = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "DefaultEmail_website", .SettingValue = "enterprisedev@broadlandgroup.org"})


        customer = New C__CUSTOMER
        customer.FIRSTNAME = "Nataliya"
        customer.LASTNAME = "Alexander"
        customer.CUSTOMERID = 456

        tenancy = New C_TENANCY
        tenancy.TENANCYID = 123

        prop = New P__PROPERTY
        prop.PROPERTYID = 101112
        prop.ADDRESS1 = "The Street"
        prop.ADDRESS2 = "The village"
        prop.ADDRESS3 = Nothing
        prop.COUNTY = "Norfolk"
        prop.HOUSENUMBER = "1"
        prop.POSTCODE = "NR1 1BA"
        prop.TOWNCITY = "Norwich"

        actual = target.PopulatePayload(customer, "nataliya.alexander@broadlandgroup.org", Nothing, tenancy, prop, applet, config.ToArray)

        Assert.AreEqual(expected.Address1, actual.Address1)
        Assert.AreEqual(expected.Address2, actual.Address2)
        Assert.AreEqual(expected.Address3, actual.Address3)
        Assert.AreEqual(expected.AddressCountry, actual.AddressCountry)
        Assert.AreEqual(expected.Applet, actual.Applet)
        Assert.AreEqual(expected.CallbackUrl, actual.CallbackUrl)
        Assert.AreEqual(expected.CancelUrl, actual.CancelUrl)
        Assert.AreEqual(expected.ConfirmationEmail, actual.ConfirmationEmail)
        Assert.AreEqual(expected.County, actual.County)
        Assert.AreEqual(expected.CustomerId, actual.CustomerId)
        Assert.AreEqual(expected.DefaultEmail, actual.DefaultEmail)
        Assert.AreEqual(expected.Email, actual.Email)
        Assert.AreEqual(String.Empty, actual.EmployeeFirstName)
        Assert.AreEqual(0, actual.EmployeeId)
        Assert.AreEqual(String.Empty, actual.EmployeeLastName)
        Assert.AreEqual(String.Empty, actual.EmployeeName)
        Assert.AreEqual(expected.FirstName, actual.FirstName)
        Assert.AreEqual(expected.Housenumber, actual.Housenumber)
        Assert.AreEqual(expected.LastName, actual.LastName)
        Assert.AreEqual(expected.Postcode, actual.Postcode)
        Assert.AreEqual(expected.PropertyId, actual.PropertyId)
        Assert.AreEqual(expected.SuccessUrl, actual.SuccessUrl)
        Assert.AreEqual(expected.TenancyId, actual.TenancyId)
        Assert.AreEqual(expected.TokenPayload, actual.TokenPayload)
        Assert.AreEqual(expected.TokenUrl, actual.TokenUrl)
        Assert.AreEqual(expected.TownCity, actual.TownCity)

    End Sub

    <TestMethod()> _
    Public Sub PopulatePayload_PopulateDataStaff_IsValid()

        Dim target As UrlHelper = New UrlHelper()
        Dim config As List(Of F_PAYTHRU_CONFIG) = Nothing
        Dim customer As C__CUSTOMER = Nothing
        Dim employee As E__EMPLOYEE = Nothing
        Dim tenancy As C_TENANCY = Nothing
        Dim prop As P__PROPERTY = Nothing
        Dim applet As String = "staff"

        Dim actual As Payload = Nothing

        Dim expected As New Payload With {
                                        .SuccessUrl = "https://tenantsonline.broadlandhousing.org/successtest",
                                        .CancelUrl = "https://tenantsonline.broadlandhousing.org/canceltest",
                                        .CallbackUrl = "https://tenantsonline.broadlandhousing.org/callbacktest",
                                        .FirstName = "Nataliya",
                                        .LastName = "Alexander",
                                        .Email = "nataliya.alexander@broadlandgroup.org",
                                        .TenancyId = 123,
                                        .CustomerId = 456,
                                        .EmployeeFirstName = "John",
                                        .EmployeeLastName = "Smith",
                                        .EmployeeId = 789,
                                        .TokenUrl = "https://api.demo.paythru.com/gettoken",
                                        .PropertyId = 101112,
                                        .TokenPayload = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland",
                                        .Address1 = "The Street",
                                        .Address2 = "The village",
                                        .Address3 = Nothing,
                                        .County = "Norfolk",
                                        .Housenumber = "1",
                                        .Postcode = "NR1 1BA",
                                        .TownCity = "Norwich",
                                        .Applet = "staff",
                                        .DefaultEmail = "enterprisedev@broadlandgroup.org"
                                       }

        config = New List(Of F_PAYTHRU_CONFIG)
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "SuccessUrl_staff", .SettingValue = "https://tenantsonline.broadlandhousing.org/successtest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "CancelUrl_staff", .SettingValue = "https://tenantsonline.broadlandhousing.org/canceltest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "CallbackUrl_staff", .SettingValue = "https://tenantsonline.broadlandhousing.org/callbacktest"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "TokenUrl_staff", .SettingValue = "https://api.demo.paythru.com/gettoken"})

        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "TokenPayload_staff", .SettingValue = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland"})
        config.Add(New F_PAYTHRU_CONFIG With {.SettingName = "DefaultEmail_staff", .SettingValue = "enterprisedev@broadlandgroup.org"})


        customer = New C__CUSTOMER
        customer.FIRSTNAME = "Nataliya"
        customer.LASTNAME = "Alexander"
        customer.CUSTOMERID = 456

        tenancy = New C_TENANCY
        tenancy.TENANCYID = 123

        employee = New E__EMPLOYEE
        employee.FIRSTNAME = "John"
        employee.LASTNAME = "Smith"
        employee.EMPLOYEEID = 789

        prop = New P__PROPERTY
        prop.PROPERTYID = 101112
        prop.ADDRESS1 = "The Street"
        prop.ADDRESS2 = "The village"
        prop.ADDRESS3 = Nothing
        prop.COUNTY = "Norfolk"
        prop.HOUSENUMBER = "1"
        prop.POSTCODE = "NR1 1BA"
        prop.TOWNCITY = "Norwich"

        actual = target.PopulatePayload(customer, "nataliya.alexander@broadlandgroup.org", employee, tenancy, prop, applet, config.ToArray)

        Assert.AreEqual(expected.Address1, actual.Address1)
        Assert.AreEqual(expected.Address2, actual.Address2)
        Assert.AreEqual(expected.Address3, actual.Address3)
        Assert.AreEqual(expected.AddressCountry, actual.AddressCountry)
        Assert.AreEqual(expected.Applet, actual.Applet)
        Assert.AreEqual(expected.CallbackUrl, actual.CallbackUrl)
        Assert.AreEqual(expected.CancelUrl, actual.CancelUrl)
        Assert.AreEqual(expected.ConfirmationEmail, actual.ConfirmationEmail)
        Assert.AreEqual(expected.County, actual.County)
        Assert.AreEqual(expected.CustomerId, actual.CustomerId)
        Assert.AreEqual(expected.DefaultEmail, actual.DefaultEmail)
        Assert.AreEqual(expected.Email, actual.Email)
        Assert.AreEqual(expected.EmployeeFirstName, actual.EmployeeFirstName)
        Assert.AreEqual(expected.EmployeeId, actual.EmployeeId)
        Assert.AreEqual(expected.EmployeeLastName, actual.EmployeeLastName)
        Assert.AreEqual(expected.EmployeeName, actual.EmployeeName)
        Assert.AreEqual(expected.FirstName, actual.FirstName)
        Assert.AreEqual(expected.Housenumber, actual.Housenumber)
        Assert.AreEqual(expected.LastName, actual.LastName)
        Assert.AreEqual(expected.Postcode, actual.Postcode)
        Assert.AreEqual(expected.PropertyId, actual.PropertyId)
        Assert.AreEqual(expected.SuccessUrl, actual.SuccessUrl)
        Assert.AreEqual(expected.TenancyId, actual.TenancyId)
        Assert.AreEqual(expected.TokenPayload, actual.TokenPayload)
        Assert.AreEqual(expected.TokenUrl, actual.TokenUrl)
        Assert.AreEqual(expected.TownCity, actual.TownCity)

    End Sub

    <TestMethod()> _
    Public Sub PopulateEmail_AddressIsNothing_EmptyString()
        Dim target As UrlHelper = New UrlHelper()
        Dim address As C_ADDRESS = Nothing
        Dim expected As String = String.Empty
        Dim actual As String
        actual = target.PopulateEmail(address)
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub PopulateEmail_EmailIsNotSet_EmptyString()
        Dim target As UrlHelper = New UrlHelper()
        Dim address As C_ADDRESS = New C_ADDRESS
        Dim expected As String = String.Empty
        Dim actual As String
        actual = target.PopulateEmail(address)
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub PopulateEmail_EmailIsNothing_EmptyString()
        Dim target As UrlHelper = New UrlHelper()
        Dim address As C_ADDRESS = New C_ADDRESS With {.EMAIL = Nothing}
        Dim expected As String = String.Empty
        Dim actual As String
        actual = target.PopulateEmail(address)
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub PopulateEmail_EmailIsEmpty_EmptyString()
        Dim target As UrlHelper = New UrlHelper()
        Dim address As C_ADDRESS = New C_ADDRESS With {.EMAIL = String.Empty}
        Dim expected As String = String.Empty
        Dim actual As String
        actual = target.PopulateEmail(address)
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub PopulateEmail_EmailIsValid_IsMatch()
        Dim target As UrlHelper = New UrlHelper()
        Dim address As C_ADDRESS = New C_ADDRESS With {.EMAIL = "nataliya.alexander@broadlandgroup.org"}
        Dim expected As String = "nataliya.alexander@broadlandgroup.org"
        Dim actual As String
        actual = target.PopulateEmail(address)
        Assert.AreEqual(expected, actual)

    End Sub


    '''<summary>
    '''A test for PopulatePayload
    '''</summary>
    <TestMethod()> _
    Public Sub PopulatePayloadTest()
        Dim target As UrlHelper = New UrlHelper() ' TODO: Initialize to an appropriate value
        Dim customer As C__CUSTOMER = Nothing ' TODO: Initialize to an appropriate value
        Dim email As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim employee As E__EMPLOYEE = Nothing ' TODO: Initialize to an appropriate value
        Dim tenancy As C_TENANCY = Nothing ' TODO: Initialize to an appropriate value
        Dim prop As P__PROPERTY = Nothing ' TODO: Initialize to an appropriate value
        Dim applet As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim config() As F_PAYTHRU_CONFIG = Nothing ' TODO: Initialize to an appropriate value
        Dim expected As Payload = Nothing ' TODO: Initialize to an appropriate value
        Dim actual As Payload
        actual = target.PopulatePayload(customer, email, employee, tenancy, prop, applet, config)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub
End Class