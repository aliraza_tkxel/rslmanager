﻿Imports System.IO

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Paythru.Core



'''<summary>
'''This is a test class for NotifyProcessorTest and is intended
'''to contain all NotifyProcessorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class NotifyProcessorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    <TestMethod()> _
    <Description("Integration")>
    Public Sub ProcessTest_Integration_Valid()

        Assert.Inconclusive("This test should not be used on live environment as this is an intergration test and will insert test data in the database. Please check your connection string and see comments in the test method on how to run this test.")
        '*******************************************************************************
        ' If you need to test end to end intergration, please uncomment the below.
        ' Ensure that the database connection is not pointing to live in app.config
        '*******************************************************************************

        'Dim transactionId As String

        'transactionId = "40E715B3-07AD-484B-A559-8423A9129120" ' Valid
        'transactionId = "" ' Empty
        'transactionId = "1637D6F9-E4F0-49E3-9193-03E17E715579" ' Not Valid

        'Dim target As NotifyProcessor = New NotifyProcessor()
        'Dim payload As String = "addressPropertyName=&address1=8 Somersby Close&address2= King's Lynn&address3= Norfolk&addressTown=0&addressCounty=0&addressPostcode=0&addressCountry=0&cardLastFour=4444&cardExpiryMonth=10&cardExpiryYear=19&items0Name=rent&items0Price=2.23&items0Quantity=1&items0Reference=&personTitle=0&personFirstName=Maurice&personSurname=Abrams&personMobileNumber=0&personHomePhone=&personEmail=enterprisedev@broadlandgroup.org&transactionKey=f26289ca461e8f97a35f64b7fba63abc&transactionTime=2013-12-17T13:53:54+00:00&transactionStatus=Success&transactionValue=2.23&transactionType=Auth&transactionCurrency=GBP&transactionAuthCode=20200&transactionClass=staff&transactionReference=" & transactionId & "&transactionToken=sWWoGVVy&transactionMaid=0&transactionIpAddress=213.120.95.250&custom_tenancy_ref=971730&custom_customer_ref=8596&custom_staff_id=943&custom_staff_name=Anna Alexander&custom_tenant_property=8 Somersby Close, King's Lynn, Norfolk, PE30 3WD"
        'Dim stream As New System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(payload))
        'target.Process(stream)

    End Sub

End Class
