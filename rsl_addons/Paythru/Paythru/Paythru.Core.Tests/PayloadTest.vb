﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Paythru.Core



'''<summary>
'''This is a test class for PayloadTest and is intended
'''to contain all PayloadTest Unit Tests
'''</summary>
<TestClass()> _
Public Class PayloadTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    <TestMethod()>
    Public Sub EmployeeName_FullName_AsExpected()

        Dim target As Payload = New Payload()
        target.EmployeeFirstName = "Nataliya"
        target.EmployeeLastName = "Alexander"

        Dim expected As String = "Nataliya Alexander"
        Dim actual As String

        actual = target.EmployeeName
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    Public Sub EmployeeName_WithSpaces_AsExpected()

        Dim target As Payload = New Payload()
        target.EmployeeFirstName = " Nataliya "
        target.EmployeeLastName = " Alexander "

        Dim expected As String = "Nataliya Alexander"
        Dim actual As String

        actual = target.EmployeeName
        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub GetPayload_Website_AsExpected()

        Dim target As Payload = New Payload()
        target.SuccessUrl = "http://www.broadlandhousing.org/successtest"
        target.CancelUrl = "http://www.broadlandhousing.org/canceltest"
        target.CallbackUrl = "http://www.broadlandhousing.org/callbacktest"
        target.CustomerId = 11111
        target.Email = "nataliya.alexander@broadlandgroup.org"
        target.EmployeeFirstName = "Nataliya"
        target.EmployeeLastName = "Alexander"
        target.EmployeeId = 22222
        target.FirstName = "Peter"
        target.LastName = "Brown"
        target.PropertyId = "33333"
        target.TokenUrl = "https://api.demo.paythru.com/gettoken"
        target.TokenPayload = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland"
        target.PropertyId = "PA000111122"
        target.TenancyId = "3333333"
        target.Applet = "website"
        target.TransactionId = Guid.Parse("15083929-7ef2-42d5-ab14-4f1c0f75cd70")
        target.Housenumber = "1"
        target.Address1 = "The Street"
        target.Address2 = ""
        target.Address3 = ""
        target.TownCity = "Norwich"
        target.County = "Norfolk"
        target.Postcode = "NR1 1AA"

        Dim applet As String = "website"
        Dim expected As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=website&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_tenant_property=1 The Street, Norwich, Norfolk, NR1 1AA&trans_reference=15083929-7ef2-42d5-ab14-4f1c0f75cd70"
        Dim actual As String
        actual = target.GetPayload()

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub GetPayload_Staff_AsExpected()

        Dim target As Payload = New Payload()

        target.SuccessUrl = "http://www.broadlandhousing.org/successtest"
        target.CancelUrl = "http://www.broadlandhousing.org/canceltest"
        target.CallbackUrl = "http://www.broadlandhousing.org/callbacktest"
        target.CustomerId = 11111
        target.Email = "nataliya.alexander@broadlandgroup.org"
        target.EmployeeFirstName = "Nataliya"
        target.EmployeeLastName = "Alexander"
        target.EmployeeId = 22222
        target.FirstName = "Peter"
        target.LastName = "Brown"
        target.PropertyId = "33333"
        target.TokenUrl = "https://api.demo.paythru.com/gettoken"
        target.TokenPayload = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=staff&redemption_redirect_url=/merchants/broadland"
        target.PropertyId = "PA000111122"
        target.TenancyId = "3333333"
        target.Applet = "staff"
        target.TransactionId = Guid.Parse("15083929-7ef2-42d5-ab14-4f1c0f75cd70")
        target.Housenumber = "1"
        target.Address1 = "The Street"
        target.Address2 = ""
        target.Address3 = ""
        target.TownCity = "Norwich"
        target.County = "Norfolk"
        target.Postcode = "NR1 1AA"

        Dim applet As String = "website"
        Dim expected As String = "api_key=qHIEikIU3kubEPxVgS&api_password=64HnX4YgFVXuBk9hXs&version=5&applet=staff&redemption_redirect_url=/merchants/broadland&success_url=http://www.broadlandhousing.org/successtest&cancel_url=http://www.broadlandhousing.org/canceltest&callback_url=http://www.broadlandhousing.org/callbacktest&first_name=Peter&surname=Brown&email=nataliya.alexander@broadlandgroup.org&custom_tenancy_ref=3333333&custom_customer_ref=11111&custom_staff_id=22222&custom_staff_name=Nataliya Alexander&custom_tenant_property=1 The Street, Norwich, Norfolk, NR1 1AA&trans_reference=15083929-7ef2-42d5-ab14-4f1c0f75cd70"
        Dim actual As String
        actual = target.GetPayload()

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub ConfirmationEmail_CustomerEmailSupplied_CustomerEmail()
        Dim target As Payload = New Payload()
        target.Email = "nataliya.alexander@broadlandgroup.org"
        target.DefaultEmail = String.Empty

        Dim expected As String = "nataliya.alexander@broadlandgroup.org"
        Dim actual As String
        actual = target.ConfirmationEmail

        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub ConfirmationEmail_CustomerEmailNotSupplied_DefaultEmail()
        Dim target As Payload = New Payload()
        target.Email = Nothing
        target.DefaultEmail = "nataliya.alexander@broadlandgroup.org"

        Dim expected As String = "nataliya.alexander@broadlandgroup.org"
        Dim actual As String
        actual = target.ConfirmationEmail

        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub ConfirmationEmail_CustomerAndDefaultEmailNothing_Nothing()
        Dim target As Payload = New Payload()
        target.Email = Nothing
        target.DefaultEmail = Nothing

        Dim expected As String = Nothing
        Dim actual As String
        actual = target.ConfirmationEmail

        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub ConfirmationEmail_CustomerAndDefaultEmailSupplied_CustomerEmail()
        Dim target As Payload = New Payload()
        target.Email = "nataliya.alexander@broadlandgroup.org"
        target.DefaultEmail = "test.test@broadlandgroup.org"

        Dim expected As String = "nataliya.alexander@broadlandgroup.org"
        Dim actual As String
        actual = target.ConfirmationEmail

        Assert.AreEqual(expected, actual)
    End Sub

End Class
