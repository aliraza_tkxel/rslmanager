﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Paythru.Core



'''<summary>
'''This is a test class for AddressHelperTest and is intended
'''to contain all AddressHelperTest Unit Tests
'''</summary>
<TestClass()> _
Public Class AddressHelperTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub Address_Complete_IsValid()

        Dim houseNumber As String = "1"
        Dim address1 As String = "The Street"
        Dim address2 As String = "The Village"
        Dim address3 As String = Nothing
        Dim townCity As String = "Norwich"
        Dim county As String = "Norfolk"
        Dim postCode As String = "NR1 1BA"
        Dim target As AddressHelper = New AddressHelper(houseNumber, address1, address2, address3, townCity, county, postCode) ' TODO: Initialize to an appropriate value
        Dim actual As String
        Dim expected As String = "1 The Street, The Village, Norwich, Norfolk, NR1 1BA"
        actual = target.Address

        Assert.AreEqual(actual, expected)

    End Sub

End Class
