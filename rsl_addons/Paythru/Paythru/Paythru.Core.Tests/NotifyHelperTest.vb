﻿Imports System.Collections.Generic

Imports Paythru.Data
Imports System.Collections.Specialized
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Paythru.Core

'''<summary>
'''This is a test class for NotifyHelperTest and is intended
'''to contain all NotifyHelperTest Unit Tests
'''</summary>
<TestClass()> _
Public Class NotifyHelperTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for PopulateCallbackLog
    '''</summary>
    <TestMethod()> _
    Public Sub PopulateCallbackLog_Populate_IsValid()

        Dim collection As New NameValueCollection
        Dim target As NotifyHelper = New NotifyHelper()
        Dim payloadActual As String = "addressPropertyName=&address1=28 Carlton Gardens&address2= Surrey Street&address3= Norwich&addressTown=0&addressCounty=0&addressPostcode=0&addressCountry=0&cardLastFour=5100&cardExpiryMonth=02&cardExpiryYear=14&items0Name=rent&items0Price=110&items0Quantity=1&items0Reference=&personTitle=0&personFirstName=Vanessa&personSurname=Aldred&personMobileNumber=0&personHomePhone=&personEmail=enterprisedev@broadlandgroup.org&transactionKey=dd9c2d948f7cfe0eaef541a6b207019c&transactionTime=2014-01-08T13:50:03+00:00&transactionStatus=Success&transactionValue=110.00&transactionType=Auth&transactionCurrency=GBP&transactionAuthCode=20200&transactionClass=staff&transactionReference=afdd36c5-64de-479f-b84b-1b58c40a181a&transactionToken=SCvpPPB6&transactionMaid=0&transactionIpAddress=213.120.95.250&custom_tenancy_ref=177210&custom_customer_ref=409&custom_staff_id=416&custom_staff_name=David Hender&custom_tenant_property=28 Carlton Gardens, Surrey Street, Norwich, Norfolk, NR1 3LF"
        Dim payloadExpected As String = "addressPropertyName=&address1=28 Carlton Gardens&address2= Surrey Street&address3= Norwich&addressTown=0&addressCounty=0&addressPostcode=0&addressCountry=0&items0Name=rent&items0Price=110&items0Quantity=1&items0Reference=&personTitle=0&personFirstName=Vanessa&personSurname=Aldred&personMobileNumber=0&personHomePhone=&personEmail=enterprisedev@broadlandgroup.org&transactionKey=dd9c2d948f7cfe0eaef541a6b207019c&transactionTime=2014-01-08T13:50:03+00:00&transactionStatus=Success&transactionValue=110.00&transactionType=Auth&transactionCurrency=GBP&transactionAuthCode=20200&transactionClass=staff&transactionReference=afdd36c5-64de-479f-b84b-1b58c40a181a&transactionToken=SCvpPPB6&transactionMaid=0&transactionIpAddress=213.120.95.250&custom_tenancy_ref=177210&custom_customer_ref=409&custom_staff_id=416&custom_staff_name=David Hender&custom_tenant_property=28 Carlton Gardens, Surrey Street, Norwich, Norfolk, NR1 3LF"
        Dim parameters As NameValueCollection = collection
        Dim actual As F_PAYTHRU_CALLBACK_LOG
        Dim dt As DateTime = DateTime.Now.AddMinutes(-1)

        collection.Add("custom_customer_ref", "123")
        collection.Add("custom_staff_id", "789")
        collection.Add("custom_staff_name", "Matt Jones")
        collection.Add("custom_tenancy_ref", "101112")
        collection.Add("items0Name", "Legal Recharges")
        collection.Add("items0Price", "100.0")
        collection.Add("items0Quantity", "2")
        collection.Add("items0Reference", "")
        collection.Add("personEmail", "nataliya.alexander@broadlandgroup.org")
        collection.Add("personFirstName", "Nataliya")
        collection.Add("personSurname", "Alexander")
        collection.Add("personHomePhone", "01603111111")
        collection.Add("personMobileNumber", "077788556698")
        collection.Add("personTitle", "Mrs")
        collection.Add("transactionAuthCode", "20200")
        collection.Add("transactionClass", "website")
        collection.Add("transactionCurrency", "GBP")
        collection.Add("transactionIpAddress", "213.120.95.250")
        collection.Add("transactionKey", "5b95cf6f8319813aa9cd8c421da2a3bd")
        collection.Add("transactionMaid", "0")
        collection.Add("transactionReference", "AE652A7B-6ABF-491C-B101-E228EF562387")
        collection.Add("transactionStatus", "Success")
        collection.Add("transactionTime", "2013-10-22T11:25:44+01:00")
        collection.Add("transactionToken", "nyRzz8do")
        collection.Add("transactionType", "Auth")
        collection.Add("transactionValue", "1.00")

        actual = target.PopulateCallbackLog(payloadActual, parameters)

        Assert.AreEqual(actual.CallbackId, 0)
        Assert.AreEqual(actual.CreatedBy, Environment.UserName)
        Assert.AreEqual(actual.CustomCustomerRef, "123")
        Assert.AreEqual(actual.CustomStaffId, "789")
        Assert.AreEqual(actual.CustomStaffName, "Matt Jones")
        Assert.AreEqual(actual.CustomTenancyRef, "101112")
        Assert.IsTrue(actual.DateCreated > dt And actual.DateCreated < DateTime.Now.AddMinutes(1))
        Assert.AreEqual(actual.DateProcessed, Nothing)
        Assert.IsTrue(actual.DateReceived > dt And actual.DateReceived < DateTime.Now.AddMinutes(1))
        Assert.AreEqual(actual.Items0name, "Legal Recharges")
        Assert.AreEqual(actual.Items0price, "100.0")
        Assert.AreEqual(actual.Items0quantity, "2")
        Assert.AreEqual(actual.Items0reference, "")
        Assert.AreEqual(actual.Payload, payloadExpected)
        Assert.AreEqual(actual.PersonEmail, "nataliya.alexander@broadlandgroup.org")
        Assert.AreEqual(actual.PersonFirstName, "Nataliya")
        Assert.AreEqual(actual.PersonHomePhone, "01603111111")
        Assert.AreEqual(actual.PersonMobileNumber, "077788556698")
        Assert.AreEqual(actual.PersonSurname, "Alexander")
        Assert.AreEqual(actual.PersonTitle, "Mrs")
        Assert.AreEqual(actual.TransactionAuthCode, "20200")
        Assert.AreEqual(actual.TransactionClass, "website")
        Assert.AreEqual(actual.TransactionCurrency, "GBP")
        Assert.AreEqual(actual.TransactionId.ToString().ToUpper(), "AE652A7B-6ABF-491C-B101-E228EF562387")
        Assert.AreEqual(actual.TransactionIpAddress, "213.120.95.250")
        Assert.AreEqual(actual.TransactionKey, "5b95cf6f8319813aa9cd8c421da2a3bd")
        Assert.AreEqual(actual.TransactionMaid, "0")
        Assert.AreEqual(actual.TransactionStatus, "Success")
        Assert.AreEqual(actual.TransactionTime, "2013-10-22T11:25:44+01:00")
        Assert.AreEqual(actual.TransactionToken, "nyRzz8do")
        Assert.AreEqual(actual.TransactionType, "Auth")
        Assert.AreEqual(actual.TransactionValue, "1.00")

    End Sub

    <TestMethod()> _
    Public Sub UpdateProcessDate_DateUpdated_IsValid()

        Dim flagDate As DateTime = DateTime.Now.AddMinutes(-1)
        Dim target As NotifyHelper = New NotifyHelper()
        Dim log As New F_PAYTHRU_CALLBACK_LOG
        target.UpdateProcessDate(log)

        Assert.IsTrue(Date.Parse(log.DateProcessed) < DateTime.Now.AddMinutes(1) AndAlso Date.Parse(log.DateProcessed) > flagDate)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_Rent()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim expected As Integer = 1
        Dim actual As Integer = target.GetItemType("Rent", New PaythruEntities())

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_Recharge()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim expected As Integer = 5
        Dim actual As Integer = target.GetItemType("Recharge", New PaythruEntities())

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_LegalRecharge()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim expected As Integer = 13
        Dim actual As Integer = target.GetItemType("legal_recharge", New PaythruEntities())

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub UpdateProcessDate_DateTimeNow_IsValid()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim log As New F_PAYTHRU_CALLBACK_LOG
        Dim dt As DateTime = DateTime.Now.AddMinutes(-1)

        target.UpdateProcessDate(log)

        Assert.IsTrue(log.DateProcessed.HasValue = True And log.DateProcessed > dt And log.DateProcessed < DateTime.Now.AddMinutes(1))

    End Sub

    <TestMethod()> _
    Public Sub PopulateRentJournal_Populated_IsValid()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim log As New F_PAYTHRU_CALLBACK_LOG With {.CustomTenancyRef = "12345", .TransactionTime = "2013-10-22T11:25:44+01:00", .Items0price = "100.00"}

        Dim actual As F_RENTJOURNAL = target.PopulateRentJournal(log, 5, 93)

        Assert.AreEqual(actual.TENANCYID, 12345)
        Assert.AreEqual(actual.TRANSACTIONDATE, New DateTime(2013, 10, 22, 0, 0, 0, 0))
        Assert.AreEqual(actual.AMOUNT, New Decimal(-100.0))
        Assert.AreEqual(actual.STATUSID, 2)
        Assert.AreEqual(actual.PAYMENTTYPE, 93)
        Assert.AreEqual(actual.ITEMTYPE, 5)

    End Sub

    <TestMethod()> _
    Public Sub PopulateCashPosting_Populated_IsValid()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL
      
        Dim log As New F_PAYTHRU_CALLBACK_LOG With {.CustomTenancyRef = "12345",
                                                    .TransactionTime = "2013-10-22T11:25:44+01:00",
                                                    .Items0price = "100.00",
                                                    .CustomStaffId = "943",
                                                    .CustomCustomerRef = "678910",
                                                    .PersonFirstName = "Nataliya",
                                                    .PersonSurname = "Alexander"}

        Dim actual As F_CASHPOSTING

        actual = target.PopulateCashPosting(log, journal, 13, 20, 93, -2)

        Assert.AreEqual(actual.CREATEDBY, 943)
        Assert.AreEqual(actual.CREATIONDATE, New DateTime(2013, 10, 22, 0, 0, 0, 0))
        Assert.AreEqual(actual.RECEIVEDFROM, "Nataliya Alexander")
        Assert.AreEqual(actual.AMOUNT, 100.0)
        Assert.AreEqual(actual.OFFICE, 20)
        Assert.AreEqual(actual.ITEMTYPE, 13)
        Assert.AreEqual(actual.PAYMENTTYPE, 93)
        Assert.AreEqual(actual.TENANCYID, 12345)
        Assert.AreEqual(actual.CUSTOMERID, 678910)
        Assert.IsNotNull(actual.F_RENTJOURNAL)

    End Sub

    <TestMethod()> _
    Public Sub PopulateCashPosting_PopulatedDefaultUserId_IsValid()

        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL

        Dim log As New F_PAYTHRU_CALLBACK_LOG With {.CustomTenancyRef = "12345",
                                                    .TransactionTime = "2013-10-22T11:25:44+01:00",
                                                    .Items0price = "100.00",
                                                    .CustomCustomerRef = "678910",
                                                    .PersonFirstName = "Nataliya",
                                                    .PersonSurname = "Alexander"}

        Dim actual As F_CASHPOSTING

        actual = target.PopulateCashPosting(log, journal, 13, 20, 93, -2)

        Assert.AreEqual(actual.CREATEDBY, -2)
        Assert.AreEqual(actual.CREATIONDATE, New DateTime(2013, 10, 22, 0, 0, 0, 0))
        Assert.AreEqual(actual.RECEIVEDFROM, "Nataliya Alexander")
        Assert.AreEqual(actual.AMOUNT, 100.0)
        Assert.AreEqual(actual.OFFICE, 20)
        Assert.AreEqual(actual.ITEMTYPE, 13)
        Assert.AreEqual(actual.PAYMENTTYPE, 93)
        Assert.AreEqual(actual.TENANCYID, 12345)
        Assert.AreEqual(actual.CUSTOMERID, 678910)
        Assert.IsNotNull(actual.F_RENTJOURNAL)

    End Sub

    <TestMethod()> _
    Public Sub ValidateJournal_AllItemsValid_NoException()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = 1, .TENANCYID = 123, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_ItemTypeIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_ItemTypeIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = Nothing, .TENANCYID = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_ItemTypeIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = 0, .TENANCYID = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_ItemTypeIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = -1, .TENANCYID = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_ItemTypeIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = -199, .TENANCYID = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
   <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_TenancyIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.ITEMTYPE = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_TenancyIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = Nothing, .ITEMTYPE = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_TenancyIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = 0, .ITEMTYPE = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_TenancyIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = -1, .ITEMTYPE = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_TenancyIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = -199, .ITEMTYPE = 123}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()> _
    Public Sub ValidateCashPosting_AllItemsValid_NoException()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .OFFICE = 20}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_ItemTypeIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_ItemTypeIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = Nothing, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_ItemTypeIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = 0, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_ItemTypeIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = -1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_ItemTypeIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = -199, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
   <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_TenancyIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .ITEMTYPE = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_TenancyIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = Nothing, .ITEMTYPE = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_TenancyIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = 0, .ITEMTYPE = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_TenancyIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -1, .ITEMTYPE = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_TenancyIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -199, .ITEMTYPE = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_CustomerIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = 123, .ITEMTYPE = 123, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_CustomerIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = Nothing, .ITEMTYPE = 123, .CUSTOMERID = Nothing, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_CustomerIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = 0, .ITEMTYPE = 123, .CUSTOMERID = 0, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_CustomerIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -1, .ITEMTYPE = 123, .CUSTOMERID = -1, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_CustomerIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -199, .ITEMTYPE = 123, .CUSTOMERID = -199, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_RentJournalIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -199, .ITEMTYPE = 123, .CUSTOMERID = -199, .F_RENTJOURNAL = Nothing}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_RentJournalIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 93, .TENANCYID = -199, .ITEMTYPE = 123, .CUSTOMERID = -199}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_PaymentTypeIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_PaymentTypeIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = Nothing, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_PaymentTypeIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = 0, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_PaymentTypeIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = -1, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_PaymentTypeIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.PAYMENTTYPE = -199, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_PaymentTypeIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.TENANCYID = 123, .ITEMTYPE = 1}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_PaymentTypeIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.PAYMENTTYPE = Nothing, .TENANCYID = 123, .ITEMTYPE = 1}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_PaymentTypeIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.PAYMENTTYPE = 0, .TENANCYID = 123, .ITEMTYPE = 1}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_PaymentTypeIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.PAYMENTTYPE = -1, .TENANCYID = 123, .ITEMTYPE = 1}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateJournal_PaymentTypeIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim journal As New F_RENTJOURNAL With {.PAYMENTTYPE = -199, .TENANCYID = 123, .ITEMTYPE = 1}

        ' this method will raise exception is any of the values are not valid
        target.ValidateJournal(journal)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
   <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_OfficeIsNotSet_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_OfficeIsNothing_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.OFFICE = Nothing, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_OfficeIsZero_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.OFFICE = 0, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_OfficeIsMinus1_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.OFFICE = -1, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(System.Exception))>
    Public Sub ValidateCashPosting_OfficeIsMinus199_Exception()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim posting As New F_CASHPOSTING With {.OFFICE = -199, .ITEMTYPE = 1, .TENANCYID = 123, .CUSTOMERID = 456, .F_RENTJOURNAL = New F_RENTJOURNAL, .PAYMENTTYPE = 93}

        ' this method will raise exception is any of the values are not valid
        target.ValidateCashPosting(posting)
        Assert.IsTrue(True)
    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_GetRentItemTypeId()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim Description As String = "rent"
        Dim context As New PaythruEntities

        Dim expected As Integer = 1
        Dim actual As Integer
        actual = target.GetItemType(Description, context)

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_GetRechargeItemTypeId()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim Description As String = "recharge"
        Dim context As New PaythruEntities

        Dim expected As Integer = 5
        Dim actual As Integer
        actual = target.GetItemType(Description, context)

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetItemType_Integration_GetLegalRechargesItemTypeId()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim Description As String = "legal_recharge"
        Dim context As New PaythruEntities

        Dim expected As Integer = 13
        Dim actual As Integer
        actual = target.GetItemType(Description, context)

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()>
    <Description("Integration")>
    Public Sub GetOfficeId_Integration_Found()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim context As New PaythruEntities
        Dim expected As Integer = 20
        Dim actual As Integer

        actual = target.GetOfficeId(context)
        Assert.AreEqual(expected, actual)

    End Sub

    '''<summary>
    '''A test for GetDefaultUserId
    '''</summary>
    <TestMethod()>
    <Description("Integration")>
    Public Sub GetDefaultUserId_Integration_Found()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim context As New PaythruEntities
        Dim expected As Integer = -2
        Dim actual As Integer
        actual = target.GetDefaultUserId(context)

        Assert.AreEqual(expected, actual)

    End Sub

    '''<summary>
    '''A test for GetPaymentTypeId
    '''</summary>
    <TestMethod()>
    <Description("Integration")>
    Public Sub GetPaymentTypeId_Integration_Found()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim context As New PaythruEntities
        Dim expected As Integer = 93
        Dim actual As Integer
        actual = target.GetPaymentTypeId(context)

        Assert.AreEqual(expected, actual)

    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_ReponseLogIsNothing_ReturnsFalse()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = "success"
        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = Nothing
        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_IsValidOneListEntry_ReturnsTrue()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = "success"
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("C229A534-933F-49DF-9ADB-B019653A493E")})

        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList

        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_IsValidMultipleListEntry_ReturnsTrue()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = "success"
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("C229A534-933F-49DF-9ADB-B019653A493E")})
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("EFF16A8B-55AE-48F3-9A02-A6D0CAA0808A")})

        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList

        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_IsValidEmptyListEntry_ReturnsFalse()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = "success"
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)
     
        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList

        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_TransactionStatusIsNothing_ReturnsFalse()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = Nothing
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)

        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("EFF16A8B-55AE-48F3-9A02-A6D0CAA0808A")})

        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_TransactionStatusIsEmpty_ReturnsFalse()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = String.Empty
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)

        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("EFF16A8B-55AE-48F3-9A02-A6D0CAA0808A")})

        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub IsValidTransaction_TransactionStatusIsInvalid_ReturnsFalse()
        Dim target As NotifyHelper = New NotifyHelper()
        Dim transactionStatus As String = "somethingelse"
        Dim responseList As New List(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)

        Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = responseList
        responseList.Add(New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG With {.TransactionId = Guid.Parse("EFF16A8B-55AE-48F3-9A02-A6D0CAA0808A")})

        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.IsValidTransaction(transactionStatus, responseLog)
        Assert.AreEqual(expected, actual)
    End Sub

End Class