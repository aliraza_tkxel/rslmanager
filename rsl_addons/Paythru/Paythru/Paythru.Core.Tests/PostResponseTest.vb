﻿Imports System.Text
Imports System.Xml

<TestClass()>
Public Class PostResponseTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()>
    <Description("PostResponse")>
    Public Sub GetRedemptionUrl_ValidValue_UrlFound()

        Dim postResponse As New PostResponse
        Dim expected As String = "https://broadland.demo.paythru.com/t/nyRzz8do"

        postResponse.Content = "<?xml version=""1.0"" encoding=""utf-8""?><paythruResponse><token><merchant>Broadland</merchant><applet>website</applet><tokenid>nyRzz8do</tokenid><redemption_url>https://broadland.demo.paythru.com/t/nyRzz8do</redemption_url><token_start>2013-10-22T10:17:04Z</token_start><token_expiry>2013-10-29T10:17:04Z</token_expiry><uses>1</uses><redemptions>-1</redemptions><success_url>https://testcrm.broadlandhousinggroup.org/</success_url><cancel_url>https://tenantsonline.broadlandhousing.org/</cancel_url><callback_url>https://testcrm.broadlandhousinggroup.org/PaythruWeb/Paythru.svc/notify</callback_url><trans_reference/><trans_type>auth</trans_type><trans_cat>purchase</trans_cat><currency>GBP</currency><creation>2013-10-22T10:17:04Z</creation></token><user><first_name>Nataliya</first_name><surname>Alexander</surname><email>nataliya.alexander@broadlandgroup.org</email></user><items><item><name/><price/><quantity>1</quantity></item></items></paythruResponse>"

        Dim actual As String = postResponse.GetRedemptionUrl

        Assert.AreEqual(expected, actual, False)

    End Sub

    <TestMethod()>
    <Description("PostResponse")>
    <ExpectedException(GetType(System.Xml.XmlException))>
    Public Sub GetRedemptionUrl_EmptyContent_Exception()

        Dim postResponse As New PostResponse
        Dim expected As String = "https://broadland.demo.paythru.com/t/nyRzz8do"

        postResponse.Content = String.Empty

        Dim actual As String = postResponse.GetRedemptionUrl

        Assert.AreEqual(expected, actual, False)

    End Sub

    <TestMethod()>
    <Description("PostResponse")>
    <ExpectedException(GetType(System.ArgumentNullException))>
    Public Sub GetRedemptionUrl_NothingContent_Exception()

        Dim postResponse As New PostResponse
        Dim expected As String = "https://broadland.demo.paythru.com/t/nyRzz8do"

        postResponse.Content = Nothing

        Dim actual As String = postResponse.GetRedemptionUrl

        Assert.AreEqual(expected, actual, False)

    End Sub

    <TestMethod()>
    <Description("PostResponse")>
    Public Sub GetRedemptionUrl_NotEqual_Exception()

        Dim postResponse As New PostResponse
        Dim expected As String = "Randomteststring"

        postResponse.Content = "<?xml version=""1.0"" encoding=""utf-8""?><paythruResponse><token><merchant>Broadland</merchant><applet>website</applet><tokenid>nyRzz8do</tokenid><redemption_url>https://broadland.demo.paythru.com/t/nyRzz8do</redemption_url><token_start>2013-10-22T10:17:04Z</token_start><token_expiry>2013-10-29T10:17:04Z</token_expiry><uses>1</uses><redemptions>-1</redemptions><success_url>https://testcrm.broadlandhousinggroup.org/</success_url><cancel_url>https://tenantsonline.broadlandhousing.org/</cancel_url><callback_url>https://testcrm.broadlandhousinggroup.org/PaythruWeb/Paythru.svc/notify</callback_url><trans_reference/><trans_type>auth</trans_type><trans_cat>purchase</trans_cat><currency>GBP</currency><creation>2013-10-22T10:17:04Z</creation></token><user><first_name>Nataliya</first_name><surname>Alexander</surname><email>nataliya.alexander@broadlandgroup.org</email></user><items><item><name/><price/><quantity>1</quantity></item></items></paythruResponse>"

        Dim actual As String = postResponse.GetRedemptionUrl

        Assert.AreNotEqual(expected, actual, False)

    End Sub

    <TestMethod()>
   <Description("PostResponse")>
   <ExpectedException(GetType(System.NullReferenceException))>
    Public Sub GetRedemptionUrl_NodeNotFound_Exception()

        Dim postResponse As New PostResponse
        Dim expected As String = "Randomteststring"

        postResponse.Content = "<?xml version=""1.0"" encoding=""utf-8""?><paythruResponse><token><merchant>Broadland</merchant><applet>website</applet><tokenid>nyRzz8do</tokenid><redemption_url_dodgy>https://broadland.demo.paythru.com/t/nyRzz8do</redemption_url_dodgy><token_start>2013-10-22T10:17:04Z</token_start><token_expiry>2013-10-29T10:17:04Z</token_expiry><uses>1</uses><redemptions>-1</redemptions><success_url>https://testcrm.broadlandhousinggroup.org/</success_url><cancel_url>https://tenantsonline.broadlandhousing.org/</cancel_url><callback_url>https://testcrm.broadlandhousinggroup.org/PaythruWeb/Paythru.svc/notify</callback_url><trans_reference/><trans_type>auth</trans_type><trans_cat>purchase</trans_cat><currency>GBP</currency><creation>2013-10-22T10:17:04Z</creation></token><user><first_name>Nataliya</first_name><surname>Alexander</surname><email>nataliya.alexander@broadlandgroup.org</email></user><items><item><name/><price/><quantity>1</quantity></item></items></paythruResponse>"

        Dim actual As String = postResponse.GetRedemptionUrl

        Assert.AreNotEqual(expected, actual, False)

    End Sub


End Class
