﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Client
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.bsParameters = New System.Windows.Forms.BindingSource(Me.components)
        Me.tcMain = New System.Windows.Forms.TabControl()
        Me.tpGetUrl = New System.Windows.Forms.TabPage()
        Me.llRedemptionUrl = New System.Windows.Forms.LinkLabel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtProperty = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTenancyId = New System.Windows.Forms.TextBox()
        Me.cboApplet = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboCustomer = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboEmployee = New System.Windows.Forms.ComboBox()
        Me.txtQueryString = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblResponseGet = New System.Windows.Forms.Label()
        Me.txtResponseGet = New System.Windows.Forms.TextBox()
        Me.btnSubmitGet = New System.Windows.Forms.Button()
        Me.lblUrlGet = New System.Windows.Forms.Label()
        Me.txtUrlGet = New System.Windows.Forms.TextBox()
        Me.tpNotify = New System.Windows.Forms.TabPage()
        Me.btnGenerateString = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgParameters = New System.Windows.Forms.DataGridView()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValueDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtParameters = New System.Windows.Forms.TextBox()
        Me.lblParameters = New System.Windows.Forms.Label()
        Me.lblResponseNotify = New System.Windows.Forms.Label()
        Me.txtResponseNotify = New System.Windows.Forms.TextBox()
        Me.btnSubmitNotify = New System.Windows.Forms.Button()
        Me.lblUrlNotify = New System.Windows.Forms.Label()
        Me.txtUrlNotify = New System.Windows.Forms.TextBox()
        Me.tpTestCardInfo = New System.Windows.Forms.TabPage()
        Me.wbBrowser = New System.Windows.Forms.WebBrowser()
        Me.lblConnectionString = New System.Windows.Forms.Label()
        CType(Me.bsParameters, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcMain.SuspendLayout()
        Me.tpGetUrl.SuspendLayout()
        Me.tpNotify.SuspendLayout()
        CType(Me.dgParameters, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpTestCardInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'bsParameters
        '
        Me.bsParameters.DataSource = GetType(PaythruTestHarness.NotifyParameters)
        '
        'tcMain
        '
        Me.tcMain.Controls.Add(Me.tpGetUrl)
        Me.tcMain.Controls.Add(Me.tpNotify)
        Me.tcMain.Controls.Add(Me.tpTestCardInfo)
        Me.tcMain.Location = New System.Drawing.Point(6, 7)
        Me.tcMain.Name = "tcMain"
        Me.tcMain.SelectedIndex = 0
        Me.tcMain.Size = New System.Drawing.Size(834, 688)
        Me.tcMain.TabIndex = 0
        Me.tcMain.Text = Global.PaythruTestHarness.My.MySettings.Default.NotifyUrl
        '
        'tpGetUrl
        '
        Me.tpGetUrl.Controls.Add(Me.llRedemptionUrl)
        Me.tpGetUrl.Controls.Add(Me.Label7)
        Me.tpGetUrl.Controls.Add(Me.txtProperty)
        Me.tpGetUrl.Controls.Add(Me.Label6)
        Me.tpGetUrl.Controls.Add(Me.txtTenancyId)
        Me.tpGetUrl.Controls.Add(Me.cboApplet)
        Me.tpGetUrl.Controls.Add(Me.Label5)
        Me.tpGetUrl.Controls.Add(Me.Label4)
        Me.tpGetUrl.Controls.Add(Me.txtAddress)
        Me.tpGetUrl.Controls.Add(Me.Label3)
        Me.tpGetUrl.Controls.Add(Me.cboCustomer)
        Me.tpGetUrl.Controls.Add(Me.Label2)
        Me.tpGetUrl.Controls.Add(Me.cboEmployee)
        Me.tpGetUrl.Controls.Add(Me.txtQueryString)
        Me.tpGetUrl.Controls.Add(Me.Label1)
        Me.tpGetUrl.Controls.Add(Me.lblResponseGet)
        Me.tpGetUrl.Controls.Add(Me.txtResponseGet)
        Me.tpGetUrl.Controls.Add(Me.btnSubmitGet)
        Me.tpGetUrl.Controls.Add(Me.lblUrlGet)
        Me.tpGetUrl.Controls.Add(Me.txtUrlGet)
        Me.tpGetUrl.Location = New System.Drawing.Point(4, 22)
        Me.tpGetUrl.Name = "tpGetUrl"
        Me.tpGetUrl.Padding = New System.Windows.Forms.Padding(3)
        Me.tpGetUrl.Size = New System.Drawing.Size(826, 662)
        Me.tpGetUrl.TabIndex = 0
        Me.tpGetUrl.Text = "GetUrl"
        Me.tpGetUrl.UseVisualStyleBackColor = True
        '
        'llRedemptionUrl
        '
        Me.llRedemptionUrl.AutoSize = True
        Me.llRedemptionUrl.Location = New System.Drawing.Point(10, 599)
        Me.llRedemptionUrl.Name = "llRedemptionUrl"
        Me.llRedemptionUrl.Size = New System.Drawing.Size(16, 13)
        Me.llRedemptionUrl.TabIndex = 26
        Me.llRedemptionUrl.TabStop = True
        Me.llRedemptionUrl.Text = "..."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(475, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = "Property Ref:"
        '
        'txtProperty
        '
        Me.txtProperty.Location = New System.Drawing.Point(553, 122)
        Me.txtProperty.Name = "txtProperty"
        Me.txtProperty.Size = New System.Drawing.Size(258, 20)
        Me.txtProperty.TabIndex = 24
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(475, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Tenancy Ref:"
        '
        'txtTenancyId
        '
        Me.txtTenancyId.Location = New System.Drawing.Point(553, 161)
        Me.txtTenancyId.Name = "txtTenancyId"
        Me.txtTenancyId.Size = New System.Drawing.Size(258, 20)
        Me.txtTenancyId.TabIndex = 22
        '
        'cboApplet
        '
        Me.cboApplet.FormattingEnabled = True
        Me.cboApplet.Items.AddRange(New Object() {"staff", "website"})
        Me.cboApplet.Location = New System.Drawing.Point(553, 202)
        Me.cboApplet.Name = "cboApplet"
        Me.cboApplet.Size = New System.Drawing.Size(258, 21)
        Me.cboApplet.TabIndex = 21
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(507, 205)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Applet:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Property Address:"
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(16, 132)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(338, 137)
        Me.txtAddress.TabIndex = 18
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Customer Name:"
        '
        'cboCustomer
        '
        Me.cboCustomer.FormattingEnabled = True
        Me.cboCustomer.Location = New System.Drawing.Point(104, 77)
        Me.cboCustomer.Name = "cboCustomer"
        Me.cboCustomer.Size = New System.Drawing.Size(250, 21)
        Me.cboCustomer.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(484, 252)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Staff Name:"
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(553, 248)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(258, 21)
        Me.cboEmployee.TabIndex = 14
        '
        'txtQueryString
        '
        Me.txtQueryString.Location = New System.Drawing.Point(10, 301)
        Me.txtQueryString.Multiline = True
        Me.txtQueryString.Name = "txtQueryString"
        Me.txtQueryString.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtQueryString.Size = New System.Drawing.Size(801, 127)
        Me.txtQueryString.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 282)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Path:"
        '
        'lblResponseGet
        '
        Me.lblResponseGet.AutoSize = True
        Me.lblResponseGet.Location = New System.Drawing.Point(10, 431)
        Me.lblResponseGet.Name = "lblResponseGet"
        Me.lblResponseGet.Size = New System.Drawing.Size(58, 13)
        Me.lblResponseGet.TabIndex = 4
        Me.lblResponseGet.Text = "Response:"
        '
        'txtResponseGet
        '
        Me.txtResponseGet.Location = New System.Drawing.Point(10, 447)
        Me.txtResponseGet.Multiline = True
        Me.txtResponseGet.Name = "txtResponseGet"
        Me.txtResponseGet.Size = New System.Drawing.Size(801, 145)
        Me.txtResponseGet.TabIndex = 3
        '
        'btnSubmitGet
        '
        Me.btnSubmitGet.Location = New System.Drawing.Point(736, 610)
        Me.btnSubmitGet.Name = "btnSubmitGet"
        Me.btnSubmitGet.Size = New System.Drawing.Size(75, 23)
        Me.btnSubmitGet.TabIndex = 2
        Me.btnSubmitGet.Text = "Submit"
        Me.btnSubmitGet.UseVisualStyleBackColor = True
        '
        'lblUrlGet
        '
        Me.lblUrlGet.AutoSize = True
        Me.lblUrlGet.Location = New System.Drawing.Point(7, 15)
        Me.lblUrlGet.Name = "lblUrlGet"
        Me.lblUrlGet.Size = New System.Drawing.Size(23, 13)
        Me.lblUrlGet.TabIndex = 1
        Me.lblUrlGet.Text = "Url:"
        '
        'txtUrlGet
        '
        Me.txtUrlGet.Location = New System.Drawing.Point(7, 35)
        Me.txtUrlGet.Name = "txtUrlGet"
        Me.txtUrlGet.Size = New System.Drawing.Size(804, 20)
        Me.txtUrlGet.TabIndex = 0
        Me.txtUrlGet.Text = Global.PaythruTestHarness.My.MySettings.Default.GetUrl
        '
        'tpNotify
        '
        Me.tpNotify.Controls.Add(Me.btnGenerateString)
        Me.tpNotify.Controls.Add(Me.Label8)
        Me.tpNotify.Controls.Add(Me.dgParameters)
        Me.tpNotify.Controls.Add(Me.txtParameters)
        Me.tpNotify.Controls.Add(Me.lblParameters)
        Me.tpNotify.Controls.Add(Me.lblResponseNotify)
        Me.tpNotify.Controls.Add(Me.txtResponseNotify)
        Me.tpNotify.Controls.Add(Me.btnSubmitNotify)
        Me.tpNotify.Controls.Add(Me.lblUrlNotify)
        Me.tpNotify.Controls.Add(Me.txtUrlNotify)
        Me.tpNotify.Location = New System.Drawing.Point(4, 22)
        Me.tpNotify.Name = "tpNotify"
        Me.tpNotify.Padding = New System.Windows.Forms.Padding(3)
        Me.tpNotify.Size = New System.Drawing.Size(826, 662)
        Me.tpNotify.TabIndex = 1
        Me.tpNotify.Text = "Notify"
        Me.tpNotify.UseVisualStyleBackColor = True
        '
        'btnGenerateString
        '
        Me.btnGenerateString.Location = New System.Drawing.Point(704, 370)
        Me.btnGenerateString.Name = "btnGenerateString"
        Me.btnGenerateString.Size = New System.Drawing.Size(101, 23)
        Me.btnGenerateString.TabIndex = 14
        Me.btnGenerateString.Text = "Generate String"
        Me.btnGenerateString.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 386)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(86, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Parameter string:"
        '
        'dgParameters
        '
        Me.dgParameters.AutoGenerateColumns = False
        Me.dgParameters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgParameters.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NameDataGridViewTextBoxColumn, Me.ValueDataGridViewTextBoxColumn})
        Me.dgParameters.DataSource = Me.bsParameters
        Me.dgParameters.Location = New System.Drawing.Point(11, 74)
        Me.dgParameters.Name = "dgParameters"
        Me.dgParameters.Size = New System.Drawing.Size(799, 287)
        Me.dgParameters.TabIndex = 12
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        Me.NameDataGridViewTextBoxColumn.Width = 300
        '
        'ValueDataGridViewTextBoxColumn
        '
        Me.ValueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ValueDataGridViewTextBoxColumn.DataPropertyName = "Value"
        Me.ValueDataGridViewTextBoxColumn.HeaderText = "Value"
        Me.ValueDataGridViewTextBoxColumn.Name = "ValueDataGridViewTextBoxColumn"
        '
        'txtParameters
        '
        Me.txtParameters.Location = New System.Drawing.Point(11, 402)
        Me.txtParameters.Multiline = True
        Me.txtParameters.Name = "txtParameters"
        Me.txtParameters.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtParameters.Size = New System.Drawing.Size(798, 83)
        Me.txtParameters.TabIndex = 11
        Me.txtParameters.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblParameters
        '
        Me.lblParameters.AutoSize = True
        Me.lblParameters.Location = New System.Drawing.Point(8, 55)
        Me.lblParameters.Name = "lblParameters"
        Me.lblParameters.Size = New System.Drawing.Size(63, 13)
        Me.lblParameters.TabIndex = 10
        Me.lblParameters.Text = "Parameters:"
        '
        'lblResponseNotify
        '
        Me.lblResponseNotify.AutoSize = True
        Me.lblResponseNotify.Location = New System.Drawing.Point(13, 492)
        Me.lblResponseNotify.Name = "lblResponseNotify"
        Me.lblResponseNotify.Size = New System.Drawing.Size(58, 13)
        Me.lblResponseNotify.TabIndex = 9
        Me.lblResponseNotify.Text = "Response:"
        '
        'txtResponseNotify
        '
        Me.txtResponseNotify.Location = New System.Drawing.Point(12, 514)
        Me.txtResponseNotify.Multiline = True
        Me.txtResponseNotify.Name = "txtResponseNotify"
        Me.txtResponseNotify.Size = New System.Drawing.Size(798, 101)
        Me.txtResponseNotify.TabIndex = 8
        '
        'btnSubmitNotify
        '
        Me.btnSubmitNotify.Location = New System.Drawing.Point(735, 621)
        Me.btnSubmitNotify.Name = "btnSubmitNotify"
        Me.btnSubmitNotify.Size = New System.Drawing.Size(75, 23)
        Me.btnSubmitNotify.TabIndex = 7
        Me.btnSubmitNotify.Text = "Submit"
        Me.btnSubmitNotify.UseVisualStyleBackColor = True
        '
        'lblUrlNotify
        '
        Me.lblUrlNotify.AutoSize = True
        Me.lblUrlNotify.Location = New System.Drawing.Point(8, 12)
        Me.lblUrlNotify.Name = "lblUrlNotify"
        Me.lblUrlNotify.Size = New System.Drawing.Size(23, 13)
        Me.lblUrlNotify.TabIndex = 6
        Me.lblUrlNotify.Text = "Url:"
        '
        'txtUrlNotify
        '
        Me.txtUrlNotify.Location = New System.Drawing.Point(12, 32)
        Me.txtUrlNotify.Name = "txtUrlNotify"
        Me.txtUrlNotify.Size = New System.Drawing.Size(798, 20)
        Me.txtUrlNotify.TabIndex = 5
        Me.txtUrlNotify.Text = Global.PaythruTestHarness.My.MySettings.Default.NotifyUrl
        '
        'tpTestCardInfo
        '
        Me.tpTestCardInfo.Controls.Add(Me.wbBrowser)
        Me.tpTestCardInfo.Location = New System.Drawing.Point(4, 22)
        Me.tpTestCardInfo.Name = "tpTestCardInfo"
        Me.tpTestCardInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTestCardInfo.Size = New System.Drawing.Size(826, 662)
        Me.tpTestCardInfo.TabIndex = 2
        Me.tpTestCardInfo.Text = "Test Card Info"
        Me.tpTestCardInfo.UseVisualStyleBackColor = True
        '
        'wbBrowser
        '
        Me.wbBrowser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbBrowser.Location = New System.Drawing.Point(3, 3)
        Me.wbBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbBrowser.Name = "wbBrowser"
        Me.wbBrowser.Size = New System.Drawing.Size(820, 656)
        Me.wbBrowser.TabIndex = 0
        Me.wbBrowser.Url = New System.Uri("", System.UriKind.Relative)
        '
        'lblConnectionString
        '
        Me.lblConnectionString.Location = New System.Drawing.Point(7, 709)
        Me.lblConnectionString.Name = "lblConnectionString"
        Me.lblConnectionString.Size = New System.Drawing.Size(829, 42)
        Me.lblConnectionString.TabIndex = 1
        Me.lblConnectionString.Text = "<< Connection String >>"
        '
        'Client
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(848, 760)
        Me.Controls.Add(Me.lblConnectionString)
        Me.Controls.Add(Me.tcMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Client"
        Me.Text = "Paythru Client"
        CType(Me.bsParameters, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcMain.ResumeLayout(False)
        Me.tpGetUrl.ResumeLayout(False)
        Me.tpGetUrl.PerformLayout()
        Me.tpNotify.ResumeLayout(False)
        Me.tpNotify.PerformLayout()
        CType(Me.dgParameters, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpTestCardInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tcMain As System.Windows.Forms.TabControl
    Friend WithEvents tpGetUrl As System.Windows.Forms.TabPage
    Friend WithEvents tpNotify As System.Windows.Forms.TabPage
    Friend WithEvents lblUrlGet As System.Windows.Forms.Label
    Friend WithEvents txtUrlGet As System.Windows.Forms.TextBox
    Friend WithEvents lblResponseGet As System.Windows.Forms.Label
    Friend WithEvents txtResponseGet As System.Windows.Forms.TextBox
    Friend WithEvents btnSubmitGet As System.Windows.Forms.Button
    Friend WithEvents lblResponseNotify As System.Windows.Forms.Label
    Friend WithEvents txtResponseNotify As System.Windows.Forms.TextBox
    Friend WithEvents btnSubmitNotify As System.Windows.Forms.Button
    Friend WithEvents lblUrlNotify As System.Windows.Forms.Label
    Friend WithEvents txtUrlNotify As System.Windows.Forms.TextBox
    Friend WithEvents txtParameters As System.Windows.Forms.TextBox
    Friend WithEvents lblParameters As System.Windows.Forms.Label
    Friend WithEvents txtQueryString As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboCustomer As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboApplet As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTenancyId As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtProperty As System.Windows.Forms.TextBox
    Friend WithEvents dgParameters As System.Windows.Forms.DataGridView
    Friend WithEvents bsParameters As System.Windows.Forms.BindingSource
    Friend WithEvents NameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValueDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateString As System.Windows.Forms.Button
    Friend WithEvents llRedemptionUrl As System.Windows.Forms.LinkLabel
    Friend WithEvents tpTestCardInfo As System.Windows.Forms.TabPage
    Friend WithEvents wbBrowser As System.Windows.Forms.WebBrowser
    Friend WithEvents lblConnectionString As System.Windows.Forms.Label

End Class
