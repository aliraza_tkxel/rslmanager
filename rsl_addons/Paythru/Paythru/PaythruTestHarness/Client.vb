﻿Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Web
Imports System.Xml.Linq
Imports System.Xml.XPath
Imports System.Configuration

Public Class Client

    Public db As Entities
    Private Const QueryString As String = "/geturl?tenancyId={0}&customerId={1}&applet={3}&employeeId={2}"

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        db = New Entities()

    End Sub

    Private Sub btnSubmitNotify_Click(sender As Object, e As System.EventArgs) Handles btnSubmitNotify.Click

        Try
            Me.PostToNotify()
        Catch ex As Exception
            Me.txtResponseNotify.Text = ex.ToString()
        End Try

    End Sub

    Private Sub btnSubmitGet_Click(sender As Object, e As System.EventArgs) Handles btnSubmitGet.Click

        Try
            Me.GetUrl()
        Catch ex As Exception
            Me.txtResponseGet.Text = ex.ToString()
        End Try

    End Sub

    Private Sub PostToNotify()

        Dim payLoad As String = Me.txtParameters.Text.Trim
        Dim stream As IO.Stream = Nothing
        Dim postBin() As Byte = Nothing
        Dim responseStatus As String = Nothing
        Dim responseContent As String = Nothing

        postBin = Encoding.GetEncoding("UTF-8").GetBytes(payLoad)

        ' Create a request for the URL. 
        Dim request As WebRequest = _
          WebRequest.Create(Me.txtUrlNotify.Text.Trim)
        ' If required by the server, set the credentials.
        request.Credentials = CredentialCache.DefaultCredentials
        request.Method = "post"
        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = postBin.Length

        stream = request.GetRequestStream
        stream.Write(postBin, 0, postBin.Length)
        stream.Flush()
        stream.Close()

        ' Get the response.
        Dim response As WebResponse = request.GetResponse()
        ' Display the status.
        responseStatus = (CType(response, HttpWebResponse).StatusDescription)

        ' Get the stream containing content returned by the server.
        Dim dataStream As IO.Stream = response.GetResponseStream()
        ' Open the stream using a StreamReader for easy access.
        Dim reader As New StreamReader(dataStream)
        ' Read the content.
        responseContent = reader.ReadToEnd()
        ' Display the content.
        Me.txtResponseNotify.Text = String.Format("{0} {1}", responseStatus, responseContent)
        ' Clean up the streams and the response.
        reader.Close()
        response.Close()

    End Sub

    Private Sub GetUrl()

        Dim payLoad As String = Me.txtQueryString.Text.Trim
        Dim request As System.Net.HttpWebRequest = _
          System.Net.HttpWebRequest.Create(String.Concat(Me.txtUrlGet.Text.Trim, Me.txtQueryString.Text.Trim))

        request.Method = "get"
        request.ContentType = "text/xml; encoding='utf-8'"

        Dim response As System.Net.HttpWebResponse = request.GetResponse
        Dim responseStatus As String = (CType(response, HttpWebResponse).StatusDescription)
        Dim stream As IO.Stream = response.GetResponseStream()
        Dim reader As New StreamReader(stream)
        Dim responseContent As String = reader.ReadToEnd()

        Me.txtResponseGet.Text = String.Format("Status: {1}{0}Content: {2}", Environment.NewLine, responseStatus, responseContent)
        Me.llRedemptionUrl.Text = XDocument.Parse(responseContent).Root.Value

        reader.Close()
        response.Close()

    End Sub

    Private Sub Client_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.cboApplet.SelectedIndex = 0

        Me.cboEmployee.DataSource = From item In db.E__EMPLOYEE Order By item.LASTNAME Ascending
        Me.cboEmployee.DisplayMember = "FULLNAME"
        Me.cboCustomer.ValueMember = "EMPLOYEEID"

        Me.cboCustomer.DisplayMember = "FULLNAME"
        Me.cboCustomer.ValueMember = "CUSTOMERID"
        Me.cboCustomer.DataSource = From cust In db.C__CUSTOMER
                                    Join ten In db.C_CUSTOMERTENANCY On cust.CUSTOMERID Equals ten.CUSTOMERID
                                    Where ten.ENDDATE.HasValue = False
                                    Order By cust.LASTNAME Ascending
                                    Select cust

        Me.PopulateQueryString()
        Me.PopulateNotifyParameters()

        AddHandler Me.cboCustomer.SelectedIndexChanged, AddressOf Combo_SelectedIndexChanged
        AddHandler Me.cboEmployee.SelectedIndexChanged, AddressOf Combo_SelectedIndexChanged
        AddHandler Me.cboApplet.SelectedIndexChanged, AddressOf Combo_SelectedIndexChanged

        Me.wbBrowser.Navigate(New System.Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "TestCardInfo.htm")).AbsoluteUri)
        Me.lblConnectionString.Text = ConfigurationManager.ConnectionStrings.Item(0).ConnectionString

        Select Case True
            Case Me.lblConnectionString.Text.Contains("BHG-EBTEST")
                Me.lblConnectionString.ForeColor = Color.Blue
            Case Me.lblConnectionString.Text.Contains("BHG-CRMLIVE")
                Me.lblConnectionString.ForeColor = Color.Red
            Case Me.lblConnectionString.Text.Contains("BHG-EBDEV")
                Me.lblConnectionString.ForeColor = Color.Green
            Case Me.lblConnectionString.Text.Contains("BHG-DEV1")
                Me.lblConnectionString.ForeColor = Color.Yellow
            Case Me.lblConnectionString.Text.Contains("(local)")
                Me.ColourConnectionString()
            Case Else
                Me.lblConnectionString.BackColor = Color.Red
                Me.lblConnectionString.ForeColor = Color.White
                MessageBox.Show("!!!!Check connection string is correct!!!!")
        End Select

    End Sub

    Private Sub ColourConnectionString()

        Select Case True
            Case Environment.MachineName = "BHG-EBTEST"
                Me.lblConnectionString.ForeColor = Color.Blue
            Case Environment.MachineName = "BHG-CRMLIVE"
                Me.lblConnectionString.ForeColor = Color.Red
            Case Environment.MachineName = "BHG-EBDEV"
                Me.lblConnectionString.ForeColor = Color.Green
            Case Environment.MachineName = "BHG-DEV1"
                Me.lblConnectionString.ForeColor = Color.Yellow
            Case Else
                Me.lblConnectionString.BackColor = Color.Red
                Me.lblConnectionString.ForeColor = Color.White
                MessageBox.Show("!!!!Check connection string is correct!!!!")
        End Select

    End Sub


    Private Sub Combo_SelectedIndexChanged(sender As Object, e As System.EventArgs)
        Me.PopulateQueryString()
    End Sub

    Private Sub PopulateQueryString()
        Dim customerId As Integer = CType(Me.cboCustomer.SelectedValue, Integer)
        Dim employeeId As Integer = CType(Me.cboEmployee.SelectedValue, E__EMPLOYEE).EMPLOYEEID

        Dim item1 = From ten In db.C_CUSTOMERTENANCY
                  Join t In db.C_TENANCY On ten.TENANCYID Equals t.TENANCYID
                  Join P In db.P__PROPERTY On P.PROPERTYID Equals t.PROPERTYID
                  Where ten.CUSTOMERID = customerId
                  Select New With {.Property = P, .TenancyId = t.TENANCYID}

        Dim item = item1.FirstOrDefault

        Dim addressParts() As String = New String() {item.Property.HOUSENUMBER, item.Property.ADDRESS1, item.Property.ADDRESS2, item.Property.ADDRESS3, item.Property.TOWNCITY, item.Property.COUNTY}

        Me.txtAddress.Text = String.Join(Environment.NewLine, addressParts)
        Me.txtTenancyId.Text = item.TenancyId
        Me.txtProperty.Text = item.Property.PROPERTYID
        Me.txtQueryString.Text = String.Format(QueryString, item.TenancyId, customerId, employeeId, Me.cboApplet.SelectedItem)
    End Sub

    Private Sub btnGenerateString_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerateString.Click

        Try

            Dim query As String = String.Join("&", (From item In Me.bsParameters
                    Select String.Format("{0}={1}", CType(item, NotifyParameters).Name, CType(item, NotifyParameters).Value)).ToArray())



            query = HttpUtility.UrlEncode(query)

            Me.txtParameters.Text = query

        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try

    End Sub

    Private Sub PopulateNotifyParameters()

        Me.bsParameters.Add(New NotifyParameters With {.Name = "addressPropertyName", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "address1", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "address2", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "address3", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "addressTown", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "addressCounty", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "addressPostcode", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "addressCountry", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "cardLastFour", .Value = "2129"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "cardExpiryMonth", .Value = "11"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "cardExpiryYear", .Value = "14"})

        Me.bsParameters.Add(New NotifyParameters With {.Name = "items0Name", .Value = "rent"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "items0Price", .Value = "100"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "items0Quantity", .Value = "1"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "items0Reference", .Value = ""})

        Me.bsParameters.Add(New NotifyParameters With {.Name = "personTitle", .Value = "Ms"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "personFirstName", .Value = "Nataliya"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "personSurname", .Value = "Alexander"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "personMobileNumber", .Value = ""})

        Me.bsParameters.Add(New NotifyParameters With {.Name = "personHomeNumber", .Value = ""})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "personEmail", .Value = "nataliya.alexander@broadlandgroup.org"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionKey", .Value = "5b95cf6f8319813aa9cd8c421da2a3bd"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionTime", .Value = "2013-10-22T11:25:44+01:00"})

        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionStatus", .Value = "Success"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionValue", .Value = "1.00"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionType", .Value = "Auth"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionCurrency", .Value = "GBP"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionAuthCode", .Value = "20200"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionClass", .Value = "website"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionReference", .Value = "28AB78A3-3576-43EB-959E-F80C8FDC3B5F"})

        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionToken", .Value = "nyRzz8do"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionMaid", .Value = "0"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "transactionIpAddress", .Value = "213.120.95.250"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "custom_tenancy_ref", .Value = "40061"})
        Me.bsParameters.Add(New NotifyParameters With {.Name = "custom_customer_ref", .Value = "4272"})

    End Sub

    Private Sub llRedemptionUrl_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles llRedemptionUrl.LinkClicked
        Process.Start(Me.llRedemptionUrl.Text)
    End Sub

End Class