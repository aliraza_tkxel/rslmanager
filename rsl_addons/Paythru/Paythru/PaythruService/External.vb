﻿
Imports System.IO
Imports Paythru.Core
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class External
    Implements IExternal

    Public Sub Notify(input As Stream) Implements IExternal.Notify

        Try
      
            Dim processor As New NotifyProcessor
            processor.Process(input)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            If (rethrow) Then
                Throw
            End If

        End Try

    End Sub

End Class
