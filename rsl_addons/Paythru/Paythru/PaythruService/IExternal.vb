﻿
Imports System.IO
Imports System.ServiceModel.Web

<ServiceContract()>
Public Interface IExternal

    <OperationContract()> _
    <WebInvoke(Method:="POST", UriTemplate:="notify")> _
    Sub Notify(ByVal input As Stream)

End Interface