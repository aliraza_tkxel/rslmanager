﻿
Imports Paythru.Core
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Internal
    Implements IInternal

    Public Function GetUrl(ByVal tenancyId As String,
                           ByVal customerId As String,
                           ByVal employeeId As String,
                           ByVal applet As String) As String Implements IInternal.GetUrl

        Try

            Dim processor As New UrlProcessor
            Return processor.Process(tenancyId, customerId, employeeId, applet)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            If (rethrow) Then
                Throw
            End If

            Return Nothing

        End Try

    End Function

End Class