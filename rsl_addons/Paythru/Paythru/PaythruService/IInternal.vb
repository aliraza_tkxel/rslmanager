﻿
Imports System.ServiceModel.Web

<ServiceContract()>
Public Interface IInternal

    <OperationContract()> _
    <WebGet(UriTemplate:="geturl?tenancyId={tenancyId}&customerId={customerId}&applet={applet}&employeeId={employeeId}")> _
    Function GetUrl(ByVal tenancyId As String,
                    ByVal customerId As String,
                    ByVal employeeId As String,
                    ByVal applet As String) As String


End Interface
