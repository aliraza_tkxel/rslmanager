﻿Public Class AddressHelper

    Private Property Housenumber As String
    Private Property Address1 As String
    Private Property Address2 As String
    Private Property Address3 As String
    Private Property TownCity As String
    Private Property County As String
    Private Property Postcode As String

    Sub New(ByVal houseNumber As String, ByVal address1 As String, ByVal address2 As String, ByVal address3 As String, ByVal townCity As String, ByVal county As String, ByVal postCode As String)

        Me.Housenumber = houseNumber
        Me.Address1 = address1
        Me.Address2 = address2
        Me.Address3 = address3
        Me.TownCity = townCity
        Me.County = county
        Me.Postcode = postCode

    End Sub

    Public ReadOnly Property Address() As String
        Get

            Dim addressLine1 As String = String.Join(" ", New String() {Me.Housenumber.Trim, Me.Address1.Trim})
            Dim addressParts() As String = New String() {addressLine1, Me.Address2, Me.Address3, Me.TownCity, Me.County, Me.Postcode}

            Return String.Join(", ", addressParts.Where(Function(item) Not String.IsNullOrWhiteSpace(item)).Select(Function(item) item.Trim))

        End Get
    End Property

End Class

