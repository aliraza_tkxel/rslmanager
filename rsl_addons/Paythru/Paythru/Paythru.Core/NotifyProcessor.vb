﻿
Imports System.IO
Imports System.Web
Imports Paythru.Data

Public Class NotifyProcessor

    Sub New()
        Me.Log = New F_PAYTHRU_CALLBACK_LOG
        Me.Context = New PaythruEntities
    End Sub

    Private Property Log As F_PAYTHRU_CALLBACK_LOG
    Private Property Context As PaythruEntities

    Public Sub Process(ByVal data As Stream)

        Trace.Write("NotifyProcessor|Process|Started")

        Try

            Dim helper As New NotifyHelper
            Dim payload As String = helper.ReadStream(data)
            payload = HttpUtility.UrlDecode(payload)

            Trace.Write(String.Format("NotifyProcessor|Process|Payload|{0}", payload))

            Dim parameters As Specialized.NameValueCollection = HttpUtility.ParseQueryString(payload)
            Dim log As F_PAYTHRU_CALLBACK_LOG = helper.PopulateCallbackLog(payload, parameters)
            helper.InsertCallbackLog(log, Me.Context)
            Dim responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG) = helper.GetResponseLog(log.TransactionId, Me.Context)

            If helper.IsValidTransaction(log.TransactionStatus, responseLog) Then

                Dim itemTypeId As Integer = helper.GetItemType(log.Items0name, Me.Context)
                Dim paymentTypeId As Integer = helper.GetPaymentTypeId(Me.Context)
                Dim createdBy As Integer = helper.GetDefaultUserId(Me.Context)
                Dim officeId As Integer = helper.GetOfficeId(Me.Context)

                Dim journal As F_RENTJOURNAL = helper.PopulateRentJournal(log, itemTypeId, paymentTypeId)
                Dim posting As F_CASHPOSTING = helper.PopulateCashPosting(log, journal, itemTypeId, officeId, paymentTypeId, createdBy)

                helper.ValidateJournal(journal)
                helper.ValidateCashPosting(posting)
                helper.SavePayment(journal, posting, Me.Context)

                helper.UpdateProcessDate(log)

            End If


            helper.UpdateCallbackLog(log, Me.Context)

        Catch ex As Exception

            Trace.Write(String.Format("NotifyProcessor|Process|Exception|{0}", ex.ToString()))

            Throw

        End Try

        Trace.Write("NotifyProcessor|Process|Finished")

    End Sub

End Class