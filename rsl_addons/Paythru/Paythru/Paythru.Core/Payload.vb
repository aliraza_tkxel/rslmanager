﻿Imports Paythru.Data

Public Class Payload

    Private Const FORMAT_PAYLOAD_WEBSITE As String = "&success_url={0}&cancel_url={1}&callback_url={2}&first_name={3}&surname={4}&email={5}&custom_tenancy_ref={6}&custom_customer_ref={7}&custom_tenant_property={8}&trans_reference={9}&maid={7}"
    Private Const FORMAT_PAYLOAD_STAFF As String = "&success_url={0}&cancel_url={1}&callback_url={2}&first_name={3}&surname={4}&email={5}&custom_tenancy_ref={6}&custom_customer_ref={7}&custom_staff_id={8}&custom_staff_name={9}&custom_tenant_property={10}&trans_reference={11}"

    Public Property SuccessUrl As String
    Public Property CancelUrl As String
    Public Property CallbackUrl As String
    Public Property FirstName As String
    Public Property LastName As String
    Public Property Email As String
    Public Property TenancyId As Integer
    Public Property CustomerId As Integer
    Public Property EmployeeFirstName As String
    Public Property EmployeeLastName As String
    Public Property EmployeeId As Integer
    Public Property TokenPayload As String
    Public Property PropertyId As String
    Public Property TokenUrl As String
    Public Property Housenumber As String
    Public Property Address1 As String
    Public Property Address2 As String
    Public Property Address3 As String
    Public Property TownCity As String
    Public Property County As String
    Public Property Postcode As String
    Public Property Applet As String
    Public Property DefaultEmail As String
    Public Property TransactionId As Guid

    Public ReadOnly Property AddressCountry As String
        Get
            Return "UK"
        End Get
    End Property

    Public ReadOnly Property EmployeeName As String
        Get
            Return String.Format("{0} {1}", Me.EmployeeFirstName.Trim, Me.EmployeeLastName.Trim).Trim
        End Get
    End Property

    Public Function GetPayload() As String

        Dim payload As String
        Dim addressHelper As New AddressHelper(Me.Housenumber, Me.Address1, Me.Address2, Me.Address3, Me.TownCity, Me.County, Me.Postcode)

        Select Case Me.Applet.ToLower()

            Case Constants.Website

                payload = String.Concat(TokenPayload, String.Format(FORMAT_PAYLOAD_WEBSITE, New Object() {SuccessUrl,
                                                                                                         CancelUrl,
                                                                                                         CallbackUrl,
                                                                                                         FirstName,
                                                                                                         LastName,
                                                                                                         ConfirmationEmail,
                                                                                                         TenancyId,
                                                                                                         CustomerId,
                                                                                                         addressHelper.Address,
                                                                                                         TransactionId}))

            Case Constants.Staff

                payload = String.Concat(TokenPayload, String.Format(FORMAT_PAYLOAD_STAFF, New Object() {SuccessUrl,
                                                                                                        CancelUrl,
                                                                                                        CallbackUrl,
                                                                                                        FirstName,
                                                                                                        LastName,
                                                                                                        ConfirmationEmail,
                                                                                                        TenancyId,
                                                                                                        CustomerId,
                                                                                                        EmployeeId,
                                                                                                        EmployeeName,
                                                                                                        addressHelper.Address,
                                                                                                        TransactionId}))
            Case Else

                Throw New AppletNotFoundException

        End Select

        Return payload

    End Function

    Public ReadOnly Property ConfirmationEmail() As String
        Get
            If String.IsNullOrWhiteSpace(Me.Email) Then
                Return Me.DefaultEmail
            Else
                Return Me.Email
            End If
        End Get
    End Property

End Class

Public Class AppletNotFoundException
    Inherits Exception

End Class