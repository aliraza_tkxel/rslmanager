﻿
Imports System.Text
Imports System.IO
Imports System.Net
Imports Paythru.Data

Public Class UrlHelper

    Public Function Post(ByVal url As String, ByVal payload As String) As PostResponse

        Dim postBin() As Byte = Nothing
        Dim postResponse As New PostResponse

        Trace.Write(String.Format("{0}|{1}", url, payload))

        postBin = Encoding.GetEncoding("UTF-8").GetBytes(payload)

        Dim request As WebRequest = WebRequest.Create(url)

        request.Method = "post"
        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = postBin.Length

        Using requestStream As Stream = request.GetRequestStream

            requestStream.Write(postBin, 0, postBin.Length)
            requestStream.Flush()
            requestStream.Close()

            Using response As WebResponse = request.GetResponse

                postResponse.Status = (CType(response, HttpWebResponse).StatusDescription)

                Using reader As StreamReader = New StreamReader(response.GetResponseStream())

                    postResponse.Content = reader.ReadToEnd
                    reader.Close()

                End Using

                response.Close()

            End Using

        End Using

        Return postResponse

    End Function

    Public Sub SaveRequestLog(ByVal log As F_PAYTHRU_GET_TOKEN_REQUEST_LOG,
                               ByVal context As PaythruEntities)

        context.F_PAYTHRU_GET_TOKEN_REQUEST_LOG.AddObject(log)
        context.SaveChanges()

    End Sub

    Public Function PopulateRequestLog(ByVal payload As Payload) As F_PAYTHRU_GET_TOKEN_REQUEST_LOG

        Dim log As New F_PAYTHRU_GET_TOKEN_REQUEST_LOG

        log.TransactionId = payload.TransactionId
        log.RequestUrl = payload.TokenUrl
        log.Applet = payload.Applet
        log.SuccessUrl = payload.SuccessUrl
        log.CancelUrl = payload.CancelUrl
        log.CallbackUrl = payload.CallbackUrl
        log.FirstName = payload.FirstName
        log.Surname = payload.LastName
        log.Email = payload.ConfirmationEmail
        log.Maid = payload.CustomerId.ToString
        log.CustomTenancyRef = payload.TenancyId.ToString
        log.CustomCustomerRef = payload.CustomerId.ToString
        log.CustomStaffName = payload.EmployeeName
        log.CustomStaffId = payload.EmployeeId.ToString
        log.AddressPropertyName = payload.Housenumber
        log.Address1 = payload.Address1
        log.Address2 = payload.Address2
        log.Address3 = payload.Address3
        log.AddressTown = payload.TownCity
        log.AddressCounty = payload.County
        log.AddressPostcode = payload.Postcode
        log.AddressCountry = payload.AddressCountry
        log.DateSubmitted = DateTime.Now
        log.CreatedBy = Environment.UserName

        log.Payload = payload.GetPayload()

        Return log

    End Function


    Public Function PopulateConfig(ByVal context As PaythruEntities) As F_PAYTHRU_CONFIG()

        Return context.SelectConfigAll().ToArray()

    End Function

    Public Function PopulatePayload(ByVal customer As C__CUSTOMER,
                                    ByVal email As String,
                                    ByVal employee As E__EMPLOYEE,
                                    ByVal tenancy As C_TENANCY,
                                    ByVal prop As P__PROPERTY,
                                    ByVal applet As String,
                                    ByVal config As F_PAYTHRU_CONFIG()) As Payload

        Dim nameFormat As String = "{0}_{1}"

        Dim payload As New Payload With {
                                         .SuccessUrl = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "SuccessUrl", applet)).First().SettingValue,
                                         .CancelUrl = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "CancelUrl", applet)).First().SettingValue,
                                         .CallbackUrl = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "CallbackUrl", applet)).First().SettingValue,
                                         .FirstName = customer.FIRSTNAME,
                                         .LastName = customer.LASTNAME,
                                         .Email = email,
                                         .TenancyId = tenancy.TENANCYID,
                                         .CustomerId = customer.CUSTOMERID,
                                         .TokenUrl = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "TokenUrl", applet)).First().SettingValue,
                                         .PropertyId = prop.PROPERTYID,
                                         .TokenPayload = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "TokenPayload", applet)).First().SettingValue,
                                         .Address1 = prop.ADDRESS1,
                                         .Address2 = prop.ADDRESS2,
                                         .Address3 = prop.ADDRESS3,
                                         .County = prop.COUNTY,
                                         .Housenumber = prop.HOUSENUMBER,
                                         .Postcode = prop.POSTCODE,
                                         .TownCity = prop.TOWNCITY,
                                         .Applet = applet,
                                         .DefaultEmail = config.Where(Function(item) item.SettingName = String.Format(nameFormat, "DefaultEmail", applet)).First().SettingValue,
                                         .TransactionId = Guid.NewGuid()
                                        }

        If applet = Constants.Staff Then
            payload.EmployeeId = employee.EMPLOYEEID
            payload.EmployeeLastName = employee.LASTNAME
            payload.EmployeeFirstName = employee.FIRSTNAME
        Else
            payload.EmployeeId = 0
            payload.EmployeeLastName = String.Empty
            payload.EmployeeFirstName = String.Empty
        End If

        Return payload

    End Function

    Public Function PopulateCustomer(ByVal customerId As Integer, ByVal context As PaythruEntities) As C__CUSTOMER

        Return context.SelectCustomer(customerId).First()

    End Function

    Public Function PopulateEmployee(ByVal employeeId As Integer, ByVal context As PaythruEntities) As E__EMPLOYEE

        Return context.SelectEmployee(employeeId).First()

    End Function

    Public Function PopulateTenancy(ByVal tenancyId As Integer, ByVal context As PaythruEntities) As C_TENANCY

        Return context.SelectTenancy(tenancyId).First()

    End Function

    Public Function PopulateAddress(ByVal customerId As Integer, ByVal context As PaythruEntities) As C_ADDRESS

        Return context.SelectCustomerAddress(customerId).FirstOrDefault()

    End Function

    Public Function PopulateEmail(ByVal address As C_ADDRESS) As String

        If address IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(address.EMAIL) Then
            Return address.EMAIL
        Else
            Return String.Empty
        End If

    End Function

    Public Function PopulateProperty(ByVal propertyId As String, ByVal context As PaythruEntities) As P__PROPERTY

        Return context.SelectProperty(propertyId).First()

    End Function

    Public Function PopulateResponseLog(ByVal postData As PostResponse, ByVal transactionId As Guid) As F_PAYTHRU_GET_TOKEN_RESPONSE_LOG

        Dim log As New F_PAYTHRU_GET_TOKEN_RESPONSE_LOG

        log.TransactionId = transactionId
        log.Payload = XDocument.Parse(postData.Content).ToString
        log.DateReceived = DateTime.Now
        log.CreatedBy = Environment.UserName
        log.DateCreated = DateTime.Now
        log.RedemptionUrl = postData.GetRedemptionUrl

        Return log

    End Function

    Public Sub SaveResponseLog(ByVal log As F_PAYTHRU_GET_TOKEN_RESPONSE_LOG, ByVal context As PaythruEntities)

        context.F_PAYTHRU_GET_TOKEN_RESPONSE_LOG.AddObject(log)
        context.SaveChanges()

    End Sub

End Class