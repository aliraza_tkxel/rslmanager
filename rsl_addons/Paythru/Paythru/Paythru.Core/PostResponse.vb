﻿
Imports System.Xml.Linq
Imports System.Xml.XPath

Public Class PostResponse

    Public Status As String
    Public Content As String

    Public Function GetRedemptionUrl() As String

        Dim document As XDocument = XDocument.Parse(Me.Content)
        Dim url As String = document.XPathSelectElement("//redemption_url").Value

        Return url

    End Function

End Class