﻿Imports System.IO
Imports Paythru.Data

Public Class NotifyHelper

    Public Function ReadStream(ByVal input As Stream) As String

        Dim reader As New StreamReader(input)
        Return reader.ReadToEnd()

    End Function

    Public Sub InsertCallbackLog(ByVal log As F_PAYTHRU_CALLBACK_LOG, ByVal context As PaythruEntities)

        context.F_PAYTHRU_CALLBACK_LOG.AddObject(log)
        context.SaveChanges()

    End Sub

    Public Sub UpdateCallbackLog(ByVal log As F_PAYTHRU_CALLBACK_LOG, ByVal context As PaythruEntities)

        context.SaveChanges()

    End Sub

    Public Function IsValidTransaction(ByVal transactionStatus As String, ByVal responseLog As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)) As Boolean

        Dim isValid As Boolean = False

        If transactionStatus IsNot Nothing AndAlso transactionStatus.ToLower() = "success" Then

            If responseLog IsNot Nothing AndAlso responseLog.ToList().Count > 0 Then

                isValid = True

            End If

        End If

        Return isValid

    End Function

    Public Function GetResponseLog(ByVal transactionId As System.Guid?, ByVal context As PaythruEntities) As IEnumerable(Of F_PAYTHRU_GET_TOKEN_RESPONSE_LOG)

        Return From item In context.SelectResponseLogByTransactionId(transactionId)

    End Function

    Public Function PopulateCallbackLog(ByVal payload As String, ByVal parameters As Specialized.NameValueCollection) As F_PAYTHRU_CALLBACK_LOG

        Dim log As New F_PAYTHRU_CALLBACK_LOG

        log.CreatedBy = Environment.UserName
        log.DateReceived = DateTime.Now
        log.Payload = String.Join("&", (From item In payload.Split("&") Select item Where Not (item.StartsWith("cardLastFour") OrElse item.StartsWith("cardExpiryMonth") OrElse item.StartsWith("cardExpiryYear"))).ToArray())
        log.CustomCustomerRef = parameters.Item("custom_customer_ref")
        log.CustomStaffId = parameters.Item("custom_staff_id")
        log.CustomStaffName = parameters.Item("custom_staff_name")
        log.CustomTenancyRef = parameters.Item("custom_tenancy_ref")
        log.Items0name = parameters.Item("items0Name")
        log.Items0price = parameters.Item("items0Price")
        log.Items0quantity = parameters.Item("items0Quantity")
        log.Items0reference = parameters.Item("items0Reference")
        log.PersonEmail = parameters.Item("personEmail")
        log.PersonFirstName = parameters.Item("personFirstName")
        log.PersonSurname = parameters.Item("personSurname")
        log.PersonHomePhone = parameters.Item("personHomePhone")
        log.PersonMobileNumber = parameters.Item("personMobileNumber")
        log.PersonTitle = parameters.Item("personTitle")
        log.TransactionAuthCode = parameters.Item("transactionAuthCode")
        log.TransactionClass = parameters.Item("transactionClass")
        log.TransactionCurrency = parameters.Item("transactionCurrency")
        log.TransactionIpAddress = parameters.Item("transactionIpAddress")
        log.TransactionId = Guid.Parse("{00000000-0000-0000-0000-000000000000}")
        Guid.TryParse(parameters.Item("transactionReference"), log.TransactionId)
        log.TransactionKey = parameters.Item("transactionKey")
        log.TransactionMaid = parameters.Item("transactionMaid")
        log.TransactionStatus = parameters.Item("transactionStatus")
        log.TransactionTime = parameters.Item("transactionTime").Replace(" ", "+")
        log.TransactionToken = parameters.Item("transactionToken")
        log.TransactionType = parameters.Item("transactionType")
        log.TransactionValue = parameters.Item("transactionValue")
        log.DateCreated = DateTime.Now

        Return log

    End Function

    Public Sub UpdateProcessDate(ByVal log As F_PAYTHRU_CALLBACK_LOG)

        log.DateProcessed = DateTime.Now

    End Sub

    Public Function PopulateRentJournal(ByVal log As F_PAYTHRU_CALLBACK_LOG, ByVal itemTypeId As Integer, ByVal paymentTypeId As Integer) As F_RENTJOURNAL

        Dim journal As New F_RENTJOURNAL
        journal.TENANCYID = Integer.Parse(log.CustomTenancyRef)
        journal.TRANSACTIONDATE = DateTime.Parse(log.TransactionTime).Date
        journal.ITEMTYPE = itemTypeId
        journal.PAYMENTTYPE = paymentTypeId
        journal.AMOUNT = Decimal.Parse(log.Items0price) * -1
        journal.STATUSID = 2
        journal.ACCOUNTTIMESTAMP = DateTime.Now
        journal.ISDEBIT = 1

        Return journal

    End Function

    Public Sub ValidateJournal(ByVal journal As F_RENTJOURNAL)

        If journal.TENANCYID <= 0 Then
            Throw New Exception(String.Format("Invalid TENANCYID in F_RENTJOURNAL. TENANCYID is {0}", journal.TENANCYID))
        End If

        If journal.ITEMTYPE.HasValue = False Then
            Throw New Exception("Invalid ITEMTYPE in F_RENTJOURNAL. ITEMTYPE is null.")
        End If

        If journal.ITEMTYPE.HasValue = True AndAlso journal.ITEMTYPE.Value <= 0 Then
            Throw New Exception(String.Format("Invalid ITEMTYPE in F_RENTJOURNAL. ITEMTYPE is {0}", journal.ITEMTYPE.Value))
        End If

        If journal.PAYMENTTYPE.HasValue = False Then
            Throw New Exception("Invalid PAYMENTTYPE in F_RENTJOURNAL. PAYMENTTYPE is null.")
        End If

        If journal.PAYMENTTYPE.HasValue = True AndAlso journal.PAYMENTTYPE.Value <= 0 Then
            Throw New Exception(String.Format("Invalid PAYMENTTYPE in F_RENTJOURNAL. PAYMENTTYPE is {0}", journal.PAYMENTTYPE.Value))
        End If

    End Sub

    Public Function PopulateCashPosting(ByVal log As F_PAYTHRU_CALLBACK_LOG,
                                        ByVal journal As F_RENTJOURNAL,
                                        ByVal itemTypeId As Integer,
                                        ByVal officeId As Integer,
                                        ByVal paymentTypeId As Integer,
                                        ByVal defaultUserId As Integer) As F_CASHPOSTING

        Dim posting As New F_CASHPOSTING
        Dim createdBy As Integer = defaultUserId

        If Not String.IsNullOrEmpty(log.CustomStaffId) Then
            createdBy = Integer.Parse(log.CustomStaffId)
        End If

        posting.CREATEDBY = createdBy
        posting.CREATIONDATE = DateTime.Parse(log.TransactionTime).Date
        posting.RECEIVEDFROM = String.Format("{0} {1}", log.PersonFirstName, log.PersonSurname)
        posting.AMOUNT = Decimal.Parse(log.Items0price)
        posting.OFFICE = officeId
        posting.ITEMTYPE = itemTypeId
        posting.PAYMENTTYPE = paymentTypeId
        posting.TENANCYID = Integer.Parse(log.CustomTenancyRef)
        posting.CUSTOMERID = Integer.Parse(log.CustomCustomerRef)
        posting.CASHPOSTINGORIGIN = 1
        posting.F_RENTJOURNAL = journal

        Return posting

    End Function

    Public Sub ValidateCashPosting(ByVal posting As F_CASHPOSTING)

        If posting.TENANCYID.HasValue = False Then
            Throw New Exception("Invalid TENANCYID in F_CASHPOSTING. TENANCYID is null.")
        End If

        If posting.TENANCYID.HasValue = True AndAlso posting.TENANCYID.Value <= 0 Then
            Throw New Exception(String.Format("Invalid TENANCYID in F_CASHPOSTING. TENANCYID is {0}", posting.TENANCYID.Value))
        End If

        If posting.CUSTOMERID.HasValue = False Then
            Throw New Exception("Invalid CUSTOMERID in F_CASHPOSTING. CUSTOMERID is null.")
        End If

        If posting.CUSTOMERID.HasValue = True AndAlso posting.CUSTOMERID.Value <= 0 Then
            Throw New Exception(String.Format("Invalid CUSTOMERID in F_CASHPOSTING. CUSTOMERID is {0}", posting.CUSTOMERID.Value))
        End If

        If posting.F_RENTJOURNAL Is Nothing Then
            Throw New Exception("Invalid F_RENTJOURNAL in F_CASHPOSTING. F_RENTJOURNAL is null.")
        End If

        If posting.ITEMTYPE <= 0 Then
            Throw New Exception(String.Format("Invalid ITEMTYPE in F_CASHPOSTING. ITEMTYPE is {0}", posting.ITEMTYPE))
        End If

        If posting.PAYMENTTYPE <= 0 Then
            Throw New Exception(String.Format("Invalid PAYMENTTYPE in F_CASHPOSTING. PAYMENTTYPE is {0}", posting.PAYMENTTYPE))
        End If

        If posting.OFFICE <= 0 Then
            Throw New Exception(String.Format("Invalid OFFICE in F_CASHPOSTING. OFFICE is {0}", posting.OFFICE))
        End If

    End Sub

    Public Function GetItemType(ByVal Description As String, ByVal context As PaythruEntities) As Integer

        Return context.SelectItemType(Description).First().ItemTypeId

    End Function

    Public Function GetOfficeId(ByVal context As PaythruEntities) As Integer

        Return Integer.Parse(context.SelectConfigByName("OfficeId").First().SettingValue)

    End Function

    Public Function GetDefaultUserId(ByVal context As PaythruEntities) As Integer

        Return Integer.Parse(context.SelectConfigByName("DefaultUserId").First().SettingValue)

    End Function

    Public Function GetPaymentTypeId(ByVal context As PaythruEntities) As Integer

        Return Integer.Parse(context.SelectConfigByName("PaymentTypeId").First().SettingValue)

    End Function

    Public Sub SavePayment(ByVal journal As F_RENTJOURNAL, ByVal posting As F_CASHPOSTING, ByVal context As PaythruEntities)

        context.F_RENTJOURNAL.AddObject(journal)
        context.F_CASHPOSTING.AddObject(posting)
        context.SaveChanges()

    End Sub

End Class