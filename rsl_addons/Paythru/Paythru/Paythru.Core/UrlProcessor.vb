﻿Imports Paythru.Data

Public Class UrlProcessor

    Sub New()
        Me.Context = New PaythruEntities
    End Sub

    Private Property Context As PaythruEntities

    Public Function Process(ByVal tenancyId As String,
                            ByVal customerId As String,
                            ByVal employeeId As String,
                            ByVal applet As String) As String

        Trace.Write("UrlProcessor|Process|Started")

        Try

            Trace.Write(String.Format("UrlProcessor|Process|tenancyId={0}|customerId={1}|employeeId={2}|applet={3}", tenancyId, customerId, employeeId, applet))

            Dim helper As New UrlHelper
            Dim config As F_PAYTHRU_CONFIG() = helper.PopulateConfig(Me.Context)
            Dim customer As C__CUSTOMER = helper.PopulateCustomer(Integer.Parse(customerId), Me.Context)
            Dim address As C_ADDRESS = helper.PopulateAddress(Integer.Parse(customerId), Me.Context)
            Dim email As String = helper.PopulateEmail(address)
            Dim employee As E__EMPLOYEE = Nothing

            If applet = Constants.Staff Then
                employee = helper.PopulateEmployee(Integer.Parse(employeeId), Me.Context)
            End If

            Dim tenancy As C_TENANCY = helper.PopulateTenancy(Integer.Parse(tenancyId), Me.Context)
            Dim prop As P__PROPERTY = helper.PopulateProperty(tenancy.PROPERTYID, Me.Context)
            Dim payload As Payload = helper.PopulatePayload(customer, email, employee, tenancy, prop, applet, config)
            Dim requestLog As F_PAYTHRU_GET_TOKEN_REQUEST_LOG = helper.PopulateRequestLog(payload)

            helper.SaveRequestLog(requestLog, Me.Context)

            Dim postData As PostResponse = helper.Post(requestLog.RequestUrl, requestLog.Payload)
            Dim responseLog As F_PAYTHRU_GET_TOKEN_RESPONSE_LOG = helper.PopulateResponseLog(postData, requestLog.TransactionId)

            helper.SaveResponseLog(responseLog, Me.Context)

            Return responseLog.RedemptionUrl

        Catch ex As Exception

            Trace.Write(String.Format("UrlProcessor|Process|Exception|{0}", ex.ToString()))

            Throw

        End Try

        Trace.Write("UrlProcessor|Process|Finished")

    End Function

End Class