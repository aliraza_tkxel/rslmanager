﻿Imports System
Imports System.Data
Imports PropertyInspection_BusinessLogic
Imports PropertyInspection_BusinessObject
Imports PropertyInspection_Utilities
Imports System.Drawing
Imports System.Globalization
Imports System.Threading
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class AreaDetails
    Inherits System.Web.UI.Page
    Dim propertyID As String = "BHA0000119"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim resultDataSet As DataSet = New DataSet()
        Dim objInspectionRecordMainPageAreaBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL
        objInspectionRecordMainPageAreaBL.getAreaDetails(resultDataSet, propertyID)
        SessionManager.removeInspectionRecordDataSet()
        SessionManager.setInspectionRecordDataSet(resultDataSet)
        rptCheckListLocationName.DataSource = resultDataSet
        rptCheckListLocationName.DataBind()


    End Sub

#Region "CheckList"
    Sub rptCheckListLocatioName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptCheckListLocationName.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.getInspectionRecordDataSet()
            Dim rptCheckListAreaName As Repeater = DirectCast(e.Item.FindControl("rptCheckListAreaName"), Repeater)
            'Dim rptCheckListItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)
            Dim lblCheckListLocationId As Label = DirectCast(e.Item.FindControl("lblCheckListLocationId"), Label)
            'filter TableArea where location id is lblCheckListLocationId.text 
            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables("TableArea")
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "LocationID =  " + lblCheckListLocationId.Text.ToString()
            filteredDt = areaDv.ToTable()

            rptCheckListAreaName.DataSource = filteredDt
            rptCheckListAreaName.DataBind()

            'Dim lblAreaId As Label = DirectCast(e.Item.FindControl("lblAreaId"), Label)
            'Dim LocationId As Integer = Convert.ToInt32(lblCheckListLocationId.Text)
            'Dim AreaId As Integer = Convert.ToInt32(lblAreaId.Text)
            'Dim AreaNameDt As DataTable = resultDataSet.Tables("TableArea")
            'Dim ItemNameDt As DataTable = resultDataSet.Tables("TableItem")
            'Dim filterExpL As String = "LocationID = " + Convert.ToString(LocationId)
            'Dim filterExpI As String = "AreaID = " + Convert.ToString(AreaId)
            'Dim drarray() As DataRow
            'Dim itemarray() As DataRow

            'drarray = AreaNameDt.Select(filterExpL)
            'itemarray = ItemNameDt.Select(filterExpI)
            'rptCheckListAreaName.DataSource = drarray.CopyToDataTable()
            'rptCheckListAreaName.DataBind()
            'rptCheckListItemName.DataSource = itemarray.CopyToDataTable()
            'rptCheckListItemName.DataBind()

        End If
    End Sub

    Sub rptCheckListAreaName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.getInspectionRecordDataSet()
            Dim rptCheckListItemName As Repeater = DirectCast(e.Item.FindControl("rptCheckListItemName"), Repeater)
            Dim lblCheckListAreaId As Label = DirectCast(e.Item.FindControl("lblCheckListAreaId"), Label)
            'Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListItemId"), Label)

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables("TableItem")
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "AreaID =  " + lblCheckListAreaId.Text.ToString()
            filteredDt = areaDv.ToTable()

            rptCheckListItemName.DataSource = filteredDt
            rptCheckListItemName.DataBind()


        End If
    End Sub

    Sub rptCheckListItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.getInspectionRecordDataSet()

            Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListItemId"), Label)
            Dim lblUpdated As Label = DirectCast(e.Item.FindControl("lblUpdated"), Label)
            Dim subItemResult = (From ps In resultDataSet.Tables("TableSubItem") Where ps.Item("ParentItemId").ToString() = lblCheckListItemId.Text.ToString() Select ps)
            If subItemResult.Count() > 0 Then
                'Dim filteredDt As DataTable = New DataTable()
                'Dim areaDt As DataTable = resultDataSet.Tables("TableUpdated")
                'Dim areaDv As DataView = areaDt.AsDataView()
                'areaDv.RowFilter = "ItemID = " + lblCheckListItemId.Text.ToString()
                'filteredDt = areaDv.ToTable()
                'If (filteredDt.Rows.Count > 1) Then
                '    lblUpdated.Text = "Yes"
                'Else
                '    lblUpdated.Text = "No"
                'End If

                Dim rptSubItem As Repeater = DirectCast(e.Item.FindControl("rptSubItem"), Repeater)
                Dim filteredSubItemDt As DataTable = New DataTable()
                Dim subItemDt As DataTable = resultDataSet.Tables("TableSubItem")
                Dim subItemDv As DataView = subItemDt.AsDataView()
                subItemDv.RowFilter = "ParentItemId = " + lblCheckListItemId.Text.ToString()
                filteredSubItemDt = subItemDv.ToTable()
                rptSubItem.DataSource = filteredSubItemDt
                rptSubItem.DataBind()

            Else
                Dim divCheckListSubItem As HtmlContainerControl = DirectCast(e.Item.FindControl("divCheckListSubItem"), HtmlContainerControl)
                divCheckListSubItem.Visible = False
                Dim filteredDt As DataTable = New DataTable()
                Dim areaDt As DataTable = resultDataSet.Tables("TableUpdated")
                Dim areaDv As DataView = areaDt.AsDataView()
                areaDv.RowFilter = "ItemID = " + lblCheckListItemId.Text.ToString()
                filteredDt = areaDv.ToTable()
                If (filteredDt.Rows.Count > 0) Then
                    lblUpdated.Text = "Yes"
                Else
                    lblUpdated.Text = "No"
                End If
            End If

        End If
    End Sub


    Sub rptCheckListSubItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.getInspectionRecordDataSet()

            Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListSubItemId"), Label)
            Dim lblUpdated As Label = DirectCast(e.Item.FindControl("lblUpdated"), Label)


            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables("TableUpdated")
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "ItemID = " + lblCheckListItemId.Text.ToString()
            filteredDt = areaDv.ToTable()
            If (filteredDt.Rows.Count > 0) Then
                lblUpdated.Text = "Yes"
            Else
                lblUpdated.Text = "No"
            End If


        End If
    End Sub
#End Region

End Class