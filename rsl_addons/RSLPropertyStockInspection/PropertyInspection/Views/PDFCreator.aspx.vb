﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports System.Xml
Imports System.Xml.Linq

Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing

Imports System.Configuration.Assemblies
Imports System.Collections
Imports System.Globalization

Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports EO.Pdf
Imports PropertyInspection_Utilities
Imports PropertyInspection_BusinessLogic
Imports log4net


Public Class PDFCreator
    Inherits System.Web.UI.Page
    Dim objInspectionRecordMainPageAreaBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim propertyId As String = Request.QueryString("PropertyId")
            Dim surveyId As String = Request.QueryString("SurveyId")
            Dim createdBy As String = Request.QueryString("CreatedBy")
            processDetailsDocument(propertyId, surveyId, createdBy)
        End If
    End Sub

#Region "populate PropertyRecordInspection"
    ''' <summary>
    ''' Populate PropertyRecordInspection
    ''' </summary>
    ''' <param name="pid"></param>
    ''' <param name="sid"></param>
    ''' <param name="uid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function populatePropertyRecordInspection(ByVal pid As String, ByVal sid As String, ByVal uid As String) As String

        Dim pdfCreator As PDFCreator = New PDFCreator()
        pdfCreator.processDetailsDocument(pid, sid, uid)
        Return "success"
    End Function
#End Region


    Public Sub processDetailsDocument(ByVal propertyId As String, ByVal surveyId As String, ByVal createdBy As String)

        Dim documentTemplateUrl As String = Me.ToAbsoluteUrl(String.Format("~/Views/Details.aspx?PropertyId={0}&SurveyId={1}&CreatedBy={2}", propertyId, surveyId, createdBy))

        '' Set file parameters
        Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")
        Dim detailsDocumentRootPath As String = Server.MapPath("\PDF\")
        Dim detailsDocumentName As String = "Inspection Record" + propertyId + "_" + uniqueString + ".pdf"
        Dim detailsDocumentPath As String = detailsDocumentRootPath + detailsDocumentName

        If (Directory.Exists(detailsDocumentRootPath) = False) Then
            Directory.CreateDirectory(detailsDocumentRootPath)
        End If

        Me.makeEoPdf(propertyId, detailsDocumentPath, documentTemplateUrl)
        Dim detailsInByteForm As Byte() = readDetailsDocument(detailsDocumentPath)
        objInspectionRecordMainPageAreaBL.saveDetailsDocument(propertyId, surveyId, detailsInByteForm, createdBy)

    End Sub

    Public Sub makeEoPdf(ByVal propertyId As String, ByVal detailsDocumentPath As String, ByVal documentTemplateUrl As String)

        'Convert the Url to PDF
        EO.Pdf.Runtime.AddLicense( _
            "S8+4iVmXpLHn4KXj8wjpjEOXpLHLn1mXpM0M452X+Aob5HaZyeDZz53dprEh" + _
            "5Kvq7QAZvFuovL/boVmmwp61n1mXpM3a4KXj8wjpjEOXpLHLu6jp6PYdyKfd" + _
            "87EP4K3cwbPhrm+mtsHct1uX9wYNxLHn7QMQ8nrrwbPhrm+mtsHcuFuX+vYd" + _
            "8qLm8s7NsHGZpMDpjEOXpLHLu6zg6/8M867p6c8SsKjb3Ovy1KCwuN4Q0qno" + _
            "u9sk97Huwc7nrqzg6/8M867p6c+4iXWm8PoO5Kfq6c+4iXXj7fQQ7azcwp61" + _
            "n1mXpM0X6Jzc8gQQyJ21uMHis3CotMjgtW+zs/0U4p7l9/b043eEjrHLn1mz" + _
            "8PoO5Kfq6fbpu2jj7fQQ7azc6Q==")

        'HtmlToPdf.Options.OutputArea = New RectangleF(0.0F, 0.5F, 8.0F, 20.0F)
        HtmlToPdf.Options.PageSize = PdfPageSizes.A4
        Dim resultDataSet As DataSet = New DataSet()
        Dim objInspectionRecordMainPageAreaBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL
        objInspectionRecordMainPageAreaBL.getMainPageContent(resultDataSet, propertyId)
        Dim propertyAddress As String = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.PropertyAddressColumn)
        propertyAddress = "<div style=' font-family: Arial; font-size: 11px;float:left; background-color:#fff;  height:55px;'>" + propertyAddress + "</div>"
        Dim headerWhiteout As String = "<div style='font-family: Arial; font-size: 11px; float:right; text-align:right; background-color:#fff; width:100px; height:55px;'>" + "{page_number}" + "</div>"
        EO.Pdf.HtmlToPdf.Options.FooterHtmlFormat = propertyAddress + headerWhiteout
        HtmlToPdf.ConvertUrl(documentTemplateUrl, detailsDocumentPath)

    End Sub


    Private Function readDetailsDocument(ByVal detailsDocumentPath As String)
        Dim detailsInByteForm As Byte()
        Using stream = New FileStream(detailsDocumentPath, FileMode.Open, FileAccess.Read)
            Using reader = New BinaryReader(stream)
                detailsInByteForm = reader.ReadBytes(CInt(stream.Length))
            End Using
        End Using
        Return detailsInByteForm
    End Function

    Public Function ToAbsoluteUrl(ByVal relativeUrl As String) As String
        If String.IsNullOrEmpty(relativeUrl) Then
            Return relativeUrl
        End If

        If HttpContext.Current Is Nothing Then
            Return relativeUrl
        End If

        If relativeUrl.StartsWith("/") Then
            relativeUrl = relativeUrl.Insert(0, "~")
        End If
        If Not relativeUrl.StartsWith("~/") Then
            relativeUrl = relativeUrl.Insert(0, "~/")
        End If

        Dim url = HttpContext.Current.Request.Url
        Dim port = If(url.Port <> 80, (":" + url.Port.ToString()), [String].Empty)

        Return [String].Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl))
    End Function
End Class