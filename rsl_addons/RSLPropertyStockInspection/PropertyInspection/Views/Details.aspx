﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Details.aspx.vb" Inherits="PropertyInspection.Details" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Record of Inspection</title>
    <style type="text/css">
        table
        {
            border-collapse: collapse !important;
            border-spacing: 0;
        }
        body
        {
            font-family: Arial;
            font-size: 11px;
        }
        table > tbody > tr > td > table > tbody > tr > td > table td:nth-child(1)
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        
        
        #tblPropertyDetails
        {
            margin-bottom: 10px;
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;
        }
        .td-main-detail
        {
            width: 30%;
            border: 1px solid black;
            border-collapse: collapse;
            padding-left: 5px;
        }
        .td-main-detail-value
        {
            width: 50%;
            border: 1px solid black;
            border-collapse: collapse;
            height: 25px;
            padding-left: 5px;
        }
        .td-photograph
        {
            width: 20%;
            text-align: center;
            border: 1px solid black;
            border-collapse: collapse;
        }
        
        
        
        
        
        
        
        #tblAsbestosDetails
        {
            margin-bottom: 20px;
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;
        }
        .td-asbestos-risk
        {
            width: 30%;
            padding-left: 5px;
            border: 1px solid black;
            border-collapse: collapse;
        }
        .td-asbestos-risk-detail
        {
            width: 70%;
            height: 25px;
            border: 1px solid black;
            border-collapse: collapse; /*border-left: 1px solid black;
            border-top: 1px solid black;
            border-bottom: 1px solid black;*/
        }
        .td-asbestos
        {
            /*width: 110px;*/
            height: auto; /*border-bottom: 1px solid black; 
            border: 1px solid black;
            border-collapse: collapse;*/
        }
        .td-asbestos-tr > td
        {
            height: 25px;
            padding-left: 5px;
            border-bottom: 1px solid; /* padding-top: 5px;            
            padding-bottom: 5px;
            padding-left: 5px;*/
        }
        .td-asbestos-tr:last-child > td
        {
            border-bottom: 0px solid transparent;
        }
        
        
        #tblTableOfContents
        {
            width: 100%;
        }
        .td-content
        {
            width: 30%;
        }
        .td-content-detail
        {
            width: 70%;
            height: 20px;
            text-align: left;
        }
        
        
        #tblchecklistAreaDetails > tbody > tr > td > table > tbody > tr > td > table:last-child, #tblchecklistAreaDetails > tbody > tr:last-child > td > table:last-child
        {
            border-bottom: 0px none transparent !important;
        }
        
        
        
        
        /*        .td-previous-survey-left
        {
            width: 161px;
            height: 48px;
        }
        .td-previous-survey-right
        {
            width: 114px;
            height: 48px;
        }*/
        .top
        {
            border-top: thin solid;
            border-color: black;
        }
        
        .bottom
        {
            border-bottom: thin solid;
            border-color: black;
        }
        
        .left
        {
            border-left: thin solid;
            border-color: black;
        }
        
        .right
        {
            border-right: thin solid;
            border-color: black;
        }
        
        .table-width
        {
            width: 100%;
        }
        
        .last tr th:lastchild
        {
            border-bottom: none;
        }
        .td-detail-location-first
        {
            width: 100%;
            border-right: 1px solid;
            border-left: 1px solid;
            border-top: 1px solid; /*padding-left: 1%;*/
        }
        .td-detail-location
        {
            width: 100%;
            border-right: 0px solid;
            border-bottom: 1px solid;
            padding-left: 1%;
        }
        .tr-details-heading
        {
            page-break-before: always;
        }
        .bold
        {
            font-weight: bold;
        }
        
        
        #tblInspectionPhotographs
        {
            width: 100%;
            border: 1px solid black;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        
        #tblInspectionPhotographs > tbody > tr:last-child > td:first-child, #tblInspectionPhotographs > tbody > tr:last-child > td:last-child > table > tbody > tr > td, #tblInspectionPhotographs > tbody > tr:last-child > td:last-child > table > tbody > tr > td:last-child > table > tbody > tr > td, #tblInspectionPhotographs > tbody > tr:last-child > td:last-child > table > tbody > tr > td:last-child > table > tbody > tr > td:last-child > table > tbody > tr > td
        {
            border-bottom: 0px none transparent !important;
        }
    </style>
</head>
<body>
    <form id="formPropertyInspection" runat="server">
    <asp:HiddenField ID="hdnPageNo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPaging" runat="server" Value="0" />
    <div id="imgBHGImage" style="width: 100%" align="right">
        <img src="../Images/LOGOLETTERFinance.gif" alt="" style="height: 75px" />
    </div>
    <div id="upperTable" style="page-break-after: auto;">
        <h2>
            Record of Inspection</h2>
        <table id="tblPropertyDetails">
            <tr>
                <td class="td-main-detail">
                    Property:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblPropertyAddress" runat="server" Text='<%# Eval("Property") %>' />
                </td>
                <td rowspan="8" class="td-photograph">
                    <asp:Image ID="imgPhotographs" runat="server" ImageUrl="../Images/ErrorMessage.png"
                        Style="height: 220px !important; width: 100%" />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Property Ref:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblPropertyRef" runat="server" Text='<% Eval("PropertyRef") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Tenancy Ref:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblTenancyRef" runat="server" Text='<% Eval("TenancyRef") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Customer Ref:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblCustomerRef" runat="server" Text='<% Eval("CustomerRef") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Surveyor:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblSurveyor" runat="server" Text='<% Eval("SurveyorName") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Inspection Type:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblInspectionType" runat="server" Text='<% Eval("InspectionType") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Date/Time:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblDateTime" runat="server" Text='<% Eval("Date/Time") %>' />
                </td>
            </tr>
            <tr>
                <td class="td-main-detail">
                    Completed:
                </td>
                <td class="td-main-detail-value">
                    <asp:Label ID="lblPropertyCompleted" runat="server" Text='<% Eval("Completed") %>' />
                </td>
            </tr>
        </table>
        <table id="tblAsbestosDetails">
            <tr>
                <td valign="top" class="td-asbestos-risk">
                    Asbestos Risk:
                </td>
                <td class="td-asbestos-risk-detail">
                    <asp:Label ID="lblAsbestos" runat="server" Visible="False"></asp:Label>
                    <asp:GridView ID="grdAsbestos" Width="100%" runat="server" AllowPaging="False" AutoGenerateColumns="True"
                        AutoGenerateRows="True" AutoSizeColumnMode="GridViewAutoSizeColumnsMode.Fill"
                        ShowHeader="False" BorderWidth="0px" GridLines="None" RowStyle-CssClass="td-asbestos-tr">
                    </asp:GridView>
                    <%--<asp:GridView ID="grdAsbestos" Width="100%" runat="server" AllowPaging="False" AutoGenerateColumns="True"
                        AutoGenerateRows="True" AutoSizeColumnMode="GridViewAutoSizeColumnsMode.Fill"
                        ShowHeader="False" BorderWidth="0px" GridLines="None" class="td-asbestos" RowStyle-CssClass="td-asbestos-tr">
                    </asp:GridView>--%>
                </td>
            </tr>
            <tr>
                <td valign="top" class="td-asbestos-risk">
                    Previous Survey:
                </td>
                <td class="td-asbestos-risk-detail">
                    <asp:Label ID="lblPreviousSurvey" runat="server" Text='<% Eval("PreviousSurvey") %>' />
                </td>
            </tr>
        </table>
        <asp:GridView ID="grdDimensions" runat="server" AllowPaging="False" AutoGenerateColumns="True"
            AutoGenerateRows="True" Height="307px" Width="50%" BorderColor="Black" class="td-asbestos">
            <RowStyle HorizontalAlign="Center" />
        </asp:GridView>
    </div>
    <div id="divTableOfContents" style="page-break-before: always; page-break-after: always">
        <h2>
            Contents
        </h2>
        <table id="tblTableOfContents">
            <tr>
                <td class="td-content">
                    <asp:Label ID="lblTOCCheckList" runat="server" Text="Checklist"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblCheckListNo" runat="server" Text="3"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td-content">
                    <asp:Label ID="lblTOCDetailsConstruction" runat="server" Text="1. Details: Main Construction"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblDetailsConstructionNo" runat="server" Text="5"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td-content">
                    <asp:Label ID="lblTOCDetailsExternals" runat="server" Text="2. Details: Externals"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblDetailsExternalsNo" runat="server" Text="6"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td-content">
                    <asp:Label ID="lblTOCDetailsInternals" runat="server" Text="3. Details: Internals"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblDetailsInternalsNo" runat="server" Text="10"></asp:Label>
                </td>
            </tr>
            <tr id="trTOCInspectionNotes" runat="server">
                <td class="td-content" runat="server">
                    <asp:Label ID="lblTOCInspectionDetailsDynamicId" runat="server" Text="4."></asp:Label>
                    <asp:Label ID="lblTOCInspectionDetails" runat="server" Text="Inspection Notes"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblInspectionDetailsNo" runat="server" Text="11"></asp:Label>
                </td>
            </tr>
            <tr id="trTOCInspectionPhotoGraphs" runat="server">
                <td class="td-content">
                    <asp:Label ID="lblTOCInspectionPhotographsDynamicId" runat="server" Text="5."></asp:Label>
                    <asp:Label ID="lblInspectionPhotographs" runat="server" Text="Inspection Photographs"></asp:Label>
                </td>
                <td class="td-content-detail">
                    <asp:Label ID="lblInspectionPhotographsNo" runat="server" Text="11"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCheckList" style="page-break-after: auto">
        <h2>
            Checklist:</h2>
        <table id="tblchecklistAreaDetails" width="99%" style="border: 1px solid; border-collapse: collapse;
            margin-bottom: 20px" cellpadding="0">
            <asp:Repeater ID="rptCheckListLocationName" runat="server">
                <ItemTemplate>
                    <tr>
                        <td width="20%" style="border-bottom: 1px solid; border-right: 1px solid; height: 25px;">
                            <asp:Label ID="lblCheckListLocationHeading" runat="server" Text="Checklist"></asp:Label>
                        </td>
                        <td style="border-bottom: 1px solid; border-right: 1px solid; padding-right: 10%;"
                            align="right" width="80%">
                            <asp:Label ID="lblChecklistUpdatedHeading" runat="server" Text="Updated"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" valign="top" style="border-bottom: 1px solid; padding-left: 4%; padding-top: 6px;">
                            <asp:Label ID="lblCheckListLocationId" Visible="false" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                            <asp:Label ID="lblCheckListLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                        </td>
                        <td width="80%">
                            <asp:Repeater ID="rptCheckListAreaName" runat="server" OnItemDataBound="rptCheckListAreaName_DataBound">
                                <ItemTemplate>
                                    <table id="Area" width="100%" style="border-collapse: collapse; border-bottom: 1px solid;"
                                        cellpadding="0">
                                        <tr>
                                            <td width="30%" valign="top" style="border-right: 1px solid; border-left: 1px solid;
                                                padding-left: 2%; padding-top: 6px;">
                                                <asp:Label ID="lblCheckListAreaId" Visible="false" runat="server" Text='<%# Bind("AreaID") %>'></asp:Label>
                                                <asp:Label ID="lblCheckListAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                            </td>
                                            <td width="70%">
                                                <asp:Repeater ID="rptCheckListItemName" runat="server" OnItemDataBound="rptCheckListItemName_DataBound">
                                                    <ItemTemplate>
                                                        <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid; margin-bottom: -2px;">
                                                            <tr>
                                                                <td width="50%" valign="top" style="border-right: 1px solid; padding-left: 2%;">
                                                                    <asp:Label ID="lblCheckListItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                    <asp:Label ID="lblCheckListItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td width="80%" id="divCheckListSubItem" runat="server">
                                                                    <asp:Repeater runat="server" ID="rptSubItem" OnItemDataBound="rptCheckListSubItemName_DataBound">
                                                                        <ItemTemplate>
                                                                            <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid; margin-bottom: -2px;">
                                                                                <tr>
                                                                                    <td width="50%" style="border-right: 1px solid; padding-left: 2%;">
                                                                                        <asp:Label ID="lblCheckListSubItemId" Visible="false" runat="server" Text='<%# Bind("SubItemID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblCheckListSubItemName" runat="server" Text='<%# Bind("SubItemName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="border-color: Black; border-left: 1px; border-bottom: 1px; border-top: 1px;
                                                                                        padding-left: 2%;">
                                                                                        <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                                <td style="border-color: Black; border-left: 1px; border-bottom: 1px; border-top: 1px;
                                                                    padding-left: 2%;">
                                                                    <%--  <asp:Label ID="lblParameterId" Visible="false" runat="server" Text='<%# Bind("ItemId") %>'></asp:Label>
                                                                    <asp:Label ID="lblPropertyId" Visible="false" runat="server" Text='<%# Bind("PropertyId") %>'></asp:Label>--%>
                                                                    <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div id="divAreaDetails">
        <table id="tblAreaDetails" width="99%" class="test" style="border-collapse: collapse;
            margin-bottom: 20px">
            <asp:Repeater ID="rptLocationName" runat="server">
                <ItemTemplate>
                    <tr runat="server">
                        <td>
                            <div id="trDetailsHeading" runat="server">
                                <h2>
                                    <asp:Label ID="lblDetailsDynamicNo" class="bold" runat="server" Text="1."></asp:Label>
                                    <asp:Label ID="lblStaticHeading" class="bold" runat="server" Text="Details:"></asp:Label>
                                    <asp:Label ID="lblHeadingLocationName" class="bold" runat="server" Text="Main Construction"></asp:Label>
                                </h2>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td id="trLocationId" valign="top" runat="server" width="100%" colspan="2" class="td-detail-location-first">
                            <asp:Label ID="lblLocationId" Visible="false" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                            <asp:Label ID="lblLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" class="td-detail-location-first">
                            <asp:Repeater ID="rptAreaName" runat="server" OnItemDataBound="rptAreaName_DataBound">
                                <ItemTemplate>
                                    <table id="Area" width="100%" style="border-collapse: collapse; margin-bottom: -2px;">
                                        <tr>
                                            <td width="10%" valign="top" style="border-bottom: 1px solid; padding-left: 1%; padding-top: 5px;">
                                                <asp:Label ID="lblAreaId" Visible="false" runat="server" Text='<%# Bind("AreaID") %>'></asp:Label>
                                                <asp:Label ID="AreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                            </td>
                                            <td width="80%">
                                                <asp:Repeater ID="rptItemName" runat="server" OnItemDataBound="rptItemName_DataBound">
                                                    <ItemTemplate>
                                                        <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid; margin-bottom: -2px;">
                                                            <tr>
                                                                <td width="20%" valign="top" style="border-right: 1px solid; border-left: 1px solid;
                                                                    padding-left: 1%;">
                                                                    <asp:Label ID="lblItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                    <asp:Label ID="ItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td width="80%" id="divSubItem" runat="server">
                                                                    <asp:Repeater runat="server" ID="rptSubItem" OnItemDataBound="rptSubItemName_DataBound">
                                                                        <ItemTemplate>
                                                                            <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid; margin-bottom: -2px;">
                                                                                <tr>
                                                                                    <td width="15%" valign="top" style="border-right: 1px solid; padding-left: 1%;">
                                                                                        <asp:Label ID="lblSubItemId" Visible="false" runat="server" Text='<%# Bind("SubItemID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblSubItemName" runat="server" Text='<%# Bind("SubItemName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td width="85%" id="divNonHeating" runat="server">
                                                                                        <asp:Repeater ID="rptParamName" runat="server" OnItemDataBound="rptParamName_DataBound">
                                                                                            <ItemTemplate>
                                                                                                <table width="100%" class="last" style="border-collapse: collapse; margin-bottom: -2px;">
                                                                                                    <tr>
                                                                                                        <td width="55.15%" style="border-right: 1px solid; border-bottom: 1px solid; padding-left: 2%;">
                                                                                                            <asp:Label ID="lblParameterId" Visible="false" runat="server" Text='<%# Bind("ParameterID") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblItemParamId" Visible="false" runat="server" Text='<%# Bind("ItemParamId") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblControlType" Visible="false" runat="server" Text='<%# Bind("ControlType") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblParameterName" runat="server" Text='<%# Bind("ParameterName") %>'></asp:Label>
                                                                                                        </td>
                                                                                                        <td width="50%" style="border-bottom: 1px solid; padding-left: 1%; margin-right: -2px;">
                                                                                                            <asp:Label ID="lblParameterValue" runat="server" Text=''></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                    <td width="85%" id="divHeating" runat="server">
                                                                                        <asp:Repeater runat="server" ID="rptHeating" OnItemDataBound="rptHeating_DataBound">
                                                                                            <ItemTemplate>
                                                                                                <table width="100%" style="border-collapse: collapse; border-bottom: 1px solid; margin-bottom: -2px;">
                                                                                                    <tr>
                                                                                                        <td width="20%" valign="top" style="border-right: 1px solid; padding-left: 1%;">
                                                                                                            <asp:Label ID="lblHeatingId" Visible="false" runat="server" Text='<%# Bind("HeatingId") %>'></asp:Label>                                                                                                          
                                                                                                            <asp:Label ID="lblHeatingName" runat="server" Text='<%# Bind("HeatingName") %>'></asp:Label>
                                                                                                        </td>
                                                                                                        <td width="80%">
                                                                                                            <asp:Repeater ID="rptParamName" runat="server" OnItemDataBound="rptParamName_DataBound">
                                                                                                                <ItemTemplate>
                                                                                                                    <table width="100%" class="last" style="border-collapse: collapse; margin-bottom: -2px;">
                                                                                                                        <tr>
                                                                                                                            <td width="39%" style="border-right: 1px solid; border-bottom: 1px solid; padding-left: 2%;">
                                                                                                                                <asp:Label ID="lblParameterId" Visible="false" runat="server" Text='<%# Bind("ParameterID") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblItemParamId" Visible="false" runat="server" Text='<%# Bind("ItemParamId") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblControlType" Visible="false" runat="server" Text='<%# Bind("ControlType") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblParameterName" runat="server" Text='<%# Bind("ParameterName") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblHeatingId" Visible="false" runat="server" Text='<%# Bind("HeatingId") %>'></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td width="50%" style="border-bottom: 1px solid; padding-left: 1%; margin-right: -2px;">
                                                                                                                                <asp:Label ID="lblParameterValue" runat="server" Text=''></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:Repeater>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                                <td width="80%" colspan="2">
                                                                    <asp:Repeater ID="rptParamName" runat="server" OnItemDataBound="rptParamName_DataBound">
                                                                        <ItemTemplate>
                                                                            <table width="100%" class="last" style="border-collapse: collapse; margin-bottom: -2px;
                                                                                margin-right: -12px;">
                                                                                <tr>
                                                                                    <td width="61.65%" style="border-right: 1px solid; border-bottom: 1px solid; padding-left: 2%;">
                                                                                        <asp:Label ID="lblParameterId" Visible="false" runat="server" Text='<%# Bind("ParameterID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblItemParamId" Visible="false" runat="server" Text='<%# Bind("ItemParamId") %>'></asp:Label>
                                                                                        <asp:Label ID="lblControlType" Visible="false" runat="server" Text='<%# Bind("ControlType") %>'></asp:Label>
                                                                                        <asp:Label ID="lblParameterName" runat="server" Text='<%# Bind("ParameterName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td width="50%" style="border-bottom: 1px solid; padding-left: 1%; margin-right: -2px;">
                                                                                        <asp:Label ID="lblParameterValue" runat="server" Text=''></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div id="divInspectionNotes" runat="server">
        <div id="divInspectionNotesHeading" runat="server">
            <h2>
                <asp:Label ID="lblNotesDynamicNo" class="bold" runat="server" Text="4."></asp:Label>
                <asp:Label ID="lblNotesStaticHeading" class="bold" runat="server" Text="Inspection Notes "></asp:Label>
            </h2>
        </div>
        <table id="tblInspectionNotes" width="99%" style="border: 1px solid; border-collapse: collapse;
            margin-bottom: 20px">
            <asp:Repeater ID="rptInspectionLocationName" runat="server" OnItemDataBound="rptInspectionLocationName_DataBound">
                <ItemTemplate>
                    <tr>
                        <td width="20%" style="border-bottom: 1px solid">
                            <asp:Label ID="lblInspectionLocationHeading" runat="server" Text="InspectionNotes"></asp:Label>
                        </td>
                        <td width="20%" style="border-top: 1px solid">
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" valign="top" style="border-bottom: 1px solid; padding-left: 4%;">
                            <asp:Label ID="lblInspectionLocationId" Visible="false" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                            <asp:Label ID="lblInspectionLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                        </td>
                        <td width="80%">
                            <asp:Repeater ID="rptInspectionAreaName" runat="server" OnItemDataBound="rptInspectionAreaName_DataBound">
                                <ItemTemplate>
                                    <table id="tblInspectionAreaName" width="100%" style="border-collapse: collapse;
                                        border-bottom: 1px solid; margin-bottom: -2px; margin-top: -1px;">
                                        <tr>
                                            <td width="30%" valign="top" style="border-right: 1px solid; border-left: 1px solid;
                                                border-top: 1px solid; padding-left: 2%;">
                                                <asp:Label ID="lblInspectionAreaId" Visible="false" runat="server" Text='<%# Bind("AreaID") %>'></asp:Label>
                                                <asp:Label ID="lblInspectionAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptInspectionItemName" runat="server" OnItemDataBound="rptInspectionItemName_DataBound">
                                                    <ItemTemplate>
                                                        <table id="tblInspectionItemName" width="100%" style="border-collapse: collapse;
                                                            border-bottom: 1px solid; margin-bottom: -2px; margin-top: -1px;">
                                                            <tr>
                                                                <td width="50%" valign="top" style="border-right: 1px solid; border-top: 1px solid;
                                                                    padding-left: 2%;">
                                                                    <asp:Label ID="lblInspectionItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                    <asp:Label ID="lblInspectionItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td width="80%">
                                                                    <asp:Repeater ID="rptInspectionNotes" runat="server">
                                                                        <ItemTemplate>
                                                                            <table id="tblInspectionNotes" width="100%" style="border-collapse: collapse; margin-bottom: -2px;
                                                                                margin-top: -1px;">
                                                                                <tr>
                                                                                    <td width="50%" style="border-top: 1px solid; padding-left: 2%;">
                                                                                        <asp:Label ID="lblInspectionPropertyId" Visible="false" runat="server" Text='<%# Bind("PROPERTYID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblInspectionNotes" runat="server" Text='<%# Bind("Notes") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div id="divInspectionPhotographs" runat="server">
        <div id="divPhotographsHeading" runat="server">
            <h2>
                <asp:Label ID="lblPhotoGraphsDynamicNo" class="bold" runat="server" Text="5."></asp:Label>
                <asp:Label ID="lblPhotoGraphsHeading" class="bold" runat="server" Text="Inspection Photographs "></asp:Label>
            </h2>
        </div>
        <table id="tblInspectionPhotographs" width="99%"  cellpadding="0">
            <asp:Repeater ID="rptPhotographsLocationName" runat="server" OnItemDataBound="rptPhotographsLocationName_DataBound">
                <ItemTemplate>
                    <tr>
                        <td colspan="2" style="border-bottom: 1px solid black; border-collapse: collapse;
                            height: 25px;">
                            <asp:Label ID="lblInspectionPhotographsHeading" runat="server" Text="Inspection Photographs"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" valign="top" style="padding-left: 2%; border-bottom: 1px solid black;
                            border-right: 1px solid black; border-collapse: collapse;">
                            <asp:Label ID="lblPhotographsLocationId" Visible="false" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                            <asp:Label ID="lblPhotographsLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                        </td>
                        <td width="80%">
                            <asp:Repeater ID="rptPhotographsAreaName" runat="server" OnItemDataBound="rptPhotographsAreaName_DataBound">
                                <ItemTemplate>
                                    <table id="tblPhotographsAreaName" width="100%" style="border: 0px; border-collapse: collapse;"
                                        cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="50%" valign="top" style="padding-left: 2%; border-right: 1px solid black;
                                                border-bottom: 1px solid black; border-collapse: collapse;">
                                                <asp:Label ID="lblPhotographsAreaId" Visible="false" runat="server" Text='<%# Bind("AreaID") %>'></asp:Label>
                                                <asp:Label ID="lblPhotographsAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                            </td>
                                            <td width="50%">
                                                <asp:Repeater ID="rptPhotographsItemName" runat="server" OnItemDataBound="rptPhotographsItemName_DataBound">
                                                    <ItemTemplate>
                                                        <table id="tblPhotographsItemName" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%" valign="top" style="padding-left: 2%; border-bottom: 1px solid black;
                                                                    border-right: 1px solid black; border-collapse: collapse;">
                                                                    <asp:Label ID="lblPhotographsItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                    <asp:Label ID="lblPhotographsItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td width="30%" style="border-bottom: 1px solid black; border-collapse: collapse;">
                                                                    <asp:Repeater ID="rptInspectionPhotographs" runat="server">
                                                                        <ItemTemplate>
                                                                            <table id="tblInspectionPhotographsImage" width="100%">
                                                                                <tr>
                                                                                    <td width="100%" style="text-align: center; border: None;">
                                                                                        <asp:Label ID="lblPhotographsPropertyId" Visible="false" runat="server" Text='<%# Bind("PROPERTYID") %>'></asp:Label>
                                                                                        <asp:Image ID="imgItemPhotographs" runat="server" Width="100" Height="100" src='<%# Eval("ImageName") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    </form>
</body>
</html>
