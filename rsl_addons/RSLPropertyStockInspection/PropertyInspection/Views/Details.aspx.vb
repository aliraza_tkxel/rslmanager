﻿Imports System
Imports System.Data
Imports PropertyInspection_BusinessLogic
Imports PropertyInspection_BusinessObject
Imports PropertyInspection_Utilities
Imports System.Drawing
Imports System.Globalization
Imports System.Threading
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Web



Public Class Details
    Inherits System.Web.UI.Page

#Region "Properties"
    Dim resultDataSet As DataSet = New DataSet()
#End Region

#Region "Events"

#Region "Page load event"
    ''' <summary>
    ''' Page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim propertyId As String = Request.QueryString("PropertyId")
            Dim surveyId As String = Request.QueryString("SurveyId")
            populateMainPageDetail(propertyId)
            Dim objInspectionRecordMainPageAreaBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL
            objInspectionRecordMainPageAreaBL.getAreaDetails(resultDataSet, propertyId, surveyId)
            populateLocation()
            populateInspectionNotes()
            populateInspectionPhotographs()
        End If

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Inspection Photographs"

    Sub populateInspectionPhotographs()
        If (resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt).Rows.Count > 0) Then
            Dim locationDt As DataTable = New DataTable()
            locationDt.Columns.Add("LocationID")
            locationDt.Columns.Add("LocationName")

            For Each dtRow As DataRow In resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt).Rows
                Dim location = locationDt.AsEnumerable()
                Dim locationResult = (From loc In location Where loc("LocationID").ToString = dtRow("LocationID").ToString And loc("LocationName").ToString = dtRow("LocationName").ToString() Select loc)


                If locationResult.Count = 0 Then
                    locationDt.Rows.Add(dtRow("LocationID"), dtRow("LocationName"))
                End If
            Next

            rptPhotographsLocationName.DataSource = locationDt
            rptPhotographsLocationName.DataBind()
            If (resultDataSet.Tables(ApplicationConstants.InspectionNotesDt).Rows.Count > 0) Then
                trTOCInspectionPhotoGraphs.Visible = True
                lblTOCInspectionPhotographsDynamicId.Visible = True
                lblTOCInspectionPhotographsDynamicId.Text = "5."
                lblPhotoGraphsDynamicNo.Text = "5."
            Else
                trTOCInspectionPhotoGraphs.Visible = "True"
                lblTOCInspectionPhotographsDynamicId.Text = "4."
                lblPhotoGraphsDynamicNo.Text = "4."
            End If
        Else
            divInspectionPhotographs.Visible = False
            trTOCInspectionPhotoGraphs.Visible = False
        End If
    End Sub
#End Region

#Region "Populate Inspection Notes"

    Sub populateInspectionNotes()
        If (resultDataSet.Tables(ApplicationConstants.InspectionNotesDt).Rows.Count > 0) Then

            Dim locationDt As DataTable = New DataTable()
            locationDt.Columns.Add("LocationID")
            locationDt.Columns.Add("LocationName")

            For Each dtRow As DataRow In resultDataSet.Tables(ApplicationConstants.InspectionNotesDt).Rows
                Dim location = locationDt.AsEnumerable()
                Dim locationResult = (From loc In location Where loc("LocationID").ToString = dtRow("LocationID").ToString And loc("LocationName").ToString = dtRow("LocationName").ToString() Select loc)


                If locationResult.Count = 0 Then
                    locationDt.Rows.Add(dtRow("LocationID"), dtRow("LocationName"))
                End If
            Next

            trTOCInspectionNotes.Visible = True
            lblTOCInspectionDetailsDynamicId.Visible = True
            'lblTOCInspectionDetails.Visible = "True"
            lblTOCInspectionDetailsDynamicId.Text = "4."
            'lblTOCInspectionDetails.Text = "Inspection Notes"

            rptInspectionLocationName.DataSource = locationDt
            rptInspectionLocationName.DataBind()
        Else
            divInspectionNotes.Visible = False
            trTOCInspectionNotes.Visible = False
        End If
    End Sub
#End Region

#Region "Populate location"

    Sub populateLocation()
        rptCheckListLocationName.DataSource = resultDataSet
        rptCheckListLocationName.DataBind()

        rptLocationName.DataSource = resultDataSet
        rptLocationName.DataBind()
    End Sub
#End Region

#Region "Populate Main Page Detail"
    ''' <summary>
    ''' get Main page content
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateMainPageDetail(ByVal propertyId As String)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objInspectionRecordMainPageAreaBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL
        objInspectionRecordMainPageAreaBL.getMainPageContent(resultDataSet, propertyId)

        lblPropertyAddress.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.PropertyAddressColumn)
        lblPropertyRef.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.PropertyReferenceColumn)
        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.TenancyReferenceColumn))) Then
            lblTenancyRef.Text = ApplicationConstants.NotAvailable
        Else
            lblTenancyRef.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.TenancyReferenceColumn)
        End If
        If Not (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.ImageName))) Then
            imgPhotographs.ImageUrl = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.ImageName)

        End If
        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.CustomerReferenceColumn))) Then
            lblCustomerRef.Text = ApplicationConstants.NotAvailable
        Else
            lblCustomerRef.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.CustomerReferenceColumn)
        End If

        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.SurveyorNameColumn))) Then
            lblSurveyor.Text = ApplicationConstants.NotAvailable
        Else
            lblSurveyor.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.SurveyorNameColumn)
        End If

        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.InspectionTypeColumn))) Then
            lblInspectionType.Text = ApplicationConstants.NotAvailable
        Else
            lblInspectionType.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.InspectionTypeColumn)
        End If


        lblDateTime.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.DateTimeColumn)

        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.CompletedColumn))) Then
            lblPropertyCompleted.Text = ApplicationConstants.NotAvailable
        Else
            lblPropertyCompleted.Text = resultDataSet.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0).Item(ApplicationConstants.CompletedColumn)
        End If

        If (IsDBNull(resultDataSet.Tables(ApplicationConstants.SurveyDt).Rows(0).Item(ApplicationConstants.PreviousSurveyColumn))) Then
            lblPreviousSurvey.Text = ApplicationConstants.NotAvailable
        Else
            lblPreviousSurvey.Text = resultDataSet.Tables(ApplicationConstants.SurveyDt).Rows(0).Item(ApplicationConstants.PreviousSurveyColumn)
        End If

        If (resultDataSet.Tables(ApplicationConstants.AsbestosDt).Rows.Count > 0) Then
            grdAsbestos.DataSource = resultDataSet.Tables(ApplicationConstants.AsbestosDt)
            grdAsbestos.DataBind()
        Else
            lblAsbestos.Visible = True
            lblAsbestos.Text = ApplicationConstants.AsbestosNone
        End If


        If (resultDataSet.Tables(ApplicationConstants.DimensionsDt).Rows.Count > 0) Then
            grdDimensions.DataSource = resultDataSet.Tables(ApplicationConstants.DimensionsDt)
            grdDimensions.DataBind()
        Else
            grdDimensions.Visible = False
        End If
    End Sub

#End Region

#Region "CheckList"
    ''' <summary>
    ''' Return Checklist Content
    ''' </summary>
    ''' <param name="Sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub rptCheckListLocatioName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptCheckListLocationName.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptCheckListAreaName As Repeater = DirectCast(e.Item.FindControl("rptCheckListAreaName"), Repeater)
            'Dim rptCheckListItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)
            Dim lblCheckListLocationId As Label = DirectCast(e.Item.FindControl("lblCheckListLocationId"), Label)
            'filter TableArea where location id is lblCheckListLocationId.text 
            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.AreaDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "LocationID =  " + lblCheckListLocationId.Text.ToString()
            filteredDt = areaDv.ToTable()

            rptCheckListAreaName.DataSource = filteredDt
            rptCheckListAreaName.DataBind()

        End If
    End Sub

    Sub rptCheckListAreaName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptCheckListItemName As Repeater = DirectCast(e.Item.FindControl("rptCheckListItemName"), Repeater)
            Dim lblCheckListAreaId As Label = DirectCast(e.Item.FindControl("lblCheckListAreaId"), Label)
            'Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListItemId"), Label)

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.ItemDt)
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "AreaID =  " + lblCheckListAreaId.Text.ToString()
            filteredDt = areaDv.ToTable()

            rptCheckListItemName.DataSource = filteredDt
            rptCheckListItemName.DataBind()


        End If
    End Sub

    Sub rptCheckListItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListItemId"), Label)
            Dim lblUpdated As Label = DirectCast(e.Item.FindControl("lblUpdated"), Label)
            Dim subItemResult = (From ps In resultDataSet.Tables(ApplicationConstants.SubItemDt) Where ps.Item("ParentItemId").ToString() = lblCheckListItemId.Text.ToString() Select ps)
            If subItemResult.Count() > 0 Then
                Dim rptSubItem As Repeater = DirectCast(e.Item.FindControl("rptSubItem"), Repeater)
                Dim filteredSubItemDt As DataTable = New DataTable()
                Dim subItemDt As DataTable = resultDataSet.Tables(ApplicationConstants.SubItemDt)
                Dim subItemDv As DataView = subItemDt.AsDataView()
                subItemDv.RowFilter = "ParentItemId = " + lblCheckListItemId.Text.ToString()
                filteredSubItemDt = subItemDv.ToTable()
                rptSubItem.DataSource = filteredSubItemDt
                rptSubItem.DataBind()

            Else
                Dim divCheckListSubItem As HtmlContainerControl = DirectCast(e.Item.FindControl("divCheckListSubItem"), HtmlContainerControl)
                divCheckListSubItem.Visible = False
                Dim filteredDt As DataTable = New DataTable()
                Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.UpdatedDt)
                Dim areaDv As DataView = areaDt.AsDataView()
                areaDv.RowFilter = "ItemID = " + lblCheckListItemId.Text.ToString()
                filteredDt = areaDv.ToTable()
                If (filteredDt.Rows.Count > 0) Then
                    lblUpdated.Text = ApplicationConstants.Yes
                Else
                    lblUpdated.Text = ApplicationConstants.No
                End If
            End If

        End If
    End Sub


    Sub rptCheckListSubItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblCheckListItemId As Label = DirectCast(e.Item.FindControl("lblCheckListSubItemId"), Label)
            Dim lblUpdated As Label = DirectCast(e.Item.FindControl("lblUpdated"), Label)


            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.UpdatedDt)
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "ItemID = " + lblCheckListItemId.Text.ToString()
            filteredDt = areaDv.ToTable()
            If (filteredDt.Rows.Count > 0) Then
                lblUpdated.Text = ApplicationConstants.Yes
            Else
                lblUpdated.Text = ApplicationConstants.No
            End If


        End If
    End Sub
#End Region

#Region "AreaDetails"
    ''' <summary>
    ''' Get Area Details
    ''' </summary>
    ''' <param name="Sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub rptLocationName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptLocationName.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptAreaName As Repeater = DirectCast(e.Item.FindControl("rptAreaName"), Repeater)
            Dim trDetailsHeading As HtmlControl = DirectCast(e.Item.FindControl("trDetailsHeading"), HtmlControl)

            Dim trLocationId As HtmlControl = DirectCast(e.Item.FindControl("trLocationId"), HtmlControl)

            Dim lblLocationId As Label = DirectCast(e.Item.FindControl("lblLocationId"), Label)
            Dim lblDetailsDynamicNo As Label = DirectCast(trDetailsHeading.FindControl("lblDetailsDynamicNo"), Label)
            Dim lblHeadingLocationName As Label = DirectCast(trDetailsHeading.FindControl("lblHeadingLocationName"), Label)
            'filter TableArea where location id is lblLocationId.text 
            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.AreaDt)
            Dim areaDv As DataView = areaDt.AsDataView()
            Dim locationId As Integer = Convert.ToInt32(lblLocationId.Text)
            trLocationId.Style.Add("background-color", "#E0FFFF")
            If locationId > 1 Then
                'tdDetailsHeading.Attributes.Remove("Class")
                trDetailsHeading.Attributes.Add("class", "tr-details-heading")

                'trLocationId.Attributes.Remove("class")
                ' trLocationId.Attributes.Add("class", "td-detail-location")
            End If
            If locationId = 2 Then
                lblDetailsDynamicNo.Text = "2."
                lblHeadingLocationName.Text = " Externals"
                trLocationId.Style.Add("background-color", "#FFE0FB")
                Dim paging As Integer = hdnPageNo.Value
                Dim currentPage As Double = (paging / 43) + 5
                lblDetailsExternalsNo.Text = Decimal.Ceiling(currentPage).ToString()
            End If
            If locationId = 3 Then
                lblDetailsDynamicNo.Text = "3."
                lblHeadingLocationName.Text = " Internals"
                trLocationId.Style.Add("background-color", "#FFFFEB")
                Dim paging As Integer = hdnPageNo.Value
                Dim currentPage As Double = (paging / 43) + 5
                lblDetailsInternalsNo.Text = Decimal.Ceiling(currentPage).ToString()
                hdnPaging.Value = Decimal.Ceiling(currentPage).ToString()
            End If
            areaDv.RowFilter = "LocationID =  " + lblLocationId.Text.ToString()
            filteredDt = areaDv.ToTable()
            checkAreaExist(filteredDt)
            rptAreaName.DataSource = filteredDt
            rptAreaName.DataBind()

        End If
    End Sub

    Sub rptAreaName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)
            Dim lblAreaId As Label = DirectCast(e.Item.FindControl("lblAreaId"), Label)
            'Dim lblItemId As Label = DirectCast(e.Item.FindControl("lblItemId"), Label)

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.ItemDt)
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "AreaID =  " + lblAreaId.Text.ToString()
            filteredDt = areaDv.ToTable()
            checkItemExist(filteredDt)
            rptItemName.DataSource = filteredDt
            rptItemName.DataBind()


        End If
    End Sub

    Sub rptItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptParamName As Repeater = DirectCast(e.Item.FindControl("rptParamName"), Repeater)
            Dim lblItemId As Label = DirectCast(e.Item.FindControl("lblItemId"), Label)
            Dim itemId As String = lblItemId.Text
            Dim subItemResult = (From ps In resultDataSet.Tables(ApplicationConstants.SubItemDt)
                                 Where ps.Item("ParentItemId").ToString() = lblItemId.Text.ToString()
                                 Select ps)
            If subItemResult.Count() > 0 Then
                Dim filteredDt As DataTable = New DataTable()
                Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
                Dim parameterDv As DataView = parameterDt.AsDataView()
                parameterDv.RowFilter = "ItemID = " + lblItemId.Text.ToString()
                filteredDt = parameterDv.ToTable()
                'checkItemExist(filteredDt)
                hdnPageNo.Value = Convert.ToString(Convert.ToInt32(hdnPageNo.Value) + filteredDt.Rows.Count())
                rptParamName.DataSource = filteredDt
                rptParamName.DataBind()


                Dim rptSubItem As Repeater = DirectCast(e.Item.FindControl("rptSubItem"), Repeater)
                Dim filteredSubItemDt As DataTable = New DataTable()
                Dim subItemDt As DataTable = resultDataSet.Tables(ApplicationConstants.SubItemDt)
                Dim subItemDv As DataView = subItemDt.AsDataView()
                subItemDv.RowFilter = "ParentItemId = " + itemId
                filteredSubItemDt = subItemDv.ToTable()
                checkSubItemExist(filteredSubItemDt)
                rptSubItem.DataSource = filteredSubItemDt
                rptSubItem.DataBind()
            Else

                Dim divSubItem As HtmlContainerControl = DirectCast(e.Item.FindControl("divSubItem"), HtmlContainerControl)
                divSubItem.Visible = False
                '  Dim divSubItem As HtmlGenericControl = DirectCast(e.Item.FindControl("divSubItem"), HtmlGenericControl)
                Dim filteredDt As DataTable = New DataTable()
                Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
                Dim parameterDv As DataView = parameterDt.AsDataView()
                parameterDv.RowFilter = "ItemID = " + lblItemId.Text.ToString()
                filteredDt = parameterDv.ToTable()
                hdnPageNo.Value = Convert.ToString(Convert.ToInt32(hdnPageNo.Value) + filteredDt.Rows.Count())
                rptParamName.DataSource = filteredDt
                rptParamName.DataBind()
            End If

        End If
    End Sub


    Sub rptSubItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim lblSubItemId As Label = DirectCast(e.Item.FindControl("lblSubItemId"), Label)
            Dim lblSubItemName As Label = DirectCast(e.Item.FindControl("lblSubItemName"), Label)
            
            If lblSubItemName.Text.Equals(ApplicationConstants.HeatingItem) Then

                Dim divNonHeating As HtmlContainerControl = DirectCast(e.Item.FindControl("divNonHeating"), HtmlContainerControl)
                divNonHeating.Visible = False

                Dim heatingDt As DataTable = New DataTable()
                heatingDt = resultDataSet.Tables(ApplicationConstants.HeatingsDt)

                Dim rptHeating As Repeater = DirectCast(e.Item.FindControl("rptHeating"), Repeater)
                rptHeating.DataSource = heatingDt
                rptHeating.DataBind()
            Else

                Dim divHeating As HtmlContainerControl = DirectCast(e.Item.FindControl("divHeating"), HtmlContainerControl)
                divHeating.Visible = False                

                Dim filteredDt As DataTable = New DataTable()
                Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)

                Dim parameterDv As DataView = parameterDt.AsDataView()
                parameterDv.RowFilter = "ItemID = " + lblSubItemId.Text.ToString()
                filteredDt = parameterDv.ToTable()
                hdnPageNo.Value = Convert.ToString(Convert.ToInt32(hdnPageNo.Value) + filteredDt.Rows.Count())

                Dim rptParamName As Repeater = DirectCast(e.Item.FindControl("rptParamName"), Repeater)
                rptParamName.DataSource = filteredDt
                rptParamName.DataBind()

            End If

        End If
    End Sub

    Sub rptHeating_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim lblHeatingId As Label = DirectCast(e.Item.FindControl("lblHeatingId"), Label)

            'Get Selected Heating Type Id of the current Heating
            Dim heatingParameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
            Dim currentHeatingDtRows As DataRow = heatingParameterDt.Select(String.Format("HeatingId = {0}", lblHeatingId.Text.ToString())).FirstOrDefault()
            Dim selectedHeatingTypeId As String = currentHeatingDtRows.Item("SelectedHeatingType")

            'Filter parameter dataset based on HeatingId
            Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
            Dim parameterDv As DataView = parameterDt.AsDataView()
            parameterDv.RowFilter = String.Format("HeatingId = {0} and (ParameterValueId = {1} or ParameterValueId IS NULL)", lblHeatingId.Text.ToString(), selectedHeatingTypeId)

            Dim filteredDt As DataTable = New DataTable()
            filteredDt = parameterDv.ToTable()
            hdnPageNo.Value = Convert.ToString(Convert.ToInt32(hdnPageNo.Value) + filteredDt.Rows.Count())

            Dim rptParamName As Repeater = DirectCast(e.Item.FindControl("rptParamName"), Repeater)
            rptParamName.DataSource = filteredDt
            rptParamName.DataBind()

        End If
    End Sub

    Sub rptParamName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblParameterId As Label = DirectCast(e.Item.FindControl("lblParameterId"), Label)
            Dim lblItemParamId As Label = DirectCast(e.Item.FindControl("lblItemParamId"), Label)
            Dim lblItemId As Label = DirectCast(e.Item.FindControl("lblItemId"), Label)
            Dim lblItemParamName As Label = DirectCast(e.Item.FindControl("lblParameterName"), Label)
            Dim lblParameterValue As Label = DirectCast(e.Item.FindControl("lblParameterValue"), Label)
            Dim lblControlType As Label = DirectCast(e.Item.FindControl("lblControlType"), Label)
            Dim lblHeatingId As Label = DirectCast(e.Item.FindControl("lblHeatingId"), Label)
            Dim heatingId As String = String.Empty
            If Not IsNothing(lblHeatingId) Then
                heatingId = lblHeatingId.Text
            End If

            If lblControlType.Text <> "Date" Then

                Dim preInsertedResult = (From ps In resultDataSet.Tables(ApplicationConstants.PreInsertedValueDt)
                                         Where ps.Item("ItemParamID").ToString() = lblItemParamId.Text.ToString()
                                         Select ps)

                If Not String.IsNullOrEmpty(heatingId) Then
                    preInsertedResult = preInsertedResult.Where(Function(p) Not IsDBNull(p.Item("HeatingId")) AndAlso p.Item("HeatingId") = heatingId)
                End If


                If preInsertedResult.Count > 0 Then

                    If lblControlType.Text.ToUpper = "TextBox".ToUpper Or lblControlType.Text.ToUpper = "TextArea".ToUpper Then
                        lblParameterValue.Text = preInsertedResult.First.Item("PARAMETERVALUE").ToString()
                    ElseIf lblControlType.Text.ToUpper = "Checkboxes".ToUpper Then
                        Dim chkList As String = "<table style='width:100%;'>"
                        For Each lst As DataRow In preInsertedResult
                            If lst.Item("IsCheckBoxSelected") = True Then
                                chkList += "<tr style='border-bottom: 2px solid'><td>" + If(IsDBNull(lst.Item("PARAMETERVALUE")), "Yes", lst.Item("PARAMETERVALUE")) + "</td></tr>"
                            End If
                        Next
                        chkList += "</table>"
                        lblParameterValue.Text = chkList
                    Else
                        Dim insertedValueId As String = preInsertedResult.First.Item("VALUEID").ToString()
                        Dim parmeterValueResult = (From ps In resultDataSet.Tables(ApplicationConstants.ParamValueDt) Where ps.Item("ValueID").ToString() = insertedValueId Select ps)
                        If parmeterValueResult.Count > 0 Then
                            lblParameterValue.Text = parmeterValueResult.First.Item("ValueDetail").ToString()
                        Else
                            lblParameterValue.Text = ApplicationConstants.NotAvailable
                        End If
                    End If
                Else
                    lblParameterValue.Text = ApplicationConstants.NotAvailable
                End If
            ElseIf lblControlType.Text.ToUpper = "Date".ToUpper Then
                Dim parameterValue As String = String.Empty
                Me.getItemDates(lblParameterId.Text, lblItemId.Text, lblItemParamName.Text, parameterValue, resultDataSet, heatingId)
                If parameterValue Is Nothing Or parameterValue = "" Then
                    lblParameterValue.Text = ApplicationConstants.NotAvailable
                Else
                    lblParameterValue.Text = parameterValue
                End If
            End If
        End If
    End Sub


    Sub checkAreaExist(ByRef resultDt As DataTable)
        Dim newFilteredDt As DataTable = New DataTable()
        newFilteredDt.Columns.Add("AreaID")
        newFilteredDt.Columns.Add("LocationId")
        newFilteredDt.Columns.Add("AreaName")
        For Each dr As DataRow In resultDt.Rows
            Dim itemDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.ItemDt)
            Dim areaDv As DataView = areaDt.AsDataView()
            areaDv.RowFilter = "AreaID =  " + dr("AreaID").ToString()
            itemDt = areaDv.ToTable()
            Dim result As Integer = checkItemExist(itemDt)
            If result > 0 Then
                newFilteredDt.Rows.Add(dr("AreaID"), dr("LocationId"), dr("AreaName"))
            End If

        Next
        resultDt = newFilteredDt
    End Sub

    Function checkItemExist(ByRef resultDt As DataTable)
        Dim result As Integer = 0
        Dim newFilteredDt As DataTable = New DataTable()
        newFilteredDt.Columns.Add("ItemID")
        newFilteredDt.Columns.Add("AreaID")
        newFilteredDt.Columns.Add("ItemName")

        For Each dr As DataRow In resultDt.Rows
            Dim subItemResult = (From ps In resultDataSet.Tables(ApplicationConstants.SubItemDt) Where ps.Item("ParentItemId").ToString() = dr("ItemID").ToString() Select ps)
            If subItemResult.Count() > 0 Then
                Dim filteredSubItemDt As DataTable = New DataTable()
                Dim subItemDt As DataTable = resultDataSet.Tables(ApplicationConstants.SubItemDt)
                Dim subItemDv As DataView = subItemDt.AsDataView()
                subItemDv.RowFilter = "ParentItemId = " + dr("ItemID").ToString()
                filteredSubItemDt = subItemDv.ToTable()
                Dim res As Integer = checkSubItemExist(filteredSubItemDt)
                If res > 0 Then
                    result = result + res
                    newFilteredDt.Rows.Add(dr("ItemID"), dr("AreaID"), dr("ItemName"))
                End If
            Else
                Dim filteredDt As DataTable = New DataTable()
                Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
                Dim parameterDv As DataView = parameterDt.AsDataView()
                parameterDv.RowFilter = "ItemID = " + dr("ItemID").ToString()
                filteredDt = parameterDv.ToTable()
                If filteredDt.Rows.Count > 0 Then
                    result = result + 1
                    newFilteredDt.Rows.Add(dr("ItemID"), dr("AreaID"), dr("ItemName"))
                End If
            End If
        Next
        resultDt = newFilteredDt
        Return result
    End Function

    Function checkSubItemExist(ByRef resultDt As DataTable)
        Dim result As Integer = 0
        Dim newFilteredDt As DataTable = New DataTable()
        newFilteredDt.Columns.Add("SubItemID")
        newFilteredDt.Columns.Add("AreaID")
        newFilteredDt.Columns.Add("SubItemName")
        newFilteredDt.Columns.Add("ParentItemId")
        For Each dr As DataRow In resultDt.Rows
            Dim filteredDt As DataTable = New DataTable()
            Dim parameterDt As DataTable = resultDataSet.Tables(ApplicationConstants.ParamNameDt)
            Dim parameterDv As DataView = parameterDt.AsDataView()
            parameterDv.RowFilter = "ItemID = " + dr("SubItemID").ToString()
            filteredDt = parameterDv.ToTable()
            If filteredDt.Rows.Count > 0 Then
                result = result + 1
                newFilteredDt.Rows.Add(dr("SubItemID"), dr("AreaID"), dr("SubItemName"), dr("ParentItemId"))
            End If
        Next
        resultDt = newFilteredDt
        Return result
    End Function
#End Region

#Region "DateTypeParameters"
    ''' <summary>
    ''' Return values of date type Parameters for CheckList and Area Details
    ''' </summary>
    ''' <param name="parameterId"></param>
    ''' <param name="itemId"></param>
    ''' <param name="paramName"></param>
    ''' <param name="parameterValue"></param>
    ''' <param name="parameterDataSet"></param>
    ''' <remarks></remarks>
    Private Sub getItemDates(ByVal parameterId As String, ByVal itemId As String, ByVal paramName As String, ByRef parameterValue As String, ByVal parameterDataSet As DataSet, ByVal heatingId As String)        

        Dim datesResultset = (From ps In parameterDataSet.Tables(ApplicationConstants.DatesDt)
                              Where ps.Item("ItemID").ToString() = itemId
                              Select ps)

        If Not String.IsNullOrEmpty(heatingId) Then
            datesResultset = datesResultset.Where(Function(p) Not IsDBNull(p.Item("HeatingId")) AndAlso p.Item("HeatingId") = heatingId)
        End If


        If paramName.Contains("Due") Then
            If paramName.Contains("Rewire Due") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "Last Rewired")

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                        parameterValue = ItemDatesResult.First.Item("DueDate")
                    End If
                End If

            ElseIf paramName.Contains("Cur Due") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "CUR Last Replaced")

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                        parameterValue = ItemDatesResult.First.Item("DueDate")
                    End If
                End If

            ElseIf paramName.Contains("Upgrade Due") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "Electrical Upgrade Last Done")

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                        parameterValue = ItemDatesResult.First.Item("DueDate")
                    End If
                End If

            ElseIf paramName.ToUpper() = "Replacement Due".ToUpper() Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterID") Is DBNull.Value)

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                        parameterValue = ItemDatesResult.First.Item("DueDate")
                        End If
                    End If
            Else
                Dim ReplacementDueResult = datesResultset.Where(Function(p) p.Item("ParameterID").ToString() = parameterId.ToString())

                If ReplacementDueResult.Count > 0 Then
                    If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                        parameterValue = ReplacementDueResult.First.Item("DueDate")
                        End If
                    End If
            End If
        Else
            If paramName.Contains("Last Rewired") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "Last Rewired")

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                        parameterValue = ItemDatesResult.First.Item("LastDone")
                    End If
                End If
            ElseIf paramName.Contains("CUR Last Replaced") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "CUR Last Replaced")
                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                        parameterValue = ItemDatesResult.First.Item("LastDone")
                    End If
                End If
            ElseIf paramName.Contains("Upgrade Last Done") Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterName") = "Electrical Upgrade Last Done")
                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                        parameterValue = ItemDatesResult.First.Item("LastDone")
                    End If
                End If

            ElseIf paramName.ToUpper() = "Last Replaced".ToUpper() Then
                Dim ItemDatesResult = datesResultset.Where(Function(p) p.Item("ParameterID") Is DBNull.Value)

                If ItemDatesResult.Count > 0 Then
                    If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                        parameterValue = ItemDatesResult.First.Item("LastDone")
                    End If
                End If
            Else

                Dim ReplacementDueResult = datesResultset.Where(Function(p) p.Item("ParameterID").ToString() = parameterId.ToString())

                If ReplacementDueResult.Count > 0 Then
                    If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                        parameterValue = ReplacementDueResult.First.Item("DueDate")
                    End If

                End If
            End If
        End If
    End Sub
#End Region

#Region "InspectionNotes"
    ''' <summary>
    ''' Return content for Inspection Notes
    ''' </summary>
    ''' <param name="Sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Sub rptInspectionLocationName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ' If (resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt).Rows.Count > 0) Then
            Dim rptInspectionAreaName As Repeater = DirectCast(e.Item.FindControl("rptInspectionAreaName"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)

            Dim lblInspectionLocationId As Label = DirectCast(e.Item.FindControl("lblInspectionLocationId"), Label)
            'filter TableArea where location id is lblLocationId.text 

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionNotesDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "LocationID =  " + lblInspectionLocationId.Text.ToString()
            filteredDt = areaDv.ToTable()
            Dim filteredAreaDt As DataTable = New DataTable()
            filteredAreaDt.Columns.Add("AreaID")
            filteredAreaDt.Columns.Add("AreaName")

            For Each dtRow As DataRow In filteredDt.Rows
                Dim area = filteredAreaDt.AsEnumerable()
                Dim areaResult = (From are In area Where are("AreaID").ToString = dtRow("AreaID").ToString And are("AreaName").ToString = dtRow("AreaName").ToString() Select are)
                If areaResult.Count = 0 Then
                    filteredAreaDt.Rows.Add(dtRow("AreaID"), dtRow("AreaName"))
                End If
            Next
            rptInspectionAreaName.DataSource = filteredAreaDt
            rptInspectionAreaName.DataBind()
            'End If

        End If
    End Sub

    Sub rptInspectionAreaName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptInspectionItemName As Repeater = DirectCast(e.Item.FindControl("rptInspectionItemName"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)
            Dim lblInspectionAreaId As Label = DirectCast(e.Item.FindControl("lblInspectionAreaId"), Label)
            'filter TableArea where location id is lblLocationId.text 
            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionNotesDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "AreaID =  " + lblInspectionAreaId.Text.ToString()
            filteredDt = areaDv.ToTable()
            Dim filteredItemDt As DataTable = New DataTable()
            filteredItemDt.Columns.Add("ItemID")
            filteredItemDt.Columns.Add("ItemName")

            For Each dtRow As DataRow In filteredDt.Rows
                Dim area = filteredItemDt.AsEnumerable()
                Dim areaResult = (From are In area Where are("ItemID").ToString = dtRow("ItemID").ToString And are("ItemName").ToString = dtRow("ItemName").ToString() Select are)
                If areaResult.Count = 0 Then
                    filteredItemDt.Rows.Add(dtRow("ItemID"), dtRow("ItemName"))
                End If
            Next


            rptInspectionItemName.DataSource = filteredItemDt
            rptInspectionItemName.DataBind()
        End If
    End Sub

    Sub rptInspectionItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptInspectionNotes As Repeater = DirectCast(e.Item.FindControl("rptInspectionNotes"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)
            Dim lblInspectionItemId As Label = DirectCast(e.Item.FindControl("lblInspectionItemId"), Label)
            Dim lblInspectionNotes As Label = DirectCast(e.Item.FindControl("lblInspectionNotes"), Label)
            'filter TableArea where location id is lblLocationId.text 
            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionNotesDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "ItemId =  " + lblInspectionItemId.Text.ToString()
            filteredDt = areaDv.ToTable()

            Dim currentPage As Double = (filteredDt.Rows.Count() / 25) + Convert.ToInt32(lblDetailsInternalsNo.Text)
            lblInspectionDetailsNo.Text = Decimal.Ceiling(currentPage).ToString()
            hdnPageNo.Value = Decimal.Ceiling(currentPage).ToString()
            'If (filteredDt.Rows.Count > 0) Then
            rptInspectionNotes.DataSource = filteredDt
            rptInspectionNotes.DataBind()
            'Else
            'lblInspectionNotes.Text = "N/A"
            'End If



        End If
    End Sub

#End Region

#Region "InspectionPhotographs"
    ''' <summary>
    ''' Return content for Inspection Photographs
    ''' </summary>
    ''' <param name="Sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub rptPhotographsLocationName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptPhotographsAreaName As Repeater = DirectCast(e.Item.FindControl("rptPhotographsAreaName"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)

            Dim lblPhotographsLocationId As Label = DirectCast(e.Item.FindControl("lblPhotographsLocationId"), Label)
            'filter TableArea where location id is lblLocationId.text 

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "LocationID =  " + lblPhotographsLocationId.Text.ToString()
            filteredDt = areaDv.ToTable()
            Dim filteredAreaDt As DataTable = New DataTable()
            filteredAreaDt.Columns.Add("AreaID")
            filteredAreaDt.Columns.Add("AreaName")

            For Each dtRow As DataRow In filteredDt.Rows
                Dim area = filteredAreaDt.AsEnumerable()
                Dim areaResult = (From are In area Where are("AreaID").ToString = dtRow("AreaID").ToString And are("AreaName").ToString = dtRow("AreaName").ToString() Select are)
                If areaResult.Count = 0 Then
                    filteredAreaDt.Rows.Add(dtRow("AreaID"), dtRow("AreaName"))
                End If
            Next
            rptPhotographsAreaName.DataSource = filteredAreaDt
            rptPhotographsAreaName.DataBind()



        End If
    End Sub

    Sub rptPhotographsAreaName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptPhotographsItemName As Repeater = DirectCast(e.Item.FindControl("rptPhotographsItemName"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)

            Dim lblPhotographsAreaId As Label = DirectCast(e.Item.FindControl("lblPhotographsAreaId"), Label)
            'filter TableArea where location id is lblLocationId.text 

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "AreaID =  " + lblPhotographsAreaId.Text.ToString()
            filteredDt = areaDv.ToTable()
            Dim filteredItemDt As DataTable = New DataTable()
            filteredItemDt.Columns.Add("ItemID")
            filteredItemDt.Columns.Add("ItemName")

            For Each dtRow As DataRow In filteredDt.Rows
                Dim area = filteredItemDt.AsEnumerable()
                Dim areaResult = (From are In area Where are("ItemID").ToString = dtRow("ItemID").ToString And are("ItemName").ToString = dtRow("ItemName").ToString() Select are)
                If areaResult.Count = 0 Then
                    filteredItemDt.Rows.Add(dtRow("ItemID"), dtRow("ItemName"))
                End If
            Next



            rptPhotographsItemName.DataSource = filteredItemDt
            rptPhotographsItemName.DataBind()

        End If
    End Sub

    Sub rptPhotographsItemName_DataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptInspectionPhotographs As Repeater = DirectCast(e.Item.FindControl("rptInspectionPhotographs"), Repeater)
            'Dim rptItemName As Repeater = DirectCast(e.Item.FindControl("rptItemName"), Repeater)

            Dim lblPhotographsItemId As Label = DirectCast(e.Item.FindControl("lblPhotographsItemId"), Label)
            'filter TableArea where location id is lblLocationId.text 

            Dim filteredDt As DataTable = New DataTable()
            Dim areaDt As DataTable = resultDataSet.Tables(ApplicationConstants.InspectionPhotographsDt)
            Dim areaDv As DataView = areaDt.AsDataView()


            areaDv.RowFilter = "ItemId =  " + lblPhotographsItemId.Text.ToString()
            filteredDt = areaDv.ToTable()

            Dim currentPage As Double = (filteredDt.Rows.Count() / 25) + Convert.ToInt32(hdnPaging.Value)
            lblInspectionPhotographsNo.Text = Decimal.Ceiling(currentPage).ToString()
            hdnPageNo.Value = Decimal.Ceiling(currentPage).ToString()
            rptInspectionPhotographs.DataSource = filteredDt
            rptInspectionPhotographs.DataBind()

        End If
    End Sub

#End Region

#End Region

End Class


