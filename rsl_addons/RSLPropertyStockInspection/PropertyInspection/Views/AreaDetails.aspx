﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AreaDetails.aspx.vb" Inherits="PropertyInspection.AreaDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .last tr th:lastchild
        {
            border-bottom: none;
        }
        tr:last-child
        {
            border-bottom: none;
        }
        
        table
        {
            border-collapse: collapse;
            border-spacing: 0;
        }
    </style>
</head>
<body>
    <form id="formCheckList" runat="server">
    <div id="divCheckList">
        <p>
            <strong>Checklist:</strong></p>
        <table id="tblchecklistAreaDetails" width="80%" style="border: 2px solid; border-collapse: collapse;
            margin-bottom: 20px">
            <asp:Repeater ID="rptCheckListLocationName" runat="server">
                <ItemTemplate>
                    <tr>
                        <td width="20%" style="border-bottom: 2px solid; border-right: 2px solid;">
                            <asp:Label ID="lblCheckListLocationHeading" runat="server" Text="Checklist"></asp:Label>
                        </td>
                        <td style="border-bottom: 2px solid; border-right: 2px solid; padding-right: 10%;"
                            align="right">
                            <asp:Label ID="lblChecklistUpdatedHeading" runat="server" Text="Updated"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" style="border-bottom: 2px solid; padding-left:4%;">
                            <asp:Label ID="lblCheckListLocationId" Visible="false" runat="server" Text='<%# Bind("LocationID") %>'></asp:Label>
                            <asp:Label ID="lblCheckListLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                        </td>
                        <td width="90%">
                            <asp:Repeater ID="rptCheckListAreaName" runat="server" OnItemDataBound="rptCheckListAreaName_DataBound">
                                <ItemTemplate>
                                    <table id="Area" width="100%" style="border-collapse: collapse; border-bottom: 2px solid">
                                        <tr>
                                            <td width="30%" style="border-right: 2px solid; border-left: 2px solid; padding-left:2%;">
                                                <asp:Label ID="lblCheckListAreaId" Visible="false" runat="server" Text='<%# Bind("AreaID") %>'></asp:Label>
                                                <asp:Label ID="lblCheckListAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptCheckListItemName" runat="server" OnItemDataBound="rptCheckListItemName_DataBound">
                                                    <ItemTemplate>
                                                        <table width="100%" style="border-collapse: collapse; border-bottom: 2px solid">
                                                            <tr>
                                                                <td width="50%" style="border-right: 2px solid; padding-left:2%;">
                                                                    <asp:Label ID="lblCheckListItemId" Visible="false" runat="server" Text='<%# Bind("ItemID") %>'></asp:Label>
                                                                    <asp:Label ID="lblCheckListItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td width="80%" id="divCheckListSubItem" runat="server">
                                                                    <asp:Repeater runat="server" ID="rptSubItem" OnItemDataBound="rptCheckListSubItemName_DataBound">
                                                                        <ItemTemplate>
                                                                            <table width="100%" style="border-collapse: collapse; border-bottom: 2px solid">
                                                                                <tr>
                                                                                    <td width="50%" style="border-right: 2px solid; padding-left:2%;">
                                                                                        <asp:Label ID="lblCheckListSubItemId" Visible="false" runat="server" Text='<%# Bind("SubItemID") %>'></asp:Label>
                                                                                        <asp:Label ID="lblCheckListSubItemName" runat="server" Text='<%# Bind("SubItemName") %>'></asp:Label>
                                                                                    </td>
                                                                                    <td style="border-color: Black; border-left: 1px; border-bottom: 1px; border-top: 1px; padding-left:2%;">
                                                                                        <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                                <td style="border-color: Black; border-left: 1px; border-bottom: 1px; border-top: 1px; padding-left:2%;">
                                                                    <%--  <asp:Label ID="lblParameterId" Visible="false" runat="server" Text='<%# Bind("ItemId") %>'></asp:Label>
                                                                    <asp:Label ID="lblPropertyId" Visible="false" runat="server" Text='<%# Bind("PropertyId") %>'></asp:Label>--%>
                                                                    <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    </form>
</body>
</html>
