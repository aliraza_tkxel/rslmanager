﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecordOfInspection.aspx.vb" Inherits="PropertyInspection.RecordOfInspection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 161px;
        }
        .style2
        {
            width: 320px;
        }
        #tblUpperLeft
        {
            height: 194px;
            width: 765px;
        }
        .style6
        {
            width: 166px;
            height: 46px;
        }
        .style7
        {
            width: 238px;
        }
        
        #tblPropertyDetails 
        {
            margin-bottom:2px;
        }
        
        #tblAsbestosDetails
        {
            margin-bottom:20px;
        }
        .style8
        {
            width: 161px;
            height: 48px;
        }
        .style9
        {
            width: 166px;
            height: 48px;
        }
        .style10
        {
            width: 161px;
            height: 46px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id = "upperTable">
        
            <p><strong>Record of Inspection</strong></p>         
            <table id = "tblPropertyDetails" style="border-collapse:collapse">
                    <tr>
                        <td class="style1" style="border-top:1px solid;border-left:1px solid;border-bottom:1px solid">Property:</td>
                        <td class="style2" style="border-top:1px solid;border-left:1px solid;border-bottom:1px solid"><asp:label id="lblPropertyAddress" runat="server" Text='<%# Eval("Property") %>'/></td>
                        <td rowspan="8" class="style7" style="border-left:1px solid;border-bottom:1px solid;border-top:1px solid;border-right:1px solid">Image Here</td>
                        
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Property Ref:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblPropertyRef" runat="server" Text='<% Eval("PropertyRef") %>'/></td>
                        
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Tenancy Ref:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblTenancyRef" runat="server" Text='<% Eval("TenancyRef") %>' /></td>
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Customer Ref:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblCustomerRef" runat="server" Text='<% Eval("CustomerRef") %>' /></td>
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Surveyor:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblSurveyor" runat="server" Text='<% Eval("SurveyorName") %>'/></td>
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Inspection Type:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblInspectionType" runat="server" Text='<% Eval("InspectionType") %>' /></td>
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Date/Time:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblDateTime" runat="server" Text='<% Eval("Date/Time") %>' /></td>
                    </tr>
                    <tr>
                        <td class="style1" style="border-left:1px solid;border-bottom:1px solid">Completed:</td>
                        <td class="style2" style="border-left:1px solid;border-bottom:1px solid"><asp:label id="lblPropertyCompleted" runat="server" Text='<% Eval("Completed") %>' /></td>   
                    </tr>                                   
            </table>

            <table id="tblAsbestosDetails" style="border-collapse:collapse"">
                <tr>
                    <td class="style10" style="border-top:1px solid;border-left:1px solid;border-bottom:1px solid">Asbestos Risk: </td>
                    <td class="style6" style="border-top:1px solid;border-left:1px solid;border-bottom:1px solid;  border-right:1px solid"><asp:label ID="lblAsbestosRisk" runat="server" Text='<% Eval("AsbestosRisk") %>'/></td>
                </tr>
                <tr>
                    <td class="style8" style="border-left:1px solid;border-bottom:1px solid">Previous Survey: </td>
                    <td class="style9" style="border-left:1px solid;border-bottom:1px solid;  border-right:1px solid"><asp:label ID="lblPreviousSurvey" runat="server" Text='<% Eval("PreviousSurvey") %>'/></td>
                </tr>
                
            </table>
        <asp:GridView ID="grdDimensions" runat="server" AllowPaging="False" 
                AutoGenerateColumns="True" AutoGenerateRows="True" Height="307px" Width="373px">
        </asp:GridView>
           
    </div>
    </form>
</body>
</html>
