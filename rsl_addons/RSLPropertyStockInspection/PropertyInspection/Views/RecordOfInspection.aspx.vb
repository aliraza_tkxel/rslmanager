﻿Imports System
Imports System.Data
Imports PropertyInspection_BusinessLogic
Imports PropertyInspection_BusinessObject
Imports PropertyInspection_Utilities
Imports System.Drawing
Imports System.Globalization
Imports System.Threading
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class RecordOfInspection
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyID As String = "BHA0000119"
        Dim objInspectionRecordMainPageBL As InspectionRecordMainPageBL = New InspectionRecordMainPageBL
        objInspectionRecordMainPageBL.getMainPageContent(resultDataSet, propertyID)
        lblPropertyAddress.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("Property")
        lblPropertyRef.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("PropertyRef")
        If (IsDBNull(resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("TenancyRef"))) Then
            lblTenancyRef.Text = "N/A"
        Else
            lblTenancyRef.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("TenancyRef")
        End If

        If (IsDBNull(resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("CustomerRef"))) Then
            lblCustomerRef.Text = "N/A"
        Else
            lblCustomerRef.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("CustomerRef")
        End If

        If (IsDBNull(resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("SurveyorName"))) Then
            lblSurveyor.Text = "N/A"
        Else
            lblSurveyor.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("SurveyorName")
        End If

        If (IsDBNull(resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("InspectionType"))) Then
            lblInspectionType.Text = "N/A"
        Else
            lblInspectionType.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("InspectionType")
        End If


        lblDateTime.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("Date/Time")

        If (IsDBNull(resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("Completed"))) Then
            lblPropertyCompleted.Text = "N/A"
        Else
            lblPropertyCompleted.Text = resultDataSet.Tables("TablePropertyDetails").Rows(0).Item("Completed")
        End If

        If (IsDBNull(resultDataSet.Tables("TableSurvey").Rows(0).Item("PreviousSurvey"))) Then
            lblPreviousSurvey.Text = "N/A"
        Else
            lblPreviousSurvey.Text = resultDataSet.Tables("TableSurvey").Rows(0).Item("PreviousSurvey")
        End If


        grdDimensions.DataSource = resultDataSet.Tables("TableDimensions")
        grdDimensions.DataBind()


    End Sub

End Class