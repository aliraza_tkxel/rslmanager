﻿Imports System
Imports System.IO
Imports PropertyInspection_BusinessObject
Imports PropertyInspection_Utilities
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data


Namespace PropertyInspection_DataAccess
    Public Class InspectionRecordMainPageDAL : Inherits BaseDAL

#Region "Functions"

#Region "Inspection Record Main Page"

        Sub getMainPageContent(ByRef resultDataSet As DataSet, ByRef propertyID As String)

            Dim ParameterList As ParameterList = New ParameterList()

            Dim paraPropertyID As ParameterBO = New ParameterBO("propertyID", propertyID, DbType.String)
            ParameterList.Add(paraPropertyID)

            Dim IResultDataReader As IDataReader = MyBase.SelectRecord(ParameterList, SpNameConstants.InspectionRecordMainPage)
            resultDataSet.Load(IResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.PropertyDetailsDt, ApplicationConstants.AsbestosDt, ApplicationConstants.SurveyDt, ApplicationConstants.DimensionsDt)
        End Sub

#End Region

#Region "AreaDetails"

        Sub getAreaDetails(ByRef resultDataSet As DataSet, ByRef propertyID As String, ByRef surveyID As String)
            Dim ParameterList As ParameterList = New ParameterList()
            Dim paraPropertyID As ParameterBO = New ParameterBO("propertyID", propertyID, DbType.String)
            ParameterList.Add(paraPropertyID)

            Dim paraSurveyID As ParameterBO = New ParameterBO("surveyID", surveyID, DbType.Int32)
            ParameterList.Add(paraSurveyID)
            Dim IResultDataReader As IDataReader = MyBase.SelectRecord(ParameterList, SpNameConstants.InspectionRecordAreaDetials)
            resultDataSet.Load(IResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.LocationDt, ApplicationConstants.AreaDt, ApplicationConstants.ItemDt, ApplicationConstants.SubItemDt, ApplicationConstants.UpdatedDt, ApplicationConstants.ParamNameDt, ApplicationConstants.ParamValueDt, ApplicationConstants.PreInsertedValueDt, ApplicationConstants.DatesDt, ApplicationConstants.InspectionNotesDt, ApplicationConstants.InspectionPhotographsDt, ApplicationConstants.HeatingsDt)
        End Sub
#End Region

#Region "SavePDFDocument"
        Public Function saveDetailsDocument(ByVal propertyId As String, ByVal surveyId As Integer, ByVal InspectionDocument As Byte(), ByVal createdBy As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim surveyIdParam As ParameterBO = New ParameterBO("surveyId", surveyId, DbType.Int32)
            parametersList.Add(surveyIdParam)

            Dim InspectionDocumentParam As ParameterBO = New ParameterBO("InspectionDocument", InspectionDocument, DbType.Binary)
            parametersList.Add(InspectionDocumentParam)

            Dim CreatedByParam As ParameterBO = New ParameterBO("createdBy", createdBy, DbType.String)
            parametersList.Add(CreatedByParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveInspectionRecordPDFDocument)

            Return True
        End Function
#End Region

#End Region

    End Class
End Namespace
