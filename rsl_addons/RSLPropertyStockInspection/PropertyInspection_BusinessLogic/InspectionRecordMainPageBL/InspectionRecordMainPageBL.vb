﻿Imports System.Web
Imports PropertyInspection_DataAccess
Imports PropertyInspection_BusinessObject
Imports PropertyInspection_Utilities
Imports System.Drawing
Imports System.IO
Imports System.Web.UI.WebControls
Imports System
Imports System.Data.Common
Imports System.Data.SqlClient


Namespace PropertyInspection_BusinessLogic

    Public Class InspectionRecordMainPageBL
        Dim objInspectionRecordMainPageDAL As InspectionRecordMainPageDAL = New InspectionRecordMainPageDAL()

#Region "MainPageView"

        Sub getMainPageContent(ByRef resultDataSet As DataSet, ByRef propertyID As String)
            'Dim objInspectionRecordMainPageDAL As InspectionRecordMainPageDAL = New InspectionRecordMainPageDAL()
            objInspectionRecordMainPageDAL.getMainPageContent(resultDataSet, propertyID)
        End Sub

#End Region

#Region "AreaDetails"
        Sub getAreaDetails(ByRef resultDataSet As DataSet, ByRef propertyID As String, ByRef surveyID As String)

            objInspectionRecordMainPageDAL.getAreaDetails(resultDataSet, propertyID, surveyID)
        End Sub
#End Region

#Region "SavePDFDocument"
        Sub saveDetailsDocument(ByVal propertyId As String, ByVal surveyId As Integer, ByVal InspectionDocument As Byte(), ByVal createdBy As String)
            objInspectionRecordMainPageDAL.saveDetailsDocument(propertyId, surveyId, InspectionDocument, createdBy)
        End Sub
#End Region
    End Class

End Namespace
