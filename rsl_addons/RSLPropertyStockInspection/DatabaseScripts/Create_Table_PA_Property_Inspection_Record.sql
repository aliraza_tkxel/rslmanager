USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PA_Property_Inspection_Record]    Script Date: 08/15/2014 16:29:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PA_Property_Inspection_Record](
	[InspectionId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](50) NULL,
	[SurveyId] [int] NULL,
	[InspectionDocument] [image] NULL,
	[inspectionDate] [datetime] NULL,
	[createdBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PA_Property_Inspection_record] PRIMARY KEY CLUSTERED 
(
	[InspectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


