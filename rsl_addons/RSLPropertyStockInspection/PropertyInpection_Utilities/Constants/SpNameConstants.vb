﻿Imports System

Namespace PropertyInspection_Utilities

    Public Class SpNameConstants

#Region "Record of Inspection"
        Public Const InspectionRecordMainPage As String = "PROPERTYINSPECTION_InspectionRecordMainPage"
        Public Const InspectionRecordAreaDetials As String = "PROPERTYINSPECTION_InspectionRecordAreaDetails"
        Public Const SaveInspectionRecordPDFDocument As String = "PROPERTYINSPECTION_SaveDetailsDocument"
#End Region

    End Class

End Namespace
