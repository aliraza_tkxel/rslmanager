﻿Namespace PropertyInspection_Utilities

    Public Class ApplicationConstants

#Region "General"
        Public Const NotAvailable As String = "N/A"
        Public Const AsbestosNone As String = "None"
        Public Const Yes As String = "Yes"
        Public Const No As String = "No"
#End Region

#Region "MainPageDataTables"
        Public Const PropertyDetailsDt As String = "TablePropertyDetails"
        Public Const AsbestosDt As String = "TableAsbestos"
        Public Const SurveyDt As String = "TableSurvey"
        Public Const DimensionsDt As String = "TableDimensions"

#End Region

#Region "Details"
        Public Const LocationDt As String = "TableLocation"
        Public Const AreaDt As String = "TableArea"
        Public Const ItemDt As String = "TableItem"
        Public Const SubItemDt As String = "TableSubItem"
        Public Const HeatingsDt As String = "TableHeating"
        Public Const UpdatedDt As String = "TableUpdated"
        Public Const ParamNameDt As String = "TableParamName"
        Public Const ParamValueDt As String = "TableParamValue"
        Public Const PreInsertedValueDt As String = "TablePreInsertedValue"
        Public Const DatesDt As String = "TableDates"
        Public Const InspectionNotesDt As String = "TableInspectionNotes"
        Public Const InspectionPhotographsDt As String = "TableInspectionPhotographs"

#End Region

#Region "DataTableColumnNames"
        Public Const PropertyAddressColumn As String = "Property"
        Public Const PropertyReferenceColumn As String = "PropertyRef"
        Public Const TenancyReferenceColumn As String = "TenancyRef"
        Public Const CustomerReferenceColumn As String = "CustomerRef"
        Public Const SurveyorNameColumn As String = "SurveyorName"
        Public Const InspectionTypeColumn As String = "InspectionType"
        Public Const DateTimeColumn As String = "Date/Time"
        Public Const CompletedColumn As String = "Completed"
        Public Const PreviousSurveyColumn As String = "PreviousSurvey"
        Public Const ImageName As String = "ImageName"
        Public Const ImageUrl As String = "ImagePath"
#End Region

#Region "Dataset keys"
        Public Const HeatingItem As String = "Heating"
#End Region

    End Class
End Namespace
