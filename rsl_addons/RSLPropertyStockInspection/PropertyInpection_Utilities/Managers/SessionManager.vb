﻿Imports System.Web.HttpContext
Imports PropertyInspection_BusinessObject
Imports System.Web.UI.WebControls

Public Class SessionManager

#Region "MainPage"

#Region "Set/Get InspectionRecordMainPageDataSet"
    Public Const InspectionRecordMainPageDataSet As String = "InspectionRecordMainPageDataSet"
#End Region

#Region "Set InspectionRecordMainPageDataSet"
    Public Shared Sub setInspectionRecordMainPageDataSet(ByRef InspectionMainPageDataSet As DataSet)
        Current.Session(InspectionRecordMainPageDataSet) = InspectionMainPageDataSet
    End Sub
#End Region

#Region "Get InspectionRecordMainPageDataSet"
    Public Shared Function getInspectionRecordMainPageDataSet() As DataSet
        If (IsNothing(Current.Session(InspectionRecordMainPageDataSet))) Then
            Dim recallTheDataSet As New DataSet
            Return recallTheDataSet
        Else
            Return CType(Current.Session(InspectionRecordMainPageDataSet), DataSet)
        End If
    End Function
#End Region

#Region "Remove InspectionRecordMainPageDataSet"
    Public Shared Sub removeInspectionRecordMainPageDataSet()
        If (Not IsNothing(Current.Session(InspectionRecordMainPageDataSet))) Then
            Current.Session.Remove(InspectionRecordMainPageDataSet)
        End If
    End Sub
#End Region

#End Region


#Region "Details"
#Region "Set/Get InspectionRecordDataSet"
    Public Const InspectionRecordDataSet As String = "InspectionRecordDataSet"
#End Region

#Region "Set InspectionRecordDataSet"

    Public Shared Sub setInspectionRecordDataSet(ByRef InspectionDataSet As DataSet)
        Current.Session(InspectionRecordDataSet) = InspectionDataSet
    End Sub

#End Region

#Region "Get InspectionRecordDataSet"

    Public Shared Function getInspectionRecordDataSet() As DataSet
        If (IsNothing(Current.Session(InspectionRecordDataSet))) Then
            Dim recallDataSet As New DataSet
            Return recallDataSet
        Else
            Return CType(Current.Session(InspectionRecordDataSet), DataSet)
        End If
    End Function
#End Region

#Region "Remove InspectionRecordDataSet"
    Public Shared Sub removeInspectionRecordDataSet()
        If (Not IsNothing(Current.Session(InspectionRecordDataSet))) Then
            Current.Session.Remove(InspectionRecordDataSet)
        End If
    End Sub
#End Region
#End Region

End Class
