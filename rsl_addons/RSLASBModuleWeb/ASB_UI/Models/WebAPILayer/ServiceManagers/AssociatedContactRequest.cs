﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Assoicated Contact Request format
    /// </summary>
    public class AssociatedContactRequest
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// Converts this object into JSON String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}