﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Assoicated contact request/response object
    /// </summary>
    public class AssociatedContact
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Contact Name
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// Organization
        /// </summary>
        public string Organization { get; set; }
        /// <summary>
        /// Telephone
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// Contact ID
        /// </summary>
        public int ContactId { get; set; }
        /// <summary>
        /// Status of contact
        /// </summary>
        public bool IsActive { get; set; }
    }
}