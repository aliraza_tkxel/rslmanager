﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Services that are not related to any specific module
    /// </summary>
    public class CommonServices : BaseService
    {
        /// <summary>
        /// Get case categories
        /// </summary>
        /// <returns><see cref="IEnumerable{Category}"/></returns>
        public IEnumerable<Category> GetCategories()
        {
            string endpoint = "Common/GetCategory";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<Category>> result = null;

            result = Task.Factory.StartNew<IEnumerable<Category>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<Category>>(client.DownloadString(GetBaseUrl() + endpoint));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Get case risk levels
        /// </summary>
        /// <returns><see cref="IEnumerable{RiskLevel}"/></returns>
        public IEnumerable<RiskLevel> GetRiskLevels()
        {
            string endpoint = "Common/GetRiskLevel";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<RiskLevel>> result = null;

            result = Task.Factory.StartNew<IEnumerable<RiskLevel>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<RiskLevel>>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Get case officers
        /// </summary>
        /// <returns><see cref="IEnumerable{CaseOfficer}"/></returns>
        public IEnumerable<CaseOfficer> GetCaseOfficers()
        {
            string endpoint = "Common/GetCaseOfficer";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<CaseOfficer>> result;

            result = Task.Factory.StartNew<IEnumerable<CaseOfficer>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CaseOfficer>>(client.DownloadString(GetBaseUrl() + endpoint));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get Stages
        /// </summary>
        /// <returns><see cref="IEnumerable{Stage}"/></returns>
        public IEnumerable<Stage> GetStages()
        {
            string endpoint = "Common/GetStages";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<Stage>> result;

            result = Task.Factory.StartNew<IEnumerable<Stage>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<Stage>>(client.DownloadString(GetBaseUrl() + endpoint));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get Stages
        /// </summary>
        /// <returns><see cref="IEnumerable{Stage}"/></returns>
        public IEnumerable<CloseType> GetClosingTypes()
        {
            string endpoint = "Common/GetClosingTypes";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<CloseType>> result;

            result = Task.Factory.StartNew<IEnumerable<CloseType>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<CloseType>>(client.DownloadString(GetBaseUrl() + endpoint));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get SubCategories
        /// </summary>
        /// <returns><see cref="IEnumerable{SubCategory}"/></returns>
        public IEnumerable<SubCategory> GetSubCategories(int stage)
        {
            string endpoint = "Common/GetSubCategories";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<SubCategory>> result;

            result = Task.Factory.StartNew<IEnumerable<SubCategory>>(() =>
            {
                try
                {
                    //client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    //var requestString = stage.ToString();
                    //var response = client.UploadString(GetBaseUrl() + endpoint, "POST", requestString);
                    return JsonConvert.DeserializeObject<List<SubCategory>>(client.DownloadString(GetBaseUrl() + endpoint + "?stageId=" + stage));
                    //return new List<Stage>();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get Stages
        /// </summary>
        /// <returns><see cref="IEnumerable{Types}"/></returns>
        public IEnumerable<Types> GetTypes(int subCategory)
        {
            string endpoint = "Common/GetTypes";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<Types>> result;

            result = Task.Factory.StartNew<IEnumerable<Types>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<Types>>(client.DownloadString(GetBaseUrl() + endpoint + "?subCategoryId=" + subCategory));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Is team lead
        /// </summary>
        /// <param name="caseid">Case ID</param>
        /// <returns><see cref="CaseDetailResponse"/></returns>
        public int IsManager(int employeeId)
        {
            string endpoint = "Common/IsManager?employeeId=" + employeeId;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<int> result;
            var url = GetBaseUrl() + endpoint;
            result = Task.Factory.StartNew<int>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<int>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get Stages
        /// </summary>
        /// <returns><see cref="IEnumerable{SubType}"/></returns>
        public IEnumerable<SubType> GetSubTypes(int types)
        {
            string endpoint = "Common/GetSubTypes";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<SubType>> result;

            result = Task.Factory.StartNew<IEnumerable<SubType>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<SubType>>(client.DownloadString(GetBaseUrl() + endpoint + "?typeId=" + types));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// Get Incident Types
        /// </summary>
        /// <returns><see cref="IEnumerable{CaseOfficer}"/></returns>
        public IEnumerable<IncidentType> GetIncidentTypes()
        {
            string endpoint = "Common/GetIncidentType";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<IncidentType>> result;

            result = Task.Factory.StartNew<IEnumerable<IncidentType>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<IncidentType>>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Gets person types
        /// </summary>
        /// <returns>List of <see cref="PersonTypes"/></returns>
        public IEnumerable<PersonTypes> GetPersonTypes()
        {
            string endpoint = "Common/GetPersonType";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<PersonTypes>> result;

            result = Task.Factory.StartNew<IEnumerable<PersonTypes>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<PersonTypes>>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        public IEnumerable<PersonTypes> GetPersonTitles()
        {
            string endpoint = "Common/GetPersonTitles";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<PersonTypes>> result;

            result = Task.Factory.StartNew<IEnumerable<PersonTypes>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<PersonTypes>>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reqBO">Object of <see cref="PersonRequest"/></param>
        /// <returns>Object of <see cref="Person"/></returns>
        public Person GetPerson(PersonRequest reqBO)
        {
            string endpoint = "Person/PopulatePersonDetails";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = reqBO.ToString();
                var response = client.UploadString(GetBaseUrl() + endpoint, "POST", requestString);
                Person resp = Person.ToObject(response);
                //if (resp != null)
                //{
                //    resp.Id = 1;
                //}
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Search matching persons
        /// </summary>
        /// <param name="request"><see cref="SearchPersonRequest"/></param>
        /// <returns>JSON Array. <see cref="JArray"/></returns>
        public  string SearchPerson(SearchPersonRequest request)
        {
            string endpoint = "person/SearchPerson";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                //SearchPersonResponse matches = SearchPersonResponse.ToObject(response);
                //JArray matches = JArray.Parse(response);
                //dynamic matches = Json.Decode(response);
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ":" + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Search matching Complainent/Preperator
        /// </summary>
        /// <param name="request"><see cref="SearchPersonRequest"/></param>
        /// <returns>JSON Array. <see cref="JArray"/></returns>
        public string SearchCompPrep(SearchPersonRequest request)
        {
            string endpoint = "person/SearchCompPrep";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                //SearchPersonResponse matches = SearchPersonResponse.ToObject(response);
                //JArray matches = JArray.Parse(response);
                //dynamic matches = Json.Decode(response);
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ":" + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Updates person info
        /// </summary>
        /// <param name="request"><see cref="UpdatePersonRequest"/></param>
        /// <returns><see cref="Person"/></returns>
        public Person UpdatePerson(UpdatePersonRequest request)
        {
            string endpoint = "Person/UpdatePerson";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(GetBaseUrl() + endpoint, "POST", requestString);
                Person resp = Person.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Get ActionTypes from API
        /// </summary>
        /// <returns>List of <see cref="ActionType"/></returns>
        public IEnumerable<ActionType> GetActionTypes()
        {
            string endpoint = "Common/GetActionType";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<IEnumerable<ActionType>> result = null;

            result = Task.Factory.StartNew<IEnumerable<ActionType>>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<ActionType>>(client.DownloadString(GetBaseUrl() + endpoint));

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }

        #region Menu/Navigation 
        /// <summary>
        /// Get Top Navigation Menu items based upon user
        /// </summary>
        /// <param name="request"><see cref="MenuRequest"/></param>
        /// <returns><see cref="MenuResponse"/> object</returns>
        public MenuResponse GetTopNavigationMenu(MenuRequest request)
        {
            string endpoint = "Menus/GetMenus";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(GetBaseUrl() + endpoint, "POST", requestString);
                MenuResponse menuItems = JsonConvert.DeserializeObject<MenuResponse>(response);
                return menuItems;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        #endregion
    }
}