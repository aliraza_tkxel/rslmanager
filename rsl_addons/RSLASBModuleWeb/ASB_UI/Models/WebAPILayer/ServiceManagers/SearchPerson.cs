﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response when matching is found
    /// </summary>
    public class SearchPerson
    {
        public string PersonName { get; set; }
        public string Dob { get; set; }
        public string personType { get; set; }
        public int PersonId { get; set; }
    }
}