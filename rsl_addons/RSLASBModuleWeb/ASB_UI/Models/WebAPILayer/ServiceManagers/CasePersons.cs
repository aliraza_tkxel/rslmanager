﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Associated Perpetrators/Persons of New Case
    /// </summary>
    public class CasePersons
    {
        /// <summary>
        /// Person role as "Perpetrator/Complainant"
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// Person role as "Perpetrator/Complainant"
        /// </summary>
        public string PersonRole { get; set; }
        /// <summary>
        /// Person ID
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
    }
}