﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class CaseAmendDetail
    {
        public string DateRecorded { get; set; }
        public string DateReported { get; set; }
        public string IncidentDate { get; set; }
        public string IncidentTime { get; set; }
        public int CaseOfficer { get; set; }
        public int? CaseOfficerTwo { get; set; }
        public int Category { get; set; }
        public int incidentType { get; set; }
        public int? RiskLevel { get; set; }
        public string IncidentDescription { get; set; }
        public int PoliceNotified { get; set; }
        public int CrimeCaseNumber { get; set; }
        public string NextFollowupDate { get; set; }
        public string FollowupDescription { get; set; }
        public string ClosedDate { get; set; }
        public string ClosedDescription { get; set; }
        public string CaseStatus { get; set; }
        public string ReviewDate { get; set; }
        public int StageId { get; set; }
        public int? SelectedSubCategoryId { get; set; }
        public int? SelectedTypeId { get; set; }
        public int? SelectedSubTypeId { get; set; }
    }
}