﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class AmendCaseDetailResponse
    {
        public CaseAmendDetail objUpdateCaseBO { get; set; }

        public IEnumerable<CasePersons> objPersonList { get; set; }

        public static AmendCaseDetailResponse ToObject(string jsonResponse) {
            AmendCaseDetailResponse obj = (AmendCaseDetailResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(AmendCaseDetailResponse));
            return obj;
        }
    }
}