﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Class for Add/Remove persons from case
    /// </summary>
    public class CasePersonOperationRequest
    {
        /// <summary>
        /// Person Info. <see cref="CasePersons"/>
        /// </summary>
        public CasePersons objPerson { get; set; }
        /// <summary>
        /// Recorded by
        /// </summary>
        public int RecordedBy { get; set; }
        /// <summary>
        /// Converts this object to String
        /// </summary>
        /// <returns>JSON string</returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}