﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Request object to fetch previous/associated cases of person
    /// </summary>
    public class PreviousCasesRequest
    {
        /// <summary>
        /// Person ID to which previous cases required
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Employee or Customer
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// Complainant or Perpatrator
        /// </summary>
        public string PersonRole { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }


        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}