﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Request format for sending new case information on server.
    /// </summary>
    public class SaveCaseRequest
    {
        /// <summary>
        /// New Case forms data
        /// </summary>
        public NewCaseFields objCaseBO { get; set; }
        /// <summary>
        /// List of associated persons
        /// </summary>
        public IList<CasePersons> objPersonBO { get; set; }
        /// <summary>
        /// Recorded By Employee ID
        /// </summary>
        public int RecordedBy { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}