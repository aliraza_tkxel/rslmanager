﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// 
    /// </summary>
    public class CaseFilters
    {
        public int categoryId { get; set; }
        public int riskLevelId { get; set; }
        public int caseOfficerId { get; set; }
        public bool IsOverDueCases { get; set; }
        public bool IsHighRisk { get; set; }
        public bool IsOpenCases { get; set; }
        public bool openOrClose { get; set; }
        public int? personId { get; set; }
        public string personType { get; set; }
    }
}