﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Menu items that are displayed when hovered over Customer
    /// </summary>
    public class ModuleItem
    {
        /// <summary>
        /// Module ID
        /// </summary>
        public int ModuleId { get; set; }
        /// <summary>
        /// Module Name
        /// </summary>
        public string ModuleName { get; set; }
        /// <summary>
        /// Module Url for Page redirection
        /// </summary>
        public string Url { get; set; }
    }
}