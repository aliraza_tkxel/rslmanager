﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Person response in Case Amend/Detail screen
    /// </summary>
    public class CasePersonResponse
    {
        public string PersonType { get; set; }
        public string PersonRole { get; set; }
        public int PersonId { get; set; }
        public int CaseId { get; set; }
    }
}