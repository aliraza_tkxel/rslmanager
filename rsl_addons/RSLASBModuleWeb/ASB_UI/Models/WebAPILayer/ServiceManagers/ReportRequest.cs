﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{ 
    public class ReportRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int categoryId { get; set; }
        public int RiskLevelId { get; set; }
        public string Search { get; set; }
        public int CaseStatus { get; set; }
        public bool IsOverdue { get; set; }
        public int CaseOfficerId { get; set; }
        public DateTime? ReportedFrom { get; set; }
        public DateTime? ReportedTo { get; set; }
        public string sortOrder { get; set; }
        public string sortBy { get; set; }
        /// <summary>
        /// Converts object to string json
        /// </summary>
        /// <returns>Json format of object</returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}