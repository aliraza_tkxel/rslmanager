﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// 
    /// </summary>
    public class DashboardRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CaseFilters objDashboardCaseFilterBO { get; set; }

        /// <summary>
        /// Converts object to string json
        /// </summary>
        /// <returns>Json format of object</returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}