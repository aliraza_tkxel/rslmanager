﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Menu Response
    /// </summary>
    public class MenuResponse
    {
        /// <summary>
        /// Sub Menu Items
        /// </summary>
        public IEnumerable<MenuItem> objMenusBO { get; set; }
        /// <summary>
        /// Sub Menu Items of Customer Module
        /// </summary>
        public IEnumerable<ModuleItem> objLstCustomerModuleMenus { get; set; }
        /// <summary>
        /// Converts JSON response into Object
        /// </summary>
        /// <param name="jsonResponse">string response of JSON</param>
        /// <returns><see cref="MenuResponse"/> object</returns>
        public static MenuResponse ToObject(string jsonResponse)
        {
            MenuResponse obj = (MenuResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(MenuResponse));
            return obj;
        }

    }   
}