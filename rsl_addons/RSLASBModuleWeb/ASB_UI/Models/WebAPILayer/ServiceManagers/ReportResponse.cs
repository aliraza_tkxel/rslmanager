﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class ReportResponse
    {
        public Pagination objPaginationBO { get; set; }
        public IEnumerable<RepCase> objCaseBO { get; set; }
        public static ReportResponse ToObject(string jsonResponse)
        {
            ReportResponse obj = (ReportResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(ReportResponse));
            return obj;
        }
    }
}