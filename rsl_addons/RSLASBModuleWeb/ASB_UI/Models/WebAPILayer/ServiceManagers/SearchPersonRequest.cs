﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Search person request format
    /// </summary>
    public class SearchPersonRequest
    {
        /// <summary>
        /// Filter, Customer or BHG Employee
        /// </summary>
        public string filter { get; set; }
        /// <summary>
        /// Input characters when typed in text box
        /// </summary>
        public string input { get; set; }
        /// <summary>
        /// Converts this oject into json string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}