﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Request BO to fetch person details
    /// </summary>
    public class PersonRequest
    {
        /// <summary>
        /// Person Type
        /// </summary>
        public string personType { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        public int PersonId { get; set; }

        /// <summary>
        /// Converts object to string json
        /// </summary>
        /// <returns>Json format of object</returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}