﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// All Case related services
    /// </summary>
    public class CaseServices : BaseService
    {
        /// <summary>
        /// Gets previous cases of Complainant/Perpatrator
        /// </summary>
        /// <param name="request"><see cref="PreviousCasesRequest"/></param>
        /// <returns><see cref="PreviousCasesResponse"/></returns>
        public PreviousCasesResponse GetPreviousCases(PreviousCasesRequest request)
        {
            string endpoint = "Case/GetAssociatedCasesList";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(GetBaseUrl() + endpoint, "POST", request.ToString());
                PreviousCasesResponse resp = PreviousCasesResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Get Joint Tenancy Previous Cases. This method is different from <see cref="CaseServices.GetPreviousCases(PreviousCasesRequest)"/>
        /// </summary>
        /// <param name="pIntPersonId">Person Id</param>
        /// <returns><see cref="PreviousCasesResponse"/></returns>
        public PreviousCasesResponse GetPersonsAssociatedCases(int pIntPersonId)
        {
            string endpoint = "Case/GetJointTenancyCasesList?customerId=" + pIntPersonId;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<PreviousCasesResponse> result;
            var url = GetBaseUrl() + endpoint;
            result = Task.Factory.StartNew<PreviousCasesResponse>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<PreviousCasesResponse>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Send case data to server
        /// </summary>
        /// <param name="request"><see cref="SaveCaseRequest"/> object</param>
        /// <returns>Saved Case Id</returns>
        public string SaveCase(SaveCaseRequest request)
        {
            string endpoint = "Case/AddCase";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                string resp = response.ToString();
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Send case data to server for Updating details
        /// </summary>
        /// <param name="request"><see cref="UpdateCaseRequest"/> object</param>
        /// <returns>Saved Case Id</returns>
        public string UpdateCaseDetails(UpdateCaseRequest request)
        {
            string endpoint = "Case/UpdateCaseDetails";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Encoding = UTF8Encoding.UTF8;
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                string resp = response.ToString();
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Saves new person in the system
        /// </summary>
        /// <param name="request"><see cref="Person"/> object</param>
        /// <returns>Person ID</returns>
        public string SaveNewPerson(Person request)
        {
            string endpoint = "Person/AddPerson";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                string resp = response.ToString();
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Case details 
        /// </summary>
        /// <param name="caseid">Case ID</param>
        /// <returns><see cref="CaseDetailResponse"/></returns>
        public CaseDetailResponse GetCaseDetails(int caseid)
        {
            string endpoint = "Case/PopulateUpdateCaseData?caseId=" + caseid;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<CaseDetailResponse> result;
            var url = GetBaseUrl() + endpoint;
            result = Task.Factory.StartNew<CaseDetailResponse>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<CaseDetailResponse>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Amend Case details 
        /// </summary>
        /// <param name="caseid">Case ID</param>
        /// <returns><see cref="CaseDetailResponse"/></returns>
        public AmendCaseDetailResponse GetAmendCaseDetails(int caseid)
        {
            string endpoint = "Case/PopulateUpdateAmendCaseData?caseId=" + caseid;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;

            Task<AmendCaseDetailResponse> result;
            var url = GetBaseUrl() + endpoint;
            result = Task.Factory.StartNew<AmendCaseDetailResponse>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<AmendCaseDetailResponse>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
        /// <summary>
        /// Get all associated case files from server
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public FileResponse GetAllFiles(AllFilesRequest request)
        {
            string endpoint = "File/GetAllFiles";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                FileResponse resp = FileResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Removes file from case
        /// </summary>
        /// <param name="request"><see cref="FileRemoveRequest"/></param>
        /// <returns><see cref="FileResponse"/></returns>
        public FileResponse RemoveFile(FileRemoveRequest request)
        {
            string endpoint = "File/RemoveFile";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                FileResponse resp = FileResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Adds file on server
        /// </summary>
        /// <param name="request"><see cref="FileRequest"/></param>
        /// <returns><see cref="FileResponse"/></returns>
        public FileResponse AddFile(FileRequest request)
        {
            string endpoint = "File/AddFile";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                FileResponse resp = FileResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Calls service to Add Contact in Associated case
        /// </summary>
        /// <param name="request"><see cref="AddContactRequest"/></param>
        /// <returns><see cref="AssociatedContactResponse"/></returns>
        public AssociatedContactResponse AddContact(AddContactRequest request)
        {
            string endpoint = "Contact/AddContact";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                AssociatedContactResponse resp = AssociatedContactResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Removes contact from case
        /// </summary>
        /// <param name="request"><see cref="RemoveContactRequest"/></param>
        /// <returns><see cref="AssociatedContactResponse"/></returns>
        public AssociatedContactResponse RemoveContact(RemoveContactRequest request)
        {
            string endpoint = "Contact/RemoveContact";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                AssociatedContactResponse resp = AssociatedContactResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Get all Associated contacts of case. This is used in pagination as well.
        /// </summary>
        /// <param name="request"><see cref="AssociatedContactRequest"/></param>
        /// <returns>Object of <see cref="AssociatedContactResponse"/></returns>
        public AssociatedContactResponse GetContacts(AssociatedContactRequest request)
        {

            string endpoint = "Contact/GetAllContact";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                AssociatedContactResponse resp = AssociatedContactResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Removes person from case
        /// </summary>
        /// <param name="request"></param>
        /// <returns>True if Cuccessfuly added in case</returns>
        public bool AddPersonInCase(CasePersonOperationRequest request)
        {
            string endpoint = "Case/AddPersonsAgainstCase";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                var resp = response.Trim().ToLower();
                return resp.Equals("true") ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool RemovePersonFromCase(CasePersonOperationRequest request)
        {
            string endpoint = "Case/RemovePersonsFromCase";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                var resp = response.Trim().ToLower();
                return resp.Equals("true") ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Get Journal/Actions of associated Cases
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public JournalDataResponse GetCaseJournalData(JournalDataRequest request)
        {
            string endpoint = "Journal/GetJournalData";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                JournalDataResponse resp = JournalDataResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Adds action in Journal through API
        /// </summary>
        /// <param name="request"><see cref="AddActionRequest"/></param>
        /// <returns>True if added Successfuly False otherwise</returns>
        public bool AddActionInJournal(AddActionRequest request)
        {
            string endpoint = "Journal/AddActionInJournal";
            var url = GetBaseUrl() + endpoint;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(url, "POST", requestString);
                bool resp = response.ToLower().Trim().Equals("true") ? true : false;
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}