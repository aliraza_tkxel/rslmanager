﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Menu Reponse 
    /// </summary>
    public class MenuItem
    {
        /// <summary>
        /// Menu Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// Menu name to be shown on Top Navigation
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// List os Sub Menus
        /// </summary>
        public IEnumerable<SubMenuItem> objLstSubMenus { get; set; }
    }
}