﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Remove contact Request
    /// </summary>
    public class RemoveContactRequest
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RemovedBy { get; set; }
        /// <summary>
        /// Contact ID
        /// </summary>
        public int ContactId { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// Converts object to JSON string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}