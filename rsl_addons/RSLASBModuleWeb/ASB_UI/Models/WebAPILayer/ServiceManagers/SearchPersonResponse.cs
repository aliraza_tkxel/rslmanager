﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response received from Searcg Person API
    /// </summary>
    public class SearchPersonResponse
    {
        /// <summary>
        /// List of mathing persons
        /// </summary>
        public IEnumerable<SearchPerson> matches { get; set; }
        /// <summary>
        /// Converts jsonResponse into Object
        /// </summary>
        /// <param name="jsonResponse">JSON string</param>
        /// <returns><see cref="SearchPersonResponse"/></returns>
        public static SearchPersonResponse ToObject(string jsonResponse)
        {
            SearchPersonResponse obj = (SearchPersonResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(SearchPersonResponse));
            return obj;
        }
    }
}