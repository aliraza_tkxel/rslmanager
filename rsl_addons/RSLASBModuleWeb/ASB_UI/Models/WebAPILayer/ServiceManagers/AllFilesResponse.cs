﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class AllFilesResponse
    {
        public IEnumerable<FileParams> objFileBO { get; set; }
        public Pagination objPaginationBO { get; set; }

        public static AllFilesResponse ToObject(string jsonResponse)
        {
            AllFilesResponse obj = (AllFilesResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(AllFilesResponse));
            return obj;
        }
    }
}