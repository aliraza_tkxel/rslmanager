﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Request format for sending new case information on server.
    /// </summary>
    public class UpdateCaseRequest
    {
        /// <summary>
        /// New Case forms data
        /// </summary>
        public UpdateCaseFields objCaseBO { get; set; }
        public int RecordedBy { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}