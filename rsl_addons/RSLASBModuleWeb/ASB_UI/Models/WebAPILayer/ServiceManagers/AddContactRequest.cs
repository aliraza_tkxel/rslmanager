﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Add Contact Request 
    /// </summary>
    public class AddContactRequest
    {
        /// <summary>
        /// New Contact information
        /// </summary>
        public AssociatedContact objContactBO { get; set; }
        /// <summary>
        /// <see cref="Pagination"/> 
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// Recorded by Employee
        /// </summary>
        public int RecordedBy { get; set; }
        /// <summary>
        /// Converts object to JSON String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}