﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class NewCaseFields
    {
        public DateTime DateRecorded { get; set; }
        public DateTime DateReported { get; set; }
        public DateTime IncidentDate { get; set; }
        public string IncidentTime { get; set; }
        public int CaseOfficer { get; set; }
        public int? CaseOfficerTwo { get; set; }
        public int Category { get; set; }
        public int Stage { get; set; }
        public int? SubCategory { get; set; }
        public int? Types { get; set; }
        public int? SubType { get; set; }
        public int incidentType { get; set; }
        public int? RiskLevel { get; set; }
        public string IncidentDescription { get; set; }
        public bool PoliceNotified { get; set; }
        public int CrimeCaseNumber { get; set; }
        public DateTime NextFollowupDate { get; set; }
        public string FollowupDescription { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ClosedDescription { get; set; }
        public int CaseStatus { get; set; }
        public DateTime? ReviewDate { get; set; }
    }
}