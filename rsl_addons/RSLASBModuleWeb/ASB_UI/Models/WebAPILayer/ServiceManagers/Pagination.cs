﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// For Pagination 
    /// </summary>
    public class Pagination
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRows { get; set; }
        public int TotalPages { get; set; }
        /// <summary>
        /// Function Name
        /// </summary>
        public string FunctionName { get; set; }
    }
}