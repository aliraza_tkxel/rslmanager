﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class FileParams
    {
        public int CaseId { get; set; }
        public string DateUploaded { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public bool IsActive { get; set; }
        public string FileType { get; set; }
        public string UploadedBy { get; set; }
        public string FileNotes { get; set; }
    }
}