﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class DashboardResponse
    {
        public Pagination objPaginationBO { get; set; }
        public DashboardAlert objDashboardAlertsBO { get; set; }
        public IEnumerable<Case> objDashboardCaseInfoBO { get; set; }
        public static DashboardResponse ToObject(string jsonResponse)
        {
            DashboardResponse obj = (DashboardResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse, 
                typeof(DashboardResponse));
            return obj;
        }
    }
}