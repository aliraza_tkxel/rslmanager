﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class FileRemoveRequest
    {
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int caseId { get; set; }
        /// <summary>
        /// File id to be deleted
        /// </summary>
        public int fileId { get; set; }
        /// <summary>
        /// "Document" or "Photograph"
        /// </summary>
        public string fileType { get; set; }
        /// <summary>
        /// ID of employee. TODO - Get it from Session
        /// </summary>
        public int RemovedBy { get; set; }
        /// <summary>
        /// Pagination
        /// </summary>
        public Pagination objPaginationBO { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}