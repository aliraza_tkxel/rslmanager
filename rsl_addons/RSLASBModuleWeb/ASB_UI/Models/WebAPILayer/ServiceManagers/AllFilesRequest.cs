﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// All files Request
    /// </summary>
    public class AllFilesRequest
    {
        public int caseId { get; set; }
        public string fileType { get; set; }
        public Pagination objPaginationBO { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}