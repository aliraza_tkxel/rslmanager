﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class FileResponse
    {
        public IEnumerable<FileParams> objFileBO { get; set; }
        public Pagination objPaginationBO { get; set; }
        public static FileResponse ToObject(string jsonResponse)
        {
            FileResponse obj = (FileResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(FileResponse));
            return obj;
        }
    }
}