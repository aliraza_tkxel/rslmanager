﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Add Action in Journal 
    /// </summary>
    public class AddActionRequest
    {
        /// <summary>
        /// Date when Action is recorded
        /// </summary>
        public DateTime RecordedDate { get; set; }
        /// <summary>
        /// Action ID
        /// </summary>
        public int ActionId { get; set; }
        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// Action Notes or Comments
        /// </summary>
        public string ActionNotes { get; set; }
        /// <summary>
        /// Employee/USer recorded this action
        /// </summary>
        public int RecordedBy { get; set; }
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// File path if Action is of Adding/Removing Document/Photograph 
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// File name if Action is of Adding/Removing Document/Photograph
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Followup date 
        /// </summary>
        public DateTime? FollowupDate { get; set; }
        /// <summary>
        /// Case closed date when
        /// </summary>
        public DateTime? ClosedDate { get; set; }
        /// <summary>
        /// Case Review date 
        /// </summary>
        public DateTime ReviewDate { get; set; }
        /// <summary>
        /// Case Stage if action is Stages
        /// </summary>
        public int? Stage { get; set; }
        /// <summary>
        /// Case SubCategory if action is Stages
        /// </summary>
        public int? SubCategory { get; set; }
        /// <summary>
        /// Case Types if action is Stages
        /// </summary>
        public int? Types { get; set; }
        /// <summary>
        /// Case SubType if action is Stages
        /// </summary>
        public int? SubType { get; set; }
        public int? ClosingType { get; set; }
        /// <summary>
        /// Converts this object to JSON String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}