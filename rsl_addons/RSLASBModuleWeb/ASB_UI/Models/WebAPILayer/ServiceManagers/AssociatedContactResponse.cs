﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response from Server
    /// </summary>
    public class AssociatedContactResponse
    {
        /// <summary>
        /// List of contacts
        /// </summary>
        public IEnumerable<AssociatedContact> objContactBO { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// Converts JSON response to object
        /// </summary>
        /// <param name="jsonResponse"></param>
        /// <returns></returns>
        public static AssociatedContactResponse ToObject(string jsonResponse)
        {
            AssociatedContactResponse obj = (AssociatedContactResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(AssociatedContactResponse));
            return obj;
        }
    }
}