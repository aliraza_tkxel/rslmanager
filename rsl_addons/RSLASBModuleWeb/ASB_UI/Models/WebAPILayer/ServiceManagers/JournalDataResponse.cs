﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response of All Journal Data
    /// </summary>
    public class JournalDataResponse
    {
        /// <summary>
        /// List of Journals
        /// </summary>
        public IEnumerable<CaseJournal> objJournalBO { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }
        /// <summary>
        /// Converts Json String of this type to Object
        /// </summary>
        /// <param name="jsonResponse">JSON String</param>
        /// <returns><see cref="JournalDataResponse"/></returns>
        public static JournalDataResponse ToObject(string jsonResponse)
        {
            JournalDataResponse obj = (JournalDataResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(JournalDataResponse));
            return obj;
        }
    }
}