﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class CaseDetailResponse
    {
        public CaseDetail objUpdateCaseBO { get; set; }

        public IEnumerable<CasePersons> objPersonList { get; set; }

        public static CaseDetailResponse ToObject(string jsonResponse) {
            CaseDetailResponse obj = (CaseDetailResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(CaseDetailResponse));
            return obj;
        }
    }
}