﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class SessionService:BaseService
    {
        public SessionResponse CreateSession(int userId)
        {
            string endpoint = "Session/CreateSession?userId=" + userId;
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            Task<SessionResponse> result;

            result = Task.Factory.StartNew<SessionResponse>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<SessionResponse>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
    }
}