﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Holds detail of single Action/Journal
    /// </summary>
    public class CaseJournal
    {
        /// <summary>
        /// Date when Action is recorded
        /// </summary>
        public DateTime RecordedDate { get; set; }
        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// Action Notes or Comments
        /// </summary>
        public string ActionNotes { get; set; }
        /// <summary>
        /// Employee/USer recorded this action
        /// </summary>
        public string RecordedBy { get; set; }
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// File path if Action is of Adding/Removing Document/Photograph 
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// File name if Action is of Adding/Removing Document/Photograph
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Stage if action is Stage
        /// </summary>
        public string stage { get; set; }
        /// <summary>
        /// Stage if action is Stage
        /// </summary>
        public int? StageId { get; set; }
        /// <summary>
        /// Subcategory if action is Stage
        /// </summary>
        public string SubCategory { get; set; }
        /// <summary>
        /// Subcategory if action is Stage
        /// </summary>
        public int? SubCategoryId { get; set; }
        /// <summary>
        /// Review Date for the action
        /// </summary>
        public DateTime? ReviewDate { get; set; }
    }
}