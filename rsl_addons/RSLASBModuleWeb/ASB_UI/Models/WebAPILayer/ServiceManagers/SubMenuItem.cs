﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response received 
    /// </summary>
    public class SubMenuItem
    {
        /// <summary>
        /// Parent MenuItem id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// Menu text
        /// </summary>
        public string SubMenuName { get; set; }
        /// <summary>
        /// Redirect Path
        /// </summary>
        public string Path { get; set; }
    }
}