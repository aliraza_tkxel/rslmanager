﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class FileRequest
    {
        public int CaseId { get; set; }
        public string DateUploaded { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public bool IsActive { get; set; }
        public string FileType { get; set; }
        public int UploadedBy { get; set; }
        public string FileNotes { get; set; }
        public Pagination objPaginationBO { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}