﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class ReportService : BaseService
    {
        public ReportResponse GetOpenCases(ReportRequest request)
        {

            string endpoint = "Reports/GetCaseManagementReportData";
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var requestString = request.ToString();
                var response = client.UploadString(GetBaseUrl() + endpoint, "POST", request.ToString());
                ReportResponse resp = ReportResponse.ToObject(response);
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}