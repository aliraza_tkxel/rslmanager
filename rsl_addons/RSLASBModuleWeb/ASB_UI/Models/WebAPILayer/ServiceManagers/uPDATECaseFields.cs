﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class UpdateCaseFields
    {
        public int CaseId { get; set; }
        public int CaseOfficer { get; set; }
        public int? CaseOfficerTwo { get; set; }
        public int Category { get; set; }
        public int Stage { get; set; }
        public int? SubCategory { get; set; }
        public int? Types { get; set; }
        public int? SubType { get; set; }
        public int incidentType { get; set; }
        public int? RiskLevel { get; set; }
        public string IncidentDescription { get; set; }
        public bool PoliceNotified { get; set; }
        public string FollowupDescription { get; set; }
    }
}