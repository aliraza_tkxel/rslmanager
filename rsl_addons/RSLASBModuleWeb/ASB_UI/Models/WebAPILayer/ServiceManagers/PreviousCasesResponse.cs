﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// Response object
    /// </summary>
    public class PreviousCasesResponse
    {
        /// <summary>
        /// List of <see cref="PreviousCase"/>
        /// </summary>
        public IEnumerable<PreviousCase> objAssociatedCasesPopupBO { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination objPaginationBO { get; set; }

        /// <summary>
        /// Converts json string to Object
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static PreviousCasesResponse ToObject(string jsonResponse)
        {
            PreviousCasesResponse obj = (PreviousCasesResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(PreviousCasesResponse));
            return obj;
        }
    }
}