﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    /// <summary>
    /// For Top Menu Request
    /// </summary>
    public class MenuRequest
    {
        /// <summary>
        /// User Id.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Module Name.
        /// </summary>
        public string ModuleName
        {
            get { return "Customer"; }
        }
        /// <summary>
        /// Converts object into Json String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}