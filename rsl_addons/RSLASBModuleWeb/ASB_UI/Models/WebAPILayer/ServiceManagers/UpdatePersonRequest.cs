﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.ServiceManagers
{
    public class UpdatePersonRequest
    {
        /// <summary>
        /// Person ID
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Type: "Customer" or "Employee"
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// Telephone Home
        /// </summary>
        public string TelephoneHome { get; set; }
        /// <summary>
        /// Telephone Work
        /// </summary>
        public string TelephoneWork { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Alternate Contact
        /// </summary>
        public string AlternateContact { get; set; }
        /// <summary>
        /// Converts this object to JSON String
        /// </summary>
        /// <returns>JSON String</returns>
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}