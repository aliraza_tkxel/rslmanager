﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Action type
    /// </summary>
    public class ActionType
    {
        public int key { get; set; }
        public string value { get; set; }
    }
}