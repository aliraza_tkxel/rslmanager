﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    public class DashboardAlert
    {
        /// <summary>
        /// Model for Dashboard Alerts
        /// </summary>
        public int openCaseCount { get; set; }
        public int highRiskCasesCount { get; set; }
        public int overdueCasesCount { get; set; }
        public bool IsOverdue { get; set; }
        public bool IsHighRisk { get; set; }
        public bool IsOpenCase { get; set; }
    }
}