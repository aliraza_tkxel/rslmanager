﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Case Incident Type
    /// </summary>
    public class IncidentType
    {
        /// <summary>
        /// Type ID
        /// </summary>
        public int key { get; set; }
        /// <summary>
        /// Type Name
        /// </summary>
        public string value { get; set; }
    }
}