﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Person role. Complainant or Perpatrator
    /// </summary>
    public enum PerosnRole
    {
        /// <summary>
        /// Complainant
        /// </summary>
        Complainant,
        /// <summary>
        /// Perpatrator
        /// </summary>
        Perpatrator
    }
    /// <summary>
    /// Person type. Employee or Customer
    /// </summary>
    public enum PersonType
    {
        /// <summary>
        /// BHG Organization Employee
        /// </summary>
        Employee,
        /// <summary>
        /// Customer if person is not employee of BHG Organization
        /// </summary>
        Customer
    }
    public enum PersonTitle
    {

    }
    /// <summary>
    /// Holds Person's information including their roles and types
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Identity
        /// </summary>
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string PersonTitle { get; set; }
        public int? TitleId { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        /// <summary>
        /// Complete name. <see cref="FirstName"/> and <see cref="LastName"/>
        /// </summary>
        public string FullName {
            get {
                return  PersonTitle+" "+ FirstName +" " + MiddleName +" " + LastName;
            }
        }
        /// <summary>
        /// This Name property is different from other Name properties. It is used when added New Person
        /// from within Complainant/Perpetrator tab as Non Tenant
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Person role. <see cref="PerosnRole"/>
        /// </summary>
        public PerosnRole Role { get; set; }
        /// <summary>
        /// Person type. <see cref="PersonType"/>
        /// </summary>
        public PersonType Type { get; set; }

        public string TenancyRef { get; set; }

        public string Organization { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address { get { return Address1 + " " + Address2; } }

        public string City { get; set; }
        public string PostCode { get; set; }

        public string TelephoneHome { get; set; }
        public string TelephoneWork { get; set; }

        public string Mobile1 { get; set; }

        public string Mobile2 { get; set; }
        public string Email { get; set; }

        public string AlternateContact { get; set; }

        public string TelephoneContact { get; set; }

        public int PersonType { get; set; }

        public IEnumerable<PersonTypes> Types { get; set; }

        public IEnumerable<PersonTypes> Titles { get; set; }
        /// <summary>
        /// Converts response to <see cref="Person"/> object
        /// </summary>
        /// <param name="jsonResponse"></param>
        /// <returns>Object of <see cref="Person"/></returns>
        public static Person ToObject(string jsonResponse)
        {
            Person obj = (Person)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(Person));
            return obj;
        }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}