﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Previous case object to show on Dialog
    /// </summary>
    public class PreviousCase
    {
        /// <summary>
        /// Case id or Reference number
        /// </summary>
        public int Ref { get; set; }
        public string PersonRole { get; set; }
        public string PersonType { get; set; }
        public string IncidentDate { get; set; }
        public string Category { get; set; }
        public string IncidentType { get; set; }
        public string Status { get; set; }
    }
}