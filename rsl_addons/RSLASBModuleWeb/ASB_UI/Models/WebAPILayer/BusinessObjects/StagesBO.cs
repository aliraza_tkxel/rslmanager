﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    public class StagesBO
    {
        public int SelectedStageId { get; set; }
        /// <summary>
        /// List of all <see cref="Stage"/>s
        /// </summary>
        [Required(ErrorMessage = "Please select Stage")]
        public IEnumerable<Stage> _Stages { get; set; }
        /// <summary>
        /// Selected SubCategory id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedSubCategoryId { get; set; }
        /// <summary>
        /// List of all <see cref="SubCategory"/>s
        /// </summary>
        public IEnumerable<SubCategory> _SubCategories { get; set; }
        /// <summary>
        /// Selected Typer id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedTypeId { get; set; }
        /// <summary>
        /// List of all <see cref="Types"/>s
        /// </summary>
        public IEnumerable<Types> _Types { get; set; }
        /// <summary>
        /// Selected SubType id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedSubTypeId { get; set; }
        /// <summary>
        /// List of all <see cref="SubType"/>s
        /// </summary>
        public IEnumerable<SubType> _SubTypes { get; set; }
    }
}