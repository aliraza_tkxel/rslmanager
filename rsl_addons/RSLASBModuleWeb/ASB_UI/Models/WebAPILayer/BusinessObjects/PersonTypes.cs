﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Person types. When added New person as Tenant
    /// </summary>
    public class PersonTypes
    {
        /// <summary>
        /// Type Id
        /// </summary>
        public int key { get; set; }
        /// <summary>
        /// Type value
        /// </summary>
        public string value { get; set; }
    }
}