﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    public class ReportAlert
    {
        /// <summary>
        /// 
        /// </summary>
        public int openCaseCount { get; set; }
        public int highRiskCasesCount { get; set; }
        public int overdueCasesCount { get; set; }
        public bool IsOverdue { get; set; }
    }
}