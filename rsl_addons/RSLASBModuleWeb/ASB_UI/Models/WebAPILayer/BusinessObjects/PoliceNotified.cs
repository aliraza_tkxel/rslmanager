﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Whether police notified
    /// </summary>
    public class PoliceNotified
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}