﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    public class Case
    {
        public int caseId { get; set; }
        public string address { get; set; }
        public string postCode { get; set; }
        public string asbCategory { get; set; }
        public string asbIncidentType { get; set; }
        public string asbRiskLevel { get; set; }
        public string asbCaseOfficer { get; set; }
        public string caseStatus { get; set; }
        public string followupDate { get; set; }
        public int asbCategoryId { get; set; }
        public int? asbRiskLevelId { get; set; }
        public int asbCaseOfficerId { get; set; }
        public DateTime? closedDate { get; set; }
        public int? StageId { get; set; }
        public string Stage { get; set; }
        public DateTime DateLogged { get; set; }

    }
}