﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Models.WebAPILayer.BusinessObjects
{
    /// <summary>
    /// Case Document
    /// </summary>
    public class Document
    {
        public int DocumentId { get; set; }
        public DateTime DateUploaded { get; set; }
        public string FileName { get; set; }
        public Person UploadedBy { get; set; }
        public string FilePath { get; set; }
    }
}