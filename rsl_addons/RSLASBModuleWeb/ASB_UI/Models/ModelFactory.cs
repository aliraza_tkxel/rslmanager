﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;

namespace ASB_UI.Models
{
    /// <summary>
    /// A factory that generates object stubs.
    /// </summary>
    public static class ModelFactory
    {
        
        /// <summary>
        /// Helper method. Converts <see cref="PersonRole"/> enum to string
        /// </summary>
        /// <param name="role"><see cref="PersonRole"/></param>
        /// <returns><see cref="PerosnRole.Complainant"/> as string or <see cref="PerosnRole.Perpatrator"/> as string</returns>
        public static string PerosnRoleToString(PerosnRole role)
        {
            return role == PerosnRole.Complainant ? "Complainant" : "Perpatrator";
        }
        /// <summary>
        /// Helper method. Converts <see cref="PersonType"/> enum to string
        /// </summary>
        /// <param name="type"><see cref="PersonType"/></param>
        /// <returns><see cref="PersonType.Customer"/> as string or <see cref="PersonType.Customer"/> as string</returns>
        public static string PersonTypeToString(PersonType type)
        {
            return type == PersonType.Employee ? "Employee" : "Customer";
        }
        /// <summary>
        /// Creates new <see cref="CaseOfficer"/> object
        /// </summary>
        /// <param name="id">Case officer Id</param>
        /// <param name="name">Case officer name</param>
        /// <returns><see cref="CaseOfficer"/></returns>
        public static CaseOfficer NewCaseOfficer(int id, string name)
        {
            CaseOfficer co = new CaseOfficer();
            co.key = id;
            co.value = name;
            return co;
        }
        /// <summary>
        /// Creates sample list of <see cref="CaseOfficer"/>s
        /// </summary>
        /// <returns><see cref="List{CaseOfficer}"/></returns>
        public static IList<CaseOfficer> GetSampleCaseOfficers()
        {
            IList<CaseOfficer> officers = new List<CaseOfficer>();
            officers.Add(NewCaseOfficer(1, "ABC"));
            officers.Add(NewCaseOfficer(2, "DEF"));
            officers.Add(NewCaseOfficer(3, "GHI"));
            officers.Add(NewCaseOfficer(4, "JKL"));
            officers.Add(NewCaseOfficer(5, "MNO"));
            officers.Add(NewCaseOfficer(6, "PQR"));
            officers.Add(NewCaseOfficer(7, "STU"));
            officers.Add(NewCaseOfficer(8, "VWX"));
            officers.Add(NewCaseOfficer(9, "YZ1"));
            officers.Add(NewCaseOfficer(10, "AB2"));

            return officers;
        }
        /// <summary>
        /// List of all Category. <see cref="Category"/>. ReadOnly property
        /// </summary>
        public static IEnumerable<Category> CaseCategories
        {
            get
            {
                string[] _categories = new string[] { "Environmental", "Nuisance", "Personal" };
                IList<Category> c = new List<Category>();
                for (int i = 0; i < _categories.Length; i++)
                {
                    c.Add(new Category() { key = i + 1, value = _categories[i] });
                }
                return c;
            }
        }

        /// <summary>
        /// List of all <see cref="RiskLevel"/>s. ReadOnly property
        /// </summary>
        public static IEnumerable<RiskLevel> Risks
        {
            get
            {
                string[] _levels = new string[] { "Low", "Medium", "High" };
                IList<RiskLevel> rl = new List<RiskLevel>();
                for (int i = 0; i < _levels.Length; i++)
                {
                    rl.Add(new RiskLevel() { key = i + 1, value = _levels[i] });
                }
                return rl;
            }
        }
        /// <summary>
        /// List of all <see cref="IncidentType"/>s. ReadOnly property
        /// </summary>
        public static IEnumerable<IncidentType> Types
        {

            get
            {
                string[] _types = new string[] {"Alcohol", "Communal areas/loitering",
                    "Domestic voilance", "Drugs", "Garder", "Harassment/Threats", "Hate related", "Noise", "Other criminal behaviour",
                    "Other voilance", "Pets/animals", "Prostitution/Sex", "Rubbish", "Vandalism","Vehicles"};
                IList<IncidentType> it = new List<IncidentType>();
                for (int i = 0; i < _types.Length; i++)
                {
                    it.Add(new IncidentType() { key = i + 1, value = _types[i] });
                }
                return it;
            }
        }
        /// <summary>
        /// Police notified or not
        /// </summary>
        public static IEnumerable<PoliceNotified> PoliceNotifiedList
        {
            get
            {
                IList<PoliceNotified> status = new List<PoliceNotified>();
                status.Add(new PoliceNotified() { key = "1", value = "Yes" });
                status.Add(new PoliceNotified() { key = "0", value = "No" });
                return status;
            }
        }
    }
}