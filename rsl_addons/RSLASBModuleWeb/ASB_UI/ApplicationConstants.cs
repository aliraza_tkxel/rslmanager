﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI
{
    public class ApplicationConstants
    {
        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string UsersAccountDeactivated = "Your access to ASB Module has been de-activiated. Please contact administrator.";
        public const string AsbMenu = "ASB Management";
        /// <summary>
        /// DateFormat key. Saved in Web.config
        /// </summary>
        public const string KeyDateFormat = "DateFormat";
        /// <summary>
        /// DateFormat key for Model. Saved in Web.config
        /// </summary>
        public const string KeyDateFormatModel = "DateFormatForModel";
        /// <summary>
        /// Virtual Directory Key. This key is used to get virtual directory name from Web.config 
        /// </summary>
        public const string VirtualDirectory = "VirtualDirectory";
        public const int MaxPageSizeLimit = 500;

    }
}