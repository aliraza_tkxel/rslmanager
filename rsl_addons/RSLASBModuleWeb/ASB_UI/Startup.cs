﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASB_UI.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace ASB_UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
