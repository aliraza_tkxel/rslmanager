﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.CreateNewCase.SubViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class PersonViewModel
    {
        public string PersonType { get; set; }
        public string PersonRole { get; set; }
        public int PersonId { get; set; }
        public int CaseId { get; set; }
    }
}