﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.CreateNewCase.SubViewModels
{
    /// <summary>
    /// View Model for Previous/Associated cases dialog of Complainant or Perpatrator
    /// </summary>
    public class PreviousCasesViewModel
    {
        public IEnumerable<PreviousCase> PreviousCases { get; set; }
        public Pagination Paging { get; set; }
    }
}