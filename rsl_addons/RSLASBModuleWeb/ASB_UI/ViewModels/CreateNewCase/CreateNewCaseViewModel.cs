﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.ViewModels.CreateNewCase.SubViewModels;

namespace ASB_UI.ViewModels.CreateNewCase
{
    public class CreateNewCaseViewModel
    {
        public NewCaseForm NewCase { get; set; }
        public PreviousCasesViewModel PrevCasesVM { get; set; }
        public IEnumerable<string> Persons { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string fileNotes { get; set; }
    }
}