﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.Reports.SubViewModels
{
    public class ReportCase
    {

        public Pagination objPaginationBO { get; set; }
        public IEnumerable<RepCase> objCaseBO { get; set; }
    }
}