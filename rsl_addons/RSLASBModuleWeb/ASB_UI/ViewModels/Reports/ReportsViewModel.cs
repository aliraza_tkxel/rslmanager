﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.ViewModels.Reports.SubViewModels;
namespace ASB_UI.ViewModels.Reports
{
    public class ReportsViewModel
    {
        /// <summary>
        /// Category ID. 
        /// </summary>
        public int CategoryId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Category> Categories { get; set; }

        /// <summary>
        /// RiskLevel ID. 
        /// </summary>
        public int RiskLevelId { get; set; }
        /// <summary>
        /// ReadOnly property. 
        /// </summary>
            
        public IEnumerable<RiskLevel> RiskLevels { get; set; }

        /// <summary>
        /// CaseOfficer ID. 
        /// </summary>
        public int CaseOfficerId { get; set; }
        /// <summary>
        /// ReadOnly property. 
        /// </summary>

        public IEnumerable<CaseOfficer> CaseOfficers { get; set; }
        /// <summary>
        /// Case Status. 
        /// </summary>
        public int CaseStatus { get; set; }
        /// <summary>
        /// Report alerts
        /// </summary>
        public ReportAlert Alerts { get; set; }
        /// <summary>
        /// Dashboard Open Cases with filters applied. 
        /// </summary>
        //public IEnumerable<Case> Cases { get; set; }
        public ReportCase ReportOpenCases { get; set; }
    }
}
