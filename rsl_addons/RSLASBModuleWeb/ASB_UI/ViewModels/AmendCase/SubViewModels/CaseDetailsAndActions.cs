﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    public class CaseDetailsAndActions
    {
        public CaseDetail CaseDetails {get; set;}
        public CaseActionsViewModel CaseActions { get; set;}
    }
}