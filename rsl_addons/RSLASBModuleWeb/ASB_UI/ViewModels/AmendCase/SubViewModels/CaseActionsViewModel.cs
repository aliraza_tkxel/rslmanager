﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Case Actions/Journals ViewModel
    /// </summary>
    public class CaseActionsViewModel
    {
        /// <summary>
        /// List of Actions/Journal
        /// </summary>
        public IList<CaseAction> ActionsList { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination Paging { get; set; }
    }
}