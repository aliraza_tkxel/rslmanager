﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class CaseAction
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Date when action is performed
        /// </summary>
        public string RecordedDate { get; set; }
        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// Action Notes
        /// </summary>
        public string ActionNotes { get; set; }
        /// <summary>
        /// Employee/User performing action
        /// </summary>
        public string RecordedBy { get; set; }
        /// <summary>
        /// If Action is Photograph/Deocument 
        /// </summary>
        public bool HasAttachment { get; set; }
        /// <summary>
        /// File Name if <see cref="HasAttachment"/> is "true"
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// File Path is <see cref="HasAttachment"/> is "true"
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// Stage if action is Stage
        /// </summary>
        public string stage { get; set; }
        /// <summary>
        /// Stage if action is Stage
        /// </summary>
        public int? StageId { get; set; }
        /// <summary>
        /// Subcategory if action is Stage
        /// </summary>
        public string SubCategory { get; set; }
        /// <summary>
        /// Subcategory if action is Stage
        /// </summary>
        public int? SubCategoryId { get; set; }
        /// <summary>
        /// Review Date for the action
        /// </summary>
        public string ReviewDate { get; set; }
    }
}