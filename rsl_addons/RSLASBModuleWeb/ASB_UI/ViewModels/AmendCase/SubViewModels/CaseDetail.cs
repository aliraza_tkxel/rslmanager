﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    public class CaseDetail
    {
        /// <summary>
        /// New Case ID. Auto Generated
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Date when case is recorded on the system. Auto Generated current date.
        /// </summary>
        public string DateRecorded { get; set; }
        /// <summary>
        /// Date when case is reported. It is different from <see cref="DateRecorded"/>
        /// </summary>
        public string DateReported { get; set; }
        /// <summary>
        /// Date when incidence(case) actually happend
        /// </summary>
        public string DateOfIncident { get; set; }
        /// <summary>
        /// Time when incident happened
        /// </summary>
        public string TimeOfIncident { get; set; }
        /// <summary>
        /// Case Officer name
        /// </summary>
        public string CaseOfficer { get; set; }
        /// <summary>
        /// Case Officer Two name
        /// </summary>
        public string CaseOfficerTwo { get; set; }
        /// <summary>
        /// Category name
        /// </summary>
        public string Category { get; set; }
        ///<summary>
        /// Tyoe of Incident
        /// </summary>
        public string TypeOfIncident { get; set; }
        /// <summary>
        /// Risk Level
        /// </summary>
        public string LevelOfRisk { get; set; }
        /// <summary>
        /// Incident Description
        /// </summary>
        public string IncidentDescription { get; set; }
        /// <summary>
        /// Is police Notified about Incident
        /// </summary>
        public string  IsPoliceNotified{ get; set; }
        /// <summary>
        /// Crime case number
        /// </summary>
        public int CrimeCaseNo { get; set; }
        /// <summary>
        /// Next follow up date
        /// </summary>
        public string NextFollowUp { get; set; }
        /// <summary>
        /// Notes for next follow up
        /// </summary>
        public string NextFollowUpNote { get; set; }
        /// <summary>
        /// Case closing date
        /// </summary>
        public string CaseClosed { get; set; }
        /// <summary>
        /// Case closing notes
        /// </summary>
        public string ResolutionNotes { get; set; }
        /// <summary>
        /// Case status, open or closed
        /// </summary>
        public string CaseStatus { get; set; }
        public string ReviewDate { get; set; }
        public string Stage { get; set; }
        public int? StageId { get; set; }
    }
}