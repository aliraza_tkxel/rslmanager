﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Parameters to download file
    /// </summary>
    public class FileDownloadParams
    {
        /// <summary>
        /// File name
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// File path
        /// </summary>
        public string FilePath { get; set; }
    }
}