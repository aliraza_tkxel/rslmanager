﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class AddActionDialogModel
    {
        /// <summary>
        /// ID of Selected ActionType
        /// </summary>
        public int SelectedActionId { get; set; }
        /// <summary>
        /// List of Actions
        /// </summary>
        public IEnumerable<ActionType> ActionTypes { get; set; }
        /// <summary>
        /// Action Notes. Pre populated
        /// </summary>
        public string ActionNotes { get; set; }
        public StagesBO stagesobject { get; set; }
        public int selectedClosingType { get; set; }
        public IEnumerable<CloseType> ClosingTypes { get; set; }

    }
}