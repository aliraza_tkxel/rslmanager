﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Model for Document and Photos
    /// </summary>
    public class FileCaseViewModel
    {
        public Pagination Paging { get; set; }
        public IList<FileParameters> Params { get; set; }
    }
    
}