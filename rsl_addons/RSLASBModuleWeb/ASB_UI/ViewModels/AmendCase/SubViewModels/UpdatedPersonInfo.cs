﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// View model when Person is updated
    /// </summary>
    public class UpdatedPersonInfo
    {
        /// <summary>
        /// Person ID
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Type: "Customer" or "Employee"
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// Telephone Home
        /// </summary>
        public string TelephoneH { get; set; }
        /// <summary>
        /// Telephone Work
        /// </summary>
        public string TelephoneW { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Alternate Contact
        /// </summary>
        public string AlternateContact { get; set; }
    }
}