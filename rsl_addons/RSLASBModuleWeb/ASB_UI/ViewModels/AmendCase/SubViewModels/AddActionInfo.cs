﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Info object when Action is added in Journal
    /// </summary>
    public class AddActionInfo
    {
        /// <summary>
        /// Date when Action is recorded
        /// </summary>
        public DateTime RecordedDate { get; set; }
        /// <summary>
        /// Action Id
        /// </summary>
        public int ActionId { get; set; }
        /// <summary>
        /// Action Name
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// Action Notes or Comments
        /// </summary>
        public string ActionNotes { get; set; }
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Followup date if Action is 
        /// </summary>
        public string FollowUpDate { get; set; }
        /// <summary>
        /// Case close date if action is Case Close
        /// </summary>
        public string ClosedDate { get; set; }
        /// <summary>
        /// Case Review date 
        /// </summary>
        public string ReviewDate { get; set; }
        /// <summary>
        /// Case Stage if action is Stages
        /// </summary>
        public int? Stage { get; set; }
        /// <summary>
        /// Case SubCategory if action is Stages
        /// </summary>
        public int? SubCategory { get; set; }
        /// <summary>
        /// Case Types if action is Stages
        /// </summary>
        public int? Types { get; set; }
        /// <summary>
        /// Case SubType if action is Stages
        /// </summary>
        public int? SubType { get; set; }
        public int? selectedClosingType { get; set; }

    }
}