﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Person tab model
    /// </summary>
    public class PersonsViewModel
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// User/Employee ID managing persons
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// List of persons
        /// </summary>
        public IList<CasePerson> Persons { get; set; }
    }
}