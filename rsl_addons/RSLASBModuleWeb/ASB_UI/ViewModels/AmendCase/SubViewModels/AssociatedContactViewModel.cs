﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// ViewModel for Associated Contacts. 
    /// </summary>
    public class AssociatedContactViewModel
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// ID of employee who recorded/added this contact
        /// </summary>
        public int RecordedById { get; set; }
        /// <summary>
        /// Name of employee who recorded/added this contact
        /// </summary>
        public string RecordedBy { get; set; }
        /// <summary>
        /// List of Associated Contacts
        /// </summary>
        public IList<AssociatedContact> Contacts { get; set; }
        /// <summary>
        /// <see cref="Pagination"/>
        /// </summary>
        public Pagination Paging { get; set; }
    }
}