﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    public class NewFileParams
    {
        /// <summary>
        /// 
        /// </summary>  
        public string DateUploaded { get; set; }
        /// <summary>
        /// File Name
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// Name of employee who uploaded files
        /// </summary>
        public string UploadedBy { get; set; }
        /// <summary>
        /// Id of employee who uploaded file
        /// </summary>
        public int UploadedById { get; set; }
        /// <summary>
        /// Associated Case Id
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Is file available or removed from case. False if removed from case
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// File type. <see cref="FileType"/>
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// File Notes. <see cref="FileNotes"/>
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageNumber { get; set; }

    }
}