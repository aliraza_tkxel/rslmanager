﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Class to hold information of file to be deleted
    /// </summary>
    public class RemoveFileParams
    {
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// File id to be deleted
        /// </summary>
        public int FileId { get; set; }
        /// <summary>
        /// File name
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// "Document" or "Photograph"
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// ID of employee. TODO - Get it from Session
        /// </summary>
        public int RemovedBy { get; set; }
        /// <summary>
        /// Page Number
        /// </summary>
        public int PageNumber { get; set; }
        /// <summary>
        /// Page Size. Fixed 5
        /// </summary>
        public int PageSize { get; set; }
    }
}