﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models;
using System.Web.Mvc;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Holds Case information
    /// </summary>
    public class AmendCaseForm
    {
        /// <summary>
        /// New Case ID. Auto Generated
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Date when case is recorded on the system. Auto Generated current date.
        /// </summary>
        public string DateRecorded { get; set; }
        /// <summary>
        /// Date when case is reported. It is different from <see cref="DateRecorded"/>
        /// </summary>
        [Required(ErrorMessage = "Date Reported is required")]
        public string DateReported { get; set; }
        /// <summary>
        /// Date when incidence(case) actually happend
        /// </summary>
        [Required(ErrorMessage = "Date of Incident is required")]
        public string DateOfIncident { get; set; }
        /// <summary>
        /// Time when incident happened
        /// </summary>
        [Required(ErrorMessage = "Time of Incident is required")]
        public string TimeOfIncident { get; set; }
        /// <summary>
        /// Selected case officer id. It will be used when Case information is sent in Request
        /// </summary>
        public int SelectedCaseOfficerId { get; set; }
        /// <summary>
        /// List of all <see cref="CaseOfficer"/>s
        /// </summary>
        [Required(ErrorMessage = "Please select Case Officer")]
        public IEnumerable<CaseOfficer> _CaseOfficers { get; set; }
        /// <summary>
        /// Selected case officer 2 id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedCaseOfficerTwoId { get; set; }
        /// <summary>
        /// List of all <see cref="CaseOfficer"/>s
        /// </summary>
        public IEnumerable<CaseOfficer> _CaseOfficersTwo { get; set; }
        /// <summary>
        /// Selected Stage id. It will be used when Case information is sent in Request
        /// </summary>
        public int SelectedStageId { get; set; }
        /// <summary>
        /// List of all <see cref="Stage"/>s
        /// </summary>
        [Required(ErrorMessage = "Please select Stage")]
        public IEnumerable<Stage> _Stages { get; set; }
        /// <summary>
        /// Selected SubCategory id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedSubCategoryId { get; set; }
        /// <summary>
        /// List of all <see cref="SubCategory"/>s
        /// </summary>
        public IEnumerable<SubCategory> _SubCategories { get; set; }
        /// <summary>
        /// Selected Typer id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedTypeId { get; set; }
        /// <summary>
        /// List of all <see cref="Types"/>s
        /// </summary>
        public IEnumerable<Types> _Types { get; set; }
        /// <summary>
        /// Selected SubType id. It will be used when Case information is sent in Request
        /// </summary>
        public int? SelectedSubTypeId { get; set; }
        /// <summary>
        /// List of all <see cref="SubType"/>s
        /// </summary>
        public IEnumerable<SubType> _SubTypes { get; set; }
        /// <summary>
        /// Selected Category id. It will be used when Case information is sent in Request
        /// </summary>
        public int SelectedCategoryId { get; set; }
        /// <summary>
        /// List of all case Categories. See <see cref="Category"/>
        /// </summary>
        [Required(ErrorMessage = "Please select Vulnerability")]
        public IEnumerable<Category> _Categories { get; set; }
        /// <summary>
        /// Selected incident type id. It will be used when Case information is sent in Request
        /// </summary>
        public int SelectedIncidentTypeId { get; set; }
        /// <summary>
        /// List of all <see cref="IncidentType"/>s
        /// </summary>
        [Required(ErrorMessage = "Please select Incident")]
        public IEnumerable<IncidentType> _TypeOfIncident { get; set; }
        /// <summary>
        /// Selected risk level id. It will be used when Case information is sent in Request
        /// </summary>
        public int SelectedRiskLevelId { get; set; }
        /// <summary>
        /// List of all <see cref="RiskLevel"/>s
        /// </summary>
        //[Required(ErrorMessage = "Please select Risk Level")]
        public IEnumerable<RiskLevel> _LevelOfRisks { get; set; }
        /// <summary>
        /// Incident Description
        /// </summary>
        [Required(ErrorMessage = "Incident Description is required")]
        [AllowHtml]
        public string IncidentDescription { get; set; }
        /// <summary>
        /// Selected value of Police notified ot not
        /// </summary>
        public int SelectedPoliceNotifyId { get; set; }
        /// <summary>
        /// Is police Notified about Incident
        /// </summary>
        [Required(ErrorMessage = "Please select Police Notified")]
        public IEnumerable<PoliceNotified> IsPoliceNotified
        {
            get
            {
                return ModelFactory.PoliceNotifiedList;
            }
        }
        /// <summary>
        /// Crime case number
        /// </summary>
        public int CrimeCaseNo { get; set; }
        /// <summary>
        /// Next follow up date
        /// </summary>
        /// <summary>
        /// Notes for next follow up
        /// </summary>
        //[Required(ErrorMessage = "Notes for Follow Up is required")]
        [AllowHtml]
        public string NextFollowUpNote { get; set; }
        public string NextFollowUpDate { get; set; }
        /// <summary>
        /// Case closing date
        /// </summary>
        public string CaseClosed { get; set; }
        /// <summary>
        /// Either case is Open or closed
        /// </summary>
        public bool IsCaseOpen { get; set; }
        /// <summary>
        /// Case closing notes
        /// </summary>
        public string ResolutionNotes { get; set; }
        public string CaseStatus { get; set; }
        public string ReviewDate { get; set; }
    }
}