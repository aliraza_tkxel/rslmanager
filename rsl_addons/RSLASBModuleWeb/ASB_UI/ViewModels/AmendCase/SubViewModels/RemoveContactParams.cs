﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Remove contacts parameters
    /// </summary>
    public class RemoveContactParams
    {
        /// <summary>
        /// CaseID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// ID who removed contact
        /// </summary>
        public int RemovedBy { get; set; }
        /// <summary>
        /// Contact ID
        /// </summary>
        public int ContactId { get; set; }
        /// <summary>
        /// Page number
        /// </summary>
        public int  PageNumber { get; set; }
        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize { get; set; }
    }
}