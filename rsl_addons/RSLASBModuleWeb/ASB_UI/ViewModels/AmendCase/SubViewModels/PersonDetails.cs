﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Person Detail when adding/removing from case
    /// </summary>
    public class PersonDetails
    {
        /// <summary>
        /// Person ID
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Type. Customer or Employee
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// Complainant or Perpetrator
        /// </summary>
        public string PersonRole { get; set; }
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Employee performing Add/Update/Operations
        /// </summary>
        public int RecordedBy { get; set; }
    }
}