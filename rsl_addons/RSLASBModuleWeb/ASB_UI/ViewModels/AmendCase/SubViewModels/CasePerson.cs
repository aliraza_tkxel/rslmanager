﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Single person details
    /// </summary>
    public class CasePerson
    {
        /// <summary>
        /// PersonType. "Customer" or "Employee"
        /// </summary>
        public string PersonType { get; set; }
        /// <summary>
        /// PersonRole. "Complainant" or "Perpetrator"
        /// </summary>
        public string PersonRole { get; set; }
        /// <summary>
        /// PersonID
        /// </summary>
        public int PersonId { get; set; }
        /// <summary>
        /// Associated Case ID
        /// </summary>
        public int CaseId { get; set; }
    }
}