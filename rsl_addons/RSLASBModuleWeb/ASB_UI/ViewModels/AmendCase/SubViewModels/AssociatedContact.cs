﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase.SubViewModels
{
    /// <summary>
    /// Single Contact information
    /// </summary>
    public class AssociatedContact
    {
        /// <summary>
        /// Contact ID
        /// </summary>
        public int ContactId { get; set; }
        /// <summary>
        /// Contact name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Organization
        /// </summary>
        public string Organization { get; set; }
        /// <summary>
        /// Telephone number
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// Is associated with case or not. True if associated
        /// </summary>
        public bool IsActive { get; set; }
    }
}