﻿using ASB_UI.ViewModels.AmendCase.SubViewModels;
using ASB_UI.ViewModels.CreateNewCase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.AmendCase
{
    /// <summary>
    /// 
    /// </summary>
    public class AmendCaseViewModel
    {
        /// <summary>
        /// Case ID
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CaseDetailsAndActions DetailsAndActions { get; set; }
        /// <summary>
        /// ViewModel for Person section
        /// </summary>
        public PersonsViewModel PersonModel { get; set; }
        /// <summary>
        /// Model for Document section
        /// </summary>
        public FileCaseViewModel FileViewModel { get; set; }
        /// <summary>
        /// Model for Photograph section
        /// </summary>
        public FileCaseViewModel PhotoViewModel { get; set; }
        /// <summary>
        /// Model for Assoicated Contacts
        /// </summary>
        public AssociatedContactViewModel ContactsModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool PreviousWindow { get; set; }
    }
}