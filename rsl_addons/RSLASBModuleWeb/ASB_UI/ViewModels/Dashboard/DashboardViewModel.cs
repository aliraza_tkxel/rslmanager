﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using ASB_UI.ViewModels.Dashboard.SubViewModels;

namespace ASB_UI.ViewModels.Dashboard
{
    public class DashboardViewModel
    {
        public int CategoryId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Category> Categories { get; set; }

        public int RiskLevelId { get; set; }
        /// <summary>
        /// ReadOnly property. 
        /// </summary>
        public IEnumerable<RiskLevel> RiskLevels { get; set; }

        public int CaseOfficerId { get; set; }
        /// <summary>
        /// ReadOnly property
        /// </summary>
        public IEnumerable<CaseOfficer> CaseOfficers { get; set; }
        /// <summary>
        /// Dashboard alerts
        /// </summary>
        public DashboardAlert Alerts { get; set; }
        /// <summary>
        /// Dashboard Open Cases with filters applied. 
        /// </summary>
        //public IEnumerable<Case> Cases { get; set; }
        public DasboardCase DashboardOpenCases { get; set; }
    }
}