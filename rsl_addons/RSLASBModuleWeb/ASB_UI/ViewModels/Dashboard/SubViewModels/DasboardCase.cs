﻿using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.ViewModels.Dashboard.SubViewModels
{
    public class DasboardCase
    {
        public Pagination Paging { get; set; }
        public IEnumerable<Case> Cases { get; set; }
    }
}