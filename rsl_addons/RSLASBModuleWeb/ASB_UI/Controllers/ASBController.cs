﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASB_UI.Controllers
{
    public class ASBController : BaseController
    {
        // GET: ASB
        public ActionResult Index(bool reports=false)
        {
            var userid = 0;
            if (!(Request.QueryString["User"]==null))
            {
                userid = int.Parse(Request.QueryString["User"]);
            }
            if(reports && userid!=0)
            {
                int userId = userid;
                Session["UserId"] = userId;
                ViewBag.UserId = GetUserIdFromSession();
            }
            else
            {
                ViewBag.UserId = userid;
            }
            //int userId = int.Parse(Request.QueryString["UserId"]);
            //Session["UserId"] = userId;
            ViewBag.reports = reports;
            //ViewBag.UserId = GetUserIdFromSession() ;
            return View("ASBHome");
        }
    }
}