﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASB_UI.ViewModels.Reports;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using Newtonsoft.Json;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using ASB_UI.ViewModels.Reports.SubViewModels;
using System.IO;

namespace ASB_UI.Controllers
{
    public class ReportController : BaseController
    {
        // GET: Reports
        public ActionResult Report(string ReportedFrom, string ReportedTo, int catId = 0, int riskId = 0,
                            int pageSize = 25, string searchVal = "", int pageNumber = 1,int caseStatus=0, 
                            bool isOverdue = false, int caseOfficer = 0, string sortReport= "Reported", string sOrder="DESC")
        {
            ViewBag.CategoryId = catId;
            ViewBag.RiskLevelId = riskId;
            ViewBag.CaseOfficerId = caseOfficer;
            CommonServices com = new CommonServices();
            ReportsViewModel ViewModel = new ReportsViewModel();
            CommonServices dropDownsService = new CommonServices();
            ViewModel.Categories = dropDownsService.GetCategories();
            ViewModel.RiskLevels = dropDownsService.GetRiskLevels();
            ViewModel.CaseOfficers = com.GetCaseOfficers();
            DateTime? reportF = null;
            DateTime? reportT = null;
            if(ReportedFrom!= null && ReportedFrom !="")
            {
                if(ReportedFrom[1]=='/' && ReportedFrom[3]=='/')
                {
                    ReportedFrom = "0" + ReportedFrom.Substring(0,2) + "0" + ReportedFrom.Substring(2);
                }
                else if(ReportedFrom[1]=='/')
                {
                    ReportedFrom = "0" + ReportedFrom;
                }
                else if (ReportedFrom[4] == '/')
                {
                    ReportedFrom = ReportedFrom.Substring(0, 3) + "0" + ReportedFrom.Substring(3);
                }
                reportF = DateTime.ParseExact(ReportedFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            if (ReportedTo != null && ReportedTo != "")
            {
                if (ReportedTo[1] == '/' && ReportedTo[3] == '/')
                {
                    ReportedTo = "0" + ReportedTo.Substring(0, 2) + "0" + ReportedTo.Substring(2);
                }
                else if (ReportedTo[1] == '/')
                {
                    ReportedTo = "0" + ReportedTo;
                }
                else if (ReportedTo[4] == '/')
                {
                    ReportedTo = ReportedTo.Substring(0, 3) + "0" + ReportedTo.Substring(3);
                }
                reportT = DateTime.ParseExact(ReportedTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            if(reportF!=null && reportT!=null)
            {
                if(reportF>reportT)
                {
                    reportF = reportT = null;
                }
            }
            ReportRequest req = new ReportRequest()
            {
                objPaginationBO = new Pagination() { PageSize = pageSize, PageNumber = pageNumber },
                categoryId = catId,
                RiskLevelId = riskId,
                Search = searchVal,
                CaseStatus = caseStatus,
                IsOverdue = isOverdue,
                CaseOfficerId = caseOfficer,
                ReportedFrom= reportF,
                ReportedTo= reportT,
                sortOrder=sortReport,
                sortBy = sOrder
            };

            ReportResponse response = new ReportService().GetOpenCases(req);

            ViewModel.ReportOpenCases = new ReportCase()
            {
                objPaginationBO = new Pagination()
                {
                    PageNumber = response.objPaginationBO.PageNumber,
                    PageSize = response.objPaginationBO.PageSize,
                    TotalPages = response.objPaginationBO.TotalPages,
                    TotalRows = response.objPaginationBO.TotalRows
                },
                objCaseBO = response.objCaseBO
            };
            //ViewModel.ReportOpenCases.objCaseBO.order
            ViewModel.CategoryId = catId;
            ViewModel.RiskLevelId = riskId;
            ViewModel.CaseStatus = caseStatus;
            ViewBag.DefDateFormat = DEFAULT_DATE_FORMAT;

            ViewBag.DateFormatModel = DATE_FORMAT_FOR_MODEL;
                return View(ViewModel);
           
        }
        public FileResult ExportClientsListToCSV(string sortReport, DateTime? ReportedFrom, DateTime? ReportedTo, int catId = 0, int riskId = 0,
                            int pageSize = 25, string searchVal = "", int pageNumber = 1, int caseStatus = 0, int caseOfficer = 0)
        {
            ReportsViewModel ViewModel = CreateReportsViewModel(ReportedFrom, ReportedTo, catId, riskId, pageSize, searchVal, pageNumber, caseStatus, sortReport);
            StringWriter sw = new StringWriter();
            sw.WriteLine("\"Ref Num\",\"Date\",\"Complainant(s)\",\"Address\",\"Postcode\",\"Local Authority\",\"Perpetrator(s)\",\"Current Stage\",\"Case Officer\",\"Type\",\"Risk\",\"Status\",\"Closed\"");
            string delimiter = ",";
            foreach (var openCase in ViewModel.ReportOpenCases.objCaseBO)
            {
                string ComplainantNameStr = "";
                string addressStr = "";
                string postcodeStr = "";
                string PerpetratorNameStr = "";
                string localAuthorityStr = "";
                if (openCase.objComplainantList != null && openCase.objComplainantList.Count!=0)
                {
                    var ComplainantNameList = openCase.objComplainantList.Select(g => new { g.FirstName, g.LastName }).ToList();
                    var ComplainantName = ComplainantNameList.Select(i => i.FirstName + " " + i.LastName).Aggregate((i, j) => i + delimiter + j);
                    ComplainantNameStr = ComplainantName;

                    var addressList = openCase.objComplainantList.Select(g => new { g.Address1, g.Address2 }).ToList();
                    var address = addressList.Select(i => i.Address1 + " " + i.Address2).Aggregate((i, j) => i + delimiter + j);
                    addressStr = address;

                    var postcodeList = openCase.objComplainantList.Select(g => new { g.PostCode }).ToList();
                    var postcode = postcodeList.Select(i => i.PostCode).Aggregate((i, j) => i + delimiter + j);
                    postcodeStr = postcode;

                    var laList = openCase.objComplainantList.Select(g => new { g.LocalAuthority }).ToList();
                    var localauthority = laList.Select(i => i.LocalAuthority).Aggregate((i, j) => i + delimiter + j);
                    localAuthorityStr = localauthority;
                }
                if (openCase.objPerpetratorList != null && openCase.objPerpetratorList.Count != 0)
                {
                    var PerpetratorNameList = openCase.objPerpetratorList.Select(g => new { g.FirstName, g.LastName }).ToList();
                    var PerpetratorName = PerpetratorNameList.Select(i => i.FirstName + " " + i.LastName).Aggregate((i, j) => i + delimiter + j);
                    PerpetratorNameStr = PerpetratorName;
                }
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\"",
                                           openCase.CaseId,
                                           openCase.ReportedDate.ToString(DATE_FORMAT_FOR_MODEL),
                                           ComplainantNameStr,
                                           addressStr,
                                           postcodeStr,
                                           localAuthorityStr,
                                           PerpetratorNameStr,
                                           openCase.Stage,
                                           openCase.CaseOfficer,
                                           openCase.IncidentType,
                                           openCase.Risk,
                                           openCase.CaseStatus,
                                           openCase.ClosedDate==null?"":string.Format(DATE_FORMAT_FOR_MODEL,openCase.ClosedDate)
                                           ));

            }

            return File(new System.Text.UTF8Encoding().GetBytes(sw.ToString()), "application/octet-stream", "CSVReport.csv");
        }
        public ReportsViewModel CreateReportsViewModel(DateTime? ReportedFrom, DateTime? ReportedTo, int catId = 0, int riskId = 0,
                            int pageSize = 25, string searchVal = "", int pageNumber = 1, int caseStatus = 0, string reportSort= "Reported", int caseOfficer = 0)
        {
            ReportsViewModel ViewModel = new ReportsViewModel();
            CommonServices dropDownsService = new CommonServices();
            ViewModel.Categories = dropDownsService.GetCategories();
            ViewModel.RiskLevels = dropDownsService.GetRiskLevels();

            ReportRequest req = new ReportRequest()
            {
                objPaginationBO = new Pagination() { PageSize = pageSize, PageNumber = pageNumber },
                categoryId = catId,
                RiskLevelId = riskId,
                Search = searchVal,
                CaseStatus = caseStatus,
                ReportedFrom = ReportedFrom,
                ReportedTo = ReportedTo,
                sortOrder = reportSort,
                CaseOfficerId=caseOfficer
            };

            ReportResponse response = new ReportService().GetOpenCases(req);

            ViewModel.ReportOpenCases = new ReportCase()
            {
                objPaginationBO = new Pagination()
                {
                    PageNumber = response.objPaginationBO.PageNumber,
                    PageSize = response.objPaginationBO.PageSize,
                    TotalPages = response.objPaginationBO.TotalPages,
                    TotalRows = response.objPaginationBO.TotalRows
                },
                objCaseBO = response.objCaseBO
            };
            return ViewModel;
        }

    }
}