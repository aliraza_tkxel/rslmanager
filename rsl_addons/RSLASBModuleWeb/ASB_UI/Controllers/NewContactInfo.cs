﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASB_UI.Controllers
{
    /// <summary>
    /// Contact information received from View
    /// </summary>
    public class NewContactInfo
    {
        /// <summary>
        /// Case id
        /// </summary>
        public int CaseId { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// Organization
        /// </summary>
        public string Organization { get; set; }
        /// <summary>
        /// Telephone number
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// Employee id who added contact
        /// </summary>
        public int AddedBy { get; set; }
        /// <summary>
        /// Either removed from case or not
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Page Number
        /// </summary>
        public int PageNumber { get; set; }
        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }
    }
}