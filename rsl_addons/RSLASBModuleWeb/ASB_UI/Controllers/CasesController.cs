﻿using ASB_UI.ViewModels;
using ASB_UI.ViewModels.AmendCase;
using ASB_UI.ViewModels.CreateNewCase;
using ASB_UI.ViewModels.AmendCase.SubViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using ASB_UI.ViewModels.CreateNewCase.SubViewModels;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Rotativa;
using System.Configuration;
using System.Text.RegularExpressions;

namespace ASB_UI.Controllers
{
    public class CasesController : BaseController
    {
        #region New Case
        // GET: Cases
        public ActionResult Cases()
        {
            CreateNewCaseViewModel newCaseViewModel = new CreateNewCaseViewModel();
            newCaseViewModel.NewCase = new NewCaseForm();
            string defaultdate = DateTime.Today.ToString().Split(' ')[0];
            //defaultdate = defaultdate.Split('/')[1] + "/" + defaultdate.Split('/')[0] + "/" + defaultdate.Split('/')[2];
            newCaseViewModel.NewCase.DateReported = defaultdate;
            newCaseViewModel.NewCase.DateRecorded = defaultdate;
            newCaseViewModel.NewCase.DateOfIncident = defaultdate;
            newCaseViewModel.NewCase.NextFollowUp = defaultdate;
            newCaseViewModel.NewCase.CaseClosed = null;
            newCaseViewModel.Persons = new List<string>();
            newCaseViewModel.PrevCasesVM = new PreviousCasesViewModel();
            newCaseViewModel.PrevCasesVM.Paging = new Pagination()
            {
                PageNumber = 1,
                PageSize = 5
            };
            newCaseViewModel.PrevCasesVM.PreviousCases = new List<PreviousCase>()
            {
                new PreviousCase()
            };
            ViewBag.IsNewCase = true;
            //Save Date Format in ViewBag
            ViewBag.DefDateFormat = DEFAULT_DATE_FORMAT;
            ViewBag.DefDateFormatForModel = DATE_FORMAT_FOR_MODEL;
            CommonServices service = new CommonServices();
            newCaseViewModel.NewCase._CaseOfficers = service.GetCaseOfficers();
            newCaseViewModel.NewCase._CaseOfficersTwo = newCaseViewModel.NewCase._CaseOfficers;
            newCaseViewModel.NewCase._Categories = service.GetCategories();
            newCaseViewModel.NewCase._LevelOfRisks = service.GetRiskLevels();
            newCaseViewModel.NewCase._TypeOfIncident = service.GetIncidentTypes();
            newCaseViewModel.NewCase._Stages = service.GetStages();
            return PartialView("Cases", newCaseViewModel);
        }

        #endregion

        #region Get Categories for selected stage
        public ActionResult Categories(int? stage)
        {
            var subcategories=new List<SubCategory>();
            if(stage!=null)
            {
                CommonServices service = new CommonServices();
                subcategories=service.GetSubCategories((int)stage).ToList();
                //categories.Add(new Category() { key = 1, value = "One" });
            }     
            return Json(subcategories, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Types for selected stage
        public ActionResult Types(int? subcategory)
        {
            var subcategories = new List<Types>();
            if (subcategory != null)
            {
                CommonServices service = new CommonServices();
                subcategories = service.GetTypes((int)subcategory).ToList();
                //categories.Add(new Category() { key = 1, value = "One" });
            }
            return Json(subcategories, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get SubTypes for selected stage
        public ActionResult SubTypes(int? types)
        {
            var subcategories = new List<SubType>();
            if (types != null)
            {
                CommonServices service = new CommonServices();
                subcategories = service.GetSubTypes((int)types).ToList();
                //categories.Add(new Category() { key = 1, value = "One" });
            }
            return Json(subcategories, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Amend Case
        public ActionResult AmendCaseWithDetail(int caseId)
        {
            CommonServices common = new CommonServices();
            AmendCaseViewModelWithDetails acvm = MakeAmendCaseViewModelWithDetails(caseId);

            ViewBag.IsNewCase = false;
            ViewBag.CaseId = caseId;
            ViewBag.EmpId = GetUserIdFromSession();
            ViewBag.IsManager = common.IsManager(GetUserIdFromSession());
            return PartialView("~/Views/Cases/Amending/AmendCaseWithDetails.cshtml", acvm);
        }
        /// <summary>
        /// Creates <see cref="AmendCaseViewModel"/> object when CaseId id provided
        /// </summary>
        /// <param name="pIntCaseId">Case ID</param>
        /// <returns>Object Model of <see cref="AmendCaseViewModel"/></returns>
        private AmendCaseViewModelWithDetails MakeAmendCaseViewModelWithDetails(int pIntCaseId)
        {
            AmendCaseViewModelWithDetails acvm = new AmendCaseViewModelWithDetails();
            acvm.CaseId = pIntCaseId;
            acvm.AmendCaseDetails = new AmendCaseForm();
            //Fetch data from Service
            CaseServices caseDetailService = new CaseServices();
            AmendCaseDetailResponse caseresponse = caseDetailService.GetAmendCaseDetails(pIntCaseId);
            acvm.AmendCaseDetails = new ViewModels.AmendCase.SubViewModels.AmendCaseForm()
            {
                CaseClosed = caseresponse.objUpdateCaseBO.ClosedDate,
                CaseId = pIntCaseId,
                SelectedCaseOfficerId = caseresponse.objUpdateCaseBO.CaseOfficer,
                SelectedCaseOfficerTwoId = caseresponse.objUpdateCaseBO.CaseOfficerTwo,
                CaseStatus = caseresponse.objUpdateCaseBO.CaseStatus,
                SelectedCategoryId = caseresponse.objUpdateCaseBO.Category,
                CrimeCaseNo = caseresponse.objUpdateCaseBO.CrimeCaseNumber,
                DateOfIncident = caseresponse.objUpdateCaseBO.IncidentDate,
                DateRecorded = caseresponse.objUpdateCaseBO.DateRecorded,
                DateReported = caseresponse.objUpdateCaseBO.DateReported,
                IncidentDescription = caseresponse.objUpdateCaseBO.IncidentDescription,
                SelectedPoliceNotifyId = caseresponse.objUpdateCaseBO.PoliceNotified,
                SelectedRiskLevelId = caseresponse.objUpdateCaseBO.RiskLevel==null?0: (int)caseresponse.objUpdateCaseBO.RiskLevel,
                NextFollowUpDate = caseresponse.objUpdateCaseBO.NextFollowupDate,
                NextFollowUpNote = caseresponse.objUpdateCaseBO.FollowupDescription,
                ResolutionNotes = caseresponse.objUpdateCaseBO.ClosedDescription,
                TimeOfIncident = caseresponse.objUpdateCaseBO.IncidentTime,
                SelectedIncidentTypeId = caseresponse.objUpdateCaseBO.incidentType,
                SelectedStageId = caseresponse.objUpdateCaseBO.StageId,
                SelectedSubCategoryId = caseresponse.objUpdateCaseBO.SelectedSubCategoryId,
                SelectedSubTypeId = caseresponse.objUpdateCaseBO.SelectedSubTypeId,
                SelectedTypeId = caseresponse.objUpdateCaseBO.SelectedTypeId,
                ReviewDate = caseresponse.objUpdateCaseBO.ReviewDate
            };
            CommonServices service = new CommonServices();
            acvm.AmendCaseDetails._CaseOfficers = service.GetCaseOfficers();
            acvm.AmendCaseDetails._CaseOfficersTwo = acvm.AmendCaseDetails._CaseOfficers;
            acvm.AmendCaseDetails._Categories = service.GetCategories();
            acvm.AmendCaseDetails._LevelOfRisks = service.GetRiskLevels();
            acvm.AmendCaseDetails._TypeOfIncident = service.GetIncidentTypes();
            acvm.AmendCaseDetails._Stages = service.GetStages();
            acvm.AmendCaseDetails._SubCategories = service.GetSubCategories(acvm.AmendCaseDetails.SelectedStageId);
            acvm.AmendCaseDetails._Types = service.GetTypes(acvm.AmendCaseDetails.SelectedStageId);
            acvm.AmendCaseDetails._SubTypes = service.GetSubTypes(acvm.AmendCaseDetails.SelectedStageId);
            acvm.PersonModel = new PersonsViewModel()
            {
                CaseId = pIntCaseId,
                UserId = GetUserIdFromSession(),
            };
            acvm.PersonModel.Persons = new List<CasePerson>();
            foreach (var p in caseresponse.objPersonList)
            {
                CasePerson person = new CasePerson();
                person.CaseId = p.CaseId;
                person.PersonId = p.PersonId;
                person.PersonRole = p.PersonRole;
                person.PersonType = p.PersonType;
                acvm.PersonModel.Persons.Add(person);
            }

            //Initiate All Documents request
            AllFilesRequest docRequest = new AllFilesRequest()
            {
                caseId = pIntCaseId,
                fileType = "Document",
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };
            FileResponse docResponse = caseDetailService.GetAllFiles(docRequest);
            acvm.FileViewModel = ToFileViewModel(docResponse);
            //Initiate All Photographs request
            AllFilesRequest photoRequest = new AllFilesRequest()
            {
                caseId = pIntCaseId,
                fileType = "Photograph",
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };
            FileResponse photoResponse = caseDetailService.GetAllFiles(photoRequest);
            acvm.PhotoViewModel = ToFileViewModel(photoResponse);

            //Initiate All Associated Contacts request
            AssociatedContactRequest contactsRequest = new AssociatedContactRequest()
            {
                CaseId = pIntCaseId,
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };
            AssociatedContactResponse contactsResponse = caseDetailService.GetContacts(contactsRequest);
            acvm.ContactsModel = ToContactsModel(contactsResponse);
            acvm.ContactsModel.CaseId = pIntCaseId;
            acvm.ContactsModel.RecordedById = GetUserIdFromSession();

            //Initiate Case Journals/Actions Request
            JournalDataRequest actionsRequest = new JournalDataRequest()
            {
                CaseId = pIntCaseId,
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 10
                }
            };
            JournalDataResponse actionsResponse = caseDetailService.GetCaseJournalData(actionsRequest);
            acvm.CaseActions = ToCaseActions(actionsResponse);
            ViewBag.DefDateFormatForModel = DATE_FORMAT_FOR_MODEL;
            return acvm;
        }
        public ActionResult AmendCase(int caseId)
        {
            CommonServices common = new CommonServices();
            AmendCaseViewModel acvm = MakeAmendCaseViewModel(caseId);

            ViewBag.IsNewCase = false;
            ViewBag.CaseId = caseId;
            ViewBag.EmpId = GetUserIdFromSession();
            ViewBag.IsManager = common.IsManager(GetUserIdFromSession());

            if (!acvm.DetailsAndActions.CaseDetails.CaseStatus.ToLower().Equals("open"))
            {
                return PartialView("~/Views/Cases/ReadOnly/ViewCase.cshtml", acvm);
            }

            return PartialView("~/Views/Cases/Amending/AmendCase.cshtml", acvm);
        }
        /// <summary>
        /// Creates <see cref="AmendCaseViewModel"/> object when CaseId id provided
        /// </summary>
        /// <param name="pIntCaseId">Case ID</param>
        /// <returns>Object Model of <see cref="AmendCaseViewModel"/></returns>
        private AmendCaseViewModel MakeAmendCaseViewModel(int pIntCaseId, bool isFullListRequired = false)
        {
            Pagination fullListPageBO = new Pagination(){ PageNumber = 1, PageSize = ApplicationConstants.MaxPageSizeLimit };

            AmendCaseViewModel acvm = new AmendCaseViewModel();
            acvm.CaseId = pIntCaseId;
            acvm.DetailsAndActions = new CaseDetailsAndActions();
            //Fetch data from Service
            CaseServices caseDetailService = new CaseServices();
            CaseDetailResponse caseresponse = caseDetailService.GetCaseDetails(pIntCaseId);
            acvm.DetailsAndActions.CaseDetails = new ViewModels.AmendCase.SubViewModels.CaseDetail()
            {
                CaseClosed = caseresponse.objUpdateCaseBO.ClosedDate,
                CaseId = pIntCaseId,
                CaseOfficer = caseresponse.objUpdateCaseBO.CaseOfficer,
                CaseOfficerTwo= caseresponse.objUpdateCaseBO.CaseOfficerTwo,
                CaseStatus = caseresponse.objUpdateCaseBO.CaseStatus,
                Category = caseresponse.objUpdateCaseBO.Category,
                CrimeCaseNo = caseresponse.objUpdateCaseBO.CrimeCaseNumber,
                DateOfIncident = caseresponse.objUpdateCaseBO.IncidentDate,
                DateRecorded = caseresponse.objUpdateCaseBO.DateRecorded,
                DateReported = caseresponse.objUpdateCaseBO.DateReported,
                IncidentDescription = caseresponse.objUpdateCaseBO.IncidentDescription,
                IsPoliceNotified = caseresponse.objUpdateCaseBO.PoliceNotified,
                LevelOfRisk = caseresponse.objUpdateCaseBO.RiskLevel,
                NextFollowUp = caseresponse.objUpdateCaseBO.NextFollowupDate,
                NextFollowUpNote = caseresponse.objUpdateCaseBO.FollowupDescription,
                ResolutionNotes = caseresponse.objUpdateCaseBO.ClosedDescription,
                TimeOfIncident = caseresponse.objUpdateCaseBO.IncidentTime,
                TypeOfIncident = caseresponse.objUpdateCaseBO.incidentType,
                Stage= caseresponse.objUpdateCaseBO.Stage,
                StageId=caseresponse.objUpdateCaseBO.StageId,
                ReviewDate= caseresponse.objUpdateCaseBO.ReviewDate
            };
            acvm.PersonModel = new PersonsViewModel()
            {
                CaseId = pIntCaseId,
                UserId = GetUserIdFromSession(),
            };
            acvm.PersonModel.Persons = new List<CasePerson>();
            foreach (var p in caseresponse.objPersonList)
            {
                CasePerson person = new CasePerson();
                person.CaseId = p.CaseId;
                person.PersonId = p.PersonId;
                person.PersonRole = p.PersonRole;
                person.PersonType = p.PersonType;
                acvm.PersonModel.Persons.Add(person);
            }

            //Initiate All Documents request
            AllFilesRequest docRequest = new AllFilesRequest()
            {
                caseId = pIntCaseId,
                fileType = "Document",
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };

            if (isFullListRequired)
            {
                docRequest.objPaginationBO = fullListPageBO;
            }
            
            FileResponse docResponse = caseDetailService.GetAllFiles(docRequest);
            acvm.FileViewModel = ToFileViewModel(docResponse);
            //Initiate All Photographs request
            AllFilesRequest photoRequest = new AllFilesRequest()
            {
                caseId = pIntCaseId,
                fileType = "Photograph",
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };

            if (isFullListRequired)
            {
                photoRequest.objPaginationBO = fullListPageBO;
            }

            FileResponse photoResponse = caseDetailService.GetAllFiles(photoRequest);
            acvm.PhotoViewModel = ToFileViewModel(photoResponse);

            //Initiate All Associated Contacts request
            AssociatedContactRequest contactsRequest = new AssociatedContactRequest()
            {
                CaseId = pIntCaseId,
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 5
                }
            };

            if (isFullListRequired)
            {
                contactsRequest.objPaginationBO = fullListPageBO;
            }

            AssociatedContactResponse contactsResponse = caseDetailService.GetContacts(contactsRequest);
            acvm.ContactsModel = ToContactsModel(contactsResponse);
            acvm.ContactsModel.CaseId = pIntCaseId;
            acvm.ContactsModel.RecordedById = GetUserIdFromSession();

            //Initiate Case Journals/Actions Request
            JournalDataRequest actionsRequest = new JournalDataRequest()
            {
                CaseId = pIntCaseId,
                objPaginationBO = new Pagination()
                {
                    PageNumber = 1,
                    PageSize = 10
                }
            };

            if (isFullListRequired)
            {
                actionsRequest.objPaginationBO = fullListPageBO;
            }

            JournalDataResponse actionsResponse = caseDetailService.GetCaseJournalData(actionsRequest);
            acvm.DetailsAndActions.CaseActions = ToCaseActions(actionsResponse);
            ViewBag.DefDateFormatForModel = DATE_FORMAT_FOR_MODEL;
            return acvm;
        }

        /// <summary>
        /// Converts Actions/Journal response to List of Actions that is Acceptable to ViewModel
        /// </summary>
        /// <param name="response"><see cref="JournalDataResponse"/></param>
        /// <returns>List of <see cref="CaseAction"/></returns>
        private CaseActionsViewModel ToCaseActions(JournalDataResponse response)
        {
            CaseActionsViewModel model = new CaseActionsViewModel();
            //var reverselist = response.objJournalBO.Reverse();
            model.ActionsList = new List<CaseAction>();
            foreach (var action in response.objJournalBO)
            {
                CaseAction newAction = new CaseAction()
                {
                    Action = action.Action,
                    ActionNotes = action.ActionNotes,
                    CaseId = action.CaseId,
                    RecordedBy = action.RecordedBy,
                    RecordedDate = String.Format(DATE_FORMAT_FOR_MODEL, action.RecordedDate),
                    HasAttachment = (action.FileName == null) ? false : true,
                    StageId = action.StageId,
                    stage=action.stage==null?string.Empty:action.stage,
                    SubCategoryId=action.SubCategoryId,
                    SubCategory=action.SubCategory==null?string.Empty:action.SubCategory,
                    ReviewDate= action.ReviewDate==null? "N/A":  String.Format(DATE_FORMAT_FOR_MODEL, action.ReviewDate),
                };
                //Set FileName and FilePath separtely depending on the HasAttachement value
                newAction.FileName = (newAction.HasAttachment) ? action.FileName : null;
                newAction.FilePath = (newAction.HasAttachment) ? action.FilePath : null;
                //Add in List
                model.ActionsList.Add(newAction);
            }
            model.Paging = new Pagination()
            {
                PageNumber = response.objPaginationBO.PageNumber,
                PageSize = response.objPaginationBO.PageSize,
                TotalPages = response.objPaginationBO.TotalPages,
                TotalRows = response.objPaginationBO.TotalRows,
                FunctionName = "Action"
            };
            return model;
        }
        /// <summary>
        /// Converts Contact response into Contacts ViewModel
        /// </summary>
        /// <param name="response"><see cref="AssociatedContactResponse"/></param>
        /// <returns><see cref="AssociatedContactViewModel"/></returns>
        private AssociatedContactViewModel ToContactsModel(AssociatedContactResponse response)
        {
            AssociatedContactViewModel model = new AssociatedContactViewModel();
            model.Contacts = new List<ASB_UI.ViewModels.AmendCase.SubViewModels.AssociatedContact>();
            foreach (var c in response.objContactBO)
            {
                ASB_UI.ViewModels.AmendCase.SubViewModels.AssociatedContact contact = new ViewModels.AmendCase.SubViewModels.AssociatedContact()
                {
                    IsActive = c.IsActive,
                    Name = c.ContactName,
                    Organization = c.Organization,
                    Telephone = c.Telephone,
                    ContactId = c.ContactId
                };
                model.Contacts.Add(contact);
            }
            model.Paging = new Pagination()
            {
                PageNumber = response.objPaginationBO.PageNumber,
                PageSize = response.objPaginationBO.PageSize,
                TotalPages = response.objPaginationBO.TotalPages,
                TotalRows = response.objPaginationBO.TotalRows,
                FunctionName = "Contact"
            };
            return model;
        }
        #endregion

        #region Previous Cases
        /// <summary>
        /// Previous cases of Person
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="ptype"></param>
        /// <param name="prole"></param>
        /// <param name="pnum"></param>
        /// <param name="psize"></param>
        /// <returns></returns>
        public ActionResult PreviousCases(int pid, string ptype, string prole, int pnum, int psize)
        {
            PreviousCasesRequest request = new PreviousCasesRequest()
            {
                PersonId = pid,
                PersonType = ptype,
                PersonRole = prole,
                objPaginationBO = new Pagination()
                {
                    PageNumber = pnum,
                    PageSize = psize
                }
            };
            CaseServices service = new CaseServices();
            PreviousCasesResponse response = service.GetPreviousCases(request);
            PreviousCasesViewModel pcvm = new PreviousCasesViewModel();
            //if (response.objAssociatedCasesPopupBO.Count() == 0)
            //{
            //    return Json(false);
            //}
            pcvm.PreviousCases = response.objAssociatedCasesPopupBO;
            pcvm.Paging = response.objPaginationBO;
            pcvm.Paging.FunctionName = "PreviousCases";
            //pcvm.PreviousWindow = prevWin;
            return PartialView("~/Views/Cases/_PartialPreviousCases.cshtml", pcvm);
        }

        public ActionResult CustomerPreviousCases(int pid)
        {
            int userId = int.Parse(Request.QueryString["UserId"]);
            Session["UserId"] = userId;
            CaseServices service = new CaseServices();
            PreviousCasesResponse response = service.GetPersonsAssociatedCases(pid);
            PreviousCasesViewModel pcvm = new PreviousCasesViewModel();

            pcvm.PreviousCases = response.objAssociatedCasesPopupBO;
            //pcvm.Paging = response.objPaginationBO;
            //pcvm.Paging.FunctionName = "CustomerPreviousCases";
            ViewBag.CustomerId = pid;
            return View("~/Views/Cases/CasesList.cshtml", pcvm);
        }

        #endregion

        #region Save Case

        /// <summary>
        /// Saves case in database through API
        /// </summary>
        /// <param name="param"><see cref="CreateNewCaseViewModel"/> object</param>
        /// <returns>Saved case ID</returns>
        [HttpPost]
        public JsonResult SaveCase(CreateNewCaseViewModel param)
        {            
            SaveCaseRequest request = ToSaveCaseRequest(param);
            CaseServices service = new CaseServices();
            int caseid = Int32.Parse(service.SaveCase(request));
            if(param.File!=null)
            {
                var uploaded=UploadAddVulnerabilityFile(caseid, "Document", param.File, param.fileNotes);
            }
            //AmendCaseViewModel acvm = MakeAmendCaseViewModel(caseid);

            ViewBag.IsNewCase = false;
            ViewBag.CaseId = caseid;
            ViewBag.EmpId = GetUserIdFromSession();
            return Json(new { success = true, CaseId = caseid });
            //return PartialView("~/Views/Cases/Amending/AmendCase.cshtml", acvm);
        }
        
        /// <summary>
        /// Saves case in database through API
        /// </summary>
        /// <param name="param"><see cref="CreateNewCaseViewModel"/> object</param>
        /// <returns>Saved case ID</returns>
        [HttpPost]
        public JsonResult AddNewCaseFile(int caseId, string filenotes)
        {
            logger.Debug("Entry");
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                var uploaded = UploadAddVulnerabilityFile(caseId, "Document", file, filenotes);
            }
            logger.Debug("Exit");
            return Json(new { success = true});
            //return PartialView("~/Views/Cases/Amending/AmendCase.cshtml", acvm);
        }

        /// <summary>
        /// Converts <see cref="CreateNewCaseViewModel" to <see cref="SaveCaseRequest" object/>/>
        /// </summary>
        /// <param name="data">object of <see cref="CreateNewCaseViewModel"/></param>
        /// <returns><see cref="SaveCaseRequest"/> object</returns>
        private SaveCaseRequest ToSaveCaseRequest(CreateNewCaseViewModel data)
        {
            DateTime daterep, nextfollowdate, incidentdate;
            
            if (data.NewCase.DateReported[1] == '/' && data.NewCase.DateReported[3] == '/')
            {
                data.NewCase.DateReported = "0" + data.NewCase.DateReported.Substring(0, 2) + "0" + data.NewCase.DateReported.Substring(2);
            }
            else if (data.NewCase.DateReported[1] == '/')
            {
                data.NewCase.DateReported = "0" + data.NewCase.DateReported;
            }
            else if (data.NewCase.DateReported[4] == '/')
            {
                data.NewCase.DateReported = data.NewCase.DateReported.Substring(0, 3) + "0" + data.NewCase.DateReported.Substring(3);
            }
            daterep = DateTime.ParseExact(data.NewCase.DateReported, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (data.NewCase.NextFollowUp[1] == '/' && data.NewCase.NextFollowUp[3] == '/')
            {
                data.NewCase.NextFollowUp = "0" + data.NewCase.NextFollowUp.Substring(0, 2) + "0" + data.NewCase.NextFollowUp.Substring(2);
            }
            else if (data.NewCase.NextFollowUp[1] == '/')
            {
                data.NewCase.NextFollowUp = "0" + data.NewCase.NextFollowUp;
            }
            else if (data.NewCase.NextFollowUp[4] == '/')
            {
                data.NewCase.NextFollowUp = data.NewCase.NextFollowUp.Substring(0, 3) + "0" + data.NewCase.NextFollowUp.Substring(3);
            }
            nextfollowdate = DateTime.ParseExact(data.NewCase.NextFollowUp, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (data.NewCase.DateOfIncident[1] == '/' && data.NewCase.DateOfIncident[3] == '/')
            {
                data.NewCase.DateOfIncident = "0" + data.NewCase.DateOfIncident.Substring(0, 2) + "0" + data.NewCase.DateOfIncident.Substring(2);
            }
            else if (data.NewCase.DateOfIncident[1] == '/')
            {
                data.NewCase.DateOfIncident = "0" + data.NewCase.DateOfIncident;
            }
            else if (data.NewCase.DateOfIncident[4] == '/')
            {
                data.NewCase.DateOfIncident = data.NewCase.DateOfIncident.Substring(0, 3) + "0" + data.NewCase.DateOfIncident.Substring(3);
            }
            incidentdate = DateTime.ParseExact(data.NewCase.DateOfIncident, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            SaveCaseRequest req = new SaveCaseRequest();
            req.objCaseBO = new NewCaseFields()
            {
                CaseOfficer = data.NewCase.SelectedCaseOfficerId,
                CaseOfficerTwo = data.NewCase.SelectedCaseOfficerTwoId == data.NewCase.SelectedCaseOfficerId ? null : data.NewCase.SelectedCaseOfficerTwoId,
                CaseStatus = 1,
                ReviewDate=DateTime.Now,
                Category = data.NewCase.SelectedCategoryId,
                Stage = data.NewCase.SelectedStageId,
                SubCategory = data.NewCase.SelectedSubCategoryId,
                Types=data.NewCase.SelectedTypeId,
                SubType=data.NewCase.SelectedSubTypeId,
                //ClosedDate = DateTime.Parse(string.IsNullOrEmpty(data.NewCase.CaseClosed) ? string.Empty : data.NewCase.CaseClosed),
                ClosedDate = data.NewCase.CaseClosed,
                ClosedDescription = string.IsNullOrEmpty(data.NewCase.ResolutionNotes) ? "" : data.NewCase.ResolutionNotes,
                CrimeCaseNumber = data.NewCase.CrimeCaseNo,
                //DateRecorded = data.NewCase.DateRecorded,
                DateRecorded = DateTime.Today,
                DateReported = daterep,
                IncidentDate = incidentdate,
                IncidentDescription = data.NewCase.IncidentDescription,
                IncidentTime = data.NewCase.TimeOfIncident,
                incidentType = data.NewCase.SelectedIncidentTypeId,
                NextFollowupDate = nextfollowdate,
                PoliceNotified = data.NewCase.SelectedPoliceNotifyId == 0 ? false : true,
                RiskLevel = data.NewCase.SelectedRiskLevelId,
                FollowupDescription = string.IsNullOrEmpty(data.NewCase.NextFollowUpNote) ? "" : data.NewCase.NextFollowUpNote
            };
            req.RecordedBy = GetUserIdFromSession();
            req.objPersonBO = new List<CasePersons>();
            if (data.Persons != null)
            {
                foreach (var p in data.Persons)
                {
                    string[] vals = p.Split(',');
                    req.objPersonBO.Add(new CasePersons()
                    {
                        PersonId = Int32.Parse(vals[0]),
                        PersonRole = vals[2],
                        PersonType = vals[1]
                    });
                }
            }
            return req;
        }

        #endregion

        #region
        /// <summary>
        /// Updates case in database through API
        /// </summary>
        /// <param name="param"><see cref="AmendCaseForm"/> object</param>
        /// <returns>Updated case ID</returns>
        [HttpPost]
        public JsonResult UpdateCase(AmendCaseForm param)
        {
            UpdateCaseRequest request = ToUpdateCaseRequest(param);
            CaseServices service = new CaseServices();
            int caseid = Int32.Parse(service.UpdateCaseDetails(request));
            ViewBag.IsNewCase = false;
            ViewBag.CaseId = caseid;
            ViewBag.EmpId = GetUserIdFromSession();
            return Json(new { success = true, CaseId = caseid });
            //return PartialView("~/Views/Cases/Amending/AmendCase.cshtml", acvm);
        }
        /// <summary>
        /// Converts <see cref="AmendCaseForm" to <see cref="UpdateCaseRequest" object/>/>
        /// </summary>
        /// <param name="data">object of <see cref=AmendCaseForm"/></param>
        /// <returns><see cref="UpdateCaseRequest"/> object</returns>
        private UpdateCaseRequest ToUpdateCaseRequest(AmendCaseForm data)
        {
            UpdateCaseRequest req = new UpdateCaseRequest();
            req.objCaseBO = new UpdateCaseFields()
            {
                CaseId=data.CaseId,
                CaseOfficer = data.SelectedCaseOfficerId,
                CaseOfficerTwo = data.SelectedCaseOfficerTwoId == data.SelectedCaseOfficerId ? null : data.SelectedCaseOfficerTwoId,
                Category = data.SelectedCategoryId,
                Stage = data.SelectedStageId,
                SubCategory = data.SelectedSubCategoryId,
                Types = data.SelectedTypeId,
                SubType = data.SelectedSubTypeId,
                //ClosedDate = DateTime.Parse(string.IsNullOrEmpty(data.NewCase.CaseClosed) ? string.Empty : data.NewCase.CaseClosed),
                //DateRecorded = data.NewCase.DateRecorded,
                IncidentDescription = data.IncidentDescription,
                incidentType = data.SelectedIncidentTypeId,
                PoliceNotified = data.SelectedPoliceNotifyId == 0 ? false : true,
                RiskLevel = data.SelectedRiskLevelId,
                FollowupDescription = string.IsNullOrEmpty(data.NextFollowUpNote) ? "" : data.NextFollowUpNote
            };
            req.RecordedBy = GetUserIdFromSession();
            return req;
        }
        #endregion

        #region File Management

        /// <summary>
        /// Get Files from server. This is pagination method called from _PartialDocument.cshtml file
        /// </summary>
        /// <param name="pIntCaseId">Case ID</param>
        /// <param name="pStrType">"Document" or "Photograph"</param>
        /// <param name="pIntPgeNum">Page number</param>
        /// <param name="pIntPgSze">Page size</param>
        /// <returns></returns>
        public ActionResult GetFiles(int pIntCaseId, string pStrType, int pIntPgeNum, int pIntPgSze)
        {
            CaseServices fileService = new CaseServices();
            //Initiate All Files request
            AllFilesRequest fileRequest = new AllFilesRequest()
            {
                caseId = pIntCaseId,
                fileType = pStrType,
                objPaginationBO = new Pagination()
                {
                    PageNumber = pIntPgeNum,
                    PageSize = pIntPgSze,
                }
            };
            FileResponse fileResponse = fileService.GetAllFiles(fileRequest);
            FileCaseViewModel model = ToFileViewModel(fileResponse);
            ViewBag.EmpId = GetUserIdFromSession();
            ViewBag.IsManager = new CommonServices().IsManager(GetUserIdFromSession());
            ViewBag.IsNewCase = false;
            ViewBag.CaseId = pIntCaseId;
            if (pStrType.Equals("Document"))
            {
                return PartialView("~/Views/Cases/_PartialDocuments.cshtml", model);
            }
            else
            {
                return PartialView("~/Views/Cases/_PartialPhotographs.cshtml", model);
            }
            //return PartialView("~/Views/Cases/_PartialDocuments.cshtml", model);
        }
        /// <summary>
        /// Called when file to be removed
        /// </summary>
        /// <param name="objFileParams"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveFile(RemoveFileParams objRmFileParams)
        {
            if (ModelState.IsValid)
            {
                FileRemoveRequest removeRequest = new FileRemoveRequest()
                {
                    caseId = objRmFileParams.CaseId,
                    fileId = objRmFileParams.FileId,
                    fileType = objRmFileParams.Type,
                    RemovedBy = objRmFileParams.RemovedBy,
                    objPaginationBO = new Pagination()
                    {
                        PageNumber = objRmFileParams.PageNumber,
                        PageSize = objRmFileParams.PageSize
                    }
                };
                CaseServices removeService = new CaseServices();
                FileResponse response = removeService.RemoveFile(removeRequest);
                FileCaseViewModel model = ToFileViewModel(response);
                ViewBag.EmpId = GetUserIdFromSession();
                ViewBag.IsManager = new CommonServices().IsManager(GetUserIdFromSession());
                ViewBag.IsNewCase = false;
                ViewBag.CaseId = objRmFileParams.CaseId;

                RemoveFileFromDisk(GetPhysicalPath(objRmFileParams.CaseId, objRmFileParams.Type) + "\\" + objRmFileParams.FileName);

                //RemoveFileFromDisk(objRmFileParams.FilePath + "\\" + objRmFileParams.FileName);
                if (objRmFileParams.Type.Equals("Document"))
                {
                    return PartialView("~/Views/Cases/_PartialDocuments.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/Cases/_PartialPhotographs.cshtml", model);
                }
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable, "Error Processing Request");
            }
            //return PartialView();
        }
        /// <summary>
        /// Removes file from Disk as well.
        /// </summary>
        /// <param name="filePath">File path</param>
        private void RemoveFileFromDisk(string filePath)
        {
            string truePath = Path.GetFullPath(filePath).Replace(@"\\", @"\");
            logger.Info("FILE PATH :: " + true);
            if (System.IO.File.Exists(truePath))
            {
                System.IO.File.Delete(truePath);
            }
        }
        /// <summary>
        /// Uploads file on server.
        /// </summary>
        /// <returns>JSON including status, message and file path</returns>
        public ActionResult UploadFile(int caseid, string type)
        {
            HttpPostedFileBase myFile = Request.Files[0];
            bool isUploaded = false;
            string message = "File upload failed";
            string fileName = string.Empty;
            if (myFile != null)
            {
                string virtualDirName = ConfigurationManager.AppSettings[ApplicationConstants.VirtualDirectory].ToString();

                string pathForSaving = GetPhysicalPath(caseid, type);

                //string pathForSaving = path = Server.MapPath("~/Uploads/Case" + caseid + "/" + type);
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        //myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                        fileName = GetUniqueFileName(myFile.FileName);
                        myFile.SaveAs(Path.Combine(pathForSaving, fileName));
                        isUploaded = true;
                        message = "File uploaded successfully!";
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message, filename = fileName }, "text/html");
        }

        /// <summary>
        /// Uploads file on server.
        /// </summary>
        /// <returns>JSON including status, message and file path</returns>
        public bool UploadAddVulnerabilityFile(int caseid, string type, HttpPostedFileBase filename, string filenotes)
        {
            HttpPostedFileBase myFile = filename;
            bool isUploaded = false;
            string message = "File upload failed";
            string fileName = string.Empty;
            if (myFile != null)
            {
                string virtualDirName = ConfigurationManager.AppSettings[ApplicationConstants.VirtualDirectory].ToString();

                string pathForSaving = GetPhysicalPath(caseid, type);

                //string pathForSaving = path = Server.MapPath("~/Uploads/Case" + caseid + "/" + type);
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        //myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                        fileName = GetUniqueFileName(myFile.FileName);
                        myFile.SaveAs(Path.Combine(pathForSaving, fileName));
                        isUploaded = true;
                        FileParameters fileParams = new FileParameters()
                        {
                            CaseId = caseid,
                            DateUploaded = DateTime.Now.ToString(),
                            FileName = fileName,
                            FilePath = pathForSaving,
                            IsActive = true,
                            Type = "Document",
                            UploadedById = GetUserIdFromSession(),
                            Notes = filenotes

                        };
                        FileRequest filerequest = ToFileRequest(fileParams, new Pagination());
                        CaseServices caseservice = new CaseServices();
                        FileResponse response = caseservice.AddFile(filerequest);
                        message = "File uploaded successfully!";
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return isUploaded;
        }
        /// <summary>
        /// Gets unique file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetUniqueFileName(string fileName)
        {
            dynamic ext = Path.GetExtension(fileName);
            string fileNameWithoutExtention = Path.GetFileNameWithoutExtension(fileName);
            fileNameWithoutExtention = windowsSafeFileName(fileNameWithoutExtention, '-');
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileNameWithoutExtention + uniqueString + ext;

            return fileName;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="replacementChar"></param>
        /// <returns></returns>
        private string windowsSafeFileName(string fileName, char replacementChar)
        {
            char[] chrArray = new char[] { '\'', '\\', '/', ':', '*', '?', '\"', '<', '>', '|', ' ', '#' };
            char[] chrArray1 = chrArray;
            if (fileName.IndexOfAny(chrArray1) >= 0)
            {
                for (int i = 0; i < (int)chrArray1.Length; i++)
                {
                    fileName = fileName.Replace(chrArray1[i], replacementChar);
                }
            }
            return fileName;
        }
        /// <summary>
        /// Download file
        /// </summary>
        /// <param name="pObjFleDnldParams"><see cref="FileDownloadParams"/></param>
        /// <returns><see cref="FileResult"/></returns>
        public ActionResult DownloadFile(string FileName, int CaseId, string FileType)
        {
            try
            {
                ////string virtualDirPath = ConfigurationManager.AppSettings["VDP"].ToString();
                //string truePath = GetPhysicalPath(CaseId, FileType) + "/" + HttpUtility.UrlDecode(FileName);
                ////string truePath = makePath.Replace(@"\\", @"\");
                //byte[] fileBytes = System.IO.File.ReadAllBytes(truePath);
                //var response = new FileContentResult(fileBytes, "application/octet-stream");
                //response.FileDownloadName = Regex.Replace(FileName, @"[^0-9a-zA-Z.]+", "_");
                //return response;
                string truePath = GetPhysicalPath(CaseId, FileType) + "/" + HttpUtility.UrlDecode(FileName);
                if (System.IO.File.Exists(truePath))
                {
                    return File(truePath, "application/jpg", FileName);
                }
                else
                {
                    return PartialView("~/Views/Shared/_PartialFileRemovedError.cshtml");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.InnerException);
                return PartialView("~/Views/Shared/_PartialFileRemovedError.cshtml");
                //return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddFileInCase(NewFileParams model)
        {
            FileParameters fileParams = new FileParameters()
            {
                CaseId = model.CaseId,
                DateUploaded = model.DateUploaded,
                FileName = model.FileName,
                FilePath = model.FilePath,
                IsActive = model.IsActive,
                Type = model.Type,
                UploadedById = model.UploadedById,
                Notes=model.Notes

            };
            Pagination paging = new Pagination()
            {
                PageNumber = model.PageNumber,
                PageSize = model.PageSize
            };
            FileRequest request = ToFileRequest(fileParams, paging);
            CaseServices service = new CaseServices();
            FileResponse response = service.AddFile(request);
            FileCaseViewModel modelDoc = ToFileViewModel(response);
            ViewBag.EmpId = GetUserIdFromSession();
            ViewBag.isManager = new CommonServices().IsManager(GetUserIdFromSession());
            ViewBag.CaseId = model.CaseId;
            ViewBag.IsNewCase = false;
            if (model.Type.Equals("Document"))
            {
                modelDoc.Paging.FunctionName = "Document";
                return PartialView("~/Views/Cases/_PartialDocuments.cshtml", modelDoc);
            }
            else
            {
                modelDoc.Paging.FunctionName = "Photograph";
                return PartialView("~/Views/Cases/_PartialPhotographs.cshtml", modelDoc);
            }
            //}
        }
        /// <summary>
        /// Converts <see cref="FileCaseViewModel"/> to <see cref="FileRequest" object/>
        /// </summary>
        /// <param name="model"><see cref="FileCaseViewModel"/></param>
        /// <param name="pagination"><see cref="Pagination"/></param>
        /// <returns><see cref="FileRequest"/></returns>
        private FileRequest ToFileRequest(FileParameters model, Pagination pagination)
        {
            FileRequest request = new FileRequest()
            {
                CaseId = model.CaseId,
                DateUploaded = model.DateUploaded,
                FileName = model.FileName,
                FilePath = model.FilePath,
                FileType = model.Type,
                IsActive = model.IsActive,
                UploadedBy = model.UploadedById,
                FileNotes=model.Notes
            };
            request.objPaginationBO = pagination;
            return request;
        }
        /// <summary>
        /// Converts response to ViewModel
        /// </summary>
        /// <param name="response"><see cref="FileResponse"/></param>
        /// <returns><see cref="FileCaseViewModel"/></returns>
        private FileCaseViewModel ToFileViewModel(FileResponse response)
        {
            string fileType = response.objFileBO.Count() == 0 ? string.Empty : response.objFileBO.FirstOrDefault().FileType;
            FileCaseViewModel model = new FileCaseViewModel();
            model.Params = new List<FileParameters>();
            foreach (var p in response.objFileBO)
            {
                FileParameters fp = new FileParameters()
                {
                    CaseId = p.CaseId,
                    DateUploaded = p.DateUploaded,
                    FileId = p.FileId,
                    FileName = p.FileName,
                    FilePath = p.FilePath,
                    IsActive = p.IsActive,
                    Type = p.FileType,
                    UploadedBy = p.UploadedBy,
                    Notes=p.FileNotes
                };
                model.Params.Add(fp);
            }
            model.Paging = new Pagination();
            if (response.objPaginationBO == null)
            {
                model.Paging.PageNumber = 1;
                model.Paging.PageSize = 5;
            }
            else
            {
                model.Paging.PageNumber = response.objPaginationBO.PageNumber;
                model.Paging.PageSize = response.objPaginationBO.PageSize;
                model.Paging.TotalRows = response.objPaginationBO.TotalRows;
                model.Paging.TotalPages = response.objPaginationBO.TotalPages;
                model.Paging.FunctionName = fileType;
            }
            return model;
        }
        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }
        /// <summary>
        /// Gets physical path
        /// </summary>
        /// <param name="pIntCaseId">Case ID</param>
        /// <param name="pStrFileType">File Type. "Document" or "Photograph"</param>
        /// <returns>Physical path</returns>
        private string GetPhysicalPath(int pIntCaseId, string pStrFileType)
        {
            string virtualDirName = ConfigurationManager.AppSettings[ApplicationConstants.VirtualDirectory].ToString();
            string virtualPath = "~/../" + virtualDirName + "/" + pIntCaseId + "/" + pStrFileType;
            string physicalPath = Server.MapPath(virtualPath);
            logger.Info("PHYSICAL PATH :: " + physicalPath);
            return physicalPath;
        }
        /// <summary>
        /// Check if file exists or not in directory
        /// </summary>
        /// <param name="CaseId">Case Id</param>
        /// <param name="FileName">File Name</param>
        /// <param name="FileType">FileType: Document/Photograph</param>
        /// <returns><see cref="JsonResult"/> with parameter <i>Exists</i>. If true then file exists false otherwise</returns>
        public ActionResult GetFileUrl(int CaseId, string FileName, string FileType)
        {
            string truePath = GetPhysicalPath(CaseId, FileType) + "/" + HttpUtility.UrlDecode(FileName);
            if (!System.IO.File.Exists(truePath))
            {
                return Content(string.Empty);
            }
            var hostname = HttpContext.Request.Url.Host;
            var relativeUrl = "https://" + hostname + "/" + ConfigurationManager.AppSettings[ApplicationConstants.VirtualDirectory].ToString() + "/" + CaseId + "/" + FileType + "/" + FileName;
            return Content(relativeUrl);
        }

        #endregion

        #region Contacts Management

        /// <summary>
        /// Action method when Add New Contact is added
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddContact(NewContactInfo contact)
        {
            AddContactRequest request = ToContactRequest(contact);
            CaseServices servie = new CaseServices();
            AssociatedContactResponse response = servie.AddContact(request);
            AssociatedContactViewModel model = ToContactsModel(response);
            model.CaseId = contact.CaseId;
            model.RecordedById = contact.AddedBy;
            return PartialView("~/Views/Cases/_PartialContacts.cshtml", model);
        }
        /// <summary>
        /// Converts model into Request
        /// </summary>
        /// <param name="info"><see cref="NewContactInfo"/></param>
        /// <returns><see cref="AddContactRequest"/></returns>
        private AddContactRequest ToContactRequest(NewContactInfo info)
        {
            AddContactRequest request = new AddContactRequest()
            {
                objContactBO = new Models.WebAPILayer.ServiceManagers.AssociatedContact()
                {
                    CaseId = info.CaseId,
                    ContactId = 0,
                    ContactName = info.ContactName,
                    IsActive = info.IsActive,
                    Organization = info.Organization,
                    Telephone = info.Telephone
                },
                objPaginationBO = new Pagination()
                {
                    PageNumber = info.PageNumber,
                    PageSize = info.PageSize
                },
                RecordedBy = GetUserIdFromSession()
            };
            return request;
        }
        /// <summary>
        /// Action called when Page is changed within Contacts
        /// </summary>
        /// <param name="caseId">Case ID</param>
        /// <param name="pgNum">Page Number</param>
        /// <param name="pgSize">Page Size</param>
        /// <returns></returns>
        public ActionResult GetContacts(int caseId, int pgNum, int pgSize)
        {
            AssociatedContactRequest request = new AssociatedContactRequest();
            request.CaseId = caseId;
            request.objPaginationBO = new Pagination()
            {
                PageNumber = pgNum,
                PageSize = pgSize
            };
            CaseServices service = new CaseServices();
            AssociatedContactViewModel model = ToContactsModel(service.GetContacts(request));
            model.CaseId = caseId;
            model.RecordedById = GetUserIdFromSession();
            return PartialView("~/Views/Cases/_PartialContacts.cshtml", model);
        }
        /// <summary>
        /// Action method when remove contact is performed from View
        /// </summary>
        /// <param name="paramteres"><see cref="RemoveContactParams"/></param>
        /// <returns></returns>
        public ActionResult RemoveContact(RemoveContactParams paramteres)
        {
            RemoveContactRequest request = new RemoveContactRequest()
            {
                CaseId = paramteres.CaseId,
                ContactId = paramteres.ContactId,
                RemovedBy = paramteres.RemovedBy,
                objPaginationBO = new Pagination()
                {
                    PageNumber = paramteres.PageNumber,
                    PageSize = paramteres.PageSize
                }
            };

            CaseServices service = new CaseServices();
            AssociatedContactViewModel model = ToContactsModel(service.RemoveContact(request));
            model.CaseId = paramteres.CaseId;
            model.RecordedById = paramteres.RemovedBy;
            return PartialView("~/Views/Cases/_PartialContacts.cshtml", model);
        }

        #endregion

        #region Persons Management

        public ActionResult NewPersonForm(string name)
        {
            CommonServices service = new CommonServices();
            Person p = new Person();
            p.Name = name;
            p.Titles = service.GetPersonTitles();
            p.Types = service.GetPersonTypes();
            return PartialView("~/Views/Cases/_PartialAddPerson.cshtml", p);
        }


        [HttpPost]
        public int SaveNewPerson(Person person)
        {
            CaseServices service = new CaseServices();
            string id = service.SaveNewPerson(person);
            return Int32.Parse(id);
        }

        /// <summary>
        /// Populates person details when added in Perpetrator/Complainant tabs
        /// </summary>
        /// <param name="pId">Person ID</param>
        /// <param name="pType">Person Type</param>
        /// <returns></returns>
        public ActionResult PersonDetail(int pId, string pType)
        {
            CommonServices service = new CommonServices();
            PersonRequest pr = new PersonRequest() { PersonId = pId, personType = pType };
            Person person = service.GetPerson(pr);
            return PartialView("~/Views/Cases/_PartialPersonDetails.cshtml", person);
        }
        /// <summary>
        /// Action method when Serach Person is performed from view.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchPerson(string input, string filter)
        {
            SearchPersonRequest request = new SearchPersonRequest()
            {
                filter = filter,
                input = input
            };
            CommonServices service = new CommonServices();
            var data = service.SearchPerson(request);
            //var matches = service.SearchPerson(request);
            return Content(data, "application/json");
        }
        /// <summary>
        /// Action method when Person is Added in the case
        /// </summary>
        /// <param name="person"></param>
        /// <returns>True if added Successfuly false otherwise</returns>
        [HttpPost]
        public bool AddPersonInCase(PersonDetails person)
        {
            return ManagePersonInCase(person, 1);
        }
        /// <summary>
        /// Action method when Person is Removed from case
        /// </summary>
        /// <param name="person"></param>
        /// <returns>True if Removed Successfuly false otherwise</returns>
        [HttpPost]
        public bool RemovePersonFromCase(PersonDetails person)
        {
            return ManagePersonInCase(person, -1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public ActionResult PersonUpdateView(int pId, string pType)
        {
            CommonServices service = new CommonServices();
            PersonRequest request = new PersonRequest() { PersonId = pId, personType = pType };
            Person person = service.GetPerson(request);
            if (person == null)
            {
                person = new Person();
                person.Id = -1;
            }
            person.Id = pId;
            ViewBag.PersonType = pType;
            return PartialView("~/Views/Cases/_PartialUpdatePerson.cshtml", person);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public ActionResult PersonUpdateViewNewCase(int pId, string pType)
        {
            CommonServices service = new CommonServices();
            PersonRequest request = new PersonRequest() { PersonId = pId, personType = pType };
            Person person = service.GetPerson(request);
            if (person == null)
            {
                person = new Person();
                person.Id = -1;
            }
            person.Id = pId;
            if(person.Email=="N/A")
            {
                person.Email = "";
            }
            if (person.AlternateContact == "N/A")
            {
                person.AlternateContact = "";
            }
            if (person.TelephoneHome == "N/A")
            {
                person.TelephoneHome = "";
            }
            if (person.TelephoneWork == "N/A")
            {
                person.TelephoneWork = "";
            }
            ViewBag.PersonType = pType;
            return PartialView("~/Views/Cases/_PartialUpdatePersonNewCase.cshtml", person);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult UpdatePerson(UpdatedPersonInfo model)
        {
            CommonServices service = new CommonServices();
            UpdatePersonRequest request = new UpdatePersonRequest()
            {
                PersonId = model.PersonId,
                PersonType = model.PersonType,
                AlternateContact = model.AlternateContact,
                Email = model.Email,
                TelephoneHome = model.TelephoneH,
                TelephoneWork = model.TelephoneW
            };
            Person person = service.UpdatePerson(request);
            if (person == null)
            {
                person = new Person();
                person.Id = -1;
            }
            person.Id = model.PersonId;
            return PartialView("~/Views/Cases/_PartialPersonDetails.cshtml", person);
        }
        /// <summary>
        /// Adds or remove person from case
        /// </summary>
        /// <param name="person"><see cref="PersonDetails"/></param>
        /// <param name="operation">Operation. 1 for Add and -1 for remove</param>
        /// <returns>True if successful operation is performed, False otherwise</returns>
        private bool ManagePersonInCase(PersonDetails person, int operation)
        {
            bool hasPerformed = false;
            CaseServices service = new CaseServices();
            CasePersonOperationRequest request = new CasePersonOperationRequest()
            {
                RecordedBy = GetUserIdFromSession(),
                objPerson = new CasePersons()
                {
                    CaseId = person.CaseId,
                    PersonId = person.PersonId,
                    PersonRole = person.PersonRole,
                    PersonType = person.PersonType
                }
            };
            switch (operation)
            {
                case 1:
                    hasPerformed = service.AddPersonInCase(request);
                    break;
                case -1:
                    hasPerformed = service.RemovePersonFromCase(request);
                    break;
                default:
                    hasPerformed = false;
                    break;
            }
            return hasPerformed;
        }

        #endregion

        #region Action/Journal Management

        /// <summary>
        /// Updated Case details when Follow Up date is changed to reflect on UI.
        /// </summary>
        /// <param name="caseId">Case ID</param>
        /// <returns></returns>
        public ActionResult UpdateCaseDetailOnFollowUp(int caseId)
        {
            CaseServices service = new CaseServices();
            CaseDetailResponse response = service.GetCaseDetails(caseId);
            ViewModels.AmendCase.SubViewModels.CaseDetail details = new ViewModels.AmendCase.SubViewModels.CaseDetail()
            {
                CaseClosed = response.objUpdateCaseBO.ClosedDate,
                CaseId = caseId,
                CaseOfficer = response.objUpdateCaseBO.CaseOfficer,
                CaseOfficerTwo=response.objUpdateCaseBO.CaseOfficerTwo,
                CaseStatus = response.objUpdateCaseBO.CaseStatus,
                Category = response.objUpdateCaseBO.Category,
                CrimeCaseNo = response.objUpdateCaseBO.CrimeCaseNumber,
                DateOfIncident = response.objUpdateCaseBO.IncidentDate,
                DateRecorded = response.objUpdateCaseBO.DateRecorded,
                DateReported = response.objUpdateCaseBO.DateReported,
                IncidentDescription = response.objUpdateCaseBO.IncidentDescription,
                IsPoliceNotified = response.objUpdateCaseBO.PoliceNotified,
                LevelOfRisk = response.objUpdateCaseBO.RiskLevel,
                NextFollowUp = response.objUpdateCaseBO.NextFollowupDate,
                NextFollowUpNote = response.objUpdateCaseBO.FollowupDescription,
                ResolutionNotes = response.objUpdateCaseBO.ClosedDescription,
                TimeOfIncident = response.objUpdateCaseBO.IncidentTime,
                TypeOfIncident = response.objUpdateCaseBO.incidentType,
                ReviewDate = response.objUpdateCaseBO.ReviewDate,
                Stage= response.objUpdateCaseBO.Stage
            };
            return PartialView("~/Views/Cases/Amending/_PartialCaseDetail.cshtml", details);
        }
        /// <summary>
        /// Action method called when clicked on Page number whithin Actions tab
        /// </summary>
        /// <param name="pIntCaseId">Case id</param>
        /// <param name="pIntPgeNum">Page number</param>
        /// <param name="pIntPgSze">Page size</param>
        /// <returns>PartialView with <see cref="CaseActionsViewModel"/> as model</returns>
        public ActionResult GetJournals(int pIntCaseId, int pIntPgeNum, int pIntPgSze)
        {
            JournalDataRequest request = new JournalDataRequest();
            request.CaseId = pIntCaseId;
            request.objPaginationBO = new Pagination()
            {
                PageNumber = pIntPgeNum,
                PageSize = pIntPgSze
            };
            CaseServices service = new CaseServices();
            JournalDataResponse response = service.GetCaseJournalData(request);
            CaseActionsViewModel model = ToCaseActions(response);
            ViewBag.PagerFunctionName = "Actions";
            return PartialView("~/Views/Cases/Amending/_PartialCaseActions.cshtml", model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowAddActionDialog(string prevActionNotes)
        {
            AddActionDialogModel model = new AddActionDialogModel();
            CommonServices service = new CommonServices();
            model.ActionTypes = service.GetActionTypes();
            model.stagesobject = new StagesBO();
            model.stagesobject._Stages = service.GetStages();
            model.ClosingTypes = service.GetClosingTypes();
            model.stagesobject._SubCategories = new List<SubCategory>();
            model.stagesobject._Types = new List<Types>();
            model.stagesobject._SubTypes = new List<SubType>();
            if (string.IsNullOrEmpty(prevActionNotes) || prevActionNotes.Equals("N/A"))
            {
                model.ActionNotes = string.Empty;
            }
            else
            {
                model.ActionNotes = prevActionNotes;
            }
            ViewBag.DefDateFormat = DEFAULT_DATE_FORMAT;
            return PartialView("~/Views/Cases/Amending/_PartialActionDialog.cshtml", model);
        }

        public ActionResult ShowNotesDialog(string notes, string type)
        {
            ViewBag.Notes = notes;
            if (type == "Photograph")
            {
                ViewBag.DocType = "Photo Notes";
            }
            else
            {
                ViewBag.DocType = "Document Notes";
            }
            return PartialView("~/Views/Cases/Amending/_PartialNotesDialog.cshtml");
        }
        /// <summary>
        /// Adds Action in Journal 
        /// </summary>
        /// <param name="pObjActInfo"><see cref="AddActionInfo"/></param>
        /// <returns>JSON with data of ActionAdded, Action and ActionId</returns>
        public ActionResult AddAction(AddActionInfo pObjActInfo)
        {
            AddActionRequest request = ToActionRequest(pObjActInfo);
            CaseServices service = new CaseServices();
            bool added = service.AddActionInJournal(request);
            return Json(new { ActionAdded = added, Action = pObjActInfo.Action, ActionId = pObjActInfo.ActionId });
            //return added;
        }
        /// <summary>
        /// Converts <see cref="AddActionInfo"/> object into <see cref="AddActionRequest"/>
        /// </summary>
        /// <param name="pObjActInfo"><see cref="AddActionInfo"/></param>
        /// <returns><see cref="AddActionRequest"/></returns>
        private AddActionRequest ToActionRequest(AddActionInfo pObjActInfo)
        {
            DateTime? closedDate = null, followdate = null;
            if(pObjActInfo.ClosedDate !=null && pObjActInfo.ClosedDate != "")
            {
                if (pObjActInfo.ClosedDate[1] == '/' && pObjActInfo.ClosedDate[3] == '/')
                {
                    pObjActInfo.ClosedDate = "0" + pObjActInfo.ClosedDate.Substring(0, 2) + "0" + pObjActInfo.ClosedDate.Substring(2);
                }
                else if (pObjActInfo.ClosedDate[1] == '/')
                {
                    pObjActInfo.ClosedDate = "0" + pObjActInfo.ClosedDate;
                }
                else if (pObjActInfo.ClosedDate[4] == '/')
                {
                    pObjActInfo.ClosedDate = pObjActInfo.ClosedDate.Substring(0, 3) + "0" + pObjActInfo.ClosedDate.Substring(3);
                }
                closedDate = DateTime.ParseExact(pObjActInfo.ClosedDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var currenttime = DateTime.Now.TimeOfDay;
                closedDate = closedDate + currenttime;
            }
            if (pObjActInfo.FollowUpDate != null && pObjActInfo.FollowUpDate != "")
            {
                if (pObjActInfo.FollowUpDate[1] == '/' && pObjActInfo.FollowUpDate[3] == '/')
                {
                    pObjActInfo.FollowUpDate = "0" + pObjActInfo.FollowUpDate.Substring(0, 2) + "0" + pObjActInfo.FollowUpDate.Substring(2);
                }
                else if (pObjActInfo.FollowUpDate[1] == '/')
                {
                    pObjActInfo.FollowUpDate = "0" + pObjActInfo.FollowUpDate;
                }
                else if (pObjActInfo.FollowUpDate[4] == '/')
                {
                    pObjActInfo.FollowUpDate = pObjActInfo.FollowUpDate.Substring(0, 3) + "0" + pObjActInfo.FollowUpDate.Substring(3);
                }
                followdate = DateTime.ParseExact(pObjActInfo.FollowUpDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var currenttime = DateTime.Now.TimeOfDay;
                followdate = followdate + currenttime;
            }
            DateTime ReviewDate;
                       
            if (pObjActInfo.ReviewDate[1] == '/' && pObjActInfo.ReviewDate[3] == '/')
            {
                pObjActInfo.ReviewDate = "0" + pObjActInfo.ReviewDate.Substring(0, 2) + "0" + pObjActInfo.ReviewDate.Substring(2);
            }
            else if (pObjActInfo.ReviewDate[1] == '/')
            {
                pObjActInfo.ReviewDate = "0" + pObjActInfo.ReviewDate;
            }
            else if (pObjActInfo.ReviewDate[4] == '/')
            {
                pObjActInfo.ReviewDate = pObjActInfo.ReviewDate.Substring(0, 3) + "0" + pObjActInfo.ReviewDate.Substring(3);
            }
            ReviewDate = DateTime.ParseExact(pObjActInfo.ReviewDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            
            AddActionRequest request = new AddActionRequest()
            {
                ActionId = pObjActInfo.ActionId,
                Action = pObjActInfo.Action,
                ActionNotes = pObjActInfo.ActionNotes,
                CaseId = pObjActInfo.CaseId,
                ClosedDate = closedDate/*pObjActInfo.ClosedDate*/,
                FileName = null,
                FilePath = null,
                FollowupDate = followdate,
                ReviewDate= ReviewDate,
                RecordedBy = GetUserIdFromSession(),
                //RecordedDate = pObjActInfo.RecordedDate
                RecordedDate = DateTime.Now,
                Stage= pObjActInfo.Stage,
                SubCategory=pObjActInfo.SubCategory,
                Types=pObjActInfo.Types,
                SubType=pObjActInfo.SubType,
                ClosingType=pObjActInfo.selectedClosingType
            };
            return request;
        }
        #endregion

        #region Case Detail in New Window

        /// <summary>
        /// Opens case details in new tab
        /// </summary>
        /// <param name="caseId">CaseId</param>
        /// <returns>Complete View</returns>
        public ActionResult ViewCaseDetail(int caseId)
        {
            ViewBag.CaseId = caseId;
            return View("~/Views/Cases/NewWindow/CaseDetail.cshtml");
        }

        #endregion

        #region Warning Dialog

        /// <summary>
        /// Warning Dialog Action Method
        /// </summary>
        /// <param name="context">Context from where Warning Dialog is called</param>
        /// <returns>PartialView of Warning Dialog</returns>
        public ActionResult ShowWarningDialog(string context)
        {
            ViewBag.RemoveType = context;
            return PartialView("~/Views/Cases/_PartialWarningDialog.cshtml");
        }

        #endregion

        #region PDF

        public ActionResult PersonDetailPDF(List<CasePerson> personList)
        {
            List<Person> personsList = new List<Person>();
            foreach (var member in personList)
            {
                CommonServices service = new CommonServices();
                PersonRequest pr = new PersonRequest() { PersonId = member.PersonId, personType = member.PersonType };
                Person person = service.GetPerson(pr);
                if (person == null)
                {
                    person = new Person();
                    person.Id = -1;
                }
                if (person.Id != 0)
                {
                    person.AlternateContact = string.IsNullOrEmpty(person.AlternateContact) ? "N/A" : person.AlternateContact;
                }
                if (member.PersonRole == "Complainant")
                {
                    person.Role = PerosnRole.Complainant;
                }
                else
                {
                    person.Role = PerosnRole.Perpatrator;
                }
                personsList.Add(person);
            }
            return PartialView("~/Views/Cases/PDF/_PersonDetailPartialView.cshtml", personsList);
        }
        public ActionResult ExportPDF(int caseId)
        {

            //int userId = int.Parse(Request.QueryString["UserId"]);
            //Session["UserId"] = userId;
            bool isFullListRequired = true;
            AmendCaseViewModel acvm = MakeAmendCaseViewModel(caseId, isFullListRequired);

            ViewBag.IsNewCase = false;
            ViewBag.CaseId = caseId;
            ViewBag.EmpId = GetUserIdFromSession();
            //return View("~/Views/Cases/PDf/ExportPDF.cshtml", acvm);
            string fileName = "PDF Export of Case " + caseId + ".pdf";
            return new ViewAsPdf("~/Views/Cases/PDf/ExportPDF.cshtml", acvm)
            {
                FileName = fileName,
                //CustomSwitches = "--header-spacing \"2\" --header-font-size \"15\" --header-left \"Anti Social Behaviour Case\" --footer-spacing \"5\" --footer-font-size \"8\" --footer-left \"Anit Social Behaviour Case\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-left \"Anit Social Behaviour Case\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                //CustomSwitches = customSwitches,
                PageMargins = { Top = 15, Bottom = 20 },
                PageSize = Rotativa.Options.Size.A4
            };
        }

        public ActionResult GeneratePdf(int case_id)
        {
            var id = GetUserIdFromSession();
            //string customSwitches = string.Format("--print-media-type --allow {0} --header-html {0} --header-spacing -10",
            //    Url.Action("Header", "Cases", null, "https"));
            string fileName = "PDF Export of Case " + case_id + ".pdf";
            return new ActionAsPdf("ExportPDF", new { UserId = id, caseId = case_id })
            {
                FileName = fileName,
                CustomSwitches = "--header-spacing \"2\" --header-font-size \"15\" --header-left \"Anti Social Behaviour Case\" --footer-spacing \"5\" --footer-font-size \"8\" --footer-left \"Anit Social Behaviour Case\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                //CustomSwitches = customSwitches,
                PageMargins = { Top = 20, Bottom = 20 },
                PageSize = Rotativa.Options.Size.A4
            };
        }
        [AllowAnonymous]
        public ActionResult Footer()
        {
            return View("~/PDF/PdfFooter.cshtml");
        }

        [AllowAnonymous]
        public ActionResult Header()
        {
            return View("~/PDF/PdfHeader.cshtml");
        }

        #endregion 
    }
}