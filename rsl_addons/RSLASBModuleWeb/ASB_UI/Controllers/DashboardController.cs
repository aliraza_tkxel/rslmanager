﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASB_UI.ViewModels.Dashboard;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using ASB_UI.Models.WebAPILayer.BusinessObjects;
using Newtonsoft.Json;
using ASB_UI.Models.WebAPILayer.ServiceManagers;
using ASB_UI.ViewModels.Dashboard.SubViewModels;

namespace ASB_UI.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: ASBDashboard
        public ActionResult Dashboard(bool overdue, int catId = 0, int riskId = 0, int officerId = 0,
                            int pageSize = 25, int pageNumber = 1, bool highrisk = false, bool openOrClose=true, int ?personId=null, string personType=null)
        {

            ViewBag.CategoryId = catId;
            ViewBag.RiskLevelId = riskId;
            ViewBag.CaseOfficerId = officerId;

            DashboardViewModel ViewModel = new DashboardViewModel();
            CommonServices dropDownsService = new CommonServices();
            ViewModel.Categories = dropDownsService.GetCategories();
            ViewModel.RiskLevels = dropDownsService.GetRiskLevels();
            ViewModel.CaseOfficers = dropDownsService.GetCaseOfficers();

            DashboardRequest req = new DashboardRequest()
            {
                objPaginationBO = new Pagination() { PageSize = pageSize, PageNumber = pageNumber },
                objDashboardCaseFilterBO = new CaseFilters()
                {
                    caseOfficerId = officerId,
                    categoryId = catId,
                    riskLevelId = riskId,
                    IsOverDueCases = overdue,
                    IsHighRisk = highrisk,
                    openOrClose= openOrClose,
                    personId=personId,
                    personType=personType                  
                    //IsOpenCases = true
                }
            };

            DashboardResponse response = new DashboardService().GetOpenCases(req);

            req.objPaginationBO.PageSize = req.objPaginationBO.PageNumber = -1;
            req.objDashboardCaseFilterBO.openOrClose = true;
            req.objDashboardCaseFilterBO.caseOfficerId = 0;
            req.objDashboardCaseFilterBO.riskLevelId = 0;
            req.objDashboardCaseFilterBO.categoryId = 0;
            req.objDashboardCaseFilterBO.personId = null;
            req.objDashboardCaseFilterBO.personType = null;
            req.objDashboardCaseFilterBO.IsHighRisk = false;
            req.objDashboardCaseFilterBO.IsOverDueCases = false;
            DashboardResponse mapResponse = new DashboardService().GetOpenCases(req);
            ViewModel.Alerts = new DashboardAlert()
            {
                highRiskCasesCount = response.objDashboardAlertsBO.highRiskCasesCount,
                openCaseCount = response.objDashboardAlertsBO.openCaseCount,
                overdueCasesCount = response.objDashboardAlertsBO.overdueCasesCount,
                IsOverdue = overdue,
                IsHighRisk = highrisk
            };

            ViewModel.DashboardOpenCases = new DasboardCase()
            {
                Paging = new Pagination()
                {
                    PageNumber = response.objPaginationBO.PageNumber,
                    PageSize = response.objPaginationBO.PageSize,
                    TotalPages = response.objPaginationBO.TotalPages,
                    TotalRows = response.objPaginationBO.TotalRows,
                    FunctionName = "Dashboard"
                },
                Cases = response.objDashboardCaseInfoBO
            };
            ViewBag.DefDateFormat = DEFAULT_DATE_FORMAT;
            ViewBag.DefDateFormatForModel = DATE_FORMAT_FOR_MODEL;
            //Case[] caseslit = ViewModel.DashboardOpenCases.Cases.ToArray();
            Case[] caseslit = mapResponse.objDashboardCaseInfoBO.ToArray();
            string[] addresses = new string[caseslit.Length];
            string[] casetypes = new string[caseslit.Length];
            for (int i = 0; i < caseslit.Length; i++)
            {
                addresses[i] = caseslit[i].address + " "+ caseslit[i].postCode;
                casetypes[i] = caseslit[i].asbRiskLevel;
            }
            ViewBag.Addresses = addresses;
            ViewBag.Types = casetypes;
            return View(ViewModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterOn"></param>
        /// <returns></returns>
        public ActionResult Filter(string filterOn)
        {
            int filterId = String.IsNullOrEmpty(filterOn) ? 0 : Int32.Parse(filterOn);

            DashboardRequest req = new DashboardRequest()
            {
                objPaginationBO = new Pagination() { PageSize = 1, PageNumber = 1 },
                objDashboardCaseFilterBO = new CaseFilters() { caseOfficerId = 0, categoryId = filterId, riskLevelId = 0 }
            };
            DashboardResponse response = new DashboardService().GetOpenCases(req);
            DasboardCase newListOfCases = new DasboardCase()
            {
                Paging = new Pagination()
                {
                    PageNumber = response.objPaginationBO.PageNumber,
                    PageSize = response.objPaginationBO.PageSize,
                    TotalPages = response.objPaginationBO.TotalPages,
                    TotalRows = response.objPaginationBO.TotalRows
                },
                Cases = response.objDashboardCaseInfoBO
            };
            return PartialView("~/Views/Dashboard/_PartialOpenCases.cshtml", newListOfCases);
        }

        /// <summary>
        /// Action method when Serach Complainent/Preperator is performed from view.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchCompPrep(string input)
        {
            SearchPersonRequest request = new SearchPersonRequest()
            {
                input = input,
            };
            CommonServices service = new CommonServices();
            var data = service.SearchCompPrep(request);
            //var matches = service.SearchPerson(request);
            return Content(data, "application/json");
        }

        public ActionResult PageChange(int pageNumber, int pageSize)
        {
            DashboardRequest req = new DashboardRequest()
            {
                objPaginationBO = new Pagination() { PageSize = pageSize, PageNumber = pageNumber },
                objDashboardCaseFilterBO = new CaseFilters() { caseOfficerId = 0, categoryId = 0, riskLevelId = 0 }
            };

            DashboardResponse response = new DashboardService().GetOpenCases(req);
            DasboardCase newListOfCases = new DasboardCase()
            {
                Paging = new Pagination()
                {
                    PageNumber = response.objPaginationBO.PageNumber,
                    PageSize = response.objPaginationBO.PageSize,
                    TotalPages = response.objPaginationBO.TotalPages,
                    TotalRows = response.objPaginationBO.TotalRows
                },
                Cases = response.objDashboardCaseInfoBO
            };
            return PartialView("~/Views/Dashboard/_PartialOpenCases.cshtml", newListOfCases);
        }
    }
}