﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASB_UI.Controllers
{
    public class BridgeController : Controller
    {
        // GET: Bridge
        public ActionResult Index()
        {
            int userId = int.Parse(Request.QueryString["UserId"]);
            Session["UserId"] = userId;
            return RedirectToAction("Index","ASB");
        }
    }
}