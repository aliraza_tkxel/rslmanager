﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASB_UI.Controllers
{
    public class MenuController : BaseController
    {
        // GET: Menu
        public ActionResult GetTopNavigationMenu()
        {
            var incomingRequest = "~/../..";
            MenuRequest request = new MenuRequest();
            request.UserId = GetUserIdFromSession();
            CommonServices service = new CommonServices();
            MenuResponse menus =  service.GetTopNavigationMenu(request);
            menus.objMenusBO = AddHostUrlInMenuPath(incomingRequest, menus.objMenusBO);
            menus.objLstCustomerModuleMenus = AddHostUrlInMenuPath(incomingRequest, menus.objLstCustomerModuleMenus);
            return PartialView("~/Views/Shared/TopNavigation.cshtml", menus);
        }

        public ActionResult GetTopNavigationFromCustomerModule()
        {
            var incomingRequest = "../../../../";
            MenuRequest request = new MenuRequest();
            request.UserId = GetUserIdFromSession();
            CommonServices service = new CommonServices();
            MenuResponse menus = service.GetTopNavigationMenu(request);
            menus.objMenusBO = AddHostUrlInMenuPath(incomingRequest, menus.objMenusBO);
            menus.objLstCustomerModuleMenus = AddHostUrlInMenuPath(incomingRequest, menus.objLstCustomerModuleMenus);
            return PartialView("~/Views/Shared/TopNavigation.cshtml", menus);
        }


        /// <summary>
        /// Adds Host url in Menu Items path
        /// </summary>
        /// <param name="url">Host url</param>
        /// <param name="menuItems"><see cref="IEnumerable{MenuItem}"/></param>
        /// <returns></returns>
        private IEnumerable<MenuItem> AddHostUrlInMenuPath(string url, IEnumerable<MenuItem> menuItems)
        {
            string subMenu;
            foreach (var parent in menuItems)
            {
                foreach (var sub in parent.objLstSubMenus)
                {
                    subMenu = sub.SubMenuName;
                    if (string.Compare(subMenu,ApplicationConstants.AsbMenu,true)==0)
                    {
                        sub.Path += GetUserIdFromSession();
                    }
                }
                                 
                parent.objLstSubMenus.Select(c => { c.Path = url + "/" + c.Path; return c; }).ToList();                              
            }
            return menuItems;
        }
        /// <summary>
        /// Adds Host url in Menu Items path
        /// </summary>
        /// <param name="url">Host url</param>
        /// <param name="menuItems"><see cref="IEnumerable{ModuleItem}"/></param>
        /// <returns></returns>
        private IEnumerable<ModuleItem> AddHostUrlInMenuPath(string url, IEnumerable<ModuleItem> menuItems)
        {
            menuItems.Select(m => { m.Url = url + m.Url; return m; }).ToList();
            return menuItems;
        }
    }
}