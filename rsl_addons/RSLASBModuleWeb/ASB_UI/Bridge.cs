﻿using ASB_UI.Models.WebAPILayer.ServiceManagers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ASB_UI
{
    public class Bridge : MSDN.SessionPage
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Bridge
        int classicUserId = 0;
        public bool LoadUser(int userId)
        {
            bool isActive = false;
            classicUserId = userId;
            //checkClassicAspSession(userId);
            SessionService objSessionService = new SessionService();
            SessionResponse objSessionResponse = objSessionService.CreateSession(classicUserId);
            if (objSessionResponse.IsActive == 0)
            {
                isActive = false;//error msg
            }
            else
            {
                isActive = true;
            }
            return isActive;

        }

        #region "Check Classic Asp Session"
        //private void checkClassicAspSession(int userId)
        //{
           
        //     if (ASPSession["USERID"] != null)
        //     {
        //        classicUserId = int.Parse(ASPSession["USERID"].ToString());
        //     }
        //     else
        //    {
        //       redirectToLoginPage();
        //    }
        //}
        #endregion

        #region "Redirect To Login Page"
        public void redirectToLoginPage()
        {
            HttpContext.Current.Response.Redirect(ApplicationConstants.LoginPath, true);
        }
        #endregion


    }


}