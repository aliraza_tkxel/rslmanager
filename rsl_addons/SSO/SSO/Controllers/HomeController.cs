﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSO.Models;

namespace SSO.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Console.WriteLine("ActionResult Index()");
            string  URLLink = ConfigurationManager.AppSettings["ida:LoginRedirectUrl"];
            Console.WriteLine("ActionResult Index() URLLink {0}", URLLink);
            string redirectLink = "";
            try
            {
                var model = new DomainViewModel();
                Guid newSecureKey = Guid.NewGuid();
                model.Id = User.Identity.Name;
                model.SecureKey = newSecureKey.ToString();
                
                var domainModel = new DomainModel();

                domainModel.CreateSecureKey(model);

                redirectLink = URLLink + newSecureKey;
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
                redirectLink = ""; 
            }
             
            return Redirect(redirectLink);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}