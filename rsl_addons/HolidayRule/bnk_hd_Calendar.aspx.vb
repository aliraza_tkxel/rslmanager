Imports dsCalendar
Imports dsCalendarTableAdapters
Partial Class bnk_hd_Calendar
    Inherits System.Web.UI.Page
    Dim taBnkHd As New G_BANKHOLIDAYSTableAdapter
    Dim dsBnkHd As New dsCalendar

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            If ddlTeam.SelectedItem.Value = 1 Then
                taBnkHd.FillCalendar(dsBnkHd.G_BANKHOLIDAYS)
            Else
                taBnkHd.FillCalendarMe(dsBnkHd.G_BANKHOLIDAYS)
            End If
        Else
            taBnkHd.FillCalendar(dsBnkHd.G_BANKHOLIDAYS)
        End If

    End Sub

    Protected Sub cal_BnkHd_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles cal_BnkHd.DayRender
        Dim dtEvent As DateTime
        For Each dataR As Data.DataRow In dsBnkHd.G_BANKHOLIDAYS.Rows
            dtEvent = dataR("BHDATE").ToString
            If dtEvent.Equals(e.Day.Date) Then
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        Next
    End Sub

    Protected Sub cal_BnkHd_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cal_BnkHd.SelectionChanged
        txtCal.Text = cal_BnkHd.SelectedDate

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim ds As New dsCalendar
        Dim iBha As Integer = 0
        Dim iME As Integer = 0
        Dim k As Integer = 0
        Dim sFlag As String = "Insert"
        Dim drow As Data.DataRow = ds.G_BANKHOLIDAYS.NewRow
        ' check whether this date exists in the table or not?
        Dim recCount As Integer = taBnkHd.GetRecCount(CType(txtCal.Text, DateTime))

        ' for BHA ataff
        taBnkHd.FillCalendarAll(ds.G_BANKHOLIDAYS)

        If recCount > 0 Then
            For Each dr As Data.DataRow In ds.G_BANKHOLIDAYS.Rows
                If (dr("BHDATE") = CType(txtCal.Text, DateTime)) Then ' means date is already in the table
                    sFlag = ""
                    If ddlTeam.SelectedItem.Value = 1 Then
                        dr("BHA") = 1
                        dr("MeridianEast") = 1
                    End If
                    If ddlTeam.SelectedItem.Value = 2 Then dr("MeridianEast") = 1
                End If
                k = k + 1
            Next
        Else
            drow("BHDATE") = CType(txtCal.Text, DateTime)

            If ddlTeam.SelectedItem.Value = 1 Then
                drow("BHA") = 1
                drow("MeridianEast") = 1
            ElseIf ddlTeam.SelectedItem.Value = 2 Then
                drow("BHA") = 0
                drow("MeridianEast") = 1
            End If

            ds.G_BANKHOLIDAYS.Rows.Add(drow)
        End If

        taBnkHd.Update(ds)

        ' Show the changes on the Calendar
        If ddlTeam.SelectedItem.Value = 1 Then
            taBnkHd.FillCalendar(ds.G_BANKHOLIDAYS)
        Else
            taBnkHd.FillCalendarMe(ds.G_BANKHOLIDAYS)
        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim ds As New dsCalendar

        Dim i As Integer
        ' Loop though the table 
        i = 0
        taBnkHd.FillCalendarAll(ds.G_BANKHOLIDAYS)

        'For Each dr As Data.DataRow In ds.G_BANKHOLIDAYS.Rows
        '    If (dr("BHDATE") = CType(txtCal.Text, DateTime)) Then
        '        ds.G_BANKHOLIDAYS.Rows(i).Delete()
        '    End If
        '    i = i + 1
        'Next

        '''''
        ' check whether this date exists in the table or not?
        Dim recCount As Integer = taBnkHd.GetRecCount(CType(txtCal.Text, DateTime))


        If recCount > 0 Then
            For Each dr As Data.DataRow In ds.G_BANKHOLIDAYS.Rows
                If (dr("BHDATE") = CType(txtCal.Text, DateTime)) Then
                    If ddlTeam.SelectedItem.Value = 1 Then
                        dr("BHA") = 0
                        dr("MeridianEast") = 0
                    End If
                    If ddlTeam.SelectedItem.Value = 2 Then dr("MeridianEast") = 0
                End If
                i = i + 1
            Next
        End If

        'ds.G_BANKHOLIDAYS.AcceptChanges()
        taBnkHd.Update(ds)

        ' Show the changes on the Calendar
        If ddlTeam.SelectedItem.Value = 1 Then
            taBnkHd.FillCalendar(ds.G_BANKHOLIDAYS)
        Else
            taBnkHd.FillCalendarMe(ds.G_BANKHOLIDAYS)
        End If
    End Sub

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged
        If ddlTeam.SelectedItem.Value = 1 Then
            taBnkHd.FillCalendar(dsBnkHd.G_BANKHOLIDAYS)
        Else
            taBnkHd.FillCalendarMe(dsBnkHd.G_BANKHOLIDAYS)
        End If

    End Sub
End Class
