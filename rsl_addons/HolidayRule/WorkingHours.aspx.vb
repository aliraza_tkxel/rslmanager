Imports dsWorkingHours
Imports dsWorkingHoursTableAdapters
Imports System.Data.SqlClient
Partial Class WorkingHours
    Inherits System.Web.UI.Page
    Dim empId As Integer
    Dim UserId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call displayHours()
    End Sub

    Public Sub displayHours()
        gvHours.Visible = False

        Dim holidayRule As Integer = 0
        Dim partFullTime As Integer = 0
        Dim startdate As String
        empId = 1
        UserId = 1
        empId = Request.QueryString("EMPLOYEEID")
        UserId = Request.QueryString("USERID")
        startDate = Request.QueryString("STARTDATE")

        Dim dSet As New dsWorkingHours
        Dim tAdapter As New E_WorkingHoursTableAdapter
        Dim dRow As Data.DataRow = dSet.E_WorkingHours.NewRow

   
        If Request.QueryString("holidayrule") <> "" And Request.QueryString("partFull") <> "" Then

            holidayRule = Request.QueryString("holidayrule")
            partFullTime = Request.QueryString("partFull")
            lblWarning.Text = ""
            ' it means there is no record in DB against this Employee
            If gvHours.Rows.Count = 0 Then

                If partFullTime = 1 Then ' for full time staff

                    Select Case holidayRule
                        ' For Meridian East Holiday Rule
                        'Case 6, 7, 8, 9
                        '    dRow("EMPLOYEEID") = empId
                        '    dRow("MON") = 7.5
                        '    dRow("TUE") = 7.5
                        '    dRow("WED") = 7.5
                        '    dRow("THU") = 7.5
                        '    dRow("FRI") = 7.5
                        '    dRow("SAT") = 0
                        '    dRow("SUN") = 0
                        '    dRow("TOTAL") = 37.5
                        '    dRow("STARTDATE") = startdate
                        '    Session("total") = 37.5
                        '    Session("totalDays") = 5

                        ' For BH Holiday Rule
                        Case 1, 2, 3, 4, 5, 6, 7, 8, 9
                            dRow("EMPLOYEEID") = empId
                            dRow("MON") = 7.5
                            dRow("TUE") = 7.5
                            dRow("WED") = 7.5
                            dRow("THU") = 7.5
                            dRow("FRI") = 7.0
                            dRow("SAT") = 0
                            dRow("SUN") = 0
                            dRow("TOTAL") = 37
                            dRow("STARTDATE") = startdate
                            dRow("CREATEDBY") = UserId
                            dRow("LASTACTIONUSER") = UserId
                            Session("total") = 37
                            Session("totalDays") = 5
                        Case Else
                            dRow("EMPLOYEEID") = empId
                            dRow("MON") = 0
                            dRow("TUE") = 0
                            dRow("WED") = 0
                            dRow("THU") = 0
                            dRow("FRI") = 0
                            dRow("SAT") = 0
                            dRow("SUN") = 0
                            dRow("TOTAL") = 0
                            dRow("STARTDATE") = startdate
                            dRow("CREATEDBY") = UserId
                            dRow("LASTACTIONUSER") = UserId
                            Session("total") = 0
                            Session("totalDays") = 0

                    End Select
                ElseIf partFullTime = 2 Then
                    dRow("EMPLOYEEID") = empId
                    dRow("MON") = 0
                    dRow("TUE") = 0
                    dRow("WED") = 0
                    dRow("THU") = 0
                    dRow("FRI") = 0
                    dRow("SAT") = 0
                    dRow("SUN") = 0
                    dRow("TOTAL") = 0
                    dRow("CREATEDBY") = UserId
                    dRow("LASTACTIONUSER") = UserId
                    dRow("STARTDATE") = startdate
                    Session("total") = 0
                    Session("totalDays") = 0

                End If

                dSet.E_WorkingHours.Rows.Add(dRow)
                tAdapter.Update(dSet)
                gvHours.Visible = True
                gvHours.DataBind()

            Else

                ' get the record from the E_WorkingHours Table
                tAdapter.FillWorkingHours(dSet.E_WorkingHours, empId)
                For Each dr As Data.DataRow In dSet.E_WorkingHours.Rows
                    Session("total") = dr("Total")
                Next
                Session("totalDays") = tAdapter.E_WORK_DAYS_IN_WEEK(empId)
                gvHours.Visible = True

            End If
        Else
            lblWarning.Text = "Please close this window and select some options from the <b>Holiday Rule</b> and <b>Full/Part Time</b> drop downs"
            Session("total") = 0
        End If
    End Sub

    Protected Sub gvHours_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHours.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            'e.Row.Attributes.Add("Title", CType(e.Row.FindControl("hdCreatedOn"), HiddenField).Value)
            e.Row.Cells(10).Attributes.Add("Title", "Created By: " + CType(e.Row.FindControl("hdCreatedBy"), HiddenField).Value & vbCrLf & "Date: " + CType(e.Row.FindControl("hdCreatedOn"), HiddenField).Value)
        End If
    End Sub

    Protected Sub gvHours_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles gvHours.RowUpdated

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings(1).ConnectionString)
        conn.Open()
        ' calling a function
        Dim queryString As String = "SELECT dbo.E_WORK_DAYS_IN_WEEK(" & empId & ")"
        Dim command = New SqlCommand(queryString, conn)
        Session("totalDays") = Convert.ToString(command.ExecuteScalar())
        conn.Close()

        gvHoursHistory.DataBind()

        Dim script As String
        script = "<script type=""text/javascript"" language=""javascript""> RefreshParent(); </script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "closepop", script)

    End Sub

    Protected Sub gvHours_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvHours.RowUpdating

        Dim i As Integer
        Dim totalDays As Integer = 0
        Dim total As Decimal = 0
        'Loop through all the textboxes and add them up
        For i = 2 To 8 'gvHours.Rows(e.RowIndex).Cells.Count - 4
            total = total + CType(CType(CType(gvHours.Rows(e.RowIndex).Cells(i).Controls(1), TextBox).Text, String), Decimal)
            'If CType(CType(CType(gvHours.Rows(e.RowIndex).Cells(i).Controls(1), TextBox).Text, String), Decimal) > 0 Then
            'totalDays = totalDays + 1
            'End If
        Next
        Session("total") = total

    End Sub

    Protected Sub gvHoursHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHoursHistory.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            e.Row.Cells(10).Attributes.Add("Title", "Created By: " + CType(e.Row.FindControl("hdCreatedBy_history"), HiddenField).Value & vbCrLf & "Date: " + CType(e.Row.FindControl("hdCreatedOn_history"), HiddenField).Value)
        End If
    End Sub
End Class
