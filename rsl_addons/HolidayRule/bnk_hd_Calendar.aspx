<%@ Page Language="VB" AutoEventWireup="false" CodeFile="bnk_hd_Calendar.aspx.vb" Inherits="bnk_hd_Calendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bank Holiday Calendar</title>
        <link href="css/WorkingHour.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="height: 18px" class="caption">Team: </td>
                <td style="height: 18px">
                     <asp:DropDownList ID="ddlTeam" runat="server" AutoPostBack="true" CssClass="textbox150">
                        <asp:ListItem Value="1">BHA</asp:ListItem>
                        <asp:ListItem Value="2">Meridian East</asp:ListItem>
                    </asp:DropDownList><br />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 18px"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Calendar ID="cal_BnkHd" runat="server" BackColor="White" BorderColor="Black"
                        BorderStyle="Solid" CellSpacing="1" Font-Names="Arial" Font-Size="9pt" ForeColor="Black"
                        Height="250px" NextPrevFormat="ShortMonth" Width="330px">
                        <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                        <TodayDayStyle BackColor="#999999" ForeColor="White" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <DayStyle BackColor="#CCCCCC" />
                        <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                        <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
                        <TitleStyle BackColor="#333399" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt"
                            ForeColor="White" Height="12pt" />
                    </asp:Calendar>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 18px"></td>
            </tr>
            <tr>
                <td class="caption"> Date: </td>
                <td>
                    <asp:TextBox ID="txtCal" runat="server" CssClass="textbox150" ReadOnly="true" Width="100px"></asp:TextBox>&nbsp; 
                    <asp:RequiredFieldValidator ID="rfvDate" runat="server" Text="*" ControlToValidate="txtCal"></asp:RequiredFieldValidator>&nbsp;
                      <asp:Button ID="btnAdd" runat="server" Text="Add" />&nbsp; &nbsp; &nbsp;
                      <asp:Button ID="btnDelete" runat="server" Text="Delete" />  
                      
                </td>    
            </tr>
           
                        
    </table>
    </div>
    </form>
</body>
</html>
