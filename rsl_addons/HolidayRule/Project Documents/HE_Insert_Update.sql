
Insert into E_Holidayrule values('Officer',22,27)
Insert into E_Holidayrule values('Directorate Manager',25,30)
Insert into E_Holidayrule values('Senior Manager',27,32)
Insert into E_Holidayrule values('Executive Director',30,32)
Insert into E_Holidayrule values('Chief Executive',32,37)
Insert into E_Holidayrule values('Meridian East (Band A)',23,null)
Insert into E_Holidayrule values('Meridian East (Band B)',24,null)
Insert into E_Holidayrule values('Meridian East (Band C)',25,null)
Insert into E_Holidayrule values('Meridian East (Band D)',26,null)


----------------------------------------------------------------------------------------------------
-- COPY ALL FULLTIME TEMPS TO FULLTIME AND PARTIME TEMPS TO PART TIME
BEGIN TRAN
Delete From E_Partfulltime Where  Partfulltimeid =4
update e_partfulltime set description='temp' where partfulltimeid =3
ROLLBACK
----------------------------------------------------------------------------------------------------


INSERT INTO E_HOLIDAYENTITLEMENT_INDICATOR VALUES ('Days')
INSERT INTO E_HOLIDAYENTITLEMENT_INDICATOR VALUES ('Hours')
----------------------------------------------------------------------------------------------------


ALTER TABLE E_JOBDETAILS ADD HOLIDAYRULE INT
ALTER TABLE E_JOBDETAILS ADD FTE FLOAT
ALTER TABLE E_JOBDETAILS ADD HolidayIndicator INT
ALTER TABLE E_JOBDETAILS ADD CARRYFORWARD float
ALTER TABLE E_JOBDETAILS ADD BANKHOLIDAY FLOAT
ALTER TABLE E_JOBDETAILS ADD TOTALLEAVE FLOAT
----------------------------------------------------------------------------------------------------
-- also reduce size of reason and notes to 1000
ALTER TABLE E_ABSENCE ADD DURATION_HRS FLOAT

ALTER TABLE E_ABSENCE MODIFY REASDON NVARCHAR(1000)
ALTER TABLE E_ABSENCE MODIFY NOTES NVARCHAR(1000)
----------------------------------------------------------------------------------------------------

-- set bhdate of G_BANKHOLIDAYS as PK
ALTER TABLE G_BANKHOLIDAYS ADD BHA SMALLINT
ALTER TABLE G_BANKHOLIDAYS ADD MeridianEast SMALLINT

----------------------------------------------------------------------------------------------------
INSERT INTO AC_PAGES VALUES ('Bank Holiday Management',2,1,1,'BnkHd_Management.asp',NULL)
INSERT INTO AC_NOACCESS VALUES (343,3,246,0)
----------------------------------------------------------------------------------------------------
UPDATE G_BANKHOLIDAYS SET BHA=1
----------------------------------------------------------------------------------------------------



