-- CREATE NEW VIEWS, PROCEDURES AND FUNCTIONS
-- VIEWS
-- create view vwCurrentDate

----------------------------------------------------------------------------------------------------
-- FUNCTIONS
FUNCTION [dbo].[E_BANK_HDAY_ADJUSTMENT]
FUNCTION [dbo].[EMPLOYEE_ANNUAL_START_END_DATE]
FUNCTION [DBO].[E_CALCULATE_CARRY_FWD]
FUNCTION [DBO].ROUND_TO_HALF
FUNCTION [dbo].[E_WORK_DAYS_IN_WEEK]
----------------------------------------------------------------------------------------------------
-- PROCS
proc E_HOLIDAY_ADJUSTMENT
PROCEDURE [dbo].[E_BOOK_ANNUAL_LEAVE_HRS]
PROCEDURE [dbo].[E_HOLIDAY_DETAIL]
PROCEDURE [dbo].[E_CARRY_FWD_ADJUSTMENT]
----------------------------------------------------------------------------------------------------

