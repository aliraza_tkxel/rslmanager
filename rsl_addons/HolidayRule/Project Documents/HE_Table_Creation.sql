USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[E_WorkingHours]    Script Date: 08/13/2009 17:09:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[E_WorkingHours](
	[Wid] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Mon] [float] NULL CONSTRAINT [DF_E_WorkingHours_Mon]  DEFAULT ((0.0)),
	[Tue] [float] NULL CONSTRAINT [DF_E_WorkingHours_Tue]  DEFAULT ((0.0)),
	[Wed] [float] NULL CONSTRAINT [DF_E_WorkingHours_Wed]  DEFAULT ((0.0)),
	[Thu] [float] NULL CONSTRAINT [DF_E_WorkingHours_Thu]  DEFAULT ((0.0)),
	[Fri] [float] NULL CONSTRAINT [DF_E_WorkingHours_Fri]  DEFAULT ((0.0)),
	[Sat] [float] NULL CONSTRAINT [DF_E_WorkingHours_Sat]  DEFAULT ((0.0)),
	[Sun] [float] NULL CONSTRAINT [DF_E_WorkingHours_Sun]  DEFAULT ((0.0)),
	[Total] [float] NULL CONSTRAINT [DF_E_WorkingHours_Total]  DEFAULT ((0.0)),
 CONSTRAINT [PK_E_WorkingHours] PRIMARY KEY CLUSTERED 
(
	[Wid] ASC
)
) ON [PRIMARY]

GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


ALTER TABLE dbo.E_WorkingHours ADD STARTDATE SMALLDATETIME
GO
ALTER TABLE dbo.E_WorkingHours ADD CREATEDBY INT
GO
ALTER TABLE dbo.E_WorkingHours ADD CREATEDON SMALLDATETIME DEFAULT GETDATE()

GO
-- Created by:Adnan
-- Date:14-07-2009
-- Reason: To Update the Total column
--/--------------------------------------------------------------------------------------------/
CREATE  TRIGGER [dbo].[WORKINGHOURS_UPDATE] ON [dbo].[E_WorkingHours]
FOR UPDATE
AS

/* THIS TRIGGER WILL RUN ON THE MODIFICATION OF HOURS ON DAYS I.E MON,TUE,WED,THU,FRI,SAT & SUN
 */

BEGIN

	UPDATE E_WORKINGHOURS SET TOTAL = I.MON + I.TUE + I.WED + I.THU + I.FRI + I.SAT + I.SUN 
	FROM E_WORKINGHOURS
	INNER JOIN INSERTED I ON I.WID=E_WORKINGHOURS.WID
	WHERE (SUBSTRING(COLUMNS_UPDATED(),1,1)) >= 8 OR  (SUBSTRING(COLUMNS_UPDATED(),2,1) BETWEEN 1 AND 3)

END


GO

USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[E_HOLIDAYRULE]    Script Date: 08/13/2009 17:12:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[E_HOLIDAYRULE](
	[EID] [int] IDENTITY(1,1) NOT NULL,
	[HolidayRule] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Holiday_Days] [float] NULL,
	[Max_Holiday_Days] [float] NULL,
 CONSTRAINT [PK_E_HOLIDAYRULE] PRIMARY KEY CLUSTERED 
(
	[EID] ASC
)
) ON [PRIMARY]

GO

CREATE TABLE dbo.E_HOLIDAYENTITLEMENT_INDICATOR
(
Sid int identity(1,1),
Indicator nvarchar(10)
)

GO

USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_WorkingHours_History]    Script Date: 06/02/2011 10:53:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[E_WorkingHours_History](
	[Wid] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[Mon] [float] NULL,
	[Tue] [float] NULL,
	[Wed] [float] NULL,
	[Thu] [float] NULL,
	[Fri] [float] NULL,
	[Sat] [float] NULL,
	[Sun] [float] NULL,
	[Total] [float] NULL,
	[CreatedBy] [int] NULL,
	[ctimestamp] [smalldatetime] NULL,
 CONSTRAINT [PK_E_WorkingHours_History] PRIMARY KEY CLUSTERED 
(
	[Wid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Mon]  DEFAULT ((0.0)) FOR [Mon]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Tue]  DEFAULT ((0.0)) FOR [Tue]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Wed]  DEFAULT ((0.0)) FOR [Wed]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Thu]  DEFAULT ((0.0)) FOR [Thu]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Fri]  DEFAULT ((0.0)) FOR [Fri]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Sat]  DEFAULT ((0.0)) FOR [Sat]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Sun]  DEFAULT ((0.0)) FOR [Sun]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  CONSTRAINT [DF_E_WorkingHours_History_Total]  DEFAULT ((0.0)) FOR [Total]
GO

ALTER TABLE [dbo].[E_WorkingHours_History] ADD  DEFAULT (getdate()) FOR [ctimestamp]
GO


