<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WorkingHours.aspx.vb" Inherits="WorkingHours" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">

    <link href="css/WorkingHour.css" type="text/css" rel="stylesheet" />

    <title>Working Hours of Employee</title>
    
    
  
</head>
<body onunload='RefreshParent();'>
<script language="javascript" type="text/javascript">

    function closeWindow() {
        window.close();
    }

    function RefreshParent() {
        // if(!window.opener.location)

        var holidayRule = window.opener.document.RSLFORM.sel_HOLIDAYRULE[window.opener.document.RSLFORM.sel_HOLIDAYRULE.selectedIndex].value;
        var fulltime = window.opener.document.RSLFORM.sel_PARTFULLTIME[window.opener.document.RSLFORM.sel_PARTFULLTIME.selectedIndex].value;
        var FTE;
        var totalHours = '<%=Session("total")%>';
        var totalDays = '<%=Session("totalDays")%>';
        if (totalHours != 0) {
            switch (holidayRule) {
//                case '6':
//                    FTE = Math.round((totalHours / 37.5 * 100)) / 100;
//                    break;

//                case '7':
//                    FTE = Math.round((totalHours / 37.5 * 100)) / 100;
//                    break;

//                case '8':
//                    FTE = Math.round((totalHours / 37.5 * 100)) / 100;
//                    break;

//                case '9':
//                    FTE = Math.round((totalHours / 37.5 * 100)) / 100;
//                    break;

                // BRS - MEARS RULE
                case '13':
                    FTE = 0;
                    break;

                default:
                    FTE = Math.round((totalHours / 37 * 100)) / 100;
            }
            window.opener.document.RSLFORM.txt_HOURS.value = totalHours;
            window.opener.document.RSLFORM.txt_FTE.value = FTE;
            window.opener.document.RSLFORM.txt_TOTALWORKDAYS.value = totalDays;
            window.opener.calculateHoliday();
        }

    }

    </script>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblWarning" runat="server" CssClass="caption" ForeColor="Red"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lblNewPattern" runat="server" CssClass="caption" ForeColor="black" Text="Enter New Working Patterns" Font-Bold="true"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
            
                <asp:GridView ID="gvHours" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlDataSource1" DataKeyNames="Wid,Total" ForeColor="" 
                    CssClass="caption" BorderColor="Black" CellPadding="4" HeaderStyle-BackColor="Control">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Wid" HeaderText="Wid" InsertVisible="False" ReadOnly="True" SortExpression="Wid" Visible="False" />
                        
                        <asp:TemplateField HeaderText="Start" SortExpression="StartDate">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtStartDate" runat="server" Text='<%# Bind("StartDate") %>' Width="70px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Format="dd/MM/yyyy" TargetControlID="txtStartDate">
                                </asp:CalendarExtender>
                                <asp:RegularExpressionValidator ID="retxtStartDate" runat="server" ErrorMessage="Incorrect Start Date format" Text="*" ControlToValidate="txtStartDate" ValidationExpression="(0?[1-9]|[12][0-9]|3[01])[//.](0?[1-9]|1[012])[//.](19|20)\d\d"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rvtxtStartDate" runat="server" ErrorMessage="Please Input Start Date" Text="*" ControlToValidate="txtStartDate"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvtxtStartDate" runat="server" Text="*" ErrorMessage="Please Input date greater than the employee start date" ControlToValidate="txtStartDate" Type="Date" Operator="GreaterThanEqual" ValueToCompare='<%#Request.QueryString("STARTDATE") %>'></asp:CompareValidator>

                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("StartDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Monday" SortExpression="Mon">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMon" runat="server" Text='<%# Bind("Mon") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvMon" runat="server" ErrorMessage="Please enter value between 0 and 24" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtMon" Text="*" Type="Double" ></asp:RangeValidator>
                               <asp:RequiredFieldValidator ID="rfvMon" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtMon" Text="*"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Mon") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tuesday" SortExpression="Tue" >
                            <EditItemTemplate >
                                <asp:TextBox ID="txtTue" runat="server" Text='<%# Bind("Tue") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvTue" runat="server" text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtTue" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvTue" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtTue" Text="*"></asp:RequiredFieldValidator>                        
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Tue") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wednesday" SortExpression="Wed">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtWed" runat="server" Text='<%# Bind("Wed") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvWed" runat="server" Text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtWed" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvWed" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtWed" Text="*"></asp:RequiredFieldValidator>                        
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Wed") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Thursday" SortExpression="Thu">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtThu" runat="server" Text='<%# Bind("Thu") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvThu" runat="server" text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtThu" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvThu" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtThu" Text="*"></asp:RequiredFieldValidator>                                   
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Thu") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Friday" SortExpression="Fri">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFri" runat="server" Text='<%# Bind("Fri") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvFri" runat="server" text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtFri" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                             <asp:RequiredFieldValidator ID="rfvFri" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtFri" Text="*"></asp:RequiredFieldValidator>                                          
                             </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Fri") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Saturday" SortExpression="Sat">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSat" runat="server" Text='<%# Bind("Sat") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvSat" runat="server" Text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtSat" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                               <asp:RequiredFieldValidator ID="rfvSat" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtSat" Text="*"></asp:RequiredFieldValidator>                                          
                               
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Sat") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sunday" SortExpression="Sun">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSun" runat="server" Text='<%# Bind("Sun") %>' Width="35px"></asp:TextBox>
                                <asp:RangeValidator ID="rvSun" runat="server" text="*" MaximumValue="24"
                                    MinimumValue="0" ControlToValidate="txtSun" Type="Double" ErrorMessage="Please enter value between 0 and 24"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvSun" runat="server" ErrorMessage="Please enter value between 0 and 24" ControlToValidate="txtSun" Text="*"></asp:RequiredFieldValidator>                                     
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("Sun") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />

                        <asp:TemplateField HeaderText="Created By">
                            <ItemTemplate>
                                <asp:image runat="server" ID="imgInfo" ImageUrl="~/images/info-red.gif"/>
                                <asp:HiddenField ID="hdGroupNumber" Value='<%# Bind("GroupNumber") %>' runat="server"   />
                                <asp:HiddenField ID="hdCreatedBy" Value='<%# Bind("CreatedBy") %>' runat="server"   />
                                <asp:HiddenField ID="hdCreatedOn" Value='<%# Bind("CreatedOn") %>' runat="server"   />                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" ButtonType="Button" />

                    </Columns>
                    <EditRowStyle BackColor="#FFFFC0" BorderColor="Red" HorizontalAlign="Center"  />
                </asp:GridView>
            
            </td>
        </tr>
        <tr>
            <td><br /></td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lblPreviousPattern" runat="server" CssClass="caption" ForeColor="black" Text="Previous Working Patterns" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:GridView ID="gvHoursHistory" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceWorkHistory" ForeColor="" CssClass="caption" BorderColor="Black" CellPadding="4" HeaderStyle-BackColor="Control">
                    <Columns>
                        <asp:BoundField DataField="START" HeaderText="Start" ReadOnly="True" 
                            SortExpression="START" >
                        
                        </asp:BoundField>
                        <asp:BoundField DataField="END" HeaderText="End" ReadOnly="True" SortExpression="END" />
                        <asp:BoundField DataField="MON" HeaderText="Monday" SortExpression="MON" />
                        <asp:BoundField DataField="TUE" HeaderText="Tuseday" SortExpression="TUE" />
                        <asp:BoundField DataField="WED" HeaderText="Wednesday" SortExpression="WED" />
                        <asp:BoundField DataField="THU" HeaderText="Thursday" SortExpression="THU" />
                        <asp:BoundField DataField="FRI" HeaderText="Friday" SortExpression="FRI" />
                        <asp:BoundField DataField="SAT" HeaderText="Saturday" SortExpression="SAT" />
                        <asp:BoundField DataField="SUN" HeaderText="Sunday" SortExpression="SUN" />
                        <asp:BoundField DataField="TOTAL" HeaderText="Total" SortExpression="TOTAL" />
                        <asp:TemplateField HeaderText="Created By">
                            <ItemTemplate>
                                <asp:image runat="server" ID="imgInfo" ImageUrl="~/images/info-red.gif"/>
                                <asp:HiddenField ID="hdGroupNumber_history" Value='<%# Bind("GroupNumber") %>' runat="server"   />
                                <asp:HiddenField ID="hdCreatedBy_history" Value='<%# Bind("CreatedBy") %>' runat="server"   />
                                <asp:HiddenField ID="hdCreatedOn_history" Value='<%# Bind("CreatedOn") %>' runat="server"   />
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceWorkHistory" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:RSLBHALiveConnectionString %>" SelectCommand="SELECT CONVERT(NVARCHAR(10),startdate,103) AS START,CONVERT(NVARCHAR(10),ENDDATE,103) AS [END], MON,TUE,WED,THU,FRI,SAT,SUN,TOTAL,ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'') as CREATEDBY,CTIMESTAMP AS CREATEDON, GroupNumber
                                        FROM E_WORKINGHOURS_HISTORY WH
                                        LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=WH.CREATEDBY
                                        WHERE WH.EMPLOYEEID=@EMPLOYEEID
                                        AND WH.TOTAL!=0
                                        ORDER BY WID DESC
                                        ">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="EMPLOYEEID" QueryStringField="EMPLOYEEID" />
                        <asp:QueryStringParameter Name="UserId" QueryStringField="USERID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
         <tr>
            <td align="center">
                <input id="btnClose" type="button" value="Close Window" onclick='closeWindow();' />
            </td>
        </tr> 
          
         </table>
         
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RSLBHALiveConnectionString %>"
            SelectCommand="SELECT [Wid],CONVERT(NVARCHAR(10),STARTDATE,103) as STARTDATE,[Mon], [Tue], [Wed], [Thu], [Fri], [Sat], [Sun], [Total],ISNULL(E.FIRSTNAME,'') + ' ' +  ISNULL(E.LASTNAME,'') AS CreatedBy, CreatedOn, GroupNumber
                            FROM [E_WORKINGHOURS] W
                            LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=ISNULL(W.CREATEDBY,@USERID)
                             WHERE (W.EMPLOYEEID = @EMPLOYEEID)"
            UpdateCommand="UPDATE E_WORKINGHOURS SET MON=@MON,TUE=@TUE,WED=@WED,THU=@THU,FRI=@FRI,SAT=@SAT,SUN=@SUN,STARTDATE=@STARTDATE,CREATEDBY=@USERID,CREATEDON=GETDATE(), LASTACTIONUSER=@UpdateUserId, LASTACTIONTIME=GETDATE(),GroupNumber=@GroupNumber
OUTPUT INSERTED.EMPLOYEEID,DELETED.STARTDATE,INSERTED.STARTDATE-1,DELETED.MON,DELETED.TUE,DELETED.WED,DELETED.THU,DELETED.FRI,DELETED.SAT,DELETED.SUN,DELETED.TOTAL,DELETED.CREATEDBY,DELETED.CREATEDON,DELETED.GroupNumber INTO E_WORKINGHOURS_HISTORY
WHERE WID=@WID">
            <SelectParameters>
                <asp:QueryStringParameter Name="EmployeeId" QueryStringField="EMPLOYEEID" Type="Int32" />
                <asp:QueryStringParameter Name="UserId" QueryStringField="USERID" Type="Int32" />                
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter DefaultValue="0" Name="MON" />
                <asp:Parameter DefaultValue="0" Name="TUE" />
                <asp:Parameter DefaultValue="0" Name="WED" />
                <asp:Parameter DefaultValue="0" Name="THU" />
                <asp:Parameter DefaultValue="0" Name="FRI" />
                <asp:Parameter DefaultValue="0" Name="SAT" />
                <asp:Parameter DefaultValue="0" Name="SUN" />
                <asp:Parameter Name="STARTDATE" />
                <asp:QueryStringParameter Name="UserId" QueryStringField="UserId" Type="Int32" />
                <asp:QueryStringParameter Name="UpdateUserId" QueryStringField="UpdateUserId" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="WID" />                
            </UpdateParameters>
        </asp:SqlDataSource>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
            <br />
          <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="caption"/>
           
       
    </form>
    
</body>
</html>
