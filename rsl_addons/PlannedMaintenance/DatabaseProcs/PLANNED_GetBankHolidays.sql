-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,20 Oct 2014>
-- Description:	<Description,,Get Bank Holidays details to skip these holidays from Intelligent Scheduling>
-- =============================================
ALTER PROCEDURE PLANNED_GetBankHolidays
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select Convert(CHAR(10), BHDATE, 101) as BHDATE from G_BANKHOLIDAYS
END
GO
