USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetAllEmpoloyees]    Script Date: 01/16/2014 00:32:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--	EXEC PLANNED_GetAllEmpoloyees	
-- 	Author:		Noor Muhammad
-- 	Create date: 15 Jan, 2013
-- 	Description:	This procedure 'll return all the operatives
-- 	Web Page: ReplacementList.aspx
-- =============================================*/
ALTER PROCEDURE [dbo].[PLANNED_GetAllEmpoloyees]
		
AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT 
		E__EMPLOYEE.FIRSTNAME +' '+E__EMPLOYEE.LASTNAME as EmployeeName
		,E__EMPLOYEE.EMPLOYEEID as EmployeeId 
	From E__EMPLOYEE 
	INNER JOIN AC_LOGINS on AC_LOGINS.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID
	WHERE AC_LOGINS.ACTIVE =1
	order by EmployeeName ASC
		
END
