/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	6th December 2013
--  Description:	COMPTRADEID is added to identify the exact trade of an appointment
--					
 '==============================================================================*/


ALTER TABLE PLANNED_APPOINTMENTS_HISTORY
ADD COMPTRADEID INT

ALTER TABLE PLANNED_APPOINTMENTS_HISTORY
ADD CONSTRAINT FK_APPOINTMENTHISTORY_COMPTRADEID
FOREIGN KEY (COMPTRADEID)
REFERENCES PLANNED_COMPONENT_TRADE(COMPTRADEID)