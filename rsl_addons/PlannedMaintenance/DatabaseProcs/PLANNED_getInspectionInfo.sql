USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_getInspectionInfo]    Script Date: 01/16/2014 00:59:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PLANNED_getInspectionInfo] 

/* ===========================================================================
 '   NAME:           PLANNED_getInspectionInfo
-- EXEC	[dbo].[PLANNED_getInspectionInfo]	
-- Author:		<Ahmed Mehmood>
-- Create date: <2/11/2013>
-- Description:	<GET INSPECTION INFORMATION>
-- Web Page: ReplacementList.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@inspectionId int = -1		
	)
		
AS

DECLARE @JOURNALHISTORYID INT

SELECT	Convert(varchar,PLANNED_INSPECTION_APPOINTMENTS.APPOINTMENTDATE,103) as AppointmentDate
		,PLANNED_INSPECTION_APPOINTMENTS.APPOINTMENTTIME as AppointmentStartTime
		,PLANNED_INSPECTION_APPOINTMENTS.APPOINTMENTENDTIME as AppointmentEndTime
		,ISNULL(PLANNED_INSPECTION_APPOINTMENTS.ACTIONID,-1) as ActionId
		--,ISNULL(PLANNED_INSPECTION_APPOINTMENTS.TRADEID,-1) as TradeId
		,PLANNED_INSPECTION_APPOINTMENTS.ASSIGNEDTO as OperativeId
		
FROM	PLANNED_INSPECTION_APPOINTMENTS INNER JOIN PLANNED_JOURNAL ON PLANNED_INSPECTION_APPOINTMENTS.JOURNALID = PLANNED_JOURNAL.JOURNALID
	--	LEFT JOIN PLANNED_COMPONENT_TRADE ON PLANNED_JOURNAL.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID
	--	LEFT JOIN G_TRADE ON PLANNED_INSPECTION_APPOINTMENTS.TRADEID = G_TRADE.TRADEID
		
WHERE	PLANNED_INSPECTION_APPOINTMENTS.inspectionid=@inspectionId


SELECT  @JOURNALHISTORYID = PLANNED_INSPECTION_APPOINTMENTS.JOURNALHISTORYID
FROM PLANNED_INSPECTION_APPOINTMENTS
WHERE PLANNED_INSPECTION_APPOINTMENTS.inspectionid=@inspectionId 

SELECT	PLANNED_SAVEDLETTERS.letterId as letterId 
FROM	PLANNED_SAVEDLETTERS
WHERE	PLANNED_SAVEDLETTERS.journalhistoryid = @JOURNALHISTORYID

SELECT	CONVERT(VARCHAR,PLANNED_Documents.DocumentId) as LetterDocValue ,PLANNED_Documents.DocumentName as LetterDocName ,CONVERT(VARCHAR,PLANNED_SAVEDLETTERS.letterId) as LetterId
FROM	PLANNED_Documents left outer join PLANNED_SAVEDLETTERS on  PLANNED_SAVEDLETTERS.journalhistoryid = PLANNED_Documents.journalhistoryId 
WHERE	PLANNED_Documents.journalhistoryId = @JOURNALHISTORYID
	
				















