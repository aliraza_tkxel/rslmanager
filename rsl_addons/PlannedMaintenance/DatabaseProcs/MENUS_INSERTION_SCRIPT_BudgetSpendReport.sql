 /* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	27 November 2014
--  Description:	Menu Insertion Scripts for "Budget Spend Report"
--					
 '==============================================================================*/

DECLARE @PLANNED_MENUID INT
DECLARE @PLANNED_REPORTS_PAGEID INT


--======================================================================================================
--												MENUS
--======================================================================================================
	
-- GETTING MENUID OF PLANNED	
	
SELECT 	@PLANNED_MENUID	= AC_MENUS.MENUID 
FROM	AC_MENUS 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID 
WHERE	AC_MENUS.DESCRIPTION = 'Planned'
		AND AC_MODULES.DESCRIPTION = 'Property'

--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
-- GETTING PAGEID OF PLANNED REPORT PAGE

SELECT	@PLANNED_REPORTS_PAGEID = PG.PAGEID  
FROM	AC_PAGES AS PG
		INNER JOIN AC_MENUS AS MN ON PG.MENUID = MN.MENUID 
		INNER JOIN AC_MODULES AS MD ON MN.MODULEID = MD.MODULEID  
WHERE	MD.DESCRIPTION = 'Property'
		AND MN.DESCRIPTION = 'Planned'
		AND PG.DESCRIPTION = 'Reports'


--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================

--======================
-- Planned > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[DisplayInTree],[ParentPage])
VALUES	('Budget Spend',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/BudgetSpendReport.aspx','','8',2,1,@PLANNED_REPORTS_PAGEID)
