USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PLANNED_GetSearchProperty] 

/* ===========================================================================
 ' NAME: PLANNED_GetSearchProperty
-- EXEC	 [dbo].[PLANNED_GetSearchProperty]
--		
-- Author:		<Ahmed Mehmood>
-- Create date: <12/11/2013>
-- Description:	< Returns the search results for property >
-- Web Page: MiscWorks.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@searchText varchar (MAX) = ''
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),		                
	        @SearchCriteria varchar(8000),
	        @mainSelectQuery varchar(8000)
	        

    --========================================================================================
	--							PROPERTY SEARCH
    --========================================================================================
         
    SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'SearchProperty.Address LIKE ''%'+ LTRIM(@searchText) +'%'' AND' 
                                                                                                                                                                                                                      
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT TOP(100)' +                      
    CHAR(10) + 'SearchProperty.PropertyId as id ,SearchProperty.Address as value'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT	P__PROPERTY.PROPERTYID as PropertyId,ISNULL(P__PROPERTY.FLATNUMBER,'''')+'' ''+ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
									FROM	P__PROPERTY ) SearchProperty'  	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	    