/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	10 March 2014
--  Description:	Populate the mapping table PLANNED_COMPONENT_ITEM.
--					
 '==============================================================================*/


-- POPULATE PLANNED_COMPONENT_ITEM

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Roof'),dbo.GetItemId('Main Construction','Dwelling','Roof'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Fascias'),dbo.GetItemId('Externals','Dwelling','Fascias'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Front Windows'),dbo.GetItemId('Externals','Dwelling','Front Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Rear Windows'),dbo.GetItemId('Externals','Dwelling','Rear Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Side Windows'),dbo.GetItemId('Externals','Dwelling','Side Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Front Roof Windows'),dbo.GetItemId('Externals','Dwelling','Front Roof Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Rear Roof Windows'),dbo.GetItemId('Externals','Dwelling','Rear Roof Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Side Roof Windows'),dbo.GetItemId('Externals','Dwelling','Side Roof Windows'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Front Door'),dbo.GetItemId('Externals','Dwelling','Front Door'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Rear Door'),dbo.GetItemId('Externals','Dwelling','Rear Door'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Side Door'),dbo.GetItemId('Externals','Dwelling','Side Door'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Electrics - Upgrade Due'),dbo.GetItemId('Internals','Services','Electrics'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Rewire Due'),dbo.GetItemId('Internals','Services','Electrics'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('CUR Due'),dbo.GetItemId('Internals','Services','Electrics'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Water heater - Mains'),dbo.GetItemId('Internals','Services','Water'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Water heater - Gas'),dbo.GetItemId('Internals','Services','Water'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Water heater - Electric'),dbo.GetItemId('Internals','Services','Water'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Water heater - Cylinder'),dbo.GetItemId('Internals','Services','Water'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Gas'),dbo.GetItemId('Internals','Services','Gas'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Radiators'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Gas fires'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - HVAC'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Underfloor'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Night storage'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Air source'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Solar'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Warm air'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Heating system - Ground source heat pump'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Emitters Replacement Due'),dbo.GetItemId('Internals','Services','Heating'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Bathroom'),dbo.GetItemId('Internals','Accommodation','Bathroom'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Kitchen'),dbo.GetItemId('Internals','Accommodation','Kitchen'),1)

INSERT INTO [PLANNED_COMPONENT_ITEM]([COMPONENTID],[ITEMID],[ISACTIVE])
VALUES (dbo.GetComponentId('Separate W/C'),dbo.GetItemId('Internals','Accommodation','Separate W/C'),1)