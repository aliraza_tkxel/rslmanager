/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	15 Jan 2014
--  Description:	AppointmentEndTime is added to accomodate the changes of v1.2.2
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE PLANNED_INSPECTION_APPOINTMENTS
ADD APPOINTMENTENDTIME NVARCHAR(10)