USE [RSLBHALive]
GO
/****** Object:  Trigger [dbo].[PLANNED_AFTER_UPDATE_PLANNED_APPAOINTMENTS]    Script Date: 04/10/2015 19:10:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--====================================
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,8th Dec,2013>
-- Description:	<Description,,This trigger 'll insert the reocord in planned_appointments_history after updation in planned_appointments
--====================================
ALTER TRIGGER [dbo].[PLANNED_AFTER_UPDATE_PLANNED_APPAOINTMENTS]  
 ON [dbo].[PLANNED_APPOINTMENTS]   
AFTER UPDATE  
AS  
BEGIN  	
	INSERT INTO PLANNED_APPOINTMENTS_HISTORY
           ([APPOINTMENTID]
           ,[TENANCYID]
           ,[JournalId]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]
           ,[CUSTOMERNOTES]
           ,[ISPENDING]
           ,[JOURNALSUBSTATUS]
           ,[APPOINTMENTALERT]
           ,[APPOINTMENTCALENDER]
           ,[SURVEYOURSTATUS]
           ,[APPOINTMENTSTATUS]
           ,[SURVEYTYPE]
           ,[COMPTRADEID]
           ,[isMiscAppointment]
           ,[DURATION]
           ,[CompletionNotes])
           SELECT 
            i.[APPOINTMENTID]
           ,i.[TENANCYID]
           ,i.[JournalId]
           ,i.[JOURNALHISTORYID]
           ,i.[APPOINTMENTDATE]
           ,i.[APPOINTMENTENDDATE]
           ,i.[APPOINTMENTSTARTTIME]
           ,i.[APPOINTMENTENDTIME]
           ,i.[ASSIGNEDTO]
           ,i.[CREATEDBY]
           ,i.[LOGGEDDATE]
           ,i.[APPOINTMENTNOTES]
           ,i.[CUSTOMERNOTES]
           ,i.[ISPENDING]
           ,i.[JOURNALSUBSTATUS]
           ,i.[APPOINTMENTALERT]
           ,i.[APPOINTMENTCALENDER]
           ,i.[SURVEYOURSTATUS]
           ,i.[APPOINTMENTSTATUS]
           ,i.[SURVEYTYPE]
           ,i.[COMPTRADEID]
           ,i.[isMiscAppointment]
           ,i.[DURATION]
           ,i.[CompletionNotes]
           FROM INSERTED i                         
 END    
