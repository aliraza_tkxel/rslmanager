/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	3-9-2014
--  Description:	DURATION of the appointment
--					
 '==============================================================================*/

ALTER TABLE PLANNED_APPOINTMENTS ADD DURATION FLOAT NULL
ALTER TABLE PLANNED_APPOINTMENTS_HISTORY ADD DURATION FLOAT NULL
