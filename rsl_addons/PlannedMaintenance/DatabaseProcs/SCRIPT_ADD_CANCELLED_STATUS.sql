/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	25th November 2013
--					
 '==============================================================================*/


INSERT INTO PLANNED_STATUS
           ([TITLE]
           ,[RANKING]
           ,[CREATEDDATE]
           ,[CREATEDBY]
           ,[MODIFIEDBY]
           ,[MODIFIEDDATE]
           ,[ISEDITABLE])
     VALUES
           ('Cancelled'
           ,0
           ,GETDATE()
           ,615
           ,NULL
           ,NULL
           ,0)
GO


