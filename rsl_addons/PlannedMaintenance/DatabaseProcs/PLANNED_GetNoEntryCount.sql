USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PLANNED_GetNoEntryCount] 

/* ===========================================================================
 '   NAME:           PLANNED_GetNoEntryCount
-- EXEC	[dbo].[PLANNED_GetNoEntryCount]	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/09/2013>
-- Description:	<Get No Entry Count>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		
		@componentId int = -1

	)
	
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),	        
	 @mainSelectQuery varchar(5500),        
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = '	PLANNED_APPOINTMENTS.APPOINTMENTSTATUS=''Complete'' AND PLANNED_SUBSTATUS.TITLE = ''No Entry'' AND '
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND'  
    

	SET @SelectClause = 'SELECT COUNT(*) as TotalCount '                                            		
	SET @FromClause = CHAR(10) + 'FROM PLANNED_NOENTRY
		INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_NOENTRY.APPOINTMENTID = PLANNED_APPOINTMENTS.APPOINTMENTID
		INNER JOIN PLANNED_JOURNAL ON  PLANNED_APPOINTMENTS.JOURNALID = PLANNED_JOURNAL.JOURNALID
		INNER JOIN PLANNED_SUBSTATUS ON PLANNED_APPOINTMENTS.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID'
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 )'        
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)
		

		















