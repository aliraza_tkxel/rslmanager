/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	6 March 2014
--  Description:	Add SOrder column to order components.
--					
 '==============================================================================*/
-- ADD SOrder Column
ALTER TABLE PLANNED_COMPONENT
ADD SOrder int