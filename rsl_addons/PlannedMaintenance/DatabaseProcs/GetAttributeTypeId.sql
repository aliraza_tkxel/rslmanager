USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE function [dbo].[GetAttributeTypeId](@description as varchar(50))              
 returns int              
 as               
 begin              
           
    declare @typeId as int              
          
	SELECT @typeId = TYPEID
    FROM PA_ATTRIBUTE_TYPES  	
	WHERE description = @description
         
    return(@typeId)              
               
 end  

GO


