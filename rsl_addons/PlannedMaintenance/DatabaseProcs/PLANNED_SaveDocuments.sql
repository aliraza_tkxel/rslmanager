USE RSLBHALive
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = dbo.PLANNED_SaveDocuments
--		@journalHistoryId = 1,
--		@documentName = N'abc.pdf'

--SELECT	'Return Value' = @return_value

-- Author:		<Ahmed Mehmood>
-- Create date: <1 Nov, 2013>
-- Description:	<This stored procedure will save the reccord in PLANNED_SaveDocuments with journalhistory id>
-- Web Page: ReplacementList.aspx
-- =============================================
CREATE PROCEDURE dbo.PLANNED_SaveDocuments 
	-- Add the parameters for the stored procedure here
	@journalHistoryId int,
	@documentName nvarchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO PLANNED_Documents
           (DocumentName
           ,JournalHistoryId
           ,CreatedDate
           ,ModifiedDate           
           )
     VALUES
           (@documentName 
           ,@journalHistoryId            
           ,CURRENT_TIMESTAMP 
           ,CURRENT_TIMESTAMP 
           )
END
