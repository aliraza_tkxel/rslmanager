
/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	31st March 2014
--  Description:	Update 'Gas' to 'Appliance Servicing'
--					
 '==============================================================================*/

UPDATE	PA_ITEM
SET		ItemName = 'Appliance Servicing'
WHERE	ItemName = 'Gas'