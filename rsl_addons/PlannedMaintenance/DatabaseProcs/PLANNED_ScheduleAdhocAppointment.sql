USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_ScheduleAdhocAppointment]    Script Date: 03/06/2015 15:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Name:		PLANNED_ScheduleAdhocAppointment
-- Author:		<Author, Muhammad Awais>      
-- Create date:	<Create Date,11/19/2013>      
-- Description:	<Description,Update/Insert PLANNED_JOURNAL, PLANNED_JOURNAL_HISTORY, PLANNED_MISC_TRADE
--Last modified Date:	17/07/2014      
---======================================================================      
      
-- =============================================      
ALTER PROCEDURE [dbo].[PLANNED_ScheduleAdhocAppointment]      
 -- Add the parameters for the stored procedure here      
@PropertyId varchar(20)      
,@userId int      
,@customerNotes varchar(1000)      
,@appointmentNotes varchar(1000)                
,@parameterId int      
,@parameterValueId int      
,@appointmentTypeId INT      
,@PMO INT   
,@tradesDt AS PLANNED_MiscAdhocTrade READONLY    
,@isSaved int = 0 out      
,@journalIdOut int = -1 out      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
               
DECLARE       
      
@ToBeArranged int      
,@JournalId int      
,@JournalHistoryId int      
,@AppointmentId int      
,@AppointmentHistoryId int      
      
SELECT  @ToBeArranged = PLANNED_STATUS.statusid FROM PLANNED_STATUS WHERE PLANNED_STATUS.title ='To be Arranged'      
      
BEGIN TRANSACTION;      
BEGIN TRY      
-- =============================================      
-- get status history id of "Arranged"      
-- =============================================      
  Declare @ArrangedHistoryId int      
  SELECT @ArrangedHistoryId= MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE StatusId =@ToBeArranged      
-- ====================================================================================================      
--          INSERTION (PLANNED_JOURNAL)      
-- ====================================================================================================      
       
 -- This script block has been disabled as PMO/JournalId is now comming from front end, from PMOs       
 -- dropdown on misc work screen.      
 --IF @PMO = -1       
	--BEGIN      
		INSERT PLANNED_JOURNAL ([PROPERTYID]      
				,[COMPONENTID]      
				,[STATUSID]      
				,[ACTIONID]      
				,[CREATIONDATE]      
				,[CREATEDBY]
				,[APPOINTMENTTYPEID])      
		VALUES (@PropertyId,null,@ToBeArranged,null,GETDATE(),@userId,@appointmentTypeId)      
		SELECT @JournalId = SCOPE_IDENTITY()      
	--END
--ELSE        
--	SET @JournalId = @PMO      
	PRINT 'JOURNALID = ' + CONVERT( VARCHAR, @JournalId )
        
-- ====================================================================================================      
--          INSERTION (PLANNED_JOURNAL_HISTORY)      
-- ====================================================================================================      
--IF @PMO = -1       
--	BEGIN        
		INSERT PLANNED_JOURNAL_HISTORY ([JOURNALID]      
				,[PROPERTYID]      
				,[COMPONENTID]      
				,[STATUSID]      
				,[ACTIONID]      
				,[CREATIONDATE]      
				,[CREATEDBY]      
				,[NOTES]      
				,[ISLETTERATTACHED]      
				,[StatusHistoryId]      
				,[ActionHistoryId]      
				,[IsDocumentAttached])
		VALUES ( @JournalId, @PropertyId, NULL, @ToBeArranged, null, GETDATE(), @userId, NULL, 0, @ArrangedHistoryId, NULL, 0 )
		
		SELECT @JournalHistoryId = SCOPE_IDENTITY()      
--	END      
--ELSE      
--	BEGIN        
--		SELECT @JournalHistoryId = MAX(JOURNALHISTORYID)      
--		FROM PLANNED_JOURNAL_HISTORY      
--		WHERE JOURNALID = @JournalId      
--	END      

	PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)      

-- ====================================================================================================      
--          INSERTION (PLANNED_MISC_TRADES)      
-- ====================================================================================================      
IF @parameterId = -1 or @parameterId=0 
	BEGIN
		SET @parameterId = NULL
	END 

IF @parameterValueId = -1 or @parameterValueId=0 
	BEGIN
		SET @parameterValueId = NULL
	END     

INSERT INTO PLANNED_MISC_TRADE      
           ([TradeId]      
           ,[Duration]      
           ,[ParameterId]      
           ,[ParameterValueId]
           ,JournalId)      
                 
           Select TRADEID      
           ,DURATION
           ,@parameterId      
           ,@parameterValueId
           ,@JournalId  From @tradesDt
                    
            
END TRY      

BEGIN CATCH       
	IF @@TRANCOUNT > 0      
		BEGIN           
			ROLLBACK TRANSACTION;         
			SET @isSaved = 0              
		END      
		DECLARE @ErrorMessage NVARCHAR(4000);      
		DECLARE @ErrorSeverity INT;      
		DECLARE @ErrorState INT;      
      
		SELECT @ErrorMessage = ERROR_MESSAGE(),      
				@ErrorSeverity = ERROR_SEVERITY(),      
				@ErrorState = ERROR_STATE();      
      
		-- Use RAISERROR inside the CATCH block to return       
		-- error information about the original error that       
		-- caused execution to jump to the CATCH block.      
		RAISERROR (@ErrorMessage, -- Message text.      
					@ErrorSeverity, -- Severity.      
					@ErrorState -- State.     
				);      
END CATCH;      
      
IF @@TRANCOUNT > 0      
	BEGIN        
		COMMIT TRANSACTION;        
		SET @isSaved = 1      
	END

SET @journalIdOut = @JournalId      
      
END
