/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	5th November 2013
--  Description:	TRADEID is added to identify the exact trade of an appointment
--					
 '==============================================================================*/

USE [RSLBHALive1]
GO


ALTER TABLE PLANNED_INSPECTION_APPOINTMENTS
ADD TRADEID INT

ALTER TABLE dbo.PLANNED_INSPECTION_APPOINTMENTS ADD FOREIGN KEY (TRADEID) REFERENCES G_TRADE (TradeId)
GO