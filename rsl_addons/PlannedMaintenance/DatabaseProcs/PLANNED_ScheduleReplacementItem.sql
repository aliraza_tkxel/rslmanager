USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_ScheduleReplacementItem]    Script Date: 05/06/2014 17:57:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,10/29/2013>
-- Description:	<Description,Update/Insert PLANNED_JOURNAL
--Last modified Date:10/29/2013
---======================================================================
--DECLARE	@return_value int
--DECLARE @SelectedReplacementItems AS PLANNED_SELECTED_REPLACEMENT_ITEM;
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (1,'A000001')
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (2,'A000002')
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (3,'A000003')   

--EXEC	@return_value = [dbo].[PLANNED_ScheduleReplacementItem]
--		@USERID = 142,
--		@SelectedReplacementItems = @ReplacementItem

-- =============================================
ALTER PROCEDURE PLANNED_ScheduleReplacementItem
	-- Add the parameters for the stored procedure here
	@USERID int
	,@SelectedReplacementItems as PLANNED_SELECTED_REPLACEMENT_ITEM readonly
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
   
--DECLARE @SelectedReplacementItems AS PLANNED_SELECTED_REPLACEMENT_ITEM;
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (19,'A010980003')
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (29,'C030290000')
--INSERT INTO @SelectedReplacementItems(COMPONENTID,PROPERTYID)VALUES (30,'BHA0000914')     
        
DECLARE 
@ComponentId int
,@InspectionArrangeId int
,@ToBeArrangeId int
,@PropertyId varchar(20)
,@StatusId int
,@ActionId int
,@JournalId int
,@CreationDate smalldatetime
,@CreatedBy int 
,@CancelledId int
--,@USERID int -- NEED TO REMOVE MUST
--SET @USERID=144
 
select  @InspectionArrangeId = PLANNED_STATUS.statusid from PLANNED_STATUS where PLANNED_STATUS.title ='Inspection Arranged'
select  @ToBeArrangeId = PLANNED_STATUS.statusid from PLANNED_STATUS where PLANNED_STATUS.title ='To be Arranged'
select  @CancelledId =  PLANNED_STATUS.statusid from PLANNED_STATUS where PLANNED_STATUS.title ='Cancelled'
DECLARE selectedItemsCursor CURSOR -- Declare cursor

LOCAL SCROLL STATIC
 
FOR
SELECT COMPONENTID, PROPERTYID FROM @SelectedReplacementItems
 
OPEN selectedItemsCursor -- open the cursor

FETCH NEXT FROM selectedItemsCursor INTO @ComponentId, @PropertyId
   
WHILE @@FETCH_STATUS = 0

 BEGIN
 
 	IF EXISTS (	SELECT 1
				FROM PLANNED_JOURNAL 
				WHERE PLANNED_JOURNAL.PROPERTYID = @PropertyId
				AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
				AND (PLANNED_JOURNAL.STATUSID = @InspectionArrangeId OR PLANNED_JOURNAL.STATUSID = @ToBeArrangeId OR PLANNED_JOURNAL.STATUSID = @CancelledId) )
		BEGIN	
 
		
		-- If record is found then change the status to 'To be Arranged'
 
		DECLARE journalCursor CURSOR 
		FOR  
		SELECT JOURNALID,STATUSID ,ACTIONID ,CREATIONDATE,CREATEDBY
		FROM PLANNED_JOURNAL 
		WHERE PLANNED_JOURNAL.PROPERTYID = @PropertyId
		AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
		AND (PLANNED_JOURNAL.STATUSID = @InspectionArrangeId OR PLANNED_JOURNAL.STATUSID = @ToBeArrangeId OR PLANNED_JOURNAL.STATUSID = @CancelledId )

		OPEN journalCursor   
		FETCH NEXT FROM journalCursor INTO @JournalId,@StatusId,@ActionId,@CreationDate,@CreatedBy 

		WHILE @@FETCH_STATUS = 0   
		BEGIN	

		--	Update PLANNED_JOURNAL by setting status to �To be arranged�
				
			 UPDATE PLANNED_JOURNAL
            SET PLANNED_JOURNAL.STATUSID = @ToBeArrangeId
            WHERE PLANNED_JOURNAL.PROPERTYID = @PropertyId
				AND PLANNED_JOURNAL.COMPONENTID =@ComponentId
				AND ( PLANNED_JOURNAL.STATUSID = @InspectionArrangeId OR PLANNED_JOURNAL.STATUSID = @ToBeArrangeId OR PLANNED_JOURNAL.STATUSID = @CancelledId ) 

		--  Insert into PLANNED_JOURNAL_HISTORY

			INSERT PLANNED_JOURNAL_HISTORY  ([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached])
			VALUES(@JournalId,@PropertyId,@ComponentId,@ToBeArrangeId,@ActionId,@CreationDate,@CreatedBy,NULL,0,NULL,NULL,0)


			FETCH NEXT FROM journalCursor INTO @JournalId,@StatusId,@ActionId,@CreationDate,@CreatedBy   
		END   

		CLOSE journalCursor   
		DEALLOCATE journalCursor
		END 	
	ELSE
		BEGIN
		
		-- Insert into PLANNED_JOURNAL with Status of �To be Arranged�
		
		INSERT PLANNED_JOURNAL
		 ([PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY])
		VALUES  (@PropertyId,@ComponentId,@ToBeArrangeId,NULL,GETDATE(),@USERID)
		
		SELECT @JournalId = SCOPE_IDENTITY()
		
		print @JournalId
		-- Insert into PLANNED_JOURNAL_HISTORY
		
		INSERT PLANNED_JOURNAL_HISTORY([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached]) VALUES
			(@JournalId,@PropertyId,@ComponentId,@ToBeArrangeId,NULL,GETDATE(),@USERID,NULL,0,NULL,NULL,0)
		
           
		END 
       
 
   --PRINT convert(varchar,@ComponentId) + ' ' + @PropertyId -- print the name
 
   FETCH NEXT FROM selectedItemsCursor INTO @ComponentId, @PropertyId
 
END

 
CLOSE selectedItemsCursor -- close the cursor

DEALLOCATE selectedItemsCursor -- Deallocate the cursor
   
END
