USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetSignOff]    Script Date: 12/07/2013 19:45:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		AliRaza
-- Create date: 07/Dec/2013
-- Description:	This SP displays all the Sign Off values. 
-- Useage: View Letter
-- EXEC [dbo].[PLANNED_GetSignOff] 
-- IMPORTANT: Please make suer that there's a entry in the AS_LookupType which has the TypeName as "SignOff"
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_GetSignOff]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		AS_LookupCode.LookupCodeId, 
		AS_LookupCode.CodeName 	
	FROM 
		AS_LookupCode INNER JOIN
		AS_LookupType on AS_LookupCode.LookupTypeId=AS_LookupType.LookupTypeId
	WHERE 
		AS_LookupType.TypeName = 'SignOff'
END
