USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PLANNED_GetInprogressAppointmentOperatives] 

/* ===========================================================================
 '   NAME:           PLANNED_GetInprogressAppointmentOperatives
-- EXEC	[dbo].[PLANNED_GetInprogressAppointmentOperatives]	
-- Author:		<Ahmed Mehmood>
-- Create date: <10/10/2013>
-- Description:	<Get operatives of Inprogress appointments>
-- Web Page: Dashboard.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@componentId int = -1		
	)
		
AS
	
	DECLARE
	 @SelectClause varchar(8000),
	 @FromClause   varchar(8000),
	 @WhereClause  varchar(8000),        
	 @mainSelectQuery varchar(5500),      
	 @SearchCriteria varchar(8000)

         
    SET @SearchCriteria = '	PLANNED_STATUS.TITLE=''Arranged'' 
							AND PLANNED_SUBSTATUS.TITLE IN (''InProgress'',''In Progress'') 
							AND PLANNED_APPOINTMENTS.APPOINTMENTSTATUS IN (''InProgress'',''In Progress'')  AND'
        
    IF NOT @componentId = -1
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +' PLANNED_JOURNAL.COMPONENTID = '+ CONVERT(VARCHAR(10),@componentId) + ' AND'  


	SET @SelectClause =	'	SELECT	DISTINCT PLANNED_APPOINTMENTS.ASSIGNEDTO as OperativeId
									,E__EMPLOYEE.FIRSTNAME +'' ''+ E__EMPLOYEE.LASTNAME as OperativeName '
                                           		
	SET @FromClause =	'	FROM	PLANNED_APPOINTMENTS
									INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
									INNER JOIN P__PROPERTY ON  PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
									INNER JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
									LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID									
									INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
									INNER JOIN PLANNED_SUBSTATUS ON PLANNED_APPOINTMENTS.JOURNALSUBSTATUS = PLANNED_SUBSTATUS.SUBSTATUSID 																						 			'
	
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
			CHAR(10) + CHAR(10) + @SearchCriteria +CHAR(10) + CHAR(9) + ' 1=1 )'        
	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
	
	print (@mainSelectQuery)
	EXEC (@mainSelectQuery)
				















