/* ===========================================================================
--  Author:			Aamir Waheed
--  DATE CREATED:	20 October 2014
--  Description:	One time Insertion scripts for PLANNED_CONTRACTOR_WORK_TYPE.
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY

--======================================================================
-- Insert Status 'Condition to be Arranged'
--======================================================================
DECLARE @WorkType NVARCHAR(100) = 'Adaptation'

IF NOT EXISTS (SELECT * FROM PLANNED_CONTRACTOR_WORK_TYPE WHERE WorkType = @WorkType)	
BEGIN
	PRINT 'PLANNED_CONTRACTOR_WORK_TYPE: WorkTypeId = 4' + ',WorkType = ' + @WorkType
	SET IDENTITY_INSERT PLANNED_CONTRACTOR_WORK_TYPE ON;	
	INSERT INTO PLANNED_CONTRACTOR_WORK_TYPE (WorkTypeId, WorkType) VALUES (4,@WorkType)
	SET IDENTITY_INSERT PLANNED_CONTRACTOR_WORK_TYPE OFF;
END

DECLARE @itemId int
DECLARE @parameterId int
DECLARE @ValueDetail NVARCHAR(50) = 'Sliding Door'
select @itemId=ItemID from PA_ITEM where ItemName ='Aids & Adaptations' And ParentItemId=(select ItemID from PA_ITEM where ItemName ='Bathroom ')
select @parameterId = ParameterId from PA_ITEM_PARAMETER where ItemId = @itemId 

IF NOT EXISTS (SELECT ValueID FROM PA_PARAMETER_VALUE WHERE ParameterID = @parameterId AND ValueDetail = @ValueDetail)	
BEGIN
	PRINT 'Adding Parameter Value: ParameterID = ' + CONVERT(NVARCHAR,@parameterId) + ',Value Detail = ' + @ValueDetail
	INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail, Sorder, IsActive)Values (@parameterId,@ValueDetail,8,1)	
END
Update PA_PARAMETER_VALUE SET IsActive =0  where ValueDetail='Change bathroom door for sliding door'

END TRY BEGIN CATCH
IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END

DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH
  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 