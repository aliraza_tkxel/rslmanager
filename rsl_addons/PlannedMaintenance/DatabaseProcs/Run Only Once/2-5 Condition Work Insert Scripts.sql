/* ===========================================================================
--  Author:			Aamir Waheed
--  DATE CREATED:	08 August 2014
--  Description:	One time Insertion scripts for Condition Works.
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY

--======================================================================
-- Insert Status 'Condition to be Arranged'
--======================================================================

DECLARE @Status NVARCHAR(50) = 'Condition to be Arranged'

IF NOT EXISTS (SELECT
	S.STATUSID
FROM PLANNED_STATUS S
WHERE S.TITLE = @Status) BEGIN
INSERT INTO PLANNED_STATUS (TITLE,
CREATEDDATE,
CREATEDBY,
RANKING,
ISEDITABLE)
	VALUES (@Status, GETDATE(), 0, 0, 0)
END ELSE PRINT 'This Status ''' + @Status + ''' already exists. (No need to insert)' + CHAR(10) + CHAR(13)

--======================================================================
-- Insert Status 'Condition Arranged'
--======================================================================

SET @Status = 'Condition Arranged'

IF NOT EXISTS (SELECT
	S.STATUSID
FROM PLANNED_STATUS S
WHERE S.TITLE = @Status) BEGIN
INSERT INTO PLANNED_STATUS (TITLE,
CREATEDDATE,
CREATEDBY,
RANKING,
ISEDITABLE)
	VALUES (@Status, GETDATE(), 0, 0, 0)
END ELSE PRINT 'This Status ''' + @Status + ''' already exists. (No need to insert)' + CHAR(10) + CHAR(13)

--======================================================================
-- Insert Appointment Type 'Condition'
--======================================================================

DECLARE @AppointmentType nvarchar(20) = 'Condition'

IF NOT EXISTS (SELECT
	AT.Planned_Appointment_TypeId
FROM Planned_Appointment_Type AT
WHERE AT.Planned_Appointment_Type = @AppointmentType) BEGIN
INSERT INTO Planned_Appointment_Type (Planned_Appointment_Type, IsActive)
	VALUES (@AppointmentType, 0)
END ELSE PRINT 'This appointment type ''' + @AppointmentType + ''' already exists. (No need to insert)' + CHAR(10) + CHAR(13)

--======================================================================
-- Insert Status 'Condition Works'
--======================================================================

DECLARE @ConditionWorksStatusId int = NULL -- Use: To insert statusId while inserting condition work actions

SET @Status = 'Condition Works'

IF NOT EXISTS (SELECT
	S.STATUSID
FROM PLANNED_STATUS S
WHERE S.TITLE = @Status) BEGIN
INSERT INTO PLANNED_STATUS (TITLE,
CREATEDDATE,
CREATEDBY,
RANKING,
ISEDITABLE)
	VALUES (@Status, GETDATE(), 0, 0, 0)

SET @ConditionWorksStatusId = SCOPE_IDENTITY()

END ELSE BEGIN
SELECT
	@ConditionWorksStatusId = S.STATUSID
FROM PLANNED_STATUS S
WHERE S.TITLE = @Status
PRINT 'This Status ''' + @Status + ''' already exists. (No need to insert)' + CHAR(10) + CHAR(13)
END

--======================================================================
-- Insert Action 'Recommended'
--======================================================================

DECLARE @Action nvarchar(300) = 'Recommended'

IF NOT EXISTS (SELECT
	A.ActionId
FROM PLANNED_Action A
WHERE A.TITLE = @Action
AND A.Title = @Action) BEGIN
INSERT INTO PLANNED_Action (StatusId, Title, Ranking, CreatedDate, CreatedBy, IsEditable)
	VALUES (@ConditionWorksStatusId, @Action, 1, GETDATE(), 0, 0)
END ELSE BEGIN
PRINT 'This Action ''' + @Action + ''' already exists, for status ''Condition Works''. (No need to insert)' + CHAR(10) + CHAR(13)
END

--======================================================================
-- Insert Action 'Approved'
--======================================================================

SET @Action = 'Approved'

IF NOT EXISTS (SELECT
	A.ActionId
FROM PLANNED_Action A
WHERE A.TITLE = @Action
AND A.Title = @Action) BEGIN
INSERT INTO PLANNED_Action (StatusId, Title, Ranking, CreatedDate, CreatedBy, IsEditable)
	VALUES (@ConditionWorksStatusId, @Action, 2, GETDATE(), 0, 0)
END ELSE BEGIN
PRINT 'This Action ''' + @Action + ''' already exists, for status ''Condition Works''. (No need to insert)' + CHAR(10) + CHAR(13)
END

--======================================================================
-- Insert Action 'Rejected'
--======================================================================

SET @Action = 'Rejected'

IF NOT EXISTS (SELECT
	A.ActionId
FROM PLANNED_Action A
WHERE A.TITLE = @Action
AND A.Title = @Action) BEGIN
INSERT INTO PLANNED_Action (StatusId, Title, Ranking, CreatedDate, CreatedBy, IsEditable)
	VALUES (@ConditionWorksStatusId, @Action, 3, GETDATE(), 0, 0)
END ELSE BEGIN
PRINT 'This Action ''' + @Action + ''' already exists, for status ''Condition Works''. (No need to insert)' + CHAR(10) + CHAR(13)
END

END TRY BEGIN CATCH
IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END

DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH
  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 