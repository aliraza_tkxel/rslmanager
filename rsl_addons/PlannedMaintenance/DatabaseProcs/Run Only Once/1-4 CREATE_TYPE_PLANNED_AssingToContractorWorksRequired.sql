-- ================================
-- Create User-defined Table Type
-- ================================
USE RSLBHALIVE
GO

-- Create the data type
CREATE TYPE dbo.PLANNED_AssingToContractorWorksRequired AS TABLE 
(
	WorksRequired NVARCHAR(4000) NOT NULL, 
	NetCost SMALLMONEY NOT NULL, 
	VatType INT NOT NULL,
	VAT SMALLMONEY NOT NULL,
	GROSS SMALLMONEY NOT NULL, 
	PIStatus INT NOT NULL,
	ExpenditureId INT NOT NULL
    --PRIMARY KEY (WorksRequired)
)
GO
