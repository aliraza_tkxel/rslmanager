/*
   17 July 201412:10:24
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.E__EMPLOYEE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.PLANNED_REASON
	(
	ReasonId int NOT NULL IDENTITY (1, 1),
	Reason nvarchar(200) NOT NULL,
	IsEditable bit NOT NULL,
	CreatedBy int NOT NULL,
	CreatedDate datetime2(2) NOT NULL,
	ModifiedBy int NULL,
	ModifiedDate datetime2(2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PLANNED_REASON ADD CONSTRAINT
	PK_PLANNED_REASON PRIMARY KEY CLUSTERED 
	(
	ReasonId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PLANNED_REASON ADD CONSTRAINT
	FK_PLANNED_REASON_E__EMPLOYEE FOREIGN KEY
	(
	CreatedBy
	) REFERENCES dbo.E__EMPLOYEE
	(
	EMPLOYEEID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PLANNED_REASON ADD CONSTRAINT
	FK_PLANNED_REASON_E__EMPLOYEE1 FOREIGN KEY
	(
	ModifiedBy
	) REFERENCES dbo.E__EMPLOYEE
	(
	EMPLOYEEID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PLANNED_REASON SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PLANNED_REASON', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PLANNED_REASON', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PLANNED_REASON', 'Object', 'CONTROL') as Contr_Per 