/* ===========================================================================
--  Author:			Aamir Waheed
--  DATE CREATED:	23 August 2014
--  Description:	One time Deletion scripts to drop stored procedure
--						 PLANNED_GetContactDropDownValuesbyPropertyId.
--					
 '==============================================================================*/

DECLARE @StoredProcedureToDelete NVARCHAR(256) = 'PLANNED_GetContactDropDownValuesbyPropertyId'


IF EXISTS (SELECT * FROM sys.procedures WHERE name = @StoredProcedureToDelete)
BEGIN
	DECLARE @SQL NVARCHAR(1000) = 'DROP PROCEDURE ' + @StoredProcedureToDelete
	EXEC sys.sp_executesql @SQL
	PRINT 'Stroted Procedure: "' + @StoredProcedureToDelete + '" deleted successfully.'
END
ELSE
	PRINT 'Stroted Procedure: "' + @StoredProcedureToDelete + '" not exists.'