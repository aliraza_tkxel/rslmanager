USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[Planned_Appointment_Type]    Script Date: 08/08/2014 17:36:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Planned_Appointment_Type](
	[Planned_Appointment_TypeId] [int] IDENTITY(1,1) NOT NULL,
	[Planned_Appointment_Type] [nvarchar](100) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]

GO


