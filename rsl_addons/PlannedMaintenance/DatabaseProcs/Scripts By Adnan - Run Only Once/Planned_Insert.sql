
INSERT INTO dbo.PLANNED_COMPONENT
        ( COMPONENTNAME ,
          ISACCOUNTING ,
          CYCLE ,
          FREQUENCY ,
          MATERIALCOST ,
          LABOURCOST ,
          EDITEDBY ,
          EDITEDON
        )
VALUES  ('Structure',1,10,'yrs',2000,1000,615,GETDATE()),
('Surface water drainage',1,5,'yrs',2000,1000,615,GETDATE()),
('Fascias',0,10,'yrs',2000,1000,615,GETDATE()),
('Front Windows',1,10,'yrs',2000,1000,615,GETDATE()),
('Rear Windows',1,30,'yrs',2000,1000,615,GETDATE()),
('Front Door',1,10,'yrs',2000,1000,615,GETDATE()),
('Electrics - CUR',1,10,'yrs',2000,1000,615,GETDATE()),
('Electrics - Upgrade Due',1,20,'yrs',2000,1000,615,GETDATE()),
('Gas',1,10,'yrs',2000,1000,615,GETDATE()),
('Heating',1,10,'yrs',2000,1000,615,GETDATE()),
('Bathroom',1,10,'yrs',2000,1000,615,GETDATE()),
('Kitchen',1,10,'yrs',2000,1000,615,GETDATE()),
('Cloakroom',1,1,'yrs',2000,1000,615,GETDATE()),
('Separate W/C - w/c',1,10,'yrs',2000,1000,615,GETDATE()),
('Separate W/C - handbasin',0,10,'yrs',2000,1000,615,GETDATE())

----------------------------------------------------------
INSERT INTO dbo.PLANNED_STATUS
        ( TITLE ,
          Ranking,
          CREATEDDATE ,
          CREATEDBY
 
        )
VALUES  ( N'Inspection Arranged' ,0, GETDATE(),615),( N'To be Arranged' , 0,GETDATE(),615),( N'Arranged' ,0, GETDATE(),615),( N'Completed' ,0, GETDATE(),615)

----------------------------------------------------------
INSERT INTO dbo.PLANNED_SUBSTATUS
        ( STATUSID, TITLE )
VALUES  (3,'In Progress'),(3,'Paused'),(3,'No Entry'),(3,'Complete'),(4,'Complete')

----------------------------------------------------------
INSERT INTO dbo.PLANNED_Action
        ( StatusId ,
          Title ,
          Ranking ,
          CreatedDate ,
          ModifiedDate ,
          CreatedBy ,
          ModifiedBy ,
          IsEditable
        )
VALUES  ( 1 , N'Letter' , 0 , '20131008 15:57:40' ,'20131008 15:57:40' , 615 , null , 1 ),
  ( 1 , N'Telephone' , 0 , '20131008 15:57:40' ,'20131008 15:57:40' , 615 , null , 1 )
----------------------------------------------------------