USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 15/07/2014
-- Description:	Get list Conditional Work Appointments Arranged
-- Last modified Date:
/*
	DECLARE	@totalCount int

	SELECT	@totalCount = 0

	EXEC	[dbo].[PLANNED_GetConditionalArrangedList]
			@searchText = NULL,
			@pageSize = 1,
			@pageNumber = 30,
			@sortColumn = N'Address',
			@sortOrder = N'DESC',
			@totalCount = @totalCount OUTPUT

	SELECT	@totalCount as N'@totalCount'


*/
-- =============================================

ALTER PROCEDURE [dbo].[PLANNED_GetConditionalArrangedList]
	-- Add the parameters for the stored procedure here
			
		@searchText VARCHAR(200) = '',		

	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(2000),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(1000),	
        @mainSelectQuery varchar(6000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int
		
				
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
									
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( ''PMO''+CONVERT(VARCHAR,J.JOURNALID) LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR J.JOURNALID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PROPERTYADDRESS.Address LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR RIGHT(''0000''+ CONVERT(VARCHAR,A.APPOINTMENTID),4) LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR A.APPOINTMENTID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR P.POSTCODE LIKE ''%' + @searchText + '%'') '
		END	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
		 ''PMO''+CONVERT(VARCHAR,J.JOURNALID) AS Ref
		,RIGHT(''0000''+ CONVERT(VARCHAR,A.APPOINTMENTID),4) AS JSN
		,ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'' '') +'' ''+ ISNULL(P.ADDRESS3,'''') as Address
		,P.POSTCODE AS Postcode
		,ISNULL(I.ItemName,''N/A'') Component		
		,LEFT(E.FIRSTNAME,1) + ISNULL('' '' + LEFT(E.LASTNAME,1),'''') as Operative
		,T.Duration AS Duration
		, ISNULL(CONVERT(VARCHAR(3),datename(weekday,A.APPOINTMENTDATE)) +'' ''+
		CONVERT(VARCHAR(11), A.APPOINTMENTDATE, 106),''N/A'') as Appointment

		-- Sorting purpose column
		,J.JOURNALID as RefSort
		,A.APPOINTMENTID as JSNSort
		,P.HouseNumber as HouseNumber
		,P.ADDRESS1 as PrimaryAddress
		,A.APPOINTMENTDATE as AppointmentDateSort		
		,A.AppointmentId as AppointmentId
		,P.PropertyId
		,I.ItemID AS ComponentId
		'

		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'		
		FROM 
		PLANNED_JOURNAL J
		INNER JOIN PLANNED_APPOINTMENTS A ON A.JournalId = J.JOURNALID
		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID
		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = A.ASSIGNEDTO
		INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID AND S.TITLE = ''Condition Arranged''
		INNER JOIN PLANNED_MISC_TRADE T ON T.AppointmentId = A.APPOINTMENTID

		INNER JOIN PLANNED_CONDITIONWORKS CW ON CW.JournalId = J.JOURNALID
		INNER JOIN PA_PROPERTY_ATTRIBUTES PA ON PA.AttributeId = CW.AttributeId
		INNER JOIN PA_ITEM_PARAMETER IP ON IP.ItemParamID = PA.ITEMPARAMID
		INNER JOIN PA_ITEM I ON I.ItemID = IP.ItemId

		INNER JOIN (SELECT	P.PROPERTYID as PID,ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'' '') +'' ''+ ISNULL(P.ADDRESS3,'''') as Address
					FROM	P__PROPERTY P) AS PROPERTYADDRESS ON P.PROPERTYID = PROPERTYADDRESS.PID
		'
		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 				
		END
		
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'RefSort' 				
		END
		
		IF(@sortColumn = 'JSN')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'JSNSort' 				
		END
		
		IF(@sortColumn = 'Appointment')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AppointmentDateSort' 				
		END
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
	
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
		

END
GO
