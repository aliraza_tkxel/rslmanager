USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_ScheduleConfirmedPlannedAppointments]    Script Date: 04/10/2015 15:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit

--EXEC	@return_value = [dbo].[PLANNED_ScheduleConfirmedPlannedAppointments]
--		@appointmentIds = N'16,21,22,23',
--		@propertyId = N'A500090007',
--		@journalId = 2,
--		@componentId = 1,
--		@createdBy = 615,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,7th Dec,2013>
-- Description:	<Description,,This stored procedure 'll update the appointment status from pending to confirm>
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_ScheduleConfirmedPlannedAppointments]
	@appointmentIds as varchar(max),
	@propertyId as varchar(20),
	@journalId as integer, 
	@componentId as integer,
	@createdBy as integer,	
	@isSaved as bit output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY 
		Declare @newStatusHistoryId int
		Declare @newStatusId int
		-- =============================================
		--get status id of "Arrnaged"
		-- =============================================
		SELECT @newStatusId= STATUSID 
		FROM PLANNED_STATUS 
		WHERE PLANNED_STATUS.TITLE ='Arranged'		
		--Print 'here1'
		
		-- =============================================
		-- get status history id of "Arranged"
		-- =============================================
		SELECT @newStatusHistoryId= MAX(StatusHistoryId) FROM PLANNED_StatusHistory WHERE StatusId =@newStatusId
		--Print 'here2'		
		
		-- =============================================
		--update the planned journal
		-- =============================================
		UPDATE PLANNED_JOURNAL 
		SET STATUSID =@newStatusId
		Where JOURNALID = @journalId
		--Print 'here4'
		-- =============================================
		--insert into journal history
		-- =============================================
		If @componentId = -1
			BEGIN
				INSERT PLANNED_JOURNAL_HISTORY
			([JOURNALID]
           ,[PROPERTYID]
           ,[COMPONENTID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]
           ,[StatusHistoryId]
           ,[ActionHistoryId]
           ,[IsDocumentAttached]) VALUES
			(@journalId,@PropertyId,NULL,@newStatusId,null,GETDATE(),@createdBy,NULL,0,@newStatusHistoryId,NULL,0)
		
			END
		Else
			Begin			
				INSERT PLANNED_JOURNAL_HISTORY
				([JOURNALID]
				   ,[PROPERTYID]
				   ,[COMPONENTID]
				   ,[STATUSID]
				   ,[ACTIONID]
				   ,[CREATIONDATE]
				   ,[CREATEDBY]
				   ,[NOTES]
				   ,[ISLETTERATTACHED]
				   ,[StatusHistoryId]
				   ,[ActionHistoryId]
				   ,[IsDocumentAttached]) VALUES
					(@journalId,@PropertyId,@componentId,@newStatusId,null,GETDATE(),@createdBy,NULL,0,@newStatusHistoryId,NULL,0)
					--Print 'here5'
			End		
			
			DECLARE @JournalHistoryId int
			SELECT @JournalHistoryId = SCOPE_IDENTITY()
			-- =============================================
		-- update appointments status with is pending = 0
		-- =============================================
		Declare @updateAppointmentQuery varchar(max)
		SET @updateAppointmentQuery = 'UPDATE PLANNED_APPOINTMENTS
		SET ISPENDING = 0 ,JOURNALHISTORYID='+convert(VARCHAR,@JournalHistoryId)+'
		WHERE APPOINTMENTID IN ('+@appointmentIds+')'
		EXEC (@updateAppointmentQuery)
		--print @appointmentIds 
		--Print 'here3'
		 -- =============================================
		 --insert into planned_appointments_History using the following trigger 
		 --PLANNED_AFTER_UPDATE_PLANNED_APPAOINTMENTS
		 -- =============================================
		
	END TRY 
	BEGIN CATCH 
	
	  IF @@TRANCOUNT >0	  
	   BEGIN	   
		ROLLBACK TRANSACTION;
		SET @isSaved = 0;		
	   END 
	   DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
			 @ErrorSeverity, -- Severity.
			 @ErrorState -- State.
			 );
	 END CATCH  
	 
	 IF @@TRANCOUNT >0
	 BEGIN
	  COMMIT TRANSACTION;
	  SET @isSaved = 1
	 END 
END
