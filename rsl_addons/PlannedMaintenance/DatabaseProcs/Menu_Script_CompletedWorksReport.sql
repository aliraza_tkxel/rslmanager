	/* ===========================================================================
--  Author:			Salman Nazir
--  DATE CREATED:	19/03/2015
--  Description:	Insertion scripts for Works Completed Report
--					
 '==============================================================================*/
	
	BEGIN TRANSACTION
	BEGIN TRY  
	
	DECLARE @PLANNED_MENUID INT
	DECLARE @PLANNED_REPORTS_PAGEID INT
	--======================================================================================================
--												MENUS
--======================================================================================================
	
-- GETTING MENUID OF PLANNED	
	
SELECT 	@PLANNED_MENUID	= AC_MENUS.MENUID 
FROM	AC_MENUS 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID 
WHERE	AC_MENUS.DESCRIPTION = 'Planned'
		AND AC_MODULES.DESCRIPTION = 'Property'


--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
-- GETTING PAGEID OF PLANNED REPORT PAGE

SELECT	@PLANNED_REPORTS_PAGEID = PG.PAGEID  
FROM	AC_PAGES AS PG
		INNER JOIN AC_MENUS AS MN ON PG.MENUID = MN.MENUID 
		INNER JOIN AC_MODULES AS MD ON MN.MODULEID = MD.MODULEID  
WHERE	MD.DESCRIPTION = 'Property'
		AND MN.DESCRIPTION = 'Planned'
		AND PG.DESCRIPTION = 'Reports'

--Print(@PLANNED_MENUID)
--Print(@PLANNED_REPORTS_PAGEID)
--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================
 	
--======================
-- Planned > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('Completed Works',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/CompletedWorks.aspx','','2',2,@PLANNED_REPORTS_PAGEID,1)


Update AC_PAGES Set ORDERTEXT = 6 Where ParentPage = @PLANNED_REPORTS_PAGEID And DESCRIPTION = 'Spend Report'	
		
	
	END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 