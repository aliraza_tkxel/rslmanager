USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PLANNED_GetComponentTrades]    Script Date: 11/20/2013 18:14:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =============================================
--EXEC	[dbo].[PLANNED_GetComponentTrades]	@journalId = 399
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,20 Nov,2013>
-- Last Updated By: Noor Muhammad
-- Last Updated Date: 20th Nov 2013
-- Description:	<Description,,This stored procedure fetch trades & its status against the component >
-- WebPage: AvailableAppointments.aspx
-- ============================================= */
ALTER PROCEDURE [dbo].[PLANNED_GetComponentTrades] 
	-- Add the parameters for the stored procedure here
	@journalId as int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT
	PC.COMPONENTID							AS ComponentId
	,PC.COMPONENTNAME						AS Component
	,COALESCE(PA.DURATION, PCT.DURATION)	AS Duration
	,PCT.COMPTRADEID						AS ComponentTradeId
	,T.TradeId								AS TradeId
	,T.Description							AS Trade
	,S.STATUSID								AS StatusId
	,S.TITLE								AS Status
FROM
	PLANNED_JOURNAL
	INNER JOIN PLANNED_COMPONENT_TRADE PCT
		ON PLANNED_JOURNAL.COMPONENTID = PCT.COMPONENTID
	INNER JOIN PLANNED_COMPONENT PC
		ON PCT.COMPONENTID = PC.COMPONENTID
	INNER JOIN G_TRADE T
		ON T.TradeId = PCT.TRADEID
	INNER JOIN PLANNED_STATUS S
		ON S.STATUSID = PLANNED_JOURNAL.STATUSID
	LEFT JOIN
	(
		SELECT
			PA.COMPTRADEID	AS COMPTRADEID
			,PA.DURATION	AS DURATION
		FROM
			PLANNED_APPOINTMENTS AS PA
		WHERE
			JournalId = @journalId
			AND PA.ISPENDING = 1
			AND PA.APPOINTMENTSTATUS = 'Not Started'			
	) AS PA
		ON PCT.COMPTRADEID = PA.COMPTRADEID
WHERE
	PLANNED_JOURNAL.JOURNALID = @journalId
	AND PCT.COMPTRADEID NOT IN
	(
		SELECT
			COMPTRADEID
		FROM
			PLANNED_REMOVED_SCHEDULING_TRADES
		WHERE
			JOURNALID = @journalId
	)
END