USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_UpdatePlannedAppointmentNotes @appointmentId
-- Author:		Ahmed Mehmood
-- Create date: <12/4/2013>
-- Last Modified: <12/4/2013>
-- Description:	<Update notes of an appointment. >
-- Webpage : ViewArrangedAppointments.aspx

-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_UpdatePlannedAppointmentNotes]
	@appointmentId int
	,@customerNotes varchar(1000)
	,@jobsheetNotes varchar(1000)
	,@isSaved int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	UPDATE	PLANNED_APPOINTMENTS
	SET		CUSTOMERNOTES = @customerNotes
			,APPOINTMENTNOTES = @jobsheetNotes 
	WHERE	APPOINTMENTID = @appointmentId 

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END