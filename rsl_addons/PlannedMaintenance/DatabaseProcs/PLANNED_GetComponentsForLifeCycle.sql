USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,10/8/2013>
-- Description:	<Description,Get Components For LifeCycle,>
--DECLARE	@totalCount int
--EXEC	[dbo].[PLANNED_GetComponentsForLifeCycle]
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = 'SOrder',
--		@sortOrder = 'DESC',
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetComponentsForLifeCycle]
	-- Add the parameters for the stored procedure here
		@pageSize int = 40,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'SOrder',
		@sortOrder varchar (5) = 'ASC',
		@totalCount int = 0 output
AS
BEGIN

	DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),       	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),   
        
        --variables for paging
        @offset int,
		@limit int		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT DISTINCT TOP ('+convert(varchar(10),@limit)+') pc.COMPONENTID , pc.COMPONENTNAME ,pc.EDITEDBY,CONVERT(VARCHAR(10), pc.EDITEDON, 103) + '' '' + LEFT(CONVERT(VARCHAR, pc.EDITEDON, 108), 5) EDITEDON ,LEFT(e.FIRSTNAME, 1)+'' ''+e.LASTNAME USERNAME,pc.Sorder as SOrder '
		
		-- End building SELECT clause
		--======================================================================================== 							
	--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'		
		FROM	PLANNED_COMPONENT pc INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = pc.EDITEDBY '
		-- End building From clause
		--======================================================================================== 							
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'COMPONENTNAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'COMPONENTNAME' 		
		END
		
		IF(@sortColumn = 'EDITEDBY')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'USERNAME' 		
		END
		
		IF(@sortColumn = 'EDITEDON')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'EDITEDON' 		
		END				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================					
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause  + @orderClause 
			print(@mainSelectQuery)
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
	
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================		
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT  @totalCount = COUNT(*) ' + @fromClause 
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END 
GO

