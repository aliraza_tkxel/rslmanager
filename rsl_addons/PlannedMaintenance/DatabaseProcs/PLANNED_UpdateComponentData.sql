-- Stored Procedure

-- =============================================
-- EXEC PL_UpdateComponentData 
--@componentId = 1,
--@frequency = 1,
--@cycle = 10,
--@labourCost = 10.5,
--@materialCost = 100.5
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/03/2014>
-- Description:	<Description,,Update the parameters value>
-- WebPage: AnnualSpendReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_UpdateComponentData]
@componentId int,
@frequency varchar(50),
@cycle int,
@labourCost decimal,
@materialCost decimal,
@editedBy int,
@selectedScheme int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @startYear int
	Set @startYear = DATEPART(YEAR, GETDATE())

	Drop Table tempPlannedComponent
	select * into tempPlannedComponent from PLANNED_COMPONENT 

    -- Insert statements for procedure here
	Update tempPlannedComponent Set FREQUENCY = @frequency,CYCLE=@cycle,LABOURCOST=@labourCost
	,MATERIALCOST=@materialCost, EDITEDON = GETDATE(), EDITEDBY = @editedBy
	Where COMPONENTID = @componentId

	Select tblCompoName.COMPONENTNAME,'< '+ tblComponent.yr as yr,ISNULL(tblComponent.totalProperties,0) as PropertyCount ,ISNULL(tblComponent.cost,0) as cost
,ISNULL(tblCurrentComponent.totalProperties,0) as PropertyCount,ISNULL(tblCurrentComponent.cost,0) as Cost,@startYear as yr
,ISNULL(tblNextComponent.totalProperties,0) as PropertyCount,ISNULL(tblNextComponent.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,1, GETDATE())) as yr
,ISNULL(tbl2016Component.totalProperties,0) as PropertyCount,ISNULL(tbl2016Component.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,2, GETDATE())) as yr
,ISNULL(tblnext2016Component.totalProperties,0) as PropertyCount,ISNULL(tblnext2016Component.cost,0) as cost,DATEPART(YEAR, DateAdd(YEAR,3, GETDATE())) as yr
From
	(Select COMPONENTNAME,COMPONENTID from PLANNED_COMPONENT) as tblCompoName
	Left outer Join  
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	Where DATEPART(year,DueDate) < @startYear AND P_SCHEME.SCHEMEID  = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblComponent
	on tblCompoName.COMPONENTID = tblComponent.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	Where DATEPART(year,DueDate) = @startYear AND P_SCHEME.SCHEMEID  = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) AS tblCurrentComponent 
	on tblComponent.COMPONENTID = tblCurrentComponent.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	Where DATEPART(year,DueDate) = DATEADD(year,1, @startYear) AND P_SCHEME.SCHEMEID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblNextComponent
	on tblNextComponent.COMPONENTID = tblCompoName.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	Where DATEPART(year,DueDate) = DATEADD(year,2, @startYear) AND P_SCHEME.SCHEMEID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tbl2016Component
	on tbl2016Component.COMPONENTID = tblCompoName.COMPONENTID
	Left outer JOIN 
	(Select Count(PA_PROPERTY_ITEM_DATES.PROPERTYID) as totalProperties,dbo.PLANNED_fnDecimalToCurrency(SUM(MATERIALCOST+LABOURCOST)) as cost,CONVERT(varchar(10),@startYear) as yr  
	, PLANNED_COMPONENT.COMPONENTID
	from PLANNED_COMPONENT
	INNER JOIN PA_PROPERTY_ITEM_DATES  on PLANNED_COMPONENT.COMPONENTID = PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID
	INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = PA_PROPERTY_ITEM_DATES.PROPERTYID
	INNER JOIN P_SCHEME on P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID 
	Where DATEPART(year,DueDate) = DATEADD(year,3, @startYear) AND P_SCHEME.SCHEMEID = @selectedScheme
	Group By PLANNED_COMPONENT.COMPONENTID) as tblnext2016Component
	on tblnext2016Component.COMPONENTID = tblCompoName.COMPONENTID
END
GO