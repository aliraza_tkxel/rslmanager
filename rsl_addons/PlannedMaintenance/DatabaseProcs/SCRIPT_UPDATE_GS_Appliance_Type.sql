/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	18 April 2014
--  Description:	Update GS_Appliance_Type related to Appliances tab parameter values.
--					
 '==============================================================================*/


UPDATE	GS_Appliance_Type
SET		ISACTIVE = 0
WHERE	APPLIANCETYPE NOT IN ('Combi Boiler','Cooker','Fire','Traditional Boiler','Wall Heater')

UPDATE	GS_Appliance_Type
SET		ISACTIVE = 0
WHERE	APPLIANCETYPEID = 81
