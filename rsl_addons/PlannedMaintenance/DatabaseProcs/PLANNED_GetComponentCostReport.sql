USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetComponentCostReport]    Script Date: 01/11/2016 12:41:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Exec PL_GetComponentCostReport
-- Author:    	<Author,,Salman Nazir>
-- Create date: <Create Date,,12/03/2014>
-- Description:	<Description,,get the cost component report data.>
-- WebPage: AnnualSpendReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetComponentCostReport] (@startYear varchar(15),
@selectedScheme int,
@searchText varchar(100),
@FLAG bit
, @dueDateDt AS PLANNED_PROPERTY_REPLACEMENT_DUEDATES READONLY
, @editedComponentsDt AS PLANNED_EDITED_COMPONENTS READONLY)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
  --Declare @currentyear int
  --SET @currentyear = (select DATEPART(year,getdate()))

  --=========================================================
  -- CONVERT VARIABLE TABLES TO #TEMP TABLES FOR MANIPULATION
  --=========================================================

  CREATE TABLE
  #TMP_EDITED_COMPONENTS (
    TMP_COMPONENTID smallint,
    TMP_COMPONENTNAME nvarchar(100),
    TMP_CYCLE int,
    TMP_FREQUENCY nvarchar(10),
    TMP_MATERIALCOST float,
    TMP_LABOURCOST float,
    TMP_COSTFLAG bit,
    TMP_CYCLEFLAG bit
  )

  INSERT INTO #TMP_EDITED_COMPONENTS (TMP_COMPONENTID, TMP_COMPONENTNAME, TMP_CYCLE, TMP_FREQUENCY, TMP_MATERIALCOST, TMP_LABOURCOST, TMP_COSTFLAG, TMP_CYCLEFLAG)
    SELECT
      COMPONENTID,
      COMPONENTNAME,
      CYCLE,
      FREQUENCY,
      MATERIALCOST,
      LABOURCOST,
      COSTFLAG,
      CYCLEFLAG
    FROM @editedComponentsDt

  CREATE TABLE
  #TMP_UPDATED_PROPERTY_DUES (
    TMP_SID int,
    TMP_DUEDATE varchar(10)
  )

  INSERT INTO #TMP_UPDATED_PROPERTY_DUES (TMP_SID, TMP_DUEDATE)
    SELECT
      SID,
      DUEDATE
    FROM @dueDateDt

  --=========================================================


  -- Insert statements for procedure here
  IF @FLAG = 0
  BEGIN
    SELECT
      tblCompoName.COMPONENTID,
      tblCompoName.COMPONENTNAME,
      '-1' AS Operator,
      ISNULL(tblComponent.yr, @startYear) AS yr,
      ISNULL(tblComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblComponent.cost, 0) AS cost,
      tblComponent.isEdited,
      '0' AS Operator2,
      ISNULL(tblCurrentComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblCurrentComponent.cost, 0) AS Cost,
      @startYear AS yr,
      tblCurrentComponent.isEdited,
      '0' AS Operator3,
      ISNULL(tblNextComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblNextComponent.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 1, GETDATE())) AS yr,
      tblNextComponent.isEdited,
      '0' AS Operator4,
      ISNULL(tbl2016Component.totalProperties, 0) AS PropertyCount,
      ISNULL(tbl2016Component.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 2, GETDATE())) AS yr,
      tbl2016Component.isEdited,
      '0' AS Operator5,
      ISNULL(tblnext2016Component.totalProperties, 0) AS PropertyCount,
      ISNULL(tblnext2016Component.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 3, GETDATE())) AS yr,
      tblnext2016Component.isEdited
    FROM (SELECT
          COMPONENTNAME,
          COMPONENTID,
          SOrder
        FROM PLANNED_COMPONENT
        WHERE ISACCOUNTING = 1) AS tblCompoName
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
              INNER JOIN PLANNED_COMPONENT_ITEM ON PID.PLANNED_COMPONENTID = PLANNED_COMPONENT_ITEM.COMPONENTID
          WHERE DATEPART(YEAR, PID.DueDate) < CONVERT(int, @startYear)
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblComponent ON tblCompoName.COMPONENTID = tblComponent.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear)
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblCurrentComponent ON tblComponent.COMPONENTID = tblCurrentComponent.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 1
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblNextComponent ON tblNextComponent.COMPONENTID = tblCompoName.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 2
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tbl2016Component ON tbl2016Component.COMPONENTID = tblCompoName.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 3
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblnext2016Component ON tblnext2016Component.COMPONENTID = tblCompoName.COMPONENTID
    WHERE tblCompoName.COMPONENTNAME LIKE '%' + @searchText + '%'
    ORDER BY tblCompoName.SOrder ASC
  END
  ELSE
  BEGIN
    SELECT
      tblCompoName.COMPONENTID,
      tblCompoName.COMPONENTNAME,
      '0' AS Operator,
      ISNULL(tblComponent.yr, @startYear) AS yr,
      ISNULL(tblComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblComponent.cost, 0) AS cost,
      tblComponent.isEdited,
      '0' AS Operator2,
      ISNULL(tblCurrentComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblCurrentComponent.cost, 0) AS Cost,
      DATEPART(YEAR, DATEADD(YEAR, 1, @startYear)) AS yr,
      tblCurrentComponent.isEdited,
      '0' AS Operator3,
      ISNULL(tblNextComponent.totalProperties, 0) AS PropertyCount,
      ISNULL(tblNextComponent.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 2, @startYear)) AS yr,
      tblNextComponent.isEdited,
      '0' AS Operator4,
      ISNULL(tbl2016Component.totalProperties, 0) AS PropertyCount,
      ISNULL(tbl2016Component.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 3, @startYear)) AS yr,
      tbl2016Component.isEdited,
      '0' AS Operator5,
      ISNULL(tblnext2016Component.totalProperties, 0) AS PropertyCount,
      ISNULL(tblnext2016Component.cost, 0) AS cost,
      DATEPART(YEAR, DATEADD(YEAR, 4, @startYear)) AS yr,
      tblnext2016Component.isEdited
    FROM (SELECT
          COMPONENTNAME,
          COMPONENTID,
          SOrder
        FROM PLANNED_COMPONENT
        WHERE ISACCOUNTING = 1) AS tblCompoName
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear)
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblComponent ON tblCompoName.COMPONENTID = tblComponent.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 1
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblCurrentComponent ON tblComponent.COMPONENTID = tblCurrentComponent.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 2
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblNextComponent ON tblNextComponent.COMPONENTID = tblCompoName.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 3
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tbl2016Component ON tbl2016Component.COMPONENTID = tblCompoName.COMPONENTID
        LEFT OUTER JOIN (SELECT
            COUNT(PID.PROPERTYID) AS totalProperties,
            dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) AS cost,
            CONVERT(varchar(10), @startYear) AS yr,
            PLANNED_COMPONENT.COMPONENTID,
            ISNULL(MAX(isEdited), 0) AS isEdited
          FROM PLANNED_COMPONENT
              INNER JOIN (SELECT DISTINCT
                  PID_IN.SID AS SID,
                  PID_IN.PROPERTYID AS PROPERTYID,
                  PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL AND
                      TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN CASE
                        WHEN TEC.TMP_CYCLEFLAG = 1 THEN CASE
                            WHEN TEC.TMP_FREQUENCY = 'Years' THEN DATEADD(YYYY, TEC.TMP_CYCLE, PID_IN.LastDone)
                            ELSE DATEADD(MM, TEC.TMP_CYCLE, PID_IN.LastDone)
                          END
                        ELSE PID_IN.DueDate
                      END
                    WHEN TUPD.TMP_SID IS NOT NULL THEN CONVERT(datetime, TUPD.TMP_DUEDATE)
                    ELSE PID_IN.DueDate
                  END AS DueDate,
                  PID_IN.LastDone AS LastDone,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL THEN TEC.TMP_LABOURCOST + TEC.TMP_MATERIALCOST
                    ELSE PC.LABOURCOST + PC.MATERIALCOST
                  END AS Cost,
                  CASE
                    WHEN TEC.TMP_COMPONENTID IS NOT NULL OR
                      TUPD.TMP_SID IS NOT NULL THEN 1
                    ELSE 0
                  END AS isEdited
                FROM PA_PROPERTY_ITEM_DATES AS PID_IN
                    INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
                    INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID
                    LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID
                    LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
                    LEFT JOIN (SELECT DISTINCT
                        PPA_NUM.PROPERTYID,
                        PIP_NUM.ItemId,
                        PPA_NUM.PARAMETERVALUE
                      FROM PA_PROPERTY_ATTRIBUTES PPA_NUM
                          INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
                          INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID
                      WHERE PP_NUM.ParameterName = 'Number') PNP ON PCI.ITEMID = PNP.ItemId
                        AND PID_IN.PROPERTYID = PNP.PROPERTYID

                 LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE '%windows%' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN ('0', 'Not Applicable') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1) AS PID ON PLANNED_COMPONENT.COMPONENTID = PID.PLANNED_COMPONENTID
              INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PID.PROPERTYID
              INNER JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID
          WHERE DATEPART(YEAR, PID.DueDate) = CONVERT(int, @startYear) + 4
            AND (@selectedScheme = -1
            OR P_SCHEME.SCHEMEID = @selectedScheme)
          GROUP BY PLANNED_COMPONENT.COMPONENTID) AS tblnext2016Component ON tblnext2016Component.COMPONENTID = tblCompoName.COMPONENTID
    WHERE tblCompoName.COMPONENTNAME LIKE '%' + @searchText + '%'
    ORDER BY tblCompoName.SOrder ASC
  END
  DROP TABLE #TMP_EDITED_COMPONENTS
  DROP TABLE #TMP_UPDATED_PROPERTY_DUES

END
