--====================================
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,8th Dec,2013>
-- Description:	<Description,,This trigger 'll insert the reocord in planned_appointments_history after insertion in planned_appointments
--====================================
ALTER TRIGGER PLANNED_AFTER_INSERT_PLANNED_APPAOINTMENTS  
 ON PLANNED_APPOINTMENTS   
AFTER INSERT  
AS  
BEGIN  	
	INSERT INTO PLANNED_APPOINTMENTS_HISTORY
           ([APPOINTMENTID]
           ,[TENANCYID]
           ,[JournalId]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]
           ,[CUSTOMERNOTES]
           ,[ISPENDING]
           ,[JOURNALSUBSTATUS]
           ,[APPOINTMENTALERT]
           ,[APPOINTMENTCALENDER]
           ,[SURVEYOURSTATUS]
           ,[APPOINTMENTSTATUS]
           ,[SURVEYTYPE]
           ,[COMPTRADEID]
           ,[isMiscAppointment]
           ,[DURATION])
           SELECT 
            i.[APPOINTMENTID]
           ,i.[TENANCYID]
           ,i.[JournalId]
           ,i.[JOURNALHISTORYID]
           ,i.[APPOINTMENTDATE]
           ,i.[APPOINTMENTENDDATE]
           ,i.[APPOINTMENTSTARTTIME]
           ,i.[APPOINTMENTENDTIME]
           ,i.[ASSIGNEDTO]
           ,i.[CREATEDBY]
           ,i.[LOGGEDDATE]
           ,i.[APPOINTMENTNOTES]
           ,i.[CUSTOMERNOTES]
           ,i.[ISPENDING]
           ,i.[JOURNALSUBSTATUS]
           ,i.[APPOINTMENTALERT]
           ,i.[APPOINTMENTCALENDER]
           ,i.[SURVEYOURSTATUS]
           ,i.[APPOINTMENTSTATUS]
           ,i.[SURVEYTYPE]
           ,i.[COMPTRADEID]
           ,i.[isMiscAppointment]
           ,i.[DURATION]
           FROM INSERTED i                         
 END    
