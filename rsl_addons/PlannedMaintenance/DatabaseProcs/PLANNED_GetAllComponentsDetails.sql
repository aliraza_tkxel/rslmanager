/****** Object:  StoredProcedure [dbo].[PL_GetAllComponentsDetails]    Script Date: 04/07/2014 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec PL_GetAllComponentsDetails
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,10/03/2014>
-- Description:	<Description,,Get all the components to edit the values>
-- WebPage: AnnualSpendReport -> Edit Parameters
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetAllComponentsDetails]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select COMPONENTID,ComponentName as COMPONENT,CYCLE,
	(CASE 
	 When FREQUENCY = 'yrs'Then
	 'Years'
	 When FREQUENCY = 'mnths' Then
	 'Months'
	 End
	) as FREQUENCY,MATERIALCOST,LABOURCOST from PLANNED_COMPONENT
END
