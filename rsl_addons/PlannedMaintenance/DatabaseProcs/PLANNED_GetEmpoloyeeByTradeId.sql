USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetFromTelEmailPrintLetterByEmployeeID]    Script Date: 12/13/2013 00:30:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--	EXEC PLANNED_GetEmpoloyeeByTradeId
	@tradeId = 1
-- 	Author:		Noor Muhammad
-- 	Create date: 13 Dec, 2013
-- 	Description:	This procedure 'll return the operatives that exist against trades
-- 	Web Page: PrintLetter.aspx
-- =============================================*/
CREATE PROCEDURE [dbo].[PLANNED_GetEmpoloyeeByTradeId]
	
	@tradeId int
AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT 
		E__EMPLOYEE.FIRSTNAME +' '+E__EMPLOYEE.LASTNAME as EmployeeName
		,EMPLOYEEID as EmployeeId 
	From E_TRADE
	INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = E_TRADE.EmpId 
	WHERE 
	E_TRADE.TradeId =@tradeId
		
END
