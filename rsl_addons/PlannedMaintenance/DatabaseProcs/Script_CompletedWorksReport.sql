/* ==============================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	4 Feb 2016 
--  Description:	Script for changes mentions in Ticket # 9571
 '==============================================================================*/


--===============================================================================
-- RENAME COMPLETED REPORT
--===============================================================================
IF NOT EXISTS(	SELECT	* 
				FROM	AC_PAGES 
				WHERE	AC_PAGES.DESCRIPTION = 'Job Sheet Status')
BEGIN

	UPDATE	AC_PAGES
	SET		DESCRIPTION = 'Job Sheet Status'
	WHERE	DESCRIPTION = 'Completed Works'     
	
END       
GO

--===============================================================================
-- INSERT Job Sheet Status Detail page
--===============================================================================

DECLARE @PLANNED_MENUID INT
DECLARE @PLANNED_JOBSHEETSTATUS_PAGEID INT 

SELECT	@PLANNED_MENUID = MENUID
FROM	AC_MENUS 
WHERE	DESCRIPTION = 'Planned'

SELECT	@PLANNED_JOBSHEETSTATUS_PAGEID = PAGEID
FROM	AC_PAGES 
WHERE	DESCRIPTION = 'Job Sheet Status'
	
IF NOT EXISTS(	SELECT	* 
				FROM	AC_PAGES 
				WHERE	AC_PAGES.DESCRIPTION = 'View Job Sheet Status Detail')
BEGIN	
	INSERT INTO [AC_PAGES]([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
	VALUES ('View Job Sheet Status Detail',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ViewArrangedAppointments.aspx?src=rptcmpt','8',3,@PLANNED_JOBSHEETSTATUS_PAGEID,'',0)
END 
     
   
--===============================================================================
-- INSERT MISSING APPOINTMENT TYPE ID FOR LEGACY MISC APPOINTMENTS
--=============================================================================== 

DECLARE	@MISC_APT_TYPE_ID INT
SELECT	@MISC_APT_TYPE_ID = Planned_Appointment_TypeId 
FROM	Planned_Appointment_Type
WHERE	Planned_Appointment_Type = 'Misc'

UPDATE	PLANNED_APPOINTMENTS
SET		Planned_Appointment_TypeId = @MISC_APT_TYPE_ID
WHERE	Planned_Appointment_TypeId IS NULL AND isMiscAppointment = 1

--===============================================================================
-- INSERT MISSING APPOINTMENT TYPE ID FOR LEGACY PLANNED APPOINTMENTS
--=============================================================================== 

DECLARE	@PLANNED_APT_TYPE_ID INT
SELECT	@PLANNED_APT_TYPE_ID = Planned_Appointment_TypeId 
FROM	Planned_Appointment_Type
WHERE	Planned_Appointment_Type = 'Planned'

UPDATE	PLANNED_APPOINTMENTS
SET		Planned_Appointment_TypeId = @PLANNED_APT_TYPE_ID
WHERE	Planned_Appointment_TypeId IS NULL OR  Planned_Appointment_TypeId = 0
