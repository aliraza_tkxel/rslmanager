USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_AddStandardLetter]    Script Date: 12/07/2013 19:58:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noor Muhammad
-- Create date: 05/11/2013
-- Description:	This stored procedure adds a standard letter into the database. 
-- Web Page: CreateLetter.aspx
-- EXEC [dbo].[PLANNED_AddStandardLetter]
--		@StatusId = 1,
--		@ActionId = 1,
--		@Title = N'Title',
--		@Code = N'1',
--		@Body = N'Body',
--		@CreatedBy = 615,
--		@ModifiedBy = 615
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_AddStandardLetter]
	-- Add the parameters for the stored procedure here
	@StatusId			int,
	@ActionId			int,
	@Title				varchar(50),
	@Code				varchar(50),
	@Body				varchar(MAX),
	@CreatedBy			int,
	@ModifiedBy			int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PLANNED_StandardLetters]
           ([StatusId]
           ,[ActionId]
           ,[Title]
           ,[Code]
           ,[Body]
           ,[CreatedBy]
           ,[ModifiedBy]
           ,[CreatedDate]
           ,[ModifiedDate]
           ,[SignOffLookupCode]
           ,[TeamId]
           ,[FromResourceId]
           ,[IsPrinted]
           ,[IsActive])
     VALUES
           (@StatusId,
           @ActionId,
           @Title,
           @Code,
           @Body,
           @CreatedBy,
           @ModifiedBy,
           GETDATE(),
           GETDATE(),
           null,
           null,
           null,
           0,
           1)
END
