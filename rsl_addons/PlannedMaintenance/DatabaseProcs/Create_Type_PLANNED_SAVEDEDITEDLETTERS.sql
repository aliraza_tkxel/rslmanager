USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNED_SAVEDEDITEDLETTERS]    Script Date: 12/07/2013 20:07:30 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PLANNED_SAVEDEDITEDLETTERS' AND ss.name = N'dbo')
DROP TYPE [dbo].[PLANNED_SAVEDEDITEDLETTERS]
GO

USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNED_SAVEDEDITEDLETTERS]    Script Date: 12/07/2013 20:07:30 ******/
CREATE TYPE [dbo].[PLANNED_SAVEDEDITEDLETTERS] AS TABLE(
	[LETTERCONTENT] [nvarchar](max) NULL,
	[LETTERID] [int] NOT NULL,
	[LETTERTITLE] [varchar](max) NULL,
	[TEAMID] [int] NULL,
	[FromResourceId] [int] NULL,
	[SignOffLookupCode] [int] NULL,
	[RentBalance] [float] NULL,
	[RentCharge] [float] NULL,
	[TodayDate] [date] NULL
)
GO


