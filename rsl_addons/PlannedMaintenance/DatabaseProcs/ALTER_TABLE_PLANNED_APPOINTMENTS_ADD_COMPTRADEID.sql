/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	6th November 2013
--  Description:	COMPTRADEID is added to identify the exact trade of an appointment
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE PLANNED_APPOINTMENTS
ADD COMPTRADEID INT

ALTER TABLE PLANNED_APPOINTMENTS
ADD CONSTRAINT FK_COMPTRADEID
FOREIGN KEY (COMPTRADEID)
REFERENCES PLANNED_COMPONENT_TRADE(COMPTRADEID)

