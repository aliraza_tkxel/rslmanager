USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetStandardLetterById]    Script Date: 12/07/2013 19:46:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noor Muhammad
-- Create date: 04/11/2013
-- Description:	This SP returns a standard letter based on an ID
-- Useage: view Letter
-- EXEC: [dbo].[PLANNED_GetStandardLetterByID]
--			@StandardLetterID=1
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetStandardLetterById]
	-- Add the parameters for the stored procedure here
	@StandardLetterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		PLANNED_StandardLetters.StandardLetterId,
		PLANNED_StandardLetters.StatusId,
		PLANNED_StandardLetters.ActionId,
		PLANNED_StandardLetters.Title,
		PLANNED_StandardLetters.Code,
		PLANNED_StandardLetters.Body
	FROM
		PLANNED_StandardLetters
	WHERE
		PLANNED_StandardLetters.StandardLetterId=@StandardLetterID
			
END
