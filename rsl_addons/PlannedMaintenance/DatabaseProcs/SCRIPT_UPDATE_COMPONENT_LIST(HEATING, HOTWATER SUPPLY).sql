/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	16 April 2014
--  Description:	Update the component list.
--					
 '==============================================================================*/

--======================================================================
-- DELETE DEPRECATED COMPONENTS
--19 ) Heating system - Radiators
--20 ) Heating system - Gas fires
--21 ) Heating system - HVAC
--22 ) Heating system - Underfloor
--23 ) Heating system - Night storage
--24 ) Heating system - Air source
--25 ) Heating system - Solar
--26 ) Heating system - Warm air
--27 ) Heating system - Ground source heat pump

--28 ) Emitters Replacement
--======================================================================

-- PLANNED_COMPONENT_HISTORY
DELETE 
FROM PLANNED_COMPONENT_HISTORY
WHERE PLANNED_COMPONENT_HISTORY.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')
)

-- PLANNED_NOENTRY
DELETE 
FROM PLANNED_NOENTRY
WHERE PLANNED_NOENTRY.APPOINTMENTID IN 
	(SELECT PLANNED_APPOINTMENTS.APPOINTMENTID
	FROM PLANNED_APPOINTMENTS
	WHERE PLANNED_APPOINTMENTS.JournalId  IN
		(SELECT PLANNED_JOURNAL.JOURNALID 
		FROM PLANNED_JOURNAL
		WHERE PLANNED_JOURNAL.COMPONENTID IN
			(SELECT PLANNED_COMPONENT.COMPONENTID  
			FROM PLANNED_COMPONENT 
			WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement'
									)  
			)
		)
	)

-- PLANNED_JOBTIMESHEET
DELETE 
FROM PLANNED_JOBTIMESHEET
WHERE PLANNED_JOBTIMESHEET.APPOINTMENTID IN 
	(SELECT PLANNED_APPOINTMENTS.APPOINTMENTID
	FROM PLANNED_APPOINTMENTS
	WHERE PLANNED_APPOINTMENTS.JournalId  IN
		(SELECT PLANNED_JOURNAL.JOURNALID 
		FROM PLANNED_JOURNAL
		WHERE PLANNED_JOURNAL.COMPONENTID IN
			(SELECT PLANNED_COMPONENT.COMPONENTID  
			FROM PLANNED_COMPONENT 
			WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement'
									)  
			)
		)
	)

-- PLANNED_PAUSED
DELETE 
FROM PLANNED_PAUSED
WHERE PLANNED_PAUSED.APPOINTMENTID IN 
	(SELECT PLANNED_APPOINTMENTS.APPOINTMENTID
	FROM PLANNED_APPOINTMENTS
	WHERE PLANNED_APPOINTMENTS.JournalId  IN
		(SELECT PLANNED_JOURNAL.JOURNALID 
		FROM PLANNED_JOURNAL
		WHERE PLANNED_JOURNAL.COMPONENTID IN
			(SELECT PLANNED_COMPONENT.COMPONENTID  
			FROM PLANNED_COMPONENT 
			WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement'
									)  
			)
		)
	)

-- PLANNED_APPOINTMENTS_HISTORY
DELETE 
FROM PLANNED_APPOINTMENTS_HISTORY
WHERE PLANNED_APPOINTMENTS_HISTORY.JournalId  IN
(SELECT PLANNED_JOURNAL.JOURNALID 
FROM PLANNED_JOURNAL
WHERE PLANNED_JOURNAL.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
))

-- PLANNED_APPOINTMENTS
DELETE 
FROM PLANNED_APPOINTMENTS
WHERE PLANNED_APPOINTMENTS.JournalId  IN
(SELECT PLANNED_JOURNAL.JOURNALID 
FROM PLANNED_JOURNAL
WHERE PLANNED_JOURNAL.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
))

-- PLANNED_COMPONENT_TRADE
DELETE 
FROM PLANNED_COMPONENT_TRADE
WHERE PLANNED_COMPONENT_TRADE.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
)

-- PLANNED_JOURNAL_HISTORY
DELETE 
FROM PLANNED_JOURNAL_HISTORY
WHERE PLANNED_JOURNAL_HISTORY.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
)

-- PLANNED_INSPECTION_APPOINTMENTS
DELETE 
FROM PLANNED_INSPECTION_APPOINTMENTS
WHERE PLANNED_INSPECTION_APPOINTMENTS.JournalId  IN
(SELECT PLANNED_JOURNAL.JOURNALID 
FROM PLANNED_JOURNAL
WHERE PLANNED_JOURNAL.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
))

-- PLANNED_JOURNAL
DELETE 
FROM PLANNED_JOURNAL
WHERE PLANNED_JOURNAL.COMPONENTID IN
(
SELECT PLANNED_COMPONENT.COMPONENTID  
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
)

-- PA_PROPERTY_ITEM_DATES
UPDATE	PA_PROPERTY_ITEM_DATES
SET		PLANNED_COMPONENTID = NULL
WHERE	PLANNED_COMPONENTID IN
(
SELECT	PLANNED_COMPONENT.COMPONENTID  
FROM	PLANNED_COMPONENT 
WHERE	COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
) 

-- PLANNED_COMPONENT_ITEM
DELETE 
FROM	PLANNED_COMPONENT_ITEM
WHERE	COMPONENTID IN 
(
SELECT	PLANNED_COMPONENT.COMPONENTID  
FROM	PLANNED_COMPONENT 
WHERE	COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  
)

-- PLANNED_COMPONENT
DELETE 
FROM PLANNED_COMPONENT 
WHERE COMPONENTNAME IN ('Heating system - Radiators','Heating system - Gas fires','Heating system - HVAC','Heating system - Underfloor','Heating system - Night storage','Heating system - Air source','Heating system - Solar','Heating system - Warm air','Heating system - Ground source heat pump','Emitters Replacement')  


--======================================================================
-- ADD NEW COMPONENTS
--19 ) Heating - Air Source
--20 ) Heating - Ground Source
--21 ) Heating - LPG - Combi Boiler
--22 ) Heating - LPG - Fire
--23 ) Heating - LPG - Traditional Boiler
--24 ) Heating - LPG - Wall Heater
--25 ) Heating - Main Electric - Wall Heater
--26 ) Heating - Main Gas - Combi Boiler
--27 ) Heating - Main Gas - Fire
--28 ) Heating - Main Gas - Traditional Boiler
--29 ) Heating - Main Gas - Wall Heater
--30 ) Heating - Oil - Traditional Boiler
--31 ) Heating - Solar
--32 ) Heating - Wind
--33 ) Emitters - Radiators
--34 ) Emitters - Gas Fire
--35 ) Emitters - Under Floor
--36 ) Emitters - Night Storage
--======================================================================

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Air Source',1,10,'yrs',2000,1000,760,GETDATE(),19)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Ground Source',1,10,'yrs',2000,1000,760,GETDATE(),20)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - LPG - Combi Boiler',1,10,'yrs',2000,1000,760,GETDATE(),21)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - LPG - Fire',1,10,'yrs',2000,1000,760,GETDATE(),22)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - LPG - Traditional Boiler',1,10,'yrs',2000,1000,760,GETDATE(),23)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - LPG - Wall Heater',1,10,'yrs',2000,1000,760,GETDATE(),24)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Main Electric - Wall Heater',1,10,'yrs',2000,1000,760,GETDATE(),25)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Main Gas - Combi Boiler',1,10,'yrs',2000,1000,760,GETDATE(),26)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Main Gas - Fire',1,10,'yrs',2000,1000,760,GETDATE(),27)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Main Gas - Traditional Boiler',1,10,'yrs',2000,1000,760,GETDATE(),28)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Main Gas - Wall Heater',1,10,'yrs',2000,1000,760,GETDATE(),29)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Oil - Traditional Boiler',1,10,'yrs',2000,1000,760,GETDATE(),30)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Solar',1,10,'yrs',2000,1000,760,GETDATE(),31)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Heating - Wind',1,10,'yrs',2000,1000,760,GETDATE(),32)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Emitters - Radiators',1,10,'yrs',2000,1000,760,GETDATE(),33)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Emitters - Gas Fire',1,10,'yrs',2000,1000,760,GETDATE(),34)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Emitters - Under Floor',1,10,'yrs',2000,1000,760,GETDATE(),35)

INSERT INTO [PLANNED_COMPONENT]([COMPONENTNAME],[ISACCOUNTING],[CYCLE],[FREQUENCY],[MATERIALCOST],[LABOURCOST],[EDITEDBY],[EDITEDON],[SOrder])
VALUES ('Emitters - Night Storage',1,10,'yrs',2000,1000,760,GETDATE(),36)

--======================================================================
-- UPDATE SORT ORDER OF EXISTING COMPONENTS
--37 ) Bathroom
--38 ) Kitchen
--39 ) Separate W/C
--======================================================================

UPDATE PLANNED_COMPONENT SET SOrder =37  WHERE PLANNED_COMPONENT.COMPONENTNAME ='Bathroom'
UPDATE PLANNED_COMPONENT SET SOrder =38  WHERE PLANNED_COMPONENT.COMPONENTNAME ='Kitchen'
UPDATE PLANNED_COMPONENT SET SOrder =39  WHERE PLANNED_COMPONENT.COMPONENTNAME ='Separate W/C'

--======================================================================
-- UPDATE NAMES OF EXISTING COMPONENTS
--15 ) Hot water supply - Mains
--16 ) Hot water supply - Gas
--17 ) Hot water supply - Electric
--18 ) Hot water supply - Cylinder
--======================================================================

UPDATE PLANNED_COMPONENT SET COMPONENTNAME ='Hot water supply - Mains' WHERE COMPONENTNAME ='Water heater - Mains'
UPDATE PLANNED_COMPONENT SET COMPONENTNAME ='Hot water supply - Gas' WHERE COMPONENTNAME ='Water heater - Gas'
UPDATE PLANNED_COMPONENT SET COMPONENTNAME ='Hot water supply - Electric' WHERE COMPONENTNAME ='Water heater - Electric'
UPDATE PLANNED_COMPONENT SET COMPONENTNAME ='Hot water supply - Cylinder' WHERE COMPONENTNAME ='Water heater - Cylinder'