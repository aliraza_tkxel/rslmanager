/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	17 July 2014
--  Description:	Insertion scripts for PLANNED_REASON 
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

INSERT INTO [PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
VALUES ('Work not required',1,760,GETDATE(),NULL,NULL)

INSERT INTO [PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
VALUES ('To be included in the next programme',1,760,GETDATE(),NULL,NULL)

INSERT INTO [PLANNED_REASON]([Reason],[IsEditable],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
VALUES ('Work completed as a Reactive Repair',1,760,GETDATE(),NULL,NULL)


END TRY 

	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 