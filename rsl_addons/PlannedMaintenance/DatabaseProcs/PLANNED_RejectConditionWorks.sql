USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_RejectConditionWorks]    Script Date: 08/04/2014 11:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_RejectConditionWorks 
-- @conditionWorkId = 1,
-- @isRejected = 1
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,21/07/2014>
-- Description:	<Description,,Reject Condition Works>
-- WebPage: ConditionRating.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_RejectConditionWorks]
@conditionWorkId INT
,@rejectionNotes NVARCHAR(4000)
,@replacementDue NVARCHAR(10)
,@reasonId INT
,@userId INT
,@isRejected INT = 0 out

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
BEGIN TRY

	DECLARE @ComponentId INT, @StatusId INT, @StatusTitle nvarchar(150), @ActionId INT,
	 @ActionTitle nvarchar(150), @PropertyId varchar(20),@JournalId INT, @ItemId INT

	IF @replacementDue = '-1'
		BEGIN
			SET @replacementDue = NULL
		END


-- ===================================================================================
	-- (1) Check component from PLANNED_CONDITIONWORKS
			-- SET @StatusId 'Rejected'
			-- SET @ActionId 'Rejected'
-- ===================================================================================
			
		SELECT	@ComponentId = pc.ComponentId ,@PropertyId = ppa.PROPERTYID , @ItemId = PIP.ItemId   
		FROM	PLANNED_CONDITIONWORKS PC
				INNER JOIN	PA_PROPERTY_ATTRIBUTES PPA ON PC.AttributeId = PPA.ATTRIBUTEID
				INNER JOIN	PA_ITEM_PARAMETER PIP ON PPA.ITEMPARAMID = PIP.ItemParamID  
		WHERE	ConditionWorksId = 	@conditionWorkId
		
	
		SELECT	@StatusId = STATUSID 
		FROM	PLANNED_STATUS
		WHERE	TITLE = 'Condition Works'
			
		SELECT	@ActionId = ActionId  
		FROM	PLANNED_Action 
		WHERE	TITLE = 'Rejected'
			
-- ===================================================================================			
	-- (2) Make an entry in PLANNED_JOURNAL, Get
		-- PropertyId from @PropertyId
		-- ComponentId from @ComponentId
		-- Statusid from @StatusId
		-- Actionid from @ActionId
		-- Current date 
		-- CreatedBy UserId from session
-- ===================================================================================
		
		INSERT PLANNED_JOURNAL ([PROPERTYID],[COMPONENTID],[STATUSID],[ACTIONID],[CREATIONDATE],[CREATEDBY])
		VALUES  (@PropertyId,@ComponentId,@StatusId,@ActionId,GETDATE(),@userId)
				
		SELECT @JournalId = SCOPE_IDENTITY()	
		
	
-- ===================================================================================	
	-- (3) Make an entry in PLANNED_JOURNAL_HISTORY
-- ===================================================================================	

		INSERT PLANNED_JOURNAL_HISTORY
		([JOURNALID],[PROPERTYID],[COMPONENTID],[STATUSID],[ACTIONID],[CREATIONDATE],[CREATEDBY],[NOTES],[ISLETTERATTACHED],[StatusHistoryId],[ActionHistoryId],[IsDocumentAttached]) 
		VALUES
		(@JournalId,@PropertyId,@ComponentId,@StatusId,@ActionId,GETDATE(),@userId,NULL,0,NULL,NULL,0)
	
	
-- ===================================================================================	
	-- (4) Make an entry in PLANNED_CONDITIONWORKS_REJECTED
-- ===================================================================================	
	
		INSERT INTO [PLANNED_CONDITIONWORKS_REJECTED]
				([ConditionWorksId],[RejectionNotes],[ReasonId],[CreatedBy],[CreatedDate])
		VALUES	(@conditionWorkId,@rejectionNotes,@reasonId,@userId,GETDATE())


-- ===================================================================================	
	-- (5) Update JournalId,ConditionAction in PLANNED_CONDITIONWORKS
-- ===================================================================================	
	
		UPDATE	PLANNED_CONDITIONWORKS
		SET		JournalId = @JournalId
				,ConditionAction = @ActionId
		WHERE	ConditionWorksId = @conditionWorkId
			
-- ===================================================================================	
	-- (6) Update in PLANNED_CONDITIONWORKS_HISTORY
-- ===================================================================================	
	
		INSERT INTO [PLANNED_CONDITIONWORKS_HISTORY]
				([ConditionWorksId],[WorksRequired],[AttributeId],[ValueId],[ConditionAction]
				,[ComponentId],[JournalId],[CreatedBy],[CreatedDate])
		SELECT	ConditionWorksId, WorksRequired, PC.AttributeId, PPA.VALUEID , PC.ConditionAction
				, PC.ComponentId, PC.JournalId, PC.CreatedBy , GETDATE()
		FROM	PLANNED_CONDITIONWORKS AS PC
				INNER JOIN PA_PROPERTY_ATTRIBUTES AS PPA ON PC.AttributeId = PPA.ATTRIBUTEID 
		WHERE	PC.ConditionWorksId = @conditionWorkId
		
-- ===================================================================================	
	-- (7) Update ReplacementDue in PA_PROPERTY_ITEM_DATES
-- ===================================================================================	

	IF(@ComponentId IS NULL)
		BEGIN
			UPDATE	PA_PROPERTY_ITEM_DATES 
			SET		DueDate  = @replacementDue
					,UPDATEDON = GETDATE()
					,UPDATEDBY = @userId
			WHERE	ItemId = @ItemId 
					AND PROPERTYID = @PropertyId
					AND PLANNED_COMPONENTID IS NULL
		END
	ELSE
		BEGIN
			UPDATE	PA_PROPERTY_ITEM_DATES 
			SET		DueDate  = @replacementDue
					,UPDATEDON = GETDATE()
					,UPDATEDBY = @userId
			WHERE	PLANNED_COMPONENTID = @ComponentId 
					AND PROPERTYID = @PropertyId		
		END	
	
END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isRejected = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isRejected = 1
 END
	
	
END
