USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNED_EDITED_COMPONENT]    Script Date: 04/10/2014 17:02:58 ******/

CREATE TYPE [dbo].[PLANNED_EDITED_COMPONENTS] AS TABLE(
	[COMPONENTID] [smallint] NOT NULL,
	[COMPONENTNAME] [nvarchar](100) NULL,
	[CYCLE] [int] NULL,
	[FREQUENCY] [nvarchar](10) NULL,
	[MATERIALCOST] [float] NULL,
	[LABOURCOST] [float] NULL
	,COSTFLAG bit null
	,CYCLEFLAG bit null
)
GO


