USE [RSLBHALive]
GO
--Created By : Ahmed Mehmood
--Description:This is table valued parameter for insertion/updation of PLANNED_JOURNAL table.
--This shall be used to insert the multiple records in table at once.
--Web Page:ReplacementList.aspx


CREATE TYPE [dbo].[PLANNED_SELECTED_REPLACEMENT_ITEM] AS TABLE(
	[COMPONENTID] [INT] NULL,
	[PROPERTYID] varchar(20) NULL
)
GO


