/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	6 Dec 2013
--  Description:	Add appointment end date column in planned_appointments & planned_appointments_history
--					
 '==============================================================================*/
 
USE [RSLBHALive]
GO

ALTER TABLE PLANNED_APPOINTMENTS
ADD APPOINTMENTENDDATE smalldatetime NULL

ALTER TABLE PLANNED_APPOINTMENTS_HISTORY
ADD APPOINTMENTENDDATE smalldatetime NULL
