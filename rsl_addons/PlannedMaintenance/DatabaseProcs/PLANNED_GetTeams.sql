USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetTeams]    Script Date: 12/07/2013 19:41:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		AliRaza
-- Create date: 07/Dec/2012
-- Description:	This SP displays all the Teams that are active
-- Useage: View Letter
-- EXEC: [dbo].[AS_GetTeams]
-- =============================================

CREATE PROCEDURE [dbo].[PLANNED_GetTeams]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		E_Team.TEAMID, 
		E_Team.TEAMNAME 
	FROM
		E_Team 
	WHERE 
		E_TEAM.ACTIVE = 1
END
