/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	11 June 2014
--  Description:	Insertion scripts for property related Menus, pages and subpages
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

DECLARE @PLANNED_MENUID INT
DECLARE @PLANNED_REPORTS_PAGEID INT,@PLANNED_CONDITION_RATING_PAGEID INT
		,@SCHEDULING_PAGEID INT, @SCHEDULE_CONDITION_WORKS_PAGEID INT
	
--======================================================================================================
--												MENUS
--======================================================================================================
	
-- GETTING MENUID OF PLANNED	
	
SELECT 	@PLANNED_MENUID	= AC_MENUS.MENUID 
FROM	AC_MENUS 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID 
WHERE	AC_MENUS.DESCRIPTION = 'Planned'
		AND AC_MODULES.DESCRIPTION = 'Property'


--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
-- GETTING PAGEID OF PLANNED REPORT PAGE

SELECT	@PLANNED_REPORTS_PAGEID = PG.PAGEID  
FROM	AC_PAGES AS PG
		INNER JOIN AC_MENUS AS MN ON PG.MENUID = MN.MENUID 
		INNER JOIN AC_MODULES AS MD ON MN.MODULEID = MD.MODULEID  
WHERE	MD.DESCRIPTION = 'Property'
		AND MN.DESCRIPTION = 'Planned'
		AND PG.DESCRIPTION = 'Reports'
		
		
SELECT	@SCHEDULING_PAGEID = PG.PAGEID  
FROM	AC_PAGES AS PG
		INNER JOIN AC_MENUS AS MN ON PG.MENUID = MN.MENUID 
		INNER JOIN AC_MODULES AS MD ON MN.MODULEID = MD.MODULEID  
WHERE	MD.DESCRIPTION = 'Property'
		AND MN.DESCRIPTION = 'Planned'
		AND PG.DESCRIPTION = 'Scheduling'
 

--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================
 	
--======================
-- Planned > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('Condition Rating',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/ConditionRating.aspx','','4',2,@PLANNED_REPORTS_PAGEID,1)
SET		@PLANNED_CONDITION_RATING_PAGEID = SCOPE_IDENTITY()

--INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
--VALUES	('Assigned to Contractor',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/AssignedToContractor.aspx','','5',2,@PLANNED_REPORTS_PAGEID)

--======================
-- Planned > Scheduling 
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('Schedule Condition Works',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Scheduling/ScheduleConditionWorks.aspx','','2',2,@SCHEDULING_PAGEID,1)
SET		@SCHEDULE_CONDITION_WORKS_PAGEID = SCOPE_IDENTITY()

UPDATE	AC_PAGES
SET		DESCRIPTION  = 'Schedule Planned Works'
WHERE	DESCRIPTION = 'Schedule Works'

UPDATE	AC_PAGES
SET		ORDERTEXT   = '3'
WHERE	DESCRIPTION = 'Misc Works'

--======================================================================================================
--												PAGES	(LEVEL 3)
--======================================================================================================

--============================================
-- Planned > Reports > Condition Rating
--============================================

--INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
--VALUES	('Condition More Detail',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Reports/ConditionMoreDetail.aspx','','1',3,@PLANNED_CONDITION_RATING_PAGEID)

--================================================
-- Planned > Scheduling > Schedule Condition Works
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('Condition Available Appointments',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ConditionAvailableAppointments.aspx','','1',3,@SCHEDULE_CONDITION_WORKS_PAGEID,0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('Condition Appointment Summary',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ConditionAppointmentSummary.aspx','','2',3,@SCHEDULE_CONDITION_WORKS_PAGEID,0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage],[DisplayInTree])
VALUES	('View Condition Arranged Appointments',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ViewConditionArrangedAppointments.aspx','','3',3,@SCHEDULE_CONDITION_WORKS_PAGEID,0)

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 