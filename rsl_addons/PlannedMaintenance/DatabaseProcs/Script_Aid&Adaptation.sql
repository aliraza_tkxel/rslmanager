update PA_ITEM set IsActive=0 where ItemID=(
	select top 1 itemId from PA_ITEM where areaid=(
		select top 1 AreaID from PA_AREA where AreaName='Accommodation'
	) and itemname='Aids & Adaptations' and itemsorder=21
) 


-- Externals > Dwelling > Aids & Adaptations
declare @itemid int;
declare @paramid int;
insert into [RSLBHALive].[dbo].[PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive) values
((select top 1 AreaID from PA_AREA where AreaName='Dwelling' and LocationId=2),'Aids & Adaptations',0,1)

set @itemid = SCOPE_IDENTITY()
  
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive)
values
('Adaptations','String','Checkboxes',0,0,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Kee Klamp Railing to Entrance',0,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Ramps',1,1)

-------------------------------------------

declare @areaid int;
select top 1 @areaid=AreaID from PA_AREA where AreaName='Accommodation';

------------------------------------------------------------

-- Internals > Accommodation > Kitchen > Aids & Adaptations

insert into [RSLBHALive].[dbo].[PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId)
  values
  (@areaid,'Aids & Adaptations',0,1,(select top 1 itemid from [RSLBHALive].[dbo].[PA_ITEM] where ItemName='Kitchen' ))
set @itemid = SCOPE_IDENTITY()  
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive)
values
('Adaptations','String','Checkboxes',0,0,1)
set @paramid = SCOPE_IDENTITY()
insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Fully Adapted Kitchen',0,1)


------------------------------------------------------------
-- Internals > Accommodation > Bathroom > Aids & Adaptations
insert into [RSLBHALive].[dbo].[PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId)
  values
(@areaid,'Aids & Adaptations',0,1,(select top 1 itemid from [RSLBHALive].[dbo].[PA_ITEM] where ItemName='Bathroom' ))
set @itemid = SCOPE_IDENTITY()  
  
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive)
values
('Adaptations','String','Checkboxes',0,0,1)
set @paramid = SCOPE_IDENTITY()  

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Shower Room',0,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Level Access Shower with tray',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Over Bath Shower',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Specialist Wash Dry WC',3,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Separate WC',4,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Separate Bath',5,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Separate W/H/Basin',6,1)


insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Shower screen',7,1)

--------------------------------------


-- Internals > Accommodation > Aids & Adaptations

insert into [RSLBHALive].[dbo].[PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive)
  values
  (@areaid,'Aids & Adaptations',0,1)
set @itemid = SCOPE_IDENTITY()   
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive)
values
('Adaptations','String','Checkboxes',0,0,1)
set @paramid = SCOPE_IDENTITY()  
insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Thresholds',0,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Change bathroom door for sliding door',1,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Widen doorways',2,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Stair Lift',3,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'Through Floor Lift',4,1)


