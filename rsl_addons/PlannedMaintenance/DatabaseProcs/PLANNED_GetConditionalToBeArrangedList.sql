USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@totalCount int

--SELECT	@totalCount = 0

--EXEC	@return_value = [dbo].[PLANNED_GetConditionalToBeArrangedList]
--		@searchText = NULL,
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'PMO',
--		@sortOrder = N'Desc',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'
-- Author:		Aamir Waheed
-- Create date: 10/07/2014
-- Description:	
-- =============================================
 
CREATE PROCEDURE [dbo].[PLANNED_GetConditionalToBeArrangedList]
	-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200) = NULL,
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		-- variables for status
		,@status varchar(100)		
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
									
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( J.JOURNALID LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PROPERTYADDRESS.Address LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR P.POSTCODE LIKE ''%' + @searchText + '%'') '
		END	
				
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = ' SELECT top ('+convert(varchar(10),@limit)+') 		
		J.JOURNALID AS PMO 		
		,ISNULL(P.HouseNumber,'''') +'' ''+ ISNULL(P.ADDRESS1,'''') +'' ''+ ISNULL(P.ADDRESS2,'' '') +'' ''+ ISNULL(P.ADDRESS3,'''') AS Address	
		,P.POSTCODE AS Postcode
		,COALESCE(I.ItemName, ''N/A'') AS Component		
		, LEFT(E.FIRSTNAME,1) + ISNULL('' '' + LEFT(E.LASTNAME,1),'''') AS [BY]
		, CW.WorksRequired AS WorksRequired
		,P.HouseNumber AS HouseNumber
		,P.ADDRESS1 AS PrimaryAddress
		,P.PropertyId AS PropertyId
		,LEFT(E.FIRSTNAME,1) + ISNULL('' '' + LEFT(E.LASTNAME,1),'''') BySort		
		'
		
		-- End building SELECT clause
		--======================================================================================== 							

		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'
		FROM PLANNED_JOURNAL J
		INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID AND S.TITLE = ''Condition To Be Arranged''
		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID
		INNER JOIN (SELECT	P__PROPERTY.PROPERTYID as PID,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY)AS PROPERTYADDRESS ON PROPERTYADDRESS.PID = P.PROPERTYID
		INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = J.CREATEDBY
		INNER JOIN PLANNED_CONDITIONWORKS CW ON CW.JOURNALID = J.JOURNALID
		INNER JOIN PA_PROPERTY_ATTRIBUTES PA ON PA.ATTRIBUTEID = CW.ATTRIBUTEID
		INNER JOIN PA_ITEM_PARAMETER IP ON IP.ItemParamID = PA.ITEMPARAMID		
		INNER JOIN PA_ITEM I ON I.ItemID = IP.ItemId		
		'
		-- End building From clause
		--======================================================================================== 														  
		
				
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)'
		END
		IF(@sortColumn = 'BY')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' BySort '
		END
								
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
		
END