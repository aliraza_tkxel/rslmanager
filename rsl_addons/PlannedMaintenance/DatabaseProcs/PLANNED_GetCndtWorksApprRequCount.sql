/****** Object:  StoredProcedure [dbo].[PLANNED_GetCndtWorksApprRequCount]    Script Date: 03/02/2015 11:08:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetCndtWorksApprRequCount] 
/* ===========================================================================
-- NAME:		PLANNED_GetCndtWorksApprRequCount
-- EXEC			[dbo].[PLANNED_GetCndtWorksApprRequCount] -1
-- Author:		<Muhammad Awais>
-- Create date: <03/02/2015>
-- Description:	<Get Replacement Due Current Year Count>
-- Web Page:	Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1
)
AS
	DECLARE @SearchCriteria varchar(500) = ''

	IF @componentId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND [PCW].ComponentId = ' + CONVERT(nvarchar(10), @componentId) 	 	
		END
	 
	SELECT COUNT(PCW.ConditionWorksId) as TotalCount
	  FROM PLANNED_CONDITIONWORKS [PCW] 
	       INNER JOIN PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID
	       INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId  
			INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID
			INNER JOIN PA_PARAMETER [PARAM] ON [PARAM].ParameterID = IP.ParameterId AND PARAM.ParameterName LIKE 'Condition Rating' AND ([ATT].PARAMETERVALUE = 'Unsatisfactory' OR [ATT].PARAMETERVALUE = 'Potentially Unsatisfactory')
			INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = [ATT].VALUEID AND (PV.ValueDetail = 'Unsatisfactory' OR PV.ValueDetail = 'Potentially Unsatisfactory')
	 WHERE [PLA].Title = 'Recommended' + @searchCriteria 


