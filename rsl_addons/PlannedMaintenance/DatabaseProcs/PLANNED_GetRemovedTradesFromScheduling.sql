USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[PLANNED_GetRemovedTradesFromScheduling]
--		@pmo = 54,
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,28 August,2013>
-- Description:	<Description,,This procedure 'll delete the pending appointments>
-- =============================================
ALTER  PROCEDURE [dbo].[PLANNED_GetRemovedTradesFromScheduling] 
	@pmo int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT 
		PLANNED_COMPONENT.COMPONENTID as ComponentId
		,PLANNED_COMPONENT.COMPONENTNAME as Component
		,COALESCE ( PLANNED_REMOVED_SCHEDULING_TRADES.DURATION,PLANNED_COMPONENT_TRADE.DURATION ) as Duration
		,PLANNED_COMPONENT_TRADE.COMPTRADEID as ComponentTradeId
		,G_TRADE.TradeId  as TradeId
		,G_TRADE.Description  as Trade
		,0 as StatusId
		,'Removed from scheduling' as Status
	FROM 
		PLANNED_REMOVED_SCHEDULING_TRADES  
		INNER JOIN PLANNED_COMPONENT_TRADE ON PLANNED_REMOVED_SCHEDULING_TRADES.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID
		INNER JOIN PLANNED_COMPONENT ON PLANNED_COMPONENT_TRADE.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
		INNER JOIN G_TRADE ON G_TRADE.TradeId = PLANNED_COMPONENT_TRADE.TRADEID 
	WHERE 
		PLANNED_REMOVED_SCHEDULING_TRADES.JOURNALID = @pmo

    
END
