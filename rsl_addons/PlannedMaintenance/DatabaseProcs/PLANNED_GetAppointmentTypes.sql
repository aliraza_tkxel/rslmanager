-- Stored Procedure

-- =============================================  
-- Author:  <Author,,Zahid Ali>  
-- Create date: <Create Date,,15/07/2014>  
-- Description: <Description,,Get planned appoint types>  
-- =============================================  
ALTER PROCEDURE [dbo].[PLANNED_GetAppointmentTypes]  
  @isActive bit

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

    -- Insert statements for procedure here  
 If @isActive = 1
	 BEGIN
		 SELECT Planned_Appointment_TypeId as TypeID,Planned_Appointment_Type as TypeValue from PLANNED_APPOINTMENT_TYPE  
		 WHERE IsActive = 1  
	 END
 ELSE
	 BEGIN
		SELECT Planned_Appointment_TypeId as TypeID,Planned_Appointment_Type as TypeValue from PLANNED_APPOINTMENT_TYPE  
	 END
END  
GO