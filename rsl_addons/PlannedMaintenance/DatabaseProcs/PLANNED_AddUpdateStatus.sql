USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_AddUpdateStatus]    Script Date: 11/12/2013 17:43:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[PLANNED_AddUpdateStatus]
--		@Title = 'New Status',
--		@Ranking = 6,
--		@CreatedBy = 615,
--		@modifiedBy = 615
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,04/11/2013>
-- Description:	<Description,,This stored procdure 'll be used to add and update the status>
-- Web page: Status.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_AddUpdateStatus](
	-- Add the parameters for the stored procedure here
	@Title varchar(1000),
	@Ranking int,
	@CreatedBy int,	
	@modifiedBy int,
	@isSaved bit out
	)
AS
BEGIN
Declare @oldStatusId int
Declare @maxRanking int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    		
	--Begin the transaction
	BEGIN Transaction 
	BEGIN TRY 	
	   --If some other status has the same ranking then fetch that status id of that status		
	   SELECT @oldStatusId = StatusId from PLANNED_Status WHERE PLANNED_Status.Ranking = @Ranking
	  -- count the ranking that is assigned uptill now
	   SELECT @maxRanking = MAX(Ranking) FROM PLANNED_Status
		
		--If some other status has the same ranking then we 'll update the ranking of that status
		if @oldStatusId > 0
			BEGIN 
				--Now update the status which has the same rank as new status
				update PLANNED_Status SET Ranking = @maxRanking WHERE StatusId = @oldStatusId
				declare @oldStatusTitle varchar(100)			
				SELECT @oldStatusTitle = Title FROM PLANNED_Status WHERE StatusId = @oldStatusId
				--Insert the change of rank in history table
				INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
				VALUES (@oldStatusId,@oldStatusTitle,@maxRanking,@CreatedBy,@modifiedBy,GETDATE(),GETDATE(),1)
				
				--Now insert the new status
				INSERT INTO PLANNED_Status (Title,Ranking,CreatedBy,IsEditable,CreatedDate)
				VALUES (@Title,@Ranking,@CreatedBy,1,GETDATE())
				--Insert the new status in history table
				INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
				VALUES (SCOPE_IDENTITY(),@Title,@Ranking,@CreatedBy,@modifiedBy,GETDATE(),GETDATE(),1)								
			END
		ELSE
			BEGIN
				--Insert the new status
				INSERT INTO PLANNED_Status (Title,Ranking,CreatedBy,IsEditable,CreatedDate)
				VALUES (@Title,@Ranking,@CreatedBy,1,GETDATE())				
				
				INSERT INTO PLANNED_StatusHistory(StatusId,Title,Ranking,CreatedBy,ModifiedBy,CreatedDate,ModifiedDate,IsEditable)
				VALUES (SCOPE_IDENTITY(),@Title,@Ranking,@CreatedBy,@modifiedBy,GETDATE(),GETDATE(),1)
			END	
	END TRY
	
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END
	
	EXEC PLANNED_AllStauses
	
	
END
