USE [RSLBHALive]
GO

/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	5th December 2013
--  Description:	Table containing cancelled appointments
--					
 '==============================================================================*/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PLANNED_CANCELLED_JOBS](
	[CancelID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AppointmentId] [int] NULL,
	[RecordedOn] [smalldatetime] NULL,
	[Notes] [nvarchar](1000) NULL,
 CONSTRAINT [PK_PLANNED_CANCELLED_JOBS] PRIMARY KEY CLUSTERED 
(
	[CancelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PLANNED_CANCELLED_JOBS]  WITH CHECK ADD  CONSTRAINT [FK_CANCELLED_APPOINTMENTID] FOREIGN KEY([AppointmentId])
REFERENCES [dbo].[PLANNED_APPOINTMENTS] ([APPOINTMENTID])
GO

ALTER TABLE [dbo].[PLANNED_CANCELLED_JOBS] CHECK CONSTRAINT [FK_CANCELLED_APPOINTMENTID]
GO





