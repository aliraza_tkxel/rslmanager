/****** Object:  StoredProcedure [dbo].[PLANNED_GetCndtWorksApprovedCount]    Script Date: 03/02/2015 11:08:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetCndtWorksApprovedCount] 
/* ===========================================================================
-- NAME:		PLANNED_GetCndtWorksApprovedCount
-- EXEC			[dbo].[PLANNED_GetCndtWorksApprovedCount] -1
-- Author:		<Muhammad Awais>
-- Create date: <03/02/2015>
-- Description:	<Get Replacement Due Current Year Count>
-- Web Page:	Dashboard.aspx
'==============================================================================*/
(
	@componentId int = -1
)
AS
	DECLARE @SearchCriteria varchar(500) = ''

	IF @componentId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' WHERE J.ComponentId = ' + CONVERT(nvarchar(10), @componentId) 	 	
		END
	 
	SELECT COUNT(J.JOURNALID) AS TOTALCOUNT
	  FROM PLANNED_JOURNAL J  
			INNER JOIN PLANNED_STATUS S ON S.STATUSID = J.STATUSID AND S.TITLE = 'Condition To Be Arranged' 
	+ @searchCriteria

