/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	18 April 2014
--  Description:	Mapping items and components
--					
 '==============================================================================*/

--======================================================================
-- HEATING
--======================================================================

--19 ) Heating - Air Source
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Air Source'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Air Source'),null,null)

--20 ) Heating - Ground Source
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Ground Source'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Ground Source'),null,null)

--21 ) Heating - LPG - Combi Boiler
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - LPG - Combi Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'LPG'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Combi Boiler'))

--22 ) Heating - LPG - Fire
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - LPG - Fire'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'LPG'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Fire'))

--23 ) Heating - LPG - Traditional Boiler
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - LPG - Traditional Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'LPG'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Traditional Boiler'))

--24 ) Heating - LPG - Wall Heater
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - LPG - Wall Heater'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'LPG'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Wall Heater'))

--25 ) Heating - Main Electric - Wall Heater
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Main Electric - Wall Heater'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Mains Electric'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Wall Heater'))

--26 ) Heating - Main Gas - Combi Boiler
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Main Gas - Combi Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Combi Boiler'))

--27 ) Heating - Main Gas - Fire
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Main Gas - Fire'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Fire'))

--28 ) Heating - Main Gas - Traditional Boiler
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Main Gas - Traditional Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Traditional Boiler'))

--29 ) Heating - Main Gas - Wall Heater
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Main Gas - Wall Heater'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Mains Gas'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Wall Heater'))

--30 ) Heating - Oil - Traditional Boiler
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Oil - Traditional Boiler'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Oil'),DBO.FN_GetParameterId('Boiler type'),dbo.FN_GetParameterValueId('Boiler type' ,'Traditional Boiler'))

--31 ) Heating - Solar
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Solar'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Solar'),null,null)

--32 ) Heating - Wind
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Heating - Wind'),DBO.FN_GetItemId('Heating'),1,DBO.FN_GetParameterId('Primary heating fuel'),dbo.FN_GetParameterValueId('Primary heating fuel' ,'Wind'),null,null)


--======================================================================
-- EMITTERS
--======================================================================

--33 ) Emitters - Radiators
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Emitters - Radiators'),DBO.FN_GetItemId('Emitters'),1,DBO.FN_GetParameterId('Emitter type'),dbo.FN_GetParameterValueId('Emitter type' ,'Radiators'),null,null)

--34 ) Emitters - Gas Fire
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Emitters - Gas Fire'),DBO.FN_GetItemId('Emitters'),1,DBO.FN_GetParameterId('Emitter type'),dbo.FN_GetParameterValueId('Emitter type' ,'Gas Fire'),null,null)

--35 ) Emitters - Under Floor
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Emitters - Under Floor'),DBO.FN_GetItemId('Emitters'),1,DBO.FN_GetParameterId('Emitter type'),dbo.FN_GetParameterValueId('Emitter type' ,'Under Floor'),null,null)

--36 ) Emitters - Night Storage
INSERT INTO PLANNED_COMPONENT_ITEM ([COMPONENTID],[ITEMID],[ISACTIVE],[PARAMETERID],[VALUEID],[SubParameter],[SubValue])
VALUES (DBO.GetComponentId('Emitters - Night Storage'),DBO.FN_GetItemId('Emitters'),1,DBO.FN_GetParameterId('Emitter type'),dbo.FN_GetParameterValueId('Emitter type' ,'Night Storage'),null,null)


--======================================================================
-- HOT WATER SUPPLY
--15 ) Hot water supply - Mains
--16 ) Hot water supply - Gas
--17 ) Hot water supply - Electric
--18 ) Hot water supply - Cylinder
--======================================================================


UPDATE	PLANNED_COMPONENT_ITEM 
SET		VALUEID = dbo.FN_GetParameterValueId('Type','From main heating')
WHERE	COMPONENTITEMID = 4

UPDATE	PLANNED_COMPONENT_ITEM 
SET		VALUEID = dbo.FN_GetParameterValueId('Type','Gas Water heater')
WHERE	COMPONENTITEMID = 5

UPDATE	PLANNED_COMPONENT_ITEM 
SET		VALUEID = dbo.FN_GetParameterValueId('Type','Electric Water heater')
WHERE	COMPONENTITEMID = 6

UPDATE	PLANNED_COMPONENT_ITEM 
SET		VALUEID = dbo.FN_GetParameterValueId('Type','Cylinder')
WHERE	COMPONENTITEMID = 7


