/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	28 December 2015
--  Description:	Script to correct the orders in number dropdown field and enable 'Not Applicable'
 '==============================================================================*/


DECLARE @valueId int

SELECT	@valueId = valueId
FROM	PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.PARAMETERID = PA_PARAMETER.PARAMETERID
WHERE	PARAMETERNAME = 'Number' and valueDetail = 'Please Select'

UPDATE	PA_PARAMETER_VALUE
SET		isActive = 0
FROM	PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.PARAMETERID = PA_PARAMETER.PARAMETERID
WHERE	PARAMETERNAME = 'Number' and valueId > @valueId

UPDATE	PA_PARAMETER_VALUE
SET		isActive = 1
FROM	PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.PARAMETERID = PA_PARAMETER.PARAMETERID
WHERE	PARAMETERNAME = 'Number' and valueDetail = 'Not Applicable'

UPDATE	PA_PARAMETER_VALUE
SET		sOrder = 14
FROM	PA_PARAMETER_VALUE
		INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.PARAMETERID = PA_PARAMETER.PARAMETERID
WHERE	PARAMETERNAME = 'Number' and valueDetail = 'Not Applicable'