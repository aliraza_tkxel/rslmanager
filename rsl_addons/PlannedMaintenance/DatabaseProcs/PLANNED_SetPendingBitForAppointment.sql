USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_DeleteAppointment]    Script Date: 12/11/2013 11:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@isSaved bit
--EXEC	[dbo].[PLANNED_SetPendingBitForAppointment]
--		@appointmentId = 1,
--		@isPending= 1,
--		@isSaved = @isSaved OUTPUT
--		SELECT	@isSaved as N'@isSaved'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,7 Dec,2013>
-- Description:	<Description,,This procedure 'll set the pending bit for appointment>
-- =============================================
CREATE  PROCEDURE [dbo].[PLANNED_SetPendingBitForAppointment] 
	@appointmentId int,
	@isPending bit,
	@isSaved bit OUT
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY	
		UPDATE PLANNED_APPOINTMENTS SET ISPENDING = @isPending  WHERE PLANNED_APPOINTMENTS.APPOINTMENTID = @appointmentId				
		set @isSaved = 1
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN 
			ROLLBACK TRANSACTION;
			SET @isSaved = 0;													
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return 
			-- error information about the original error that 
			-- caused execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
	END CATCH 
	
	IF @@TRANCOUNT >0 
	BEGIN
		COMMIT TRANSACTION;
		SET @isSaved = 1
	END 
    
END
