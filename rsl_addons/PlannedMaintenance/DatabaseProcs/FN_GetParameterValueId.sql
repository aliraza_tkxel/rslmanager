
CREATE function [dbo].[FN_GetParameterValueId](@parameterName as varchar(50), @parameterValue as varchar(50))              
 returns int              
 as               
 begin              
           
    declare @parameterValueId as int              
          
	SELECT	@parameterValueId = ValueID  
    FROM	PA_PARAMETER_VALUE
			INNER JOIN PA_PARAMETER ON PA_PARAMETER_VALUE.ParameterID = PA_PARAMETER.ParameterID
	WHERE	PA_PARAMETER.isactive = 1 
			AND ParameterName = @parameterName
			AND ValueDetail = @parameterValue
	         
    return(@parameterValueId)              
               
 end  

GO


