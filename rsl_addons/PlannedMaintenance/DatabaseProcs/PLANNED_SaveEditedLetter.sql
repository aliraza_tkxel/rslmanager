USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[PLANNED_SaveEditedLetter]
--		@journalHistoryId = 12,
--		@body = N'Body',
--		@standardLetterId = 1,
--		@title = N'Letter',
--		@teamId = 1,
--		@resourceId = 1,
--		@signOffId = 1,
--		@rentBalance = 0,
--		@rentCharge = 0,
--		@todayDate = NULL

--SELECT	'Return Value' = @return_value

-- Author:		<Ahmed Mehmood>
-- Create date: <1 Nov, 2013>
-- Description:	<This stored procedure will save the reccord in PLANNED_SAVEDLETTERS with journalhistory id>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_SaveEditedLetter] 
	-- Add the parameters for the stored procedure here
	@journalHistoryId int,
	@body nvarchar(MAX),
	@standardLetterId int,	
	@title varchar(max),
	@teamId int,
	@resourceId int,
	@signOffId int,		
	@rentBalance float,
	@rentCharge float,
	@todayDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO PLANNED_SAVEDLETTERS
           ([JOURNALHISTORYID]
           ,[LETTERCONTENT]
           ,[LETTERID]
           ,[LETTERTITLE]
           ,[TEAMID]
           ,[FromResourceId]
           ,[SignOffLookupCode]
           ,[RentBalance]
           ,[RentCharge]
           ,[TodayDate]
           ,[CreatedDate]
           ,[ModifiedDate])
     VALUES
           (@journalHistoryId
           ,@body
           ,@standardLetterId
           ,@title
           ,@teamId
           ,@resourceId
           ,@signOffId
           ,@rentBalance
           ,@rentCharge
           ,@todayDate
           ,GETDATE()
           ,GETDATE())
END
