/* ===========================================================================  
 ' NAME: PLANNED_GetLocationAdaptations  
-- EXEC  [dbo].[PLANNED_GetLocationAdaptations]  
--    
-- Author:  <Zahid Ali>  
-- Create date: <15/07/2014>  
-- Description: < Returns the Adaptations against given parameterID of Location.>  
-- Web Page: MiscWorks.aspx  
 '==============================================================================*/  
Alter PROCEDURE PLANNED_GetLocationAdaptations   
 @parameterID Integer = -1  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 select ValueID, ValueDetail from PA_PARAMETER_VALUE where ParameterID=@parameterID And IsActive=1  
END  