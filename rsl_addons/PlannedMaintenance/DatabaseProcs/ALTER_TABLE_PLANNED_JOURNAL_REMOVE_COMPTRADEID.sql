
/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	6th November 2013
--  Description:	COMPTRADEID removed from PLANNED_JOURNAL
--					
 '==============================================================================*/

USE [RSLBHALive]
GO

ALTER TABLE PLANNED_JOURNAL 
DROP CONSTRAINT FK__PLANNED_J__COMPT__4D76EE0F

ALTER TABLE PLANNED_JOURNAL
DROP COLUMN COMPTRADEID