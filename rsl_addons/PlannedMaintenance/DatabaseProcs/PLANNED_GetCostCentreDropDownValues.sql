SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 30/04/2014
-- Description:	To get Cost centre drop down values to be used in assign to contractor process for Planned Maintanance.Module
-- Web Page:	AssignToContractor.ascx(User Control)
-- =============================================
CREATE PROCEDURE PLANNED_GetCostCentreDropDownValues 

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here
DECLARE @YRange int
SELECT
	@YRange = YRange
FROM F_FiscalYears
WHERE YStart <= CONVERT(date, GETDATE())
AND YEnd >= CONVERT(date, GETDATE())

-- Get Cost Centre Drop down values
SELECT DISTINCT
	CC.COSTCENTREID AS [id],
	CC.DESCRIPTION AS [description]
FROM F_COSTCENTRE CC
INNER JOIN F_COSTCENTRE_ALLOCATION CCA 
	ON CCA.COSTCENTREID = CC.COSTCENTREID
	AND CCA.ACTIVE = 1
	AND (CCA.FISCALYEAR = @YRange
	OR CCA.FISCALYEAR IS NULL)
WHERE CC.COSTCENTREID NOT IN (13, 22, 23)
AND EXISTS (SELECT
	COSTCENTREID
FROM F_HEAD HE
INNER JOIN F_HEAD_ALLOCATION HEA
	ON HEA.HEADID = HE.HEADID
	AND HEA.ACTIVE = 1
	AND HE.COSTCENTREID = CC.COSTCENTREID
	AND (HEA.FISCALYEAR = @YRange
	OR HEA.FISCALYEAR IS NULL))
END
GO