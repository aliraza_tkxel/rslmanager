USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_AllStauses]    Script Date: 11/04/2013 15:28:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_AllStauses
-- Author:		<Noor Muhammad>
-- Create date: <31/10/2013>
-- Description:	<This Stored Proceedure shows the statuses on the Resoucres Page, Status Listing >
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_AllStauses]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	PLANNED_Status.Title as Title,
	PLANNED_Status.Ranking as Ranking,
	PLANNED_Status.StatusId as StatusId,
	IsEditable as IsEditable
	FROM PLANNED_Status
	Order BY Ranking 
	
	
END
