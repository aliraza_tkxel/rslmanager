/****** Object:  StoredProcedure [dbo].[PLANNED_SetJobSheetAppointmentStatus]    Script Date: 02/03/2015 12:41:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[PLANNED_SetJobSheetAppointmentStatus]
/* ===========================================================================
--	EXEC PLANNED_SetJobSheetAppointmentStatus
		@AppointmentJSNId = 343,		
		@AppointmentStatusId = 14,
		@Notes = 'The Entry was not possible',
		@LastActionDate GetDate(),
		@LastActionUserID 65
--  Author:			Muhammad Awais
--  DATE CREATED:	03 February 2015
--  Description:	To Set Appointment Status to Cancel/Complete, and Insert into Table NoEntry, 
--					 and Insert Tranction into History
--  Webpage:		View/Reports/AssignedToContractor.aspx 
 '==============================================================================*/
@AppointmentJSNId INT,
@AppointmentStatusId INT,
@Notes NVARCHAR(MAX),
@LastActionDate DATETIME,
@OrderId INT,
@LastActionUserID INT,
@Result INT OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON
	
--	BEGIN TRAN
	
		DECLARE @PROPERTYID NVARCHAR(MAX), @COMPONENTID INT
		SELECT @PROPERTYID = PLANNED_JOURNAL.PROPERTYID,
			   @COMPONENTID = PLANNED_JOURNAL.COMPONENTID
		  FROM PLANNED_JOURNAL 
		 WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId

		---- Update Status (Cancel StatusID) in PLANNED_JOURNAL
		--UPDATE PLANNED_JOURNAL
		--   SET StatusID = @AppointmentStatusId
		-- WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId

		-- Insert the Cancel tranction in History Table (FL_FAULT_LOG_HISTORY)
		INSERT INTO PLANNED_JOURNAL_HISTORY (JOURNALID, PROPERTYID, COMPONENTID, STATUSID, ACTIONID, CREATIONDATE, 
					CREATEDBY, NOTES, ISLETTERATTACHED, StatusHistoryId ,ActionHistoryId ,IsDocumentAttached )
		VALUES (@AppointmentJSNId, @PROPERTYID, @COMPONENTID, @AppointmentStatusId, NULL, @LastActionDate,
					 @LastActionUserID, @Notes, 0, NULL, NULL, 0  )


		-- Update Status (To be Arranged StatusID) in PLANNED_JOURNAL
		DECLARE @StatusId INT  
		 SELECT @StatusId = STATUSID  
		   FROM PLANNED_STATUS WHERE Lower( PLANNED_STATUS.TITLE ) = lower( 'To be Arranged' )

		UPDATE PLANNED_JOURNAL  
		   SET PLANNED_JOURNAL.STATUSID = @StatusId  
		 WHERE PLANNED_JOURNAL.JOURNALID = @AppointmentJSNId  
  
		-- Insert the Cancel tranction in History Table (FL_FAULT_LOG_HISTORY)
		INSERT INTO PLANNED_JOURNAL_HISTORY (JOURNALID, PROPERTYID, COMPONENTID, STATUSID, ACTIONID, CREATIONDATE, 
					CREATEDBY, NOTES, ISLETTERATTACHED, StatusHistoryId ,ActionHistoryId ,IsDocumentAttached )
		VALUES (@AppointmentJSNId, @PROPERTYID, @COMPONENTID, @StatusId, NULL, @LastActionDate,
					 @LastActionUserID, @Notes, 0, NULL, NULL, 0  )

		IF @OrderId > 0 
		BEGIN
			-- Update Status (StatusID) in PLANNED_JOURNAL
			UPDATE F_PURCHASEORDER 
			   SET F_PURCHASEORDER.POSTATUS = 2
			 WHERE F_PURCHASEORDER.ORDERID = @OrderId
		END
		--------------------------------
		-- If insertion fails, goto HANDLE_ERROR block
		--IF @@ERROR <> 0 GOTO HANDLE_ERROR
		
		--COMMIT TRAN
		SET @RESULT = 1
		--RETURN
END

--HANDLE_ERROR:
	--ROLLBACK TRAN  
	--SET @RESULT=-1
--RETURN

GO
