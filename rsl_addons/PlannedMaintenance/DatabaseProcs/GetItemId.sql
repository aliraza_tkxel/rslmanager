USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE function [dbo].[GetItemId](@locationName as varchar(20),@areaName as varchar(20),@itemName as varchar(50))              
 returns int              
 as               
 begin              
           
    declare @itemId as int              
          
	SELECT @itemId = ItemID
    FROM PA_ITEM  inner join PA_AREA on pa_item.AreaID = PA_AREA.AreaID 
	inner join PA_LOCATION on PA_AREA.LocationId = PA_LOCATION.LocationID 
	WHERE PA_ITEM.isactive = 1 
	AND LocationName = @locationName
	AND AreaName = @areaName
	AND ItemName = @itemName
         
    return(@itemId)              
               
 end  

GO


