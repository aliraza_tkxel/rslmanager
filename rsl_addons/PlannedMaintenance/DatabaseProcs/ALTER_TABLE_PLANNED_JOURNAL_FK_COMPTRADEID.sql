/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	11th October 2013
--  Description:	COMPTRADEID is added to identify the exact trade of an appointment
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE PLANNED_JOURNAL
ADD COMPTRADEID INT

ALTER TABLE dbo.PLANNED_JOURNAL ADD FOREIGN KEY (COMPTRADEID) REFERENCES PLANNED_COMPONENT_TRADE (COMPTRADEID)
GO