USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC PLANNED_GetAllAppointmentStatuses
-- Author:		<Ahmed Mehmood>
-- Create date: <23/10/2013>
-- Description:	<Returns All Appointment statuses>
-- Webpage: ReplacementList.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetAllAppointmentStatuses] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN

SELECT PLANNED_STATUS.STATUSID as StatusId, PLANNED_STATUS.TITLE as StatusTitle    
FROM PLANNED_STATUS  where TITLE LIKE 'Inspection Arranged' OR TITLE  LIKE 'To be Arranged' OR TITLE LIKE 'Arranged' OR TITLE LIKE 'Completed' OR TITLE LIKE 'Cancelled' OR TITLE LIKE 'Cancelled' OR TITLE LIKE 'Assigned To Contractor'

END