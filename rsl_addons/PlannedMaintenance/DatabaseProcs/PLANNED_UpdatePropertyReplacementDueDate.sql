USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_UpdatePropertyReplacementDueDate @appointmentId
-- Author:		Ahmed Mehmood
-- Create date: <19/3/2014>
-- Last Modified: <19/3/2014>
-- Description:	<Update Property. >
-- Webpage : AnnualSpendPlanner.aspx

-- =============================================
CREATE PROCEDURE [dbo].[PLANNED_UpdatePropertyReplacementDueDate]
	@dueDateDt as PLANNED_PROPERTY_REPLACEMENT_DUEDATES readonly
	,@isSaved int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY


	CREATE TABLE #TMPDUEDATES( TMP_SID int, TMP_DUEDATE VARCHAR(10) )
	
	INSERT	INTO #TMPDUEDATES (TMP_SID, TMP_DUEDATE) 
	SELECT	SID, DUEDATE 
	FROM	@dueDateDt 
	
	
	UPDATE	PA_PROPERTY_ITEM_DATES 
	SET		PA_PROPERTY_ITEM_DATES.DueDate = #TMPDUEDATES.TMP_DUEDATE 
			,LastDone = GETDATE()
	FROM	#TMPDUEDATES
	WHERE	PA_PROPERTY_ITEM_DATES.SID = #TMPDUEDATES.TMP_SID   


	DROP TABLE #TMPDUEDATES 
	
END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END