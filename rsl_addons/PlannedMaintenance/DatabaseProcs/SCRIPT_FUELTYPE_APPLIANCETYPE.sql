/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	18 April 2014
--  Description:	Update Appliances tab parameter values.
--					
 '==============================================================================*/
 
 -- FUEL DROPDOWN
 INSERT INTO P_FUELTYPE ([FUELTYPE]) VALUES ('Air Source')
 INSERT INTO P_FUELTYPE ([FUELTYPE]) VALUES ('Ground Source')
 INSERT INTO P_FUELTYPE ([FUELTYPE]) VALUES ('LPG')
 UPDATE P_FUELTYPE SET FUELTYPE = 'Mains Electric' WHERE FUELTYPE = 'Electric'
 UPDATE P_FUELTYPE SET FUELTYPE = 'Mains Gas' WHERE FUELTYPE = 'Gas'
 UPDATE	P_FUELTYPE SET FUELTYPE = 'Solar' WHERE FUELTYPEID = 4

 -- TYPE
 
		UPDATE	GS_Appliance_Type  
		SET		ISACTIVE = 0  
		WHERE APPLIANCETYPEID   NOT IN
		(
			SELECT	APPLIANCETYPEID   
			FROM	GS_Appliance_Type
			WHERE	APPLIANCETYPE  IN ('Combi Boiler','Cooker/Hob','Fire','Traditional Boiler','Wall Heater')
		)
		
		UPDATE	GS_Appliance_Type
		SET		APPLIANCETYPE = 'Cooker' 
		WHERE	APPLIANCETYPE = 'Cooker/Hob'
