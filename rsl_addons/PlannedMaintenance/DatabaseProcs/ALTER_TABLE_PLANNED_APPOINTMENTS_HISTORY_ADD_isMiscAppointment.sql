/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	6h December 2013
--  Description:	isMiscAppointment is added to identify appointment type
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE PLANNED_APPOINTMENTS_HISTORY
ADD isMiscAppointment bit NOT NULL DEFAULT(0)
