/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	3-9-2014
--  Description:	DURATION of the appointment
--					
 '==============================================================================*/

ALTER TABLE PLANNED_REMOVED_SCHEDULING_TRADES ADD DURATION FLOAT NULL

