
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 14/05/2014
-- Description:	Get reason related to condition.
-- =============================================

CREATE PROCEDURE PLANNED_GetConditionReasons

AS
	SELECT	ReasonId ,Reason 
	FROM	PLANNED_REASON 
	ORDER BY ReasonId  ASC