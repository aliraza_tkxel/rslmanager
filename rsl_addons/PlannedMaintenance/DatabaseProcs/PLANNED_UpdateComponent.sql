/****** Object:  StoredProcedure [dbo].[PLANNED_UpdateComponent]    Script Date: 11/20/2014 17:07:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,10/11/2013>
-- Description:	<Description,Update PLANNED_COMPONENT and insert into PLANNED_COMPONENT_HISTORY and delete and inset into PLANNED_COMPONENT_TRADE>
--Last modified Date:10/11/2013
---======================================================================
--DECLARE	@return_value int
--DECLARE @TradesInsert AS PLANNEDTRADE;
--INSERT INTO @TradesInsert(TRADEID,DURATION)
--VALUES (1,1)

--DECLARE @TradesDelete AS PLANNEDTRADE;
--INSERT INTO @TradesDelete(TRADEID,DURATION)
--VALUES (1,1)
--EXEC	@return_value = [dbo].[PLANNED_UpdateComponent]
--		@COMPONENTID = 19,
--		@COMPONENTNAME = N'kitchen',
--		@ISACCOUNTING = 1,
--		@CYCLE = 12,
--		@FREQUENCY = N'yrs',
--		@MATERIALCOST = 1200,
--		@LABOURCOST = 1000,
--		@EDITEDBY = 615,
--		@EDITEDON = '10/11/2013',
--		@TradesInsert = @TradesInsert,
--		@TradesDelete = @TradesDelete

-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_UpdateComponent]
	-- Add the parameters for the stored procedure here
	@COMPONENTID int
	,@COMPONENTNAME nvarchar(100)
	,@ISACCOUNTING bit
	,@CYCLE int
	,@FREQUENCY nvarchar(10)
	,@ANNUALBUDGETCOST float
	,@MATERIALCOST float
	,@LABOURCOST float
	,@EDITEDBY int
	,@EDITEDON smalldatetime
	,@TradesInsert as PLANNEDCOMPONENTTRADE readonly
	,@TradesDelete as PLANNEDCOMPONENTTRADE readonly
	,@TradesOrder as PLANNEDCOMPONENTTRADE readonly

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	

	--====================================
	-- UPDATING PLANNED_COMPONENT
	--====================================
	
	create table #temp_trades_delete
	(
	TRADEID INT NULL,
	DURATION FLOAT NULL,
	SORDER INT NULL
	)
	
	INSERT INTO #temp_trades_delete
    SELECT * FROM @TradesDelete;
	
	
	Update PLANNED_COMPONENT
	   SET ISACCOUNTING = @ISACCOUNTING,
	       CYCLE = @CYCLE,
	       FREQUENCY = @FREQUENCY,
	       ANNUALBUDGETCOST = @ANNUALBUDGETCOST,
	       MATERIALCOST = @MATERIALCOST,
	       LABOURCOST=@LABOURCOST,EDITEDBY=@EDITEDBY,EDITEDON=GETDATE()
	WHERE COMPONENTID=@COMPONENTID    
	
	
	
	--====================================
	-- UPDATING PA_PROPERTY_ITEM_DATES
	--====================================
	
	
	UPDATE	PA_PROPERTY_ITEM_DATES
	SET		PA_PROPERTY_ITEM_DATES.DueDate = (CASE	WHEN @FREQUENCY = 'yrs' THEN 
														DATEADD(YEAR,@CYCLE,PA_PROPERTY_ITEM_DATES.LastDone)
													ELSE
														DATEADD(MONTH,@CYCLE,PA_PROPERTY_ITEM_DATES.LastDone) END )
	WHERE	PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID = @COMPONENTID
	
	
	--====================================
	-- INSERTING PLANNED_COMPONENT_HISTORY
	--====================================    
    
	INSERT INTO PLANNED_COMPONENT_HISTORY(COMPONENTID,COMPONENTNAME,ISACCOUNTING,CYCLE,FREQUENCY,MATERIALCOST,
	LABOURCOST,EDITEDBY,EDITEDON, ANNUALBUDGETCOST )
	VALUES(@COMPONENTID,@COMPONENTNAME, @ISACCOUNTING,@CYCLE,@FREQUENCY,@MATERIALCOST,@LABOURCOST,
	@EDITEDBY,GETDATE(), @ANNUALBUDGETCOST)      
  
	DELETE FROM PLANNED_COMPONENT_TRADE  
	where (	PLANNED_COMPONENT_TRADE.COMPTRADEID IN (
							SELECT TempComTrade.COMPTRADEID 
							FROM #temp_trades_delete INNER JOIN PLANNED_COMPONENT_TRADE TempComTrade on
								 TempComTrade.COMPONENTID  = @COMPONENTID
								 and #temp_trades_delete.TRADEID = TempComTrade.TRADEID 
								 and #temp_trades_delete.DURATION = TempComTrade.DURATION ))
							
	DROP TABLE #temp_trades_delete
   
	INSERT INTO PLANNED_COMPONENT_TRADE(COMPONENTID,TRADEID,DURATION,sorder)
	SELECT   @COMPONENTID ,TRADEID ,DURATION , SORDER FROM @TradesInsert;
  
  
  --==========================================================
  --	UPDATING TRADE ORDER
  --==========================================================
  
	CREATE TABLE #COMP_TRADE_TEMP( TRADEID INT NULL, DURATION FLOAT NULL , SORDER INT NULL)
	
	INSERT INTO #COMP_TRADE_TEMP(TRADEID,DURATION,SORDER)
	SELECT  TRADEID ,DURATION , SORDER FROM @TradesOrder;
  
	UPDATE
		PLANNED_COMPONENT_TRADE 
	SET
		PLANNED_COMPONENT_TRADE.SORDER = #COMP_TRADE_TEMP.SORDER
	FROM
		#COMP_TRADE_TEMP
	WHERE
		PLANNED_COMPONENT_TRADE.COMPONENTID = @COMPONENTID
		AND PLANNED_COMPONENT_TRADE.TRADEID = #COMP_TRADE_TEMP.TRADEID 
		AND PLANNED_COMPONENT_TRADE.DURATION = #COMP_TRADE_TEMP.DURATION  
		
	DROP TABLE #COMP_TRADE_TEMP 

END