-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <23/07/2014>
-- Description:	Alerts related to Condition Works
-- =============================================


INSERT INTO [E_ALERTS] ([AlertID],[AlertName],[SOrder],[AlertUrl])
VALUES (19,'Condition Works Approval Required',19,'/PlannedMaintenance/Bridge.aspx?pg=cr')

INSERT INTO [E_ALERTS] ([AlertID],[AlertName],[SOrder],[AlertUrl])
VALUES (20,'Approved Condition Works',20,'/PlannedMaintenance/Bridge.aspx?pg=scw')