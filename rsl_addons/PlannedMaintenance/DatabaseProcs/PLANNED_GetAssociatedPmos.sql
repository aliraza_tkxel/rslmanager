USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PLANNED_GetAssociatedPmos] 

/* ===========================================================================
 ' NAME:			PLANNED_GetAssociatedPmos
-- EXEC				[dbo].[PLANNED_GetAssociatedPmos]	
-- Author:			<Ahmed Mehmood>
-- Create date:		<8/4/2014>
-- Description:		<Get Associated Pmos>
-- Web Page:		MiscWorks.aspx
 '==============================================================================*/

(
    -- These Parameters are passed as Search Criteria
	@propertyId VARCHAR(20)
)
	
AS

	SELECT	PLANNED_JOURNAL.JOURNALID AS JournalId, JOURNAL_PMO.PMO +' ('+COMPONENTNAME + ') ' as Description
	FROM	PLANNED_JOURNAL
			INNER JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
			INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
			INNER JOIN (	SELECT	PLANNED_JOURNAL.JOURNALID as JID 
									, 'PMO'+CONVERT(VARCHAR,PLANNED_JOURNAL.JOURNALID) as PMO
							FROM	PLANNED_JOURNAL ) AS JOURNAL_PMO
							ON PLANNED_JOURNAL.JOURNALID = JOURNAL_PMO.JID    
	WHERE 1 = 1
			AND PROPERTYID = @propertyId
			AND CREATIONDATE >= DATEADD(YEAR,-1,GETDATE())
			AND PLANNED_STATUS.TITLE IN ( 'Arranged', 'In Progress', 'Completed' )