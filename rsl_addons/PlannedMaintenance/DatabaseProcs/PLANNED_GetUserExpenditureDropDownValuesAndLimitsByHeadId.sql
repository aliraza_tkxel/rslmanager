USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId]    Script Date: 04/13/2015 15:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId @UserId = 943, @HeadId = 65
-- Author:		Aamir Waheed
-- Create date: 30/04/2014
-- Description:	To Get Current User Expenditures drop down values and it limits to process purchase order
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId] 
	-- Add the parameters for the stored procedure here
	@UserId int, 
	@HeadId int 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here
DECLARE @YRange int,
@YStart smalldatetime,
@YEnd smalldatetime

SELECT
	@YRange = YRange,
	@YStart = YStart,
	@YEnd = YEnd
FROM F_FiscalYears
WHERE YStart <= CONVERT(date, GETDATE())
AND YEnd >= CONVERT(date, GETDATE())

SELECT
	DESCRIPTION AS [description],
	CONVERT(DECIMAL,ISNULL(EL.LIMIT, 0.0)) AS LIMIT,
	EX.EXPENDITUREID AS [id],
	CONVERT(DECIMAL,(ISNULL(EXA.EXPENDITUREALLOCATION, 0) - ISNULL(GROSSCOST, 0))) AS REMAINING
FROM	F_EXPENDITURE EX
		INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID
													AND EXA.ACTIVE = 1
													AND (EXA.FISCALYEAR = @YRange
														OR EXA.FISCALYEAR IS NULL)
		INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE
		LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = @UserId
		LEFT JOIN (	SELECT SUM(GROSSCOST) AS GROSSCOST,EXPENDITUREID
					FROM F_PURCHASEITEM
					WHERE ACTIVE = 1
						AND PIDATE >= CONVERT(date, @YStart)
						AND PIDATE <= CONVERT(date, @YEnd)
					GROUP BY EXPENDITUREID) PI ON PI.EXPENDITUREID = EX.EXPENDITUREID
WHERE (HEADID = @HeadId)
ORDER BY DESCRIPTION
END
