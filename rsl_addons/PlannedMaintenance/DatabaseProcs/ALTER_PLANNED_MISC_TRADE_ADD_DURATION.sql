
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PLANNED_MISC_TRADE ADD
	Duration int NOT NULL CONSTRAINT DF_PLANNED_MISC_TRADE_Duration DEFAULT 0
GO
ALTER TABLE dbo.PLANNED_MISC_TRADE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PLANNED_MISC_TRADE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PLANNED_MISC_TRADE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PLANNED_MISC_TRADE', 'Object', 'CONTROL') as Contr_Per 