/****** Object:  StoredProcedure [dbo].[PLANNED_GetAssignedToContractorReport]    Script Date: 02/13/2015 12:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--EXEC	[dbo].[PLANNED_GetAssignedToContractorReport]
--		@searchText = N'',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Reported',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT		
-- Author:		<Aamir Waheed>
-- Create date: <04/06/2014>
-- Description:	<Get list of Planned Work Assigned to Contractor>
-- Web Page: /PlannedMaintenance/Views/Reports/AssignedToContractor.aspx
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetAssignedToContractorReport] 
( 
	-- Add the parameters for the stored procedure here
		@searchText varchar(8000) = '',
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'ReportedSort',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
		DECLARE @SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int

--Paging Formula
SET @offset = 1 + (@pageNumber - 1) * @pageSize
SET @limit = (@offset + @pageSize) - 1

--========================================================================================
-- Begin building SearchCriteria clause
-- These conditions will be added into where clause based on search criteria provided

SET @searchCriteria = ' ( PS.TITLE = ''Assigned To Contractor'' ) '

IF (@searchText != '' OR @searchText != NULL) BEGIN
-- Modified by Muhammad Awais - previously where condition was duplicated
SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ( PJ.JOURNALID LIKE ''%' + @searchText + '%'' '
					+ 'OR ISNULL(Prop.HouseNumber,'''') +'' ''+ ISNULL(Prop.ADDRESS1,'''')
						+'' ''+ ISNULL(Prop.ADDRESS2,'''') +'' ''+ ISNULL(Prop.ADDRESS3,'''') LIKE ''%' + @searchText + '%'' ) '

--SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (FREETEXT(Prop.HouseNumber ,''' + @searchText + ''')  OR FREETEXT(Prop.ADDRESS1, ''' + @searchText + ''')) OR  PJ.JOURNALID LIKE ''%' + @searchText + '%'' '
--					+ 'OR ISNULL(Prop.HouseNumber,'''') +'' ''+ ISNULL(Prop.ADDRESS1,'''')
--						+'' ''+ ISNULL(Prop.ADDRESS2,'''') +'' ''+ ISNULL(Prop.ADDRESS3,'''') LIKE ''%' + @searchText + '%'' '
END

-- End building SearchCriteria clause   
--========================================================================================

SET NOCOUNT ON;
--========================================================================================	        
-- Begin building SELECT clause
-- Insert statements for procedure here

SET @selectClause = 'SELECT top (' + CONVERT(varchar(10), @limit) + ') 
					PJ.JOURNALID AS JOURNALID,
					''PMO'' + CONVERT(NVARCHAR,PJ.JOURNALID) as PMO,
					CONVERT(nvarchar(17),PJ.CREATIONDATE, 113) as Reported
					,PJ.CREATIONDATE AS ReportedSort
					,ISNULL(Prop.HouseNumber,'''') +'' ''+ ISNULL(Prop.ADDRESS1,'''')
						+'' ''+ ISNULL(Prop.ADDRESS2,'''') +'' ''+ ISNULL(Prop.ADDRESS3,'''') as Address
					,ISNULL(CA.AreaName,''N/A'') Location
					,ISNULL(PC.COMPONENTNAME, ''N/A'') Description
					,C.NAME Subcontractor
					,CONVERT(nvarchar(17),CW.AssignedDate, 113) Assigned
					,CW.AssignedDate AS AssignedSort
					,ISNULL(NULLIF(ISNULL(E.Firstname, '''') 
						+ ISNULL('' '''''' + E.LASTNAME, ''''),''''),''N/A'') as UserInitials
					,CONVERT(nvarchar(17),DATEADD(DAY,28, CW.AssignedDate), 113)  CompletionDue
					,DATEADD(DAY,28, CW.AssignedDate) AS CompletionDueSort
					,PS.TITLE Status '
-- End building SELECT clause
--======================================================================================== 							


--========================================================================================    
-- Begin building FROM clause
SET @fromClause = CHAR(10) + 'FROM PLANNED_JOURNAL PJ		
		INNER JOIN P__PROPERTY Prop on PJ.PROPERTYID = Prop.PROPERTYID
		LEFT JOIN PLANNED_COMPONENT PC ON PJ.COMPONENTID = PC.COMPONENTID
		LEFT JOIN PLANNED_COMPONENT_ITEM PCI ON PJ.COMPONENTID = PCI.COMPONENTID
		LEFT JOIN PA_ITEM Item ON PCI.ITEMID = Item.ItemID
		LEFT JOIN PA_AREA CA ON Item.AreaID = CA.AreaID
		INNER JOIN PLANNED_CONTRACTOR_WORK CW ON PJ.JOURNALID = CW.JournalId
		INNER JOIN S_ORGANISATION C on CW.ContractorId = C.ORGID
		INNER JOIN PLANNED_STATUS PS ON PJ.STATUSID = PS.STATUSID
		INNER JOIN E__EMPLOYEE E on CW.AssignedBy = E.EMPLOYEEID
		'
-- End building From clause
--======================================================================================== 														  



--========================================================================================    
-- Begin building OrderBy clause		

-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
IF (@sortColumn = 'Address') BEGIN
SET @sortColumn = CHAR(10) + 'Address'
END

IF (@sortColumn = 'Reported') BEGIN
SET @sortColumn = CHAR(10) + 'ReportedSort'
END

IF (@sortColumn = 'CompletionDue') BEGIN
SET @sortColumn = CHAR(10) + 'CompletionDueSort'
END

IF (@sortColumn = 'Assigned') BEGIN
SET @sortColumn = CHAR(10) + 'AssignedSort'
END


SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

-- End building OrderBy clause
--========================================================================================								

--========================================================================================
-- Begin building WHERE clause

-- This Where clause contains subquery to exclude already displayed records			  

SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria

-- End building WHERE clause
--========================================================================================

--========================================================================================
-- Begin building the main select Query

SET @mainSelectQuery = @selectClause + @fromClause + @whereClause + @orderClause

-- End building the main select Query
--========================================================================================																																			

--========================================================================================
-- Begin building the row number query

SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row	
								FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'

-- End building the row number query
--========================================================================================

--========================================================================================
-- Begin building the final query 

SET @finalQuery = ' SELECT *
							FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result 
							WHERE
							Result.row between' + CHAR(10) + CONVERT(varchar(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(varchar(10), @limit)

-- End building the final query
--========================================================================================									

--========================================================================================
-- Begin - Execute the Query 
--PRINT (@finalQuery)
EXEC (@finalQuery)
-- End - Execute the Query 
--========================================================================================									

----========================================================================================
---- Begin building Count Query 

DECLARE @selectCount nvarchar(2000),
@parameterDef nvarchar(500)

SET @parameterDef = '@totalCount int OUTPUT';
SET @selectCount = 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

--print @selectCount
EXECUTE sp_executesql	@selectCount,
						@parameterDef,
						@totalCount OUTPUT;

---- End building the Count Query
----========================================================================================	

END

