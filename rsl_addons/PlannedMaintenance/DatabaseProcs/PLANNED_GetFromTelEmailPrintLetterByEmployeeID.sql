USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetFromTelEmailPrintLetterByEmployeeID]    Script Date: 12/13/2013 00:23:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--	EXEC PLANNED_GetFromTelEmailPrintLetterByEmployeeID
	@EmployeeID = 34
-- 	Author:		Noor Muhammad
-- 	Create date: 13 Dec,2013
-- 	Description:	To Get Employee Direct Dial Contact and Email by Employee ID
-- 	Web Page: PrintLetter.aspx
-- =============================================*/
CREATE PROCEDURE [dbo].[PLANNED_GetFromTelEmailPrintLetterByEmployeeID]
	
	@EmployeeID int
AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT
		ISNULL(E_CONTACT.WORKDD, 'N/A') AS FromDirectDial,		
		ISNULL(E_CONTACT.WORKEMAIL, 'N/A') AS FromEmail,
		ISNULL(E_JOBDETAILS.JOBTITLE, 'Job Title N/A') AS FromJobTitle
		
	FROM
		E__EMPLOYEE
		INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
		INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
		
	WHERE
		E__EMPLOYEE.EMPLOYEEID = @EmployeeID
		
END
