USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetPendingComponentTradeAppointments]    Script Date: 12/06/2013 19:42:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[PLANNED_GetConfirmAppointments]
--		@propertyId = N'A010060001',
--		@journalId = 1
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,6th Dec,2013>
-- Description:	<Description,,This stored procedure 'll return the pending pending appointments against the property & journal id>
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetConfirmAppointments]
	@propertyId varchar(20),
	@journalId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT 
	PLANNED_APPOINTMENTS.APPOINTMENTID as AppointmentId
	,PLANNED_APPOINTMENTS.JournalId as JournalId
	,PLANNED_APPOINTMENTS.COMPTRADEID as ComponentTradeId
	,E__EMPLOYEE.EMPLOYEEID AS OperativeId
	,ISNULL(E__EMPLOYEE.FIRSTNAME ,'')+' '+ISNULL(E__EMPLOYEE.LASTNAME ,'')	as OperativeName	
	,convert(datetime,convert(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE,103) + ' ' + PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME,103)as StartDate
	,convert(datetime,convert(varchar(10), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE,103) + ' ' + PLANNED_APPOINTMENTS.APPOINTMENTENDTIME,103)as EndDate		
	FROM 
		PLANNED_APPOINTMENTS 
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN E__EMPLOYEE ON PLANNED_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
	WHERE 
	1=1
	AND PLANNED_APPOINTMENTS.ISPENDING = 1
	AND PLANNED_JOURNAL.PROPERTYID =@propertyId
	AND PLANNED_JOURNAL.JOURNALID = @journalId

END
