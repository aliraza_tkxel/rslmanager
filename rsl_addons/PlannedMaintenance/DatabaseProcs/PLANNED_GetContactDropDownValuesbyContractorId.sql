
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 22/10/2014
-- Description:	Get the employeeId and employeeName as contacts of the organization.
-- =============================================
CREATE PROCEDURE PLANNED_GetContactDropDownValuesbyContractorId 
	-- Add the parameters for the stored procedure here
	@contractorId INT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here

SELECT
	E.EMPLOYEEID AS [id],
    ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') AS [description]
    FROM E__EMPLOYEE E
    INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID AND JD.ACTIVE = 1
    WHERE E.ORGID = @contractorId

END
GO