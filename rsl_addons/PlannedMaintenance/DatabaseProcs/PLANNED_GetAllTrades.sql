USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC PLANNED_GetAllTrades

-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,30/Oct/2013>
-- Description:	<Description,,PLANNED_GetAllTrades >
-- Web Page: ReplacementList.aspx => Get all trades 
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetAllTrades](
@componentId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	SELECT	G_TRADE.TradeId as TradeId, G_TRADE.Description as Description		
	From	G_TRADE  							  

	--SELECT	G_TRADE.TradeId TradeId, G_TRADE.Description Description
	--FROM	PLANNED_COMPONENT_TRADE 
	--		INNER JOIN G_TRADE ON PLANNED_COMPONENT_TRADE.TRADEID = G_TRADE.TradeId
	--WHERE	PLANNED_COMPONENT_TRADE.COMPONENTID = @componentId
					
END
