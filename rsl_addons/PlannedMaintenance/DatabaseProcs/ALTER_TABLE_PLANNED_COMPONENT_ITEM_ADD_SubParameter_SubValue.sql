/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	8-4-2014
--  Description:	SubParameter and SubValue columns are added in mapping table inorder to map parameter value to parameter value mapping.
--					e.g 'Heating - Main Gas - Wall Heater'				
 '==============================================================================*/

ALTER TABLE PLANNED_COMPONENT_ITEM ADD SubParameter int NULL

ALTER TABLE PLANNED_COMPONENT_ITEM ADD SubValue int NULL


