/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	17 April 2014
--  Description:	Update items and parameters.
--					
 '==============================================================================*/

--========================================================
-- UPDATE ITEM LEVEL
--========================================================

--	Appliance Servicing
--	Heating & Water Supply
Declare @HeatingAndWaterId INT ,@Emitters INT

UPDATE	PA_ITEM SET ItemName = 'Appliance Servicing',ItemSorder = 2 WHERE ItemName = 'Gas'
UPDATE	PA_ITEM SET ItemSorder = 2 WHERE ItemName = 'Appliance Servicing'
INSERT INTO PA_ITEM ([AreaID],[ItemName],[ItemSorder],[IsActive],[ParentItemId])
VALUES (8,'Heating & Water Supply',3,1,NULL)
SELECT	@HeatingAndWaterId = ItemID FROM PA_ITEM WHERE ItemName = 'Heating & Water Supply'
       
   
--	Heating
--	Emitters
--	Hot Water Supply

UPDATE	PA_ITEM SET ItemSorder = 1,ParentItemId =@HeatingAndWaterId  WHERE ItemName = 'Heating'

INSERT INTO PA_ITEM ([AreaID],[ItemName],[ItemSorder],[IsActive],[ParentItemId])
VALUES (8,'Emitters',2,1,@HeatingAndWaterId)
SELECT	@Emitters = ItemID FROM PA_ITEM WHERE ItemName = 'Emitters'

UPDATE	PA_ITEM SET ItemName = 'Hot Water Supply', ItemSorder = 3,ParentItemId =@HeatingAndWaterId WHERE ItemName = 'Water'



--========================================================
-- UPDATE PARAMETER LEVEL
--========================================================

-----------
-- Heating 
-----------

-- Primary heating fuel

--========================
--  (ONLY FOR DEVELOPMENT)
--========================

--INSERT INTO PA_PARAMETER_VALUE([ParameterID],[ValueDetail],[Sorder],[IsActive])
--VALUES (65 ,'Mains Electric',2,1)
--INSERT INTO PA_PARAMETER_VALUE([ParameterID],[ValueDetail],[Sorder],[IsActive])
--VALUES (65 ,'Solar',8,1)

--UPDATE PA_PARAMETER_VALUE SET Sorder= 6 WHERE ValueDetail = 'Air Source'
--UPDATE PA_PARAMETER_VALUE SET Sorder= 7 WHERE ValueDetail = 'Ground Source'

--UPDATE PA_PARAMETER_VALUE SET Sorder= 3 WHERE ValueDetail = 'LPG'
--UPDATE PA_PARAMETER_VALUE SET Sorder= 4 WHERE ValueDetail = 'Oil'
--UPDATE PA_PARAMETER_VALUE SET Sorder= 5 WHERE ValueDetail = 'Wind'
--UPDATE PA_PARAMETER_VALUE SET ValueDetail = 'Mains Gas' WHERE ValueDetail = 'Main gas'

--========================
-- (ONLY FOR TEST)
--========================

UPDATE PA_PARAMETER_VALUE SET ValueDetail = 'Mains Gas' WHERE ValueDetail = 'Main gas'

INSERT INTO PA_PARAMETER_VALUE([ParameterID],[ValueDetail],[Sorder],[IsActive])
VALUES (65 ,'Mains Electric',2,1)

UPDATE PA_PARAMETER_VALUE SET Sorder= 3 WHERE ValueDetail = 'LPG'
UPDATE PA_PARAMETER_VALUE SET Sorder= 4 WHERE ValueDetail = 'Oil'
UPDATE PA_PARAMETER_VALUE SET Sorder= 5 WHERE ValueDetail = 'Wind'
UPDATE PA_PARAMETER_VALUE SET Sorder= 6 WHERE ValueDetail = 'Air Source'
UPDATE PA_PARAMETER_VALUE SET Sorder= 7 WHERE ValueDetail = 'Ground Source'

INSERT INTO PA_PARAMETER_VALUE([ParameterID],[ValueDetail],[Sorder],[IsActive])
VALUES (65 ,'Solar',8,1)


--Boiler type
DECLARE @boilerTypeParamId int ,@heatingItemId int

SELECT	@heatingItemId = ItemID 
FROM	PA_ITEM 
WHERE	ItemName = 'Heating'

INSERT INTO PA_PARAMETER( ParameterName, DataType, ControlType, IsDate, ParameterSorder,IsActive)
VALUES('Boiler type','String','Dropdown',0,4,1 )       

SET @boilerTypeParamId = SCOPE_IDENTITY();
INSERT INTO PA_ITEM_PARAMETER ( ItemId, ParameterId, IsActive )
VALUES(@heatingItemId,@boilerTypeParamId,1)

INSERT INTO PA_PARAMETER_VALUE ( ParameterID, ValueDetail, Sorder, IsActive)
VALUES( @boilerTypeParamId,'Traditional Boiler', 1, 1),
( @boilerTypeParamId,'Fire', 2, 1) ,
( @boilerTypeParamId,'Combi Boiler', 3, 1) ,
( @boilerTypeParamId,'Wall Heater', 4, 1) 


-----------
-- Emitters
-----------

--Emitters type
DECLARE @emittersTypeParamId int,@emitterItemId int

SELECT	@emitterItemId = ItemID 
FROM	PA_ITEM 
WHERE	ItemName = 'Emitters'

INSERT INTO PA_PARAMETER( ParameterName, DataType, ControlType, IsDate, ParameterSorder,IsActive)
VALUES('Emitter type','String','Dropdown',0,1,1 )       

SET @emittersTypeParamId = SCOPE_IDENTITY();
INSERT INTO PA_ITEM_PARAMETER ( ItemId, ParameterId, IsActive )
VALUES(@emitterItemId,@emittersTypeParamId,1)

INSERT INTO PA_PARAMETER_VALUE ( ParameterID, ValueDetail, Sorder, IsActive)
VALUES( @emittersTypeParamId,'Radiators', 1, 1),
( @emittersTypeParamId,'Gas Fire', 2, 1) ,
( @emittersTypeParamId,'Under Floor', 3, 1) ,
( @emittersTypeParamId,'Night Storage', 4, 1) 

-- Number of emitters
UPDATE	PA_PARAMETER SET ParameterName = 'Number of emitters',ParameterSorder = 2 WHERE ParameterName =  'No of Emitters'
UPDATE	PA_ITEM_PARAMETER SET ItemId = @emitterItemId WHERE ItemId = 74 AND ParameterId = 69

-- Control type
UPDATE	PA_PARAMETER SET ParameterSorder = 3 WHERE ParameterName =  'Control Type'
UPDATE	PA_ITEM_PARAMETER SET ItemId = @emitterItemId WHERE ItemId = 74 AND ParameterId = 70

--Last Replaced & Replacement due Condition Rating
UPDATE	PA_PARAMETER SET IsActive = 0 WHERE ParameterName =  'Emitters Replacement Due'
UPDATE	PA_PARAMETER SET IsActive = 0 WHERE ParameterName =  'Emitters Last Replaced'

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive )
VALUES(@emitterItemId,127,1), (@emitterItemId,128,1),(@emitterItemId,71,1)

-------------------
-- Hot Water Supply
-------------------

-- Fuel Type (!!!!!!!!!! WAITING FOR FEEDBACK !!!!!!!!!!)
--UPDATE	PA_PARAMETER SET IsActive = 0 WHERE ParameterID = 148

DECLARE @FuelTypeParamId int ,@hotWaterSupplyItemId int

SELECT	@hotWaterSupplyItemId = ItemID 
FROM	PA_ITEM 
WHERE	ItemName = 'Hot Water Supply'

INSERT INTO PA_PARAMETER( ParameterName, DataType, ControlType, IsDate, ParameterSorder,IsActive)
VALUES('Fuel type','String','Dropdown',0,1,1 )       

SET @FuelTypeParamId = SCOPE_IDENTITY();
INSERT INTO PA_ITEM_PARAMETER ( ItemId, ParameterId, IsActive )
VALUES(@hotWaterSupplyItemId,@FuelTypeParamId,1)

INSERT INTO PA_PARAMETER_VALUE ( ParameterID, ValueDetail, Sorder, IsActive)
VALUES
( @FuelTypeParamId,'Mains Gas', 1, 1),
( @FuelTypeParamId,'Mains Electric', 2, 1) ,
( @FuelTypeParamId,'LPG', 3, 1) ,
( @FuelTypeParamId,'Oil', 4, 1) ,
( @FuelTypeParamId,'Wind', 5, 1) ,
( @FuelTypeParamId,'Air Source', 6, 1) , 	
( @FuelTypeParamId,'Ground Source', 7, 1) , 
( @FuelTypeParamId,'Solar', 8, 1)


-- Water heater type

DECLARE @waterHeaterTypeParamId int, @hotWaterSupplyTypeItemId int

SELECT	@hotWaterSupplyTypeItemId = ItemID 
FROM	PA_ITEM 
WHERE	ItemName = 'Hot Water Supply'

INSERT INTO PA_PARAMETER( ParameterName, DataType, ControlType, IsDate, ParameterSorder,IsActive)
VALUES('Water heater type','String','Dropdown',0,2,1 )       

SET @waterHeaterTypeParamId = SCOPE_IDENTITY();
INSERT INTO PA_ITEM_PARAMETER ( ItemId, ParameterId, IsActive )
VALUES(@hotWaterSupplyTypeItemId,@waterHeaterTypeParamId,1)

INSERT INTO PA_PARAMETER_VALUE ( ParameterID, ValueDetail, Sorder, IsActive)
VALUES( @waterHeaterTypeParamId,'Traditional Boiler', 1, 1),
( @waterHeaterTypeParamId,'Fire', 2, 1) ,
( @waterHeaterTypeParamId,'Combi Boiler', 3, 1) ,
( @waterHeaterTypeParamId,'Wall Heater', 4, 1) 

-- Update SOrder Boiler Manufacturer, Immersion Type, Cylinder Insulation, Stop Cock Location

UPDATE PA_PARAMETER SET ParameterSorder = 3 WHERE ParameterName = 'Boiler Manufacturer'
UPDATE PA_PARAMETER SET ParameterSorder = 4 WHERE ParameterName = 'Immersion Type'
UPDATE PA_PARAMETER SET ParameterSorder = 5 WHERE ParameterName = 'Cylinder Insulation'
UPDATE PA_PARAMETER SET ParameterSorder = 6 WHERE ParameterName = 'Stop Cock Location'
UPDATE PA_PARAMETER SET IsActive = 0 WHERE ParameterName = 'Gas Heater Manufacturer'

