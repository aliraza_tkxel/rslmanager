USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetConditionRatingList]    Script Date: 01/07/2016 10:18:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <07/16/2014>
-- Description:	<Get condition rating list>
-- Web Page: ConditionRating.aspx
-- EXEC PLANNED_GetConditionRatingList 'Broughton Close Hindolveston', 1
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetConditionRatingList]
( 
	-- Add the parameters for the stored procedure here
		@searchText varchar(200),
		@isFullList bit,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address',
		@sortOrder varchar (5) = 'DESC'
		,@totalCount int = 0 output	
)
AS
BEGIN
	DECLARE 
		@SelectClause varchar(2500),
        @fromClause   varchar(2500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(500),	
        @mainSelectQuery varchar(6000),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(8000),

        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int,
		@dataLimitation varchar(10)
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria =	' 1=1 AND [PLA].Title = ''Recommended''' 
		 			
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN									
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( P.POSTCODE LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P.Address LIKE ''%' + @searchText + '%'') '
		END	
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		if (@isFullList = 1) 
			set @dataLimitation = ''
		else
			set @dataLimitation = 'TOP ('+CONVERT(VARCHAR(10),@limit)+')'
		
		SET @selectClause = 'SELECT '+@dataLimitation+'
		 
		PCW.ConditionWorksId as ConditionWorksId
		,P.PROPERTYID as PropertyId
		,P.Address AS Address
		,P.POSTCODE As Postcode
		,ISNULL(L.LocationName + '' > '' ,'''') + A.AreaName [Location]
		,COALESCE(PC.COMPONENTNAME,I.ITEMNAME,''N/A'') AS Component
		,[ATT].PARAMETERVALUE AS [Condition]
		--,COALESCE(CONVERT(NVARCHAR,YEAR(COMPONENT_DATES.DueDate)),CONVERT(NVARCHAR,YEAR(NON_COMPONENT_DATES.DueDate)),''-'') AS Due
		,CASE	WHEN [PCW].ComponentId IS NULL THEN
					
					COALESCE(CONVERT(NVARCHAR,YEAR(NON_COMPONENT_DATES.DueDate)),''-'')
						 
				ELSE 
					CONVERT(NVARCHAR,YEAR(COMPONENT_DATES.DueDate))
		END AS Due		
		,ISNULL(LEFT(E.FIRSTNAME,1) + LEFT(E.LASTNAME,1), ''NA'') + ISNULL('' '' + CONVERT(NVARCHAR, [ATT].UPDATEDON,103), '''')  AS [By]
		,I.ItemName [ItemName]
		,P.Address1 as Address1
		,P.Towncity as Towncity
		,EC.MOBILE AS Mobile
		,P.HouseNumber as HouseNumber
		'
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'FROM 
		PLANNED_CONDITIONWORKS [PCW] 
		INNER JOIN	PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID
		INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId  
		INNER JOIN (SELECT	P__PROPERTY.PROPERTYID AS PROPERTYID
					, ISNULL(NULLIF(ISNULL(''Flat No:'' + FLATNUMBER + '', '', '''') + ISNULL(HOUSENUMBER, '''') + ISNULL('' '' + ADDRESS1, '''')+ ISNULL('' '' + ADDRESS2, '''')+ ISNULL('' '' + ADDRESS3, '''') ,''''),''N/A'') AS [Address]
					,P__PROPERTY.POSTCODE AS POSTCODE
					,ISNULL(HOUSENUMBER, '''') + ISNULL('' '' + ADDRESS1, '''') AS Address1
					,ISNULL(TOWNCITY, '''') AS Towncity
					,HOUSENUMBER as HouseNumber 
					FROM P__PROPERTY) AS P ON P.PROPERTYID = [ATT].PROPERTYID

		INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID
		INNER JOIN PA_PARAMETER [PARAM] ON [PARAM].ParameterID = IP.ParameterId AND PARAM.ParameterName LIKE ''%Condition Rating%'' AND ([ATT].PARAMETERVALUE = ''Unsatisfactory'' OR [ATT].PARAMETERVALUE = ''Potentially Unsatisfactory'')
		INNER JOIN PA_PARAMETER_VALUE PV ON PV.ValueID = [ATT].VALUEID AND (PV.ValueDetail = ''Unsatisfactory'' OR PV.ValueDetail = ''Potentially Unsatisfactory'')
		INNER JOIN PA_ITEM [I] ON [I].ItemId = [IP].ItemId 
		INNER JOIN PA_AREA [A] ON [A].AreaID = [I].AreaID
		INNER JOIN PA_LOCATION [L] ON [L].LocationID = [A].LocationId
		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = [ATT].UPDATEDBY
		LEFT JOIN E_CONTACT EC ON E.EMPLOYEEID = EC.EMPLOYEEID
		
		LEFT JOIN (			SELECT	MAX(SID) AS SID,PROPERTYID, PLANNED_COMPONENTID
					FROM	PA_PROPERTY_ITEM_DATES		
					WHERE	 PLANNED_COMPONENTID IS NOT NULL
					GROUP BY PROPERTYID,planned_componentid  ) PID_COMPONENTS ON [PCW].ComponentId = PID_COMPONENTS.PLANNED_COMPONENTID AND [ATT].PROPERTYID = PID_COMPONENTS.PROPERTYID 
		LEFT JOIN PA_PROPERTY_ITEM_DATES [COMPONENT_DATES] ON PID_COMPONENTS.SID = [COMPONENT_DATES].SID

		LEFT JOIN (	SELECT	MAX(SID) AS SID,PROPERTYID, ItemId
					FROM	PA_PROPERTY_ITEM_DATES		
					--WHERE	 PLANNED_COMPONENTID IS NULL
					GROUP BY PROPERTYID,ItemId  ) AS PID_NON_COMPONENTS ON [IP].ItemId = PID_NON_COMPONENTS.ItemId AND [ATT].PROPERTYID = PID_NON_COMPONENTS.PROPERTYID
		LEFT JOIN PA_PROPERTY_ITEM_DATES NON_COMPONENT_DATES ON PID_NON_COMPONENTS.SID = NON_COMPONENT_DATES.SID
		LEFT JOIN PLANNED_COMPONENT [PC] ON [PC].COMPONENTID = [PCW].COMPONENTID
'
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' cast(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 	
			
		END
		
		IF(@sortColumn = 'By')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' [By]' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		PRINT @finalQuery
		
		--========================================================================================
		-- Begin - Execute the Query 
		
		if (@isFullList = 1) 
			BEGIN
				EXEC (@mainSelectQuery)
			END		
		else
			BEGIN	
				EXEC (@finalQuery)
			END
		
																										
		-- End - Execute the Query 
		--========================================================================================	
		PRINT 'Done Final Query'
		-- End building the main select Query
		--========================================================================================																																			

		Declare @selectTotalcount nvarchar(4000)
		SET @selectTotalcount = 'SELECT count(*) as TotalCount' + @fromClause + @whereClause
		PRINT @selectTotalcount
		EXEC (@selectTotalcount)

		
		--========================================================================================		
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
	
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================					
END
