USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PLANNED_GetYearlyComponentPropertyList]    Script Date: 01/11/2016 12:44:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Author,Ahmed Mehmood>
--		DECLARE	@return_value int,
--		@totalCount int

--		EXEC	[dbo].[PLANNED_GetYearlyComponentPropertyList]
--		@componentId = 40,
--		@operator = 0,
--		@year = N'2013',
--		@schemeId = -1,
--		@searchText = N'C03',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Address',
--		@sortOrder = N'desc',
--		@totalCount = @totalCount OUTPUT
-- Create date: <Create Date,17/3/2014>
-- Description: Returns yearly component property list.
--Last modified Date:17/3/2014
-- =============================================

	ALTER PROCEDURE [dbo].[PLANNED_GetYearlyComponentPropertyList]
	-- Add the parameters for the stored procedure here
		@componentId int
		,@operator int
		,@year varchar(10)
		,@schemeId int
		,@isFullList bit
		,@dueDateDt as PLANNED_PROPERTY_REPLACEMENT_DUEDATES readonly
		,@editedComponentsDt as PLANNED_EDITED_COMPONENTS readonly

	-- Parameters which would help in sorting and paging
		,@pageSize int = 30
		,@pageNumber int = 1
		,@sortColumn varchar(500) = 'Address'
		,@sortOrder varchar (5) = 'DESC'

AS
BEGIN
	DECLARE 

		@SelectClause varchar(8000),
        @fromClause   varchar(8000),
        @whereClause  varchar(8000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(8000),        
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(3000),

        --variables for paging
        @offset int,
		@limit int
		,@dataLimitation varchar(10)

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided

		SET @searchCriteria = ' 1=1 AND PID.PLANNED_COMPONENTID = '+ +CONVERT(nvarchar(10), @componentId) 	  


		-- YEAR OPERATOR 
		-- -1 => "PREVIOUS YEARS"
		-- 0 => "CURRENT YEAR"
		-- 1 => "UPCOMING YEARS"

		IF @operator = -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(VARCHAR(10),YEAR(PID.DueDate)) < '+CONVERT(nvarchar(10), @year) 	 	
		ELSE IF @operator = 0
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(VARCHAR(10),YEAR(PID.DueDate)) = '+CONVERT(nvarchar(10), @year) 	 	
		ELSE
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND CONVERT(VARCHAR(10),YEAR(PID.DueDate)) > '+CONVERT(nvarchar(10), @year) 	


		IF @schemeId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P.SCHEMEID = '+CONVERT(nvarchar(10), @schemeId) 	


		-- End building SearchCriteria clause   
		--========================================================================================


		--=========================================================
		-- CONVERT VARIABLE TABLES TO #TEMP TABLES FOR MANIPULATION
		--=========================================================

		CREATE TABLE 
		#TMP_EDITED_COMPONENTS
		(TMP_COMPONENTID smallint
		,TMP_COMPONENTNAME nvarchar(100)
		,TMP_CYCLE INT
		,TMP_FREQUENCY NVARCHAR(10)
		,TMP_MATERIALCOST FLOAT
		,TMP_LABOURCOST FLOAT
		,TMP_COSTFLAG bit
		,TMP_CYCLEFLAG bit)

		INSERT	INTO #TMP_EDITED_COMPONENTS (TMP_COMPONENTID, TMP_COMPONENTNAME,TMP_CYCLE,TMP_FREQUENCY,TMP_MATERIALCOST,TMP_LABOURCOST,TMP_COSTFLAG,TMP_CYCLEFLAG) 
		SELECT	COMPONENTID, COMPONENTNAME, CYCLE,FREQUENCY,MATERIALCOST,LABOURCOST,COSTFLAG,CYCLEFLAG
		FROM	@editedComponentsDt 

		CREATE TABLE 
		#TMP_UPDATED_PROPERTY_DUES
		( TMP_SID int
		, TMP_DUEDATE VARCHAR(10) )

		INSERT	INTO #TMP_UPDATED_PROPERTY_DUES (TMP_SID, TMP_DUEDATE) 
		SELECT	SID, DUEDATE 
		FROM	@dueDateDt 

		--=========================================================


		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here

		IF (@isFullList = 1) SET @dataLimitation = '' ELSE SET @dataLimitation = 'top (' + CONVERT(VARCHAR(10), @limit) + ')'


		SET @selectClause = 'SELECT ' + @dataLimitation + '

		  PID.PROPERTYID AS Ref 
		,ISNULL(PD.SCHEMENAME,'''') AS Scheme	
		,ISNULL(PROPERTYADDRESS.Address,'''') AS Address
		,ISNULL(P.POSTCODE,'''') AS Postcode
		,ISNULL(PC.COMPONENTNAME,'''') AS Component
		,ISNULL(Year(PID.LastDone),'''') AS Replaced
		,ISNULL(Year(PID.DueDate),'''') AS Due
		,ISNULL(dbo.PLANNED_fnDecimalToCurrency(PID.Cost),'''') AS Cost 

		-- For Sorting
		,ISNULL(PID.SID,'''') as DueSID
		,ISNULL(P.HouseNumber,'''') as HouseNumber
		,ISNULL(P.ADDRESS1,'''') as PrimaryAddress

		'


		-- End building SELECT clause
		--======================================================================================== 							


		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +'	

		FROM	
		(	SELECT	DISTINCT	PID_IN.SID AS SID
					,PID_IN.PROPERTYID AS PROPERTYID
					,PID_IN.PLANNED_COMPONENTID AS PLANNED_COMPONENTID
					,CASE  
						WHEN	TEC.TMP_COMPONENTID IS NOT NULL AND TUPD.TMP_SID IS NOT NULL THEN						
									TUPD.TMP_DUEDATE 													
						WHEN	TEC.TMP_COMPONENTID IS NOT NULL THEN
								CASE 
									WHEN  TEC.TMP_CYCLEFLAG = 1 THEN
										CASE	
											WHEN TEC.TMP_FREQUENCY = ''Years''  THEN
												DATEADD(YYYY,TEC.TMP_CYCLE,PID_IN.LastDone)
											ELSE
												DATEADD(MM,TEC.TMP_CYCLE,PID_IN.LastDone)
										END
									ELSE
									   PID_IN.DueDate 	
								END
						WHEN	TUPD.TMP_SID IS NOT NULL THEN
									TUPD.TMP_DUEDATE 
						ELSE
									PID_IN.DueDate 
					END as DueDate
					,PID_IN.LastDone AS LastDone
					,CASE	
						WHEN	TEC.TMP_COMPONENTID IS NOT NULL   THEN
								TEC.TMP_LABOURCOST +TEC.TMP_MATERIALCOST 
						ELSE	
								PC.LABOURCOST +PC.MATERIALCOST
					END AS Cost

			FROM	PA_PROPERTY_ITEM_DATES AS PID_IN
					INNER JOIN PLANNED_COMPONENT_ITEM AS PCI ON PID_IN.PLANNED_COMPONENTID = PCI.COMPONENTID
					INNER JOIN PLANNED_COMPONENT AS PC ON PCI.COMPONENTID = PC.COMPONENTID 
					LEFT JOIN #TMP_EDITED_COMPONENTS AS TEC ON PCI.COMPONENTID = TEC.TMP_COMPONENTID 
					LEFT JOIN #TMP_UPDATED_PROPERTY_DUES AS TUPD ON PID_IN.SID = TUPD.TMP_SID
					LEFT JOIN 
										(SELECT	DISTINCT PPA_NUM.PROPERTYID,PIP_NUM.ItemId,PPA_NUM.PARAMETERVALUE
										FROM	PA_PROPERTY_ATTRIBUTES PPA_NUM
												INNER JOIN PA_ITEM_PARAMETER PIP_NUM ON PPA_NUM.ITEMPARAMID = PIP_NUM.ITEMPARAMID
												INNER JOIN PA_PARAMETER PP_NUM ON PIP_NUM.ParameterId = PP_NUM.ParameterID											
										WHERE	PP_NUM.ParameterName = ''Number'' 
										) PNP ON PCI.ITEMID = PNP.ItemId AND  PID_IN.PROPERTYID = PNP.PROPERTYID							
									
					LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_PARAM ON PCI.ITEMID = P_PARAM.ItemId
						AND P_PARAM.ParameterId =  PCI.PARAMETERID
                        AND PID_IN.PROPERTYID = P_PARAM.PROPERTYID
                        
                    LEFT JOIN (SELECT
                        PPA_PARAM.PROPERTYID,
                        PIP_PARAM.ItemId,
                        PIP_PARAM.ParameterId,
                        PPA_PARAM.VALUEID
                      FROM PA_PROPERTY_ATTRIBUTES PPA_PARAM
                          INNER JOIN PA_ITEM_PARAMETER PIP_PARAM ON PIP_PARAM.ITEMPARAMID = PPA_PARAM.ITEMPARAMID
                      ) P_SUBPARAM ON PCI.ITEMID = P_SUBPARAM.ItemId
						AND P_SUBPARAM.ParameterId =  PCI.SubParameter
                        AND PID_IN.PROPERTYID = P_SUBPARAM.PROPERTYID

                WHERE	CASE	WHEN PC.COMPONENTNAME LIKE ''%windows%'' THEN 
								CASE WHEN PNP.PARAMETERVALUE NOT IN (''0'', ''Not Applicable'') OR PNP.PARAMETERVALUE IS NULL THEN 
									1
									ELSE 
									0
								END 
							WHEN PCI.SUBVALUE IS NOT NULL THEN 
								   CASE WHEN P_SUBPARAM.VALUEID = PCI.SUBVALUE AND P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END  
							WHEN PCI.VALUEID IS NOT NULL AND PCI.SUBVALUE IS NULL THEN 
								   CASE WHEN P_PARAM.VALUEID = PCI.VALUEID THEN 
									1
									ELSE 
									0
									END                  
						ELSE 
								1
						END = 1
					 							
			) AS PID
		INNER JOIN P__PROPERTY AS P ON PID.PROPERTYID = P.PROPERTYID 
		INNER JOIN (SELECT	P__PROPERTY.PROPERTYID as PROPERTYID
							,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'' '') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
					FROM	P__PROPERTY)AS PROPERTYADDRESS ON P.PROPERTYID = PROPERTYADDRESS.PROPERTYID 
		INNER JOIN P_SCHEME AS PD on P.SCHEMEID = PD.SCHEMEID  
		INNER JOIN PLANNED_COMPONENT AS PC ON PID.PLANNED_COMPONENTID = PC.COMPONENTID 		

		'
		-- End building From clause
		--======================================================================================== 														  


		--========================================================================================    
		-- Begin building OrderBy clause		

		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' CAST(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 				
		END


		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		-- End building OrderBy clause
		--========================================================================================								

		--========================================================================================
		-- Begin building WHERE clause

		-- This Where clause contains subquery to exclude already displayed records			  

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		-- End building WHERE clause
		--========================================================================================

		--========================================================================================
		-- Begin building the main select Query

		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query

		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		-- End building the row number query
		--========================================================================================

		--========================================================================================
		-- Begin building the final query 

		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		-- End building the final query
		--========================================================================================									

		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		IF (@isFullList = 1) 
			BEGIN
				EXEC (@mainSelectQuery)
			END 
		ELSE 
			BEGIN
				EXEC (@finalQuery)
			END
																											
		-- End - Execute the Query 
		--========================================================================================									

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectTotalAmount varchar(MAX)
		SET @selectTotalAmount = 'SELECT dbo.PLANNED_fnDecimalToCurrency(SUM(PID.Cost)) as TotalCost ' + @fromClause + @whereClause

		print(@selectTotalAmount)
		EXEC (@selectTotalAmount)


		Declare @selectCount varchar(MAX), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT count(*) as TotalCount ' + @fromClause + @whereClause

		print(@selectCount)
		EXEC (@selectCount)

		DROP TABLE #TMP_EDITED_COMPONENTS
		DROP TABLE #TMP_UPDATED_PROPERTY_DUES

		-- End building the Count Query
		--========================================================================================	


END
