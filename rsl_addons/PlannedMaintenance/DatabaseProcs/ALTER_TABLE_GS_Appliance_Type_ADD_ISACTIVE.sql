/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	21-4-2014
--  Description:	isActive is used to populate ACTIVE Appliance Type
--					
 '==============================================================================*/

ALTER TABLE GS_Appliance_Type ADD ISACTIVE BIT NULL
UPDATE GS_Appliance_Type SET ISACTIVE = 1