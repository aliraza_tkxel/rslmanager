USE [RSLBHALive]
GO 
--Created By : Ali Raza
--Description:This is table valued parameter for insertion of trade in component life cycle
--This shall be used to insert the multiple records in table at once.
--Web Page:Components.aspx

/****** Object:  UserDefinedTableType [dbo].[PLANNEDCOMPONENTTRADE]    Script Date: 10/23/2013 12:44:30 ******/
CREATE TYPE [dbo].[PLANNEDCOMPONENTTRADE] AS TABLE(
	[TRADEID] [int] NULL,
	[DURATION] [float] NULL,
	[SORDER] [int] NULL
)
GO

