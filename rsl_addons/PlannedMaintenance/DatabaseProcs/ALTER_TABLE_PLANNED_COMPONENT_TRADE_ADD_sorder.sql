/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	22th November 2013
--  Description:	sorder is added to identify the order in which trade is added
--					
 '==============================================================================*/
 
USE [RSLBHALive]
GO

ALTER TABLE PLANNED_COMPONENT_TRADE
ADD sorder INT NOT NULL DEFAULT(1)
