
/* ===========================================================================
--  DATE CREATED:	13 March 2014
--  Description:	Update componentid in PA_PROPERTY_ITEM_DATES from mapping table.					
'==============================================================================*/


--====================================================
-- ITEM
--====================================================


UPDATE	PA_PROPERTY_ITEM_DATES
SET		PLANNED_COMPONENTID = PCI.COMPONENTID 
FROM	PLANNED_COMPONENT_ITEM AS PCI
WHERE	PCI.ITEMID = PA_PROPERTY_ITEM_DATES.ItemId
		AND  PCI.PARAMETERID IS NULL
		AND PCI.VALUEID IS NULL
		AND PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID IS NULL
	
--====================================================
-- PARAMETER
--====================================================
		
		
UPDATE	PA_PROPERTY_ITEM_DATES
SET		PLANNED_COMPONENTID = PCI.COMPONENTID
FROM	PLANNED_COMPONENT_ITEM AS PCI
WHERE	PA_PROPERTY_ITEM_DATES.ITEMID = PCI.ITEMID
		AND  PA_PROPERTY_ITEM_DATES.ParameterId = PCI.PARAMETERID 
		AND  PCI.VALUEID = NULL
		AND  PA_PROPERTY_ITEM_DATES.PLANNED_COMPONENTID IS NULL
		
		
--====================================================
-- PARAMETER VALUE
--====================================================
		
DECLARE @sid int
DECLARE @itemid int
DECLARE @componentid int
DECLARE @propertyid varchar(20)

SET		@componentid = NULL

DECLARE db_cursor CURSOR FOR  
SELECT	sid,itemid,propertyid 
FROM    PA_PROPERTY_ITEM_DATES
WHERE	PLANNED_COMPONENTID IS NULL

OPEN	db_cursor   
FETCH NEXT FROM db_cursor INTO @sid,@itemid,@propertyid   

WHILE @@FETCH_STATUS = 0   
BEGIN   

		SELECT  @componentid =PCI.COMPONENTID
		FROM	PLANNED_COMPONENT_ITEM AS PCI
				INNER JOIN PA_ITEM_PARAMETER AS PIP on PCI.ITEMID = PIP.ItemId 
				AND PCI.PARAMETERID =  PIP.ParameterId
				INNER JOIN PA_PROPERTY_ATTRIBUTES AS PPA ON PIP.ItemParamID =  PPA.ITEMPARAMID AND PCI.VALUEID = PPA.VALUEID						
		WHERE	PPA.PROPERTYID = @propertyid 
				AND PIP.ITEMID = @itemid
				AND PCI.PARAMETERID IS NOT NULL
				AND PCI.VALUEID IS NOT NULL
		
		
		UPDATE	PA_PROPERTY_ITEM_DATES
		SET		PLANNED_COMPONENTID = @componentid
		WHERE	SID = @sid
		
		SET		@componentid = NULL
				             
       FETCH NEXT FROM db_cursor INTO @sid,@itemid,@propertyid  
END   

CLOSE db_cursor   
DEALLOCATE db_cursor