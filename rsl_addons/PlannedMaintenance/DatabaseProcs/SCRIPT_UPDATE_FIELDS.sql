/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	6 Dec 2013
--  Description:	Update Parameter
--					
 '==============================================================================*/

INSERT INTO [RSLBHALive].[dbo].[PA_PARAMETER_VALUE]([ParameterID],[ValueDetail],[Sorder],[IsActive])
VALUES (67 ,'ground source heat pump',9,1)
           
UPDATE PA_PARAMETER_VALUE
SET SORDER = 8, ISACTIVE =1
WHERE VALUEID= 784


update pa_parameter set parametersorder = 1 where parameterid = 63
update pa_parameter set parametersorder = 2 where parameterid = 60
update pa_parameter set parametersorder = 3 where parameterid = 59
update pa_parameter set parametersorder = 4 where parameterid = 61
update pa_parameter set parametersorder = 5 where parameterid = 62
update pa_parameter set parametersorder = 6 where parameterid = 120

update pa_parameter set parametersorder = 7 where parameterid = 121
update pa_parameter set parametersorder = 8 where parameterid = 57
update pa_parameter set parametersorder = 9 where parameterid = 147
update pa_parameter set parametersorder = 10 where parameterid = 58
update pa_parameter set parametersorder = 11 where parameterid = 122

update pa_parameter set parametername = 'CUR Due' where parameterid = 122



INSERT INTO [RSLBHALive].[dbo].[PA_ITEM_PARAMETER]([ItemId],[ParameterId],[IsActive])
VALUES(76,127,1)
           
INSERT INTO [RSLBHALive].[dbo].[PA_ITEM_PARAMETER]([ItemId],[ParameterId],[IsActive])
VALUES (76,128,1)
