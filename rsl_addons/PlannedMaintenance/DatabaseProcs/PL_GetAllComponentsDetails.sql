-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec PL_GetAllComponentsDetails
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,10/03/2014>
-- Description:	<Description,,Get all the components to edit the values>
-- WebPage: AnnualSpendReport -> Edit Parameters
-- =============================================
CREATE PROCEDURE PL_GetAllComponentsDetails 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select COMPONENTID,ComponentName as COMPONENT,CYCLE,FREQUENCY,MATERIALCOST,LABOURCOST from PLANNED_COMPONENT
END
GO
