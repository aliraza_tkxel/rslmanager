USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--	EXEC PLANNED_GetOperativesLeavesAndAppointments
		@operativeId = 615,
		@startDate = '12/12/2012'
		
-- 	Author:		Noor Muhammad
-- 	Create date: 13 Dec 2013
-- 	Description:	To Get Employee 's leaves and appointment
-- 	Web Page: ReplacementList.aspx
-- =============================================*/
ALTER PROCEDURE [dbo].[PLANNED_GetOperativesLeavesAndAppointments]
	
	@operativeId int,
	@startDate as date
AS
BEGIN
		
	SET NOCOUNT ON;
    
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days	
	
	SELECT E_ABSENCE.STARTDATE as StartDate
	, E_ABSENCE.RETURNDATE	as EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType as HolType
	,E_ABSENCE.duration as Duration
	,CASE
		WHEN (E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.STARTDATE)))
			THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
		WHEN HolType = '' THEN '08:00 AM'
		WHEN HolType = 'M' THEN '09:00 AM'
		WHEN HolType = 'A' THEN '01:00 PM'
		WHEN HolType = 'F' THEN '00:00 AM'
		WHEN HolType = 'F-F' THEN '00:00 AM'
		WHEN HolType = 'F-M' THEN '00:00 AM'
		WHEN HolType = 'A-F' THEN '01:00 PM'
	END as StartTime
	,CASE
		WHEN (E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.RETURNDATE)))
			THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, E_ABSENCE.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
		WHEN HolType = '' THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00' 
		WHEN HolType = 'M' THEN '01:00 PM'
		WHEN HolType = 'A' THEN '05:00 PM'
		WHEN HolType = 'F' THEN '11:59 PM'
		WHEN HolType = 'F-F' THEN '11:59 PM'
		WHEN HolType = 'F-M' THEN '01:00 PM'
		WHEN HolType = 'A-F' THEN '11:59 PM'
	END as EndTime
	,CASE
		WHEN (E_ABSENCE.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.STARTDATE)))
			THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.STARTDATE)
		WHEN HolType = '' THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '09:00 AM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
	END as StartTimeInMin
	,CASE
		WHEN (E_ABSENCE.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, E_ABSENCE.RETURNDATE)))
			THEN DATEDIFF(mi, '1970-01-01', E_ABSENCE.RETURNDATE)
		WHEN HolType = ''
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(E_ABSENCE.DURATION_HRS, 0) + 8)) + ':00', 103))
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '05:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
	END as EndTimeInMin
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	WHERE
	(
		-- To filter for planned i.e annual leaves etc. where approval is needed
		( 
			E_ABSENCE.ITEMSTATUSID = 5
			AND itemnatureid >= 2
		)
		OR
		-- To filter for sickness leaves. where the operative is now returned to work.
		( 
			ITEMNATUREID = 1
			AND E_ABSENCE.ITEMSTATUSID = 2
			AND E_ABSENCE.RETURNDATE IS NOT NULL 
		)
	)
	AND E_ABSENCE.RETURNDATE >= @startDate
	AND E_JOURNAL.employeeid =@operativeId
	UNION ALL SELECT DISTINCT
	CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS StartDate
	,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS EndDate
	,@operativeId																																		AS OperativeId
	,'F'																																				AS HolType
	,1																																					AS duration
	,'00:00 AM'																																			AS StartTime
	,'11:59 PM'																																			AS EndTime
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
FROM
	G_BANKHOLIDAYS bh		
WHERE
	bh.BHDATE >= CONVERT(DATE, GETDATE())	
	AND bh.BHA = 1

	--=================================================================================================================
	------------------------------------------------------ Step 2------------------------------------------------------						
	--This query selects the appointments operative
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT AppointmentDate as AppointmentStartDate
	,AppointmentDate as AppointmentEndDate
	,OperativeId as OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
	,P__PROPERTY.TownCity as TownCity    
	,P__PROPERTY.County as County
	,'Fault Appointment'  as AppointmentType
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
	inner join p__property on fl_fault_log.propertyid = p__property.propertyid				
	WHERE 
	1=1
	AND appointmentdate >= @startDate
	AND (FL_FAULT_STATUS.Description <> 'Cancelled' AND FL_FAULT_STATUS.Description <> 'Complete')
	AND operativeid = @operativeId
	-----------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL 	
	SELECT 
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TownCity
		,P__PROPERTY.County as County
		,'Gas Appointment' as AppointmentType  
	FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE AS_JOURNAL.IsCurrent = 1		
		AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= @startDate	
		AND AS_APPOINTMENTS.ASSIGNEDTO =@operativeId
	-----------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,P__PROPERTY.TownCity as TownCity    
		,P__PROPERTY.County as County
		,'Planned Appointment' as AppointmentType
	FROM 
		PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,AppointmentDate,103) >= @startDate
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO = @operativeId
		
	-----------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Inspection Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentDate
		,APPOINTMENTDATE as AppointmentDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,P__PROPERTY.TownCity as TownCity    
		,P__PROPERTY.County as County
		,'Planned Appointment' as AppointmentType
	FROM 
		PLANNED_INSPECTION_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_INSPECTION_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,AppointmentDate,103) >= @startDate		
		AND PLANNED_INSPECTION_APPOINTMENTS.ASSIGNEDTO = @operativeId		
		
		
	-----------------------------------------------------------------------------------------------------------------------
	-- PDR Appointments	
UNION ALL SELECT DISTINCT
	APPOINTMENTSTARTDATE																												AS AppointmentDate
	,APPOINTMENTENDDATE																													AS AppointmentEndDate
	,ASSIGNEDTO																															AS OperativeId
	,APPOINTMENTSTARTTIME																												AS StartTime
	,APPOINTMENTENDTIME																													AS EndTime
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTSTARTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))	AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))		AS EndTimeInSec
	,COALESCE(p__property.postcode, P_SCHEME.SCHEMECODE, P_BLOCK.POSTCODE)																AS PostCode
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ISNULL(P_SCHEME.SCHEMENAME, '')
		ELSE ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
			+ ISNULL(P_BLOCK.ADDRESS1, '')
			+ ISNULL(', ' + P_BLOCK.ADDRESS2, '')
			+ ISNULL(' ' + P_BLOCK.ADDRESS3, '')
	END																																	AS Address
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN P__PROPERTY.TOWNCITY
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ''
		ELSE P_BLOCK.TOWNCITY
	END																																	AS TownCity
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN P__PROPERTY.COUNTY
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ''
		ELSE P_BLOCK.COUNTY
	END																																	AS County
	,PDR_MSATType.MSATTypeName + ' Appointment'																							AS AppointmentType
FROM
	PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
		LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID
		LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
WHERE
	PDR_APPOINTMENTS.ASSIGNEDTO = @operativeId
	AND (
	CONVERT(DATE, APPOINTMENTSTARTDATE, 103) >= CONVERT(DATE, GETDATE())
	OR CONVERT(DATE, AppointmentEndDate, 103) >= CONVERT(DATE, GETDATE())
	)
	AND (PDR_STATUS.TITLE <> 'Cancelled'
	AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	)
END
