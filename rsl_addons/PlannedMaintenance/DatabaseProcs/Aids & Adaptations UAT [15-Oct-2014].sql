DECLARE @itemId int
DECLARE @parameterId int
select @itemId=ItemID from PA_ITEM where ItemName ='Aids & Adaptations' And ParentItemId=(select ItemID from PA_ITEM where ItemName ='Bathroom ')
select @parameterId = ParameterId from PA_ITEM_PARAMETER where ItemId = @itemId 
INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail, Sorder, IsActive)Values (@parameterId,'Sliding Door',8,1)
Update PA_PARAMETER_VALUE SET IsActive =0  where ValueDetail='Change bathroom door for sliding door'
