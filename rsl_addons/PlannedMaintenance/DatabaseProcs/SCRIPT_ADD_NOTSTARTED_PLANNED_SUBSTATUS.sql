/* ===========================================================================
-- Author:		<Ahmed Mehmood>
-- Create date: <20/10/2014>
-- Description:	SCRIPT TO INSERT 'NotStarted' SUB STATUS.
 '==============================================================================*/


IF EXISTS (SELECT 1 FROM PLANNED_SUBSTATUS WHERE TITLE ='NotStarted')
BEGIN	
	PRINT 'ALREADY EXIST'
END
ELSE
BEGIN 
	DECLARE	@ARRANGED_STATUS INT

	SELECT	@ARRANGED_STATUS = STATUSID
	FROM	PLANNED_STATUS
	WHERE	TITLE = 'Arranged'

	INSERT INTO [PLANNED_SUBSTATUS]
           ([STATUSID]
           ,[TITLE])
    VALUES
           (@ARRANGED_STATUS
           ,'NotStarted')
    
    PRINT 'SUCCESSFULLY INSERTED'    
           
END



