USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PLANNED_ApproveConditionWorks 
--@conditionWorkId = 1,
--@isApproved = 1
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,21/07/2014>
-- Description:	<Description,,Approve Condition Works>
-- WebPage: ConditionRating.aspx
-- =============================================
CREATE PROCEDURE PLANNED_ApproveConditionWorks
@conditionWorkId int
,@userId int
,@isApproved int = 0 out

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
BEGIN TRY

	DECLARE @ComponentId INT, @StatusId INT, @StatusTitle nvarchar(150), @ActionId INT,
	 @ActionTitle nvarchar(150), 
	@PropertyId varchar(20),@JournalId INT

-- ===================================================================================
	-- (1) Check component from PLANNED_CONDITIONWORKS
		-- If ComponentId IS NULL  
			-- SET @StatusId 'Condition to be Arranged'
			-- SET @ActionId 'Approved'
		-- else
			-- SET @StatusId 'To be Arranged'
			-- SET @ActionId NULL
-- ===================================================================================
			
		SELECT	@ComponentId = pc.ComponentId ,@PropertyId = ppa.PROPERTYID  
		FROM	PLANNED_CONDITIONWORKS PC
				INNER JOIN PA_PROPERTY_ATTRIBUTES PPA ON PC.AttributeId = PPA.ATTRIBUTEID 
		WHERE	ConditionWorksId = 	@conditionWorkId
		
		If @ComponentId IS NULL 
		BEGIN
			SET @StatusTitle = 'Condition to be Arranged' 		
		END
		ELSE
		BEGIN		
			SET @StatusTitle = 'To be Arranged'
		END
		
			SET @ActionTitle = 'Approved'	
			
			SELECT	@StatusId = STATUSID 
			FROM	PLANNED_STATUS
			WHERE	TITLE = @StatusTitle
			
			SELECT	@ActionId = ActionId  
			FROM	PLANNED_Action 
			WHERE	TITLE = @ActionTitle
			
-- ===================================================================================			
	-- (2) Make an entry in PLANNED_JOURNAL, Get
		-- PropertyId from @PropertyId
		-- ComponentId from @ComponentId
		-- Statusid from @StatusId
		-- Actionid from @ActionId
		-- Current date 
		-- CreatedBy UserId from session
-- ===================================================================================
		
		INSERT PLANNED_JOURNAL ([PROPERTYID],[COMPONENTID],[STATUSID],[ACTIONID],[CREATIONDATE],[CREATEDBY])
		VALUES  (@PropertyId,@ComponentId,@StatusId,@ActionId,GETDATE(),@userId)
				
		SELECT @JournalId = SCOPE_IDENTITY()	
		
	
-- ===================================================================================	
	-- (3) Make an entry in PLANNED_JOURNAL_HISTORY
-- ===================================================================================	

		INSERT PLANNED_JOURNAL_HISTORY
		([JOURNALID],[PROPERTYID],[COMPONENTID],[STATUSID],[ACTIONID],[CREATIONDATE],[CREATEDBY],[NOTES],[ISLETTERATTACHED],[StatusHistoryId],[ActionHistoryId],[IsDocumentAttached]) 
		VALUES
		(@JournalId,@PropertyId,@ComponentId,@StatusId,@ActionId,GETDATE(),@userId,NULL,0,NULL,NULL,0)

-- ===================================================================================	
	-- (4) Update JournalId,ConditionAction in PLANNED_CONDITIONWORKS
-- ===================================================================================	
	
		UPDATE	PLANNED_CONDITIONWORKS
		SET		JournalId = @JournalId
				,ConditionAction = @ActionId
		WHERE	ConditionWorksId = @conditionWorkId
			
-- ===================================================================================	
	-- (5) Update in PLANNED_CONDITIONWORKS_HISTORY
-- ===================================================================================	
	
		INSERT INTO [PLANNED_CONDITIONWORKS_HISTORY]
				([ConditionWorksId],[WorksRequired],[AttributeId],[ValueId],[ConditionAction]
				,[ComponentId],[JournalId],[CreatedBy],[CreatedDate])
		SELECT	ConditionWorksId, WorksRequired, PC.AttributeId, PPA.VALUEID , PC.ConditionAction
				, PC.ComponentId, PC.JournalId, PC.CreatedBy , GETDATE()
		FROM	PLANNED_CONDITIONWORKS AS PC
				INNER JOIN PA_PROPERTY_ATTRIBUTES AS PPA ON PC.AttributeId = PPA.ATTRIBUTEID 
		WHERE	PC.ConditionWorksId = @conditionWorkId
		          
	
END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isApproved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isApproved = 1
 END
	
	
END
