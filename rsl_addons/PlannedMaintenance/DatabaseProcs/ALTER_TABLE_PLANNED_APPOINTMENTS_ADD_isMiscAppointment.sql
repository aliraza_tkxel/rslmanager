/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	25th November 2013
--  Description:	isMiscAppointment is added to identify appointment type
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE PLANNED_APPOINTMENTS
ADD isMiscAppointment bit NOT NULL DEFAULT(0)
