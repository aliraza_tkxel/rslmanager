﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System

Public Class NotesBO

#Region "Attributes"

    Private _customerNotes As String
    Private _appointmentNotes As String

#End Region

#Region "Construtor"
    Public Sub New()

        _customerNotes = String.Empty
        _appointmentNotes = String.Empty

    End Sub
#End Region

#Region "Properties"

    <StringLengthValidator(0, 1000)> _
    Public Property AppointmentNotes() As String
        Get
            Return _appointmentNotes
        End Get
        Set(ByVal value As String)
            _appointmentNotes = value
        End Set
    End Property

    <StringLengthValidator(0, 1000)> _
    Public Property CustomerNotes() As String
        Get
            Return _customerNotes
        End Get
        Set(ByVal value As String)
            _customerNotes = value
        End Set
    End Property

#End Region

End Class

