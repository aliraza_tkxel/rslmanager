﻿Imports System

Namespace PL_BusinessObject
    Public Class DropDownBO

#Region "Attributes"

        Private _Id As Integer
        Private _description As String

#End Region

#Region "Constructor"

        Public Sub New(ByVal id As Integer, ByVal name As String)
            _Id = id
            _description = name
        End Sub

#End Region

#Region "Properties"


        Public Property Id() As Integer
            Get
                Return _Id
            End Get
            Set(ByVal value As Integer)
                _Id = value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

#End Region

#Region "Functions"

#End Region

    End Class
End Namespace
