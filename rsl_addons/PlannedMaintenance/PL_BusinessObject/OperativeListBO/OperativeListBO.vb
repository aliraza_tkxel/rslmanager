﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators

Namespace PL_BusinessObject

    Public Class OperativeListBO

#Region "Private"
        Private _tradeId As Integer
        Private _duration As Decimal
        Private _sorder As Integer
#End Region

#Region "Public"
        <RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive, MessageTemplate:="Please select trade. <br/>")> _
        Public Property TradeId() As Integer
            Get
                Return _tradeId
            End Get
            Set(ByVal value As Integer)
                _tradeId = value
            End Set
        End Property
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Please select duration. <br/>")> _
        Public Property Duration() As Decimal
            Get
                Return _duration
            End Get
            Set(ByVal value As Decimal)
                _duration = value
            End Set
        End Property

        Public Property SOrder() As Integer
            Get
                Return _sorder
            End Get
            Set(ByVal value As Integer)
                _sorder = value
            End Set
        End Property

#End Region
    End Class
End Namespace
