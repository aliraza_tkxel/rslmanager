﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class RejectConditionBO

#Region "Attributes"
    Private _conditionWorkId As Integer
    Private _rejectionNotes As String
    Private _replacementDue As Integer
    Private _reasonId As Integer
    Private _userId As Integer
#End Region

#Region "Constructor"

    Public Sub New()

        _conditionWorkId = -1
        _rejectionNotes = String.Empty
        _replacementDue = -1
        _reasonId = -1
        _userId = -1

    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _conditionWorkId
    Public Property ConditionWorkId() As Integer

        Get
            Return _conditionWorkId
        End Get

        Set(ByVal value As Integer)
            _conditionWorkId = value
        End Set

    End Property

    ' Get / Set property for _rejectionNotes
    <StringLengthValidator(1, 4000, MessageTemplate:="Please enter rejection notes.")> _
    Public Property RejectionNotes() As String

        Get
            Return _rejectionNotes
        End Get

        Set(ByVal value As String)
            _rejectionNotes = value
        End Set

    End Property

    ' Get / Set property for _replacementDue

    Public Property ReplacementDue() As Integer

        Get
            Return _replacementDue
        End Get

        Set(ByVal value As Integer)
            _replacementDue = value
        End Set

    End Property

    ' Get / Set property for _reasonId
    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="Please select reason.")>
    Public Property ReasonId() As Integer
        Get
            Return _reasonId
        End Get

        Set(ByVal value As Integer)
            _reasonId = value
        End Set

    End Property

    ' Get / Set property for _userId
    Public Property UserId() As String

        Get
            Return _userId
        End Get

        Set(ByVal value As String)
            _userId = value
        End Set

    End Property

#End Region

End Class
