﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class MiscOperativeSearchBO

#Region "Attributes"

    Private _propertyId As String
    Private _propertyAddress As String
    Private _worksRequired As String

    Private _operativeTradeId As Integer
    Private _duration As Double
    Private _startDate As Date
    Private _type As Integer
    Private _selectionType As String

#End Region

#Region "Properties"

    <StringLengthValidator(1, 20, MessageTemplate:="Please select property/scheme/block.")> _
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property

    Public Property PropertyAddress() As String
        Get
            Return _propertyAddress
        End Get
        Set(ByVal value As String)
            _propertyAddress = value
        End Set
    End Property

    Public Property SelectionType() As String
        Get
            Return _selectionType
        End Get
        Set(ByVal value As String)
            _selectionType = value
        End Set
    End Property
    <RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive, MessageTemplate:="Please select Type. <br/>")> _
    Public Property Type() As Integer
        Get
            Return _type
        End Get
        Set(ByVal value As Integer)
            _type = value
        End Set
    End Property
    <StringLengthValidator(1, 4000, MessageTemplate:="Please enter works required description.")> _
    Public Property Worksrequired() As String
        Get
            Return _worksRequired
        End Get
        Set(ByVal value As String)
            _worksRequired = value
        End Set
    End Property


    Public Property OperativeTradeId() As Integer
        Get
            Return _operativeTradeId
        End Get
        Set(ByVal value As Integer)
            _operativeTradeId = value
        End Set
    End Property


    Public Property Duration() As Double
        Get
            Return _duration
        End Get
        Set(ByVal value As Double)
            _duration = value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

#End Region

End Class
