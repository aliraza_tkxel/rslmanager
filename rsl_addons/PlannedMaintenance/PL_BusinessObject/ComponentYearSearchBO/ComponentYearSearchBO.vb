﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class ComponentYearSearchBO

#Region "Attributes"
    Private _componentId As Integer
    Private _componentName As String
    Private _operator As Integer
    Private _year As String
    Private _schemeId As Integer
    Private _searchText As String
#End Region

#Region "Construtor"
    Public Sub New()

        _componentId = -1
        _componentName = String.Empty
        _operator = 0
        _year = String.Empty
        _schemeId = -1
        _searchText = String.Empty

    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _componentId
    Public Property ComponentId() As Integer

        Get
            Return _componentId
        End Get

        Set(ByVal value As Integer)
            _componentId = value
        End Set

    End Property

    ' Get / Set property for _componentName
    Public Property ComponentName() As String

        Get
            Return _componentName
        End Get

        Set(ByVal value As String)
            _componentName = value
        End Set

    End Property

    ' Get / Set property for _operator
    Public Property YearOperator() As Integer

        Get
            Return _operator
        End Get

        Set(ByVal value As Integer)
            _operator = value
        End Set

    End Property
    ' Get / Set property for _year
    Public Property DueYear() As String

        Get
            Return _year
        End Get

        Set(ByVal value As String)
            _year = value
        End Set

    End Property
    ' Get / Set property for _schemeId
    Public Property SchemeId() As Integer

        Get
            Return _schemeId
        End Get

        Set(ByVal value As Integer)
            _schemeId = value
        End Set

    End Property
    ' Get / Set property for _searchText
    Public Property SearchText() As String

        Get
            Return _searchText
        End Get

        Set(ByVal value As String)
            _searchText = value
        End Set

    End Property

#End Region

End Class
