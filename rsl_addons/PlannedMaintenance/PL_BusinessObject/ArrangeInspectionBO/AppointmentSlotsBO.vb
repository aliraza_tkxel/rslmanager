﻿Public Class AppointmentSlotsBO
    Private _startTime As String
    Public Property StartTime() As String
        Get
            Return _startTime
        End Get
        Set(ByVal value As String)
            _startTime = value
        End Set
    End Property

    Private _endTime As String
    Public Property EndTime() As String
        Get
            Return _endTime
        End Get
        Set(ByVal value As String)
            _endTime = value
        End Set
    End Property

End Class
