﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Namespace PL_BusinessObject
    Public Class ArrangeInspectionBO

#Region "Private"
        Private _propertyId As String

        Private _tenancyId As Integer
        Private _componentId As Integer
        Private _statusId As Integer
        Private _actionId As Integer
        Private _userId As Integer
        Private _operativeId As Integer
        Private _tradeId As Integer

        Private _isLetterAttached As Boolean
        Private _isDocumentAttached As Boolean

        Private _appointmentDate As String
        Private _appointmentStartTime As String
        Private _appointmentEndTime As String

        Private _letterList As List(Of Integer)
        Private _docList As List(Of String)

        Private _inspectionId As Integer
        Private _documentPath As String


#End Region

#Region "Public Properties"

        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        Public Property TenancyId() As Integer
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Integer)
                _tenancyId = value
            End Set
        End Property

        Public Property ComponentId() As Integer
            Get
                Return _componentId
            End Get
            Set(ByVal value As Integer)
                _componentId = value
            End Set
        End Property


        Public Property StatusId() As Integer
            Get
                Return _statusId
            End Get
            Set(ByVal value As Integer)
                _statusId = value
            End Set
        End Property

        <RegexValidator("^\d+$", MessageTemplate:="Please select Action.<br />")>
        Public Property ActionId() As Integer
            Get
                Return _actionId
            End Get
            Set(ByVal value As Integer)
                _actionId = value
            End Set
        End Property

        Public Property UserId() As Integer
            Get
                Return _userId
            End Get
            Set(ByVal value As Integer)
                _userId = value
            End Set
        End Property

        '<RegexValidator("^(?:[1-9]\d*(?:\.\d+)?|0\.0*[1-9]\d*)$", MessageTemplate:="Please select operative. <br />")>
        Public Property OperativeId() As Integer
            Get
                Return _operativeId
            End Get
            Set(ByVal value As Integer)
                _operativeId = value
            End Set
        End Property

        '<RegexValidator("^(?:[1-9]\d*(?:\.\d+)?|0\.0*[1-9]\d*)$", MessageTemplate:="Please select trade. <br />")>
        Public Property TradeId() As Integer
            Get
                Return _tradeId
            End Get
            Set(ByVal value As Integer)
                _tradeId = value
            End Set
        End Property


        Public Property IsLetterAttached() As Boolean
            Get
                Return _isLetterAttached
            End Get
            Set(ByVal value As Boolean)
                _isLetterAttached = value
            End Set
        End Property

        Public Property IsDocumentAttached() As Boolean
            Get
                Return _isDocumentAttached
            End Get
            Set(ByVal value As Boolean)
                _isDocumentAttached = value
            End Set
        End Property

        <NotNullValidator(MessageTemplate:="Please select date. <br />")>
        Public Property AppointmentDate() As String
            Get
                Return _appointmentDate
            End Get
            Set(ByVal value As String)
                _appointmentDate = value
            End Set
        End Property

        Public Property AppointmentStartTime() As String
            Get
                Return _appointmentStartTime
            End Get
            Set(ByVal value As String)
                _appointmentStartTime = value
            End Set
        End Property

        Public Property AppointmentEndTime() As String
            Get
                Return _appointmentEndTime
            End Get
            Set(ByVal value As String)
                _appointmentEndTime = value
            End Set
        End Property

        Public Property LetterList() As List(Of Integer)
            Get
                Return _letterList
            End Get
            Set(ByVal value As List(Of Integer))
                _letterList = value
            End Set
        End Property

        Public Property DocList() As List(Of String)
            Get
                Return _docList
            End Get
            Set(ByVal value As List(Of String))
                _docList = value
            End Set
        End Property

        Public Property InspectionId() As Integer
            Get
                Return _inspectionId
            End Get
            Set(ByVal value As Integer)
                _inspectionId = value
            End Set
        End Property

        Public Property DocumentPath() As String
            Get
                Return _documentPath
            End Get
            Set(ByVal value As String)
                _documentPath = value
            End Set
        End Property

#End Region

    End Class
End Namespace
