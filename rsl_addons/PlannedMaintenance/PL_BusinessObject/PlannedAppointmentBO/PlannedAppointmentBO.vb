﻿Imports System

Public Class PlannedAppointmentBO

#Region "Attributes"

    Private _propertyId As String

    Private _tradeName As String
    Private _tradeId As Integer
    Private _compTradeId As Integer

    Private _componentName As String
    Private _componentId As Integer

    Private _duration As Double
    'TODO:remove start time and end time
    Private _startTime As String
    Private _endTime As String
    Private _startDate As Date
    Private _endDate As Date
    'TODO:remove operative name from here and shift to SchedulingBO
    Private _operativeName As String
    'TODO:remove operative id from here and shift to SchedulingBO
    Private _operativeId As Integer
    'TODO:remove pmo from here and shift to SchedulingBO
    Private _pmo As Integer
    Private _jsn As Integer
    Private _appointmentId As Integer
    Private _startDateDefaultFormat As String
    'TODO:remove user id from here and shift to SchedulingBO
    Private _userId As Integer
    'TODO:remove tenancyid from here and shift to SchedulingBO
    Private _tenancyId As Integer
    Private _customerNotes As String
    Private _appointmentNotes As String

    Private _index As Integer

    Private _isPending As Boolean

    Private _type As String

    'TODO:remove total duration from here and shift to SchedulingBO
    ''' <summary>
    ''' This 'll have the total duration of all trades
    ''' </summary>
    ''' <remarks></remarks>
    Private _totalDuration As Double
#End Region

#Region "Construtor"
    Public Sub New()

        _propertyId = String.Empty
        _tradeName = String.Empty
        _type = String.Empty
        _tradeId = -1
        _compTradeId = -1
        _componentName = String.Empty
        _componentId = -1
        _duration = 0
        _startTime = String.Empty
        _endTime = String.Empty
        _startDate = DateTime.Now
        _endDate = DateTime.Now
        _operativeName = String.Empty
        _operativeId = -1
        _pmo = -1
        _jsn = -1
        _startDateDefaultFormat = String.Empty
        _userId = 0
        _tenancyId = -1
        _customerNotes = String.Empty
        _appointmentNotes = String.Empty
        _index = 1
        _isPending = True
        _appointmentId = -1        
        _totalDuration = 0
    End Sub
#End Region

#Region "Properties"

    Public Property AppointmentId() As Integer
        Get
            Return _appointmentId
        End Get
        Set(ByVal value As Integer)
            _appointmentId = value
        End Set
    End Property

    Public Property AppointmentNotes() As String
        Get
            Return _appointmentNotes
        End Get
        Set(ByVal value As String)
            _appointmentNotes = value
        End Set
    End Property

    Public Property CustomerNotes() As String
        Get
            Return _customerNotes
        End Get
        Set(ByVal value As String)
            _customerNotes = value
        End Set
    End Property

    Public Property TenancyId() As Integer
        Get
            Return _tenancyId
        End Get
        Set(ByVal value As Integer)
            _tenancyId = value
        End Set
    End Property

    Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property

    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    Public Property TradeName() As String
        Get
            Return _tradeName
        End Get
        Set(ByVal value As String)
            _tradeName = value
        End Set
    End Property

    Public Property TradeId() As Integer
        Get
            Return _tradeId
        End Get
        Set(ByVal value As Integer)
            _tradeId = value
        End Set
    End Property

    Public Property CompTradeId() As Integer
        Get
            Return _compTradeId
        End Get
        Set(ByVal value As Integer)
            _compTradeId = value
        End Set
    End Property

    Public Property ComponentName() As String
        Get
            Return _componentName
        End Get
        Set(ByVal value As String)
            _componentName = value
        End Set
    End Property

    Public Property ComponentId() As Integer
        Get
            Return _componentId
        End Get
        Set(ByVal value As Integer)
            _componentId = value
        End Set
    End Property

    Public Property Duration() As Double
        Get
            Return _duration
        End Get
        Set(ByVal value As Double)
            _duration = value
        End Set
    End Property

    Public Property StartTime() As String
        Get
            Return _startTime
        End Get
        Set(ByVal value As String)
            _startTime = value
        End Set
    End Property

    Public Property EndTime() As String
        Get
            Return _endTime
        End Get
        Set(ByVal value As String)
            _endTime = value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    Public Property OperativeName() As String
        Get
            Return _operativeName
        End Get
        Set(ByVal value As String)
            _operativeName = value
        End Set
    End Property

    Public Property OperativeId() As Integer
        Get
            Return _operativeId
        End Get
        Set(ByVal value As Integer)
            _operativeId = value
        End Set
    End Property

    Public Property PMO() As Integer
        Get
            Return _pmo
        End Get
        Set(ByVal value As Integer)
            _pmo = value
        End Set
    End Property

    Public Property Jsn() As Integer
        Get
            Return _jsn
        End Get
        Set(ByVal value As Integer)
            _jsn = value
        End Set
    End Property

    Public Property StartDateDefault() As String
        Get
            Return _startDateDefaultFormat
        End Get
        Set(ByVal value As String)
            _startDateDefaultFormat = value
        End Set
    End Property

    Public Property Index() As Integer
        Get
            Return _index
        End Get
        Set(ByVal value As Integer)
            _index = value
        End Set
    End Property

    Public Property IsPending() As Boolean
        Get
            Return _isPending
        End Get
        Set(ByVal value As Boolean)
            _isPending = value
        End Set
    End Property

    ''' <summary>
    ''' This 'll have the total duration of all trades
    ''' </summary>
    ''' <remarks></remarks>
    Public Property TotalDuration As Double
        Get
            Return _totalDuration
        End Get
        Set(ByVal value As Double)
            _totalDuration = value
        End Set
    End Property
#End Region
#Region "Functions"
    ''' <summary>
    ''' This funciton 'll reset the appointment related info so rest of the info can be used again
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub resetAppointmentInfo()
        _tradeName = String.Empty
        _tradeId = -1
        _compTradeId = -1
        _duration = 0
        _startTime = String.Empty
        _endTime = String.Empty
        _startDate = DateTime.Now
        _endDate = DateTime.Now
        _operativeName = String.Empty
        _operativeId = -1
        _jsn = "-1"
        _startDateDefaultFormat = String.Empty
        _customerNotes = String.Empty
        _appointmentNotes = String.Empty
        _index = 1
        _isPending = True
        _appointmentId = -1
    End Sub
#End Region
End Class
