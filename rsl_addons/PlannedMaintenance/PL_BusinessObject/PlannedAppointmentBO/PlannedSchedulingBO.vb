﻿Public Class PlannedSchedulingBO

    ''' <summary>
    ''' This contains the record of property info in data table , which user selects for arranging the appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private _appointmentInfoDt As DataTable
    ''' <summary>
    ''' This containts the all component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Private _allTradesDt As DataTable
    ''' <summary>
    ''' This containts the component and trade infromation (with complete status) in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Private _completeTradesDt As DataTable
    ''' <summary>
    ''' This containts the temporary component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Private _tempTradesDt As DataTable
    ''' <summary>
    ''' This containts the confirmed component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Private _confirmedTradesDt As DataTable
    ''' <summary>
    ''' This containts the removed component trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Private _removedTradesDt As DataTable
    ''' <summary>
    ''' This contains the appointment record that is in pending state
    ''' </summary>
    ''' <remarks></remarks>
    Private _confirmAppointmentsDt As DataTable
    ''' <summary>
    ''' This contains the the datatable of all arranged appointments and their information
    ''' </summary>
    ''' <remarks></remarks>
    Private _arrangedTradesAppointmentsDt As DataTable
    ''' <summary>
    ''' This contains the property info in datatable
    ''' </summary>
    ''' <remarks></remarks>
    Private _propertyInfoDt As DataTable
    

#Region "Construtor"
    Public Sub New()
        _appointmentInfoDt = New DataTable
        _allTradesDt = New DataTable
        _tempTradesDt = New DataTable
        _confirmedTradesDt = New DataTable
        _removedTradesDt = New DataTable
        _confirmAppointmentsDt = New DataTable
        _arrangedTradesAppointmentsDt = New DataTable
        _propertyInfoDt = New DataTable
    End Sub
#End Region

#Region "Properties"
    ''' <summary>
    ''' This contains the record of property info in data table , which user selects for arranging the appointment
    ''' </summary>
    ''' <remarks></remarks>
    Public Property AppointmentInfoDt() As DataTable
        Get
            Return _appointmentInfoDt
        End Get
        Set(ByVal value As DataTable)
            _appointmentInfoDt = value
        End Set
    End Property

    ''' <summary>
    ''' This containts the all component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property AllTradesDt() As DataTable
        Get
            Return _allTradesDt
        End Get
        Set(ByVal value As DataTable)
            _allTradesDt = value
        End Set
    End Property


    ''' <summary>
    ''' This containts the component and trade infromation (with complete appointment status) in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property CompleteTradesDt() As DataTable
        Get
            Return _allTradesDt
        End Get
        Set(ByVal value As DataTable)
            _allTradesDt = value
        End Set
    End Property

    ''' <summary>
    ''' This containts the temp component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property TempTradesDt() As DataTable
        Get
            Return _tempTradesDt
        End Get
        Set(ByVal value As DataTable)
            _tempTradesDt = value
        End Set
    End Property

    ''' <summary>
    ''' This containts the confirmed component and trade infromation in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property ConfirmedTradesDt() As DataTable
        Get
            Return _confirmedTradesDt
        End Get
        Set(ByVal value As DataTable)
            _confirmedTradesDt = value
        End Set
    End Property

    ''' <summary>
    ''' This contains the removed component trade information in data table, against the selected property.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property RemovedTradesDt() As DataTable
        Get
            Return _removedTradesDt
        End Get
        Set(ByVal value As DataTable)
            _removedTradesDt = value
        End Set
    End Property

    ''' <summary>
    ''' This contains the appointment record that is in pending state
    ''' </summary>
    ''' <remarks></remarks>
    Public Property ConfirmAppointmentsDt() As DataTable
        Get
            Return _confirmAppointmentsDt
        End Get
        Set(ByVal value As DataTable)
            _confirmAppointmentsDt = value
        End Set
    End Property

    ''' <summary>
    ''' This contains the all the appointments that has been arrange against component
    ''' </summary>
    ''' <remarks></remarks>
    Public Property ArrangedTradesAppointmentsDt() As DataTable
        Get
            Return _arrangedTradesAppointmentsDt
        End Get
        Set(ByVal value As DataTable)
            _arrangedTradesAppointmentsDt = value
        End Set
    End Property

    ''' <summary>
    ''' This contains the property info in datatable
    ''' </summary>
    ''' <remarks></remarks>
    Public Property PropertyInfoDt() As DataTable
        Get
            Return _propertyInfoDt
        End Get
        Set(ByVal value As DataTable)
            _propertyInfoDt = value
        End Set
    End Property

#End Region
End Class
