﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Public Class ConditionEmailBO
#Region "Attributes"
    Private _address As String
    Private _towncity As String
    Private _username As String
    Private _email As String
    Private _requestedBy As String
    Private _component As String
    Private _postCode As String

#End Region

#Region "Constructor"
    Public Sub New()
        _address = String.Empty
        _towncity = String.Empty
        _username = String.Empty
        _email = String.Empty
        _requestedBy = String.Empty
        _component = String.Empty
        _postCode = String.Empty
    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _address
    Public Property Address() As String

        Get
            Return _address
        End Get

        Set(ByVal value As String)
            _address = value
        End Set

    End Property

    ' Get / Set property for _towncity
    Public Property Towncity() As String

        Get
            Return _towncity
        End Get

        Set(ByVal value As String)
            _towncity = value
        End Set

    End Property

    ' Get / Set property for _postCode
    Public Property PostCode() As String

        Get
            Return _postCode
        End Get

        Set(ByVal value As String)
            _postCode = value
        End Set

    End Property

    ' Get / Set property for _username
    Public Property Username() As String

        Get
            Return _username
        End Get

        Set(ByVal value As String)
            _username = value
        End Set

    End Property

    ' Get / Set property for _email
    <RegexValidator("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$", MessageTemplate:="Invalid Email address.")>
    Public Property Email() As String

        Get
            Return _email
        End Get

        Set(ByVal value As String)
            _email = value
        End Set

    End Property

    Public Property RequestedBy() As String

        Get
            Return _requestedBy
        End Get

        Set(ByVal value As String)
            _requestedBy = value
        End Set

    End Property

    Public Property Component() As String

        Get
            Return _component
        End Get

        Set(ByVal value As String)
            _component = value
        End Set

    End Property

#End Region
End Class
