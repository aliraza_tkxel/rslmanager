﻿Public Class AppointmentBO

    'TODO: Use short hand for properties if no special requirement for get /set
    ' e-g Public Property NewProperty As String

    Private _jsn As String
    Public Property Jsn() As String
        Get
            Return _jsn
        End Get
        Set(ByVal value As String)
            _jsn = value
        End Set
    End Property

    Private _pmo As String
    Public Property Pmo() As String
        Get
            Return _pmo
        End Get
        Set(ByVal value As String)
            _pmo = value
        End Set
    End Property


    Private _operativeName As String
    Public Property OperativeName() As String
        Get
            Return _operativeName
        End Get
        Set(ByVal value As String)
            _operativeName = value
        End Set
    End Property

    Private _tradeName As String
    Public Property TradeName() As String
        Get
            Return _tradeName
        End Get
        Set(ByVal value As String)
            _tradeName = value
        End Set
    End Property

    Private _componentName As String
    Public Property ComponentName() As String
        Get
            Return _componentName
        End Get
        Set(ByVal value As String)
            _componentName = value
        End Set
    End Property


    Private _duration As String
    Public Property Duration() As String
        Get
            Return _duration
        End Get
        Set(ByVal value As String)
            _duration = value
        End Set
    End Property

    Private _totalDuration As String
    Public Property TotalDuration() As String
        Get
            Return _totalDuration
        End Get
        Set(ByVal value As String)
            _totalDuration = value
        End Set
    End Property

    Private _status As String
    Public Property AppointmentStatus() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    Private _time As String
    Public Property Time() As String
        Get
            Return _time
        End Get
        Set(ByVal value As String)
            _time = value
        End Set
    End Property
    'TODO: Save Date and Time in memory as DateTime data type & Apply formating when binding to UI
    Private _startDate As String

    Public Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property
    'TODO: Save Date and Time in memory as DateTime data type & Apply formating when binding to UI
    Private _endDate As String
    Public Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property


End Class
