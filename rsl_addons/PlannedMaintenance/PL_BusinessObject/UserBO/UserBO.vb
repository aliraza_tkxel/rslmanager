﻿Namespace PL_BusinessObject
Public Class UserBO
    Private _employeeId As String
    Public Property EmployeeId() As Integer
        Get
            Return _employeeId
        End Get
        Set(ByVal value As Integer)
            _employeeId = value
        End Set
    End Property

    Private _userLoginId As Integer
    Public Property UserLoginId() As Integer
        Get
            Return _userLoginId
        End Get
        Set(ByVal value As Integer)
            _userLoginId = value
        End Set
    End Property

    Private _firstName As String
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property

    Private _lastName As String
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property

End Class
End Namespace

