﻿Public Class RemovedTradeDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public jsn As String = String.Empty
    Public componentName As String = String.Empty
    Public durationString As String = String.Empty
    Public tradeName As String = String.Empty
    Public status As String = String.Empty

    Private Const jsnColName As String = "JSN:"
    Private Const componentColName As String = "Component:"
    Private Const durationColName As String = "Duration:"
    Private Const tradeColName As String = "Trade:"
    Public Const statusColName As String = "Status:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(jsnColName) = jsn
        dr(componentColName) = componentName
        dr(durationColName) = durationString
        dr(tradeColName) = tradeName
        dr(statusColName) = status
        dt.Rows.Add(dr)
    End Sub

    Private Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(jsnColName)
        dt.Columns.Add(componentColName)
        dt.Columns.Add(durationColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(statusColName)
    End Sub

End Class



