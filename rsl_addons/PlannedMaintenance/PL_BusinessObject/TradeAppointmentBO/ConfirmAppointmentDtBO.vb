﻿Public Class ConfirmAppointmentDtBO
    Implements IDtBO

    Public operative As String = String.Empty
    Public startDate As String = String.Empty
    Public endDate As String = String.Empty
    Public lastColumn As String = String.Empty
    Public appointmentId As Integer = 0

    Public dt As DataTable
    Private dr As DataRow
    Private Const operativeColName As String = "Operative:"
    Private Const timeColName As String = "Start date:"
    Private Const dateColName As String = "End date:"
    Private Const lastColName As String = " "
    Public Const appointmentIdColName As String = "AppointmentId:"

    Sub New()
        Me.createDataTable()
    End Sub

    Private Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(operativeColName)
        dt.Columns.Add(timeColName)
        dt.Columns.Add(dateColName)
        dt.Columns.Add(lastColName)
        dt.Columns.Add(appointmentIdColName)
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(operativeColName) = operative
        dr(timeColName) = startDate
        dr(dateColName) = endDate
        dr(lastColName) = lastColumn
        dr(appointmentIdColName) = appointmentId
        dt.Rows.Add(dr)
    End Sub
End Class
