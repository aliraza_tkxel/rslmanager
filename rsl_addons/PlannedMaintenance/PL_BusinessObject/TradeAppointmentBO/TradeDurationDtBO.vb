﻿Public Class TradeDurationDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public tradeId As String = String.Empty
    Public duration As String = String.Empty
    Public durationString As String = String.Empty
    Public tradeName As String = String.Empty
    Public sOrder As String = String.Empty

    Private Const tradeIdColName As String = "TradeId"
    Private Const durationColName As String = "Duration"
    Private Const durationStringColName As String = "DurationString"
    Private Const tradeColName As String = "TradeName"
    Public Const sOrderColName As String = "SortOrder"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(tradeIdColName) = tradeId
        dr(durationStringColName) = durationString
        dr(durationColName) = duration
        dr(tradeColName) = tradeName
        dr(sOrderColName) = sOrder
        dt.Rows.Add(dr)
    End Sub

    Private Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(tradeIdColName)
        dt.Columns.Add(durationStringColName)
        dt.Columns.Add(durationColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(sOrderColName)
    End Sub

End Class


