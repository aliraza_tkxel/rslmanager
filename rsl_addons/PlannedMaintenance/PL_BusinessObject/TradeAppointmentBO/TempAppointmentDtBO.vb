﻿Public Class TempAppointmentDtBO
    Implements IDtBO

    Public operative As String = String.Empty        
    Public startDateString As String = String.Empty
    Public endDateString As String = String.Empty
    Public lastColumn As String = "--"
    Public operativeId As Integer = 0
    Public startTime As String = String.Empty
    Public endTime As String = String.Empty    
    Public tradeId As Integer = 0
    Public lastAdded As Boolean = True

    Public dt As DataTable
    Private dr As DataRow

    Public Const operativeColName As String = "Operative:"
    Public Const startDateStringColName As String = "Start date:"
    Public Const endDateStringColName As String = "End date:"
    Public Const lastColName As String = " "
    Public Const operativeIdColName As String = "OperativeId"
    Public Const startTimeColName As String = "StartTime"
    Public Const endTimeColName As String = "EndTime"    
    Public Const tradeIdColName As String = "TradeId"
    Public Const lastAddedColName As String = "LastAdded:"

    Sub New()
        Me.createDataTable()
    End Sub

    Private Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(operativeColName)
        dt.Columns.Add(startDateStringColName)
        dt.Columns.Add(endDateStringColName)
        dt.Columns.Add(lastColName)
        dt.Columns.Add(operativeIdColName)
        dt.Columns.Add(startTimeColName)
        dt.Columns.Add(endTimeColName)
        dt.Columns.Add(tradeIdColName)
        dt.Columns.Add(lastAddedColName)
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(operativeColName) = operative
        dr(startDateStringColName) = startDateString
        dr(endDateStringColName) = endDateString
        dr(lastColName) = lastColumn
        dr(operativeIdColName) = operativeId
        dr(startTimeColName) = startTime
        dr(endTimeColName) = endTime
        dr(tradeIdColName) = tradeId
        dr(lastAddedColName) = lastAdded
        dt.Rows.Add(dr)
    End Sub

End Class

