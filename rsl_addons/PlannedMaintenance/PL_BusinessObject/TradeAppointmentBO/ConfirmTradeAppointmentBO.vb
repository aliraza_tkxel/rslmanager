﻿Imports System.Globalization

Public Class ConfirmTradeAppointmentBO

    Private _confirmedComponentTradeId As Integer
    Public Property ComponentTradeId() As Integer
        Get
            Return _confirmedComponentTradeId
        End Get
        Set(ByVal value As Integer)
            _confirmedComponentTradeId = value
        End Set
    End Property

    Public confirmTradeDtBo As ConfirmTradeDtBO = New ConfirmTradeDtBO
    Public confirmAppointmentDtBo As ConfirmAppointmentDtBO = New ConfirmAppointmentDtBO
End Class
