﻿Public Class TempTradeDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public jsn As String = String.Empty
    Public componentName As String = String.Empty
    Public durationString As String = String.Empty
    Public tradeName As String = String.Empty
    Public status As String = String.Empty
    Public tradeId As Integer = 0
    Public tradeDurationHrs As Double = 0
    Public tradeDurationDays As Double = 0
    Public componentTradeId As Integer = 0
    Public componentId As Integer = 0

    Private Const jsnColName As String = "JSN:"
    Private Const componentColName As String = "Component:"
    Private Const durationStringColName As String = "Duration:"
    Private Const tradeColName As String = "Trade:"
    Private Const statusColName As String = "Status:"
    Public Const tradeIdColName As String = "TradeId:"
    Public Const tradeDurationHrsColName As String = "TradeDurationHrs:"
    Public Const tradeDurationDaysColName As String = "TradeDurationDays:"
    Public Const componentTradeIdColName As String = "ComponentTradeId:"
    Public Const componentIdColName As String = "ComponentId:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(jsnColName) = jsn
        dr(componentColName) = componentName
        dr(durationStringColName) = durationString
        dr(tradeColName) = tradeName
        dr(statusColName) = status
        dr(tradeIdColName) = tradeId
        dr(tradeDurationHrsColName) = tradeDurationHrs
        dr(tradeDurationDaysColName) = tradeDurationDays
        dr(componentTradeIdColName) = componentTradeId
        dr(componentIdColName) = componentId
        dt.Rows.Add(dr)
    End Sub

    Private Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(jsnColName)
        dt.Columns.Add(componentColName)
        dt.Columns.Add(durationStringColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(statusColName)
        dt.Columns.Add(tradeIdColName)
        dt.Columns.Add(tradeDurationHrsColName)
        dt.Columns.Add(tradeDurationDaysColName)
        dt.Columns.Add(componentTradeIdColName)
        dt.Columns.Add(componentIdColName)
    End Sub

End Class


