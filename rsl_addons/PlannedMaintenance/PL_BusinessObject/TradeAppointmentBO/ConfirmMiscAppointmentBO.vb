﻿Imports System.Globalization

Public Class ConfirmMiscAppointmentBO

    Private _duration As String
    Public Property Duration() As String
        Get
            Return _duration
        End Get
        Set(ByVal value As String)
            _duration = value
        End Set
    End Property

    Private _durationString As String
    Public Property DurationString() As String
        Get
            Return _durationString
        End Get
        Set(ByVal value As String)
            _durationString = value
        End Set
    End Property

    Private _trade As String
    Public Property Trade() As String
        Get
            Return _trade
        End Get
        Set(ByVal value As String)
            _trade = value
        End Set
    End Property

    Private _tradeId As String
    Public Property TradeId() As String
        Get
            Return _tradeId
        End Get
        Set(ByVal value As String)
            _tradeId = value
        End Set
    End Property

    Private _pmo As Integer
    Public Property PMO() As Integer
        Get
            Return _pmo
        End Get
        Set(ByVal value As Integer)
            _pmo = value
        End Set
    End Property

    Public tempAppointmentDtBo As TempAppointmentDtBO = New TempAppointmentDtBO


End Class
