﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class EditedComponentBO

#Region "Attributes"
    Private _componentId As Integer
    Private _componentName As String
    Private _materialCost As Double
    Private _labourCost As Double
    Private _cycle As Integer
    Private _frequency As String
#End Region

#Region "Construtor"
    Public Sub New()

        _componentId = -1
        _componentName = String.Empty
        _materialCost = 0
        _labourCost = 0
        _cycle = -1
        _frequency = String.Empty

    End Sub
#End Region


#Region "Properties"


    ' Get / Set property for _componentId
    Public Property ComponentId() As Integer

        Get
            Return _componentId
        End Get

        Set(ByVal value As Integer)
            _componentId = value
        End Set

    End Property

    ' Get / Set property for _componentName
    Public Property ComponentName() As String

        Get
            Return _componentName
        End Get

        Set(ByVal value As String)
            _componentName = value
        End Set

    End Property

    ' Get / Set property for _materialCost
    Public Property MaterialCost() As Double

        Get
            Return _materialCost
        End Get

        Set(ByVal value As Double)
            _materialCost = value
        End Set

    End Property

    ' Get / Set property for _labourCost
    Public Property LabourCost() As Double

        Get
            Return _labourCost
        End Get

        Set(ByVal value As Double)
            _labourCost = value
        End Set

    End Property

    ' Get / Set property for _cycle
    Public Property Cycle() As Integer

        Get
            Return _cycle
        End Get

        Set(ByVal value As Integer)
            _cycle = value
        End Set

    End Property

    ' Get / Set property for _frequency
    Public Property Frequency() As String

        Get
            Return _frequency
        End Get

        Set(ByVal value As String)
            _frequency = value
        End Set

    End Property

#End Region

End Class
