﻿Imports System

Public Class MiscAppointmentBO

#Region "Attributes"

    Private _propertyId As String
    Private _type As String
    Private _propertyAddress As String
    Private _tradeName As String
    Private _tradeId As String

    Private _duration As Double

    Private _startTime As String
    Private _endTime As String
    Private _startDate As Date
    Private _endDate As Date

    Private _operativeName As String
    Private _operativeId As Integer

    Private _pmo As Integer
    Private _jsn As Integer
    Private _startDateDefaultFormat As DateTime

    Private _userId As Integer
    Private _tenancyId? As Integer
    Private _customerNotes As String
    Private _appointmentNotes As String

    Private _locationName As String
    Private _locationId As Integer

    Private _adaptationName As String
    Private _adaptationId As Integer

    Private _worksRequired As String
    Private _appointmentType As String
    Private _appointmentTypeId As Integer
    Private _index As Integer
#End Region

#Region "Construtor"
    Public Sub New()
        _propertyAddress = String.Empty
        _propertyId = String.Empty
        _type = String.Empty
        _tradeName = String.Empty
        _worksRequired = String.Empty
        _tradeId = -1
        _duration = 0
        _startTime = String.Empty
        _endTime = String.Empty
        '_startDate = String.Empty
        '_endDate = String.Empty
        _operativeName = String.Empty
        _operativeId = -1
        _pmo = -1
        _jsn = -1
        '_startDateDefaultFormat = String.Empty
        _userId = 0        
        _customerNotes = String.Empty
        _appointmentNotes = String.Empty

        _adaptationName = String.Empty
        _adaptationId = -1

        _locationName = String.Empty
        _locationId = -1
        _appointmentTypeId = -1
        _appointmentType = String.Empty
    End Sub
#End Region

#Region "Properties"
    Public Property PropertyAddress() As String
        Get
            Return _propertyAddress
        End Get
        Set(ByVal value As String)
            _propertyAddress = value
        End Set
    End Property
    Public Property AppointmentNotes() As String
        Get
            Return _appointmentNotes
        End Get
        Set(ByVal value As String)
            _appointmentNotes = value
        End Set
    End Property

    Public Property CustomerNotes() As String
        Get
            Return _customerNotes
        End Get
        Set(ByVal value As String)
            _customerNotes = value
        End Set
    End Property

    Public Property TenancyId() As Integer?
        Get
            Return _tenancyId
        End Get
        Set(ByVal value As Integer?)
            _tenancyId = value
        End Set
    End Property

    Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property

    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    Public Property TradeName() As String
        Get
            Return _tradeName
        End Get
        Set(ByVal value As String)
            _tradeName = value
        End Set
    End Property

    Public Property TradeId() As String
        Get
            Return _tradeId
        End Get
        Set(ByVal value As String)
            _tradeId = value
        End Set
    End Property

    Public Property Duration() As Double
        Get
            Return _duration
        End Get
        Set(ByVal value As Double)
            _duration = value
        End Set
    End Property

    Public Property StartTime() As String
        Get
            Return _startTime
        End Get
        Set(ByVal value As String)
            _startTime = value
        End Set
    End Property

    Public Property EndTime() As String
        Get
            Return _endTime
        End Get
        Set(ByVal value As String)
            _endTime = value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    Public Property OperativeName() As String
        Get
            Return _operativeName
        End Get
        Set(ByVal value As String)
            _operativeName = value
        End Set
    End Property

    Public Property OperativeId() As Integer
        Get
            Return _operativeId
        End Get
        Set(ByVal value As Integer)
            _operativeId = value
        End Set
    End Property

    Public Property PMO() As Integer
        Get
            Return _pmo
        End Get
        Set(ByVal value As Integer)
            _pmo = value
        End Set
    End Property

    Public Property JSN() As Integer
        Get
            Return _jsn
        End Get
        Set(ByVal value As Integer)
            _jsn = value
        End Set
    End Property

    Public Property StartDateDefault() As DateTime
        Get
            Return _startDateDefaultFormat
        End Get
        Set(ByVal value As DateTime)
            _startDateDefaultFormat = value
        End Set
    End Property

    Public Property LocationName() As String
        Get
            Return _locationName
        End Get
        Set(ByVal value As String)
            _locationName = value
        End Set
    End Property

    Public Property LocationId() As Integer
        Get
            Return _locationId
        End Get
        Set(ByVal value As Integer)
            _locationId = value
        End Set
    End Property

    Public Property AdaptationName() As String
        Get
            Return _adaptationName
        End Get
        Set(ByVal value As String)
            _adaptationName = value
        End Set
    End Property

    Public Property AdaptationId() As Integer
        Get
            Return _adaptationId
        End Get
        Set(ByVal value As Integer)
            _adaptationId = value
        End Set
    End Property

    Public Property WorksRequired() As String
        Get
            Return _worksRequired
        End Get
        Set(ByVal value As String)
            _worksRequired = value
        End Set
    End Property

    Public Property AppointmentTypeId() As Integer
        Get
            Return _appointmentTypeId
        End Get
        Set(ByVal value As Integer)
            _appointmentTypeId = value
        End Set
    End Property
    Public Property AppointmentType() As String
        Get
            Return _appointmentType
        End Get
        Set(ByVal value As String)
            _appointmentType = value
        End Set
    End Property
    Public Property Index() As Integer
        Get
            Return _index
        End Get
        Set(ByVal value As Integer)
            _index = value
        End Set
    End Property

#End Region

End Class
