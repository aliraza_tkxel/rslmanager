﻿Imports System

Namespace PL_BusinessObject


    Public Class SavedLetterBO

#Region "Attributes"

        Private _journalHistoryId As Integer
        Private _letterBody As String
        Private _letterTitle As String
        Private _letterId As Integer
        Private _teamId As Integer
        Private _fromResourceId As Integer
        Private _signOffLookupCode As Integer


#End Region

#Region "Construtor"

        Public Sub New(ByVal journalHistoryId As Integer, ByVal letterBody As String, ByVal letterTitle As String, ByVal letterId As Integer, ByVal teamId As Integer, ByVal fromResourceId As Integer, ByVal signOffLookupCode As Integer)
            _journalHistoryId = journalHistoryId
            _letterBody = letterBody
            _letterTitle = letterTitle
            _letterId = letterId
            _teamId = teamId
            _fromResourceId = fromResourceId
            _signOffLookupCode = signOffLookupCode
        End Sub

#End Region

#Region "Properties"

        Public Property JournalHistoryId() As Integer
            Get
                Return _journalHistoryId
            End Get
            Set(ByVal value As Integer)
                _journalHistoryId = value
            End Set
        End Property

        Public Property LetterBody() As String
            Get
                Return _letterBody
            End Get
            Set(ByVal value As String)
                _letterBody = value
            End Set
        End Property

        Public Property LetterTitle() As String
            Get
                Return _letterTitle
            End Get
            Set(ByVal value As String)
                _letterTitle = value
            End Set
        End Property

        Public Property LetterId() As Integer
            Get
                Return _letterId
            End Get
            Set(ByVal value As Integer)
                _letterId = value
            End Set
        End Property

        Public Property TeamId() As Integer
            Get
                Return _teamId
            End Get
            Set(ByVal value As Integer)
                _teamId = value
            End Set
        End Property

        Public Property FromResourceId() As Integer
            Get
                Return _fromResourceId
            End Get
            Set(ByVal value As Integer)
                _fromResourceId = value
            End Set
        End Property

        Public Property SignOffLookupCodeId() As Integer
            Get
                Return _signOffLookupCode
            End Get
            Set(ByVal value As Integer)
                _signOffLookupCode = value
            End Set
        End Property

#End Region

#Region "Functions"

#End Region

    End Class
End Namespace