﻿Imports System

Namespace PL_BusinessObject
    Public Class LetterBO

#Region "Attributes"

        Private _letterId As Integer
        Private _statusId As Integer
        Private _actionId As Integer
        Private _title As String
        Private _code As String
        Private _body As String
        Private _createdBy As Integer
        Private _modifiedBy As Integer

#End Region

#Region "Construtor"

        Public Sub New()            
        End Sub

        Public Sub New(ByVal statusId As Integer, ByVal actionId As Integer, ByVal title As String, ByVal code As String, ByVal body As String, ByVal createdBy As Integer, ByVal modifiedBy As Integer)
            _statusId = statusId
            _actionId = actionId
            _title = title
            _code = code
            _body = body
            _createdBy = createdBy
            _modifiedBy = modifiedBy
        End Sub

        Public Sub New(ByVal letterId As Integer, ByVal statusId As Integer, ByVal actionId As Integer, ByVal title As String, ByVal code As String, ByVal body As String, ByVal createdBy As Integer, ByVal modifiedBy As Integer)
            _letterId = letterId
            _statusId = statusId
            _actionId = actionId
            _title = title
            _code = code
            _body = body
            _createdBy = createdBy
            _modifiedBy = modifiedBy
        End Sub

#End Region

#Region "Properties"

        Public Property LetterId() As Integer
            Get
                Return _letterId
            End Get
            Set(ByVal value As Integer)
                _letterId = value
            End Set
        End Property

        Public Property StatusId() As Integer
            Get
                Return _statusId
            End Get
            Set(ByVal value As Integer)
                _statusId = value
            End Set
        End Property

        Public Property ActionId() As Integer
            Get
                Return _actionId
            End Get
            Set(ByVal value As Integer)
                _actionId = value
            End Set
        End Property

        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal value As String)
                _code = value
            End Set
        End Property

        Public Property Body() As String
            Get
                Return _body
            End Get
            Set(ByVal value As String)
                _body = value
            End Set
        End Property

        Public Property CreatedBy() As Integer
            Get
                Return _createdBy
            End Get
            Set(ByVal value As Integer)
                _createdBy = value
            End Set
        End Property

        Public Property ModifiedBy() As Integer
            Get
                Return _modifiedBy
            End Get
            Set(ByVal value As Integer)
                _modifiedBy = value
            End Set
        End Property

#End Region


    End Class
End Namespace