﻿

Namespace PL_BusinessObject
    Public Class SavedLetterPDFBO

#Region "Attributes"

        Private _tenancyRefValue As String
        Private _letterDateValue As Date
        Private _tenantNameValue As String
        Private _houseNumberValue As String
        Private _addressLine1Value As String
        Private _addressLine2Value As String
        Private _townCityValue As String
        Private _countyValue As String
        Private _postCodeValue As String
        Private _letterTitleValue As String
        Private _letterBodyValue As String
        Private _signOffValue As String
        Private _fromResourceValue As String
        Private _teamValue As String
        Private _fromEmailValue As String
        Private _fromDirectDialValue As String

#End Region

#Region "Constructor"

        Public Sub New(ByVal tenancyRef As String, ByVal letterDate As Date, ByVal tenantName As String, ByVal houseNumber As String, ByVal addressLine1 As String, ByVal addressLine2 As String, ByVal townCity As String, ByVal county As String, ByVal postCode As String, ByVal letterTitle As String, ByVal letterBody As String, ByVal signOff As String, ByVal fromResource As String, ByVal team As String, ByVal email As String, ByVal directDial As String)

            _tenancyRefValue = tenancyRef
            _letterDateValue = letterDate
            _tenantNameValue = tenantName
            _houseNumberValue = houseNumber
            _addressLine1Value = addressLine1
            _addressLine2Value = addressLine2
            _townCityValue = townCity
            _countyValue = county
            _postCodeValue = postCode
            _letterTitleValue = letterTitle
            _letterBodyValue = letterBody
            _signOffValue = signOff
            _fromResourceValue = fromResource
            _teamValue = team
            _fromEmailValue = email
            _fromDirectDialValue = directDial

        End Sub

        Public Sub New()
        End Sub

#End Region

#Region "Properties"

        Public Property TenancyRef() As String
            Get
                Return _tenancyRefValue
            End Get
            Set(ByVal value As String)
                _tenancyRefValue = value
            End Set
        End Property

        Public Property LetterDate() As Date
            Get
                Return _letterDateValue
            End Get
            Set(ByVal value As Date)
                _letterDateValue = value
            End Set
        End Property

        Public Property TenantName() As String
            Get
                Return _tenantNameValue
            End Get
            Set(ByVal value As String)
                _tenantNameValue = value
            End Set
        End Property

        Public Property HouseNumber() As String
            Get
                Return _houseNumberValue
            End Get
            Set(ByVal value As String)
                _houseNumberValue = value
            End Set
        End Property

        Public Property AddressLine1() As String
            Get
                Return _addressLine1Value
            End Get
            Set(ByVal value As String)
                _addressLine1Value = value
            End Set
        End Property

        Public Property AddressLine2() As String
            Get
                Return _addressLine2Value
            End Get
            Set(ByVal value As String)
                _addressLine2Value = value
            End Set
        End Property

        Public Property TownCity() As String
            Get
                Return _townCityValue
            End Get
            Set(ByVal value As String)
                _townCityValue = value
            End Set
        End Property

        Public Property CountyValue() As String
            Get
                Return _countyValue
            End Get
            Set(ByVal value As String)
                _countyValue = value
            End Set
        End Property

        Public Property PostCode() As String
            Get
                Return _postCodeValue
            End Get
            Set(ByVal value As String)
                _postCodeValue = value
            End Set
        End Property

        Public Property LetterTitle() As String
            Get
                Return _letterTitleValue
            End Get
            Set(ByVal value As String)
                _letterTitleValue = value
            End Set
        End Property

        Public Property LetterBody() As String
            Get
                Return _letterBodyValue
            End Get
            Set(ByVal value As String)
                _letterBodyValue = value
            End Set
        End Property

        Public Property SignOff() As String
            Get
                Return _signOffValue
            End Get
            Set(ByVal value As String)
                _signOffValue = value
            End Set
        End Property


        Public Property FromResource() As String
            Get
                Return _fromResourceValue
            End Get
            Set(ByVal value As String)
                _fromResourceValue = value
            End Set
        End Property


        Public Property Team() As String
            Get
                Return _teamValue
            End Get
            Set(ByVal value As String)
                _teamValue = value
            End Set
        End Property

        Public Property DirectEmail() As String
            Get
                Return _fromEmailValue
            End Get
            Set(ByVal value As String)
                _fromEmailValue = value
            End Set
        End Property

        Public Property DirectDial() As String
            Get
                Return _fromDirectDialValue
            End Get
            Set(ByVal value As String)
                _fromDirectDialValue = value
            End Set
        End Property

#End Region

    End Class
End Namespace
