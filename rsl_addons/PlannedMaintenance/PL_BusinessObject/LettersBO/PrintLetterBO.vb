﻿Namespace PL_BusinessObject


    Public Class PrintLetterBO

        Private _letterTitle As String
        Public Property LetterTitle() As String
            Get
                Return _letterTitle
            End Get
            Set(ByVal value As String)
                _letterTitle = value
            End Set
        End Property

        Private _letterBody As String
        Public Property LetterBody() As String
            Get
                Return _letterBody
            End Get
            Set(ByVal value As String)
                _letterBody = value
            End Set
        End Property

        Private _signOff As String
        Public Property SignOff() As String
            Get
                Return _signOff
            End Get
            Set(ByVal value As String)
                _signOff = value
            End Set
        End Property

        Private _resourceTeamName As String
        Public Property ResourceTeamName() As String
            Get
                Return _resourceTeamName
            End Get
            Set(ByVal value As String)
                _resourceTeamName = value
            End Set
        End Property

        Private _from As String
        Public Property From() As String
            Get
                Return _from
            End Get
            Set(ByVal value As String)
                _from = value
            End Set
        End Property

        Private _employeeId As Integer
        Public Property EmployeeId() As Integer
            Get
                Return _employeeId
            End Get
            Set(ByVal value As Integer)
                _employeeId = value
            End Set
        End Property

        Private _fromDirectTel As String
        Public Property FromDirectDial() As String
            Get
                Return _fromDirectTel
            End Get
            Set(ByVal value As String)
                _fromDirectTel = value
            End Set
        End Property

        Private _fromEmail As String
        Public Property FromEmail() As String
            Get
                Return _fromEmail
            End Get
            Set(ByVal value As String)
                _fromEmail = value
            End Set
        End Property

        Private _fromJobTitle As String
        Public Property FromJobTitle() As String
            Get
                Return _fromJobTitle
            End Get
            Set(ByVal value As String)
                _fromJobTitle = value
            End Set
        End Property

    End Class
End Namespace