﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class ConditionSmsBO

#Region "Attributes"
    Private _address As String
    Private _towncity As String
    Private _username As String
    Private _mobile As String
#End Region

#Region "Constructor"
    Public Sub New()
        _address = String.Empty
        _towncity = String.Empty
        _username = String.Empty
        _mobile = String.Empty
    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _address
    Public Property Address() As String

        Get
            Return _address
        End Get

        Set(ByVal value As String)
            _address = value
        End Set

    End Property

    ' Get / Set property for _towncity
    Public Property Towncity() As String

        Get
            Return _towncity
        End Get

        Set(ByVal value As String)
            _towncity = value
        End Set

    End Property

    ' Get / Set property for _username
    Public Property Username() As String

        Get
            Return _username
        End Get

        Set(ByVal value As String)
            _username = value
        End Set

    End Property

    ' Get / Set property for _mobile
    <RegexValidator("^[0-9]+$", MessageTemplate:="Invalid mobile number.<br />")>
    Public Property Mobile() As String

        Get
            Return _mobile
        End Get

        Set(ByVal value As String)
            _mobile = value
        End Set

    End Property

#End Region

End Class

