﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation


Namespace PL_BusinessObject
    Public Class WorkRequiredBo

#Region "Properties"

#Region "Work Required"

        <StringLengthValidator(0, RangeBoundaryType.Exclusive, 1000, RangeBoundaryType.Inclusive, MessageTemplate:="""Work Required"" must not exceed 1000 characters.")> _
        <StringLengthValidator(0, RangeBoundaryType.Exclusive, 1000, RangeBoundaryType.Ignore, MessageTemplate:="You must enter detail of ""Work Required"".")> _
        Public Property WorkRequired As String

#End Region

#Region "Net Cost"
        '<TypeConversionValidator(GetType(Decimal), MessageTemplate:="Net cost must be a number.")> _
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Net Cost must be a positive number.")> _
        Public Property NetCost As Decimal
#End Region

#Region "Vat Drop Down Value"

        <NotNullValidator(MessageTemplate:="Please Select Vat.")>
        <TypeConversionValidator(GetType(Integer), MessageTemplate:="Please Select Vat.")>
        <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="Please Select Vat.")>
        Public Property VatIdDDLValue As Integer

#End Region

#Region "Vat Amount"

        <TypeConversionValidator(GetType(Decimal), MessageTemplate:="VAT must be a number.")>
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="VAT must be a positive number.")>
        Public Property Vat As Decimal

#End Region

#Region "Total"

        <TypeConversionValidator(GetType(Decimal), MessageTemplate:="Total must be a number.")>
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Total must be a positive number.")>
        Public Property Total As Decimal

#End Region

#Region "Expenditure Id"

        <TypeConversionValidator(GetType(Integer), MessageTemplate:="You must select a valid expenditure from list.")>
        <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="You must select a valid expenditure from list.")>
        Public Property ExpenditureId() As Integer

#End Region

#Region "Is Pending"

        Public Property IsPending As Boolean

#End Region

#Region "Vat Rate"

        Public Property VatRate As Decimal

#End Region

#End Region

    End Class
End Namespace
