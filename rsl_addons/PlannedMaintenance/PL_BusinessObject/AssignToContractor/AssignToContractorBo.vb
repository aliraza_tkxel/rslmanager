﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Namespace PL_BusinessObject
    Public Class AssignToContractorBo

#Region "Properties"

        Public Property PMO As Integer

        Public Property ContractorId As Integer

        Public Property UserId As Integer

        Public Property Estimate As Decimal

        '<StringLengthValidator(0, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive, MessageTemplate:="""Estimate Ref"" must not exceed 200 characters.")>
        Public Property EstimateRef As String

        Public Property AssignWorkType As Integer

        Public Property EmpolyeeId As Integer

        Public Property POStatus As Integer

        Public Property PropertyId As String

        Public Property WorksRequired As DataTable

        'Add Purchase OrderId which is generated when assigned to Contractor- 
        'Salman Nazir (06/03/2015)
        Public Property OrderId As Integer

        Public Property Type As String

#End Region

    End Class
End Namespace
