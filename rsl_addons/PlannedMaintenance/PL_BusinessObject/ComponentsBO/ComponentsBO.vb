﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Namespace PL_BusinessObject
    Public Class ComponentsBO

#Region "Private"
        Private _componentId As Integer
        Private _componentName As String
        Private _isComponent As Boolean

        Private _cycle As Integer

        Private _frequency As String
        Private _AnnualBudgetCost As Decimal
        Private _meterialCost As Decimal
        Private _labourCost As Decimal
        Private _editedBy As Integer
        Private _editedOn As DateTime
        Private _empName As String
#End Region

#Region "Public Properties"
        Public Property ComponentID() As Integer
            Get
                Return _componentId
            End Get
            Set(ByVal value As Integer)
                _componentId = value
            End Set
        End Property

        Public Property ComponentName() As String
            Get
                Return _componentName
            End Get
            Set(ByVal value As String)
                _componentName = value
            End Set
        End Property

        Public Property IsAccount() As Boolean
            Get
                Return _isComponent
            End Get
            Set(ByVal value As Boolean)
                _isComponent = value
            End Set
        End Property

        <RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive, MessageTemplate:="Please select cycle.<br/>")> _
        Public Property Cycle() As Integer
            Get
                Return _cycle
            End Get
            Set(ByVal value As Integer)
                _cycle = value
            End Set
        End Property

        <StringLengthValidator(1, 5)> _
        Public Property Frequency() As String
            Get
                Return _frequency
            End Get
            Set(ByVal value As String)
                _frequency = value
            End Set
        End Property
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "999999999999999.00", RangeBoundaryType.Inclusive, MessageTemplate:="Enter valid Annual Budget Cost.<br/>")> _
        Public Property AnnualBudgetCost() As Decimal
            Get
                Return _AnnualBudgetCost
            End Get
            Set(ByVal value As Decimal)
                _AnnualBudgetCost = value
            End Set
        End Property
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Value must be greater than 0.<br/>")> _
        Public Property MeterialCost() As Decimal
            Get
                Return _meterialCost
            End Get
            Set(ByVal value As Decimal)
                _meterialCost = value
            End Set
        End Property
        <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Value must be greater than 0.<br/>")> _
        Public Property LabourCost() As Decimal
            Get
                Return _labourCost
            End Get
            Set(ByVal value As Decimal)
                _labourCost = value
            End Set
        End Property

        Public Property EditedBy() As Integer
            Get
                Return _editedBy
            End Get
            Set(ByVal value As Integer)
                _editedBy = value
            End Set
        End Property
        Public Property EditedOn() As DateTime
            Get
                Return _editedOn
            End Get
            Set(ByVal value As DateTime)
                _editedOn = value
            End Set
        End Property

        Public Property EmpName() As String
            Get
                Return _empName
            End Get
            Set(ByVal value As String)
                _empName = value
            End Set
        End Property
#End Region
    End Class
End Namespace
