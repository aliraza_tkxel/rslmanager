﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators

Namespace PL_BusinessObject
    Public Class StatusBO

#Region "Attributes"

        Private _statusId As Integer
        Private _title As String

#End Region

#Region "Construtor"

        Public Sub New()            
        End Sub

        Public Sub New(ByVal id As Integer, ByVal name As String)
            _statusId = id
            _title = name
        End Sub

#End Region

#Region "Properties"
        Public Property StatusId() As Integer
            Get
                Return _statusId
            End Get
            Set(ByVal value As Integer)
                _statusId = value
            End Set
        End Property

        <NotNullValidator(MessageTemplate:="Please enter title. <br />")>
        <StringLengthValidator(1, 25)>
        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property
#End Region

#Region "Functions"

#End Region

    End Class
End Namespace
