﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators

Namespace PL_BusinessObject
    Public Class ActionBO

#Region "Attributes"

        Private _actionId As Integer
        <NotNullValidator(MessageTemplate:="Please enter title. <br />")>
        <StringLengthValidator(1, 25)>
        Private _title As String

#End Region

#Region "Construtor"

        Public Sub New()            
        End Sub

        Public Sub New(ByVal id As Integer, ByVal name As String)
            _actionId = id
            _title = name
        End Sub

#End Region

#Region "Properties"


        Public Property ActionId() As Integer
            Get
                Return _actionId
            End Get
            Set(ByVal value As Integer)
                _actionId = value
            End Set
        End Property

        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

#End Region

#Region "Functions"

#End Region

    End Class
End Namespace