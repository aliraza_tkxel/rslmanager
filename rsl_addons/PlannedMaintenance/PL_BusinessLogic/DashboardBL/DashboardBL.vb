﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports PL_Utilities
Imports PL_DataAccess


Namespace PL_BusinessLogic

    Public Class DashboardBL

#Region "Replacement Due (Current Year)"
        ''' <summary>
        ''' Replacement Due (Current Year)
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getReplacementsCount(ByVal componentId As Integer, ByVal currentYear As Integer, ByVal yearOperator As Integer) As String
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Return objDashboardDAL.getReplacementDueCount(componentId, currentYear, yearOperator)
        End Function
#End Region

#Region "Replacement Due (Not scheduled)"
        ''' <summary>
        ''' Replacement Due (Not scheduled)
        ''' </summary>    
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getReplacementDueNotScheduleCount(ByVal componentId As Integer) As Integer
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Return objDashboardDAL.getReplacementDueNotScheduleCount(componentId)
        End Function
#End Region

#Region "Replacement Due (Scheduled)"
        ''' <summary>
        ''' Replacement Due (Scheduled)
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getReplacementDueScheduleCount(ByVal componentId As Integer) As Integer
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Return objDashboardDAL.getReplacementDueScheduleCount(componentId)
        End Function
#End Region

#Region "NoEntry Count"
        ''' <summary>
        ''' NoEntry Count
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getNoEntryCount(ByVal componentId As Integer) As String
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Dim resultDS As DataSet = New DataSet()
            objDashboardDAL.getNoEntryCount(resultDS, componentId)
            Return resultDS.Tables(0).Rows(0).Item(0)

        End Function
#End Region

#Region "Recall Inprogress count"
        ''' <summary>
        ''' Recall Inprogress count
        ''' </summary>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getInprogressCount(ByVal componentId As Integer)
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Dim resultDS As DataSet = New DataSet()
            objDashboardDAL.getInprogressCount(resultDS, componentId)
            Return resultDS.Tables(0).Rows(0).Item(0)
        End Function
#End Region

#Region "Get Inprogress Appointments"
        ''' <summary>
        ''' Returns inprogress Appointments.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getInprogressAppointments(ByRef resultDataSet As DataSet, ByVal componentId As Integer, ByVal operativeId As Integer)
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            objDashboardDAL.getInprogressAppointments(resultDataSet, componentId, operativeId)
        End Sub
#End Region

#Region "Get RSLModules"
        ''' <summary>
        ''' Returns RSL Modules list
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByVal userId As String)
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            objDashboardDAL.getRSLModules(resultDataSet, Convert.ToInt32(userId))
        End Sub
#End Region

#Region "Get All Components"
        ''' <summary>
        ''' Get All Components
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllComponents(ByRef resultDataSet As DataSet)
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            objDashboardDAL.getAllComponents(resultDataSet)
        End Sub
#End Region

#Region "Get Inprogress Appointments Operatives"
        ''' <summary>
        ''' Get Inprogress Appointments Operatives
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Sub getInprogressAppointmentOperatives(ByRef resultDataSet As DataSet, ByVal componentId As Integer)
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            objDashboardDAL.getInprogressAppointmentOperatives(resultDataSet, componentId)
        End Sub
#End Region

#Region "Adaptation Count"
        ''' <summary>
        ''' Adaptation Not Scheduled
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getAdaptationCount(ByVal componentId As Integer, ByVal adaptationStatus As String, ByVal appointmentType As String) As String
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Dim resultDS As DataSet = New DataSet()
            objDashboardDAL.getAdaptationCount(resultDS, componentId, adaptationStatus, appointmentType)
            Return resultDS.Tables(0).Rows(0).Item(0)

        End Function
#End Region

#Region " Get Condition Works Approval Required Count "
        ''' <summary>
        ''' Get Condition Works Approval Required Count 
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getConditionWorksApprovalRequiredCount(ByVal componentId As Integer) As String
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Dim resultDS As DataSet = New DataSet()
            objDashboardDAL.getConditionWorksApprovalRequiredCount(resultDS, componentId)
            Return resultDS.Tables(1).Rows(0).Item(0)

        End Function
#End Region

#Region " Get Condition Works Approved Count "
        ''' <summary>
        ''' Get Condition Works Approved Count 
        ''' </summary>        
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getConditionWorksApprovedCount(ByVal componentId As Integer) As String
            Dim objDashboardDAL As DashboardDAL = New DashboardDAL()
            Dim resultDS As DataSet = New DataSet()
            objDashboardDAL.getConditionWorksApprovedCount(resultDS, componentId)
            Return resultDS.Tables(0).Rows(0).Item(0)

        End Function
#End Region

    End Class

End Namespace