﻿Imports System
Imports PL_BusinessObject
Imports PL_DataAccess
Imports PL_Utilities

Namespace PL_BusinessLogic
    Public Class LettersBL

#Region "Attributes"
        Dim objLettersDAL As LettersDAL = New LettersDAL()


#End Region

#Region "Functions"

#Region "Get Letters"
        Public Sub getAllLetters(ByRef resultDataSet As DataSet)

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getAllLetters(resultDataSet)

        End Sub

        Public Sub getLettersForSearch(ByRef letterBO As LetterBO, ByRef resultDataSet As DataSet)

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getLettersForSearch(letterBO, resultDataSet)

        End Sub

        Public Sub getLetterById(ByVal letterId As Integer, ByRef resultDataSet As DataSet)

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getLetterById(letterId, resultDataSet)

        End Sub

        Public Function getSavedLetterById(ByVal letterId As Integer, ByRef resultSet As DataSet) As SavedLetterPDFBO

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getSavedLetterById(letterId, resultSet)

            Dim tenancyRefValue As String = resultSet.Tables(0).Rows(0).Item("TENANCY")
            Dim letterDateValue As Date = New Date()
            Dim tenantNameValue As String = resultSet.Tables(0).Rows(0).Item("TenantName")
            Dim houseNumberValue As String = resultSet.Tables(0).Rows(0).Item("HOUSENUMBER")
            Dim addressLine1Value As String = resultSet.Tables(0).Rows(0).Item("ADDRESS1")
            Dim addressLine2Value As String = resultSet.Tables(0).Rows(0).Item("ADDRESS2")
            Dim townCityValue As String = resultSet.Tables(0).Rows(0).Item("TOWNCITY")
            Dim countyValue As String = resultSet.Tables(0).Rows(0).Item("COUNTY")
            Dim postCodeValue As String = resultSet.Tables(0).Rows(0).Item("POSTCODE")
            Dim letterTitleValue As String = resultSet.Tables(0).Rows(0).Item("LETTERTITLE")
            Dim letterBodyValue As String = resultSet.Tables(0).Rows(0).Item("LETTERCONTENT")
            Dim signOffValue As String = resultSet.Tables(0).Rows(0).Item("CodeName")
            Dim fromResourceValue As String = resultSet.Tables(0).Rows(0).Item("EmployeeName")
            Dim teamValue As String = resultSet.Tables(0).Rows(0).Item("TEAMNAME")
            Dim fromEmailValue As String = resultSet.Tables(0).Rows(0).Item("WORKEMAIL")
            Dim fromDirectDialValue As String = resultSet.Tables(0).Rows(0).Item("WORKDD")
            Dim rentBalance As Double = resultSet.Tables(0).Rows(0).Item("RentBalance")
            Dim rentCharge As Double = resultSet.Tables(0).Rows(0).Item("RentCharge")
            Dim todayDate As Date = resultSet.Tables(0).Rows(0).Item("TodayDate")

            letterBodyValue = GeneralHelper.repalceRcRBTd(letterBodyValue, rentBalance, rentCharge, todayDate)

            Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO(tenancyRefValue, Date.Now, tenantNameValue, houseNumberValue, addressLine1Value, addressLine2Value, townCityValue, countyValue, postCodeValue, letterTitleValue, letterBodyValue, signOffValue, fromResourceValue, teamValue, fromEmailValue, fromDirectDialValue)

            Return savedLetterPDFBO


        End Function

#Region "get Customer And Property Rb Rc"
        Public Sub getCustomerAndPropertyRbRc(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef rentBalance As Double, ByRef rentCharge As Double)
            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getCustomerAndPropertyRbRc(resultDataSet, propertyId, rentBalance, rentCharge)
        End Sub
#End Region
#End Region


#Region "Create new Letter Template"

#Region "Get Status"

        Public Sub getStatus(ByRef resultDataSet As DataSet)

            Dim stautsActionDal As StatusActionDAL = New StatusActionDAL()
            stautsActionDal.getAllStatuses(resultDataSet)

        End Sub

#End Region

#Region "Add Standard Letter Template"

        Public Sub addStandardLetterTemplate(ByRef letterBO As LetterBO, ByRef resultSet As DataSet)


            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.addStandardLetter(letterBO, resultSet)


        End Sub

#End Region

#Region "update Standard Letter Template"

        Public Sub updateStandardLetterTemplate(ByRef letterBO As LetterBO, ByRef resultSet As DataSet)


            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.updateStandardLetter(letterBO, resultSet)


        End Sub

#End Region

#Region "Delete Standard Letter Template"

        Public Sub deleteStandardLetter(ByVal letterId As Integer, ByRef resultDataSet As DataSet)

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.deleteStandardLetter(letterId, resultDataSet)

        End Sub

#End Region


#Region "Add Saved Letter"
        Public Sub addSavedLetter(ByRef savedLetterBO As SavedLetterBO, ByRef savedLetterOptionsBO As SavedLetterOptionsBO, ByRef resultSet As DataSet)

            ' first get the letter. 
            Dim objLettersDAL As LettersDAL = New LettersDAL()
            Dim letterResultSet As DataSet = New DataSet()
            objLettersDAL.getLetterById(savedLetterBO.LetterId, letterResultSet)

            Dim rawBodyText As String = letterResultSet.Tables(0).Rows(0).Item(1)

            savedLetterBO.LetterBody = rawBodyText

            Me.addCustomSavedLetter(savedLetterBO, savedLetterOptionsBO, resultSet)


        End Sub

        Public Sub addCustomSavedLetter(ByRef savedLetterBO As SavedLetterBO, ByRef savedLetterOptionsBO As SavedLetterOptionsBO, ByRef resultSet As DataSet)

            ' modify the letter
            Dim rawBodyText As String = savedLetterBO.LetterBody

            If (Not savedLetterOptionsBO.RentCharge = -1) Then
                rawBodyText = rawBodyText.Replace("[RC]", savedLetterOptionsBO.RentCharge.ToString)
            End If

            If (Not savedLetterOptionsBO.CurrentRentBalance = -1) Then
                rawBodyText = rawBodyText.Replace("[RB]", savedLetterOptionsBO.CurrentRentBalance.ToString)
            End If

            If (IsNothing(savedLetterOptionsBO.TodaysDate)) Then
                Dim dateString As String = savedLetterOptionsBO.TodaysDate.Day + " " + savedLetterOptionsBO.TodaysDate.Date.ToString + " " + savedLetterOptionsBO.TodaysDate.Month.ToString + ", " + savedLetterOptionsBO.TodaysDate.Year.ToString
                rawBodyText = rawBodyText.Replace("[TD]", dateString)
            End If

            savedLetterBO.LetterBody = rawBodyText

            ' save the Saved Letter
            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.addSavedLetter(savedLetterBO, resultSet)



        End Sub

#End Region

#End Region


#Region "Get SignOFF Types, Teams and From"
        Public Sub getSignOffTypes(ByRef signOffList As List(Of DropDownBO))

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getSignOffTypes(signOffList)

        End Sub


        Public Sub getTeams(ByRef teamList As List(Of DropDownBO))

            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getTeams(teamList)

        End Sub

        Public Sub getFromUserByTeamId(ByVal teamID As Integer, ByRef userList As List(Of DropDownBO))


            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getFromUserByTeamID(teamID, userList)

        End Sub
#End Region


#Region "Employee(From) Direct Dial and Email Address By Employee ID (Print Letter)"

        Sub getFromTelEmailPrintLetterByEmployeeID(ByRef printLetterBo As PrintLetterBO)
            Dim objLettersDAL As LettersDAL = New LettersDAL()
            objLettersDAL.getFromTelEmailPrintLetterByEmployeeID(printLetterBo)
        End Sub

#End Region

#End Region



    End Class
End Namespace