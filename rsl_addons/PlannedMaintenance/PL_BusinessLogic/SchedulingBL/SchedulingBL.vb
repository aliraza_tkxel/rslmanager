﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports PL_Utilities
Imports PL_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports PL_BusinessObject
Imports System.Data.SqlClient
Imports System.Globalization


Namespace PL_BusinessLogic

    Public Class SchedulingBL

#Region "Get Appointment to be Arranged List"
        ''' <summary>
        ''' Get Appointment to be Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer,
                                                       ByVal searchText As String, ByVal componentId As Integer, Optional ByRef type As String = "") As Integer
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.getAppointmentToBeArrangedList(resultDataSet, objPageSortBo, schemeId, searchText, componentId, type)

        End Function
#End Region

#Region "Get Appointment Cancelled List"
        ''' <summary>
        ''' Get Appointment Cancelled List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function getAppointmentCancelledList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal searchText As String, ByVal componentId As Integer) As Integer
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.getAppointmentCancelledList(resultDataSet, objPageSortBo, schemeId, searchText, componentId)

        End Function
#End Region

#Region "Cancel Appointment to be Arranged List"
        ''' <summary>
        ''' Get Appointment to be Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function cancelAppointmentToBeArrangedList(ByVal pmo As Integer, ByVal reasonId As Integer, ByVal reason As String, ByVal userId As Integer,
                                                          ByVal prevDue As DateTime, ByVal newDue As DateTime, ByVal itemId As Integer, ByVal propId As String,
                                                          ByVal componentId As Integer, ByVal type As String) As Integer
            If (String.IsNullOrEmpty(type)) Then
                type = ApplicationConstants.propertyType
            End If
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.cancelAppointmentToBeArrangedList(pmo, reasonId, reason, userId, prevDue, newDue, itemId, propId, componentId, type)

        End Function
#End Region

#Region "Get Appointments Arranged List"
        ''' <summary>
        ''' Get Appointments Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function getAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer,
                                                    ByVal searchText As String, ByVal componentId As Integer, ByVal appointmentDate As String,
                                                    Optional ByRef type As String = "")
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.getAppointmentsArrangedList(resultDataSet, objPageSortBo, schemeId, searchText, componentId, appointmentDate, type)

        End Function
#End Region


#Region "Get Associated Appointments List"
        ''' <summary>
        ''' Get Appointments Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Sub getAssociatedAppointmentsList(ByRef resultDataSet As DataSet, ByVal journalId As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getAssociatedAppointmentList(resultDataSet, journalId)

        End Sub
#End Region


#Region "Get Property Search Result"
        ''' <summary>
        ''' Get Property Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <remarks></remarks>
        Public Sub getPropertySearchResult(ByRef resultDataSet As DataSet, ByVal search As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getPropertySearchResult(resultDataSet, search)
        End Sub
#End Region

#Region "Get Location Adaptations"
        ''' <summary>
        ''' Get Location Adaptations
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="parameterID"></param>
        ''' <remarks></remarks>
        Public Sub getLocationAdaptations(ByRef resultDataSet As DataSet, ByVal parameterID As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getLocationAdaptations(resultDataSet, parameterID)
        End Sub
#End Region

#Region "Get Confirm Appointments"
        ''' <summary>
        ''' This function 'll pending appointments against property and journal id
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getConfirmAppointments(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal journalId As Integer, Optional ByVal type As String = ApplicationConstants.propertyType)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getConfirmTradeAppointments(resultDataSet, propertyId, journalId, type)
        End Sub

#End Region

#Region "Get Confirm Appointments"
        ''' <summary>
        ''' This function 'll pending appointments against property and journal id
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getMiscConfirmAppointments(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal journalId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getMiscConfirmTradeAppointments(resultDataSet, propertyId, journalId)
        End Sub

#End Region

#Region "Get Component Trades"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getComponentTrades(ByRef resultDataSet As DataSet, ByVal journalId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getComponentTrades(resultDataSet, journalId)
        End Sub

#End Region

#Region "Get Misc Component Trades"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getMiscComponentTrades(ByRef resultDataSet As DataSet, ByVal journalId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getMiscComponentTrades(resultDataSet, journalId)
        End Sub

#End Region


#Region "Get Misc Component Details"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getMiscComponentDetails(ByRef resultDataSet As DataSet, ByVal journalId As Integer, ByVal tradeId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getMiscComponentDetails(resultDataSet, journalId, tradeId)
        End Sub

#End Region


#Region "Get Property Customer Detail"
        ''' <summary>
        ''' Get Property Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyCustomerDetail(ByRef resultDataSet As DataSet, ByVal propertyId As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getPropertyCustomerDetail(resultDataSet, propertyId)
        End Sub
#End Region

#Region "Get Scheme Detail"
        ''' <summary>
        ''' Get Scheme Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getSchemeDetail(ByRef resultDataSet As DataSet, ByVal schemeId As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getSchemeDetail(resultDataSet, schemeId)
        End Sub
#End Region

#Region "Get Block Detail"
        ''' <summary>
        ''' Get Block Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="blockId"></param>
        ''' <remarks></remarks>
        Public Sub getBlockDetail(ByRef resultDataSet As DataSet, ByVal blockId As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getBlockDetail(resultDataSet, blockId)
        End Sub
#End Region

#Region "Get Address By Type"
        ''' <summary>
        ''' Get Address By Type
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="Id"></param>
        ''' <param name="type"></param>
        ''' <remarks></remarks>
        Public Sub getAddressByType(ByRef resultDataSet As DataSet, ByVal Id As String, ByVal type As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getAddressByType(resultDataSet, Id, type)
        End Sub
#End Region

#Region "Schedule Misc Work Appointment"
        ''' <summary>
        ''' Schedule Misc Work Appointment
        ''' </summary>
        ''' <param name="objMiscAppointmentBO"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Function scheduleMiscWorkAppointment(ByVal objMiscAppointmentBO As MiscAppointmentBO, ByRef pmo As Integer, ByVal fromScheduling As Boolean)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.scheduleMiscWorkAppointment(objMiscAppointmentBO, pmo, fromScheduling)
        End Function
#End Region

#Region "Schedule Planned Work Appointment"
        ''' <summary>
        ''' Schedule Planned Work Appointment
        ''' </summary>
        ''' <param name="objPlannedAppointmentBO"></param>
        ''' <param name="appointmentId"></param>
        ''' <remarks></remarks>
        Public Function schedulePlannedWorkAppointment(ByVal objPlannedAppointmentBO As PlannedAppointmentBO, ByRef appointmentId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.schedulePlannedWorkAppointment(objPlannedAppointmentBO, appointmentId)
        End Function
#End Region

#Region "Update Planned Work Appointment"
        ''' <summary>
        ''' Update Planned Work Appointment
        ''' </summary>
        ''' <param name="objPlannedAppointmentBO"></param>        
        ''' <remarks></remarks>
        Public Function updatePlannedWorkAppointment(ByVal objPlannedAppointmentBO As PlannedAppointmentBO)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            If (String.IsNullOrEmpty(objPlannedAppointmentBO.Type)) Then
                objPlannedAppointmentBO.Type = ApplicationConstants.propertyType
            End If
            Return objSchedulingDal.updatePlannedWorkAppointment(objPlannedAppointmentBO)
        End Function
#End Region

#Region "Get Arranged Appointments Detail"
        ''' <summary>
        ''' Get Arranged Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getArrangedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getArrangedAppointmentsDetail(resultDataSet, pmo)
        End Sub
#End Region

#Region "Get Cancelled Appointments Detail"
        ''' <summary>
        ''' Get Cancelled Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getCancelledAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getCancelledAppointmentsDetail(resultDataSet, pmo)
        End Sub
#End Region

#Region "Get Email Appointments Detail"
        ''' <summary>
        ''' Get Cancelled Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getEmailAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getEmailAppointmentsDetail(resultDataSet, pmo)
        End Sub
#End Region

#Region "Get Condition Arranged Appointments Detail"
        ''' <summary>
        ''' Get Condition Arranged Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getConditionArrangedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)

            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getConditionArrangedAppointmentsDetail(resultDataSet, pmo)

        End Sub

#End Region

#Region "Update appointment notes"
        ''' <summary>
        ''' Update appointment notes
        ''' </summary>
        ''' <param name="customerNotes"></param>
        ''' <param name="jobsheetNotes"></param>
        ''' <param name="appointmentId"></param>
        ''' <remarks></remarks>
        Public Function updatePlannedAppointmentsNotes(ByVal customerNotes As String, ByVal jobsheetNotes As String, ByVal appointmentId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.updatePlannedAppointmentsNotes(customerNotes, jobsheetNotes, appointmentId)
        End Function
#End Region

#Region "Cancel appointment"
        ''' <summary>
        ''' Cancel appointment
        ''' </summary>
        ''' <param name="pmo"></param>
        ''' <param name="reason"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function cancelAppointment(ByVal pmo As Integer, ByVal reason As String, ByVal appontmntstatus As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.cancelAppointment(pmo, reason, appontmntstatus)
        End Function
#End Region

#Region "Get Available Operatives"
        ''' <summary>
        ''' This function 'll get the list of operatives that are available
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tradeIds"></param>
        ''' <param name="propertyId"></param>
        ''' <param name="startDate"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet, ByVal tradeIds As String, ByVal propertyId As String, ByVal startDate As Date)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getAvailableOperatives(resultDataSet, tradeIds, propertyId, startDate)
        End Sub

#End Region

#Region "Delete Appointment"
        ''' <summary>
        ''' This function 'll call the dal to delete the appointment
        ''' </summary>
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function deleteAppointment(ByVal appointmentId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.deleteAppointment(appointmentId)
        End Function

#End Region

#Region "Delete Misc Appointment"
        ''' <summary>
        ''' This function 'll call the dal to delete the misc appointment
        ''' </summary>
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function deleteMiscAppointment(ByVal appointmentId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.deleteMiscAppointment(appointmentId)
        End Function

#End Region

#Region "set Pending Bit For Appointment"
        ''' <summary>
        ''' This function 'll make the is pending = 0 against the appointment 
        ''' </summary>
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function setPendingBitForAppointment(ByVal appointmentId As Integer, ByVal isPending As Boolean)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.setPendingBitForAppointment(appointmentId, isPending)
        End Function

#End Region

#Region "Get Operative Leaves And Appointments"
        ''' <summary>
        ''' This function 'll return the leaves and appointments of operative
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getOperativesLeavesAndAppointments(ByRef resultDataSet As DataSet, ByVal startDate As Date)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getOperativesLeavesAndAppointments(resultDataSet, startDate)
        End Sub
        Public Sub getOperativesLeavesAndAppointments(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal startDate As Date)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getOperativesLeavesAndAppointments(resultDataSet, operativeId, startDate)
        End Sub
#End Region

#Region "Schedule Confirmed Planned Appointments"
        ''' <summary>
        ''' This function 'll schedule the appointments that have been confirmed
        ''' </summary>        
        ''' <param name="appointmentIds"></param>        
        ''' <remarks></remarks>
        Public Function scheduleConfirmedPlannedAppointments(ByVal appointmentIds As String, ByVal propertyId As String,
                                                             ByVal journalId As Integer, ByVal componentId As Integer,
                                                             ByVal createdBy As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.scheduleConfirmedPlannedAppointments(appointmentIds, propertyId, journalId, componentId, createdBy)

        End Function
#End Region

#Region "Region for Removed Trades"

#Region "Remove trade from scheduling"
        ''' <summary>
        ''' This function 'll remove trade from scheduling
        ''' </summary>
        ''' <param name="compTradeId"></param>        
        ''' <remarks></remarks>
        Public Function removeTradeFromScheduling(ByVal pmo As Integer, ByVal compTradeId As Integer, ByVal duration As Double)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.removeTradeFromScheduling(pmo, compTradeId, duration)
        End Function


#End Region

#Region "Remove misc trade from scheduling"
        ''' <summary>
        ''' This function 'll remove trade from scheduling
        ''' </summary>
        ''' <param name="miscTradeId"></param>        
        ''' <remarks></remarks>
        Public Function removeMiscTradeFromScheduling(ByVal pmo As Integer, ByVal miscTradeId As Integer, ByVal duration As Double)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.removeMiscTradeFromScheduling(pmo, miscTradeId, duration)
        End Function


#End Region


#Region "Get Removed trades from scheduling"
        ''' <summary>
        ''' Get Removed trades from scheduling
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getRemovedTradesFromScheduling(ByRef resultDataset As DataSet, ByVal pmo As Integer)

            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getRemovedTradesFromScheduling(resultDataset, pmo)

        End Sub
#End Region

#Region "Get Removed misc trades from scheduling"
        ''' <summary>
        ''' Get Removed trades from scheduling
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getRemovedMiscTradesFromScheduling(ByRef resultDataset As DataSet, ByVal pmo As Integer)

            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getRemovedMiscTradesFromScheduling(resultDataset, pmo)

        End Sub
#End Region

#Region "Get Removed Trades And Appointments"
        ''' <summary>
        ''' This function 'll return the removed trades and appointments
        ''' </summary>
        ''' <remarks></remarks>
        Public Function getRemovedTradesAndAppointments(ByVal removedTradeDt As DataTable) As List(Of RemovedTradeAppointmentBO)
            Dim removedTradeAptmentBO As RemovedTradeAppointmentBO = New RemovedTradeAppointmentBO()
            Dim removedTradeAptBOList As List(Of RemovedTradeAppointmentBO) = New List(Of RemovedTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In removedTradeDt.Rows
                removedTradeAptmentBO.removedTradeDtBO.jsn = "N/A"
                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    removedTradeAptmentBO.removedTradeDtBO.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    removedTradeAptmentBO.removedTradeDtBO.jsn = "N/A"
                End If
                removedTradeAptmentBO.removedTradeDtBO.componentName = compTradrow.Item("Component")
                removedTradeAptmentBO.removedTradeDtBO.durationString = GeneralHelper.appendHourLabel(CType(compTradrow.Item("Duration"), Double))
                removedTradeAptmentBO.removedTradeDtBO.tradeName = compTradrow.Item("Trade")
                removedTradeAptmentBO.removedTradeDtBO.status = ApplicationConstants.StatusRemovedFromScheduling

                removedTradeAptmentBO.removedTradeDtBO.addNewDataRow()

                Dim componentTradeId As Integer = Convert.ToInt32(compTradrow.Item("ComponentTradeId"))
                removedTradeAptmentBO.ComponentTradeId = componentTradeId
                removedTradeAptBOList.Add(removedTradeAptmentBO)
                removedTradeAptmentBO = New RemovedTradeAppointmentBO()
            Next

            Return removedTradeAptBOList
        End Function

#End Region


#Region "Get Misc Removed Trades And Appointments"
        ''' <summary>
        ''' This function 'll return the removed trades and appointments
        ''' </summary>
        ''' <remarks></remarks>
        Public Function getMiscRemovedTradesAndAppointments(ByVal removedTradeDt As DataTable) As List(Of RemovedTradeAppointmentBO)
            Dim removedTradeAptmentBO As RemovedTradeAppointmentBO = New RemovedTradeAppointmentBO()
            Dim removedTradeAptBOList As List(Of RemovedTradeAppointmentBO) = New List(Of RemovedTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In removedTradeDt.Rows
                removedTradeAptmentBO.removedTradeDtBO.jsn = "N/A"
                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    removedTradeAptmentBO.removedTradeDtBO.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    removedTradeAptmentBO.removedTradeDtBO.jsn = "N/A"
                End If
                removedTradeAptmentBO.removedTradeDtBO.componentName = compTradrow.Item("Planned_Appointment_Type")
                removedTradeAptmentBO.removedTradeDtBO.durationString = GeneralHelper.appendHourLabel(CType(compTradrow.Item("Duration"), Double))
                removedTradeAptmentBO.removedTradeDtBO.tradeName = compTradrow.Item("Trade")
                removedTradeAptmentBO.removedTradeDtBO.status = ApplicationConstants.StatusRemovedFromScheduling

                removedTradeAptmentBO.removedTradeDtBO.addNewDataRow()

                Dim componentTradeId As Integer = Convert.ToInt32(compTradrow.Item("ComponentTradeId"))
                removedTradeAptmentBO.ComponentTradeId = componentTradeId
                removedTradeAptBOList.Add(removedTradeAptmentBO)
                removedTradeAptmentBO = New RemovedTradeAppointmentBO()
            Next

            Return removedTradeAptBOList
        End Function

#End Region


#Region "Delete Removed Trade"
        ''' <summary>
        ''' This function 'll call the dal to delete an entry from removed trade
        ''' </summary>
        ''' <param name="compTradeId"></param>        
        ''' <remarks></remarks>
        Public Function deleteRemovedTrade(ByVal compTradeId As Integer, ByVal pmo As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.deleteRemovedTrade(compTradeId, pmo)
        End Function

#End Region

#End Region

#Region "Region For Confirmed Trade and Appointments - Intelligent Scheduling"

#Region "Get Confirmed Trades And Appointments"
        ''' <summary>
        ''' This function 'll return the confirmed trades and appointments
        ''' </summary>
        ''' <remarks></remarks>
        Public Function getConfirmedTradesAndAppointments(ByVal confirmedTradeDt As DataTable, ByVal confirmAppointmentsDt As DataTable) As List(Of ConfirmTradeAppointmentBO)
            Dim confirmTradeAptBo As ConfirmTradeAppointmentBO = New ConfirmTradeAppointmentBO()
            Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In confirmedTradeDt.Rows
                confirmTradeAptBo.confirmTradeDtBo.jsn = "N/A"
                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    confirmTradeAptBo.confirmTradeDtBo.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    confirmTradeAptBo.confirmTradeDtBo.jsn = "N/A"
                End If
                If compTradrow.Table.Columns.Contains("Component") Then
                    confirmTradeAptBo.confirmTradeDtBo.componentName = compTradrow.Item("Component")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    confirmTradeAptBo.confirmTradeDtBo.componentName = "N/A"
                End If
                confirmTradeAptBo.confirmTradeDtBo.durationString = GeneralHelper.appendHourLabel(CType(compTradrow.Item("Duration"), Double))
                confirmTradeAptBo.confirmTradeDtBo.tradeName = compTradrow.Item("Trade")
                If compTradrow.Table.Columns.Contains("Status") Then
                    If compTradrow.Item("Status") = ApplicationConstants.StatusToBeArranged Then
                        confirmTradeAptBo.confirmTradeDtBo.status = ApplicationConstants.StatusPending
                    ElseIf compTradrow.Item("Status") = ApplicationConstants.StatusArranged Then
                        confirmTradeAptBo.confirmTradeDtBo.status = compTradrow.Item("InterimStatus")
                    End If
                Else
                    confirmTradeAptBo.confirmTradeDtBo.status = "N/A"
                End If

                confirmTradeAptBo.confirmTradeDtBo.addNewDataRow()
                Dim componentTradeId As Integer = Convert.ToInt32(compTradrow.Item("ComponentTradeId"))
                confirmTradeAptBo.ComponentTradeId = componentTradeId

                Dim query = (From res In confirmAppointmentsDt.AsEnumerable Where res.Item("ComponentTradeId").Equals(componentTradeId) Or IsDBNull(res.Item("ComponentTradeId")) Select res)

                If query.Count() > 0 Then
                    confirmTradeAptBo.confirmAppointmentDtBo.operative = query.First.Item("OperativeName")
                    confirmTradeAptBo.confirmAppointmentDtBo.startDate = GeneralHelper.convertDateToCustomString(query.First.Item("StartDate"))
                    confirmTradeAptBo.confirmAppointmentDtBo.endDate = GeneralHelper.convertDateToCustomString(query.First.Item("EndDate"))
                    confirmTradeAptBo.confirmAppointmentDtBo.appointmentId = query.First.Item("AppointmentId")
                    confirmTradeAptBo.confirmAppointmentDtBo.addNewDataRow()
                    ''once appointment is added in list then create a new item 
                    ''for new list of trade and appointment against them
                    confirmTradeAptBoList.Add(confirmTradeAptBo)
                    confirmTradeAptBo = New ConfirmTradeAppointmentBO()
                End If
            Next

            Return confirmTradeAptBoList
        End Function

#End Region


#Region "Get Misc Confirmed Trades And Appointments"
        ''' <summary>
        ''' This function 'll return the confirmed trades and appointments
        ''' </summary>
        ''' <remarks></remarks>
        Public Function getMiscConfirmedTradesAndAppointments(ByVal confirmedTradeDt As DataTable, ByVal confirmAppointmentsDt As DataTable) As List(Of ConfirmTradeAppointmentBO)
            Dim confirmTradeAptBo As ConfirmTradeAppointmentBO = New ConfirmTradeAppointmentBO()
            Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In confirmedTradeDt.Rows
                confirmTradeAptBo.confirmTradeDtBo.jsn = "N/A"
                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    confirmTradeAptBo.confirmTradeDtBo.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    confirmTradeAptBo.confirmTradeDtBo.jsn = "N/A"
                End If
                If compTradrow.Table.Columns.Contains("Planned_Appointment_Type") Then
                    confirmTradeAptBo.confirmTradeDtBo.componentName = compTradrow.Item("Planned_Appointment_Type")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    confirmTradeAptBo.confirmTradeDtBo.componentName = "N/A"
                End If
                confirmTradeAptBo.confirmTradeDtBo.durationString = GeneralHelper.appendHourLabel(CType(compTradrow.Item("Duration"), Double))
                confirmTradeAptBo.confirmTradeDtBo.tradeName = compTradrow.Item("Trade")
                If compTradrow.Table.Columns.Contains("Status") Then
                    If compTradrow.Item("Status") = ApplicationConstants.StatusToBeArranged Then
                        confirmTradeAptBo.confirmTradeDtBo.status = ApplicationConstants.StatusPending
                    ElseIf compTradrow.Item("Status") = ApplicationConstants.StatusArranged Then
                        confirmTradeAptBo.confirmTradeDtBo.status = compTradrow.Item("InterimStatus")
                    End If
                Else
                    confirmTradeAptBo.confirmTradeDtBo.status = "N/A"
                End If

                confirmTradeAptBo.confirmTradeDtBo.addNewDataRow()
                Dim componentTradeId As Integer = Convert.ToInt32(compTradrow.Item("ComponentTradeId"))
                confirmTradeAptBo.ComponentTradeId = componentTradeId

                Dim query = (From res In confirmAppointmentsDt.AsEnumerable Where res.Item("ComponentTradeId").Equals(componentTradeId) Select res)

                If query.Count() > 0 Then
                    confirmTradeAptBo.confirmAppointmentDtBo.operative = query.First.Item("OperativeName")
                    confirmTradeAptBo.confirmAppointmentDtBo.startDate = GeneralHelper.convertDateToCustomString(query.First.Item("StartDate"))
                    confirmTradeAptBo.confirmAppointmentDtBo.endDate = GeneralHelper.convertDateToCustomString(query.First.Item("EndDate"))
                    confirmTradeAptBo.confirmAppointmentDtBo.appointmentId = query.First.Item("AppointmentId")
                    confirmTradeAptBo.confirmAppointmentDtBo.addNewDataRow()
                    ''once appointment is added in list then create a new item 
                    ''for new list of trade and appointment against them
                    confirmTradeAptBoList.Add(confirmTradeAptBo)
                    confirmTradeAptBo = New ConfirmTradeAppointmentBO()
                End If
            Next

            Return confirmTradeAptBoList
        End Function

#End Region

#End Region

#Region "Region For Misc Appointments - Intelligent Scheduling"

#Region "get Available Misc Appointments"
        ''' <summary>
        ''' This function 'll return the 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="startDate"></param>
        ''' <param name="tradeDurationDt"></param>        
        ''' <remarks></remarks>
        Public Function getAvailableMiscAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempAppointmentDtBo As TempAppointmentDtBO, ByVal startDate As Date, ByVal tradeDurationDt As DataTable)

            Dim tempMiscAptBo As TempMiscAppointmentBO = New TempMiscAppointmentBO()
            Dim tempMiscAptBoList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
            'Loop through trade           
            For Each miscTradrow As DataRow In tradeDurationDt.Rows

                tempMiscAptBo.Duration = miscTradrow.Item("Duration")
                tempMiscAptBo.DurationString = miscTradrow.Item("DurationString")
                tempMiscAptBo.TradeId = miscTradrow.Item("TradeId")
                tempMiscAptBo.Trade = miscTradrow.Item("TradeName")

                Dim tradeId As Integer = miscTradrow.Item("TradeId")
                Dim filteredOperativesDt As DataTable = operativesDt
                Dim filteredAppointmentsDt As DataTable = appointmentsDt
                'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
                ' This() 'll further help us to minimize the time to create appointment slots
                filterOperativesLeavesAppointmentsOnTrade(filteredOperativesDt, leavesDt, filteredAppointmentsDt, tradeId)
                ''this function 'll create the appointment slot based on leaves, appointments & start date
                Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt, tempMiscAptBo.tempAppointmentDtBo, startDate, miscTradrow.Item("Duration"), GeneralHelper.getAppointmentCreationLimitForSingleRequest())

                ''once appointment is added in list then create a new item 
                ''for new list of trade and appointment against them
                tempMiscAptBoList.Add(tempMiscAptBo)
                tempMiscAptBo = New TempMiscAppointmentBO()
            Next

            Return tempMiscAptBoList

        End Function
#End Region

#Region "get Available Condition Appointments"
        ''' <summary>
        ''' This function 'll return the 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="startDate"></param>
        ''' <param name="durationHours"></param>        
        ''' <remarks></remarks>
        Public Sub getAvailableConditionAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempAppointmentDtBo As TempAppointmentDtBO, ByVal startDate As Date, ByVal durationHours As Double)
            Me.createTempAppointmentSlotsWithCoreAndExtendedHours(operativesDt, leavesDt, appointmentsDt, tempAppointmentDtBo, startDate, durationHours, GeneralHelper.getMiscAppointmentCreationLimitForSingleRequest())
        End Sub


#End Region

#Region "add More Temp Misc Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tempTradeAptBo"></param>
        ''' <param name="startDate"></param>
        ''' <param name="startAgain">This 'll help to identify that user wants to fetch the appointments onward from the selected date</param>
        ''' <remarks></remarks>
        Public Sub addMoreTempMiscAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempTradeAptBo As TempMiscAppointmentBO, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
            ' this id 'll retain the operative id, against which the slots were created
            Dim previousOperativeId As Integer = 0
            'retreive the trade id 
            Dim tradeId As Integer = tempTradeAptBo.TradeId
            Dim filteredOperativesDt As DataTable = operativesDt
            'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
            ' This function 'll furthur help us to minimize the time to create appointment slots
            Dim filteredAppointmentsDt As DataTable = appointmentsDt
            filterOperativesLeavesAppointmentsOnTrade(filteredOperativesDt, leavesDt, filteredAppointmentsDt, tradeId)
            If startAgain = False Then
                'fetch the previous operative id so the operatives name should appear in order                
                Dim appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
                If appResult.Count() > 0 Then
                    previousOperativeId = appResult.LastOrDefault.Item(TempAppointmentDtBO.operativeIdColName)
                    'This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                    'as we have previous operative id , so it 'll continue creating the appointment slots after exsiting slots
                    Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt,
                                                  tempTradeAptBo.tempAppointmentDtBo, startDate,
                                                  tempTradeAptBo.Duration,
                                                  GeneralHelper.getAppointmentCreationLimitForSingleRequest(), previousOperativeId)
                End If
            Else
                'This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                ' This 'll restart appointments slot from the start date
                tempTradeAptBo.tempAppointmentDtBo.dt.Clear()
                Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt,
                                              filteredAppointmentsDt, tempTradeAptBo.tempAppointmentDtBo,
                                              startDate, tempTradeAptBo.Duration,
                                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())
            End If
        End Sub
#End Region
#End Region

#Region "Region For Temporary Trade And Appointments - Intelligent Scheduling"

#Region "add More Temp Trade Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tempTradeAptBo"></param>
        ''' <param name="startDate"></param>
        ''' <param name="startAgain">This 'll help to identify that user wants to fetch the appointments onward from the selected date</param>
        ''' <remarks></remarks>
        Public Sub addMoreTempTradeAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempTradeAptBo As TempTradeAppointmentBO, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
            ' this id 'll retain the operative id, against which the slots were created
            Dim previousOperativeId As Integer = 0
            'retreive the trade id 
            Dim tradeId As Integer = tempTradeAptBo.tempTradeDtBo.tradeId
            Dim filteredOperativesDt As DataTable = operativesDt
            'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
            ' This function 'll furthur help us to minimize the time to create appointment slots
            Dim filteredAppointmentsDt As DataTable = appointmentsDt
            filterOperativesLeavesAppointmentsOnTrade(filteredOperativesDt, leavesDt, filteredAppointmentsDt, tradeId)
            If startAgain = False Then
                'fetch the previous operative id so the operatives name should appear in order                
                Dim appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
                If appResult.Count() > 0 Then
                    previousOperativeId = appResult.LastOrDefault.Item(TempAppointmentDtBO.operativeIdColName)
                    'This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                    'as we have previous operative id , so it 'll continue creating the appointment slots after exsiting slots
                    Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt,
                                                  tempTradeAptBo.tempAppointmentDtBo, startDate,
                                                  tempTradeAptBo.tempTradeDtBo.tradeDurationHrs,
                                                  GeneralHelper.getAppointmentCreationLimitForSingleRequest(), previousOperativeId)
                End If
            Else
                'This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                ' This 'll restart appointments slot from the start date
                tempTradeAptBo.tempAppointmentDtBo.dt.Clear()
                Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt,
                                              filteredAppointmentsDt, tempTradeAptBo.tempAppointmentDtBo,
                                              startDate, tempTradeAptBo.tempTradeDtBo.tradeDurationHrs,
                                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())
            End If
        End Sub
#End Region

#Region "get Temp Trade Appointments"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="compTradeDt"></param>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getTempTradeAppointments(ByVal compTradeDt As DataTable, ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal startDate As Date)
            Dim tempTradeAptBo As TempTradeAppointmentBO = New TempTradeAppointmentBO()
            Dim tempTradeAptBoList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In compTradeDt.Rows
                'save the start date             
                tempTradeAptBo.StartSelectedDate = startDate

                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.jsn = "N/A"
                End If
                'changes here
                If compTradrow.Table.Columns.Contains("Component") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentName = compTradrow.Item("Component")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentName = "N/A"
                End If
                'tempTradeAptBo.tempTradeDtBo.componentName = compTradrow.Item("Component")
                '-------------
                tempTradeAptBo.tempTradeDtBo.durationString = GeneralHelper.appendHourLabel(Convert.ToDouble(compTradrow.Item("Duration")))
                tempTradeAptBo.tempTradeDtBo.tradeName = compTradrow.Item("Trade")
                If compTradrow.Table.Columns.Contains("Status") Then
                    'in case of appointments with status "arranged" or others
                    If compTradrow.Item("Status") = ApplicationConstants.StatusArranged Then
                        'if status is already arranged 
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("InterimStatus")
                    Else
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("Status")
                    End If
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.status = "N/A"
                End If
                If compTradrow.Table.Columns.Contains("PlannedSubStatus") Then
                    If compTradrow.Item("PlannedSubStatus") = ApplicationConstants.StatusNoEntry Then
                        'if Planned Sub Status is no entry 
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("PlannedSubStatus")
                    End If

                    If compTradrow.Item("PlannedSubStatus") = ApplicationConstants.StatusInterimNotStarted Then
                        'if Planned Sub Status is not Started
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("PlannedSubStatus")
                    End If
                End If
                tempTradeAptBo.tempTradeDtBo.tradeId = compTradrow.Item("TradeId")
                tempTradeAptBo.tempTradeDtBo.tradeDurationHrs = CType(compTradrow.Item("Duration"), Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
                tempTradeAptBo.tempTradeDtBo.tradeDurationDays = CType(compTradrow.Item("Duration"), Double) / GeneralHelper.getAppointmentDurationHours()
                ' changes here
                If compTradrow.Table.Columns.Contains("ComponentTradeId") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentTradeId = compTradrow.Item("ComponentTradeId")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentTradeId = "-1"
                End If
                If compTradrow.Table.Columns.Contains("ComponentId") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentId = compTradrow.Item("ComponentId")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentId = -1
                End If
                'tempTradeAptBo.tempTradeDtBo.componentTradeId = compTradrow.Item("ComponentTradeId")
                'tempTradeAptBo.tempTradeDtBo.componentId = compTradrow.Item("ComponentId")
                tempTradeAptBo.tempTradeDtBo.addNewDataRow()

                Dim tradeId As Integer = compTradrow.Item("TradeId")
                'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
                ' This() 'll further help us to minimize the time to create appointment slots
                Dim filteredOperativesDt As DataTable = operativesDt
                Dim filteredAppointmentsDt As DataTable = appointmentsDt
                Dim tempLeavesDt As DataTable = leavesDt
                filterOperativesLeavesAppointmentsOnTrade(filteredOperativesDt, tempLeavesDt, filteredAppointmentsDt, tradeId)
                ''this function 'll create the appointment slot based on leaves, appointments & due date
                'Me.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, _
                '                              tempTradeAptBo.tempAppointmentDtBo, startDate, _
                '                              tempTradeAptBo.tempTradeDtBo.tradeDurationHrs, _
                '                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())

                Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, tempLeavesDt, filteredAppointmentsDt,
                                                              tempTradeAptBo.tempAppointmentDtBo, startDate,
                                                              tempTradeAptBo.tempTradeDtBo.tradeDurationHrs,
                                                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())

                ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
                orderAppointmentsByTimeAndOperative(tempTradeAptBo)

                ''once appointment is added in list then create a new item 
                ''for new list of trade and appointment against them
                tempTradeAptBoList.Add(tempTradeAptBo)
                tempTradeAptBo = New TempTradeAppointmentBO()
            Next

            Return tempTradeAptBoList
        End Function
#End Region

#Region "get Misc Temp Trade Appointments"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="compTradeDt"></param>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getMiscTempTradeAppointments(ByVal compTradeDt As DataTable, ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal startDate As Date)
            Dim tempTradeAptBo As TempTradeAppointmentBO = New TempTradeAppointmentBO()
            Dim tempTradeAptBoList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)

            'Loop through trade           
            For Each compTradrow As DataRow In compTradeDt.Rows
                'save the start date             
                tempTradeAptBo.StartSelectedDate = startDate

                If compTradrow.Table.Columns.Contains("JSN") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.jsn = compTradrow.Item("JSN")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.jsn = "N/A"
                End If
                'changes here
                If compTradrow.Table.Columns.Contains("Planned_Appointment_Type") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentName = compTradrow.Item("Planned_Appointment_Type")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentName = "N/A"
                End If
                'tempTradeAptBo.tempTradeDtBo.componentName = compTradrow.Item("Component")
                '-------------
                tempTradeAptBo.tempTradeDtBo.durationString = GeneralHelper.appendHourLabel(Convert.ToDouble(compTradrow.Item("Duration")))
                tempTradeAptBo.tempTradeDtBo.tradeName = compTradrow.Item("Trade")
                If compTradrow.Table.Columns.Contains("Status") Then
                    'in case of appointments with status "arranged" or others
                    If compTradrow.Item("Status") = ApplicationConstants.StatusArranged Then
                        'if status is already arranged 
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("InterimStatus")
                    Else
                        tempTradeAptBo.tempTradeDtBo.status = compTradrow.Item("Status")
                    End If
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.status = "N/A"
                End If

                tempTradeAptBo.tempTradeDtBo.tradeId = compTradrow.Item("ComponentTradeId")
                tempTradeAptBo.tempTradeDtBo.tradeDurationHrs = CType(compTradrow.Item("Duration"), Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
                tempTradeAptBo.tempTradeDtBo.tradeDurationDays = CType(compTradrow.Item("Duration"), Double) / GeneralHelper.getAppointmentDurationHours()
                ' changes here
                If compTradrow.Table.Columns.Contains("ComponentTradeId") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentTradeId = compTradrow.Item("ComponentTradeId")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentTradeId = "-1"
                End If
                If compTradrow.Table.Columns.Contains("ComponentId") Then
                    'in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.componentId = compTradrow.Item("ComponentId")
                Else
                    'in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.componentId = -1
                End If
                'tempTradeAptBo.tempTradeDtBo.componentTradeId = compTradrow.Item("ComponentTradeId")
                'tempTradeAptBo.tempTradeDtBo.componentId = compTradrow.Item("ComponentId")
                tempTradeAptBo.tempTradeDtBo.addNewDataRow()

                Dim tradeId As Integer = compTradrow.Item("ComponentTradeId")
                'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
                ' This() 'll further help us to minimize the time to create appointment slots
                Dim filteredOperativesDt As DataTable = operativesDt
                Dim filteredAppointmentsDt As DataTable = appointmentsDt
                filterOperativesLeavesAppointmentsOnTrade(filteredOperativesDt, leavesDt, filteredAppointmentsDt, tradeId)
                ''this function 'll create the appointment slot based on leaves, appointments & due date
                'Me.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, _
                '                              tempTradeAptBo.tempAppointmentDtBo, startDate, _
                '                              tempTradeAptBo.tempTradeDtBo.tradeDurationHrs, _
                '                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())

                Me.createTempAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt,
                                                              tempTradeAptBo.tempAppointmentDtBo, startDate,
                                                              tempTradeAptBo.tempTradeDtBo.tradeDurationHrs,
                                                              GeneralHelper.getAppointmentCreationLimitForSingleRequest())

                ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
                orderAppointmentsByTimeAndOperative(tempTradeAptBo)

                ''once appointment is added in list then create a new item 
                ''for new list of trade and appointment against them
                tempTradeAptBoList.Add(tempTradeAptBo)
                tempTradeAptBo = New TempTradeAppointmentBO()
            Next

            Return tempTradeAptBoList
        End Function
#End Region

#Region "create Temp Appointment Slots"
        ''' <summary>
        ''' 'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tempAppointmentDtBo"></param>
        ''' <param name="selectedStartDate"></param>
        ''' <param name="previousOperativeId"></param>
        ''' <remarks></remarks>
        Private Sub createVoidTempAppointmentSlots(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable,
                                              ByRef tempAppointmentDtBo As TempAppointmentDtBO, ByVal selectedStartDate As Date,
                                              ByVal tradeDurationHrs As Double, ByVal requiredResultCount As Integer, Optional ByVal previousOperativeId As Integer = 0)
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim officialDayStartHour As Double = Me.getTodaysStartingHour(selectedStartDate)
            Dim officialDayEndHour As Double = GeneralHelper.getOfficialDayEndHour()
            Dim displayedTimeSlotCount As Integer = 1
            Dim aptStartDate As Date
            Dim aptEndDate As Date
            Dim aptStartTime As New Date
            Dim aptEndTime As New Date
            Dim aptStartHour As Double = 0
            Dim aptEndHour As Double = 0
            Dim appointmentDueDate As Date

            While (displayedTimeSlotCount <= requiredResultCount)
                'First Condition: If no operative found then display error and exit the loop
                If operativesDt.Rows.Count = 0 Then
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    operativeId = Convert.ToString(operativeDr.Item("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr.Item("FullName"))
                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 
                    If (previousOperativeId <> 0) Then

                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For
                        End If

                    End If

                    'set the appointment starting hour e.g 9 (represents 9 o'clock in the morning)  
                    'now we 'll add up the number(hours) in date to get date and time
                    aptStartDate = GeneralHelper.getDateTimeFromDateAndNumber(selectedStartDate, officialDayStartHour)
                    appointmentDueDate = CType(Date.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59", Date)

                    If aptStartDate >= appointmentDueDate Then
                        Exit While
                    End If
                    'skip the weekend if it exists
                    Me.skipWeekend(aptStartDate, officialDayStartHour)
                    'initially appointment end date time 'll be equal to appointment start date time. After that we 'll use trade duration to calculate the appointment end date time
                    aptEndDate = aptStartDate
                    aptEndHour = officialDayStartHour
                    aptStartHour = officialDayStartHour

                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempAppointmentDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots Where app(TempAppointmentDtBO.operativeIdColName) = operativeId Select app Order By CType(app(TempAppointmentDtBO.endDateStringColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        aptEndDate = CType(slotsResult.Last.Item(TempAppointmentDtBO.endDateStringColName), Date)
                        aptEndDate = CType(aptEndDate.ToLongDateString() + " " + slotsResult.Last.Item(TempAppointmentDtBO.endTimeColName).ToString(), Date)
                    End If
                    'set appointment start date , end time
                    Me.setAppointmentStartEndTime(aptStartDate, aptEndDate, aptStartTime, aptEndTime, aptStartHour, aptEndHour, tradeDurationHrs)
                    Dim slotCheckCounter As Integer = 1
                    'this loop 'll create the appointment slots but it 'll not end until and unless it 'll be sure
                    'that no appointment or leave exist in the new created appointment time slot
                    While (slotCheckCounter > 0)
                        'this slotCheckCounter = 0 , means that we are asuming time slot is valid i.e no appointment or leave exist during this assumed time slot
                        'if leave or appointment 'll exist then this slotCheckCounter 'll be incremented by 1 and one more while loop cycle 'll be executed.
                        slotCheckCounter = 0
                        'check the leaves if exist or not
                        Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, aptEndDate, aptStartTime, aptEndTime)
                        'if leave exist get the new date time but still its possiblity that leave ''ll exist in the next date time so 'll also be checked again.
                        If leaveResult.Count() > 0 Then
                            aptEndDate = CType(leaveResult.Last().Item("EndDate"), Date)
                            aptEndDate = GeneralHelper.combineDateAndTime(aptEndDate, leaveResult.Last().Item("EndTime").ToString())
                            'set appointment start date , end time
                            Me.setAppointmentStartEndTime(aptStartDate, aptEndDate, aptStartTime, aptEndTime, aptStartHour, aptEndHour, tradeDurationHrs)
                            'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot, 
                            'that no appointment or leave exist during this time slot
                            slotCheckCounter += 1
                        End If
                        'if appoinment exist in this time slot get the new date time but still its possiblity that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this 'll be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, aptEndDate, aptStartTime, aptEndTime, aptStartDate)
                        If Not IsNothing(appResult) And appResult.Count() > 0 Then
                            aptEndDate = CType(appResult.Last().Item("AppointmentEndDate"), Date)
                            ' aptEndDate = GeneralHelper.combineDateAndTime(aptEndDate, appResult.Last.Item("EndTime").ToString())
                            aptEndDate = GeneralHelper.unixTimeStampToDateTime(appResult.Last.Item("EndTimeInSec").ToString())
                            'set appointment start date , end time
                            Me.setAppointmentStartEndTime(aptStartDate, aptEndDate, aptStartTime, aptEndTime, aptStartHour, aptEndHour, tradeDurationHrs)
                            'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot,
                            'that no appointment or leave exist during this time slot
                            slotCheckCounter += 1
                        End If

                    End While ' while loop end
                    'add this time slot in appointment list
                    tempAppointmentDtBo.operativeId = operativeId
                    tempAppointmentDtBo.operative = operativeName
                    tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(aptStartTime)
                    tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(aptEndTime)
                    tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(aptStartDate)
                    tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(aptEndDate)
                    tempAppointmentDtBo.lastAdded = True
                    updateLastAdded(tempAppointmentDtBo)
                    tempAppointmentDtBo.addNewDataRow()

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > requiredResultCount Then
                        Exit While
                    End If
                Next
            End While 'outer while loop end
        End Sub
#End Region

#Region "set appointment start end time"
        ''' <summary>
        ''' This function calculates the appointment start time and appointment end time. We are assuming that end date 'll be used to calculate the new start date and end date
        ''' </summary>
        ''' <param name="aptEndDate"></param>
        ''' <param name="aptStartTime"></param>
        ''' <param name="aptEndTime"></param>
        ''' <param name="aptEndHour"></param>
        ''' <param name="tDuration"></param>
        ''' <remarks></remarks>
        Private Sub setAppointmentStartEndTime(ByRef aptStartDate As Date, ByRef aptEndDate As Date, ByRef aptStartTime As Date, ByRef aptEndTime As Date, ByRef aptStartHour As Double, ByRef aptEndHour As Double, ByVal tDuration As Double)
            Dim officialDayEndHour As Double = GeneralHelper.getOfficialDayEndHour()
            Dim officialDayStartHour As Double = GeneralHelper.getOfficialDayStartHour()
            Dim aptEndLapse As Double = 0

            'calculate the appointment end time and end hour. This 'll be used to find out the next date , time, hour            
            aptEndTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate)
            aptEndHour = GeneralHelper.getHoursFromTime(aptEndTime)

            'if coming date and time is  change the day if time ends
            Me.changeDayIfTimeEnd(aptEndDate, aptEndHour, officialDayEndHour)

            'now use appointment end date, end time & end hour to calculate the appointment start date, start time & start hour 
            aptStartDate = aptEndDate
            aptStartTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate)
            aptEndHour = GeneralHelper.getHoursFromTime(aptStartTime)
            aptStartHour = aptEndHour

            'This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
            Dim dayCounter As Integer = 0
            'run the loop until duration become zero
            While (tDuration > 0)
                dayCounter += 1
                Dim lapse As Double = 0
                'calculate the remaining time/lapse of the day
                lapse = officialDayEndHour - aptEndHour
                'subtract the total lapse/remaining time of the day from the trade duration
                tDuration = tDuration - lapse
                'if duration is less than zero than it means now we have to calculate the end hour of appointment otherwise we have to go to one day ahead to calculate the end time
                If (tDuration < 0) Then
                    'aptEndHour = GeneralHelper.getOfficialDayStartHour() + Math.Abs(tDuration)
                    'aptEndLapse = Math.Abs(tDuration)
                    If dayCounter > 1 Then
                        aptEndHour = officialDayStartHour + tDuration + lapse
                        aptEndLapse = aptEndHour - officialDayStartHour
                    Else
                        aptEndHour = aptStartHour + tDuration + lapse
                        aptEndLapse = aptEndHour - aptStartHour
                    End If

                ElseIf (tDuration = 0) Then
                    aptEndLapse = lapse
                ElseIf tDuration > 0 Then
                    'we have to go to one day ahead to calculate the end time, so setting running hour = dayEndhour
                    aptEndHour = officialDayEndHour
                End If
                Me.changeDayIfTimeEnd(aptEndDate, aptEndHour, officialDayEndHour)
            End While
            aptEndDate = aptEndDate.AddHours(aptEndLapse)
            aptEndTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate)
            aptEndHour = GeneralHelper.getHoursFromTime(aptEndTime)
        End Sub
#End Region

#Region "Skip Weekend"
        ''' <summary>
        ''' This function 'll skip the weekend (saturday or sunday)
        ''' </summary>
        ''' <param name="aptStartDate"></param>
        ''' <param name="aptStartHour"></param>
        ''' <remarks></remarks>
        Private Sub skipWeekend(ByRef aptStartDate As Date, ByRef aptStartHour As Double)
            If aptStartDate.DayOfWeek = DayOfWeek.Saturday Then
                aptStartDate = aptStartDate.AddDays(2)
                aptStartHour = GeneralHelper.getOfficialDayStartHour()
            ElseIf aptStartDate.DayOfWeek = DayOfWeek.Sunday Then
                aptStartDate = aptStartDate.AddDays(1)
                aptStartHour = GeneralHelper.getOfficialDayStartHour()
            End If
            aptStartDate = GeneralHelper.getDateTimeFromDateAndNumber(aptStartDate, aptStartHour)
        End Sub
#End Region

#Region "change Day If Time End"
        Private Function changeDayIfTimeEnd(ByRef aptEndDate As Date, ByRef aptEndHour As Double, ByVal OfficialDayEndHour As Double) As Boolean
            If (aptEndHour >= OfficialDayEndHour) Then
                'go to next day because current day slots have finished
                'Also set the running hour to zero for the nex day 
                aptEndDate = aptEndDate.AddDays(1)
                aptEndHour = GeneralHelper.getOfficialDayStartHour()
                aptEndDate = GeneralHelper.getDateTimeFromDateAndNumber(aptEndDate, aptEndHour)
                Me.skipWeekend(aptEndDate, aptEndHour)
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="aptStartTime"></param>
        ''' <param name="aptEndTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal aptEndTime As Date)

            aptStartTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            aptEndTime = CType(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(aptStartTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(aptEndTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso (
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin")))
                               Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="aptStartTime "></param>
        ''' <param name="aptEndTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal aptEndTime As Date, Optional ByVal aptStartDate As Date = Nothing)

            If aptStartDate = Nothing Then
                aptStartTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            Else
                aptStartTime = CType(aptStartDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            End If

            aptEndTime = CType(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(aptStartTime)
            endTimeInSec = GeneralHelper.convertDateInSec(aptEndTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (
                                                               (startTimeInSec = app("StartTimeInSec") And endTimeInSec < app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("EndTimeInSec") And endTimeInSec = app("EndTimeInSec")) _
                                                            Or (startTimeInSec > app("StartTimeInSec") And endTimeInSec < app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("StartTimeInSec") And endTimeInSec > app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("StartTimeInSec") And endTimeInSec > app("StartTimeInSec")) _
                                                            Or (startTimeInSec < app("EndTimeInSec") And endTimeInSec > app("EndTimeInSec")))
                             Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region

#Region "Filter Operatives Leaves Appointments On Trade"
        ''' <summary>
        ''' This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. This 'll furthur help us to minimize the time to create appointment slots
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tradeId"></param>
        ''' <remarks></remarks>
        Private Sub filterOperativesLeavesAppointmentsOnTrade(ByRef operativesDt As DataTable, ByRef leavesDt As DataTable, ByRef appointmentsDt As DataTable, ByVal tradeId As Integer)

            'filter the operatives on the basis of trade             
            Dim operativesRs = (From op In operativesDt Where op.Item("TradeId") = tradeId Select op)

            If operativesRs.Count() > 0 Then
                'save filtered operatives in separate data table
                operativesDt = operativesRs.CopyToDataTable()
                'make comma separated string of operative ids. This 'll be used to fetch the 
                Dim operativeIds As String = operativesRs.Select(Function(x) x("EmployeeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())

                'query the leaves data set to fetch only the appointments of selected operatives
                Dim leavesRs = (From ap In leavesDt Where String.Join(",", operativeIds).Contains(ap.Item("OperativeId")))
                If leavesRs.Count() > 0 Then
                    'save filtered appointments in separate data table
                    leavesDt = leavesRs.CopyToDataTable()
                End If

                'query the appointment data set to fetch only the appointments of selected operatives
                Dim appointmentRs = (From ap In appointmentsDt Where String.Join(",", operativeIds).Contains(ap.Item("OperativeId")))
                If appointmentRs.Count() > 0 Then
                    'save filtered appointments in separate data table
                    appointmentsDt = appointmentRs.CopyToDataTable()
                End If
            Else
                operativesDt.Clear()

            End If
        End Sub
#End Region

#Region "Add Hours in Start Date"
        ''' <summary>
        ''' Requirement is to start the appointments from today then appointment slots should start from 2 hours ahead of current time
        ''' </summary>
        ''' <param name="startDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getTodaysStartingHour(ByRef startDate As Date)
            'if both date are same then add two hours in it
            If DateTime.Compare(startDate.Date, Date.Today) = 0 Then
                startDate = DateTime.Now.AddHours(2)
            Else
                startDate = GeneralHelper.getDateTimeFromDateAndNumber(startDate, GeneralHelper.getOfficialDayStartHour())
                'startDate.AddHours(GeneralHelper.getOfficialDayStartHour())
            End If
            Return startDate.Hour
        End Function
#End Region

#Region "Order Temp Appointments By Distance"
        ''' <summary>
        ''' 'this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        ''' </summary>
        ''' <param name="tempTradeAptBo"></param>
        ''' <remarks></remarks>
        Public Sub orderAppointmentsByTimeAndOperative(ByRef tempTradeAptBo As TempTradeAppointmentBO)
            Dim appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable
            'Ordert The appointment, First By Distance then By Date and Then By Start Time.
            Dim appResult = (From app In appointments
                             Select app
                             Order By
                             CType(app(TempAppointmentDtBO.startDateStringColName), Date) Ascending _
                             , app(TempAppointmentDtBO.operativeColName) Ascending)

            If appResult.Count() > 0 Then
                tempTradeAptBo.tempAppointmentDtBo.dt = appResult.CopyToDataTable()
            End If

        End Sub

#End Region

#Region "update Last added column in tempTradeAptBo datatable"
        ''' <summary>
        ''' 'update Last added column in tempTradeAptBo datatable 
        ''' </summary>
        ''' <param name="tempTradeAptBo"></param>
        ''' <remarks></remarks>
        Public Sub updateLastAdded(ByRef tempTradeAptBo As TempAppointmentDtBO)
            Dim appointments = tempTradeAptBo.dt.AsEnumerable
            Dim appResult = (From app In appointments
                             Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
            If appResult.Count() > 0 Then
                appResult.First.Item(TempAppointmentDtBO.lastAddedColName) = False

            End If

        End Sub

#End Region
#End Region

#Region "Region For Intelligent Schduling on Replacement List"

#Region "create Single Day Operatives Appointment Slots"

        ''' <summary>
        ''' This function 'll return the data set containin leaves and employees
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub createSingleDayOperativesAppointmentSlots(ByRef appointmentSlotList As List(Of AppointmentSlotsBO),
                                                              ByVal leavesDt As DataTable,
                                                              ByVal appointmentsDt As DataTable,
                                                              ByVal operativeId As Integer,
                                                              ByVal selectedStartDate As Date,
                                                              ByVal tradeDurationHrs As Double)


            Dim officialDayStartHour As Double = Me.getTodaysStartingHour(selectedStartDate)
            Dim officialDayEndHour As Double = GeneralHelper.getOfficialDayEndHour()
            Dim runningDate As Date
            Dim aptStartTime As New Date
            Dim aptEndTime As New Date
            Dim runningHour As Double = 0
            Dim appointmentSlot As AppointmentSlotsBO = New AppointmentSlotsBO()

            'set the appointment starting hour e.g 9 (represents 9 o'clock in the morning)  
            'now we 'll add up the number(hours) in date to get date and time
            runningDate = GeneralHelper.getDateTimeFromDateAndNumber(selectedStartDate, officialDayStartHour)

            'initially appointment end date time 'll be equal to appointment start date time. 
            ' After that we 'll use trade duration to calculate the appointment end date time            
            runningHour = officialDayStartHour

            'set the appointment start time, end time, end hour
            Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, tradeDurationHrs)

            'This loop will run until the end of the official day end hour i.e 17:00 
            While (runningHour <= GeneralHelper.getOfficialDayEndHour)
                Dim slotCheckCounter As Integer = 1
                Dim isDayEnd As Boolean
                'this loop 'll create the appointment slots but it 'll not end until and unless it 'll be sure
                'that no appointment or leave exist in the new created appointment time slot
                While (slotCheckCounter > 0)
                    'this slotCheckCounter = 0 , means that we are asuming time slot is valid i.e no appointment or leave exist during this assumed time slot
                    'if leave or appointment 'll exist then this slotCheckCounter 'll be incremented by 1 and one more while loop cycle 'll be executed.
                    slotCheckCounter = 0

                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, runningDate, aptStartTime, aptEndTime)
                    'if leave exist get the new date time but still its possiblity that leave ''ll exist in the next date time so 'll also be checked again.
                    If leaveResult.Count() > 0 Then
                        runningDate = CType(leaveResult.Last().Item("EndDate"), Date)
                        runningDate = GeneralHelper.combineDateAndTime(runningDate, leaveResult.Last().Item("EndTime").ToString())
                        'set appointment start date , end time                        
                        Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, tradeDurationHrs)
                        isDayEnd = checkIfOfficialDayTimeEnd(runningDate, runningHour, officialDayEndHour)
                        If isDayEnd = True Then
                            Exit While
                        End If
                        'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot, 
                        'that no appointment or leave exist during this time slot
                        slotCheckCounter += 1
                    End If

                    'if appoinment exist in this time slot get the new date time but still its possiblity that appointment ''ll exist in the next time slot, so this while loop 'll run until
                    'this 'll be established that no new appointment actually exist in this time slot
                    Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, runningDate, aptStartTime, aptEndTime)
                    If Not IsNothing(appResult) And appResult.Count() > 0 Then
                        runningDate = CType(appResult.Last().Item("AppointmentEndDate"), Date)
                        runningDate = GeneralHelper.combineDateAndTime(runningDate, appResult.Last.Item("EndTime").ToString())
                        'set appointment start date , end time
                        Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, tradeDurationHrs)

                        'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot,
                        'that no appointment or leave exist during this time slot
                        slotCheckCounter += 1
                    End If

                    isDayEnd = checkIfOfficialDayTimeEnd(runningDate, runningHour, officialDayEndHour)
                    If isDayEnd = True Then
                        Exit While
                    End If
                End While 'end while loop -- slotCheckCounter 
                appointmentSlot.StartTime = GeneralHelper.get24HourTimeFormatFromDate(aptStartTime)
                appointmentSlot.EndTime = GeneralHelper.get24HourTimeFormatFromDate(aptEndTime)
                appointmentSlotList.Add(appointmentSlot)
                appointmentSlot = New AppointmentSlotsBO()

                'set appointment start date , end time
                Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, tradeDurationHrs)

            End While 'end while loop -- aptEndHour
        End Sub
#End Region

#Region "set appointment start end time"
        ''' <summary>
        ''' This function 'll set appointment start and end time. This function is for the appointment that can be day with in one day.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="aptStartTime"></param>
        ''' <param name="aptEndTime"></param>
        ''' <param name="runningHour"></param>
        ''' <param name="tradeDuration"></param>
        ''' <remarks></remarks>
        Private Sub setAppointmentStartEndTime(ByRef runningDate As Date, ByRef aptStartTime As Date, ByRef aptEndTime As Date, ByRef runningHour As Double, ByVal tradeDuration As Double)
            runningDate = GeneralHelper.getDateTimeFromDateAndNumber(runningDate, runningHour)
            aptStartTime = CType(runningDate.ToString("HH") + ":" + runningDate.ToString("mm"), Date)
            'runningHour = CType(aptStartTime.ToString("HH"), Double) + (CType(aptStartTime.ToString("mm"), Double) / 60)
            runningHour = runningHour + tradeDuration
            aptEndTime = CType(runningDate.AddHours(tradeDuration).ToString("HH") + ":" + runningDate.AddHours(tradeDuration).ToString("mm"), Date)
        End Sub
#End Region

#Region "check If Official Day Time End"
        Private Function checkIfOfficialDayTimeEnd(ByRef aptEndDate As Date, ByRef aptEndHour As Double, ByVal OfficialDayEndHour As Double) As Boolean
            If (aptEndHour >= OfficialDayEndHour) Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "is Operative Available"
        ''' <summary>
        ''' This Function 'll check either operative is available in this time slot or not
        ''' </summary>
        ''' <remarks></remarks>
        Public Function isOperativeAvailable(ByVal resultDataSet As DataSet, ByVal startDate As Date, ByVal operativeId As Integer, ByVal startDateTime As Date, ByVal endDateTime As Date)
            Dim isAvailable As Boolean = True
            'Dim startDateTime As Date = CType(startDate.AddHours(startHour).ToString("HH") + ":" + startDate.AddHours(startHour).ToString("mm"), Date)
            'Dim endDateTime As Date = CType(startDate.AddHours(endHour).ToString("HH") + ":" + startDate.AddHours(endHour).ToString("mm"), Date)

            Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(resultDataSet.Tables(ApplicationConstants.OperativesLeaves), operativeId, startDate, startDateTime, endDateTime)
            'if leave exist get the new date time but still its possibility that leave ''ll exist in the next date time so 'll also be checked again.
            If leaveResult.Count() > 0 Then
                isAvailable = False
            End If

            Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(resultDataSet.Tables(ApplicationConstants.OperativesAppointments), operativeId, startDate, startDateTime, endDateTime)
            If Not IsNothing(appResult) And appResult.Count() > 0 Then
                isAvailable = False
            End If
            Return isAvailable
        End Function
#End Region

#End Region

#Region "Get risk and vulnerability info"
        ''' <summary>
        ''' Get risk and vulnerability info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getRiskAndVulnerabilityInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getRiskAndVulnerabilityInfo(resultDataSet, journalId)

        End Sub
#End Region

#Region "Get associated PMOs"
        ''' <summary>
        ''' Get associated PMOs
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAssociatedPmos(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef type As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getAssociatedPmos(resultDataSet, propertyId, type)
        End Sub
#End Region

#Region "Get Locations"
        ''' <summary>
        ''' Get Locations
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getLocations(ByRef resultDataSet As DataSet)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getLocations(resultDataSet)
        End Sub
#End Region

#Region "Get Appointment types"
        ''' <summary>
        ''' Get planned Appointment types
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAppointmentTypes(ByRef resultDataSet As DataSet, ByVal isActive As Boolean)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getAppointmentTypes(resultDataSet, isActive)
        End Sub
#End Region


#Region "Get Condition Appointments to be Arranged List"

        ''' <summary>
        ''' Get Condition Works Appointment to be Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getConditionAppointmentsToBeArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.getConditionAppointmentsToBeArrangedList(resultDataSet, objPageSortBo, searchText)
        End Function

#End Region

#Region "Get Condition Appointment to be Arranged List"

        ''' <summary>
        ''' Get Condition Works Appointment Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function getConditionAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.getConditionAppointmentsArrangedList(resultDataSet, objPageSortBo, searchText)
        End Function

#End Region

#Region "Schedule Condition Works Appointment"
        ''' <summary>
        ''' Schedule Condition Works Appointment
        ''' </summary>
        ''' <param name="objConditionWorksAppointmentBO"></param>
        ''' <param name="appointmentId"></param>
        ''' <remarks></remarks>
        Public Function scheduleConditionWorksAppointment(ByVal objConditionWorksAppointmentBO As ConditionWorksAppointmentBO, ByRef appointmentId As Integer)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.scheduleConditionWorksAppointment(objConditionWorksAppointmentBO, appointmentId)
        End Function
#End Region

#Region "Schedule Confirmed Planned Appointments"
        ''' <summary>
        ''' This function 'll confirm scheduled Condition appointments. (i.e set ispending to false)
        ''' </summary>
        ''' <remarks></remarks>
        Public Function ConfirmedCoditionAppointments(ByVal journalId As Integer, ByVal userId As Integer)
            'TODO:check in stored procedure that appointment is already booked against this operative or not.
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.ConfirmedCoditionAppointments(journalId, userId)
        End Function

#End Region


#Region "Add Appointment to Scheduling"
        ''' <summary>
        ''' Add Appointment to Scheduling
        ''' </summary>
        ''' <param name="objMiscAppointmentBO"></param>
        ''' <param name="pmo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function addAppointmentToScheduling(ByVal objMiscAppointmentBO As MiscAppointmentBO, ByRef pmo As Integer, ByVal tradesDt As DataTable)
            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            Return objSchedulingDal.addAppointmentToScheduling(objMiscAppointmentBO, pmo, tradesDt)
        End Function
#End Region


#Region "Intelligent Scheduling Algorithm with core working and extended working hours"


#Region "create  TempAppointment Slots"
        Public Sub createTempAppointmentSlotsWithCoreAndExtendedHours(operativesDt As DataTable, leavesDt As DataTable, appointmentsDt As DataTable, ByRef tempAppointmentDtBo As TempAppointmentDtBO,
                                                   ByVal selectedStartDate As DateTime, ByVal tradeDurationHrs As Double, ByVal requiredResultCount As Integer,
         Optional previousOperativeId As Integer = 0)


            If tradeDurationHrs <= 0 Then
                Exit Sub
            End If
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim patchName As String = String.Empty
            Dim dueDateCheckCount As New Hashtable()
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(selectedStartDate)
            Dim dayEndHour As Double = GeneralHelper.getOfficialDayEndHour()

            Dim displayedTimeSlotCount As Integer = 1
            Dim sDate As DateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
            Dim appointmentStartDateTime As System.DateTime = Nothing
            Dim appointmentEndDate As System.DateTime = Nothing
            'System.DateTime aptEndTime = new System.DateTime();


            Dim operativeEligibility As New Dictionary(Of Integer, Boolean)()
            Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getOfficialDayStartHour().ToString() + ":00").ToString("HH:mm")
            Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + (If(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString()))).ToString("HH:mm")
            Dim skipOperative As Boolean = False

            Dim appointmentDueDate As DateTime = Convert.ToDateTime(DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")

            'Just to initlize start date and end date.
            appointmentStartDateTime = selectedStartDate
            appointmentEndDate = appointmentStartDateTime

            'Calculate Time slots given in count
            ' Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            While displayedTimeSlotCount <= requiredResultCount
                Dim uniqueCounter As Integer = 0
                Dim operativeCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop

                If operativesDt.Rows.Count = 0 Then
                    ' TODO: might not be correct. Was : Exit While
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    skipOperative = False
                    uniqueCounter += 1
                    operativeCounter += 1
                    operativeId = Convert.ToInt32(operativeDr("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr("FullName"))
                    patchName = Convert.ToString(operativeDr("PatchName"))
                    Dim operativeWorkingHours As New DataSet()
                    Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()

                    objSchedulingDAL.getOperativeWorkingHours(operativeWorkingHours, operativeId, ofcCoreStartTime, ofcCoreEndTime)
                    SessionManager.setOperativeworkingHourDs(operativeWorkingHours)


                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 

                    If (previousOperativeId <> 0) Then
                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For

                        End If
                    End If

                    'if syetem does not get any slot for an operative then system will check in maximum days which Defined in web.cong file.
                    ' Other wise system will goes to infinit loop. 
                    appointmentDueDate = Convert.ToDateTime(System.DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")


                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempAppointmentDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots
                                       Where app(TempAppointmentDtBO.operativeIdColName) = operativeId
                                       Select app
                                       Order By CType(app(TempAppointmentDtBO.endDateStringColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = Convert.ToDateTime(slotsResult.Last().Field(Of String)(TempAppointmentDtBO.endDateStringColName))
                    Else
                        'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        'appointmentStartDateTime = sDate;
                        appointmentStartDateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
                    End If



                    'Set appointment start time and appointment end time according to Core, Extended and On call Hour

                    Me.setVoidAppointmentStartEndTime(appointmentStartDateTime, appointmentEndDate, tradeDurationHrs, skipOperative, leavesDt, appointmentsDt,
                     operativeId, appointmentDueDate, dueDateCheckCount, uniqueCounter, tempAppointmentDtBo)


                    If (skipOperative = True) Then
                        'Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        'If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        'this condition 'll check either due date check has been executed against all the operatives
                        If (dueDateCheckCount.Count = operativesDt.Rows.Count) Then
                            'break; // TODO: might not be correct. Was : 
                            Return
                        Else
                            Continue For
                        End If
                    End If

                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    ' Dim distance As Double = getDistanceFromProperty(appointmentsDt, operativeId, appointmentStartDateTime, GeneralHelper.getHrsAndMinString(appointmentStartDateTime), propertySchedulingBo)

                    'update the lastAdded column in tempfaultBO.dt
                    'updateLastAdded(ref tempFaultAptBo);
                    'add this time slot in appointment list
                    tempAppointmentDtBo.operativeId = operativeId
                    tempAppointmentDtBo.operative = operativeName
                    tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(appointmentStartDateTime)
                    tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(appointmentEndDate)
                    tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(appointmentStartDateTime)
                    tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(appointmentEndDate)
                    tempAppointmentDtBo.lastAdded = True
                    updateLastAdded(tempAppointmentDtBo)
                    tempAppointmentDtBo.addNewDataRow()
                    'tempFaultAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom, distanceTo);

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > requiredResultCount Then
                        ' TODO: might not be correct. Was : Exit While
                        Exit For
                    End If
                Next
            End While
            'Calculate Time slots loop ends here
        End Sub

#End Region




#Region "is Opertive Available"
        Private Function isOpertiveAvailable(operativeWorkingHourDt As DataTable, dateToCheck As DateTime, ByRef dueDateCheckCount As Hashtable, operativeId As Integer, uniqueCounter As Integer, appointmentDueDate As DateTime) As Boolean

            '1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            Dim operativeAvailable As Boolean = checkDateIsGreaterThanDueDateAndAddToSkipList(uniqueCounter, dueDateCheckCount, dateToCheck, operativeId, appointmentDueDate) _
                                                OrElse Not checkOperativeAvailability(dateToCheck, operativeWorkingHourDt)

            Return operativeAvailable
        End Function

#End Region

#Region "if Date Is Greater Than Due Date"

        ''' <summary>
        ''' This function 'll check if running date is greater than due date of fault. If its true then add the entry in hash table and this (current) operative 'll not process any more for appointments
        ''' The count of total entries in hash table 'll help us to check that all operatives has been processed against the fault due date
        ''' </summary>
        ''' <param name="uniqueCounter"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="operativeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkDateIsGreaterThanDueDateAndAddToSkipList(ByRef uniqueCounter As Integer, ByRef dueDateCheckCount As Hashtable, ByVal dateToCheck As Date, ByVal operativeId As Integer, ByVal appointmentDueDate As DateTime) As Boolean
            Dim operativeSkipped As Boolean = False

            'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            If CType(dateToCheck.ToShortDateString(), Date) > appointmentDueDate Then
                operativeSkipped = True
                'this variable 'll check either due date check has been executed against all the operatives
                If Not (dueDateCheckCount.ContainsValue(operativeId)) Then
                    If dueDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                    Else
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                    End If
                End If
            End If
            Return operativeSkipped
        End Function

#End Region

#Region "Check operative availability"
        ''' <summary>
        ''' This function checks whether operative is fully absent in core and extended hours. If yes then this 
        ''' operative will be skipped.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="operativeHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkOperativeAvailability(ByVal runningDate As DateTime, ByVal operativeHoursDt As DataTable)

            Dim isAvailable As Boolean = True
            Dim workingHours = operativeHoursDt.AsEnumerable

            Dim coreWorkingHoursResult = (From app In workingHours
                                          Where app(ApplicationConstants.StartDateCol) = "NA" _
                                          And app(ApplicationConstants.EndDateCol) = "NA" _
                                          And app(ApplicationConstants.StartTimeCol) <> ""
                                          Select app)

            Dim extendedWorkingHoursResult = (From app In workingHours
                                              Where app(ApplicationConstants.StartDateCol) <> "NA"
                                              Select app)

            If (extendedWorkingHoursResult.Count > 0) Then
                extendedWorkingHoursResult = (From app In extendedWorkingHoursResult.CopyToDataTable().AsEnumerable()
                                              Where Convert.ToDateTime(app(ApplicationConstants.StartDateCol)) >= runningDate
                                              Select app)
            End If

            If coreWorkingHoursResult.Count = 0 And extendedWorkingHoursResult.Count = 0 Then
                isAvailable = False
            End If

            Return isAvailable

        End Function

#End Region

#Region "Get operatives available time slots for given date/day in ascending order of StartTime column"

        Private Function getOperativeAvailableSlotsByDateTime(ByVal operativeWorkingHourDt As DataTable, ByVal dateToFilter As Date) As DataTable
            'Check one or more working hours slots from the operative data table.
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable
                                   Where optHour(ApplicationConstants.StartDateCol) = dateToFilter.ToString("dd/MM/yyyy") _
                                         Or ((optHour(ApplicationConstants.WeekDayCol).ToString() = dateToFilter.DayOfWeek.ToString) _
                                        And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                        And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                        And optHour(ApplicationConstants.StartTimeCol) <> ""
                                        )
                                   Select optHour
                                   Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > dateToFilter.TimeOfDay))
            ''And If(optHour(ApplicationConstants.EndTimeCol) = "", False, CType(optHour(ApplicationConstants.EndTimeCol), TimeSpan) >= dateToFilter.TimeOfDay)

            ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            Dim operativeWorkingHourForCurrentDayDt = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable, operativeWorkingHourDt.Clone)
            Return operativeWorkingHourForCurrentDayDt
        End Function

#End Region

#Region "set Appointment Start and  End Time"
        ''' <summary>
        ''' This function calculates the appointment start time and appointment end time. We are assuming that end date 'll be used to calculate the new start date and end date
        ''' </summary>
        ''' <param name="appointmentStartDate"></param>
        ''' <param name="appointmentEndDate"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="skipOperative"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentDueDate"></param>
        ''' <param name="dueDateCheckCount"></param>
        ''' <param name="uniqueCounter"></param>
        ''' <remarks></remarks>
        Private Sub setVoidAppointmentStartEndTime(ByRef appointmentStartDate As DateTime, ByRef appointmentEndDate As DateTime, workDuration As Double, ByRef skipOperative As Boolean, leavesDt As DataTable, appointmentsDt As DataTable,
         operativeId As Integer, appointmentDueDate As DateTime, ByRef dueDateCheckCount As Hashtable, ByRef uniqueCounter As Integer, ByVal tempTradeAptBo As TempAppointmentDtBO)
            Dim isAppointmentCreated As Boolean = False
            Dim isExtendedApptVerified As Boolean = False
            'Get Operative core and extended working hours from session
            Dim operativeWorkingHourDs As DataSet = SessionManager.getOperativeworkingHourDs
            Dim operativeWorkingHourDt As DataTable = operativeWorkingHourDs.Tables(0)

            Dim currentRunningDate As System.DateTime = appointmentStartDate

            Dim loggingTime As Integer = 0
            'Do Until: appointment is created or the operative is skipped
            Do
                'Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future) 

                If isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, appointmentDueDate) Then

                    skipOperative = True
                Else
                    'Get operatives running day available time in ascending order of StartTime column
                    '  Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate)


                    isAppointmentCreated = getOperativeAvailableSlotsForMultipleDays(operativeWorkingHourDt, appointmentStartDate, appointmentEndDate, workDuration, appointmentDueDate, loggingTime)
                End If

                If isAppointmentCreated AndAlso Not skipOperative Then
                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate)
                    'if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    If leaveResult.Count() > 0 Then
                        appointmentStartDate = Convert.ToDateTime(leaveResult.Last().Field(Of DateTime)("EndDate"))
                        appointmentStartDate = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Field(Of String)("EndTime"))
                        isAppointmentCreated = False
                    Else
                        'if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this will be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate)
                        If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                            'appointmentStartDate = Convert.ToDateTime(appResult.Last().Field(Of DateTime)("AppointmentEndDate"))
                            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            isAppointmentCreated = False
                            ' Comment this area because Extended and Oncall hour should not be available in planned appointment 
                            'If isExtendedApptVerified = False Then

                            '    If appResult.Last().Field(Of Integer)("CreationDate") < loggingTime Then

                            '        Dim isExtendedHoursExist As Boolean = isExtendedIncludedInAppointment(operativeWorkingHourDt, appointmentsDt, appointmentStartDate, appResult.Last().Field(Of Integer)("CreationDate"), workDuration, operativeId, loggingTime, tempTradeAptBo)
                            '        If isExtendedHoursExist = False Then
                            '            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            '        Else
                            '            isExtendedApptVerified = True
                            '        End If

                            '    Else
                            '        appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            '    End If
                            '    isAppointmentCreated = False

                            'End If
                        End If
                    End If

                End If
            Loop While Not ((isAppointmentCreated OrElse skipOperative))
        End Sub

#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="leavesDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appoitmentEndDateTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appoitmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(appointmentStartDateTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(appoitmentEndDateTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso (
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin")))
                               Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime "></param>
        ''' <param name="appointmentEndDateTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")))
                             Select app Order By app("EndTimeInSec") Ascending)
            'AndAlso app("CreationDate") > loggingTime

            Return appResult
        End Function
#End Region


#Region "Day calculation algorithm."

        Private Function getOperativeAvailableSlotsForMultipleDays(operativeWorkingHourDt As DataTable, ByRef dateToFilter As DateTime, ByRef appointmentEndDate As DateTime, workDuration As Double, appointmentDueDate As DateTime,
                                                                   ByRef loggingTime As Integer) As Boolean
            Dim totalWorkingHour As Double = 0
            Dim hourDiff As Double = 0
            Dim isAppointmentCreated As Boolean = True
            Dim appointmentStartDate As DateTime = dateToFilter
            Dim isStartDateSelected As Boolean = False
            Dim isExtendedIncluded As Boolean = False
            'extended ot Oncall hours included foe specific day ot not.
            Dim remainingDuration As Double = workDuration
            Do

                Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable
                                       Where ((optHour(ApplicationConstants.WeekDayCol).ToString() = appointmentStartDate.DayOfWeek.ToString) _
                                            And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                            And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                            And optHour(ApplicationConstants.StartTimeCol) <> "")
                                       Select optHour
                                       Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > appointmentStartDate.TimeOfDay))

                ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
                Dim operativeWorkingHourForCurrentDayDt As DataTable = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())

                If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
                    'Iterate result set including core, Extended and on call hours.
                    For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                        'get operative day start hour for specific day
                        Dim nextBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.StartTimeColumn).ToString()
                        Dim nextBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockStartTime)
                        loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)
                        ' get operative day end hour for specific day
                        Dim nextBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeColumn).ToString()
                        Dim nextBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockEndTime)

                        'If operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString() <> "NA" Then
                        '    Dim extendedBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString()
                        '    Dim ExtendedBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.StartDateColumn)).ToLongDateString() + " ") & nextBlockStartTime)
                        '    Dim extendedBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString()
                        '    Dim ExtendedBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn)).ToLongDateString() + " ") & nextBlockEndTime)
                        '    If nextBlockStartDateTime.ToString("dd/MM/yyyy") = ExtendedBlockStartDateTime.ToString("dd/MM/yyyy") Then
                        '        isExtendedIncluded = True

                        '    End If
                        'End If
                        'If nextBlockEndDateTime > dateToFilter AndAlso (operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString() = "NA" OrElse isExtendedIncluded) Then
                        If nextBlockEndDateTime > dateToFilter Then

                            If Not isStartDateSelected AndAlso nextBlockStartDateTime >= appointmentStartDate Then
                                isStartDateSelected = True
                                dateToFilter = nextBlockStartDateTime
                                appointmentStartDate = nextBlockStartDateTime

                            ElseIf Not isStartDateSelected AndAlso dateToFilter > nextBlockStartDateTime Then
                                isStartDateSelected = True
                            ElseIf nextBlockStartDateTime >= appointmentStartDate Then
                                appointmentStartDate = nextBlockStartDateTime
                            End If
                            Dim operativeWorkingDayHour As Double = Convert.ToDouble(nextBlockEndDateTime.Subtract(appointmentStartDate).TotalHours)
                            appointmentEndDate = nextBlockEndDateTime



                            If totalWorkingHour + operativeWorkingDayHour > workDuration Then
                                hourDiff = (totalWorkingHour + operativeWorkingDayHour) - workDuration
                                appointmentEndDate = nextBlockEndDateTime.AddHours(-hourDiff)
                                appointmentEndDate = appointmentStartDate.AddHours(remainingDuration)
                            End If

                            If hourDiff > 0 Then
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour - hourDiff

                            ElseIf hourDiff <= 0 AndAlso isStartDateSelected Then
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour

                            End If

                            If totalWorkingHour = workDuration Then
                                Exit For
                            End If
                        End If

                    Next
                End If
                changeDay(appointmentStartDate, "00:00")
                'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
                If Convert.ToDateTime(dateToFilter.ToShortDateString()) > appointmentDueDate Then
                    isAppointmentCreated = False
                    Exit Do
                End If

                remainingDuration = workDuration - totalWorkingHour
            Loop While Not (totalWorkingHour = workDuration)


            Return isAppointmentCreated
            ' return operativeWorkingHourForCurrentDayDt;
        End Function
#End Region

#Region "Change day"

        ''' <summary>
        ''' This function will change day
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub changeDay(ByRef runningDate As Date, ByVal nextDayStartTime As String)
            runningDate = runningDate.AddDays(1)
            runningDate = CType(runningDate.ToLongDateString() + " " + nextDayStartTime, Date)
        End Sub

#End Region

#Region "Is Extended Hours included in Appointment"
        ''' <summary>
        ''' Is Extended Hours included in Appointment of Multiple days.
        ''' </summary>
        ''' <param name="operativeWorkingHourDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="dateToFilter"></param>
        ''' <param name="creationDateInSecond"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="loggingTime"></param>
        ''' <param name="tempTradeAptBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedIncludedInAppointment(operativeWorkingHourDt As DataTable, ByVal appointmentsDt As DataTable, ByRef dateToFilter As DateTime, ByVal creationDateInSecond As Integer,
                                                    ByVal workDuration As Double, ByVal operativeId As Integer, ByVal loggingTime As Integer, ByVal tempTradeAptBo As TempAppointmentDtBO)
            Dim appointmentStartDate As DateTime = dateToFilter
            Dim result As Boolean = False
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable
                                   Where optHour(ApplicationConstants.CreationDateCol) > creationDateInSecond _
                                        And optHour(ApplicationConstants.StartDateCol) <> "NA" _
                                        And optHour(ApplicationConstants.EndDateCol) <> "NA" _
                                        And optHour(ApplicationConstants.StartTimeCol) <> ""
                                   Select optHour
                                   Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending)

            Dim operativeWorkingHourForCurrentDayDt As DataTable = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())
            If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then

                For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                    Dim extendedBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.StartTimeColumn).ToString()
                    Dim extendedBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.StartDateCol)).ToLongDateString() + " ") & extendedBlockStartTime)
                    dateToFilter = extendedBlockStartDateTime
                    result = True
                    ' get operative day end hour for specific day
                    Dim extendedBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeColumn).ToString()
                    Dim extendedBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.EndDateCol)).ToLongDateString() + " ") & extendedBlockEndTime)

                    Dim isTimeSlotAlreadyAdded As Boolean = False
                    isTimeSlotAlreadyAdded = checkTimeSlotDisplayed(tempTradeAptBo, dateToFilter, extendedBlockEndDateTime, operativeId)


                    Dim apptEndDateTime As DateTime = dateToFilter.AddHours(workDuration)
                    Dim apptEndDateTimeInSec As Integer
                    Dim extendedBlockEndDateTimeInSec As Integer
                    apptEndDateTimeInSec = GeneralHelper.convertDateInSec(apptEndDateTime)
                    extendedBlockEndDateTimeInSec = GeneralHelper.convertDateInSec(extendedBlockEndDateTime)
                    If apptEndDateTimeInSec > extendedBlockEndDateTimeInSec Then
                        result = False
                        Continue For
                    Else

                        Dim isExtendedApptExist As Boolean = True
                        isExtendedApptExist = isExtendedAppointmentExist(appointmentsDt, operativeId, dateToFilter, apptEndDateTime, loggingTime)
                        If isExtendedApptExist = False Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return result

        End Function

#End Region


#Region "is Extended Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given Extended start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appointmentEndDateTime"></param>
        ''' <param name="loggingTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByRef appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date, ByVal loggingTime As Integer)

            ''çonvert time into seconds for comparison

            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer
            Dim result As Boolean = False
            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                                                        AndAlso app("CreationDate") > loggingTime
                             Select app Order By app("EndTimeInSec") Ascending)
            If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                appointmentStartDateTime = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                result = True
            End If

            Return result
        End Function
#End Region

#Region "Check Time Slot already displayed in Gridview"
        ''' <summary>
        ''' Check Time Slot already displayed in Gridview
        ''' </summary>
        ''' <param name="tempTradeAptBo"></param>
        ''' <remarks></remarks>
        Public Function checkTimeSlotDisplayed(ByVal tempTradeAptBo As TempAppointmentDtBO, ByRef appointmentStartDate As DateTime, ByVal extendedBlockEndDateTime As DateTime, operativeId As Integer)
            Dim result As Boolean = False
            Dim runningDate As DateTime = appointmentStartDate


            Dim appointments = tempTradeAptBo.dt.AsEnumerable
            Dim appResult = (From app In appointments
                             Where app(TempAppointmentDtBO.startDateStringColName) >= runningDate _
                            AndAlso app(TempAppointmentDtBO.endDateStringColName) <= extendedBlockEndDateTime _
                             And app(TempAppointmentDtBO.operativeIdColName) = operativeId Select app Order By app(TempAppointmentDtBO.startDateStringColName) Ascending)
            If appResult.Count() > 0 Then
                appointmentStartDate = appResult.Last.Item(TempAppointmentDtBO.endDateStringColName)
                result = True
            End If
            Return result
        End Function

#End Region
#End Region


    End Class

End Namespace
