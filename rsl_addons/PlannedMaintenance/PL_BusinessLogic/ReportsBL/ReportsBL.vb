﻿Imports PL_BusinessObject
Imports PL_DataAccess
Imports PL_Utilities

Namespace PL_BusinessLogic

    Public Class ReportsBL

#Region "Functions"

#Region "Get All Schemes"
        Public Sub getAllSchemes(ByRef resultDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.getAllSchemes(resultDataSet)
        End Sub
#End Region

#Region "get Cost Component Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <remarks></remarks>
        Sub getCostComponentData(ByRef resultDataset As DataSet, ByVal searchedText As String, ByVal selectedScheme As Integer, ByVal startYear As String, ByVal flag As Boolean, ByRef editedParameterDt As DataTable, ByRef updatedPropertyDt As DataTable)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)
        End Sub
#End Region
        
#Region "get Component Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <remarks></remarks>
        Sub getComponentData(ByRef resultDataset As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.getComponentData(resultDataset)
        End Sub
#End Region

#Region "Save Parameters Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="objComponentBO"></param>
        ''' <remarks></remarks>
        Sub saveParametersData(ByRef resultDataSet As DataSet, ByVal objComponentBO As ComponentsBO, ByVal selectedScheme As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.saveParametersData(resultDataSet, objComponentBO, selectedScheme)
        End Sub
#End Region
        
#Region "Get Yearly Component Property List"
        ''' <summary>
        ''' Returns Yearly Component Property List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objComponentYearSearchBO"></param>
        ''' <remarks></remarks>
        Public Function getYearlyComponentPropertyList(ByRef resultDataSet As DataSet, ByVal objComponentYearSearchBO As ComponentYearSearchBO, ByVal objPageSortBO As PageSortBO, ByVal objUpdatedPropertyDt As DataTable, ByVal objEditedComponentDt As DataTable, Optional ByVal isFullList As Boolean = False)

            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            Return objReportsDAL.getYearlyComponentPropertyList(resultDataSet, objComponentYearSearchBO, objPageSortBO, objUpdatedPropertyDt, objEditedComponentDt, isFullList)

        End Function
#End Region

#Region "Save due dates"
        ''' <summary>
        ''' Save due dates
        ''' </summary>
        ''' <param name="dueDateDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveDuedates(ByVal dueDateDt As DataTable)

            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            Return objReportsDAL.saveDuedates(dueDateDt)

        End Function

#End Region

#Region "Get Assigned to Contractor List"

        Function getAssignedToContractorReport(ByRef resultDataSet As DataSet, ByVal objPageSortBo As Object, ByVal searchText As String) As Integer
            Dim objReportsDal As New ReportsDAL
            Return objReportsDal.getAssignedToContractorReport(resultDataSet, objPageSortBo, searchText)
        End Function

#End Region

#Region "Get Condition Rating List"
        ''' <summary>
        ''' Get Condition Rating List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean, Optional ByRef componentId As String = "-1")

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getConditionRatingList(resultDataSet, objPageSortBo, searchText, isFullList, componentId)

        End Sub
#End Region




#Region "Get Condition Rating Approved List"
        ''' <summary>
        ''' Get Condition Rating Approved List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingApprovedList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean, Optional ByRef componentId As String = "-1")

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getConditionRatingApprovedList(resultDataSet, objPageSortBo, searchText, isFullList, componentId)

        End Sub
#End Region

#Region "Get Condition Rating Rejected List"
        ''' <summary>
        ''' Get Condition Rating Rejected List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingRejectedList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getConditionRatingRejectedList(resultDataSet, objPageSortBo, searchText, isFullList)

        End Sub
#End Region

#Region "Get Condition More Detail"
        ''' <summary>
        ''' Get Condition More Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getConditionMoreDetail(ByRef resultDataSet As DataSet, ByVal conditionWorkId As Integer)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getConditionMoreDetail(resultDataSet, conditionWorkId)

        End Sub
#End Region

#Region "Get Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getConditionReasons(ByRef resultDataSet As DataSet)

             Dim objReportsDal As New ReportsDAL
            objReportsDal.getConditionReasons(resultDataSet)

        End Sub
#End Region

#Region "Get Cancel Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getCancelReasons(ByRef resultDataSet As DataSet)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getCancelReasons(resultDataSet)

        End Sub
#End Region

#Region "Approve Condition Works"
        ''' <summary>
        ''' Approve Condition Works
        ''' </summary>
        ''' <param name="conditionWorkId"></param>
        ''' <remarks></remarks>
        Public Function approveConditionWorks(ByVal conditionWorkId As Integer, ByVal userId As Integer)

            Dim objReportsDal As New ReportsDAL
            Return objReportsDal.approveConditionWorks(conditionWorkId, userId)

        End Function
#End Region

#Region "Reject Condition Works"
        ''' <summary>
        ''' Reject Condition Works
        ''' </summary>
        ''' <param name="objRejectBO"></param>
        ''' <remarks></remarks>
        Public Function rejectConditionWorks(ByVal objRejectBO As RejectConditionBO)

            Dim objReportsDal As New ReportsDAL
            Return objReportsDal.rejectConditionWorks(objRejectBO)

        End Function
#End Region

#Region "Get budget spend report"
        ''' <summary>
        ''' Get budget spend report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReport(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal searchText As String, ByVal isFullList As Boolean)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getBudgetSpendReport(resultDataSet, objPageSortBo, replacementYear, schemeId, searchText, isFullList)

        End Sub
#End Region

#Region "Get budget spend report detail"
        ''' <summary>
        ''' Get budget spend report detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReportDetail(ByRef resultDataSet As DataSet, ByVal replacementYear As String, ByVal status As String, ByVal schemeId As Integer, ByVal componentId As Integer)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getBudgetSpendReportDetail(resultDataSet, replacementYear, status, schemeId, componentId)

        End Sub
#End Region

#Region "Get budget spend report excel detail"
        ''' <summary>
        ''' Get budget spend report excel detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReportExcelDetail(ByRef resultDataSet As DataSet, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal searchText As String)

            Dim objReportsDal As New ReportsDAL
            objReportsDal.getBudgetSpendReportExcelDetail(resultDataSet, replacementYear, schemeId, searchText)

        End Sub
#End Region

#Region " Get All Fiscal Years "
        Public Sub getAllFiscalYears(ByRef resultDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.getAllFiscalYears(resultDataSet)
        End Sub
#End Region

#Region "get CompletedWorks List"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="search"></param>
        ''' <param name="PlannedType"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function getCompletedWorksList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal search As String, ByVal PlannedType As Integer) As Integer
            Dim objReportsDal As New ReportsDAL()
            Return objReportsDal.getCompletedWorksList(resultDataSet, objPageSortBo, schemeId, search, PlannedType)
        End Function
#End Region

#End Region

    End Class

End Namespace
