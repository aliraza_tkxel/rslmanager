﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports PL_Utilities
Imports PL_DataAccess
Imports PL_BusinessObject

Namespace PL_BusinessLogic
    Public Class ComponentsBL
        Dim objComponentsDAL As ComponentsDAL = New ComponentsDAL()

#Region "Get Components For LifeCycle"
        ''' <summary>
        ''' Get Components For LifeCycle
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <remarks></remarks>
        Public Function getComponentsForLifeCycle(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO)
            Return objComponentsDAL.getComponentsForLifeCycle(resultDataSet, objPageSortBo)
        End Function
#End Region

#Region "get Component detail"
        ''' <summary>
        ''' get Component Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Public Sub getComponentDetail(ByRef resultDataSet As DataSet, ByVal componentId As Integer)
            objComponentsDAL.getComponentdetail(resultDataSet, Convert.ToInt32(componentId))
        End Sub
#End Region

#Region "Check can delete"
        ''' <summary>
        ''' Check can delete
        ''' </summary>
        ''' <param name="tradeId"></param>
        ''' <param name="duration"></param>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Public Function checkCanDelete(ByVal tradeId As Integer, ByVal duration As Double, ByVal componentId As Integer)
            Return objComponentsDAL.checkComponentExist(tradeId, duration, componentId)
        End Function
#End Region

#Region "get Trade LookUp Values All"
        ''' <summary>
        ''' get Trade LookUp Values All
        ''' </summary>
        ''' <param name="lstLookUp"></param>
        ''' <remarks></remarks>
        Public Sub getTradeLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            objComponentsDAL.getTradeLookUpValuesAll(lstLookUp)
        End Sub
#End Region

#Region "Update Component Detail"
  
        Public Sub UpdateComponent(ByRef objComponentBO As ComponentsBO, ByVal tradesToInsert As DataTable, ByVal tradesToDelete As DataTable, ByVal tradesOrderDt As DataTable)
            objComponentsDAL.UpdateComponent(objComponentBO, tradesToInsert, tradesToDelete, tradesOrderDt)
        End Sub
#End Region
    End Class
End Namespace