﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports PL_Utilities
Imports PL_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports PL_BusinessObject
Imports System.Data.SqlClient


Namespace PL_BusinessLogic

    Public Class ReplacementBL

#Region "Get All Components"
        ''' <summary>
        ''' Get All Components
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllComponents(ByRef resultDataSet As DataSet)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getAllComponents(resultDataSet)
        End Sub
#End Region

#Region "Get All Schemes"
        ''' <summary>
        ''' Get All Schemes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllSchemes(ByRef resultDataSet As DataSet)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getAllSchemes(resultDataSet)
        End Sub
#End Region

#Region "Get schemes against replacement year and component"
        ''' <summary>
        ''' Get All Schemes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getSchemesAgainstReplacementYearAndComponent(ByRef resultDataSet As DataSet, ByVal replacementYear As Integer, ByVal componentId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getSchemesAgainstReplacementYearAndComponent(resultDataSet, replacementYear, componentId)
        End Sub
#End Region

#Region "Get All Appointment Statuses"
        ''' <summary>
        ''' Get All Appointment Statuses
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllAppointmentStatuses(ByRef resultDataSet As DataSet)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getAllAppointmentStatuses(resultDataSet)
        End Sub
#End Region

#Region "Get Replacement List"

        Public Sub getReplacementList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal componentId As Integer, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal aptStatusId As Integer, ByVal yearOperator As Integer, ByVal isFullList As Boolean, ByVal searchText As String)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getReplacementList(resultDataSet, objPageSortBo, componentId, replacementYear, schemeId, aptStatusId, yearOperator, isFullList, searchText)
        End Sub
#End Region

#Region "Schedule selected properties"
        ''' <summary>
        ''' Schedule selected properties
        ''' </summary>
        ''' <param name="selectedProperties"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub scheduleSelectedProperties(ByVal selectedProperties As DataTable, ByVal userId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.scheduleSelectedProperties(selectedProperties, userId)
        End Sub
#End Region

#Region "Get Property Detail"
        ''' <summary>
        ''' Get Property Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Sub getPropertyDetail(ByRef resultDataSet As DataSet, ByVal propertyId As String)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getPropertyDetail(resultDataSet, propertyId)
        End Sub
#End Region

#Region "Get Operative Info"
        ''' <summary>
        ''' Get Operative Info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operaiveId"></param>
        ''' <remarks></remarks>
        Sub getOperativeInformation(ByRef resultDataSet As DataSet, ByVal operaiveId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getOperativeInformation(resultDataSet, operaiveId)
        End Sub


#End Region

#Region "Get All trades"
        ''' <summary>
        ''' Get All trades
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Sub getAllTrades(ByRef resultDataSet As DataSet, ByVal componentId As String)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getAllTrades(resultDataSet, Convert.ToInt32(componentId))
        End Sub
#End Region

#Region "get Inspection Arranged Actions"
        ''' <summary>
        ''' Get Actions by statusId
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getInspectionArrangedActions(ByRef resultDataSet As DataSet)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getInspectionArrangedActions(resultDataSet)            
        End Sub
#End Region

#Region "Get Employee By Trade Id"
        ''' <summary>
        ''' This function 'll return the employees based on trade id
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getEmployeeByTradeId(ByRef resultDataSet As DataSet, ByVal tradeId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getEmployeeByTradeId(resultDataSet, tradeId)
        End Sub
#End Region

#Region "Get All Employees"
        ''' <summary>
        ''' This function 'll return all employees
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getAllEmployees(ByRef resultDataSet As DataSet)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getAllEmployees(resultDataSet)
        End Sub
#End Region

#Region "Get Letter by actionId"
        ''' <summary>
        ''' Get Letter by actionId
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="actionId"></param>
        ''' <remarks></remarks>
        Sub getLetterByActionId(ByRef resultDataSet As DataSet, ByVal actionId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getLetterByActionId(resultDataSet, Convert.ToInt32(actionId))
        End Sub
#End Region

#Region "Arrange Inspection"
        ''' <summary>
        ''' Arrange Inspection
        ''' </summary>
        ''' <param name="objArrangeInspection"></param>
        ''' <param name="savedLetterDt"></param>
        ''' <remarks></remarks>
        Function arrangeInspection(ByVal objArrangeInspection As ArrangeInspectionBO, ByRef savedLetterDt As DataTable)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            Return objReplacementDAL.arrangeInspection(objArrangeInspection, savedLetterDt)
        End Function
#End Region

#Region "Get inspection info"
        ''' <summary>
        ''' Get inspection info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="inspectionId"></param>
        ''' <remarks></remarks>
        Sub getInspectionInfo(ByRef resultDataSet As DataSet, ByVal inspectionId As Integer)
            Dim objReplacementDAL As ReplacementDAL = New ReplacementDAL()
            objReplacementDAL.getInspectionInfo(resultDataSet, inspectionId)
        End Sub
#End Region

    End Class

End Namespace
