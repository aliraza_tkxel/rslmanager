﻿Imports System
Imports PL_BusinessObject
Imports PL_Utilities

Public Class CustomerBL
    Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

#Region "Functions"

#Region "Update customer address"
    Public Function updateAddress(ByVal objCustomerBO As CustomerBO)

        Return objCustomerDAL.updateAddress(objCustomerBO)

    End Function

#End Region

#End Region

End Class
