﻿Imports System
Imports PL_BusinessObject
Imports PL_DataAccess
Imports PL_Utilities

Namespace PL_BusinessLogic
    Public Class StatusActionBL
#Region "Functions"

#Region "Status And Action"

#Region "Get All Statuses"
        ''' <summary>
        ''' This function shall check either status exist in cache or not. If that does not exist it 'll fetch that from database
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllStatuses(ByRef resultDataSet As DataSet)
            resultDataSet = CacheManager.getAllStatuses()
            If resultDataSet.Tables.Count() = 0 Then
                Dim statusActionDal As StatusActionDAL = New StatusActionDAL()
                statusActionDal.getAllStatuses(resultDataSet)
                CacheManager.setAllStatuses(resultDataSet)
            End If            
        End Sub
#End Region

#Region "Add Status"
        ''' <summary>
        ''' This funciton will be used to add the status 
        ''' </summary>
        ''' <param name="statusTitle"></param>
        ''' <param name="selectedRanking"></param>        
        ''' <returns>returns true or false</returns>
        ''' <remarks></remarks>
        Public Function addStatus(ByVal statusTitle As String, ByVal selectedRanking As Integer) As Boolean
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            Dim allStatusDs As DataSet = New DataSet()
            Dim isSaved As Boolean = False
            isSaved = statusDal.addStatus(statusTitle, selectedRanking, allStatusDs)
            If (isSaved = True) Then
                'if saved successfully then store this dataset in cache
                CacheManager.setAllStatuses(allStatusDs)
            End If
            Return isSaved
        End Function
#End Region

#Region "Edit Status"
        ''' <summary>
        ''' 'This function 'll update the status
        ''' </summary>
        ''' <param name="statusId"></param>
        ''' <param name="statusTitle"></param>
        ''' <param name="selectedRanking"></param>
        ''' <param name="modifiedBy"></param>
        ''' <returns>saved or not in boolean</returns>
        ''' <remarks></remarks>
        Public Function editStatus(ByVal statusId As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer, ByVal modifiedBy As Integer) As Boolean
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            Dim allStatusDs As DataSet = New DataSet()
            Dim isSaved As Boolean = False
            isSaved = statusDal.editStatus(statusId, statusTitle, selectedRanking, modifiedBy, allStatusDs)
            If (isSaved = True) Then
                'if saved successfully then store this dataset in cache
                CacheManager.setAllStatuses(allStatusDs)
            End If
            Return isSaved
        End Function
#End Region

#Region "Get All Actions"
        ''' <summary>
        ''' This function shall check either actions exist in cache or not. If that does not exist it 'll fetch that from database
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllActions(ByRef resultDataSet As DataSet)
            'fetch the actions from the cache
            resultDataSet = CacheManager.getAllActions()

            'if actions does not exist cache then get it from database
            If resultDataSet.Tables.Count() = 0 Then
                Dim statusActionDal As StatusActionDAL = New StatusActionDAL()
                statusActionDal.getAllActions(resultDataSet)
                CacheManager.setAllActions(resultDataSet)
            End If
        End Sub
#End Region

#Region "Add Action"
        ''' <summary>
        ''' This function 'll add the action in database and then it 'll update the cache
        ''' </summary>
        ''' <param name="StatusId"></param>
        ''' <param name="Title"></param>
        ''' <param name="ranking"></param>
        ''' <param name="CreatedBy"></param>
        ''' <remarks></remarks>
        Public Function addAction(ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal CreatedBy As Integer)
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            Dim allActionDs As DataSet = New DataSet()
            Dim isSaved As Boolean = False
            isSaved = statusDal.addAction(StatusId, Title, ranking, CreatedBy, allActionDs)
            'if actions have been saved successfully , update the cache
            If isSaved = True Then
                CacheManager.setAllActions(allActionDs)
            End If
            Return isSaved
        End Function
#End Region

#Region "Edit Action"
        ''' <summary>
        ''' This function 'll update the action in database and then it 'll update the cache
        ''' </summary>
        ''' <param name="ActionId"></param>
        ''' <param name="StatusId"></param>
        ''' <param name="Title"></param>
        ''' <param name="ranking"></param>
        ''' <param name="modifiedBy"></param>
        ''' <remarks></remarks>
        Public Function editAction(ByVal ActionId As Integer, ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal modifiedBy As Integer)
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            Dim allActionDs As DataSet = New DataSet()
            Dim isSaved As Boolean = False
            isSaved = statusDal.editAction(ActionId, StatusId, Title, ranking, modifiedBy, allActionDs)

            'if actions have been saved successfully , update the cache
            If isSaved = True Then
                CacheManager.setAllActions(allActionDs)
            End If
            Return isSaved
        End Function

#End Region

#Region "Get Actions By StatusId"
        ''' <summary>
        ''' This function 'll return the action based on status id
        ''' </summary>
        ''' <param name="statusId"></param>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getActionsByStatusId(ByVal statusId As Integer, ByRef resultDataSet As DataSet)
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            statusDal.getActionsByStatusId(statusId, resultDataSet)
        End Sub
#End Region

#Region "get Action Ranking By StatusId"
        ''' <summary>
        ''' This fuction 'll get the rankings of action on the basis of status id
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="statusId"></param>
        ''' <remarks></remarks>
        Public Sub getActionRankingByStatusId(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            statusDal.getActionRankingByStatusId(resultDataSet, statusId)
        End Sub
#End Region

#Region "Get Actions By Status Id"
        ''' <summary>
        ''' This function 'll fetch the letters on the basis of status id
        ''' </summary>
        ''' <param name="statusId"></param>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getLettersByStatusId(ByVal statusId As Int32, ByRef resultDataSet As DataSet)
            Dim statusDal As StatusActionDAL = New StatusActionDAL()
            statusDal.getLettersByStatusId(statusId, resultDataSet)
        End Sub
#End Region



#End Region

#End Region
    End Class
End Namespace



