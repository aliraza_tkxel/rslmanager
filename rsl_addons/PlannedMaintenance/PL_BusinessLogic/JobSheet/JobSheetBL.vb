﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports PL_Utilities
Imports PL_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports PL_BusinessObject
Imports System.Data.SqlClient
Imports System.Globalization


Namespace PL_BusinessLogic

    Public Class JobSheetBL

#Region "Get Job Sheed Summary Detail By JSN(Job Sheed Number)"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.getJobSheetSummaryByJsn(ds, jsn)
        End Sub

#End Region

#Region "Get Status Look Up For Job Sheet Summary Update"

        Sub getPlannedStatusLookUp(ByRef dsPlannedStatus As DataSet)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.getPlannedStatusLookUp(dsPlannedStatus)
        End Sub

#End Region

#Region "Save/Update Appointment Stauts in Database with Notes"

        Sub setAppointmentJobSheetStatusUpdate(ByRef objJobSheetBO As JobSheetBO)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.setAppointmentJobSheetStatusUpdate(objJobSheetBO)
        End Sub

#End Region
    End Class

End Namespace
