﻿Imports System
Imports PL_Utilities
Imports PL_DataAccess
Imports PL_BusinessObject

Public Class AssignToContractorBL

#Region "Functions"

#Region "Get Cost Centre Drop Down Values."

    Sub GetCostCentreDropDownVales(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL = New AssignToContractorDAL
        objAssignToContractorDAL.GetCostCentreDropDownVales(dropDownList)
    End Sub

#End Region

#Region "Get Budget Head Drop Down Values by Cost Centre Id"

    Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef dropDownList As List(Of DropDownBO), ByRef CostCentreId As Integer)
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, CostCentreId)
    End Sub

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

    Sub GetExpenditureDropDownValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)
    End Sub

#End Region

#Region "Get Contractor Having Planned Contract"

    Sub GetPlannedContractors(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetPlannedContractors(dropDownList)
    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByRef dropDownList As List(Of DropDownBO), ByRef ContractorId As Integer)
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetContactDropDownValuesbyContractorId(dropDownList, ContractorId)
    End Sub

#End Region
#Region "Get contact email details"
    ''' <summary>
    ''' Get contact email details
    ''' </summary>
    ''' <param name="contactId"></param>
    ''' <param name="detailsForEmailDS"></param>
    ''' <remarks></remarks>

    Sub getContactEmailDetail(ByRef contactId As Integer, ByRef detailsForEmailDS As DataSet)
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.getContactEmailDetail(contactId, detailsForEmailDS)
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

    Sub getVatDropDownValues(ByRef vatBoList As List(Of VatBo))
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetVatDropDownValues(vatBoList)
    End Sub

    Sub getVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.GetVatDropDownValues(dropDownList)
    End Sub

#End Region

#Region "Assign Work to Contractor"

    Function assignToContractor(ByVal assignToContractorBo As AssignToContractorBo) As Boolean
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        Return objAssignToContractorDAL.assignToContractor(assignToContractorBo)
    End Function

#End Region

#Region "Get Details for email"

    ''' <summary>
    ''' This function is to get details for email, these details include contractor name, email and other details.
    ''' Contact details of customer/tenant.
    ''' Risk/Vulnerability details of the tenant
    ''' Property Address
    ''' </summary>
    ''' <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
    ''' <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
    ''' <remarks></remarks>
    Sub getdetailsForEmail(ByRef assignToContractorBo As AssignToContractorBo, ByRef detailsForEmailDS As DataSet)
        Dim objAssignToContractorDAL As New AssignToContractorDAL
        objAssignToContractorDAL.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
    End Sub

#End Region

#End Region

End Class
