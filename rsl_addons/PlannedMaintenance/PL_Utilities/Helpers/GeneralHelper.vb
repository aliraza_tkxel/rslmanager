﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web.UI.Page
Imports System.Text
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports PL_BusinessObject
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports PL_Utilities



Namespace PL_Utilities
    Public Class GeneralHelper

#Region "get Document Upload Path"
        Public Shared Function getDocumentUploadPath() As String
            Return ConfigurationManager.AppSettings("DocumentsUploadPath")
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime([date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "get Official Day Start Hour"
        Public Shared Function getOfficialDayStartHour() As String
            Return CType(ConfigurationManager.AppSettings("OfficialDayStartHour"), Double)
        End Function
#End Region

#Region "get Official Day End Hour"
        Public Shared Function getOfficialDayEndHour() As String
            Return CType(ConfigurationManager.AppSettings("OfficialDayEndHour"), Double)
        End Function
#End Region

#Region "get Appointment Duration Hours"
        ''' <summary>
        ''' 'Appointment Duration represents total hours to complete the appointment e.g 1 hour
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentDurationHours() As String
            Return CType(ConfigurationManager.AppSettings("AppointmentDurationHours"), Double)
        End Function
#End Region

#Region "Get Today Start Hour"

        Public Shared Function getTodayStartHour(ByVal startDate As Date) As Double
            'if both date are same then add two hours in it
            If DateTime.Compare(startDate.Date, Date.Today) = 0 Then
                startDate = DateTime.Now.AddHours(2)
            Else
                ' Return CType(ConfigurationManager.AppSettings("OfficialDayStartHour"), Double)
                Return Math.Floor(CType(startDate.ToString("HH"), Double) + (CType(startDate.ToString("mm"), Double) / 60))
                'startDate = GeneralHelper.getDateTimeFromDateAndNumber(startDate, 0)
                'startDate.AddHours(GeneralHelper.getOfficialDayStartHour())
            End If
            Return startDate.Hour

            ' Return Math.Floor(CType(startDate.ToString("HH"), Double) + (CType(startDate.ToString("mm"), Double) / 60))
        End Function

#End Region

#Region "get Unique Keys"
        Public Shared Function getUniqueKey(ByVal uniqueCounter As Integer) As String
            'this 'll be used to create the unique index of dictionary
            Dim uniqueKey As String = uniqueCounter.ToString() + SessionManager.getUserEmployeeId().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssffff")
            Return uniqueKey
        End Function
#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalMinutes
            Return timeInSec
        End Function
#End Region

#Region "Convert Date to Us Culture"
        ''' <summary>
        ''' This fucntion 'll convert the date in following format e.g Mon 12th Nov 2012
        ''' </summary>
        ''' <param name="dateToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUsCulture(ByVal dateToConvert As Date) As String
            Return CType(dateToConvert, Date).ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"))
        End Function

#End Region

#Region "Convert Time To 24 Hrs Format"
        ''' <summary>
        ''' This funciton will convert the datetime in 24 hours format
        ''' </summary>
        ''' <param name="timeToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertTimeTo24hrsFormat(ByVal timeToConvert As Date) As String
            Return timeToConvert.ToString("HH:mm")
        End Function
#End Region

#Region "repalce Rc RB Td"
        Public Shared Function repalceRcRBTd(ByRef letterBodyValue As String, ByRef rentBalance As Double, ByRef rentCharge As Double, ByRef todayDate As Date)
            letterBodyValue = letterBodyValue.Replace("[RB]", rentBalance)
            letterBodyValue = letterBodyValue.Replace("[RC]", rentCharge)
            letterBodyValue = letterBodyValue.Replace("[TD]", todayDate.ToShortDateString())
            Return letterBodyValue
        End Function
#End Region

#Region "Convert UTF8 String to ASCII String"

        Public Shared Function UTF8toASCII(ByVal text As String) As String
            Dim utf8 As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim encodedBytes As [Byte]() = utf8.GetBytes(text)
            Dim convertedBytes As [Byte]() = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, encodedBytes)
            Dim ascii As System.Text.Encoding = System.Text.Encoding.ASCII

            Return ascii.GetString(convertedBytes)
        End Function

#End Region

#Region "Get 12 Hour Time Format From Date"
        ''' <summary>
        ''' This funciton 'll extract the time(in 12 hours Format) from date and return that
        ''' </summary>
        ''' <param name="aDate">date with time in 12 hours format</param>
        ''' <returns>time in 12 hours format</returns>
        ''' <remarks></remarks>
        Public Shared Function get12HourTimeFormatFromDate(ByVal aDate As Date) As Date
            Return CType(aDate.ToString("HH") + ":" + aDate.ToString("mm"), Date)
        End Function
#End Region

#Region "Get 24 Hour Time Format From Date"
        ''' <summary>
        ''' This funciton 'll extract the time(in 24 hours Format) from date and return that
        ''' </summary>
        ''' <param name="aDate">date with time in 24 hours format</param>
        ''' <returns>time in 12 hours format</returns>
        ''' <remarks></remarks>
        Public Shared Function get24HourTimeFormatFromDate(ByVal aDate As Date)
            Return aDate.ToString("HH:mm")
        End Function
#End Region

#Region "Get Date with weekday format"
        ''' <summary>
        ''' Get Date with weekday format e.g "Tuesday 13th December 2013"
        ''' </summary>
        ''' <param name="aDate">date time</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateWithWeekdayFormat(ByVal aDate As DateTime)
            Dim startDay As String = aDate.DayOfWeek.ToString()
            Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(aDate.ToString("dd")))
            Dim startMonth As String = aDate.ToString("MMMMMMMMMMMMM")
            Dim startYear As String = aDate.ToString("yyyy")
            Return startDay + " " + startDate + " " + startMonth + " " + startYear
        End Function
#End Region

#Region "Set Grid View Pager"

#End Region

#Region "Get Hours From Time"
        ''' <summary>
        ''' This funciton 'll extract the hours from time and return that
        ''' </summary>
        ''' <param name="aTime">time </param>
        ''' <returns>number as double</returns>
        ''' <remarks></remarks>
        Public Shared Function getHoursFromTime(ByVal aTime As Date) As Double
            Return CType(aTime.ToString("HH"), Double) + (CType(aTime.ToString("mm"), Double) / 60)
        End Function

#End Region

#Region "Get Date Time From Date And Number"
        ''' <summary>
        ''' This function 'll append the date with number to get date time and return it
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateTimeFromDateAndNumber(ByVal aDate As Date, ByVal aHour As Double) As Date
            Return CType(aDate.Date.ToLongDateString() + " " + aHour.ToString() + ":00", Date)
        End Function
#End Region

#Region "Get Today Start Hour"

        Public Shared Function getMaximunLookAhead() As Double
            Return CType(ConfigurationManager.AppSettings("MaximunLookAhead"), Double)
        End Function

#End Region

#Region "Combine Date and Time"
        ''' <summary>
        ''' this function 'll append the time with date and returns date and time
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <param name="aTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function combineDateAndTime(ByVal aDate As Date, ByVal aTime As String)
            Return CType(aDate.ToLongDateString() + " " + aTime, Date)
        End Function
#End Region

#Region " Unix time stamp is seconds past epoch"
        Public Shared Function unixTimeStampToDateTime(unixTimeStamp As Double) As DateTime
            ' Unix time stamp is seconds past epoch
            Dim dtDateTime As System.DateTime = New DateTime(1970, 1, 1, 0, 0, 0, _
             0, System.DateTimeKind.Utc)
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp)
            Return dtDateTime
        End Function
#End Region

#Region "convert Date Time In to Date Time String"
        ''' <summary>
        ''' This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToCustomString(ByVal aDate As Date)
            Return (aDate.ToString("HH:mm dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")))
        End Function
#End Region

#Region "Convert Date Time to UK Date format."
        ''' <summary>
        ''' This function 'll return the date in the following format in string (dd/mm/yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUkDateFormat(ByVal aDate As String)
            Dim format As String = "dd/MM/yyyy"
            Return DateTime.ParseExact(aDate, format, CultureInfo.CreateSpecificCulture("en-UK"))
        End Function
#End Region

#Region "get Hours And Minute String"
        ''' <summary>
        ''' This function 'll convert the date into following format : (HH:mm)
        ''' </summary>
        ''' <param name="aDateTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getHrsAndMinString(ByVal aDateTime As DateTime)
            Return aDateTime.ToString("HH:mm")
        End Function
#End Region

#Region "set Grid View Pager"
        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)

            pnlPagination.Visible = False
            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

                'Set Range validation error Message and Maximum Value (Minimum value is 1).
                Dim rangevalidatorPageNumber As RangeValidator = pnlPagination.FindControl("rangevalidatorPageNumber")
                rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString()
                rangevalidatorPageNumber.MaximumValue = totalPages
            End If
        End Sub
#End Region

#Region "get Appointment Creation Limit For Single Request"
        ''' <summary>
        ''' This function 'll return the appointment creation limit for single request
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentCreationLimitForSingleRequest()
            Return 5
        End Function

#End Region

#Region "get Misc Appointment Creation Limit For Single Request"
        ''' <summary>
        ''' This function 'll return the misc appointment creation limit for single request
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getMiscAppointmentCreationLimitForSingleRequest()
            Return 20
        End Function

#End Region

#Region "append Day Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendDayLabel(ByVal aNumber As Double)
            If aNumber > 1 Then
                Return aNumber.ToString() + " days"
            Else
                Return aNumber.ToString() + " day"
            End If

        End Function
#End Region

#Region "append Hour Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendHourLabel(ByVal aNumber As Double)
            If aNumber > 1 Then
                Return aNumber.ToString() + " hours"
            Else
                Return aNumber.ToString() + " hour"
            End If

        End Function
#End Region

#Region "populate Hours in an Hour Duration"
        ''' <summary>
        ''' This function will populate the drop down with hours in an Hour Duration
        ''' </summary>
        ''' <param name="ddl">Drop down to populate (ByRef)</param>
        ''' <param name="startNumber">Optional: Start Number it is default to 1.</param>
        ''' <param name="endNumber">Optional: End Number it is default to 100.</param>
        ''' <remarks></remarks>
        Public Shared Sub populateHoursInAnHourDuration(ByRef ddl As DropDownList, Optional ByVal startNumber As Integer = 1, Optional ByVal endNumber As Integer = 100)
            For i As Integer = startNumber To endNumber
                ddl.Items.Add(New ListItem((i).ToString(), (i).ToString()))
            Next i
        End Sub
#End Region

#Region "populate Official Hours With Fifteen Min Duration"
        ''' <summary>
        ''' this function 'll populate the drop down with official hours with fifteen minute Duration
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub populateOfficialHoursWithFifteenMinDuration(ByRef ddl As DropDownList)
            Dim startHour As Double = GeneralHelper.getOfficialDayStartHour()
            Dim endHour As Double = GeneralHelper.getOfficialDayEndHour()
            For i As Double = startHour To endHour

                ddl.Items.Add(New ListItem(i.ToString() + ":00", i.ToString() + ":00"))
                If i <> endHour Then
                    ddl.Items.Add(New ListItem(i.ToString() + ":15", i.ToString() + ":15"))
                    ddl.Items.Add(New ListItem(i.ToString() + ":30", i.ToString() + ":30"))
                    ddl.Items.Add(New ListItem(i.ToString() + ":45", i.ToString() + ":45"))
                End If

            Next i
        End Sub
#End Region

#Region "Get Ordinal"

        ''' <summary>
        ''' Get Ordinal
        ''' </summary>
        ''' <param name="number"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getOrdinal(number As Integer) As String
            Dim suffix As String = [String].Empty

            Dim ones As Integer = number Mod 10
            Dim tens As Integer = CInt(Math.Floor(number / 10D)) Mod 10

            If tens = 1 Then
                suffix = "th"
            Else
                Select Case ones
                    Case 1
                        suffix = "st"
                        Exit Select

                    Case 2
                        suffix = "nd"
                        Exit Select

                    Case 3
                        suffix = "rd"
                        Exit Select
                    Case Else

                        suffix = "th"
                        Exit Select
                End Select
            End If
            Return [String].Format("{0}{1}", number, suffix)
        End Function

#End Region

#Region "Populate Reject SMS body"
        Public Shared Function populateBodyRejectConditionWorksSMS(ByVal address1 As String, ByVal townCity As String, ByVal username As String) As String
            Dim body As String = String.Empty
            body = ApplicationConstants.SMSRejectedmessage
            body = body.Replace("{Address 1}", address1)
            body = body.Replace("{Town/City}", townCity)
            body = body.Replace("{Username}", username)
            Return body
        End Function
#End Region

#Region "Populate Approved SMS body"
        Public Shared Function populateBodyApprovedConditionWorksSMS(ByVal address1 As String, ByVal townCity As String, ByVal username As String) As String
            Dim body As String = String.Empty
            body = ApplicationConstants.SMSApprovedmessage
            body = body.Replace("{Address 1}", address1)
            body = body.Replace("{Town/City}", townCity)
            body = body.Replace("{Username}", username)
            Return body
        End Function
#End Region

#Region "Send SMS"
        Public Shared Sub sendSMS(ByVal body As String, ByVal recepientMobile As String)
            Dim request As WebRequest = WebRequest.Create(ApplicationConstants.SMSUrl)
            request.Method = "POST"
            Dim strdata As String
            strdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(recepientMobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strdata)
            ' Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded"
            ' Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length
            ' Get the request stream.
            Dim dataStream As Stream = request.GetRequestStream()
            ' Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.
            dataStream.Close()
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            ' Display the status.
            'Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            ' Get the stream containing content returned by the server.f
            dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access.
            Dim reader As New StreamReader(dataStream) ' Read the content.
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Display the content.
            'Console.WriteLine(responseFromServer)
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
        End Sub
#End Region
#Region "Send SMS UPDATED API"
        Public Shared Sub sendSmsUpdatedAPI(ByVal body As String, ByVal recepientMobile As String)
            Dim result As String
            Dim apiKey As String = "resw5bAZdIs-fnUwznzIfzJM66WfRdWSQRXlFtR7V2"
            'string apiKey = "nVv8yYd3mYo-1UXMeTpnYJ4pXqspvYPrgC7HTsYH3s"; //rehan
            Dim username As String = "digitalengagementteam@broadlandgroup.org"
            Dim password As String = "Broadland@1"
            Dim numbers As String = recepientMobile ' in a comma seperated list
            Dim message As String = body
            Dim sender As String = " "

            Dim url As String = "https://api.txtlocal.com/send/?apiKey=" & apiKey & "&numbers=" & numbers & "&message=" & message & "&sender=" & sender
            'refer to parameters to complete correct url string

            Dim myWriter As StreamWriter = Nothing
            Dim objRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)

            objRequest.Method = "POST"
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(url)
            objRequest.ContentType = "application/x-www-form-urlencoded"
            Try
                myWriter = New StreamWriter(objRequest.GetRequestStream())
                myWriter.Write(url)
            Catch e As Exception
                'return e.Message;
            Finally
                myWriter.Close()
            End Try

            Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
            Using sr As New StreamReader(objResponse.GetResponseStream())
                result = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using

        End Sub
#End Region

#Region "Get Push Notification Application Address"
        Public Shared Function getPushNotificationAddress(ByVal absoluteUri As String) As String
            Dim servername As String = absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, Len(absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, absoluteUri.Substring(absoluteUri.IndexOf("//") + 2).IndexOf("/"))))
            Return "https://" + servername + "/StockConditionOfflineAPI/push/NotificationApplianceAndPlanned"
        End Function
#End Region

#Region " Send Push Notifications to Planned "

        Public Shared Function pushNotificationPlanned(ByVal appointmentType As String, ByVal appointmentStartDateTime As DateTime, ByVal appointmentEndDateTime As DateTime,
                                                       ByVal propertyAddress As String, ByVal operatorId As Integer) As Boolean
            Try
                Dim appointmentTypeDesc As String = String.Empty
                Dim appointmentMessage As String = String.Empty

                If (appointmentType.ToLower() = "new") Then ''''''' Done
                    appointmentTypeDesc = "Planned Work Scheduled"
                ElseIf (appointmentType.ToLower() = "misc") Then ''''''' Done
                    appointmentTypeDesc = "Misc Work Scheduled"
                ElseIf (appointmentType.ToLower() = "adaptation") Then ''''''' Done
                    appointmentTypeDesc = "Aids & Adaptations Work Scheduled"
                ElseIf (appointmentType.ToLower() = "conditionrating") Then ''''''' Done
                    appointmentTypeDesc = "Condition Rating Work Scheduled"
                ElseIf (appointmentType.ToLower() = "preworksinspection") Then
                    appointmentTypeDesc = "Pre-works Inspection Scheduled"
                ElseIf (appointmentType.ToLower() = "cancelled") Then ''''''' Done
                    appointmentTypeDesc = "Appointment Cancelled"
                End If

                appointmentMessage += "BHG Property Manager" + " \n\n "
                appointmentMessage += appointmentTypeDesc + " \n\n "
                appointmentMessage += appointmentStartDateTime.ToString("dd MMM yyyy HH:mm") + " "
                appointmentMessage += appointmentEndDateTime.ToString("dd MMM yyyy HH:mm") + " to "
                appointmentMessage += propertyAddress

                appointmentMessage = HttpUtility.UrlEncode(appointmentMessage)
                'Server.UrlEncode(appointmentMessage)

                Dim URL As String = String.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId)
                Dim request As WebRequest = Net.WebRequest.Create(URL)
                request.Credentials = CredentialCache.DefaultCredentials
                Dim response As WebResponse = request.GetResponse()
                Dim dataStream As Stream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)
                Dim responseFromServer As String = reader.ReadToEnd()
                reader.Close()
                response.Close()

                If (responseFromServer.ToString.Equals("true")) Then
                    Return True
                End If

                Return False

            Catch ex As Exception
                Return False
            End Try

        End Function

#End Region



#Region "Get Page Menu"

        Public Shared Function getPageMenu(ByVal page As String) As String

            Dim pageDict As Dictionary(Of String, String) = New Dictionary(Of String, String)
            pageDict.Add(PathConstants.MiscWorksPage, ApplicationConstants.MaintenanceMenu)
            pageDict.Add(PathConstants.MiscJobSheetPage, ApplicationConstants.MaintenanceMenu)
            Dim menuName As String = ApplicationConstants.PlannedMenu
            If (pageDict.ContainsKey(page)) Then
                menuName = pageDict.Item(page)
            End If
            Return menuName

        End Function

#End Region

    End Class


End Namespace
