﻿Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Web

Public Class ExcelHelper
    'Row limits older excel verion per sheet, the row limit for excel 2003 is 65536
    Const rowLimit As Integer = 65000

#Region "Get Workbook Template"
    ''' <summary>
    ''' Get Workbook Template
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getWorkbookTemplate() As String
        Dim sb = New StringBuilder(818)
        sb.AppendFormat("<?xml version=""1.0""?>{0}", Environment.NewLine)
        sb.AppendFormat("<?mso-application progid=""Excel.Sheet""?>{0}", Environment.NewLine)
        sb.AppendFormat("<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""{0}", Environment.NewLine)
        sb.AppendFormat(" xmlns:o=""urn:schemas-microsoft-com:office:office""{0}", Environment.NewLine)
        sb.AppendFormat(" xmlns:x=""urn:schemas-microsoft-com:office:excel""{0}", Environment.NewLine)
        sb.AppendFormat(" xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""{0}", Environment.NewLine)
        sb.AppendFormat(" xmlns:html=""http://www.w3.org/TR/REC-html40"">{0}", Environment.NewLine)
        sb.AppendFormat(" <Styles>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""Default"" ss:Name=""Normal"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Alignment ss:Vertical=""Top""/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Borders >{0}", Environment.NewLine)
        sb.AppendFormat("       <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("       <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("       <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("       <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Borders>{0}", Environment.NewLine)
        sb.AppendFormat("   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Interior/>{0}", Environment.NewLine)
        sb.AppendFormat("   <NumberFormat/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Protection/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s62"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""{0}", Environment.NewLine)
        sb.AppendFormat("    ss:Bold=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s63"">{0}", Environment.NewLine)
        sb.AppendFormat("   <NumberFormat ss:Format=""Short Date""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s64"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Alignment ss:Horizontal=""Right""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s65"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Alignment ss:Horizontal=""Right""/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""{0}", Environment.NewLine)
        sb.AppendFormat("    ss:Bold=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s66"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Interior ss:Color=""#F2F2F2"" ss:Pattern=""Solid""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s67"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Alignment ss:Horizontal=""Right""/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Interior ss:Color=""#F2F2F2"" ss:Pattern=""Solid""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat("  <Style ss:ID=""s68"">{0}", Environment.NewLine)
        sb.AppendFormat("   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""{0}", Environment.NewLine)
        sb.AppendFormat("    ss:Bold=""1""/>{0}", Environment.NewLine)
        sb.AppendFormat("   <Interior ss:Color=""#F2F2F2"" ss:Pattern=""Solid""/>{0}", Environment.NewLine)
        sb.AppendFormat("  </Style>{0}", Environment.NewLine)
        sb.AppendFormat(" </Styles>{0}", Environment.NewLine)
        sb.Append("{0}\r\n</Workbook>")
        Return sb.ToString()
    End Function

#End Region
    
#Region "Replace Xml Char"
    ''' <summary>
    ''' Replace Xml Char
    ''' </summary>
    ''' <param name="input"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function replaceXmlChar(input As String) As String
        input = input.Replace("&", "&amp")
        input = input.Replace("<", "&lt;")
        input = input.Replace(">", "&gt;")
        input = input.Replace("""", "&quot;")
        input = input.Replace("'", "&apos;")
        Return input
    End Function
#End Region
    
#Region "Get Cell"
    ''' <summary>
    ''' Get Cell
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="cellData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getCell(type As Type, cellData As Object) As String
        Dim data = If((TypeOf cellData Is DBNull), "", cellData)
        If type.Name.Contains("Int") OrElse type.Name.Contains("Double") OrElse type.Name.Contains("Decimal") Then
            Return String.Format("<Cell><Data ss:Type=""Number"">{0}</Data></Cell>", data)
        End If
        If type.Name.Contains("Date") AndAlso data.ToString() <> String.Empty Then
            Return String.Format("<Cell ss:StyleID=""s63""><Data ss:Type=""DateTime"">{0}</Data></Cell>", Convert.ToDateTime(data).ToString("yyyy-MM-dd"))
        End If
        Return String.Format("<Cell><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(data.ToString()))
    End Function
#End Region

#Region "Get Worksheets"
    ''' <summary>
    ''' Get Worksheets
    ''' </summary>
    ''' <param name="source"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getWorksheets(source As DataSet) As String
        Dim sw = New StringWriter()
        If source Is Nothing OrElse source.Tables.Count = 0 Then
            sw.Write("<Worksheet ss:Name=""Sheet1"">" & vbCr & vbLf & "<Table>" & vbCr & vbLf & "<Row><Cell><Data ss:Type=""String""></Data></Cell></Row>" & vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")
            Return sw.ToString()
        End If
        For Each dt As DataTable In source.Tables
            If dt.Rows.Count = 0 Then
                sw.Write("<Worksheet ss:Name=""" & replaceXmlChar(dt.TableName) & """>" & vbCr & vbLf & "<Table>" & vbCr & vbLf & "<Row><Cell  ss:StyleID=""s62""><Data ss:Type=""String""></Data></Cell></Row>" & vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")
            Else
                'write each row data                
                Dim sheetCount = 0
                For i As Integer = 0 To dt.Rows.Count - 1
                    If (i Mod rowLimit) = 0 Then
                        'add close tags for previous sheet of the same data table
                        If (i \ rowLimit) > sheetCount Then
                            sw.Write(vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")
                            sheetCount = (i \ rowLimit)
                        End If
                        sw.Write(vbCr & vbLf & "<Worksheet ss:Name=""" & replaceXmlChar(dt.TableName) & (If(((i \ rowLimit) = 0), "", Convert.ToString(i \ rowLimit))) & """>" & vbCr & vbLf & "<Table>")
                        'write column name row
                        sw.Write(vbCr & vbLf & "<Row>")
                        For Each dc As DataColumn In dt.Columns
                            sw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(dc.ColumnName)))
                        Next
                        sw.Write("</Row>")
                    End If
                    sw.Write(vbCr & vbLf & "<Row>")
                    For Each dc As DataColumn In dt.Columns
                        sw.Write(getCell(dc.DataType, dt.Rows(i)(dc.ColumnName)))
                    Next
                    sw.Write("</Row>")
                Next
                sw.Write(vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")
            End If
        Next

        Return sw.ToString()
    End Function

#End Region

#Region "Get Excel Xml from Datatable"
    ''' <summary>
    ''' Get Excel Xml
    ''' </summary>
    ''' <param name="dtInput"></param>
    ''' <param name="filename"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetExcelXml(dtInput As DataTable, filename As String) As String
        Dim excelTemplate = getWorkbookTemplate()
        Dim ds = New DataSet()
        ds.Tables.Add(dtInput.Copy())
        Dim worksheets = getWorksheets(ds)
        Dim excelXml = String.Format(excelTemplate, worksheets)
        Return excelXml
    End Function

#End Region

#Region "Get Excel Xml from Dataset"
    ''' <summary>
    ''' Get Excel Xml
    ''' </summary>
    ''' <param name="dsInput"></param>
    ''' <param name="filename"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetExcelXml(dsInput As DataSet, filename As String) As String
        Dim excelTemplate = getWorkbookTemplate()
        Dim worksheets = getWorksheets(dsInput)
        Dim excelXml = String.Format(excelTemplate, worksheets)
        Return excelXml
    End Function
#End Region

#Region "Dataset To Excel"
    ''' <summary>
    ''' To Excel
    ''' </summary>
    ''' <param name="dsInput"></param>
    ''' <param name="filename"></param>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Shared Sub ToExcel(dsInput As DataSet, filename As String, response As HttpResponse)
        Dim excelXml = GetExcelXml(dsInput, filename)
        response.Clear()
        response.AppendHeader("Content-Type", "application/vnd.ms-excel")
        response.AppendHeader("Content-disposition", "attachment; filename=" & filename)
        response.Write(excelXml)
        response.Flush()
        response.[End]()
    End Sub
#End Region

#Region "Data table To Excel"
    ''' <summary>
    ''' To Excel
    ''' </summary>
    ''' <param name="dtInput"></param>
    ''' <param name="filename"></param>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Shared Sub ToExcel(dtInput As DataTable, filename As String, response As HttpResponse)
        Dim ds = New DataSet()
        ds.Tables.Add(dtInput.Copy())
        ToExcel(ds, filename, response)
    End Sub
#End Region

#Region "Worksheets To Excel"
    ''' <summary>
    ''' To Excel
    ''' </summary>
    ''' <param name="filename"></param>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Shared Sub ToExcel(filename As String, response As HttpResponse, ByVal worksheets As String)
        Dim excelTemplate = getWorkbookTemplate()
        Dim excelXml = String.Format(excelTemplate, worksheets)
        response.Clear()
        response.AppendHeader("Content-Type", "application/vnd.ms-excel")
        response.AppendHeader("Content-disposition", "attachment; filename=" & filename + ".xls")
        response.Write(excelXml)
        response.Flush()
        response.[End]()

        'response.Clear()
        'response.Buffer = True
        'response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        'response.Charset = ""
        'response.ContentType = "application/vnd.ms-excel"

        ''response.Write(excelXml)
        'response.Output.Write(excelXml)
        'response.Flush()
        'response.End()


    End Sub
#End Region

End Class
