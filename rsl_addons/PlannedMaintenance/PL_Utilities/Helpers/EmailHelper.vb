﻿Imports System.Net.Mail

Namespace PL_Utilities

    Public Class EmailHelper

        Public Shared Sub sendHtmlFormattedEmail(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub
        Public Shared Sub sendHtmlFormattedEmailForConditionWorksApproved(ByVal recepientName As String, ByVal replyTo As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.ReplyToList.Add(replyTo)
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

        Public Shared Sub sendHtmlFormattedEmailWithAttachment(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String, ByVal filePath As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))


            Dim iCalAttachement As Attachment = New Attachment(filePath)
            mailMessage.Attachments.Add(iCalAttachement)
            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

#Region "sendEmail - Simple send email function using mailMessage"

        ''' <summary>
        ''' A simple send Email Function the mail message settings are handled on the caller function side.
        ''' </summary>
        ''' <param name="mailMessage"></param>
        ''' <remarks></remarks>
        Public Shared Sub sendEmail(ByRef mailMessage As MailMessage)

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)
        End Sub

#End Region

    End Class

End Namespace


