﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Collections
Imports System.Web.Caching

Namespace PL_Utilities
    Public Class PL_Validation


        Public Shared Function emptyString(ByVal value As String) As Boolean
            If value.Equals(String.Empty) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function lengthString(ByVal value As String, ByVal length As Integer) As Boolean
            If value.Length > length Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function isNumber(value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.numbersExp)
            If Not rgx.IsMatch(value) Then
                Return False
            Else
                Return True
            End If
        End Function

        'Changes By Aamir Waheed - 08 May 2013 - Start
        Public Shared Function isEmail(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.emailExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function
        'Changes By Aamir Waheed - 08 May 2013 - End

        Public Shared Function isMobile(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.mobileExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class
End Namespace
