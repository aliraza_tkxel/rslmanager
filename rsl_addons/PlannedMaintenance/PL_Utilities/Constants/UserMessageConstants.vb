﻿
Namespace PL_Utilities
    Public Class UserMessageConstants

#Region "not categorized"
        Public Shared PushNotificationFailed As String = "Appointment confirmed but failed to push appointment notification."
#End Region

#Region "Generic Messages"

        Public Const CompletedDate As String = "Completed date can't be greater than today."
        Public Const CompletedDateOrderedDate As String = "Completed date can't be smaller than Ordered Date."
        Public Const InvalidPageNumber As String = "Invalid Page Number."
        Public Const RecordNotFound As String = "Record not found."
        Public Const RecordNotExist As String = "No record exists"
        Public Const RecordNotSelected As String = "Please select the record."
        Public Const RecordExceed As String = "Please apply filter to load more results."
        Public Shared DistanceError As String = "Unable to get distance."
        Public Const INVALIDREQUEST As String = "Message from Google: The provided request was invalid."
        Public Const MAXELEMENTSEXCEEDED As String = "Message from Google: The product of origins and destinations exceeds the per-query limit."
        Public Const OVERQUERYLIMIT As String = "Message from Google: The service has received too many requests from your application within the allowed time period."
        Public Const REQUESTDENIED As String = "Message from Google: The service denied use of the Distance Matrix service by your application."
        Public Const UNKNOWNERROR As String = "Message from Google: The distance Matrix request could not be processed due to a server error. The request may succeed if you try again."
        Public Const NoRecordFound As String = "No record found."
        Public Const TradeAlreadyAdded = "The Operative has already been added."

        Public Const UserDoesNotExist As String = "You don't have access to Planned Maintenance. Please contact administrator."
        Public Const UsersAccountDeactivated As String = "Your access to Planned Maintenance has been de-activiated. Please contact administrator."

        Public Const SuccessfulSchedule As String = "Successfully approved."
        Public Const CompletedArrangedStatusNotSchedule As String = "Properties with appointment status Completed OR Arranged OR To be Arranged can not be scheduled."
        Public Const InvalidPropertyId As String = "Invalid PropertyId."
        Public Const ComponentTradeDeletionFailed As String = "Selected component trade cannot be deleted because it is referenced in another table."
        Public Const TenantNotExist As String = "Tenant do not exist against this property."

        Public Const ErrorDocumentUpload As String = "Error occurred while uploading the document.Please try again."
        Public Const InspectionArrangedSuccessfully As String = "Inspection arranged successfully."

        Public Const SelectOperative As String = "Please select operative."
        Public Const SelectAction As String = "Please select Action."
        Public Const SelectStartTime As String = "Please select Start time."
        Public Const SelectEndTime As String = "Please select End time."
        Public Const SelectTrade As String = "Please select Trade."
        Public Const SelectInspectionDate As String = "Please select Inspection date."
        Public Const SelectFutureDate As String = "Please select future date."
        Public Const SelectLocationAdaptation As String = "Please select Location/Adaptation."
        Public Const AppointmentSlotNotAvailable As String = "The selected surveyor is not available at the selected time, select a different appointment time or surveyor."
        Public Const SelectDropdowns As String = "Please select drop downs."
        Public Const SelectStartEndTimeError As String = "Start Time should be smaller than End Time."

        Public Const EnterWorksRequired As String = "Please enter works required description."
        Public Const SelectDuration As String = "Please select duration."
        Public Const InvalidDateFormat As String = "Invalid date. Please enter date in this format dd/MM/yyyy."
        Public Const SelectProperty As String = "Please select property."

        Public Const InspectionArrangedEmailError As String = "Inspection saved but unable to send email due to : "
        Public Const ProblemLoadingData As String = "Problem loading data."
        Public Const InvalidDueDate As String = "Please enter valid due date for Ref:"
        Public Const InvalidJobSheetIndex As String = "Invalid Job Sheet Index."
        Public Const MaxLengthExceed As String = "Please reduce length of notes. Maximum 1000 characters are allowed."

#End Region

#Region "File / Directory Path"
        Public Const DirectoryDoesNotExist As String = "This path {0} does not exist on hard drive. Please create this path on drive."
        Public Const DocumentUploadPathKeyDoesNotExist As String = "Document upload path key does not exist in web.config. Please insert the document upload path key in web.config under app settings"
        Public Const FileDoesNotExist As String = "This file {0} does not exist on hard drive."
        Public Const InvalidDocumentType As String = "Invlaid document type."
        Public Const InvalidImageType As String = "Invlaid image type."
        Public Const SelectDocument As String = "Please select the document"
        Public Const FileUploadedSuccessfully As String = "File is uploaded successfully."
#End Region

#Region "Add Activity Popup"
        Public Const SelectActionDate As String = "Please select date."
        Public Const SelectActionType As String = "Please select type."
        Public Const SelectActionStatus As String = "Please select status."
        Public Const SelectActionLetter As String = "Please select letter."
        Public Const SelectActionNotes As String = "Please enter notes."
        Public Const SelectActionLetterDoc As String = "Please add letter or upload any document in the list."
        Public Const SaveActvitySuccessfully As String = "Activity is saved successfully."
        Public Const LetterAlreadyExist As String = "You have already added this letter."
        Public Const duplicateAppointmentTime As String = "Another appointment already exist in selected Start/End time."
#End Region

#Region "Letters"
        Public Const NoLettersFound As String = "No letter found."
        Public Const NoDocumentsFound As String = "No document found."
        Public Const SelectLetter As String = "Please select the letter."

        Public Const StandardLetterCreated As String = "Standard Letter template successfully created."
        Public Const StandardLetterUpdated As String = "Standard Letter template successfully updated."
        Public Const StandardLetterDeleted As String = "Standard Letter template successfully deleted."
        Public Const StandardLetterNotCreated As String = "Unable to create Standard Letter template."
        Public Const StandardLetterNoStatus As String = "Please select the status of the Standard Letter template."
        Public Const StandardLetterNoAction As String = "Please select the action of the Standard Letter template."
        Public Const StandardLetterNoTitle As String = "Please enter the title of the Standard Letter template."
        Public Const StandardLetterNoCode As String = "Please enter the code of the Standard Letter template."
        Public Const StandardLetterCodeNotNumeric As String = "The Letter code shall only be numeric."
        Public Const StandardLetterNoBody As String = "Please enter the body of the Standard Letter template."

        Public Const SavedLetterNoLetterId As String = "Error. Could not find letter."
        Public Const SavedLetterNoBody As String = "Please enter the body of the letter."
        Public Const SavedLetterNoTitle As String = "Please enter the title of the letter."
        Public Const SavedLetterNoSignOFfLookupCode As String = "Please select the Sign Off."
        Public Const SavedLetterNoTeamId As String = "Please select your team."
        Public Const SavedLetterNoFromResourceId As String = "Please select the person who be this letter's sender."
        Public Const SavedLetterSuccess As String = "Letter saved successfully."
        Public Const SavedLetterFailure As String = "An error occured. Letter was not saved."
        Public Const InvalidLetterId As String = "Letter Id is invalid."
#End Region

#Region "Status And Actions"
        Public Const ActionSavedSuccessfuly As String = "Action saved successfully"
        Public Const ActionSavedError As String = "Error has been occured while saving the action in database. Please refresh the page and try again or contact system administrator."
        Public Const StatusSavedSuccessfuly As String = "Status saved successfully"
        Public Const StatusSavedError As String = "Error has been occured while saving the status in database. Please refresh the page and try again or contact system administrator."
        Public Const SaveRightsSuccessfuly As String = "Rights saved successfully"
        Public Const StatusUpdatedSuccessfuly As String = "Status updated successfully"
        Public Const EditActionSuccessful As String = "Action edited successfully"
        Public Const statusLength As String = "Status length is too large"
        Public Const statusTitle As String = "Status title is required"
        Public Const ActionError As String = "you can add action only under status"
#End Region

#Region "Email"

        Public Const InvalidEmail As String = "Invalid Email address."
        Public Const InspectionSavedEmailError As String = "Inspection arranged but unable to send email due to : "

#End Region

#Region "Life Cycle"
        Public Const StartDateInPast As String = "Start Date should be in future"
#End Region

#Region "Scheduling"
        Public Const ComponentTradeDoesNotExist As String = "Trade does not exist against this component. Please add trade against this component."
        Public Const OperativesNoFound As String = "Operatives are not found against this component trade. Please select the different date or add operatives against these trades"
        Public Const SaveAppointmentSuccessfully As String = "Appointment is saved successfully."
        Public Const SaveAppointmentFailed As String = "Some error occurred in saving appointments"
        Public Const AppointmentConfirmationFailed As String = "Some error occurred in confirming appointments"
        Public Const RearrangeAppointmentFailed As String = "Some error occurred in Rearrange appointments"
        Public Const InvalidPmo As String = "Invalid PMO number."
        Public Const InvalidJsn As String = "Invalid Job Sheet Number."
        Public Const AppointmentSavedEmailError As String = "Appointment saved but unable to send email due to : "
        Public Const NoOperativesExistWithInTime As String = "There are currently no operatives available to attend this."
        Public Const SelectedDateIsInPast As String = "Selected date is in past. This should be today's date or date in future."
        Public Const InvalidJobSheetAndPmoQueryString As String = "Invalid Job Sheet and PMO in query string."
        Public Const NotesUpdatedSuccessfully As String = "Notes updated successfully."
        Public Const ProblemUpdatingNotes As String = "Problem updating notes."
        Public Const EnterReason As String = "Please enter reason for cancellation."
        Public Const AppointmentCancelledSuccessfully As String = "Appointment cancelled successfully."
        Public Const AppointmentCancelledFailed As String = "Appointment cancellation has been failed. Please try again."
        Public Const UnableToRearrangeTheAppointment As String = "System is unable to rearrange the appointment. Please refresh the page & try again."
        Public Const UnableToRemoveTrade As String = "System is unable to remove the trade. Please refresh the page & try again."
        Public Const UnableToScheduleTheAppointment As String = "System is unable to schedule the appointment(s). Please refresh the page & try again."
        Public Const AppoinemtsScheduledSuccessfully As String = "Appointment has been scheduled against all trades"

        Public Const MiscTradDuration As String = "Please add at least one trade/duration."
#End Region

#Region "Customer"
        Public Const InvalidTelephone As String = "Invalid Telephone number."
        Public Const InvalidMobile As String = "Invalid Mobile number."
#End Region

#Region "Users"
        Public Const UserSavedSuccessfuly As String = "User Saved Successfuly"
        Public Const userAlreadyExists As String = "User already exists"
        Public Const UserNameNotValid As String = "User name is not valid"
        Public Const ErrorUserUpdate As String = "Error Updating Record"
        Public Const ErrorUserInsert As String = "Error Inserting Record"
        Public Const ErrorNoTradeExist As String = "No Trade Exist Against This Fault"

#End Region

#Region "Reports"
        Public Const SuccessfullySaved As String = "Successfully saved."
        Public Const FailedSave As String = "Problem occurred while saving data."
        Public Const ConditionWorkApproved As String = "Condition works has been successfully approved."
        Public Const ConditionWorkFailedToApprove As String = "Failed to approved condition works."
        Public Const ConditionWorkApprovedFailedSms As String = "Condition works has been successfully approved but failed to send SMS."
        Public Const ConditionWorkApprovedFailedEmail As String = "Condition works has been successfully approved but failed to send Email."
        Public Const ConditionWorkRejected As String = "Condition works has been successfully rejected."
        Public Const ConditionWorkRejectedFailedSms As String = "Condition works has been successfully rejected but failed to send SMS."
        Public Const ConditionWorkRejectedFailedEmail As String = "Condition works has been successfully rejected but failed to send Email."
        Public Const ConditionWorkRejectedFailedSmsEmail As String = "Condition works has been successfully rejected but failed to send SMS and Email."
        Public Const ConditionWorkFailedToRejected As String = "Failed to reject condition works."
#End Region

#Region "Assign to Contractor"

        Public Const workRequiredCount = "Please add at least one work required."
        Public Const AssignedToContractor = "Work successfully assigned to Contractor."
        Public Const AssignedToContractorFailed = "Assign work to contractor has not successful."
        Public Const EmailToContractor = "Email not sent to supplier."


#End Region

#Region "Assign Misc Work to Contractor"

        Public Const SelectaProperty = "Please select a property."

#End Region

#Region " Job Sheet Summary "

        Public Shared PopupHeaderSaveNotesCancelAppointment As String = "Cancel Appointment"
        Public Shared PopupSaveCancelAppointment As String = "Are you sure you want to Cancel the appointment?"
        Public Shared InvalidJobSheetNumber As String = "Invalid Job Sheet Number"

#End Region

    End Class
End Namespace


