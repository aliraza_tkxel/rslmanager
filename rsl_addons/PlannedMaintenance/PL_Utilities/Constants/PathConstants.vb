﻿Namespace PL_Utilities
    Public Class PathConstants

#Region "URL Constants"

        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const BridgePath As String = "~/Bridge.aspx"
        Public Const ComponentsHighlight As String = "Components.aspx"
        Public Const ReplacementListHighlight As String = "ReplacementList.aspx"
        Public Const ScheduleWorksHighlight As String = "ScheduleWorks.aspx"
        Public Const MiscWorksHighlight As String = "MiscWorks.aspx"
        Public Const MiscJobSheetHighlight As String = "MiscJobSheet.aspx"
        Public Const AnnualSpendReportHighlight As String = "AnnualSpendPlanner.aspx"
        Public Const AssignedToContractor As String = "AssignedToContractor.aspx"
        Public Const adHocWorksPath As String = "~/Views/Scheduling/MiscWorks.aspx"

        Public Const ReplacementListPath As String = "~/Views/Lifecycle/ReplacementList.aspx"
        Public Const ComponentsPath As String = "~/Views/Lifecycle/Components.aspx"
        Public Const DashboardPath As String = "~/Views/Dashboard/Dashboard.aspx"
        Public Const ScheduleConditionWorksPath As String = "~/Views/Scheduling/ScheduleConditionWorks.aspx"
        Public Const ConditionReportPath As String = "~/Views/Reports/ConditionRating.aspx"
        Public Const ConditionMoreDetailPath As String = "~/Views/Reports/ConditionMoreDetail.aspx"
        Public Const ICalFilePath As String = "~/Attachment/InspectionArrange.ics"
        Public Const PreviewLetter As String = "~/Views/Resources/PreviewLetter.aspx"
        Public Const StatusActionMain As String = "~/Views/Resources/StatusActionMain.aspx"
        Public Const MiscWorksPath As String = "~/Views/Scheduling/MiscWorks.aspx"
        Public Const BackToMiscWorks As String = "~/Views/Scheduling/MiscWorks.aspx?src=MiscJobSheet"
        Public Const ScheduleWorks As String = "~/Views/Scheduling/ScheduleWorks.aspx"
        Public Const MiscJobSheet As String = "~/Views/Scheduling/MiscJobSheet.aspx"
        Public Const ToBeArrangedAvailableAppointments As String = "~/Views/Scheduling/ToBeArrangedAvailableAppointments.aspx"
        Public Const ArrangedAvailableAppointments As String = "~/Views/Scheduling/ArrangedAvailableAppointments.aspx"
        Public Const AppointmentSummary As String = "~/Views/Scheduling/AppointmentSummary.aspx"
        Public Const ViewArrangedAppointments As String = "~/Views/Scheduling/ViewArrangedAppointments.aspx"
        Public Const ViewCancelledAppointments As String = "~/Views/Scheduling/ViewCancelledAppointments.aspx"
        Public Const ViewConditionArrangedAppointments As String = "~/Views/Scheduling/ViewConditionArrangedAppointments.aspx"
        Public Const AnnualSpendPlanner As String = "~/Views/Reports/AnnualSpendPlanner.aspx"
        Public Const AccessDeniedPath As String = "~/../PlannedMaintenance/Views/General/AccessDenied.aspx"
        Public Const SeparatorImagePath As String = "../Images/menu/sep.jpg"
        Public Const PropertyRecordPath As String = "~/../RSLApplianceServicing/Bridge.aspx?pid="
        Public Const ConditionalAvailableAppointments As String = "~/Views/Scheduling/ConditionAvailableAppointments.aspx"
        Public Const ConditionAppointmentSummary As String = "~/Views/Scheduling/ConditionAppointmentSummary.aspx"
        Public Const ScheduleConditionWorks As String = "~/Views/Scheduling/ScheduleConditionWorks.aspx"
        Public Const CompletedPlannedWork As String = "~/Views/Reports/CompletedWorks.aspx"
        Public Const CancelledApointmentReport As String = "~/Views/Reports/AppointmentsCancelled.aspx"
        Public Const MiscWorksPage As String = "MiscWorks.aspx"
        Public Const MiscJobSheetPage As String = "MiscJobSheet.aspx"

#End Region

#Region "Query String Constants"


        Public Const MiscWorksType As String = "miscworks"
        Public Const Replacement As String = "rep"
        Public Const Component As String = "comp"
        Public Const CurrentYear As String = "cy"
        Public Const Overdue As String = "od"
        Public Const NotSchedule As String = "ns"
        Public Const StatusId As String = "sid"
        Public Const LetterId As String = "lid"
        Public Const Src As String = "src"
        Public Const Resources As String = "resources"

        Public Const Index As String = "index"
        Public Const Page As String = "pg"

        Public Const Pmo As String = "pmo"
        Public Const PropertyId As String = "pid"
        Public Const Jsn As String = "jsn"
        Public Const ScheduleWorksTab As String = "tab"
        Public Const ScheduleWorksType As String = "type"

        Public Const Reschedule As String = "res"

        Public Const ComponentId = "compotentId"
        Public Const AllYears = "all"

        Public Const ConditionReport = "cr"
        Public Const ApprovedCondition = "scw"
        Public Const JobStatusReport As String = "rptcmpt"
        Public Const isReadOnly As String = "readOnly"


#End Region

    End Class
End Namespace

