﻿Imports System

Namespace PL_Utilities
    Public Class SpNameConstants

#Region "Master Page"

        Public Const GetEmployeeById As String = "FL_GetEmployeeById"
        Public Shared GetPropertyMenuList As String = "P_GetPropertyMenuList"
        Public Shared GetPropertyMenuPages As String = "P_GetPropertyMenuPages"

#End Region

#Region "Dashboard"

        Public Const GetRSLModulesList As String = "P_GetRSLModulesList"
        Public Const GetReplacementDueCurrentYearCount As String = "PLANNED_GetReplacementDueCurrentYearCount"
        'TODO: Verify from database and other projects/solution and remove this stored Procedure.
        Public Const GetReplacementDueNotScheduleCount As String = "PLANNED_GetReplacementDueNotScheduleCount"
        'TODO: Verify from database and other projects/solution and remove this stored Procedure.
        Public Const GetReplacementDueScheduleCount As String = "PLANNED_GetReplacementDueScheduledCount"
        Public Const GetNoEntryCount As String = "PLANNED_GetNoEntryCount"
        Public Const GetInprogressCount As String = "PLANNED_GetPropertiesInprogressCount"
        'TODO: Verify from database and other projects/solution and remove this stored Procedure.
        Public Const GetReplacementOverdueCount As String = "PLANNED_GetReplacementOverdueCount"
        Public Const GetAllComponents As String = "PLANNED_GetAllComponents"
        Public Const GetInprogressAppointments As String = "PLANNED_GetInprogressAppointments"
        Public Const GetInprogressAppointmentOperatives As String = "PLANNED_GetInprogressAppointmentOperatives"

        Public Const GetMiscAndAdaptationCount As String = "PLANNED_GetMiscAndAdaptationCount"
        Public Const GetConditionWorksApprovalRequiredCount As String = "PLANNED_GetCndtWorksApprRequCount"
        Public Const GetConditionWorksApprovedCount As String = "PLANNED_GetCndtWorksApprovedCount"

#End Region

#Region "Life Cycle"

        Public Const GetComponentsForLifeCycle As String = "PLANNED_GetComponentsForLifeCycle"
        Public Const CheckCompTradeExist As String = "PLANNED_CheckCompTradeExist"
        Public Const GetComponentsByID As String = "PLANNED_GetComponentDetailByID"
        Public Const getTradeLookup As String = "FL_GetTradeLookUpAll"
        Public Const UpdateComponent As String = "PLANNED_UpdateComponent"

        Public Const GetAllSchemes As String = "PLANNED_GetAllSchemes"
        Public Const GetSchemesAgainstReplacementYearAndComponent As String = "PLANNED_GetSchemesAgainstReplacementYearAndComponent"
        Public Const GetReplacementList As String = "PLANNED_GetReplacementList"
        Public Const GetAllAppointmentStatuses As String = "PLANNED_GetAllAppointmentStatuses"
        Public Const ScheduleReplacementItem As String = "PLANNED_ScheduleReplacementItem"

        Public Const GetPropertyDetail As String = "PLANNED_GetPropertyDetail"
        Public Const GetSchemeDetail As String = "PLANNED_GetSchemeDetail"
        Public Const GetBlockDetail As String = "PLANNED_GetBlockDetail"
        Public Const GetAddressByType As String = "PLANNED_GetAddressByType"
        Public Const GetAllTrades As String = "PLANNED_GetAllTrades"
        Public Const GetActionByStatusId As String = "PLANNED_GetActionByStatusId"
        Public Const GetLetterByActionId As String = "PLANNED_GetLetterByActionId"
        Public Const ArrangeInspection As String = "PLANNED_ArrangeInspection"
        Public Const GetOperativeInformation As String = "PLANNED_GetOperativeInfo"
        Public Const GetInspectionInfo As String = "PLANNED_getInspectionInfo"
        Public Const InspectionArrangedActions As String = "PLANNED_InspectionArrangedActions"
        Public Const GetEmpoloyeeByTradeId As String = "PLANNED_GetEmpoloyeeByTradeId"
        Public Const GetAllEmpoloyees As String = "PLANNED_GetAllEmpoloyees"
        Public Const GetOperativesLeavesAndAppointments As String = "PLANNED_GetOperativesLeavesAndAppointments"
        Public Const PLANNED_GetOperativesLeavesAndAppointmentsReplacementList As String = "PLANNED_GetOperativesLeavesAndAppointmentsReplacementList"



#End Region

#Region "Scheduling"

        Public Const GetAppointmentsCancelledList As String = "PLANNED_GetAppointmentsCancelledList"
        Public Const GetAppointmentsToBeArrangedList As String = "PLANNED_GetAppointmentsToBeArrangedList"
        Public Const GetAssociatedPmos As String = "PLANNED_GetAssociatedPmos"
        Public Const GetAssociatedAppointmentList As String = "PLANNED_GetAssociatedAppointments"
        Public Const GetLocations As String = "PLANNED_GetLocations"
        Public Const GetAppointmentTypes As String = "PLANNED_GetAppointmentTypes"
        Public Const GetAppointmentsArrangedList As String = "PLANNED_GetAppointmentsArrangedList"
        Public Const GetSearchProperty As String = "PLANNED_GetSearchProperty"
        Public Const GetLocationAdaptations As String = "PLANNED_GetLocationAdaptations"
        Public Const GetMiscWorkAvailableOperatives As String = "PLANNED_GetMiscWorkAvailableOperatives"
        Public Const ScheduleMiscWorkAppointment As String = "PLANNED_ScheduleMiscWorkAppointment"
        Public Const SchedulePlannedWorkAppointment As String = "PLANNED_SchedulePlannedWorkAppointment"
        Public Const GetMiscComponentTrades As String = "PLANNED_GetMiscComponentTrades"
        Public Const GetMiscComponentDetails As String = "PLANNED_GetMiscAppointmentDetails"
        Public Const GetComponentTrades As String = "PLANNED_GetComponentTrades"
        Public Const GetAvailableOperatives As String = "PLANNED_GetAvailableOperatives"
        Public Const GetArrangedAppointmentsDetailByPmo As String = "PLANNED_GetArrangedAppointmentsDetailByPmo"
        Public Const GetCancelledAppointmentsDetailByPmo As String = "PLANNED_GetCancelledAppointmentsDetailByPmo"
        Public Const PLANNED_EmailDetailByPmo As String = "PLANNED_EmailDetailByPmo"
        Public Const GetConditionArrangedAppointmentsDetailByPmo As String = "PLANNED_GetConditionArrangedAppointmentsDetailByPmo"
        Public Const UpdatePlannedAppointmentNotes As String = "PLANNED_UpdatePlannedAppointmentNotes"
        Public Const CancelArrangedAppointment As String = "PLANNED_CancelArrangedAppointment"
        Public Const GetConfirmAppointments As String = "PLANNED_GetConfirmAppointments"
        Public Const GetMiscConfirmAppointments As String = "PLANNED_GetMiscConfirmAppointments"
        Public Const DeleteAppointment As String = "PLANNED_DeleteAppointment"
        Public Const DeleteMiscAppointment As String = "PLANNED_DeleteMiscAppointment"
        Public Const DeleteRemovedTrades As String = "PLANNED_DeleteRemovedTrades"
        Public Const RemoveTradeFromScheduling As String = "PLANNED_RemoveTradeFromScheduling"
        Public Const RemoveMiscTradeFromScheduling As String = "PLANNED_RemoveMiscTradeFromScheduling"
        Public Const GetRemovedTradesFromScheduling As String = "PLANNED_GetRemovedTradesFromScheduling"
        Public Const GetRemovedMiscTradesFromScheduling As String = "PLANNED_GetRemovedMiscTradesFromScheduling"
        Public Const SetPendingBitForAppointment As String = "PLANNED_SetPendingBitForAppointment"
        Public Const ScheduleConfirmedPlannedAppointments As String = "PLANNED_ScheduleConfirmedPlannedAppointments"
        Public Const UpdatePlannedWorkAppointment As String = "PLANNED_UpdatePlannedWorkAppointment"
        Public Const GetRiskAndVulnerabilityInfo As String = "PLANNED_GetRiskAndVulnerabilityInfo"
        Public Const GetConditionalToBeArrangedList As String = "PLANNED_GetConditionalToBeArrangedList"
        Public Const GetConditionalArrangedList As String = "PLANNED_GetConditionalArrangedList"
        Public Const ScheduleConditionWorksAppointment As String = "PLANNED_ScheduleConditionWorksAppointment"
        Public Const ConfirmConditionWorksAppointment As String = "PLANNED_ConfirmConditionWorksAppointment"
        Public Const GetBankHolidays As String = "PLANNED_GetBankHolidays"
        Public Const GetJobSheetSummaryDetails As String = "PLANNED_GetJobSheetSummaryDetails"
        Public Const ScheduleAdhocAppointment As String = "PLANNED_ScheduleAdhocAppointment"
        Public Const CancelAppointmentToBeArrange As String = "PLANNED_CancelToBeArrangedAppointment"

        Public Const GetOperativeWorkingHours As String = "FL_GetOperativeWorkingHours"

#End Region

#Region "Reports"
        'Annual Spend Report
        Public Const GetAllComponentsDetails As String = "PLANNED_GetAllComponentsDetails"
        Public Const UpdateComponentData As String = "PLANNED_UpdateComponentData"
        Public Const GetComponentCostData As String = "PLANNED_GetComponentCostReport"

        Public Const GetConditionRatingList As String = "PLANNED_GetConditionRatingList"
        Public Const GetConditionRatingApprovedList As String = "PLANNED_GetConditionRatingApprovedList"
        Public Const GetConditionRatingRejectedList As String = "PLANNED_GetConditionRatingRejectedList"
        Public Const GetConditionReasons As String = "PLANNED_GetConditionReasons"
        Public Const GetCancelReasons As String = "PLANNED_GetCancelReasons"
        Public Const GetConditionMoreDetail As String = "PLANNED_GetConditionMoreDetail"
        Public Const ApproveConditionWorks As String = "PLANNED_ApproveConditionWorks"
        Public Const RejectConditionWorks As String = "PLANNED_RejectConditionWorks"

        Public Const GetYearlyComponentPropertyList As String = "PLANNED_GetYearlyComponentPropertyList"
        Public Const UpdatePropertyReplacementDueDate As String = "PLANNED_UpdatePropertyReplacementDueDate"

        Public Const GetBudgetSpendReport As String = "PLANNED_GetBudgetSpendReport"
        Public Const GetBudgetSpendReportDetail As String = "PLANNED_GetBudgetSpendReportDetail"
        Public Const GetBudgetSpendReportExcelDetail As String = "PLANNED_GetBudgetSpendReportExcelDetail"

        Public Const GetCOMPLETEDWORKSLIST As String = "PLANNED_GETCOMPLETEDWORKSLIST"

#End Region

#Region "Letters"
        Public Const GetAllLetters As String = "PLANNED_AllLetters"
        Public Const AddStandardLetter As String = "PLANNED_AddStandardLetter"
        Public Const AddSavedLetter As String = "PLANNED_AddSavedLetter"
        Public Const GetLetterById As String = "PLANNED_GetStandardLetterById"
        Public Const UpdateStandardLetter As String = "PLANNED_UpdateStandardLetterTemplate"
        Public Const DeleteStandardLetter As String = "PLANNED_DeleteStandardLetterTemplate"
        Public Const GetSavedLetterById As String = "PLANNED_GetSavedLetterById"
        Public Const getLettersByStatusId As String = "PLANNED_GetLettersByStatusId"
        Public Const PropertyLetter As String = "PLANNED_PropertyLetters"
        Public Const SaveEditedLetter As String = "PLANNED_SaveEditedLetter"
        Public Const SaveDocuments As String = "PLANNED_SaveDocuments"
        Public Const GetCustomerAndPropertyRbRc As String = "PLANNED_GetCustomerAndPropertyRbRc"
        Public Const GetSignOffTypes As String = "PLANNED_GetSignOff"
        Public Const GetTeams As String = "PLANNED_GetTeams"
        Public Const GetFromUsersFromTeamID As String = "PLANNED_GetFromUserByTeamID"
        Public Const GetFromTelEmailPrintLetterByEmployeeID As String = "PLANNED_GetFromTelEmailPrintLetterByEmployeeID"
#End Region

#Region "Status And Action"
        Public Const GetAllStauses As String = "PLANNED_AllStauses"
        Public Const AddStatus As String = "PLANNED_AddUpdateStatus"
        Public Const GetAllActions As String = "PLANNED_AllActions"
        Public Const AddAction As String = "PLANNED_AddAction"
        Public Const GetActionRankingByStatusId As String = "PLANNED_GetActionRankingByStatusId"
        Public Const GetActionsByStatusId As String = "PLANNED_GetActionByStatusId"
        Public Const EditAction As String = "PLANNED_EditAction"
        Public Const EditStatus As String = "PLANNED_EditStatus"
#End Region

#Region "Customer"
        Public Const AmendCustomerAddress As String = "FL_AmendCustomerAddress"
#End Region

#Region "Assign To contractor"

#Region "Drop Down Values"
        Public Const GetCostCentreDropDownValues As String = "PLANNED_GetCostCentreDropDownValues"
        Public Const GetBudgetHeadDropDownValuesByCostCentreId As String = "PLANNED_GetBudgetHeadDropDownValuesByCostCentreId"
        Public Const GetExpenditureDropDownValuesByBudgetHeadId As String = "PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId"
        Public Const GetContactDropDownValuesbyContractorId As String = "PLANNED_GetContactDropDownValuesbyContractorId"
        Public Const GetPlannedContractorDropDownValues As String = "PLANNED_GetPlannedContractorDropDownValues"
        Public Const GetVatDropDownValues As String = "PLANNED_GetVatDropDownValues"
#End Region

#Region "Save Assign to Contractor"
        Public Const AssignWorkToContractor As String = "PLANNED_AssignWorkToContractor"
        Public Const GetDetailforEmailToContractor As String = "Planned_GetDetailforEmailToContractor"
#End Region

#Region "Assigned to Contractor Report/List"
        Public Const GetAssignedToContractorReport As String = "PLANNED_GetAssignedToContractorReport"
        Public Const GetContactEmailDetail As String = "DF_GetContactEmailDetail"
#End Region

#End Region

#Region " Fiscal Years "
        Public Const GetAllFiscalYears As String = "PLANNED_GetAllFiscalYears"
#End Region

#Region "Job Sheet Summary Sub Contractor Update"

        Public Shared getJobSheetSummaryByJSN As String = "PLANNED_GetJobSheetSummaryByJSN"
        Public Shared getPlannedStatusLookUp As String = "PLANNED_GetStatusLookUp"
        Public Shared setAppointmentJobSheetStatus As String = "PLANNED_SetJobSheetAppointmentStatus"

#End Region


    End Class
End Namespace

