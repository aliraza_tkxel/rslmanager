﻿
Namespace PL_Utilities
    Public Class ViewStateConstants

#Region "Page Sort Constants"

        Public Const PageSortBo As String = "PageSortBo"
        Public Const TotalCount As String = "TotalCount"
        Public Const ResultDataSet As String = "ResultDataSet"
        Public Const CompleteDataSet As String = "CompleteDataSet"
        Public Const VirtualItemCount As String = "VirtualItemCount"

#End Region

#Region "View State Key Names"
        'Trades Grid View State
        'Public Const TradesGridViewState As String = "TradesViewState"
        Public Const ExistingTradesDataTable As String = "ExistingTradesDataTable"
        Public Const TradesToDeleteViewState As String = "TradesToDeleteViewState"
        Public Const TradesToInsertViewState As String = "TradesToInsertViewState"
#End Region

#Region "Dashboard"

        Public Const ComponentId As String = "ComponentId"
        Public Const OperativeId As String = "OperativeId"

#End Region

#Region "Letters & Documents"

        Public Const LetterDocList As String = "LetterDocList"
        Public Const StandardLetterId As String = "standardLetterId"
        Public Const UniqueStringLength As String = "UniqueString"
#End Region

#Region "Status Constants"
        Public Const RankingCount As String = "RankingCount"
        Public Const StatusDataTable As String = "StatusDataTable"
        Public Const ActionDataTable As String = "ActionDataTable"
        Public Const StatusIdforEdit As String = "StatusIdforEdit"
        Public Const ActionId As String = "ActionId"
        Public Const StatusIdForAddAction As String = "StatusIdForAddAction"
        Public Const AccessTreeEmployeeName As String = "AccessTreeEmployeeName"
        Public Const AllStatuses As String = "AllStatuses"
#End Region

#Region "General"
        Public Const Search As String = "Search"
        Public Const SchemeId As String = "SchemeId"
        Public Const AppointmentComponentId As String = "AppointmentComponentId"
        Public Const AppointmentDate As String = "AppointmentDate"
        Public Const AppointmentToBeArrangedDataSet As String = "AppointmentToBeArrangedDataSet"
        Public Const AppointmentsArrangedDataSet As String = "AppointmentsArrangedDataSet"
        Public Shared CurrentIndex As String = "CurrentIndex"
        Public Shared TotalJobsheets As String = "TotalJobsheets"
        Public Shared PreviouslySelecteAppointmentStartTime As String = "PreviouslySelecteAppointmentStartTime"
        Public Shared PreviouslySelecteAppointmentEndTime As String = "PreviouslySelecteAppointmentEndTime"
        Public Shared PlannedType = "PlannedType"
        Public Shared TotalCost = "TotalCost"
#End Region

#Region "Scheduling"
        Public Const MiscOperativeSearchBO As String = "MiscOperativeSearchBO"
        Public Const Pmo As String = "pmo"
        Public Const Jsn As String = "Jsn"
        Public Const PropertyId As String = "PropertyId"
        Public Const Address As String = "Address"
        Public Const Component As String = "Component"
        Public Const Due As String = "Due"
        Public Const DueDateFull As String = "DueDateFull"
        Public Const ItemId As String = "ItemId"
        Public Const Type As String = "Type"
        Public Const isReadOnly As String = "readOnly"
#End Region

#Region "Assign to Contractor"
        Public Const WorkRequiredDT As String = "WorkRequiredDT"
        Public Const AssignToContractorWorkType As String = "AssignToContractorWorkType"
        Public Const IsSaved As String = "IsSaved"
#End Region

#Region "Assign to Contractor Misc Work"

        Public Const availableMiscOperativesControlVisibleStatus As String = "availableMiscOperativesControlVisibleStatus"
        Public Const refreshButtonVisibleStatus As String = "refreshButtonVisibleStatus"

#End Region

    End Class
End Namespace

