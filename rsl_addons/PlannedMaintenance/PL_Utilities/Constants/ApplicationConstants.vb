﻿Imports System.Configuration
Namespace PL_Utilities
    Public Class ApplicationConstants

#Region "General"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const LinkButtonType As String = "LinkButtonType"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdatePanelType As String = "UpdatePanelType"
        Public Const NotAvailable As String = "N/A"
        Public Const DefaultValue As String = "-1"

        'Appointment types are used for push notification
        ' "NewAppointmentType" means new appointment is going to be created
        ' "RearrangeAppointmentType" means push notification is for appointment rearrange
        Public Shared NewAppointmentType As Integer = 1
        Public Shared RearrangeAppointmentType As Integer = 3

#End Region

#Region "Master Layout"

        Public Shared ConWebBackgroundColor As String = "#e6e6e6"
        Public Shared ConWebBackgroundProperty As String = "background-color"

        Public Shared MenuCol As String = "Menu"
        Public Shared UrlCol As String = "Url"
        Public Shared MenuIdCol As String = "MenuId"
        Public Shared PageFileNameCol As String = "PageFileName"
        Public Shared NoPageSelected As Integer = -1
        Public Shared PageFirstLevel As Integer = 1

        Public Shared ServicingMenu As String = "Servicing"
        Public Shared ResourcesMenu As String = "Resources"
        Public Shared PropertyModuleMenu As String = "Search"
        Public Shared RepairsMenu As String = "Repairs"
        Public Shared PlannedMenu As String = "Planned"
        Public Shared MaintenanceMenu = "Maintenance"
        Public Shared MultipleMenus As String = ""

        Public Shared AccessGrantedModulesDt As String = "AccessGrantedModulesDt"
        Public Shared AccessGrantedMenusDt As String = "AccessGrantedMenusDt"
        Public Shared AccessGrantedPagesDt As String = "AccessGrantedPagesDt"
        Public Shared RandomPageDt As String = "RandomPageDt"

        Public Shared GrantModulesModuleIdCol As String = "MODULEID"

        Public Shared GrantMenuModuleIdCol As String = "ModuleId"
        Public Shared GrantMenuMenuIdCol As String = "MenuId"
        Public Shared GrantMenuUrlCol As String = "Url"
        Public Shared GrantMenuDescriptionCol As String = "Menu"

        Public Shared GrantPageParentPageCol As String = "ParentPage"
        Public Shared GrantPageAccessLevelCol As String = "AccessLevel"
        Public Shared GrantPageQueryStringCol As String = "PageQueryString"
        Public Shared GrantPageFileNameCol As String = "PageFileName"
        Public Shared GrantPageUrlCol As String = "PageUrl"
        Public Shared GrantPageCoreUrlCol As String = "PageCoreUrl"
        Public Shared GrantPagePageNameCol As String = "PageName"
        Public Shared GrantPageIdCol As String = "PageId"
        Public Shared GrantPageMenuIdCol As String = "MenuId"
        Public Shared GrantPageModuleIdCol As String = "ModuleId"
        Public Shared GrantOrderTextCol As String = "ORDERTEXT"

        Public Shared RandomParentPageCol As String = "ParentPage"
        Public Shared RandomAccessLevelCol As String = "AccessLevel"
        Public Shared RandomQueryStringCol As String = "PageQueryString"
        Public Shared RandomFileNameCol As String = "PageFileName"
        Public Shared RandomPageNameCol As String = "PageName"
        Public Shared RandomPageIdCol As String = "PageId"

#End Region

#Region "Schedule Calendar"
        Public Shared startDate As Date = Date.Today
        Public Shared endDate As Date = Date.Today
        Public Shared weekDays As Integer
#End Region

#Region "Property distance "
        Public Shared propertyDistance As New DataTable
        Public Shared distanceError As Boolean = True
#End Region

#Region "Trades GridView\Table Column Name For Add Fault Page/Popup"
        ' Public Const TradeIdColumn As String = "FaultTradeId"
        Public Const tradeIdColumn As String = "TRADEID"
        Public Const tradeColumn As String = "TRADE"
        Public Const durationColumn As String = "DURATION"
        Public Const sorderColumn As String = "SORDER"
#End Region

#Region "Dashboard"

        Public Const HashTableKeyRDCurrentYear As String = "RD_CurrentYear"
        Public Const HashTableKeyRDNotScheduled As String = "RD_NotScheduled"
        Public Const HashTableKeyRDScheduled As String = "RD_Scheduled"

        Public Const HashTableKeyRDOverdue As String = "RD_Overdue"
        Public Const HashTableKeyRDNoentry As String = "RD_Noentry"
        Public Const HashTableKeyRDInprogress As String = "RD_Inprogress"

        Public Const EventArgumentUpdate As String = "_Update"
        Public Const AllSelectedValue As Integer = -1

        Public Const NonSelectedValue As Integer = -2


#End Region

#Region "Lifecycle"

        Public Const ReplacementListDataTable As String = "ReplacementListDataTable"
        Public Const ReplacementTotalCostDataTable As String = "ReplacementTotalCostDataTable"
        Public Const ReplacementTotalCountDataTable As String = "ReplacementTotalCountDataTable"

        Public Const InspectionInfoDataTable As String = "InspectionInfoDataTable"
        Public Const LetterDataTable As String = "LetterDataTable"
        Public Const DocumentsDataTable As String = "DocumentsDataTable"

        Public Const CallFromPostback As Boolean = True
        Public Const ExportFullList As Boolean = True

        '''  YEAR OPERATOR IS USED TO GET RECORDS HAVING REPLACEMENT DUE DATES IN "REPLACEMENT YEAR" , "BEFORE REPLACEMENT YEAR" OR "REPLACEMENT AND PREVIOUS YEAR"
        '''	0 => EQUAL TO REPLACEMENT YEAR
        ''' -1 => "CURRENT AND PREVIOUS YEAR"
        '''	-2 => "PREVIOUS YEAR"
        Public Const YearEqual As String = "0"
        Public Const YearLessThanEqual As String = "-1"
        Public Const YearLessThan As String = "-2"

        Public Shared appointmentTime() As String = {"00:00", "00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"}
        Public Const DocumentWord As String = "Document"
        Public Const LetterWord As String = "Letter"
        Public Const LetterPrefix As String = "SL_"
        Public Const DocPrefix As String = "Doc_"

#End Region

#Region "Resources"
        Public Const DefaultStatusLabelForTreeNode As String = "Add New Status"
        Public Const DefaultActionLabelForTreeNode As String = "Add New Action"
        Public Const DefaultStatusIdForTreeNode As Int16 = -11
        Public Const DefaultActionIdForTreeNode As Int32 = -12
        Public Const StatusTargetForTreeNode As String = "status"
        Public Const ActionTargetForTreeNode As String = "action"

#End Region

#Region "Letters Doc Data Table Column Names"
        Public Const LetterDocNameColumn As String = "LetterDocName"
        Public Const LetterDocValueColumn As String = "LetterDocValue"
        Public Const LetterIdColumn As String = "LetterId"
        Public Const LetterDocDeleteColumn As String = "Delete"
#End Region

#Region "Edit Letter Values Data Table Column Names"
        Public Const LetterTextTitleColumn As String = "LetterTitle"
        Public Const LetterBodyColumn As String = "LetterContent"
        Public Const StandardLetterId As String = "LetterId"
        Public Const TeamIdColumn As String = "TeamId"
        Public Const FromResourceIdColumn As String = "FromResourceId"
        Public Const SignOffCodeColumn As String = "SignOffLookUpCode"
        Public Const RentBalanceColumn As String = "RentBalance"
        Public Const RentChargeColumn As String = "RentCharge"
        Public Const TodayDateColumn As String = "TodayDate"
#End Region

#Region "CSS"
        Public Const TabClickedCssClass As String = "TabClicked"
        Public Const TabInitialCssClass As String = "TabInitial"
#End Region

#Region "Scheduling"
        Public Const UpdateCustomerDetails As String = "Update customer details"
        Public Const UpdateNotes As String = "Update Notes"
        Public Const SaveChanges As String = "Save Changes"
        Public Const NotSaved As String = "0"
        Public Const AvailableOperatives As String = "AvailableOperatives"
        Public Const OperativesLeaves As String = "OperativesLeaves"
        Public Const OperativesAppointments As String = "OperativesAppointments"
        Public Const TempAppointmentGridControlId As String = "grdTempAppointments"
        Public Const ConfirmAppointmentGridControlId As String = "grdConfirmAppointments"
        Public Const RemovedButtonPanelControlId As String = "pnlRemovedButtons"
        Public Const CompleteTradeGridControlId As String = "grdCompleteTrades"
        Public Const ConfirmTradeGridControlId As String = "grdConfirmTrades"
        Public Const RemovedTradeGridControlId As String = "grdRemovedTrades"
        Public Const TempTradeGridControlId As String = "grdTempTrades"
        Public Const AppointmentStartDateControlId As String = "txtStartDate"
        Public Const StartDateLabelControlId As String = "lblStartDate"
        Public Const CalDateControlId As String = "imgCalDate"
        Public Const EditImageButtonControlId As String = "imgBtnEdit"
        Public Const DoneImageButtonControlId As String = "imgBtnDone"
        Public Const DurationLabelControlId As String = "lblDuration"
        Public Const DurationDropdownControlId As String = "ddlDuration"
        Public Const DurationPanelControlId As String = "pnlDuration"
        Public Const CalExtDateControlId As String = "imgCalExtDate"
        Public Const GoControlId As String = "btnGo"
        Public Const NotPending As Integer = 0
        Public Const Pending As Integer = 1
        Public Const JobsheetCurrentColumn As String = "Row"
        Public Const PmoColumn As String = "PMO"
        Public Const ComponentColumn As String = "Component"
        Public Const TradesColumn As String = "Trade"
        Public Const JsnColumn As String = "JSN"
        Public Const JsnSearchColumn As String = "JSNSearch"
        Public Const StatusColumn As String = "Status"
        Public Const OperativeColumn As String = "Operative"
        Public Const StartTimeColumn As String = "StartTime"
        Public Const EndTimeColumn As String = "EndTime"
        Public Const DurationsColumn As String = "Duration"
        Public Const TotalDurationColumn As String = "TotalDuration"
        Public Const StartDateColumn As String = "StartDate"
        Public Const EndDateColumn As String = "EndDate"
        Public Const CustomerNotesColumn As String = "CustomerNotes"
        Public Const JobsheetNotesColumn As String = "JobsheetNotes"
        Public Const IsMiscAppointmentColumn As String = "IsMiscAppointment"
        Public Const PropertyIdColumn As String = "PropertyId"
        Public Const AppointmentIdColumn As String = "AppointmentId"
        Public Const AppointmentArrangedTab As String = "aptArranged"
        Public Const AppointmentAdaptation As String = "Adaptation"
        Public Const AppointmentMisc As String = "Misc"
        Public Const AppointmentCancelledTab As String = "aptCancelled"
        Public Const AppointmentToBeArrangedTab As String = "aptToBeArranged"
        Public Const ConfirmedAppointementGridsIdConstant As String = "c"
        Public Const RemovedAppointementGridsIdConstant As String = "r"
        Public Const CompleteAppointementGridsIdConstant As String = "comp"
        Public Const StatusToBeArranged As String = "To be Arranged"
        Public Const StatusApproved As String = "Approved"
        Public Const StatusPending As String = "Pending"
        Public Const StatusRemovedFromScheduling As String = "Removed from scheduling"
        Public Const StatusArranged As String = "Arranged"
        Public Const StatusCompleted As String = "Completed"
        Public Const StatusInterimComplete As String = "Complete"
        Public Const StatusInterimNotStarted As String = "NotStarted"
        Public Const StatusInterimInProgress As String = "InProgress"
        Public Const RiskInfo As String = "RiskInfo"
        Public Const VulnerabilityInfo As String = "VulnerabilityInfo"
        Public Const InterimStatusColumn As String = "InterimStatus"
        Public Const PlannedSubStatusColumn As String = "PlannedSubStatus"
        Public Const StatusNoEntry As String = "No Entry"

#End Region

#Region "Reports"

        Public Const PropertyListDt As String = "PropertyListDt"
        Public Const TotalCostDt As String = "TotalCostDt"
        Public Const TotalCountDt As String = "TotalCountDt"
        Public Const ConditionRatingDt As String = "ConditionRatingDt"
        Public Const ConditionRatingCountDt As String = "ConditionRatingCountDt"
        Public Const ReasonDt As String = "ReasonDt"
        Public Const PropertyCustomerDetailDt As String = "PropertyCustomerDetailDt"
        Public Const ConditionDetailDt As String = "ConditionDetailDt"
        Public Const BudgetSpendReportDt As String = "BudgetSpendReportDt"
        Public Const BudgetSpendReportDetailDt As String = "BudgetSpendReportDetailDt"
        Public Const ComponentDt As String = "ComponentDt"
        Public Const BudgetSpendReportExcelDetailDt As String = "BudgetSpendReportExcelDetailDt"
        Public Const ForecastDt As String = "ForecastDt"
        Public Const ApprovedDt As String = "ApprovedDt"
        Public Const ArrangedDt As String = "ArrangedDt"
        Public Const CompletedDt As String = "CompletedDt"
        Public Const BudgetSpendReportCountDt As String = "BudgetSpendReportCountDt"

        Public Const ComponentIdCol As String = "COMPONENTID"
        Public Const SOrderCol As String = "SOrder"
        Public Const ComponentNameCol As String = "COMPONENTNAME"
        Public Const CycleCol As String = "CYCLE"
        Public Const FrequencyCol As String = "FREQUENCY"
        Public Const MaterialCostCol As String = "MATERIALCOST"
        Public Const ComponentCostCol As String = "COST"
        Public Const LabourCostCol As String = "LABOURCOST"
        Public Const CostFlagCol As String = "COSTFLAG"
        Public Const CycleFlagCol As String = "CYCLEFLAG"

        Public Const AddressCol As String = "Address"

        Public Const SidCol As String = "SID"
        Public Const DueDateCol As String = "DUEDATE"

        Public Const SchemeCol As String = "Scheme"
        Public Const PropertyAddressCol As String = "Address"
        Public Const PropertyIdCol As String = "PropertyId"
        Public Const TownCityCol As String = "TownCity"
        Public Const CountyCol As String = "County"
        Public Const PostcodeCol As String = "Postcode"

        Public Const CustomerCol As String = "Customer"
        Public Const TelephoneCol As String = "Telephone"
        Public Const MobileCol As String = "Mobile"
        Public Const EmailCol As String = "Email"

        Public Const LocationCol As String = "Location"
        Public Const ComponentCol As String = "Component"
        Public Const StatusCol As String = "Status"
        Public Const ConditionUpdatedCol As String = "ConditionUpdated"
        Public Const ConditionUpdatedByCol As String = "ConditionUpdatedBy"

        Public Const LastSurveyCol As String = "LastSurvey"
        Public Const LastSurveyedByCol As String = "LastSurveyedBy"
        Public Const ApprovalCol As String = "Approval"
        Public Const AppointmentCol As String = "Appointment"
        Public Const ConditionRatingApproved As String = "Approved"

        Public Const PlannedComponentCol As String = "PlannedComponent"
        Public Const ReplacementCycleCol As String = "ReplacementCycle"
        Public Const ReplacementDueCol As String = "ReplacementDue"

        Public Const WorksRequiredCol As String = "WorksRequired"

        Public Const IsForecastCol As String = "ISFORECAST"
        Public Const IsApprovedCol As String = "ISAPPROVED"
        Public Const IsArrangedCol As String = "ISARRANGED"
        Public Const IsCompletedCol As String = "ISCOMPLETED"
        Public Const IsCancelledCol As String = "ISCANCELLED"

#End Region

#Region "Assign Work To Contractor"

        Public Const EmailSubject As String = "Planned Work has been Assigned"
        Public Const NoExpenditureSetup As String = "No Expenditures are Setup"
        Public Const WorkOrderedPoStatusId As Int32 = 1
        Public Const QueuedPoStatusId As Int32 = 0

#Region "Data table names for email details"

        Public Const ContractorDetailsDt As String = "ContractorDetails"
        Public Const PropertyDetailsDt As String = "PropertyDetails"
        Public Const TenantDetailsDt As String = "TenantDetails"
        Public Const TenantRiskDetailsDt As String = "TenantRiskDetails"
        Public Const TenantVulnerabilityDetailsDt As String = "TenantVulnerabilityDetails"
        Public Const OrderedByDetailsDt As String = "OrderedByDetails"
        Public Const AsbestosDt As String = "Asbestos"
#End Region

#Region "Work Required Data Table column names"

        Public Const NetCostCol As String = "NetCost"
        Public Const VatTypeCol As String = "VatType"
        Public Const VatCol As String = "Vat"
        Public Const ContactEmailDetailsDt As String = "ContactEmailDetails"
        Public Const GrossCol As String = "Gross"
        Public Const PIStatusCol As String = "PIStatus"
        Public Const ExpenditureIdCol As String = "ExpenditureId"
        'Public Const EstimateCol As String = "Estimate"
        'Public Const EstimateRefCol As String = "EstimateRef"
        Public Const MaxStringLegthWordRequired As Integer = 4000

#End Region

#End Region

#Region "Drop Down Lists"

        ''' <summary>
        ''' These two constant are use to add the "Please Select" Drop down item in a drop down list.
        ''' </summary>
        ''' <remarks>This is a good practice to avoid spelling mistakes etc.</remarks>
        Public Const DropDownDefaultValue As String = "-1"
        Public Const DropDwonDefaultText As String = "Please Select"

        Public Const ddlDefaultDataTextField As String = "description"
        Public Const ddlDefaultDataValueField As String = "id"

        Public Const noContactFound = "No contact found"

#End Region

#Region "SMS"
        'Public Shared SMSUrl As String = ConfigurationManager.AppSettings("SMSAddress")
        Public Const SMSURL As String = "~/../Includes/SMS.asp"
        Public Shared SMSUserName As String = "adnan.mirza@broadlandgroup.org"
        Public Shared SMSPassword As String = "obp5+Ne"
        Public Shared strSMS As String = "sms"
        Public Shared SMSCompany As String = "Broadland"
        Public Shared SMSRejectedmessage As String = "The requested Condition Works for {Address 1}, {Town/City} have been rejected by {Username}. Please access the property record for more information."
        Public Shared SMSApprovedmessage As String = "The requested Condition Works for {Address 1}, {Town/City} have been approved by {Username}. Please access the property record for more information."
#End Region

#Region "Email"
        'Public Shared SMSUrl As String = ConfigurationManager.AppSettings("SMSAddress")
        Public Const replyTo = "no-reply@broadlandgroup.org"
        Public Const Subject = "Condition Works Notification - APPROVED"
        Public Const rejectedSubject = "Condition Works Notification - REJECTED"
#End Region

#Region "Page Sort"

        Public Shared Ascending As String = "ASC"
        Public Shared Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Shared PageSortBo As String = "PageSortBo"
#End Region

#Region "Job Sheet Summary Sub Contractor Update"

        'Table names
        Public Shared jobSheetSummaryDetailTable As String = "JobSheetSummaryDetailTable"
        Public Shared jobSheetSummaryAsbestosTable As String = "JobSheetSummaryAsbestosTable"

#End Region

        Public Const StartDateCol As String = "StartDate"
        Public Const EndDateCol As String = "EndDate"
        Public Const StartTimeCol As String = "StartTime"
        Public Const EndTimeCol As String = "EndTime"
        Public Const WeekDayCol As String = "Name"
        Public Const CreationDateCol As String = "CreationDate"
        Public Const TypeCol As String = "Type"

        Public Const YearEndRange As Integer = 2050

#Region "Data Table Names"

        Public Const WorkingHourDataTable As String = "WorkingHourDataTable"

#End Region
#Region "Misc Appointment Types Names"

        Public Const propertyType As String = "Property"
        Public Const schemeType As String = "Scheme"
        Public Const blockType As String = "Block"

#End Region

    End Class

End Namespace

