﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

Namespace PL_Utilities
    Public Class RegularExpConstants

        Public Const AmountExp As String = "^\d+(\.\d+)?$"
        Public Const numbersExp As String = "^[0-9.]+$"
        Public Const emailExp As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$"
        Public Const stringLengthExp As String = "^[\s\S]{0,30}$"
        Public Shared mobileExp As String = "^[0-9]+$"

    End Class
End Namespace
