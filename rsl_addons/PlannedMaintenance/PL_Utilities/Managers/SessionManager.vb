﻿Imports System.Web.HttpContext
Imports PL_BusinessObject
Imports System.Web.UI.WebControls


Namespace PL_Utilities
    Public Class SessionManager

#Region "Set / Get  Planned Maintenance User Id "
        ''' <summary>
        ''' This key shall be used to save the planned maintenance user id
        ''' </summary>
        ''' <remarks></remarks>
        Public Const PlannedMaintenanceUserId As String = "PlannedMaintenanceUserId"
#Region "Set  Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to save the planned maintenance user id in session
        ''' </summary>
        ''' <param name="plannedMaintenanceUserId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPlannedMaintenanceUserId(ByRef plannedMaintenanceUserId As Integer)
            Current.Session(SessionManager.PlannedMaintenanceUserId) = plannedMaintenanceUserId
        End Sub
#End Region

#Region "get Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to get the planned maintenance user id from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPlannedMaintenanceUserId() As Integer

            If (IsNothing(Current.Session(SessionManager.PlannedMaintenanceUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.PlannedMaintenanceUserId), Integer)
            End If
        End Function
#End Region

#Region "remove Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to remove the planned maintenance user id in session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePlannedMaintenanceUserId()
            If (Not IsNothing(Current.Session(SessionManager.PlannedMaintenanceUserId))) Then
                Current.Session.Remove(SessionManager.PlannedMaintenanceUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"
        ''' <summary>
        ''' This key shall be used to save the logged in user's name
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UserFullName As String = "UserFullName"
#Region "Set User Full Name"
        ''' <summary>
        ''' This function shall be used to save the logged in user's name in session
        ''' </summary>
        ''' <param name="userFullName"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionManager.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"
        ''' <summary>
        ''' This function shall be used to get the logged in user's name from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"
        ''' <summary>
        ''' This function shall be used to remove the logged in user's name from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Current.Session.Remove(SessionManager.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"
        ''' <summary>
        ''' This key shall be used to get the Employee Id of logged in user
        ''' </summary>
        ''' <remarks></remarks>
        Public Const EmployeeId As String = "UserEmployeeId"
#Region "Set User Employee Id"
        ''' <summary>
        ''' This function shall be used to save the Employee Id of logged in user in session
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserEmployeeId(ByRef employeeId As Integer)
            Current.Session(SessionManager.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"
        ''' <summary>
        ''' This function shall be used to get the Employee Id of logged in user from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"
        ''' <summary>
        ''' This function shall be used to remove the Employee Id of logged in user from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Current.Session.Remove(SessionManager.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"
        ''' <summary>
        ''' his key shall be used to save the logged in user type in session
        ''' </summary>
        ''' <remarks></remarks>
        Public Const LoggedInUserType As String = "LoggedInUserType"
#Region "Set Logged In User Type"
        ''' <summary>
        ''' This function shall be used to save the logged in user type in session
        ''' </summary>
        ''' <param name="userType"></param>
        ''' <remarks></remarks>
        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionManager.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"
        ''' <summary>
        ''' This function shall be used to get the logged in user type from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionManager.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionManager.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"
        ''' <summary>
        ''' This function shall be used to remove the logged in user type from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionManager.LoggedInUserType))) Then
                Current.Session.Remove(SessionManager.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Conponent Detail DataSet"
        ''' <summary>
        ''' This key will be used to save the Component detail dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConponentDetailDataSet As String = "ConponentDetailDataSet"
#Region "Set Conponent Detail DataSet"
        ''' <summary>
        ''' This function shall be used to save Component detail data set in session
        ''' </summary>
        ''' <param name="ConponentDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConponentDetailDataSet(ByRef ConponentDataSet As DataSet)
            Current.Session(ConponentDetailDataSet) = ConponentDataSet
        End Sub

#End Region

#Region "Get Conponent Detail DataSet"
        ''' <summary>
        ''' This function shall be used to get component detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConponentDetailDataSet() As DataSet
            If (IsNothing(Current.Session(ConponentDetailDataSet))) Then
                Dim recallDataSet As New DataSet
                Return recallDataSet
            Else
                Return CType(Current.Session(ConponentDetailDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Conponent Detail DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved component detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeConponentDetailDataSet()
            If (Not IsNothing(Current.Session(ConponentDetailDataSet))) Then
                Current.Session.Remove(ConponentDetailDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Trades to Insert List, Set and Remove Method/Function(s)"
        ''' <summary>
        ''' This key will be used to save the Trades to Insert List
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TradesToInsertList As String = "TradesToInsertList"
#Region "Set Trades to Insert List"
        ''' <summary>
        ''' This function shall be used to save Trades to Insert List data set in session
        ''' </summary>
        ''' <param name="ConponentDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTradesToInsertList(ByRef ConponentDataSet As List(Of OperativeListBO))
            Current.Session(TradesToInsertList) = ConponentDataSet
        End Sub

#End Region

#Region "Get Trades to Insert List"
        ''' <summary>
        ''' This function shall be used to get Trades to Insert List from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTradesToInsertList() As List(Of OperativeListBO)
            If (IsNothing(Current.Session(TradesToInsertList))) Then
                Dim tradeList As New List(Of OperativeListBO)
                Return tradeList
            Else
                Return CType(Current.Session(TradesToInsertList), List(Of OperativeListBO))
            End If
        End Function

#End Region

#Region "remove Trades to Insert List"
        ''' <summary>
        ''' This function shall be used to remove saved Trades to Insert List from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTradesToInsertList()
            If (Not IsNothing(Current.Session(TradesToInsertList))) Then
                Current.Session.Remove(TradesToInsertList)
            End If
        End Sub

#End Region

#End Region

#Region "Trades Data Table, Set and Remove Method/Function(s)"
        ''' <summary>
        ''' This key will be used to save the Trades to Insert List
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TradesDataTable As String = "TradesDataTable"
#Region "Set Trades Data Table"
        ''' <summary>
        ''' This function shall be used to save Trades Data Table in session
        ''' </summary>
        ''' <param name="ConponentDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTradesDataTable(ByRef ConponentDataSet As DataTable)
            Current.Session(TradesDataTable) = ConponentDataSet
        End Sub

#End Region

#Region "Get Trades Data Table"
        ''' <summary>
        ''' This function shall be used to get Trades Data Table from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTradesDataTable() As DataTable
            If (IsNothing(Current.Session(TradesDataTable))) Then
                Dim tradeList As New DataTable
                Return tradeList
            Else
                Return CType(Current.Session(TradesDataTable), DataTable)
            End If
        End Function

#End Region

#Region "remove Trades Data Table"
        ''' <summary>
        ''' This function shall be used to remove saved Trades Data Table from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTradesDataTable()
            If (Not IsNothing(Current.Session(TradesDataTable))) Then
                Current.Session.Remove(TradesDataTable)
            End If
        End Sub

#End Region

#End Region

#Region "Trades to Delete List, Set and Remove Method/Function(s)"
        ''' <summary>
        ''' This key will be used to save the Trades to Delete List
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TradesToDeleteList As String = "TradesToDeleteList"
#Region "Set Trades To Delete List"
        ''' <summary>
        ''' This function shall be used to save Trades to Delete List data set in session
        ''' </summary>
        ''' <param name="ConponentDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTradesToDeleteList(ByRef ConponentDataSet As List(Of OperativeListBO))
            Current.Session(TradesToDeleteList) = ConponentDataSet
        End Sub

#End Region

#Region "Get Trades To Delete List"
        ''' <summary>
        ''' This function shall be used to get Trades to Delete List from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTradesToDeleteList() As List(Of OperativeListBO)
            If (IsNothing(Current.Session(TradesToDeleteList))) Then
                Dim tradeList As New List(Of OperativeListBO)
                Return tradeList
            Else
                Return CType(Current.Session(TradesToDeleteList), List(Of OperativeListBO))
            End If
        End Function

#End Region

#Region "remove Trades To Delete List"
        ''' <summary>
        ''' This function shall be used to remove saved Trades to Delete List from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTradesToDeleteList()
            If (Not IsNothing(Current.Session(TradesToDeleteList))) Then
                Current.Session.Remove(TradesToDeleteList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Status Id For Edit"
        ''' <summary>
        ''' This key shall be used to get the status id for edit
        ''' </summary>
        ''' <remarks></remarks>
        Public Const StatusIdForEdit As String = "StatusIdForEdit"
#Region "Set Status Id For Edit"
        ''' <summary>
        ''' This function shall be used to save Status Id For Edit in session
        ''' </summary>
        ''' <param name="StatusIdForEdit"></param>
        ''' <remarks></remarks>
        Public Shared Sub setStatusIdForEdit(ByRef statusIdForEdit As Integer)
            Current.Session(SessionManager.StatusIdForEdit) = statusIdForEdit
        End Sub

#End Region

#Region "get Status Id For Edit"
        ''' <summary>
        ''' This function shall be used to get the Status Id For Edit from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getStatusIdForEdit() As Integer
            If (IsNothing(Current.Session(SessionManager.StatusIdForEdit))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.StatusIdForEdit), Integer)
            End If
        End Function

#End Region

#Region "remove Status Id For Edit"
        ''' <summary>
        ''' This function shall be used to remove Status Id For Edit from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeStatusIdForEdit()
            If (Not IsNothing(Current.Session(SessionManager.StatusIdForEdit))) Then
                Current.Session.Remove(SessionManager.StatusIdForEdit)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Status Id Of Action"
        ''' <summary>
        ''' This key shall be used to get the status id of action
        ''' </summary>
        ''' <remarks></remarks>
        Public Const StatusIdOfAction As String = "StatusIdOfAction"
#Region "Set Status Id Of Action"
        ''' <summary>
        ''' This function shall be used to save Status Id Of Acton in session
        ''' </summary>
        ''' <param name="StatusIdOfAction"></param>
        ''' <remarks></remarks>
        Public Shared Sub setStatusIdOfAction(ByRef statusIdOfAction As Integer)
            Current.Session(SessionManager.StatusIdOfAction) = statusIdOfAction
        End Sub

#End Region

#Region "get Status Id Of Action"
        ''' <summary>
        ''' This function shall be used to get the Status Id of Action from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getStatusIdOfAction() As Integer
            If (IsNothing(Current.Session(SessionManager.StatusIdOfAction))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.StatusIdOfAction), Integer)
            End If
        End Function

#End Region

#Region "remove Status Id Of Action"
        ''' <summary>
        ''' This function shall be used to remove Status Id of Action from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeStatusIdOfAction()
            If (Not IsNothing(Current.Session(SessionManager.StatusIdOfAction))) Then
                Current.Session.Remove(SessionManager.StatusIdOfAction)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Status Id For Edit Action"
        ''' <summary>
        ''' This key shall be used to get the status id for edit action
        ''' </summary>
        ''' <remarks></remarks>
        Public Const StatusIdForEditAction As String = "StatusIdForEditAction"
#Region "Set Status Id For Edit"
        ''' <summary>
        ''' This function shall be used to save Status Id For Edit Action in session
        ''' </summary>
        ''' <param name="StatusIdForEditAction"></param>
        ''' <remarks></remarks>
        Public Shared Sub setStatusIdForEditAction(ByRef statusIdForEditAction As Integer)
            Current.Session(SessionManager.StatusIdForEditAction) = statusIdForEditAction
        End Sub

#End Region

#Region "get Status Id For Edit Action"
        ''' <summary>
        ''' This function shall be used to get the Status Id For Edit Action from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getStatusIdForEditAction() As Integer
            If (IsNothing(Current.Session(SessionManager.StatusIdForEditAction))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.StatusIdForEditAction), Integer)
            End If
        End Function

#End Region

#Region "remove Status Id For Edit Action"
        ''' <summary>
        ''' This function shall be used to remove Status Id For Edit Action from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeStatusIdForEditAction()
            If (Not IsNothing(Current.Session(SessionManager.StatusIdForEditAction))) Then
                Current.Session.Remove(SessionManager.StatusIdForEditAction)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Action Id For Edit Action"
        ''' <summary>
        ''' This key shall be used to get the action id for edit action
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ActionIdForEditAction As String = "ActionIdForEditAction"
#Region "Set Action Id For Edit Action"
        ''' <summary>
        ''' This function shall be used to save Action Id For Edit Action in session
        ''' </summary>
        ''' <param name="ActionIdForEditAction"></param>
        ''' <remarks></remarks>
        Public Shared Sub setActionIdForEditAction(ByRef actionIdForEditAction As String)
            Current.Session(SessionManager.ActionIdForEditAction) = actionIdForEditAction
        End Sub

#End Region

#Region "get Action Id For Edit Action"
        ''' <summary>
        ''' This function shall be used to get the Action Id For Edit Action from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getActionIdForEditAction() As Integer
            If (IsNothing(Current.Session(SessionManager.ActionIdForEditAction))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.ActionIdForEditAction), Integer)
            End If
        End Function

#End Region

#Region "remove Action Id For Edit Action"
        ''' <summary>
        ''' This function shall be used to remove Action Id For Edit Action from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeActionIdForEditAction()
            If (Not IsNothing(Current.Session(SessionManager.ActionIdForEditAction))) Then
                Current.Session.Remove(SessionManager.ActionIdForEditAction)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Letter Bo For Preview"
        ''' <summary>
        ''' This key shall be used to get letter bo for preview
        ''' </summary>
        ''' <remarks></remarks>
        Public Const LetterBoForPreview As String = "LetterBoForPreview"
#Region "Set Letter Bo For Preview"
        ''' <summary>
        ''' This function shall be used to save letter bo for preview in session
        ''' </summary>
        ''' <param name="letterBo"></param>
        ''' <remarks></remarks>
        Public Shared Sub setLetterBoForPreview(ByRef letterBo As LetterBO)
            Current.Session(SessionManager.LetterBoForPreview) = letterBo
        End Sub

#End Region

#Region "get Letter Bo For Preview"
        ''' <summary>
        ''' This function shall be used to get letter bo for preview from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getLetterBoForPreview() As LetterBO
            If (IsNothing(Current.Session(SessionManager.LetterBoForPreview))) Then
                Dim letterBo As LetterBO = New LetterBO()
                Return letterBo
            Else
                Return CType(Current.Session.Item(SessionManager.LetterBoForPreview), LetterBO)
            End If
        End Function

#End Region

#Region "remove Letter Bo For Preview"
        ''' <summary>
        ''' This function shall be used to remove letter bo for preview from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeLetterBoForPreview()
            If (Not IsNothing(Current.Session(SessionManager.LetterBoForPreview))) Then
                Current.Session.Remove(SessionManager.LetterBoForPreview)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Document Name"

        Public Const DocumentUploadName As String = "DocumentUploadName"

#Region "Set Document Name"
        Public Shared Sub setDocumentUploadName(ByRef documentUploadName As String)
            Current.Session(SessionManager.DocumentUploadName) = documentUploadName
        End Sub
#End Region


#Region "get Document Upload Name"
        Public Shared Function getDocumentUploadName() As String
            If (IsNothing(Current.Session(SessionManager.DocumentUploadName))) Then
                Return False
            Else
                Return CType(Current.Session(SessionManager.DocumentUploadName), String)
            End If
        End Function
#End Region


#Region "remove Document Name"
        Public Shared Sub removeDocumentUploadName()
            If (Not IsNothing(Current.Session(SessionManager.DocumentUploadName))) Then
                Current.Session.Remove(SessionManager.DocumentUploadName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Activity Letter Detail"

        Public Const ActivityLetterDataSet As String = "ActivityLetterDataSet"

#Region "Set Activity Letter Detail"
        Public Shared Sub setActivityLetterDetail(ByRef dt As DataTable)
            Current.Session(SessionManager.ActivityLetterDataSet) = dt
        End Sub
#End Region

#Region "Get Activity Letter Detail"
        Public Shared Function getActivityLetterDetail() As DataTable
            Return CType(Current.Session(SessionManager.ActivityLetterDataSet), DataTable)
        End Function
#End Region

#Region "Remove Activity Letter Detail"
        Public Shared Sub removeActivityLetterDetail()
            If (Not IsNothing(Current.Session(SessionManager.ActivityLetterDataSet))) Then
                Current.Session.Remove(SessionManager.ActivityLetterDataSet)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Photo Name"

        Public Const PhotoUploadName As String = "PhotoName"
#Region "Set Photo Name"
        Public Shared Sub setPhotoUploadName(ByRef photoUploadName As String)
            Current.Session(SessionManager.PhotoUploadName) = photoUploadName
        End Sub
#End Region

#Region "get Photo Upload Name"
        Public Shared Function getPhotoUploadName() As String
            If (IsNothing(Current.Session(SessionManager.PhotoUploadName))) Then
                Return False
            Else
                Return CType(Current.Session(SessionManager.PhotoUploadName), String)
            End If
        End Function
#End Region

#Region "remove Photo Name"
        Public Shared Sub removePhotoUploadName()
            If (Not IsNothing(Current.Session(SessionManager.PhotoUploadName))) Then
                Current.Session.Remove(SessionManager.PhotoUploadName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Activity Standard Letter Id"

        Public Const ActivityStandardLetterId As String = "ActivityStandardLetterId"

#Region "Set Activity Standard Letter Id"
        Public Shared Sub setActivityStandardLetterId(ByRef standardLetterId As Integer)
            Current.Session(SessionManager.ActivityStandardLetterId) = standardLetterId
        End Sub
#End Region

#Region "Get Activity Standard Letter Id"
        Public Shared Function getActivityStandardLetterId() As Integer
            If (IsNothing(Current.Session(SessionManager.ActivityStandardLetterId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.ActivityStandardLetterId), Integer)
            End If

        End Function
#End Region

#Region "remove Activity Standard Letter Id"
        Public Shared Sub removeActivityStandardLetterId()
            If (Not IsNothing(Current.Session(SessionManager.ActivityStandardLetterId))) Then
                Current.Session.Remove(SessionManager.ActivityStandardLetterId)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Misc Appointment BO"
        ''' <summary>
        ''' This key shall be used to get letter bo for preview
        ''' </summary>
        ''' <remarks></remarks>
        Public Const MiscAppointmentBO As String = "MiscAppointmentBO"

#Region "Set Misc Appointment BO"
        ''' <summary>
        ''' This function shall be used to save Misc Appointment BO in session
        ''' </summary>
        ''' <param name="miscAppointmentBo"></param>
        ''' <remarks></remarks>
        Public Shared Sub setMiscAppointmentBO(ByRef miscAppointmentBo As MiscAppointmentBO)
            Current.Session(SessionManager.MiscAppointmentBO) = miscAppointmentBo
        End Sub

#End Region

#Region "get Misc Appointment BO"
        ''' <summary>
        ''' This function shall be used to get Misc Appointment BO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getMiscAppointmentBO() As MiscAppointmentBO
            If (IsNothing(Current.Session(SessionManager.MiscAppointmentBO))) Then
                Dim miscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
                Return miscAppointmentBO
            Else
                Return CType(Current.Session.Item(SessionManager.MiscAppointmentBO), MiscAppointmentBO)
            End If
        End Function

#End Region

#Region "remove  Misc Appointment BO"
        ''' <summary>
        ''' This function shall be used to remove  Misc Appointment BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeMiscAppointmentBO()
            If (Not IsNothing(Current.Session(SessionManager.MiscAppointmentBO))) Then
                Current.Session.Remove(SessionManager.MiscAppointmentBO)
            End If
        End Sub

#End Region

#End Region

        Public Const SignatureUploadName As String = "SignatureName"

#Region "Set / Get Available Operatives Ds"
        ''' <summary>
        ''' This key shall be used to get and set the available operatives' data Set
        ''' </summary>
        ''' <remarks></remarks>
        Public Const AvailableOperativesDs As String = "AvailableOperativesDs"
#Region "Set Available Operatives Ds"
        ''' <summary>
        ''' This function shall be used to save the available operatives' dataset in session
        ''' </summary>
        ''' <param name="availableOperativesDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setAvailableOperativesDs(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionManager.AvailableOperativesDs) = availableOperativesDs
        End Sub
#End Region

#Region "Get Available Operatives Ds"
        ''' <summary>
        ''' This function shall be used to get the available operatives' dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAvailableOperativesDs() As DataSet
            If (IsNothing(Current.Session(SessionManager.AvailableOperativesDs))) Then
                Return New DataSet
            Else
                Return CType(Current.Session(SessionManager.AvailableOperativesDs), DataSet)
            End If
        End Function

#End Region

#Region "remove Available Operatives Ds"
        ''' <summary>
        ''' This function shall be used to remove the available operatives' dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeAvailableOperativesDs()
            If (Not IsNothing(Current.Session(SessionManager.AvailableOperativesDs))) Then
                Current.Session.Remove(SessionManager.AvailableOperativesDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Temp Trade Appointment Block List"
        ''' <summary>
        ''' This key shall be used to get and set the temporary trades + appointments block list in session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TempTradeAppointmetBlockList As String = "TempTradeAppointmetBlockList"
#Region "Set Temp Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to save the temporary trades + appointments block list in session. This list contains the trades and their expected appointments slot.
        ''' </summary>
        ''' <param name="tempTradeAptBoList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTempTradeAppointmetBlockList(ByRef tempTradeAptBoList As List(Of TempTradeAppointmentBO))
            Current.Session(SessionManager.TempTradeAppointmetBlockList) = tempTradeAptBoList
        End Sub
#End Region

#Region "Get Temp Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to get the temporary trades + appointments block list from session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTempTradeAppointmetBlockList() As List(Of TempTradeAppointmentBO)
            If (IsNothing(Current.Session(SessionManager.TempTradeAppointmetBlockList))) Then
                Return New List(Of TempTradeAppointmentBO)
            Else
                Return CType(Current.Session(SessionManager.TempTradeAppointmetBlockList), List(Of TempTradeAppointmentBO))
            End If
        End Function

#End Region

#Region "remove Temp Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to remove the temporary trades + appointments block list from session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTempTradeAppointmetBlockList()
            If (Not IsNothing(Current.Session(SessionManager.TempTradeAppointmetBlockList))) Then
                Current.Session.Remove(SessionManager.TempTradeAppointmetBlockList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This key shall be used to get and set the confirmed trades + appointments block list in session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConfirmTradeAppointmetBlockList As String = "ConfirmTradeAppointmetBlockList"
#Region "Set Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to save the confirmed trades + appointments block list in session. This list contains the trades and their appointments slot.
        ''' </summary>
        ''' <param name="ConfirmTradeAptBoList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConfirmTradeAppointmetBlockList(ByRef confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO))
            Current.Session(SessionManager.ConfirmTradeAppointmetBlockList) = confirmTradeAptBoList
        End Sub
#End Region

#Region "Get Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to get the confirmed trades + appointments block list from session. This list contains the trades and their appointments slot
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConfirmTradeAppointmetBlockList() As List(Of ConfirmTradeAppointmentBO)
            If (IsNothing(Current.Session(SessionManager.ConfirmTradeAppointmetBlockList))) Then
                Return New List(Of ConfirmTradeAppointmentBO)
            Else
                Return CType(Current.Session(SessionManager.ConfirmTradeAppointmetBlockList), List(Of ConfirmTradeAppointmentBO))
            End If
        End Function

#End Region

#Region "remove Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to remove the confirmed trades + appointments block list from session. This list contains the trades and their appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeConfirmTradeAppointmetBlockList()
            If (Not IsNothing(Current.Session(SessionManager.ConfirmTradeAppointmetBlockList))) Then
                Current.Session.Remove(SessionManager.ConfirmTradeAppointmetBlockList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Removed Trade Appointment Block List"
        ''' <summary>
        ''' This key shall be used to get and set the removed trades + appointments block list in session. This list contains the trades.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const RemovedTradeAppointmetBlockList As String = "RemovedTradeAppointmetBlockList"
#Region "Set Removed Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to save the removed trades + appointments block list in session. This list contains the trades. 
        ''' </summary>
        ''' <param name="removedTradeAppointmentBOList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setRemovedTradeAppointmetBlockList(ByRef removedTradeAppointmentBOList As List(Of RemovedTradeAppointmentBO))
            Current.Session(SessionManager.RemovedTradeAppointmetBlockList) = removedTradeAppointmentBOList
        End Sub
#End Region

#Region "Get Removed Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to get the removed trades + appointments block list from session. This list contains the trades
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getRemovedTradeAppointmentBOList() As List(Of RemovedTradeAppointmentBO)
            If (IsNothing(Current.Session(SessionManager.RemovedTradeAppointmetBlockList))) Then
                Return New List(Of RemovedTradeAppointmentBO)
            Else
                Return CType(Current.Session(SessionManager.RemovedTradeAppointmetBlockList), List(Of RemovedTradeAppointmentBO))
            End If
        End Function

#End Region

#Region "remove Removed Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to remove the confirmed trades + appointments block list from session. This list contains the trades.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeRemovedTradeAppointmetBlockList()
            If (Not IsNothing(Current.Session(SessionManager.RemovedTradeAppointmetBlockList))) Then
                Current.Session.Remove(SessionManager.RemovedTradeAppointmetBlockList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Planned Appointment BO"
        ''' <summary>
        ''' This key shall be used to get/set planned appointment bo 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const PlannedAppointmentBO As String = "PlannedAppointmentBO"

#Region "Set Planned Appointments BO"
        ''' <summary>
        ''' This function shall be used to save Planned Appointment BO in session
        ''' </summary>
        ''' <param name="plannedAppointmentBo"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPlannedAppointmentBO(ByRef plannedAppointmentBo As PlannedAppointmentBO)
            Current.Session(SessionManager.PlannedAppointmentBO) = plannedAppointmentBo
        End Sub

#End Region

#Region "get Planned Appointment BO"
        ''' <summary>
        ''' This function shall be used to get Planned Appointment BO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPlannedAppointmentBO() As PlannedAppointmentBO
            If (IsNothing(Current.Session(SessionManager.PlannedAppointmentBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.PlannedAppointmentBO), PlannedAppointmentBO)
            End If
        End Function

#End Region

#Region "remove Planned Appointments BO"
        ''' <summary>
        ''' This function shall be used to remove Planned Appointment BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePlannedAppointmentBO()
            If (Not IsNothing(Current.Session(SessionManager.PlannedAppointmentBO))) Then
                Current.Session.Remove(SessionManager.PlannedAppointmentBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Planned Scheduling BO"
        ''' <summary>
        ''' This key shall be used to get/set planned scheduling bo 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const PlannedSchedulingBo As String = "PlannedSchedulingBO"

#Region "Set Planned Scheduling Bo"
        ''' <summary>
        ''' This function shall be used to save Planned Scheduling Bo in session
        ''' </summary>
        ''' <param name="plannedScheduling"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPlannedSchedulingBo(ByRef plannedScheduling As PlannedSchedulingBO)
            Current.Session(SessionManager.PlannedSchedulingBo) = plannedScheduling
        End Sub

#End Region

#Region "get Planned Scheduling Bo"
        ''' <summary>
        ''' This function shall be used to get Planned Scheduling Bo from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPlannedSchedulingBo() As PlannedSchedulingBO
            If (IsNothing(Current.Session(SessionManager.PlannedSchedulingBo))) Then
                Return New PlannedSchedulingBO()
            Else
                Return CType(Current.Session.Item(SessionManager.PlannedSchedulingBo), PlannedSchedulingBO)
            End If
        End Function

#End Region

#Region "remove Planned BO"
        ''' <summary>
        ''' This function shall be used to remove Planned Scheduling BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePlannedSchedulingBo()
            If (Not IsNothing(Current.Session(SessionManager.PlannedSchedulingBo))) Then
                Current.Session.Remove(SessionManager.PlannedSchedulingBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Planned Duration Dictionary"
        ''' <summary>
        ''' This key shall be used to get/set Planned Duration Dictionary
        ''' </summary>
        ''' <remarks></remarks>
        Public Const PlannedDurationDict As String = "PlannedDurationDict"

#Region "Set Planned Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to save Planned Duration Dictionary in session
        ''' </summary>
        ''' <param name="objPlannedDuration"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPlannedDurationDict(ByRef objPlannedDuration As Dictionary(Of Integer, Double))
            Current.Session(SessionManager.PlannedDurationDict) = objPlannedDuration
        End Sub

#End Region

#Region "Get Planned Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to get Planned Duration Dictionary from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPlannedDurationDict() As Dictionary(Of Integer, Double)
            If (IsNothing(Current.Session(SessionManager.PlannedDurationDict))) Then
                Return New Dictionary(Of Integer, Double)
            Else
                Return CType(Current.Session.Item(SessionManager.PlannedDurationDict), Dictionary(Of Integer, Double))
            End If
        End Function

#End Region

#Region "Remove Planned Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to remove Planned Duration Dictionary from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePlannedDurationDict()
            If (Not IsNothing(Current.Session(SessionManager.PlannedDurationDict))) Then
                Current.Session.Remove(SessionManager.PlannedDurationDict)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Print Letter Bo"
        Public Shared PrintLetterBo As String = "PrintLetterBo"
#Region "Set Print Letter Bo"
        Public Shared Sub setPrintLetterBo(ByRef printLetterBo As PrintLetterBO)
            Current.Session(SessionManager.PrintLetterBo) = printLetterBo
        End Sub
#End Region

#Region "Get Print Letter Bo"
        Public Shared Function getPrintLetterBo() As PrintLetterBO
            If (IsNothing(Current.Session(SessionManager.PrintLetterBo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.PrintLetterBo), PrintLetterBO)
            End If
        End Function
#End Region

#Region "remove Print Letter Bo"
        Public Shared Sub removePrintLetterBo()
            If (Not IsNothing(Current.Session(SessionManager.PrintLetterBo))) Then
                Current.Session.Remove(SessionManager.PrintLetterBo)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Property Id For Print"
        Public Const PropertyId As String = "PropertyIdForPrint"
#Region "Set Property Id For Edit Letter"
        Public Shared Sub setPropertyIdForEditLetter(ByRef PropertyId As String)
            Current.Session(SessionManager.PropertyId) = PropertyId
        End Sub
#End Region

#Region "Get Property Id For Edit Letter"
        Public Shared Function getPropertyIdForEditLetter() As String
            If (IsNothing(Current.Session(SessionManager.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionManager.PropertyId), String)
            End If

        End Function
#End Region
#End Region

#Region "Set / Get To Scheduling"
        Public Const ToScheduling As String = "ToScheduling"
#Region "Set To Scheduling"
        Public Shared Sub setToScheduling(ByRef ToScheduling As Boolean)
            Current.Session(SessionManager.ToScheduling) = ToScheduling
        End Sub
#End Region

#Region "Get ToScheduling"
        Public Shared Function getToScheduling() As String
            If (IsNothing(Current.Session(SessionManager.ToScheduling))) Then
                Return False
            Else
                Return CType(Current.Session(SessionManager.ToScheduling), Boolean)
            End If

        End Function
#End Region

#Region "remove ToScheduling"
        Public Shared Sub removeToScheduling()
            If (Not IsNothing(Current.Session(SessionManager.ToScheduling))) Then
                Current.Session.Remove(SessionManager.ToScheduling)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Appointment Summary Source"
        ''' <summary>
        ''' This 'll key 'll be used to save the appointment summary source . based on this different actions 'll be performed
        ''' </summary>
        ''' <remarks></remarks>
        Public Const AppointmentSummarySource As String = "AppointmentSummarySource"
#Region "Set Appointment Summary Source"
        ''' <summary>
        ''' This 'll function 'll be used to save the appointment summary source . based on this different actions 'll be performed
        ''' </summary>
        ''' <param name="src"></param>
        ''' <remarks></remarks>
        Public Shared Sub setAppointmentSummarySource(ByRef src As String)
            Current.Session(SessionManager.AppointmentSummarySource) = src
        End Sub
#End Region

#Region "get Appointment Summary Source"
        ''' <summary>
        ''' This 'll function 'll be used to get the appointment summary source from session. Based on this different actions 'll be performed
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentSummarySource() As String
            If (IsNothing(Current.Session(SessionManager.AppointmentSummarySource))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionManager.AppointmentSummarySource), String)
            End If
        End Function
#End Region

#Region "remove Appointment Summary Source"
        ''' <summary>
        ''' This 'll function 'll be used to get the appointment summary source from session. Based on this different actions 'll be performed
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeAppointmentSummarySource()
            If (Not IsNothing(Current.Session(SessionManager.AppointmentSummarySource))) Then
                Current.Session.Remove(SessionManager.AppointmentSummarySource)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Replacement List Dataset"
        ''' <summary>
        ''' This 'll key 'll be used to save the replacement list data set when user selects the generated list button
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ReplacementListDataSet As String = "ReplacementListDataSet"
#Region "Set Replacement List DataSet"
        ''' <summary>
        ''' This 'll function 'll be used to save the replacement list data set when user selects the generated list button
        ''' </summary>
        ''' <param name="src"></param>
        ''' <remarks></remarks>
        Public Shared Sub setReplacementListDataSet(ByRef src As DataSet)
            Current.Session(SessionManager.ReplacementListDataSet) = src
        End Sub
#End Region

#Region "get Replacement List DataSet"
        ''' <summary>
        ''' This 'll function 'll be used to get the replacement list data,when appointment 'll be arranged successfully
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getReplacementListDataSet() As DataSet
            If (IsNothing(Current.Session(SessionManager.ReplacementListDataSet))) Then
                Return New DataSet
            Else
                Return CType(Current.Session(SessionManager.ReplacementListDataSet), DataSet)
            End If
        End Function
#End Region

#Region "remove Replacement List DataSet"
        ''' <summary>
        ''' This 'll function 'll be used to remove the replacement list data set from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeReplacementListDataSet()
            If (Not IsNothing(Current.Session(SessionManager.ReplacementListDataSet))) Then
                Current.Session.Remove(SessionManager.ReplacementListDataSet)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Misc Available Operatives Appointments"
        ''' <summary>
        ''' This 'll key 'll be used to save the oerative appointments that were displayed on misc works screen
        ''' </summary>
        ''' <remarks></remarks>
        Public Const MiscAvailableOperativesAppointments As String = "MiscAvailableOperativesAppointments"
#Region "Set  Misc Available Operatives Appointments"
        ''' <summary>
        ''' This 'll function 'll be used to save the perative appointments that were displayed on misc works screen
        ''' </summary>
        ''' <param name="appointments"></param>
        ''' <remarks></remarks>
        Public Shared Sub setMiscAvailableOperativesAppointments(ByRef appointments As TempAppointmentDtBO)
            Current.Session(SessionManager.MiscAvailableOperativesAppointments) = appointments
        End Sub
#End Region

#Region "get Misc Available Operatives Appointments"
        ''' <summary>
        ''' This 'll function 'll be used to get the oerative appointments that were displayed on misc works screen
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getMiscAvailableOperativesAppointments() As TempAppointmentDtBO
            If (IsNothing(Current.Session(SessionManager.MiscAvailableOperativesAppointments))) Then
                Return New TempAppointmentDtBO
            Else
                Return CType(Current.Session(SessionManager.MiscAvailableOperativesAppointments), TempAppointmentDtBO)
            End If
        End Function
#End Region

#Region "remove  Misc Available Operatives Appointments"
        ''' <summary>
        ''' This 'll function 'll be used to remove the oerative appointments that were displayed on misc works screen
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeMiscAvailableOperativesAppointments()
            If (Not IsNothing(Current.Session(SessionManager.MiscAvailableOperativesAppointments))) Then
                Current.Session.Remove(SessionManager.MiscAvailableOperativesAppointments)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Component Year Search BO"
        Public Shared ComponentYearSearchBO As String = "ComponentYearSearchBO"

#Region "Set Print Component Year Search BO"
        Public Shared Sub setComponentYearSearchBO(ByRef componentYearSearchBO As ComponentYearSearchBO)
            Current.Session(SessionManager.ComponentYearSearchBO) = componentYearSearchBO
        End Sub
#End Region

#Region "Get Component Year Search BO"
        Public Shared Function getComponentYearSearchBO() As ComponentYearSearchBO
            If (IsNothing(Current.Session(SessionManager.ComponentYearSearchBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.ComponentYearSearchBO), ComponentYearSearchBO)
            End If
        End Function
#End Region

#Region "remove Component Year Search BO"
        Public Shared Sub removeComponentYearSearchBO()
            If (Not IsNothing(Current.Session(SessionManager.ComponentYearSearchBO))) Then
                Current.Session.Remove(SessionManager.ComponentYearSearchBO)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Data Row"
        Public Shared dr As String = "DataRow"

#Region "Set dr"
        Public Shared Sub setDataRow(ByRef dr As DataRow())
            Current.Session(SessionManager.dr) = dr
        End Sub
#End Region

#Region "Get dr"
        Public Shared Function getdr() As DataRow()
            If (IsNothing(Current.Session(SessionManager.dr))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.dr), DataRow())
            End If
        End Function
#End Region

#Region "remove dr"
        Public Shared Sub removedr()
            If (Not IsNothing(Current.Session(SessionManager.dr))) Then
                Current.Session.Remove(SessionManager.dr)
            End If
        End Sub
#End Region

#End Region

#Region "Edited Component Data Table, Set and Remove Method/Function(s)"
        ''' <summary>
        ''' This key will be used to save the edited components
        ''' </summary>
        ''' <remarks></remarks>
        Public Const EditedComponentDataTable As String = "EditedComponentDataTable"
#Region "Set Edited Component Data Table"
        ''' <summary>
        ''' This function shall be used to save Edited Component Data Table in session
        ''' </summary>
        ''' <param name="objEditedComponentDataTable"></param>
        ''' <remarks></remarks>
        Public Shared Sub setEditedComponentDataTable(ByRef objEditedComponentDataTable As DataTable)
            Current.Session(EditedComponentDataTable) = objEditedComponentDataTable
        End Sub

#End Region

#Region "Get Edited Component Data Table"
        ''' <summary>
        ''' This function shall be used to get Edited Component Data Table from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getEditedComponentDataTable() As DataTable
            If (IsNothing(Current.Session(EditedComponentDataTable))) Then
                Dim table As New DataTable
                table.Columns.Add("COMPONENTID", GetType(Int16))
                table.Columns.Add("COMPONENTNAME", GetType(String))
                table.Columns.Add("CYCLE", GetType(Integer))
                table.Columns.Add("FREQUENCY", GetType(String))
                table.Columns.Add("MATERIALCOST", GetType(Double))
                table.Columns.Add("LABOURCOST", GetType(Double))
                table.Columns.Add("COSTFLAG", GetType(Boolean))
                table.Columns.Add("CYCLEFLAG", GetType(Boolean))
                Return table
            Else
                Return CType(Current.Session(EditedComponentDataTable), DataTable)
            End If
        End Function

#End Region

#Region "Remove Edited Component Data Table"
        ''' <summary>
        ''' This function shall be used to remove saved Edited Component Data Table from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeEditedComponentDataTable()
            If (Not IsNothing(Current.Session(EditedComponentDataTable))) Then
                Current.Session.Remove(EditedComponentDataTable)
            End If
        End Sub

#End Region

#End Region

#Region "Updated Property DueDate Data Table, Set and Remove Method/Function(s)"
        ''' <summary>
        ''' This key will be used to save the Updated Property DueDate
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdatedPropertyDueDatesDataTable As String = "UpdatedPropertyDueDatesDataTable"

#Region "Set Updated Property DueDate Data Table"
        ''' <summary>
        ''' This function shall be used to save Updated Property DueDate Data Table in session
        ''' </summary>
        ''' <param name="objUpdatedPropertyDueDatesDataTable"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUpdatedPropertyDueDatesDataTable(ByRef objUpdatedPropertyDueDatesDataTable As DataTable)
            Current.Session(UpdatedPropertyDueDatesDataTable) = objUpdatedPropertyDueDatesDataTable
        End Sub

#End Region

#Region "Get Updated Property DueDate Data Table"
        ''' <summary>
        ''' This function shall be used to get Updated Property DueDate Data Table from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUpdatedPropertyDueDatesDataTable() As DataTable
            If (IsNothing(Current.Session(UpdatedPropertyDueDatesDataTable))) Then
                Dim table As New DataTable
                table.Columns.Add("SID", GetType(Integer))
                table.Columns.Add("DUEDATE", GetType(String))
                Return table
            Else
                Return CType(Current.Session(UpdatedPropertyDueDatesDataTable), DataTable)
            End If
        End Function

#End Region

#Region "Remove Updated Property DueDate Data Table"
        ''' <summary>
        ''' This function shall be used to remove saved Updated Property DueDate Data Table from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUpdatedPropertyDueDatesDataTable()
            If (Not IsNothing(Current.Session(UpdatedPropertyDueDatesDataTable))) Then
                Current.Session.Remove(UpdatedPropertyDueDatesDataTable)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Next 5 Years Count"
        ''' <summary>
        ''' Increase the count on every click
        ''' </summary>
        ''' <remarks></remarks>
        Public Const next5YearsCount As String = "Next5YearsCount"
#Region "Set Next 5 Years Count"
        ''' <summary>
        ''' set the count
        ''' </summary>
        ''' <param name="src"></param>
        ''' <remarks></remarks>
        Public Shared Sub setnext5YearsCount(ByRef src As Integer)
            Current.Session(SessionManager.next5YearsCount) = src
        End Sub
#End Region

#Region "get Next 5 Years Count"
        ''' <summary>
        ''' get the count of next 5years button
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getnext5YearsCount() As Integer
            If (IsNothing(Current.Session(SessionManager.next5YearsCount))) Then
                Return New Integer
            Else
                Return CType(Current.Session(SessionManager.next5YearsCount), Integer)
            End If
        End Function
#End Region

#Region "remove Next 5 Years Count"
        ''' <summary>
        ''' remmove the count from Session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removenext5YearsCount()
            If (Not IsNothing(Current.Session(SessionManager.next5YearsCount))) Then
                Current.Session.Remove(SessionManager.next5YearsCount)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Edited Component BO"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const EditedComponentBO As String = "EditedComponentBO"
#Region "Set Edited Component BO"
        ''' <summary>
        ''' set the objEditedComponentBO
        ''' </summary>
        ''' <param name="objEditedComponentBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setEditedComponentBO(ByRef objEditedComponentBO As EditedComponentBO)
            Current.Session(SessionManager.EditedComponentBO) = objEditedComponentBO
        End Sub
#End Region

#Region "get Edited Component BO"
        ''' <summary>
        ''' get the objEditedComponentBO
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getEditedComponentBO() As EditedComponentBO
            If (IsNothing(Current.Session(SessionManager.EditedComponentBO))) Then
                Return New EditedComponentBO
            Else
                Return CType(Current.Session(SessionManager.EditedComponentBO), EditedComponentBO)
            End If
        End Function
#End Region

#Region "remove Edited Component BO"
        ''' <summary>
        ''' remmove the objEditedComponentBO from Session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeEditedComponentBO()
            If (Not IsNothing(Current.Session(SessionManager.EditedComponentBO))) Then
                Current.Session.Remove(SessionManager.EditedComponentBO)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Selected Row Index"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const SelectedRowIndex As String = "SelectedRowIndex"
#Region "Set SelectedRowIndex"
        ''' <summary>
        ''' set the objEditedComponentBO
        ''' </summary>
        ''' <param name="SelectedRowIndex"></param>
        ''' <remarks></remarks>
        Public Shared Sub setSelectedRowIndex(ByRef SelectedRowIndex As Integer)
            Current.Session(SessionManager.SelectedRowIndex) = SelectedRowIndex
        End Sub
#End Region

#Region "get SelectedRowIndex"
        ''' <summary>
        ''' get the objEditedComponentBO
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getSelectedRowIndex() As Integer
            If (IsNothing(Current.Session(SessionManager.SelectedRowIndex))) Then
                Return New Integer
            Else
                Return CType(Current.Session(SessionManager.SelectedRowIndex), Integer)
            End If
        End Function
#End Region

#Region "remove SelectedRowIndex"
        ''' <summary>
        ''' remmove the objEditedComponentBO from Session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeSelectedRowIndex()
            If (Not IsNothing(Current.Session(SessionManager.SelectedRowIndex))) Then
                Current.Session.Remove(SessionManager.SelectedRowIndex)
            End If

        End Sub
#End Region



#End Region

#Region "Set / Get Initial DataTable"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const InitialDataTable As String = "InitialDataTable"
#Region "Set SelectedRowIndex"
        ''' <summary>
        ''' set the InitialDataTable
        ''' </summary>
        ''' <param name="InitialDataTable"></param>
        ''' <remarks></remarks>
        Public Shared Sub setInitialDataTable(ByRef InitialDataTable As DataTable)
            Current.Session(SessionManager.InitialDataTable) = InitialDataTable
        End Sub
#End Region

#Region "get Initial DataTable"
        ''' <summary>
        ''' get the InitialDataTable
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getInitialDataTable() As DataTable
            If (IsNothing(Current.Session(SessionManager.InitialDataTable))) Then
                Return New DataTable
            Else
                Return CType(Current.Session(SessionManager.InitialDataTable), DataTable)
            End If
        End Function
#End Region

#Region "remove SelectedRowIndex"
        ''' <summary>
        ''' remmove the InitialDataTable from Session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeInitialDataTable()
            If (Not IsNothing(Current.Session(SessionManager.InitialDataTable))) Then
                Current.Session.Remove(SessionManager.InitialDataTable)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get  Expenditure BO List"

        ''' <summary>
        ''' This key is to manage List of Expenditure BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ExpenditureBOList As String = "ExpenditureBOList"
        ''' <summary>
        ''' To get Expenditure BO list, this is saved in view state to get LIMIT and Remaining LIMIT of the current employee.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getExpenditureBOList() As List(Of ExpenditureBO)
            Dim expenditureBOLst As List(Of ExpenditureBO) = TryCast(Current.Session(ExpenditureBOList), List(Of ExpenditureBO))
            If IsNothing(expenditureBOLst) Then
                expenditureBOLst = New List(Of ExpenditureBO)
            End If
            Return expenditureBOLst
        End Function

        Public Shared Sub removeExpenditureBOList()
            Current.Session.Remove(ExpenditureBOList)
        End Sub

        Public Shared Sub setExpenditureBOList(ByVal expenditureBOLst As List(Of ExpenditureBO))
            Current.Session(ExpenditureBOList) = expenditureBOLst
        End Sub

#End Region

#Region " Set / Get Vat BO List"

        ''' <summary>
        ''' This key is to manage List of Vat BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const VatBoList As String = "VatBoList"

        ''' <summary>
        ''' To get Vat BO list, this is saved in view state to get Vat rate for the current vat item.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getVatBOList() As List(Of VatBo)
            Dim VatBoLst As List(Of VatBo) = TryCast(Current.Session(VatBoList), List(Of VatBo))
            If IsNothing(VatBoLst) Then
                VatBoLst = New List(Of VatBo)
            End If
            Return VatBoLst
        End Function

        Public Shared Sub removeVatBOList()
            Current.Session.Remove(VatBoList)
        End Sub

        Public Shared Sub setVatBOList(ByRef VatBoLst As List(Of VatBo))
            Current.Session(VatBoList) = VatBoLst
        End Sub

#End Region

#Region "Set / Get Page Access DataSet"
        ''' <summary>
        ''' This key will be used to save the Page Access DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PageAccessDataSet As String = "PageAccessDataSet"

#Region "Set Page Access DataSet"
        ''' <summary>
        ''' This function shall be used Page Access DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPageAccessDataSet(ByRef ds As DataSet)
            Current.Session(PageAccessDataSet) = ds
        End Sub

#End Region

#Region "Get Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to get Page Access DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPageAccessDataSet()
            If (IsNothing(Current.Session(PageAccessDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PageAccessDataSet), DataSet)
            End If
        End Function

#End Region

#Region "Remove Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePageAccessDataSet()
            If (Not IsNothing(Current.Session(PageAccessDataSet))) Then
                Current.Session.Remove(PageAccessDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Condition SMS BO"
        ''' <summary>
        ''' This key shall be used to get/set Condition SMS BO
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConditionSmsBO As String = "ConditionSmsBO"
#End Region
#Region "Set Condition SMS BO"
        ''' <summary>
        ''' This function shall be used to save Condition Sms BO in session
        ''' </summary>
        ''' <param name="objConditionSmsBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConditionSmsBo(ByRef objConditionSmsBO As ConditionSmsBO)
            Current.Session(SessionManager.ConditionSmsBO) = objConditionSmsBO
        End Sub

#End Region

#Region "Set / Get Condition EMAIL BO"
        ''' <summary>
        ''' This key shall be used to get/set Condition Email BO
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConditionEmailBO As String = "ConditionEmailBO"


#Region "Set Condition EMAIL BO"
        ''' <summary>
        ''' This function shall be used to save Condition Email BO in session
        ''' </summary>
        ''' <param name="objConditionEmailBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConditionEmailBO(ByRef objConditionEmailBO As ConditionEmailBO)
            Current.Session(SessionManager.ConditionEmailBO) = objConditionEmailBO
        End Sub

#End Region

#Region "Get Condition SMS BO"
        ''' <summary>
        ''' This function shall be used to get Condition SMS BO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConditionSmsBo() As ConditionSmsBO
            If (IsNothing(Current.Session(SessionManager.ConditionSmsBO))) Then
                Return New ConditionSmsBO()
            Else
                Return CType(Current.Session.Item(SessionManager.ConditionSmsBO), ConditionSmsBO)
            End If
        End Function

#End Region

#Region "Get Condition Email BO"
        ''' <summary>
        ''' This function shall be used to get Condition SMS BO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConditionEmailBo() As ConditionEmailBO
            If (IsNothing(Current.Session(SessionManager.ConditionEmailBO))) Then
                Return New ConditionEmailBO()
            Else
                Return CType(Current.Session.Item(SessionManager.ConditionEmailBO), ConditionEmailBO)
            End If
        End Function

#End Region

#Region "Remove Condition SMS BO"
        ''' <summary>
        ''' This function shall be used to remove Condition SMS BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeConditionSmsBo()
            If (Not IsNothing(Current.Session(SessionManager.ConditionSmsBO))) Then
                Current.Session.Remove(SessionManager.ConditionSmsBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Temp Misc Appointment Block List"
        ''' <summary>
        ''' This key shall be used to get and set the temporary trades + appointments block list in session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TempMiscAppointmetBlockList As String = "TempMiscAppointmetBlockList"
#Region "Set Temp Misc Appointment Block List"
        ''' <summary>
        ''' This function shall be used to save the temporary Misc trades + appointments block list in session. This list contains the trades and their expected appointments slot.
        ''' </summary>
        ''' <param name="tempMiscAptBoList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTempMiscAppointmetBlockList(ByRef tempMiscAptBoList As List(Of TempMiscAppointmentBO))
            Current.Session(SessionManager.TempMiscAppointmetBlockList) = tempMiscAptBoList
        End Sub
#End Region

#Region "Get Temp Misc Appointment Block List"
        ''' <summary>
        ''' This function shall be used to get the temporary Misc trades + appointments block list from session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTempMiscAppointmetBlockList() As List(Of TempMiscAppointmentBO)
            If (IsNothing(Current.Session(SessionManager.TempMiscAppointmetBlockList))) Then
                Return New List(Of TempMiscAppointmentBO)
            Else
                Return CType(Current.Session(SessionManager.TempMiscAppointmetBlockList), List(Of TempMiscAppointmentBO))
            End If
        End Function

#End Region

#Region "remove Temp Misc Appointment Block List"
        ''' <summary>
        ''' This function shall be used to remove the temporary Misc trades + appointments block list from session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTempMiscAppointmetBlockList()
            If (Not IsNothing(Current.Session(SessionManager.TempMiscAppointmetBlockList))) Then
                Current.Session.Remove(SessionManager.TempMiscAppointmetBlockList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This key shall be used to get and set the confirmed trades + appointments block list in session. This list contains the trades and their expected appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConfirmMiscAppointmetBlockList As String = "ConfirmTradeAppointmetBlockList"
#Region "Set Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to save the confirmed trades + appointments block list in session. This list contains the trades and their appointments slot.
        ''' </summary>
        ''' <param name="confirmMiscAptBoList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConfirmMiscAppointmetBlockList(ByRef confirmMiscAptBoList As List(Of ConfirmMiscAppointmentBO))
            Current.Session(SessionManager.ConfirmMiscAppointmetBlockList) = confirmMiscAptBoList
        End Sub
#End Region

#Region "Get Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to get the confirmed trades + appointments block list from session. This list contains the trades and their appointments slot
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConfirmMiscAppointmetBlockList() As List(Of ConfirmMiscAppointmentBO)
            If (IsNothing(Current.Session(SessionManager.ConfirmMiscAppointmetBlockList))) Then
                Return New List(Of ConfirmMiscAppointmentBO)
            Else
                Return CType(Current.Session(SessionManager.ConfirmMiscAppointmetBlockList), List(Of ConfirmMiscAppointmentBO))
            End If
        End Function

#End Region

#Region "remove Confirm Trade Appointment Block List"
        ''' <summary>
        ''' This function shall be used to remove the confirmed trades + appointments block list from session. This list contains the trades and their appointments slot
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeConfirmMiscAppointmetBlockList()
            If (Not IsNothing(Current.Session(SessionManager.ConfirmMiscAppointmetBlockList))) Then
                Current.Session.Remove(SessionManager.ConfirmMiscAppointmetBlockList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Condition Works Appointment BO"
        ''' <summary>
        ''' This key shall be used to get/set  Condition Works Appointment BO
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ConditionWorksAppointmentBO As String = "ConditionWorksAppointmentBO"

#Region "Set Condition Works Appointment Bo"
        ''' <summary>
        ''' This function shall be used to save Condition Works Appointment Bo in session
        ''' </summary>
        ''' <param name="conditionWorksAppointmentBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setConditionWorksAppointmentBo(ByRef conditionWorksAppointmentBO As ConditionWorksAppointmentBO)
            Current.Session(SessionManager.ConditionWorksAppointmentBO) = conditionWorksAppointmentBO
        End Sub

#End Region

#Region "get Condition Works Appointment Bo"
        ''' <summary>
        ''' This function shall be used to get  Condition Works Appointment Bo from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getConditionWorksAppointmentBo() As ConditionWorksAppointmentBO
            Dim conditionalAppointmentBo = TryCast(Current.Session(SessionManager.ConditionWorksAppointmentBO), ConditionWorksAppointmentBO)
            If IsNothing(conditionalAppointmentBo) Then
                conditionalAppointmentBo = New ConditionWorksAppointmentBO
            End If
            Return conditionalAppointmentBo
        End Function

#End Region

#Region "remove Condition Works Appointment BO"
        ''' <summary>
        ''' This function shall be used to remove Condition Works Appointment BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeConditionWorksAppointmentBo()
            If (Not IsNothing(Current.Session(SessionManager.ConditionWorksAppointmentBO))) Then
                Current.Session.Remove(SessionManager.ConditionWorksAppointmentBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Summary Ds"
        ''' <summary>
        ''' This key shall be used to get and set the job sheet summary data Set
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared objjobSheetSumamryDs As DataSet ' = "JobSheetSummaryDs"

        ''' <summary>
        ''' This function shall be used to save the job sheet summary dataset in session
        ''' </summary>
        ''' <param name="prmjobSheetSumamryDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setJobSheetSummaryDs(ByRef prmjobSheetSumamryDs As DataSet)
            objjobSheetSumamryDs = prmjobSheetSumamryDs
        End Sub

        ''' <summary>
        ''' This function shall be used to get the job sheet summary dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getJobSheetSummaryDs() As DataSet
            If (IsNothing(objjobSheetSumamryDs)) Then
                Return New DataSet
            Else
                Return objjobSheetSumamryDs
            End If
        End Function

        ''' <summary>
        ''' This function shall be used to remove the job sheet summary dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeJobSheetSummaryDs()
            If (Not IsNothing(objjobSheetSumamryDs)) Then
                objjobSheetSumamryDs.Clear()
            End If
        End Sub

#End Region


#Region "Set / Get  Operative  working Hour Ds"

        Public Shared OperativeworkingHourString As String = "OperativeworkingHour"
#Region "Set Operative working Hour Ds"

        Public Shared Sub setOperativeworkingHourDs(ByRef OperativeworkingHour As DataSet)
            Current.Session(OperativeworkingHourString) = OperativeworkingHour
        End Sub

#End Region

#Region "Get Operative working Hour Ds"

        Public Shared Function getOperativeworkingHourDs() As DataSet
            If (IsNothing(Current.Session(OperativeworkingHourString))) Then
                Return Nothing
            Else
                Return CType(Current.Session(OperativeworkingHourString), DataSet)
            End If
        End Function

#End Region

#Region "remove Operative working Hour Ds"

        Public Shared Sub removeOperativeworkingHourDs()
            If (Not IsNothing(Current.Session(OperativeworkingHourString))) Then
                Current.Session.Remove(OperativeworkingHourString)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get Condition Rating Appointment Search"
        Public Shared conditionRatingAptSearch As String = "conditionRatingAptSearch"

#Region "Set Condition Rating Appointment Search"
        Public Shared Sub setConditionRatingAptSearch(ByRef searchTxt As String)
            Current.Session(SessionManager.conditionRatingAptSearch) = searchTxt
        End Sub
#End Region

#Region "Get Condition Rating Appointment Search"
        Public Shared Function getConditionRatingAptSearch() As String
            If (IsNothing(Current.Session(SessionManager.conditionRatingAptSearch))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.conditionRatingAptSearch), String)
            End If
        End Function
#End Region

#Region "Remove Condition Rating Appointment Search"
        Public Shared Sub removeConditionRatingAptSearch()
            If (Not IsNothing(Current.Session(SessionManager.conditionRatingAptSearch))) Then
                Current.Session.Remove(SessionManager.conditionRatingAptSearch)
            End If
        End Sub
#End Region

#End Region

    End Class
End Namespace