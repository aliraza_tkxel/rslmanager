﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="Dashboard.aspx.vb" Inherits="PlannedMaintenance.Dashboard" %>

<%@ Register Src="~/Controls/Dashboard/MapControl.ascx" TagName="MapControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Dashboard/OperativeAppointmentsListControl.ascx" TagName="OperativeAppointmentsListControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {

            getAllCounts(-1);

            function hideLoaders() {
                $("#loadingReplacementCurrentYear").hide();
                $("#loadingReplacementOverdue").hide();
                $("#loadingReplacementNotScheduled").hide();
                $("#loadingReplacementDueScheduled").hide();
                $("#loadingInprogress").hide();
                $("#loadingNoEntries").hide();
                $("#loadingTotalReplacements").hide();

                $("#loadingAdaptationNotScheduled").hide();
                $("#loadingAdaptationScheduled").hide();
                $("#loadingConditionApprovalRequired").hide();
                $("#loadingMiscWorksNotScheduled").hide();
                $("#loadingMiscWorksScheduled").hide();
                $("#loadingConditionWorksApproved").hide();

                $('#<%= lblReplacementCurrentYear.ClientID %>').text("0");
                $('#<%= lblReplacementNotScheduled.ClientID %>').text("0");
                $('#<%= lblReplacementDueScheduled.ClientID %>').text("0");
                $('#<%= lblInprogress.ClientID %>').text("0");
                $('#<%= lblNoEntries.ClientID %>').text("0");
                $('#<%= lblReplacementOverdue.ClientID %>').text("0");
                $('#<%= lblTotalReplacements.ClientID %>').text("0");

                $('#<%= lblAdaptationNotScheduled.ClientID %>').text("0");
                $('#<%= lblAdaptationScheduled.ClientID %>').text("0");
                $('#<%= lblConditionApprovalRequired.ClientID %>').text("0");
                $('#<%= lblMiscWorksNotScheduled.ClientID %>').text("0");
                $('#<%= lblMiscWorksScheduled.ClientID %>').text("0");
                $('#<%= lblConditionWorksApproved.ClientID %>').text("0");
            }

            function getAllCounts(ddlValue) {
                loadCounts("getCountReplacementDueCurrentYear", '#loadingReplacementCurrentYear', '#<%= lblReplacementCurrentYear.ClientID %>', ddlValue);
                loadCounts("getReplacementDueNotScheduleCount", '#loadingReplacementNotScheduled', '#<%= lblReplacementNotScheduled.ClientID %>', ddlValue);
                loadCounts("getReplacementDueScheduleCount", '#loadingReplacementDueScheduled', '#<%= lblReplacementDueScheduled.ClientID %>', ddlValue);
                loadCounts("getInprogressCount", '#loadingInprogress', '#<%= lblInprogress.ClientID %>', ddlValue);
                loadCounts("getNoEntryCount", '#loadingNoEntries', '#<%= lblNoEntries.ClientID %>', ddlValue);
                loadCounts("getReplacementOverdueCount", '#loadingReplacementOverdue', '#<%= lblReplacementOverdue.ClientID %>', ddlValue);
                loadCounts("getTotalReplacementCount", '#loadingTotalReplacements', '#<%= lblTotalReplacements.ClientID %>', ddlValue);

                loadCounts("getAdaptationNotScheduledCount", '#loadingAdaptationNotScheduled', '#<%= lblAdaptationNotScheduled.ClientID %>', ddlValue);
                loadCounts("getAdaptationScheduledCount", '#loadingAdaptationScheduled', '#<%= lblAdaptationScheduled.ClientID %>', ddlValue);
                loadCounts("getConditionWorksApprovalRequiredCount", '#loadingConditionApprovalRequired', '#<%= lblConditionApprovalRequired.ClientID %>', ddlValue);
                loadCounts("getMiscNotScheduledCount", '#loadingMiscWorksNotScheduled', '#<%= lblMiscWorksNotScheduled.ClientID %>', ddlValue);
                loadCounts("getMiscScheduledCount", '#loadingMiscWorksScheduled', '#<%= lblMiscWorksScheduled.ClientID %>', ddlValue);
                loadCounts("getConditionWorksApprovedCount", '#loadingConditionWorksApproved', '#<%= lblConditionWorksApproved.ClientID %>', ddlValue);
            }

            $("#btnUpdateDashboard").click(function () {

                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                getAllCounts(componentID);

                __doPostBack("<%=btnHidden.UniqueID %>", "_Update");
            });

            $("#imgInprogressArrow").click(function () {
                __doPostBack("<%=btnHidden.UniqueID %>", "_Image");
            });

            $("#currentYearArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Lifecycle/ReplacementList.aspx?rep=cy&comp=" + componentID;
                window.location.href = url;
            });

            $("#overDueArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Lifecycle/ReplacementList.aspx?rep=od&comp=" + componentID;
                window.location.href = url;
            });

            $("#totalReplacementsArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Lifecycle/ReplacementList.aspx?rep=all&comp=" + componentID;
                window.location.href = url;
            });

            $("#notScheduleArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?compotentId=" + componentID;
                window.location.href = url;
            });

            $("#scheduleArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?tab=aptArranged&compotentId=" + componentID;
                window.location.href = url;
            });
            //////////////////////////////////////////////////////
            $("#notAdaptationArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?type=Adaptation&compotentId=" + componentID;
                window.location.href = url;
            });

            $("#AdaptationArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?type=Adaptation&tab=aptArranged&compotentId=" + componentID;
                window.location.href = url;
            });

            $("#notMiscArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?type=Misc&compotentId=" + componentID;
                window.location.href = url;
            });

            $("#MiscArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Scheduling/ScheduleWorks.aspx?type=Misc&tab=aptArranged&compotentId=" + componentID;
                window.location.href = url;
            });

            $("#notConditionArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Reports/ConditionRating.aspx?compotentId=" + componentID;
                window.location.href = url;
            });

            $("#ConditionArrow").click(function () {
                var componentID = $('#<%= ddlComponents.ClientID %>').val();
                var url = "../Reports/ConditionRating.aspx?tab=Approved&compotentId=" + componentID;
                window.location.href = url;
            });
            //////////////////////////////////////////////////////
            function loadCounts(url, loading, label, componentID) {
                var params = '{"componentValue":"' + componentID + '"}';

                $.ajax({
                    type: "POST",
                    url: "Dashboard.aspx/" + url,
                    data: params,
                    contentType: "application/json",
                    dataType: "json",
                    beforeSend: function (xhr) {
                        $(label).text("");
                        $(loading).show();
                    },
                    complete: function () {
                        $(loading).hide();
                    },

                    success: function (result) {
                        $(label).text(result.d);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $(label).text("0");
                    }
                });
            }

        });

        $(window).load(function () {

        });

        function contains(a, obj) {
            var i = a.length;
            while (i--) {
                if (a[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        function populateMarker() {

            var postData = JSON.stringify({ componentId: $('#<%= ddlComponents.ClientID %>').val(),
                operativeId: $('#<%= ddloperatives.ClientID %>').val()
            });
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/updateMarker",
                data: postData,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var result = data.d;
                    var items = [];
                    var propertyIds = [];

                    var properties = [];

                    $.each(result, function (i, item) {

                        if (contains(propertyIds, item[1])) {

                        } else {
                            propertyIds.push(item[1]);
                        }

                    });

                    $.each(propertyIds, function (i, id) {

                        var property = new Object();
                        var appointments = [];

                        $.each(result, function (i, item) {

                            if (item[1] === id) {

                                property.id = item[1];
                                property.address = item[2];
                                property.towncity = item[7];
                                property.postcode = item[3];
                                property.county = item[8];

                                var appointment = new Object();
                                appointment.jsn = item[0];
                                appointment.operative = item[4];
                                appointment.startTime = item[6];
                                appointment.componentName = item[5];
                                appointment.duration = item[9];
                                appointment.appointmentDate = item[10];


                                appointments.push(appointment);

                            }

                        });

                        property.appointments = appointments;
                        properties.push(property);
                    });

                    addMarkers(properties);
                    return true;
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlDashboardOuter" runat="server">
        <div class="wrapper">
            <div class="r_main_div">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <asp:UpdatePanel ID="updPanelCounter" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="left_part" style="margin-right: 20px;">
                            <div class="box-left-b" style="margin-top:0; overflow:hidden;" >
                                <div class="boxborder-s-right">
                                    <div class="select_div-b">
                                        <asp:Label ID="Label2" runat="server" CssClass="label-b" Text="Component: " style="color: white; line-height: 20px;padding-left:0; width:100px;" Font-Bold="true"></asp:Label>
                                        <div class="field right" style="width:inherit">
                                            <asp:DropDownList class="styleselect" ID="ddlComponents"
                                                style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                height: 25px !important;
                                                font-size: 12px !important;
                                                width: 205px !important;
                                                margin-right: 10px;" 
                                                runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                            <button type="button" id="btnUpdateDashboard" class="btn btn-xs btn-blue right" style="padding:3px 10px !important" >Update Dashboard</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Replacement
                                        <br />
                                        due(Current year):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblReplacementCurrentYear" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="currentYearArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingReplacementCurrentYear">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Replacement
                                        <br />
                                        overdue:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblReplacementOverdue" runat="server" Text="0" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="overDueArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingReplacementOverdue">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Total Replacements
                                        <br />
                                        overdue:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblTotalReplacements" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="totalReplacementsArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingTotalReplacements">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Approved For Replacement
                                        <br />
                                        (PMOs not scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblReplacementNotScheduled" runat="server" Text="0" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="notScheduleArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingReplacementNotScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Approved For Replacement
                                        <br />
                                        (PMOs scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblReplacementDueScheduled" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="totalReplacementsArrow" href="../../Views/Scheduling/ScheduleWorks.aspx?compotentId=-1&tab=aptArranged">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingReplacementDueScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        No
                                        <br />
                                        Entries:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblNoEntries" runat="server" Text="0" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='../Reports/NoEntryReport.aspx'>
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingNoEntries">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box-red">
                                    <div class="text">
                                        Properties<br />
                                        in progress:
                                    </div>
                                    <div class="number-faults">
                                        <asp:Label ID="lblInprogress" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='#'>
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingInprogress">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Adaptation
                                        <br />
                                        (not scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblAdaptationNotScheduled" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="notAdaptationArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                        <%--<a href='../Reports/NoEntryReport.aspx'>
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>--%>
                                    </div>
                                    <div class="number-loading" id="loadingAdaptationNotScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Misc. Works
                                        <br />
                                        (not scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblMiscWorksNotScheduled" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="notMiscArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingMiscWorksNotScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Condition Works
                                        <br />
                                        (Approval Required):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblConditionApprovalRequired" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="notConditionArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingConditionApprovalRequired">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Adaptation
                                        <br />
                                        (scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblAdaptationScheduled" runat="server" Text="0" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="AdaptationArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingAdaptationScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Misc. Works
                                        <br />
                                        (scheduled):
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblMiscWorksScheduled" runat="server" Text="0" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="MiscArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingMiscWorksScheduled">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Approved
                                        <br />
                                        Condition Works:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblConditionWorksApproved" runat="server" Text="0" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a id="ConditionArrow">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingConditionWorksApproved">
                                        <img class="Loding-Image-Dashboard" alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="right_part" style="width:48%">
                    <div class="box-left-b" style="margin-top:0; overflow:hidden;" >
                        <asp:UpdatePanel ID="updPanelOperatives" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                                <div class="boxborder-s-right">
                                    <div class="select_div-b">
                                        <asp:Label ID="lblOperative" runat="server" CssClass="label-b" Text="Operative: " style="color: white; line-height: 20px;padding-left:0; width:80px;" Font-Bold="true"></asp:Label>
                                        <div class="field right" style="width:inherit">
                                            <asp:DropDownList class="styleselect" ID="ddloperatives"
                                                style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                height: 25px !important;
                                                font-size: 12px !important;
                                                width: 205px !important;
                                                margin-right: 10px;" 
                                                runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnView" class="btn btn-xs btn-blue right" style="padding:3px 10px !important" runat="server" Text="View" />
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 400px; float: left; margin-bottom:2px;">
                                    <uc1:MapControl ID="MapControl" runat="server" Visible="True" />
                                </div>
                                <div style="width: 100%;">
                                    <uc2:OperativeAppointmentsListControl ID="OperativeAppointmentsListControl" runat="server"
                                        Visible="True" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
