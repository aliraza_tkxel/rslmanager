﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class Dashboard
    Inherits PageBase

#Region "Properties"

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event fires when page loads
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then

        End If
    End Sub
#End Region

#Region "Page Load Complete"
    ''' <summary>
    ''' Event fires when page loading is completed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try
            'Dim lnkBtn As LinkButton = CType(Master.FindControl("lnkBtnAppDahsboard"), LinkButton)
            'lnkBtn.Style.Add(ApplicationConstants.ConWebBackgroundProperty, ApplicationConstants.ConWebBackgroundColor)

            If Not (IsPostBack) Then
                populateComponentDropdownList()
                populateOperativesDropdownList()
                OperativeAppointmentsListControl.populateInprogressAppointments(ddlComponents.SelectedValue, ddloperatives.SelectedValue)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Hidden button event to populate dropdown list"
    ''' <summary>
    ''' Hidden button event to populate dropdown list
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Try
            Dim argument As String = Request.Form("__EVENTARGUMENT")
            Dim componentId As Integer
            Dim operativeId As Integer

            If (argument.Equals(ApplicationConstants.EventArgumentUpdate)) Then
                populateOperativesDropdownList()
            Else
                ddloperatives.SelectedValue = ApplicationConstants.AllSelectedValue
            End If

            componentId = ddlComponents.SelectedValue
            operativeId = ddloperatives.SelectedValue

            OperativeAppointmentsListControl.populateInprogressAppointments(componentId, operativeId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Button event to show inprogress appointments in map and listview"
    ''' <summary>
    ''' Button event to show inprogress appointments in map and listview
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Try
            Dim componentId As Integer
            Dim operativeId As Integer

            componentId = ddlComponents.SelectedValue
            operativeId = ddloperatives.SelectedValue

            OperativeAppointmentsListControl.populateInprogressAppointments(componentId, operativeId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate component dropdown list"
    ''' <summary>
    ''' Populate dropdown with components.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateComponentDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        objDashboardBL.getAllComponents(resultDataSet)

        ddlComponents.DataSource = resultDataSet.Tables(0).DefaultView
        ddlComponents.DataValueField = "ComponentId"
        ddlComponents.DataTextField = "ComponentName"
        ddlComponents.DataBind()


        Dim item As ListItem = New ListItem("All", "-1")
        ddlComponents.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate operatives dropdown list"
    ''' <summary>
    ''' Populate dropdown with operatives.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateOperativesDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim componentId As Integer = ddlComponents.SelectedValue
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        objDashboardBL.getInprogressAppointmentOperatives(resultDataSet, componentId)

        ddloperatives.DataSource = resultDataSet.Tables(0).DefaultView
        ddloperatives.DataValueField = "OperativeId"
        ddloperatives.DataTextField = "OperativeName"
        ddloperatives.DataBind()


        Dim defaultItem As ListItem = New ListItem("Please select ", "-2")
        Dim allItem As ListItem = New ListItem("All", "-1")
        ddloperatives.Items.Insert(0, defaultItem)
        ddloperatives.Items.Insert(1, allItem)

    End Sub

#End Region

#Region "Update In Replacement due in Current year count"
    ''' <summary>
    ''' Returns the count of Replacement due in Current year.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountReplacementDueCurrentYear(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim currentYear As Integer = DateTime.Now.Year
        Dim componentId As Integer = Convert.ToInt32(componentValue)

        Return objDashboardBL.getReplacementsCount(componentId, currentYear, ApplicationConstants.YearEqual)
    End Function
#End Region

#Region "Update In Replacement due not scheduled"
    ''' <summary>
    ''' Returns the count Replacement due not scheduled.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getReplacementDueNotScheduleCount(ByVal componentValue As String) As Integer
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim componentId As Integer = Convert.ToInt32(componentValue)

        Return objDashboardBL.getReplacementDueNotScheduleCount(componentId)
    End Function
#End Region

#Region "Update In Replacement due scheduled Number"
    ''' <summary>
    ''' Returns the count Replacement due scheduled.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getReplacementDueScheduleCount(ByVal componentValue As String) As Integer
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim componentId As Integer = Convert.ToInt32(componentValue)
        Return objDashboardBL.getReplacementDueScheduleCount(componentId)
    End Function
#End Region

#Region "Update In Replacement Overdue Number"
    ''' <summary>
    ''' Returns the count Replacement Overdue number.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getReplacementOverdueCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim currentYear As Integer = DateTime.Now.Year
        Dim componentId As Integer = Convert.ToInt32(componentValue)

        Return objDashboardBL.getReplacementsCount(componentId, currentYear, ApplicationConstants.YearLessThan)

    End Function
#End Region

#Region "Update In No Entry count"
    ''' <summary>
    ''' Returns the count No Entry.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getNoEntryCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim componentId As Integer = Convert.ToInt32(componentValue)
        Dim result As String = objDashboardBL.getNoEntryCount(componentId)
        Return result
    End Function
#End Region

#Region "Update In Replacement due scheduled Number"
    ''' <summary>
    ''' Returns the count Replacement due scheduled.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getInprogressCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim componentId As Integer = Convert.ToInt32(componentValue)
        Dim result As String = objDashboardBL.getInprogressCount(componentId)
        Return result
    End Function
#End Region

#Region "Update In Total Replacements due and over due"
    ''' <summary>
    ''' Returns the count of Total Replacements due and over due.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getTotalReplacementCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim currentYear As Integer = DateTime.Now.Year
        Dim componentId As Integer = Convert.ToInt32(componentValue)

        Return objDashboardBL.getReplacementsCount(componentId, currentYear, ApplicationConstants.YearLessThanEqual)
    End Function
#End Region

#Region "Update Marker"
    ''' <summary>
    ''' Returns the appointments of an operative scheduled today.
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function updateMarker(ByVal componentId As Integer, ByVal operativeId As Integer) As List(Of Object())
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim dsInprogressAppointments As DataSet = New DataSet()
        objDashboardBL.getInprogressAppointments(dsInprogressAppointments, componentId, operativeId)
        Dim lstOutput As New List(Of Object())
        Dim rows As Integer
        rows = dsInprogressAppointments.Tables(0).Rows.Count - 1
        For intRow As Integer = 0 To rows
            lstOutput.Add(dsInprogressAppointments.Tables(0).Rows(intRow).ItemArray())
        Next
        Return lstOutput
    End Function
#End Region

#Region "Inprogress Marker Detail"
    ''' <summary>
    ''' Inprogress Marker Detail
    ''' </summary>
    ''' <param name="componentId"></param>
    ''' <param name="operativeId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getInprogressMarkerDetail(ByVal componentId As Integer, ByVal operativeId As Integer) As List(Of Object())
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim dsInprogressAppointments As DataSet = New DataSet()
        objDashboardBL.getInprogressAppointments(dsInprogressAppointments, componentId, operativeId)
        Dim lstOutput As New List(Of Object())
        Dim rows As Integer
        rows = dsInprogressAppointments.Tables(0).Rows.Count - 1
        For intRow As Integer = 0 To rows
            lstOutput.Add(dsInprogressAppointments.Tables(0).Rows(intRow).ItemArray())
        Next
        Return lstOutput
    End Function
#End Region

#Region "Get Adaptation Not Scheduled Number"
    ''' <summary>
    ''' Returns the count Adaptation Not Scheduled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getAdaptationNotScheduledCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getAdaptationCount(componentId, "To Be Arranged", "Adaptation")
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#Region "Get Adaptation Scheduled Number"
    ''' <summary>
    ''' Returns the count Adaptation Scheduled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getAdaptationScheduledCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getAdaptationCount(componentId, "Arranged", "Adaptation")
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#Region " Get Condition Works Approval Required Count "
    ''' <summary>
    ''' Get Condition Works Approval Required Count
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getConditionWorksApprovalRequiredCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getConditionWorksApprovalRequiredCount(componentId)
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#Region " Get Condition Works Approved Count "
    ''' <summary>
    ''' Get Condition Works Approved Count
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getConditionWorksApprovedCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getConditionWorksApprovedCount(componentId)
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#Region "Get Misc. Not Scheduled Number"
    ''' <summary>
    ''' Returns the count Misc Not Scheduled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getMiscNotScheduledCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getAdaptationCount(componentId, "To Be Arranged", "Misc")
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#Region "Get Misc. Scheduled Number"
    ''' <summary>
    ''' Returns the count Misc Scheduled
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getMiscScheduledCount(ByVal componentValue As String) As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim result As String = ""
        Dim componentId As Integer

        Try
            If (Not IsDBNull(componentValue) And Trim(componentValue) <> "") Then
                componentId = Convert.ToInt32(componentValue)
                result = objDashboardBL.getAdaptationCount(componentId, "Arranged", "Misc")
            End If
        Catch

        Finally

        End Try

        Return result
    End Function

#End Region

#End Region

End Class