﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_Utilities
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging.ImageFormat

Public Class UploadDocument
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)        
        Me.Page.Form.Enctype = "multipart/form-data"
    End Sub
#End Region

#Region "get Document Type"
    Private Function getDocumentType()
        If Not IsNothing(Request.QueryString("type")) Then
            Return Request.QueryString("type")
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidDocumentType, True)
            Return Nothing
        End If

    End Function
#End Region

#Region "upload File"

    Public Sub uploadFile(ByVal docType As String)
        If (FileUpload1.HasFile) Then

            Dim filePath As String = String.Empty
            Dim fileName As String = String.Empty

            If docType = "document" Then
                filePath = GeneralHelper.getDocumentUploadPath()
            ElseIf docType = "photo" Then
                filePath = Server.MapPath("../../Photographs/Images/")
            ElseIf docType = "signature" Then

                filePath = Server.MapPath("../../../EmployeeSignature/")

                'Need the path setup**********************************
                'filePath = HttpContext.Current.Server.MapPath("EmployeeSignatures")
                '*****************************************************
            End If

            If (Directory.Exists(filePath) = False) Then
                Directory.CreateDirectory(filePath)
            End If

            fileName = Path.GetFileName(FileUpload1.FileName)
            fileName = getUniqueFileName()

            If docType = "document" Then
                fileName = ApplicationConstants.DocPrefix + fileName
            End If

            Dim filePathName As String = filePath + fileName
            FileUpload1.SaveAs(filePathName)

            If docType = "document" Then
                Session(SessionManager.DocumentUploadName) = fileName
                If Not IsNothing(Request.QueryString("popType")) Then
                    Response.Write("<script type='text/javascript'>window.opener.fireAddDocUploadCkBoxEvent();window.close();</script>")
                Else
                    Response.Write("<script type='text/javascript'>window.opener.fireDocUploadCkBoxEvent();window.close();</script>")
                End If
            ElseIf docType = "photo" Then
                generateThumbnail(fileName)
                Session(SessionManager.PhotoUploadName) = fileName

                If Not IsNothing(Request.QueryString("photoType")) Then
                    Response.Write("<script type='text/javascript'>window.opener.fireDefectPhotoUploadCkBoxEvent();window.close();</script>")
                Else
                    Response.Write("<script type='text/javascript'>window.opener.firePhotoUploadCkBoxEvent();window.close();</script>")
                End If

            ElseIf docType = "signature" Then
                Session(SessionManager.SignatureUploadName) = fileName
                Response.Write("<script type='text/javascript'>window.opener.fireSignatureNameEvent('" & fileName & "');window.close();</script>")
            End If

        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDocument, True)
        End If

    End Sub
#End Region

#Region "get Unique File Name"
    Private Function getUniqueFileName()

        Dim fileName As String = Path.GetFileNameWithoutExtension(FileUpload1.FileName)
        Dim ext = Path.GetExtension(FileUpload1.FileName)
        Dim uniqueString As String = DateTime.Now.ToString().GetHashCode().ToString("x")

        If fileName.Length > 35 Then
            fileName = fileName.Substring(0, 35)
        End If

        fileName = fileName + uniqueString

        Return fileName + ext

    End Function
#End Region

#Region "validate File"
    Public Function validateFile() As Boolean
        Dim isSuccess = True
        Dim fileExt As String = Path.GetExtension(FileUpload1.FileName)
        Dim size As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("FileSize"))
        If Not fileExt.Equals(".pdf") AndAlso Not fileExt.Equals(".doc") AndAlso Not fileExt.Equals(".docx") AndAlso Not fileExt.Equals(".xls") AndAlso Not fileExt.Equals(".xlsx") AndAlso Not fileExt.Equals(".png") AndAlso Not fileExt.Equals(".jpg") Then
            isSuccess = False
        End If
        Return isSuccess
    End Function
#End Region

#Region "btn Ok Click"

    Protected Sub btnOk_Click1(ByVal sender As Object, ByVal e As EventArgs) Handles btnOk.Click
        Try
            Dim docType As String = String.Empty
            docType = Me.getDocumentType()
            If (Not IsNothing(docType)) Then
                Me.uploadFile(docType)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Generate Thumbnail"
    Protected Sub generateThumbnail(ByVal FileName As String)
        'Create a new Bitmap Image loading from location of origional file
        Dim FilePath As String = String.Empty
        FilePath = Server.MapPath("../../Photographs/Images/")
        Dim bm As Bitmap = System.Drawing.Image.FromFile(FilePath & FileName)
        Dim strFileName As String = FileName

        'Declare Thumbnails Height and Width
        Dim newWidth As Integer = 100
        Dim newHeight As Integer = 100
        'Create the new image as a blank bitmap
        Dim resized As Bitmap = New Bitmap(newWidth, newHeight)
        'Create a new graphics object with the contents of the origional image
        Dim g As Graphics = Graphics.FromImage(resized)
        'Resize graphics object to fit onto the resized image
        g.DrawImage(bm, New Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel)
        'Get rid of the evidence
        g.Dispose()
        FilePath = Server.MapPath("../../Photographs/Thumbs/")
        If (Directory.Exists(FilePath) = False) Then
            Directory.CreateDirectory(FilePath)
        End If
        'Create new path and filename for the resized image
        Dim newStrFileName As String = FilePath & strFileName
        'Save the new image to the same folder as the origional
        resized.Save(newStrFileName)
        resized.Dispose()
        bm.Dispose()
    End Sub
#End Region

End Class