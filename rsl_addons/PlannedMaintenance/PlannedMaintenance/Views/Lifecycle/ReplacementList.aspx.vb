﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading
Imports System.IO
Imports System.Globalization

Public Class ReplacementList
    Inherits PageBase


#Region "Properties"

    Private Shared lockObject As Object = New Object

#End Region

#Region "Events Handling"

#Region "Page load"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.setMessage(lblMessage, pnlMessage, String.Empty, True)
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, String.Empty, True)

            If Not (IsPostBack) Then
                'Me.highLightCurrentPageMenuItems()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

                populateComponentDropdownList()
                populateReplacementYearDropdownList()
                populateAppointmentStatusDropdownList()
                populateYearOperatorDropdownList()
                populateReplacementListQueryString()
                populateSchemeDropdownList()

                txtSelectedCost.Attributes.Add("readonly", "readonly")
                txtDate.Attributes.Add("readonly", "readonly")
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Replacement Year Selected Index Changed"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlReplacementYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReplacementYear.SelectedIndexChanged
        Try
            populateSchemeDropdownList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Components Selected Index Changed"
    ''' <summary>
    ''' Event of component drop down
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlComponents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlComponents.SelectedIndexChanged
        Try
            populateSchemeDropdownList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Arrange Inspection Linkbutton Event"
    ''' <summary>
    ''' Arrange Inspection Linkbutton Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnArrangeInspection(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.resetArrangeInspectionPopupControls()

            Dim lnkBtnInspectionArrange As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnInspectionArrange.NamingContainer, GridViewRow)

            Dim lblPropertyId As Label = DirectCast(row.FindControl("lblPropertyId"), Label)
            Dim lblStatus As Label = DirectCast(row.FindControl("lblAppointmentStatus"), Label)
            Dim lblComponentId As Label = DirectCast(row.FindControl("lblComponentId"), Label)
            Dim lblStatusId As Label = DirectCast(row.FindControl("lblStatusId"), Label)
            Dim inspectionIdLabel As Label = DirectCast(row.FindControl("lblInspectionId"), Label)

            lblHiddenComponentId.Text = lblComponentId.Text
            lblHiddenPropertyId.Text = lblPropertyId.Text
            lblHiddenStatusId.Text = lblStatusId.Text
            lblHiddenInspectionId.Text = inspectionIdLabel.Text

            Dim inspectionId As Integer = Convert.ToInt32(lblHiddenInspectionId.Text)
            'set proerty id in session for edit letter
            SessionManager.setPropertyIdForEditLetter(lblPropertyId.Text)

            If (inspectionId = -1) Then
                populateDefaultInspectionPopup(lblPropertyId.Text, lblStatus.Text, lblComponentId.Text)
            Else
                populateExistingInspectionPopup(lblPropertyId.Text, lblStatus.Text, lblComponentId.Text, lblStatusId.Text, inspectionId)
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn Refresh List Click"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnRefreshList_Click(sender As Object, e As EventArgs) Handles lnkBtnRefreshList.Click
        Try
            If Me.isValidStartDate() = True Then
                Me.populateOperativeDropdownList()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Generate list Event"
    ''' <summary>
    ''' Generate list Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGenerateList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerateList.Click

        Try
            'ddlYearOperator.SelectedValue = ApplicationConstants.YearEqual

            If (ddlComponents.SelectedValue = -1 And ddlReplacementYear.SelectedValue = -1 And ddlScheme.SelectedValue = -1) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDropdowns, True)
                populateReplacementList()
            Else
                If (Not populateReplacementList()) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

    Protected Sub imgSearchbtn_DataBinding(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGO.Click
        Try
            If (ddlComponents.SelectedValue = -1 And ddlReplacementYear.SelectedValue = -1 And ddlScheme.SelectedValue = -1) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDropdowns, True)
                populateReplacementList()
            Else
                If (Not populateReplacementList()) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                End If
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Schedule selected Event"
    ''' <summary>
    ''' Schedule selected Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnScheduleSelected_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleSelected.Click

        Try
            If (validateSelection()) Then
                scheduleSelectedProperties()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.CompletedArrangedStatusNotSchedule, True)
            End If



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Export Full list Event"
    ''' <summary>
    ''' Export list Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportList.Click

        Try
            exportFullList()

            'exportToExcel(ApplicationConstants.ExportFullList)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Export list Event"
    ''' <summary>
    ''' Export list Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportSelected_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportSelected.Click

        Try
            If (checkAnySelected()) Then
                exportSelectedToExcel()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RecordNotSelected, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = False
            uiMessageHelper.message = String.Empty

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Search result Grid Sorting"

    Protected Sub grdReplacementList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdReplacementList.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdReplacementList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            populateReplacementList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdReplacementList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Bind Row Data"
    Protected Sub grdReplacementList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdReplacementList.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim checkBox As CheckBox = DirectCast(e.Row.FindControl("chkReplacementItem"), CheckBox)
                checkBox.Attributes.Add("onClick", "checkSingleProperty(" + (e.Row.RowIndex + 1).ToString + ",this.checked);")

                Dim inspectionbtn As LinkButton = DirectCast(e.Row.FindControl("lnkBtnInspection"), LinkButton)
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblAppointmentStatus"), Label)

                If (lblStatus.Text.Equals("Arranged")) Then
                    inspectionbtn.Visible = False
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    ''' <summary>
    ''' ddl Action Selected Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            populateLetterDropdownList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"
    ''' <summary>
    ''' img Btn Remove Letter Doc Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

            mdlPopupArrangeInspection.Show()

        End Try
    End Sub

#End Region

#Region "Cancel button click"
    ''' <summary>
    ''' Cancel button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Try
            mdlPopupArrangeInspection.Hide()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

            mdlPopupArrangeInspection.Show()

        End Try
    End Sub

#End Region

#Region "ckBox Refresh DataSet Checked Changed"
    ''' <summary>
    ''' ckBox Refresh DataSet Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxAddRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxAddRefreshDataSet.CheckedChanged
        ckBoxAddRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId()) 'issue
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If


        End Try
        mdlPopupArrangeInspection.Show()
    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"
    ''' <summary>
    ''' CK Box Document Upload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty) 'issue
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            mdlPopupArrangeInspection.Show()

        End Try

    End Sub

#End Region

#Region "Btn Arrange Click"
    ''' <summary>
    ''' Btn Arrange Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnArrange_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnArrange.Click
        Dim isSaved As Boolean = False
        Try

            If (Me.validateInspectionArrangePopup() = True) Then
                Dim journalHistoryId As Integer = 0
                journalHistoryId = Me.arrangeInspection()

                If journalHistoryId > 0 Then

                    'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InspectionArrangedSuccessfully, False)
                    sendICalFileViaEmail()

                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
                    Else
                        isSaved = True
                        grdReplacementList.DataSource = SessionManager.getReplacementListDataSet()
                        grdReplacementList.DataBind()
                        'To remove the record of existing letters from the session,
                        ' so these these letters can be re attached on re arrange.
                        SessionManager.removeActivityLetterDetail()
                    End If

                Else
                    uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
                End If
            Else
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If isSaved = False Then
                mdlPopupArrangeInspection.Show()
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
                populateReplacementList()
            Else
                populateReplacementList()
                Me.mdlSuccessPopup.Show()
                mdlPopupArrangeInspection.Hide()

            End If

        End Try
    End Sub
#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateReplacementList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                populateReplacementList()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Arrange Inspection"
    ''' <summary>
    ''' Arrange Inspection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function arrangeInspection() As Integer
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim objArrangeInspectionBO As ArrangeInspectionBO = New ArrangeInspectionBO()
        objArrangeInspectionBO = Me.getArrangeInspectionBO()
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objArrangeInspectionBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objArrangeInspectionBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objArrangeInspectionBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objArrangeInspectionBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objArrangeInspectionBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objArrangeInspectionBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objArrangeInspectionBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objArrangeInspectionBO.IsDocumentAttached = False
            End If
        End If

        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()
        'savedLetterDt = Me.getLetterDocList()

        Return objReplacementBL.arrangeInspection(objArrangeInspectionBO, savedLetterDt)

    End Function

#End Region

#Region "Populate Arrange Inspection BO"
    ''' <summary>
    ''' Populate Arrange Inspection BO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getArrangeInspectionBO() As ArrangeInspectionBO

        Dim objArrangeInspectionBO As ArrangeInspectionBO = New ArrangeInspectionBO()
        objArrangeInspectionBO.PropertyId = lblHiddenPropertyId.Text
        objArrangeInspectionBO.ComponentId = Convert.ToInt32(lblHiddenComponentId.Text)
        objArrangeInspectionBO.StatusId = Convert.ToInt32(lblHiddenStatusId.Text)
        objArrangeInspectionBO.ActionId = Convert.ToInt32(ddlAction.SelectedValue)
        objArrangeInspectionBO.UserId = Convert.ToInt32(SessionManager.getUserEmployeeId())
        objArrangeInspectionBO.IsLetterAttached = False
        objArrangeInspectionBO.IsDocumentAttached = False
        objArrangeInspectionBO.TenancyId = Convert.ToInt32(lblHiddenTenancyId.Text)
        objArrangeInspectionBO.AppointmentDate = txtDate.Text
        objArrangeInspectionBO.AppointmentStartTime = ddlStartTime.SelectedItem.Text
        objArrangeInspectionBO.AppointmentEndTime = ddlEndTime.SelectedItem.Text
        objArrangeInspectionBO.InspectionId = Convert.ToInt32(lblHiddenInspectionId.Text)
        objArrangeInspectionBO.OperativeId = Convert.ToInt32(ddlOperative.SelectedValue)
        objArrangeInspectionBO.DocumentPath = GeneralHelper.getDocumentUploadPath()

        Return objArrangeInspectionBO
    End Function

#End Region

#Region "Validate Arrange Inspection"
    ''' <summary>
    ''' Validate Arrange Inspection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function validateInspectionArrangePopup() As Boolean

        uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)

        Dim isValid As Boolean = True

        If (ddlOperative.SelectedItem.Value = -1) Then
            uiMessageHelper.message = UserMessageConstants.SelectOperative + "<br />"
            isValid = False
        ElseIf (ddlStartTime.SelectedItem.Value.Equals("-1")) Then
            uiMessageHelper.message = UserMessageConstants.SelectStartTime + "<br />"
            isValid = False
        ElseIf (ddlEndTime.SelectedItem.Value.Equals("-1")) Then
            uiMessageHelper.message = UserMessageConstants.SelectEndTime + "<br />"
            isValid = False
        ElseIf Me.isValidStartDate() = False Then            
            isValid = False
        Else
            If (Not (ddlStartTime.SelectedItem.Value.Equals("-1")) And Not (ddlEndTime.SelectedItem.Value.Equals("-1"))) Then
                'get date
                Dim startDate As Date = Me.convertStringToDate(txtDate.Text)
                'get numbers from hours
                Dim startDateTime As Date = CType(startDate + " " + ddlStartTime.SelectedItem.Value, Date)
                Dim endDateTime As Date = CType(startDate + " " + ddlEndTime.SelectedItem.Value, Date)
                'check hours
                If startDateTime >= endDateTime Then
                    uiMessageHelper.message = UserMessageConstants.SelectStartEndTimeError + "<br />"
                    isValid = False
                Else
                    'if user didn't change the start time and end time then don't search for appointment
                    If Not ddlStartTime.SelectedItem.Value.Equals(getPreviuoslySelectedStartTime()) And Not ddlEndTime.SelectedItem.Value.Equals(getPreviuoslySelectedEndTime()) Then
                        Dim objSchedulingBl = New SchedulingBL()
                        Dim resultDataSet As DataSet = New DataSet()
                        Dim isSlotAvailable As Boolean = True
                        'get leaves and appointments of operative
                        objSchedulingBl.getOperativesLeavesAndAppointments(resultDataSet, ddlOperative.SelectedItem.Value, startDate)
                        'compare the selected time with appointments and leaves
                        isSlotAvailable = objSchedulingBl.isOperativeAvailable(resultDataSet, _
                                                             startDate, _
                                                             ddlOperative.SelectedItem.Value, _
                                                             startDateTime, _
                                                             endDateTime)
                        If isSlotAvailable = False Then
                            uiMessageHelper.message = UserMessageConstants.AppointmentSlotNotAvailable + "<br />"
                            isValid = False
                        End If
                    End If
                End If
            End If

        End If

        If (Not isValid) Then
            uiMessageHelper.IsError = True            
        End If

        Return isValid        

    End Function

#End Region

#Region "Is Letter Already Exist"
    ''' <summary>
    ''' Is Letter Already Exist
    ''' </summary>
    ''' <param name="letterId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList() 'issue
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "Remove Item Activity Letter Detail "
    ''' <summary>
    ''' Remove Item Activity Letter Detail
    ''' </summary>
    ''' <param name="letterId"></param>
    ''' <remarks></remarks>
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList() 'issue

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    ''' <summary>
    ''' Populate Letter Doc List Control
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="letterDocValue"></param>
    ''' <remarks></remarks>
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionManager.DocumentUploadName).ToString()
                Session(SessionManager.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = documentName
                dr(ApplicationConstants.LetterIdColumn) = "0"
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If

    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If

            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Reset Arrange Inspection Popup Controls"
    Protected Sub resetArrangeInspectionPopupControls()

        lblHiddenComponentId.Text = String.Empty
        lblHiddenPropertyId.Text = String.Empty
        lblHiddenTenancyId.Text = String.Empty
        lblHiddenStatusId.Text = String.Empty
        txtDate.Text = DateTime.Now.Date

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "Populate Default Inspection Popup"
    ''' <summary>
    ''' Populate Arrange Inspection Popup
    ''' </summary>
    ''' <param name="propertyId"></param>
    ''' <param name="status"></param>
    ''' <remarks></remarks>
    Sub populateDefaultInspectionPopup(ByVal propertyId As String, ByVal status As String, ByVal componentId As String)

        Me.txtDate.Text = DateTime.Now.AddDays(1).Date.ToShortDateString()
        Me.populatePropertyDetail(propertyId)
        Me.populateOperativeDropdownList()
        Me.populateStartTimeDropdownList()
        Me.populateEndTimeDropdownList()
        Me.populateActionDropdownList()
        Me.populateLetterDropdownList()

        mdlPopupArrangeInspection.Show()
    End Sub

#End Region

#Region "Populate Existing Inspection Popup"
    ''' <summary>
    ''' Populate Existing Inspection Popup
    ''' </summary>
    ''' <param name="propertyId"></param>
    ''' <param name="status"></param>
    ''' <remarks></remarks>
    Sub populateExistingInspectionPopup(ByVal propertyId As String, ByVal status As String, ByVal componentId As String, ByVal statusId As String, ByVal inspectionId As Integer)
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim resultDataset As DataSet = New DataSet
        Dim appointmentDate As String
        Dim appointmentStartTime As String
        Dim appointmentEndTime As String
        Dim operativeId As Integer
        Dim actionId As Integer
        Dim standardLetterId As Integer = -1

        objReplacementBL.getInspectionInfo(resultDataset, inspectionId)

        appointmentDate = resultDataset.Tables(ApplicationConstants.InspectionInfoDataTable).Rows(0)("AppointmentDate")
        appointmentStartTime = resultDataset.Tables(ApplicationConstants.InspectionInfoDataTable).Rows(0)("AppointmentStartTime")
        appointmentEndTime = resultDataset.Tables(ApplicationConstants.InspectionInfoDataTable).Rows(0)("AppointmentEndTime")
        operativeId = resultDataset.Tables(ApplicationConstants.InspectionInfoDataTable).Rows(0)("OperativeId")
        actionId = resultDataset.Tables(ApplicationConstants.InspectionInfoDataTable).Rows(0)("ActionId")


        If (resultDataset.Tables(ApplicationConstants.LetterDataTable).Rows.Count > 0) Then
            standardLetterId = resultDataset.Tables(ApplicationConstants.LetterDataTable).Rows(0)("letterId")
        End If
        Me.populatePropertyDetail(propertyId)
        txtDate.Text = appointmentDate
        Me.populateOperativeDropdownList()
        Try
            'if operative is not in the list which was previously selected then this line 'll give error.
            ddlOperative.SelectedValue = operativeId
        Catch ex As Exception

        End Try

        Me.populateStartTimeDropdownList()
        Me.populateEndTimeDropdownList()
        ddlStartTime.SelectedValue = appointmentStartTime
        ddlEndTime.SelectedValue = appointmentEndTime
        'save selected time in view state
        Me.setPreviuoslySelectedStartTime(appointmentStartTime)
        Me.setPreviuoslySelectedEndTime(appointmentEndTime)
        'populate action and letter
        Me.populateActionDropdownList()
        Me.populateLetterDropdownList()

        dataListLetterDoc.DataSource = resultDataset.Tables(ApplicationConstants.DocumentsDataTable).DefaultView
        dataListLetterDoc.DataBind()
        If resultDataset.Tables(ApplicationConstants.DocumentsDataTable).Rows.Count > 0 Then

            Me.setLetterDocList(resultDataset.Tables(ApplicationConstants.DocumentsDataTable))
        End If
        If (resultDataset.Tables(ApplicationConstants.LetterDataTable).Rows.Count > 0) Then
            SessionManager.setActivityLetterDetail(resultDataset.Tables(ApplicationConstants.DocumentsDataTable))
        End If

        mdlPopupArrangeInspection.Show()
    End Sub

#End Region

#Region "Populate Property Detail"
    ''' <summary>
    ''' Populate Property Detail
    ''' </summary>
    ''' <param name="propertyId"></param>
    ''' <remarks></remarks>
    Sub populatePropertyDetail(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getPropertyDetail(resultDataSet, propertyId)

        If (resultDataSet.Tables(0).Rows.Count > 0) Then

            lblPropertyAddress.Text = resultDataSet.Tables(0).Rows(0)("Address").ToString()
            txtTelepone.Text = resultDataSet.Tables(0).Rows(0)("Telephone").ToString()
            txtMobile.Text = resultDataSet.Tables(0).Rows(0)("Mobile").ToString()
            lblHiddenTenancyId.Text = resultDataSet.Tables(0).Rows(0)("TenancyId").ToString()

            Dim tenants As String = String.Empty

            For i = 0 To resultDataSet.Tables(0).Rows.Count - 1
                tenants = tenants + resultDataSet.Tables(0).Rows(i)("TenantName").ToString()
                If (i < resultDataSet.Tables(0).Rows.Count - 1) Then
                    tenants = tenants + ", "
                End If
            Next

            lblTenantName.Text = tenants

        Else
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.InvalidPropertyId, True)
        End If

    End Sub

#End Region

#Region "Populate action"
    ''' <summary>
    ''' Populate action
    ''' </summary>    
    ''' <remarks></remarks>
    Sub populateActionDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getInspectionArrangedActions(resultDataSet)

        ddlAction.DataSource = resultDataSet.Tables(0).DefaultView
        ddlAction.DataValueField = "ActionId"
        ddlAction.DataTextField = "Title"
        ddlAction.DataBind()

        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlAction.Items.Insert(0, item)
    End Sub

#End Region

#Region "Populate Letter by actionId"
    ''' <summary>
    ''' Populate Letter by actionId
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateLetterDropdownList()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getLetterByActionId(resultDataSet, ddlAction.SelectedValue)

        ddlLetter.DataSource = resultDataSet.Tables(0).DefaultView
        ddlLetter.DataValueField = "LetterId"
        ddlLetter.DataTextField = "Title"
        ddlLetter.DataBind()

        Dim item As ListItem = New ListItem("Select standard letter", "-1")
        ddlLetter.Items.Insert(0, item)

    End Sub
#End Region

#Region "Populate operative dropdown"
    ''' <summary>
    ''' Populate operative drop-down
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateOperativeDropdownList()
        'clear the old data 
        Try
            ddlOperative.Items.Clear()
            Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
            Dim resultDataSet As DataSet = New DataSet()
            'Dim objReplacementBl As ReplacementBL = New ReplacementBL()
            'Dim csvOperativeIds As String = String.Empty
            'Dim operativeIds As String() = {}
            'Dim separator As Char() = {","}
            'objReplacementBl.getAllEmployees(resultDataSet)
            'Dim appointmentSlots As New List(Of AppointmentSlotsBO)
            'appointmentSlots = getFreeSlots(0)

            'filter the employees which has empty slots in selected date
            'create an array of employee ids with empty slots in selected date
            'For Each dr As DataRow In resultDataSet.Tables(0).Rows
            '    Dim appointmentSlots As New List(Of AppointmentSlotsBO)
            '    appointmentSlots = getFreeSlots(dr.Item("EmployeeId"))

            '    If appointmentSlots.Count() > 0 Then
            '        csvOperativeIds = csvOperativeIds + dr.Item("EmployeeId").ToString() + ","
            '    End If
            'Next
            'If Not (csvOperativeIds.Equals(String.Empty)) Then
            '    csvOperativeIds.Substring(0, csvOperativeIds.Length - 1)
            'End If
            'operativeIds = csvOperativeIds.Split(separator)
            'select only the selected operatives those who are available in selected date
            'Dim query = (From rs In resultDataSet.Tables(0).AsEnumerable Where operativeIds.Contains(rs.Field(Of Integer)("EmployeeId").ToString()) Select rs)

            'If query.Count > 0 Then
            '    ddlOperative.DataSource = query.CopyToDataTable()
            'getFreeSlots(resultDataSet)
            Dim startDate As Date = convertStringToDate(Me.txtDate.Text)
            objSchedulingBl.getOperativesLeavesAndAppointments(resultDataSet, startDate)
            ddlOperative.DataSource = resultDataSet.Tables(2).DefaultView
            ddlOperative.DataValueField = "EmployeeId"
            ddlOperative.DataTextField = "FuLLName"
            ddlOperative.DataBind()
            'End If

            Dim item As ListItem = New ListItem("Please select", "-1")
            ddlOperative.Items.Insert(0, item)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Get Free Slots"
    '''' <summary>
    '''' This function 'll return the free slots of operative
    '''' </summary>
    '''' <param name="operativeId"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Private Function getFreeSlots(ByVal operativeId As Integer)
    '    Dim appointmentSlots As New List(Of AppointmentSlotsBO)
    '    Dim startDate As Date = convertStringToDate(Me.txtDate.Text)
    '    Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
    '    Dim resultDataSet As DataSet = New DataSet()
    '    objSchedulingBl.getOperativesLeavesAndAppointments(resultDataSet, operativeId, startDate)
    '    objSchedulingBl.createSingleDayOperativesAppointmentSlots(appointmentSlots, resultDataSet.Tables(0), resultDataSet.Tables(1), operativeId, startDate, 1)
    '    Return appointmentSlots
    'End Function

    '''' <summary>
    '''' This function 'll return the operative
    '''' </summary>
    '''' <param name="resultDataSet"></param>
    'Private Sub getFreeSlots(ByRef resultDataSet As DataSet)
    '    'Dim appointmentSlots As New List(Of AppointmentSlotsBO)
    '    Dim startDate As Date = convertStringToDate(Me.txtDate.Text)
    '    Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
    '    'Dim resultDataSet As DataSet = New DataSet()
    '    objSchedulingBl.getOperativesLeavesAndAppointments(resultDataSet, startDate)
    '    'objSchedulingBl.createSingleDayOperativesAppointmentSlots(appointmentSlots, resultDataSet.Tables(0), resultDataSet.Tables(1), operativeId, startDate, 1)
    '    'Return appointmentSlots
    'End Sub
#End Region

#Region "is Valid Start Date"
    ''' <summary>
    ''' This function 'll validate the start date 
    ''' Date should not be null or empty 
    ''' date must not be in past
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function isValidStartDate()
        Dim isValid As Boolean
        If Not String.IsNullOrEmpty(Me.txtDate.Text) Then
            Dim startDate As Date = convertStringToDate(Me.txtDate.Text)

            If Date.Compare(startDate.Date, Date.Now.Date) < 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StartDateInPast, True)
                isValid = False
            Else
                isValid = True
            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectInspectionDate, True)
            isValid = False
        End If
        Return isValid
    End Function
#End Region

#Region "convert String To Date"
    ''' <summary>
    ''' This function 'll convert the string date into date data type
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function convertStringToDate(ByVal startDate As String)
        Return Convert.ToDateTime(Me.txtDate.Text).Date
    End Function
#End Region

#Region "Populate Start time dropdown"
    ''' <summary>
    ''' Populate time dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateStartTimeDropdownList()
        Dim newListItem As ListItem
        ddlStartTime.Items.Clear()

        newListItem = New ListItem("Please select", "-1")
        ddlStartTime.Items.Insert(0, newListItem)
        GeneralHelper.populateOfficialHoursWithFifteenMinDuration(ddlStartTime)
    End Sub

#End Region

#Region "Populate End time dropdown"
    ''' <summary>
    ''' Populate End time dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateEndTimeDropdownList()
        Dim newListItem As ListItem
        ddlEndTime.Items.Clear()

        newListItem = New ListItem("Please select", "-1")
        ddlEndTime.Items.Insert(0, newListItem)
        GeneralHelper.populateOfficialHoursWithFifteenMinDuration(ddlEndTime)
    End Sub

#End Region

#Region "Check any selected"
    ''' <summary>
    ''' Check any selected
    ''' </summary>
    ''' <remarks></remarks>
    Function checkAnySelected()

        For i As Integer = 0 To grdReplacementList.Rows.Count - 1
            Dim row As GridViewRow = grdReplacementList.Rows(i)

            If row.RowType = DataControlRowType.DataRow Then

                Dim chkProperty As CheckBox = DirectCast(row.Cells(0).FindControl("chkReplacementItem"), CheckBox)

                If (chkProperty.Checked) Then
                    Return True
                End If
            End If
        Next

        Return False

    End Function

#End Region

#Region "Export to Excel"
    ''' <summary>
    ''' Export to Excel
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub exportSelectedToExcel()

        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "Replacement_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"

        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = "UTF-8"
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmlWrite As New HtmlTextWriter(strWrite)
        Dim htmlfrm As New HtmlForm()


        'Hide button columns from the grid to avoid them to export to excel file.
        grdReplacementList.HeaderRow.Style.Add("background-color", "#FFFFFF")
        grdReplacementList.HeaderRow.Cells(1).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(2).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(3).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(4).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(5).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(6).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(7).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(8).Style.Add("background-color", "#A4A4A4")
        grdReplacementList.HeaderRow.Cells(9).Style.Add("background-color", "#A4A4A4")


        grdReplacementList.Columns(grdReplacementList.Columns.Count - 1).Visible = False
        grdReplacementList.Columns(0).Visible = False



        For i As Integer = 0 To grdReplacementList.Rows.Count - 1
            Dim row As GridViewRow = grdReplacementList.Rows(i)

            If row.RowType = DataControlRowType.DataRow Then

                Dim chkProperty As CheckBox = DirectCast(row.Cells(0).FindControl("chkReplacementItem"), CheckBox)

                If (Not chkProperty.Checked) Then
                    row.Visible = False
                Else

                    row.Visible = True
                    row.BackColor = System.Drawing.Color.White
                End If


            End If
        Next

        grdReplacementList.Parent.Controls.Add(htmlfrm)
        htmlfrm.Attributes("runat") = "server"
        htmlfrm.Controls.Add(grdReplacementList)

        htmlfrm.RenderControl(htmlWrite)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()
        Dim grdFullReplacementList As New GridView()
        grdFullReplacementList.AllowPaging = False
        grdFullReplacementList.AutoGenerateColumns = False

        Dim schemeField As BoundField = New BoundField()
        schemeField.HeaderText = "Scheme:"
        schemeField.DataField = "Scheme"
        grdFullReplacementList.Columns.Add(schemeField)

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullReplacementList.Columns.Add(addressField)

        Dim componentField As BoundField = New BoundField()
        componentField.HeaderText = "Component:"
        componentField.DataField = "ComponentName"
        grdFullReplacementList.Columns.Add(componentField)

        Dim replacedField As BoundField = New BoundField()
        replacedField.HeaderText = "Replaced/Installed:"
        replacedField.DataField = "ReplacedDate"
        grdFullReplacementList.Columns.Add(replacedField)

        Dim replacementDueField As BoundField = New BoundField()
        replacementDueField.HeaderText = "Replacement Due:"
        replacementDueField.DataField = "ReplacementDueDate"
        grdFullReplacementList.Columns.Add(replacementDueField)

        Dim stockSurveyField As BoundField = New BoundField()
        stockSurveyField.HeaderText = "Last Stock Survey:"
        stockSurveyField.DataField = "LastSurveyDate"
        grdFullReplacementList.Columns.Add(stockSurveyField)

        Dim statusField As BoundField = New BoundField()
        statusField.HeaderText = "Appointment Status:"
        statusField.DataField = "AppointmentStatus"
        grdFullReplacementList.Columns.Add(statusField)

        Dim aptDateField As BoundField = New BoundField()
        aptDateField.HeaderText = "Appointment Date:"
        aptDateField.DataField = "AppointmentDate"
        grdFullReplacementList.Columns.Add(aptDateField)

        Dim costField As BoundField = New BoundField()
        costField.HeaderText = "Total:"
        costField.DataField = "TotalCost"
        grdFullReplacementList.Columns.Add(costField)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        objReplacementBL.getReplacementList(resultDataSet, objPageSortBo, ddlComponents.SelectedValue, ddlReplacementYear.SelectedValue, ddlScheme.SelectedValue, ddlAppointmentStatus.SelectedValue, ddlYearOperator.SelectedValue, ApplicationConstants.ExportFullList, ddltxtSearch.Value)
        grdFullReplacementList.DataSource = resultDataSet.Tables(0)
        grdFullReplacementList.DataBind()

        ExportGridToExcel(grdFullReplacementList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "Replacement_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Populate components"
    ''' <summary>
    ''' Populate all components
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateComponentDropdownList()
        Dim resultDataSet As DataSet = New DataSet()

        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getAllComponents(resultDataSet)

        ddlComponents.DataSource = resultDataSet.Tables(0).DefaultView
        ddlComponents.DataValueField = "ComponentId"
        ddlComponents.DataTextField = "ComponentName"
        ddlComponents.DataBind()

        Dim item As ListItem = New ListItem("Select a component", "-1")
        ddlComponents.Items.Insert(0, item)
    End Sub

#End Region

#Region "Schedule selected properties"
    ''' <summary>
    ''' Schedule selected properties
    ''' </summary>
    ''' <remarks></remarks>
    Sub scheduleSelectedProperties()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim selectedProperties As DataTable = generateSelectedReplacementTable()
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        For Each row As GridViewRow In grdReplacementList.Rows

            Dim componentIdLabel As Label = DirectCast(row.FindControl("lblComponentId"), Label)
            Dim propertyIdLabel As Label = DirectCast(row.FindControl("lblPropertyId"), Label)
            Dim appointmentStatusLabel As Label = DirectCast(row.FindControl("lblAppointmentStatus"), Label)
            Dim replacementCheckBox As CheckBox = DirectCast(row.FindControl("chkReplacementItem"), CheckBox)
            Dim propertyId As String = propertyIdLabel.Text
            Dim componentId As Integer = Convert.ToInt32(componentIdLabel.Text)

            If (replacementCheckBox.Checked = True AndAlso (appointmentStatusLabel.Text.ToLower.Equals("to be approved") OrElse appointmentStatusLabel.Text.ToLower.Equals("condition works") OrElse appointmentStatusLabel.Text.ToLower.Equals("inspection arranged") OrElse appointmentStatusLabel.Text.ToLower.Equals("cancelled"))) Then
                If (Not checkReplacementExist(selectedProperties, propertyId, componentId)) Then
                    selectedProperties.Rows.Add(componentId, propertyId)
                End If
            End If
        Next

        If (selectedProperties.Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RecordNotSelected, True)
        Else
            objReplacementBL.scheduleSelectedProperties(selectedProperties, userId)
            populateReplacementList()
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SuccessfulSchedule, False)

        End If

    End Sub

#End Region

#Region "Validate selection"
    ''' <summary>
    ''' Validate selection
    ''' </summary>
    ''' <remarks></remarks>
    Function validateSelection() As Boolean

        For Each row As GridViewRow In grdReplacementList.Rows

            Dim replacementCheckBox As CheckBox = DirectCast(row.FindControl("chkReplacementItem"), CheckBox)
            Dim appointmentStatusLabel As Label = DirectCast(row.FindControl("lblAppointmentStatus"), Label)

            If (replacementCheckBox.Checked = True AndAlso (appointmentStatusLabel.Text.ToLower.Equals("arranged") OrElse appointmentStatusLabel.Text.ToLower.Equals("completed") OrElse appointmentStatusLabel.Text.ToLower.Equals("to be arranged"))) Then
                Return False
            End If

        Next

        Return True

    End Function

#End Region

#Region "Check replacement exist"
    ''' <summary>
    ''' Check replacement exist
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function checkReplacementExist(ByVal replacementTable As DataTable, ByVal propertyId As String, ByVal componentId As Integer) As Boolean
        Dim query = _
                From order In replacementTable.AsEnumerable() _
                Where order.Field(Of Integer)("COMPONENTID") = componentId And order.Field(Of String)("PROPERTYID") = propertyId
                Select order

        If (query.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Generate Selected Replacement Table"
    ''' <summary>
    ''' Generate Selected Replacement Table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function generateSelectedReplacementTable() As DataTable

        Dim table As New DataTable
        table.Columns.Add("COMPONENTID", GetType(Integer))
        table.Columns.Add("PROPERTYID", GetType(String))
        Return table

    End Function

#End Region

#Region "Populate replacement year"
    ''' <summary>
    ''' Populate replacement year from 1960 to 2160
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdownList()

        Dim year As Integer = 2000

        While (year <= ApplicationConstants.YearEndRange)
            Dim yearItem As ListItem = New ListItem(Convert.ToString(year), Convert.ToString(year))
            ddlReplacementYear.Items.Add(yearItem)
            year = year + 1
        End While

        Dim item As ListItem = New ListItem("Select a replacement Year", "-1")
        ddlReplacementYear.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate year operator"
    ''' <summary>
    '''  YEAR OPERATOR IS USED TO GET RECORDS HAVING REPLACEMENT DUE DATES IN "REPLACEMENT YEAR" , "BEFORE REPLACEMENT YEAR" OR "REPLACEMENT AND PREVIOUS YEAR"
    '''	0 => EQUAL TO REPLACEMENT YEAR
    ''' -1 => "CURRENT AND PREVIOUS YEAR"
    '''	-2 => "PREVIOUS YEAR"
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateYearOperatorDropdownList()

        Dim yearEqualItem As ListItem = New ListItem("=", ApplicationConstants.YearEqual)
        Dim yearLessThanEqualItem As ListItem = New ListItem("<=", ApplicationConstants.YearLessThanEqual)
        Dim yearLessThanItem As ListItem = New ListItem("<", ApplicationConstants.YearLessThan)

        ddlYearOperator.Items.Insert(0, yearEqualItem)
        ddlYearOperator.Items.Insert(1, yearLessThanEqualItem)
        ddlYearOperator.Items.Insert(2, yearLessThanItem)

    End Sub

#End Region

#Region "Populate scheme"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSchemeDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim componentId As Integer = Me.ddlComponents.SelectedItem.Value
        Dim replacementYear As Integer = Me.ddlReplacementYear.SelectedItem.Value
        objReplacementBL.getSchemesAgainstReplacementYearAndComponent(resultDataSet, replacementYear, componentId)

        ddlScheme.DataSource = resultDataSet.Tables(0).DefaultView
        ddlScheme.DataValueField = "DevelopmentId"
        ddlScheme.DataTextField = "SchemeName"
        ddlScheme.DataBind()


        Dim item As ListItem = New ListItem("Select a Scheme", "-1")
        ddlScheme.Items.Insert(0, item)
    End Sub

#End Region

#Region "Populate appointment status"
    ''' <summary>
    ''' Populate appointment status based on the replacement list generated
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentStatusDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getAllAppointmentStatuses(resultDataSet)

        ddlAppointmentStatus.DataSource = resultDataSet.Tables(0).DefaultView
        ddlAppointmentStatus.DataValueField = "StatusId"
        ddlAppointmentStatus.DataTextField = "StatusTitle"
        ddlAppointmentStatus.DataBind()


        Dim item As ListItem = New ListItem("Select a Appt Status", "-1")
        ddlAppointmentStatus.Items.Insert(0, item)

        Dim item1 As ListItem = New ListItem("To be Approved", "0")
        ddlAppointmentStatus.Items.Insert(1, item1)
    End Sub

#End Region

#Region "Populate Replacement List"
    ''' <summary>
    ''' Populate Replacement List
    ''' </summary>
    ''' <remarks></remarks>
    Function populateReplacementList()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        Dim searchtext As String = ddltxtSearch.Value
        
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReplacementBL.getReplacementList(resultDataSet, objPageSortBo, ddlComponents.SelectedValue, ddlReplacementYear.SelectedValue, ddlScheme.SelectedValue, ddlAppointmentStatus.SelectedValue, ddlYearOperator.SelectedValue, Not ApplicationConstants.ExportFullList, searchtext)
        SessionManager.setReplacementListDataSet(resultDataSet)
        Return bindGrid(resultDataSet)

    End Function

#End Region

#Region "Bind Grid"
    ''' <summary>
    ''' Bind Grid
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function bindGrid(ByVal resultDataSet As DataSet)
        'Variables Declaration
        Dim recordFound As Boolean = False
        Dim totalCost As String
        Dim totalCount As Integer
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 5)
        Dim distinctColumns As String() = {"Scheme", "Address", "ComponentName", "ReplacedDate", "DueDate", "ReplacementDueDate", "LastSurveyDate", "AppointmentStatus", "AppointmentDate", "TotalCost", "SchemeId", "SID", "HouseNumber", "PrimaryAddress", "POSTCODE", "ReplacedDateSort", "ReplacementDueDateSort", "LastSurveyDateSort", "AppointmentDateSort", "PropertyId", "ComponentId", "StatusId", "InspectionId", "TotalCostSort"}

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        grdReplacementList.DataSource = resultDataSet.Tables(ApplicationConstants.ReplacementListDataTable).DefaultView.ToTable(True, distinctColumns)

        grdReplacementList.DataBind()

        Dim chkSelectAll As CheckBox = DirectCast(grdReplacementList.HeaderRow.FindControl("chkSelectAll"), CheckBox)

        If (resultDataSet.Tables(ApplicationConstants.ReplacementListDataTable).Rows.Count > 0) Then

            totalCost = resultDataSet.Tables(ApplicationConstants.ReplacementTotalCostDataTable).Rows(0)(0).ToString()
            lblTotalCost.Text = "£" + totalCost

            ddlAppointmentStatus.Visible = True
            pnlTotalCost.Visible = True
            Label3.Visible = True
            txtSelectedCost.Visible = True
            btnExportList.Visible = True
            btnScheduleSelected.Visible = True
            'pnlSelectedProperties.Visible = True
            chkSelectAll.Visible = True
            recordFound = True

            totalCount = resultDataSet.Tables(ApplicationConstants.ReplacementTotalCountDataTable).Rows(0)(0)

        Else
            If (ddlAppointmentStatus.Visible = False) Then
                ddlAppointmentStatus.Visible = False
            End If

            pnlTotalCost.Visible = False
            Label3.Visible = False
            txtSelectedCost.Visible = False
            btnExportList.Visible = False
            chkSelectAll.Visible = False
            btnScheduleSelected.Visible = False
            'pnlSelectedProperties.Visible = False
            recordFound = False

            totalCount = 0

        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        txtSelectedCost.Text = String.Empty

        Return recordFound

    End Function

#End Region

#Region "Populate Replacement List via Query string"

    ''' <summary>
    ''' Populate Replacement List via Query string
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementListQueryString()

        Dim currentDate = DateTime.Now
        Dim currentYear As Integer = currentDate.Year
        'currentYear = IIf(currentDate.CompareTo(New Date(currentYear, 4, 1)) < 0, currentYear - 1, currentYear)
        ddlReplacementYear.SelectedValue = currentYear

        If Not IsNothing(Request.QueryString(PathConstants.Component)) And Not IsNothing(Request.QueryString(PathConstants.Replacement)) Then

            Dim selectComponent As String = Request.QueryString(PathConstants.Component).ToString()
            ddlComponents.SelectedValue = selectComponent

            If Request.QueryString(PathConstants.Replacement).ToString() = PathConstants.Overdue Then
                ddlYearOperator.SelectedValue = ApplicationConstants.YearLessThan
            ElseIf Request.QueryString(PathConstants.Replacement).ToString() = PathConstants.NotSchedule Then
                ddlYearOperator.SelectedValue = ApplicationConstants.YearLessThanEqual
            ElseIf Request.QueryString(PathConstants.Replacement).ToString() = PathConstants.AllYears Then
                ddlYearOperator.SelectedValue = ApplicationConstants.YearLessThanEqual
            Else
                ddlYearOperator.SelectedValue = ApplicationConstants.YearEqual
            End If

        End If
        populateReplacementList()

    End Sub


#End Region

#Region "set Previuosly Selected Start Time"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="appointmentStartTime"></param>    
    ''' <remarks></remarks>
    Private Sub setPreviuoslySelectedStartTime(ByVal appointmentStartTime As String)
        ViewState(ViewStateConstants.PreviouslySelecteAppointmentStartTime) = appointmentStartTime
    End Sub
#End Region

#Region "get Previuosly Selected Start Time"
    ''' <summary>
    ''' 
    ''' </summary>    
    ''' <remarks></remarks>
    Private Function getPreviuoslySelectedStartTime()
        Return ViewState(ViewStateConstants.PreviouslySelecteAppointmentStartTime)
    End Function
#End Region

#Region "set Previuosly Selected End Time"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="appointmentEndTime"></param>
    ''' <remarks></remarks>
    Private Sub setPreviuoslySelectedEndTime(ByVal appointmentEndTime As String)
        ViewState(ViewStateConstants.PreviouslySelecteAppointmentEndTime) = appointmentEndTime
    End Sub
#End Region

#Region "get Previuosly Selected End Time"
    ''' <summary>
    ''' 
    ''' </summary>    
    ''' <remarks></remarks>
    Private Function getPreviuoslySelectedEndTime()
        Return ViewState(ViewStateConstants.PreviouslySelecteAppointmentEndTime)
    End Function
#End Region

#Region "Email Functionality"

#Region "Send iCal File via Email"
    ''' <summary>
    ''' Send iCal File via Email
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub sendICalFileViaEmail()

        Dim objArrangeInspectionBO As ArrangeInspectionBO = New ArrangeInspectionBO()
        objArrangeInspectionBO = Me.getArrangeInspectionBO()

        Dim resultDataset As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getOperativeInformation(resultDataset, objArrangeInspectionBO.OperativeId)

        Dim operativeName As String = String.Empty
        Dim operativeEmail As String = String.Empty
        operativeName = resultDataset.Tables(0).Rows(0)("Name")
        If (resultDataset.Tables(0).Rows(0)("Email").Equals(DBNull.Value)) Then
            operativeEmail = String.Empty
        Else
            operativeEmail = resultDataset.Tables(0).Rows(0)("Email")
        End If


        Dim body As String = Me.populateBody(objArrangeInspectionBO, lblPropertyAddress.Text)
        Dim subject As String = "Inspection Arranged"
        Dim recepientEmail As String = operativeEmail

        Try
            If isEmail(recepientEmail) Then
                SyncLock lockObject
                    populateiCalFile(operativeName, operativeEmail, lblPropertyAddress.Text)
                    EmailHelper.sendHtmlFormattedEmailWithAttachment(operativeName, recepientEmail, subject, body, Server.MapPath(PathConstants.ICalFilePath))
                End SyncLock
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.InspectionSavedEmailError + " " + UserMessageConstants.InvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InspectionSavedEmailError + ex.Message
            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InspectionSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try

    End Sub
#End Region

#Region "Populate Email body"
    ''' <summary>
    ''' Populate Email body
    ''' </summary>
    ''' <param name="ArrangeInspectionBO"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function populateBody(ArrangeInspectionBO As ArrangeInspectionBO, PropertyAddress As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/InspectionConfirmation.htm"))
        body.Append(reader.ReadToEnd.ToString)
        body.Replace("{AppointmentTime}", ArrangeInspectionBO.AppointmentStartTime + " - " + ArrangeInspectionBO.AppointmentEndTime)
        body.Replace("{AppointmentDate}", ArrangeInspectionBO.AppointmentDate)
        body.Replace("{Address}", PropertyAddress)
        Return body.ToString()
    End Function
#End Region

#Region "Validate Email"
    ''' <summary>
    ''' Validate Email
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function isEmail(ByVal value As String) As Boolean
        Dim rgx As New Regex(RegularExpConstants.emailExp)
        If rgx.IsMatch(value) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Populate iCal File"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateiCalFile(ByVal operativeName As String, ByVal operativeEmail As String, ByVal propertyAddress As String)

        Dim objArrangeInspectionBO As ArrangeInspectionBO = New ArrangeInspectionBO()
        objArrangeInspectionBO = Me.getArrangeInspectionBO()

        Dim startTime As DateTime = DateTime.Parse(Convert.ToDateTime(objArrangeInspectionBO.AppointmentDate + " " + objArrangeInspectionBO.AppointmentStartTime))
        Dim endTime As DateTime = DateTime.Parse(Convert.ToDateTime(objArrangeInspectionBO.AppointmentDate + " " + objArrangeInspectionBO.AppointmentEndTime))

        Dim nowasUniversalTime As String = DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ")

        Dim iCalString As StringBuilder = New StringBuilder()
        iCalString.AppendLine("BEGIN:VCALENDAR")
        iCalString.AppendLine("PRODID:-//Apple Inc.//iCal 5.0.2//EN")
        iCalString.AppendLine("VERSION:2.0")
        iCalString.AppendLine("CALSCALE:GREGORIAN")
        iCalString.AppendLine("METHOD:PUBLISH")
        'iCalString.AppendLine("X-WR-RELCALID: BHGCalendar")
        iCalString.AppendLine("X-WR-CALNAME:BHG Calendar")
        iCalString.AppendLine("X-WR-TIMEZONE:" + TimeZone.CurrentTimeZone.ToString())
        iCalString.AppendLine("X-WR-CALDESC:BHG Calendar")
        iCalString.AppendLine("BEGIN:VEVENT")
        iCalString.AppendLine("ATTENDEEROLE=REQ-PARTICIPANTCN=" + operativeName + ":MAILTO:" + operativeEmail)
        'iCalString.AppendLine("ORGANIZERCN=" + aaveData.inviterName + ":MAILTO:" + aaveData.inviterEmail)
        iCalString.AppendLine("DTSTART:" + startTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        iCalString.AppendLine("DTEND:" + endTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        iCalString.AppendLine("DTSTAMP:" + nowasUniversalTime)
        iCalString.AppendLine("UID:" + Convert.ToString(DateTime.Now.Ticks))
        iCalString.AppendLine("CREATED:" + nowasUniversalTime)
        iCalString.AppendLine("SUMMARY:" + "Planned Inspection")
        iCalString.AppendLine("DESCRIPTION:" + "An Inspection has been arranged.")
        iCalString.AppendLine("LAST-MODIFIED:" + nowasUniversalTime)
        iCalString.AppendLine("LOCATION:" + propertyAddress)
        iCalString.AppendLine("SEQUENCE:0")
        iCalString.AppendLine("STATUS:CONFIRMED")
        iCalString.AppendLine("TRANSP:TRANSPARENT")
        iCalString.AppendLine("END:VEVENT")
        iCalString.AppendLine("END:VCALENDAR")

        Dim iCalTextWriter As TextWriter = New StreamWriter(Server.MapPath(PathConstants.ICalFilePath))
        iCalTextWriter.WriteLine(iCalString)
        iCalTextWriter.Close()
    End Sub

#End Region

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

End Class