﻿Imports PL_Utilities

Public Class Components
    Inherits PageBase

#Region "Events"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Me.highLightCurrentPageMenuItems()
            Me.populateComponentList()
        End If
    End Sub
#End Region

#Region "Functions"
#Region "Populate Component List"
    ''' <summary>
    ''' Populate Component List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateComponentList()

        Dim search As String = ""
        Dim resultDataSet As DataSet = New DataSet()

        ComponentList.populateComponentList(resultDataSet, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.CompleteDataSet, resultDataSet)

    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnLifecycles"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnComponents"))
    '        'Add update panel that should be visible
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelSubLifecycles"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#End Region
End Class