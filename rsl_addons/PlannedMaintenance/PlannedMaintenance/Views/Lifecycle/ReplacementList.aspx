﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ReplacementList.aspx.vb" Inherits="PlannedMaintenance.ReplacementList" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style>
        .message
        {
            margin-left: 11px;
        }
        .searchbox
        {
            border: 1px solid Gray;
            background-image:none; 
            padding:2px; 
            font-size:12px;
            margin : 5px 5px 5px 5px;
            
            height:17px;
        }
        .list-select select
        {
            border-radius: 0px;
            border: 1px solid #b1b1b1;
            height: 25px !important;
            font-size: 12px !important;
            width: 205px !important;
            margin: -4px 10px 0 0;
        }
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
        .text_div{
            width: inherit;
        }
    </style>
    <script src="../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            var dllAppointmentStatus = document.getElementById("ContentPlaceHolder1_AppointmentStatus");
            if (dllAppointmentStatus == null) {
                $("#divAppointmentStatus").hide()
            }
            var ddlYearOperator = document.getElementById("ContentPlaceHolder1_ddlYearOperator");
            if (ddlYearOperator == null) {
                $("#divYearOperator").hide()
            }
        });

        /////// Upload Letter Work /////// 

        function openAddUploadWindow() {
            wd = window.open('../Common/UploadDocument.aspx?type=document&popType=1', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function openEditLetterWindowForAdd() {

            var letterId = document.getElementById('<%= ddlLetter.ClientID %>').value;
            if (letterId == -1) {

            }
            else {
                w = window.open('../../Views/Resources/EditLetter.aspx?pg=0&id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
            }
        }

        var w;
        function uploadError(sender, args) {
            alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
        }

        function uploadComplete() {
            __doPostBack();
        }
        function fireCheckboxEventForAddApp() {
            document.getElementById('<%= ckBoxAddRefreshDataSet.ClientID %>').checked = true;
            setTimeout('__doPostBack(\'<%= ckBoxAddRefreshDataSet.UniqueID %>\',\'\')', 0);
        }
        function fireAddDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDocumentUpload.UniqueID%>\',\'\')', 0);
        }

        /////// Selection Work /////// 

        function checkAllProperties(Checkbox) {

            var GridVwHeaderChckbox = document.getElementById("<%=grdReplacementList.ClientID %>");
            var txtBoxTotalSelectedCost = document.getElementById("<%=txtSelectedCost.ClientID %>");

            var totalSelectedCost = 0;
            var singleSelectedCost = 0;
            var checkState;

            if (Checkbox.checked && txtBoxTotalSelectedCost.value.length > 0) {
                txtBoxTotalSelectedCost.value = "";
            }

            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
                checkState = GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked;
                singleSelectedCost = calculateSelectedProperties(i, checkState);
                totalSelectedCost = totalSelectedCost + singleSelectedCost;
            }

        }

        function checkAllSelected() {
            var GridVwHeaderChckbox = document.getElementById("<%=grdReplacementList.ClientID %>");
            var selectedCount = 0;
            var totalCount = 0;

            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                if (GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    selectedCount = selectedCount + 1;
                }

            }

            if (selectedCount == GridVwHeaderChckbox.rows.length - 1) {
                return true;
            } else {
                return false;
            }


        }

        function checkSingleProperty(rowIndex, checkState) {

            var GridVwHeaderChckbox = document.getElementById("<%=grdReplacementList.ClientID %>");
            if (GridVwHeaderChckbox.rows[0].cells[0].getElementsByTagName("INPUT")[0].checked && !checkState) {
                GridVwHeaderChckbox.rows[0].cells[0].getElementsByTagName("INPUT")[0].checked = checkState;
            }

            var txtBoxTotalSelectedCost = document.getElementById("<%=txtSelectedCost.ClientID %>");
            var totalSelectedCost = 0;
            totalSelectedCost = calculateSelectedProperties(rowIndex, checkState);

            if (checkAllSelected()) {
                GridVwHeaderChckbox.rows[0].cells[0].getElementsByTagName("INPUT")[0].checked = checkState;
            }

        }

        function calculateSelectedProperties(rowIndex, checkState) {
            var GridVwHeaderChckbox = document.getElementById("<%=grdReplacementList.ClientID %>");
            var totalSelectedCost = 0;
            var singleSelectedCost = 0;

            var txtBoxTotalSelectedCost = document.getElementById("<%=txtSelectedCost.ClientID %>");
            if (txtBoxTotalSelectedCost.value.length > 0) {
                totalSelectedCost = txtBoxTotalSelectedCost.value.replace(/,/g, '');
                totalSelectedCost = parseFloat(totalSelectedCost.replace(/£/g, ''));
            }

            singleSelectedCost = parseFloat(GridVwHeaderChckbox.rows[rowIndex].cells[10].childNodes[1].childNodes[1].innerHTML.replace(/,/g, ''));

            if (checkState) {
                totalSelectedCost = totalSelectedCost + singleSelectedCost;
            } else {
                totalSelectedCost = totalSelectedCost - singleSelectedCost;
            }

            if (totalSelectedCost == 0) {
                txtBoxTotalSelectedCost.value = "";
            } else {
                txtBoxTotalSelectedCost.value = "£" + totalSelectedCost.toLocaleString();
            }

            return totalSelectedCost;

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelReplacementList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header pnlHeading">
                    <span class="header-label">Replacement List</span>
                    <asp:Panel ID="pnlSearch" runat="server" CssClass="right" Visible="True">
                        <div class="field right">
                            <asp:Button runat="server" ID="btnGO" UseSubmitBehavior="false" Text="GO" OnClick="imgSearchbtn_DataBinding"
                                CssClass="btn btn-xs btn-blue right" style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                            <input id="ddltxtSearch" runat="server" type="text" placeholder="Search"
                                style="padding:3px 10px !important; margin:-4px 10px 0 0; float:right" class="styleselect-control searchbox" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div style="overflow:hidden; border-bottom: 1px solid #A0A0A0;">
                        <div id="dropdownContainer ">
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlComponents" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlReplacementYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlScheme" runat="server" AutoPostBack="True">
                                            <asp:ListItem Selected="True" Value="-1">Please Select Scheme</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control" id="divAppointmentStatus">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlAppointmentStatus" runat="server" AutoPostBack="True" Visible="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control" id="divYearOperator">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlYearOperator" runat="server" AutoPostBack="True" Visible="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnGenerateList" runat="server" Text="Generate List"
                                CssClass="btn btn-xs btn-blue right" style="padding: 3px 10px !important; margin: 10px 10px 0 0;" />
                        </div>
                    </div>
                    <div style="overflow:hidden; border-bottom: 1px solid #A0A0A0; padding-bottom:10px;">
                        <div class="form-control">
                            <div class="text_div" >
                                <div class="label">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Total selected:" Visible="false"></asp:Label>
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtSelectedCost" runat="server" Font-Bold="true" CssClass="textBoxInnerText" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <asp:Button ID="btnScheduleSelected" runat="server" Text="Approve selected properties" Visible="false"
                                CssClass="btn btn-xs btn-blue right" style="padding: 3px 10px !important; margin: 10px 10px 0 0;" />
                        </div>
                    </div>
                    <div id="replacementlistContainer">
                            <div style="width: 100%; padding:0; border-bottom: 1px solid #A0A0A0;">
                                <cc2:PagingGridView ID="grdReplacementList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                                    Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                                    GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="5">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="40px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="checkAllProperties(this);" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkReplacementItem" runat="server" onclick=""></asp:CheckBox>
                                                <asp:Label ID="lblPropertyId" Visible="false" runat="server" Text='<%# Bind("PropertyId") %>'></asp:Label>
                                                <asp:Label ID="lblComponentId" Visible="false" runat="server" Text='<%# Bind("ComponentId") %>'></asp:Label>
                                                <asp:Label ID="lblStatusId" Visible="false" runat="server" Text='<%# Bind("StatusId") %>'></asp:Label>
                                                <asp:Label ID="lblInspectionId" Visible="false" runat="server" Text='<%# Bind("InspectionId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="15%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="40%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Postcode:" SortExpression="POSTCODE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPOSTCODE" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Component:" SortExpression="ComponentName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("ComponentName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Replaced/Installed:" SortExpression="ReplacedDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReplacedDate" runat="server" Text='<%# Bind("ReplacedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Replacement Due:" SortExpression="ReplacementDueDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReplacementDueDate" runat="server" Text='<%# Bind("ReplacementDueDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Stock Survey:" SortExpression="LastSurveyDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastSurveyDate" runat="server" Text='<%# Bind("LastSurveyDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Appointment Status:" SortExpression="AppointmentStatus">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAppointmentStatus" runat="server" Text='<%# Bind("AppointmentStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Inspection Date:" SortExpression="AppointmentDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAppointmentDate" runat="server" Text='<%# Bind("AppointmentDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="20%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total:" SortExpression="TotalCost">
                                            <ItemTemplate>
                                                <b>£<asp:Label ID="lblTotalCost" runat="server" Text='<%# Bind("TotalCost") %>'></asp:Label></b>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnInspection" runat="server" OnClick="lnkBtnArrangeInspection"
                                                    CommandArgument='<%# Eval("SID") %>'>
                                                <img src="../../Images/inspection.png" alt="Inspection" style="border:none;" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                </cc2:PagingGridView>
                            </div>
                            <%--Pager Template Start--%>
                            <div style="border-bottom: 1px solid #A0A0A0;">
                                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                                    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                                        <div class="paging-left">
                                            <span style="padding-right:10px;">
                                                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                                    &lt;&lt;First
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                                    &lt;Prev
                                                </asp:LinkButton>
                                            </span>
                                            <span style="padding-right:10px;">
                                                <b>Page:</b>
                                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                                of
                                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                            </span>
                                            <span style="padding-right:20px;">
                                                <b>Result:</b>
                                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                to
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                of
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                            </span>
                                            <span style="padding-right:10px;">
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                                </asp:LinkButton>
                                            </span>
                                        </div>
                                        <div style="width:40%; float:left">
                                            <div class="right">
                                                <span>
                                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                                                </span>
                                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                                <div class="field right" style="margin-right: 10px;">
                                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <%--Pager Template End--%>
                            <asp:Panel ID="pnlTotalCost" runat="server" Visible="false">
                                <div class="form-control">
                                    <div class="select_div" style="padding:15px 15px 15px 10px;">
                                        <div class="">
                                            <asp:Label ID="lblTotalCost" CssClass="rightControl" runat="server" Text="" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="Label1" runat="server" CssClass="rightControl" Text="Total:" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="right">
                                    <asp:Button ID="btnExportList" runat="server" Text="Export List" Visible="false" 
                                        CssClass="btn btn-xs btn-blue right" style="padding: 2px 10px !important; margin: 10px 10px 0 0;" />
                                    &nbsp
                                    <asp:Button ID="btnExportSelected" runat="server" Text="Export selections"
                                        CssClass="btn btn-xs btn-blue right" style="padding: 2px 10px !important; margin: 10px 10px 10px 0;"/>
                                </div>
                            </asp:Panel>
                        
                        <br />
                    
                        <br />
                        <%-- ARRANGE INSPECTION POPUP --%>
                        <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                        <asp:ModalPopupExtender ID="mdlPopupArrangeInspection" runat="server" TargetControlID="btnHidden"
                            PopupControlID="pnlArrangeInspection" Enabled="true" DropShadow="true" CancelControlID="btnCancel">
                        </asp:ModalPopupExtender>
                        <asp:Panel ID="pnlArrangeInspection" CssClass="left" runat="server" BackColor="White"
                            Width="400px" Height="500px" Style="padding: 15px 5px; border: 1px solid black;
                            display: none;">
                            <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                                <asp:Label ID="Label2" Text="Arrange Inspection" Font-Bold="true" runat="server"></asp:Label>
                                <asp:Button ID="btnArrange" runat="server" CssClass="rightControl" Text="Arrange"
                                    BackColor="White" />
                                <asp:Button ID="btnCancel" CssClass="rightControl" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
                            </div>
                            <br />
                            <hr />
                            <div style="clear: both; height: 1px;">
                            </div>
                            <div style="padding-left: 10px;">
                                <asp:Label ID="lblPropertyAddress" Font-Bold="true" CssClass="leftControl" runat="server"
                                    Text=""></asp:Label>
                            </div>
                            <br />
                            <hr />
                            <%--<asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
                                top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />--%>
                            <div style="overflow: auto; height: 470px; padding-top: 5px !important; padding-left: 10px;">
                                <asp:UpdatePanel ID="updPanelAddAppointmentPopup" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                    <asp:Panel ID="pnlPopupMessage" runat="server" Visible="False">
                                                        <asp:Label ID="lblPopupMessage" runat="server"></asp:Label>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Tenant:
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblTenantName" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" valign="top">
                                                    <asp:Label ID="lblHiddenTenancyId" runat="server" Visible="false" Text=""></asp:Label>
                                                    <asp:Label ID="lblHiddenPropertyId" runat="server" Visible="false" Text=""></asp:Label>
                                                    <asp:Label ID="lblHiddenComponentId" runat="server" Visible="false" Text=""></asp:Label>
                                                    <asp:Label ID="lblHiddenStatusId" runat="server" Visible="false" Text=""></asp:Label>
                                                    <asp:Label ID="lblHiddenInspectionId" runat="server" Visible="false" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Tel:
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtTelepone" ReadOnly="true" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Mob:
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtMobile" runat="server" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Date:
                                                </td>
                                                <td align="left" valign="middle" class="style5">
                                                    <span class="Required">*</span>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                                        PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                                                        Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:TextBox ID="txtDate" runat="server" Text=""></asp:TextBox>
                                                    &nbsp;<img alt="Open Calendar" src="../../Images/calendar2.png" style="vertical-align: bottom;"
                                                        id="imgCalDate" />
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Surveyor:
                                                </td>
                                                <td align="left" valign="middle" class="style5">
                                                    <span class="Required">*</span>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:DropDownList ID="ddlOperative" runat="server" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:LinkButton ID="lnkBtnRefreshList" runat="server">Refresh</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Start:
                                                </td>
                                                <td align="left" valign="middle" class="style5">
                                                    <span class="Required">*</span>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:DropDownList ID="ddlStartTime" runat="server" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top">
                                                    <%--<asp:LinkButton ID="lnkBtnRefreshTime" runat="server" ToolTip="Click on this to refresh the time slots of selected operative">Refresh</asp:LinkButton>&nbsp;--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    End:
                                                </td>
                                                <td align="left" valign="middle" class="style5">
                                                    <span class="Required">*</span>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:DropDownList ID="ddlEndTime" runat="server" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Action:
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                    Letter:
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:DropDownList ID="ddlLetter" runat="server" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="right" valign="top">
                                                    <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                                                        <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                                            <ItemTemplate>
                                                                <table class="LetterDocInnerTable">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                                                Visible="<%# False %>"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                                                Visible="<%# False %>"></asp:Label>
                                                                        </td>
                                                                        <td style="text-align: right">
                                                                            <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                                                BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue") + "%%" + Eval("LetterId")%>'
                                                                                OnClick="imgBtnRemoveLetterDoc_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <SelectedItemStyle BackColor="Gray" />
                                                        </asp:DataList>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" class="style5">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" class="style5">
                                                </td>
                                                <td align="right" valign="top">
                                                    <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAddUploadWindow()"
                                                        BackColor="White" />
                                                    &nbsp;
                                                    <asp:Button ID="btnAddViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindowForAdd()"
                                                        BackColor="White" />
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
                            CssClass="hiddenField" />
                        <asp:CheckBox ID="ckBoxAddRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
                            CssClass="hiddenField" />
                        <%--     <asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
                <asp:ModalPopupExtender ID="mdlPopUpAddAppointment" runat="server" DynamicServicePath=""
                    Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlAddAppointment"
                    DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnCancelAction"
                    BehaviorID="mdlPopupAddAppointmentBehaviorId">
                </asp:ModalPopupExtender>--%>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlSuccessPopup" runat="server">
                <asp:ImageButton ID="imgBtnCancelSuccessPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    BorderWidth="0" />
                <asp:Panel ID="pnlSuccessAppointmentMessage" runat="server" CssClass="pnl-apt-success-msg">
                    <asp:Label ID="lblSuccessAppointmentMessage" runat="server" Text="Apointment has been saved successfully."></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlSuccessPopup" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlSuccessPopup"
                DropShadow="true" CancelControlID="imgBtnCancelSuccessPopup" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportList" />
            <asp:PostBackTrigger ControlID="btnExportSelected" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
