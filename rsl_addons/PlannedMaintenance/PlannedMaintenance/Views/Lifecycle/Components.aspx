﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="Components.aspx.vb" Inherits="PlannedMaintenance.Components" %>

<%@ Register Src="~/Controls/Component/ComponentList.ascx" TagName="ComponentList"
    TagPrefix="cl" %>
<%@ Register Src="~/Controls/Component/ViewComponent.ascx" TagName="ViewComponent"
    TagPrefix="c2" %>
<%@ Register Src="~/Controls/Component/EditComponent.ascx" TagName="EditComponent"
    TagPrefix="c3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
    </style>
    <script type="text/javascript">
        function NumberOnly(field) {
           // var re = /^[0-9.]*$/;
            var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelComponentList" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="portlet">
                <div class="header pnlHeading">
                    <span class="header-label">Component</span>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <div class="">
                        <div class="componentListContainer ">
                            <cl:ComponentList runat="server" ID="ComponentList" />
                        </div>
                        <div class="viewComponentContainer">
                            <c2:ViewComponent ID="ViewComponent" runat="server" Visible="false" />
                            <c3:EditComponent ID="EditComponent" runat="server" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
