﻿Imports AjaxControlToolkit

Public Class EditLetter
    Inherits PageBase

    Public txtLetterTitle As TextBox = New TextBox()
    Public lblLetterTitle As Label = New Label()
    Public lblLetterBody As Label = New Label()
    Public richTxtComments As HtmlEditorExtender = New HtmlEditorExtender()
    Public stdLetterId As Integer

#Region "Pre Init"
    ''' <summary>
    ''' pre init 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page load event"

    ''' <summary>
    ''' Page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request.QueryString("id")) Then
            stdLetterId = CType(Request.QueryString("id"), Integer)
        End If

        Me.FindControlsOfUserControl()
    End Sub
#End Region
#Region "Find Controls"
    ''' <summary>
    ''' Find Controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub FindControlsOfUserControl()
        txtLetterTitle = DirectCast(EditLetterId.FindControl("txtLetterTitle"), TextBox)
        richTxtComments = DirectCast(EditLetterId.FindControl("richTxtComments"), HtmlEditorExtender)

    End Sub
#End Region

End Class