﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master" CodeBehind="ViewLetters.aspx.vb" Inherits="PlannedMaintenance.ViewLetters" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="group">
        <div class="portlet">
            <div class="header">
                <span class="header-label">Template Management</span>
                <span class="right" style="margin-right:10px">
                    <asp:Button ID="Button1" class="btn btn-xs btn-blue right" style="padding:3px 23px !important; margin:-3px 0 0 0;" runat="server" Text="Add a new template"
                        PostBackUrl="~/Views/Resources/CreateLetter.aspx" BackColor="White" />
                </span>
            </div>
            <div class="portlet-body">
                <asp:Panel ID="Panel1" runat="server" Style="float: right; height: 558px; margin-left: 1%; width: 100%;"
                    align="left">
                    <asp:UpdatePanel runat="server" ID="pnlTemplateManagement" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <br />
                                <br />
                            </asp:Panel>
                            <div class="panel panel-default">
                                <div class="panel-heading" style="padding:0;">
                                    <div class="row">
                                        <div class="no-form-control">
                                            <asp:DropDownList ID="ddlStatus" AutoPostBack="true" class="styleselect no-form-control-select"
                                                runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="no-form-control">
                                            <asp:TextBox ID="txtLetterTitle" class="no-form-control-input" runat="server" placeholder="Letter Title"></asp:TextBox>
                                        </div>
                                        <div class="no-form-control">
                                            <asp:DropDownList ID="ddlAction" class="styleselect no-form-control-select"
                                                runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="no-form-control">
                                            <asp:TextBox ID="txtLetterCode" class="no-form-control-input" runat="server" placeholder="Letter Code"></asp:TextBox>
                                        </div>
                                        <div style="padding:13px 10px 0 0">
                                            <asp:Button ID="btnSearch" class="btn btn-xs btn-blue right" style="padding:3px 37px !important; margin:-3px 0 0 0;" runat="server" Text="Search" BackColor="white" />
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body" style="padding:0">
                                    <asp:Panel ID="Panel2" runat="server" Style="float: right; margin-left: 1%; margin-bottom: 2%; width: 100%;"
                                        align="left" ScrollBars="Auto">
                                        <asp:Panel ID="pnlGridMessage" runat="server" Visible="false">
                                            <asp:Label ID="lblGridMessage" runat="server" Text=""></asp:Label>
                                        </asp:Panel>
                                            <asp:GridView ID="grdViewLetters" runat="server" AutoGenerateColumns="False"
                                                AllowSorting="True" PageSize="30"
                                                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                                                GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Code" ItemStyle-CssClass="dashboard">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle BorderStyle="None" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatusTitle" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblActionTitle" runat="server" Text='<%# Bind("ActionTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Title">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Modified">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblModifiedDate" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="By">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" Text="Delete" CommandArgument='<%# Eval("StandardLetterId") %>' 
                                                                OnClick="lnkBtnDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this template?');" 
                                                                class="btn btn-xs btn-blue right" style="padding:2px 10px !important; margin:0;" />
                                                            <a href='CreateLetter.aspx?lid=<%# Eval("StandardLetterId") %>'
                                                                class="btn btn-xs btn-blue right" style="padding:2px 10px !important; margin:0 5px 0 0;">Edit</a>
                                                            <a href='PreviewLetter.aspx?lid=<%# Eval("StandardLetterId") %>&src=ViewLetters'
                                                                class="btn btn-xs btn-blue right" style="padding:2px 10px !important; margin:0 5px 0 0;">Preview</a>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                            </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateProgressTemplateManagement" runat="server" AssociatedUpdatePanelID="pnlTemplateManagement"
                        DisplayAfter="10">
                        <ProgressTemplate>
                            <div class="loading-image">
                                <img alt="Please Wait" src="../../Images/progress.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
