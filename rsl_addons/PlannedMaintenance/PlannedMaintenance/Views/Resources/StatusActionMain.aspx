﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="StatusActionMain.aspx.vb" Inherits="PlannedMaintenance.StatusActionMain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="status" TagName="Status" Src="~/Controls/Resources/StatusList.ascx" %>
<%@ Register TagPrefix="letters" TagName="Letters" Src="~/Controls/Resources/Letters.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Resources" class="group">
        <div class="portlet">
            <div class="header">
                <span class="header-label">Resources</span>
            </div>
            <div class="portlet-body">
                <div style="padding:10px;">
                    <asp:UpdatePanel runat="server" ID="updPnlResources" UpdateMode="Conditional" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <div id="rightpanel">
                                <asp:Panel runat="server" ID="pnlStatusAndLetters">
                                        <asp:Label ID="Label1" runat="server"></asp:Label>
                                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    </asp:Panel>
                                    <div style="width:inherit;">
                                        <div style="width:50%; float:left;">
                                            <asp:Panel ID="pnlStatus" runat="server" >
                                                <status:Status ID="Status" runat="server" />
                                            </asp:Panel>
                                        </div>
                                        <div style="width:50%; float:left;">
                                            <asp:Panel ID="pnlLetters" runat="server" >
                                                <letters:Letters ID="Letters" runat="server" />
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>    
</asp:Content>
