﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class CreateLetter
    Inherits PageBase
    Dim stdLetterId As Integer

#Region "Event Handlers"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub

#End Region

#Region "Page Load"
    'TODO: put contents of page load in separate function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.ClientScript.RegisterClientScriptBlock(Type.GetType("System.String"), "openPreview()", " function openPreview(){ window.open('PreviewLetter.aspx?src=CreateLetter');}", True)

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not (IsPostBack) Then
                'This function 'll hight light the menu link button and it 'll also make the update panel visible
                'Me.highLightCurrentPageMenuItems()
                Me.loadStatusDropDown()

                Dim strSrc As String = Request.QueryString(PathConstants.Src)
                If ((Not String.IsNullOrEmpty(strSrc)) And (strSrc = PathConstants.Resources)) Then
                    Me.btnCancel.PostBackUrl = PathConstants.StatusActionMain
                End If

                Dim strStdLetterID As String = Request.QueryString(PathConstants.LetterId)
                If (Int32.TryParse(strStdLetterID, stdLetterId)) Then
                    Me.stdLetterId = Int32.Parse(strStdLetterID)
                    Me.loadLetter()
                    'Me.btnPreviewStandardLetter.Enabled = vbYes
                    'Me.btnPreviewStandardLetter.PostBackUrl = "~/Views/Resources/PreviewLetter.aspx?src=CreateLetter&LetterId=" + strStdLetterID
                End If
                'Me.btnPreviewStandardLetter.PostBackUrl = "~/Views/Resources/PreviewLetter.aspx?src=CreateLetter"
                Dim letterBoForPreview As LetterBO = New LetterBO()
                letterBoForPreview = SessionManager.getLetterBoForPreview()

                If ((Not String.IsNullOrEmpty(letterBoForPreview.Body)) And (Not IsNothing(letterBoForPreview.Body))) Then
                    txtLetterTemplate.Text = letterBoForPreview.Body
                End If

                If ((Not String.IsNullOrEmpty(letterBoForPreview.Title)) And (Not IsNothing(letterBoForPreview.Title))) Then
                    txtLetterTitle.Text = letterBoForPreview.Title
                End If

                If ((Not String.IsNullOrEmpty(letterBoForPreview.Code)) And (Not IsNothing(letterBoForPreview.Code))) Then
                    txtLetterCode.Text = letterBoForPreview.Code
                End If

                If ((Not String.IsNullOrEmpty(letterBoForPreview.StatusId)) And (Not IsNothing(letterBoForPreview.StatusId)) And (Not letterBoForPreview.StatusId = 0)) Then
                    ddlStatus.SelectedValue = letterBoForPreview.StatusId
                    loadActionsDropDown()
                End If

                If ((Not String.IsNullOrEmpty(letterBoForPreview.ActionId)) And (Not IsNothing(letterBoForPreview.ActionId))) Then
                    ddlAction.SelectedValue = letterBoForPreview.ActionId
                End If

                SessionManager.removeLetterBoForPreview()

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub
#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppResources"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnLetters"))
    '        'Add update panel that should be visible
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelResources"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#Region "ddl Status Selected Index Changed"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            loadActionsDropDown()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "btn Add Standard Letter Click"
    Protected Sub btnAddStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddStandardLetter.Click
        Try
            Me.saveStandardLetter()
        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNotCreated, True)
        End Try

    End Sub
#End Region

#Region "btn Preview Standard Letter Click"
    Protected Sub btnPreviewStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPreviewStandardLetter.Click
        Try
            Me.previewLetter()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try



    End Sub
#End Region

#Region "save File"
    Protected Sub saveFile(ByVal sender As Object, ByVal e As AjaxControlToolkit.AjaxFileUploadEventArgs)

        Dim fullPath As String = "/Images/Upload_test/" + e.FileName

        'Save your File
        richTxtComments.AjaxFileUpload.SaveAs(Server.MapPath(fullPath))


        'Tells the HtmlEditorExtender where the file is otherwise it will render as: <img src="" />
        e.PostedUrl = fullPath
    End Sub
#End Region

#End Region


#Region "Functions"

#Region "Load Letter"

    Protected Sub loadLetter()
        Dim resultSet As DataSet = New DataSet()
        Dim objLetterBl As LettersBL = New LettersBL()
        objLetterBl.getLetterById(Me.stdLetterId, resultSet)

        Dim statusSelected As Integer = resultSet.Tables(0).Rows(0).Item("StatusId")
        Dim actionSelected As Integer = resultSet.Tables(0).Rows(0).Item("ActionId")

        Me.txtLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")
        Me.txtLetterCode.Text = resultSet.Tables(0).Rows(0).Item("Code")

        Me.txtLetterTemplate.Text = resultSet.Tables(0).Rows(0).Item("Body")
        ddlStatus.SelectedValue = statusSelected
        Me.loadActionsDropDown()
        ddlAction.SelectedValue = actionSelected
    End Sub
#End Region

#Region "Load Status Drop Down"
    Protected Sub loadStatusDropDown()
        Dim codeId As String = "StatusId"
        Dim codeName As String = "Title"
        Dim resultDataSet As DataSet = New DataSet()
        Dim statusBl As StatusActionBL = New StatusActionBL()
        'get all the statuses  
        statusBl.getAllStatuses(resultDataSet)

        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = codeId
        ddlStatus.DataTextField = codeName
        ddlStatus.DataBind()

        ddlStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub
#End Region

#Region "Load Action Drop Down"
    Protected Sub loadActionsDropDown()
        Dim statusSelected As Int32 = CType(ddlStatus.SelectedItem.Value, Int32)

        If (Not statusSelected = -1) Then

            'ddlAction.Items.Clear()
            'ddlAction.SelectedIndex = -1
            'ddlAction.SelectedValue = vbNull
            'ddlAction.ClearSelection()

            Dim actionId As String = "ActionId"
            Dim actionName As String = "Title"
            Dim resultDataSet As DataSet = New DataSet()
            Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
            objStatusActionBl.getActionsByStatusId(statusSelected, resultDataSet)

            ddlAction.DataSource = resultDataSet
            ddlAction.DataValueField = actionId
            ddlAction.DataTextField = actionName
            ddlAction.DataBind()
        End If

        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue

        pnlAddLetterTemplate.Update()
    End Sub
#End Region

#Region "Load Actions"
    Protected Sub loadActions()
        Dim resultset As DataSet = New DataSet()
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        objStatusActionBl.getAllActions(resultset)
    End Sub
#End Region

    'TODO: save the letter in cache
    'TODO: Replace old validation with microsoft validation library
#Region "save Standard Letter"
    Private Sub saveStandardLetter()
        Dim statusSelected As Int32 = CType(CType((pnlAddLetterTemplate.FindControl("ddlStatus")), DropDownList).SelectedItem.Value, Int32)
        Dim actionSelected As Int32 = CType(CType((pnlAddLetterTemplate.FindControl("ddlAction")), DropDownList).SelectedItem.Value, Int32)

        Dim title As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTitle")), TextBox).Text, String)
        Dim code As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterCode")), TextBox).Text, String)
        Dim body As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTemplate")), TextBox).Text, String)

        Dim strStdLetterId As String = Request.QueryString(PathConstants.LetterId)
        If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
            Me.stdLetterId = Int32.Parse(strStdLetterId)
        Else
            Me.stdLetterId = -1
        End If

        If (statusSelected = ApplicationConstants.DropDownDefaultValue) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoStatus, True)
            Return
        ElseIf (actionSelected = ApplicationConstants.DropDownDefaultValue) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoAction, True)
            Return
        ElseIf (IsNothing(title) Or title = String.Empty) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoTitle, True)
            Return
        ElseIf (IsNothing(code) Or code = String.Empty) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoCode, True)
            Return
            'ElseIf (Not Int32.TryParse(code, Nothing)) Then
            '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterCodeNotNumeric, True)
            '    Return
        ElseIf (body.Length <= 3 Or IsNothing(body) Or body = String.Empty) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoBody, True)
            Return
        End If

        Dim resultset As DataSet = New DataSet()
        Dim objLetterBl As LettersBL = New LettersBL()
        Dim letterBO As LetterBO

        If (Me.stdLetterId = -1) Then
            letterBO = New LetterBO(statusSelected, actionSelected, title, code, body, 615, 615)
            objLetterBl.addStandardLetterTemplate(letterBO, resultset)
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterCreated, False)
        Else
            letterBO = New LetterBO(Me.stdLetterId, statusSelected, actionSelected, title, code, body, 615, 615)
            objLetterBl.updateStandardLetterTemplate(letterBO, resultset)
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterUpdated, False)
        End If

        Me.btnCancel.Text = "Back To List"

        If (Me.stdLetterId = -1) Then
            Me.loadStatusDropDown()
            Me.loadActionsDropDown()

            Me.txtLetterCode.Text = String.Empty
            Me.txtLetterTemplate.Text = String.Empty
            Me.txtLetterTitle.Text = String.Empty
        End If
    End Sub
#End Region

#Region "preview Letter"
    Private Sub previewLetter()
        Dim title As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTitle")), TextBox).Text, String)
        Dim body As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTemplate")), TextBox).Text, String)
        Dim code As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterCode")), TextBox).Text, String)

        Dim statusId As Integer = CType(ddlStatus.SelectedValue, Integer)
        Dim actionId As Integer = CType(ddlAction.SelectedValue, Integer)

        Dim letterBoForPreview As LetterBO = New LetterBO()
        letterBoForPreview.Title = title
        letterBoForPreview.Body = body
        letterBoForPreview.Code = code

        letterBoForPreview.StatusId = statusId
        letterBoForPreview.ActionId = actionId
        SessionManager.setLetterBoForPreview(letterBoForPreview)
        Dim strStdLetterId As String = Request.QueryString(PathConstants.LetterId)
        If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
            Me.stdLetterId = Int32.Parse(strStdLetterId)
        End If

        If (Me.stdLetterId = -1) Then
            Response.Redirect(PathConstants.PreviewLetter + "?src=CreateLetter", False)
        Else
            Response.Redirect(PathConstants.PreviewLetter + "?src=CreateLetter&" + PathConstants.LetterId + "=" + strStdLetterId, False)
        End If
    End Sub
#End Region
#End Region

End Class