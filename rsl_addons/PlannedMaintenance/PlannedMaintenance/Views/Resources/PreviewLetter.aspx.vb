﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PreviewLetter
    Inherits PageBase

    Dim stdLetterId As Integer

#Region "Events"
#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript", "Clickheretoprint()", True)
        Try

            Dim objLetterBL As LettersBL = New LettersBL()            
            Dim strStdLetterId As String = Request.QueryString(PathConstants.LetterId)
            Dim letterBoForPreview As LetterBO = New LetterBO()
            letterBoForPreview = SessionManager.getLetterBoForPreview()
            Dim strLetterBody As String = letterBoForPreview.Body
            Dim strLetterTitle As String = letterBoForPreview.Title

            If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
                Me.stdLetterId = Int32.Parse(strStdLetterId)

                If ((Not String.IsNullOrEmpty(strLetterBody)) And (Not IsNothing(strLetterBody)) And (Not String.IsNullOrEmpty(strLetterTitle)) And (Not IsNothing(strLetterBody))) Then
                    Me.lblLetterTitle.Text = strLetterTitle
                    Me.lblLetterBody.Text = strLetterBody
                Else
                    Me.loadLetter()
                End If

                Me.btnBack.PostBackUrl = String.Format("~/Views/Resources/" + Request.QueryString("src") + ".aspx?{0}={1}", PathConstants.LetterId, +strStdLetterId)
                Me.btnBack.Visible = True
            Else
                Me.lblLetterTitle.Text = strLetterTitle
                Me.lblLetterBody.Text = strLetterBody

                Me.btnBack.PostBackUrl = "~/Views/Resources/" + Request.QueryString("src") + ".aspx"
                Me.btnBack.Visible = True
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "load Letter"
    Protected Sub loadLetter()
        Dim resultSet As DataSet = New DataSet()
        Dim objLetterBl As LettersBL = New LettersBL()
        objLetterBl.getLetterById(Me.stdLetterId, resultSet)

        Me.lblLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")
        Me.lblLetterBody.Text = resultSet.Tables(0).Rows(0).Item("Body")

    End Sub
#End Region

#End Region
End Class