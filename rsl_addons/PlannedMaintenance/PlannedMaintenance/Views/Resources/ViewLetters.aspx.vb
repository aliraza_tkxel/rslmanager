﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class ViewLetters
    Inherits PageBase

#Region "Events"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Statusddl As DropDownList = New DropDownList()        

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblGridMessage, pnlGridMessage)
            If Not (IsPostBack) Then
                'This function 'll hight light the menu link button and it 'll also make the update panel visible
                'Me.highLightCurrentPageMenuItems()
                Statusddl = CType((pnlTemplateManagement.FindControl("ddlStatus")), DropDownList)
                Me.loadStatusDropDown(Statusddl)
                Me.getActions()
                Me.loadActionsDropDown(Statusddl)

                Me.getLetters(-1, -1, Nothing, Nothing)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                uiMessageHelper.setMessage(lblGridMessage, pnlGridMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppResources"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnLetters"))
    '        'Add update panel that should be visible
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelResources"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#Region "ddl Status Selected Index Changed"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            loadActionsDropDown(ddlStatus)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Search Click"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            Me.searchLetters()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Delete Click"
    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.deleteLetter(sender)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region
#Region "Functions"

#Region "Load All Letters"

    Protected Sub LoadAllLeters()

    End Sub

#End Region

#Region "load Status Drop Down"
    Protected Sub loadStatusDropDown(ByRef ddl As DropDownList)
        Dim codeId As String = "StatusId"
        Dim codeName As String = "Title"
        Dim resultDataSet As DataSet = New DataSet()
        'Create the object of status class/status.aspx to call the function 
        Dim statusBl As StatusActionBL = New StatusActionBL()
        statusBl.getAllStatuses(resultDataSet)

        ddl.DataSource = resultDataSet
        ddl.DataValueField = codeId
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "load Actions Drop Down"
    Protected Sub loadActionsDropDown(ByRef ddlStatus As DropDownList)
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        Dim statusSelected As Int32 = CType(ddlStatus.SelectedItem.Value, Int32)

        Dim actionId As String = "ActionId"
        Dim actionName As String = "Title"
        Dim resultDataSet As DataSet = New DataSet()
        objStatusActionBl.getActionsByStatusId(statusSelected, resultDataSet)

        Dim ActionsDDL As DropDownList = CType((pnlTemplateManagement.FindControl("ddlAction")), DropDownList)

        ActionsDDL.DataSource = resultDataSet
        ActionsDDL.DataValueField = actionId
        ActionsDDL.DataTextField = actionName
        ActionsDDL.DataBind()

        ActionsDDL.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ActionsDDL.SelectedValue = ApplicationConstants.DropDownDefaultValue

        pnlTemplateManagement.Update()
    End Sub
#End Region

#Region "get Actions"
    Protected Sub getActions()
        Dim resultset As DataSet = New DataSet()
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        objStatusActionBl.getAllActions(resultset)

        ViewState.Add(ViewStateConstants.ActionDataTable, resultset)
    End Sub
#End Region

#Region "search Letters"
    Protected Sub searchLetters()

        Dim statusSelected As Integer = ddlStatus.SelectedItem.Value
        Dim actionSelected As Integer = ddlAction.SelectedItem.Value

        Dim letterTitle As String = txtLetterTitle.Text
        Dim letterCode As String = txtLetterCode.Text

        If (letterTitle = String.Empty) Then
            letterTitle = Nothing
        End If

        If (letterCode = String.Empty) Then
            letterCode = Nothing
        End If

        Me.getLetters(statusSelected, actionSelected, letterTitle, letterCode)
    End Sub
#End Region

    'TODO: fetch the letters from cache
#Region "get Letters"
    Protected Sub getLetters(ByVal statusSelected As Integer, ByVal actionSelected As Integer, ByVal letterTitle As String, ByVal letterCode As String)        

        Dim objLetterBl As LettersBL = New LettersBL()
        Dim letterBO As LetterBO = New LetterBO(statusSelected, actionSelected, letterTitle, letterCode, Nothing, -1, -1)
        Dim resultSet As DataSet = New DataSet()

        objLetterBl.getLettersForSearch(letterBO, resultSet)

        If (resultSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblGridMessage, pnlGridMessage, UserMessageConstants.NoRecordFound, True)
            Me.grdViewLetters.Visible = False
        Else
            Me.grdViewLetters.Visible = True
            Me.grdViewLetters.DataSource = resultSet
            Me.grdViewLetters.DataBind()
        End If



    End Sub
#End Region

#Region "delete Letter"
    Protected Sub deleteLetter(ByVal sender As Object)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objLetterBl As LettersBL = New LettersBL()
        Dim lnkBtnDelete As LinkButton = CType(sender, LinkButton)

        Dim letterId As Integer = CType(lnkBtnDelete.CommandArgument, Integer)

        objLetterBL.deleteStandardLetter(letterId, resultDataSet)

        Me.searchLetters()

        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterDeleted, False)
    End Sub
#End Region

#End Region


End Class