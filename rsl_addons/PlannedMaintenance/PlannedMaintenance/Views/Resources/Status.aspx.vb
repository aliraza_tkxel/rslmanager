﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class Status
    Inherits PageBase

#Region "Attributes"
    Dim actionUpdate As Boolean = False
    Dim actionFlag As Boolean = False
#End Region

#Region "Events"
#Region "Pre Init"
    ''' <summary>
    ''' This function checks for user session
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        If Not IsPostBack Then
            'This function 'll hight light the menu link button and it 'll also make the update panel visible
                ' Me.highLightCurrentPageMenuItems()

            If Not Request.QueryString(PathConstants.StatusId) = "" And IsPostBack = False Then
                Dim statusId As Integer = CType(Request.QueryString(PathConstants.StatusId), Integer)
                SessionManager.setStatusIdForEdit(statusId)

                loadStatus(statusId)
            End If
        End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub    
#End Region

#Region "load Edit Status"

    Public Sub loadEditStatus() Handles Me.loadEditStatusEvent

    End Sub
#End Region

#Region "trVwStatus Tree Node Populate"
    Protected Sub trVwStatus_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwStatus.TreeNodePopulate
        Try
            If e.Node.ChildNodes.Count = 0 Then
                Select Case (e.Node.Depth)
                    Case 0
                        populateStatusNodes(e.Node)

                    Case 1
                        populateActionNodes(e.Node)
                End Select
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Status Nodes"
    Public Sub populateStatusNodes(ByVal node As TreeNode)
        Dim count As Integer = 1
        Dim resultDataSet As DataSet = New DataSet()
        Dim statusBl As StatusActionBL = New StatusActionBL()

        'get all the statuses  
        statusBl.getAllStatuses(resultDataSet)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ViewState(ViewStateConstants.StatusDataTable) = resultDataSet
            For Each row As DataRow In resultDataSet.Tables(0).Rows
                Dim newNode As TreeNode = New TreeNode()
                newNode.PopulateOnDemand = True
                newNode.Text = row("Title")
                newNode.Value = row("StatusId")
                newNode.Target = ApplicationConstants.StatusTargetForTreeNode
                node.ChildNodes.Add(newNode)
            Next
            node.ChildNodes.Add(New TreeNode(ApplicationConstants.DefaultStatusLabelForTreeNode, ApplicationConstants.DefaultStatusIdForTreeNode))
        End If
    End Sub
#End Region

#Region "Populate Action Nodes"
    Public Sub populateActionNodes(ByVal node As TreeNode)
        Dim actionDataSet As DataSet = New DataSet()
        Dim statusDataSet As DataSet = New DataSet()
        Dim statusBl As StatusActionBL = New StatusActionBL()

        Dim statusId As Integer = node.Value
        'get all the statuses  
        statusBl.getAllStatuses(statusDataSet)
        If statusDataSet.Tables(0).Rows.Count > 0 Then
            statusBl.getActionsByStatusId(statusId, actionDataSet)
            If actionDataSet.Tables(0).Rows.Count > 0 Then
                ViewState(ViewStateConstants.ActionDataTable) = actionDataSet
                For Each row As DataRow In actionDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("Title")
                    newNode.Value = row("ActionId")
                    newNode.Target = ApplicationConstants.ActionTargetForTreeNode                    
                    node.ChildNodes.Add(newNode)
                Next

            End If
            Dim addnode As TreeNode = New TreeNode()
            addnode.Value = ApplicationConstants.DefaultActionIdForTreeNode
            addnode.Text = ApplicationConstants.DefaultActionLabelForTreeNode
            node.ChildNodes.Add(addnode)
        End If

    End Sub
#End Region

#Region "Tree Load"
    Protected Sub tree_Load(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.Load        
        
    End Sub
#End Region

#Region "Tree Disposed"
    Protected Sub tree_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.Disposed

    End Sub
#End Region

#Region "Selected Node Changed"
    ''' <summary>
    ''' This event triggers when user clicks on some node. On the click selection the forms 'll be displayed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub trVwStatus_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.SelectedNodeChanged
        Try
            Me.displayAddUpdateForm(trVwStatus.SelectedNode.Value)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "TreeNode Collapsed"
    Protected Sub trVwStatus_TreeNodeCollapsed(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs)
        Try
            If actionFlag Then
                If e.Node.ChildNodes.Count > 0 Then
                    e.Node.ChildNodes.Clear()
                End If
                If Not actionUpdate Then
                    populateStatusNodes(e.Node)
                    For Each node As TreeNode In e.Node.ChildNodes
                        If Not node.Text = ApplicationConstants.DefaultStatusLabelForTreeNode Then
                            populateActionNodes(node)
                        End If
                    Next
                Else
                    populateActionNodes(e.Node)
                End If
                e.Node.Collapse()
                e.Node.Expand()
            Else
                e.Node.CollapseAll()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Reload TreeView"
    Public Sub reloadTreeView(ByVal node As TreeNode)
        Try
            node.Parent.Collapse()
            node.Parent.Expand()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region
#End Region
#Region "Functions"

    Private Event loadEditStatusEvent()

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))

    '        'Add link buttons that should be highlighted
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppResources"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnStatusAction"))

    '        'Add update panel that should be visible
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelResources"))

    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#Region "load Status Ranking"

    Public Sub loadStatusRanking(ByRef ddlRanking As DropDownList)
        Dim resultDataSet As DataSet = New DataSet()        
        Dim statusBl As StatusActionBL = New StatusActionBL()
        Try
            ddlRanking.Items.Clear()
            statusBl.getAllStatuses(resultDataSet)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next
            'adding one more last item
            ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load Status"
    ''' <summary>
    ''' This function loads the create status user control for edit/update mode. Status id 'll be used as reference s
    ''' </summary>
    ''' <param name="statusId"></param>
    ''' <remarks></remarks>
    Protected Sub loadStatus(ByVal statusId As Integer)
        ''get the reference of create status user control ( this control 'll be used for add/update status)
        Dim statusControl As Control = CType(pnlStatusControl.FindControl("sControl"), Control)
        ''get the reference of controls that resides inside create status user control
        Dim ddlStatusRanking As DropDownList = CType(statusControl.FindControl("ddlRanking"), DropDownList)        
        Dim resultDataTable As DataTable = New DataTable()
        Dim statusDataSet As DataSet = New DataSet()
        Dim statusBl As StatusActionBL = New StatusActionBL()
        statusBl.getAllStatuses(statusDataSet)
        resultDataTable = statusDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim query = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Int16)("StatusId") = Convert.ToInt16(statusId) _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = query.CopyToDataTable()

        Dim isEditable As Boolean = CType(IIf(IsDBNull(datatable.Rows(0).Item("IsEditable")) = True, True, datatable.Rows(0).Item("IsEditable")), String)
        Dim title As String = CType(IIf(IsDBNull(datatable.Rows(0).Item("Title")) = True, "", datatable.Rows(0).Item("Title")), String)
        Dim ranking As Integer = CType(IIf(IsDBNull(datatable.Rows(0).Item("Ranking")) = True, "", datatable.Rows(0).Item("Ranking")), String)

        Me.hideShowControlsAsPerSelection(statusId, title, ranking, isEditable, ApplicationConstants.StatusTargetForTreeNode)
    End Sub
#End Region

#Region "display Add Update Form"
    ''' <summary>
    ''' The main purpose of this funciton is to display the add/update form for action / status 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub displayAddUpdateForm(ByVal selectedNodeId As Int16)
        'get the status id from the selected node & set it in the session
        'selectedNodeId = Convert.ToInt16(trVwStatus.SelectedNode.Value)
        SessionManager.setStatusIdForEdit(selectedNodeId)

        Dim title As String = String.Empty
        Dim ranking As Integer = 0
        Dim isEditable As Boolean = False
        If selectedNodeId = ApplicationConstants.DefaultActionIdForTreeNode Then
            'user has clicked on Add New Action ---
            'Add New Action Here    
            Dim statusIdOfAction As Integer = trVwStatus.SelectedNode.Parent.Value
            SessionManager.setStatusIdOfAction(statusIdOfAction)
            aControl.loadActionRanking(statusIdOfAction, False)

        ElseIf selectedNodeId = ApplicationConstants.DefaultStatusIdForTreeNode Then
            ' Add Status
        Else
            ' Edit Status Here
            Dim resultDataTable As DataTable = New DataTable()
            If trVwStatus.SelectedNode.Target = ApplicationConstants.StatusTargetForTreeNode Then

                Dim statusDataSet As DataSet = New DataSet()
                Dim statusBl As StatusActionBL = New StatusActionBL()
                'get all the statuses  
                statusBl.getAllStatuses(statusDataSet)
                resultDataTable = statusDataSet.Tables(0)

                'query the data set using this will return the data row
                Dim query = (From dataRow In resultDataTable _
                    Where _
                        dataRow.Field(Of Int16)("StatusId") = selectedNodeId _
                    Select dataRow).ToList()

                'Query the data table
                Dim datatable As DataTable = query.CopyToDataTable()

                'set the popup with values of tenant 
                isEditable = CType(IIf(IsDBNull(datatable.Rows(0).Item("IsEditable")) = True, "", datatable.Rows(0).Item("IsEditable")), String)
                ranking = CType(IIf(IsDBNull(datatable.Rows(0).Item("Ranking")) = True, "", datatable.Rows(0).Item("Ranking")), Integer)
            End If
            If trVwStatus.SelectedNode.Target = ApplicationConstants.ActionTargetForTreeNode Then
                Dim selectedActionIdOnTree As Int16 = Convert.ToInt32(trVwStatus.SelectedNode.Value)
                Dim selectedStatusIdOnTree As Int16 = Convert.ToInt16(trVwStatus.SelectedNode.Parent.Value)
                SessionManager.setStatusIdForEditAction(selectedStatusIdOnTree)
                SessionManager.setActionIdForEditAction(selectedActionIdOnTree)
                aControl.loadActionRanking(selectedStatusIdOnTree, True)
                ViewState(ViewStateConstants.ActionId) = selectedActionIdOnTree

                'create data set and get all actions
                Dim actionDataSet As DataSet = New DataSet()
                Dim statusBl As StatusActionBL = New StatusActionBL()
                statusBl.getAllActions(actionDataSet)
                resultDataTable = actionDataSet.Tables(0)

                'query the data set using this will return the data row
                Dim query = (From dataRow In resultDataTable _
                    Where _
                        dataRow.Field(Of Int32)("ActionId") = selectedActionIdOnTree _
                    Select dataRow).ToList()

                'Query the data table
                Dim datatable As DataTable = query.CopyToDataTable()
                isEditable = CType(IIf(IsDBNull(datatable.Rows(0).Item("IsEditable")) = True, "", datatable.Rows(0).Item("IsEditable")), String)
                ranking = CType(IIf(IsDBNull(datatable.Rows(0).Item("Ranking")) = True, "", datatable.Rows(0).Item("Ranking")), Integer)
            End If
        End If
        'this function 'll hide and show the controls that are required to appear on forms
        Me.hideShowControlsAsPerSelection(selectedNodeId, trVwStatus.SelectedNode.Text, ranking, isEditable, trVwStatus.SelectedNode.Target, trVwStatus.SelectedNode.Parent.Text)

    End Sub
#End Region

#Region "Hide Show Controls As Per Selection"
    ''' <summary>
    ''' This function 'll hide show the controls when user 'll click on any node on tree
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub hideShowControlsAsPerSelection(ByVal selectedNodeId As Integer, ByVal title As String, ByVal ranking As Integer, ByVal isEditable As Boolean, Optional ByVal selectedTarget As String = "", Optional ByVal parentTitle As String = "")

        Dim statusControl As Control = CType(pnlStatusControl.FindControl("sControl"), Control)
        Dim actionControl As Control = CType(pnlStatusControl.FindControl("aControl"), Control)
        Dim pnlActionControl As Panel = CType(actionControl.FindControl("pnlActionControl"), Panel)
        pnlActionControl.Visible = True
        Dim txtBoxTitle As TextBox = CType(statusControl.FindControl("txtBoxTitle"), TextBox)
        'Status Controls        
        Dim ddlStatusRanking As DropDownList = CType(statusControl.FindControl("ddlRanking"), DropDownList)
        Dim btnStatusSave As Button = CType(statusControl.FindControl("btnSave"), Button)
        Dim btnStatusEdit As Button = CType(statusControl.FindControl("btnEdit"), Button)
        Dim lblNewStatus As Label = CType(statusControl.FindControl("lblNewStatus"), Label)
        Dim lblEditStatus As Label = CType(statusControl.FindControl("lblEditStatus"), Label)
        'Action Controls
        Dim ddlActionRanking As DropDownList = CType(actionControl.FindControl("ddlRanking"), DropDownList)
        Dim lblNewAction As Label = CType(actionControl.FindControl("lblNewAction"), Label)
        Dim lblEditAction As Label = CType(actionControl.FindControl("lblEditAction"), Label)
        Dim txtBoxActionTitle As TextBox = CType(actionControl.FindControl("txtBoxTitle"), TextBox)
        Dim btnSaveAction As Button = CType(actionControl.FindControl("btnSave"), Button)
        Dim btnEditAction As Button = CType(actionControl.FindControl("btnEdit"), Button)


        If selectedNodeId = ApplicationConstants.DefaultActionIdForTreeNode Then
            'if user has selected the "Add New Action"
            Dim lblStatusTitle As Label = CType(actionControl.FindControl("lblStatusTitle"), Label)
            lblStatusTitle.Text = parentTitle
            lblNewAction.Visible = True
            lblEditAction.Visible = False
            txtBoxActionTitle.ReadOnly = False
            txtBoxActionTitle.Text = String.Empty
            btnSaveAction.Visible = True
            btnEditAction.Visible = False
            statusControl.Visible = False
            actionControl.Visible = True
            actionUpdate = True
            actionFlag = True
        ElseIf selectedNodeId = ApplicationConstants.DefaultStatusIdForTreeNode Then
            ''if user has selected the "Add New Status"
            btnStatusEdit.Visible = False
            btnStatusSave.Visible = True
            txtBoxTitle.ReadOnly = False
            txtBoxTitle.Text = String.Empty            
            lblEditStatus.Visible = False
            lblNewStatus.Visible = True
            statusControl.Visible = True
            actionControl.Visible = False
            actionUpdate = False
            actionFlag = True
            Me.loadStatusRanking(ddlStatusRanking)            
        Else
            If selectedTarget = ApplicationConstants.StatusTargetForTreeNode Then
                'user has clicked on node to "Edit Status"
                txtBoxTitle.Text = title
                ' ddlStatusRanking.SelectedIndex = ranking - 1
                lblNewStatus.Visible = False
                lblEditStatus.Visible = True
                btnStatusSave.Visible = False
                btnStatusEdit.Visible = True
                If isEditable = False Then
                    txtBoxTitle.ReadOnly = True
                    btnStatusSave.Visible = False
                    btnStatusEdit.Visible = False
                Else
                    txtBoxTitle.ReadOnly = False
                End If
                statusControl.Visible = True
                actionControl.Visible = False
                Me.loadStatusRanking(ddlStatusRanking)
                ddlStatusRanking.SelectedIndex = ranking - 1
            End If

            If selectedTarget = ApplicationConstants.ActionTargetForTreeNode Then
                'user has clicked on node to "Edit Action"
                Dim lblStatusTitle As Label = CType(actionControl.FindControl("lblStatusTitle"), Label)
                lblStatusTitle.Text = parentTitle
                txtBoxActionTitle.Text = title
                If isEditable = False Then
                    txtBoxActionTitle.ReadOnly = True
                    btnSaveAction.Visible = False
                    btnEditAction.Visible = False
                Else
                    txtBoxActionTitle.ReadOnly = False
                    btnSaveAction.Visible = False
                    btnEditAction.Visible = True
                End If
                lblNewAction.Visible = False
                lblEditAction.Visible = True
                statusControl.Visible = False
                actionControl.Visible = True

                For Each i As ListItem In ddlActionRanking.Items
                    If i.Text = ranking.ToString() Then
                        i.Selected = True
                    End If
                Next
            End If

        End If

    End Sub
#End Region

#End Region

End Class