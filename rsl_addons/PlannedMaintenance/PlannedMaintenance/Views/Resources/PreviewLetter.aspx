﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PreviewLetter.aspx.vb"
    Inherits="PlannedMaintenance.PreviewLetter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<script type="text/javascript" type="javascript">
    function Clickheretoprint() {
        var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
        disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
        var content_vlue = document.getElementById("print_content").innerHTML;

        var docprint = window.open("", "", disp_setting);
        docprint.document.open();
        //  docprint.document.write('<html><head><link href="../../style/css/default.css" rel="stylesheet" type="text/css" /><title></title>');
        docprint.document.write('<html><head><title>Standard Letter Print Preview</title>');
        docprint.document.write('</head><body onLoad="self.print()" style="font-family:Arial;font-size:12pt;">');

        docprint.document.write(content_vlue);
        docprint.document.write('</body></html>');
        docprint.document.close();
        docprint.focus();
    }
    function closeWindow() {
        alert("hi");

        window.Close();

    }
</script>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server">
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <div>
        <asp:Button ID="btnBack" runat="server" Text="Back" Visible="false" BackColor="White" />
        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="Clickheretoprint();"
            BackColor="White" />
        <br />
        <br />
    </div>
    <div id="print_content">
        <asp:Label ID="Label1" runat="server">&lt;Tenancy Ref&gt;</asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server">&lt;DD/MM/YYYY&gt;</asp:Label>
        <br />
        <br />
        <br />
        <asp:Label ID="Label3" runat="server">&lt;Tenant Name&gt;</asp:Label>
        <br />
        <asp:Label ID="Label4" runat="server">&lt;House Number&gt; &lt;Address Line 1&gt;</asp:Label>
        <br />
        <asp:Label ID="Label5" runat="server">&lt;Address Line 2&gt;</asp:Label>
        <br />
        <asp:Label ID="Label6" runat="server">&lt;Town/City&gt;</asp:Label>
        <br />
        <asp:Label ID="Label7" runat="server">&lt;County&gt;</asp:Label>
        <br />
        <asp:Label ID="Label8" runat="server">&lt;Post Code&gt;</asp:Label>
        <br />
        <br />
        <br />
        Dear
        <asp:Label ID="Label9" runat="server">&lt;Tenant Name&gt;</asp:Label>
        <br />
        <br />
        <asp:Label ID="lblLetterTitle" runat="server"></asp:Label><br />
        <br />
        <asp:Label ID="lblLetterBody" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label12" runat="server">&lt;Sign Off&gt;</asp:Label>
        <br />
        <br />
        <asp:Label ID="Label13" runat="server">&lt;From Resource&gt;</asp:Label>
        <br />
        <asp:Label ID="Label14" runat="server">&lt;Team&gt;</asp:Label>
        <br />
        <asp:Label ID="Label10" runat="server">Direct Dial:</asp:Label>
        <br />
        <asp:Label ID="Label11" runat="server">Email:</asp:Label>
        <br />
    </div>
    </form>
</body>
</html>
