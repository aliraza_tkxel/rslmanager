﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="AssignedToContractor.aspx.vb" Inherits="PlannedMaintenance.AssignedToContractor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example is 5000

        function TypingInterval() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
        }
        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }

        function PrintJobSheet() {
            var str_win

            str_win = '/PlannedMaintenance/Views/Reports/PrintJobSheetSummary.aspx?JobSheetNumber=' + document.getElementById("lblJSN").innerHTML;
            window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
            return false;
        }
        function HidePanels() {
            var str_win

            str_win = document.getElementById("updPnlRepairDetail")
            str_win.hidden=true;
        }

    </script>
    <style type="text/css" media="all">
        .searchbox
        {
            background-position: right center;
            background-repeat: no-repeat;
            float: right;
            width: 200px;
        }
        
        .StatusDropdown
        {
            float: left !important;
        }
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet">
        <div class="header pnlHeading">
            <span class="header-label">Contractor Report</span>
            <div class="field right">
                <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                    <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                        class="searchbox styleselect-control searchbox searchText right" onkeyup="TypingInterval();" PlaceHolder="Quick find" runat="server">
                    </asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                        TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                </asp:Panel>
            </div>
        </div>
        <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <asp:UpdatePanel ID="updPnlContractorList" runat="server">
                <ContentTemplate>
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <cc2:PagingGridView ID="grdcontractorList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                            OnRowCreated="grdcontractorList_RowCreated" 
                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="PMO" ItemStyle-CssClass="dashboard" SortExpression="JOURNALID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPMO" runat="server" Text='<%# Bind("PMO") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reported" SortExpression="ReportedSort">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReported" runat="server" Text='<%# Bind("Reported") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Scheme" SortExpression="PJScheme">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("PJScheme") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Block" SortExpression="PJBlock">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("PJBlock") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location" SortExpression="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="160px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="250px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contractor" SortExpression="Subcontractor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSubcontractor" runat="server" Text='<%# Bind("Subcontractor") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Assigned" SortExpression="AssignedSort">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssigned" runat="server" Text='<%# Bind("Assigned") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By" SortExpression="UserInitials">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBy" runat="server" Text='<%# Bind("UserInitials") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Completion Due" SortExpression="CompletionDueSort">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCompletionDue" runat="server" Text='<%# Bind("CompletionDue") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="250px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewSubcontractorJobsheet" runat="server" CommandArgument='<%#Eval("JOURNALID")%>'
                                            Text="View" OnClick="grdContractorList_SelectedIndexChanged" UseSubmitBehavior="False"
                                            CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc2:PagingGridView>
                    </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="width:40%; float:left">
                                <div class="right">
                                    <span>
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                                    </span>
                                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                    <div class="field right" style="margin-right: 10px;">
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Pager Template End--%>
                    <%--<asp:UpdatePanel ID="updPnlJobSheetSummary" runat="server">
                <ContentTemplate>--%>
                <asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopup" Visible="false">
                    <div id="header-Popup">
                        <p id="header_label">
                            <b>Planned works</b>
                        </p>
                        <div id="header_box">
                        </div>
                    </div>
                    <div style="height: auto; clear: both;">
                        <table id="tbl_header" style="width: 98%; line-height: 20px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td colspan="3">
                                    <div style="text-align: right;">
                                        <asp:Button ID="btnPrevious" runat="server" Text="&lt; Previous" Width="100px" OnClick="btnPrevious_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnNext" runat="server" Text="Next >" Width="100px" OnClick="btnNext_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                                </td>
                                <td colspan="2" style="border-bottom: 1px solid #7F7F7F;">
                                    <b>Summary </b>
                                </td>
                                <td style="width: 30%; border-bottom: 1px solid #7F7F7F; text-align: right;">
                                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                                        Text="Page"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                                </td>
                            </tr>
                            <tr style='height: 10px;'>
                                <td style="width: 5%;">
                                </td>
                                <td colspan="3">
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                                    border-left: 1px solid #7F7F7F;">
                                </td>
                                <td style="width: 20%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                                    <b>PMO:</b>&nbsp;
                                    <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static"></asp:Label>
                                </td>
                                <td style="width: 40%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                                    <b>Planned works component:</b>&nbsp;
                                    <asp:Label ID="lblPlannedWorkComponent" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 30%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                                    <b>Trade:</b>&nbsp;
                                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                                    border-right: 1px solid #7F7F7F;">
                                </td>
                            </tr>
                        </table>
                        <table id="Table1" style="width: 100%; margin-top: 0px; line-height: 18px;">
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>PO Number:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b>Status:</b>&nbsp;
                                </td>
                                <td align="left" style="width: 35%;">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="StatusDropdown" Visible="False"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Contractor:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b>Budget:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Contact:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b>Estimate:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Works Required:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 230px;
                                        height: 40px;" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <b>NET Cost:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Ordered By:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b>VAT:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Ordered:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b>Total:</b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                </td>
                                <td style="width: 35%;">
                                </td>
                                <td style="width: 10%;">
                                    <b>Order Total:&nbsp;</b>
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr style="vertical-align: middle;">
                                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForScheme" runat="server" Text="Scheme:"></asp:Label>&nbsp;</b>
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForCustomer" runat="server" Text="Customer:"></asp:Label></b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForBlock" runat="server" Text="Block"></asp:Label>&nbsp;</b>
                                </td>
                                
                                <td style="width: 35%; vertical-align: top;">
                                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForTelephone" runat="server" Text="Telephone:"></asp:Label></b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForPostCode" runat="server" Text="Postcode:"></asp:Label>&nbsp;</b>
                                </td>
                               
                                <td style="width: 35%; vertical-align: top;">
                                    <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                                </td>
                      
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForMobile" runat="server" Text="Mobile:"></asp:Label></b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                 <td style="width: 10%;">
                                    <b><asp:Label ID="lblForHouseNumber" runat="server" Text="Address:"></asp:Label>&nbsp;</b>
                                </td>
                                <td rowspan="2" style="width: 35%; vertical-align: top;">
                                    <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                                </td>
                                <%--<td style="width: 35%;">
                                    <asp:Label ID="lblAddress2" runat="server" Text=""></asp:Label>
                                </td>--%>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForEmail" runat="server" Text="Email:"></asp:Label></b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                </td>
                                <td style="width: 35%;">
                                    <%--<asp:Label ID="lblAddress3" runat="server" Text=""></asp:Label>--%>
                                </td>
                                <td style="width: 10%;">
                                </td>
                                <td style="width: 35%;">
                                    <asp:Button ID="btn_CustDetailUpdate" runat="server" Text="Update customer details"
                                        Visible="false" />
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr style="vertical-align: middle;">
                                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td style="vertical-align: top;">
                                    <b>Asbestos:</b>
                                </td>
                                <td colspan="3">
                                    <div style="height: 100px; width: 400px; overflow: auto;">
                                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            BorderStyle="None" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="AsbRiskID" />
                                                <asp:BoundField DataField="Description">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td colspan="4">
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">
                                </td>
                                <td colspan="4">
                                    <div style="width: 100%; text-align: right; clear: both;">
                                        <div class="text_a_r_padd__r_20px">
                                            <asp:Button ID="btnBack" runat="server" OnClientClick="HidePanels()" Text=" &lt; Back" CssClass="margin_right20"
                                                Width="100px" />
                                            <asp:Button ID="btnUpdate" runat="server" CssClass="margin_right20" Text="Update"
                                                Width="100px" Enabled="false" />
                                            <input id="btnPrintJobSheetSummary" type="button" value="Print Job Sheet" class="margin_right20"
                                                onclick="PrintJobSheet()" />
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 5%;">
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="mdlPopupJobSheetSummarySubContractorUpdate" runat="server"
                    PopupControlID="pnlJobSheetSummary" TargetControlID="lblJSN" BackgroundCssClass="modalBackground"
                    CancelControlID="btnBack">
                </ajaxToolkit:ModalPopupExtender>
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                <%-- Completed Planned Panel--%>
                <asp:UpdatePanel ID="updPnlRepairDetail" ClientIDMode="Static" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlRepairDetail" runat="server" Visible="false">
                            <div class="repairDetailContainer">
                                <table style='border: 1px solid black; margin-left: 100px; height: auto; width: 350px;
                                    background-color: White;'>
                                    <tr>
                                        <td colspan="4" style="vertical-align: middle;">
                                            <asp:Panel ID="pnlCompleteMessage" runat="server" Visible="false" CssClass="message">
                                                <asp:Label ID="lblCompleteMessage" runat="server"></asp:Label>
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="vertical-align: middle;">
                                            <b>Complete </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 5%;'>
                                        </td>
                                        <td style='width: 30%;'>
                                            Completed:
                                        </td>
                                        <td style='width: 60%;'>
                                            <asp:TextBox ID="txtCompleted" runat="server" Style="float: left;"></asp:TextBox>
                                            <asp:Image ID="imgCalendar" runat="server" Height="23px" Width="25px" ImageUrl="~/Images/calendar.png"
                                                Style="float: left;" />
                                            <cc1:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtCompleted"
                                                Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtCompleted"
                                                WatermarkText="DD/MM/YYYY">
                                            </cc1:TextBoxWatermarkExtender>
                                        </td>
                                        <td style='width: 5%;'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 5%;'>
                                        </td>
                                        <td style='width: 30%;'>
                                            Time:
                                        </td>
                                        <td style='width: 30%;'>
                                            <asp:DropDownList ID="ddlHours" runat="server" Width="50px">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlMin" runat="server" Width="50px">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlFormate" runat="server" Width="50px">
                                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                                <asp:ListItem Value="PM">PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style='width: 5%;'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 5%;'>
                                        </td>
                                        <td valign="top" style='width: 30%;'>
                                            Works done:
                                        </td>
                                        <td style='width: 60%;'>
                                            <asp:TextBox runat="server" ID="txtBoxFollowOnNotes" TextMode="MultiLine" Width="97%"></asp:TextBox>
                                        </td>
                                        <td style='width: 5%;'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 5%;'>
                                        </td>
                                        <td style='width: 30%;'>
                                        </td>
                                        <td style='width: 60%;' align="right">
                                            <asp:Button runat="server" ID="btnCancel" Text="&lt; Back" Width="100px" CssClass="margin_right20" />
                                            <asp:Button runat="server" ID="btnCompleteAppSave" Text="Save" Width="100px" />
                                        </td>
                                        <td style='width: 5%;'>
                                        </td>
                                    </tr>
                                    <tr style="height: 18px;">
                                        <td colspan="4">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="mdlCompleteAppointment" runat="server" PopupControlID="pnlRepairDetail"
                            TargetControlID="txtCompleted" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
                        </ajaxToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
