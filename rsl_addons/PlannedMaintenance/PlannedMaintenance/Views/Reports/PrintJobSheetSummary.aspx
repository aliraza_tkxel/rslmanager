﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Blank.Master"
    CodeBehind="PrintJobSheetSummary.aspx.vb" Inherits="PlannedMaintenance.PrintJobSheetSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function Clickheretoprint() {
            self.print();

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopup">
        <div style="height: auto; overflow: auto; clear: both;">
            <table id="tbl_header" style="width: 100%; margin-top: 0px;">
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 90%;">
                        <h4>
                            Summary
                        </h4>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 90%; border-top: 1px solid #7F7F7F;">
                        <%--<hr style="border: 1px solid #7F7F7F;" />--%>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
            </table>
            <table id="tbl_Detail" style="width: 100%; margin-top: 5px; margin-bottom: 5px;">
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                        border-left: 1px solid #7F7F7F;">
                        PMO:&nbsp;
                        <asp:Label ID="lblJSN" runat="server"></asp:Label>
                    </td>
                    <td style="width: 35%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                        Planned works component:&nbsp;
                        <asp:Label ID="lblPlannedWorkComponent" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 30%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                        border-right: 1px solid #7F7F7F;">
                        Trade:&nbsp;
                        <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
            </table>
            <table id="Table1" style="width: 100%; margin-top: 0px;">
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        PO Number:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        Status:&nbsp;
                    </td>
                    <td align="left" style="width: 35%;">
                        <asp:DropDownList ID="ddlStatus" runat="server" Visible="False" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        Contractor:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        Budget:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        Contact:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        Estimate:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        Works Required:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 230px;
                            height: 40px;" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%;">
                        NET Cost:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        Ordered By:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        VAT:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        Ordered:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        Total:&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Order Total:&nbsp;</b>
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr style="height: 20px; margin-top: 10px; vertical-align: middle;">
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4" style="border-top: 1px solid #7F7F7F;">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForScheme" runat="server" Text="Scheme:"></asp:Label>&nbsp;</b>
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 10%;">
                                    <b><asp:Label ID="lblForCustomer" runat="server" Text="Customer:"></asp:Label></b>&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 5%;">
                                </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b><asp:Label ID="lblForBlock" runat="server" Text="Block"></asp:Label>&nbsp;</b>
                    </td>
                                
                    <td style="width: 35%; vertical-align: top;">
                        <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                    </td>
                     <td style="width: 10%;">
                        <b><asp:Label ID="lblForTelephone" runat="server" Text="Telephone:"></asp:Label></b>&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                     <td style="width: 10%;">
                        <b><asp:Label ID="lblForHouseNumber" runat="server" Text="Address:"></asp:Label>&nbsp;</b>
                    </td>
                    <td style="width: 35%; vertical-align: top;">
                        <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                    </td>
                    
                   <td style="width: 10%;">
                        <b><asp:Label ID="lblForMobile" runat="server" Text="Mobile:"></asp:Label></b>&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblAddress1" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b><asp:Label ID="lblForEmail" runat="server" Text="Email:"></asp:Label></b>&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblAddress2" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                        <%--<asp:Button ID="btn_UpdateCustDetail" runat="server" />--%>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                        <asp:Label ID="lblAddress3" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                        <%--<asp:Button ID="btn_UpdateCustDetail" runat="server" />--%>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4">
                        <br style="border-style: none; border-color: #FFFFFF">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4">
                        <b>Asbestos:</b>
                        <div style="height: 50px; overflow: auto;">
                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                BorderStyle="None" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="AsbRiskID" />
                                    <asp:BoundField DataField="Description">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
