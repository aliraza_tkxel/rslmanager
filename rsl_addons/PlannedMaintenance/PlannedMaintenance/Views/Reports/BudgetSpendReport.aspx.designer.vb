﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BudgetSpendReport
    
    '''<summary>
    '''pnlSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearch As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TextBoxWatermarkExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender1 As Global.AjaxControlToolkit.TextBoxWatermarkExtender
    
    '''<summary>
    '''ddlScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlScheme As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlReplacementYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReplacementYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''updPnlBudgetSpendReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPnlBudgetSpendReport As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''grdBudgetSpendReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdBudgetSpendReport As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''pnlPagination control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPagination As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lnkbtnPagerFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerFirst As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkbtnPagerPrev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerPrev As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lblPagerCurrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerCurrentPage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPagerTotalPages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerTotalPages As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPagerRecordStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordStart As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPagerRecordEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordEnd As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPagerRecordTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordTotal As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lnkbtnPagerNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerNext As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkbtnPagerLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerLast As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnExportToExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportToExcel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnGoPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGoPageNumber As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''rangevalidatorPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rangevalidatorPageNumber As Global.System.Web.UI.WebControls.RangeValidator
    
    '''<summary>
    '''txtPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageNumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnHidden3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHidden3 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''mdlPopupBudgetSpendReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlPopupBudgetSpendReport As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''pnlBudgetSpendReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBudgetSpendReport As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''imgBtnClosePopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnClosePopup As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''ucBudgetSpendReportDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucBudgetSpendReportDetail As Global.PlannedMaintenance.BudgetSpendReportDetail
End Class
