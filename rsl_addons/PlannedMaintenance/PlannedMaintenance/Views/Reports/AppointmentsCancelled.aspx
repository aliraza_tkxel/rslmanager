﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/MasterPage/Planned.Master"
 CodeBehind="AppointmentsCancelled.aspx.vb" Inherits="PlannedMaintenance.AppointmentsCancelled1" %>

<%@ Register TagName="AssignToContractor" TagPrefix="uc1" Src="~/Controls/Scheduling/AssignToContractor.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style>
        .leftDiv
        {
            width: 46%;
        }
        .searchbox
        {
            background-position: right center;
            background-repeat: no-repeat;
            float: right;
            width: 200px;
        }
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
    </style>
    <script type="text/javascript">

        function sendSMS(mobile, body, smsurl) {

            $.ajax({
                type: "GET",
                url: smsurl,
                data: "mobile=" + mobile + "&message=" + body + "",
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                },
                error: function (xhr, error) {
                },
            });
        }

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <div class="portlet">
            <div class="header pnlHeading">
                <span class="header-label">Cancelled Planned Works</span>
                <div class="field right">
                    <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                        <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                            class="searchbox styleselect-control searchbox searchText right" onkeyup="TypingInterval();" PlaceHolder="Quick find" runat="server">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                            TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </asp:Panel>
                </div>
            </div>
            <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
                    <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
                </asp:Panel>
                <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                    <cc2:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                        OnRowCreated="grdAppointmentToBeArranged_RowCreated" 
                        Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="25">
                        <Columns>
                            <asp:TemplateField HeaderText="PMO" ItemStyle-CssClass="dashboard" SortExpression="PMO">
                                <ItemTemplate>
                                    <asp:Label ID="lblPMO" runat="server" Text='<%# Bind("PMO") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle BorderStyle="None" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="40px"></ItemStyle>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="11%" />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Block:" SortExpression="PJBlock">
                                <ItemTemplate>
                                    <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("PJBlock") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="11%" />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                                <ItemTemplate>
                                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trade:" SortExpression="Trades">
                                <ItemTemplate>
                                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("Trades") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Due (Previous):" SortExpression="DueP">
                                <ItemTemplate>
                                    <asp:Label ID="lblDue" runat="server" Text='<%# Bind("DueP") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Due (New):" SortExpression="Due">
                                <ItemTemplate>
                                    <asp:Label ID="lblDueP" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason:" SortExpression="Reason">
                                <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField Visible ="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblJSNRef" runat="server" Text='<%# Bind("JSNRef") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnView" Text="View Details" runat="server" OnClick="btnViewClick" CommandArgument='<%#Eval("PMO")%>'
                                        visible ='<%# CheckPO(Eval("JSN")) %>'
                                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;"></asp:Button>
                                </ItemTemplate>
                                <ItemStyle Width="65px" />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                    </cc2:PagingGridView>
                </div>
                <%--Pager Template Start--%>
                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                        <div class="paging-left">
                            <span style="padding-right:10px;">
                                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                                    OnClick="lnkbtnPager_Click" >
                                    &lt;&lt;First
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                                    OnClick="lnkbtnPager_Click" >
                                    &lt;Prev
                                </asp:LinkButton>
                            </span>
                            <span style="padding-right:10px;">
                                <b>Page:</b>
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                of
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                            </span>
                            <span style="padding-right:20px;">
                                <b>Result:</b>
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                to
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                of
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </span>
                            <span style="padding-right:10px;">
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                                    OnClick="lnkbtnPager_Click" >
                                    
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                                    OnClick="lnkbtnPager_Click" >
                                        
                                </asp:LinkButton>
                            </span>
                        </div>
                        <div style="width:40%; float:left">
                            <div class="right">
                                <span>
                                    <asp:Button Text="Export to XLS" runat="server" ID="btnExportToExcel" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="btnExportToExcel_Click" />
                                </span>
                                <span>
                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important; margin-right:10px; min-width:0px;" OnClick="changePageNumber" />
                                </span>
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field right" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--Pager Template End--%>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>