﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.Validation


Public Class ConditionRating
    Inherits PageBase
    
#Region "Properties "

    'Public ReadOnly Property GetScheduleWorksUpdatePanel As UpdatePanel
    '    Get
    '        Return updPanelScheduleWorks
    '    End Get
    'End Property

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer = -1
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
                MainView.ActiveViewIndex = 0

                If lnkBtnConditionRatingToBeApprovedTab.Enabled Then
                    'div_lineAppointmentsToBeArranged.Visible = False
                    'div_lineAppointmentsArranged.Visible = True
                Else
                    'div_lineAppointmentsToBeArranged.Visible = True
                    'div_lineAppointmentsArranged.Visible = False
                End If

                If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksTab))) Then
                    activateConditionRatingToBeApproved()
                Else
                    If (Request.QueryString(PathConstants.ScheduleWorksTab).Equals(ApplicationConstants.ConditionRatingApproved)) Then
                        activateAppointmentsArranged()
                    Else
                        activateConditionRatingToBeApproved()
                    End If

                End If


            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try

            searchResults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region


#Region "Lnk Btn Condition Rating To Be Approved Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating To Be Approved Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnConditionRatingToBeApprovedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = False
        'div_lineAppointmentsArranged.Visible = True
        'div_lineConditionRatingRejected.Visible = True
        txtSearch.Text = ""
        activateConditionRatingToBeApproved()
    End Sub
#End Region

#Region "Lnk Btn Condition Rating Approved Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating Approved Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnConditionRatingApprovedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = True
        'div_lineAppointmentsArranged.Visible = False
        'div_lineConditionRatingRejected.Visible = True
        txtSearch.Text = ""
        activateAppointmentsArranged()

    End Sub
#End Region

#Region "Lnk Btn Condition Rating Rejected Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating Rejected Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnConditionRatingRejectedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = True
        'div_lineAppointmentsArranged.Visible = True
        'div_lineConditionRatingRejected.Visible = False
        txtSearch.Text = ""
        activateConditionRatingRejected()

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Activate Appointment to be Arranged"
    ''' <summary>
    ''' Activate Appointment to be Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateConditionRatingToBeApproved()
        lnkBtnConditionRatingToBeApprovedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnConditionRatingApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnConditionRatingRejectedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        populateConditionRatingToBeApprovedList()
    End Sub
#End Region

#Region "Activate Appointments Arranged"
    ''' <summary>
    ''' Activate Appointments Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentsArranged()
        lnkBtnConditionRatingApprovedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnConditionRatingToBeApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnConditionRatingRejectedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 1
        Me.populateConditionRatingApprovedList()
    End Sub
#End Region

#Region "Activate Condition Rating Rejected"
    ''' <summary>
    ''' Activate Condition Rating Rejected
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateConditionRatingRejected()
        lnkBtnConditionRatingRejectedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnConditionRatingApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnConditionRatingToBeApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 2
        Me.populateConditionRatingRejectedList()
    End Sub
#End Region


#Region "Populate Appointment To Be Arranged List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingToBeApprovedList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        ConditionRatingToBeApproved.populateConditionRatingToBeApprovedReport(resultDataSet, search, True, GetComponentId)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region

#Region "populate Condition Rating Approved List"
    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingApprovedList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        ConditionRatingApproved.populateConditionRatingApprovedReport(resultDataSet, search, True, GetComponentId)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region



#Region "populate Condition Rating Rejected List"
    ''' <summary>
    ''' populate Condition Rating Rejected List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingRejectedList()
        
        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        ConditionRatingRejected.populateConditionRatingRejectedReport(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region

#Region "Search Results"
    ''' <summary>
    ''' Search Results
    ''' </summary>
    ''' <remarks></remarks>
    Sub searchResults()

        If (MainView.ActiveViewIndex = 0) Then
            populateConditionRatingToBeApprovedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateConditionRatingApprovedList()
        Else
            populateConditionRatingRejectedList()
        End If

    End Sub

#End Region

#Region "Change Scheme"
    ''' <summary>
    ''' Change Scheme
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeScheme()

        'If (MainView.ActiveViewIndex = 0) Then
        '    populateConditionRatingToBeApprovedList()
        '    lblStartDate.Visible = False
        '    txtDate.Visible = False
        'Else
        '    populateAppointmentsArrangedList()
        '    lblStartDate.Visible = True
        '    txtDate.Visible = True
        'End If
    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region


#End Region

  
End Class