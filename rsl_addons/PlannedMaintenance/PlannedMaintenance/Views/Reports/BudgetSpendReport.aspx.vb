﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO

Public Class BudgetSpendReport
    Inherits PageBase

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not (IsPostBack) Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 20)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
                populateSchemeDropdownList()
                populateReplacementYearDropdownList()
                populateBudgetSpendReport()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "DDL Replacement Year Selected Index Changed"
    ''' <summary>
    ''' DDL Replacement Year Selected Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlReplacementYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReplacementYear.SelectedIndexChanged
        Try
            populateBudgetSpendReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "DDL Scheme Selected Index Changed"
    ''' <summary>
    ''' DDL Scheme Selected Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlScheme_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Try
            populateBudgetSpendReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try

            populateBudgetSpendReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region

#Region "Lnk Btn Property Count"
    ''' <summary>
    ''' Click on Property Count shows Due Tab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnPropertyCount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkBtnPropertyCount As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnPropertyCount.NamingContainer, GridViewRow)
            Dim hdnComponentID As HiddenField = DirectCast(row.FindControl("hdnComponentID"), HiddenField)
            Dim lblComponent As Label = DirectCast(row.FindControl("lblComponent"), Label)
            Dim componentId As Integer = Convert.ToInt32(hdnComponentID.Value)
            Dim status As String = lnkBtnPropertyCount.CommandArgument
            Dim dtPropertyList As DataTable = Me.getBudgetSpendReportDetailDatatable(componentId, status)
            ucBudgetSpendReportDetail.populateBudgetSpendReportDetail(dtPropertyList, lblComponent.Text, status, ddlReplacementYear.SelectedItem.Text)
            mdlPopupBudgetSpendReport.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Export to Excel"
    ''' <summary>
    ''' Button Export to Excel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            exportToExcel()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Previous" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            populateBudgetSpendReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                populateBudgetSpendReport()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Bind Row Data"
    ''' <summary>
    ''' Bind Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdBudgetSpendReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdBudgetSpendReport.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lnkBtnForecastCount As LinkButton = DirectCast(e.Row.FindControl("lnkBtnForecastCount"), LinkButton)
                Dim lnkBtnApprovedCount As LinkButton = DirectCast(e.Row.FindControl("lnkBtnApprovedCount"), LinkButton)
                Dim lnkBtnArrangedCount As LinkButton = DirectCast(e.Row.FindControl("lnkBtnArrangedCount"), LinkButton)
                Dim lnkBtnCompletedCount As LinkButton = DirectCast(e.Row.FindControl("lnkBtnCompletedCount"), LinkButton)

                lnkBtnForecastCount.Enabled = IIf(Convert.ToInt32(lnkBtnForecastCount.Text) > 0, True, False)
                lnkBtnApprovedCount.Enabled = IIf(Convert.ToInt32(lnkBtnApprovedCount.Text) > 0, True, False)
                lnkBtnArrangedCount.Enabled = IIf(Convert.ToInt32(lnkBtnArrangedCount.Text) > 0, True, False)
                lnkBtnCompletedCount.Enabled = IIf(Convert.ToInt32(lnkBtnCompletedCount.Text) > 0, True, False)

                Dim lblForecastCost As Label = DirectCast(e.Row.FindControl("lblForecastCost"), Label)
                Dim lblApprovedCost As Label = DirectCast(e.Row.FindControl("lblApprovedCost"), Label)
                Dim lblArrangedCost As Label = DirectCast(e.Row.FindControl("lblArrangedCost"), Label)
                Dim lblCompletedCost As Label = DirectCast(e.Row.FindControl("lblCompletedCost"), Label)
                Dim lblAnnualBudget As Label = DirectCast(e.Row.FindControl("lblAnnualBudget"), Label)
                Dim lblBudgetAvailable As Label = DirectCast(e.Row.FindControl("lblBudgetAvailable"), Label)

                lblForecastCost.Text = Double.Parse(lblForecastCost.Text).ToString("c")
                lblApprovedCost.Text = Double.Parse(lblApprovedCost.Text).ToString("c")
                lblArrangedCost.Text = Double.Parse(lblArrangedCost.Text).ToString("c")
                lblCompletedCost.Text = Double.Parse(lblCompletedCost.Text).ToString("c")
                lblAnnualBudget.Text = Double.Parse(lblAnnualBudget.Text).ToString("c")
                lblBudgetAvailable.Text = Double.Parse(lblBudgetAvailable.Text).ToString("c")

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Export to Excel"
    ''' <summary>
    ''' Export to Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportToExcel()

        Dim reportSw = New StringWriter()
        Dim fileName As String = "BudgetSpendReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)
        createBudgetSpendReportWorksheets(reportSw)        
        ExcelHelper.ToExcel(fileName, Page.Response, reportSw.ToString())

    End Sub

#End Region

#Region "Create Excel Budget Spend Report"
    ''' <summary>
    ''' Create Excel Budget Spend Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub createBudgetSpendReportWorksheets(ByRef reportSw As StringWriter)

        createBudgetSpendReportWorksheet(reportSw)
        createBudgetSpendReportDetailWorksheet(reportSw)

    End Sub

#End Region

#Region "Create budget spend report detail worksheet"
    ''' <summary>
    ''' Create budget spend report detail worksheet
    ''' </summary>
    ''' <remarks></remarks>
    Sub createBudgetSpendReportDetailWorksheet(ByRef reportSw As StringWriter)

        Dim budgetSpendExcelDs As DataSet = getBudgetSpendReportExcelDataset()
        Dim componentDt As DataTable = budgetSpendExcelDs.Tables(ApplicationConstants.ComponentDt)
        Dim dtForecast As DataTable = budgetSpendExcelDs.Tables(ApplicationConstants.ForecastDt)
        Dim dtApproved As DataTable = budgetSpendExcelDs.Tables(ApplicationConstants.ApprovedDt)
        Dim dtArranged As DataTable = budgetSpendExcelDs.Tables(ApplicationConstants.ArrangedDt)
        Dim dtCompleted As DataTable = budgetSpendExcelDs.Tables(ApplicationConstants.CompletedDt)

        Dim forecostStatus As String = "Forecast"
        Dim approvedStatus As String = "Approved"
        Dim arrangedStatus As String = "Arranged"
        Dim completedStatus As String = "Completed"

        For Each row As DataRow In componentDt.Rows
            createComponentStatusWorksheet(dtForecast, reportSw, forecostStatus, row)
            createComponentStatusWorksheet(dtApproved, reportSw, approvedStatus, row)
            createComponentStatusWorksheet(dtArranged, reportSw, arrangedStatus, row)
            createComponentStatusWorksheet(dtCompleted, reportSw, completedStatus, row)
        Next

    End Sub

#End Region

#Region "Create component status worksheet"

    Sub createComponentStatusWorksheet(ByVal statusDt As DataTable, ByRef reportSw As StringWriter, ByVal status As String, ByVal compRow As DataRow)

        Dim componentId As Integer = compRow.Item(ApplicationConstants.ComponentIdCol)
        Dim componentCost As Decimal = compRow.Item(ApplicationConstants.ComponentCostCol)
        Dim componentName As String = compRow.Item(ApplicationConstants.ComponentNameCol)
        Dim propertyAddress As String = String.Empty       

        Dim propertyList = (From res In statusDt.AsEnumerable() _
                         Where res(ApplicationConstants.ComponentIdCol) = (componentId) _
                         Select Address = res.Field(Of String)(ApplicationConstants.AddressCol)
                     ).ToList()

        Dim totalCost As Decimal = propertyList.Count * componentCost
        Dim sheetName As String = getSheetName(componentName, status)

        reportSw.Write(vbCr & vbLf & "<Worksheet ss:Name=""" & sheetName & """>")
        reportSw.Write(vbCr & vbLf & "<Table>")

        'Creating Header
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""200"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""70"" />")

        reportSw.Write(vbCr & vbLf & "<Row ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Component")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(componentName)))
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s68""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Year")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s68""><Data ss:Type=""Number"">{0}</Data></Cell>", replaceXmlChar(ddlReplacementYear.SelectedValue)))
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row  ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(status)))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s65""><Data ss:Type=""String"">{0}</Data></Cell>", totalCost.ToString("c")))
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s68""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Properties")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s68""><Data ss:Type=""Number"">{0}</Data></Cell>", propertyList.Count))
        reportSw.Write("</Row>")

        Dim rowCount As Integer = 0

        For Each item In propertyList

            reportSw.Write(vbCr & vbLf & "<Row ss:Height=""20.25"">")
            If (rowCount Mod 2 = 0) Then
                reportSw.Write(String.Format("<Cell ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(item)))
                reportSw.Write(String.Format("<Cell ss:StyleID=""s64""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(componentCost.ToString("c"))))
            Else
                reportSw.Write(String.Format("<Cell ss:StyleID=""s66""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(item)))
                reportSw.Write(String.Format("<Cell ss:StyleID=""s67""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(componentCost.ToString("c"))))
            End If

            reportSw.Write("</Row>")
            rowCount = rowCount + 1
        Next

        reportSw.Write(vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")

    End Sub

#End Region

#Region "Get Sheet Name"
    ''' <summary>
    ''' Get Sheet Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getSheetName(ByVal componentName As String, ByVal status As String)
        Dim sheetName As String = String.Empty
        Dim rgPattern = "[\\\/:\*\?""'<>|-]"
        Dim objRegEx As New Regex(rgPattern)
        componentName = objRegEx.Replace(componentName, "")

        If (componentName.Length > 20) Then

            Dim nameArr As String() = componentName.Split(" ")
            For Each nameChar In nameArr
                If (nameChar.Length > 0) Then
                    sheetName = sheetName + " " + nameChar.Substring(0, 3)
                End If
            Next

            sheetName = sheetName + " " + status
        Else
            sheetName = componentName + " " + status
        End If

        Return sheetName
    End Function

#End Region

#Region "Create budget spend report worksheet"
    ''' <summary>
    ''' Create budget spend report worksheet
    ''' </summary>
    ''' <remarks></remarks>
    Sub createBudgetSpendReportWorksheet(ByRef reportSw As StringWriter)

        Dim budgetSpendReportDs As DataSet = getBudgetSpendReportDataset(ApplicationConstants.ExportFullList)
        'Remove unnecessary columns from BudgetSpendReportDt
        budgetSpendReportDs.Tables(ApplicationConstants.BudgetSpendReportDt).Columns.Remove(ApplicationConstants.ComponentIdCol)
        budgetSpendReportDs.Tables(ApplicationConstants.BudgetSpendReportDt).Columns.Remove(ApplicationConstants.SOrderCol)
        budgetSpendReportDs.AcceptChanges()
        Dim budgetSpendReportDt As DataTable = budgetSpendReportDs.Tables(ApplicationConstants.BudgetSpendReportDt)

        reportSw.Write("<Worksheet ss:Name=""Budget Spend"">")
        reportSw.Write(vbCr & vbLf & "<Table>")

        'Creating Header
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""180"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""30"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""70"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""30"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""70"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""30"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""70"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""30"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""70"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""80"" />")
        reportSw.Write(vbCr & vbLf & "<Column ss:AutoFitWidth=""0"" ss:Width=""90"" />")

        reportSw.Write(vbCr & vbLf & "<Row  ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Year")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""Number"">{0}</Data></Cell>", replaceXmlChar(ddlReplacementYear.SelectedValue)))
        For value As Integer = 0 To budgetSpendReportDt.Columns.Count - 3
            reportSw.Write("<Cell><Data ss:Type=""String""></Data></Cell>")
        Next
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row ss:Height=""20.25"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Scheme")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ss:MergeAcross=""1"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(ddlScheme.SelectedItem.Text)))
        For value As Integer = 0 To budgetSpendReportDt.Columns.Count - 3
            reportSw.Write("<Cell><Data ss:Type=""String""></Data></Cell>")
        Next
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row  ss:Height=""20.25"">")
        For value As Integer = 0 To budgetSpendReportDt.Columns.Count - 1
            reportSw.Write("<Cell><Data ss:Type=""String""></Data></Cell>")
        Next
        reportSw.Write("</Row>")

        reportSw.Write(vbCr & vbLf & "<Row ss:AutoFitHeight=""0"" ss:Height=""30"">")
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62""><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Component")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ss:MergeAcross=""1"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Forecast")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ss:MergeAcross=""1"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Approved")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ss:MergeAcross=""1"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Arranged")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ss:MergeAcross=""1"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Completed")))       
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Annual Budget")))
        reportSw.Write(String.Format("<Cell ss:StyleID=""s62"" ><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar("Budget Available")))
        reportSw.Write("</Row>")

        For i As Integer = 0 To budgetSpendReportDt.Rows.Count - 1
            reportSw.Write(vbCr & vbLf & "<Row  ss:Height=""20.25"">")
            For Each dc As DataColumn In budgetSpendReportDt.Columns                
                reportSw.Write(getCell(dc.DataType, budgetSpendReportDt.Rows(i)(dc.ColumnName)))
            Next
            reportSw.Write("</Row>")
        Next

        reportSw.Write(vbCr & vbLf & "</Table>" & vbCr & vbLf & "</Worksheet>")

    End Sub

#End Region

#Region "Get Cell"
    ''' <summary>
    ''' Get Cell
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="cellData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getCell(type As Type, cellData As Object) As String
        Dim data = If((TypeOf cellData Is DBNull), "", cellData)
        If type.Name.Contains("Int") OrElse type.Name.Contains("Double") OrElse type.Name.Contains("Decimal") Then
            Return String.Format("<Cell><Data ss:Type=""Number"">{0}</Data></Cell>", data)
        End If
        If type.Name.Contains("Date") AndAlso data.ToString() <> String.Empty Then
            Return String.Format("<Cell ss:StyleID=""s63""><Data ss:Type=""DateTime"">{0}</Data></Cell>", Convert.ToDateTime(data).ToString("yyyy-MM-dd"))
        End If
        Return String.Format("<Cell><Data ss:Type=""String"">{0}</Data></Cell>", replaceXmlChar(data.ToString()))
    End Function
#End Region

#Region "Replace Xml Char"
    ''' <summary>
    ''' Replace Xml Char
    ''' </summary>
    ''' <param name="input"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function replaceXmlChar(input As String) As String
        input = input.Replace("&", "&amp")
        input = input.Replace("<", "&lt;")
        input = input.Replace(">", "&gt;")
        input = input.Replace("""", "&quot;")
        input = input.Replace("'", "&apos;")
        Return input
    End Function
#End Region

#Region "Populate status data table"
    ''' <summary>
    ''' Populate status data table
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="statusColumn"></param>
    ''' <remarks></remarks>
    Sub populatedStatusDatatable(ByRef dt As DataTable, ByVal statusColumn As String, ByVal componentId As Integer, ByVal componentCost As String)

        Dim propertyList = (From res In dt.AsEnumerable() _
                            Where res(ApplicationConstants.ComponentIdCol) = (componentId) And res(ApplicationConstants.StatusColumn) = 1 _
                            Select Address = res.Field(Of String)(ApplicationConstants.AddressCol), COST = componentCost
                        ).Distinct().ToList()

        For Each dataRow In propertyList
            Dim row As DataRow = dt.NewRow()
            row(ApplicationConstants.AddressCol) = dataRow.Address
            row(ApplicationConstants.ComponentCostCol) = componentCost
            dt.Rows.Add(row)
        Next

    End Sub

#End Region

#Region "Create Property List Data table"
    ''' <summary>
    ''' Create Property List Data table
    ''' </summary>
    ''' <param name="tableName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function createPropertyDataTable(ByVal tableName As String)

        Dim propertyTable As New DataTable(tableName)
        Dim addressColumn = New DataColumn(ApplicationConstants.AddressCol, GetType(String))
        propertyTable.Columns.Add(addressColumn)
        Dim costColumn As New DataColumn(ApplicationConstants.ComponentCostCol, GetType(String))
        propertyTable.Columns.Add(costColumn)
        Return propertyTable

    End Function

#End Region

#Region "Populate fiscal year dropdown"
    ''' <summary>
    ''' Populate fiscal year from table
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportBL As ReportsBL = New ReportsBL()

        Dim startYear As Integer = 1999
        Dim currentDate = DateTime.Now
        Dim currentYear = Date.Now.Year
        Dim currentFiscalYear As Integer = IIf(currentDate.CompareTo(New Date(currentYear, 4, 1)) < 0, currentYear - 1, currentYear)

        For year As Integer = startYear To currentYear + 15 Step 1
            Dim yearString = String.Format("{0} - {1}", year, year + 1)
            Dim yearItem As ListItem = New ListItem(yearString, year)
            yearItem.Selected = (year = currentFiscalYear)
            ddlReplacementYear.Items.Add(yearItem)
        Next year

        'Dim yearString As String = DateTime.Now.Year.ToString()
        'ddlReplacementYear.SelectedValue = yearString

        'objReportBL.getAllFiscalYears(resultDataSet)
        'ddlReplacementYear.DataSource = resultDataSet.Tables(0)
        'ddlReplacementYear.DataValueField = "YRange"
        'ddlReplacementYear.DataTextField = "FiscalYear"
        'ddlReplacementYear.DataBind()
    End Sub
#End Region

#Region "Populate scheme dropdown"
    ''' <summary>
    ''' Populate scheme dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSchemeDropdownList()
        Try

            Dim lstSelectAll As ListItem = New ListItem("All Schemes", "-1")
            Dim resultDataSet As DataSet = New DataSet()
            Dim objReportBL As ReportsBL = New ReportsBL()

            objReportBL.getAllSchemes(resultDataSet)
            ddlScheme.DataSource = resultDataSet.Tables(0)
            ddlScheme.DataValueField = "DevelopmentId"
            ddlScheme.DataTextField = "SchemeName"
            ddlScheme.DataBind()
            ddlScheme.Items.Insert(0, lstSelectAll)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Get Budget Spend Report Dataset"
    ''' <summary>
    ''' Populate Replacement List
    ''' </summary>
    ''' <remarks></remarks>
    Function getBudgetSpendReportDataset(ByVal isFullList As Boolean)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 20)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBL.getBudgetSpendReport(resultDataSet, objPageSortBo, ddlReplacementYear.SelectedValue, Convert.ToInt32(ddlScheme.SelectedValue), txtSearch.Text, isFullList)
        Return resultDataSet

    End Function

#End Region

#Region "Get Budget Spend Report Excel Dataset"
    ''' <summary>
    ''' Populate Replacement Excel List
    ''' </summary>
    ''' <remarks></remarks>
    Function getBudgetSpendReportExcelDataset()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        objReportsBL.getBudgetSpendReportExcelDetail(resultDataSet, ddlReplacementYear.SelectedValue, ddlScheme.SelectedValue, txtSearch.Text)
        Return resultDataSet

    End Function

#End Region

#Region "Populate Budget Spend Report"
    ''' <summary>
    ''' Populate Budget Spend Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateBudgetSpendReport()

        Dim totalCount As Integer = 0
        Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 20)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = Me.getBudgetSpendReportDataset(Not ApplicationConstants.ExportFullList)
        grdBudgetSpendReport.DataSource = resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).DefaultView
        grdBudgetSpendReport.DataBind()

        resultDataSet = Me.getBudgetSpendReportDataset(ApplicationConstants.ExportFullList)

        If Not grdBudgetSpendReport.FooterRow Is Nothing Then

            Dim lblForecastTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblForecastTotalCost"), Label)
            Dim lblApprovedTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblApprovedTotalCost"), Label)
            Dim lblArrangedTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblArrangedTotalCost"), Label)
            Dim lblCompletedTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblCompletedTotalCost"), Label)            
            Dim lblAnnualBudgetTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblAnnualBudgetTotalCost"), Label)
            Dim lblBudgetAvailableTotalCost As Label = CType(grdBudgetSpendReport.FooterRow.Cells(1).FindControl("lblBudgetAvailableTotalCost"), Label)

            lblForecastTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("FORECAST_COST"))).ToString("c")
            lblApprovedTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("APPROVED_COST"))).ToString("c")
            lblArrangedTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("ARRANGED_COST"))).ToString("c")
            lblCompletedTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("COMPLETED_COST"))).ToString("c")            
            lblAnnualBudgetTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("ANNUAL_BUDGET"))).ToString("c")
            lblBudgetAvailableTotalCost.Text = (Aggregate row As DataRow In resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).AsEnumerable() Into Sum(row.Field(Of Double)("BUDGET_AVAILABLE"))).ToString("c")

        End If

        If (resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDt).Rows.Count > 0) Then
            totalCount = resultDataSet.Tables(ApplicationConstants.BudgetSpendReportCountDt).Rows(0)(0)
            btnExportToExcel.Enabled = True
        Else
            btnExportToExcel.Enabled = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 20)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Get Budget Spend Report Dataset"
    ''' <summary>
    ''' Populate Replacement List
    ''' </summary>
    ''' <remarks></remarks>
    Function getBudgetSpendReportDetailDatatable(ByVal componentId As Integer, ByVal status As String)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        objReportsBL.getBudgetSpendReportDetail(resultDataSet, ddlReplacementYear.SelectedValue, status, Convert.ToInt32(ddlScheme.SelectedValue), componentId)
        Return resultDataSet.Tables(ApplicationConstants.BudgetSpendReportDetailDt)

    End Function

#End Region

#End Region

End Class