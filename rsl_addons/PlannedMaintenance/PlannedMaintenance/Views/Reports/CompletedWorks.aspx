﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="CompletedWorks.aspx.vb" Inherits="PlannedMaintenance.CompletedWorks" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }
        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }

        function ShowNavigation(sender, args) {
            sender._switchMode("years", true);
        }



    </script>
    <style>
        .dashboard, .dashboard td {
            padding: 0px !important; 
            font-size: 12px; 
        }
        .dashboard, .dashboard th {
             padding: 0px !important;
             font-size: 12px; 
        }
        .searchbox
        {
            border: 1px solid Gray;
            background-image:none; 
            padding:2px; 
            font-size:12px;
            margin : 5px 5px 5px 5px;
            border-radius : 0px;
            height:17px;
        }
        .list-select select
        {
            border-radius: 0px;
            border: 1px solid #b1b1b1;
            height: 25px !important;
            font-size: 12px !important;
            width: 205px !important;
            margin: -4px 10px 0 0;
        }
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="updPnlCompletedWorks" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header pnlHeading">
                    <span class="header-label">Job Sheet Status Report</span>
                    <asp:Panel ID="pnlSearch" runat="server" CssClass="right" Visible="True">
                        <div class="field right" style="width: auto;">
                            <asp:Button runat="server" ID="btnGO" UseSubmitBehavior="false" Text="GO" OnClientClick="searchTextChanged()"
                                CssClass="btn btn-xs btn-blue right" style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                            <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0; float:right" AutoCompleteType="Search" 
                                class="styleselect-control searchbox" PlaceHolder="Quick find" runat="server">
                            </asp:TextBox>
                            <div class="list-select right">
                                <div class="field" style="margin:0; width: auto;">
                                    <asp:DropDownList runat="server" ID="ddlScheme" Style="width: 200px;" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="list-select right">
                                <div class="field" style="margin:0; width: auto;">
                                    <asp:DropDownList runat="server" ID="ddlPlannedType" AutoPostBack="true" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0; width:100%">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <cc2:PagingGridView ID="grdCompletedWorks" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                            OnRowCreated="grdCompletedWorks_RowCreated" OnSorting="grdCompletedWorks_Sorting"
                            Width="100%" CssClass="dashboard webgrid table table-responsive" 
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="PMO" ItemStyle-CssClass="dashboard" SortExpression="Ref">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPMO" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" Width="7%" HorizontalAlign = "Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JSN" SortExpression="JSN" ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scheme" SortExpression="PJScheme">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("PJScheme") %>'></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Block" SortExpression="PJBlock">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("PJBlock") %>'></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" SortExpression="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Component" SortExpression="Component">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location:" SortExpression="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trade" SortExpression="Trades">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrades" runat="server" Text='<%# Bind("Trades") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appointment" SortExpression="Appointment">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Status" SortExpression="AppointmentStatus">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointmentStatus" runat="server" Text='<%# Bind("PlannedSubStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="6%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CompletedBy" SortExpression="CompletedBy">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCompletedBy" runat="server" Text='<%# Bind("CompletedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnArrow" runat="server" OnClick="lnkBtnArrow_click" CommandArgument='<%#Eval("Ref")%>'>
                                            <img src="../../Images/aero.png" alt="Appointment Summary" style="border:none;" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="7%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc2:PagingGridView>
                    </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="width:40%; float:left">
                                <div class="right">
                                    <span>
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                                    </span>
                                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                    <div class="field right" style="margin-right: 10px;">
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Pager Template End--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
