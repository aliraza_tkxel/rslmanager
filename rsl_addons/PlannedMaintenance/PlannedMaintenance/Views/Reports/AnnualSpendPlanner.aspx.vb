﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing

Public Class AnnualSpendPlanner
    Inherits PageBase

#Region "Attributes"
    Dim totalProperties As Integer = 0
    Dim totalCost As Double = 0
    Dim totalProperties1 As Integer = 0
    Dim totalCost1 As Double = 0
    Dim totalProperties2 As Integer = 0
    Dim totalCost2 As Double = 0
    Dim totalProperties3 As Integer = 0
    Dim totalCost3 As Double = 0
    Dim totalProperties4 As Integer = 0
    Dim totalCost4 As Double = 0
    Dim totalProperties5 As Integer = 0
    Dim totalCost5 As Double = 0
    Dim i As Integer = 1
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim obj As ListItem = New ListItem("Select All", "-1")
            Dim obj2 As ListItem = New ListItem("Please Select", "0")
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim resultDataSet As DataSet = New DataSet()
            Dim objReportBL As ReportsBL = New ReportsBL()
            If Not IsPostBack Then

                objReportBL.getAllSchemes(resultDataSet)
                ddlScheme.DataSource = resultDataSet.Tables(0)
                ddlScheme.DataValueField = "DevelopmentId"
                ddlScheme.DataTextField = "SchemeName"
                ddlScheme.DataBind()
                ddlScheme.Items.Insert(1, obj)
                ddlScheme.Items.Insert(0, obj2)
                btnEditParameters.Enabled = False
                txtBoxSearch.Enabled = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddlScheme Selected Index Changed"
    ''' <summary>
    ''' Select the scheme shows dataon report
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Try

            btnPrevious5YearsData.Enabled = False

            lnkBtnComponentReport.Enabled = False
            SessionManager.removeEditedComponentDataTable()
            SessionManager.removeUpdatedPropertyDueDatesDataTable()
            SessionManager.removeComponentYearSearchBO()
            SessionManager.removenext5YearsCount()
            SessionManager.removeSelectedRowIndex()
            SessionManager.removeInitialDataTable()
            Dim editedComponentDt As DataTable = generateEditedComponentDt()
            Dim updatePropertyDt As DataTable = generateUpdatedPropertyDt()
            SessionManager.setEditedComponentDataTable(editedComponentDt)
            SessionManager.setUpdatedPropertyDueDatesDataTable(updatePropertyDt)
            Dim resultDataset As DataSet = New DataSet()
            Dim objReportsBL As ReportsBL = New ReportsBL()
            Dim startYear As String = DateTime.Now.Year.ToString()
            Dim flag As Boolean = False
            Dim searchedText As String = txtBoxSearch.Text.Trim()
            Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
            Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
            lnkBtnComponentReport.CssClass = ApplicationConstants.TabClickedCssClass
            lnkBtnDueTab.CssClass = ApplicationConstants.TabInitialCssClass
            MainView.ActiveViewIndex = 0
            objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)
            SessionManager.setInitialDataTable(resultDataset.Tables(0))
            btnEditParameters.Enabled = True
            txtBoxSearch.Enabled = True
            If resultDataset.Tables(0).Rows.Count > 0 Then
                ViewState("CurrentDT") = resultDataset.Tables(0)
                grdVwCostComponent.Visible = True
                If CType(resultDataset.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
                    grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                Else
                    grdVwCostComponent.Columns(1).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                End If
                grdVwCostComponent.Columns(2).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
                grdVwCostComponent.Columns(3).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
                grdVwCostComponent.Columns(4).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
                grdVwCostComponent.Columns(5).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
                grdVwCostComponent.DataSource = resultDataset.Tables(0)
                grdVwCostComponent.DataBind()
                loadFooter()
                pnlExportToXls.Visible = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Edit Parameters"
    ''' <summary>
    ''' shows edit parameters pop up
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnEditParameters_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditParameters.Click
        Try
            uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)
            btnCancel.Enabled = True
            btnSave.Enabled = True
            SessionManager.removeSelectedRowIndex()
            Dim resultDataset As DataSet = New DataSet()
            Dim objReportsBL As ReportsBL = New ReportsBL()
            objReportsBL.getComponentData(resultDataset)
            If resultDataset.Tables(0).Rows.Count > 0 Then
                ddlComponent.DataSource = resultDataset
                ddlComponent.DataTextField = "COMPONENT"
                ddlComponent.DataValueField = "COMPONENTID"
                ddlComponent.DataBind()
                ddlCycle.DataSource = Enumerable.Range(1, 60)
                ddlCycle.DataBind()
                txtBoxMaterialCost.Text = resultDataset.Tables(0).Rows(0)("MATERIALCOST").ToString()
                txtBoxlabourCost.Text = resultDataset.Tables(0).Rows(0)("LABOURCOST").ToString()
                ddlCycle.SelectedValue = resultDataset.Tables(0).Rows(0)("Cycle").ToString()
                If resultDataset.Tables(0).Rows(0)("Frequency").ToString().Equals("Months") Then
                    ddlFrequency.SelectedValue = "1"
                Else
                    ddlFrequency.SelectedValue = "2"
                End If
                ViewState("ComponentDataset") = resultDataset
            End If
            Me.mdlPopUpEditParameters.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Save Click "
    ''' <summary>
    ''' Save edit parameters pop up
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click, btnSaveParameterData.Click
        Try
            Dim rowIndex As Integer = 0
            Dim objReportsBL As ReportsBL = New ReportsBL()
            Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
            Dim flag As Boolean = False
            If next5YearsCount > 0 Then
                flag = True
            End If

            Dim startYear As String = String.Empty
            If next5YearsCount > 0 Then
                Dim year As String = DateTime.Now.AddYears(-1).ToString()
                startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
            Else
                startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
            End If
            Dim searchedText As String = txtBoxSearch.Text.Trim()
            Dim editedParamdt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim objComponentBO As ComponentsBO = New ComponentsBO()
            Dim resultDataSet As DataSet = New DataSet()
            Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
            Dim componentID As String = ddlComponent.SelectedItem.Value
            Dim componentName As String = ddlComponent.SelectedItem.Text
            Dim expression As String = String.Empty
            Dim SelectedComponentRow As EditedComponentBO = SessionManager.getEditedComponentBO()


            Dim rowSid As Integer
            expression = "ComponentId =" + componentID
            Dim ds As DataSet = ViewState("ComponentDataset")
            Dim originalRow() As DataRow = ds.Tables(0).Select(expression)

            Dim resultRow() As DataRow = editedParamdt.Select(expression)
            If resultRow.Length > 0 Then
                'editedParamdt.
                For Each dtr As DataRow In editedParamdt.Rows
                    rowSid = dtr.Item("COMPONENTID")

                    If (rowSid = Convert.ToInt16(componentID)) Then
                        'dtr.Item("COMPONENTID") = Convert.ToInt16(ddlComponent.SelectedItem.Value)
                        dtr.Item("COMPONENTNAME") = Convert.ToString(ddlComponent.SelectedItem.Text)
                        dtr.Item("CYCLE") = Convert.ToInt32(ddlCycle.SelectedItem.Value)
                        dtr.Item("FREQUENCY") = Convert.ToString(ddlFrequency.SelectedItem.Text)
                        dtr.Item("MATERIALCOST") = Convert.ToDouble(txtBoxMaterialCost.Text)
                        dtr.Item("LABOURCOST") = Convert.ToDouble(txtBoxlabourCost.Text)
                        If (Not SelectedComponentRow.MaterialCost = (Convert.ToDouble(txtBoxMaterialCost.Text)) Or Not SelectedComponentRow.LabourCost = (Convert.ToDouble(txtBoxlabourCost.Text))) Then
                            dtr.Item("COSTFLAG") = True
                        Else
                            dtr.Item("COSTFLAG") = False
                        End If
                        If (Not SelectedComponentRow.Frequency.Equals(ddlFrequency.SelectedItem.Text) Or Not SelectedComponentRow.Cycle = Convert.ToInt32(ddlCycle.SelectedItem.Value)) Then
                            dtr.Item("CYCLEFLAG") = True
                        Else
                            dtr.Item("CYCLEFLAG") = False
                        End If

                        If Not Convert.ToInt32(originalRow(0)("Cycle")) = ddlCycle.SelectedItem.Value Or Not originalRow(0)("Frequency").Equals(ddlFrequency.SelectedItem.Text) Then
                            dtr.Item("CYCLEFLAG") = True
                        End If

                        Exit For
                    End If
                Next
            Else
                Dim row As DataRow = editedParamdt.NewRow()
                row("COMPONENTID") = CType(ddlComponent.SelectedItem.Value, Integer)
                row("COMPONENTNAME") = ddlComponent.SelectedItem.Text
                row("CYCLE") = CType(ddlCycle.SelectedItem.Value, Integer)
                row("FREQUENCY") = ddlFrequency.SelectedItem.Text
                row("MATERIALCOST") = CType(txtBoxMaterialCost.Text, Decimal)
                row("LABOURCOST") = CType(txtBoxlabourCost.Text, Decimal)
                If (Not SelectedComponentRow.MaterialCost = (Convert.ToDouble(txtBoxMaterialCost.Text)) Or Not SelectedComponentRow.LabourCost = (Convert.ToDouble(txtBoxlabourCost.Text))) Then
                    row("COSTFLAG") = True
                Else
                    row("COSTFLAG") = False
                End If
                If (Not SelectedComponentRow.Frequency.Equals(ddlFrequency.SelectedItem.Text) Or Not SelectedComponentRow.Cycle = Convert.ToInt32(ddlCycle.SelectedItem.Value)) Then
                    row("CYCLEFLAG") = True
                Else
                    row("CYCLEFLAG") = False
                End If


                If Not Convert.ToInt32(originalRow(0)("Cycle")) = ddlCycle.SelectedItem.Value Or Not originalRow(0)("Frequency").Equals(ddlFrequency.SelectedItem.Text) Then
                    row("CYCLEFLAG") = True
                End If
                editedParamdt.Rows.Add(row)
            End If

            'Compare with original values
            'CompareOriginalValues(componentID)

            editedParamdt.AcceptChanges()

            SessionManager.setEditedComponentDataTable(editedParamdt)


            'load Report here
            Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
            objReportsBL.getCostComponentData(resultDataSet, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

            'compareDatatable(SessionManager.getInitialDataTable(), resultDataSet.Tables(0))
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                'set row id in session
                Dim dr() As DataRow = resultDataSet.Tables(0).Select("ComponentId=" + componentID)
                rowIndex = resultDataSet.Tables(0).Rows.IndexOf(dr(0))
                SessionManager.setSelectedRowIndex(rowIndex)

                ' bind the grid
                ViewState("CurrentDT") = resultDataSet.Tables(0)
                grdVwCostComponent.Visible = True
                grdVwCostComponent.DataSource = resultDataSet.Tables(0)
                grdVwCostComponent.DataBind()
                If CType(resultDataSet.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
                    grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataSet.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                Else
                    grdVwCostComponent.Columns(1).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                End If
                grdVwCostComponent.Columns(2).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
                grdVwCostComponent.Columns(3).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
                grdVwCostComponent.Columns(4).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
                grdVwCostComponent.Columns(5).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
                loadFooter()

                pnlExportToXls.Visible = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
            End If
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SuccessfullySaved, False)
            mdlPopUpEditParameters.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddlComponent Selected Index Changed"
    ''' <summary>
    ''' Edit Parameter change component drop down value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlComponent_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlComponent.SelectedIndexChanged
        Try
            Dim selectedComponent As String = ddlComponent.SelectedItem.Value
            Dim ds As DataSet = ViewState("ComponentDataset")
            SessionManager.removeEditedComponentBO()
            Dim objEditedComponentBO As EditedComponentBO = New EditedComponentBO()
            Dim editedParamdt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim dt As DataTable = ds.Tables(0)
            Dim expression As String = String.Empty
            expression = "ComponentId =" + selectedComponent
            Dim resultRow() As DataRow
            resultRow = editedParamdt.Select(expression)
            If resultRow.Length > 0 Then
                ddlCycle.SelectedValue = resultRow(0)("Cycle").ToString()
                If resultRow(0)("Frequency").ToString().Equals("Months") Then
                    ddlFrequency.SelectedValue = "1"
                    'ddlFrequency.SelectedItem.Text = "Months"
                Else
                    ddlFrequency.SelectedValue = "2"
                    'ddlFrequency.SelectedItem.Text = "Years"
                End If
                txtBoxMaterialCost.Text = resultRow(0)("MaterialCost").ToString()
                txtBoxlabourCost.Text = resultRow(0)("LabourCost").ToString()


            Else
                resultRow = dt.Select(expression)
                ddlCycle.SelectedValue = resultRow(0)("Cycle").ToString()
                If resultRow(0)("Frequency").ToString().Equals("Months") Then
                    ddlFrequency.SelectedValue = "1"
                    'ddlFrequency.SelectedItem.Text = "Months"
                Else
                    ddlFrequency.SelectedValue = "2"
                    'ddlFrequency.SelectedItem.Text = "Years"
                End If
                txtBoxMaterialCost.Text = resultRow(0)("MaterialCost").ToString()
                txtBoxlabourCost.Text = resultRow(0)("LabourCost").ToString()

            End If
            objEditedComponentBO.ComponentId = CType(resultRow(0)(0).ToString(), Integer)
            objEditedComponentBO.ComponentName = resultRow(0)(1).ToString()
            objEditedComponentBO.Cycle = CType(resultRow(0)(2).ToString(), Integer)
            objEditedComponentBO.Frequency = resultRow(0)(3).ToString()
            objEditedComponentBO.MaterialCost = CType(resultRow(0)(4).ToString(), Double)
            objEditedComponentBO.LabourCost = CType(resultRow(0)(5).ToString(), Double)
            SessionManager.setEditedComponentBO(objEditedComponentBO)
            mdlPopUpEditParameters.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "txtBoxSearch Text Changed"
    ''' <summary>
    ''' Search text event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtBoxSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtBoxSearch.TextChanged
        Try
            Dim objReportsBL As ReportsBL = New ReportsBL()
            Dim resultDataset As DataSet = New DataSet()
            Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
            Dim flag As Boolean = False
            Dim startYear As String = String.Empty
            If next5YearsCount > 0 Then
                Dim year As String = DateTime.Now.AddYears(-1).ToString()
                startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
                flag = True
            Else
                startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
            End If
            
            Dim searchedText As String = txtBoxSearch.Text.Trim()
            Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)

            SessionManager.removeSelectedRowIndex()
            updateInitialDataTable(flag, startYear, selectedScheme, searchedText)

            Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
            objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

            If resultDataset.Tables(0).Rows.Count > 0 Then
                ViewState("CurrentDT") = resultDataset.Tables(0)
                grdVwCostComponent.Visible = True
                grdVwCostComponent.DataSource = resultDataset.Tables(0)
                grdVwCostComponent.DataBind()
                loadFooter()
                pnlExportToXls.Visible = True
            Else
                grdVwCostComponent.Visible = False
                grdVwCostComponent.Columns(1).HeaderText = "<2014 " + "Total Cost"
                grdVwCostComponent.Columns(2).HeaderText = "2014 " + "Total Cost"
                grdVwCostComponent.Columns(3).HeaderText = "2015 " + "Total Cost"
                grdVwCostComponent.Columns(4).HeaderText = "2016 " + "Total Cost"
                grdVwCostComponent.Columns(5).HeaderText = "2017 " + "Total Cost"
                pnlExportToXls.Visible = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grdVwCostComponent_RowDataBound"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdVwCostComponent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdVwCostComponent.RowDataBound
        ' Get the footer row.
        'Dim footerRow As GridViewRow = grdVwCostComponent.FooterRow
        'e.Row.RowType


        Dim CurrentDT As DataTable = ViewState("CurrentDT")
        Dim oldDataTable As DataTable = SessionManager.getInitialDataTable()
        Dim selectedIndex As Integer = SessionManager.getSelectedRowIndex()
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim index As Integer = e.Row.RowIndex
            Dim dr As DataRow = oldDataTable.Rows(index)
            If CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount"), Integer) <= 0 Then
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost"), Label)
                lblCost.Text += ".00"
                lnkBtnProprtyCnt.Enabled = False
                lnkBtnProprtyCnt.ForeColor = Color.Blue

            Else
                Dim hdnIsedited As HiddenField = CType(e.Row.FindControl("hdnIsEdited"), HiddenField)
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost"), Label)
                If Not CurrentDT Is Nothing Then
                    If CurrentDT.Rows.Count > 0 Then
                        Dim oldDr As DataRow = CurrentDT.Rows(index)
                        If Not dr(4).Equals(oldDr(4)) Or Not dr(5).Equals(oldDr(5)) Then
                            lblCost.ForeColor = Color.Red
                            ViewState("footer") = 1

                        End If
                    End If
                End If
                'If e.Row.RowIndex = selectedIndex Then
                '    lblCost.ForeColor = Color.Red
                '    ViewState("footer") = 1
                'End If
            End If
            If CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount1"), Integer) <= 0 Then
                Dim lnkBtnProprtyCnt1 As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount1"), LinkButton)
                Dim lblCost1 As Label = CType(e.Row.FindControl("lblCost1"), Label)
                lblCost1.Text += ".00"
                lnkBtnProprtyCnt1.Enabled = False
                lnkBtnProprtyCnt1.ForeColor = Color.Blue
            Else
                Dim hdnIsedited As HiddenField = CType(e.Row.FindControl("hdnIsEdited1"), HiddenField)
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount1"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost1"), Label)
                If Not CurrentDT Is Nothing Then
                    If CurrentDT.Rows.Count > 0 Then
                        Dim oldDr As DataRow = CurrentDT.Rows(index)
                        If Not dr(8).Equals(oldDr(8)) Or Not dr(9).Equals(oldDr(9)) Then
                            grdVwCostComponent.FooterRow.Cells(2).ForeColor = Color.Red
                            lblCost.ForeColor = Color.Red
                            ViewState("footer1") = 1

                        End If
                    End If
                End If
                'If e.Row.RowIndex = selectedIndex Then
                '    lblCost.ForeColor = Color.Red
                '    ViewState("footer1") = 1

                'End If
            End If
            If CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount2"), Integer) <= 0 Then
                Dim lnkBtnProprtyCnt2 As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount2"), LinkButton)
                Dim lblCost2 As Label = CType(e.Row.FindControl("lblCost2"), Label)
                lblCost2.Text += ".00"
                lnkBtnProprtyCnt2.Enabled = False
                lnkBtnProprtyCnt2.ForeColor = Color.Blue
            Else
                Dim hdnIsedited As HiddenField = CType(e.Row.FindControl("hdnIsEdited2"), HiddenField)
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount2"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost2"), Label)
                If Not CurrentDT Is Nothing Then
                    If CurrentDT.Rows.Count > 0 Then
                        Dim oldDr As DataRow = CurrentDT.Rows(index)
                        If Not dr(13).Equals(oldDr(13)) Or Not dr(14).Equals(oldDr(14)) Then
                            grdVwCostComponent.FooterRow.Cells(3).ForeColor = Color.Red
                            lblCost.ForeColor = Color.Red
                            ViewState("footer2") = 1

                        End If
                    End If
                End If
                'If e.Row.RowIndex = selectedIndex Then
                '    lblCost.ForeColor = Color.Red
                '    ViewState("footer2") = 1
                'End If
            End If
            If CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount3"), Integer) <= 0 Then
                Dim lnkBtnProprtyCnt3 As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount3"), LinkButton)
                Dim lblCost3 As Label = CType(e.Row.FindControl("lblCost3"), Label)
                lblCost3.Text += ".00"
                lnkBtnProprtyCnt3.Enabled = False
                lnkBtnProprtyCnt3.ForeColor = Color.Blue
            Else
                Dim hdnIsedited As HiddenField = CType(e.Row.FindControl("hdnIsEdited3"), HiddenField)
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount3"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost3"), Label)
                If Not CurrentDT Is Nothing Then
                    If CurrentDT.Rows.Count > 0 Then
                        Dim oldDr As DataRow = CurrentDT.Rows(index)
                        If Not dr(18).Equals(oldDr(18)) Or Not dr(19).Equals(oldDr(19)) Then
                            grdVwCostComponent.FooterRow.Cells(4).ForeColor = Color.Red
                            lblCost.ForeColor = Color.Red
                            ViewState("footer3") = 1

                        End If
                    End If
                End If
                'If e.Row.RowIndex = selectedIndex Then
                '    lblCost.ForeColor = Color.Red
                '    ViewState("footer3") = 1
                'End If
            End If
            If CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount4"), Integer) <= 0 Then
                Dim lnkBtnProprtyCnt4 As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount4"), LinkButton)
                Dim lblCost4 As Label = CType(e.Row.FindControl("lblCost4"), Label)
                lblCost4.Text += ".00"
                lnkBtnProprtyCnt4.Enabled = False
                lnkBtnProprtyCnt4.ForeColor = Color.Blue
            Else
                Dim hdnIsedited As HiddenField = CType(e.Row.FindControl("hdnIsEdited4"), HiddenField)
                Dim lnkBtnProprtyCnt As LinkButton = CType(e.Row.FindControl("lnkBtnPropertyCount4"), LinkButton)
                Dim lblCost As Label = CType(e.Row.FindControl("lblCost4"), Label)
                If Not CurrentDT Is Nothing Then
                    If CurrentDT.Rows.Count > 0 Then
                        Dim oldDr As DataRow = CurrentDT.Rows(index)
                        If Not dr(23).Equals(oldDr(23)) Or Not dr(24).Equals(oldDr(24)) Then
                            grdVwCostComponent.FooterRow.Cells(5).ForeColor = Color.Red
                            lblCost.ForeColor = Color.Red
                            ViewState("footer4") = 1

                        End If
                    End If
                End If
                'If e.Row.RowIndex = selectedIndex Then
                '    lblCost.ForeColor = Color.Red
                '    ViewState("footer4") = 1
                'End If
            End If
            totalProperties += CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount"), Integer)
            totalCost += CType(DataBinder.Eval(e.Row.DataItem, "Cost"), Double)
            totalProperties1 += CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount1"), Integer)
            totalCost1 += CType(DataBinder.Eval(e.Row.DataItem, "Cost1"), Double)
            totalProperties2 += CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount2"), Integer)
            totalCost2 += CType(DataBinder.Eval(e.Row.DataItem, "Cost2"), Double)
            totalProperties3 += CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount3"), Integer)
            totalCost3 += CType(DataBinder.Eval(e.Row.DataItem, "Cost3"), Double)
            totalProperties4 += CType(DataBinder.Eval(e.Row.DataItem, "PropertyCount4"), Integer)
            totalCost4 += CType(DataBinder.Eval(e.Row.DataItem, "Cost4"), Double)


        End If
        ViewState("TotalProperties") = totalProperties
        ViewState("TotalProperties1") = totalProperties1
        ViewState("TotalProperties2") = totalProperties2
        ViewState("TotalProperties3") = totalProperties3
        ViewState("TotalProperties4") = totalProperties4

        ViewState("TotalCost") = totalCost
        ViewState("TotalCost1") = totalCost1
        ViewState("TotalCost2") = totalCost2
        ViewState("TotalCost3") = totalCost3
        ViewState("TotalCost4") = totalCost4

    End Sub
#End Region

#Region "Export to XLS"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click

        Try

            ExportGridToExcel(getFullComponentReportGridView())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region

#Region "btn Previous 5Years Data"
    ''' <summary>
    ''' On button click shows next five years data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrevious5YearsData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious5YearsData.Click
        'Try

        'Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
        SessionManager.removeInitialDataTable()

        next5YearsCount = ((next5YearsCount) - 1)
        SessionManager.setnext5YearsCount(next5YearsCount)
        Dim flag As Boolean = True
        Dim searchedText As String = txtBoxSearch.Text.Trim()
        Dim startYear As String = String.Empty
        If next5YearsCount = 0 Then
            btnPrevious5YearsData.Enabled = False
            flag = False
            startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
        Else
            Dim year As String = DateTime.Now.AddYears(-1).ToString()
            startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
        End If

        getCostComponentsReport(flag, startYear, searchedText, resultDataset)

        '    Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        '    updateInitialDataTable(flag, startYear, selectedScheme, searchedText)

        '    Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
        '    Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
        '    objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

        '    ViewState("CurrentDT") = resultDataset.Tables(0)
        '    If resultDataset.Tables(0).Rows.Count > 0 Then

        '        If CType(resultDataset.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
        '            grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
        '        Else
        '            grdVwCostComponent.Columns(1).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
        '        End If
        '        grdVwCostComponent.Columns(2).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(3).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(4).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(5).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
        '        grdVwCostComponent.DataSource = resultDataset.Tables(0)
        '        grdVwCostComponent.DataBind()
        '        loadFooter()
        '        pnlExportToXls.Visible = True
        '    Else
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
        '    End If
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.message = ex.Message

        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If

        'Finally
        '    If uiMessageHelper.IsError = True Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
        '    End If

        'End Try
    End Sub
#End Region

#Region "btn Next 5Years Data"
    ''' <summary>
    ''' On button click shows next five years data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext5YearsData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext5YearsData.Click
        'Try
        'Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
        SessionManager.removeInitialDataTable()

        next5YearsCount += 1
        btnPrevious5YearsData.Enabled = True
        SessionManager.setnext5YearsCount(next5YearsCount)
        Dim flag As Boolean = True
        Dim searchedText As String = txtBoxSearch.Text.Trim()
        Dim startYear As String = String.Empty
        If next5YearsCount > 0 Then
            Dim year As String = DateTime.Now.AddYears(-1).ToString()
            startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
        Else
            startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
        End If

        getCostComponentsReport(flag, startYear, searchedText, resultDataset)

        '    Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        '    updateInitialDataTable(flag, startYear, selectedScheme, searchedText)

        '    Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
        '    Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
        '    objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

        '    ViewState("CurrentDT") = resultDataset.Tables(0)
        '    If resultDataset.Tables(0).Rows.Count > 0 Then

        '        If CType(resultDataset.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
        '            grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
        '        Else
        '            grdVwCostComponent.Columns(1).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
        '        End If
        '        grdVwCostComponent.Columns(2).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(3).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(4).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
        '        grdVwCostComponent.Columns(5).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
        '        grdVwCostComponent.DataSource = resultDataset.Tables(0)
        '        grdVwCostComponent.DataBind()
        '        loadFooter()
        '        pnlExportToXls.Visible = True
        '    Else
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
        '    End If
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.message = ex.Message

        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If

        'Finally
        '    If uiMessageHelper.IsError = True Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
        '    End If

        'End Try
    End Sub
#End Region

#Region "Lnk Btn Property Count"
    ''' <summary>
    ''' Click on Property Count shows Due Tab
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnPropertyCount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkBtnPropertyCount As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnPropertyCount.NamingContainer, GridViewRow)
            Dim rowView As DataRowView = CType(row.DataItem, DataRowView)
            Dim rowIndex As Integer = row.RowIndex
            ViewState("rowIndex") = rowIndex
            'For i As Integer = 0 To row.Cells.Count
            '    dr = row.Cells(i).Text
            '    i = i + 1
            'Next
            'SessionManager.setDataRow(row)
            Dim hdnFieldValue As HiddenField = DirectCast(row.FindControl("hdnComponentID"), HiddenField)
            Dim lblComponent As Label = DirectCast(row.FindControl("lblComponent"), Label)
            Dim commandArgument As String = CType(lnkBtnPropertyCount.CommandArgument, String)
            Dim commandArg = commandArgument.Split(";")

            Dim objComponentYearsBO As ComponentYearSearchBO = New ComponentYearSearchBO()
            lnkBtnComponentReport.CssClass = ApplicationConstants.TabInitialCssClass

            lnkBtnDueTab.CssClass = ApplicationConstants.TabClickedCssClass
            MainView.ActiveViewIndex = 1
            If commandArg(1).Equals("-1") Then
                lblHeaderYear.Text = "<" + commandArg(0)
            Else
                lblHeaderYear.Text = commandArg(0)
            End If
            lnkBtnDueTab.Visible = True
            btnSaveChanges.Enabled = True
            txtBoxSearch.Enabled = False
            ddlScheme.Enabled = False
            btnEditParameters.Enabled = False
            objComponentYearsBO.ComponentId = CType(hdnFieldValue.Value, Integer)
            objComponentYearsBO.ComponentName = lblComponent.Text
            objComponentYearsBO.DueYear = commandArg(0)
            objComponentYearsBO.SchemeId = CType(ddlScheme.SelectedItem.Value, Integer)
            objComponentYearsBO.SearchText = String.Empty
            objComponentYearsBO.YearOperator = commandArg(1)
            SessionManager.setComponentYearSearchBO(objComponentYearsBO)
            Dim isReload As Boolean = True
            AnnualComponentPropertyList.populateComponentsAnnualPropertyList(isReload)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnCloseTab Click"
    ''' <summary>
    ''' Close Due tab on cross button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCloseTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim isDataChange As Boolean = AnnualComponentPropertyList.isDataChanged()
            Dim row As GridViewRow = Nothing ' SessionManager.getdr()

            'Dim lnkBtnProCount1 As LinkButton = DirectCast(row.FindControl("lnkBtnPropertyCount"), LinkButton)
            'Dim propertyCount1 As Integer = CType(lnkBtnProCount1.Text, Integer)
            If isDataChange = True Then
                btnSaveChanges.Visible = True
                btnSaveChanges.Enabled = True
                mdlPopUpConfirmCloseTab.Show()
            Else
                activateComponentCostTab()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Cancel Parameter Pop"
    ''' <summary>
    ''' On cancel Edit Parameter popup
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            If (isComponentDataChanged()) Then
                btnCancel.Enabled = False
                btnSave.Enabled = False
                mdlPopUpEditParameters.Show()
                mdlPopUpConfirm.Show()
            Else
                LoadReport()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Load Complete"
    ' ''' <summary>
    ' ''' Load Complete Event
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    ''Private Sub AnnualSpendPlanner_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
    ''    'loadFooter()

    ''End Sub
#End Region

#Region "Btn Save Changes Click"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveChanges_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveChanges.Click
        Try
            If AnnualComponentPropertyList.isDataChanged() Then
                If AnnualComponentPropertyList.saveChanges() Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SuccessfullySaved, False)
                    Dim rowIndex As Integer = ViewState("rowIndex")
                    highlightRow(rowIndex)
                    activateComponentCostTab()
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FailedSave, True)
                End If
            Else
                activateComponentCostTab()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            updPnlComponentCost.Update()
        End Try
    End Sub
#End Region

#Region "btn Save Confirm Close PopUp Click"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveConfirmClosePopUp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveConfirmClosePopUp.Click
        Try
            If AnnualComponentPropertyList.saveChanges() Then
                MainView.ActiveViewIndex = 0
                lnkBtnDueTab.Visible = False
                btnEditParameters.Visible = True
                btnSaveChanges.Visible = False
                ddlScheme.Enabled = True
                LoadReport()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SuccessfullySaved, False)
            Else
                MainView.ActiveViewIndex = 1
                lnkBtnDueTab.Visible = True
                btnEditParameters.Visible = False
                btnSaveChanges.Visible = True
                ddlScheme.Enabled = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Continue Close PopUp Click"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BtnContinueClosePopUp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnContinueClosePopUp.Click
        Try
            lnkBtnDueTab.Visible = False
            MainView.ActiveViewIndex = 0
            btnEditParameters.Visible = True
            btnSaveChanges.Visible = False
            btnEditParameters.Enabled = True
            txtBoxSearch.Enabled = True
            ddlScheme.Enabled = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region



#End Region

#Region "Functions"

#Region "Is component information changed"
    ''' <summary>
    ''' Is component information changed
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function isComponentDataChanged()

        Dim selectedComponent As String = ddlComponent.SelectedItem.Value
        Dim ds As DataSet = ViewState("ComponentDataset")
        Dim editedParamdt As DataTable = SessionManager.getEditedComponentDataTable()
        Dim dt As DataTable = ds.Tables(0)
        Dim expression As String = String.Empty
        expression = "ComponentId =" + selectedComponent
        Dim resultRow() As DataRow

        Dim cycle As String = String.Empty
        Dim frequency As String = String.Empty
        Dim materialCost As String = String.Empty
        Dim labourCost As String = String.Empty

        Dim selectedCycle, selectedFrequency, selectedMaterialCost, selectedLabourCost As String
        resultRow = editedParamdt.Select(expression)
        If resultRow.Length > 0 Then

            cycle = resultRow(0)("Cycle").ToString()
            frequency = resultRow(0)("Frequency").ToString()
            materialCost = resultRow(0)("MaterialCost").ToString()
            labourCost = resultRow(0)("LabourCost").ToString()
        Else
            resultRow = dt.Select(expression)
            cycle = resultRow(0)("Cycle").ToString()
            frequency = resultRow(0)("Frequency").ToString()
            materialCost = resultRow(0)("MaterialCost").ToString()
            labourCost = resultRow(0)("LabourCost").ToString()
        End If

        selectedCycle = ddlCycle.SelectedValue
        selectedFrequency = ddlFrequency.SelectedItem.Text
        selectedMaterialCost = txtBoxMaterialCost.Text
        selectedLabourCost = txtBoxlabourCost.Text

        If (Not selectedCycle.Equals(cycle) Or Not selectedFrequency.Equals(frequency) Or Not selectedMaterialCost.Equals(materialCost) Or Not selectedLabourCost.Equals(labourCost)) Then
            Return True
        End If

        Return False

    End Function

#End Region

#Region "Activate Component Cost Tab"
    ''' <summary>
    ''' Activate Component Cost Tab
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateComponentCostTab()

        txtBoxSearch.Enabled = True
        ddlScheme.Enabled = True
        lnkBtnDueTab.Visible = False
        MainView.ActiveViewIndex = 0
        btnEditParameters.Visible = True
        btnSaveChanges.Visible = False
        LoadReport()

    End Sub

#End Region

#Region "Activate Due Year Tab"
    ''' <summary>
    ''' Activate Due Year Tab
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateDueYearTab()

        lnkBtnComponentReport.Visible = False
        MainView.ActiveViewIndex = 1

    End Sub

#End Region

#Region "Get Cost Components Report"
    ''' <summary>
    ''' Get Cost Components Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub getCostComponentsReport(ByVal flag As Boolean, ByVal startYear As String, ByVal searchedText As String, ByVal resultDataset As DataSet)
        Try
            Dim objReportsBL As ReportsBL = New ReportsBL()
            Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
            updateInitialDataTable(flag, startYear, selectedScheme, searchedText)

            Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
            Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
            objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

            ViewState("CurrentDT") = resultDataset.Tables(0)
            If resultDataset.Tables(0).Rows.Count > 0 Then

                If CType(resultDataset.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
                    grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                Else
                    grdVwCostComponent.Columns(1).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
                End If
                grdVwCostComponent.Columns(2).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
                grdVwCostComponent.Columns(3).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
                grdVwCostComponent.Columns(4).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
                grdVwCostComponent.Columns(5).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
                grdVwCostComponent.DataSource = resultDataset.Tables(0)
                grdVwCostComponent.DataBind()
                loadFooter()
                pnlExportToXls.Visible = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Get full Cost Component Report GridView"
    ''' <summary>
    ''' Get full Cost Component Report GridView
    ''' </summary>
    ''' <remarks></remarks>
    Function getFullComponentReportGridView()


        Dim Dt As DataTable = New DataTable()
        Dim searchText As String = txtBoxSearch.Text.Trim()
        Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        Dim noofYear As Integer = CType(ddlYears.SelectedItem.Value, Integer)
        Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
        Dim flag As Boolean = False
        If next5YearsCount > 0 Then
            flag = True
        End If
        Dim startYear As String = String.Empty
        If next5YearsCount > 0 Then
            Dim year As String = DateTime.Now.AddYears(-1).ToString()
            startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
        Else
            startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
        End If
        Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
        Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()

        Dim grdFullComponentReport As New GridView()
        grdFullComponentReport.AllowPaging = False
        grdFullComponentReport.AutoGenerateColumns = False
        grdFullComponentReport.AlternatingRowStyle.BackColor = Color.White
        grdFullComponentReport.RowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5F9E0")

        Dim actionField As BoundField = New BoundField()
        actionField.HeaderText = "Component"
        actionField.DataField = "ComponentName"
        grdFullComponentReport.Columns.Add(actionField)


        Dim PropertyCount As BoundField = New BoundField()
        PropertyCount.DataField = "PropertyCount"
        grdFullComponentReport.Columns.Add(PropertyCount)

        Dim Cost As BoundField = New BoundField()
        Cost.HeaderText = "Total Cost"
        Cost.DataField = "cost"
        grdFullComponentReport.Columns.Add(Cost)


        Dim PropertyCount1 As BoundField = New BoundField()
        PropertyCount1.DataField = "PropertyCount1"
        grdFullComponentReport.Columns.Add(PropertyCount1)

        Dim Cost1 As BoundField = New BoundField()
        Cost1.HeaderText = "Total Cost"
        Cost1.DataField = "cost1"
        grdFullComponentReport.Columns.Add(Cost1)


        Dim PropertyCount2 As BoundField = New BoundField()
        PropertyCount2.DataField = "PropertyCount2"
        grdFullComponentReport.Columns.Add(PropertyCount2)

        Dim Cost2 As BoundField = New BoundField()
        Cost2.HeaderText = "Total Cost"
        Cost2.DataField = "cost2"
        grdFullComponentReport.Columns.Add(Cost2)


        Dim PropertyCount3 As BoundField = New BoundField()
        PropertyCount3.DataField = "PropertyCount3"
        grdFullComponentReport.Columns.Add(PropertyCount3)

        Dim Cost3 As BoundField = New BoundField()
        Cost3.HeaderText = "Total Cost"
        Cost3.DataField = "cost3"
        grdFullComponentReport.Columns.Add(Cost3)


        Dim PropertyCount4 As BoundField = New BoundField()
        PropertyCount4.DataField = "PropertyCount4"
        grdFullComponentReport.Columns.Add(PropertyCount4)

        Dim Cost4 As BoundField = New BoundField()
        Cost4.HeaderText = "Total Cost"
        Cost4.DataField = "cost4"
        grdFullComponentReport.Columns.Add(Cost4)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportBL As ReportsBL = New ReportsBL()


        objReportBL.getCostComponentData(resultDataSet, searchText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)

        If CType(resultDataSet.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
            grdFullComponentReport.Columns(1).HeaderText = "<" + CType(resultDataSet.Tables(0).Rows(0)("yr"), String) + "  No of Properties"
        Else
            grdFullComponentReport.Columns(1).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr"), String) + "  No of Properties"
        End If
        grdFullComponentReport.Columns(3).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr1"), String) + "  No of Properties"
        grdFullComponentReport.Columns(5).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr2"), String) + "  No of Properties"
        grdFullComponentReport.Columns(7).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr3"), String) + "  No of Properties"
        grdFullComponentReport.Columns(9).HeaderText = CType(resultDataSet.Tables(0).Rows(0)("yr4"), String) + "  No of Properties"

        grdFullComponentReport.Columns(1).FooterText = ViewState("TotalProperties").ToString()
        grdFullComponentReport.Columns(2).FooterText = ViewState("TotalCost").ToString()

        grdFullComponentReport.Columns(4).FooterText = ViewState("TotalCost1").ToString()
        grdFullComponentReport.Columns(3).FooterText = ViewState("TotalProperties1").ToString()

        grdFullComponentReport.Columns(6).FooterText = ViewState("TotalCost2").ToString()
        grdFullComponentReport.Columns(5).FooterText = ViewState("TotalProperties2").ToString()

        grdFullComponentReport.Columns(8).FooterText = ViewState("TotalCost3").ToString()
        grdFullComponentReport.Columns(7).FooterText = ViewState("TotalProperties3").ToString()

        grdFullComponentReport.Columns(10).FooterText = ViewState("TotalCost4").ToString()
        grdFullComponentReport.Columns(9).FooterText = ViewState("TotalProperties4").ToString()

        grdFullComponentReport.ShowFooter = True

        grdFullComponentReport.DataSource = resultDataSet.Tables(0)
        grdFullComponentReport.DataBind()

        grdFullComponentReport.FooterRow.Cells(0).Text = "Total"
        For i As Integer = ((noofYear * 2) + 1) To grdFullComponentReport.Columns.Count
            If i < grdFullComponentReport.Columns.Count Then
                grdFullComponentReport.Columns(i).Visible = False
            End If

        Next
        Return grdFullComponentReport

    End Function
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "ComponentCostForecastReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.BufferOutput = True
        Response.Flush()
        Response.Close()

    End Sub

#End Region

#Region "Load Report"
    ''' <summary>
    ''' Load Report after saving data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadReport()
        Dim resultDataset As DataSet = New DataSet()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim next5YearsCount As Integer = SessionManager.getnext5YearsCount()
        Dim flag As Boolean = False
        If next5YearsCount > 0 Then
            flag = True
        End If
        Dim startYear As String = String.Empty
        If next5YearsCount > 0 Then
            Dim year As String = DateTime.Now.AddYears(-1).ToString()
            startYear = Convert.ToDateTime(year).AddYears(next5YearsCount * 5).Year.ToString()
        Else
            startYear = DateTime.Now.AddYears(next5YearsCount * 5).Year.ToString()
        End If
        Dim searchedText As String = txtBoxSearch.Text.Trim()
        Dim selectedScheme As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        lnkBtnComponentReport.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnDueTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
        Dim updatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
        objReportsBL.getCostComponentData(resultDataset, searchedText, selectedScheme, startYear, flag, editedParameterDt, updatedPropertyDt)
        ViewState("CurrentDT") = resultDataset.Tables(0)
        'compareDatatable(SessionManager.getInitialDataTable(), resultDataset.Tables(0))
        btnEditParameters.Enabled = True
        txtBoxSearch.Enabled = True
        If resultDataset.Tables(0).Rows.Count > 0 Then
            If CType(resultDataset.Tables(0).Rows(0)("Operator").ToString, Integer) = -1 Then
                grdVwCostComponent.Columns(1).HeaderText = "<" + CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
            Else
                grdVwCostComponent.Columns(1).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr"), String) + "  Total Cost"
            End If
            grdVwCostComponent.Columns(2).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr1"), String) + "  Total Cost"
            grdVwCostComponent.Columns(3).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr2"), String) + "  Total Cost"
            grdVwCostComponent.Columns(4).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr3"), String) + "  Total Cost"
            grdVwCostComponent.Columns(5).HeaderText = CType(resultDataset.Tables(0).Rows(0)("yr4"), String) + "  Total Cost"
            grdVwCostComponent.DataSource = resultDataset.Tables(0)
            grdVwCostComponent.DataBind()
            loadFooter()
            pnlExportToXls.Visible = True
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, False)
        End If

    End Sub

#End Region

#Region "load Footer"
    ''' <summary>
    ''' Load GridView Footer
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadFooter()
        If Not grdVwCostComponent.FooterRow Is Nothing Then
            Dim lblTotalProperties As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalProperties"), Label)
            Dim lblTotalProperties1 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalProperties1"), Label)
            Dim lblTotalProperties2 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalProperties2"), Label)
            Dim lblTotalProperties3 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalProperties3"), Label)
            Dim lblTotalProperties4 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalProperties4"), Label)

            Dim lblTotalCost As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalCost"), Label)
            Dim lblTotalCost1 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalCost1"), Label)
            Dim lblTotalCost2 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalCost2"), Label)
            Dim lblTotalCost3 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalCost3"), Label)
            Dim lblTotalCost4 As Label = CType(grdVwCostComponent.FooterRow.Cells(1).FindControl("lblTotalCost4"), Label)
            lblTotalProperties.Text = CType(totalProperties, String)
            lblTotalCost.Text = Format(totalCost, "0.00")

            If ViewState("footer") = 1 Then
                lblTotalProperties.ForeColor = Color.Red
                lblTotalCost.ForeColor = Color.Red
            Else
                lblTotalProperties.ForeColor = Color.Black
                lblTotalCost.ForeColor = Color.Black
            End If

            lblTotalProperties1.Text = CType(totalProperties1, String)
            lblTotalCost1.Text = Format(totalCost1, "0.00")

            If ViewState("footer1") = 1 Then
                lblTotalProperties1.ForeColor = Color.Red
                lblTotalCost1.ForeColor = Color.Red
            Else
                lblTotalProperties1.ForeColor = Color.Black
                lblTotalCost1.ForeColor = Color.Black
            End If

            lblTotalProperties2.Text = CType(totalProperties2, String)
            lblTotalCost2.Text = Format(totalCost2, "0.00")

            If ViewState("footer2") = 1 Then
                lblTotalProperties2.ForeColor = Color.Red
                lblTotalCost2.ForeColor = Color.Red
            Else

                lblTotalProperties2.ForeColor = Color.Black
                lblTotalCost2.ForeColor = Color.Black
            End If

            lblTotalProperties3.Text = CType(totalProperties3, String)
            lblTotalCost3.Text = Format(totalCost3, "0.00")

            If ViewState("footer3") = 1 Then
                lblTotalProperties3.ForeColor = Color.Red
                lblTotalCost3.ForeColor = Color.Red
            Else
                lblTotalProperties3.ForeColor = Color.Black
                lblTotalCost3.ForeColor = Color.Black
            End If

            lblTotalProperties4.Text = CType(totalProperties4, String)
            lblTotalCost4.Text = Format(totalCost4, "0.00")

            If ViewState("footer4") = 1 Then
                lblTotalProperties4.ForeColor = Color.Red
                lblTotalCost4.ForeColor = Color.Red
            Else
                lblTotalProperties4.ForeColor = Color.Black
                lblTotalCost4.ForeColor = Color.Black
            End If

        End If
        grdVwCostComponent.ShowFooter = True

        ViewState("footer") = 0
        ViewState("footer1") = 0
        ViewState("footer2") = 0
        ViewState("footer3") = 0
        ViewState("footer4") = 0
    End Sub
#End Region

#Region "Generate Updated Property DataTable"
    ''' <summary>
    ''' Generate Updated Property DataTable
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function generateUpdatedPropertyDt() As DataTable

        Dim table As New DataTable
        table.Columns.Add(ApplicationConstants.SidCol, GetType(Integer))
        table.Columns.Add(ApplicationConstants.DueDateCol, GetType(String))
        Return table

    End Function

#End Region

#Region "Generate Edited Component DataTable"
    ''' <summary>
    ''' Generate Edited Component DataTable
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function generateEditedComponentDt() As DataTable

        Dim table As New DataTable
        table.Columns.Add(ApplicationConstants.ComponentIdCol, GetType(Int16))
        table.Columns.Add(ApplicationConstants.ComponentNameCol, GetType(String))
        table.Columns.Add(ApplicationConstants.CycleCol, GetType(Integer))
        table.Columns.Add(ApplicationConstants.FrequencyCol, GetType(String))
        table.Columns.Add(ApplicationConstants.MaterialCostCol, GetType(Double))
        table.Columns.Add(ApplicationConstants.LabourCostCol, GetType(Double))
        table.Columns.Add(ApplicationConstants.CostFlagCol, GetType(Boolean))
        table.Columns.Add(ApplicationConstants.CycleFlagCol, GetType(Boolean))
        Return table

    End Function

#End Region

#Region "Compare Original Values"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CompareOriginalValues(ByVal componentID As String)
        Dim editedParameterDt As DataTable = SessionManager.getEditedComponentDataTable()
        Dim ds As DataSet = ViewState("ComponentDataset")
        Dim expression As String = "ComponentId =" + componentID
        Dim resultRow() As DataRow = editedParameterDt.Select(expression)
        Dim originalRow() As DataRow = ds.Tables(0).Select(expression)
        If resultRow(0)("Cycle").Equals(originalRow(0)("Cycle")) And resultRow(0)("Frequency").Equals(originalRow(0)("Frequency")) And resultRow(0)("MaterialCost").Equals(originalRow(0)("MaterialCost")) And resultRow(0)("LabourCost").Equals(originalRow(0)("LabourCost")) Then
            For Each dr As DataRow In resultRow
                editedParameterDt.Rows.Remove(dr)
            Next

        End If
    End Sub

#End Region

#Region "HighLight Row"
    Public Sub highlightRow(ByVal index As Integer)

        ''highlight row
        grdVwCostComponent.SelectRow(index)
        'For i As Integer = 1 To grdVwCostComponent.SelectedRow.Cells.Count - 1
        '    Dim cellId As String = grdVwCostComponent.SelectedRow.Cells(i).ToString()

        '    If Not grdVwCostComponent.SelectedRow.Cells(i).Text.Equals("0.00") Or Not grdVwCostComponent.SelectedRow.Cells(i).Text.Equals("") Then
        '        grdVwCostComponent.SelectedRow.Cells(i).ForeColor = System.Drawing.Color.Red
        '    End If
        'Next
        ''higlight footer

        'grdVwCostComponent.FooterRow.ForeColor = System.Drawing.Color.Yellow
        'grdVwCostComponent.FooterRow.BackColor = System.Drawing.Color.Red
        Dim str As String = grdVwCostComponent.HeaderRow.Cells(1).Text


    End Sub
#End Region


#Region "Compare DataTables"
    Public Sub compareDatatable(ByRef initialDataTable As DataTable, ByVal newDataTable As DataTable)
        'Dim i As Integer = 0


        'For Each dr As DataRow In newDataTable.Rows
        '    Dim oldDr As DataRow = initialDataTable.Rows(i)

        '    If Not dr(4).Equals(oldDr(4)) Or Not dr(5).Equals(oldDr(5)) Then
        '        dr("IsEdited") = CType(1, String)
        '    End If
        '    If Not dr(8).Equals(oldDr(8)) Or Not dr(9).Equals(oldDr(9)) Then
        '        dr("IsEdited1") = 1
        '    End If
        '    If Not dr(13).Equals(oldDr(13)) Or Not dr(14).Equals(oldDr(14)) Then
        '        dr("IsEdited2") = 1
        '    End If
        '    If Not dr(18).Equals(oldDr(18)) Or Not dr(19).Equals(oldDr(19)) Then
        '        dr("IsEdited3").GetType()
        '        dr.Item("IsEdited3") = True
        '    End If
        '    If Not dr(23).Equals(oldDr(23)) Or Not dr(24).Equals(oldDr(24)) Then
        '        dr("IsEdited4") = 1
        '    End If
        '    i = i + 1

        'Next
        'newDataTable.AcceptChanges()
    End Sub

#End Region

#End Region

    Private Sub updateInitialDataTable(ByVal flag As Boolean, ByVal startYear As String, ByVal selectedScheme As Integer, ByVal searchedText As String)
        Dim getEditedDT As DataTable = generateEditedComponentDt()
        Dim getUpdatedPropertyDt As DataTable = generateUpdatedPropertyDt()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim resultdataset As DataSet = New DataSet()

        objReportsBL.getCostComponentData(resultdataset, searchedText, selectedScheme, startYear, flag, getEditedDT, getUpdatedPropertyDt)

        SessionManager.setInitialDataTable(resultdataset.Tables(0))


    End Sub
End Class