﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ConditionRating.aspx.vb" Inherits="PlannedMaintenance.ConditionRating" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="ConditionRatingToBeApproved" Src="~/Controls/Reports/ConditionRatingToBeApproved.ascx" %>
<%@ Register TagPrefix="uc3" TagName="ConditionRatingApproved" Src="~/Controls/Reports/ConditionRatingApproved.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ConditionRatingRejected" Src="~/Controls/Reports/ConditionRatingRejected.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Reports/ConditionMoreDetail.ascx" TagName="ConditionMoreDetail"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        .message
        {
            padding-left: 12px;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
        .select_div select{
            height:28px !important;
            width: 205px !important
        }
        hr{
            background-color: #CCCCCC;
            border: 0 none;
            color: #A0A0A0;
            height: 1px;
            margin: 0px;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    
        .dashboard, .dashboard td
        {
            padding: 5px 5px 5px 5px !important;
        }
    </style>
    <script type="text/javascript">

        function sendSMS(mobile, body, smsurl) {

            $.ajax({
                type: "GET",
                url: smsurl,
                data: "mobile=" + mobile + "&message=" + body + "",
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                },
                error: function (xhr, error) {
                },
            });
        }

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelConditionRating" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Condition Rating Approval Report</span>
                    <div class="field right" style="width:169px;">
                        <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;">
                            <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; background-position: right; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                                class="searchbox styleselect-control searchbox searchText right" onkeyup="TypingInterval();" PlaceHolder="Quick find" runat="server">
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText right">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                        </asp:Panel>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <div style="padding: 10px 10px 0 10px;">
                        <div style="overflow:auto; padding: 0px !important;">
                            <asp:LinkButton ID="lnkBtnConditionRatingToBeApprovedTab" OnClick="lnkBtnConditionRatingToBeApprovedTab_Click" 
                                CssClass="display TabInitial" runat="server">To Be Approved: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnConditionRatingApprovedTab" OnClick="lnkBtnConditionRatingApprovedTab_Click" 
                                CssClass="display TabInitial" runat="server" style="display:block;">Approved: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnConditionRatingRejectedTab" OnClick="lnkBtnConditionRatingRejectedTab_Click" 
                                CssClass="display TabInitial" runat="server" style="display:block;">Rejected: </asp:LinkButton>
                            <span style="display:block; height: 27px; border-bottom:1px solid #c5c5c5">
                            </span>
                        </div>    
                    </div>
                    <div style="padding:0 10px">
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <uc2:ConditionRatingToBeApproved ID="ConditionRatingToBeApproved" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <uc3:ConditionRatingApproved ID="ConditionRatingApproved" runat="server" />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <uc4:ConditionRatingRejected ID="ConditionRatingRejected" runat="server" />
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>