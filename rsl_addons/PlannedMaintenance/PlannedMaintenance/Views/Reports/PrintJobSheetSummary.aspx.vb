﻿Imports PL_Utilities
Imports PL_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessObject

Public Class PrintJobSheetSummary
    Inherits PageBase

#Region "Data Members"
    Dim objJobSheetBL As New JobSheetBL()
    Dim jSNClass As String = String.Empty
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            UIMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If UIMessageHelper.IsError = False Then
                    Me.populateJobSheetSummary(jSNClass)
                End If
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "Clickheretoprint()", True)
        Catch ex As Exception
            UIMessageHelper.IsError = True
            UIMessageHelper.message = ex.Message

            If UIMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If UIMessageHelper.IsError = True Then
                UIMessageHelper.setMessage(lblMessage, pnlMessage, UIMessageHelper.message, True)
            End If
        End Try
    End Sub

    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString("JobSheetNumber")) Then
            jSNClass = Request.QueryString("JobSheetNumber").ToString
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidJobSheetNumber
        End If
    End Sub

    ''' <summary>
    ''' Get job sheet summary data on the basis of selected JSN no
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Private Sub populateJobSheetSummary(ByRef jsn As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objJobSheetBL As JobSheetBL = New JobSheetBL()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)

        objJobSheetBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)
        resetVisibility()
        controlsVisibilityByType(dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0).Item("Type").ToString())
        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)
                lblJSN.Text = .Item("AppointmentId").ToString().Trim()
                lblPlannedWorkComponent.Text = .Item("COMPONENTNAME")
                lblTrade.Text = .Item("Trade")
                lblStatus.Text = .Item("AppointmentStatus")
                lblContractor.Text = .Item("Contractor")
                lblContact.Text = .Item("Contact")
                txtWorksRequired.Text = .Item("WorkRequired")

                lblPONo.Text = .Item("PurchaseORDERID")
                lblOrderedBy.Text = .Item("ORDERBY")
                lblOrdered.Text = .Item("PODATE")

                lblBudget.Text = .Item("BUDGET")
                lblEstimate.Text = .Item("Estimate")
                lblVat.Text = .Item("Vat")
                lblNetCost.Text = .Item("NetCost")

                lblScheme.Text = .Item("SCHEMENAME")
                lblHouseNumber.Text = .Item("HOUSENUMBER")
                lblAddress1.Text = .Item("HOUSEADDRESS1")
                lblAddress2.Text = .Item("HOUSEADDRESS2")
                lblAddress3.Text = .Item("HOUSEADDRESS3")

                lblCustomer.Text = .Item("ClientName")
                lblTelephone.Text = .Item("ClientTel")
                lblMobile.Text = .Item("ClientMobile")
                lblEmail.Text = .Item("ClientEmail")

            End With
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

#Region "Controls Visibility by type"
    Sub controlsVisibilityByType(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblBlock.Visible = False
            lblForCustomer.Visible = False
            lblCustomer.Visible = False
            lblTelephone.Visible = False
            lblMobile.Visible = False
            lblForEmail.Visible = False
            lblForHouseNumber.Visible = False
            lblHouseNumber.Visible = False
            lblForTelephone.Visible = False
            lblForMobile.Visible = False
            lblEmail.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
#End Region

#Region "Reset Visibility by"
    Sub resetVisibility()
        lblForBlock.Visible = True
        lblForCustomer.Visible = True
        lblCustomer.Visible = True
        lblTelephone.Visible = True
        lblMobile.Visible = True
        lblEmail.Visible = True
        lblForHouseNumber.Visible = True
        lblHouseNumber.Visible = True
        lblForTelephone.Visible = True
        lblForMobile.Visible = True
        lblEmail.Visible = True
        lblBlock.Visible = True
    End Sub
#End Region

End Class