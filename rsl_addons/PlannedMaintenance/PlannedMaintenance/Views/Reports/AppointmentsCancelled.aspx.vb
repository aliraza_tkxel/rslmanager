﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing
Public Class AppointmentsCancelled1
    Inherits PageBase


#Region "Properties"


#End Region

#Region "Events"


#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
                Dim search As String = ""
                If Not IsNothing(SessionManager.getConditionRatingAptSearch()) Then
                    search = SessionManager.getConditionRatingAptSearch()
                    SessionManager.removeConditionRatingAptSearch()
                End If
                txtSearch.Text = search
                populateAppointmentsCancelledReport()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region


#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try
            populateAppointmentsCancelledReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Populate Appointments Cancelled Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentsCancelledReport()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = txtSearch.Text
        Dim isFullList As Boolean = False
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
        ViewState.Add(ViewStateConstants.Search, "")
        ViewState.Add(ViewStateConstants.SchemeId, -1)
        ViewState.Add(ViewStateConstants.AppointmentComponentId, -1)
        Dim totalCount As Integer = objSchedulingBL.getAppointmentCancelledList(resultDataset, objPageSortBo, -1, searchtext, -1)
        grdAppointmentToBeArranged.DataSource = resultDataset
        grdAppointmentToBeArranged.DataBind()
        If (totalCount > 0) Then
            'pnlExportToExcel.Visible = True
        Else
            'pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        setResultDataSetViewState(resultDataset)

    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Page Index Changing Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdAppointmentToBeArranged.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentCancelledList(resultDataSet, search, schemeId, componentId, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Sorting Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdAppointmentToBeArranged.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim resultDataSet As DataSet = New DataSet()

            populateAppointmentCancelledList(resultDataSet, search, schemeId, componentId, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdAppointmentToBeArranged_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 25)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            populateAppointmentCancelledList(resultDataSet, search, schemeId, componentId, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 25)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
                Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
                populateAppointmentCancelledList(resultDataSet, search, schemeId, componentId, False)
            Else
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()

        ViewState.Item(ViewStateConstants.Search) = String.Empty
        ViewState.Item(ViewStateConstants.SchemeId) = -1

        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        grdAppointmentToBeArranged.DataBind()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region


#Region "Arrow button event"
    ''' <summary>
    ''' Arrow button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewClick(sender As Object, e As EventArgs)

        Try

            Dim btnView As Button = DirectCast(sender, Button)
            Dim row As GridViewRow = DirectCast(btnView.NamingContainer, GridViewRow)
            Dim lblJsn As Label = DirectCast(row.FindControl("lblJSN"), Label)
            Dim lblJsnRef As Label = DirectCast(row.FindControl("lblJSNRef"), Label)
            Dim pmo As Integer = Convert.ToInt32(btnView.CommandArgument.ToString())
            'this fuction will save the data in session (on row which user clicked)
            saveSelectedDataInSession(lblJsn.Text, pmo)
            'redirect the page to job sheet
            Response.Redirect(PathConstants.ViewCancelledAppointments + "?" + PathConstants.Jsn + "=" + lblJsnRef.Text + "&" + PathConstants.Pmo + "=" + Convert.ToString(pmo))

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Arrow button event"
    ''' <summary>
    ''' Arrow button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(sender As Object, e As EventArgs)

        Try
            exportFullList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region


#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim pmoField As BoundField = New BoundField()
        pmoField.HeaderText = "PMO:"
        pmoField.DataField = "PMO"
        grdFullConditionList.Columns.Add(pmoField)


        Dim jsnField As BoundField = New BoundField()
        jsnField.HeaderText = "JSN:"
        jsnField.DataField = "JSN"
        grdFullConditionList.Columns.Add(jsnField)


        Dim schemeField As BoundField = New BoundField()
        schemeField.HeaderText = "Scheme:"
        schemeField.DataField = "Scheme"
        grdFullConditionList.Columns.Add(schemeField)

        Dim blockField As BoundField = New BoundField()
        blockField.HeaderText = "Block:"
        blockField.DataField = "PJBlock"
        grdFullConditionList.Columns.Add(blockField)


        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)


        Dim postCodeField As BoundField = New BoundField()
        postCodeField.HeaderText = "Postcode:"
        postCodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postCodeField)


        Dim componentField As BoundField = New BoundField()
        componentField.HeaderText = "Component:"
        componentField.DataField = "Component"
        grdFullConditionList.Columns.Add(componentField)


        Dim dueField As BoundField = New BoundField()
        dueField.HeaderText = "TRADE:"
        dueField.DataField = "TRADES"
        grdFullConditionList.Columns.Add(dueField)


        Dim duePrevField As BoundField = New BoundField()
        duePrevField.HeaderText = "Due (Previous):"
        duePrevField.DataField = "DueP"
        grdFullConditionList.Columns.Add(duePrevField)


        Dim dueNewField As BoundField = New BoundField()
        dueNewField.HeaderText = "Due (New):"
        dueNewField.DataField = "Due"
        grdFullConditionList.Columns.Add(dueNewField)


        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "REASON:"
        byField.DataField = "REASON"
        grdFullConditionList.Columns.Add(byField)

        Dim search As String = ViewState.Item(ViewStateConstants.Search)
        Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
        Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
        Dim resultDataSet As DataSet = New DataSet()


        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
        objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        Dim objPageSortBo2 As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
        objPageSortBo2 = ViewState(ViewStateConstants.PageSortBo)
        objPageSortBo2.PageSize = 65000
        objPageSortBo2.PageNumber = 1
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo2

        populateAppointmentCancelledList(resultDataSet, search, schemeId, componentId, False)

        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
        grdFullConditionList.DataSource = resultDataSet.Tables(0)
        grdFullConditionList.DataBind()
        ExportGridToExcel(grdFullConditionList)
    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "CancelledAppointments_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Buffer = True
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        HttpContext.Current.Response.Charset = ""
        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Output.Write(sw.ToString())
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()

    End Sub

#End Region

#Region "Check PO number"

    Public Function CheckPO(ByRef PO As String)
        Dim status As Boolean = True

        If (PO.EndsWith("-")) Then
            status = False
            'End If
        End If

        Return status
    End Function

#End Region

#Region "Save Selected Data In Session"
    ''' <summary>
    ''' This function will save the selected data in session (when user clicked on blue arrow)
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <param name="pmo"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedDataInSession(ByVal jsn As String, ByVal pmo As Integer)
        'get the result data set 
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        'get the record which is clicked/selected
        Dim query = (From res In resultDataSet.Tables(0).AsEnumerable Where res.Item("JSN") = jsn Select res)
        'save that selected record into datatable
        objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
        'save the datatable having selected record in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        'ceate the object of planned appointment bo
        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo.PMO = pmo
        'save property id, coponent id and jsn
        objPlannedAppointmentBo.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
        objPlannedAppointmentBo.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"), String)
        objPlannedAppointmentBo.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
        objPlannedAppointmentBo.Jsn = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("JSN"), Integer)
        'save the planned appointment bo in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)


    End Sub
#End Region

#Region "Populate Appointment Cancelled List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="search"></param>
    ''' <param name="schemeId"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Sub populateAppointmentCancelledList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal schemeId As Integer, ByVal componentId As Integer,
                                            ByVal setSession As Boolean)
        'Dim componentId As Integer = -1

        'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
        '    componentId = Request.QueryString(PathConstants.ComponentId)
        'End If

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim totalCount As Integer = 0

        ViewState.Add(ViewStateConstants.Search, search)
        ViewState.Add(ViewStateConstants.SchemeId, schemeId)
        ViewState.Add(ViewStateConstants.AppointmentComponentId, componentId)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        totalCount = objSchedulingBL.getAppointmentCancelledList(resultDataSet, objPageSortBo, schemeId, search, componentId)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        Me.setResultDataSetViewState(resultDataSet)
        grdAppointmentToBeArranged.DataBind()

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 25)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            grdAppointmentToBeArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentToBeArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentToBeArranged.DataSource = getResultDataSetViewState()
            grdAppointmentToBeArranged.DataBind()
        End If

    End Sub

#End Region


#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 25)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region




#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region


End Class