﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AssignedToContractor

    '''<summary>
    '''pnlSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearch As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''TextBoxWatermarkExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender1 As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''updPnlContractorList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPnlContractorList As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''grdcontractorList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdcontractorList As Global.Fadrian.Web.Control.PagingGridView

    '''<summary>
    '''pnlPagination control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPagination As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lnkbtnPagerFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerFirst As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerPrev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerPrev As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblPagerCurrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerCurrentPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerTotalPages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerTotalPages As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordStart As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordEnd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkbtnPagerNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerNext As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerLast As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnGoPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGoPageNumber As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rangevalidatorPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rangevalidatorPageNumber As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''txtPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''pnlJobSheetSummary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlJobSheetSummary As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnPrevious control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPrevious As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNext As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblTotalSheets control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalSheets As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblJSN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblJSN As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPlannedWorkComponent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPlannedWorkComponent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPONo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPONo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContractor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContractor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBudget As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContact As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEstimate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEstimate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtWorksRequired control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWorksRequired As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblNetCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNetCost As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOrderedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOrderedBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblVat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVat As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOrdered control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOrdered As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOrderTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOrderTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForScheme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblScheme As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForCustomer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForBlock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForBlock As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBlock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBlock As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForTelephone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForTelephone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTelephone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelephone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForPostCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForMobile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForMobile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMobile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMobile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForHouseNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForHouseNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblHouseNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHouseNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblForEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btn_CustDetailUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_CustDetailUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''grdAsbestos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdAsbestos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mdlPopupJobSheetSummarySubContractorUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlPopupJobSheetSummarySubContractorUpdate As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''updPnlRepairDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPnlRepairDetail As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlRepairDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRepairDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlCompleteMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCompleteMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblCompleteMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompleteMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtCompleted control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompleted As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''imgCalendar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCalendar As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''cntrlCalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cntrlCalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''TextBoxWatermarkExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender2 As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''ddlHours control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlHours As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMin As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlFormate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFormate As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtBoxFollowOnNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxFollowOnNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCompleteAppSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCompleteAppSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mdlCompleteAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlCompleteAppointment As Global.AjaxControlToolkit.ModalPopupExtender
End Class
