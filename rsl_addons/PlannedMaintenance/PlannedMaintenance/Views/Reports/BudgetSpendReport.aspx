﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="BudgetSpendReport.aspx.vb" Inherits="PlannedMaintenance.BudgetSpendReport" %>

<%@ Register TagName="BudgetSpendReportDetail" TagPrefix="uc1" Src="~/Controls/Reports/BudgetSpendReportDetail.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <style>
        .searchbox
        {
            border: 1px solid Gray;
            background-image:none; 
            padding:2px; 
            font-size:12px;
            margin : 5px 5px 5px 5px;
            height:17px;
            float:right;
        }
        .list-select select
        {
            border-radius: 0px;
            border: 1px solid #b1b1b1;
            height: 25px !important;
            font-size: 12px !important;
            width: 205px !important;
            margin: -4px 10px 0 0;
        }
        .dashboard th{
            padding:8px !important;
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
            width: 150px !important;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet">
        <div class="header pnlHeading">
            <span class="header-label">Budget Spend Report</span>
            <div class="field right" style="width: 70%;">
                <asp:Panel ID="pnlSearch" runat="server" Visible="True">
                    <div class="right" id="dropdownContainer ">
                        <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:2px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                            class="searchbox styleselect-control searchbox searchText right" onkeyup="TypingInterval();" PlaceHolder="Quick find" runat="server">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                            TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText right">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <div class="list-select right">
                            <div class="" style="margin:0">
                                <asp:DropDownList runat="server" ID="ddlScheme" Style="width: 200px;" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="list-select right">
                            <div class="" style="margin:0">
                                <asp:DropDownList runat="server" ID="ddlReplacementYear" AutoPostBack="true" >
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <asp:UpdatePanel ID="updPnlBudgetSpendReport" runat="server">
                <ContentTemplate>
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <asp:GridView runat="server" ID="grdBudgetSpendReport" CellSpacing="5" CellPadding="7"
                            GridLines="Vertical" Visible="true" Width="100%" BorderWidth="0" AutoGenerateColumns="false"
                            ShowHeaderWhenEmpty="true" ShowFooter="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Component :">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnComponentID" Value='<%# Bind("COMPONENTID") %>' />
                                        <asp:Label runat="server" ID="lblComponent" Text='<%# Bind("COMPONENTNAME") %>' Style="padding-left: 5px;"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    <FooterTemplate>
                                        <span style="color: Black; padding-left: 10px; font-weight:bold">Total:</span>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Forecast:">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" Style="margin: 10px; width: 5%; text-align: left;" ID="lnkBtnForecastCount"
                                            Text='<%# Bind("FORECAST_COUNT") %>' CommandArgument='Forecast' ForeColor ="Blue"  OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                        <asp:Label runat="server" ID="lblForecastCost" Style="float: right;" Text='<%# Bind("FORECAST_COST") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblForecastTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Approved:">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" Style="margin: 10px; width: 5%;" ID="lnkBtnApprovedCount"
                                            Text='<%# Bind("APPROVED_COUNT") %>' CommandArgument='To be Arranged' ForeColor ="Blue" OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                        <asp:Label runat="server" ID="lblApprovedCost" Style="float: right;" Text='<%# Bind("APPROVED_COST") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblApprovedTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Arranged:">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lnkBtnArrangedCount" Style="margin: 10px; width: 5%;"
                                            Text='<%# Bind("ARRANGED_COUNT") %>' CommandArgument='Arranged' ForeColor ="Blue" OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                        <asp:Label runat="server" ID="lblArrangedCost" Style="float: right;" Text='<%# Bind("ARRANGED_COST") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblArrangedTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Completed:">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lnkBtnCompletedCount" Style="margin: 10px; width: 5%;"
                                            Text='<%# Bind("COMPLETED_COUNT") %>' CommandArgument='Completed' ForeColor ="Blue" OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                        <asp:Label runat="server" ID="lblCompletedCost" Style="float: right;" Text='<%# Bind("COMPLETED_COST") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblCompletedTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>            
                                <asp:TemplateField HeaderText="Annual Budget">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAnnualBudget" Text='<%# Bind("ANNUAL_BUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblAnnualBudgetTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Budget Available">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblBudgetAvailable" Text='<%# Bind("BUDGET_AVAILABLE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblBudgetAvailableTotalCost" Style="float: right;" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BorderColor="#8b8687" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" />
                            <HeaderStyle BorderColor="#8b8687" BorderStyle="Solid" BorderWidth="4px" Font-Bold="True" />
                        </asp:GridView>
                    </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="width:40%; float:left">
                                <div class="right">
                                    <span>
                                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS"
                                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="btnExportToExcel_Click" />
                                    </span>
                                    <span>
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important;margin-right:10px; min-width:0px;" OnClick="changePageNumber" />
                                    </span>
                                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                    <div class="right" style="margin-right: 10px;">
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Pager Template End--%>
                    <%-- Budget Spend Report popup --%>
                    <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupBudgetSpendReport" runat="server" TargetControlID="btnHidden3"
                        PopupControlID="pnlBudgetSpendReport" CancelControlID="imgBtnClosePopup" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlBudgetSpendReport" runat="server" >
                        <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
                            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                        <uc1:BudgetSpendReportDetail id="ucBudgetSpendReportDetail" runat="server" />
                    </asp:Panel>
                </ContentTemplate>
                  <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportToExcel" />            
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    
</asp:Content>
