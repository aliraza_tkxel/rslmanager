﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing


Public Class CompletedWorks
    Inherits PageBase

#Region "Attributes"
    Dim objReportBL As ReportsBL = New ReportsBL()
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        If Not IsPostBack Then
            populateDropdowns()
            populateGrid()
        End If
    End Sub

#End Region

#Region "ddl Scheme Selected IndexChanged"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Try
            populateGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
#End Region

#Region "ddlPlannedType SelectedIndexChanged"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlPlannedType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPlannedType.SelectedIndexChanged
        Try
            populateGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region

#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try

            populateGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region

#Region "Grid Sorting"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdCompletedWorks_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCompletedWorks.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdCompletedWorks.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            populateGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub grdCompletedWorks_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                populateGrid()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "lnkBtn Arrow click"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnArrow_click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim lnkBtnArrow As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnArrow.NamingContainer, GridViewRow)
            Dim lblJsn As Label = DirectCast(row.FindControl("lblJSN"), Label)
            Dim pmo As Integer = Convert.ToInt32(lnkBtnArrow.CommandArgument.ToString())
            'this fuction will save the data in session (on row which user clicked)
            saveSelectedDataInSession(lblJsn.Text, pmo)
            'redirect the page to job sheet
            Dim targetPage As String = String.Format(PathConstants.ViewArrangedAppointments + "?{0}={1}&{2}={3}&{4}={5}&{6}={7}", PathConstants.Jsn, lblJsn.Text, PathConstants.Pmo, Convert.ToString(pmo), PathConstants.isReadOnly, "1", PathConstants.Src, PathConstants.JobStatusReport)
            Response.Redirect(targetPage)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If
            'Set PageSortBO in Viewstate
            setPageSortBoViewState(objPageSortBo)
            ' Populate Grid
            populateGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "populate Dropdowns"
    Private Sub populateDropdowns()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim plannedTypesDataSet As DataSet = New DataSet()
        Dim item As ListItem = New ListItem("Please Select", "-1")

        objReportBL.getAllSchemes(resultDataSet)
        ddlScheme.DataSource = resultDataSet.Tables(0)
        ddlScheme.DataValueField = "DevelopmentId"
        ddlScheme.DataTextField = "SchemeName"
        ddlScheme.DataBind()
        ddlScheme.Items.Insert(0, item)

        objSchedulingBL.getAppointmentTypes(plannedTypesDataSet, 0)
        ddlPlannedType.DataSource = plannedTypesDataSet.Tables(0)
        ddlPlannedType.DataValueField = "TypeID"
        ddlPlannedType.DataTextField = "TypeValue"
        ddlPlannedType.DataBind()
        ddlPlannedType.Items.Insert(0, item)

    End Sub
#End Region

#Region "populate Grid"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateGrid()

        Dim totalCount As Integer = 0
        Dim resultDataSet As DataSet = New DataSet()

        Dim objPageSortBo As PageSortBO = ViewState(ViewStateConstants.PageSortBo)
        If objPageSortBo Is Nothing Then
            objPageSortBo = New PageSortBO("DESC", "Address", 1, 30)
        End If
        'get Search, Scheme and Type values
        Dim search As String = txtSearch.Text
        Dim PlannedType As Integer = Convert.ToInt32(ddlPlannedType.SelectedValue)
        Dim schemeId As Integer = Convert.ToInt32(ddlScheme.SelectedValue)
        ' Set Viewstate of Search , Scheme and PlannedType
        ViewState.Add(ViewStateConstants.Search, search)
        ViewState.Add(ViewStateConstants.PlannedType, PlannedType)
        ViewState.Add(ViewStateConstants.SchemeId, schemeId)


        totalCount = objReportBL.getCompletedWorksList(resultDataSet, objPageSortBo, schemeId, search, PlannedType)

        grdCompletedWorks.VirtualItemCount = totalCount
        grdCompletedWorks.DataSource = resultDataSet
        grdCompletedWorks.DataBind()

        ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

        setResultDataSetViewState(resultDataSet)
        ' If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdCompletedWorks.PageCount > 1 Then
            pnlPagination.Visible = True
            setVirtualItemCountViewState(totalCount)
            'setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region


#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Save Selected Data In Session"
    ''' <summary>
    ''' This function will save the selected data in session (when user clicked on blue arrow)
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <param name="pmo"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedDataInSession(ByVal jsn As String, ByVal pmo As Integer)
        'get the result data set 
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        'get the record which is clicked/selected
        Dim query = (From res In resultDataSet.Tables(0).AsEnumerable Where res.Item("JSN") = jsn Select res)
        'save that selected record into datatable
        objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
        'save the datatable having selected record in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        'ceate the object of planned appointment bo
        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo.PMO = pmo
        'save property id, coponent id and jsn
        objPlannedAppointmentBo.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
        objPlannedAppointmentBo.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("AppointmentType"), String)
        objPlannedAppointmentBo.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
        objPlannedAppointmentBo.Jsn = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("AppointmentId"), Integer)
        'save the planned appointment bo in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)


    End Sub
#End Region


#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region



#End Region
    


End Class