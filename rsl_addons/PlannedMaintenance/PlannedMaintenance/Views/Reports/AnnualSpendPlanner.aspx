﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="AnnualSpendPlanner.aspx.vb" Inherits="PlannedMaintenance.AnnualSpendPlanner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="AnnualComponentPropertyList" Src="~/Controls/Reports/AnnualComponentPropertyList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            clearTimeout(typingTimer);
            //if ($("#<%= txtBoxSearch.ClientID %>").val()) {

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }
        function searchTextChanged() {
            __doPostBack('<%= txtBoxSearch.ClientID %>', '');
        }

        function ShowNavigation(sender, args) {
            sender._switchMode("years", true);
        }



    </script>
    <style>
        .select_div select{
            height:28px !important;
            width: 205px !important
        }
        hr{
            background-color: #CCCCCC;
            border: 0 none;
            color: #A0A0A0;
            height: 1px;
            margin: 0px;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    
        .dashboard, .dashboard td
        {
            padding: 5px 5px 5px 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:UpdatePanel runat="server" ID="updPnlComponentCost" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Component Cost Forecast Report</span>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <div style="overflow:auto; border-bottom:1px solid ##A0A0A0">
                        <asp:Panel ID="pnlSearch" CssClass="form-control right" runat="server" HorizontalAlign="Right">
                            <div class="select_div right" style="padding-top:12px; width:inherit">
                                <div class="field right" style="margin-left:0; width:inherit">
                                    <asp:Button runat="server" ID="btnEditParameters" Text="Edit Parameters" UseSubmitBehavior="false"
                                        CssClass="btn btn-xs btn-blue right" style="padding:4px 23px !important; margin:-3px 0 0 0;" />
                                    <asp:Button runat="server" ID="btnSaveChanges" Text="Save Changes" UseSubmitBehavior="false"
                                        CssClass="btn btn-xs btn-blue right" style="padding:4px 23px !important; margin:-3px 0 0 0;"
                                        Visible="false" />
                                    <asp:DropDownList runat="server" ID="ddlScheme" AutoPostBack="true " style="padding:4px  10px !important; width:150px; margin:-3px 20px 0 0;">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtBoxSearch" AutoPostBack="false" style="padding:3.4px 10px !important; width:150px; margin:-3px 20px 0 0;" AutoCompleteType="Search" 
                                        ToolTip="Search" CssClass="searchbox searchText styleselect-control right" runat="server" onkeyup="TypingInterval();">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <hr />
                    <div style="padding: 10px 10px 0 10px;">
                        <div style="overflow:auto; padding: 0px !important;">
                            <asp:LinkButton ID="lnkBtnComponentReport" CssClass="display TabInitial"
                                runat="server">Component Cost Forecast: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnDueTab" CssClass="display TabInitial" 
                                runat="server" style="display:block;" Visible="false">
                                <asp:ImageButton ImageUrl="../../Images/cross.png" ID="btnCloseTab" style="height: 15px;border: none;" 
                                    runat="server" OnClick="btnCloseTab_Click"/>
                                Due <asp:Label ID="lblHeaderYear" runat="server" ></asp:Label>: 
                            </asp:LinkButton>
                            <span style="display:block; height: 27px; border-bottom:1px solid #c5c5c5">
                            </span>
                        </div>    
                    </div>
                    <div style="padding:0 10px">
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <asp:GridView ID="grdVwCostComponent" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard dashboard-border webgrid table table-responsive" EmptyDataText="No Records Found"
                                    GridLines="Vertical" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Component :">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hdnComponentID" Value='<%# Bind("COMPONENTID") %>' />
                                                <asp:Label runat="server" ID="lblComponent" Text='<%# Bind("ComponentName") %>' style="padding-left: 10px;"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <FooterTemplate>
                                                <span style="color: Black;padding-left:10px;" font-bold="true">Total:</span>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnIsEdited" Value='<%# Bind("isEdited") %>' />
                                                <asp:LinkButton runat="server" Style="margin: 20px; width: 5%;align:left;" ID="lnkBtnPropertyCount"
                                                    Text='<%# Bind("PropertyCount") %>' CommandArgument='<%# Convert.ToString(Eval("yr")) + ";" + Convert.ToString(Eval("Operator")) %>'
                                                    OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblCost" Style="width: 5%; align: right; text-align:right;"
                                                    Text='<%# Bind("Cost") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalProperties" style="padding-left: 20px;margin-right: 10px;"></asp:Label>
                                                <asp:Label ID="lblTotalCost" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                             <asp:HiddenField runat="server" ID="hdnIsEdited1" Value='<%# Bind("isEdited1") %>' />
                                                <asp:LinkButton runat="server" Style="margin: 20px; width: 5%;" ID="lnkBtnPropertyCount1"
                                                    Text='<%# Bind("PropertyCount1") %>' CommandArgument='<%# Convert.ToString(Eval("yr1")) + ";" + Convert.ToString(Eval("Operator2")) %>'
                                                    OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblCost1" Style="width: 5%; align: right;"
                                                    Text='<%# Bind("Cost1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalProperties1" style="padding-left: 20px;margin-right: 10px;"></asp:Label>
                                                <asp:Label ID="lblTotalCost1" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                             <asp:HiddenField runat="server" ID="hdnIsEdited2" Value='<%# Bind("isEdited2") %>' />
                                                <asp:LinkButton runat="server" ID="lnkBtnPropertyCount2" Style="margin: 20px; width: 5%;"
                                                    Text='<%# Bind("PropertyCount2") %>' CommandArgument='<%# Convert.ToString(Eval("yr2")) + ";" + Convert.ToString(Eval("Operator3")) %>'
                                                    OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblCost2" Style="width: 5%; align: right;"
                                                    Text='<%# Bind("Cost2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalProperties2" style="padding-left: 20px;margin-right: 10px;"></asp:Label>
                                                <asp:Label ID="lblTotalCost2" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                             <asp:HiddenField runat="server" ID="hdnIsEdited3" Value='<%# Bind("isEdited3") %>' />
                                                <asp:LinkButton runat="server" ID="lnkBtnPropertyCount3" Style="margin: 20px; width: 5%;"
                                                    Text='<%# Bind("PropertyCount3") %>' CommandArgument='<%# Convert.ToString(Eval("yr3")) + ";" + Convert.ToString(Eval("Operator4")) %>'
                                                    OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblCost3" Style="width: 5%; align: right;"
                                                    Text='<%# Bind("Cost3") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalProperties3" style="padding-left: 20px;margin-right: 10px;"></asp:Label>
                                                <asp:Label ID="lblTotalCost3" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                             <asp:HiddenField runat="server" ID="hdnIsEdited4" Value='<%# Bind("isEdited4") %>' />
                                                <asp:LinkButton runat="server" ID="lnkBtnPropertyCount4" Style="margin: 20px; width: 5%;"
                                                    Text='<%# Bind("PropertyCount4") %>' CommandArgument='<%# Convert.ToString(Eval("yr4")) + ";" + Convert.ToString(Eval("Operator5")) %>'
                                                    OnClick="lnkBtnPropertyCount_Click"></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblCost4" Style="width: 5%; align: right;"
                                                    Text='<%# Bind("Cost4") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalProperties4" style="padding-left: 20px;margin-right: 10px;"></asp:Label>
                                                <asp:Label ID="lblTotalCost4" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div style="padding:10px;">
                                    <asp:Panel runat="server" ID="pnlExportToXls" Style="float: right;" Visible="false">
                                        Select Period to export:
                                        <asp:DropDownList runat="server" ID="ddlYears">
                                            <asp:ListItem Value="1">1 Year</asp:ListItem>
                                            <asp:ListItem Value="2">2 Year</asp:ListItem>
                                            <asp:ListItem Value="3">3 Year</asp:ListItem>
                                            <asp:ListItem Value="4">4 Year</asp:ListItem>
                                            <asp:ListItem Value="5">5 Year</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Button runat="server" ID="btnExportToExcel" Text="Export to XLS" UseSubmitBehavior="false" />
                                         <asp:Button runat="server" ID="btnPrevious5YearsData" Text="View previous 5 years" UseSubmitBehavior="false"  Enabled="false"/>
                                        <asp:Button runat="server" ID="btnNext5YearsData" Text="View next 5 years" UseSubmitBehavior="false" />                                   
                                    </asp:Panel>
                                </div>
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <uc2:AnnualComponentPropertyList ID="AnnualComponentPropertyList" runat="server" />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlEditParameters" runat="server" BackColor="White" Style="padding-left: 10px;
                padding-bottom: 10px;" Width="300px" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px">
                <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false">
                    <asp:Label ID="lblPopupMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <table>
                    <tr style="border-bottom: 1px solid black;">
                        <td>
                            Edit Parameters
                        </td>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnSave" Text="Save" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Component:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlComponent" ViewStateMode="Enabled"   Style="width: 138px; margin-left: 8px;"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cycle:
                        </td>
                        <td style="padding-left: 8px;">
                            <asp:DropDownList runat="server" ID="ddlCycle">
                            </asp:DropDownList>
                            <asp:DropDownList runat="server" ID="ddlFrequency" >
                                <asp:ListItem Value="1">Months</asp:ListItem>
                                <asp:ListItem Value="2">Years</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Material Cost:
                        </td>
                        <td>
                            £<asp:TextBox runat="server" ID="txtBoxMaterialCost"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr><td></td><td><asp:RegularExpressionValidator id="RegularExpressionValidator1"
                   ControlToValidate="txtBoxMaterialCost"
                   ValidationExpression="^\d+([.]\d{1,3})?$"
                   Display="Static"
                   EnableClientScript="true"
                   ErrorMessage="Please enter numbers only"
                   runat="server"/></td></tr>
                    <tr>
                        <td>
                            Labour Cost:
                        </td>
                        <td>
                            £<asp:TextBox runat="server" ID="txtBoxlabourCost"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr><td></td><td><asp:RegularExpressionValidator id="RegularExpressionValidator2"
                   ControlToValidate="txtBoxlabourCost"
                   ValidationExpression="^\d+([.]\d{1,3})?$"
                   Display="Static"
                   EnableClientScript="true"
                   ErrorMessage="Please enter numbers only"
                   runat="server"/></td></tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpEditParameters" runat="server" DynamicServicePath=""
                TargetControlID="lblDispPhotoGraph" PopupControlID="pnlEditParameters" DropShadow="true"
                CancelControlID="lblDispPhotoGraph">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>
            <asp:Panel runat="server" ID="pnlConfirmation" BackColor="White" Style="padding-left: 10px;
                padding-bottom: 10px;" Width="300px" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px">
                <asp:Label runat="server" ID="lblConfrimMsg" Text="Do you want save changes?"></asp:Label>
                <hr />
                <asp:Button runat="server" ID="btnContinue" Text="Continue" />
                <asp:Button runat="server" ID="btnSaveParameterData" Text="Save" OnClick="btnSave_Click" />
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpConfirm" runat="server" DynamicServicePath=""
                TargetControlID="lblPopUpConfirm" PopupControlID="pnlConfirmation" DropShadow="true"
                CancelControlID="lblPopUpConfirm">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Label ID="lblPopUpConfirm" runat="server"></asp:Label>
            <!--Confirm PoPUp on Tab Close -->
             <asp:Panel runat="server" ID="pnlConfirmCloseTab" BackColor="White" Style="padding-left: 10px;
                padding-bottom: 10px;" Width="300px" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px">
                <asp:Label runat="server" ID="Label1" Text="Do you want save changes?"></asp:Label>
                <hr />
                <asp:Button runat="server" ID="BtnContinueClosePopUp" Text="Continue" />
                <asp:Button runat="server" ID="btnSaveConfirmClosePopUp" Text="Save" />
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpConfirmCloseTab" runat="server" DynamicServicePath=""
                TargetControlID="lblConfirmCloseTab" PopupControlID="pnlConfirmCloseTab" DropShadow="true"
                CancelControlID="lblConfirmCloseTab">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Label ID="lblConfirmCloseTab" runat="server"></asp:Label>
            <!-- End-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnCloseTab" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
