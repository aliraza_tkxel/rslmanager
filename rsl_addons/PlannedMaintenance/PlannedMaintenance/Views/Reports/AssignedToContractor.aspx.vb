﻿Imports PL_BusinessObject
Imports PL_Utilities
Imports PL_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AssignedToContractor
    Inherits PageBase

#Region "Properties / Class level Attributes"

#End Region

#Region "Events"

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                Dim search As String = ""
                If Not IsNothing(SessionManager.getConditionRatingAptSearch()) Then
                    search = SessionManager.getConditionRatingAptSearch()
                    SessionManager.removeConditionRatingAptSearch()
                End If
                txtSearch.Text = search
                populateAssignedToContractorList(search)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Grid View Events"

#Region "grdcontractorList Sorting"

    Protected Sub grdcontractorList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdcontractorList.Sorting
        Try
            'Get PageSortBO from View State
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            'setting pageindex of gridview equal to zero for new search
            grdcontractorList.PageIndex = 0

            'setting new sorting expression and order
            objPageSortBo.PageNumber = 1

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.setSortDirection()

            setPageSortBoViewState(objPageSortBo)

            'populating gridview with new results
            populateAssignedToContractorList(getSearchTextViewState)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Button Click View Button Grid View"

    Protected Sub grdContractorList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdcontractorList.SelectedIndexChanged
        Try
            Dim btnView As Button = CType(sender, Button)
            Dim jsn As String = String.Empty

            jsn = btnView.CommandArgument.ToString()
            ViewStateConstants.CurrentIndex = "1"

            Me.resetJobSheetControls()
            Me.populateJobSheetSummary(jsn)
            Me.SetJobSheetSummary()
            Me.populatePlannedStatus()

            Me.pnlJobSheetSummary.Visible = True
            ddlStatus.Visible = False
            Me.mdlPopupJobSheetSummarySubContractorUpdate.Show()
            Me.pnlCompleteMessage.Visible = False
            Me.mdlCompleteAppointment.Hide()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#End Region

    Protected Sub grdcontractorList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Grid View Custom Pager Events"

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)

            populateAssignedToContractorList(search)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                populateAssignedToContractorList(search)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Search Box Text Changed"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged
        Try
            Dim objPageSorterBo As PageSortBO = getPageSortBoViewState()
            objPageSorterBo.PageNumber = 1
            grdcontractorList.PageIndex = 0
            populateAssignedToContractorList(txtSearch.Text.Trim)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

    Private Sub populateAssignedToContractorList(Optional ByRef searchText As String = "")
        Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
        Dim objReportsBl As New ReportsBL

        Dim totalCount As Integer

        Dim resultDataSet As New DataSet

        setSearchTextViewState(searchText)

        totalCount = objReportsBl.getAssignedToContractorReport(resultDataSet, objPageSortBo, searchText)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        grdcontractorList.VirtualItemCount = totalCount
        grdcontractorList.DataSource = resultDataSet
        grdcontractorList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdcontractorList.PageCount Then
            setResultDataSetViewState(resultDataSet)
        End If
    End Sub

#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO As PageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)

        If IsNothing(pageSortBO) Then
            pageSortBO = New PageSortBO("DESC", "JOURNALID", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Search Text Set/Get/Remove"

    Protected Sub setSearchTextViewState(ByRef searchText As String)
        ViewState.Item(ViewStateConstants.Search) = searchText
    End Sub

    Protected Function getSearchTextViewState() As String
        Dim searchText As String = ViewState.Item(ViewStateConstants.Search)
        If IsNothing(searchText) Then
            searchText = String.Empty
        End If
        Return searchText
    End Function

    Protected Sub removeSearchTextViewState()
        ViewState.Remove(ViewStateConstants.Search)
    End Sub

#End Region

#End Region



#Region " View Job Sheet "

#Region "populate dropdowns"

    Private Sub populateHours()

        ddlHours.Items.Clear()
        For i As Integer = 1 To 12
            ddlHours.Items.Add(New ListItem((i).ToString("D2"), (i).ToString("D2")))
        Next i
    End Sub
    Private Sub populateMin()

        ddlMin.Items.Clear()
        For i As Integer = 0 To 59
            ddlMin.Items.Add(New ListItem((i).ToString("D2"), (i).ToString("D2")))
        Next i
    End Sub
    Private Sub populatePlannedStatus()
        Try
            Dim objJobSheetBL As New JobSheetBL()
            Dim dsPlannedStatus As New DataSet

            objJobSheetBL.getPlannedStatusLookUp(dsPlannedStatus)

            ddlStatus.Items.Clear()
            ddlStatus.DataSource = dsPlannedStatus
            ddlStatus.DataValueField = "id"
            ddlStatus.DataTextField = "val"
            ddlStatus.DataBind()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try

            If btnUpdate.Text = "Update" Then
                btnUpdate.Text = "Save"

                lblStatus.Visible = False
                ddlStatus.Visible = Not (lblStatus.Visible)

                If ((ddlStatus.Items.Count > 0) And (Not IsDBNull(ddlStatus.Items.FindByText("Assigned To Contractor"))) And
                    (Not IsNothing(ddlStatus.Items.FindByText("Assigned To Contractor"))) And
                    (Trim(ddlStatus.Items.FindByText("Assigned To Contractor").ToString()) <> "")) Then
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Assigned To Contractor"))
                End If

                mdlPopupJobSheetSummarySubContractorUpdate.Show()

            ElseIf btnUpdate.Text = "Save" Then

                If ddlStatus.SelectedItem.ToString().ToLower() = "Cancelled".ToLower() Then
                    pnlJobSheetSummary.Visible = True
                    AppointmentJobSheetStatusUpdate()
                    mdlPopupJobSheetSummarySubContractorUpdate.Hide()
                    populateAssignedToContractorList()
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Get job sheet summary data on the basis of selected JSN no
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Private Sub populateJobSheetSummary(ByRef jsn As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objJobSheetBL As JobSheetBL = New JobSheetBL()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)
        objJobSheetBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)

        SessionManager.setJobSheetSummaryDs(dsJobSheetSummary)

    End Sub
    Private Sub SetJobSheetSummary()
        Dim dsJobSheetSummary As New DataSet()
        Dim currentRow, totalRows As Integer

        dsJobSheetSummary = SessionManager.getJobSheetSummaryDs()

        currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            totalRows = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count
            lblTotalSheets.Text = "Page " + currentRow.ToString() + " of " + totalRows.ToString()

            If (currentRow = 1 And currentRow = totalRows) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = False
            ElseIf (currentRow = 1) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = True
            ElseIf (currentRow = totalRows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False
            ElseIf (currentRow > 1 And currentRow < totalRows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
            'If (currentRow = 1 And currentRow = totalRows) Then
            '    btnPrevious.Enabled = False
            '    btnNext.Enabled = False
            'ElseIf (currentRow > 1 And currentRow < totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = True
            'ElseIf (currentRow > 1 And currentRow < totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = True
            'ElseIf (currentRow + 1 = totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = False
            'End If
            resetVisibility()
            controlsVisibilityByType(dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentRow - 1).Item("Type").ToString())
            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentRow - 1)
                lblJSN.Text = .Item("AppointmentId").ToString().Trim()
                lblPlannedWorkComponent.Text = .Item("COMPONENTNAME")
                lblTrade.Text = .Item("Trade")
                lblStatus.Text = .Item("AppointmentStatus")
                lblContractor.Text = .Item("Contractor")
                lblContact.Text = .Item("Contact")
                txtWorksRequired.Text = .Item("WorkRequired")

                lblPONo.Text = .Item("PurchaseORDERID")
                lblOrderedBy.Text = .Item("ORDERBY")
                lblOrdered.Text = .Item("PODATE")

                lblBudget.Text = String.Format("{0:C}", .Item("BUDGET"))
                lblEstimate.Text = String.Format("{0:C}", .Item("Estimate"))
                lblNetCost.Text = String.Format("{0:C}", .Item("NetCost"))
                lblVat.Text = String.Format("{0:C}", .Item("Vat"))

                lblTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))
                lblOrderTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))

                lblScheme.Text = .Item("SCHEMENAME")
                lblBlock.Text = .Item("PJBlock")
                lblPostCode.Text = .Item("ClientPostCode")
                lblHouseNumber.Text = .Item("HOUSENUMBER") + ", " + .Item("HOUSEADDRESS1") + ", " + .Item("HOUSEADDRESS2") + ", " + .Item("HOUSEADDRESS3")
                'lblAddress1.Text = .Item("HOUSEADDRESS1")
                'lblAddress2.Text = .Item("HOUSEADDRESS2")
                'lblAddress3.Text = .Item("HOUSEADDRESS3")

                lblCustomer.Text = .Item("ClientName")
                lblTelephone.Text = .Item("ClientTel")
                lblMobile.Text = .Item("ClientMobile")
                lblEmail.Text = .Item("ClientEmail")

                'MA' ViewState(ViewStateConstants.CustomerId) = .Item("CustomerId")
            End With
            'If lblStatus.Text.ToUpper() = "complete".ToUpper() Then
            '    btnUpdate.Enabled = False
            'Else
            '    btnUpdate.Enabled = True
            'End If
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

#Region "Controls Visibility by type"
    Sub controlsVisibilityByType(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblBlock.Visible = False
            lblForCustomer.Visible = False
            lblCustomer.Visible = False
            lblTelephone.Visible = False
            lblMobile.Visible = False
            lblForEmail.Visible = False
            lblForHouseNumber.Visible = False
            lblHouseNumber.Visible = False
            lblPostcode.Visible = False            
            lblForPostcode.Visible = False
            lblForTelephone.Visible = False
            lblForMobile.Visible = False            
            lblEmail.Visible = False
            btn_CustDetailUpdate.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
#End Region

#Region "Reset Visibility by"
    Sub resetVisibility()
        lblForBlock.Visible = True
        lblForCustomer.Visible = True
        lblCustomer.Visible = True
        lblTelephone.Visible = True
        lblMobile.Visible = True
        lblEmail.Visible = True
        lblForHouseNumber.Visible = True
        lblHouseNumber.Visible = True
        lblPostCode.Visible = True
        lblForPostCode.Visible = True
        lblForTelephone.Visible = True
        lblForMobile.Visible = True
        lblEmail.Visible = True
        btn_CustDetailUpdate.Visible = True
        lblBlock.Visible = True
    End Sub
#End Region

    ''' <summary>
    ''' Reset job sheet controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub resetJobSheetControls()
        lblJSN.Text = String.Empty
        lblPlannedWorkComponent.Text = String.Empty
        lblTrade.Text = String.Empty
        lblStatus.Text = String.Empty
        lblStatus.Visible = True
        ddlStatus.Visible = Not (lblStatus.Visible)
        lblContractor.Text = String.Empty
        lblContact.Text = String.Empty
        txtWorksRequired.Text = String.Empty

        lblPONo.Text = String.Empty
        lblOrderedBy.Text = String.Empty
        lblOrdered.Text = String.Empty

        lblBudget.Text = String.Empty
        lblEstimate.Text = String.Empty
        lblVat.Text = String.Empty
        lblNetCost.Text = String.Empty

        lblScheme.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblHouseNumber.Text = String.Empty
        'lblAddress1.Text = String.Empty
        'lblAddress2.Text = String.Empty
        'lblAddress3.Text = String.Empty

        lblCustomer.Text = String.Empty
        lblTelephone.Text = String.Empty
        lblMobile.Text = String.Empty
        lblEmail.Text = String.Empty

        btnUpdate.Enabled = True
        btnUpdate.Text = "Update"

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()
    End Sub

    Private Function AppointmentJobSheetStatusUpdate() As Integer
        Try
            Dim objJobSheetBO As New JobSheetBO
            Dim objJobSheetBL As New JobSheetBL
            Dim completionDate, completionDateTime As String
            Dim OrigCreationDate As DateTime
            uiMessageHelper.IsError = False

            objJobSheetBO.AppointmentJSNId = CType(lblJSN.Text, Integer)
            objJobSheetBO.AppointmentStatus = CType(ddlStatus.SelectedValue, Integer)
            OrigCreationDate = CType(lblOrdered.Text, DateTime)
            If (Not IsDBNull(txtBoxFollowOnNotes.Text)) Then
                objJobSheetBO.AppointmentNotes = txtBoxFollowOnNotes.Text.Trim()
            End If

            objJobSheetBO.UserID = SessionManager.getUserEmployeeId()

            If (ddlStatus.SelectedItem.Text.ToString().ToLower() = "Completed".ToLower()) Then
                objJobSheetBO.OrderId = lblPONo.Text
            End If

            If (IsDBNull(txtCompleted.Text) Or Trim(txtCompleted.Text.ToString()) = "") Then
                objJobSheetBO.LastActionDateTime = DateTime.Now
            Else
                completionDate = txtCompleted.Text.Trim
                If (Convert.ToDateTime(completionDate) > Today.Date) Then
                    uiMessageHelper.message = UserMessageConstants.CompletedDate
                    uiMessageHelper.IsError = True
                    uiMessageHelper.setMessage(lblCompleteMessage, pnlCompleteMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                    uiMessageHelper.IsError = False
                    Return 0
                End If
                completionDateTime = txtCompleted.Text.Trim + " " + ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue
                Dim completionDTOB As DateTime = Convert.ToDateTime(completionDateTime)
                If (completionDTOB < OrigCreationDate) Then
                    uiMessageHelper.message = UserMessageConstants.CompletedDateOrderedDate
                    uiMessageHelper.IsError = True
                    uiMessageHelper.setMessage(lblCompleteMessage, pnlCompleteMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                    uiMessageHelper.IsError = False
                    Return 0
                End If
                objJobSheetBO.LastActionDateTime = Convert.ToDateTime(completionDateTime)
            End If

            objJobSheetBL.setAppointmentJobSheetStatusUpdate(objJobSheetBO)

            txtBoxFollowOnNotes.Text = String.Empty
            txtCompleted.Text = String.Empty

            If objJobSheetBO.IsFlagStatus Then
                uiMessageHelper.IsError = False
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = objJobSheetBO.UserMsg
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message


            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            End If
        End Try

        Return 1

    End Function

    Private Sub updatePlannedAppointmentStatus()


        Try
            If btnUpdate.Text = "Update" Then
                btnUpdate.Text = "Save"

                lblStatus.Visible = False
                ddlStatus.Visible = Not (lblStatus.Visible)

                ShowOrHideDetailsOnStatusBasis()

            ElseIf btnUpdate.Text = "Save" Then
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

    Private Sub ShowOrHideDetailsOnStatusBasis()
        Try
            btnUpdate.Enabled = True

            If ddlStatus.SelectedItem.ToString().ToLower() = "Assigned To Contractor".ToLower() Then
                btnUpdate.Enabled = False
            End If

            If ddlStatus.SelectedItem.ToString().ToLower() = "Completed".ToLower() Then
                pnlRepairDetail.Visible = True
                pnlJobSheetSummary.Visible = True
                mdlCompleteAppointment.Show()
                populateHours()
                populateMin()
            End If

            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        ShowOrHideDetailsOnStatusBasis()
    End Sub

    Protected Sub btnCompleteAppSave_Click(sender As Object, e As EventArgs) Handles btnCompleteAppSave.Click
        Try
            If (AppointmentJobSheetStatusUpdate() = 1) Then
                pnlJobSheetSummary.Visible = False
                pnlRepairDetail.Visible = False
                mdlPopupJobSheetSummarySubContractorUpdate.Hide()
                mdlCompleteAppointment.Hide()
                populateAssignedToContractorList()
            Else
                pnlJobSheetSummary.Visible = True
                pnlRepairDetail.Visible = True
                mdlPopupJobSheetSummarySubContractorUpdate.Show()
                mdlCompleteAppointment.Show()
                'populateAssignedToContractorList()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow - 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            SetJobSheetSummary()
            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow + 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            SetJobSheetSummary()
            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

End Class