﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class ViewConditionArrangedAppointments
    Inherits PageBase


#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then

                If (getvaluesFromQueryString()) Then
                    Me.getSetAllAppointmentData()
                Else
                    disableAllControls()
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"
    ''' <summary>
    ''' Button Update Customer Detail Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateCustomerDetails_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetails.Click
        Try
            updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Back Click Event"
    ''' <summary>
    ''' Button Back Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            Me.navigateToScheduleWorks()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Previous button clicked"
    ''' <summary>
    ''' Previous button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex - 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Me.setPreviousNextButtonStates(currentIndex)
            populateAppointmentInfo()
            populatePropertyInfo()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Next button clicked"
    ''' <summary>
    ''' Next button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex + 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Me.setPreviousNextButtonStates(currentIndex)
            populateAppointmentInfo()
            populatePropertyInfo()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Cancel appointment button clicked"
    ''' <summary>
    ''' Cancel appointment button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancelAppointment_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelAppointment.Click
        Try

            openCancelAppointmentPopup()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Click here button clicked"
    ''' <summary>
    ''' Click here button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnClickHere_click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnClickHere.Click
        Try

            navigateToScheduleWorks()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Confirm cancel appointments button clicked"
    ''' <summary>
    ''' Confirm cancel appointments button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirmCancel_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirmCancel.Click
        Try
            cancelAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Popup back button clicked"
    ''' <summary>
    ''' Popup back button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBackPopup_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackPopup.Click
        Try

            mdlPopUpCancelAppointment.Hide()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Update notes button clicked"
    ''' <summary>
    ''' Update notes button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateNotes_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateNotes.Click
        Try

            updateNotes()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Function"

#Region "Cancel appointment"
    ''' <summary>
    ''' Cancel appointment
    ''' </summary>
    ''' <remarks></remarks>
    Sub cancelAppointment()

        If (txtCancelReason.Text.Trim.Length = 0) Then
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)

        Else
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim pmo As Integer = Convert.ToInt32(lblPmo.Text)
            Dim isCancelled As Integer = 0
            isCancelled = objSchedulingBL.cancelAppointment(pmo, txtCancelReason.Text, "Condition To Be Arranged")

            If (isCancelled = 1) Then
                Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
                Dim _StartDate As Date
                Dim _AppointmentStartDateTime As String, _AppointmentEndDateTime As String, _propertyAddress As String
                Dim _operativeId As Integer

                For _Index = 0 To objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Rows.Count - 1 Step 1
                    With objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Rows(_Index)

                        _StartDate = .Item("StartDate").ToString()
                        If (_StartDate.Date = Today.Date) Then

                            _AppointmentStartDateTime = String.Format("{0} {1}", .Item("StartDate"), .Item("StartTime").ToString())
                            _AppointmentEndDateTime = String.Format("{0} {1}", .Item("EndDate"), .Item("EndTime").ToString())
                            _propertyAddress = lblAddress.Text
                            _operativeId = .Item("OperativeId").ToString()

                            GeneralHelper.pushNotificationPlanned("cancelled", Convert.ToDateTime(_AppointmentStartDateTime), Convert.ToDateTime(_AppointmentEndDateTime),
                                                                  _propertyAddress, _operativeId)
                        End If
                    End With
                Next


                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.AppointmentCancelledSuccessfully, False)
                pnlConfirmButton.Visible = False
                pnlCustomerMessage.Visible = True
                txtCancelReason.Attributes.Add("readonly", "readonly")
                Me.disableAllControls()
            Else
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.AppointmentCancelledFailed, True)
            End If

        End If

        mdlPopUpCancelAppointment.Show()

    End Sub

#End Region

#Region "Reset popup controls"

    Sub resetPopupControl()
        txtCancelReason.Text = String.Empty
        uiMessageHelper.resetMessage(lblErrorMessage, pnlErrorMessage)
    End Sub

#End Region

#Region "Open cancel appointement popup"
    ''' <summary>
    ''' Open cancel appointement popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub openCancelAppointmentPopup()

        Me.resetPopupControl()

        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
        Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
        Dim associatedAppointementsCount As Integer = 0
        Dim drAssociatedAppointements() As DataRow = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Select("Row <> " + Convert.ToString(currentIndex))

        Dim drAppointment As DataRow = Me.getCurrentIndexRowFromDataset(currentIndex)
        Dim jsn As String = drAppointment(ApplicationConstants.JsnSearchColumn)
        lblCancelledJsn.Text = jsn

        If (drAssociatedAppointements.Count > 0) Then
            Dim dtAssociatedAppointments As DataTable = drAssociatedAppointements.CopyToDataTable()
            grdAssociatedAppointments.DataSource = dtAssociatedAppointments
            grdAssociatedAppointments.DataBind()
            pnlAssociatedAppointments.Visible = True
            associatedAppointementsCount = drAssociatedAppointements.Count
        Else
            pnlAssociatedAppointments.Visible = False
        End If
        lblAssociatedAppointmentsCount.Text = Convert.ToString(associatedAppointementsCount)
        mdlPopUpCancelAppointment.Show()

    End Sub

#End Region

#Region "Get values from Query String"
    ''' <summary>
    ''' Get values from Query String
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getvaluesFromQueryString()
        Dim validData As Boolean = False

        If Not IsNothing(Request.QueryString(PathConstants.Pmo)) And Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
            Dim pmo As Integer
            Dim validPmo As Boolean = Integer.TryParse(Request.QueryString(PathConstants.Pmo), pmo)
            If (validPmo) Then
                ViewState.Add(ViewStateConstants.Pmo, pmo)
                ViewState.Add(ViewStateConstants.Jsn, Request.QueryString(PathConstants.Jsn))
                validData = True
            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidJobSheetAndPmoQueryString, True)
        End If
        Return validData

    End Function

#End Region

#Region "Disable all controls"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub disableAllControls()
        btnPrevious.Enabled = False
        btnNext.Enabled = False
        btnUpdateNotes.Enabled = False
        btnCancelAppointment.Enabled = False
        btnReScheduleAppointment.Enabled = False
        btnUpdateCustomerDetails.Enabled = False
        txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
        txtJobSheetNotes.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Pre Populate Values"
    ''' <summary>
    ''' get set all appointment data
    ''' </summary>
    ''' <remarks></remarks>
    Sub getSetAllAppointmentData()

        'Removing pre existing Arranged Appointments Dataset
        Dim objConditionSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objConditionSchedulingBo = SessionManager.getPlannedSchedulingBo()
        objConditionSchedulingBo.ArrangedTradesAppointmentsDt.Clear()

        Dim pmo As Integer = Integer.Parse(ViewState(ViewStateConstants.Pmo))
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataset As DataSet = New DataSet
        objSchedulingBL.getConditionArrangedAppointmentsDetail(resultDataset, pmo)

        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            disableAllControls()
        Else

            Dim totalDuration As Double = (Aggregate row As DataRow In resultDataset.Tables(0).AsEnumerable() _
                                           Into Sum(row.Field(Of Int32)("Duration")))
            If objConditionSchedulingBo.AppointmentInfoDt.Columns.Contains("TotalDuration") = False Then
                'add column of total duration in data table which we get when user clicked on appointment arranged arrow
                objConditionSchedulingBo.AppointmentInfoDt.Columns.Add("TotalDuration")
            End If
            'update the newly created column
            Dim updatedAppointmentRow() As Data.DataRow
            'get the row based on property id
            updatedAppointmentRow = objConditionSchedulingBo.AppointmentInfoDt.Select("PropertyId = " + ViewStateConstants.PropertyId + "")
            'set the total duration column in a row
            'updatedAppointmentRow(0)("TotalDuration") = totalDuration
            For Each row In updatedAppointmentRow
                row("TotalDuration") = totalDuration
            Next

            If Not IsNothing(resultDataset.Tables(0).Columns("TotalDuration")) Then
                resultDataset.Tables(0).Columns("TotalDuration").Expression = totalDuration
            End If

            'set all appointments of component's trade in session
            objConditionSchedulingBo.ArrangedTradesAppointmentsDt = resultDataset.Tables(0).Copy()
            objConditionSchedulingBo.PropertyInfoDt = resultDataset.Tables(1).Copy()
            'save the data in session 
            SessionManager.setPlannedSchedulingBo(objConditionSchedulingBo)
            Me.populateSummaryFromQueryString()
        End If

    End Sub

#End Region

#Region "Populate summary from query string"
    ''' <summary>
    ''' Populate summary from query string
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSummaryFromQueryString()
        Dim objConditionSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objConditionSchedulingBo = SessionManager.getPlannedSchedulingBo()
        Dim dtAppointments As DataTable = objConditionSchedulingBo.ArrangedTradesAppointmentsDt
        Dim drAppointment() As DataRow

        drAppointment = dtAppointments.Select("JSN = '" + ViewState(ViewStateConstants.Jsn).ToString() + "'")

        If (drAppointment.Count > 0) Then
            Dim currentIndex As Integer = drAppointment(0).Item(ApplicationConstants.JobsheetCurrentColumn)
            Dim totalJobSheets As Integer = dtAppointments.Rows.Count

            ViewState.Add(ViewStateConstants.CurrentIndex, currentIndex)
            ViewState.Add(ViewStateConstants.TotalJobsheets, totalJobSheets)
            Me.setPreviousNextButtonStates(currentIndex)
            Me.populateAppointmentInfo()
            Me.populatePropertyInfo()

        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidJsn, True)
            disableAllControls()
        End If

    End Sub

#End Region

#Region "Set previous next button states"
    Private Sub setPreviousNextButtonStates(ByVal updatedIndex As Integer)

        Dim currentIndex As Integer = updatedIndex
        Dim totalJobSheets As Integer = Convert.ToInt32(ViewState(ViewStateConstants.TotalJobsheets))

        lblSheetNumber.Text = currentIndex
        lblTotalSheets.Text = totalJobSheets

        'Previous Button state

        If (updatedIndex - 1 < 1) Then
            btnPrevious.Enabled = False
        Else
            btnPrevious.Enabled = True
        End If

        'Next Button state
        If (updatedIndex + 1 > totalJobSheets) Then
            btnNext.Enabled = False
        Else
            btnNext.Enabled = True
        End If

    End Sub
#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' Populate Appointment Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentInfo()

        Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
        Dim drAppointment As DataRow = Me.getCurrentIndexRowFromDataset(currentIndex)

        lblPmo.Text = drAppointment.Item(ApplicationConstants.PmoColumn)
        lblComponent.Text = drAppointment.Item(ApplicationConstants.ComponentColumn)
        lblTrade.Text = drAppointment.Item(ApplicationConstants.TradesColumn)

        lblJsn.Text = drAppointment.Item(ApplicationConstants.JsnColumn)
        lblStatus.Text = drAppointment.Item(ApplicationConstants.StatusColumn)
        lblOperative.Text = drAppointment.Item(ApplicationConstants.OperativeColumn)
        lblTime.Text = drAppointment.Item(ApplicationConstants.StartTimeColumn) + " - " + drAppointment.Item(ApplicationConstants.EndTimeColumn)

        If (IsDBNull(drAppointment.Item(ApplicationConstants.DurationsColumn))) Then
            lblDuration.Text = ApplicationConstants.NotAvailable
            lblDurationTotal.Text = ApplicationConstants.NotAvailable
        ElseIf (drAppointment.Item(ApplicationConstants.IsMiscAppointmentColumn) = 1) Then
            lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.DurationsColumn)))
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.TotalDurationColumn)))
        Else
            lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.DurationsColumn)))
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.TotalDurationColumn)))
        End If


        Dim startDateTime As DateTime = Convert.ToDateTime(drAppointment.Item(ApplicationConstants.StartDateColumn))
        Dim startDay As String = startDateTime.DayOfWeek.ToString()
        Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(startDateTime.ToString("dd")))
        Dim startMonth As String = startDateTime.ToString("MMMMMMMMMMMMM")
        Dim startYear As String = startDateTime.ToString("yyyy")

        Dim endDateTime As DateTime = Convert.ToDateTime(drAppointment.Item(ApplicationConstants.EndDateColumn))
        Dim endDay As String = endDateTime.DayOfWeek.ToString()
        Dim endDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(endDateTime.ToString("dd")))
        Dim endMonth As String = endDateTime.ToString("MMMMMMMMMMMMM")
        Dim endYear As String = endDateTime.ToString("yyyy")

        lblStartDate.Text = startDay + " " + startDate + " " + startMonth + " " + startYear
        lblEndDate.Text = endDay + " " + endDate + " " + endMonth + " " + endYear


        If (IsDBNull(drAppointment.Item(ApplicationConstants.CustomerNotesColumn))) Then
            txtCustAppointmentNotes.Text = String.Empty
        Else
            txtCustAppointmentNotes.Text = drAppointment.Item(ApplicationConstants.CustomerNotesColumn)
        End If

        If (IsDBNull(drAppointment.Item(ApplicationConstants.JobsheetNotesColumn))) Then
            txtJobSheetNotes.Text = String.Empty
        Else
            txtJobSheetNotes.Text = drAppointment.Item(ApplicationConstants.JobsheetNotesColumn)
        End If

        Dim propertyId As String = drAppointment.Item(ApplicationConstants.PropertyIdColumn)
        ViewState.Add(ViewStateConstants.PropertyId, propertyId)

    End Sub

#End Region

#Region "Populate Property Info"
    ''' <summary>
    ''' Populate Property Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyInfo()

        Dim propertyId As String = ViewState(ViewStateConstants.PropertyId)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

        If (String.IsNullOrEmpty(propertyId)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            Me.disableAllControls()
        Else
            Dim resultDt As DataTable = New DataTable()
            Dim objConditionSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            objConditionSchedulingBo = SessionManager.getPlannedSchedulingBo()
            resultDt = objConditionSchedulingBo.PropertyInfoDt


            'Property Information
            lblScheme.Text = resultDt.Rows(0)("SchemeName")
            lblAddress.Text = resultDt.Rows(0)("HOUSENUMBER") + " " + resultDt.Rows(0)("ADDRESS1") + " " + resultDt.Rows(0)("ADDRESS2")
            lblTowncity.Text = resultDt.Rows(0)("TOWNCITY")
            lblCounty.Text = resultDt.Rows(0)("COUNTY")
            lblPostcode.Text = resultDt.Rows(0)("POSTCODE")

            'Customer Information
            hdnCustomerId.Value = resultDt.Rows(0)("CustomerId")
            lblCustomerName.Text = resultDt.Rows(0)("TenantName")
            lblCustomerTelephone.Text = resultDt.Rows(0)("Telephone")
            lblCustomerMobile.Text = resultDt.Rows(0)("Mobile")
            lblCustomerEmail.Text = resultDt.Rows(0)("Email")

            If (hdnCustomerId.Value = "-1") Then
                btnUpdateCustomerDetails.Enabled = False
            Else
                btnUpdateCustomerDetails.Enabled = True
            End If

        End If

    End Sub

#End Region

#Region "Get current index row from dataset"
    ''' <summary>
    ''' Get current index row from dataset
    ''' </summary>
    ''' <param name="currentIndex"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getCurrentIndexRowFromDataset(ByVal currentIndex As Integer) As DataRow

        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        Dim dtAppointments As DataTable = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt
        Dim drAppointment() As DataRow
        drAppointment = dtAppointments.Select("Row = " + Convert.ToString(currentIndex))
        Return drAppointment(0)

    End Function

#End Region

#Region "Navigate to Schedule Condition Works"
    ''' <summary>
    ''' Navigate to Schedule Condition Works
    ''' </summary>
    ''' <remarks></remarks>
    Sub navigateToScheduleWorks()
        Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
    End Sub

#End Region

#Region "Update Customer Address"
    ''' <summary>
    ''' Update Customer Address
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateAddress()

        'Update Enable
        If btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges

        Else
            'Save Changes
            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                'If Not isError Then
                Dim objCustomerBL As CustomerBL = New CustomerBL()
                Dim objCustomerBO As CustomerBO = New CustomerBO()

                objCustomerBO.CustomerId = hdnCustomerId.Value
                objCustomerBO.Telephone = txtCustomerTelephone.Text
                objCustomerBO.Mobile = txtCustomerMobile.Text
                objCustomerBO.Email = txtCustomerEmail.Text

                Dim results As ValidationResults = Validation.Validate(objCustomerBO)
                If results.IsValid Then


                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

                    End If
                Else
                    Dim message = String.Empty
                    For Each result As ValidationResult In results
                        message += result.Message
                        Exit For
                    Next
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                End If
                'End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

            End If

        End If

    End Sub

#End Region

#Region "Update notes"
    ''' <summary>
    ''' Update notes
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateNotes()

        Dim objNotesBO As NotesBO = New NotesBO()
        objNotesBO.CustomerNotes = txtCustAppointmentNotes.Text
        objNotesBO.AppointmentNotes = txtJobSheetNotes.Text

        Dim results As ValidationResults = Validation.Validate(objNotesBO)

        If (results.IsValid) Then

            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim isSaved As Integer = 0
            Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
            Dim drAppointment As DataRow = Me.getCurrentIndexRowFromDataset(currentIndex)
            Dim appointmentId As Integer = drAppointment(ApplicationConstants.AppointmentIdColumn)

            isSaved = objSchedulingBL.updatePlannedAppointmentsNotes(txtCustAppointmentNotes.Text, txtJobSheetNotes.Text, appointmentId)

            If (isSaved = 1) Then

                drAppointment(ApplicationConstants.CustomerNotesColumn) = txtCustAppointmentNotes.Text
                drAppointment(ApplicationConstants.JobsheetNotesColumn) = txtJobSheetNotes.Text

                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NotesUpdatedSuccessfully, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemUpdatingNotes, True)
            End If

        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.MaxLengthExceed, True)

        End If


    End Sub

#End Region

#End Region

End Class