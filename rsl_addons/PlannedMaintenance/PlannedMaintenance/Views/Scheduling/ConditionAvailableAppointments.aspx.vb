﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports PL_BusinessLogic
Imports System.Globalization
Imports PL_BusinessObject

Public Class ConditionAvailableAppointments
    Inherits PageBase

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
                populateTradeDropdownList()
                populateDurationDropdownList()
                setInitialValues()
                setupControlProperties()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Find me operative button Event"
    ''' <summary>
    ''' Find me operative button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFindMeOperative_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFindOperative.Click

        Try
            findOperative()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Select button click event"
    ''' <summary>
    ''' Select button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim btnTempSelect As Button = DirectCast(sender, Button)
            Dim row As GridViewRow = DirectCast(btnTempSelect.NamingContainer, GridViewRow)

            showConditionWorkAppointmentSummary(row)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "btn Refresh List Click "
    Protected Sub btnRefreshList_Click(sender As Object, e As EventArgs) Handles btnRefreshList.Click
        Try
            Me.addMoreOperativesInList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate all trade"
    ''' <summary>
    ''' Populate all trades
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateTradeDropdownList()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim resultDataSet As DataSet = New DataSet()
        objReplacementBL.getAllTrades(resultDataSet, ApplicationConstants.AllSelectedValue)

        ddlTrade.DataSource = resultDataSet.Tables(0).DefaultView
        ddlTrade.DataValueField = "TradeId"
        ddlTrade.DataTextField = "Description"
        ddlTrade.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlTrade.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDurationDropdownList()
        Dim newListItem As ListItem
        ddlDuration.Items.Clear()

        For i = 1 To 40 Step 0.5

            If (i <= 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddlDuration.Items.Add(newListItem)
        Next

        newListItem = New ListItem("Please select", "-1")
        ddlDuration.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Find Operative"
    ''' <summary>
    ''' Find Operative
    ''' </summary>
    ''' <remarks></remarks>
    Sub findOperative()

        Dim objOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objOperativeSearchBO = Me.getMiscOperativeSearchBO()
        Dim message = String.Empty
        Dim isValid As Boolean = True

        Dim results As ValidationResults = Validation.Validate(objOperativeSearchBO)

        If Not results.IsValid Then

            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
            isValid = False

        ElseIf (Not String.IsNullOrEmpty(txtDate.Text)) Then

            If (Convert.ToDateTime(txtDate.Text) < DateTime.Today()) Then
                message += UserMessageConstants.SelectFutureDate + "<br />"
                uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                isValid = False
            End If

        End If

        If (isValid) Then
            Dim resultDataset As DataSet = New DataSet()
            Dim objConditionWorkAppointmentBO As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo()

            objConditionWorkAppointmentBO.TradeId = objOperativeSearchBO.OperativeTradeId
            objConditionWorkAppointmentBO.Duration = objOperativeSearchBO.Duration
            objConditionWorkAppointmentBO.StartDate = objOperativeSearchBO.StartDate

            SessionManager.setConditionWorksAppointmentBo(objConditionWorkAppointmentBO)

            resultDataset = Me.getAvailableOperatives()
            Dim tempAptDtBo As TempAppointmentDtBO = New TempAppointmentDtBO()
            Me.populateAvailableOperativesList(resultDataset, tempAptDtBo)
        Else
            pnlAvailableOperatives.Visible = False
        End If

    End Sub

#End Region

#Region "Get Misc Operative Search BO"
    ''' <summary>
    ''' Get Misc Operative Search BO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getMiscOperativeSearchBO()

        Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objMiscOperativeSearchBO.PropertyId = hdnSelectedPropertyId.Value
        objMiscOperativeSearchBO.PropertyAddress = lblPropertyAddress.Text
        objMiscOperativeSearchBO.Worksrequired = txtWorksRequired.Text
        objMiscOperativeSearchBO.OperativeTradeId = ddlTrade.SelectedValue
        objMiscOperativeSearchBO.Duration = ddlDuration.SelectedValue

        If Not String.IsNullOrEmpty(txtDate.Text) Then
            objMiscOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text)
        Else
            objMiscOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(DateTime.Now.ToShortDateString())
        End If

        Return objMiscOperativeSearchBO
    End Function

#End Region

#Region "Set Initial Values on First load"

    Private Sub setInitialValues()
        Dim conditionalAppointmentBO As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo
        lblPropertyAddress.Text = conditionalAppointmentBO.PropertyAddress
        hdnSelectedPropertyId.Value = conditionalAppointmentBO.PropertyId
        txtWorksRequired.Text = conditionalAppointmentBO.WorksRequired
    End Sub

#End Region

#Region "Show Condition Work Appointment Summary"

    Public Sub showConditionWorkAppointmentSummary(ByRef row As GridViewRow)
        Dim lblOperative As Label = DirectCast(row.FindControl("lblOperative"), Label)
        Dim lblOperativeId As Label = DirectCast(row.FindControl("lblOperativeId"), Label)
        Dim lblStartTime As Label = DirectCast(row.FindControl("lblStartTime"), Label)
        Dim lblStartDate As Label = DirectCast(row.FindControl("lblStartDate"), Label)
        Dim lblEndTime As Label = DirectCast(row.FindControl("lblEndTime"), Label)
        Dim lblEndDate As Label = DirectCast(row.FindControl("lblEndDate"), Label)

        Dim objConditionalAppointmentBO As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo()
        objConditionalAppointmentBO.PropertyId = hdnSelectedPropertyId.Value
        objConditionalAppointmentBO.TradeName = ddlTrade.SelectedItem.Text
        objConditionalAppointmentBO.TradeId = Convert.ToInt32(ddlTrade.SelectedItem.Value)
        objConditionalAppointmentBO.Duration = Convert.ToDouble(ddlDuration.SelectedValue)
        objConditionalAppointmentBO.StartTime = lblStartTime.Text
        objConditionalAppointmentBO.EndTime = lblEndTime.Text
        objConditionalAppointmentBO.StartDate = GeneralHelper.convertDateToUkDateFormat(Convert.ToDateTime(lblStartDate.Text).ToShortDateString()) + " " + lblStartTime.Text
        objConditionalAppointmentBO.EndDate = GeneralHelper.convertDateToUkDateFormat(Convert.ToDateTime(lblEndDate.Text).ToShortDateString()) + " " + lblEndTime.Text
        objConditionalAppointmentBO.OperativeName = lblOperative.Text
        objConditionalAppointmentBO.OperativeId = Convert.ToInt32(lblOperativeId.Text)
        objConditionalAppointmentBO.UserId = SessionManager.getUserEmployeeId()
        objConditionalAppointmentBO.WorksRequired = txtWorksRequired.Text
        objConditionalAppointmentBO.AppointmentNotes = txtWorksRequired.Text

        SessionManager.setConditionWorksAppointmentBo(objConditionalAppointmentBO)
        Response.Redirect(PathConstants.ConditionAppointmentSummary)
    End Sub

#End Region

#End Region

#Region "Available Operatives Functions"

#Region "get Available Operatives"
    ''' <summary>
    ''' this function 'll get the available operatives based on following: 
    ''' trade ids of component, property id, start date 
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getAvailableOperatives() As DataSet
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objConditionWorkAppointmentBO As New ConditionWorksAppointmentBO
        objConditionWorkAppointmentBO = SessionManager.getConditionWorksAppointmentBo()

        'this function 'll get the available operatives based on following: trade ids of component, property id, start date 
        schedulingBl.getAvailableOperatives(resultDataSet, objConditionWorkAppointmentBO.TradeId.ToString(), objConditionWorkAppointmentBO.PropertyId, objConditionWorkAppointmentBO.StartDate)
        If resultDataSet.Tables(0).Rows.Count() = 0 Then
            Throw New Exception(UserMessageConstants.OperativesNoFound)
        Else
            SessionManager.setAvailableOperativesDs(resultDataSet)
        End If
        Return resultDataSet
    End Function
#End Region

#Region "Populate available operative"
    ''' <summary>
    ''' Populate available operative
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAvailableOperativesList(ByVal availableOperativesDs As DataSet, ByVal tempAptDtBo As TempAppointmentDtBO)

        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objConditionWorkAppointmentBO As New ConditionWorksAppointmentBO
        objConditionWorkAppointmentBO = SessionManager.getConditionWorksAppointmentBo()

        'this function 'll process the component trades and 'll create the appointments against them
        schedulingBl.getAvailableConditionAppointments(availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), _
                                                                       availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), _
                                                                       availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), _
                                                                       tempAptDtBo, _
                                                                       objConditionWorkAppointmentBO.StartDate, _
                                                                       objConditionWorkAppointmentBO.Duration)

        If tempAptDtBo.dt.Rows.Count() > 0 Then
            pnlAvailableOperatives.Visible = True
            pnlRefreshButton.Visible = True
            'save appointments in session 
            orderAppointmentsByTimeAndOperative(tempAptDtBo.dt)
            SessionManager.setMiscAvailableOperativesAppointments(tempAptDtBo)
            grdAvailableMisc.DataSource = tempAptDtBo.dt
            grdAvailableMisc.DataBind()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.OperativesNoFound, True)
            pnlAvailableOperatives.Visible = False
            pnlRefreshButton.Visible = False
        End If
    End Sub

#End Region

#Region "add More Operatives In List"
    ''' <summary>
    ''' This function 'll add more operatives available appointment slots in the existing list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub addMoreOperativesInList()
        Dim resultDataset As DataSet = New DataSet()
        resultDataset = SessionManager.getAvailableOperativesDs()
        Dim tempAptDtBo As TempAppointmentDtBO = New TempAppointmentDtBO()
        tempAptDtBo = SessionManager.getMiscAvailableOperativesAppointments()
        Me.populateAvailableOperativesList(resultDataset, tempAptDtBo)
    End Sub
#End Region

#Region "Setup control properties"

    Sub setupControlProperties()
        txtDate.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Order Temp Appointments By Distance"
    ''' <summary>
    ''' 'this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
    ''' </summary>
    ''' <param name="tempAppointmentDt"></param>
    ''' <remarks></remarks>
    Public Sub orderAppointmentsByTimeAndOperative(ByRef tempAppointmentDt As DataTable)
        Dim appointments = tempAppointmentDt.AsEnumerable
        'Order The appointment, First By Distance then By Date and Then By Start Time.
        Dim appResult = (From app In appointments _
                         Select app _
                         Order By _
                         CType(app(TempAppointmentDtBO.startDateStringColName), Date) Ascending _
                         , app(TempAppointmentDtBO.operativeColName) Ascending)

        If appResult.Count() > 0 Then
            tempAppointmentDt = appResult.CopyToDataTable()
        End If

    End Sub

#End Region

#End Region

#Region "View State Function"

#Region "Get/Set/Remove availableMiscOperativesControl Visible Status View State"

    Private Function getavailableOperativesControlVisibleStatusViewState() As Boolean
        Dim visibleStatus As Boolean = False

        If Not IsNothing(ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus)) Then
            visibleStatus = CType(ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus), Boolean)
        End If
        Return visibleStatus
    End Function

    Private Sub setavailableOperativesControlVisibleStatusViewState(ByVal visbleStatus As Boolean)
        ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus) = visbleStatus
    End Sub

    Private Sub removeavailableOperativesControlVisibleStatusViewState()
        ViewState.Remove(ViewStateConstants.availableMiscOperativesControlVisibleStatus)
    End Sub

#End Region

#Region "Get/Set/Remove refresh button Visible Status View State"

    Private Function getrefreshButtonVisibleStatusViewState() As Boolean
        Dim visibleStatus As Boolean = False

        If Not IsNothing(ViewState(ViewStateConstants.refreshButtonVisibleStatus)) Then
            visibleStatus = CType(ViewState(ViewStateConstants.refreshButtonVisibleStatus), Boolean)
        End If
        Return visibleStatus
    End Function

    Private Sub setrefreshButtonVisibleStatusViewState(ByVal visbleStatus As Boolean)
        ViewState(ViewStateConstants.refreshButtonVisibleStatus) = visbleStatus
    End Sub

    Private Sub removerefreshButtonVisibleStatusViewState()
        ViewState.Remove(ViewStateConstants.refreshButtonVisibleStatus)
    End Sub

#End Region

#End Region

End Class