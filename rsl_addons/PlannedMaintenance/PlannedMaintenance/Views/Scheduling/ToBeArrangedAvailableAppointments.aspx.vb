﻿Imports PL_Utilities
Imports PL_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AjaxControlToolkit
Imports System.Drawing
Imports System.Net
Imports System.IO

Public Class ToBeArrangedAvailableAppointments
    Inherits PageBase

#Region "Properties"

    ''' <summary>
    ''' This object 'll retain the values of planned appointment bo
    ''' </summary>
    ''' <remarks></remarks>
    Private _objPlannedAppointmentBo As PlannedAppointmentBO
    Public Property objPlannedAppointmentBo() As PlannedAppointmentBO
        Get
            Return _objPlannedAppointmentBo
        End Get
        Set(ByVal value As PlannedAppointmentBO)
            _objPlannedAppointmentBo = value
        End Set
    End Property

    ''' <summary>
    ''' This key 'll retain the operatives, their leaves and their appointments
    ''' </summary>
    ''' <remarks></remarks>
    Private _availableOperatives As DataSet
    Public Property availableOperativesDs() As DataSet
        Get
            Return _availableOperatives
        End Get
        Set(ByVal value As DataSet)
            _availableOperatives = value
        End Set
    End Property

    ''' <summary>
    ''' this spearator 'll be used to bind the information of appointment with select button
    ''' </summary>
    ''' <remarks></remarks>
    Private appointmentInfoSeparater As String = ":::"

    Public ReadOnly Property GetScheduleWorkType As String
        Get
            Dim type As String = ""
            If Not IsNothing(Request.QueryString(PathConstants.ScheduleWorksType)) Then
                type = Request.QueryString(PathConstants.ScheduleWorksType)
            End If
            Return type
        End Get
    End Property

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event handler for page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'get values from session
            Me.getValuesFromSession()
            'fetch the details of property and component & bind it with grid
            Me.bindPropertyComponentDetail()

            If (Not IsPostBack) Then
                'Me.highLightCurrentPageMenuItems()
                Dim tempTradeIds As String
                Dim removedCompTradeIds As String
                Dim confirmedCompTradeIds As String
                If objPlannedAppointmentBo.ComponentId = -1 Then
                    confirmedCompTradeIds = Me.getMiscConfirmAppointmentsAndCompTradeIds()
                    removedCompTradeIds = Me.getRemovedCompTradeIds()
                    tempTradeIds = Me.getMiscComponentTrades(confirmedCompTradeIds, removedCompTradeIds)
                Else
                    confirmedCompTradeIds = Me.getConfirmAppointmentsAndCompTradeIds()
                    removedCompTradeIds = Me.getRemovedCompTradeIds()
                    tempTradeIds = Me.getComponentTrades(confirmedCompTradeIds, removedCompTradeIds)
                End If
                'tempTradeIds = "2,6"
                'if duration is change while scheduling it will be reflected in Data tables in this function
                setUpdatedComponentDuration()

                'if there does not exist any trade id's to be schedule then we don't need to fetch more operatives 
                Dim availableOperativesDs As DataSet = New DataSet()
                If Not String.IsNullOrEmpty(tempTradeIds) Then
                    availableOperativesDs = Me.getAvailableOperatives(tempTradeIds)
                End If
                Me.populateConfirmedTradesAndAppointments()
                Me.populateTempTradeAndAppointments(availableOperativesDs, tempTradeIds)
                Me.populateRemovedTrades()

            ElseIf (uiMessageHelper.IsError = False) Then
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for confirmed trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawConfirmedTradeAppointmentGrid()
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawTempTradeAppointmentGrid()
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for removed trade list (this is requirement of dynamic controls on post back)
                Me.reDrawRemovedTradeAppointmentGrid()

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "btn Select Operative Click"
    Protected Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'This a sample string that will be received on this event
            'Sample ---- operative Id:::Operative Name:::Start Date:::End Date:::Trade Id:::Trade Name:::Trade Duration:::Component Trade Id:::Component Name:::BlockId
            'Values ---- 468:::Jade Burrell:::25/02/2013:::25/02/2013:::1:::Plumber:::2:::4:::Bathroom:::1

            Dim parameters As String = String.Empty
            Dim btnSelectOperative As Button = DirectCast(sender, Button)
            parameters = btnSelectOperative.CommandArgument
            If objPlannedAppointmentBo.ComponentId = -1 Then
                Me.saveSelectedMiscAppointmentInfo(parameters)
                SessionManager.setToScheduling(True)
                Me.navigatetoMiscWorkJobsheet()
            Else
                'save the selected appointment information in session
                Me.saveSelectedAppointmentInfo(parameters)
                ''we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
                ''we are removing this from session (if old data exists)
                Me.clearTempAndCofirmBlockFromSession()
                SessionManager.setAppointmentSummarySource(PathConstants.ToBeArrangedAvailableAppointments)
                Me.redirectToAppointmentSummary()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnRefresh.CommandArgument)

            'get the start date which is selected against this trade
            Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedTradeAptBlockId.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedTradeAptBlockId.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                'Me.validateStartDate(startDate)
            End If

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, startDate)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Remove from Scheduling Click"
    ''' <summary>
    ''' This function handles the Remove from Scheduling button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRemoveFromScheduling_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnRemoveFromScheduling As Button = DirectCast(sender, Button)
            Dim selectedComponentTradeId As Integer = Integer.Parse(btnRemoveFromScheduling.CommandArgument)
            btnYes.CommandArgument = selectedComponentTradeId
            mdlPopupRemoveScheduling.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Yes Click"
    ''' <summary>
    ''' This function handles Yes button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnYes As Button = DirectCast(sender, Button)
            Dim selectedComponentTradeId As Integer = Integer.Parse(btnYes.CommandArgument)
            removeTadeFromScheduling(selectedComponentTradeId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn No Click"
    ''' <summary>
    ''' This function handles Yes button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            mdlPopupRemoveScheduling.Hide()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Confirm Appointments Click"
    Protected Sub btnConfirmAppointments_Click(sender As Object, e As EventArgs) Handles btnConfirmAppointments.Click

        Try
            'Dim schedulingBl As SchedulingBL = New SchedulingBL()
            'Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
            'Dim isAppointmentPushed As Boolean = False
            'Dim appointmentIds As String = String.Empty

            Me.scheduleConfirmedAppointments()

            'appointmentIds = (From res In objPlannedSchedulingBo.ConfirmAppointmentsDt Select res.Item("AppointmentId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())

            'appointmentIds.Split(",");

            'isAppointmentPushed = Me.pushAppointmentConfirmed(appointmentId, ApplicationConstants.NewAppointmentType)

            'If (Not isAppointmentPushed) Then
            '    uiMessageHelper.IsError = True
            '    uiMessageHelper.message = UserMessageConstants.PushNotificationFailed
            'End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Go Click"
    ''' <summary>
    ''' This event 'll refresh the appointments against the specific date. Appointment slots 'll be start from date entered.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnGo.CommandArgument)

            'get the start date which is selected against this trade
            Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedTradeAptBlockId.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedTradeAptBlockId.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                startAgain = True
                'Me.validateStartDate(startDate)
            End If

            'it 'll add five more operatives in the list of selected (trade/operative) block            
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, startDate, startAgain)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Rearrange Click"
    Protected Sub btnRearrange_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'This a sample string that will be received on this event
            'Sample ---- AppointmentId
            'Values ---- 468
            Dim appointementId As String = String.Empty
            Dim btnRearrange As Button = DirectCast(sender, Button)
            Dim blockInfo() As String = btnRearrange.CommandArgument.Split(",")

            appointementId = Integer.Parse(blockInfo(0))
            Dim compTradeId As Integer = Integer.Parse(blockInfo(1))

            'appointementId = Convert.ToInt32(btnRearrange.CommandArgument)
            Me.reArrangeAppointment(appointementId, compTradeId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Arrange Click"
    Protected Sub btnArrange_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim componentTradeId As String = String.Empty
            Dim btnArrange As Button = DirectCast(sender, Button)
            componentTradeId = Convert.ToInt32(btnArrange.CommandArgument)
            Me.arrangeAppointment(componentTradeId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image btn Edit Click"
    Protected Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnEdit As ImageButton = DirectCast(sender, ImageButton)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(selectedImgBtnEdit.CommandArgument)

            Dim row As GridViewRow = DirectCast(selectedImgBtnEdit.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim imgBtnEdit As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.EditImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)
            Dim lblDuration As Label = CType(pnlDuration.FindControl(ApplicationConstants.DurationLabelControlId + selectedTradeAptBlockId.ToString()), Label)
            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)
            imgBtnDone.Attributes.Add("style", "margin-top: -20px;")
            imgBtnEdit.Visible = False
            lblDuration.Visible = False
            ddlDuration.Visible = True
            imgBtnDone.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image btn Done Click"
    Protected Sub imgBtnDone_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnDone As ImageButton = DirectCast(sender, ImageButton)
            Dim blockInfo() As String = selectedImgBtnDone.CommandArgument.Split(",")

            Dim selectedTradeAptBlockId As Integer = Integer.Parse(blockInfo(0))
            Dim compTradeId As Integer = Integer.Parse(blockInfo(1))

            Dim row As GridViewRow = DirectCast(selectedImgBtnDone.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)

            updateTradeDuration(compTradeId, ddlDuration.SelectedValue)
            Me.reloadCurrentWebPage()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "bind Property Component Detail"
    ''' <summary>
    ''' Get the data table from session manager 
    ''' bind it with grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub bindPropertyComponentDetail()

        Dim objAppointmentScheduleBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objAppointmentScheduleBo = SessionManager.getPlannedSchedulingBo()
        If objAppointmentScheduleBo.AppointmentInfoDt.Rows.Count() = 0 Then
            'if no record exist show the error
            Throw New Exception(UserMessageConstants.ComponentTradeDoesNotExist)
        Else
            'bind the data with grid        
            Me.grdPropertyComponentDetail.DataSource = objAppointmentScheduleBo.AppointmentInfoDt
            Me.grdPropertyComponentDetail.DataBind()
        End If

    End Sub
#End Region

#Region "Get Confrim Appointments And Trade ids"
    ''' <summary>
    ''' This function 'll get the pending/confirm appointments from database based on property id and journalid (pmo)
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getConfirmAppointmentsAndCompTradeIds()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim componentTradeIds As String = String.Empty
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        'TODO: fetch it from session
        schedulingBl.getConfirmAppointments(resultDataSet, objPlannedAppointmentBo.PropertyId, objPlannedAppointmentBo.PMO)

        If resultDataSet.Tables(0).Rows.Count() > 0 Then
            Dim list = (From res In resultDataSet.Tables(0) Where Not IsDBNull(res.Item("ComponentTradeId")) Select res.Item("ComponentTradeId")).ToList()
            If list.Count > 0 Then
                componentTradeIds = (From res In resultDataSet.Tables(0) Where Not IsDBNull(res.Item("ComponentTradeId")) Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Else
                componentTradeIds = String.Empty
            End If

            'save confirm appointments in session
            objPlannedSchedulingBo.ConfirmAppointmentsDt = resultDataSet.Tables(0).Copy()
                SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            End If

            Return componentTradeIds
    End Function
#End Region

#Region "Get Misc Confrim Appointments And Trade ids"
    ''' <summary>
    ''' This function 'll get the pending/confirm appointments from database based on property id and journalid (pmo)
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getMiscConfirmAppointmentsAndCompTradeIds()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim componentTradeIds As String = String.Empty
        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        'TODO: fetch it from session
        schedulingBl.getMiscConfirmAppointments(resultDataSet, objMiscAppointmentBO.PropertyId, objPlannedAppointmentBo.PMO)

        If resultDataSet.Tables(0).Rows.Count() > 0 Then
            componentTradeIds = (From res In resultDataSet.Tables(0) Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            'save confirm appointments in session
            objPlannedSchedulingBo.ConfirmAppointmentsDt = resultDataSet.Tables(0).Copy()
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        End If

        Return componentTradeIds
    End Function
#End Region

#Region "get Components Trades"
    ''' <summary>
    ''' this 'll return the trades against component
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getComponentTrades(ByVal confirmCompTradeIds As String, ByVal removedCompTradeIds As String)
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim tempTradeIds As String = String.Empty
        'this object contains misc info related to appointment (e.g confirmed and temporary trades)
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        'check the session if does not already exist fetch from the databasee
        If objPlannedSchedulingBo.AllTradesDt.Rows.Count() = 0 Then
            schedulingBl.getComponentTrades(resultDataSet, objPlannedAppointmentBo.PMO)
            If resultDataSet.Tables(0).Rows.Count() = 0 And String.IsNullOrEmpty(removedCompTradeIds) Then
                'if no record exist show the error
                Throw New Exception(UserMessageConstants.ComponentTradeDoesNotExist)
            Else
                'this component has all trade id and component trade ids
                objPlannedSchedulingBo.AllTradesDt = resultDataSet.Tables(0).Copy()
            End If
        End If

        'if confirmed component trade ids are not empty it mean some appointment have been created against some trade
        If Not String.IsNullOrEmpty(confirmCompTradeIds) Then
            'get the confirm trade ids and update the data table for confirm trades
            'make an arry of pending component trade ids
            Dim confirmedCompTradeIdsArray() As String = confirmCompTradeIds.Split(",")
            'get trades which have appointment against them                            
            Dim confrimQuery = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Where confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")))
            'Create a table from the query.
            If confrimQuery.Count > 0 Then
                objPlannedSchedulingBo.ConfirmedTradesDt = confrimQuery.CopyToDataTable()
            End If
            'get the temporary trade ids 
            Dim tempQuery = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Where Not confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")) Select res)

            'Create a table from the query.
            If tempQuery.Count() > 0 Then
                objPlannedSchedulingBo.TempTradesDt = tempQuery.CopyToDataTable()
                tempTradeIds = (From res In objPlannedSchedulingBo.AllTradesDt Where Not confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")) Select res.Item("TradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Else
                objPlannedSchedulingBo.TempTradesDt.Clear()
                Me.showTopButtons()
            End If
        ElseIf objPlannedSchedulingBo.AllTradesDt.Rows.Count() > 0 Then

            tempTradeIds = (From res In objPlannedSchedulingBo.AllTradesDt Select res.Item("TradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Dim query = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Select res)
            'Create a table from the query.
            objPlannedSchedulingBo.TempTradesDt = query.CopyToDataTable()

        ElseIf objPlannedSchedulingBo.RemovedTradesDt.Rows.Count() > 0 Then

            Me.showBackButtons()

        End If
        'savee confirmd trade data table and temporary trade datatable in session
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)

        Return tempTradeIds
    End Function
#End Region
   
#Region "get Misc Components Trades"
    ''' <summary>
    ''' this 'll return the trades against component
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getMiscComponentTrades(ByVal confirmCompTradeIds As String, ByVal removedCompTradeIds As String)
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim tempTradeIds As String = String.Empty
        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        'this object contains misc info related to appointment (e.g confirmed and temporary trades)
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()

        'check the session if does not already exist fetch from the databasee
        If objPlannedSchedulingBo.AllTradesDt.Rows.Count() = 0 Then
            schedulingBl.getMiscComponentTrades(resultDataSet, objPlannedAppointmentBo.PMO)
            If resultDataSet.Tables(0).Rows.Count() = 0 And String.IsNullOrEmpty(removedCompTradeIds) Then
                'if no record exist show the error
                'Throw New Exception(UserMessageConstants.ComponentTradeDoesNotExist)
            Else
                'this component has all trade id and component trade ids
                objPlannedSchedulingBo.AllTradesDt = resultDataSet.Tables(0).Copy()
                If resultDataSet.Tables(0).Rows.Count() <> 0 Then
                    objMiscAppointmentBO.PropertyId = objPlannedSchedulingBo.AllTradesDt.Rows(0)("PropertyId")
                    objMiscAppointmentBO.PropertyAddress = objPlannedSchedulingBo.AllTradesDt.Rows(0)("PropertyAddress")
                    objMiscAppointmentBO.Type = objPlannedSchedulingBo.AllTradesDt.Rows(0)("Type")
                End If
            End If
        End If

        'if confirmed component trade ids are not empty it mean some appointment have been created against some trade
        If Not String.IsNullOrEmpty(confirmCompTradeIds) Then
            'get the confirm trade ids and update the data table for confirm trades
            'make an arry of pending component trade ids
            Dim confirmedCompTradeIdsArray() As String = confirmCompTradeIds.Split(",")
            'get trades which have appointment against them                            
            Dim confrimQuery = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Where confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")))
            'Create a table from the query.
            objPlannedSchedulingBo.ConfirmedTradesDt = confrimQuery.CopyToDataTable()
            'get the temporary trade ids 
            Dim tempQuery = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Where Not confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")) Select res)

            'Create a table from the query.
            If tempQuery.Count() > 0 Then
                objPlannedSchedulingBo.TempTradesDt = tempQuery.CopyToDataTable()
                tempTradeIds = (From res In objPlannedSchedulingBo.AllTradesDt Where Not confirmedCompTradeIdsArray.Contains(res.Item("ComponentTradeId")) Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Else
                objPlannedSchedulingBo.TempTradesDt.Clear()
                Me.showTopButtons()
            End If
        ElseIf objPlannedSchedulingBo.AllTradesDt.Rows.Count() > 0 Then

            tempTradeIds = (From res In objPlannedSchedulingBo.AllTradesDt Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Dim query = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Select res)
            'Create a table from the query.
            objPlannedSchedulingBo.TempTradesDt = query.CopyToDataTable()

        ElseIf objPlannedSchedulingBo.RemovedTradesDt.Rows.Count() > 0 Then

            Me.showBackButtons()

        End If
        'savee confirmd trade data table and temporary trade datatable in session
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        Me.setAppointmentInSession(objMiscAppointmentBO)
        Return tempTradeIds
    End Function
#End Region



#Region "get Available Operatives"
    ''' <summary>
    ''' this function 'll get the available operatives based on following: 
    ''' trade ids of component, property id, start date 
    ''' </summary>
    ''' <param name="tradeIds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getAvailableOperatives(ByVal tradeIds As String) As DataSet
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()

        'this function 'll get the available operatives based on following: trade ids of component, property id, start date 
        schedulingBl.getAvailableOperatives(resultDataSet, tradeIds, objPlannedAppointmentBo.PropertyId, Date.Now)
        If resultDataSet.Tables(0).Rows.Count() = 0 Then
            Throw New Exception(UserMessageConstants.OperativesNoFound)
        Else
            SessionManager.setAvailableOperativesDs(resultDataSet)
        End If
        Return resultDataSet
    End Function
#End Region

#Region "Region For Confirm Trades And Appointment"
#Region "populate Confirmed Trades And Appointments"
    ''' <summary>
    ''' This fucntion populates the confrimed trades' appointments in asp.net/html controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateConfirmedTradesAndAppointments()
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        SessionManager.removeConfirmTradeAppointmetBlockList()

        ''if confirmed trades exists then create a list 
        If objPlannedSchedulingBo.ConfirmedTradesDt.Rows.Count > 0 Then
            Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
            Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)

            'this function 'll loop through dataset for confirmed trades and it 'll return the list 
            'of trades and confirmed appointments against them
            If objPlannedAppointmentBo.ComponentId = -1 Then
                confirmTradeAptBoList = objSchedulingBl.getMiscConfirmedTradesAndAppointments(objPlannedSchedulingBo.ConfirmedTradesDt, objPlannedSchedulingBo.ConfirmAppointmentsDt)
            Else
                confirmTradeAptBoList = objSchedulingBl.getConfirmedTradesAndAppointments(objPlannedSchedulingBo.ConfirmedTradesDt, objPlannedSchedulingBo.ConfirmAppointmentsDt)
            End If
            'bind the confirmed trade with grid
            Me.bindConfirmTradesAppointments(confirmTradeAptBoList)
            SessionManager.setConfirmTradeAppointmetBlockList(confirmTradeAptBoList)
        End If
    End Sub
#End Region

#Region "bind Confirm Trades Appointments"
    Private Sub bindConfirmTradesAppointments(ByVal confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO))
        If confirmTradeAptBoList.Count() > 0 Then

            Dim counter As Integer = 0
            Dim uniqueBlockId As String = String.Empty
            'panel control 
            Dim panelContainer As Panel
            'bind this above data with grid view             
            For Each item As ConfirmTradeAppointmentBO In confirmTradeAptBoList
                uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString()
                'create container for every block of confirmed trade and appointment 
                panelContainer = Me.createContainer(uniqueBlockId)

                'Add the GridView control object dynamically into the parent control
                Dim confirmedTradeGrid As New GridView()
                panelContainer.Controls.Add(createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, item))

                'Add the GridView control object dynamically into the div control
                'Create object of appointment grid
                Dim confirmedAptGrid As New GridView()
                panelContainer.Controls.Add(createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, item))
                'Add this panel container to div page container
                pnlPageContainer.Controls.Add(panelContainer)
                counter = counter + 1
            Next
        End If

    End Sub
#End Region

#Region "Create Confirmed Trade Grids"
    ''' <summary>
    ''' This function 'll create the grid control containing component and trade information of of confirmed appointments
    ''' </summary>
    ''' <param name="confirmedTradeGrid"></param>
    ''' <param name="uniqueControlId"></param>
    ''' <param name="confirmTradeAptBo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedTradeGrids(ByVal confirmedTradeGrid As GridView, ByVal uniqueControlId As String, ByVal confirmTradeAptBo As ConfirmTradeAppointmentBO)
        'Create object of trade grid
        confirmedTradeGrid.ID = ApplicationConstants.ConfirmTradeGridControlId + uniqueControlId
        confirmedTradeGrid.EnableViewState = True
        confirmedTradeGrid.CssClass = "grid_temp_trade"
        'confirmedTradeGrid.RowStyle.BackColor = ColorTranslator.FromHtml("#E6E6E6")
        'confirmedTradeGrid.AlternatingRowStyle.BackColor = ColorTranslator.FromHtml("#FFFFFF")
        confirmedTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedTradeGrid.DataSource = confirmTradeAptBo.confirmTradeDtBo.dt
        confirmedTradeGrid.DataBind()

        Return confirmedTradeGrid
    End Function
#End Region

#Region "Create Confirmed Appointment Grids"
    ''' <summary>
    ''' This function creates the grid control containing confirmed appointments
    ''' </summary>
    ''' <param name="confirmedAptGrid"></param>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="confirmTradeAptBo"></param>
    ''' ''' <param name="hideRearrnageButton"> 'this value 'll always be false excetp when all appointments 'll have "Arranged" status</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedAppointmentGrids(ByVal confirmedAptGrid As GridView, ByVal uniqueBlockId As String, ByVal confirmTradeAptBo As ConfirmTradeAppointmentBO, Optional ByVal hideRearrnageButton As Boolean = False)

        confirmedAptGrid.ID = ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId
        confirmedAptGrid.EnableViewState = True
        confirmedAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        confirmedAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedAptGrid.DataSource = confirmTradeAptBo.confirmAppointmentDtBo.dt
        confirmedAptGrid.DataBind()
        'hide the unwanted columns from the grid
        confirmedAptGrid.HeaderRow.Cells(4).Visible = False
        Dim gvr As GridViewRow = confirmedAptGrid.Rows(0)
        gvr.Cells(4).Visible = False

        'this if condition body 'll always execute except when all appointments 'll be arranged, 
        'then this if condition body 'll not execute
        If hideRearrnageButton = False Then
            'Add the rearrange button 
            Dim btnRearrange As Button = New Button()
            btnRearrange.ID = "btnRearrange" + uniqueBlockId.ToString()
            btnRearrange.Text = "Rearrange"
            btnRearrange.CssClass = "row-button"
            btnRearrange.CommandArgument = confirmTradeAptBo.confirmAppointmentDtBo.dt.Rows(0).Item(ConfirmAppointmentDtBO.appointmentIdColName) + "," + Convert.ToString(confirmTradeAptBo.ComponentTradeId)
            AddHandler btnRearrange.Click, AddressOf btnRearrange_Click
            gvr.Cells(3).Controls.Add(btnRearrange)
        End If


        Return confirmedAptGrid
    End Function
#End Region

#Region "Rearrange Appointment"
    Private Sub reArrangeAppointment(ByVal appointmentId As Integer, ByVal compTradeId As Integer)
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        ' Changes for Misc appointment 

        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()

        Dim result As Boolean

        If objPlannedAppointmentBo.ComponentId = -1 Then
            result = schedulingBl.deleteMiscAppointment(appointmentId)
            'Save duration for current session of scheduling
            Dim confirmTradeDr As DataRow = objPlannedSchedulingBo.ConfirmedTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
            If Not IsNothing(confirmTradeDr) Then
                Dim duration As Double = confirmTradeDr.Item("Duration")
                Me.updateTradeDuration(compTradeId, duration)
            End If
        Else
            result = schedulingBl.deleteAppointment(appointmentId)
            'Save duration for current session of scheduling
            Dim confirmTradeDr As DataRow = objPlannedSchedulingBo.ConfirmedTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
            If Not IsNothing(confirmTradeDr) Then
                Dim duration As Double = confirmTradeDr.Item("Duration")
                Me.updateTradeDuration(compTradeId, duration)
            End If
        End If
        If result = True Then

            

            'Also delete this record from the confirmed trade data table
            Dim query = (From res In objPlannedSchedulingBo.ConfirmAppointmentsDt.AsEnumerable() Where Convert.ToInt32(res.Item("AppointmentId")) <> appointmentId Select res)
            If query.Count() > 0 Then
                objPlannedSchedulingBo.ConfirmAppointmentsDt = query.CopyToDataTable()
            Else
                objPlannedSchedulingBo.ConfirmAppointmentsDt.Clear()
            End If
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Me.reloadCurrentWebPage()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToRearrangeTheAppointment, True)
        End If
    End Sub
#End Region

#Region "reDraw Confirmed Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then redraw the controls of confirmed trades and appointment list''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawConfirmedTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for confirmed trades and appointment list
        Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)
        confirmTradeAptBoList = SessionManager.getConfirmTradeAppointmetBlockList()
        Me.bindConfirmTradesAppointments(confirmTradeAptBoList)
    End Sub
#End Region

#Region "Schedule Confirmed Appointments"
    ''' <summary>
    ''' This function 'll be called when user have created the appointment against all trades but those appointments are in pending status. 
    ''' This funciton 'll update isPending status to 0 and 'll also change the status from "To Be Arrnaged" ---> "Arrnaged"
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub scheduleConfirmedAppointments()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
        'get the appointment id's that have been confirmed.
        Dim appointmentIds As String = String.Empty
        If objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows.Count() > 0 Then
            appointmentIds = (From res In objPlannedSchedulingBo.ConfirmAppointmentsDt Select res.Item("AppointmentId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            Dim result As Boolean = False
            result = schedulingBl.scheduleConfirmedPlannedAppointments(appointmentIds, _
                                                                           objPlannedAppointmentBo.PropertyId, _
                                                                           objPlannedAppointmentBo.PMO, _
                                                                           objPlannedAppointmentBo.ComponentId, _
                                                                           SessionManager.getUserEmployeeId())

            If result = True Then
                '' Send Push Notifications to operators
                Dim _Index, _operativeId As Integer
                Dim _propertyAddress As String
                Dim _AppointmentStartDateTime As DateTime, _AppointmentEndDateTime As DateTime
                Dim _dtpropertyDetails As DataTable

                For _Index = 0 To objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows.Count - 1 Step 1
                    If (Not IsDBNull(objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows(_Index)("StartDate")) And
                       objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows(_Index)("StartDate").ToString() <> "") Then

                        _AppointmentStartDateTime = Convert.ToDateTime(objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows(_Index)("StartDate").ToString())

                        If (_AppointmentStartDateTime.Date = Today.Date) Then
                            _dtpropertyDetails = grdPropertyComponentDetail.DataSource()
                            _propertyAddress = IIf(IsDBNull(_dtpropertyDetails.Rows(0)("Address")), "", _dtpropertyDetails.Rows(0)("Address").ToString())
                            _propertyAddress = _propertyAddress + " " + IIf(IsDBNull(_dtpropertyDetails.Rows(0)("PostCode")), "", _dtpropertyDetails.Rows(0)("PostCode").ToString())

                            _AppointmentEndDateTime = Convert.ToDateTime(objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows(_Index)("EndDate").ToString())
                            _operativeId = objPlannedSchedulingBo.ConfirmAppointmentsDt.Rows(_Index)("OperativeId")

                            GeneralHelper.pushNotificationPlanned("New", _AppointmentStartDateTime, _AppointmentEndDateTime, _propertyAddress, _operativeId)
                        End If
                    End If
                Next

                Me.btnConfirmAppointments.Enabled = False
                Me.partialRedrawAndRebindTradeAppointmentConfirmedGrids()
                Me.partialRedrawAndRebindRemovedTradeGrids()
                SessionManager.removePlannedAppointmentBO()
                SessionManager.removePlannedSchedulingBo()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppoinemtsScheduledSuccessfully, False)
                redirectToAppointmentToBeArrangedList()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToScheduleTheAppointment, True)
            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToScheduleTheAppointment, True)
        End If
    End Sub
#End Region

#Region "Partial Redraw & Rebind Confrimed Trade Appointment Grids"
    ''' <summary>
    ''' This function 'll only redraw the confirmed grids. reset of the controls 'll be remain same. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub partialRedrawAndRebindTradeAppointmentConfirmedGrids()
        Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)
        confirmTradeAptBoList = SessionManager.getConfirmTradeAppointmetBlockList()

        Dim counter As Integer = 0
        Dim uniqueBlockId As String = String.Empty

        'bind this above data with grid view             
        For Each confirmTradeAptBo As ConfirmTradeAppointmentBO In confirmTradeAptBoList
            uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString()

            ' change the status to "Arranged"
            confirmTradeAptBo.confirmTradeDtBo.dt.Rows(0).Item(ConfirmTradeDtBO.statusColName) = ApplicationConstants.StatusArranged

            'Add the GridView control object dynamically into the parent control
            Dim confirmedTradeGrid As GridView = New GridView
            confirmedTradeGrid = CType(pnlPageContainer.FindControl(ApplicationConstants.ConfirmTradeGridControlId + uniqueBlockId), GridView)
            Me.createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, confirmTradeAptBo)

            'Add the GridView control object dynamically into the div control
            'find & Create object of appointment grid
            Dim confirmedAptGrid As New GridView
            confirmedAptGrid = CType(pnlPageContainer.FindControl(ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId), GridView)
            Me.createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, confirmTradeAptBo, True)

            counter = counter + 1
        Next
    End Sub
#End Region

#End Region

#Region "Region For Temporary trade And Appointments"

#Region "Populate temp Trade And Appointment"
    Private Sub populateTempTradeAndAppointments(ByVal availableOperativesDs As DataSet, ByVal tradeIds As String)
        Dim tempTradeExist As Boolean = False
        'check if the unconfirmed trades exists in table
        If Not (String.IsNullOrEmpty(tradeIds)) Then
            tempTradeExist = True
        Else
            Me.unHideBackButton()
        End If

        'if unconfirmed trades exists then create a list 
        If tempTradeExist = True Then            '
            Dim schedulingBl As SchedulingBL = New SchedulingBL()
            Dim tempTradeAptBoList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
            'this function 'll process the component trades and 'll create the appointments against them
            If objPlannedAppointmentBo.ComponentId = -1 Then
                tempTradeAptBoList = schedulingBl.getMiscTempTradeAppointments(objPlannedSchedulingBo.TempTradesDt, availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), Date.Now)
            Else
                tempTradeAptBoList = schedulingBl.getTempTradeAppointments(objPlannedSchedulingBo.TempTradesDt, availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), Date.Now)
            End If
            'save list in session 
            SessionManager.setTempTradeAppointmetBlockList(tempTradeAptBoList)
            Me.bindTempTradesAppointment(tempTradeAptBoList)
        Else
            SessionManager.removeTempTradeAppointmetBlockList()
        End If

    End Sub
#End Region

#Region "bind Temp Trade Appointments"
    ''' <summary>
    ''' This funciton 'll bind the data with temporary trade and appointments block
    ''' </summary>
    ''' <param name="tempTradeAptBoList"></param>
    ''' <remarks></remarks>
    Private Sub bindTempTradesAppointment(ByVal tempTradeAptBoList As List(Of TempTradeAppointmentBO))
        If IsNothing(tempTradeAptBoList) Then
            Me.unHideBackButton()
        Else
            'panel control 
            Dim panelContainer As Panel
            ' We are considering trade and its respective operative list as block so 
            'these varaibles 'll be used to recognize each block when some button/checkbox 'll be clicked e.g refresh list button
            Dim uniqueControlId As Integer = 0
            Dim uniqueControlIdString As String = uniqueControlId.ToString()

            'bind this above data with grid view             
            For Each item As TempTradeAppointmentBO In tempTradeAptBoList
                'create container for every block of temporary Trades and appointment 
                panelContainer = Me.createContainer(uniqueControlIdString)

                'Create object of Trade grid
                Dim tempTradeGrid As New GridView
                tempTradeGrid = Me.createTempTradesGrids(tempTradeGrid, uniqueControlIdString, item)
                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(tempTradeGrid)

                'Create object of appointment grid
                Dim tempAptGrid As New GridView()
                'Add the GridView control into the div control/panel
                tempAptGrid = Me.createTempAppointmentGrids(tempAptGrid, uniqueControlIdString, item)
                panelContainer.Controls.Add(tempAptGrid)

                'Add this panel container to div page container
                pnlPageContainer.Controls.Add(panelContainer)

                'this varaible 'll be used to recognize each control
                uniqueControlId = uniqueControlId + 1
                uniqueControlIdString = uniqueControlId.ToString()
            Next
        End If
    End Sub
#End Region

#Region "create Container"
    ''' <summary>
    ''' This function creates the html container for every block of trade and appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContainer(ByVal uniqueBlockId As String)
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + uniqueBlockId
        divContainer.CssClass = "trade-appointment-block"
        Return divContainer
    End Function
#End Region

#Region "create Temp Trade Grids"
    ''' <summary>
    ''' This function 'll create the temporary trade grid on the basis of unique id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createTempTradesGrids(ByVal tempTradeGrid As GridView, ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)

        tempTradeGrid.ID = ApplicationConstants.TempTradeGridControlId + uniqueBlockId
        tempTradeGrid.EnableViewState = True
        tempTradeGrid.CssClass = "grid_temp_trade"
        tempTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        tempTradeGrid.DataSource = item.tempTradeDtBo.dt
        tempTradeGrid.DataBind()

        'hide the last column from temporary trade grid(that are not required in grid view) because those columns 
        'were there in datatable and those columns have been automatically created, when we bound datatable with grid view
        'the columns of these data table 'll be used through sessoion
        tempTradeGrid.HeaderRow.Cells(5).Visible = False
        tempTradeGrid.HeaderRow.Cells(6).Visible = False
        tempTradeGrid.HeaderRow.Cells(7).Visible = False
        tempTradeGrid.HeaderRow.Cells(8).Visible = False
        tempTradeGrid.HeaderRow.Cells(9).Visible = False
        'run the loop on grid view rows to remove the cells (that are not required in grid view) from each row 
        'and also to add the select button in each row
        For Each gvr As GridViewRow In tempTradeGrid.Rows
            gvr.Cells(5).Visible = False
            gvr.Cells(6).Visible = False
            gvr.Cells(7).Visible = False
            gvr.Cells(8).Visible = False
            gvr.Cells(9).Visible = False
            gvr.Cells(2).Controls.Add(createDurationControls(uniqueBlockId, item))

        Next
        Return tempTradeGrid
    End Function
#End Region

#Region "Create Duration Controls"
    ''' <summary>
    ''' This function 'll create the duration control on the top of trade grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createDurationControls(ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)

        'Edit ImageButton
        Dim imgBtnEdit As ImageButton = New ImageButton()
        imgBtnEdit.ID = ApplicationConstants.EditImageButtonControlId + uniqueBlockId.ToString()
        imgBtnEdit.ImageUrl = "../../Images/edit.png"
        imgBtnEdit.CssClass = "temp-trade-content-left"
        imgBtnEdit.Visible = True
        imgBtnEdit.CommandArgument = uniqueBlockId
        AddHandler imgBtnEdit.Click, AddressOf imgBtnEdit_Click

        'Duration Label
        Dim lblDuration As Label = New Label()
        lblDuration.ID = ApplicationConstants.DurationLabelControlId + uniqueBlockId.ToString()
        lblDuration.Text = IIf(item.tempTradeDtBo.tradeDurationHrs > 1, Convert.ToString(item.tempTradeDtBo.tradeDurationHrs) + " hours", Convert.ToString(item.tempTradeDtBo.tradeDurationHrs) + " hour")
        lblDuration.Visible = True

        'Duration Dropdown
        Dim ddlDuration As DropDownList = New DropDownList()
        ddlDuration.ID = ApplicationConstants.DurationDropdownControlId + uniqueBlockId.ToString()
        ddlDuration.Visible = False
        populateDurationDropdownList(ddlDuration)
        ddlDuration.SelectedValue = item.tempTradeDtBo.tradeDurationHrs

        'Done ImageButton
        Dim imgBtnDone As ImageButton = New ImageButton()
        imgBtnDone.ID = ApplicationConstants.DoneImageButtonControlId + uniqueBlockId.ToString()
        imgBtnDone.ImageUrl = "../../Images/done.png"
        imgBtnDone.Visible = False
        imgBtnDone.CommandArgument = uniqueBlockId + "," + item.tempTradeDtBo.componentTradeId.ToString
        AddHandler imgBtnDone.Click, AddressOf imgBtnDone_Click

        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-trade-content"
        pnl.ID = ApplicationConstants.DurationPanelControlId + uniqueBlockId.ToString()
        pnl.Controls.Add(imgBtnEdit)
        pnl.Controls.Add(lblDuration)
        pnl.Controls.Add(ddlDuration)
        pnl.Controls.Add(imgBtnDone)
        Return pnl

    End Function
#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDurationDropdownList(ByRef ddl As DropDownList)
        Dim newListItem As ListItem
        ddl.Items.Clear()

        For i = 1 To 100

            If (i = 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddl.Items.Add(newListItem)
        Next

    End Sub

#End Region

#Region "create Temp Appointment Grids"
    ''' <summary>
    ''' This function 'll create the temporary appointment grid on the basis of unique block id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createTempAppointmentGrids(ByVal tempAptGrid As GridView, ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)
        Dim isEmptyGrid As Boolean = False

        tempAptGrid.ID = ApplicationConstants.TempAppointmentGridControlId + uniqueBlockId.ToString()
        tempAptGrid.ShowFooter = True
        tempAptGrid.EnableViewState = True
        tempAptGrid.RowStyle.BackColor = ColorTranslator.FromHtml("#E6E6E6")
        tempAptGrid.AlternatingRowStyle.BackColor = ColorTranslator.FromHtml("#FFFFFF")
        tempAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        tempAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        'Check if , are there any appointments or not .If not then display message
        If item.tempAppointmentDtBo.dt.Rows.Count = 0 Then
            Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoOperativesExistWithInTime)
            isEmptyGrid = True
        Else
            tempAptGrid.DataSource = item.tempAppointmentDtBo.dt
            tempAptGrid.DataBind()
            'add row span on footer and add back button, refresh list button and view calendar button
            tempAptGrid.FooterRow.Cells(0).Controls.Add(createAppointmentFooterControls(uniqueBlockId, item))
        End If

        'after adding the buttons add col span to footer so that buttons should appear according to design
        If tempAptGrid.Rows.Count > 0 Then
            Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
            For i As Integer = m - 1 To 1 Step -1
                tempAptGrid.FooterRow.Cells.RemoveAt(i)
            Next i
            tempAptGrid.FooterRow.Cells(0).ColumnSpan = 4 '4 is the number of visible columns to span.
        End If

        'create header of appointment grid and add start date calenar in it
        Dim tradAptGridHeaderCell As TableCell = tempAptGrid.HeaderRow.Cells(3)
        tradAptGridHeaderCell.Controls.Add(Me.createAppointmentHeaderControls(uniqueBlockId.ToString(), item.StartSelectedDate))

        'declare the variables that 'll store the information required to bind with select button in grid                
        Dim operativeId As String = String.Empty
        Dim operativeName As String = String.Empty
        Dim startDateString As String = String.Empty
        Dim endDateString As String = String.Empty
        Dim dtCounter As Integer = 0
        Dim completeAppointmentInfo As String = String.Empty

        'remove the header of unwanted columns
        tempAptGrid.HeaderRow.Cells(4).Visible = False
        tempAptGrid.HeaderRow.Cells(5).Visible = False
        tempAptGrid.HeaderRow.Cells(6).Visible = False
        tempAptGrid.HeaderRow.Cells(7).Visible = False
        tempAptGrid.HeaderRow.Cells(8).Visible = False
        'if grid is not emtpy then remove the cells and also create select button and set command argument with that
        If isEmptyGrid = False Then
            'run the loop on grid view rows to remove the cells from each row 
            'and also to add the select button in each row
            For Each gvr As GridViewRow In tempAptGrid.Rows
                gvr.Cells(4).Visible = False
                gvr.Cells(5).Visible = False
                gvr.Cells(6).Visible = False
                gvr.Cells(7).Visible = False
                gvr.Cells(8).Visible = False
                'get the information that should be set with select button                                             
                operativeId = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeIdColName)
                operativeName = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeColName)
                startDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.startDateStringColName)
                endDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.endDateStringColName)
                dtCounter += 1

                'save this operative and time slot information in a string, after that it 'll be bind with select button
                completeAppointmentInfo = operativeId.ToString() _
                    + Me.appointmentInfoSeparater + operativeName _
                    + Me.appointmentInfoSeparater + startDateString _
                    + Me.appointmentInfoSeparater + endDateString _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeName _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeDurationHrs.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentTradeId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentName _
                    + Me.appointmentInfoSeparater + uniqueBlockId

                'Add the select button 
                Dim btnSelectOperative As Button = New Button()
                btnSelectOperative.ID = "btnSelectOperative" + uniqueBlockId.ToString()
                btnSelectOperative.Text = "Select"
                btnSelectOperative.CssClass = "row-button"
                btnSelectOperative.CommandArgument = completeAppointmentInfo
                AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                gvr.Cells(3).Controls.Add(btnSelectOperative)
            Next
        End If
        Return tempAptGrid
    End Function
#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef tempTradeAptBo As TempTradeAppointmentBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        tempTradeAptBo.tempAppointmentDtBo.dt.Rows.Add(tempTradeAptBo.tempAppointmentDtBo.dt.NewRow())
        grid.DataSource = tempTradeAptBo.tempAppointmentDtBo.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        tempTradeAptBo.tempAppointmentDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Appointment Footer Controls"
    ''' <summary>
    ''' This function 'll create the footer buttons of appointment grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createAppointmentFooterControls(ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)
        Dim buttonsContainer As Panel = New Panel()
        Dim btnBack As Button = New Button()
        Dim btnRefreshList As Button = New Button()
        Dim btnViewCalendar As Button = New Button()
        Dim btnRemoveFromScheduling As Button = New Button()
        'set the back button 
        btnBack.Text = "< Back to Scheduling"

        Dim responseUrl As String = PathConstants.ScheduleWorks
        If GetScheduleWorkType <> "" Then
            responseUrl = responseUrl + "?" + PathConstants.ScheduleWorksType + "=" + GetScheduleWorkType
        End If

        If GetComponentId <> -1 Then
            If responseUrl.Contains("?") Then
                responseUrl = responseUrl + "&" + PathConstants.ComponentId + "=" + GetComponentId
            Else
                responseUrl = responseUrl + "?" + PathConstants.ComponentId + "=" + GetComponentId
            End If
        End If

        btnBack.PostBackUrl = responseUrl
        btnBack.CssClass = "trade-appointment-left-buttons"
        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = uniqueBlockId
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click
        'set the view calendar button
        btnViewCalendar.Text = "View Calendar"
        btnViewCalendar.Enabled = False
        'set the Remove From Scheduling
        btnRemoveFromScheduling.Text = " Remove from Scheduling"
        btnRemoveFromScheduling.CssClass = "trade-appointment-img-buttons"
        btnRemoveFromScheduling.CommandArgument = item.tempTradeDtBo.componentTradeId
        AddHandler btnRemoveFromScheduling.Click, AddressOf btnRemoveFromScheduling_Click

        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "trade-appointment-buttons"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnBack)
        buttonsContainer.Controls.Add(btnRemoveFromScheduling)
        buttonsContainer.Controls.Add(btnRefreshList)
        buttonsContainer.Controls.Add(btnViewCalendar)
        Return buttonsContainer
    End Function
#End Region

#Region "Create Appointment Header Controls"
    ''' <summary>
    ''' This function 'll create the date control on the top of appointment gird
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createAppointmentHeaderControls(ByVal uniqueBlockId As String, startDate As Date)
        'start label
        Dim lblStartDate As Label = New Label()
        lblStartDate.ID = ApplicationConstants.StartDateLabelControlId + uniqueBlockId
        lblStartDate.Text = "Start Date:"
        'date text box
        Dim txtDate As TextBox = New TextBox()
        txtDate.ID = ApplicationConstants.AppointmentStartDateControlId + uniqueBlockId
        txtDate.Text = startDate.Date
        'image button
        Dim imgCalDate As HtmlImage = New HtmlImage()
        imgCalDate.ID = ApplicationConstants.CalDateControlId + uniqueBlockId
        imgCalDate.Src = "../../Images/calendar.png"
        'calendar extendar
        Dim calExtStartDate As CalendarExtender = New CalendarExtender()
        calExtStartDate.PopupButtonID = imgCalDate.ID
        calExtStartDate.PopupPosition = CalendarPosition.Right
        calExtStartDate.TargetControlID = txtDate.ID
        calExtStartDate.ID = ApplicationConstants.CalExtDateControlId + uniqueBlockId
        calExtStartDate.Format = "dd/MM/yyyy"
        Dim currentDate As Date = Date.Now()
        calExtStartDate.StartDate = currentDate.ToString("dd/MM/yyyy")
        'create go button
        Dim btnGo As Button = New Button()
        btnGo.ID = ApplicationConstants.GoControlId + uniqueBlockId
        btnGo.Text = "Go"
        btnGo.CommandArgument = uniqueBlockId
        AddHandler btnGo.Click, AddressOf btnGo_Click
        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-appointment-header"
        pnl.Controls.Add(lblStartDate)
        pnl.Controls.Add(txtDate)
        pnl.Controls.Add(calExtStartDate)
        pnl.Controls.Add(imgCalDate)
        pnl.Controls.Add(btnGo)
        Return pnl
    End Function
#End Region

#Region "reDraw Temp Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then
    ''' redraw the controls for temporary trade and appointment list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawTempTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary trade and appointment list
        Dim tempTradeAptList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
        tempTradeAptList = SessionManager.getTempTradeAppointmetBlockList()
        Me.bindTempTradesAppointment(tempTradeAptList)
    End Sub
#End Region

#Region "refresh The Opertives List"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="selectedTradeAptBlockId"></param>    
    ''' <param name="startAgain">This variable 'll identify either appointment slots should be recreated from the selected start date or not</param>
    ''' <remarks></remarks>
    Private Sub refreshTheOpertivesList(ByVal selectedTradeAptBlockId As Integer, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
        Dim tempTradeAptList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
        Dim tempTradeAptBo As TempTradeAppointmentBO = New TempTradeAppointmentBO()
        tempTradeAptList = SessionManager.getTempTradeAppointmetBlockList()
        'get the specific object from the list of trade& appointments
        tempTradeAptBo = tempTradeAptList(selectedTradeAptBlockId)
        'increase the count of operatives by 
        tempTradeAptBo.DisplayCount = tempTradeAptBo.DisplayCount + 5
        'if user didn't change the start date 
        If startAgain = True Then
            tempTradeAptBo.StartSelectedDate = startDate
        End If
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        'get the available operatives, their leaves, appointments and selected start date
        availableOperativesDs = SessionManager.getAvailableOperativesDs()
        schedulingBl.addMoreTempTradeAppointments(availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), tempTradeAptBo, startDate, startAgain)
        'save in temporary trade appointment list
        tempTradeAptList(selectedTradeAptBlockId) = tempTradeAptBo
        ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        schedulingBl.orderAppointmentsByTimeAndOperative(tempTradeAptBo)

        ''get the instance of gridview control
        Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl("grdTempAppointments" + selectedTradeAptBlockId.ToString()), GridView)
        Me.createTempAppointmentGrids(grdTempAppointments, selectedTradeAptBlockId, tempTradeAptBo)
    End Sub
#End Region

#Region "save Selected Appointment Info"
    ''' <summary>
    ''' This function 'll save the selected appointment information in session
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedAppointmentInfo(ByVal parameters As String)
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        Dim separater As String() = {Me.appointmentInfoSeparater}
        Dim appointmentParameters() As String = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries)
        objPlannedAppointmentBo.OperativeId = Integer.Parse(appointmentParameters(0))
        objPlannedAppointmentBo.OperativeName = appointmentParameters(1)
        objPlannedAppointmentBo.StartDate = appointmentParameters(2)
        objPlannedAppointmentBo.EndDate = appointmentParameters(3)
        objPlannedAppointmentBo.TradeId = Integer.Parse(appointmentParameters(4))
        objPlannedAppointmentBo.TradeName = appointmentParameters(5)
        objPlannedAppointmentBo.Duration = appointmentParameters(6)
        objPlannedAppointmentBo.CompTradeId = Integer.Parse(appointmentParameters(7))
        objPlannedAppointmentBo.ComponentId = Integer.Parse(appointmentParameters(8))
        objPlannedAppointmentBo.ComponentName = appointmentParameters(9)
        objPlannedAppointmentBo.TotalDuration = (Aggregate row As DataRow In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() _
                        Into Sum(row.Field(Of Double)("Duration")))
        objPlannedAppointmentBo.Index = appointmentParameters(10) + 1
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
    End Sub
#End Region


#Region "save Selected Appointment Info"
    ''' <summary>
    ''' This function 'll save the selected appointment information in session
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedMiscAppointmentInfo(ByVal parameters As String)

        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        Dim separater As String() = {Me.appointmentInfoSeparater}
        Dim appointmentParameters() As String = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries)
        'objMiscAppointmentBO.PropertyId = hdnSelectedPropertyId.Value
        'objMiscAppointmentBO.PropertyAddress = hdnSelectedPropertyAddress.Value
        objMiscAppointmentBO.OperativeId = Integer.Parse(appointmentParameters(0))
        objMiscAppointmentBO.OperativeName = appointmentParameters(1)
        objMiscAppointmentBO.StartDate = appointmentParameters(2)
        objMiscAppointmentBO.StartTime = ""
        objMiscAppointmentBO.EndTime = ""
        objMiscAppointmentBO.EndDate = appointmentParameters(3)
        objMiscAppointmentBO.TradeId = Integer.Parse(appointmentParameters(4))
        objMiscAppointmentBO.TradeName = appointmentParameters(5)
        objMiscAppointmentBO.Duration = appointmentParameters(6)
        objMiscAppointmentBO.PMO = objPlannedAppointmentBo.PMO


        'If ddlType.SelectedItem.Text.ToUpper = "Adaptation".ToUpper Then
        '    objMiscAppointmentBO.AdaptationId = Convert.ToInt32(ddlAdaptation.SelectedValue)
        '    objMiscAppointmentBO.AdaptationName = If(ddlAdaptation.SelectedValue = "-1", "", ddlAdaptation.SelectedItem.Text)
        '    objMiscAppointmentBO.LocationId = Convert.ToInt32(ddlLocation.SelectedValue)
        '    objMiscAppointmentBO.LocationName = If(ddlLocation.SelectedValue = "-1", "", ddlLocation.SelectedItem.Text)

        'End If
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        schedulingBl.getMiscComponentDetails(resultDataSet, objMiscAppointmentBO.PMO, objMiscAppointmentBO.TradeId)


        objMiscAppointmentBO.AppointmentNotes = resultDataSet.Tables(0).Rows(0).Item("APPOINTMENTNOTES")
        objMiscAppointmentBO.CustomerNotes = resultDataSet.Tables(0).Rows(0).Item("CUSTOMERNOTES")
        objMiscAppointmentBO.AppointmentTypeId = resultDataSet.Tables(0).Rows(0).Item("Planned_Appointment_TypeId")
        objMiscAppointmentBO.AppointmentType = resultDataSet.Tables(0).Rows(0).Item("Planned_Appointment_Type")
        objMiscAppointmentBO.LocationName = resultDataSet.Tables(0).Rows(0).Item("LOCATION")
        objMiscAppointmentBO.AdaptationName = resultDataSet.Tables(0).Rows(0).Item("ADAPTATION")

        'objMiscAppointmentBO.TotalDuration = (Aggregate row As DataRow In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() _
        'Into Sum(row.Field(Of Double)("Duration")))
        objMiscAppointmentBO.Index = appointmentParameters(7) + 1
        Me.setAppointmentInSession(objMiscAppointmentBO)
        ' SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
    End Sub
#End Region


#Region "Set Appointment in Session"
    ''' <summary>
    ''' Set Appointment in Session
    ''' </summary>
    ''' <remarks></remarks>
    Sub setAppointmentInSession(ByVal objMiscAppointmentBO As MiscAppointmentBO)
        SessionManager.setMiscAppointmentBO(objMiscAppointmentBO)
    End Sub

#End Region


#Region "Navigate to MiscWork Jobsheet"
    ''' <summary>
    ''' Navigate to MiscWork Jobsheet
    ''' </summary>
    ''' <remarks></remarks>
    Sub navigatetoMiscWorkJobsheet()
        Response.Redirect(PathConstants.MiscJobSheet)
    End Sub
#End Region


#Region "Validate Start Date"
    ''' <summary>
    ''' This funciton 'll perform the validation of start date
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <remarks></remarks>
    Private Sub validateStartDate(ByVal startDate As Date)
        Dim result As Integer = DateTime.Compare(startDate.Date, Date.Today)
        If result < 0 Then
            Throw New Exception(UserMessageConstants.SelectedDateIsInPast)
        End If
    End Sub
#End Region

#Region "Redirect to Appointment Summary And Confirmation"
    Private Sub redirectToAppointmentSummary()

        Response.Redirect(PathConstants.AppointmentSummary, True)
    End Sub
#End Region

#Region "Redirect to Appointment To Be Arrange"
    Private Sub redirectToAppointmentToBeArrangedList()

        Response.Redirect(PathConstants.ScheduleWorks, True)
    End Sub
#End Region

#Region "Update trade duration"
    ''' <summary>
    ''' Update trade duration
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateTradeDuration(ByVal compTradeId As Integer, ByVal duration As Double)

        Dim objDurationDict As Dictionary(Of Integer, Double) = SessionManager.getPlannedDurationDict()
        If (objDurationDict.ContainsKey(compTradeId)) Then
            objDurationDict(compTradeId) = duration
        Else
            objDurationDict.Add(compTradeId, duration)
        End If

        SessionManager.setPlannedDurationDict(objDurationDict)

        'Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        'objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        'Dim dr As DataRow = objPlannedSchedulingBo.AllTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
        'dr.Item("Duration") = duration
        'objPlannedSchedulingBo.AllTradesDt.AcceptChanges()
        'SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)

    End Sub

#End Region


#End Region

#Region "Region for Removed trade"

#Region "Get Removed Component Trade ids"
    ''' <summary>
    ''' This function 'll get the removed Component Trade ids from database
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getRemovedCompTradeIds()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim componentTradeIds As String = String.Empty
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        'MISC CHANGES FOR GETTING REMOVED

        If objPlannedAppointmentBo.ComponentId = -1 Then
            schedulingBl.getRemovedMiscTradesFromScheduling(resultDataSet, objPlannedAppointmentBo.PMO)
        Else
            schedulingBl.getRemovedTradesFromScheduling(resultDataSet, objPlannedAppointmentBo.PMO)
        End If


        If resultDataSet.Tables(0).Rows.Count() > 0 Then
            componentTradeIds = (From res In resultDataSet.Tables(0) Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            'save removed Component Trade in session
            objPlannedSchedulingBo.RemovedTradesDt = resultDataSet.Tables(0).Copy()
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        End If

        Return componentTradeIds
    End Function
#End Region

#Region "Get Misc Removed Component Trade ids"
    ''' <summary>
    ''' This function 'll get the removed Component Trade ids from database
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getMiscRemovedCompTradeIds()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim componentTradeIds As String = String.Empty
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        'MISC CHANGES FOR GETTING REMOVED

        If objPlannedAppointmentBo.ComponentId = -1 Then
            schedulingBl.getRemovedMiscTradesFromScheduling(resultDataSet, objPlannedAppointmentBo.PMO)
        Else
            schedulingBl.getRemovedTradesFromScheduling(resultDataSet, objPlannedAppointmentBo.PMO)
        End If


        If resultDataSet.Tables(0).Rows.Count() > 0 Then
            componentTradeIds = (From res In resultDataSet.Tables(0) Select res.Item("ComponentTradeId")).Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
            'save removed Component Trade in session
            objPlannedSchedulingBo.RemovedTradesDt = resultDataSet.Tables(0).Copy()
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        End If

        Return componentTradeIds
    End Function
#End Region

#Region "Remove trade from Scheduling"

    Sub removeTadeFromScheduling(ByVal compTradeId As Integer)

        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
        Dim duration As Double = (From res In objPlannedSchedulingBo.TempTradesDt.AsEnumerable()
                            Where Convert.ToInt32(res.Item("ComponentTradeId")) = compTradeId
                            Select Convert.ToDouble(res.Item("Duration"))).First

        'MISC CHANGES FOR GETTING REMOVED
        Dim result
        If objPlannedAppointmentBo.ComponentId = -1 Then
            result = schedulingBl.removeMiscTradeFromScheduling(objPlannedAppointmentBo.PMO, compTradeId, duration)
        Else
            result = schedulingBl.removeTradeFromScheduling(objPlannedAppointmentBo.PMO, compTradeId, duration)
        End If
        If result = True Then
            'Remove CompTradeId from TempTradesDt
            Dim tempQuery = (From res In objPlannedSchedulingBo.TempTradesDt.AsEnumerable() Where Convert.ToInt32(res.Item("ComponentTradeId")) <> compTradeId)
            If tempQuery.Count() > 0 Then
                objPlannedSchedulingBo.TempTradesDt = tempQuery.CopyToDataTable()
            Else
                objPlannedSchedulingBo.TempTradesDt.Clear()
            End If

            'Remove CompTradeId from AllTradesDt
            Dim allTradeQuery = (From res In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() Where Convert.ToInt32(res.Item("ComponentTradeId")) <> compTradeId)
            If allTradeQuery.Count() > 0 Then
                objPlannedSchedulingBo.AllTradesDt = allTradeQuery.CopyToDataTable()
            Else
                objPlannedSchedulingBo.AllTradesDt.Clear()
            End If

            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Me.reloadCurrentWebPage()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToRemoveTrade, True)
        End If

    End Sub

#End Region

#Region "populate Removed Trades "
    ''' <summary>
    ''' This function populates the removed trades' in asp.net/html controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateRemovedTrades()
        'this object contains misc info related to appointment

        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        SessionManager.removeRemovedTradeAppointmetBlockList()

        ''if confirmed trades exists then create a list 
        If objPlannedSchedulingBo.RemovedTradesDt.Rows.Count > 0 Then
            Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
            Dim removedTradeAppointmentBOList As List(Of RemovedTradeAppointmentBO) = New List(Of RemovedTradeAppointmentBO)

            'this function 'll loop through dataset for removed trades and it 'll return the list 
            'of trades
            If objPlannedAppointmentBo.ComponentId = -1 Then
                removedTradeAppointmentBOList = objSchedulingBl.getMiscRemovedTradesAndAppointments(objPlannedSchedulingBo.RemovedTradesDt)
            Else
                removedTradeAppointmentBOList = objSchedulingBl.getRemovedTradesAndAppointments(objPlannedSchedulingBo.RemovedTradesDt)
            End If
            'bind the confirmed trade with grid
            Me.bindRemovedTradesAppointments(removedTradeAppointmentBOList)
            SessionManager.setRemovedTradeAppointmetBlockList(removedTradeAppointmentBOList)
        End If
    End Sub
#End Region

#Region "bind Removed Trades Appointments"
    Private Sub bindRemovedTradesAppointments(ByVal removedTradeAptBoList As List(Of RemovedTradeAppointmentBO))
        If removedTradeAptBoList.Count() > 0 Then

            Dim counter As Integer = 0
            Dim uniqueBlockId As String = String.Empty
            'panel control 
            Dim panelContainer As Panel
            'bind this above data with grid view             
            For Each item As RemovedTradeAppointmentBO In removedTradeAptBoList
                uniqueBlockId = ApplicationConstants.RemovedAppointementGridsIdConstant + counter.ToString()
                'create container for every block of confirmed trade and appointment 
                panelContainer = Me.createContainer(uniqueBlockId)

                'Add the GridView control object dynamically into the parent control
                Dim removedTradeGrid As New GridView()
                panelContainer.Controls.Add(createRemovedTradeGrids(removedTradeGrid, uniqueBlockId, item))

                'Add the GridView control object dynamically into the div control
                Dim removedButtonsPanel As New Panel()
                panelContainer.Controls.Add(createRemovedAppointmentGrids(removedButtonsPanel, uniqueBlockId, item))
                'Add this panel container to div page container
                pnlPageContainer.Controls.Add(panelContainer)
                counter = counter + 1
            Next
        End If

    End Sub
#End Region

#Region "Create Removed Trade Grids"
    ''' <summary>
    ''' This function 'll create the grid control containing component and trade information
    ''' </summary>
    ''' <param name="removedTradeGrid"></param>
    ''' <param name="uniqueControlId"></param>
    ''' <param name="removedTradeAptBo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createRemovedTradeGrids(ByVal removedTradeGrid As GridView, ByVal uniqueControlId As String, ByVal removedTradeAptBo As RemovedTradeAppointmentBO)
        'Create object of trade grid
        removedTradeGrid.ID = ApplicationConstants.RemovedTradeGridControlId + uniqueControlId
        removedTradeGrid.EnableViewState = True
        removedTradeGrid.CssClass = "grid_temp_trade"
        removedTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        removedTradeGrid.DataSource = removedTradeAptBo.removedTradeDtBO.dt
        removedTradeGrid.DataBind()
        If (removedTradeGrid.Rows.Count > 0) Then
            removedTradeGrid.Rows(0).Cells(4).ForeColor = Color.Red
        End If
        Return removedTradeGrid
    End Function
#End Region

#Region "Create Removed Appointment Grids"
    ''' <summary>
    ''' This function creates the grid control containing Arrange button
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="removedTradeAptBo"></param>
    ''' ''' <param name="hideArrangeButton"> 'this value 'll always be false except when all appointments 'll have "Arranged" status</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createRemovedAppointmentGrids(ByVal buttonsContainer As Panel, ByVal uniqueBlockId As String, ByVal removedTradeAptBo As RemovedTradeAppointmentBO, Optional ByVal hideArrangeButton As Boolean = False)

        buttonsContainer.ID = ApplicationConstants.RemovedButtonPanelControlId + uniqueBlockId
        buttonsContainer.CssClass = "trade-appointment-buttons"

        'this if condition body 'll always execute except when all appointments 'll be arranged, 
        'then this if condition body 'll not execute
        If hideArrangeButton = False Then
            'Add the arrange button 
            Dim btnArrange As Button = New Button()
            btnArrange.ID = "btnArrange" + uniqueBlockId.ToString()
            btnArrange.Text = "Arrange"
            btnArrange.CssClass = "row-button"
            btnArrange.CommandArgument = removedTradeAptBo.ComponentTradeId
            AddHandler btnArrange.Click, AddressOf btnArrange_Click
            buttonsContainer.Controls.Add(btnArrange)
        Else
            buttonsContainer.Visible = False
        End If

        Return buttonsContainer
    End Function
#End Region

#Region "Arrange Appointment"
    Private Sub arrangeAppointment(ByVal componentTradeId As Integer)
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
        'Remove an entry from Removed Trades Table
        Dim result = schedulingBl.deleteRemovedTrade(componentTradeId, objPlannedAppointmentBo.PMO)
        If result = True Then

            'Save duration for current session of scheduling
            Dim removeTradeDr As DataRow = objPlannedSchedulingBo.RemovedTradesDt.Select("ComponentTradeId=" + Convert.ToString(componentTradeId)).FirstOrDefault()
            If Not IsNothing(removeTradeDr) Then
                Dim duration As Double = removeTradeDr.Item("Duration")
                Me.updateTradeDuration(componentTradeId, duration)
            End If

            'Make an entry in AllTradesDt and TempTradesDt
            Dim resultDataSet As DataSet = New DataSet
            schedulingBl.getComponentTrades(resultDataSet, objPlannedAppointmentBo.PMO)
            'this component has all trade id and component trade ids
            objPlannedSchedulingBo.AllTradesDt = resultDataSet.Tables(0).Copy()

            'Remove an entry from RemovedTradesDt
            Dim removedQuery = (From res In objPlannedSchedulingBo.RemovedTradesDt.AsEnumerable() Where Convert.ToInt32(res.Item("ComponentTradeId")) <> componentTradeId Select res)
            If removedQuery.Count() > 0 Then
                objPlannedSchedulingBo.RemovedTradesDt = removedQuery.CopyToDataTable()
            Else
                objPlannedSchedulingBo.RemovedTradesDt.Clear()
            End If
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Me.reloadCurrentWebPage()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToRearrangeTheAppointment, True)
        End If
    End Sub
#End Region

#Region "reDraw Removed Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed data tables with grid, events with buttons then redraw the controls of removed trades list''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawRemovedTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for removed trades and appointment list
        Dim removedTradeAptBoList As List(Of RemovedTradeAppointmentBO) = New List(Of RemovedTradeAppointmentBO)
        removedTradeAptBoList = SessionManager.getRemovedTradeAppointmentBOList()
        Me.bindRemovedTradesAppointments(removedTradeAptBoList)
    End Sub
#End Region

#Region "Partial Redraw & Rebind Removed Trade Grids"
    ''' <summary>
    ''' This function 'll only redraw the removed grids. reset of the controls 'll be remain same. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub partialRedrawAndRebindRemovedTradeGrids()
        Dim removedTradeAptBoList As List(Of RemovedTradeAppointmentBO) = New List(Of RemovedTradeAppointmentBO)
        removedTradeAptBoList = SessionManager.getRemovedTradeAppointmentBOList()

        Dim counter As Integer = 0
        Dim uniqueBlockId As String = String.Empty

        'bind this above data with grid view             
        For Each removedTradeAptBo As RemovedTradeAppointmentBO In removedTradeAptBoList
            uniqueBlockId = ApplicationConstants.RemovedAppointementGridsIdConstant + counter.ToString()

            'Add the GridView control object dynamically into the parent control
            Dim removedTradeGrid As GridView = New GridView
            removedTradeGrid = CType(pnlPageContainer.FindControl(ApplicationConstants.RemovedTradeGridControlId + uniqueBlockId), GridView)
            Me.createRemovedTradeGrids(removedTradeGrid, uniqueBlockId, removedTradeAptBo)

            Dim removedAptPanel As New Panel
            removedAptPanel = CType(pnlPageContainer.FindControl(ApplicationConstants.RemovedButtonPanelControlId + uniqueBlockId), Panel)
            Me.createRemovedAppointmentGrids(removedAptPanel, uniqueBlockId, removedTradeAptBo, True)

            counter = counter + 1
        Next
    End Sub
#End Region

#End Region

#Region "unhide Back Button"
    Private Sub unHideBackButton()
    End Sub
#End Region

#Region "clear Panel Page Container"
    ''' <summary>
    ''' 'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
    ''' This control(divPageContainer) contains all the grids that 'll be displayed on page.
    ''' This function should be called on page load and on every event handler
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearPanelPageContainer()
        pnlPageContainer.Controls.Clear()
    End Sub
#End Region

#Region "get Values From Session"
    ''' <summary>
    ''' This function 'll be used to get the values from session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getValuesFromSession()
        objPlannedAppointmentBo = SessionManager.getPlannedAppointmentBO()
    End Sub
#End Region

#Region "clear Temp And Cofirm Block From Session"
    ''' <summary>
    ''' we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
    ''' we are removing this from session (if old data exists)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearTempAndCofirmBlockFromSession()
        'removing the temporary appointments from session
        SessionManager.removeTempTradeAppointmetBlockList()

    End Sub
#End Region

#Region "show Top Buttons"
    ''' <summary>
    ''' When all appointments 'll be arranged then the top two buttons 'll be displayed
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub showTopButtons()
        propDetailContainer.Visible = True
        pnlTopButtons.Visible = True
    End Sub
#End Region

#Region "show back Buttons"
    ''' <summary>
    ''' When all appointments 'll be arranged then the back button
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub showBackButtons()
        propDetailContainer.Visible = True
        pnlBackButton.Visible = True
    End Sub
#End Region

#Region "Redirect to Schedule Works"
    ''' <summary>
    ''' This function 'll redirect the user to schedule works page 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub redirectToScheduleWorks()
        Response.Redirect(PathConstants.ScheduleWorks)
    End Sub
#End Region

#Region "Reload the Current Web Page"
    ''' <summary>
    ''' This function 'll reload the current page. This is being used when user rearrange the scheduled appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reloadCurrentWebPage()
        Response.Redirect(Request.RawUrl)
    End Sub
#End Region

#Region "Set updated component duration"
    ''' <summary>
    ''' This function updates duration in data tables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setUpdatedComponentDuration()

        Dim objDurationDict As Dictionary(Of Integer, Double) = SessionManager.getPlannedDurationDict()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        For Each item As KeyValuePair(Of Integer, Double) In objDurationDict
            Dim compTradeId As Integer = item.Key
            Dim duration As Double = item.Value

            If objPlannedSchedulingBo.AllTradesDt.Rows.Count > 0 Then
                Dim allTradeDr As DataRow = objPlannedSchedulingBo.AllTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
                If Not IsNothing(allTradeDr) Then
                    allTradeDr.Item("Duration") = duration
                    objPlannedSchedulingBo.AllTradesDt.AcceptChanges()
                End If
            End If

            If objPlannedSchedulingBo.TempTradesDt.Rows.Count > 0 Then
                Dim tempTradeDr As DataRow = objPlannedSchedulingBo.TempTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
                If Not IsNothing(tempTradeDr) Then
                    tempTradeDr.Item("Duration") = duration
                    objPlannedSchedulingBo.TempTradesDt.AcceptChanges()
                End If
            End If

            If objPlannedSchedulingBo.RemovedTradesDt.Rows.Count > 0 Then
                Dim removeTradeDr As DataRow = objPlannedSchedulingBo.RemovedTradesDt.Select("ComponentTradeId=" + Convert.ToString(compTradeId)).FirstOrDefault()
                If Not IsNothing(removeTradeDr) Then
                    removeTradeDr.Item("Duration") = duration
                    objPlannedSchedulingBo.RemovedTradesDt.AcceptChanges()
                End If
            End If

        Next

        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)

    End Sub
#End Region

    '#Region "Send Push Notifications"

    '    Public Function pushNotificationAppliance(ByVal appointmentType As String, ByVal appointmentDate As DateTime, ByVal appointmentStartTime As DateTime,
    '                                              ByVal appointmentEndTime As DateTime, ByVal propertyAddress As String, ByVal operatorId As Integer) As Boolean
    '        Try
    '            Dim appointmentTypeDesc As String = String.Empty
    '            Dim appointmentMessage As String = String.Empty

    '            If (appointmentType.ToLower() = "new") Then
    '                appointmentTypeDesc = "New Appliance Service Scheduled"
    '            ElseIf (appointmentType.ToLower() = "existing") Then
    '                appointmentTypeDesc = "Appliance Service Rearranged"
    '            End If

    '            appointmentMessage += "BHG Property Manager" + "\n\n"
    '            appointmentMessage += appointmentTypeDesc + "\n\n"
    '            appointmentMessage += appointmentDate.ToString("d MMM yyyy") + " "
    '            appointmentMessage += appointmentStartTime.ToString("HH:mm") + " to "
    '            appointmentMessage += appointmentEndTime.ToString("HH:mm") + "\n"
    '            appointmentMessage += propertyAddress

    '            appointmentMessage = Server.UrlEncode(appointmentMessage)

    '            Dim URL As String = String.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId)
    '            Dim request As WebRequest = Net.WebRequest.Create(URL)
    '            request.Credentials = CredentialCache.DefaultCredentials
    '            Dim response As WebResponse = request.GetResponse()
    '            Dim dataStream As Stream = response.GetResponseStream()
    '            Dim reader As New StreamReader(dataStream)
    '            Dim responseFromServer As String = reader.ReadToEnd()
    '            reader.Close()
    '            response.Close()

    '            If (responseFromServer.ToString.Equals("true")) Then
    '                Return True
    '            End If

    '            Return False

    '        Catch ex As Exception
    '            Return False
    '        End Try

    '    End Function

    '#End Region

#End Region

End Class