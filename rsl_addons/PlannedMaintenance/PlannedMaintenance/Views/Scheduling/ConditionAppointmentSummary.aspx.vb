﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic
Imports PL_BusinessObject

Public Class ConditionAppointmentSummary
    Inherits PageBase

#Region "Events"

#Region "Page Load"

    ''' <summary>
    ''' page load function
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.populateControls()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Schedule Appointment Click Event"
    ''' <summary>
    ''' Button Schedule Appointment Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnScheduleAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleAppointment.Click
        Try
            Me.scheduleAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Add More Operatives Click"

    Protected Sub btnAddMoreOperatives_Click(sender As Object, e As EventArgs) Handles btnAddMoreOperatives.Click
        Try
            navigateToScheduleconditionWorks()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Red Cross Button Click"

    Protected Sub imgBtnClosePopup_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgBtnClosePopup.Click
        Try
            navigateToScheduleconditionWorks()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Confirm Appointment Click"

    Protected Sub btnConfirmAppointment_Click(sender As Object, e As EventArgs) Handles btnConfirmAppointment.Click
        Try
            confirmAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Controls"
    ''' <summary>
    ''' This function 'll populate the user control from the data in session and the data from database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateControls()
        'populate the property related controls 
        Dim objConditionalAppointmentBo As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo

        Dim propertyId As String = objConditionalAppointmentBo.PropertyId
        Dim tenancyId As Integer = Me.appointmentSummaryControl.populatePropertyInfo(propertyId)
        objConditionalAppointmentBo.TenancyId = tenancyId
        txtCustAppointmentNotes.Text = String.Empty
        txtJobSheetNotes.Text = objConditionalAppointmentBo.WorksRequired
        SessionManager.setConditionWorksAppointmentBo(objConditionalAppointmentBo)
        'populate the appointment related controls
        Me.populateAppointmentInfo()
    End Sub
#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' This function 'll pouplate the object of appointment bo from planned appointment bo and then it passess appointment bo
    ''' to user control, to pouplate control
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateAppointmentInfo()

        Dim objConditionalAppointmentBo As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo
        lblSheetNumberTitle.Text = 1
        lblTotalSheets.Text = 1
        Dim objAppointmentBo As AppointmentBO = New AppointmentBO()
        objAppointmentBo.Pmo = objConditionalAppointmentBo.JournalId.ToString()
        objAppointmentBo.OperativeName = objConditionalAppointmentBo.OperativeName
        objAppointmentBo.ComponentName = objConditionalAppointmentBo.ComponentName
        objAppointmentBo.TradeName = objConditionalAppointmentBo.TradeName
        objAppointmentBo.Duration = objConditionalAppointmentBo.Duration.ToString() + " hour(s)"
        objAppointmentBo.TotalDuration = objConditionalAppointmentBo.Duration.ToString() + " hour(s)"
        'objAppointmentBo.Time = GeneralHelper.get24HourTimeFormatFromDate(objPlannedAppointmentBO.StartDate) + "-" + GeneralHelper.get24HourTimeFormatFromDate(objPlannedAppointmentBO.EndDate)
        objAppointmentBo.StartDate = GeneralHelper.get24HourTimeFormatFromDate(objConditionalAppointmentBo.StartDate) + " " + GeneralHelper.getDateWithWeekdayFormat(objConditionalAppointmentBo.StartDate)
        objAppointmentBo.EndDate = GeneralHelper.get24HourTimeFormatFromDate(objConditionalAppointmentBo.EndDate) + " " + GeneralHelper.getDateWithWeekdayFormat(objConditionalAppointmentBo.EndDate)
        Me.appointmentSummaryControl.populateAppointmentInfo(objAppointmentBo)

    End Sub
#End Region

#Region "Schedule Appointment"
    ''' <summary>
    ''' Schedule Appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub scheduleAppointment()

        Dim objConditionWorksAppointmentBO As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim isSaved As String = String.Empty
        Dim appointmentId As Integer = Convert.ToInt32(ApplicationConstants.DefaultValue)

        If (IsNothing(objConditionWorksAppointmentBO)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            btnScheduleAppointment.Enabled = False
        Else
            objConditionWorksAppointmentBO.CustomerNotes = txtCustAppointmentNotes.Text
            objConditionWorksAppointmentBO.AppointmentNotes = txtJobSheetNotes.Text
            objConditionWorksAppointmentBO.StartTime = objConditionWorksAppointmentBO.StartDate.ToString("hh:mm tt")
            objConditionWorksAppointmentBO.EndTime = objConditionWorksAppointmentBO.EndDate.ToString("hh:mm tt")


            isSaved = objSchedulingBL.scheduleConditionWorksAppointment(objConditionWorksAppointmentBO, appointmentId)

            If (isSaved.Equals(ApplicationConstants.NotSaved)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
            Else
                'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
                mdlConfirmAppointment.Show()
                btnScheduleAppointment.Enabled = False
                Me.appointmentSummaryControl.disableUpdateCustomerButton()
                txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
                txtJobSheetNotes.Attributes.Add("readonly", "readonly")
                objConditionWorksAppointmentBO.AppointmentId = appointmentId
                objConditionWorksAppointmentBO.IsPending = True
                SessionManager.setConditionWorksAppointmentBo(objConditionWorksAppointmentBO)
            End If

        End If


    End Sub

#End Region

#Region "Confirm Appointment"

    Private Sub confirmAppointment()
        Dim objConditionWorksAppointmentBO As ConditionWorksAppointmentBO = SessionManager.getConditionWorksAppointmentBo
        Dim objSchedulingBL As New SchedulingBL
        Dim _StartDate As Date
        Dim _AppointmentStartDateTime As String, _AppointmentEndDateTime As String, _propertyAddress As String
        Dim _operativeId As Integer

        Dim issaved As Boolean = objSchedulingBL.ConfirmedCoditionAppointments(objConditionWorksAppointmentBO.JournalId, SessionManager.getPlannedMaintenanceUserId)

        If issaved Then
            _StartDate = objConditionWorksAppointmentBO.StartDate

            If (_StartDate.ToString("dd/MM/yyyy") = Today.ToString("dd/MM/yyyy")) Then
                _AppointmentStartDateTime = objConditionWorksAppointmentBO.StartDate.ToString("dd/MM/yyyy") + " " + objConditionWorksAppointmentBO.StartTime.ToString()
                _AppointmentEndDateTime = objConditionWorksAppointmentBO.EndDate.ToString("dd/MM/yyyy") + " " + objConditionWorksAppointmentBO.EndTime.ToString()
                _propertyAddress = objConditionWorksAppointmentBO.PropertyAddress
                _operativeId = objConditionWorksAppointmentBO.OperativeId

                GeneralHelper.pushNotificationPlanned("conditionrating", Convert.ToDateTime(_AppointmentStartDateTime), Convert.ToDateTime(_AppointmentEndDateTime),
                                                      _propertyAddress, _operativeId)
            End If

            Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
        End If

    End Sub

#End Region

#Region "Navigate to Schedule Condition Works"

    Private Sub navigateToScheduleconditionWorks()
        Response.Redirect(PathConstants.ScheduleConditionWorks)
    End Sub

#End Region

#End Region

End Class