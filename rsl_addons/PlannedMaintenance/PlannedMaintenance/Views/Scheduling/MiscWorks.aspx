﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="MiscWorks.aspx.vb" Inherits="PlannedMaintenance.MiscWorks" %>

<%@ Register TagName="AssignToContractor" TagPrefix="uc1" Src="~/Controls/Scheduling/AssignToContractor.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="c3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
 

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitAutoCompl();

        });

        
        

        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoCompl();
        }

        function InitAutoCompl() {
            var autocompleter = $('#<%= txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "MiscWorks.aspx/getPropertySearchResult",
                        data: "{'searchText':'" + $('#<%= txtSearch.ClientID %>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                var imgpath = "";
                                if(item.split('/')[2] === "Property")
                                {
                                    imgpath = "../../Images/icon_property.png"
                                }
                                else if(item.split('/')[2] === "Scheme")
                                {
                                    imgpath = "../../Images/icon_scheme.png"
                                }
                                else
                                {
                                    imgpath = "../../Images/icon_block.png"
                                }
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1] + '/' + item.split('/')[2],
                                    type: item.split('/')[2],
                                    imageName: imgpath,
                                }
                            }));

                        },
                        error: function (result) {
                        }
                    });
                },
                select: function (event, ui) {
                    
                    document.getElementById('<%= hdnSelectedPropertyId.ClientID %>').value = ui.item.val.split('/')[0]
                    document.getElementById('<%= hdnSelectedPropertyAddress.ClientID %>').value = ui.item.label
                    document.getElementById('<%= hdnSelectedType.ClientID %>').value = ui.item.val.split('/')[1]
                    document.getElementById('<%= txtSearch.ClientID %>').value = ui.item.label
                    __doPostBack("<%=btnHidden.UniqueID %>", "_Image");

                }
            });
            if(autocompleter.data("autocomplete" ))
            {
                autocompleter.data("autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").data( "item.autocomplete", item )
                                    .append("<a><img style='vertical-align:middle; padding-right: 7px;' src='" + item.imageName + "' height='30px' width='30px'/>"+ item.label + "</a>")
                                    .appendTo(ul);
                };
            }
            autocompleter.unbind("blur.autocomplete") // Prevent to close autocomplete when scrolling
            // bind a listener that will hide the menu when clicking on the body of the page.
            $("body:not(.ui-autocomplete)").live('click', function () {
                autocompleter.autocomplete("close");
            });
        }

        function getQuerystring(name) {
            var hash = document.location.hash;
            var match = "";
            if (hash != undefined && hash != null && hash != '') {
                match = RegExp('[?&]' + name + '=([^&]*)').exec(hash);
            }
            else {
                match = RegExp('[?&]' + name + '=([^&]*)').exec(document.location.search);
            }

            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelMiscWork" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle">
                <b>Schedule ad hoc works</b>
            </div>
            <br />
            <asp:HiddenField ID="hdnAdaptationVal" runat="server" />
            <asp:HiddenField ID="hdnAdaptationText" runat="server" />
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <div class="mainContainer" style="width: 98%;">
                <div class="leftContainer" id="leftContainer" runat="server">
                    <asp:Panel runat="server" ID="pnlAddMiscWorks">
                        <table style="width: 100%;">
                            <tr>
                                <td valign="top">
                                    <b>Find a Property/Scheme/Block:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearch" runat="server" Width="85%" CssClass="searchbox" AutoPostBack="false"
                                        AutoCompleteType="Search"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                        TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                    <div style="display: none;">
                                        <asp:HiddenField ID="hdnSelectedPropertyId" runat="server" />
                                        <asp:HiddenField ID="hdnSelectedPropertyAddress" runat="server" />
                                        <asp:HiddenField ID="hdnSelectedType" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>PMO's:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPmos" Width="102%" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnPmos" runat="server" />
                                    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>Type:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlType" runat="server" Width="102%" OnSelectedIndexChanged="ddlType_SelectedIndexChanded"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class='trLocation' visible="false" runat="server" id="trLocation">
                                <td valign="top">
                                    <b>Location:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLocation" runat="server" Width="100%" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanded"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class='trAdaptation' visible="false" runat="server" id="trAdaptation">
                                <td valign="top">
                                    <b>Adaptation:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAdaptation" runat="server" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>Works required:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWorksRequired" runat="server" TextMode="MultiLine" Height="100px"
                                        Width="100%" MaxLength="1000"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>Appointment notes:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAppointmentNotes" runat="server" TextMode="MultiLine" Height="100px"
                                        Width="100%" MaxLength="1000"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Operative trade:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTrade" Width="100%" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Duration:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDuration" runat="server">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnAdd" Text="Add" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%;" valign="top">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top">
                                    <asp:UpdatePanel ID="updOperativesList" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="grdTradeOperative" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                BorderStyle="Solid" BorderWidth="1px" GridLines="None" BorderColor="#BBBBBB"
                                                Font-Bold="False" CssClass="component-trade-list">
                                                <Columns>
                                                    <asp:BoundField DataField="TradeName" HeaderText="Trade" ShowHeader="False" />
                                                    <asp:BoundField DataField="DurationString" HeaderText="Duration" ShowHeader="False" />
                                                    <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete"><img src="../../Images/cross2.png" alt="Delete Fault"  style="border:none; width:16px; " />  </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                        <ItemStyle Width="10px" BorderStyle="None" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Start date:</b>
                                </td>
                                <td>
                                    <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:TextBox ID="txtDate" Width="100%" runat="server"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                        TargetControlID="txtDate" WatermarkText="(Optional)" WatermarkCssClass="searchTextDefault">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                </td>
                            </tr>
                            <tr style="height: 35px; text-align: center;">
                                <td colspan="2" align="right">
                                    <asp:Button ID="btnAddToScheduling" Text="Add to Scheduling" runat="server" />
                                    <asp:Button ID="btnAssignToContractor" Text="Assign To Contractor" runat="server" />
                                    <asp:Button ID="btnFindOperative" runat="server" Text="Find me an operative" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td>
                                </td>
                                <td style="text-align: right;">
                                    <br />
                                    <asp:Button ID="btnAssignToContractor" Text="Assign To Contractor" runat="server" />
                                </td>
                            </tr>--%>
                        </table>
                    </asp:Panel>
                    <uc1:AssignToContractor ID="assignMiscWorkToContractor" runat="server" Visible="false">
                    </uc1:AssignToContractor>
                </div>
                <asp:Panel ID="pnlAvailableOperatives" runat="server" CssClass="misc-rightContainer"
                    Visible="false">
                </asp:Panel>
                <asp:Panel ID="pnlRefreshButton" runat="server" Visible="false">
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
