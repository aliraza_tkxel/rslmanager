﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ConditionAppointmentSummary.aspx.vb" Inherits="PlannedMaintenance.ConditionAppointmentSummary" %>

<%@ Register Src="~/Controls/Scheduling/AppointmentSummary.ascx" TagName="AppointmentSummary"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelAppointment" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="width: 99%">
                <b>Schedule Condition Works Appointment</b>
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <div class="mainContainer">
                <asp:Panel ID="pnlHeader" Width="100%" runat="server">
                    <asp:Label ID="lblAppointmentSummaryTitle" runat="server" CssClass="leftControl"
                        Font-Bold="true" Text="    Appointment Summary"></asp:Label>
                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text=""></asp:Label>
                    <asp:Label ID="lblOf" runat="server" CssClass="rightControl" Font-Bold="true" Text="of"></asp:Label>
                    <asp:Label ID="lblSheetNumberTitle" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text=""></asp:Label>
                    <asp:Label ID="lblJobSheetTitle" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="Job Sheet"></asp:Label>
                </asp:Panel>
                <br />
                <hr />
                <br />
                <uc1:AppointmentSummary ID="appointmentSummaryControl" runat="server" />
                <asp:Panel ID="pnlNotes" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    If the customer has any preferred contact time,<br />
                                    or ways in which we could contact them,<br />
                                    please enter them in the box below:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label20" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Please enter information specific to the
                                    <br />
                                    condition works in the box below:
                                    <br />
                                    &nbsp
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <br />
                <div style="text-align: right; width: 100%;">
                    <asp:Button ID="btnBack" runat="server" Text="< Back" PostBackUrl="~/Views/Scheduling/ConditionAvailableAppointments.aspx" />
                    &nbsp &nbsp
                    <asp:Button ID="btnScheduleAppointment" runat="server" Text="Schedule Appointment" />
                    &nbsp &nbsp
                </div>
            </div>
            <%-- Modal PopUp Confirm Appointment - Start --%>
            <%-- This hidden field is added just to set as Target Control Id for modal popup extender. --%>
            <asp:HiddenField ID="hdnConfirmAppointment" runat="server">
            </asp:HiddenField>
            <asp:ModalPopupExtender ID="mdlConfirmAppointment" runat="server" TargetControlID="hdnConfirmAppointment"
                PopupControlID="pnlConfirmAppointment" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlConfirmAppointment" runat="server" Style="width: 350px" CssClass="modalPopup">
                <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <div class="popupHeading" style="font-weight: bold; padding-left: 12px;">
                    Confirm Appointment
                </div>
                <hr>
                <div class="popupContent" style="padding-left: 25px; padding-right: 25px; line-height: 200%;">
                    If you would like to add more trades/operatives<br />
                    please select "Add More Operatives" or to confirm<br />
                    the appointment select "Confirm".
                </div>
                <div class="popupButtons" style="margin-top: 15px; padding-left: 35px;">
                    <asp:Button Text="Add More Operatives" runat="server" ID="btnAddMoreOperatives" />
                    <asp:Button Text="Confirm Appointment" runat="server" ID="btnConfirmAppointment" /></div>
            </asp:Panel>
            <%-- Modal PopUp Confirm Appointment - End --%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
