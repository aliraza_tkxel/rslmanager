﻿Imports PL_Utilities
Imports PL_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AjaxControlToolkit
Imports System.Drawing

Public Class ArrangedAvailableAppointments
    Inherits PageBase


#Region "Properties"

    ''' <summary>
    ''' This object 'll retain the values of planned appointment bo
    ''' </summary>
    ''' <remarks></remarks>
    Private _objPlannedAppointmentBo As PlannedAppointmentBO
    Public Property objPlannedAppointmentBo() As PlannedAppointmentBO
        Get
            Return _objPlannedAppointmentBo
        End Get
        Set(ByVal value As PlannedAppointmentBO)
            _objPlannedAppointmentBo = value
        End Set
    End Property

    ''' <summary>
    ''' This key 'll retain the operatives, their leaves and their appointments
    ''' </summary>
    ''' <remarks></remarks>
    Private _availableOperatives As DataSet
    Public Property availableOperativesDs() As DataSet
        Get
            Return _availableOperatives
        End Get
        Set(ByVal value As DataSet)
            _availableOperatives = value
        End Set
    End Property

    ''' <summary>
    ''' this spearator 'll be used to bind the information of appointment with select button
    ''' </summary>
    ''' <remarks></remarks>
    Private appointmentInfoSeparater As String = ":::"

    Public ReadOnly Property GetScheduleWorkType As String
        Get
            Dim type As String = ""
            If Not IsNothing(Request.QueryString(PathConstants.ScheduleWorksType)) Then
                type = Request.QueryString(PathConstants.ScheduleWorksType)
            End If
            Return type
        End Get
    End Property

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event handler for page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'get values from session
            Me.getValuesFromSession()
            'fetch the details of property and component & bind it with grid
            Me.bindPropertyComponentDetail()

            If (Not IsPostBack) Then
                'Me.highLightCurrentPageMenuItems()
                Me.getConfirmedTradeAppointments()
                Dim tempTradeIds As String = Me.getComponentTrades()
                Dim availableOperativesDs As DataSet = New DataSet()
                'if temp trade ids does not found then we don't need to find operatives
                If Not String.IsNullOrEmpty(tempTradeIds) Then
                    availableOperativesDs = Me.getAvailableOperatives(tempTradeIds)
                End If
                Me.populateCompleteTrades()
                Me.populateConfirmedTradesAndAppointments()
                Me.populateTempTradeAndAppointments(availableOperativesDs, tempTradeIds)
            ElseIf (uiMessageHelper.IsError = False) Then
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for confirmed trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawConfirmedTradeAppointmentGrid()
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawTempTradeAppointmentGrid()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "btn Select Operative Click"
    Protected Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'This a sample string that will be received on this event
            'Sample ---- operative Id:::Operative Name:::Start Date:::End Date:::Trade Id:::Trade Name:::Trade Duration:::Component Trade Id:::Component Name:::BlockId
            'Values ---- 468:::Jade Burrell:::25/02/2013:::25/02/2013:::1:::Plumber:::2:::4:::Bathroom:::1
            Dim parameters As String = String.Empty
            Dim btnSelectOperative As Button = DirectCast(sender, Button)
            parameters = btnSelectOperative.CommandArgument
            'save the selected appointment information in session
            Me.saveSelectedAppointmentInfo(parameters)
            ''we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
            ''we are removing this from session (if old data exists)
            Me.clearTempAndCofirmBlockFromSession()
            SessionManager.setAppointmentSummarySource(PathConstants.ArrangedAvailableAppointments)
            Me.redirectToAppointmentSummary()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnRefresh.CommandArgument)

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, DateTime.Now)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Confirm Appointments Click"
    Protected Sub btnConfirmAppointments_Click(sender As Object, e As EventArgs) Handles btnConfirmAppointments.Click
        Try
            Me.scheduleConfirmedAppointments()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Go Click"
    ''' <summary>
    ''' This event 'll refresh the appointments against the specific date. Appointment slots 'll be start from date entered.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnGo.CommandArgument)

            'get the start date which is selected against this trade
            Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedTradeAptBlockId.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedTradeAptBlockId.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                startAgain = True
                'Me.validateStartDate(startDate)
            End If

            'it 'll add five more operatives in the list of selected (trade/operative) block            
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, startDate, startAgain)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Rearrange Click"
    Protected Sub btnRearrange_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'This a sample string that will be received on this event
            'Sample ---- AppointmentId
            'Values ---- 468
            Dim appointementId As String = String.Empty
            Dim btnRearrange As Button = DirectCast(sender, Button)
            appointementId = Convert.ToInt32(btnRearrange.CommandArgument)
            Me.reArrangeAppointment(appointementId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "bind Property Component Detail"
    ''' <summary>
    ''' Get the data table from session manager 
    ''' bind it with grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub bindPropertyComponentDetail()

        Dim objAppointmentScheduleBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objAppointmentScheduleBo = SessionManager.getPlannedSchedulingBo()
        If objAppointmentScheduleBo.AppointmentInfoDt.Rows.Count() = 0 Then
            'if no record exist show the error
            Throw New Exception(UserMessageConstants.ComponentTradeDoesNotExist)
        Else
            'bind the data with grid        
            Me.grdPropertyComponentDetail.DataSource = objAppointmentScheduleBo.AppointmentInfoDt
            Me.grdPropertyComponentDetail.DataBind()
        End If

    End Sub
#End Region

#Region "Get Confrimed Trade Appointments"
    ''' <summary>
    ''' This function 'll get the pending/confirmed appointments from database based on property id and journalid (pmo)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getConfirmedTradeAppointments()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        'TODO: fetch it from session
        schedulingBl.getConfirmAppointments(resultDataSet, objPlannedAppointmentBo.PropertyId, objPlannedAppointmentBo.PMO, objPlannedAppointmentBo.Type)

        If resultDataSet.Tables(0).Rows.Count() > 0 Then
            'save confirm appointments in session
            objPlannedSchedulingBo.ConfirmAppointmentsDt = resultDataSet.Tables(0).Copy()
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        End If
    End Sub
#End Region

#Region "get Components Trades"
    ''' <summary>
    ''' this 'll return the trades against component
    ''' </summary>
    ''' <remarks></remarks>
    Private Function getComponentTrades()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim tempTradeIds As String = String.Empty

        'this object contains misc info related to appointment (e.g confirmed and temporary trades)
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        Dim completeQuery = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable()
                             Where (res.Item("InterimStatus") = ApplicationConstants.StatusInterimComplete Or res.Item("InterimStatus") = ApplicationConstants.StatusInterimInProgress) _
                                 And res.Item("PlannedSubStatus") <> ApplicationConstants.StatusNoEntry)

        If completeQuery.Count() > 0 Then
            'Create a table from the query.
            objPlannedSchedulingBo.CompleteTradesDt = completeQuery.CopyToDataTable()
        End If

        Dim confirmQuery = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable()
                            Where ((res.Item("InterimStatus") <> ApplicationConstants.StatusInterimComplete) _
                                Or (res.Item("PlannedSubStatus") = ApplicationConstants.StatusNoEntry)) _
                            AndAlso res.Item("IsPending") = 1
                            Select res)
        If confirmQuery.Count() > 0 Then
            'Create a table from the query.
            objPlannedSchedulingBo.ConfirmedTradesDt = confirmQuery.CopyToDataTable()
        End If


        Dim tempQuery = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable()
                         Where ((res.Item("InterimStatus") <> ApplicationConstants.StatusInterimComplete) _
                         AndAlso (res.Item("InterimStatus") <> ApplicationConstants.StatusInterimInProgress) _
                             Or (res.Item("PlannedSubStatus") = ApplicationConstants.StatusNoEntry)) _
                         AndAlso (res.Item("IsPending") = False)
                         Select res)
        If tempQuery.Count() > 0 Then
            'Create a table from the query.
            objPlannedSchedulingBo.TempTradesDt = tempQuery.CopyToDataTable()
        End If

        Dim tradeIdsQuery = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable()
                             Where ((res.Item("InterimStatus") <> ApplicationConstants.StatusInterimComplete) _
                                  Or (res.Item("PlannedSubStatus") = ApplicationConstants.StatusNoEntry)) _
                             AndAlso (res.Item("IsPending") = False)
                             Select res.Item("TradeId"))
        If tradeIdsQuery.Count() > 0 Then
            tempTradeIds = tradeIdsQuery.Aggregate(Function(x, y) x.ToString() + "," + y.ToString())
        End If

        'savee confirmd trade data table and temporary trade datatable in session
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)

        Return tempTradeIds
    End Function
#End Region

#Region "get Available Operatives"
    ''' <summary>
    ''' this function 'll get the available operatives based on following: 
    ''' trade ids of component, property id, start date 
    ''' </summary>
    ''' <param name="tradeIds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getAvailableOperatives(ByVal tradeIds As String) As DataSet
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()

        'this function 'll get the available operatives based on following: trade ids of component, property id, start date 
        schedulingBl.getAvailableOperatives(resultDataSet, tradeIds, objPlannedAppointmentBo.PropertyId, Date.Now)
        If resultDataSet.Tables(0).Rows.Count() = 0 Then
            Throw New Exception(UserMessageConstants.OperativesNoFound)
        Else
            SessionManager.setAvailableOperativesDs(resultDataSet)
        End If
        Return resultDataSet
    End Function
#End Region

#Region "Regino For Complete Trades"

#Region "Populate Complete Trades"
    ''' <summary>
    ''' This function 'll be used to poopulate the complete trades
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateCompleteTrades()
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        If objPlannedSchedulingBo.CompleteTradesDt.Rows.Count > 0 Then
            bindCompleteTrades(objPlannedSchedulingBo.CompleteTradesDt)
        End If
    End Sub
#End Region

#Region "bind Complete Trades"
    Private Sub bindCompleteTrades(ByVal completeTradesDt As DataTable)

        Dim counter As Integer = 0
        Dim uniqueBlockId As String = String.Empty
        'panel control 
        Dim panelContainer As Panel
        'bind this above data with grid view             
        For Each completeTradeRow As DataRow In completeTradesDt.Rows
            uniqueBlockId = ApplicationConstants.CompleteAppointementGridsIdConstant + counter.ToString()
            'create container for every block of confirmed trade and appointment 
            panelContainer = Me.createContainer(uniqueBlockId)

            'Add the GridView control object dynamically into the parent control
            Dim confirmedTradeGrid As New GridView()
            Dim completeTradeDtBo As CompleteTradeDtBO = New CompleteTradeDtBO()

            completeTradeDtBo.jsnString = completeTradeRow("JSN")
            completeTradeDtBo.componentName = completeTradeRow("Component")
            completeTradeDtBo.durationString = completeTradeRow("Duration")
            completeTradeDtBo.tradeName = completeTradeRow("Trade")
            completeTradeDtBo.status = completeTradeRow("PlannedSubStatus")
            completeTradeDtBo.addNewDataRow()

            panelContainer.Controls.Add(createCompleteTradeGrids(confirmedTradeGrid, uniqueBlockId, completeTradeDtBo.dt))

            'Add this panel container to div page container
            pnlPageContainer.Controls.Add(panelContainer)
            counter = counter + 1
        Next
    End Sub
#End Region

#Region "Create Complete Trade Grids"
    ''' <summary>
    ''' This function 'll create the grid control containing component and trade information of of completed appointments
    ''' </summary>
    ''' <param name="completeTradeGrid"></param>
    ''' <param name="uniqueControlId"></param>
    ''' <param name="tradeDt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createCompleteTradeGrids(ByVal completeTradeGrid As GridView, ByVal uniqueControlId As String, ByVal tradeDt As DataTable)
        'Create object of trade grid
        completeTradeGrid.ID = ApplicationConstants.CompleteTradeGridControlId + uniqueControlId
        completeTradeGrid.EnableViewState = True
        completeTradeGrid.CssClass = "grid_temp_trade"
        completeTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        completeTradeGrid.DataSource = tradeDt
        completeTradeGrid.DataBind()

        Return completeTradeGrid
    End Function
#End Region

#End Region

#Region "Region For Confirm Trades And Appointment"
#Region "populate Confirmed Trades And Appointments"
    ''' <summary>
    ''' This fucntion populates the confrimed trades' appointments in asp.net/html controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateConfirmedTradesAndAppointments()
        'this object contains misc info related to appointment
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        ''if confirmed trades exists then create a list 
        If objPlannedSchedulingBo.ConfirmedTradesDt.Rows.Count > 0 Then
            Dim objSchedulingBl As SchedulingBL = New SchedulingBL()
            Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)

            'this function 'll loop through dataset for confirmed trades and it 'll return the list 
            'of trades and confirmed appointments against them
            confirmTradeAptBoList = objSchedulingBl.getConfirmedTradesAndAppointments(objPlannedSchedulingBo.ConfirmedTradesDt, objPlannedSchedulingBo.ConfirmAppointmentsDt)
            'bind the confirmed trade with grid
            Me.bindConfirmTradesAppointments(confirmTradeAptBoList)
            SessionManager.setConfirmTradeAppointmetBlockList(confirmTradeAptBoList)
            Me.btnConfirmAppointments.Enabled = True
        Else
            Me.btnConfirmAppointments.Enabled = False
        End If

    End Sub
#End Region

#Region "bind Confirm Trades Appointments"
    Private Sub bindConfirmTradesAppointments(ByVal confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO))
        If confirmTradeAptBoList.Count() > 0 Then

            Dim counter As Integer = 0
            Dim uniqueBlockId As String = String.Empty
            'panel control 
            Dim panelContainer As Panel
            'bind this above data with grid view             
            For Each item As ConfirmTradeAppointmentBO In confirmTradeAptBoList
                uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString()
                'create container for every block of confirmed trade and appointment 
                panelContainer = Me.createContainer(uniqueBlockId)

                'Add the GridView control object dynamically into the parent control
                Dim confirmedTradeGrid As New GridView()
                panelContainer.Controls.Add(createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, item))

                'Add the GridView control object dynamically into the div control
                'Create object of appointment grid
                Dim confirmedAptGrid As New GridView()
                panelContainer.Controls.Add(createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, item))
                'Add this panel container to div page container
                pnlPageContainer.Controls.Add(panelContainer)
                counter = counter + 1
            Next
        End If

    End Sub
#End Region

#Region "Create Confirmed Trade Grids"
    ''' <summary>
    ''' This function 'll create the grid control containing component and trade information of of confirmed appointments
    ''' </summary>
    ''' <param name="confirmedTradeGrid"></param>
    ''' <param name="uniqueControlId"></param>
    ''' <param name="confirmTradeAptBo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedTradeGrids(ByVal confirmedTradeGrid As GridView, ByVal uniqueControlId As String, ByVal confirmTradeAptBo As ConfirmTradeAppointmentBO)
        'Create object of trade grid
        confirmedTradeGrid.ID = ApplicationConstants.ConfirmTradeGridControlId + uniqueControlId
        confirmedTradeGrid.EnableViewState = True
        confirmedTradeGrid.CssClass = "grid_temp_trade"
        confirmedTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedTradeGrid.DataSource = confirmTradeAptBo.confirmTradeDtBo.dt
        confirmedTradeGrid.DataBind()

        Return confirmedTradeGrid
    End Function
#End Region

#Region "Create Confirmed Appointment Grids"
    ''' <summary>
    ''' This function creates the grid control containing confirmed appointments
    ''' </summary>
    ''' <param name="confirmedAptGrid"></param>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="confirmTradeAptBo"></param>
    ''' ''' <param name="hideRearrnageButton"> 'this value 'll always be false excetp when all appointments 'll have "Arranged" status</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedAppointmentGrids(ByVal confirmedAptGrid As GridView, ByVal uniqueBlockId As String, ByVal confirmTradeAptBo As ConfirmTradeAppointmentBO, Optional ByVal hideRearrnageButton As Boolean = False)

        confirmedAptGrid.ID = ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId
        confirmedAptGrid.EnableViewState = True
        confirmedAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        confirmedAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedAptGrid.DataSource = confirmTradeAptBo.confirmAppointmentDtBo.dt
        confirmedAptGrid.DataBind()
        'hide the unwanted columns from the grid
        confirmedAptGrid.HeaderRow.Cells(4).Visible = False
        Dim gvr As GridViewRow = confirmedAptGrid.Rows(0)
        gvr.Cells(4).Visible = False

        'this if condition body 'll always execute except when all appointments 'll be arranged, 
        'then this if condition body 'll not execute
        If hideRearrnageButton = False Then
            'Add the rearrange button 
            Dim btnRearrange As Button = New Button()
            btnRearrange.ID = "btnRearrange" + uniqueBlockId.ToString()
            btnRearrange.Text = "Rearrange"
            btnRearrange.CssClass = "row-button"
            btnRearrange.CommandArgument = confirmTradeAptBo.confirmAppointmentDtBo.dt.Rows(0).Item(ConfirmAppointmentDtBO.appointmentIdColName)
            AddHandler btnRearrange.Click, AddressOf btnRearrange_Click
            gvr.Cells(3).Controls.Add(btnRearrange)
        End If


        Return confirmedAptGrid
    End Function
#End Region

#Region "Rearrange Appointment"
    Private Sub reArrangeAppointment(ByVal appointmentId As Integer)
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()

        Dim result = schedulingBl.setPendingBitForAppointment(appointmentId, False)
        If result = True Then
            'also delete this record from the confirmed trade data table
            Dim query = (From res In objPlannedSchedulingBo.ConfirmAppointmentsDt.AsEnumerable() Where Convert.ToInt32(res.Item("AppointmentId")) <> appointmentId Select res)
            If query.Count() > 0 Then
                objPlannedSchedulingBo.ConfirmAppointmentsDt = query.CopyToDataTable()
            Else
                objPlannedSchedulingBo.ConfirmAppointmentsDt.Clear()
            End If

            'now set record in dataset as isPending =0 
            ' this means that appointment is available to rearrange again.
            Dim updatedAppointmentRow() As Data.DataRow
            updatedAppointmentRow = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Select("AppointmentId = " + appointmentId.ToString() + "")
            updatedAppointmentRow(0)("IsPending") = 0

            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Me.reloadCurrentWebPage()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToRearrangeTheAppointment, True)
        End If
    End Sub
#End Region

#Region "reDraw Confirmed Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then redraw the controls of confirmed trades and appointment list''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawConfirmedTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for confirmed trades and appointment list
        Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)
        confirmTradeAptBoList = SessionManager.getConfirmTradeAppointmetBlockList()
        Me.bindConfirmTradesAppointments(confirmTradeAptBoList)
    End Sub
#End Region

#Region "Schedule Confirmed Appointments"
    ''' <summary>
    ''' This function 'll be called when user have created the appointment against all trades but those appointments are in pending status. 
    ''' This funciton 'll update isPending status to 0 and 'll also change the status from "To Be Arrnaged" ---> "Arrnaged"
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub scheduleConfirmedAppointments()
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = SessionManager.getPlannedSchedulingBo()
        'get the appointment id's that have been confirmed.
        Dim appointmentIds As String = String.Empty
        Dim query = (From res In objPlannedSchedulingBo.ConfirmAppointmentsDt Select res.Item("AppointmentId"))
        If query.Count() > 0 Then
            appointmentIds = query.Aggregate(Function(x, y) x.ToString() + "," + y.ToString())

            Dim result As Boolean = False
            result = schedulingBl.scheduleConfirmedPlannedAppointments(appointmentIds,
                                                                           objPlannedAppointmentBo.PropertyId,
                                                                           objPlannedAppointmentBo.PMO,
                                                                           objPlannedAppointmentBo.ComponentId,
                                                                           SessionManager.getUserEmployeeId())
            If result = True Then
                Me.btnConfirmAppointments.Enabled = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppoinemtsScheduledSuccessfully, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UnableToScheduleTheAppointment, True)
            End If
        End If
    End Sub
#End Region

#Region "Partial Redraw & Rebind Confrimed Trade Appointment Grids"
    ''' <summary>
    ''' This function 'll only redraw the confirmed grids. reset of the controls 'll be remain same. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub partialRedrawAndRebindTradeAppointmentConfirmedGrids()
        Dim confirmTradeAptBoList As List(Of ConfirmTradeAppointmentBO) = New List(Of ConfirmTradeAppointmentBO)
        confirmTradeAptBoList = SessionManager.getConfirmTradeAppointmetBlockList()

        Dim counter As Integer = 0
        Dim uniqueBlockId As String = String.Empty

        'bind this above data with grid view             
        For Each confirmTradeAptBo As ConfirmTradeAppointmentBO In confirmTradeAptBoList
            uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString()

            ' change the status to "Arranged"
            confirmTradeAptBo.confirmTradeDtBo.dt.Rows(0).Item(ConfirmTradeDtBO.statusColName) = ApplicationConstants.StatusArranged

            'Add the GridView control object dynamically into the parent control
            Dim confirmedTradeGrid As GridView = New GridView
            confirmedTradeGrid = CType(pnlPageContainer.FindControl(ApplicationConstants.ConfirmTradeGridControlId + uniqueBlockId), GridView)
            Me.createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, confirmTradeAptBo)

            'Add the GridView control object dynamically into the div control
            'find & Create object of appointment grid
            Dim confirmedAptGrid As New GridView
            confirmedAptGrid = CType(pnlPageContainer.FindControl(ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId), GridView)
            Me.createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, confirmTradeAptBo, True)

            counter = counter + 1
        Next
    End Sub
#End Region

#End Region

#Region "Region For Temporary trade And Appointments"

#Region "Populate temp Trade And Appointment"
    Private Sub populateTempTradeAndAppointments(ByVal availableOperativesDs As DataSet, ByVal tradeIds As String)
        Dim tempTradeExist As Boolean = False
        'check if the unconfirmed trades exists in table
        If Not (String.IsNullOrEmpty(tradeIds)) Then
            tempTradeExist = True
        Else
            Me.unHideBackButton()
        End If

        'if unconfirmed trades exists then create a list 
        If tempTradeExist = True Then            '
            Dim schedulingBl As SchedulingBL = New SchedulingBL()
            Dim tempTradeAptBoList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
            'this function 'll process the component trades and 'll create the appointments against them
            tempTradeAptBoList = schedulingBl.getTempTradeAppointments(objPlannedSchedulingBo.TempTradesDt, availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), Date.Now)
            'save list in session 
            SessionManager.setTempTradeAppointmetBlockList(tempTradeAptBoList)
            Me.bindTempTradesAppointment(tempTradeAptBoList)
        End If

    End Sub
#End Region

#Region "bind Temp Trade Appointments"
    ''' <summary>
    ''' This funciton 'll bind the data with temporary trade and appointments block
    ''' </summary>
    ''' <param name="tempTradeAptBoList"></param>
    ''' <remarks></remarks>
    Private Sub bindTempTradesAppointment(ByVal tempTradeAptBoList As List(Of TempTradeAppointmentBO))
        If IsNothing(tempTradeAptBoList) Then
            Me.unHideBackButton()
        Else
            'panel control 
            Dim panelContainer As Panel
            ' We are considering trade and its respective operative list as block so 
            'these varaibles 'll be used to recognize each block when some button/checkbox 'll be clicked e.g refresh list button
            Dim uniqueControlId As Integer = 0
            Dim uniqueControlIdString As String = uniqueControlId.ToString()

            'bind this above data with grid view             
            For Each item As TempTradeAppointmentBO In tempTradeAptBoList
                'create container for every block of temporary Trades and appointment 
                panelContainer = Me.createContainer(uniqueControlIdString)

                'Create object of Trade grid
                Dim tempTradeGrid As New GridView
                tempTradeGrid = Me.createTempTradesGrids(tempTradeGrid, uniqueControlIdString, item)
                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(tempTradeGrid)

                'Create object of appointment grid
                Dim tempAptGrid As New GridView()
                'Add the GridView control into the div control/panel
                tempAptGrid = Me.createTempAppointmentGrids(tempAptGrid, uniqueControlIdString, item)
                panelContainer.Controls.Add(tempAptGrid)

                'Add this panel container to div page container
                pnlPageContainer.Controls.Add(panelContainer)

                'this varaible 'll be used to recognize each control
                uniqueControlId = uniqueControlId + 1
                uniqueControlIdString = uniqueControlId.ToString()
            Next
        End If
    End Sub
#End Region

#Region "create Container"
    ''' <summary>
    ''' This function creates the html container for every block of trade and appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContainer(ByVal uniqueBlockId As String)
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + uniqueBlockId
        divContainer.CssClass = "trade-appointment-block"
        Return divContainer
    End Function
#End Region

#Region "create Temp Trade Grids"
    ''' <summary>
    ''' This function 'll create the temporary trade grid on the basis of unique id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createTempTradesGrids(ByVal tempTradeGrid As GridView, ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)

        tempTradeGrid.ID = ApplicationConstants.TempTradeGridControlId + uniqueBlockId
        tempTradeGrid.EnableViewState = True
        tempTradeGrid.CssClass = "grid_temp_trade"
        tempTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        tempTradeGrid.DataSource = item.tempTradeDtBo.dt
        tempTradeGrid.DataBind()

        'hide the last column from temporary trade grid(that are not required in grid view) because those columns 
        'were there in datatable and those columns have been automatically created, when we bound datatable with grid view
        'the columns of these data table 'll be used through sessoion
        tempTradeGrid.HeaderRow.Cells(5).Visible = False
        tempTradeGrid.HeaderRow.Cells(6).Visible = False
        tempTradeGrid.HeaderRow.Cells(7).Visible = False
        tempTradeGrid.HeaderRow.Cells(8).Visible = False
        tempTradeGrid.HeaderRow.Cells(9).Visible = False
        'run the loop on grid view rows to remove the cells (that are not required in grid view) from each row 
        'and also to add the select button in each row
        For Each gvr As GridViewRow In tempTradeGrid.Rows
            gvr.Cells(5).Visible = False
            gvr.Cells(6).Visible = False
            gvr.Cells(7).Visible = False
            gvr.Cells(8).Visible = False
            gvr.Cells(9).Visible = False
        Next
        Return tempTradeGrid
    End Function
#End Region

#Region "create Temp Appointment Grids"
    ''' <summary>
    ''' This function 'll create the temporary appointment grid on the basis of unique block id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createTempAppointmentGrids(ByVal tempAptGrid As GridView, ByVal uniqueBlockId As String, item As TempTradeAppointmentBO)

        Dim isEmptyGrid As Boolean = False

        tempAptGrid.ID = ApplicationConstants.TempAppointmentGridControlId + uniqueBlockId.ToString()
        tempAptGrid.ShowFooter = True
        tempAptGrid.EnableViewState = True
        tempAptGrid.RowStyle.BackColor = ColorTranslator.FromHtml("#E6E6E6")
        tempAptGrid.AlternatingRowStyle.BackColor = ColorTranslator.FromHtml("#FFFFFF")
        tempAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        tempAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"

        'we need jsn to link that with back button. for that we 'll get component id from the created appointment/trade slots. 
        'From componentTradeId, we 'll get jsn from tradeAppointmentArrangedDt 
        'get the component trade id 
        Dim componentTradeIdQuery As IEnumerable(Of DataRow) = (From res In item.tempTradeDtBo.dt.AsEnumerable() Select res)
        Dim componentTradeId As Integer = componentTradeIdQuery.First().Item(TempTradeDtBO.componentTradeIdColName)
        'get jsn from the appointment arranged data table
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        Dim jsnQuery As IEnumerable(Of DataRow) = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable() Where res.Item("ComponentTradeId") = componentTradeId Select res)
        Dim jsn As String = jsnQuery.First().Item("JSNSearch")

        'Check if , are there any appointments or not .If not then display message
        If item.tempAppointmentDtBo.dt.Rows.Count = 0 Then
            Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoOperativesExistWithInTime)
            isEmptyGrid = True
        Else
            tempAptGrid.DataSource = item.tempAppointmentDtBo.dt
            tempAptGrid.DataBind()
            'add row span on footer and add back button, refresh list button and view calendar button
            tempAptGrid.FooterRow.Cells(0).Controls.Add(createAppointmentFooterControls(uniqueBlockId, jsn))
        End If

        'after adding the buttons add col span to footer so that buttons should appear according to design
        If tempAptGrid.Rows.Count > 0 Then
            Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
            For i As Integer = m - 1 To 1 Step -1
                tempAptGrid.FooterRow.Cells.RemoveAt(i)
            Next i
            tempAptGrid.FooterRow.Cells(0).ColumnSpan = 4 '4 is the number of visible columns to span.
        End If

        'create header of appointment grid and add start date calendar in it
        Dim tradAptGridHeaderCell As TableCell = tempAptGrid.HeaderRow.Cells(3)
        tradAptGridHeaderCell.Controls.Add(Me.createAppointmentHeaderControls(uniqueBlockId.ToString(), item.StartSelectedDate))

        'declare the variables that 'll store the information required to bind with select button in grid                
        Dim operativeId As String = String.Empty
        Dim operativeName As String = String.Empty
        Dim startDateString As String = String.Empty
        Dim endDateString As String = String.Empty
        Dim dtCounter As Integer = 0
        Dim completeAppointmentInfo As String = String.Empty

        'remove the header of unwanted columns
        tempAptGrid.HeaderRow.Cells(4).Visible = False
        tempAptGrid.HeaderRow.Cells(5).Visible = False
        tempAptGrid.HeaderRow.Cells(6).Visible = False
        tempAptGrid.HeaderRow.Cells(7).Visible = False
        tempAptGrid.HeaderRow.Cells(8).Visible = False
        'if grid is not empty then remove the cells and also create select button and set command argument with that
        If isEmptyGrid = False Then
            'run the loop on grid view rows to remove the cells from each row 
            'and also to add the select button in each row
            For Each gvr As GridViewRow In tempAptGrid.Rows
                gvr.Cells(4).Visible = False
                gvr.Cells(5).Visible = False
                gvr.Cells(6).Visible = False
                gvr.Cells(7).Visible = False
                gvr.Cells(8).Visible = False
                'get the information that should be set with select button                                             
                operativeId = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeIdColName)
                operativeName = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeColName)
                startDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.startDateStringColName)
                endDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.endDateStringColName)
                dtCounter += 1

                'save this operative and time slot information in a string, after that it 'll be bind with select button
                completeAppointmentInfo = operativeId.ToString() _
                    + Me.appointmentInfoSeparater + operativeName _
                    + Me.appointmentInfoSeparater + startDateString _
                    + Me.appointmentInfoSeparater + endDateString _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeName _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.tradeDurationHrs.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentTradeId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentId.ToString() _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.componentName _
                    + Me.appointmentInfoSeparater + uniqueBlockId _
                    + Me.appointmentInfoSeparater + item.tempTradeDtBo.jsn

                'Add the select button 
                Dim btnSelectOperative As Button = New Button()
                btnSelectOperative.ID = "btnSelectOperative" + uniqueBlockId.ToString()
                btnSelectOperative.Text = "Select"
                btnSelectOperative.CssClass = "row-button"
                btnSelectOperative.CommandArgument = completeAppointmentInfo
                AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                gvr.Cells(3).Controls.Add(btnSelectOperative)
            Next
        End If
        Return tempAptGrid
    End Function
#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef tempTradeAptBo As TempTradeAppointmentBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        tempTradeAptBo.tempAppointmentDtBo.dt.Rows.Add(tempTradeAptBo.tempAppointmentDtBo.dt.NewRow())
        grid.DataSource = tempTradeAptBo.tempAppointmentDtBo.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        tempTradeAptBo.tempAppointmentDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Appointment Footer Controls"
    ''' <summary>
    ''' This function 'll create the footer buttons of appointment grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createAppointmentFooterControls(ByVal uniqueBlockId As String, ByVal jsn As String)
        Dim buttonsContainer As Panel = New Panel()
        Dim btnBack As Button = New Button()
        Dim btnRefreshList As Button = New Button()
        Dim btnViewCalendar As Button = New Button()
        'set the back button 
        btnBack.Text = "< Back to Scheduling"
        Dim responseUrl As String = PathConstants.ViewArrangedAppointments + "?" + PathConstants.Jsn + "=" + jsn + "&" + PathConstants.Pmo + "=" + objPlannedAppointmentBo.PMO.ToString()
        If GetScheduleWorkType <> "" Then
            responseUrl = responseUrl + "&" + PathConstants.ScheduleWorksType + "=" + GetScheduleWorkType
        End If

        If GetComponentId <> -1 Then
            responseUrl = responseUrl + "&" + PathConstants.ComponentId + "=" + GetComponentId
        End If

        btnBack.PostBackUrl = responseUrl
        btnBack.CssClass = "trade-appointment-left-buttons"
        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = uniqueBlockId
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click
        'set the view calendar button
        btnViewCalendar.Text = "View Calendar"
        btnViewCalendar.Enabled = False
        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "trade-appointment-buttons"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnBack)
        buttonsContainer.Controls.Add(btnRefreshList)
        buttonsContainer.Controls.Add(btnViewCalendar)
        Return buttonsContainer
    End Function
#End Region

#Region "Create Appointment Header Controls"
    ''' <summary>
    ''' This function 'll create the date control on the top of appointment gird
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createAppointmentHeaderControls(ByVal uniqueBlockId As String, startDate As Date)
        'start label
        Dim lblStartDate As Label = New Label()
        lblStartDate.ID = ApplicationConstants.StartDateLabelControlId + uniqueBlockId
        lblStartDate.Text = "Start Date:"
        'date text box
        Dim txtDate As TextBox = New TextBox()
        txtDate.ID = ApplicationConstants.AppointmentStartDateControlId + uniqueBlockId
        txtDate.Text = startDate.Date
        'image button
        Dim imgCalDate As HtmlImage = New HtmlImage()
        imgCalDate.ID = ApplicationConstants.CalDateControlId + uniqueBlockId
        imgCalDate.Src = "../../Images/calendar.png"
        'calendar extendar
        Dim calExtStartDate As CalendarExtender = New CalendarExtender()
        calExtStartDate.PopupButtonID = imgCalDate.ID
        calExtStartDate.PopupPosition = CalendarPosition.Right
        calExtStartDate.TargetControlID = txtDate.ID
        calExtStartDate.ID = ApplicationConstants.CalExtDateControlId + uniqueBlockId
        calExtStartDate.Format = "dd/MM/yyyy"
        Dim currentDate As Date = Date.Now()
        calExtStartDate.StartDate = currentDate.ToString("dd/MM/yyyy")

        'create go button
        Dim btnGo As Button = New Button()
        btnGo.ID = ApplicationConstants.GoControlId + uniqueBlockId
        btnGo.Text = "Go"
        btnGo.CommandArgument = uniqueBlockId
        AddHandler btnGo.Click, AddressOf btnGo_Click
        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-appointment-header"
        pnl.Controls.Add(lblStartDate)
        pnl.Controls.Add(txtDate)
        pnl.Controls.Add(calExtStartDate)
        pnl.Controls.Add(imgCalDate)
        pnl.Controls.Add(btnGo)
        Return pnl
    End Function
#End Region

#Region "reDraw Temp Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then
    ''' redraw the controls for temporary trade and appointment list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawTempTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary trade and appointment list
        Dim tempTradeAptList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
        tempTradeAptList = SessionManager.getTempTradeAppointmetBlockList()
        Me.bindTempTradesAppointment(tempTradeAptList)
    End Sub
#End Region

#Region "refresh The Opertives List"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="selectedTradeAptBlockId"></param>    
    ''' <param name="startAgain">This variable 'll identify either appointment slots should be recreated from the selected start date or not</param>
    ''' <remarks></remarks>
    Private Sub refreshTheOpertivesList(ByVal selectedTradeAptBlockId As Integer, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
        Dim tempTradeAptList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
        Dim tempTradeAptBo As TempTradeAppointmentBO = New TempTradeAppointmentBO()
        tempTradeAptList = SessionManager.getTempTradeAppointmetBlockList()
        'get the specific object from the list of trade& appointments
        tempTradeAptBo = tempTradeAptList(selectedTradeAptBlockId)
        'increase the count of operatives by 
        tempTradeAptBo.DisplayCount = tempTradeAptBo.DisplayCount + 5
        'if user didn't change the start date 
        If startAgain = True Then
            tempTradeAptBo.StartSelectedDate = startDate
        End If
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        'get the available operatives, their leaves, appointments and selected start date
        availableOperativesDs = SessionManager.getAvailableOperativesDs()
        schedulingBl.addMoreTempTradeAppointments(availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), tempTradeAptBo, startDate, startAgain)
        'save in temporary trade appointment list
        tempTradeAptList(selectedTradeAptBlockId) = tempTradeAptBo
        ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        schedulingBl.orderAppointmentsByTimeAndOperative(tempTradeAptBo)

        ''get the instance of gridview control
        Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl("grdTempAppointments" + selectedTradeAptBlockId.ToString()), GridView)
        Me.createTempAppointmentGrids(grdTempAppointments, selectedTradeAptBlockId, tempTradeAptBo)
    End Sub
#End Region

#Region "save Selected Appointment Info"
    ''' <summary>
    ''' This function 'll save the selected appointment information in session
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedAppointmentInfo(ByVal parameters As String)
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()

        Dim separater As String() = {Me.appointmentInfoSeparater}
        Dim appointmentParameters() As String = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries)
        objPlannedAppointmentBo.OperativeId = Integer.Parse(appointmentParameters(0))
        objPlannedAppointmentBo.OperativeName = appointmentParameters(1)
        objPlannedAppointmentBo.StartDate = appointmentParameters(2)
        objPlannedAppointmentBo.EndDate = appointmentParameters(3)
        objPlannedAppointmentBo.TradeId = Integer.Parse(appointmentParameters(4))
        objPlannedAppointmentBo.TradeName = appointmentParameters(5)
        objPlannedAppointmentBo.Duration = appointmentParameters(6)
        objPlannedAppointmentBo.CompTradeId = Integer.Parse(appointmentParameters(7))
        objPlannedAppointmentBo.ComponentId = Integer.Parse(appointmentParameters(8))
        objPlannedAppointmentBo.ComponentName = appointmentParameters(9)
        objPlannedAppointmentBo.Jsn = appointmentParameters(11)
        objPlannedAppointmentBo.TotalDuration = objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("TotalDuration")

        If (objPlannedAppointmentBo.CompTradeId > 0) Then
            Dim query As IEnumerable(Of DataRow) = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable Where res.Item("ComponentTradeId") = objPlannedAppointmentBo.CompTradeId Select res)
            objPlannedAppointmentBo.AppointmentId = Convert.ToInt32(query.First().Item("AppointmentId"))
            objPlannedAppointmentBo.CustomerNotes = query.FirstOrDefault().Item("CustomerNotes").ToString()
            objPlannedAppointmentBo.AppointmentNotes = query.FirstOrDefault().Item("JobSheetNotes").ToString()
        Else
            Dim query As IEnumerable(Of DataRow) = (From res In objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.AsEnumerable Where res.Item("JSN") = objPlannedAppointmentBo.Jsn Select res)
            objPlannedAppointmentBo.AppointmentId = Convert.ToInt32(query.First().Item("AppointmentId"))
            objPlannedAppointmentBo.CustomerNotes = query.FirstOrDefault().Item("CustomerNotes").ToString()
            objPlannedAppointmentBo.AppointmentNotes = query.FirstOrDefault().Item("JobSheetNotes").ToString()
        End If

        objPlannedAppointmentBo.Index = appointmentParameters(10) + 1
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
    End Sub
#End Region

#Region "Validate Start Date"
    ''' <summary>
    ''' This funciton 'll perform the validation of start date
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <remarks></remarks>
    Private Sub validateStartDate(ByVal startDate As Date)
        Dim result As Integer = DateTime.Compare(startDate.Date, Date.Today)
        If result < 0 Then
            Throw New Exception(UserMessageConstants.SelectedDateIsInPast)
        End If
    End Sub
#End Region

#Region "Redirect to Appointment Summary And Confirmation"
    Private Sub redirectToAppointmentSummary()

        Response.Redirect(PathConstants.AppointmentSummary, True)
    End Sub
#End Region

#End Region

#Region "unhide Back Button"
    Private Sub unHideBackButton()
    End Sub
#End Region

#Region "clear Panel Page Container"
    ''' <summary>
    ''' 'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
    ''' This control(divPageContainer) contains all the grids that 'll be displayed on page.
    ''' This function should be called on page load and on every event handler
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearPanelPageContainer()
        pnlPageContainer.Controls.Clear()
    End Sub
#End Region

#Region "get Values From Session"
    ''' <summary>
    ''' This function 'll be used to get the values from session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getValuesFromSession()
        objPlannedAppointmentBo = SessionManager.getPlannedAppointmentBO()
    End Sub
#End Region

#Region "clear Temp And Cofirm Block From Session"
    ''' <summary>
    ''' we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
    ''' we are removing this from session (if old data exists)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearTempAndCofirmBlockFromSession()
        'removing the temporary appointments from session
        SessionManager.removeTempTradeAppointmetBlockList()

    End Sub
#End Region

#Region "show Top Buttons"
    ''' <summary>
    ''' When all appointments 'll be arranged then the top two buttons 'll be displayed
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub showTopButtons()
        pnlTopButtons.Visible = True
    End Sub
#End Region

#Region "Redirect to Schedule Works"
    ''' <summary>
    ''' This function 'll redirect the user to schedule works page 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub redirectToScheduleWorks()
        Response.Redirect(PathConstants.ScheduleWorks)
    End Sub
#End Region

#Region "Reload the Current Web Page"
    ''' <summary>
    ''' This function 'll reload the current page. This is being used when user rearrange the scheduled appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reloadCurrentWebPage()
        Response.Redirect(Request.RawUrl)
    End Sub
#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#End Region

End Class