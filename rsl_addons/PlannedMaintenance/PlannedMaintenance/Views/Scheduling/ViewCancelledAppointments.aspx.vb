﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class ViewCancelledAppointments
    Inherits PageBase


#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
                If (getvaluesFromQueryString()) Then
                    Me.getSetAllAppointmentData()
                Else
                    disableAllControls()
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region


#Region "Button Back Click Event"
    ''' <summary>
    ''' Button Back Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            If Not IsNothing(Request.QueryString(PathConstants.Src)) Then
                Response.Redirect(PathConstants.CompletedPlannedWork)
            Else
                Me.navigateToScheduleWorks()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Previous button clicked"
    ''' <summary>
    ''' Previous button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex - 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Me.setPreviousNextButtonStates(currentIndex)
            populateAppointmentInfo()
            populatePropertyInfo()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Next button clicked"
    ''' <summary>
    ''' Next button clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex + 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Me.setPreviousNextButtonStates(currentIndex)
            populateAppointmentInfo()
            populatePropertyInfo()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region


#End Region

#Region "Function"




#Region "Get values from Query String"
    ''' <summary>
    ''' Get values from Query String
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getvaluesFromQueryString()
        Dim validData As Boolean = False

        If Not IsNothing(Request.QueryString(PathConstants.Pmo)) And Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
            Dim pmo As Integer
            Dim validPmo As Boolean = Integer.TryParse(Request.QueryString(PathConstants.Pmo), pmo)
            If (validPmo) Then
                ViewState.Add(ViewStateConstants.Pmo, pmo)
                ViewState.Add(ViewStateConstants.Jsn, Request.QueryString(PathConstants.Jsn))
                ViewState.Add(ViewStateConstants.isReadOnly, Request.QueryString(PathConstants.isReadOnly))
                validData = True
            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidJobSheetAndPmoQueryString, True)
        End If
        Return validData

    End Function

#End Region

#Region "Disable all controls"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub disableAllControls()
        btnPrevious.Enabled = False
        btnNext.Enabled = False
        btnReScheduleAppointment.Enabled = False
        txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
        txtJobSheetNotes.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Pre Populate Values"
    ''' <summary>
    ''' get set all appointment data
    ''' </summary>
    ''' <remarks></remarks>
    Sub getSetAllAppointmentData()

        'Removing pre existing Arranged Appointments Dataset
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Clear()

        Dim isReadOnly As Int32
        Integer.TryParse(ViewState(ViewStateConstants.isReadOnly), isReadOnly)

        Dim pmo As Integer = Integer.Parse(ViewState(ViewStateConstants.Pmo))
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataset As DataSet = New DataSet
        objSchedulingBL.getCancelledAppointmentsDetail(resultDataset, pmo)

        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            disableAllControls()
        Else
            If isReadOnly = 1 Then
                txtCustAppointmentNotes.ReadOnly = True
                txtJobSheetNotes.ReadOnly = True
            End If
            'Dim totalDuration As Double = (Aggregate row As DataRow In resultDataset.Tables(0).AsEnumerable() _
            '                               Into Sum(row.Field(Of Double)("Duration")))
            Dim totalDuration As Double = resultDataset.Tables(0).Compute("Sum(Duration)", "")

            If objPlannedSchedulingBo.AppointmentInfoDt.Columns.Contains("TotalDuration") = False Then
                'add column of total duration in data table which we get when user clicked on appointment arranged arrow
                objPlannedSchedulingBo.AppointmentInfoDt.Columns.Add("TotalDuration")
            End If
            'update the newly created column
            Dim updatedAppointmentRow() As Data.DataRow
            'get the row based on property id
            updatedAppointmentRow = objPlannedSchedulingBo.AppointmentInfoDt.Select("PropertyId = " + ViewStateConstants.PropertyId + "")
            'set the total duration column in a row
            'updatedAppointmentRow(0)("TotalDuration") = totalDuration
            For Each row In updatedAppointmentRow
                row("TotalDuration") = totalDuration
            Next

            If Not IsNothing(resultDataset.Tables(0).Columns("TotalDuration")) Then
                resultDataset.Tables(0).Columns("TotalDuration").Expression = totalDuration
            End If

            'set all appointments of component's trade in session
            objPlannedSchedulingBo.ArrangedTradesAppointmentsDt = resultDataset.Tables(0).Copy()
            objPlannedSchedulingBo.PropertyInfoDt = resultDataset.Tables(1).Copy()
            'save the data in session 
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Me.populateSummaryFromQueryString()
        End If

    End Sub

#End Region

#Region "Populate summary from query string"
    ''' <summary>
    ''' Populate summary from query string
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSummaryFromQueryString()
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        Dim dtAppointments As DataTable = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt
        Dim drAppointment() As DataRow

        drAppointment = dtAppointments.Select("JSNSearch = '" + ViewState(ViewStateConstants.Jsn).ToString() + "'")

        If (drAppointment.Count > 0) Then
            Dim currentIndex As Integer = drAppointment(0).Item(ApplicationConstants.JobsheetCurrentColumn)
            Dim totalJobSheets As Integer = dtAppointments.Rows.Count

            ViewState.Add(ViewStateConstants.CurrentIndex, currentIndex)
            ViewState.Add(ViewStateConstants.TotalJobsheets, totalJobSheets)
            Me.setPreviousNextButtonStates(currentIndex)
            Me.populateAppointmentInfo()
            Me.populatePropertyInfo()

        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidJsn, True)
            disableAllControls()
        End If

    End Sub

#End Region

#Region "Set previous next button states"
    Private Sub setPreviousNextButtonStates(ByVal updatedIndex As Integer)

        Dim currentIndex As Integer = updatedIndex
        Dim totalJobSheets As Integer = Convert.ToInt32(ViewState(ViewStateConstants.TotalJobsheets))

        lblSheetNumber.Text = currentIndex
        lblTotalSheets.Text = totalJobSheets

        'Previous Button state

        If (updatedIndex - 1 < 1) Then
            btnPrevious.Enabled = False
        Else
            btnPrevious.Enabled = True
        End If

        'Next Button state
        If (updatedIndex + 1 > totalJobSheets) Then
            btnNext.Enabled = False
        Else
            btnNext.Enabled = True
        End If

    End Sub
#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' Populate Appointment Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentInfo()

        Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
        Dim drAppointment As DataRow = Me.getCurrentIndexRowFromDataset(currentIndex)

        lblPmo.Text = drAppointment.Item(ApplicationConstants.PmoColumn)
        lblComponent.Text = drAppointment.Item(ApplicationConstants.ComponentColumn)
        lblTrade.Text = drAppointment.Item(ApplicationConstants.TradesColumn)

        lblJsn.Text = drAppointment.Item(ApplicationConstants.JsnColumn)
        lblStatus.Text = drAppointment.Item(ApplicationConstants.InterimStatusColumn)
        lblOperative.Text = drAppointment.Item(ApplicationConstants.OperativeColumn)
        ' lblTime.Text = drAppointment.Item(ApplicationConstants.StartTimeColumn) + " - " + drAppointment.Item(ApplicationConstants.EndTimeColumn)

        If (IsDBNull(drAppointment.Item(ApplicationConstants.DurationsColumn))) Then
            lblDuration.Text = ApplicationConstants.NotAvailable
            lblDurationTotal.Text = ApplicationConstants.NotAvailable
        ElseIf (drAppointment.Item(ApplicationConstants.IsMiscAppointmentColumn) = 1) Then
            lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.DurationsColumn)))
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.TotalDurationColumn)))
        Else
            lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.DurationsColumn)))
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment.Item(ApplicationConstants.TotalDurationColumn)))
        End If


        Dim startDateTime As DateTime = Convert.ToDateTime(drAppointment.Item(ApplicationConstants.StartDateColumn) + " " + drAppointment.Item(ApplicationConstants.StartTimeColumn))
        'Dim startDay As String = startDateTime.DayOfWeek.ToString()
        'Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(startDateTime.ToString("dd")))
        'Dim startMonth As String = startDateTime.ToString("MMMMMMMMMMMMM")
        'Dim startYear As String = startDateTime.ToString("yyyy")

        Dim endDateTime As DateTime = Convert.ToDateTime(drAppointment.Item(ApplicationConstants.EndDateColumn) + " " + drAppointment.Item(ApplicationConstants.EndTimeColumn))
        'Dim endDay As String = endDateTime.DayOfWeek.ToString()
        'Dim endDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(endDateTime.ToString("dd")))
        'Dim endMonth As String = endDateTime.ToString("MMMMMMMMMMMMM")
        'Dim endYear As String = endDateTime.ToString("yyyy")

        'lblStartDate.Text = startDay + " " + startDate + " " + startMonth + " " + startYear
        'lblEndDate.Text = endDay + " " + endDate + " " + endMonth + " " + endYear

        'Dim startDateTime As DateTime = Convert.ToDateTime(objMiscAppointmentBO.StartDate + " " + objMiscAppointmentBO.StartTime)
        'Dim endDateTime As DateTime = Convert.ToDateTime(objMiscAppointmentBO.EndDate + " " + objMiscAppointmentBO.EndTime)

        lblStartDate.Text = Convert.ToDateTime(startDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime)
        lblEndDate.Text = Convert.ToDateTime(endDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime)

        If (IsDBNull(drAppointment.Item(ApplicationConstants.CustomerNotesColumn))) Then
            txtCustAppointmentNotes.Text = String.Empty
        Else
            txtCustAppointmentNotes.Text = drAppointment.Item(ApplicationConstants.CustomerNotesColumn)
        End If

        If (IsDBNull(drAppointment.Item(ApplicationConstants.JobsheetNotesColumn))) Then
            txtJobSheetNotes.Text = String.Empty
        Else
            txtJobSheetNotes.Text = drAppointment.Item(ApplicationConstants.JobsheetNotesColumn)
        End If

        Dim propertyId As String = drAppointment.Item(ApplicationConstants.PropertyIdColumn)
        ViewState.Add(ViewStateConstants.PropertyId, propertyId)
        Dim type As String = drAppointment.Item(ApplicationConstants.TypeCol)
        ViewState.Add(ViewStateConstants.Type, type)
    End Sub

#End Region

#Region "Populate Property Info"
    ''' <summary>
    ''' Populate Property Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyInfo()

        Dim propertyId As String = ViewState(ViewStateConstants.PropertyId)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

        If (String.IsNullOrEmpty(propertyId)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            Me.disableAllControls()
        Else
            Dim resultDt As DataTable = New DataTable()
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
            resultDt = objPlannedSchedulingBo.PropertyInfoDt

            Dim type As String = ViewState(ViewStateConstants.Type)
            controlsVisibilityByType(type)
            If Not IsNothing(resultDt) AndAlso resultDt.Rows.Count > 0 Then
                'Property Information
                lblScheme.Text = If(IsDBNull(resultDt.Rows(0)("SchemeName")), "NA", resultDt.Rows(0)("SchemeName"))
                lblBlock.Text = If(IsDBNull(resultDt.Rows(0)("Address")), "NA", resultDt.Rows(0)("Address"))
                lblAddress.Text = IIf(IsDBNull(resultDt.Rows(0)("HOUSENUMBER")), "", resultDt.Rows(0)("HOUSENUMBER")) + " " +
                                  IIf(IsDBNull(resultDt.Rows(0)("ADDRESS1")), "", resultDt.Rows(0)("ADDRESS1")) + " " +
                                  IIf(IsDBNull(resultDt.Rows(0)("ADDRESS2")), "", resultDt.Rows(0)("ADDRESS2"))
                lblTowncity.Text = IIf(IsDBNull(resultDt.Rows(0)("TOWNCITY")), "", resultDt.Rows(0)("TOWNCITY"))
                lblCounty.Text = IIf(IsDBNull(resultDt.Rows(0)("COUNTY")), "", resultDt.Rows(0)("COUNTY"))
                lblPostcode.Text = IIf(IsDBNull(resultDt.Rows(0)("POSTCODE")), "", resultDt.Rows(0)("POSTCODE"))

                'Customer Information
                hdnCustomerId.Value = IIf(IsDBNull(resultDt.Rows(0)("CustomerId")), "", resultDt.Rows(0)("CustomerId"))
                lblCustomerName.Text = IIf(IsDBNull(resultDt.Rows(0)("TenantName")), "", resultDt.Rows(0)("TenantName"))
                lblCustomerTelephone.Text = IIf(IsDBNull(resultDt.Rows(0)("Telephone")), "", resultDt.Rows(0)("Telephone"))
                lblCustomerMobile.Text = IIf(IsDBNull(resultDt.Rows(0)("Mobile")), "", resultDt.Rows(0)("Mobile"))
                lblCustomerEmail.Text = IIf(IsDBNull(resultDt.Rows(0)("Email")), "", resultDt.Rows(0)("Email"))

            End If

        End If

    End Sub

#End Region

#Region "Controls Visibility by type"
    Sub controlsVisibilityByType(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblForCustomerName.Visible = False
            lblCustomerName.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            lblForAddress.Visible = False
            lblAddress.Visible = False
            lblTowncity.Visible = False
            lblCounty.Visible = False
            lblPostcode.Visible = False
            lblForTowncity.Visible = False
            lblForCounty.Visible = False
            lblForPostcode.Visible = False
            lblForCustomerTelephone.Visible = False
            lblForCustomerMobile.Visible = False
            lblForCustomerEmail.Visible = False
            LabelFor18.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            Label18.Visible = False
            'btnUpdateCustomerDetails.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
#End Region

#Region "Get current index row from dataset"
    ''' <summary>
    ''' Get current index row from dataset
    ''' </summary>
    ''' <param name="currentIndex"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getCurrentIndexRowFromDataset(ByVal currentIndex As Integer) As DataRow

        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
        Dim dtAppointments As DataTable = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt
        Dim drAppointment() As DataRow
        drAppointment = dtAppointments.Select("Row = " + Convert.ToString(currentIndex))
        Return drAppointment(0)

    End Function

#End Region

#Region "Navigate to Schedule Works"
    ''' <summary>
    ''' Navigate to Schedule Works
    ''' </summary>
    ''' <remarks></remarks>
    Sub navigateToScheduleWorks()
        Response.Redirect(PathConstants.CancelledApointmentReport)
    End Sub

#End Region


#Region "Update notes"
    ''' <summary>
    ''' Update notes
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateNotes()

        Dim objNotesBO As NotesBO = New NotesBO()
        objNotesBO.CustomerNotes = txtCustAppointmentNotes.Text
        objNotesBO.AppointmentNotes = txtJobSheetNotes.Text

        Dim results As ValidationResults = Validation.Validate(objNotesBO)

        If (results.IsValid) Then

            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim isSaved As Integer = 0
            Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
            Dim drAppointment As DataRow = Me.getCurrentIndexRowFromDataset(currentIndex)
            Dim appointmentId As Integer = drAppointment(ApplicationConstants.AppointmentIdColumn)

            isSaved = objSchedulingBL.updatePlannedAppointmentsNotes(txtCustAppointmentNotes.Text, txtJobSheetNotes.Text, appointmentId)

            If (isSaved = 1) Then

                drAppointment(ApplicationConstants.CustomerNotesColumn) = txtCustAppointmentNotes.Text
                drAppointment(ApplicationConstants.JobsheetNotesColumn) = txtJobSheetNotes.Text

                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NotesUpdatedSuccessfully, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemUpdatingNotes, True)
            End If

        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.MaxLengthExceed, True)

        End If


    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#End Region


End Class