﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ToBeArrangedAvailableAppointments.aspx.vb" Inherits="PlannedMaintenance.ToBeArrangedAvailableAppointments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="headingTitle">
        <b>
            <%--Schedule planned works--%>
            <asp:GridView ID="grdPropertyComponentDetail" runat="server" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="White" GridLines="None" ShowHeader="False" Width="100%"
                PageSize="1" RowStyle-Wrap="True">
                <Columns>
                    <asp:TemplateField HeaderText="PMO" ShowHeader="True" ItemStyle-ForeColor="White">
                        <ItemTemplate>
                            PMO<asp:Label ID="lbPmo" runat="server" Text='<%# Eval("PMO") %>'></asp:Label>
                            &nbsp;<asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("PrimaryAddress") %>'></asp:Label>,
                            &nbsp;<asp:Label ID="lbHouseNumberAddress" runat="server" Text='<%# Eval("HouseNumber")+" "+Eval("PrimaryAddress") %>'></asp:Label>,
                            &nbsp;<asp:Label ID="lblPostCode" runat="server" Text='<%# Eval("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </b>
    </div>
    <div>
        <asp:UpdatePanel ID="updPanelMain" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message scheduling-message">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="prop-detail-container" id="propDetailContainer" runat="server" visible="false"
                    enableviewstate="true">
                    <asp:Panel ID="pnlTopButtons" runat="server" CssClass="top-buttons" Visible="false">
                        <asp:Button ID="btnBackToScheduling" runat="server" Text="< Back to Scheduling" PostBackUrl="~/Views/Scheduling/ScheduleWorks.aspx"
                            CssClass="margin-top-right" /><asp:Button ID="btnConfirmAppointments" runat="server"
                                Text="Confirm Appointments" CssClass="margin-top-right" /></asp:Panel>
                    <div style="clear: both;">
                    </div>
                    <asp:Panel ID="pnlBackButton" runat="server" CssClass="top-buttons" Visible="false">
                        <asp:Button ID="btnBack" runat="server" Text="< Back to Scheduling" PostBackUrl="~/Views/Scheduling/ScheduleWorks.aspx"
                            CssClass="margin-top-right" /></asp:Panel>
                    <div style="clear: both;">
                    </div>
                </div>
                <asp:Panel ID="pnlPageContainer" runat="server" CssClass="trade-appointment-blocks-container">
                </asp:Panel>
                <%-- REMOVE FROM SCHEDULING POPUP - Start --%>
                <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
                <asp:ModalPopupExtender ID="mdlPopupRemoveScheduling" runat="server" TargetControlID="btnHidden3"
                    PopupControlID="pnlRemoveScheduling" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                </asp:ModalPopupExtender>
                <asp:Panel ID="pnlRemoveScheduling" runat="server" BackColor="White" Width="350px"
                    Style="font-weight: normal; font-size: 13px; padding: 10px; border: 1px solid black;">
                    <div style="width: 100%; font-weight: bold; text-align: center;">
                        <b>Remove from Scheduling? </b>
                    </div>
                    <div style="clear: both; height: 1px;">
                    </div>
                    <hr />
                    <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                        <asp:Label ID="lblErrorMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="pnlText" runat="server" HorizontalAlign="Center">
                        Are you sure this trade is not required?
                        <br />
                        <br />
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="pnlPopupButton" runat="server" HorizontalAlign="Center">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_click" />
                        &nbsp &nbsp
                        <asp:Button ID="btnNo" runat="server" Text="No" OnClick="btnNo_click" />
                    </asp:Panel>
                    <br />
                </asp:Panel>
                <%-- REMOVE FROM SCHEDULING POPUP - End --%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
