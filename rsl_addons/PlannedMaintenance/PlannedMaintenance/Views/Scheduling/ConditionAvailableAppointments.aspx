﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ConditionAvailableAppointments.aspx.vb" Inherits="PlannedMaintenance.ConditionAvailableAppointments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="c3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelConditionalWork" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="width: 100%;">
                <b>Schedule Condition Works Appointment</b>
            </div>
            <br />
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <div class="mainContainer">
                <div class="leftContainer" id="leftContainer" runat="server">
                    <asp:Panel runat="server" ID="pnlAddMiscWorks">
                        <table style="width: 100%;" cellspacing="10">
                            <tr>
                                <td valign="top" colspan="2">
                                    <b>Address:
                                        <asp:Label ID="lblPropertyAddress" Text="" runat="server" />
                                    </b>
                                    <div style="display: none;">
                                        <asp:HiddenField ID="hdnSelectedPropertyId" runat="server" />
                                    </div>
                                </td>
                            </tr>                            
                            <tr>
                                <td valign="top">
                                    <b>Works required:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWorksRequired" runat="server" TextMode="MultiLine" Height="200px"
                                        Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Operative trade:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTrade" Width="102%" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Duration:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDuration" Width="102%" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Start date:</b>
                                </td>
                                <td>
                                    <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:TextBox ID="txtDate" Width="100%" runat="server"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                        TargetControlID="txtDate" WatermarkText="(Optional)" WatermarkCssClass="searchTextDefault">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="text-align: right;">
                                    <br />
                                    <asp:Button ID="btnFindOperative" runat="server" Text="Find me an operative" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <asp:Panel ID="pnlAvailableOperatives" runat="server" Visible="false" CssClass="rightContainer">
                    <c3:PagingGridView ID="grdAvailableMisc" runat="server" AutoGenerateColumns="False"
                        BorderWidth="0px" BorderStyle="Solid" Width="100%" AllowSorting="false" AllowPaging="False"
                        GridLines="None" HeaderStyle-CssClass="gridfaults-header" CellSpacing="10" HorizontalAlign="Left"
                        CellPadding="2">
                        <Columns>
                            <asp:TemplateField HeaderText="Operative:">
                                <ItemTemplate>
                                    <asp:Label ID="lblOperative" runat="server" Text='<%# Eval("Operative:") %>'></asp:Label>
                                    <asp:Label ID="lblOperativeId" runat="server" Text='<%# Eval("OperativeId") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblStartTime" runat="server" Text='<%# Eval("StartTime") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("Start Date:") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEndTime" runat="server" Text='<%# Eval("EndTime") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("End Date:") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start date:">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDateTime" runat="server" Text='<%# Eval("Start Date:") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="60%" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" OnClick="btnSelect_Click" runat="server" Text="Select" />
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle Font-Bold="True" />
                    </c3:PagingGridView>
                </asp:Panel>
                <asp:Panel ID="pnlRefreshButton" runat="server" Visible="false">
                    <asp:Button ID="btnRefreshList" runat="server" Text="Refresh List" CssClass="misc-refresh-btn" /></asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
