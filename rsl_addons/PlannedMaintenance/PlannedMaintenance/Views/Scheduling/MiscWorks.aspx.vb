﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports PL_BusinessLogic
Imports System.Globalization
Imports PL_BusinessObject

Public Class MiscWorks
    Inherits PageBase

#Region "Properties "
    Public Shared tradeDurationDtBO As TradeDurationDtBO
    ''' <summary>
    ''' this spearator 'll be used to bind the information of appointment with select button
    ''' </summary>
    ''' <remarks></remarks>
    Private appointmentInfoSeparater As String = ":::"
#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
           
            SessionManager.setToScheduling(False)
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim msg = Request.QueryString("msg")
            Dim src = Request.QueryString("src")
            If Not IsPostBack And src Is Nothing Then
                If (Not IsNothing(msg) And msg = "1") Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppoinemtsScheduledSuccessfully, False)
                End If
                'Me.highLightCurrentPageMenuItems()
                SessionManager.removeMiscAppointmentBO()
                SessionManager.removeTradesDataTable()
                Me.populateAllDropdownList()
                'setupControlProperties()
                tradeDurationDtBO = New TradeDurationDtBO()
                pnlAvailableOperatives.Visible = False
                SessionManager.removeMiscAppointmentBO()
                btnAdd.Enabled = True
                btnFindOperative.Enabled = True
            ElseIf (uiMessageHelper.IsError = False) Then

                If src = "MiscJobSheet" Then
                    Me.populateAllDropdownList()
                    btnAdd.Enabled = False
                    btnFindOperative.Enabled = False
                End If
                pnlAvailableOperatives.Visible = True

                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for confirmed trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawConfirmedTradeAppointmentGrid()
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary trade and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawTempTradeAppointmentGrid()


            End If

            Me.SetContractorButtonVisiblity()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region



    Protected Sub ddlType_SelectedIndexChanded(ByVal sender As Object, ByVal e As EventArgs)
        If ddlType.SelectedItem.Text.ToUpper = "Adaptation".ToUpper Then
            trLocation.Visible = True
            'trAdaptation.Style.Remove("display")
            trLocation.Style.Remove("display")
        Else
            trLocation.Visible = False
            trAdaptation.Visible = False

        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanded(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedValue > 0 Then
            trAdaptation.Visible = True

            getLocationAdaptationsByparameterID(ddlLocation.SelectedValue)
        Else

            trAdaptation.Visible = False

        End If
    End Sub

#Region "Find me operative button Event"
    ''' <summary>
    ''' Find me operative button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFindMeOperative_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFindOperative.Click

        Try
            ' If SessionManager.getTradesDataTable().Rows.Count > 0 Then
            SessionManager.removeMiscAppointmentBO()
            SessionManager.removeConfirmMiscAppointmetBlockList()
            findOperative()
            If (uiMessageHelper.IsError = False) Then
                If ddlType.SelectedItem.Text = "Adaptation" Then
                    hdnAdaptationVal.Value = ddlAdaptation.SelectedValue
                    ddlAdaptation.ClearSelection()
                    ddlAdaptation.Items.FindByValue(hdnAdaptationVal.Value).Selected = True
                    If hdnAdaptationVal.Value <> "" Then
                        trAdaptation.Visible = True
                        trLocation.Visible = True
                    End If
                End If

                btnAdd.Enabled = False
                btnFindOperative.Enabled = False
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "btn Select Operative Click"
    Protected Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'This a sample string that will be received on this event
            'Sample ---- operative Id:::Operative Name:::Start Date:::End Date:::Trade Id:::Trade Name:::Trade Duration:::Component Trade Id:::Component Name:::BlockId
            'Values ---- 468:::Jade Burrell:::25/02/2013:::25/02/2013:::1:::Plumber:::2:::4:::Bathroom:::1
            Dim parameters As String = String.Empty
            Dim btnSelectOperative As Button = DirectCast(sender, Button)
            parameters = btnSelectOperative.CommandArgument
            'save the selected appointment information in session
            Me.saveSelectedAppointmentInfo(parameters)
            ''we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
            ''we are removing this from session (if old data exists)
            '  Me.clearTempAndCofirmBlockFromSession()
            ' SessionManager.setAppointmentSummarySource(PathConstants.ToBeArrangedAvailableAppointments)
            If ddlType.SelectedItem.Text = "Adaptation" Then

            End If

            Me.navigatetoMiscWorkJobsheet()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnRefresh.CommandArgument)

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block
            Dim startDate As Date = GeneralHelper.convertDateToUkDateFormat(DateTime.Now.ToShortDateString())
            If Not String.IsNullOrEmpty(txtDate.Text) Then
                startDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text)
            End If
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, startDate)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "Hidden button event to populate dropdown list"
    ''' <summary>
    ''' Hidden button event to populate dropdown list
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Try
            SessionManager.removeMiscAppointmentBO()
            SessionManager.removeConfirmMiscAppointmetBlockList()
            SessionManager.removeTradesDataTable()
            SessionManager.removeTempMiscAppointmetBlockList()
            populatePmoDropdownlist()
            pnlAvailableOperatives.Controls.Clear()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "btn Assign to Contractor Click"

    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssignToContractor.Click
        Try
            assginMiscWorkToContractor()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Assign to Contractor Control Close Button Click"

    Protected Sub assignMiscWorkToContractorCloseButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles assignMiscWorkToContractor.CloseButtonClicked
        Try
            hideAssigntoContractorControl()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "btnAdd click event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            addRequiredOperatives()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grdTradeOperative row deleting event"
    ''' <summary>
    ''' grid row deleting event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdTradeOperative_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTradeOperative.RowDeleting
        Try


            If tradeDurationDtBO.dt IsNot Nothing AndAlso tradeDurationDtBO.dt.Rows.Count > 0 Then
                Dim dr = tradeDurationDtBO.dt.Rows(e.RowIndex)

                Dim tradeID = dr("TradeId")
                Dim duration = dr("Duration")
                tradeDurationDtBO.dt.Rows.RemoveAt(e.RowIndex)
                tradeDurationDtBO.dt.AcceptChanges()

                Dim order As Integer = 1
                For Each dtr As DataRow In tradeDurationDtBO.dt.Rows
                    dtr.Item("SortOrder") = order
                    order = order + 1
                Next
                tradeDurationDtBO.dt.AcceptChanges()

                SessionManager.setTradesDataTable(tradeDurationDtBO.dt)

                grdTradeOperative.DataSource = SessionManager.getTradesDataTable()
                grdTradeOperative.DataBind()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add to Scheduling Event"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddToScheduling_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddToScheduling.Click
        Try
            Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim isSaved As String = String.Empty
            Dim pmo As Integer = Convert.ToInt32(ApplicationConstants.DefaultValue)
            Dim tradesDt As DataTable = New DataTable()



            Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
            objMiscOperativeSearchBO = Me.getMiscOperativeSearchBO()

            'Dim dateTimeText As Date
            ' Dim isValidDate As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)

            Dim message = String.Empty
            Dim isValid As Boolean = True

            objMiscOperativeSearchBO.StartDate = DateTime.ParseExact(objMiscOperativeSearchBO.StartDate, "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
            Dim results As ValidationResults = Validation.Validate(objMiscOperativeSearchBO)

            If Not results.IsValid Then

                For Each result As ValidationResult In results
                    message += result.Message
                    Exit For
                Next
                uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                uiMessageHelper.IsError = True

                isValid = False
                Return
            End If







            objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()
            objMiscAppointmentBO.PropertyId = hdnSelectedPropertyId.Value
            objMiscAppointmentBO.Type = hdnSelectedType.Value
            objMiscAppointmentBO.UserId = SessionManager.getUserEmployeeId()
            objMiscAppointmentBO.CustomerNotes = txtWorksRequired.Text
            objMiscAppointmentBO.AppointmentNotes = txtAppointmentNotes.Text
            objMiscAppointmentBO.AppointmentTypeId = Convert.ToInt32(ddlType.SelectedItem.Value)
            If ddlType.SelectedItem.Text.ToUpper = "Adaptation".ToUpper Then
                If ddlAdaptation.SelectedValue = "" Then
                    objMiscAppointmentBO.AdaptationId = -1
                    ddlAdaptation.SelectedValue = -1
                    objMiscAppointmentBO.AdaptationName = ""
                Else
                    objMiscAppointmentBO.AdaptationId = Convert.ToInt32(ddlAdaptation.SelectedValue)
                    objMiscAppointmentBO.AdaptationName = If(ddlAdaptation.SelectedValue = "-1", "", ddlAdaptation.SelectedItem.Text)
                End If
                If ddlLocation.SelectedValue = "" Then
                    objMiscAppointmentBO.LocationId = -1
                    ddlLocation.SelectedValue = -1
                    objMiscAppointmentBO.LocationName = ""
                Else
                    objMiscAppointmentBO.LocationId = Convert.ToInt32(ddlLocation.SelectedValue)
                    objMiscAppointmentBO.LocationName = If(ddlLocation.SelectedValue = "-1", "", ddlLocation.SelectedItem.Text)
                End If

            Else
                objMiscAppointmentBO.AdaptationName = ""
                objMiscAppointmentBO.LocationName = ""

            End If
            'objMiscAppointmentBO.StartTime = DateTime.Parse(objMiscAppointmentBO.StartDate).ToString("hh:mm tt") 'objMiscAppointmentBO.StartTime.ToString("hh:mm tt")
            'objMiscAppointmentBO.EndTime = DateTime.Parse(objMiscAppointmentBO.EndDate).ToString("hh:mm tt") 'objMiscAppointmentBO.EndTime.ToString("hh:mm tt")
            If SessionManager.getTradesDataTable().Rows.Count > 0 Then
                tradesDt = SessionManager.getTradesDataTable()

                isSaved = objSchedulingBL.addAppointmentToScheduling(objMiscAppointmentBO, pmo, tradesDt)

                If (isSaved.Equals(ApplicationConstants.NotSaved)) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
                End If
                Response.Redirect("~/Views/Scheduling/MiscWorks.aspx?msg=1")
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppoinemtsScheduledSuccessfully, False)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.MiscTradDuration
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region " Hide Assigned to Contractor button "

    Private Sub SetContractorButtonVisiblity()

        Dim result As Boolean
        Dim value As String = ConfigurationManager.AppSettings("ShowAssignedToContractorButton")

        If value IsNot Nothing AndAlso Boolean.TryParse(value, result) Then
            Me.btnAssignToContractor.Visible = result
        End If

    End Sub

#End Region

#Region "Navigate to MiscWork Jobsheet"
    ''' <summary>
    ''' Navigate to MiscWork Jobsheet
    ''' </summary>
    ''' <remarks></remarks>
    Sub navigatetoMiscWorkJobsheet()
        Response.Redirect(PathConstants.MiscJobSheet)
    End Sub
#End Region

#Region "Add Misc Works Functions"

#Region "Populate All drop down list"
    Sub populateAllDropdownList()
        populateTradeDropdownList()
        populateLocationDropdownList()
        populateAppointmentTypeDropdownList()
        populateDurationDropdownList()
        Me.populatePmoDropdownlist()

        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()

        If objMiscAppointmentBO.PropertyId <> String.Empty Then
            hdnSelectedPropertyId.Value = objMiscAppointmentBO.PropertyId
            hdnSelectedPropertyAddress.Value = objMiscAppointmentBO.PropertyAddress
            ''Update for Bug639()
            hdnSelectedType.Value = objMiscAppointmentBO.Type
            hdnPmos.Value = objMiscAppointmentBO.PMO
            ddlType.SelectedValue = objMiscAppointmentBO.AppointmentTypeId
            txtSearch.Text = objMiscAppointmentBO.PropertyAddress
            txtAppointmentNotes.Text = objMiscAppointmentBO.AppointmentNotes.Trim
            txtWorksRequired.Text = objMiscAppointmentBO.CustomerNotes.Trim
            'populateLocationAdaptationsDropdownList()
            If ddlType.SelectedItem.Text = "Adaptation" Then
                trAdaptation.Visible = True
                trLocation.Visible = True
                ddlLocation.SelectedValue = objMiscAppointmentBO.LocationId

                getLocationAdaptationsByparameterID(objMiscAppointmentBO.LocationId)
                ddlAdaptation.SelectedValue = objMiscAppointmentBO.AdaptationId
            End If
            Dim dt As DataTable = New DataTable
            dt = SessionManager.getTradesDataTable()
            grdTradeOperative.DataSource = dt
            grdTradeOperative.DataBind()
        End If
    End Sub
#End Region
#Region "Populate PMO drop down"
    ''' <summary>
    ''' Populate PMO drop down
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePmoDropdownlist()
        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        Dim propertyId As String = String.Empty
        Dim type As String = String.Empty
        If objMiscAppointmentBO.PropertyId <> String.Empty Then
            propertyId = objMiscAppointmentBO.PropertyId
            type = objMiscAppointmentBO.Type
        Else
            propertyId = hdnSelectedPropertyId.Value()
            type = hdnSelectedType.Value
        End If


        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()

        objSchedulingBL.getAssociatedPmos(resultDataSet, propertyId, type)

        ddlPmos.Items.Clear()
        ddlPmos.DataSource = resultDataSet.Tables(0).DefaultView
        ddlPmos.DataValueField = "JournalId"
        ddlPmos.DataTextField = "Description"
        ddlPmos.DataBind()

        Dim newListItem As ListItem
        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            newListItem = New ListItem("Please select", "-1")
            If objMiscAppointmentBO.PMO > 0 Then
                ddlPmos.SelectedValue = objMiscAppointmentBO.PMO
            End If
        Else
            newListItem = New ListItem("No record found", "-1")
        End If

        ddlPmos.Items.Insert(0, newListItem)


    End Sub

#End Region

#Region "Populate all trade"
    ''' <summary>
    ''' Populate all trades
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateTradeDropdownList()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim resultDataSet As DataSet = New DataSet()
        objReplacementBL.getAllTrades(resultDataSet, ApplicationConstants.AllSelectedValue)

        ddlTrade.DataSource = resultDataSet.Tables(0).DefaultView
        ddlTrade.DataValueField = "TradeId"
        ddlTrade.DataTextField = "Description"
        ddlTrade.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlTrade.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate appointment type dropdown list"
    ''' <summary>
    ''' Populate  appointment type dropdown list
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentTypeDropdownList()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        objSchedulingBL.getAppointmentTypes(resultDataSet, 1)

        ddlType.DataSource = resultDataSet.Tables(0).DefaultView
        ddlType.DataValueField = "TypeID"
        ddlType.DataTextField = "TypeValue"
        ddlType.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlType.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate location dropdown list"
    ''' <summary>
    ''' Populate location dropdown list
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateLocationDropdownList()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        objSchedulingBL.getLocations(resultDataSet)

        ddlLocation.DataSource = resultDataSet.Tables(0).DefaultView
        ddlLocation.DataValueField = "ItemID"
        ddlLocation.DataTextField = "ItemName"
        ddlLocation.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlLocation.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDurationDropdownList()
        Dim newListItem As ListItem
        ddlDuration.Items.Clear()

        For i = 1 To 40

            If (i = 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddlDuration.Items.Add(newListItem)
        Next

        newListItem = New ListItem("Please select", "-1")
        ddlDuration.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Setup control properties"

    Sub setupControlProperties()
        'txtDate.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Find Operative"
    ''' <summary>
    ''' Find Operative
    ''' </summary>
    ''' <remarks></remarks>
    Sub findOperative()

        Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objMiscOperativeSearchBO = Me.getMiscOperativeSearchBO()

        'Dim dateTimeText As Date
        ' Dim isValidDate As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)

        Dim message = String.Empty
        Dim isValid As Boolean = True

        Dim results As ValidationResults = Validation.Validate(objMiscOperativeSearchBO)

        If Not results.IsValid Then

            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
            uiMessageHelper.IsError = True

            isValid = False

        ElseIf (Not String.IsNullOrEmpty(txtDate.Text)) Then

            If (Convert.ToDateTime(txtDate.Text) < DateTime.Today()) Then
                message += UserMessageConstants.SelectFutureDate + "<br />"
                uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                uiMessageHelper.IsError = True
                isValid = False
            End If

        End If

        If ddlType.SelectedItem.Text.ToUpper.ToString = "Adaptation".ToUpper Then
            If (ddlLocation.SelectedValue = "-1" Or ddlAdaptation.SelectedValue = "" Or ddlAdaptation.SelectedValue = "-1") Then
                message += UserMessageConstants.SelectLocationAdaptation + "<br />"
                uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                uiMessageHelper.IsError = True
                isValid = False
            End If
        End If

        If (isValid) Then
            If SessionManager.getTradesDataTable().Rows.Count > 0 Then
                Dim resultDataset As DataSet = New DataSet()
                Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
                Dim tempTradeIds As String = String.Empty
                For Each dtr As DataRow In tradeDurationDtBO.dt.Rows
                    'make a comma separated string
                    tempTradeIds += dtr.Item("TradeId") + ","
                Next
                'remove the last comma
                tempTradeIds = tempTradeIds.Remove(tempTradeIds.Length - 1, 1)
                objMiscAppointmentBO.PropertyId = objMiscOperativeSearchBO.PropertyId
                objMiscAppointmentBO.Type = objMiscOperativeSearchBO.SelectionType
                objMiscAppointmentBO.TradeId = tempTradeIds ' objMiscOperativeSearchBO.OperativeTradeId

                objMiscAppointmentBO.Duration = objMiscOperativeSearchBO.Duration
                objMiscAppointmentBO.StartDate = objMiscOperativeSearchBO.StartDate
                Me.setAppointmentInSession(objMiscAppointmentBO)
                resultDataset = Me.getAvailableOperatives()
                Dim tempAptDtBo As TempAppointmentDtBO = New TempAppointmentDtBO()
                Me.populateAvailableOperativesList(resultDataset, tempAptDtBo)
            Else
                pnlAvailableOperatives.Visible = False
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.MiscTradDuration
            End If
        Else
            pnlAvailableOperatives.Visible = False
        End If

    End Sub

#End Region

#Region "Get Misc Operative Search BO"
    ''' <summary>
    ''' Get Misc Operative Search BO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getMiscOperativeSearchBO()

        Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objMiscOperativeSearchBO.PropertyId = hdnSelectedPropertyId.Value
        objMiscOperativeSearchBO.SelectionType = hdnSelectedType.Value
        objMiscOperativeSearchBO.PropertyAddress = txtSearch.Text
        objMiscOperativeSearchBO.Worksrequired = txtWorksRequired.Text
        objMiscOperativeSearchBO.OperativeTradeId = ddlTrade.SelectedValue
        objMiscOperativeSearchBO.Duration = ddlDuration.SelectedValue
        objMiscOperativeSearchBO.Type = ddlType.SelectedValue
        If Not String.IsNullOrEmpty(txtDate.Text) Then
            objMiscOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text)
        Else
            objMiscOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(DateTime.Now.ToShortDateString())
        End If

        Return objMiscOperativeSearchBO
    End Function

#End Region

#Region "Search Property"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getPropertySearchResult(ByVal searchText As String) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        objSchedulingBL.getPropertySearchResult(resultDataset, searchText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        For Each row As DataRow In resultDataset.Tables(1).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        For Each row As DataRow In resultDataset.Tables(2).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        Return result

    End Function

#End Region



#Region "Get Location Adaptations By ParameterId"
    ''' <summary>
    ''' Location Adaptations 
    ''' </summary>
    ''' <param name="parameterID"></param>
    ''' <remarks></remarks>
    Sub getLocationAdaptationsByparameterID(ByVal parameterID As Integer)

        Dim resultDataset As DataSet = New DataSet()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        objSchedulingBL.getLocationAdaptations(resultDataset, parameterID)
        ddlAdaptation.DataSource = resultDataset.Tables(0).DefaultView
        ddlAdaptation.DataValueField = "ValueID"
        ddlAdaptation.DataTextField = "ValueDetail"
        ddlAdaptation.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlAdaptation.Items.Insert(0, newListItem)

    End Sub

#End Region
#End Region

#Region "Available Operatives Functions"

#Region "get Available Operatives"
    ''' <summary>
    ''' this function 'll get the available operatives based on following: 
    ''' trade ids of component, property id, start date 
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getAvailableOperatives() As DataSet
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()

        'this function 'll get the available operatives based on following: trade ids of component, property id, start date 
        schedulingBl.getAvailableOperatives(resultDataSet, objMiscAppointmentBO.TradeId.ToString(), objMiscAppointmentBO.PropertyId, objMiscAppointmentBO.StartDate)
        If resultDataSet.Tables(0).Rows.Count() = 0 Then
            Throw New Exception(UserMessageConstants.OperativesNoFound)
        Else
            SessionManager.setAvailableOperativesDs(resultDataSet)
        End If
        Return resultDataSet
    End Function
#End Region

#Region "Populate available operative"
    ''' <summary>
    ''' Populate available operative
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAvailableOperativesList(ByVal availableOperativesDs As DataSet, ByVal tempAptDtBo As TempAppointmentDtBO)

        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        objMiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        Dim tempMiscAptBoList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
        'this function 'll process the component trades and 'll create the appointments against them
        tempMiscAptBoList = schedulingBl.getAvailableMiscAppointments(availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), _
                                                                       availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), _
                                                                       availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), _
                                                                       tempAptDtBo, _
                                                                       objMiscAppointmentBO.StartDate, _
                                                                       tradeDurationDtBO.dt)


        pnlAvailableOperatives.Visible = True
        pnlRefreshButton.Visible = True
        'save list in session 
        SessionManager.setTempMiscAppointmetBlockList(tempMiscAptBoList)
        Me.bindTempTradesAppointment(tempMiscAptBoList)
    End Sub

#End Region
#Region "bind Temp Trade Appointments"
    ''' <summary>
    ''' This function 'll bind the data with temporary trade and appointments block
    ''' </summary>
    ''' <param name="tempMiscAptBoList"></param>
    ''' <remarks></remarks>
    Private Sub bindTempTradesAppointment(ByVal tempMiscAptBoList As List(Of TempMiscAppointmentBO))
        If IsNothing(tempMiscAptBoList) Then
            ' Me.unHideBackButton()
        Else
            'panel control 
            Dim panelContainer As Panel
            ' We are considering trade and its respective operative list as block so 
            'these variables 'll be used to recognize each block when some button/check box 'll be clicked e.g refresh list button
            Dim uniqueControlId As Integer = 0
            Dim uniqueControlIdString As String = uniqueControlId.ToString()

            'bind this above data with grid view             
            For Each item As TempMiscAppointmentBO In tempMiscAptBoList
                'create container for every block of temporary Trades and appointment 
                panelContainer = Me.createContainer(uniqueControlIdString)
                'Create object of Trade grid
                Dim lblDuration As HtmlGenericControl = New HtmlGenericControl("div")
                lblDuration.InnerText = "Duration: " + item.DurationString
                lblDuration.Attributes.Add("class", "tempTrades")

                Dim lblTrade As HtmlGenericControl = New HtmlGenericControl("div")
                lblTrade.InnerText = "Trade: " + item.Trade
                lblTrade.Attributes.Add("class", "tempTrades")


                panelContainer.Controls.Add(lblDuration)
                panelContainer.Controls.Add(lblTrade)

                'Create object of appointment grid
                Dim tempAptGrid As New GridView()
                'Add the GridView control into the div control/panel
                tempAptGrid = Me.createTempAppointmentGrids(tempAptGrid, uniqueControlIdString, item)
                panelContainer.Controls.Add(tempAptGrid)

                'Add this panel container to div page container
                pnlAvailableOperatives.Controls.Add(panelContainer)

                'this varaible 'll be used to recognize each control
                uniqueControlId = uniqueControlId + 1
                uniqueControlIdString = uniqueControlId.ToString()
            Next
        End If
    End Sub
#End Region
#Region "add More Operatives In List"
    ''' <summary>
    ''' This function 'll add more operatives available appointment slots in the existing list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub addMoreOperativesInList()
        Dim resultDataset As DataSet = New DataSet()
        resultDataset = SessionManager.getAvailableOperativesDs()
        Dim tempAptDtBo As TempAppointmentDtBO = New TempAppointmentDtBO()
        tempAptDtBo = SessionManager.getMiscAvailableOperativesAppointments()
        Me.populateAvailableOperativesList(resultDataset, tempAptDtBo)
    End Sub
#End Region
#Region "create Container"
    ''' <summary>
    ''' This function creates the html container for every block of trade and appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContainer(ByVal uniqueBlockId As String)
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + uniqueBlockId
        divContainer.CssClass = "trade-appointment-block"
        Return divContainer
    End Function
#End Region
#Region "Set Appointment in Session"
    ''' <summary>
    ''' Set Appointment in Session
    ''' </summary>
    ''' <remarks></remarks>
    Sub setAppointmentInSession(ByVal objMiscAppointmentBO As MiscAppointmentBO)
        SessionManager.setMiscAppointmentBO(objMiscAppointmentBO)
    End Sub

#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef tempTradeAptBo As TempMiscAppointmentBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        tempTradeAptBo.tempAppointmentDtBo.dt.Rows.Add(tempTradeAptBo.tempAppointmentDtBo.dt.NewRow())
        grid.DataSource = tempTradeAptBo.tempAppointmentDtBo.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        tempTradeAptBo.tempAppointmentDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region
#Region "create Temp Appointment Grids"
    ''' <summary>
    ''' This function 'll create the temporary appointment grid on the basis of unique block id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createTempAppointmentGrids(ByVal tempAptGrid As GridView, ByVal uniqueBlockId As String, ByVal item As TempMiscAppointmentBO)
        Dim isEmptyGrid As Boolean = False

        tempAptGrid.ID = ApplicationConstants.TempAppointmentGridControlId + uniqueBlockId.ToString()
        tempAptGrid.ShowFooter = True
        tempAptGrid.EnableViewState = True
        tempAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        tempAptGrid.HeaderStyle.CssClass = "available-apptmts-Headerrow"
        'Check if , are there any appointments or not .If not then display message
        If item.tempAppointmentDtBo.dt.Rows.Count = 0 Then
            Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoOperativesExistWithInTime)
            isEmptyGrid = True
        Else
            tempAptGrid.DataSource = item.tempAppointmentDtBo.dt
            tempAptGrid.DataBind()
            'add row span on footer and add back button, refresh list button and view calendar button
            tempAptGrid.FooterRow.Cells(0).Controls.Add(createAppointmentFooterControls(uniqueBlockId))
        End If

        'after adding the buttons add col span to footer so that buttons should appear according to design
        If tempAptGrid.Rows.Count > 0 Then
            Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
            For i As Integer = m - 1 To 1 Step -1
                tempAptGrid.FooterRow.Cells.RemoveAt(i)
            Next i
            tempAptGrid.FooterRow.Cells(0).ColumnSpan = 4 '4 is the number of visible columns to span.
        End If

        'create header of appointment grid and add start date calenar in it
        Dim tradAptGridHeaderCell As TableCell = tempAptGrid.HeaderRow.Cells(3)
        '  tradAptGridHeaderCell.Controls.Add(Me.createAppointmentHeaderControls(uniqueBlockId.ToString(), item.StartSelectedDate))

        'declare the variables that 'll store the information required to bind with select button in grid                
        Dim operativeId As String = String.Empty
        Dim operativeName As String = String.Empty
        Dim startDateString As String = String.Empty
        Dim endDateString As String = String.Empty
        Dim dtCounter As Integer = 0
        Dim completeAppointmentInfo As String = String.Empty

        'remove the header of unwanted columns
        tempAptGrid.HeaderRow.Cells(2).Visible = False
        tempAptGrid.HeaderRow.Cells(4).Visible = False
        tempAptGrid.HeaderRow.Cells(5).Visible = False
        tempAptGrid.HeaderRow.Cells(6).Visible = False
        tempAptGrid.HeaderRow.Cells(7).Visible = False
        tempAptGrid.HeaderRow.Cells(8).Visible = False
        'if grid is not emtpy then remove the cells and also create select button and set command argument with that
        If isEmptyGrid = False Then
            'run the loop on grid view rows to remove the cells from each row 
            'and also to add the select button in each row
            For Each gvr As GridViewRow In tempAptGrid.Rows
                gvr.Cells(2).Visible = False
                gvr.Cells(4).Visible = False
                gvr.Cells(5).Visible = False
                gvr.Cells(6).Visible = False
                gvr.Cells(7).Visible = False
                gvr.Cells(8).Visible = False
                'get the information that should be set with select button                                             
                operativeId = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeIdColName)
                operativeName = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeColName)
                startDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.startDateStringColName)
                endDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.endDateStringColName)
                dtCounter += 1

                'save this operative and time slot information in a string, after that it 'll be bind with select button
                completeAppointmentInfo = operativeId.ToString() _
                    + Me.appointmentInfoSeparater + operativeName _
                    + Me.appointmentInfoSeparater + startDateString _
                    + Me.appointmentInfoSeparater + endDateString _
                    + Me.appointmentInfoSeparater + item.TradeId.ToString() _
                    + Me.appointmentInfoSeparater + item.Trade _
                      + Me.appointmentInfoSeparater + item.Duration _
                    + Me.appointmentInfoSeparater + uniqueBlockId
                '+ Me.appointmentInfoSeparater + item.tempTradeDtBo.componentTradeId.ToString() _
                '+ Me.appointmentInfoSeparater + item.tempTradeDtBo.componentId.ToString() _
                '+ Me.appointmentInfoSeparater + item.tempTradeDtBo.componentName _
                '+ Me.appointmentInfoSeparater + uniqueBlockId

                'Add the select button 
                Dim btnSelectOperative As Button = New Button()
                btnSelectOperative.ID = "btnSelectOperative" + uniqueBlockId.ToString()
                btnSelectOperative.Text = "Select"
                btnSelectOperative.CssClass = "row-button"
                btnSelectOperative.CommandArgument = completeAppointmentInfo
                AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                gvr.Cells(3).Controls.Add(btnSelectOperative)
            Next
        End If
        Return tempAptGrid
    End Function
#End Region

#Region "refresh The Opertives List"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="selectedTradeAptBlockId"></param>    
    ''' <param name="startAgain">This variable 'll identify either appointment slots should be recreated from the selected start date or not</param>
    ''' <remarks></remarks>
    Private Sub refreshTheOpertivesList(ByVal selectedTradeAptBlockId As Integer, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
        'Dim tempTradeAptList As List(Of TempTradeAppointmentBO) = New List(Of TempTradeAppointmentBO)
        'Dim tempTradeAptBo As TempTradeAppointmentBO = New TempTradeAppointmentBO()
        Dim tempMiscAptBo As TempMiscAppointmentBO = New TempMiscAppointmentBO()
        Dim tempMiscAptBoList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)

        tempMiscAptBoList = SessionManager.getTempMiscAppointmetBlockList()
        Dim availableOperativesDs As DataSet = New DataSet()
        'get the specific object from the list of trade& appointments
        tempMiscAptBo = tempMiscAptBoList(selectedTradeAptBlockId)
        'increase the count of operatives by 
        ' tempMiscAptBo.tempAppointmentDtBo.DisplayCount = tempMiscAptBo.DisplayCount + 5
        ''if user didn't change the start date 
        'If startAgain = True Then
        '    tempMiscAptBo.StartSelectedDate = startDate
        'End If
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        'get the available operatives, their leaves, appointments and selected start date
        availableOperativesDs = SessionManager.getAvailableOperativesDs()
        schedulingBl.addMoreTempMiscAppointments(availableOperativesDs.Tables(ApplicationConstants.AvailableOperatives), availableOperativesDs.Tables(ApplicationConstants.OperativesLeaves), availableOperativesDs.Tables(ApplicationConstants.OperativesAppointments), tempMiscAptBo, startDate, startAgain)
        'save in temporary trade appointment list
        tempMiscAptBoList(selectedTradeAptBlockId) = tempMiscAptBo
        ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        ' schedulingBl.orderAppointmentsByTimeAndOperative(tempMiscAptBo)

        ''get the instance of gridview control
        Dim grdTempAppointments As GridView = CType(pnlAvailableOperatives.FindControl("grdTempAppointments" + selectedTradeAptBlockId.ToString()), GridView)
        Me.createTempAppointmentGrids(grdTempAppointments, selectedTradeAptBlockId, tempMiscAptBo)
    End Sub
#End Region
#Region "create Appointment Footer Controls"
    ''' <summary>
    ''' This function 'll create the footer buttons of appointment grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createAppointmentFooterControls(ByVal uniqueBlockId As String)
        Dim buttonsContainer As Panel = New Panel()

        Dim btnRefreshList As Button = New Button()


        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = uniqueBlockId
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click


        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "trade-appointment-buttons"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnRefreshList)
        Return buttonsContainer
    End Function
#End Region

#Region "reDraw Temp Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then
    ''' redraw the controls for temporary trade and appointment list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawTempTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary trade and appointment list
        Dim tempTradeAptList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
        tempTradeAptList = SessionManager.getTempMiscAppointmetBlockList()
        Me.bindTempTradesAppointment(tempTradeAptList)
    End Sub
#End Region

#Region "clear Temp And Cofirm Block From Session"
    ''' <summary>
    ''' we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
    ''' we are removing this from session (if old data exists)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearTempAndCofirmBlockFromSession()
        'removing the temporary appointments from session
        SessionManager.removeTempMiscAppointmetBlockList()

    End Sub
#End Region

#Region "save Selected Appointment Info"
    ''' <summary>
    ''' This function 'll save the selected appointment information in session
    ''' </summary>
    ''' <param name="parameters"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedAppointmentInfo(ByVal parameters As String)

        Dim objMiscAppointmentBO As MiscAppointmentBO = New MiscAppointmentBO()
        Dim separater As String() = {Me.appointmentInfoSeparater}
        Dim appointmentParameters() As String = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries)
        objMiscAppointmentBO.PropertyId = hdnSelectedPropertyId.Value
        objMiscAppointmentBO.Type = hdnSelectedType.Value
        objMiscAppointmentBO.PropertyAddress = hdnSelectedPropertyAddress.Value
        objMiscAppointmentBO.OperativeId = Integer.Parse(appointmentParameters(0))
        objMiscAppointmentBO.OperativeName = appointmentParameters(1)
        objMiscAppointmentBO.StartDate = appointmentParameters(2)
        objMiscAppointmentBO.EndDate = appointmentParameters(3)
        objMiscAppointmentBO.TradeId = Integer.Parse(appointmentParameters(4))
        objMiscAppointmentBO.TradeName = appointmentParameters(5)
        objMiscAppointmentBO.Duration = appointmentParameters(6)
        If Request.QueryString("src") = "MiscJobSheet" Then
            objMiscAppointmentBO.PMO = Convert.ToInt32(hdnPmos.Value)
        Else
            objMiscAppointmentBO.PMO = Convert.ToInt32(ddlPmos.SelectedValue)
        End If


        If ddlType.SelectedItem.Text.ToUpper = "Adaptation".ToUpper Then
            objMiscAppointmentBO.AdaptationId = Convert.ToInt32(ddlAdaptation.SelectedValue)
            objMiscAppointmentBO.AdaptationName = If(ddlAdaptation.SelectedValue = "-1", "", ddlAdaptation.SelectedItem.Text)
            objMiscAppointmentBO.LocationId = Convert.ToInt32(ddlLocation.SelectedValue)
            objMiscAppointmentBO.LocationName = If(ddlLocation.SelectedValue = "-1", "", ddlLocation.SelectedItem.Text)

        Else
            objMiscAppointmentBO.AdaptationName = ""
            objMiscAppointmentBO.LocationName = ""

        End If


        objMiscAppointmentBO.AppointmentNotes = txtAppointmentNotes.Text.Trim
        objMiscAppointmentBO.CustomerNotes = txtWorksRequired.Text.Trim
        objMiscAppointmentBO.AppointmentTypeId = ddlType.SelectedValue
        objMiscAppointmentBO.AppointmentType = ddlType.SelectedItem.Text.Trim()

        'objMiscAppointmentBO.TotalDuration = (Aggregate row As DataRow In objPlannedSchedulingBo.AllTradesDt.AsEnumerable() _
        'Into Sum(row.Field(Of Double)("Duration")))
        objMiscAppointmentBO.Index = appointmentParameters(7) + 1
        Me.setAppointmentInSession(objMiscAppointmentBO)
        ' SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
    End Sub
#End Region

#Region "reDraw Confirmed Trade Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then redraw the controls of confirmed trades and appointment list''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawConfirmedTradeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for confirmed trades and appointment list
        Dim confirmMiscAptBoList As List(Of ConfirmMiscAppointmentBO) = New List(Of ConfirmMiscAppointmentBO)
        confirmMiscAptBoList = SessionManager.getConfirmMiscAppointmetBlockList()
        Me.bindConfirmMiscAppointments(confirmMiscAptBoList)
    End Sub
#End Region
#Region "bind Confirm Trades Appointments"
    Private Sub bindConfirmMiscAppointments(ByVal confirmMiscAptBoList As List(Of ConfirmMiscAppointmentBO))
        If confirmMiscAptBoList.Count() > 0 Then

            Dim counter As Integer = 0
            Dim uniqueBlockId As String = String.Empty
            'panel control 
            Dim panelContainer As Panel
            'bind this above data with grid view             
            For Each item As ConfirmMiscAppointmentBO In confirmMiscAptBoList
                uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString()
                'create container for every block of confirmed trade and appointment 
                panelContainer = Me.createContainer(uniqueBlockId)
                'Create object of Trade grid
                Dim lblDuration As HtmlGenericControl = New HtmlGenericControl("div")
                lblDuration.InnerText = "Duration: " + item.DurationString
                lblDuration.Attributes.Add("class", "tempTrades")

                Dim lblTrade As HtmlGenericControl = New HtmlGenericControl("div")
                lblTrade.InnerText = "Trade: " + item.Trade
                lblTrade.Attributes.Add("class", "tempTrades")


                panelContainer.Controls.Add(lblDuration)
                panelContainer.Controls.Add(lblTrade)
                'Add the GridView control object dynamically into the parent control
                'Dim confirmedTradeGrid As New GridView()
                'panelContainer.Controls.Add(createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, item))

                'Add the GridView control object dynamically into the div control
                'Create object of appointment grid
                Dim confirmedAptGrid As New GridView()
                panelContainer.Controls.Add(createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, item))
                'Add this panel container to div page container
                pnlAvailableOperatives.Controls.Add(panelContainer)
                counter = counter + 1
            Next
        End If

    End Sub
#End Region
#Region "Create Confirmed Appointment Grids"
    ''' <summary>
    ''' This function creates the grid control containing confirmed appointments
    ''' </summary>
    ''' <param name="confirmedAptGrid"></param>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="confirmMiscAptBo"></param>
    ''' ''' <param name="hideRearrnageButton"> 'this value 'll always be false excetp when all appointments 'll have "Arranged" status</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedAppointmentGrids(ByVal confirmedAptGrid As GridView, ByVal uniqueBlockId As String, ByVal confirmMiscAptBo As ConfirmMiscAppointmentBO, Optional ByVal hideRearrnageButton As Boolean = False)

        confirmedAptGrid.ID = ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId
        confirmedAptGrid.EnableViewState = True
        confirmedAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        confirmedAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedAptGrid.DataSource = confirmMiscAptBo.tempAppointmentDtBo.dt
        confirmedAptGrid.DataBind()
        'hide the unwanted columns from the grid
        confirmedAptGrid.HeaderRow.Cells(3).Visible = False
        confirmedAptGrid.HeaderRow.Cells(4).Visible = False
        confirmedAptGrid.HeaderRow.Cells(5).Visible = False
        confirmedAptGrid.HeaderRow.Cells(6).Visible = False
        confirmedAptGrid.HeaderRow.Cells(7).Visible = False
        confirmedAptGrid.HeaderRow.Cells(8).Visible = False
        Dim gvr As GridViewRow = confirmedAptGrid.Rows(0)
        gvr.Cells(3).Visible = False
        gvr.Cells(4).Visible = False
        gvr.Cells(5).Visible = False
        gvr.Cells(6).Visible = False
        gvr.Cells(7).Visible = False
        gvr.Cells(8).Visible = False
        Return confirmedAptGrid
    End Function
#End Region

#Region "Create Confirmed Trade Grids"
    ''' <summary>
    ''' This function 'll create the grid control containing component and trade information of of confirmed appointments
    ''' </summary>
    ''' <param name="confirmedTradeGrid"></param>
    ''' <param name="uniqueControlId"></param>
    ''' <param name="confirmTradeAptBo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createConfirmedTradeGrids(ByVal confirmedTradeGrid As GridView, ByVal uniqueControlId As String, ByVal confirmTradeAptBo As ConfirmMiscAppointmentBO)
        'Create object of trade grid
        confirmedTradeGrid.ID = ApplicationConstants.ConfirmTradeGridControlId + uniqueControlId
        confirmedTradeGrid.EnableViewState = True
        confirmedTradeGrid.CssClass = "grid_temp_trade"
        confirmedTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        confirmedTradeGrid.DataSource = confirmTradeAptBo.tempAppointmentDtBo.dt
        confirmedTradeGrid.DataBind()

        Return confirmedTradeGrid
    End Function
#End Region
#End Region



#Region "Assign Misc Work To Contractor"

    Private Sub assginMiscWorkToContractor()
        If hdnSelectedPropertyId.Value = String.Empty OrElse txtSearch.Text.Trim = String.Empty Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectaProperty, True)
        Else
            leftContainer.Style.Item("border-style") = BorderStyle.None.ToString()
            leftContainer.Style.Item("border-width") = "0px"
            leftContainer.Style.Item("width") = "600px"

            setavailableMiscOperativesControlVisibleStatusViewState(pnlAvailableOperatives.Visible)
            pnlAvailableOperatives.Visible = False

            setrefreshButtonVisibleStatusViewState(pnlRefreshButton.Visible)
            pnlRefreshButton.Visible = False

            pnlAddMiscWorks.Visible = False

            assignMiscWorkToContractor.SetCloseButtonText = "< Back"
            If ddlType.SelectedItem.Text.ToUpper = "Adaptation".ToUpper Then
                assignMiscWorkToContractor.PopulateControl(hdnSelectedPropertyId.Value, AssignToContractorType.Adaptation, -1, txtWorksRequired.Text, hdnSelectedType.Value)
            Else
                assignMiscWorkToContractor.PopulateControl(hdnSelectedPropertyId.Value, AssignToContractorType.Miscellaneous, -1, txtWorksRequired.Text, hdnSelectedType.Value)
            End If

            assignMiscWorkToContractor.Visible = True

            updPanelMiscWork.Update()
        End If
    End Sub

#End Region

#Region "hide Assign to Contractor Control"

    Private Sub hideAssigntoContractorControl()
        If assignMiscWorkToContractor.IsSaved Then
            Response.Redirect("~/Views/Scheduling/MiscWorks.aspx")
        Else
            leftContainer.Style.Remove("border-style")
            leftContainer.Style.Remove("border-width")
            leftContainer.Style.Remove("width")

            pnlAddMiscWorks.Visible = True

            pnlRefreshButton.Visible = getrefreshButtonVisibleStatusViewState()
            pnlAvailableOperatives.Visible = getavailableMiscOperativesControlVisibleStatusViewState()

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            assignMiscWorkToContractor.Visible = False

            updPanelMiscWork.Update()
        End If
    End Sub

#End Region


#Region "Add Trade to Trade Grid "
    ''' <summary>
    ''' Add Trade to Trade Grid
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub addRequiredOperatives()
        Dim dt As DataTable = New DataTable()

        Dim objOperativeListBO As OperativeListBO = New OperativeListBO()
        objOperativeListBO.TradeId = ddlTrade.SelectedValue
        objOperativeListBO.Duration = ddlDuration.SelectedValue
        objOperativeListBO.SOrder = grdTradeOperative.Rows.Count + 1

        Dim results As ValidationResults = Validation.Validate(objOperativeListBO)
        If results.IsValid Then
            If SessionManager.getTradesDataTable().Rows.Count > 0 Then
                dt = SessionManager.getTradesDataTable()
            Else
                tradeDurationDtBO = New TradeDurationDtBO()
                dt = tradeDurationDtBO.dt
            End If

            'Create a new row
            ' Dim dr = dt.NewRow

            If ddlTrade.SelectedValue <> "" And ddlTrade.SelectedValue <> -1 Then
                tradeDurationDtBO.tradeId = ddlTrade.SelectedValue
                tradeDurationDtBO.tradeName = ddlTrade.SelectedItem.Text
                tradeDurationDtBO.duration = ddlDuration.SelectedValue
                tradeDurationDtBO.durationString = ddlDuration.SelectedItem.Text
                tradeDurationDtBO.sOrder = dt.Rows.Count + 1


            End If
            Dim operativeResult = (From ps In dt Where ps.Item("TradeId").ToString() = ddlTrade.SelectedValue Select ps)

            'Start - Check if Trade is Already in the Trades Table/Grid If yes Show Message else Add
            If operativeResult.Count > 0 Then
                Dim ErrorMesg = UserMessageConstants.TradeAlreadyAdded '"The Trade has already been added."
                uiMessageHelper.setMessage(lblMessage, pnlMessage, ErrorMesg, True)
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                ' dt.Rows.Add(dr)
                tradeDurationDtBO.addNewDataRow()

                SessionManager.setTradesDataTable(dt)
                grdTradeOperative.DataSource = dt
                grdTradeOperative.DataBind()
                'End - Check if Trade is already in Trades from database.
            End If
        Else
            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message

            Next
            uiMessageHelper.IsError = True
            uiMessageHelper.message = message
        End If
    End Sub

#End Region

#End Region

#Region "View State Function"

#Region "Get/Set/Remove availableMiscOperativesControl Visible Status View State"

    Private Function getavailableMiscOperativesControlVisibleStatusViewState() As Boolean
        Dim visibleStatus As Boolean = False

        If Not IsNothing(ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus)) Then
            visibleStatus = CType(ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus), Boolean)
        End If
        Return visibleStatus
    End Function

    Private Sub setavailableMiscOperativesControlVisibleStatusViewState(ByVal visbleStatus As Boolean)
        ViewState(ViewStateConstants.availableMiscOperativesControlVisibleStatus) = visbleStatus
    End Sub

    Private Sub removeavailableMiscOperativesControlVisibleStatusViewState()
        ViewState.Remove(ViewStateConstants.availableMiscOperativesControlVisibleStatus)
    End Sub

#End Region

#Region "Get/Set/Remove refresh button Visible Status View State"

    Private Function getrefreshButtonVisibleStatusViewState() As Boolean
        Dim visibleStatus As Boolean = False

        If Not IsNothing(ViewState(ViewStateConstants.refreshButtonVisibleStatus)) Then
            visibleStatus = CType(ViewState(ViewStateConstants.refreshButtonVisibleStatus), Boolean)
        End If
        Return visibleStatus
    End Function

    Private Sub setrefreshButtonVisibleStatusViewState(ByVal visbleStatus As Boolean)
        ViewState(ViewStateConstants.refreshButtonVisibleStatus) = visbleStatus
    End Sub

    Private Sub removerefreshButtonVisibleStatusViewState()
        ViewState.Remove(ViewStateConstants.refreshButtonVisibleStatus)
    End Sub

#End Region

#Region "Existing Trades DataTable View State Get, Set and Remove Method/Function(s)"

    Protected Sub setExistingTradesDataTableViewState(ByRef Tradesdt As DataTable)
        ViewState(ViewStateConstants.ExistingTradesDataTable) = Tradesdt
    End Sub

    Protected Function getExistingTradesDataTableViewState() As DataTable
        If ViewState(ViewStateConstants.ExistingTradesDataTable) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.ExistingTradesDataTable), DataTable)
        Else
            Return New DataTable()
        End If
    End Function

    Protected Sub removeExistingTradesDataTableViewState()
        ViewState.Remove(ViewStateConstants.ExistingTradesDataTable)
    End Sub

#End Region


#End Region

End Class