﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports System.Globalization

Public Class MiscJobSheet
    Inherits PageBase

#Region "Properties"

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            'Me.highLightCurrentPageMenuItems()
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                prePopulatedValues()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"
    ''' <summary>
    ''' Button Update Customer Detail Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateCustomerDetails_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetails.Click
        Try
            updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Schedule Appointment Click Event"
    ''' <summary>
    ''' Button Schedule Appointment Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub btnScheduleAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleAppointment.Click
        Try

            Dim objNotesBO As NotesBO = New NotesBO()
            objNotesBO.CustomerNotes = txtCustAppointmentNotes.Text
            objNotesBO.AppointmentNotes = txtJobSheetNotes.Text

            Dim results As ValidationResults = Validation.Validate(objNotesBO)

            If (results.IsValid) Then
                scheduleAppointment()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Back Click Event"
    ''' <summary>
    ''' Button Back Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            navigateToMiscWorks()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Function"

#Region "Navigate to Misc Works"
    ''' <summary>
    ''' Navigate to Misc Works
    ''' </summary>
    ''' <remarks></remarks>
    Sub navigateToMiscWorks()
        ' SessionManager.removeMiscAppointmentBO()
        If SessionManager.getToScheduling() = True Then
            Response.Redirect(PathConstants.ToBeArrangedAvailableAppointments)
        End If
        Dim tempMiscAptList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
        tempMiscAptList = SessionManager.getTempMiscAppointmetBlockList()
        If tempMiscAptList.Count > 0 Then
            Response.Redirect(PathConstants.BackToMiscWorks)
        Else
            Response.Redirect(PathConstants.MiscWorksPath)
        End If

    End Sub

#End Region

#Region "Schedule Appointment"
    ''' <summary>
    ''' Schedule Appointment
    ''' </summary>
    ''' <remarks></remarks>
    Sub scheduleAppointment()
        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        'Dim confirmMiscAptBoList As List(Of ConfirmMiscAppointmentBO) = New List(Of ConfirmMiscAppointmentBO)
        'confirmMiscAptBoList = SessionManager.getConfirmMiscAppointmetBlockList()
        Dim confirmMiscAppointmentBO As ConfirmMiscAppointmentBO = New ConfirmMiscAppointmentBO()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim isSaved As String = String.Empty
        Dim pmo As Integer = Convert.ToInt32(ApplicationConstants.DefaultValue)

        Dim _StartDate As Date
        Dim _AppointmentStartDateTime As String, _AppointmentEndDateTime As String, _propertyAddress As String, _appointmentType As String
        Dim _operativeId As Integer

        If (IsNothing(objMiscAppointmentBO)) Then

            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False

        Else
            objMiscAppointmentBO.CustomerNotes = txtCustAppointmentNotes.Text
            objMiscAppointmentBO.AppointmentNotes = txtJobSheetNotes.Text
            objMiscAppointmentBO.StartTime = DateTime.Parse(objMiscAppointmentBO.StartDate).ToString("hh:mm tt") 'objMiscAppointmentBO.StartTime.ToString("hh:mm tt")
            objMiscAppointmentBO.EndTime = DateTime.Parse(objMiscAppointmentBO.EndDate).ToString("hh:mm tt") 'objMiscAppointmentBO.EndTime.ToString("hh:mm tt")

            isSaved = objSchedulingBL.scheduleMiscWorkAppointment(objMiscAppointmentBO, pmo, SessionManager.getToScheduling())

            If (isSaved.Equals(ApplicationConstants.NotSaved)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
            Else
                _StartDate = objMiscAppointmentBO.StartDate

                If (_StartDate.ToString("dd/MM/yyyy") = Today.ToString("dd/MM/yyyy")) Then
                    _AppointmentStartDateTime = objMiscAppointmentBO.StartDate.ToString()
                    _AppointmentEndDateTime = objMiscAppointmentBO.EndDate.ToString()
                    _propertyAddress = objMiscAppointmentBO.PropertyAddress
                    _operativeId = objMiscAppointmentBO.OperativeId
                    _appointmentType = objMiscAppointmentBO.AppointmentType

                    GeneralHelper.pushNotificationPlanned(_appointmentType, Convert.ToDateTime(_AppointmentStartDateTime), Convert.ToDateTime(_AppointmentEndDateTime),
                                                          _propertyAddress, _operativeId)
                End If

                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
                btnScheduleAppointment.Enabled = False
                btnUpdateCustomerDetails.Enabled = False
                txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
                txtJobSheetNotes.Attributes.Add("readonly", "readonly")
                hid_isAppointmentCreated.Value = 1
                objMiscAppointmentBO.PMO = pmo
                lblPmo.Text = pmo


                SessionManager.setMiscAppointmentBO(objMiscAppointmentBO)
                ' set confirm appointment in session
                confirmMiscAppointmentBO.PMO = pmo
                confirmMiscAppointmentBO.Trade = objMiscAppointmentBO.TradeName
                confirmMiscAppointmentBO.TradeId = objMiscAppointmentBO.TradeId
                confirmMiscAppointmentBO.Duration = objMiscAppointmentBO.Duration
                confirmMiscAppointmentBO.DurationString = GeneralHelper.appendHourLabel(objMiscAppointmentBO.Duration)

                confirmMiscAppointmentBO.tempAppointmentDtBo.operativeId = objMiscAppointmentBO.OperativeId
                confirmMiscAppointmentBO.tempAppointmentDtBo.operative = objMiscAppointmentBO.OperativeName
                confirmMiscAppointmentBO.tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(objMiscAppointmentBO.StartTime)
                confirmMiscAppointmentBO.tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(objMiscAppointmentBO.EndTime)
                confirmMiscAppointmentBO.tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(objMiscAppointmentBO.StartDate)
                confirmMiscAppointmentBO.tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(objMiscAppointmentBO.EndDate)
                confirmMiscAppointmentBO.tempAppointmentDtBo.tradeId = objMiscAppointmentBO.TradeId
                confirmMiscAppointmentBO.tempAppointmentDtBo.addNewDataRow()
                'confirmMiscAptBoList.Add(confirmMiscAppointmentBO)
                'SessionManager.setConfirmMiscAppointmetBlockList(confirmMiscAptBoList)

                'remove tem misc appointment from session which has been completed
                Dim tempMiscAptList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
                Dim updatedTempMiscAptList As List(Of TempMiscAppointmentBO) = New List(Of TempMiscAppointmentBO)
                tempMiscAptList = SessionManager.getTempMiscAppointmetBlockList()

                For Each item As TempMiscAppointmentBO In tempMiscAptList
                    If item.TradeId <> objMiscAppointmentBO.TradeId Then
                        updatedTempMiscAptList.Add(item)
                    End If
                Next
                'set updated misc appointment in session
                SessionManager.setTempMiscAppointmetBlockList(updatedTempMiscAptList)
            End If
        End If
    End Sub

#End Region

#Region "Pre Populate Values"
    ''' <summary>
    ''' Pre Populate Values
    ''' </summary>
    ''' <remarks></remarks>
    Sub prePopulatedValues()
        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()

        If (String.IsNullOrEmpty(objMiscAppointmentBO.PropertyId)) Then

            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False

        Else
            Me.populateAppointmentInfo()
            Me.populatePropertyInfo()
            Me.populateNotes()
            Me.checkAppointmentCreated()            
        End If

    End Sub

#End Region

#Region "Check appointment created."
    ''' <summary>
    ''' Check appointment created.
    ''' </summary>
    ''' <remarks></remarks>
    Sub checkAppointmentCreated()

        If (Not hid_isAppointmentCreated.Value.Equals("0")) Then
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False
            txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
            txtJobSheetNotes.Attributes.Add("readonly", "readonly")
        End If

    End Sub

#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' Populate Appointment Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentInfo()
        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()

        If (objMiscAppointmentBO.PMO = Convert.ToInt32(ApplicationConstants.DefaultValue)) Then
            lblPmo.Text = ApplicationConstants.NotAvailable
        Else
            lblPmo.Text = objMiscAppointmentBO.PMO
        End If
        If objMiscAppointmentBO.AppointmentType.ToUpper = "Misc".ToUpper Then
            lblAppointmentType.Text = "Planned works component: Miscellaneous works"
        Else
            lblAppointmentType.Text = "Planned works component: Aids & Adaptations"
        End If
        lblTrade.Text = objMiscAppointmentBO.TradeName
        lblOperative.Text = objMiscAppointmentBO.OperativeName
        lblDuration.Text = GeneralHelper.appendHourLabel(objMiscAppointmentBO.Duration)
        lblDurationTotal.Text = GeneralHelper.appendHourLabel(objMiscAppointmentBO.Duration)
        lblAdaptation.Text = objMiscAppointmentBO.AdaptationName
        lblLocation.Text = objMiscAppointmentBO.LocationName
        'lblTime.Text = DateTime.Parse(objMiscAppointmentBO.StartTime).ToString("HH:mm") + " - " + DateTime.Parse(objMiscAppointmentBO.EndTime).ToString("HH:mm")
        Dim startDateTime As DateTime = Convert.ToDateTime(objMiscAppointmentBO.StartDate + " " + objMiscAppointmentBO.StartTime)
        Dim endDateTime As DateTime = Convert.ToDateTime(objMiscAppointmentBO.EndDate + " " + objMiscAppointmentBO.EndTime)

        lblStartDate.Text = Convert.ToDateTime(objMiscAppointmentBO.StartDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime)
        lblEndDate.Text = Convert.ToDateTime(objMiscAppointmentBO.EndDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime)

    End Sub

#End Region

#Region "Populate Property Info"
    ''' <summary>
    ''' Populate Property Info
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyInfo()

        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim propertyId As String = objMiscAppointmentBO.PropertyId
        objMiscAppointmentBO.UserId = SessionManager.getUserEmployeeId()

        If (String.IsNullOrEmpty(propertyId)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False
        Else
            Dim resultDataset As DataSet = New DataSet()

            If (objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                objSchedulingBL.getPropertyCustomerDetail(resultDataset, propertyId)
            Else
                objSchedulingBL.getAddressByType(resultDataset, propertyId, objMiscAppointmentBO.Type)
            End If

            'objSchedulingBL.getPropertyCustomerDetail(resultDataset, propertyId)
            Dim resultDt As DataTable = resultDataset.Tables(0)



            If (resultDt.Rows.Count = 0 And objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.TenantNotExist, True)
                btnScheduleAppointment.Enabled = False
                btnUpdateCustomerDetails.Enabled = False
            Else
                controlsVisibilityByType(objMiscAppointmentBO.Type)
                lblScheme.Text = resultDt.Rows(0)("SchemeName")
                'Property Information
                If (objMiscAppointmentBO.Type = ApplicationConstants.blockType Or objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                    lblBlock.Text = resultDt.Rows(0)("Block")
                End If
                'lblScheme.Text = resultDt.Rows(0)("SchemeName")
                'lblBlock.Text = resultDt.Rows(0)("Block")
                If (objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                    lblAddress.Text = resultDt.Rows(0)("HOUSENUMBER") + " " + resultDt.Rows(0)("ADDRESS1") + " " + resultDt.Rows(0)("ADDRESS2")
                    lblTowncity.Text = resultDt.Rows(0)("TOWNCITY")
                    lblCounty.Text = resultDt.Rows(0)("COUNTY")
                    lblPostcode.Text = resultDt.Rows(0)("POSTCODE")

                    'Customer Information
                    hdnCustomerId.Value = resultDt.Rows(0)("CustomerId")
                    lblCustomerName.Text = If(Not resultDt.Rows(0)("TenantName") = String.Empty, resultDt.Rows(0)("TenantName"), "NA")
                    lblCustomerTelephone.Text = If(Not IsDBNull(resultDt.Rows(0)("Telephone")), resultDt.Rows(0)("Telephone"), "NA")
                    lblCustomerMobile.Text = If(Not IsDBNull(resultDt.Rows(0)("Mobile")), resultDt.Rows(0)("Mobile"), "NA")
                    lblCustomerEmail.Text = If(Not IsDBNull(resultDt.Rows(0)("Email")), resultDt.Rows(0)("Email"), "NA")
                    objMiscAppointmentBO.TenancyId = resultDt.Rows(0)("TenancyId")
                End If


            End If

        End If

        SessionManager.setMiscAppointmentBO(objMiscAppointmentBO)

    End Sub

#End Region

#Region "Controls Visibility by type"
    Sub controlsVisibilityByType(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblForCustomerName.Visible = False
            lblCustomerName.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            lblForAddress.Visible = False
            lblAddress.Visible = False
            lblTowncity.Visible = False
            lblCounty.Visible = False
            lblPostcode.Visible = False
            lblForTowncity.Visible = False
            lblForCounty.Visible = False
            lblForPostcode.Visible = False
            lblForCustomerTelephone.Visible = False
            lblForCustomerMobile.Visible = False
            lblForCustomerEmail.Visible = False
            LabelFor4.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            Label4.Visible = False
            btnUpdateCustomerDetails.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
#End Region

#Region "Populate Notes"
    ''' <summary>
    ''' Populate Notes
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateNotes()
        Dim objMiscAppointmentBO As MiscAppointmentBO = SessionManager.getMiscAppointmentBO()
        txtCustAppointmentNotes.Text = objMiscAppointmentBO.AppointmentNotes
        If objMiscAppointmentBO.WorksRequired = "" Then
            txtJobSheetNotes.Text = objMiscAppointmentBO.CustomerNotes
        Else
            txtJobSheetNotes.Text = objMiscAppointmentBO.WorksRequired + " " + objMiscAppointmentBO.CustomerNotes
        End If

    End Sub

#End Region

#Region "Update Customer Address"
    ''' <summary>
    ''' Update Customer Address
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateAddress()

        'Update Enable
        If btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges

        Else
            'Save Changes
            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                'If Not isError Then
                Dim objCustomerBL As CustomerBL = New CustomerBL()
                Dim objCustomerBO As CustomerBO = New CustomerBO()

                objCustomerBO.CustomerId = hdnCustomerId.Value
                objCustomerBO.Telephone = txtCustomerTelephone.Text
                objCustomerBO.Mobile = txtCustomerMobile.Text
                objCustomerBO.Email = txtCustomerEmail.Text

                Dim results As ValidationResults = Validation.Validate(objCustomerBO)
                If results.IsValid Then


                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

                    End If
                Else
                    Dim message = String.Empty
                    For Each result As ValidationResult In results
                        message += result.Message
                        Exit For
                    Next
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                End If
                'End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

            End If

        End If

    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnMiscWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#End Region
End Class