﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master" CodeBehind="ScheduleConditionWorks.aspx.vb" Inherits="PlannedMaintenance.ScheduleConditionWorks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="AppointmentToBeArranged" Src="~/Controls/Condition-Rating/ConditionalToBeArranged.ascx" %>
<%@ Register TagPrefix="uc2" TagName="AppointmentArranged" Src="~/Controls/Condition-Rating/ConditionalArranged.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .message
        {
            padding-left:12px; 
        }        
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }
        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelScheduleWorks" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="padding-left:11px;">
                <b>Schedule Condition Works</b>
            </div>
            <br />
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <table style="border: thin solid #000000; width: 100%;">
                <tr>
                    <td style="padding-left: 8px; padding-right: 1px; padding-top: 20px;">
                        <asp:LinkButton ID="lnkBtnAppointmentToBeArrangedTab" OnClick="lnkBtnAppointmentToBeArrangedTab_Click"
                            CssClass="TabInitial" runat="server" BorderStyle="Solid">Appointments to be arranged: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnAppointmentsArrangedTab" OnClick="lnkBtnAppointmentsArrangedTab_Click"
                            CssClass="TabInitial" runat="server" BorderStyle="Solid">Appointments arranged: </asp:LinkButton>
                        <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right">                            
                            <asp:TextBox ID="txtSearch" runat="server" Width="200px" CssClass="searchbox" Style="float: none;
                                margin-left: 10px; margin-right: 30px;" onkeyup="TypingInterval();" AutoPostBack="false"
                                AutoCompleteType="Search"></asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                        </asp:Panel>
                        <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                            width: 100%;">
                        </div>
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <uc1:AppointmentToBeArranged ID="AppointmentToBeArranged" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <uc2:AppointmentArranged ID="AppointmentArranged" runat="server" />
                            </asp:View>                           
                        </asp:MultiView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>