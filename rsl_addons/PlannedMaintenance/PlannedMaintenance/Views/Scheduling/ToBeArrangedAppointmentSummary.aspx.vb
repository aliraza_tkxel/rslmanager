﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic

Public Class ToBeArrangedAppointmentSummary
    Inherits PageBase

    Private _requestPageName As String
    Public Property RequestPageName() As String
        Get
            Return _requestPageName
        End Get
        Set(ByVal value As String)
            _requestPageName = value
        End Set
    End Property

#Region "Page Pre Render"
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.setUrlOfNextAvailableAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Load"
    ''' <summary>
    ''' page load function
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
            End If
            Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
            objPlannedAppointmentBo = SessionManager.getPlannedAppointmentBO()
            objPlannedAppointmentBo.UserId = SessionManager.getUserEmployeeId()
            SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
            Me.populateControls()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Schedule Appointment Click Event"
    ''' <summary>
    ''' Button Schedule Appointment Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnScheduleAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleAppointment.Click
        Try

            ' If (Me.validateNotes()) Then
            Me.scheduleAppointment()
            'End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Functions"

#Region "Populate Controls"
    ''' <summary>
    ''' This function 'll populate the user control from the data in session and the data from database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateControls()
        'populate the property related controls 
        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo = SessionManager.getPlannedAppointmentBO()
        Dim propertyId As String = objPlannedAppointmentBo.PropertyId
        Dim tenancyId As Integer = Me.appointmentSummaryControl.populatePropertyInfo(propertyId)
        objPlannedAppointmentBo.TenancyId = tenancyId
        txtCustAppointmentNotes.Text = objPlannedAppointmentBo.CustomerNotes
        txtJobSheetNotes.Text = objPlannedAppointmentBo.AppointmentNotes
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
        'populate the appointment related controls
        Me.populateAppointmentInfo()
    End Sub
#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' This function 'll pouplate the object of appointment bo from planned appointment bo and then it passess appointment bo
    ''' to user control, to pouplate control
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateAppointmentInfo()

        Dim objPlannedAppointmentBO As PlannedAppointmentBO = SessionManager.getPlannedAppointmentBO()
        lblSheetNumberTitle.Text = 1
        lblTotalSheets.Text = 1
        Dim objAppointmentBo As AppointmentBO = New AppointmentBO()
        objAppointmentBo.Pmo = objPlannedAppointmentBO.PMO.ToString()
        objAppointmentBo.OperativeName = objPlannedAppointmentBO.OperativeName
        objAppointmentBo.ComponentName = objPlannedAppointmentBO.ComponentName
        objAppointmentBo.TradeName = objPlannedAppointmentBO.TradeName
        objAppointmentBo.Duration = objPlannedAppointmentBO.Duration.ToString() + " day(s)"
        objAppointmentBo.TotalDuration = objPlannedAppointmentBO.TotalDuration.ToString() + " day(s)"
        objAppointmentBo.Time = GeneralHelper.get24HourTimeFormatFromDate(objPlannedAppointmentBO.StartDate) + "-" + GeneralHelper.get24HourTimeFormatFromDate(objPlannedAppointmentBO.EndDate)
        objAppointmentBo.StartDate = GeneralHelper.getDateWithWeekdayFormat(objPlannedAppointmentBO.StartDate)
        objAppointmentBo.EndDate = GeneralHelper.getDateWithWeekdayFormat(objPlannedAppointmentBO.EndDate)
        Me.appointmentSummaryControl.populateAppointmentInfo(objAppointmentBo)

    End Sub
#End Region

#Region "Set Url of Next Available Appointment"
    ''' <summary>
    ''' Navigate to Next Available Appointment
    ''' </summary>
    ''' <remarks></remarks>
    Sub setUrlOfNextAvailableAppointment()
        If SessionManager.getAppointmentSummarySource() = PathConstants.ToBeArrangedAvailableAppointments Then
            btnBack.PostBackUrl = PathConstants.ToBeArrangedAvailableAppointments
        ElseIf SessionManager.getAppointmentSummarySource() = PathConstants.ArrangedAvailableAppointments Then
            btnBack.PostBackUrl = PathConstants.ArrangedAvailableAppointments
        End If

    End Sub

#End Region

#Region "Schedule Appointment"
    ''' <summary>
    ''' Schedule Appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub scheduleAppointment()

        Dim objPlannedAppointmentBo As PlannedAppointmentBO = SessionManager.getPlannedAppointmentBO()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim isSaved As String = String.Empty
        Dim appointmentId As Integer = Convert.ToInt32(ApplicationConstants.DefaultValue)

        If (IsNothing(objPlannedAppointmentBo)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            btnScheduleAppointment.Enabled = False
        Else
            objPlannedAppointmentBo.CustomerNotes = txtCustAppointmentNotes.Text
            objPlannedAppointmentBo.AppointmentNotes = txtJobSheetNotes.Text
            objPlannedAppointmentBo.StartTime = objPlannedAppointmentBo.StartDate.ToString("hh:mm tt")
            objPlannedAppointmentBo.EndTime = objPlannedAppointmentBo.EndDate.ToString("hh:mm tt")

            'If request is coming from appointment to be arranged
            If SessionManager.getAppointmentSummarySource() = PathConstants.ToBeArrangedAvailableAppointments Then
                isSaved = objSchedulingBL.schedulePlannedWorkAppointment(objPlannedAppointmentBo, appointmentId)
            ElseIf SessionManager.getAppointmentSummarySource() = PathConstants.ArrangedAvailableAppointments Then
                'if request is coming from appointment arranged
                appointmentId = objPlannedAppointmentBo.AppointmentId
                isSaved = objSchedulingBL.updatePlannedWorkAppointment(objPlannedAppointmentBo)

                'udpate the arranged trades appointment dt in scheduling bo
                Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
                objPlannedSchedulingBo = SessionManager.getPlannedSchedulingBo()
                Dim updatedAppointmentRow() As Data.DataRow
                updatedAppointmentRow = objPlannedSchedulingBo.ArrangedTradesAppointmentsDt.Select("AppointmentId = " + appointmentId.ToString() + "")
                updatedAppointmentRow(0)("IsPending") = 1
                SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            End If


            If (isSaved.Equals(ApplicationConstants.NotSaved)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
                btnScheduleAppointment.Enabled = False
                Me.appointmentSummaryControl.disableUpdateCustomerButton()
                txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
                txtJobSheetNotes.Attributes.Add("readonly", "readonly")
                objPlannedAppointmentBo.AppointmentId = appointmentId
                objPlannedAppointmentBo.IsPending = True
                SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)                
                'reset the appointment info so rest of the info can be used again
                objPlannedAppointmentBo.resetAppointmentInfo()
                SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)

            End If

        End If


    End Sub

#End Region

#Region "Update Appointment Session"
    ''' <summary>
    ''' After scheduling appointment, appointmentId is set to current selected PlannedAppointmentBO
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateAppointmentSession()
        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo = SessionManager.getPlannedAppointmentBO()        
    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region

#End Region
End Class