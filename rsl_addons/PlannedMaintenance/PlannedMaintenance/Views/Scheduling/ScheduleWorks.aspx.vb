﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic

Public Class ScheduleWorks
    Inherits PageBase

#Region "Properties "

    Public ReadOnly Property GetScheduleWorksUpdatePanel As UpdatePanel
        Get
            Return updPanelScheduleWorks
        End Get
    End Property

    Public ReadOnly Property GetScheduleWorkType As String
        Get
            Dim type As String = ""
            If Not IsNothing(Request.QueryString(PathConstants.ScheduleWorksType)) Then
                type = Request.QueryString(PathConstants.ScheduleWorksType)
            End If
            Return type
        End Get
    End Property


#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
                MainView.ActiveViewIndex = 0
                populateSchemeDropdownList()
                populateComponentDropdownList()

                If lnkBtnAppointmentToBeArrangedTab.Enabled Then
                    div_lineAppointmentsToBeArranged.Visible = False
                    div_lineAppointmentsArranged.Visible = True
                Else
                    div_lineAppointmentsToBeArranged.Visible = True
                    div_lineAppointmentsArranged.Visible = False
                End If

                ' load Misc & Adaptation
                Dim search As String = ""
                If Not IsNothing(SessionManager.getConditionRatingAptSearch()) Then
                    search = SessionManager.getConditionRatingAptSearch()
                    SessionManager.removeConditionRatingAptSearch()
                End If
                txtSearch.Text = search

                If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksType))) Then
                    'load Schedule
                    If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksTab))) Then
                        activateAppointmentToBeArranged()
                    Else
                        If (Request.QueryString(PathConstants.ScheduleWorksTab).Equals(ApplicationConstants.AppointmentArrangedTab)) Then
                            activateAppointmentsArranged()
                        Else
                            activateAppointmentToBeArranged()
                        End If

                    End If
                Else
                    'Load Misc & Adaptation
                    If (Request.QueryString(PathConstants.ScheduleWorksType).Equals(ApplicationConstants.AppointmentAdaptation)) Then
                        If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksTab))) Then
                            'Load Adaptation
                            activateAppointmentToBeArranged()
                        Else
                            If (Request.QueryString(PathConstants.ScheduleWorksTab).Equals(ApplicationConstants.AppointmentArrangedTab)) Then
                                'Load arranged Adaptation
                                activateAppointmentsArranged()
                            Else
                                'Load Adaptation
                                activateAppointmentToBeArranged()
                            End If
                        End If
                    ElseIf (Request.QueryString(PathConstants.ScheduleWorksType).Equals(ApplicationConstants.AppointmentMisc)) Then
                        If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksTab))) Then
                            'Load Misc
                            activateAppointmentToBeArranged()
                        Else
                            If (Request.QueryString(PathConstants.ScheduleWorksTab).Equals(ApplicationConstants.AppointmentArrangedTab)) Then
                                'Load arranged Misc
                                activateAppointmentsArranged()
                            Else
                                'Load Misc
                                activateAppointmentToBeArranged()
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try

            searchResults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region

#Region "Dropdown List Scheme Selected Index Changed Event"
    ''' <summary>
    ''' Dropdown List Scheme Selected Index Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Try
            changeScheme()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region

#Region "Lnk Btn Appointment To Be Arranged Tab Click"
    ''' <summary>
    ''' Lnk Btn Appointment To Be Arranged Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppointmentToBeArrangedTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblStartDate.Visible = False
        txtDate.Visible = False

        div_lineAppointmentsToBeArranged.Visible = False
        div_lineAppointmentsArranged.Visible = True

        activateAppointmentToBeArranged()
    End Sub
#End Region

#Region "Lnk Btn Appointments Arranged Tab Click"
    ''' <summary>
    ''' Lnk Btn Appointments Arranged Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppointmentsArrangedTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblStartDate.Visible = True
        txtDate.Visible = True

        div_lineAppointmentsToBeArranged.Visible = True
        div_lineAppointmentsArranged.Visible = False

        activateAppointmentsArranged()

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Activate Appointment to be Arranged"
    ''' <summary>
    ''' Activate Appointment to be Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentToBeArranged()
        lnkBtnAppointmentToBeArrangedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAppointmentsArrangedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        populateAppointmentToBeArrangedList()
    End Sub
#End Region

#Region "Activate Appointments Arranged"
    ''' <summary>
    ''' Activate Appointments Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentsArranged()
        lnkBtnAppointmentsArrangedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAppointmentToBeArrangedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 1
        Me.populateAppointmentsArrangedList()
    End Sub
#End Region

#Region "Populate scheme"
    ''' <summary>
    ''' Populate all schemes
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSchemeDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getAllSchemes(resultDataSet)

        ddlScheme.DataSource = resultDataSet.Tables(0).DefaultView
        ddlScheme.DataValueField = "DevelopmentId"
        ddlScheme.DataTextField = "SchemeName"
        ddlScheme.DataBind()

        Dim item As ListItem = New ListItem("Select a Scheme", "-1")
        ddlScheme.Items.Insert(0, item)
    End Sub

#End Region

#Region "Populate component dropdown list"
    ''' <summary>
    ''' Populate dropdown with components.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateComponentDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        objDashboardBL.getAllComponents(resultDataSet)

        ddlComponents.DataSource = resultDataSet.Tables(0).DefaultView
        ddlComponents.DataValueField = "ComponentId"
        ddlComponents.DataTextField = "ComponentName"
        ddlComponents.DataBind()

        Dim item As ListItem = New ListItem("Select a Component", "-1")
        ddlComponents.Items.Insert(0, item)

        Dim componentid As Integer
        If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
            componentid = Request.QueryString(PathConstants.ComponentId)
        End If
        ddlComponents.SelectedIndex = ddlComponents.Items.IndexOf(ddlComponents.Items.FindByValue(componentid))
    End Sub

#End Region

#Region "Populate Appointment To Be Arranged List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentToBeArrangedList()

        Dim search As String = txtSearch.Text
        Dim schemeId As Integer = ddlScheme.SelectedValue
        Dim componentId As Integer = ddlComponents.SelectedValue
        Dim resultDataSet As DataSet = New DataSet()

        AppointmentToBeArranged.populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, True, GetScheduleWorkType)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.AppointmentToBeArrangedDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Appointments Arranged List"
    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentsArrangedList()
        Dim search As String = txtSearch.Text
        Dim schemeId As Integer = ddlScheme.SelectedValue
        Dim componentId As Integer = ddlComponents.SelectedValue
        Dim appointmentDate As String = txtDate.Text.Trim()
        Dim resultDataSet As DataSet = New DataSet()

        AppointmentArranged.populateAppointmentsArrangedList(resultDataSet, search, schemeId, componentId, appointmentDate, True, GetScheduleWorkType)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.AppointmentsArrangedDataSet, resultDataSet)

    End Sub

#End Region

#Region "Search Results"
    ''' <summary>
    ''' Search Results
    ''' </summary>
    ''' <remarks></remarks>
    Sub searchResults()

        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        Else
            populateAppointmentsArrangedList()
        End If

    End Sub

#End Region

#Region "Change Scheme"
    ''' <summary>
    ''' Change Scheme
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeScheme()

        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
            lblStartDate.Visible = False
            txtDate.Visible = False
        Else
            populateAppointmentsArrangedList()
            lblStartDate.Visible = True
            txtDate.Visible = True
        End If
    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region


#End Region

    Protected Sub ddlComponents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlComponents.SelectedIndexChanged
        Try
            changeScheme()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

    Protected Sub txtDate_TextChanged(sender As Object, e As EventArgs) Handles txtDate.TextChanged
        Try
            changeScheme()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
End Class