﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ArrangedAvailableAppointments.aspx.vb" Inherits="PlannedMaintenance.ArrangedAvailableAppointments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="headingTitle">
        <b>Schedule planned works</b>
    </div>
    <div>
        <asp:UpdatePanel ID="updPanelMain" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message scheduling-message">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="prop-detail-container">
                    <div style="float:left;">
                        <asp:Label ID="lblNextAvailableAppointment" runat="server" Text="Next available appointment:"
                            CssClass="avialble-apt-label"></asp:Label></div>
                            
                    <asp:Panel ID="pnlTopButtons" runat="server" CssClass="top-buttons" Visible="true" >
                        <asp:Button ID="btnBackToScheduling" runat="server" Text="< Back to Scheduling" PostBackUrl="~/Views/Scheduling/ScheduleWorks.aspx"  CssClass="margin-top-right"/><asp:Button
                            ID="btnConfirmAppointments" runat="server" Text="Confirm Appointments" CssClass="margin-top-right" /></asp:Panel>                    
                    <div style="clear:both;"></div>
                    <asp:GridView ID="grdPropertyComponentDetail" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="Black" GridLines="None" ShowHeader="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="PMO" ShowHeader="True">
                                <ItemTemplate>
                                    <asp:Label ID="lbPmo" runat="server" Text='<%# Eval("Ref") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address1" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("PrimaryAddress") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HouseNumberAddress" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Label ID="lbHouseNumberAddress" runat="server" Text='<%# Eval("HouseNumber")+","+Eval("PrimaryAddress") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Post Code" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Eval("Postcode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Component Name" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Eval("Component") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%# Eval("TotalDuration","{0} hour(s)") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:Panel ID="pnlPageContainer" runat="server" CssClass="trade-appointment-blocks-container">
                </asp:Panel>                
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
