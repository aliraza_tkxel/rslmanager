﻿Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessLogic

Public Class ScheduleConditionWorks
    Inherits PageBase

#Region "Properties "

    Public ReadOnly Property GetScheduleWorksUpdatePanel As UpdatePanel
        Get
            Return updPanelScheduleWorks
        End Get
    End Property

#End Region

#Region "Events"

#Region "Page Load Event"

    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then

                MainView.ActiveViewIndex = 0
                If (IsNothing(Request.QueryString(PathConstants.ScheduleWorksTab))) Then
                    activateAppointmentToBeArranged()
                Else
                    Dim search As String = ""
                    If Not IsNothing(SessionManager.getConditionRatingAptSearch()) Then
                        search = SessionManager.getConditionRatingAptSearch()
                        SessionManager.removeConditionRatingAptSearch()
                    End If
                    txtSearch.Text = search
                    If (Request.QueryString(PathConstants.ScheduleWorksTab).Equals(ApplicationConstants.AppointmentArrangedTab)) Then
                        activateAppointmentsArranged()
                    Else
                        activateAppointmentToBeArranged()
                    End If
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"

    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged
        Try
            searchResults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Lnk Btn Appointment To Be Arranged Tab Click"

    ''' <summary>
    ''' Lnk Btn Appointment To Be Arranged Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppointmentToBeArrangedTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        activateAppointmentToBeArranged()
    End Sub

#End Region

#Region "Lnk Btn Appointments Arranged Tab Click"

    ''' <summary>
    ''' Lnk Btn Appointments Arranged Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppointmentsArrangedTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        activateAppointmentsArranged()
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Activate Appointment to be Arranged"

    ''' <summary>
    ''' Activate Appointment to be Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentToBeArranged()
        lnkBtnAppointmentToBeArrangedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAppointmentsArrangedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        populateAppointmentToBeArrangedList()
    End Sub

#End Region

#Region "Activate Appointments Arranged"

    ''' <summary>
    ''' Activate Appointments Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentsArranged()
        lnkBtnAppointmentsArrangedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAppointmentToBeArrangedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 1
        Me.populateAppointmentsArrangedList()
    End Sub

#End Region

#Region "Populate Appointment To Be Arranged List"

    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentToBeArrangedList()

        Dim search As String = txtSearch.Text

        Dim resultDataSet As DataSet = New DataSet()
        AppointmentToBeArranged.populateAppointmentToBeArrangedList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.AppointmentToBeArrangedDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Appointments Arranged List"

    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAppointmentsArrangedList()

        Dim search As String = txtSearch.Text

        Dim resultDataSet As DataSet = New DataSet()

        AppointmentArranged.populateAppointmentsArrangedList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        setAppointmentsArrangedDataSetViewState(resultDataSet)

    End Sub

#End Region

#Region "Search Results"

    ''' <summary>
    ''' Search Results
    ''' </summary>
    ''' <remarks></remarks>
    Sub searchResults()

        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        Else
            populateAppointmentsArrangedList()
        End If

    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Appointments Arranged DataSet Get/Remove/Set"

    Private Function getAppointmentsArrangedDataSetViewState()
        Dim appointmentsArrangedDataSet As DataSet = TryCast(ViewState.Item(ViewStateConstants.AppointmentsArrangedDataSet), DataSet)
        If appointmentsArrangedDataSet Is Nothing Then
            appointmentsArrangedDataSet = New DataSet
        End If
        Return appointmentsArrangedDataSet
    End Function

    Private Sub removeAppointmentsArrangedDataSetViewState()
        ViewState.Remove(ViewStateConstants.AppointmentsArrangedDataSet)
    End Sub

    Private Sub setAppointmentsArrangedDataSetViewState(ByRef appointmentsArrangedDataSet As DataSet)
        ViewState.Add(ViewStateConstants.AppointmentsArrangedDataSet, appointmentsArrangedDataSet)
    End Sub

#End Region

#End Region

End Class