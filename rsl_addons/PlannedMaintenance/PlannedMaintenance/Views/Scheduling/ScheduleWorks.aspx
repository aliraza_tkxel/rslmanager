﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Planned.Master"
    CodeBehind="ScheduleWorks.aspx.vb" Inherits="PlannedMaintenance.ScheduleWorks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="AppointmentToBeArranged" Src="~/Controls/Scheduling/AppointmentToBeArranged.ascx" %>
<%@ Register TagPrefix="uc2" TagName="AppointmentArranged" Src="~/Controls/Scheduling/AppointmentArranged.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        .message
        {
            padding-left: 12px;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelScheduleWorks" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="padding-left: 11px;">
                <b>Schedule planned works</b>
            </div>
            <br />
            <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <table style="border: thin solid #000000; width: 100%;">
                <tr>
                    <td style="padding-left: 8px; padding-top: 10px; width: 22%;">
                        <asp:LinkButton ID="lnkBtnAppointmentToBeArrangedTab" OnClick="lnkBtnAppointmentToBeArrangedTab_Click"
                            CssClass="TabInitial" runat="server" BorderStyle="Solid" Width="90%">Appointments to be arranged: </asp:LinkButton>
                    </td>
                    <td style="padding-right: 1px; padding-top: 10px; width: 18%;">
                        <asp:LinkButton ID="lnkBtnAppointmentsArrangedTab" OnClick="lnkBtnAppointmentsArrangedTab_Click"
                            CssClass="TabInitial" runat="server" BorderStyle="Solid" Width="87%">Appointments arranged: </asp:LinkButton>
                    </td>
                    <td style="width: 18%;">
                        <asp:Label ID="lblStartDate" runat="server" Font-Bold="true" Text="Start Date:" Visible="false"></asp:Label>
                        <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                            PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:TextBox ID="txtDate" Width="100px" runat="server" AutoPostBack="true" Visible="false"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                            TargetControlID="txtDate" WatermarkText="Start Date" WatermarkCssClass="searchTextDefault">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </td>
                    <td style="width: 15%;">
                        <asp:DropDownList ID="ddlComponents" runat="server" Width="98%" Visible="True" AutoPostBack="True">
                            <asp:ListItem Value="-1"><i>Select Component</i></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%;">
                        <asp:DropDownList ID="ddlScheme" runat="server" Width="98%" Visible="True" AutoPostBack="True">
                            <asp:ListItem Value="-1"><i>Select Scheme</i></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 12%;">
                        <%--<asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right">--%>
                        <asp:TextBox ID="txtSearch" runat="server" Width="100px" class="searchbox" Style="float: none;
                            margin-left: 5px;" onkeyup="TypingInterval();" AutoPostBack="false" AutoCompleteType="Search"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                            TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <%--</asp:Panel>--%>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 10px; padding-right: 1px;">
                        <div id="div_lineAppointmentsToBeArranged" runat="server" style="border-bottom: 1px solid black; clear: both;
                            width: 100%;">
                        </div>
                    </td>
                    <td style="padding-left: 3px;">
                        <div id="div_lineAppointmentsArranged" runat="server" style="border-bottom: 1px solid black; clear: both; 
                            width: 100%;">
                        </div>
                    </td>
                    <td colspan="4" style="padding-left: 1px; padding-right: 1px;">
                        <div style="border-bottom: 1px solid black; clear: both;
                            width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-left: 8px; padding-right: 1px; padding-top: 5px;">
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <uc1:AppointmentToBeArranged ID="AppointmentToBeArranged" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <uc2:AppointmentArranged ID="AppointmentArranged" runat="server" />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
