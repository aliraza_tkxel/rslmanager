﻿<?xml version="1.0"?>

<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->

<configuration>
    <configSections>
        <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
        <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
        <section name="dataConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings, Microsoft.Practices.EnterpriseLibrary.Data, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
    </configSections>
    
    <loggingConfiguration name="Logging Application Block" tracingEnabled="true" defaultCategory="General" logWarningsWhenNoCategoriesMatch="true">
        <listeners>
            <add fileName="log.log" header="----------------------------------------" footer="----------------------------------------" formatter="Text Formatter" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" traceOutputOptions="None" filter="All" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="FlatFile TraceListener" />
            <add source="Enterprise Library Logging" formatter="Text Formatter" log="log" machineName="" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" traceOutputOptions="Timestamp" filter="All" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="Formatted EventLog TraceListener" />
        </listeners>
        <formatters>
            <add template="Timestamp: {timestamp}&#xA;Message: {message}&#xA;Category: {category}&#xA;Priority: {priority}:: {priority}&#xA;EventId{eventid}: {eventid}&#xA;Severity: {severity}:{severity}&#xA;Title:{title}&#xA;Machine: {machine}&#xA;Application Domain: {appDomain}&#xA;Process Id: {processId}&#xA;Process Name: {processName}&#xA;Win32 Thread Id: {win32ThreadId}&#xA;Thread Name: {threadName}&#xA;Extended Properties: {dictionary({key} - {value}&#xA;)}" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="Text Formatter" />
        </formatters>
        <categorySources>
            <add switchValue="All" name="General">
                <listeners>
                    <add name="FlatFile TraceListener" />
                </listeners>
            </add>
        </categorySources>
        <specialSources>
            <allEvents switchValue="All" name="All Events" />
            <notProcessed switchValue="All" name="Unprocessed Category" />
            <errors switchValue="All" name="Logging Errors &amp; Warnings">
                <listeners>
                    <add name="FlatFile TraceListener" />
                </listeners>
            </errors>
        </specialSources>
    </loggingConfiguration>
    
    <exceptionHandling>
        <exceptionPolicies>
            <add name="Exception Policy">
                <exceptionTypes>
                    <add type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow" name="Exception">
                        <exceptionHandlers>
                            <add logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" priority="0" useDefaultLogger="false" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="Logging Handler" />
                        </exceptionHandlers>
                    </add>
                </exceptionTypes>
            </add>
        </exceptionPolicies>
    </exceptionHandling>
    
    <dataConfiguration defaultDatabase="PlannedMaintenance" />
    
    <connectionStrings>
      <add name="PlannedMaintenance" connectionString="Data Source=(local);Initial Catalog=RSLBHALive;Persist Security Info=True;Integrated Security=True;App=PlannedMaintenance"
          providerName="System.Data.SqlClient" />
    </connectionStrings>

    <appSettings>
        <add key="DocumentsUploadPath" value="C:\AppliancesDocs\Documents\" />
        <add key="ChartImageHandler" value="storage=file;timeout=20;dir=c:\TempImageFiles\;" />
        <add key="FileSize" value="2097152" />
        <!--OfficialDayStartHour represents job start time in the morning e.g 9:00 AM-->
        <add key="OfficialDayStartHour" value="9" />
        <!--OfficialDayEndHour represents job end time in the evening e.g 5:00 PM-->
        <add key="OfficialDayEndHour" value="17" />
        <add key="AppointmentDurationHours" value="8" />
      <add key="MaximunLookAhead" value="400" />
      <add key ="ShowAssignedToContractorButton" value="True"/>
      <add key="SMSAddress" value="https://sms.webequator.com/api"/>
      <add key="CoreDayStartHour" value="8:00" />
      <add key="CoreDayEndHour" value="16:30" />
    </appSettings>

  <system.web>
    <compilation debug="true" strict="false" explicit="true" targetFramework="4.0" />
    <httpRuntime requestValidationMode="2.0" />
    <profile>
      <providers>
        <clear/>
        <add name="AspNetSqlProfileProvider" type="System.Web.Profile.SqlProfileProvider" connectionStringName="ApplicationServices" applicationName="/"/>
      </providers>
    </profile>

    <roleManager enabled="false">
      <providers>
        <clear/>
        <add name="AspNetSqlRoleProvider" type="System.Web.Security.SqlRoleProvider" connectionStringName="ApplicationServices" applicationName="/" />
        <add name="AspNetWindowsTokenRoleProvider" type="System.Web.Security.WindowsTokenRoleProvider" applicationName="/" />
      </providers>
    </roleManager>

      <pages validateRequest="false" enableEventValidation="false">
          <controls>
              <add tagPrefix="ajaxToolkit" namespace="AjaxControlToolkit" assembly="AjaxControlToolkit" />

          </controls>
      </pages>
      
      <globalization requestEncoding="utf-8" responseEncoding="utf-8" culture="en-GB" uiCulture="en-GB" />

      <sessionState
      mode="InProc"
      timeout="1440"
      cookieName="sessionCookie"
      cookieless="false"
      regenerateExpiredSessionId="true"
      stateNetworkTimeout="1440">
      </sessionState>
  </system.web>
  <system.net>
    <mailSettings>
      <smtp from="noreply@broadlandgroup.org">
        <network host="smtp.office365.com"
                 password="S@xupad2"
                 userName="outgoingmail@broadlandgroup.org"
                 port="25" enableSsl="true"/>
      </smtp>
    </mailSettings>
  </system.net>
  <system.webServer>
     <modules runAllManagedModulesForAllRequests="true"/>
  </system.webServer>
</configuration>
