﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CreateStatus.ascx.vb"
    Inherits="PlannedMaintenance.CreateStatus" %>    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Panel runat="server" ID="pnlStatusControl" Visible="true" Style="padding-left: 100px;
    padding-top: 20px;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server" CssClass="message"></asp:Label>
    </asp:Panel>
    <h3 style="padding-left: 8px; display: block;">
        <asp:Label runat="server" ID="lblNewStatus">Add New Status</asp:Label>
        <asp:Label runat="server" ID="lblEditStatus" Visible="false">Edit Status </asp:Label>
    </h3>
    <table style="width: 100%;" border="0" class="add-edit">
        <tr>
            <td>
                Status Title:<span class="Required">*</span>
            </td>
            <td>
                <asp:TextBox ID="txtBoxTitle" runat="server" Style="width: 153px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Ranking:<span class="Required">*</span>
            </td>
            <td>
                <asp:DropDownList ID="ddlRanking" runat="server" Style="height: 25px; width: 160px;">
                </asp:DropDownList>
                <br />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <br />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="White" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" />
                <asp:Button ID="btnEdit" runat="server" Text="Edit" Visible="false" BackColor="White" />
            </td>
        </tr>
    </table>
</asp:Panel>
