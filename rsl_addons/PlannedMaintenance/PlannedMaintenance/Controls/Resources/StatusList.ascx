﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="StatusList.ascx.vb" Inherits="PlannedMaintenance.StatusList" %>
<style type="text/css">
    .statusList
    {
        border-bottom: 1px solid gray;
    }   
    
    .statusList td
    {
        width: 26%;
        text-align: left; 
        font-weight: bold; 
        font-size: 12px; 
        padding: 0 0 0 12px !important;
    }     
    #Resources td{
        padding: 2px !important;
    }
</style>

<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<div class="box-left-b" style="margin:0; width:98%">
    <div class="boxborder-s-right" style="padding-bottom:22px;">
        <div class="select_div-b">
            <div class="label-text" style="color: white; width: 135px;padding-left:0;font-family: Tahoma;">
                Status and Action
            </div>
            <div style="float: right;>
                <div class="field">
                    <asp:Button ID="btnAddStatus" runat="server"  CssClass="btn btn-blue btn-xs right" style="padding: 3px 20px !important;"
                        Text="Add" 
                        PostBackUrl="~/Views/Resources/Status.aspx" />
                </div>
            </div>
        </div>
        <div class="box-s" style="text-align: center">
            <div style=" width: 100%; padding:0">
                <asp:GridView ID="grdStatus" runat="server" AutoGenerateColumns="False" Width="100%"
                    Style="overflow: scroll;" ShowHeader="False" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnEditStatus" runat="server" Text="Edit" CommandArgument='<%# Bind("StatusId") %>'
                                    PostBackUrl='<%# Eval("StatusId", "~/Views/Resources/Status.aspx?sid={0}") %>'
                                    CssClass="btn btn-blue btn-xs right" style="padding: 2px 20px !important;" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</div>