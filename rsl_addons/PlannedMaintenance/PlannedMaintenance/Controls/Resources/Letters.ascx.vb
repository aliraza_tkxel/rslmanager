﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing


Public Class Letters
    Inherits UserControlBase

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.getLetters()
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "get Letters"
    Public Sub getLetters()
        Dim objLettersBL As LettersBL = New LettersBL()
        Dim resultDataset As DataSet = New DataSet()
        objLettersBL.getAllLetters(resultDataset)
        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoLettersFound, True)
        Else            
            grdLetters.DataSource = resultDataset
            grdLetters.DataBind()

        End If
    End Sub
#End Region

#End Region
End Class