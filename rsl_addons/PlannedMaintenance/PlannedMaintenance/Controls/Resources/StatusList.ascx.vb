﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class StatusList
    Inherits UserControlBase

#Region "Functions"

#Region "Page Load"
    ''' <summary>
    ''' This function 'll be called on page load and call function to get the all statuses
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.getStatus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "get Status"
    ''' <summary>
    ''' This fuction 'll fetch all the status from the database and bind those with grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getStatus()
        Dim resourceBl As StatusActionBL = New StatusActionBL()
        Dim resultDataset As DataSet = New DataSet()
        resourceBl.getAllStatuses(resultDataset)

        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RecordNotFound, True)
        Else
            grdStatus.DataSource = resultDataset
            grdStatus.DataBind()

        End If
    End Sub
#End Region

#End Region


End Class