﻿Imports System.Data
Imports System.Data.SqlClient
Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class EditLetterControl
    Inherits UserControlBase
    'Inherits System.Web.UI.UserControl

    Dim objStatusBL As StatusActionBL = New StatusActionBL()
    'Dim objActionBL As ActionBL = New ActionBL()
    Dim objLetterBL As LettersBL = New LettersBL()
    Dim stdLetterId As Integer


#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            UIMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsNothing(Request.QueryString("id")) Then
                Dim strStdLetterId As String = Request.QueryString("id")
                If Not (IsPostBack) Then
                    If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
                        Me.stdLetterId = Int32.Parse(strStdLetterId)
                        ViewState(ViewStateConstants.StandardLetterId) = Me.stdLetterId
                        Me.loadLetter()
                    Else
                        UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoLetterId, True)
                    End If

                    Me.loadSignOffDdl(Me.ddlSignOff)
                    Me.loadTeamDdl(Me.ddlTeams)
                End If
            Else
                UIMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.InvalidLetterId, True)
            End If


        Catch ex As Exception
            UIMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If UIMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If UIMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Event Handlers"

#Region "ddl Teams Selected Index Changed "

    Protected Sub ddlTeams_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTeams.SelectedIndexChanged
        Me.loadFromUserDdl(Me.ddlFrom)
    End Sub
#End Region

#Region "btn Add Saved Letter Click"

    Protected Sub btnAddSavedLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddSavedLetter.Click

        Try
            'Dim savedLetterBO As SavedLetterBO = New SavedLetterBO(5, letterBody, letterTitle, Me.stdLetterId, teamId, fromResourceId, signOffLookupCode)
            'Dim savedLetterOptionsBO As SavedLetterOptionsBO = New SavedLetterOptionsBO(-1, -1, Nothing)

            'Dim resultSet As DataSet = New DataSet()

            'objLetterBL.addCustomSavedLetter(savedLetterBO, savedLetterOptionsBO, resultSet)


            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterSuccess, False)
            If (Me.saveLetterInSession() = True) Then
                UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterSuccess, False)
            End If

        Catch ex As Exception
            UIMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If UIMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If UIMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Print Standard Letter Click"

    Protected Sub btnPrintStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrintStandardLetter.Click
        Dim printLetterBo As PrintLetterBO = New PrintLetterBO()
        If (validateLetter()) Then
            printLetterBo.LetterTitle = txtLetterTitle.Text
            printLetterBo.LetterBody = txtLetterTemplate.Text
            printLetterBo.SignOff = ddlSignOff.SelectedItem.Text
            printLetterBo.ResourceTeamName = ddlTeams.SelectedItem.Text
            printLetterBo.From = ddlFrom.SelectedItem.Text
            printLetterBo.EmployeeID = ddlFrom.SelectedItem.Value
            SessionManager.setPrintLetterBo(printLetterBo)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "openPrintLetterWindow()", True)
        End If
    End Sub

#End Region

#End Region

#Region "Methods"

#Region "Save Letter In Session"
    Protected Function saveLetterInSession()
        Dim success As Boolean = False
        If (validateLetter() = True) Then
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow
            Dim letterAleadyAdded As Boolean = False
            dt.Columns.Add(ApplicationConstants.LetterBodyColumn)
            dt.Columns.Add(ApplicationConstants.StandardLetterId)
            dt.Columns.Add(ApplicationConstants.LetterTextTitleColumn)
            dt.Columns.Add(ApplicationConstants.TeamIdColumn)
            dt.Columns.Add(ApplicationConstants.FromResourceIdColumn)
            dt.Columns.Add(ApplicationConstants.SignOffCodeColumn)
            dt.Columns.Add(ApplicationConstants.RentBalanceColumn)
            dt.Columns.Add(ApplicationConstants.RentChargeColumn)
            dt.Columns.Add(ApplicationConstants.TodayDateColumn)

            If (Not IsNothing(SessionManager.getActivityLetterDetail())) Then
                dt = SessionManager.getActivityLetterDetail()

                If (isLetterAlreadyAdded(dt) = True) Then
                    success = False
                    letterAleadyAdded = True
                    UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.LetterAlreadyExist, True)
                End If
            End If

            If (letterAleadyAdded = False) Then
                dr = dt.NewRow()
                dr(ApplicationConstants.LetterTextTitleColumn) = txtLetterTitle.Text
                dr(ApplicationConstants.LetterBodyColumn) = txtLetterTemplate.Text
                dr(ApplicationConstants.StandardLetterId) = ViewState(ViewStateConstants.StandardLetterId)
                dr(ApplicationConstants.TeamIdColumn) = ddlTeams.SelectedValue
                dr(ApplicationConstants.FromResourceIdColumn) = ddlFrom.SelectedValue
                dr(ApplicationConstants.SignOffCodeColumn) = ddlSignOff.SelectedValue
                dr(ApplicationConstants.RentBalanceColumn) = "0"
                dr(ApplicationConstants.RentChargeColumn) = "0"
                dr(8) = Date.Now
                dt.Rows.Add(dr)
                SessionManager.setActivityLetterDetail(dt)
                SessionManager.setActivityStandardLetterId(ViewState(ViewStateConstants.StandardLetterId))
                success = True
            End If
        End If
        Return success
    End Function
#End Region

#Region "If Letter Already Added"
    Protected Function isLetterAlreadyAdded(ByRef dt As DataTable)
        Dim isLetterAlreadyExist As Boolean = False
        Dim letterId As Integer = Integer.Parse(ViewState(ViewStateConstants.StandardLetterId))

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) = letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                isLetterAlreadyExist = True
            Else
                isLetterAlreadyExist = False
            End If
        End If
        Return isLetterAlreadyExist
    End Function
#End Region

#Region "Validate Letter "
    Protected Function validateLetter() As Boolean
        Dim success As Boolean = True
        If (IsNothing(Me.txtLetterTitle.Text) Or Me.txtLetterTitle.Text = String.Empty) Then
            success = False
            UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTitle, True)
        ElseIf (IsNothing(Me.txtLetterTemplate.Text) Or Me.txtLetterTemplate.Text = String.Empty) Then
            success = False
            UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoBody, True)
        ElseIf (Me.ddlSignOff.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoSignOFfLookupCode, True)
        ElseIf (Me.ddlTeams.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTeamId, True)
        ElseIf (Me.ddlFrom.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            UIMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoFromResourceId, True)
        End If
        Return success
    End Function
#End Region

#Region "Reset Controls"
    Protected Sub resetControls()
        Me.txtLetterTitle.Text = String.Empty
        Me.txtLetterTemplate.Text = String.Empty
        Me.ddlSignOff.SelectedValue = ApplicationConstants.DropDownDefaultValue
        ddlTeams.SelectedValue = ApplicationConstants.DropDownDefaultValue
        Me.ddlFrom.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letter"
    Protected Sub loadLetter()
        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLetterById(Me.stdLetterId, resultSet)

        Me.txtLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")

        Me.txtLetterTemplate.Text = resultSet.Tables(0).Rows(0).Item("Body")
    End Sub
#End Region

#Region "load Sign Off Ddl"

    Protected Sub loadSignOffDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getSignOffTypes(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Sign Off Type", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "load Team Ddl "
    Protected Sub loadTeamDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getTeams(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Team", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub
#End Region

#Region "load From User Ddl"
    Protected Sub loadFromUserDdl(ByRef ddl As DropDownList)
        Dim teamSelected As Int32 = CType(Me.ddlTeams.SelectedItem.Value, Int32)

        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getFromUserByTeamId(teamSelected, ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select From", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#End Region


End Class
