﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CreateAction.ascx.vb"
    Inherits="PlannedMaintenance.CreateAction" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 292px;
    }
    .style2
    {
        width: 122px;
    }
    .btn-edit-save
    {
        margin-left: 88px;
    }        
</style>
<asp:Panel runat="server" ID="pnlActionControl" Style="padding-left: 100px; padding-top: 20px;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server" CssClass="message"></asp:Label>
    </asp:Panel>
    <h3 style="padding-left: 8px; display: block;">
        <asp:Label runat="server" ID="lblNewAction">Add New Action</asp:Label>
        <asp:Label runat="server" ID="lblEditAction" Visible="false">Edit Action </asp:Label>
    </h3>
    <table style="width: 100%;">
        <tr>
            <td class="style2">
                Action Title:<span class="Required">*</span>
            </td>
            <td>
                <asp:Label ID="lblStatusTitle" runat="server"></asp:Label>
                <asp:TextBox ID="txtBoxTitle" runat="server"></asp:TextBox>
                <asp:Panel ID="pnlErrorMsg" runat="server" Visible="false">
                    <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></asp:Panel>
                <%-- <asp:RequiredFieldValidator runat="server" ID="validatortxtBoxTitle" CssClass="Required" ControlToValidate="txtBoxTitle" ErrorMessage ="Title is required"></asp:RequiredFieldValidator>  
                --%>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Ranking:<span class="Required">*</span>
            </td>
            <td>
                <asp:DropDownList ID="ddlRanking" runat="server" Style="height: 25px; width: 204px;">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style2">
            </td>
            <td>
                <div style="float: left;">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="White" />
                </div>
                <div style="padding-left: 149px">
                    <asp:Button ID="btnSave" runat="server" Text="Save" />
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" Visible="false" BackColor="White"
                        CssClass="btn-edit-save" />
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
