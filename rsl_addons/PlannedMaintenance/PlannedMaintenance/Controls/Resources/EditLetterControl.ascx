﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EditLetterControl.ascx.vb"
    Inherits="PlannedMaintenance.EditLetterControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    ._lblMessage
    {
        float: left;
        margin-bottom: 10px;
        width: 100%;
    }
    .ajax__html_editor_extender_container
    {
        width: 100% !important;        
    }
    .ajax__html_editor_extender_container, .ajax__html_editor_extender_container *
    {
        font-family: Arial !important;
        font-size: 12pt !important;
    }
    .code_box
    {
        border: 2px solid #000;
        width: 300px;
        min-height: 155px;
    }
    .code_box p
    {
        margin-left: 10px;
    }
    .letter_buttons
    {
        float: right;
    }
    .Arial12pt, .Arial12pt *
    {
        font-family: Arial !important;
        font-size: 12pt !important;
    }
</style>
<asp:Panel ID="pnlLetterContent" runat="server">
    <div id="Resources" class="group">
        <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
            font-weight: bold; margin: 0 0 6px; font-size: 15px;">
            <font color="white">Edit Letter Template</font>
        </p>
        <div style="float: right; height: 558px; margin-left: 1%; width: 100%;" align="left">
            <asp:UpdatePanel runat="server" ID="pnlAddLetterTemplate" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server" Text="" CssClass="lblMessage"></asp:Label>
                    </asp:Panel>
                    <span style="float: left; width: 80px;">Letter Title: </span><span>
                        <asp:TextBox ID="txtLetterTitle" runat="server" ClientIDMode="Static" ReadOnly="True">            
                        </asp:TextBox>
                    </span>
                    <br />
                    <br />
                    <span>
                        <asp:TextBox ID="txtLetterTemplate" TextMode="MultiLine" Columns="60" Rows="15" runat="server"
                            Font-Bold="False" CssClass="Arial12pt" />
                        <asp:HtmlEditorExtender ID="richTxtComments" TargetControlID="txtLetterTemplate"
                            EnableSanitization="False" runat="server">
                            <Toolbar>
                                <ajaxToolkit:Bold />
                                <ajaxToolkit:Italic />
                                <ajaxToolkit:Underline />
                                <ajaxToolkit:StrikeThrough />                                
                                <ajaxToolkit:InsertOrderedList />
                                <ajaxToolkit:InsertUnorderedList />
                                <ajaxToolkit:Undo />
                                <ajaxToolkit:Redo />
                                <ajaxToolkit:InsertImage />
                            </Toolbar>
                        </asp:HtmlEditorExtender>
                    </span>
                    <br />
                    <div style="width: 100%; float: left;">
                        <div class="code_box" style="float: left">
                            <p>
                                Enter the following codes for the following:</p>
                            <p>
                                Rent Charge = <a href="javascript:void(0)" onclick="insertText('[RC]');">[RC]</a></p>
                            <p>
                                Current Rent Balance = <a href="javascript:void(0)" onclick="insertText('[RB]');">[RB]</a></p>
                            <p>
                                Today's Date = <a href="javascript:void(0)" onclick="insertText('[TD]');">[TD]</a></p>
                        </div>
                        <div style="width: 30px; float: left; height: 155px;">
                        </div>
                        <div class="code_box" style="width: 400px; float: right;">
                            <p>
                                <span style="width: 100%; float: left; margin-bottom: 13px;"><span style="width: 150px;
                                    float: left;">Select Sign Off: </span><span style="width: 150px; float: left;">
                                        <asp:DropDownList ID="ddlSignOff" class="styleselect" runat="server">
                                        </asp:DropDownList>
                                    </span></span><span style="width: 100%; float: left; margin-bottom: 13px;"><span
                                        style="width: 150px; float: left;">Select Team Name: </span><span style="width: 150px;
                                            float: left;">
                                            <asp:DropDownList ID="ddlTeams" class="styleselect" runat="server" OnSelectedIndexChanged="ddlTeams_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </span></span><span style="width: 100%; float: left; margin-bottom: 13px;"><span
                                            style="width: 150px; float: left;">Select From: </span><span style="width: 150px;
                                                float: left;">
                                                <asp:DropDownList ID="ddlFrom" class="styleselect" runat="server">
                                                    <asp:ListItem Value="-1">Please Select From</asp:ListItem>
                                                </asp:DropDownList>
                                            </span></span>
                            </p>
                        </div>
                    </div>
                    <br />
                    <div style="clear: both;">
                    </div>
                    <div class="letter_buttons" style="float: right; padding-top: 10px; text-align: right;
                        width: 100%;">
                        <%If (Not IsNothing(Request.QueryString("pg")) And Request.QueryString("pg") = 0) Then%>
                        <asp:Button ID="btnCancel" runat="server" Text="Close" OnClientClick="closePopup('0')"
                            BackColor="White" />
                        <%Else%>
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="closePopup('1')"
                            BackColor="White" />
                        <%End If%>
                        <asp:Button ID="btnPrintStandardLetter" runat="server" Text="Print" BackColor="White" />
                        <%--<input type="text" class="hiddenField" id="txtPrintStandardLetterHidden"  onchange="Clickheretoprint()" runat="server"/>--%>
                        <%--<asp:CheckBox ID="ckBoxPrintStandardLetter" runat="server" CssClas="hiddenField"  />--%>
                        <asp:Button ID="btnAddSavedLetter" runat="server" Text="Save Letter" BackColor="White" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnPrintStandardLetter" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Panel>
