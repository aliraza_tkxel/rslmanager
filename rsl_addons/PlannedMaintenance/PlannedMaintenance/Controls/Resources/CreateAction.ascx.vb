﻿Imports System
Imports PL_Utilities
Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class CreateAction
    Inherits UserControlBase
#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub
#End Region

#Region "btn Save Click"
    ''' <summary>
    ''' Event handler for save action 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            If Me.validateForm() = True Then
                Me.saveNewAction()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionError, True)
            End If
        End Try
    End Sub
#End Region

#Region "reset Control"
    ''' <summary>
    ''' function to rest the add /edit action form
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub resetControl()        
        txtBoxTitle.Text = String.Empty
    End Sub
#End Region

#Region "btn Edit Click"
    ''' <summary>
    ''' Event to handle the edit action 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Try
            If Me.validateForm() = True Then
                Me.editAction()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Cancel Click"
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim ActionControl As UserControl = CType(updPanelStatus.FindControl("aControl"), UserControl)
        ActionControl.Visible = False
        Me.reloadTreeView()
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "Validate Form"
    ''' <summary>
    ''' This function 'll validate the action form fields
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function validateForm()

        Dim actionBo As ActionBO = New ActionBO()
        actionBo.Title = txtBoxTitle.Text

        'create the object of microsoft enterprise validation class
        Dim results As ValidationResults = Validation.Validate(actionBo)
        If results.IsValid Then
            Return True
        Else
            'find first message in validation result and set it 
            For Each result As ValidationResult In results
                uiMessageHelper.setMessage(lblMessage, pnlMessage, result.Message, True)
                'after setting the first message just exit.
                Exit For
            Next
            Return False
        End If
    End Function
#End Region

#Region "Save New Action "
    ''' <summary>
    ''' This function 'll save the action in database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub saveNewAction()
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        Dim title As String = txtBoxTitle.Text
        Dim ranking As Integer = ddlRanking.SelectedItem.Text
        Dim treeView As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
        Dim statusId As Integer = SessionManager.getStatusIdOfAction()
        Dim createdBy As Integer = SessionManager.getUserEmployeeId()
        Dim isSaved As Boolean = False
        'save the action & reset the controls
        isSaved = objStatusActionBl.addAction(statusId, title, ranking, createdBy)
        If isSaved = False Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionSavedError, True)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionSavedSuccessfuly, False)
            resetControl()

            'now update the controls on page
            Dim updPnlStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim actionControl As UserControl = CType(updPnlStatus.FindControl("aControl"), UserControl)
            Dim pnlActionControl As Panel = CType(actionControl.FindControl("pnlActionControl"), Panel)

            'reload the tree view
            reloadTreeView()
            Dim status As Status = New Status()
            Me.loadActionRanking(statusId, False)
            txtBoxTitle.Text = String.Empty
            pnlActionControl.Visible = True
            updPnlStatus.Update()
        End If

    End Sub
#End Region

#Region "Edit Action"
    ''' <summary>
    ''' This function 'll update the action data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub editAction()

        Dim title As String = txtBoxTitle.Text
        Dim ranking As Integer = CType((ddlRanking.SelectedItem.Value), Integer)
        Dim treeView As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
        Dim statusId As Integer = SessionManager.getStatusIdForEditAction()
        Dim actionId As Integer = SessionManager.getActionIdForEditAction()
        Dim modifiedBy As Integer = SessionManager.getUserEmployeeId()
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        Dim isSaved As Boolean = False
        'save the updated action & set the user message
        isSaved = objStatusActionBl.editAction(actionId, statusId, title, ranking, modifiedBy)

        If isSaved = False Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionSavedError, True)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EditActionSuccessful, False)

            Dim updPnlStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim actionControl As UserControl = CType(updPnlStatus.FindControl("aControl"), UserControl)
            Dim pnlActionControl As Panel = CType(actionControl.FindControl("pnlActionControl"), Panel)

            'refresh the tree view
            reloadTreeView()
            updPnlStatus.Update()
            Dim status As Status = New Status()            
            Me.loadActionRanking(statusId, True)            
            ddlRanking.SelectedIndex = ranking - 1
            pnlActionControl.Visible = True
        End If
        

    End Sub
#End Region

#Region "reload Tree View"
    ''' <summary>
    ''' This function 'll reload the tree view
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub reloadTreeView()
        Try
            Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim tree As TreeView = New TreeView()
            tree = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
            Dim objStatus As Status = New Status()
            tree.Nodes(0).ChildNodes.Clear()
            tree.Height = 0
            objStatus.populateStatusNodes(tree.Nodes(0))
            objStatus.populateActionNodes(tree.Nodes(0))
           
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load Tree Selected Nodes"
    Public Sub loadTreeSelectedNodes(ByVal StatusId As Integer, ByVal ActionId As Integer)
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
        Dim objStatus As Status = New Status()
        objStatus.populateStatusNodes(tree.Nodes(0))
        objStatus.populateActionNodes(tree.Nodes(0))
        tree.ExpandDepth = ActionId
    End Sub
#End Region

#Region "load Action Ranking"
    Public Sub loadActionRanking(ByVal statusId As Integer, ByVal checkEdit As Boolean)
        Dim resultDataSet As DataSet = New DataSet()
        Try            
            Dim statusBl As StatusActionBL = New StatusActionBL()
            ddlRanking.Items.Clear()
            statusBl.getActionRankingByStatusId(resultDataSet, statusId)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next
            If checkEdit = False Then
                ddlRanking.Items.Add(New ListItem((resultDataSet.Tables(0).Rows.Count + 1).ToString(), "-1"))
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region
End Class