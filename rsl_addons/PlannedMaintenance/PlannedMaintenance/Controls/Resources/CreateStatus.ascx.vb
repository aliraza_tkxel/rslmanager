﻿Imports System
Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class CreateStatus
    Inherits UserControlBase

#Region "Events"
#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                'Me.loadStatusRanking(False)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Save Click"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            If Me.validateForm() = True Then
                Me.saveNewStatus()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Cancel Click"
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim statusControl As UserControl = CType(updPanelStatus.FindControl("sControl"), UserControl)
        statusControl.Visible = False
        Me.reloadTreeView()
    End Sub
#End Region

#Region "btn Edit Click"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Try
            If Me.validateForm() = True Then
                Me.editStatus()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "refresh View"
    Sub refreshView(ByRef node As TreeNode)
        collapseAndExpandNode(node.Parent)
    End Sub
#End Region

#Region " cllapse And Expand Node"
    Sub collapseAndExpandNode(ByRef node As TreeNode)
        node.Collapse()
        node.Expand()
        selectNode()
    End Sub
#End Region

#Region "Select Node"
    Sub selectNode()
        Dim node As TreeNode
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
        For Each node In tree.Nodes
            If node.ChildNodes.Count > 0 Then
                Dim node1 As TreeNode
                For Each node1 In node.ChildNodes
                    If node1.Text = Session("SelectedNodeText") Then
                        node1.Select()
                    End If
                    If node1.ChildNodes.Count > 0 Then
                        Dim node2 As TreeNode
                        For Each node2 In node1.ChildNodes
                            If node2.Text = Session("SelectedNodeText") Then
                                node2.Select()
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "Validate Form"
    ''' <summary>
    ''' This function 'll validate the status form fields
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function validateForm()

        Dim statusBo As StatusBO = New StatusBO()
        statusBo.Title = txtBoxTitle.Text

        'create the object of microsoft enterprise validation class
        Dim results As ValidationResults = Validation.Validate(statusBo)
        If results.IsValid Then
            Return True
        Else
            'find first message in validation result and set it 
            For Each result As ValidationResult In results
                uiMessageHelper.setMessage(lblMessage, pnlMessage, result.Message, True)
                'after setting the first message just exit.
                Exit For
            Next
            Return False
        End If
    End Function
#End Region

#Region "Save New Status"
    ''' <summary>
    ''' This fuctnion saves the new status in database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub saveNewStatus()

        Dim title As String = txtBoxTitle.Text
        Dim selectedRanking As Integer = CType(ddlRanking.SelectedItem.Text, Integer)        

        'now save the status
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        Dim isSaved As Boolean = False
        isSaved = objStatusActionBl.addStatus(title, selectedRanking)

        If isSaved = False Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusSavedError, True)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusSavedSuccessfuly, False)

            'update the tree panel in parent page.
            Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim tree As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)

            'refresh the nodes of tree view
            reloadTreeView()
            updPanelStatus.Update()
            txtBoxTitle.Text = String.Empty
            Dim status As New Status()
            status.loadStatusRanking(ddlRanking)            
        End If
        
    End Sub
#End Region

#Region "Edit Status"
    Private Sub editStatus()
        'get status id from session 
        Dim statusId As Integer = 0
        statusId = SessionManager.getStatusIdForEdit()        
        Dim title As String = txtBoxTitle.Text
        Dim modifiedBy As Integer = SessionManager.getUserEmployeeId()
        Dim objStatusActionBl As StatusActionBL = New StatusActionBL()
        Dim selectedRanking As Integer = CType(ddlRanking.SelectedItem.Text, Integer)
        'update the status
        Dim isSaved As Boolean = False
        isSaved = objStatusActionBl.editStatus(statusId, title, selectedRanking, modifiedBy)
        If isSaved = False Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusSavedError, True)
        Else
            'display the message
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusUpdatedSuccessfuly, False)

            'update the controls
            Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
            tree.PopulateNodesFromClient = True
            reloadTreeView()
            updPanelStatus.Update()
        End If
    End Sub
#End Region

#Region "reload Tree View"
    ''' <summary>
    ''' This funciton 'll refresh the nodes of tree view
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub reloadTreeView()

        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)

        Dim objStatus As Status = New Status()
        tree.Nodes(0).ChildNodes.Clear()
        tree.Height = 0

        objStatus.populateStatusNodes(tree.Nodes(0))
        objStatus.populateActionNodes(tree.Nodes(0))
    End Sub
#End Region

#End Region
End Class