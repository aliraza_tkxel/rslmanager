﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Public Class ViewComponent
    Inherits UserControlBase

#Region "Properties"
    Dim objComponentsBl As ComponentsBL = New ComponentsBL()
    Public componentID As Integer = 0
#End Region
#Region "Events"
#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Grid operative Row data bound"
    Protected Sub grdOperative_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOperative.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Text = (e.Row.RowIndex + 1).ToString() + ". " + Server.HtmlDecode(e.Row.Cells(0).Text).ToString()
                e.Row.Cells(1).Text = "&nbsp;&nbsp;: " + Server.HtmlDecode(e.Row.Cells(1).Text).ToString() + " hour(s)"

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn edit click event"
    ''' <summary>
    ''' btn edit click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            Dim editComponentControl As EditComponent = CType(Me.Parent.FindControl("EditComponent"), EditComponent)
            Dim resultDataSet As DataSet = New DataSet()
            editComponentControl.populateComponentDetail(resultDataSet)
            Dim viewComponentControl As ViewComponent = CType(Me.Parent.FindControl("ViewComponent"), ViewComponent)
            Dim ddlTrade As DropDownList = CType(editComponentControl.FindControl("ddlTrade"), DropDownList)
            ddlTrade.SelectedIndex = 0

            Dim ddlDuration As DropDownList = CType(editComponentControl.FindControl("ddlDuration"), DropDownList)
            ddlDuration.SelectedIndex = 0
            editComponentControl.Visible = True
            viewComponentControl.Visible = False

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region
#End Region
#Region "Functions"

#Region "Populate component detail"
    Public Sub populateComponentDetail(ByRef resultDataSet As DataSet, ByVal comId As Integer)
        objComponentsBl.getComponentDetail(resultDataSet, comId)
        componentID = comId
        If Not resultDataSet.Tables(0).Rows.Count = 0 Then
            SessionManager.removeConponentDetailDataSet()
            SessionManager.setConponentDetailDataSet(resultDataSet)
            Dim componentdt As DataTable = New DataTable()
            Dim operativesdt As DataTable = New DataTable()
            componentdt = resultDataSet.Tables("Componentdetail")
            If Not componentdt.Rows.Count = 0 Then
                lblComponentName.Text = componentdt.Rows(0)("COMPONENTNAME")
                If componentdt.Rows(0).Item("ISACCOUNTING") = True Then
                    lblAccounting.Text = "Yes"
                Else
                    lblAccounting.Text = "No"
                End If
                If componentdt.Rows(0).Item("FREQUENCY").ToString() = "yrs" Then
                    lblCycle.Text = componentdt.Rows(0).Item("CYCLE").ToString() + " Years "
                Else
                    lblCycle.Text = componentdt.Rows(0).Item("CYCLE").ToString() + " Months "
                End If

                Dim ldec_AnnualBudget As Double
                If Double.TryParse(componentdt.Rows(0).Item("ANNUALBUDGETCOST"), ldec_AnnualBudget) Then
                    lblAnnualBudgetCost.Text = ldec_AnnualBudget.ToString("c")
                End If

                Dim materialcost As Double
                If Double.TryParse(componentdt.Rows(0).Item("MATERIALCOST").ToString(), materialcost) Then
                    lblMaterialCost.Text = materialcost.ToString("c")
                End If

                Dim labourcost As Double
                If Double.TryParse(componentdt.Rows(0).Item("LABOURCOST").ToString(), labourcost) Then
                    lblLabourCost.Text = labourcost.ToString("c")
                End If

            End If
            operativesdt = resultDataSet.Tables("OperativesDetail")
            grdOperative.DataSource = operativesdt
            grdOperative.DataBind()
            grdOperative.Attributes.Add("rules", "None")
            If Not operativesdt.Rows.Count = 0 Then
                lblOperative.Text = ""
            Else
                lblOperative.Text = "NA"
            End If
        End If
    End Sub
#End Region

#End Region





End Class