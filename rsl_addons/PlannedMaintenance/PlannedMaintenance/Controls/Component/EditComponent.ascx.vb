﻿Imports PL_BusinessObject
Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Reflection
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class EditComponent
    Inherits UserControlBase
#Region "Properties"
    Public Shared componentId As Integer = 0
#End Region

#Region "Events"
#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnAmend.Visible = False
            btnAdd.Visible = True
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                GetAddFaultLookUpValues()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grdOperative Row Data Bound"
    ''' <summary>
    ''' grdOperative Row Data Bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdOperative_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOperative.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'e.Row.Cells(0).Text = (e.Row.RowIndex + 1).ToString() + ". " + Server.HtmlDecode(e.Row.Cells(0).Text).ToString()
                'e.Row.Cells(1).Text = "&nbsp;&nbsp;: " + Server.HtmlDecode(e.Row.Cells(1).Text).ToString() + " hour(s) &nbsp;&nbsp;"
                Dim lblTrade = DirectCast(e.Row.FindControl("lblTRADE"), Label)
                Dim lblDuration = DirectCast(e.Row.FindControl("lblDURATION"), Label)

                e.Row.Cells(0).Text = (e.Row.RowIndex + 1).ToString() + ". " + Server.HtmlDecode(lblTrade.Text).ToString()
                e.Row.Cells(1).Text = "&nbsp;&nbsp;: " + Server.HtmlDecode(lblDuration.Text).ToString() + " hour(s) &nbsp;&nbsp;"

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grid view Row edit"
    ''' <summary>
    ''' grdOperative Row edit event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdOperative_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdOperative.RowEditing
        Try
            Dim dt As DataTable = New DataTable()
            dt = SessionManager.getTradesDataTable()

            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Dim dr = dt.Rows(e.NewEditIndex)
                Dim tradeID = dr("TRADEID")
                Dim duration = dr("DURATION")
                hdnCompTradeId.Value = dr("COMPTRADEID")
                HdnDuration.Value = duration
                hdnTradeId.Value = tradeID
                ddlTrade.SelectedValue = tradeID
                If duration = 0 Then
                    ddlDuration.SelectedValue = -1
                Else
                    ddlDuration.SelectedValue = duration
                End If

                btnAdd.Visible = False
                btnAmend.Visible = True
                grdOperative.DataSource = SessionManager.getTradesDataTable()
                grdOperative.DataBind()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnAdd click event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            btnAmend.Visible = False
            addRequiredOperatives()
            grdOperative.DataSource = SessionManager.getTradesDataTable()
            grdOperative.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnAmend click event"

    Protected Sub btnAmend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAmend.Click
        Try
            Dim dt As DataTable = New DataTable
            Dim objOperativeListBO As OperativeListBO = New OperativeListBO()
            objOperativeListBO.TradeId = ddlTrade.SelectedValue
            objOperativeListBO.Duration = ddlDuration.SelectedValue
            objOperativeListBO.SOrder = grdOperative.Rows.Count + 1

            Dim results As ValidationResults = Validation.Validate(objOperativeListBO)
            If results.IsValid Then
                If SessionManager.getTradesDataTable() IsNot Nothing Then
                    dt = SessionManager.getTradesDataTable()
                End If

                ' check updated record exist in data base or in list

                Dim existingTradesdt As DataTable = getExistingTradesDataTableViewState()
                Dim existingTrades = (From ps In existingTradesdt Where ps.Item("TradeID").ToString() = ddlTrade.SelectedValue And ps.Item("DURATION").ToString() = ddlDuration.SelectedValue Select ps).FirstOrDefault()

                If Not IsNothing(existingTrades) Then
                    Dim matches = existingTrades.Table().Select("COMPTRADEID <> '" + hdnCompTradeId.Value + "' and TradeID = '" + ddlTrade.SelectedValue + "' and DURATION = '" + ddlDuration.SelectedValue + "'")
                    If matches.Count > 0 Then
                        Dim ErrorMesg = UserMessageConstants.TradeAlreadyAdded '"The Trade has already been added."
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, ErrorMesg, True)
                    Else
                        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                        'End - Check if Trade is already in Trades from database.
                    End If
                End If

                Dim compTradeRow = dt.Select("COMPTRADEID = '" + hdnCompTradeId.Value + "'")
                compTradeRow(0)("TradeID") = ddlTrade.SelectedValue
                compTradeRow(0)("DURATION") = ddlDuration.SelectedValue

                ' set grid Data source
                SessionManager.setTradesDataTable(dt)
                grdOperative.DataSource = dt
                grdOperative.DataBind()
            End If

            '''''''----------------------------------------------''''''''
            btnAmend.Visible = False
            btnAdd.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grid view Row deleting"
    ''' <summary>
    ''' grdOperative Row Deleting event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdOperative_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOperative.RowDeleting
        Try
            Dim dt As DataTable = New DataTable()
            dt = SessionManager.getTradesDataTable()

            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

                Dim dr = dt.Rows(e.RowIndex)

                Dim tradeID = dr("TRADEID")
                Dim duration = dr("DURATION")
                Dim sorder = dr(ApplicationConstants.sorderColumn)

                If (checkCanDelete(tradeID, duration, EditComponent.componentId)) Then
                    dt.Rows.RemoveAt(e.RowIndex)
                    dt.AcceptChanges()

                    Dim order As Integer = 1
                    For Each dtr As DataRow In dt.Rows
                        dtr.Item(ApplicationConstants.sorderColumn) = order
                        order = order + 1
                    Next
                    dt.AcceptChanges()

                    SessionManager.setTradesDataTable(dt)

                    grdOperative.DataSource = SessionManager.getTradesDataTable()
                    grdOperative.DataBind()

                    'Start - Check if Trade is already in Trades from database. 
                    'If yes then Insert in List to delete trades else do nothing
                    'And also if a trade is added to Trades to Insert List, Delete it from there
                    Dim objOperativeListBO As OperativeListBO = New OperativeListBO()
                    objOperativeListBO.TradeId = tradeID
                    objOperativeListBO.Duration = duration
                    objOperativeListBO.SOrder = sorder

                    Dim existingTradesdt As DataTable = getExistingTradesDataTableViewState()
                    Dim existingTrades = (From ps In existingTradesdt Where ps.Item("TradeID").ToString() = objOperativeListBO.TradeId And ps.Item("DURATION").ToString() = objOperativeListBO.Duration Select ps)


                    If existingTrades.Count > 0 Then
                        Dim tradesToDelete As List(Of OperativeListBO) = New List(Of OperativeListBO)

                        If SessionManager.getTradesToDeleteList() IsNot Nothing Then
                            tradesToDelete = SessionManager.getTradesToDeleteList()
                        End If
                        tradesToDelete.Add(objOperativeListBO)
                        SessionManager.setTradesToDeleteList(tradesToDelete)
                    End If

                    Dim tradesToInsert As List(Of OperativeListBO) = Nothing
                    If SessionManager.getTradesToInsertList() IsNot Nothing Then
                        tradesToInsert = SessionManager.getTradesToInsertList()
                    End If

                    If tradesToInsert.Count > 0 Then
                        'AndAlso tradesToInsert.Contains(objOperativeListBO) Then
                        SessionManager.removeTradesToInsertList()
                        For Each item As OperativeListBO In tradesToInsert
                            If item.TradeId = tradeID And item.Duration = duration Then
                                tradesToInsert.Remove(item)
                                Exit For
                            End If
                        Next
                        ' tradesToInsert.RemoveAt(tradesToInsert.IndexOf(objOperativeListBO))
                        SessionManager.setTradesToInsertList(tradesToInsert)
                    End If

                Else
                    grdOperative.DataSource = SessionManager.getTradesDataTable()
                    grdOperative.DataBind()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ComponentTradeDeletionFailed, True)
                End If

                'End - Check if Trade is already in Trades from database.
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Save click event"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim objComponentBO As ComponentsBO = New ComponentsBO()
            objComponentBO.ComponentID = EditComponent.componentId
            objComponentBO.ComponentName = lblComponentName.Text.Trim()
            objComponentBO.IsAccount = chkAccounting.Checked
            objComponentBO.Cycle = ddlCycle.SelectedValue
            objComponentBO.Frequency = ddlFrequency.SelectedValue
            objComponentBO.LabourCost = Convert.ToDecimal(txtLabourCost.Text.Trim())
            objComponentBO.AnnualBudgetCost = Convert.ToDecimal(txtAnnualBudget.Text.Trim())
            objComponentBO.MeterialCost = Convert.ToDecimal(txtMaterialCost.Text.Trim())
            objComponentBO.EditedBy = SessionManager.getPlannedMaintenanceUserId()
            objComponentBO.EditedOn = DateTime.Now

            Dim results As ValidationResults = Validation.Validate(objComponentBO)
            If results.IsValid Then
                Dim tradesOrderDt As DataTable = New DataTable()
                tradesOrderDt = SessionManager.getTradesDataTable()
                'dt.Columns.Remove("COMPONENTID")
                tradesOrderDt.Columns.Remove("TRADE")

                Dim tradesToInsert As List(Of OperativeListBO) = New List(Of OperativeListBO)

                If SessionManager.getTradesToInsertList() IsNot Nothing Then
                    tradesToInsert = SessionManager.getTradesToInsertList()
                End If

                Dim tradesToDelete As List(Of OperativeListBO) = Nothing
                If SessionManager.getTradesToDeleteList() IsNot Nothing Then
                    tradesToDelete = SessionManager.getTradesToDeleteList()
                End If

                Dim objComponentsBl As ComponentsBL = New ComponentsBL()
                'Call Update Component method to update the component detail
                Dim tempTradesToInsert = ConvertToDataTable(tradesToInsert)
                'tempTradesToInsert.Columns.Remove("COMPTRADEID")
                Dim tempTradesToDelete = ConvertToDataTable(tradesToDelete)
                'tempTradesToDelete.Columns.Remove("COMPTRADEID")
                objComponentsBl.UpdateComponent(objComponentBO, tempTradesToInsert, tempTradesToDelete, tradesOrderDt)

                ''Hide the edit component control and show the view component control
                Dim editComponentControl As EditComponent = CType(Me.Parent.FindControl("EditComponent"), EditComponent)
                Dim viewComponentControl As ViewComponent = CType(Me.Parent.FindControl("ViewComponent"), ViewComponent)
                Dim componentList As ComponentList = CType(Me.Parent.FindControl("ComponentList"), ComponentList)

                editComponentControl.Visible = False
                viewComponentControl.Visible = True
                Dim resultDataSet As DataSet = New DataSet()
                viewComponentControl.populateComponentDetail(resultDataSet, EditComponent.componentId)

                Dim selectedIndex As Integer = componentList.getSelectedRowIndex()
                Dim componentListResultDataSet As DataSet = New DataSet()
                componentList.populateComponentList(componentListResultDataSet, True)
                componentList.applyDeselectedRowStyle()
                componentList.selectGridViewRow(selectedIndex)

            Else
                Dim message = String.Empty
                For Each result As ValidationResult In results
                    message += result.Message

                Next
                uiMessageHelper.IsError = True
                uiMessageHelper.message = message

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            'If (uiMessageHelper.message.Equals(UserMessageConstants.ComponentTradeDeletionFailed)) Then
            '    showViewComponentControl()
            'End If

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Check component trade can be deleted"
    ''' <summary>
    ''' Check component trade can be deleted
    ''' </summary>
    ''' <remarks></remarks>
    Function checkCanDelete(ByVal tradeId As Integer, ByVal duration As Double, ByVal componentId As Integer)

        Dim objComponentsBl As ComponentsBL = New ComponentsBL()
        Dim check As Integer = objComponentsBl.checkCanDelete(tradeId, duration, componentId)

        Return IIf(check = 1, False, True)

    End Function

#End Region

#Region "btn canel click event"

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            btnAdd.Visible = True
            showViewComponentControl()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "Show view component control"
    ''' <summary>
    ''' Hide the edit component control and show the view component control
    ''' </summary>
    ''' <remarks></remarks>
    Sub showViewComponentControl()

        Dim editComponentControl As EditComponent = CType(Me.Parent.FindControl("EditComponent"), EditComponent)
        Dim viewComponentControl As ViewComponent = CType(Me.Parent.FindControl("ViewComponent"), ViewComponent)
        editComponentControl.Visible = False
        viewComponentControl.Visible = True
        Dim resultDataSet As DataSet = New DataSet()
        viewComponentControl.populateComponentDetail(resultDataSet, Me.componentId)
    End Sub

#End Region

#Region " Component Look Up Values"
    ''' <summary>
    ''' Component Look Up Values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetAddFaultLookUpValues()
        getCycleLookUpValues(ddlCycle)
        getTradeLookUpValues(ddlTrade)
        getDurationLookUpValues(ddlDuration)
    End Sub

    'End Region "Add Fault Look Up Values"
#End Region

#Region "Look UP Values Functions"
#Region "Trades Lookup Values"
    Private Sub getTradeLookUpValues(ByRef ddlname As DropDownList)

        Dim lstLookUp As New List(Of LookUpBO)
        Dim objSelLookUpBO As LookUpBO = New LookUpBO("-1", "Select Trade")
        lstLookUp.Add(objSelLookUpBO)

        Dim objComponentsBl As ComponentsBL = New ComponentsBL()
        objComponentsBl.getTradeLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub
#End Region

#Region "Cycle Lookup Values"

    Private Sub getCycleLookUpValues(ByRef ddlname As DropDownList)

        Dim lstLookUp As New List(Of LookUpBO)
        For i As Integer = 1 To 100
            Dim objLookUpBO As LookUpBO = New LookUpBO(i, i.ToString())
            lstLookUp.Add(objLookUpBO)
        Next i
        PopulateLookup(ddlname, lstLookUp)
    End Sub
#End Region

#Region "Duration Lookup Values"

    Private Sub getDurationLookUpValues(ByRef ddlname As DropDownList)

        ddlname.Items.Add(New ListItem("Select duration (Hours)", "-1"))
        GeneralHelper.populateHoursInAnHourDuration(ddlname)
    End Sub
#End Region
#End Region

#Region "Populate Lookup"

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values from given lookuplist.
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="lstLookup">LookUpList: LookUpList to Bind with dropdown list</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with lookup values in lookup list.</remarks>
    ''' 
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Select", ""))
            ddlLookup.DataBind()
        End If
    End Sub

    'End Region "Populate Lookup"
#End Region

#Region "Populate component detail"
    ''' <summary>
    ''' Populate component detail and operatives detail
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Public Sub populateComponentDetail(ByRef resultDataSet As DataSet)
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            SessionManager.removeTradesToDeleteList()
            SessionManager.removeTradesToInsertList()
            removeExistingTradesDataTableViewState()
            resultDataSet = SessionManager.getConponentDetailDataSet()
            If resultDataSet.Tables.Count = 2 Then
                If Not resultDataSet.Tables(0).Rows.Count = 0 Then
                    Dim componentdt As DataTable = New DataTable()
                    Dim operativesdt As DataTable = New DataTable()
                    componentdt = resultDataSet.Tables("Componentdetail")
                    If Not componentdt.Rows.Count = 0 Then
                        componentId = componentdt.Rows(0)("COMPONENTID")
                        lblComponentName.Text = componentdt.Rows(0)("COMPONENTNAME")
                        chkAccounting.Checked = componentdt.Rows(0).Item("ISACCOUNTING")
                        ddlCycle.SelectedValue = componentdt.Rows(0).Item("CYCLE")
                        ddlFrequency.SelectedValue = componentdt.Rows(0).Item("FREQUENCY")
                        'txtLabourCost.Text = componentdt.Rows(0).Item("LABOURCOST")
                        'txtAnnualBudget.Text = componentdt.Rows(0).Item("ANNUALBUDGETCOST")
                        'txtMaterialCost.Text = componentdt.Rows(0).Item("MATERIALCOST")

                        Dim ldec_AnnualBudget As Double
                        If Double.TryParse(componentdt.Rows(0).Item("ANNUALBUDGETCOST"), ldec_AnnualBudget) Then
                            txtAnnualBudget.Text = ldec_AnnualBudget.ToString()
                        End If

                        Dim materialcost As Double
                        If Double.TryParse(componentdt.Rows(0).Item("MATERIALCOST").ToString(), materialcost) Then
                            txtMaterialCost.Text = materialcost.ToString()
                        End If

                        Dim labourcost As Double
                        If Double.TryParse(componentdt.Rows(0).Item("LABOURCOST").ToString(), labourcost) Then
                            txtLabourCost.Text = labourcost.ToString()
                        End If


                    End If
                    operativesdt = resultDataSet.Tables("OperativesDetail")
                    SessionManager.removeTradesDataTable()
                    SessionManager.setTradesDataTable(operativesdt)
                    setExistingTradesDataTableViewState(operativesdt)
                    grdOperative.DataSource = operativesdt
                    grdOperative.DataBind()
                    ' updOperativesList.Update()
                    grdOperative.Attributes.Add("rules", "None")

                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add Trade to Trade Grid - Amend Fault"

    Private Sub addRequiredOperatives()
        Dim dt As DataTable = New DataTable

        Dim objOperativeListBO As OperativeListBO = New OperativeListBO()
        objOperativeListBO.TradeId = ddlTrade.SelectedValue
        objOperativeListBO.Duration = ddlDuration.SelectedValue
        objOperativeListBO.SOrder = grdOperative.Rows.Count + 1

        Dim results As ValidationResults = Validation.Validate(objOperativeListBO)
        If results.IsValid Then
            If SessionManager.getTradesDataTable() IsNot Nothing Then
                dt = SessionManager.getTradesDataTable()
            End If

            'Create a new row
            Dim dr = dt.NewRow

            If ddlTrade.SelectedValue <> "" And ddlTrade.SelectedValue <> -1 Then

                dr(ApplicationConstants.durationColumn) = ddlDuration.SelectedValue
                dr(ApplicationConstants.tradeIdColumn) = ddlTrade.SelectedValue
                dr(ApplicationConstants.tradeColumn) = ddlTrade.SelectedItem.Text
                dr(ApplicationConstants.sorderColumn) = dt.Rows.Count + 1

            End If
            Dim operativeResult = (From ps In dt Where ps.Item("TradeID").ToString() = ddlTrade.SelectedValue And ps.Item("DURATION").ToString() = ddlDuration.SelectedValue Select ps)

            'Start - Check if Trade is Already in the Trades Table/Grid If yes Show Message else Add
            If operativeResult.Count > 0 Then
                Dim ErrorMesg = UserMessageConstants.TradeAlreadyAdded '"The Trade has already been added."
                uiMessageHelper.setMessage(lblMessage, pnlMessage, ErrorMesg, True)
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                dt.Rows.Add(dr)

                SessionManager.setTradesDataTable(dt)
                grdOperative.DataSource = dt
                grdOperative.DataBind()
                'Start - Check if Trade is already in Trades from database.
                'If no then Insert in List to add trades else do nothing
                'And also if trade is already in tradesToDelete list, delete it from there


                Dim existingTradesdt As DataTable = getExistingTradesDataTableViewState()
                Dim existingTrades = (From ps In existingTradesdt Where ps.Item("TradeID").ToString() = ddlTrade.SelectedValue And ps.Item("DURATION").ToString() = ddlDuration.SelectedValue Select ps)

                If Not IsNothing(existingTrades) Then
                    Dim tradesToInsert As List(Of OperativeListBO) = New List(Of OperativeListBO)

                    If SessionManager.getTradesToInsertList() IsNot Nothing Then
                        tradesToInsert = SessionManager.getTradesToInsertList()
                    End If
                    tradesToInsert.Add(objOperativeListBO)
                    SessionManager.setTradesToInsertList(tradesToInsert)
                End If

                Dim tradesToDelete As List(Of OperativeListBO) = Nothing
                If SessionManager.getTradesToDeleteList() IsNot Nothing Then
                    tradesToDelete = SessionManager.getTradesToDeleteList()
                End If

                If tradesToDelete IsNot Nothing AndAlso tradesToDelete.Count > 0 _
                    AndAlso tradesToDelete.Contains(objOperativeListBO) Then
                    tradesToDelete.Remove(objOperativeListBO)
                    SessionManager.setTradesToDeleteList(tradesToDelete)
                End If

                'End - Check if Trade is already in Trades from database.
            End If
        Else
            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message

            Next
            uiMessageHelper.IsError = True
            uiMessageHelper.message = message
        End If
    End Sub

#End Region

#Region "Convert list to datatable"
    ''' <summary>
    ''' Convert Generic list to data table
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="list"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertToDataTable(Of T)(ByVal list As List(Of T)) As DataTable
        Dim dt As New DataTable()
        For Each info As PropertyInfo In GetType(T).GetProperties()
            dt.Columns.Add(New DataColumn(info.Name, info.PropertyType))
        Next
        If list IsNot Nothing Then
            For Each tt As T In list
                Dim row As DataRow = dt.NewRow()
                For Each info As PropertyInfo In GetType(T).GetProperties()
                    row(info.Name) = info.GetValue(tt, Nothing)
                Next
                dt.Rows.Add(row)
            Next
        End If
        Return dt
    End Function

#End Region

#End Region

#Region "view state function"
#Region "Existing Trades DataTable View State Get, Set and Remove Method/Function(s)"

    Protected Sub setExistingTradesDataTableViewState(ByRef Tradesdt As DataTable)
        ViewState(ViewStateConstants.ExistingTradesDataTable) = Tradesdt
    End Sub

    Protected Function getExistingTradesDataTableViewState() As DataTable
        If ViewState(ViewStateConstants.ExistingTradesDataTable) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.ExistingTradesDataTable), DataTable)
        Else
            Return New DataTable()
        End If
    End Function

    Protected Sub removeExistingTradesDataTableViewState()
        ViewState.Remove(ViewStateConstants.ExistingTradesDataTable)
    End Sub

#End Region

#End Region

End Class