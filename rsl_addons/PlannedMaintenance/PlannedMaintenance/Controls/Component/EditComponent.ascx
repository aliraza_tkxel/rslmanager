﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EditComponent.ascx.vb"
    Inherits="PlannedMaintenance.EditComponent" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel ID="updPanelViewComponent" runat="server" style="margin-right: 30px;">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="portlet">
            <div class="header pnlHeading">
                <asp:Label ID="lblComponentName" CssClass="header-label" runat="server" Text=""></asp:Label>
                <div class="field right">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-xs btn-gray"
                        style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-xs btn-blue"
                        ValidationGroup="save" style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                </div>
            </div>
            <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                <asp:HiddenField ID="hdnCompTradeId" runat="server" value="" />
                <asp:HiddenField ID="HdnDuration" runat="server" value="" />
                <asp:HiddenField ID="hdnTradeId" runat="server" value="" />
                <div class="editComponentDetail">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 40%;">
                                Component Accounting:
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkAccounting" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">
                                Cycle:
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCycle" runat="server" CssClass="editControlSmall">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="editControlNormal">
                                    <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                    <asp:ListItem Value="yrs" Text="Years"></asp:ListItem>
                                    <asp:ListItem Value="mnths" Text="Months"></asp:ListItem>
                                </asp:DropDownList>
                        </tr>
                        <tr>
                            <td style="width: 40%;">
                                Annual Budget:
                                <div style='float: right; font-weight: normal;'>
                                    £ &nbsp;</div>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAnnualBudget" runat="server" CssClass="editControlSmall" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAnnualBudget"
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">
                                Material cost:
                                <div style='float: right; font-weight: normal;'>
                                    £ &nbsp;</div>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtMaterialCost" runat="server" CssClass="editControlSmall" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMaterialCost"
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">
                                Labour Cost:<div style='float: right; font-weight: normal;'>
                                    £ &nbsp;</div>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLabourCost" runat="server" CssClass="editControlSmall" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLabourCost"
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;" valign="top">
                                Required operatives:
                            </td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlTrade" runat="server" CssClass="editControlLarge">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;" valign="top">
                                &nbsp;
                            </td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlDuration" runat="server" CssClass="editControlLarge">
                                </asp:DropDownList>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="editComponentAddButton" />
                                <asp:Button ID="btnAmend" runat="server" Text="Amend" CssClass="editComponentAddButton" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;" valign="top">
                                &nbsp;
                            </td>
                            <td align="left" valign="top">
                                <asp:UpdatePanel ID="updOperativesList" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="grdOperative" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            BorderStyle="Solid" BorderWidth="1px" GridLines="None" BorderColor="#BBBBBB"
                                            Font-Bold="False" CssClass="component-trade-list">
                                            <Columns>
                                                <asp:TemplateField HeaderText="TRADE" >
                                                    <ItemTemplate>
                                                       <asp:Label Id="lblTRADE" runat="server" Text='<%# Eval("TRADE") %>'></asp:label>
                                                   </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="DURATION" >
                                                    <ItemTemplate>
                                                       <asp:Label Id="lblDURATION" runat="server" Text='<%# Eval("DURATION") %>'></asp:label>
                                                   </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit"><img src="../../Images/edit.png" alt="Edit Trade"  style="border:none; width:16px; " /> </asp:LinkButton>
                                                        <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete"><img src="../../Images/cross.png" alt="Delete Fault"  style="border:none; width:16px; " />  </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="10px" BorderStyle="None" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
