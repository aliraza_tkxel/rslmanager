﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ViewComponent.ascx.vb"
    Inherits="PlannedMaintenance.ViewComponent" %>

<asp:UpdatePanel ID="updPanelViewComponent" runat="server" style="margin-right: 30px;">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="portlet">
            <div class="header pnlHeading">
                <asp:Label ID="lblComponentName" CssClass="header-label" runat="server" Text=""></asp:Label>
                <div class="field right">
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="viewComponentEditButton btn btn-xs btn-blue"
                        style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                </div>
            </div>
            <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                <div class="viewComponentDetail">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 40%;">Component Accounting:
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAccounting" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">Cycle:</td>
                            <td align="left">
                                <asp:Label ID="lblCycle" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">Annual Budget:</td>
                            <td align="left">
                                <asp:Label ID="lblAnnualBudgetCost" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <td style="width: 40%;">Material cost:</td>
                        <td align="left">
                            <asp:Label ID="lblMaterialCost" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 40%;">Labour Cost:</td>
                            <td align="left">
                                <asp:Label ID="lblLabourCost" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 40%;">Required operatives:</td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                <asp:GridView ID="grdOperative" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                    BorderStyle="None" BorderWidth="0px" GridLines="None">
                                    <Columns>
                                        <asp:BoundField DataField="TRADE" HeaderText="Trade"
                                            ShowHeader="False" />
                                        <asp:BoundField DataField="DURATION" HeaderText="Duration" ShowHeader="False" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <%--<div class="viewContainer">
            <div class="viewComponent">
                <div class="viewComponentName">
                    Component:
                    
                </div>
                <div class="viewComponentEdit">
                    
                </div>
            </div>
            <div class="viewComponentDetail">
                
            </div>
        </div>--%>
    </ContentTemplate>
</asp:UpdatePanel>