﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ComponentList.ascx.vb"
    Inherits="PlannedMaintenance.ComponentList" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>

<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
<div style=" border: 1px solid #A0A0A0; width: 100%; padding:0">
    <cc1:PagingGridView ID="grdComponentList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
        OnRowCreated="grdComponentList_RowCreated" OnSelectedIndexChanged="grdComponentList_SelectedIndexChanged" OnSelectedIndexChanging ="grdComponentList_SelectedIndexChanging" 
        Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="40">
        <Columns>
            <asp:TemplateField HeaderText="Component:" ItemStyle-CssClass="dashboard" SortExpression="COMPONENTNAME">
                <ItemTemplate>
                    <asp:HiddenField ID="hdnComponentID" runat="server" Value='<%# Bind("COMPONENTID") %>' />
                <asp:Label ID="lblCOMPONENTNAME" runat="server" Text='<%# Bind("COMPONENTNAME") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle BorderStyle="None" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Last edited:" SortExpression="EDITEDON">
                <ItemTemplate>
                    <asp:Label ID="lblRecorded" runat="server" Text='<%# Bind("EDITEDON") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                <ItemStyle HorizontalAlign="Left" Width="30%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="By:" SortExpression="EDITEDBY">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("USERNAME") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                <ItemStyle HorizontalAlign="Left" Width="20%" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
</div>
