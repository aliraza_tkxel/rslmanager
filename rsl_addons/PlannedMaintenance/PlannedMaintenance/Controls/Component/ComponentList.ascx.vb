﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class ComponentList
    Inherits UserControlBase

#Region "Properties"
    Dim objComponentsBl As ComponentsBL = New ComponentsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "SOrder", 1, 10000)
    Dim totalCount As Integer = 0
   

#End Region

#Region "Events"
#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub
#End Region

#Region "Grid Component List Page Index Changing Event"
    ''' <summary>
    ''' Grid Component List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdComponentList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdComponentList.PageIndexChanging

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdComponentList.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim resultDataSet As DataSet = New DataSet()
            populateComponentList(resultDataSet, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Component List Sorting Event"
    ''' <summary>
    ''' Grid Completed Fault List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdComponentList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdComponentList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdComponentList.PageIndex = 0
            objPageSortBo.setSortDirection()
            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Dim resultDataSet As DataSet = New DataSet()
            populateComponentList(resultDataSet, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdComponentList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Grid Component List row data bound"
    Protected Sub grdComponentList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdComponentList.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.BackColor = Drawing.Color.White
                Dim row As GridViewRow = e.Row
                e.Row.Attributes("onclick") = Me.Page.GetPostBackClientEvent(Me.grdComponentList, "Select$" & e.Row.RowIndex)
                e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'")

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
#End Region

#Region " Grid Component List Selected Index Changed "
    Protected Sub grdComponentList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdComponentList.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdComponentList.SelectedRow
            Dim componentId As HiddenField = DirectCast(row.FindControl("hdnComponentID"), HiddenField)
            row.Style.Add("background-color", "#BBBBBB")
            row.Style.Add("font-weight", "normal")
            Dim viewComponetControl As ViewComponent = CType(Me.Parent.FindControl("ViewComponent"), ViewComponent)
            Dim resultDataSet As DataSet = New DataSet()
            viewComponetControl.populateComponentDetail(resultDataSet, Convert.ToInt32(componentId.Value))
            Dim editComponentControl As EditComponent = CType(Me.Parent.FindControl("EditComponent"), EditComponent)
            editComponentControl.Visible = False
            viewComponetControl.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Grid Component List Selected Index Changing"
    Protected Sub grdComponentList_SelectedIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles grdComponentList.SelectedIndexChanging
        Try
            applyDeselectedRowStyle()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Get selected row index"
    ''' <summary>
    ''' Get selected row index
    ''' </summary>
    ''' <remarks></remarks>
    Function getSelectedRowIndex()
        Return grdComponentList.SelectedRow.RowIndex
    End Function

#End Region

#Region "Apply deselected row style"
    ''' <summary>
    ''' Apply deselected row style
    ''' </summary>
    ''' <remarks></remarks>
    Sub applyDeselectedRowStyle()
        Dim row As GridViewRow = grdComponentList.SelectedRow
        If Not IsNothing(row) Then
            row.Style.Add("background-color", "#FFFFFF")
            row.Style.Add("font-weight", "normal")
        End If
    End Sub

#End Region

#Region "Select GridView row"
    ''' <summary>
    ''' Select GridView row
    ''' </summary>
    ''' <remarks></remarks>
    Sub selectGridViewRow(ByVal index As Integer)
        grdComponentList.SelectRow(index)
    End Sub

#End Region

#Region "Populate Components List"

    Sub populateComponentList(ByRef resultDataSet As DataSet, ByVal setSession As Boolean)

        totalCount = objComponentsBl.getComponentsForLifeCycle(resultDataSet, objPageSortBo)

        If Not resultDataSet.Tables(0).Rows.Count = 0 Then

            If setSession Then
                ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
                ViewState.Add(ViewStateConstants.TotalCount, totalCount)
            End If

            grdComponentList.VirtualItemCount = totalCount
            grdComponentList.DataSource = resultDataSet
            grdComponentList.DataBind()

            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            If grdComponentList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdComponentList.PageCount Then
                setVirtualItemCountViewState(totalCount)
                setResultDataSetViewState(resultDataSet)
            End If

        End If

    End Sub

#End Region

#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()
        ' ViewState.Item(ViewStateConstants.Search) = ""
        ' ViewState.Item(ViewStateConstants.OperativeId) = -1
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdComponentList.VirtualItemCount = totalCount
        grdComponentList.DataSource = resultDataSet
        grdComponentList.DataBind()

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdComponentList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdComponentList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region
#End Region


   


End Class