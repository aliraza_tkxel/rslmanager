﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net



Public Class AppointmentArranged
    Inherits UserControlBase

#Region "Properties"

    Public ReadOnly Property GetScheduleWorkType As String
        Get
            Dim type As String = ""
            If Not IsNothing(Request.QueryString(PathConstants.ScheduleWorksType)) Then
                type = Request.QueryString(PathConstants.ScheduleWorksType)
            End If
            Return type
        End Get
    End Property

    Public ReadOnly Property GetPageSize As Integer
        Get
            Return 10
        End Get
    End Property
#End Region

#Region "Events"

#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
        End If

    End Sub

#End Region

#Region "Grid Appointments Arranged List Page Index Changing Event"
    ''' <summary>
    ''' Grid Appointments Arranged List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentsArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentsArranged.PageIndexChanging

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdAppointmentsArranged.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim appointmentDate As String = ViewState.Item(ViewStateConstants.AppointmentDate)
            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentsArrangedList(resultDataSet, search, schemeId, componentId, appointmentDate, False, GetScheduleWorkType)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Appointments Arranged List Sorting Event"
    ''' <summary>
    ''' Grid Appointments Arranged List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentsArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentsArranged.Sorting

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdAppointmentsArranged.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim appointmentDate As String = ViewState.Item(ViewStateConstants.AppointmentDate)
            Dim resultDataSet As DataSet = New DataSet()

            populateAppointmentsArrangedList(resultDataSet, search, schemeId, componentId, appointmentDate, False, GetScheduleWorkType)
            setResultDataSetViewState(resultDataSet)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim appointmentDate As String = ViewState.Item(ViewStateConstants.AppointmentDate)
            populateAppointmentsArrangedList(resultDataSet, search, schemeId, componentId, appointmentDate, False, GetScheduleWorkType)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
                Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
                Dim appointmentDate As String = ViewState.Item(ViewStateConstants.AppointmentDate)
                populateAppointmentsArrangedList(resultDataSet, search, schemeId, componentId, appointmentDate, False, GetScheduleWorkType)
            Else
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"
    ' ''' <summary>
    ' ''' Bind To Grid
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Sub bindToGrid()
    '    Dim search As String
    '    Dim schemeid As Integer
    '    Dim componentid As Integer
    '    'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
    '    '    componentId = Request.QueryString(PathConstants.ComponentId)
    '    'End If

    '    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    '    Dim totalCount As Integer = 0
    '    Dim resultDataSet As DataSet = New DataSet()

    '    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)

    '    ViewState.Add(ViewStateConstants.Search, search)
    '    ViewState.Add(ViewStateConstants.SchemeId, schemeId)

    '    objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
    '    'ViewState.Item(ViewStateConstants.Search) = String.Empty
    '    'ViewState.Item(ViewStateConstants.SchemeId) = -1

    '    'Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
    '    '
    '    'resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)

    '    objSchedulingBL.getAppointmentsArrangedList(resultDataSet, objPageSortBo, schemeId, search, componentId)

    '    grdAppointmentsArranged.VirtualItemCount = totalCount
    '    grdAppointmentsArranged.DataSource = resultDataSet
    '    grdAppointmentsArranged.DataBind()

    '    objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

    '    ' If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then
    '    If grdAppointmentsArranged.PageCount > 1 Then
    '        pnlPagination.Visible = True
    '        setVirtualItemCountViewState(totalCount)
    '        setResultDataSetViewState(resultDataSet)
    '    End If

    'End Sub

#End Region

#Region "Arrow button event"
    ''' <summary>
    ''' Arrow button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnArrow_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim lnkBtnArrow As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnArrow.NamingContainer, GridViewRow)
            Dim lblJsn As Label = DirectCast(row.FindControl("lblJSN"), Label)
            Dim pmo As Integer = Convert.ToInt32(lnkBtnArrow.CommandArgument.ToString())
            'this fuction will save the data in session (on row which user clicked)
            saveSelectedDataInSession(lblJsn.Text, pmo)
            'redirect the page to job sheet
            Dim responseUrl As String = PathConstants.ViewArrangedAppointments + "?" + PathConstants.Jsn + "=" + lblJsn.Text + "&" + PathConstants.Pmo + "=" + Convert.ToString(pmo)
            If GetScheduleWorkType <> "" Then
                responseUrl = responseUrl + "&" + PathConstants.ScheduleWorksType + "=" + GetScheduleWorkType
            End If

            Dim ddlParent As DropDownList = Parent.FindControl("ddlComponents")
            If Not IsNothing(ddlParent) Then
                responseUrl = responseUrl + "&" + PathConstants.ComponentId + "=" + ddlParent.SelectedValue
            End If

            Response.Redirect(responseUrl)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "btn Cancel Appointment Click"
    ''' <summary>
    ''' event handler for schedule button.
    ''' get the journal id from the command argument
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancelAppointment_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument
            Dim cancelBtn As ImageButton = CType(sender, ImageButton)
            Dim journalId As String = CType(cancelBtn.CommandArgument(), String)
            ViewState.Item(ViewStateConstants.Pmo) = journalId
            Dim cAddress As String, cDue As Integer
            cDue = 0
            cAddress = ""
            'save the filtered row in new data table and save that data table in session
            Dim appointmentToBeArrangeDs As DataSet = New DataSet()
            'get the result data set from view state
            appointmentToBeArrangeDs = Me.getResultDataSetViewState()
            Dim query = (From res In appointmentToBeArrangeDs.Tables(0)
                         Where res("Ref") = (journalId) And (res("Duration") Is Nothing = False)
                         Select res)
            Dim plannedAppointment As PlannedAppointmentBO = New PlannedAppointmentBO()
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            If query.Count() > 0 Then
                'get the row of related to specific journal id and save it in new datatable

                objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
                'save pmo 
                plannedAppointment.PMO = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("RefSort"), Integer)
                ViewState.Item(ViewStateConstants.Pmo) = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("RefSort"), Integer)
                'save property id
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"))) Then
                    plannedAppointment.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
                    plannedAppointment.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"), String)
                    ViewState.Item(ViewStateConstants.PropertyId) = plannedAppointment.PropertyId
                    ViewState.Item(ViewStateConstants.Type) = plannedAppointment.Type
                End If

                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"))) Then
                    plannedAppointment.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
                    ViewState.Item(ViewStateConstants.ComponentId) = plannedAppointment.ComponentId
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Component"))) Then
                    plannedAppointment.ComponentName = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Component"), String)
                    ViewState.Item(ViewStateConstants.Component) = plannedAppointment.ComponentName
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Address"))) Then
                    If (plannedAppointment.Type = ApplicationConstants.propertyType) Then
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Address"), String)
                    ElseIf (plannedAppointment.Type = ApplicationConstants.schemeType) Then
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PJScheme"), String)
                    Else
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PJBlock"), String)
                    End If
                    'cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Address"), String)
                    ViewState.Item(ViewStateConstants.Address) = cAddress
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Due"))) Then
                    cDue = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Due"), Integer)
                    ViewState.Item(ViewStateConstants.Due) = cDue
                Else
                    cDue = CType(0, Integer)
                    ViewState.Item(ViewStateConstants.Due) = cDue
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("DueDateFull"))) Then
                    ViewState.Item(ViewStateConstants.DueDateFull) = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("DueDateFull"), DateTime)
                Else
                    ViewState.Item(ViewStateConstants.DueDateFull) = Nothing
                End If
            End If
            showApprovePopup(cAddress, plannedAppointment.ComponentName, cDue.ToString, plannedAppointment.PMO.ToString)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Next Click"
    ''' <summary>
    ''' btn Next Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument

            showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                            ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 0)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Show Approve Popup"
    ''' <summary>
    ''' Show Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showApprovePopup(ByVal propertyAddress As String, ByVal component As String, ByVal due As String,
                         ByVal PMO As String)

        resetApprovePopup()
        lblApprovePropertyAddress.Text = propertyAddress
        lblApproveComponent.Text = component
        If due = "0" Then
            lblApproveDue.Text = "N/A"
        Else
            lblApproveDue.Text = due
        End If
        lblApprovePMO.Text = PMO
        mdlPopUpApproveCondition.Show()

    End Sub

#End Region

#Region "Reset Approve Popup"
    ''' <summary>
    ''' Reset Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetApprovePopup()

        uiMessageHelper.resetMessage(lblApproveErrorMessage, pnlApproveErrorMessage)
        lblApprovePropertyAddress.Text = String.Empty
        lblApproveComponent.Text = String.Empty
        lblApproveDue.Text = String.Empty
        lblApprovePMO.Text = String.Empty

    End Sub

#End Region

#Region "Show Reject Popup"
    ''' <summary>
    ''' Show Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showRejectPopup(ByVal propertyAddress As String, ByVal component As String, ByVal due As String,
                         ByVal PMO As String, ByVal err As Integer)
        If (err = 0) Then
            resetRejectPopup()
        End If

        lblCancelPropertyAddress.Text = propertyAddress
        lblCancelComponent.Text = component
        If due = "0" Then
            due = "N/A"
        End If
        lblCancelDue.Text = due
        lblCancelPMO.Text = PMO
        lblCurrentDue.Text = due
        If (err = 1) Then
            lblErrReject.Text = "All Fields are mandatory and must be filled"
            lblErrReject.Visible = True
        ElseIf (err = 2) Then
            lblErrReject.Text = "Notes length should be less than 300"
            lblErrReject.Visible = True
        Else
            lblErrReject.Visible = False
        End If
        If (err = 0) Then
            Dim resultDataSet As DataSet = New DataSet()
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            objSchedulingBL.getAssociatedAppointmentsList(resultDataSet, ViewState.Item(ViewStateConstants.Pmo))
            For Each r As DataRow In resultDataSet.Tables(0).Rows
                For Each item In r.ItemArray
                    lblAssociatedJSN.Text += "JSN" + item.ToString() + "<br /> "
                Next
            Next
        End If

        mdlPopupRejectCondition.Show()
    End Sub

#End Region

#Region "Reset Reject Popup"
    ''' <summary>
    ''' Reset Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetRejectPopup()

        uiMessageHelper.resetMessage(lblRejectErrorMessage, pnlRejectErrorMessage)

        populateReasonDropdown()
        populateReplacementYearDropdown()
        txtRejectNotes.Text = String.Empty
        lblCancelPropertyAddress.Text = String.Empty
        lblCancelComponent.Text = String.Empty
        lblCancelDue.Text = String.Empty
        lblCancelPMO.Text = String.Empty
        lblCurrentDue.Text = String.Empty
        lblAssociatedJSN.Text = String.Empty
    End Sub

#End Region

#Region "Populate replacement year"
    ''' <summary>
    ''' Populate replacement year from 2000 to 2050
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdown()

        ddlReplacementDue.Items.Clear()

        Dim year As Integer = Convert.ToInt32(DateTime.Now.Year.ToString())

        While (year <= ApplicationConstants.YearEndRange)
            Dim yearItem As ListItem = New ListItem(Convert.ToString(year), Convert.ToString(year))
            ddlReplacementDue.Items.Add(yearItem)
            year = year + 1
        End While

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddlReplacementDue.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate reason dropdown"
    ''' <summary>
    ''' Populate reason dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReasonDropdown()

        ddlReason.Items.Clear()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        objReportsBl.getCancelReasons(resultDataset)

        ddlReason.DataSource = resultDataset.Tables(0).DefaultView
        ddlReason.DataValueField = "ReasonId"
        ddlReason.DataTextField = "Reason"
        ddlReason.DataBind()


        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlReason.Items.Insert(0, item)

    End Sub

#End Region

#Region "btn Confirm Reject Click"
    ''' <summary>
    ''' btn Next Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirmReject(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument

            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim totalCount As Integer = 0, isCancelled As Integer = 0
            Dim pmo, reasonId, reason, userId, prevDue, ipmo, itemId, propId, componentId, type
            Dim newDue As DateTime

            If (ddlReason.SelectedIndex < 1 Or ddlReplacementDue.SelectedIndex < 1 Or txtRejectNotes.Text = "") Then
                showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 1)
            ElseIf (txtRejectNotes.Text.Length > 300) Then
                showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 2)
            Else

                pmo = ViewState.Item(ViewStateConstants.Pmo)
                reasonId = Convert.ToInt32(ddlReason.SelectedValue)
                reason = txtRejectNotes.Text
                userId = SessionManager.getPlannedMaintenanceUserId()
                prevDue = ViewState.Item(ViewStateConstants.DueDateFull)
                itemId = ViewState.Item(ViewStateConstants.ItemId)
                propId = ViewState.Item(ViewStateConstants.PropertyId)
                type = ViewState.Item(ViewStateConstants.Type)
                newDue = Format(DateTime.Now(), "ddMMMyyyy")
                newDue = newDue.AddYears(ddlReplacementDue.SelectedIndex - 1)
                ipmo = Integer.Parse(pmo)
                componentId = ViewState.Item(ViewStateConstants.ComponentId)
                isCancelled = objSchedulingBL.cancelAppointmentToBeArrangedList(ipmo, reasonId, reason, userId, prevDue, newDue, itemId, propId, componentId, type)
                If isCancelled = 1 Then
                    sendCancelledEmail(pmo)
                End If

                Dim responseUrl As String = PathConstants.ScheduleWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab
                If GetScheduleWorkType <> "" Then
                    responseUrl = responseUrl + "&" + PathConstants.ScheduleWorksType + "=" + GetScheduleWorkType
                End If
                Page.Response.Redirect(responseUrl, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region



#Region "Send Cancelled Email"
    ''' <summary>
    ''' Send Cancelled Email
    ''' </summary>
    ''' <remarks></remarks>
    Sub sendCancelledEmail(pmo As Integer)
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim name As String, jsn As String, reasonE As String, address As String, postCode As String, startDate As String, startTime As String, email As String
        Dim resultDataSet As DataSet = New DataSet()
        objSchedulingBL.getEmailAppointmentsDetail(resultDataSet, pmo)
        address = resultDataSet.Tables(1).Rows.Item(0).Item("HOUSENUMBER").ToString() + " " + resultDataSet.Tables(1).Rows.Item(0).Item("Address1").ToString() + " " + resultDataSet.Tables(1).Rows.Item(0).Item("Address2").ToString() + " , " +
            resultDataSet.Tables(1).Rows.Item(0).Item("TownCity").ToString() + " , " + resultDataSet.Tables(1).Rows.Item(0).Item("County").ToString()
        postCode = resultDataSet.Tables(1).Rows.Item(0).Item("PostCode").ToString()
        For Each row As DataRow In resultDataSet.Tables(0).Rows
            name = row.Item("Operative")
            jsn = row.Item("JSN")
            startDate = row.Item("StartDate")
            startTime = row.Item("StartTime")
            email = row.Item("Email")
            reasonE = ddlReason.SelectedItem.Text
            '----------------------------------------------------------------------------------
            If String.IsNullOrEmpty(email) _
                OrElse Not PL_Validation.isEmail(email) Then

                Throw New Exception("Unable to send email, invalid email address.")
            Else

                Dim body As New StringBuilder
                Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/CancelEmailToOperative.html"))
                body.Append(reader.ReadToEnd())

                ' Set contractor detail(s) '
                '==========================================='
                'Populate Contractor Contact Name
                body.Replace("{OperativeName}", name)
                '==========================================='



                ' Populate work, estimate and cost details
                '==========================================='
                ' Set PMO (planned work reference)
                body.Replace("{JSN}", jsn)
                'Set Order Id 
                body.Replace("{StartDate}", startDate)
                'Set Ordered By
                body.Replace("{StartTime}", startTime)
                'Set DD Dial
                body.Replace("{Reason}", reasonE)

                body.Replace("{Address}", address)
                'Set DD Dial
                body.Replace("{PostCode}", postCode)
                '==========================================='

                ' There is an asbestos present at the below property. Please contact Broadland Housing for details.
                ' Attach logo images with email
                '==========================================='

                Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                '==========================================='

                Dim mailMessage As New Mail.MailMessage

                mailMessage.Subject = "Cancelled Planned Works : JSN# " & jsn
                mailMessage.To.Add(New MailAddress(email, name))
                ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                mailMessage.Body = body.ToString
                mailMessage.AlternateViews.Add(alternatevw)
                mailMessage.IsBodyHtml = True
                EmailHelper.sendEmail(mailMessage)

            End If





            '----------------------------------------------------------




        Next row

    End Sub

#End Region

#Region "Populate Appointments Arranged List"
    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="search"></param>
    ''' <param name="schemeId"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Sub populateAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal schemeId As Integer, ByVal componentId As Integer,
                                         ByVal appointmentDate As String, ByVal setSession As Boolean, Optional ByRef type As String = "")
        'Dim componentId As Integer = -1

        'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
        '    componentId = Request.QueryString(PathConstants.ComponentId)
        'End If

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim totalCount As Integer = 0

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)

        ViewState.Add(ViewStateConstants.Search, search)
        ViewState.Add(ViewStateConstants.SchemeId, schemeId)
        ViewState.Add(ViewStateConstants.AppointmentComponentId, componentId)
        ViewState.Add(ViewStateConstants.AppointmentDate, appointmentDate)

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        totalCount = objSchedulingBL.getAppointmentsArrangedList(resultDataSet, objPageSortBo, schemeId, search, componentId, appointmentDate, type)

        grdAppointmentsArranged.VirtualItemCount = totalCount
        grdAppointmentsArranged.DataSource = resultDataSet
        grdAppointmentsArranged.DataBind()

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        ' If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If objPageSortBo.TotalPages > 1 Then
            ' pnlPagination.Visible = True
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then
            grdAppointmentsArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentsArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentsArranged.DataSource = getResultDataSetViewState()
            grdAppointmentsArranged.DataBind()
        End If

    End Sub

#End Region

#Region "Save Selected Data In Session"
    ''' <summary>
    ''' This function will save the selected data in session (when user clicked on blue arrow)
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <param name="pmo"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedDataInSession(ByVal jsn As String, ByVal pmo As Integer)
        'get the result data set 
        Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        'get the record which is clicked/selected
        Dim query = (From res In resultDataSet.Tables(0).AsEnumerable Where res.Item("JSN") = jsn Select res)
        'save that selected record into datatable
        objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
        'save the datatable having selected record in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
        'ceate the object of planned appointment bo
        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo.PMO = pmo
        'save property id, coponent id and jsn
        objPlannedAppointmentBo.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
        objPlannedAppointmentBo.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"), String)
        objPlannedAppointmentBo.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
        objPlannedAppointmentBo.Jsn = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("AppointmentId"), Integer)
        'save the planned appointment bo in session. This will be used later on intelligent scheduling screen
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)


    End Sub
#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region


End Class