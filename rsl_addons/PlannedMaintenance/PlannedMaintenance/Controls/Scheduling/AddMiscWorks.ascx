﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AddMiscWorks.ascx.vb"
    Inherits="PlannedMaintenance.AddMiscWorks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Panel runat="server" ID="pnlAddMiscWorks">
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });


        function SearchText() {

            $('#<%= txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "MiscWorks.aspx/getPropertySearchResult",
                        data: "{'searchText':'" + $('#<%= txtSearch.ClientID %>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1]
                                }
                            }));

                        },
                        error: function (result) {

                        }
                    });
                },
                select: function (event, ui) {

                    document.getElementById('<%= hdnSelectedPropertyId.ClientID %>').value = ui.item.val
                }
            });
        }
    </script>
    <asp:Panel ID="pnlAddMiscMessage" runat="server" Visible="False">
        <br />
        <asp:Label ID="lblAddMiscMessage" runat="server"></asp:Label>
        <br />
    </asp:Panel>
    <table style="width: 100%;" cellspacing="10">
        <tr>
            <td valign="top">
                <b>Find a property:</b>
            </td>
            <td>
                <asp:TextBox ID="txtSearch" runat="server" Width="85%" class="searchbox" AutoPostBack="false"
                    AutoCompleteType="Search"></asp:TextBox>
                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                </ajaxToolkit:TextBoxWatermarkExtender>
                <div style="display: none;">
                    <asp:HiddenField ID="hdnSelectedPropertyId" runat="server" />
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <b>Works required:</b>
            </td>
            <td>
                <asp:TextBox ID="txtWorksRequired" runat="server" TextMode="MultiLine" Height="200px"
                    Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <b>Operative trade:</b>
            </td>
            <td>
                <asp:DropDownList ID="ddlTrade" Width="102%" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <b>Duration:</b>
            </td>
            <td>
                <asp:DropDownList ID="ddlDuration" Width="102%" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <b>Start date:</b>
            </td>
            <td>
                <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:TextBox ID="txtDate" Width="100%" runat="server"></asp:TextBox>
                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                    TargetControlID="txtDate" WatermarkText="(Optional)" WatermarkCssClass="searchTextDefault">
                </ajaxToolkit:TextBoxWatermarkExtender>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="text-align: right;">
                <br />
                <asp:Button ID="btnFindOperative" runat="server" Text="Find me an operative" />
            </td>
        </tr>
    </table>
</asp:Panel>
