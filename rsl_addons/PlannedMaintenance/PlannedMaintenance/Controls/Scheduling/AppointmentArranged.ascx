﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentArranged.ascx.vb"
    Inherits="PlannedMaintenance.AppointmentArranged" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div style="height: 500px; overflow: auto;">
    <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
        <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
    </asp:Panel>
    <cc2:PagingGridView ID="grdAppointmentsArranged" runat="server" AllowPaging="False"
        AllowSorting="True" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White"
        CellPadding="2" ForeColor="Black" GridLines="None"  PageSize="30" HeaderStyle-CssClass="gridfaults-header"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Ref:" SortExpression="Ref" ItemStyle-Width="5%">
                <ItemTemplate>
                    <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ItemStyle-Width="5%">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="20%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme:" SortExpression="PJScheme">
                <ItemTemplate>
                    <asp:Label ID="lblPJScheme" runat="server" Text='<%# Bind("PJScheme") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="20%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block:" SortExpression="PJBlock">
                <ItemTemplate>
                    <asp:Label ID="lblPJBlock" runat="server" Text='<%# Bind("PJBlock") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="20%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                <ItemTemplate>
                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                <ItemTemplate>
                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="14%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Type:" SortExpression="AppointmentType">
                <ItemTemplate>
                    <asp:Label ID="lblAppointmentType" runat="server" Text='<%# Bind("AppointmentType") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Trade:" SortExpression="TradeTitle">
                <ItemTemplate>
                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("TradeTitle") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Operative:" SortExpression="Operative">
                <ItemTemplate>
                    <asp:Label ID="lblOperative" runat="server" Text='<%# Bind("Operative") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Duration:" SortExpression="Duration">
                <ItemTemplate>
                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment:" SortExpression="Appointment">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status:" SortExpression="PLANNEDSTATUS">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("PlannedSubStatus") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="9%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgBtnCancelAppointment" Height="20px" Width="20px" runat="server" CommandArgument='<%#Eval("Ref")%>'
                         OnClick="btnCancelAppointment_Click" src="../../Images/cross2.png" BorderWidth="0" >
                    </asp:ImageButton>
                    
                </ItemTemplate>
                <ItemStyle Width="1%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton  ID="lnkBtnArrow" runat="server" OnClick="lnkBtnArrow_click" CommandArgument='<%#Eval("RefSort")%>'>
                                <img src="../../Images/arrow.gif" alt="Appointment Arranged" style="border:none; float:right;" />
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Width="1%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
        </Columns>

        <RowStyle BackColor="#E6E6E6"></RowStyle>    
        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        <HeaderStyle CssClass="gridfaults-header" />
    </cc2:PagingGridView>
    <br />
    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
        background-color: #FAFAD2; border-color: #ADADAD; border-width: 1px; border-style: Solid;
        vertical-align: middle;">
        <table style="width: 57%; margin: 0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td align="right" style="display: none;">
                                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                              </td>
                        </tr>
                    </tbody>
                </table>

    </asp:Panel>
    <br />

     <!----------------------------------------------------- Approve  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelApprovePopup" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlApproveCondition" CssClass="left" runat="server" Visible="true" BackColor="White"
                            Width="380px" Style=" top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; padding-bottom:10px; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style=" text-align:center; background-color:Black; padding:10px; color:White; font-weight: bold;">
                                <b>Cancel Planned Works </b>
                            </div>
                            
                           <asp:Panel ID="pnlApproveErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblApproveErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <p style="text-align: center;">
                                    You are about to cancel the approved PMO
                                    <asp:Label ID="lblApprovePMO" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    for the
                                    <asp:Label ID="lblApproveComponent" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    at
                                    <asp:Label ID="lblApprovePropertyAddress" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    that are due in
                                    <asp:Label ID="lblApproveDue" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    <br />
                                    Select ‘Next’ if you wish to continue, or ‘Back’
                                    to return to the scheduling area.
                                </p>
                                <asp:Panel ID="pnlApproveButtons" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnBack" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px; float:left;" runat="server" Text="< Back" />
                                    <asp:Button ID="btnNext" OnClick="btnNext_Click" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px;" runat="server" Text="Next >" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopUpApproveCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblApprovePopup" PopupControlID="pnlApproveCondition"
                            DropShadow="true" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblApprovePopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
    <!----------------------------------------------------- Reject  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelRejectPopup" runat="server" >
                    <ContentTemplate>
                        <asp:Panel ID="pnlRejectCondition" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style=" top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; padding-bottom:10px; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style=" text-align:center; background-color:Black; padding:10px; color:White; font-weight: bold;">
                                <b>Cancel Planned Works </b>
                            </div>
                           
                            <asp:Panel ID="pnlRejectErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblRejectErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            
                            <div style="margin-left: 10px; margin-right: 10px;">
                            <asp:Label ID="lblErrReject" ForeColor="Red" Font-Bold="true" runat="server" Text="" Visible = "false">
                                            </asp:Label>
                                <p style="text-align: center;">
                                    You are about to cancel the approved PMO
                                    <asp:Label ID="lblCancelPMO" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    for the
                                    <asp:Label ID="lblCancelComponent" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    at
                                    <asp:Label ID="lblCancelPropertyAddress" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    that are due in
                                    <asp:Label ID="lblCancelDue" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    <br />
                                    The associated JSNs will be cancelled:
                                    <br /> 
                                    <asp:Label ID="lblAssociatedJSN" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                </p>
                                <br />
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            Reason:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReason" Width="160px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Details:
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtRejectNotes" Height="70px" Width="93%" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Current Due Date:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCurrentDue" Font-Bold="true" runat="server" Text="">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            New Due Date:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReplacementDue" Width="120px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                               
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                    TargetControlID="txtRejectNotes" WatermarkText="Please enter some notes explaining the reason for the rejection."
                                    WatermarkCssClass="descriptionText">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <asp:Panel ID="pnlRejectButtons" runat="server" HorizontalAlign="Right">
                                 <asp:Button ID="CancelledBack" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px; float:left;" runat="server" Text="< Back" />
                                    
                                    <asp:Button ID="btnRejectConditionWorks"  BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px;"
                                    OnClick="btnConfirmReject" runat="server"  Text="Confirm cancellation" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopupRejectCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblRejectPopup" PopupControlID="pnlRejectCondition"
                            DropShadow="true"  BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblRejectPopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
          
</div>
