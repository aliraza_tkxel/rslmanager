﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentsCancelled.ascx.vb" Inherits="PlannedMaintenance.AppointmentsCancelled" %>
<%@ Register TagName="AssignToContractor" TagPrefix="uc1" Src="~/Controls/Scheduling/AssignToContractor.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div style="height: 500px; overflow: auto;">
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
    <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
        <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
    </asp:Panel>
    <cc2:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AllowPaging="False"
        AllowSorting="True" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White"
        CellPadding="4" ForeColor="Black" GridLines="None" PageSize="30" HeaderStyle-CssClass="gridfaults-header"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="PMO:" SortExpression="PMO" ItemStyle-Width="40px">
                <ItemTemplate>
                    <asp:Label ID="lblPmo" runat="server" Text='<%# Bind("PMO") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="40px"></ItemStyle>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ItemStyle-Width="40px">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="40px"></ItemStyle>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="11%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                <ItemTemplate>
                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                <ItemTemplate>
                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trade:" SortExpression="Trades">
                <ItemTemplate>
                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("Trades") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Due (Previous):" SortExpression="DueP">
                <ItemTemplate>
                    <asp:Label ID="lblDue" runat="server" Text='<%# Bind("DueP") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Due (New):" SortExpression="Due">
                <ItemTemplate>
                    <asp:Label ID="lblDueP" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Reason:" SortExpression="Reason">
                <ItemTemplate>
                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField Visible ="false">
                <ItemTemplate>
                    <asp:Label ID="lblJSNRef" runat="server" Text='<%# Bind("JSNRef") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnView" Text="View Details" runat="server" OnClick="btnViewClick" CommandArgument='<%#Eval("PMO")%>'
                     visible ='<%# CheckPO(Eval("JSN")) %>'    ></asp:Button>
                </ItemTemplate>
                <ItemStyle Width="65px" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#E6E6E6"></RowStyle>
        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        <HeaderStyle CssClass="gridfaults-header" />
    </cc2:PagingGridView>
    <br />
    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
        background-color: #FAFAD2; border-color: #ADADAD; border-width: 1px; border-style: Solid;
        vertical-align: middle;">
        <table style="width: 57%; margin: 0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button Text="Export to XLS" runat="server" ID="btnExportToExcel" OnClick="btnExportToExcel_Click" />
                            </td>
                            <td align="right" style="display: none;">
                                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                              </td>
                        </tr>
                    </tbody>
                </table>

    </asp:Panel>
    <br />
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
