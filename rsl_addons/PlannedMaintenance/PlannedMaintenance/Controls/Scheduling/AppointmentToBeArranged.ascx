﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentToBeArranged.ascx.vb"
    Inherits="PlannedMaintenance.AppointmentToBeArranged" %>
<%@ Register TagName="AssignToContractor" TagPrefix="uc1" Src="~/Controls/Scheduling/AssignToContractor.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div style="height: 500px; overflow: auto;">
    <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
        <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
    </asp:Panel>
    <cc2:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AllowPaging="False"
        AllowSorting="True" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White"
        CellPadding="4" ForeColor="Black" GridLines="None" PageSize="30" HeaderStyle-CssClass="gridfaults-header"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="PMO:" SortExpression="PMO" ItemStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblPmo" runat="server" Text='<%# Bind("PMO") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px"></ItemStyle>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="15%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block:" SortExpression="PJBlock">
                <ItemTemplate>
                    <asp:Label ID="lblPJBlock" runat="server" Text='<%# Bind("PJBlock") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                <ItemTemplate>
                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                <ItemTemplate>
                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type:" SortExpression="AppointmentType">
                <ItemTemplate>
                    <asp:Label ID="lblAppointmentType" runat="server" Text='<%# Bind("AppointmentType") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trade:" SortExpression="Trades">
                <ItemTemplate>
                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("Trades") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Due:" SortExpression="Due">
                <ItemTemplate>
                    <asp:Label ID="lblDue" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Operatives:" SortExpression="Operatives">
                <ItemTemplate>
                    <asp:Label ID="lblOperatives" runat="server" Text='<%# Bind("Operatives") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Duration:" SortExpression="Duration">
                <ItemTemplate>
                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button Text="Assign to Contractor" ID="btnAssignToContractor" runat="server" style="text-align: center;vertical-align: middle; margin-top:8px;"
                        CommandArgument='<%#Eval("PMO").ToString() + ";" + Eval("PropertyId").ToString() + ";" + Eval("AppointmentTypeID").ToString() + ";" + Eval("Type").ToString()%>'
                        OnClick="btnAssignToContrator_Click" Visible='<%# Boolean.Parse(ConfigurationManager.AppSettings("ShowAssignedToContractorButton")).ToString() %>' />
                    <asp:Button ID="btnSchedule" Text="Schedule" runat="server" CommandArgument='<%#Eval("PMO")%>'
                        OnClick="btnSchedule_Click" style="text-align: center;vertical-align: middle;margin-top:8px;"></asp:Button>
                    <asp:ImageButton ID="imgBtnCancelAppointment" ImageAlign="Middle" runat="server" CommandArgument='<%#Eval("PMO")%>'
                        ImageUrl="~/Images/cross2.png" BorderWidth="0"  OnClick="btnCancelAppointment_Click" />
                </ItemTemplate>
                <ItemStyle Width="215px" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#E6E6E6"></RowStyle>
        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        <HeaderStyle CssClass="gridfaults-header" />
    </cc2:PagingGridView>
    <br />
    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
        background-color: #FAFAD2; border-color: #ADADAD; border-width: 1px; border-style: Solid;
        vertical-align: middle;">
        <table style="width: 57%; margin: 0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td align="right" style="display: none;">
                                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                              </td>
                        </tr>
                    </tbody>
                </table>

    </asp:Panel>
    <br />
    <%-- RISK POPUP - Start --%>
    <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
    <asp:ModalPopupExtender ID="mdlPopupRisk" runat="server" TargetControlID="btnHidden3"
        PopupControlID="pnlRiskAndVulnerability" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlRiskAndVulnerability" runat="server" BackColor="White" Width="350px"
        Style="font-weight: normal; font-size: 13px; padding: 10px;">
        <div style="width: 100%; font-weight: bold;">
            <b>Risk and Vulnerability Info </b>
        </div>
        <div style="clear: both; height: 1px;">
        </div>
        <hr />
        <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
            <asp:Label ID="lblErrorMessage" runat="server">
            </asp:Label>
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlRisks" runat="server" Visible="false">
            The customer has the following Risk
            <br />
            recorded against their current details
            <br />
            <br />
            <asp:GridView ID="grdRisks" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATDESC") %>'></asp:Label>
                            <asp:Label ID="separator" runat="server" Text=' - '></asp:Label>
                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SUBCATDESC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlVulnerability" runat="server" Visible="false">
            The customer has the following Vulnerability
            <br />
            recorded against their current details
            <br />
            <br />
            <asp:GridView ID="grdVulnerability" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATDESC") %>'></asp:Label>
                            <asp:Label ID="separator" runat="server" Text=' - '></asp:Label>
                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SUBCATDESC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="pnlConfirmButton" runat="server" HorizontalAlign="Right">
            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_click" />
        </asp:Panel>
    </asp:Panel>

    <%-- RISK POPUP - End --%>
    <!----------------------------------------------------- Approve  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelApprovePopup" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlApproveCondition" CssClass="left" runat="server" Visible="true" BackColor="White"
                            Width="380px" Style=" top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; padding-bottom:10px; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style=" text-align:center; background-color:Black; padding:10px; color:White; font-weight: bold;">
                                <b>Cancel Planned Works </b>
                            </div>
                            
                           <asp:Panel ID="pnlApproveErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblApproveErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <p style="text-align: center;">
                                    You are about to cancel the approved PMO
                                    <asp:Label ID="lblApprovePMO" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    for the
                                    <asp:Label ID="lblApproveComponent" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    at
                                    <asp:Label ID="lblApprovePropertyAddress" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    that are due in
                                    <asp:Label ID="lblApproveDue" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    <br />
                                    Select ‘Next’ if you wish to continue, or ‘Back’
                                    to return to the scheduling area.
                                </p>
                                <asp:Panel ID="pnlApproveButtons" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnBack" OnClick="btnBack_Click" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px; float:left;" runat="server" Text="< Back" />
                                    <asp:Button ID="btnNext" OnClick="btnNext_Click" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px;" runat="server" Text="Next >" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopUpApproveCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblApprovePopup" PopupControlID="pnlApproveCondition"
                            DropShadow="true" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblApprovePopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
    <!----------------------------------------------------- Reject  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelRejectPopup" runat="server" >
                    <ContentTemplate>
                        <asp:Panel ID="pnlRejectCondition" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style=" top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; padding-bottom:10px; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style=" text-align:center; background-color:Black; padding:10px; color:White; font-weight: bold;">
                                <b>Cancel Planned Works </b>
                            </div>
                           
                            <asp:Panel ID="pnlRejectErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblRejectErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <asp:Label ID="lblErrReject" ForeColor="Red" Font-Bold="true" runat="server" Text="" Visible = "false">
                                            </asp:Label>
                                <p style="text-align: center;">
                                    You are about to cancel the approved PMO
                                    <asp:Label ID="lblCancelPMO" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    for the
                                    <asp:Label ID="lblCancelComponent" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    at
                                    <asp:Label ID="lblCancelPropertyAddress" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    that are due in
                                    <asp:Label ID="lblCancelDue" Font-Bold="true" runat="server" Text="">
                                    </asp:Label>
                                    
                                </p>
                                <br />
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            Reason:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReason" Width="160px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Details:
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtRejectNotes" Height="70px" Width="93%" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Current Due Date:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCurrentDue" Font-Bold="true" runat="server" Text="">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            New Due Date:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReplacementDue" Width="120px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                               
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                    TargetControlID="txtRejectNotes" WatermarkText="Please enter some notes explaining the reason for the rejection."
                                    WatermarkCssClass="descriptionText">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <asp:Panel ID="pnlRejectButtons" runat="server" HorizontalAlign="Right">
                                 <asp:Button ID="CancelledBack" BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px; float:left;" runat="server" Text="< Back" />
                                    
                                    <asp:Button ID="btnRejectConditionWorks"  BorderStyle="Solid" Style=" font-size:small; padding:3px; padding-left:15px; padding-right:15px;" 
                                    runat="server" OnClick="btnConfirmReject" Text="Confirm cancellation" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopupRejectCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblRejectPopup" PopupControlID="pnlRejectCondition"
                            DropShadow="true"  BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblRejectPopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
          
               
    <%-- Modal PopUp Assign to Contractor - Start --%>
    <%-- This hidden button is added just to set as Target Control Id for modal popup extender. --%>
    <asp:Button ID="btnHiddenAssigntoContractor" runat="server" Text="" Style="display: none;" />
    <asp:ModalPopupExtender ID="mdlPopupAssignToContractor" runat="server" TargetControlID="btnHiddenAssigntoContractor"
        PopupControlID="pnlAssignToContractor" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlAssignToContractor" runat="server" Style="min-width: 600px; max-width: 700px;">
        <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        <uc1:AssignToContractor ID="ucAssignToContractor" runat="server" />
    </asp:Panel>
    <%-- Modal PopUp Assign - End --%>
    
</div>
