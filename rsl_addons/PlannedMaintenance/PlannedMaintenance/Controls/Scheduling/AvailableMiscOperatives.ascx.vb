﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AvailableMiscOperatives
    Inherits UserControlBase

#Region "Properties"
#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblAvailableMiscMessage, pnlAvailableMiscMessage)
    End Sub

#End Region

#Region "Find me operative button Event"
    ''' <summary>
    ''' Find me operative button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFindMeOperative_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAvailableMiscMessage, pnlAvailableMiscMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate available operative"
    ''' <summary>
    ''' Populate available operative
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateAvailableOperativesList(ByVal objMiscOperativeSearchBO As MiscOperativeSearchBO)

        ' Dim resultDataSet As DataSet = New DataSet()
        ' objSchedulingBL.getMiscWorkAvailableOperativesList(resultDataSet, objMiscOperativeSearchBO)

        grdAvailableMisc.DataSource = Me.generateDummyOperativesTable()
        grdAvailableMisc.DataBind()



    End Sub

#End Region

#Region "Generate Dummy Operatives table"
    ''' <summary>
    ''' Generate Dummy Operatives table
    ''' </summary>
    ''' <remarks></remarks>
    Function generateDummyOperativesTable() As DataTable

        Dim table As New DataTable

        table.Columns.Add("OperativeId", GetType(Integer))
        table.Columns.Add("Operative", GetType(String))
        table.Columns.Add("StartTime", GetType(String))
        table.Columns.Add("StartDate", GetType(String))
        table.Columns.Add("StartDateTime", GetType(String))

        table.Rows.Add(142, "Kevin Blake", "09:00", " Mon 12 Nov 2013", "09:00 Mon 12 Nov 2013")
        table.Rows.Add(143, "Bob Findley", "10:00", " Tue 13 Nov 2013", "10:00 Tue 13 Nov 2013")
        table.Rows.Add(144, "Kevin Frost", "10:30", " Mon 12 Nov 2013", "10:30 Mon 12 Nov 2013")
        table.Rows.Add(145, "Albert Mallett", "09:00", " Tue 13 Nov 2013", "09:00 Tue 13 Nov 2013")
        table.Rows.Add(146, "Peter Parsons", "09:00", " Mon 12 Nov 2013", "09:00 Mon 12 Nov 2013")

        Return table

    End Function

#End Region

#End Region

End Class