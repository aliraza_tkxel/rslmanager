﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net

Public Enum AssignToContractorType As Integer
    Planned = 1
    Miscellaneous = 2
    Conditional = 3
	Adaptation = 4
End Enum

Public Class AssignToContractor
    Inherits UserControlBase

#Region "Guide Lines to use this User Control"
    ' Please follow these Guide line while referencing this user control in you page.
    '1- Add Jquery in Page or in master page.
    '2- Call Populate by 

#End Region

#Region "Properties"
    Dim Direct = 0
#Region "Is Saved Property to use in consumer page to determine either work is assign to contractor or not"

    Public Property IsSaved() As Boolean
        Get
            Dim isSavedret As Boolean = False
            If Not IsNothing(ViewState(ViewStateConstants.IsSaved)) Then
                isSavedret = CType(ViewState(ViewStateConstants.IsSaved), Boolean)
            End If
            Return isSavedret
        End Get
        Set(ByVal value As Boolean)
            ViewState(ViewStateConstants.IsSaved) = value
        End Set
    End Property

#End Region

#Region "Set Close Button Text - to Customize the button text from consumer"

    Public WriteOnly Property SetCloseButtonText As String
        Set(ByVal value As String)
            btnClose.Text = value
        End Set
    End Property

#End Region

#End Region

#Region "Control Events"

    Public Event CloseButtonClicked As EventHandler

#End Region

#Region "Events Handling"

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

#End Region

#End Region

#Region "Control Events"

#Region "Button Events"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            Page.Validate("addWorkRequired")
            If Page.IsValid Then
                AddPurchaseIteminWorkRequiredDT()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click
        RaiseEvent CloseButtonClicked(Me, New EventArgs)
    End Sub

    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssignToContractor.Click
        Try
            assignWorkToContractor()
            If (ddlContact.Text = "-1") Then
                mdlPopupRis.Show()
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Drop down Events"

#Region "ddl Cost Centre Selected Index Change"

    Protected Sub ddlCostCentre_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCostCentre.SelectedIndexChanged
        Try
            Dim costCentreId As Integer = CType(ddlCostCentre.SelectedValue, Integer)
            GetBudgetHeadDropDownValuesByCostCentreId(costCentreId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Budget Head Selected Index Change"

    Protected Sub ddlBudgetHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBudgetHead.SelectedIndexChanged
        Try
            Dim budgetHeadId As Integer = CType(ddlBudgetHead.SelectedValue, Integer)
            GetExpenditureDropDownValuesByBudgetHeadId(budgetHeadId, SessionManager.getPlannedMaintenanceUserId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region
    Protected Sub btnOk_click(sender As Object, e As EventArgs)
        mdlPopupRis.Hide()
    End Sub

#Region "ddl Vat Selected Index Change"

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlVat.SelectedIndexChanged
        Try
            Page.Validate("addWorkRequired")
            CalculateVatAndTotal()
            ddlVat.Focus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Contractor Selected Index Change"

    Private Sub ddlContractor_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlContractor.SelectedIndexChanged
        GetContactDropDownValuesbyContractorId(ddlContractor.SelectedValue)
    End Sub

#End Region
#Region "ddl Contact Selected Index Change"

    Private Sub ddlContact_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlContact.SelectedIndexChanged

        If (Not ddlContact.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue)) Then
            Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
            Dim detailsForEmailDS As New DataSet()
            objAssignToContractorBL.getContactEmailDetail(ddlContact.SelectedValue, detailsForEmailDS)

            If IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt)) _
                   Or detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt).Rows.Count = 0 Then
                mdlPopupRis.Show()
            End If
        End If


    End Sub

#End Region

#End Region

#Region "TextBox Events"

    Protected Sub txtNetCost_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtNetCost.TextChanged
        Try
            CalculateVatAndTotal()
            txtNetCost.Focus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Grid View Events"

#Region "Grid Works Detail Row data bound"

    Protected Sub grdWorksDeatil_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdWorksDeatil.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim workRequiredDt As DataTable = getWorkRequiredDTViewState()

                If workRequiredDt.Rows.Count > 0 Then
                    e.Row.Cells(1).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.NetCostCol & ")", "").ToString
                    e.Row.Cells(2).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.VatCol & ")", "").ToString
                    e.Row.Cells(3).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.GrossCol & ")", "").ToString
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#End Region

#End Region

#Region "Functions"

#Region "Populate User Control By Passing Values from Page"

    ''' <summary>
    ''' Supply Required PropertyId from page in which you need to refer this user control.
    ''' </summary>
    ''' <param name="propertyId">Required: PropertyId of property for which you want assign a work to contractor</param>
    ''' <param name="assignToContractorWorkType"></param>
    ''' <param name="PMO"></param>
    ''' <remarks></remarks>
    Public Sub PopulateControl(ByVal propertyId As String, ByVal assignToContractorWorkType As AssignToContractorType, Optional ByVal PMO As Integer = -1, Optional ByVal worksRequired As String = "", Optional ByVal type As String = ApplicationConstants.propertyType)


        Parallel.Invoke(AddressOf GetPlannedContractors,
                        AddressOf GetCostCentreDropDownVales)

        GetVatDropDownValues()

        ResetControls()
        txtWorksRequired.Text = worksRequired
        setAssignToContractorWorkTypeViewState(assignToContractorWorkType)
        setPropertyIdViewState(propertyId)
        setPMOViewState(PMO)
        setTypeViewState(type)

    End Sub

#End Region

#Region "Add Purchase Items in Work Required Data Table"

    Private Sub AddPurchaseIteminWorkRequiredDT()

        Dim objWorkRequiredBo As New WorkRequiredBo
        objWorkRequiredBo.WorkRequired = txtWorksRequired.Text.Trim
        objWorkRequiredBo.NetCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
        objWorkRequiredBo.VatIdDDLValue = ddlVat.SelectedValue
        objWorkRequiredBo.Vat = If(IIf(String.IsNullOrEmpty(txtVat.Text.Trim), Nothing, txtVat.Text.Trim), 0.0)
        objWorkRequiredBo.Total = If(IIf(String.IsNullOrEmpty(txtTotal.Text.Trim), Nothing, txtTotal.Text.Trim), 0.0)
        objWorkRequiredBo.ExpenditureId = ddlExpenditure.SelectedValue

        'Dim result = Validation.Validate(Of WorkRequiredBo)(objWorkRequiredBo)

        'If result.IsValid Then

        Dim workRequiredDt = getWorkRequiredDTViewState()
        Dim workRequiredRow = workRequiredDt.NewRow

        workRequiredRow(ApplicationConstants.ExpenditureIdCol) = objWorkRequiredBo.ExpenditureId
        workRequiredRow(ApplicationConstants.WorksRequiredCol) = objWorkRequiredBo.WorkRequired
        workRequiredRow(ApplicationConstants.NetCostCol) = objWorkRequiredBo.NetCost
        workRequiredRow(ApplicationConstants.VatTypeCol) = objWorkRequiredBo.VatIdDDLValue
        workRequiredRow(ApplicationConstants.VatCol) = objWorkRequiredBo.Vat
        workRequiredRow(ApplicationConstants.GrossCol) = objWorkRequiredBo.Total
        workRequiredRow(ApplicationConstants.PIStatusCol) = getPIStatus(objWorkRequiredBo.ExpenditureId, objWorkRequiredBo.Total)

        workRequiredDt.Rows.Add(workRequiredRow)

        resetWorksRequired()

        setWorkRequiredDTViewState(workRequiredDt)

        bindWorkRequiredGrid()

        'Else
        'Dim errorMessages As New StringBuilder
        'For Each item In result
        '    errorMessages.AppendLine(item.Message.ToString() + "<br />")
        'Next

        'If errorMessages.Length > 0 Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, errorMessages.ToString(), True)
        'End If

        'End If
    End Sub

#End Region

#Region "Get Cost Centre Drop Down Values."

    Private Sub GetCostCentreDropDownVales()

        Dim dropDownList = New List(Of DropDownBO)
        Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
        objAssignToContractorBL.GetCostCentreDropDownVales(dropDownList)

        BindDropDownList(ddlCostCentre, dropDownList)

    End Sub

#End Region

#Region "Get Budget Head Drop Down Values by Cost Centre Id"

    Private Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef CostCentreId As Integer)

        If CostCentreId = ApplicationConstants.DropDownDefaultValue Then
            resetBudgetHead()
            resetExpenditureddl()
        Else
            Dim dropDownList As New List(Of DropDownBO)
            Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
            objAssignToContractorBL.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, CostCentreId)
            BindDropDownList(ddlBudgetHead, dropDownList)
        End If

    End Sub

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

    Private Sub GetExpenditureDropDownValuesByBudgetHeadId(ByVal BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        If BudgetHeadId = ApplicationConstants.DropDownDefaultValue Then
            resetExpenditureddl()
        Else
            Dim expenditureBOList As New List(Of ExpenditureBO)
            Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)

            'Save expenditure BO list in Session to get Employee LIMIT and remaining at the stage of adding an item
            ' in work required list and to mark it as pending/in queue for authorization if needed.
            SessionManager.setExpenditureBOList(expenditureBOList)

            ddlExpenditure.Items.Clear()
            ddlExpenditure.DataSource = expenditureBOList
            ddlExpenditure.DataTextField = ApplicationConstants.ddlDefaultDataTextField
            ddlExpenditure.DataValueField = ApplicationConstants.ddlDefaultDataValueField
            ddlExpenditure.DataBind()

            If expenditureBOList.Count = 0 Then
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.NoExpenditureSetup, ApplicationConstants.DropDownDefaultValue))
            Else
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
            End If
        End If
    End Sub

#End Region

#Region "Get Contractor Having Planned Contract"

    Private Sub GetPlannedContractors()
        Dim dropDownList As New List(Of DropDownBO)

        Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
        objAssignToContractorBL.GetPlannedContractors(dropDownList)

        BindDropDownList(ddlContractor, dropDownList)
    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByVal ContractorId As Integer)
        ddlContact.Items.Clear()
        Dim dropDownBoList As New List(Of DropDownBO)

        Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
        objAssignToContractorBL.GetContactDropDownValuesbyContractorId(dropDownBoList, ContractorId)

        If dropDownBoList.Count = 0 Then
            ddlContact.Items.Insert(0, New ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue))
        Else
            BindDropDownList(ddlContact, dropDownBoList)
        End If
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Id as Value Field and Vat Name as text field."

    Sub GetVatDropDownValues()
        Dim vatBoList As New List(Of VatBo)

        Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL

        objAssignToContractorBL.getVatDropDownValues(vatBoList)

        ddlVat.Items.Clear()
        ddlVat.DataSource = vatBoList
        ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlVat.DataBind()

        ddlVat.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))

        ' Save Vat Bo List in session, to get vat rate while adding a work required.
        SessionManager.setVatBOList(vatBoList)

    End Sub

#End Region

#Region "Bind Drop Down List"

    Private Sub BindDropDownList(ByRef ddlToBind As DropDownList, ByRef dropDownItemsList As List(Of DropDownBO), Optional ByVal insertDefault As Boolean = True)
        ddlToBind.Items.Clear()
        ddlToBind.DataSource = dropDownItemsList
        ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlToBind.DataBind()

        If insertDefault Then
            ddlToBind.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Calculate Vat and Total"

    Private Sub CalculateVatAndTotal()
        Dim vatRateId As Integer = ddlVat.SelectedValue
        Dim VatRate As Decimal = 0.0

        If vatRateId >= 0 Then
            VatRate = getVatRateByVatId(vatRateId)
        End If

        Dim netCost As Decimal

        If Decimal.TryParse(txtNetCost.Text.Trim, 0.0) Then
            netCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
            Dim vat As Decimal = netCost * VatRate / 100
            Dim total As Decimal = netCost + vat

            txtNetCost.Text = String.Format("{0:0.00}", netCost)
            txtVat.Text = String.Format("{0:0.00}", vat)
            txtTotal.Text = String.Format("{0:0.00}", total)
        Else
            txtVat.Text = String.Empty
            txtTotal.Text = String.Empty
        End If

    End Sub

#End Region

#Region "Get Vat Rate by VatId"

    Private Function getVatRateByVatId(ByVal vatRateId As Integer) As Decimal
        Dim vatRate As Decimal = 0.0

        Dim vatBoList As List(Of VatBo) = SessionManager.getVatBOList

        Dim vatBo As VatBo = vatBoList.Find(Function(i) i.Id = vatRateId)

        If Not IsNothing(vatBo) Then
            vatRate = vatBo.VatRate
        End If

        Return vatRate
    End Function

#End Region

#Region "Reset Controls (on first populate)"

    Private Sub ResetControls()
        'Set is Saved property to false on populate.
        IsSaved = False
        btnAssignToContractor.Enabled = True
        btnAdd.Enabled = True

        resetBudgetHead()
        resetExpenditureddl()

        'It was excluded on client request. It was not removed from code base for reuse.
        'txtEstimate.Text = String.Empty
        txtEstimateRef.Text = String.Empty

        resetWorksRequired()

        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        removeWorkRequiredDTViewState()
        bindWorkRequiredGrid()

    End Sub

#End Region

#Region "Bind Work Required Grid"

    Private Sub bindWorkRequiredGrid()
        Dim dtWorksRequired As DataTable = getWorkRequiredDTViewState()
        Dim addExtraSpaces As Boolean = False

        If dtWorksRequired.Rows.Count = 0 Then
            For Each col As DataColumn In dtWorksRequired.Columns
                col.AllowDBNull = True
            Next
            Dim newRow As DataRow = dtWorksRequired.NewRow
            dtWorksRequired.Rows.Add(newRow)
            addExtraSpaces = True
        End If

        grdWorksDeatil.DataSource = dtWorksRequired
        grdWorksDeatil.DataBind()
        If addExtraSpaces Then
            grdWorksDeatil.Rows(0).Cells(0).Text = "<br /><br /><br />"
        End If
    End Sub

#End Region

#Region "Assign Work To Contractor"

    Private Sub assignWorkToContractor()

        Dim workRequiredDT As DataTable = getWorkRequiredDTViewState()

        If workRequiredDT.Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.workRequiredCount, True)
        Else
            If Page.IsValid Then
                Dim assignToContractorBo As New AssignToContractorBo
                assignToContractorBo.WorksRequired = workRequiredDT
                assignToContractorBo.ContractorId = ddlContractor.SelectedValue
                assignToContractorBo.EmpolyeeId = ddlContact.SelectedValue
                assignToContractorBo.PMO = getPMOViewState()
                'It was excluded on client request. It was not removed from code base for reuse.
                'assignToContractorBo.Estimate = If(IIf(String.IsNullOrEmpty(txtEstimate.Text.Trim), Nothing, txtEstimate.Text.Trim), 0.0)
                assignToContractorBo.Estimate = Nothing
                assignToContractorBo.EstimateRef = txtEstimateRef.Text
                assignToContractorBo.PropertyId = getPropertyIdViewState()
                assignToContractorBo.Type = getTypeViewState()
                assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(workRequiredDT)
                assignToContractorBo.AssignWorkType = getAssignToContractorWorkTypeViewState()
                assignToContractorBo.UserId = SessionManager.getPlannedMaintenanceUserId
                Direct = assignToContractorBo.POStatus
                Dim objAssignToContractorBl As New AssignToContractorBL
                Dim isSavedStatus As Boolean = objAssignToContractorBl.assignToContractor(assignToContractorBo)

                If isSavedStatus Then
                    'Set Property Is Saved to True.
                    Me.IsSaved = True
                    'Disable btnAssignToContractor to avoid assigning the same work more than once. 
                    btnAssignToContractor.Enabled = False
                    btnAdd.Enabled = False

                    Dim message As String = UserMessageConstants.AssignedToContractor
                    Try
                        If Direct = 3 Then
                            Dim detailsForEmailDS As New DataSet()
                            Dim objAssignToContractor As New AssignToContractorBL
                            objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
                            If Not String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) _
                                                AndAlso PL_Validation.isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) Then
                                sendEmailtoContractor(assignToContractorBo)
                            Else
                                message = message + UserMessageConstants.EmailToContractor
                            End If
                        End If
                    Catch ex As Exception
                        message += "<br />but " + ex.Message
                    End Try
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, False)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AssignedToContractorFailed, True)
                End If

            End If
        End If

    End Sub

#End Region

#Region "Helper Functions"

#Region "Get Pi Status (Pending Status)"

    Private Function getPIStatus(ByVal expenditureId As Integer, ByVal gross As Decimal) As Object
        Dim pIStatus As Integer = 3 ' 3 = "Work Ordered" in table F_POSTATUS
        Dim expenditureBoList As List(Of ExpenditureBO) = SessionManager.getExpenditureBOList
        Direct = 3
        'Check if an item is costing 0 (zero) or less then it should not go to queued list.
        If (gross > 0) Then
            Dim ExpenditureBo As ExpenditureBO = expenditureBoList.Find(Function(i) i.Id = expenditureId)

            If Not IsNothing(ExpenditureBo) Then
                If (gross > ExpenditureBo.Limit OrElse gross > ExpenditureBo.Remaining) Then
                    pIStatus = 0 ' 0 = "Queued" in table F_POSTATUS
                    Direct = 0
                End If
            End If
        End If

        Return pIStatus
    End Function

#End Region

#Region "Set PO Status"

    Private Function getPOStatusFromWorkItemsDt(ByVal workRequiredDT As DataTable) As Integer
        Dim pOStatus As Integer = 3 ' 3 = "Work Ordered" in table F_POSTATUS

        If (workRequiredDT.Select(ApplicationConstants.PIStatusCol & " = 0").Count > 0) Then
            pOStatus = 0 ' 0 = "Queued" in table F_POSTATUS
        End If

        Return pOStatus
    End Function

#End Region

#Region "Populate Body and Send Email to Contractor"

    Private Sub sendEmailtoContractor(ByRef assignToContractorBo As AssignToContractorBo)
        Dim detailsForEmailDS As New DataSet()
        Dim objAssignToContractor As New AssignToContractorBL
        objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)

        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then

                If String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) _
                    OrElse Not PL_Validation.isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) Then

                    Throw New Exception("Unable to send email, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AssignWorkToContractor.html"))
                    body.Append(reader.ReadToEnd())

                    ' Set contractor detail(s) '
                    '==========================================='
                    'Populate Contractor Contact Name
                    body.Replace("{ContractorContactName}", detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName"))
                    '==========================================='



                    ' Populate work, estimate and cost details
                    '==========================================='
                    ' Set PMO (planned work reference)
                    body.Replace("{PMO}", assignToContractorBo.PMO.ToString)
                    'Set Order Id 
                    body.Replace("{OrderId}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("OrderId"))
                    'Set Ordered By
                    body.Replace("{OrderedBy}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("OrderedBy"))
                    'Set DD Dial
                    body.Replace("{DDial}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("DDial"))
                    'Set Email
                    body.Replace("{Email}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("Email"))

                    ' Populate Estimate and Estimate Reference
                    'It was excluded on client request. It was not removed from code base for reuse.
                    'body.Replace("{Estimate}", assignToContractorBo.Estimate)
                    body.Replace("{EstimateRef}", If(assignToContractorBo.EstimateRef, "N/A"))

                    'Get sum/total of net cost from works required data table.
                    body.Replace("{NetCost}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString)
                    'Get sum/total of Vat from works required data table.
                    body.Replace("{VAT}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString)
                    'Get sum/total of Gross/Total from works required data table.
                    body.Replace("{TOTAL}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString)

                    'Get all works detail from works required data table in form of ordered list.
                    body.Replace("{WorksRequired}", getWorksRequired(assignToContractorBo.WorksRequired))
                    '==========================================='


                    ' Property detail(s) (address)
                    '==========================================='

                    'Get Property Address details from property details data set
                    If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDt)) _
                        AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDt).Rows.Count > 0 Then

                        body.Replace("{Bedrooms}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDt).Rows(0)("Bedrooms").ToString())

                        'Set Ordered By
                        If (detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt) IsNot Nothing AndAlso
                            detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt).Rows.Count > 0) Then
                            'Set Asbestos
                            Dim AsbestosInfo As String = ""

                            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt).Rows
                                AsbestosInfo = AsbestosInfo + "<tr>"
                                AsbestosInfo = AsbestosInfo + "<td>" + row("ASBRISKLEVELDESCRIPTION") + " : " + "</td>" + "<td>" + row("RISKDESCRIPTION") & "</td>"
                                AsbestosInfo = AsbestosInfo + "<tr/>"


                            Next

                            body.Replace("{Asbestos}", "{Asbestos}")

                            'TODO: Generate Asbestos Grid from dataset and set here.
                            body.Replace("{Asbestos}", AsbestosInfo)

                            ''It was excluded on client request. It was not removed from code base for reuse.
                            'Dim warning As LinkedResource = New LinkedResource(Server.MapPath("~/Images/warning.png"))
                            'warning.ContentId = "warning_Id"
                            'body.Replace("{Asbestos}", String.Format("cid:{0}", warning.ContentId) + "  There is an asbestos present at the below property. Please contact Broadland Housing for details.")
                        Else
                            body.Replace("{Asbestos}", "None")
                        End If
                        With detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDt)(0)
                            body.Replace("{Address}", .Item("FullStreetAddress"))
                            body.Replace("{TownCity}", .Item("TOWNCITY"))
                            body.Replace("{County}", .Item("COUNTY"))
                            body.Replace("{PostCode}", .Item("POSTCODE"))
                        End With
                    Else
                        ' This case may not occur but it is done for completeness
                        body.Replace("{Address}", "N/A")
                        body.Replace("{TownCity}", "")
                        body.Replace("{County}", "")
                        body.Replace("{PostCode}", "")
                    End If
                    '==========================================='


                    ' Set Tenant Details (name, telephone and vulnerability), in case a tenant is selected
                    '==========================================='

                    'Set Customer Name and Telephone
                    If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)) _
                        AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt).Rows.Count > 0 Then
                        With detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)(0)
                            body.Replace("{TenantName}", .Item("FullName"))
                            body.Replace("{Telephone}", .Item("TEL"))
                            body.Replace("{Mobile}", .Item("MOBILE"))
                        End With
                    Else
                        body.Replace("{TenantName}", "N/A")
                        body.Replace("{Telephone}", "N/A")
                        body.Replace("{Mobile}", "N/A")
                    End If

                    'Get Risk And Vulnerability Details form details data set.
                    body.Replace("{RiskDetail}", getRiskDetails(detailsForEmailDS))
                    body.Replace("{VulnerabilityDetail}", getVulnerabilityDetails(detailsForEmailDS))

                    '==========================================='

                    ' There is an asbestos present at the below property. Please contact Broadland Housing for details.
                    ' Attach logo images with email
                    '==========================================='
                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                    '==========================================='

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString, detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName").ToString))
                    ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub

#End Region

#Region "Get Works Required - Concatenated in form of ordered list to add in email"

    Private Function getWorksRequired(ByVal dataTable As DataTable) As String
        Dim worksRequired As New StringBuilder
        If dataTable.Rows.Count = 1 Then
            worksRequired.Append(dataTable(0)(ApplicationConstants.WorksRequiredCol).ToString)
        Else
            worksRequired.Append("<ol>")
            For Each row As DataRow In dataTable.Rows
                worksRequired.Append("<li>" + row(ApplicationConstants.WorksRequiredCol).ToString + "</li>")
            Next
            worksRequired.Append("</ol>")
        End If
        Return worksRequired.ToString
    End Function

#End Region

#Region "Get Risk Details And Vulnerability Details - As concatenated string split on separate row."

    Private Function getRiskAndVulnerabilityDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim RiskAndVulnerabilityDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) Then

            RiskAndVulnerabilityDetails.Clear()

            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows.Count > 0 Then

                RiskAndVulnerabilityDetails.Append("<Stong>Risk Deatils:</Stong><br />")

                For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows
                    RiskAndVulnerabilityDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
                Next
            End If

            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows.Count > 0 Then

                If Not RiskAndVulnerabilityDetails.ToString = String.Empty Then
                    RiskAndVulnerabilityDetails.Append("<br /><br />")
                End If
                RiskAndVulnerabilityDetails.Append("<Stong>Vulnerability Deatils:</Stong><br />")

                For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows
                    RiskAndVulnerabilityDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
                Next
            End If
        End If

        Return RiskAndVulnerabilityDetails.ToString
    End Function

#End Region

#Region "Get Risk Details - As concatenated string split on separate row."

    Private Function getRiskDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim RiskDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows.Count > 0 Then

            RiskDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows
                RiskDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next
        End If

        Return RiskDetails.ToString
    End Function

#End Region

#Region "Get Vulnerability Details - As concatenated string split on separate row."

    Private Function getVulnerabilityDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim vulnerabilityDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows.Count > 0 Then

            vulnerabilityDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows
                vulnerabilityDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next

        End If

        Return vulnerabilityDetails.ToString
    End Function

#End Region

#Region "Get Property Asbestos Grid/Table to use in Email"

    Private Function GetPropertyAsbestos(ByRef asbestosDt As DataTable) As String

        'For Example Asbestos Grid From Property Summary Page
        '<asp:GridView ID="grdPropertyAsbestos" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
        '    runat="server" AutoGenerateColumns="False" ShowHeader="false" HorizontalAlign="left"
        '    Width="100%" GridLines="None" CellPadding="3" CellSpacing="5">
        '    <Columns>
        '        <asp:BoundField HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-Width="150px"
        '            DataField="ASBRISKLEVELDESCRIPTION" />
        '        <asp:BoundField DataField="RISKDESCRIPTION" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="left" />
        '    </Columns>
        '    <EmptyDataTemplate>
        '        No Records Found</EmptyDataTemplate>
        '</asp:GridView>

        Dim renderedGrid As String = String.Empty

        'Create a grid view for asbestos
        Dim asbestosGrid As New GridView

        'Set Asbestos Grid Properties
        asbestosGrid.ID = "grdPropertyAsbestos"
        asbestosGrid.ShowHeader = False
        asbestosGrid.HorizontalAlign = HorizontalAlign.Left
        asbestosGrid.GridLines = GridLines.None
        asbestosGrid.AutoGenerateColumns = False
        asbestosGrid.ShowHeaderWhenEmpty = False
        asbestosGrid.EmptyDataText = String.Empty
        asbestosGrid.CellSpacing = 5
        asbestosGrid.CellPadding = 3

        'Set Asbestos Grid Columns
        Dim asbriskLevelDescription As New BoundField
        asbriskLevelDescription.DataField = "ASBRISKLEVELDESCRIPTION"
        asbestosGrid.Columns.Add(asbriskLevelDescription)

        Dim riskDescription As New BoundField
        riskDescription.DataField = "RISKDESCRIPTION"
        asbestosGrid.Columns.Add(riskDescription)

        'Bind Data table to asbestosGrid
        asbestosGrid.DataSource = asbestosDt
        asbestosGrid.DataBind()

        'Render Asbestos Grid and get string.
        Using sw As New StringWriter()
            Using hw As New HtmlTextWriter(sw)
                asbestosGrid.RenderControl(hw)
                Dim sr As New StringReader(sw.ToString())
                renderedGrid = sw.ToString()
            End Using
        End Using

        Return renderedGrid

    End Function

#End Region

#Region "Works Required Custom Validation"

    Protected Sub cvWorksRequired_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvWorksRequired.ServerValidate
        Dim length As Integer = Len(txtWorksRequired.Text)
        lblWorksRequiredCount.Text = length.ToString()
        If length < 1 Or length > 4000 Then
            args.IsValid = False
            cvWorksRequired.ErrorMessage = "Works Required must not exceed 4000 characters. Your Characters: " + length.ToString()
        Else
            args.IsValid = True
        End If
    End Sub

#End Region

#End Region

#Region "Controls Reset Functions"

#Region "Reset Expenditure Dropdown List"

    Private Sub resetExpenditureddl()
        ddlExpenditure.Items.Clear()
        If ddlExpenditure.Items.Count = 0 Then
            ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Reset Budget Head"

    Private Sub resetBudgetHead()
        ddlBudgetHead.Items.Clear()
        If ddlBudgetHead.Items.Count = 0 Then
            ddlBudgetHead.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Reset Works Required"

    Private Sub resetWorksRequired()

        If ddlVat.Items.Count > 0 Then
            ddlVat.SelectedIndex = 0
        End If

        txtWorksRequired.Text = String.Empty
        txtNetCost.Text = String.Empty
        txtVat.Text = String.Empty
        txtTotal.Text = String.Empty
    End Sub

#End Region

#End Region

#End Region

#Region "View State Functions"

#Region "Work Required Data Table"

    ''' <summary>
    ''' Get the Work Required Data Table from the view state if a Data Table is not there
    ''' Create a new data table and add required column.
    ''' This is to be used to get data table of this type for the first time.
    ''' </summary>
    ''' <returns>A data table containing the required columns of work required grid.</returns>
    ''' <remarks></remarks>
    Private Function getWorkRequiredDTViewState() As DataTable
        Dim workRequiredDt As DataTable
        workRequiredDt = TryCast(ViewState(ViewStateConstants.WorkRequiredDT), DataTable)

        If IsNothing(workRequiredDt) Then
            workRequiredDt = New DataTable

            '1- Add Work Required Column with data type string
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.WorksRequiredCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.WorksRequiredCol, GetType(String))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = String.Empty
                'Set Maximum length for Work Required Column.
                insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired
            End If

            '2- Add Net Cost Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '3- Add Vat type Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            '4- Add Vat Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '5- Add Gross/Total Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.GrossCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.GrossCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '6- Add PI Status Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = False
            End If

            '7- Add Expenditure ID Col with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            ''6- Add Estimate Column
            'If Not workRequiredDt.Columns.Contains(ApplicationConstants.EstimateCol) Then
            '    Dim insertedColumn As DataColumn
            '    insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.EstimateCol, GetType(Decimal))
            '    insertedColumn.AllowDBNull = False
            'End If

            ''7  - Add Estimate Ref Column
            'If Not workRequiredDt.Columns.Contains(ApplicationConstants.EstimateRefCol) Then
            '    workRequiredDt.Columns.Add(ApplicationConstants.EstimateRefCol, GetType(String))
            'End If

            'Dim row = workRequiredDt.NewRow
            'workRequiredDt.Rows.Add(row)

        End If

        Return workRequiredDt
    End Function

    Private Sub removeWorkRequiredDTViewState()
        ViewState.Remove(ViewStateConstants.WorkRequiredDT)
    End Sub

    Private Sub setWorkRequiredDTViewState(ByVal dt As DataTable)
        ViewState(ViewStateConstants.WorkRequiredDT) = dt
    End Sub

#End Region

#Region "PropertyId"

    Private Function getPropertyIdViewState() As String
        Return TryCast(ViewState(ViewStateConstants.PropertyId), String)
    End Function

    Private Sub setPropertyIdViewState(ByVal propertyId As String)
        ViewState(ViewStateConstants.PropertyId) = propertyId
    End Sub

    Private Sub removePropertyIdViewState()
        ViewState.Remove(ViewStateConstants.PropertyId)
    End Sub

#End Region

#Region "PMO"

    Private Function getPMOViewState() As Integer
        Dim pmo As Integer = -1
        If Not IsNothing(ViewState(ViewStateConstants.Pmo)) Then
            pmo = DirectCast(ViewState(ViewStateConstants.Pmo), Integer)
        End If
        Return pmo
    End Function

    Private Sub setPMOViewState(ByVal pmo As Integer)
        ViewState(ViewStateConstants.Pmo) = pmo
    End Sub

    Private Sub removePMOViewState()
        ViewState.Remove(ViewStateConstants.Pmo)
    End Sub

#End Region

#Region "Type"

    Private Function getTypeViewState() As String
        Return TryCast(ViewState(ViewStateConstants.Type), String)
    End Function

    Private Sub setTypeViewState(ByVal type As String)
        ViewState(ViewStateConstants.Type) = type
    End Sub

    Private Sub removeTypeViewState()
        ViewState.Remove(ViewStateConstants.Type)
    End Sub

#End Region

#Region "Assign Work Type"

    Private Function getAssignToContractorWorkTypeViewState() As AssignToContractorType
        Dim assignToContractorWorkType As AssignToContractorType = AssignToContractorType.Planned
        If Not IsNothing(ViewState(ViewStateConstants.AssignToContractorWorkType)) Then
            assignToContractorWorkType = ViewState(ViewStateConstants.AssignToContractorWorkType)
        End If

        Return assignToContractorWorkType
    End Function

    Private Sub setAssignToContractorWorkTypeViewState(ByVal assignToContractorWorkType As AssignToContractorType)
        ViewState(ViewStateConstants.AssignToContractorWorkType) = assignToContractorWorkType
    End Sub

    Private Sub removeAssignToContractorWorkTypeViewState()
        ViewState.Remove(ViewStateConstants.AssignToContractorWorkType)
    End Sub

#End Region

#End Region

End Class