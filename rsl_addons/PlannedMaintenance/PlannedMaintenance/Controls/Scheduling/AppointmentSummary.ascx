﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentSummary.ascx.vb"
    Inherits="PlannedMaintenance.AppointmentSummary1" %>

            <asp:Panel ID="pnlCustMessage" runat="server" Visible="False">
                <asp:Label ID="lblCustMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>

<div style="padding: 10px; border: 1px solid;">
    <table>
        <tr>
            <td style="width: 20%;">
                PMO: &nbsp &nbsp
                <asp:Label ID="lblPmo" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 66%;">
                Planned works component:
                <asp:Label ID="lblComponent" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 15%;">
                Trade: &nbsp &nbsp
                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</div>
<br />
<br />
<asp:Panel ID="pnlAppointmentDetail" runat="server">
    <div class="leftDiv">
        <table style="width: 100%;" cellspacing="10" cellpadding="5">
            <tr>
                <td class="labelTd">
                    <asp:Label ID="Label2" runat="server" Text="Operative:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="Label3" runat="server" Text="Duration:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="Label4" runat="server" Text="Duration Total:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="leftDiv">
        <table style="width: 100%;" cellspacing="10" cellpadding="5">
           <%-- <tr>
                <td class="labelTd">
                    <asp:Label ID="Label5" runat="server" Text="Time:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="Label6" runat="server" Text="Start Date:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="Label7" runat="server" Text="End Date:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<div style="clear: both">
</div>
<hr />
<div style="clear: both">
</div>
<asp:Panel ID="pnlPropertyDetail" runat="server">
    <div class="leftDiv">
        <table style="width: 100%;" cellspacing="10" cellpadding="5">
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForScheme" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                </td>
            </tr>
             <tr>
                                <td class="labelTd">
                                    <asp:Label ID="lblForBlock" runat="server" Font-Bold="true" Text="Block:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForAddress" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForTowncity" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForCounty" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForPostcode" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="leftDiv">
        <table style="width: 100%;" cellspacing="10" cellpadding="5">
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForCustomerName" runat="server" Text="Customer:"></asp:Label>
                </td>
                <td>
                    <asp:HiddenField ID="hdnCustomerId" runat="server" />
                    <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForCustomerTelephone" runat="server" Text="Telephone:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                    <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForCustomerMobile" runat="server" Text="Mobile:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                    <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="lblForCustomerEmail" runat="server" Text="Email:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                    <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="labelTd">
                    <asp:Label ID="LabelFor18" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="clear: both">
    </div>
    <div style="text-align: right;">
        <asp:Button ID="btnUpdateCustomerDetails" runat="server" Text="Update customer details" />
    </div>
</asp:Panel>
<div style="clear: both">
</div>
<hr />
<div style="clear: both">
</div>

