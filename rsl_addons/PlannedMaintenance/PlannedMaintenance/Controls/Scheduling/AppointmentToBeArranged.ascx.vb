﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AppointmentToBeArranged
    Inherits UserControlBase

#Region "Properties"

    Public ReadOnly Property GetScheduleWorkType As String
        Get
            Dim type As String = ""
            If Not IsNothing(Request.QueryString(PathConstants.ScheduleWorksType)) Then
                type = Request.QueryString(PathConstants.ScheduleWorksType)
            End If
            Return type
        End Get
    End Property

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            resetRejectPopup()
            resetApprovePopup()
            Dim objPageSortBo As PageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
            'objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            If (IsNothing(objPageSortBo)) Then
                objPageSortBo = New PageSortBO("DESC", "Address", 1, 30)
            End If
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
        End If
    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Page Index Changing Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdAppointmentToBeArranged.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, False, GetScheduleWorkType)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Sorting Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdAppointmentToBeArranged.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            Dim resultDataSet As DataSet = New DataSet()

            populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, False, GetScheduleWorkType)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "btn Schedule Click"
    ''' <summary>
    ''' event handler for schedule button.
    ''' get the journal id from the command argument
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSchedule_Click(sender As Object, e As EventArgs)
        Try
            ViewState("AssignToContractorClick") = False
            'get the journal id from the command argument
            Dim scheduleBtn As Button = CType(sender, Button)
            Dim journalId As Integer = CType(scheduleBtn.CommandArgument(), Integer)

            'save the filtered row in new data table and save that data table in session
            If Me.saveDataForScheduling(journalId) = True Then

                'Check whether risk or vulnerability exist against the customer
                If (checkRiskAndVulnerability(journalId)) Then
                    mdlPopupRisk.Show()
                Else
                    SessionManager.removePlannedDurationDict()
                    Me.redirectToBeArrangedAvailableAppointments()
                End If

            Else
                'it means there is an error so show error
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.ComponentTradeDoesNotExist, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Cancel Appointment Click"
    ''' <summary>
    ''' event handler for schedule button.
    ''' get the journal id from the command argument
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancelAppointment_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument
            Dim cancelBtn As ImageButton = CType(sender, ImageButton)
            Dim journalId As Integer = CType(cancelBtn.CommandArgument(), Integer)
            ViewState.Item(ViewStateConstants.Pmo) = journalId
            Dim cAddress As String, cDue As Integer
            cDue = 0
            cAddress = ""
            'save the filtered row in new data table and save that data table in session
            Dim appointmentToBeArrangeDs As DataSet = New DataSet()
            'get the result data set from view state
            appointmentToBeArrangeDs = Me.getResultDataSetViewState()
            Dim query = (From res In appointmentToBeArrangeDs.Tables(0)
                        Where res("PMO") = (journalId) And (res("Duration") Is Nothing = False)
                        Select res)
            Dim plannedAppointment As PlannedAppointmentBO = New PlannedAppointmentBO()
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            If query.Count() > 0 Then
                'get the row of related to specific journal id and save it in new datatable
                
                objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
                'save pmo 
                plannedAppointment.PMO = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PMO"), Integer)
                ViewState.Item(ViewStateConstants.Pmo) = plannedAppointment.PMO
                'save property id
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"))) Then
                    plannedAppointment.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
                    ViewState.Item(ViewStateConstants.PropertyId) = plannedAppointment.PropertyId
                End If

                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"))) Then
                    plannedAppointment.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"), String)
                    ViewState.Item(ViewStateConstants.Type) = plannedAppointment.Type
                End If

                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"))) Then
                    plannedAppointment.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
                    ViewState.Item(ViewStateConstants.ComponentId) = plannedAppointment.ComponentId
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Component"))) Then
                    plannedAppointment.ComponentName = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Component"), String)
                    ViewState.Item(ViewStateConstants.Component) = plannedAppointment.ComponentName
                Else
                    plannedAppointment.ComponentName = "N/A"
                    ViewState.Item(ViewStateConstants.Component) = "N/A"
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Address"))) Then
                    If (plannedAppointment.Type = ApplicationConstants.propertyType) Then
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Address"), String)
                    ElseIf (plannedAppointment.Type = ApplicationConstants.schemeType) Then
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PJScheme"), String)
                    Else
                        cAddress = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PJBlock"), String)
                    End If
                    ViewState.Item(ViewStateConstants.Address) = cAddress
                Else
                    cAddress = "N/A"
                    ViewState.Item(ViewStateConstants.Address) = "N/A"
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Due"))) Then
                    cDue = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Due"), Integer)
                    ViewState.Item(ViewStateConstants.Due) = cDue
                Else
                    cDue = 0
                    ViewState.Item(ViewStateConstants.Due) = cDue
                End If

                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("DueDateFull"))) Then
                    ViewState.Item(ViewStateConstants.DueDateFull) = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("DueDateFull"), DateTime)
                End If
                If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ItemId"))) Then
                    ViewState.Item(ViewStateConstants.ItemId) = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ItemId"), Integer)
                End If
            End If
                showApprovePopup(cAddress, plannedAppointment.ComponentName, cDue.ToString, plannedAppointment.PMO.ToString)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region


#Region "btn Back Click"
    ''' <summary>
    ''' btn Back Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBack_Click(sender As Object, e As EventArgs)
        Dim x
        x = 0
    End Sub
#End Region


#Region "btn Next Click"
    ''' <summary>
    ''' btn Next Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument

            showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                            ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 0)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region


#Region "btn Confirm Reject Click"
    ''' <summary>
    ''' btn Next Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirmReject(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument

            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim totalCount As Integer = 0
            Dim pmo, reasonId, reason, userId, prevDue, ipmo, itemId, propId, componentId, type
            Dim newDue As DateTime
            If (ddlReason.SelectedIndex < 1 Or ddlReplacementDue.SelectedIndex < 1 Or txtRejectNotes.Text = "") Then
                showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 1)
            ElseIf (txtRejectNotes.Text.Length > 300) Then
                showRejectPopup(ViewState.Item(ViewStateConstants.Address), ViewState.Item(ViewStateConstants.Component),
                ViewState.Item(ViewStateConstants.Due).ToString, ViewState.Item(ViewStateConstants.Pmo), 2)
            Else

                pmo = ViewState.Item(ViewStateConstants.Pmo)
                reasonId = Convert.ToInt32(ddlReason.SelectedValue)
                reason = txtRejectNotes.Text
                userId = SessionManager.getPlannedMaintenanceUserId()
                prevDue = ViewState.Item(ViewStateConstants.DueDateFull)
                itemId = ViewState.Item(ViewStateConstants.ItemId)
                propId = ViewState.Item(ViewStateConstants.PropertyId)
                type = ViewState.Item(ViewStateConstants.Type)
                newDue = Format(DateTime.Now(), "ddMMMyyyy")
                newDue = newDue.AddYears(ddlReplacementDue.SelectedIndex - 1)
                ipmo = Integer.Parse(pmo)
                componentId = ViewState.Item(ViewStateConstants.ComponentId)
                objSchedulingBL.cancelAppointmentToBeArrangedList(ipmo, reasonId, reason, userId, prevDue, newDue, itemId, propId, componentId, type)
                Page.Response.Redirect(Page.Request.Url.ToString(), True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Ok Click"
    ''' <summary>
    ''' btn Ok Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnOk_click(sender As Object, e As EventArgs)
        Try
            If ViewState("AssignToContractorClick") = True Then
                Dim commandArguments() As String = ViewState("AssignToContractorCommanArgument")
                Dim journalId As Integer = CType(commandArguments(0), Integer)
                Dim propertyId As String = CType(commandArguments(1), String)
                Dim appointmentTypeId As Integer = CType(commandArguments(2), Integer)
                Dim type As String = CType(commandArguments(3), String)
                populateAppointmentToBeArrangedList(journalId, propertyId, appointmentTypeId, type:=type)
            Else
                redirectToBeArrangedAvailableAppointments()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Assign to Contractor Click"
    ''' <summary>
    ''' Populate Assign to contractor user control.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAssignToContrator_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ViewState("AssignToContractorClick") = True
            uiMessageHelper.resetMessage(lblMessageControl, pnlMessageControl)
            'Get the journalId(PMO) from command arguments
            Dim btnAssignToContractor As Button = CType(sender, Button)
            Dim commandArguments() As String = btnAssignToContractor.CommandArgument.Split(";")
            Dim journalId As Integer = CType(commandArguments(0), Integer)
            Dim propertyId As String = CType(commandArguments(1), String)
            Dim appointmentTypeID As Integer = CType(commandArguments(2), Integer)
            Dim type As String = CType(commandArguments(3), String)

            'Check whether risk or vulnerability exist against the customer
            If (checkRiskAndVulnerability(journalId)) Then
                ViewState("AssignToContractorCommanArgument") = commandArguments
                mdlPopupRisk.Show()
            Else
                populateAppointmentToBeArrangedList(journalId, propertyId, appointmentTypeID, type:=type)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucAssignToContractor.CloseButtonClicked, imgBtnClosePopup.Click
        Try
            mdlPopupAssignToContractor.Hide()

            'In case a work is assigned to contractor re-populate the appointment to be arranged grid as data is changed.
            If ucAssignToContractor.IsSaved Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
                objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

                objPageSortBo.PageNumber = 1
                grdAppointmentToBeArranged.PageIndex = 0
                objPageSortBo.setSortDirection()

                ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
                Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
                Dim resultDataSet As DataSet = New DataSet()

                populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, False, GetScheduleWorkType)

                DirectCast(Parent.FindControl("updPanelScheduleWorks"), UpdatePanel).Update()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
            populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, False, GetScheduleWorkType)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
                Dim componentId As Integer = ViewState.Item(ViewStateConstants.AppointmentComponentId)
                populateAppointmentToBeArrangedList(resultDataSet, search, schemeId, componentId, False, GetScheduleWorkType)
            Else
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()

        ViewState.Item(ViewStateConstants.Search) = String.Empty
        ViewState.Item(ViewStateConstants.SchemeId) = -1

        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        grdAppointmentToBeArranged.DataBind()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Schedule Appointment To Be Arranged"
    ''' <summary>
    ''' Schedule Appointment To Be Arranged
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub scheduleAppointmentToBeArranged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            'Dim propertyId As String = ViewState.Item(ViewStateConstants.PropertyId)
            'Dim customerId As Integer = ViewState.Item(ViewStateConstants.CustomerId)
            'SessionManager.setPropertyId(propertyId)
            'SessionManager.setCustomerId(customerId)

            'Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Jsn + "=" + lblFaultId.Text)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Populate Appointment To Be Arranged List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="search"></param>
    ''' <param name="schemeId"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Sub populateAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal schemeId As Integer, ByVal componentId As Integer,
                                            ByVal setSession As Boolean, Optional ByRef type As String = "")
        'Dim componentId As Integer = -1

        'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
        '    componentId = Request.QueryString(PathConstants.ComponentId)
        'End If

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim totalCount As Integer = 0

        ViewState.Add(ViewStateConstants.Search, search)
        ViewState.Add(ViewStateConstants.SchemeId, schemeId)
        ViewState.Add(ViewStateConstants.AppointmentComponentId, componentId)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        totalCount = objSchedulingBL.getAppointmentToBeArrangedList(resultDataSet, objPageSortBo, schemeId, search, componentId, type)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        Me.setResultDataSetViewState(resultDataSet)
        grdAppointmentToBeArranged.DataBind()

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If objPageSortBo.TotalPages > 1 Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            grdAppointmentToBeArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentToBeArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentToBeArranged.DataSource = getResultDataSetViewState()
            grdAppointmentToBeArranged.DataBind()
        End If

    End Sub

#End Region

#Region "save Data For Scheduling"
    ''' <summary>
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="journalId"></param>
    ''' <remarks></remarks>
    Private Function saveDataForScheduling(ByVal journalId As Integer) As Boolean
        Dim appointmentToBeArrangeDs As DataSet = New DataSet()
        'get the result data set from view state
        appointmentToBeArrangeDs = Me.getResultDataSetViewState()
        Dim query = (From res In appointmentToBeArrangeDs.Tables(0)
                    Where res("PMO") = (journalId) And (res("Duration") Is Nothing = False)
                    Select res)

        If query.Count() > 0 Then
            'get the row of related to specific journal id and save it in new datatable
            Dim plannedAppointment As PlannedAppointmentBO = New PlannedAppointmentBO()
            Dim objPlannedSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
            objPlannedSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
            'save pmo 
            plannedAppointment.PMO = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PMO"), Integer)
            'save property id
            If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"))) Then
                plannedAppointment.PropertyId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
            End If

            If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"))) Then
                plannedAppointment.Type = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("Type"), String)
            End If

            If (Not IsDBNull(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"))) Then
                plannedAppointment.ComponentId = CType(objPlannedSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
            Else
                plannedAppointment.ComponentId = -1
            End If


            'remove the session that is being used in appointment arranged and appointment to be arranged
            SessionManager.removePlannedAppointmentBO()
            SessionManager.removePlannedSchedulingBo()

            'save this planned appointment bo in session . This object 'll be used and populated in next screens
            SessionManager.setPlannedAppointmentBO(plannedAppointment)
            SessionManager.setPlannedSchedulingBo(objPlannedSchedulingBo)
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "redirect To Available Appointments"
    ''' <summary>
    ''' redirect to avaialble appointments
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub redirectToBeArrangedAvailableAppointments()
        Dim responseUrl As String = PathConstants.ToBeArrangedAvailableAppointments
        If GetScheduleWorkType <> "" Then
            responseUrl = responseUrl + "?" + PathConstants.ScheduleWorksType + "=" + GetScheduleWorkType
        End If

        If GetComponentId <> -1 Then
            If responseUrl.Contains("?") Then
                responseUrl = responseUrl + "&" + PathConstants.ComponentId + "=" + GetComponentId
            Else
                responseUrl = responseUrl + "?" + PathConstants.ComponentId + "=" + GetComponentId
            End If
        End If
        Response.Redirect(responseUrl)
    End Sub
#End Region

#Region "Check risk and vulnerability"
    ''' <summary>
    ''' Check risk and vulnerability
    ''' </summary>
    ''' <remarks></remarks>
    Public Function checkRiskAndVulnerability(ByVal journalId As Integer)

        Dim riskExist As Boolean = False
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataset As DataSet = New DataSet()
        objSchedulingBL.getRiskAndVulnerabilityInfo(resultDataset, journalId)

        If (resultDataset.Tables(ApplicationConstants.RiskInfo).Rows.Count > 0) Then
            grdRisks.DataSource = resultDataset.Tables(ApplicationConstants.RiskInfo).DefaultView
            grdRisks.DataBind()
            pnlRisks.Visible = True

            riskExist = True
        Else
            pnlRisks.Visible = False
        End If

        If (resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).Rows.Count > 0) Then
            grdVulnerability.DataSource = resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).DefaultView
            grdVulnerability.DataBind()
            pnlVulnerability.Visible = True

            riskExist = True

        Else
            pnlVulnerability.Visible = False
        End If

        Return riskExist

    End Function

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Scheme", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Show Approve Popup"
    ''' <summary>
    ''' Show Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showApprovePopup(ByVal propertyAddress As String, ByVal component As String, ByVal due As String,
                         ByVal PMO As String)

        resetApprovePopup()
        lblApprovePropertyAddress.Text = propertyAddress
        lblApproveComponent.Text = component
        If due = "0" Then
            lblApproveDue.Text = "N/A"
        ELSE
            lblApproveDue.Text = due
        End If
        lblApprovePMO.Text = PMO
        mdlPopUpApproveCondition.Show()

    End Sub

#End Region

#Region "Reset Approve Popup"
    ''' <summary>
    ''' Reset Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetApprovePopup()

        uiMessageHelper.resetMessage(lblApproveErrorMessage, pnlApproveErrorMessage)
        lblApprovePropertyAddress.Text = String.Empty
        lblApproveComponent.Text = String.Empty
        lblApproveDue.Text = String.Empty
        lblApprovePMO.Text = String.Empty

    End Sub

#End Region

#Region "Show Reject Popup"
    ''' <summary>
    ''' Show Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showRejectPopup(ByVal propertyAddress As String, ByVal component As String, ByVal due As String,
                         ByVal PMO As String, ByVal err As Integer)
        If (err = 0) Then
            resetRejectPopup()
        End If
        lblCancelPropertyAddress.Text = propertyAddress
        lblCancelComponent.Text = component
        If due = "0" Then
            due = "N/A"
        End If
        lblCancelDue.Text = due
        lblCancelPMO.Text = PMO
        lblCurrentDue.Text = due
        If (err = 1) Then
            lblErrReject.Text = "All Fields are mandatory and must be filled"
            lblErrReject.Visible = True
        ElseIf (err = 2) Then
            lblErrReject.Text = "Notes length should be less than 300"
            lblErrReject.Visible = True
        Else
            lblErrReject.Visible = False
        End If
        mdlPopupRejectCondition.Show()
    End Sub

#End Region

#Region "Reset Reject Popup"
    ''' <summary>
    ''' Reset Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetRejectPopup()

        uiMessageHelper.resetMessage(lblRejectErrorMessage, pnlRejectErrorMessage)
       
        populateReasonDropdown()
        populateReplacementYearDropdown()
        txtRejectNotes.Text = String.Empty
        lblCancelPropertyAddress.Text = String.Empty
        lblCancelComponent.Text = String.Empty
        lblCancelDue.Text = String.Empty
        lblCancelPMO.Text = String.Empty
        lblCurrentDue.Text = String.Empty
    End Sub

#End Region

#Region "Populate replacement year"
    ''' <summary>
    ''' Populate replacement year from 2000 to 2050
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdown()

        ddlReplacementDue.Items.Clear()

        Dim year As Integer = Convert.ToInt32(DateTime.Now.Year.ToString())

        While (year <= ApplicationConstants.YearEndRange)
            Dim yearItem As ListItem = New ListItem(Convert.ToString(year), Convert.ToString(year))
            ddlReplacementDue.Items.Add(yearItem)
            year = year + 1
        End While

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddlReplacementDue.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate reason dropdown"
    ''' <summary>
    ''' Populate reason dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReasonDropdown()

        ddlReason.Items.Clear()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        objReportsBl.getCancelReasons(resultDataset)

        ddlReason.DataSource = resultDataset.Tables(0).DefaultView
        ddlReason.DataValueField = "ReasonId"
        ddlReason.DataTextField = "Reason"
        ddlReason.DataBind()


        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlReason.Items.Insert(0, item)

    End Sub

#End Region


#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region

    Sub populateAppointmentToBeArrangedList(ByVal journalId As Integer, ByVal propertyId As String, ByVal appointmentTypeID As Integer, ByVal type As String)
        ucAssignToContractor.PopulateControl(propertyId, appointmentTypeID, journalId, type:=type)
        mdlPopupAssignToContractor.Show()
    End Sub
End Class