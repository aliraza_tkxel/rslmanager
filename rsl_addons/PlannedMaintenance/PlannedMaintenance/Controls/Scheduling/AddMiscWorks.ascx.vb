﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AddMiscWorks
    Inherits UserControlBase

#Region "Properties"

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblAddMiscMessage, pnlAddMiscMessage)
        If Not (IsPostBack) Then
            populateTradeDropdownList()
            populateDurationDropdownList()
            setupControlProperties()

        End If

    End Sub

#End Region

#Region "Setup control properties"

    Sub setupControlProperties()
        txtDate.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Validate Add Misc Works Fields"
    ''' <summary>
    ''' Validate Add Misc Works Fields
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function validateFields() As Boolean

        uiMessageHelper.resetMessage(lblAddMiscMessage, pnlAddMiscMessage)

        Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objMiscOperativeSearchBO = Me.getMiscOperativeSearchBO()

        Dim dateTimeText As Date
        Dim isValidDate As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)

        Dim message = String.Empty
        Dim isValid As Boolean = True

        If (String.IsNullOrEmpty(objMiscOperativeSearchBO.PropertyId) Or String.IsNullOrWhiteSpace(objMiscOperativeSearchBO.PropertyId)) Then
            message += UserMessageConstants.SelectProperty + "<br />"
            isValid = False
        ElseIf (String.IsNullOrEmpty(objMiscOperativeSearchBO.Worksrequired) Or String.IsNullOrWhiteSpace(objMiscOperativeSearchBO.Worksrequired)) Then
            message += UserMessageConstants.EnterWorksRequired + "<br />"
            isValid = False

        ElseIf (objMiscOperativeSearchBO.OperativeTradeId = -1) Then
            message += UserMessageConstants.SelectTrade + "<br />"
            isValid = False
        ElseIf (objMiscOperativeSearchBO.Duration = -1) Then
            message += UserMessageConstants.SelectDuration + "<br />"
            isValid = False
        ElseIf (Not String.IsNullOrEmpty(txtDate.Text)) Then

            If (Convert.ToDateTime(txtDate.Text) <= DateTime.Today()) Then
                message += UserMessageConstants.SelectFutureDate + "<br />"
                isValid = False
            End If

        End If

        uiMessageHelper.setMessage(lblAddMiscMessage, pnlAddMiscMessage, message, True)
        Return isValid

    End Function

#End Region

#Region "Get Misc Operative Search BO"

    Function getMiscOperativeSearchBO()

        Dim objMiscOperativeSearchBO As MiscOperativeSearchBO = New MiscOperativeSearchBO()
        objMiscOperativeSearchBO.PropertyId = hdnSelectedPropertyId.Value
        objMiscOperativeSearchBO.PropertyAddress = txtSearch.Text
        objMiscOperativeSearchBO.Worksrequired = txtWorksRequired.Text
        objMiscOperativeSearchBO.OperativeTradeId = ddlTrade.SelectedValue
        objMiscOperativeSearchBO.Duration = ddlDuration.SelectedValue
        objMiscOperativeSearchBO.StartDate = txtDate.Text
        Return objMiscOperativeSearchBO
    End Function


#End Region


#Region "Find me operative button Event"
    ''' <summary>
    ''' Find me operative button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFindMeOperative_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFindOperative.Click

        Try
            If (validateFields()) Then

                Dim availableMiscOperativesControl As AvailableMiscOperatives = CType(Me.Parent.FindControl("AvailableMiscOperatives"), AvailableMiscOperatives)
                Dim resultDataSet As DataSet = New DataSet()
                availableMiscOperativesControl.populateAvailableOperativesList(Me.getMiscOperativeSearchBO)
                availableMiscOperativesControl.Visible = True

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddMiscMessage, pnlAddMiscMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate all trade"
    ''' <summary>
    ''' Populate all trades
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateTradeDropdownList()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        objReplacementBL.getAllTrades(resultDataSet, ApplicationConstants.AllSelectedValue)

        ddlTrade.DataSource = resultDataSet.Tables(0).DefaultView
        ddlTrade.DataValueField = "TradeId"
        ddlTrade.DataTextField = "Description"
        ddlTrade.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlTrade.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDurationDropdownList()
        Dim newListItem As ListItem
        ddlDuration.Items.Clear()

        For i = 1 To 9

            If (i = 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddlDuration.Items.Add(newListItem)
        Next

        newListItem = New ListItem("Please select", "-1")
        ddlDuration.Items.Insert(0, newListItem)

    End Sub

#End Region

#End Region

End Class