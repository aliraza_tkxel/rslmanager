﻿Imports PL_Utilities
Imports PL_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessObject

Public Class AppointmentSummary1
    Inherits UserControlBase        

#Region "Page Load "
    ''' <summary>
    ''' Page load function
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblCustMessage, pnlCustMessage)
    End Sub
#End Region

#Region "Button Update Customer Detail Click Event"
    ''' <summary>
    ''' Button Update Customer Detail Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateCustomerDetails_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetails.Click
        Try
            Me.updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(CType(Parent.FindControl("lblCustMessage"), Label), CType(Parent.FindControl("pnlCustMessage"), Panel), uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Populate Property Info"
    ''' <summary>
    ''' Populate Property Info
    ''' </summary>
    ''' <remarks></remarks>
    Public Function populatePropertyInfo(ByVal propertyId As String, Optional ByVal type As String = ApplicationConstants.propertyType)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim tenancyId As Integer
        If (String.IsNullOrEmpty(propertyId)) Then
            Dim btnScheduleAppointment As Button = CType(Parent.FindControl("btnScheduleAppointment"), Button)
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False
            Throw New Exception(UserMessageConstants.InvalidPropertyId)
        Else
            Dim resultDataset As DataSet = New DataSet()
            If (type = ApplicationConstants.propertyType) Then
                objSchedulingBL.getPropertyCustomerDetail(resultDataset, propertyId)
            ElseIf (type = ApplicationConstants.schemeType) Then
                objSchedulingBL.getSchemeDetail(resultDataset, propertyId)
            Else
                objSchedulingBL.getBlockDetail(resultDataset, propertyId)
            End If
            'objSchedulingBL.getPropertyCustomerDetail(resultDataset, propertyId)
            Dim resultDt As DataTable = resultDataset.Tables(0)
            controlsVisibilityByType(type)
            'Property Information
            lblScheme.Text = resultDt.Rows(0)("SchemeName")
            lblBlock.Text = resultDt.Rows(0)("Address")
            lblAddress.Text = resultDt.Rows(0)("HOUSENUMBER") + " " + resultDt.Rows(0)("ADDRESS1") + " " + resultDt.Rows(0)("ADDRESS2")
            lblTowncity.Text = resultDt.Rows(0)("TOWNCITY")
            lblCounty.Text = resultDt.Rows(0)("COUNTY")
            lblPostcode.Text = resultDt.Rows(0)("POSTCODE")

            'Customer Information
            hdnCustomerId.Value = resultDt.Rows(0)("CustomerId")
            lblCustomerName.Text = resultDt.Rows(0)("TenantName")
            lblCustomerTelephone.Text = resultDt.Rows(0)("Telephone")
            lblCustomerMobile.Text = resultDt.Rows(0)("Mobile")
            lblCustomerEmail.Text = resultDt.Rows(0)("Email")
            tenancyId = resultDt.Rows(0)("TenancyId")

            If (tenancyId = -1) Then
                btnUpdateCustomerDetails.Enabled = False
            Else
                btnUpdateCustomerDetails.Enabled = True
            End If

        End If
        Return tenancyId
    End Function

#End Region

#Region "Controls Visibility by type"
    Sub controlsVisibilityByType(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblForCustomerName.Visible = False
            lblCustomerName.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            lblForAddress.Visible = False
            lblAddress.Visible = False
            lblTowncity.Visible = False
            lblCounty.Visible = False
            lblPostcode.Visible = False
            lblForTowncity.Visible = False
            lblForCounty.Visible = False
            lblForPostcode.Visible = False
            lblForCustomerTelephone.Visible = False
            lblForCustomerMobile.Visible = False
            lblForCustomerEmail.Visible = False
            LabelFor18.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            Label18.Visible = False
            btnUpdateCustomerDetails.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
#End Region

#Region "Populate Appointment Info"
    ''' <summary>
    ''' this function 'll populate the appointment information
    ''' </summary>    
    ''' <remarks></remarks>
    Public Sub populateAppointmentInfo(ByVal appointmentBo As AppointmentBO)
        lblPmo.Text = appointmentBo.Pmo
        lblComponent.Text = appointmentBo.ComponentName
        lblTrade.Text = appointmentBo.TradeName
        lblOperative.Text = appointmentBo.OperativeName
        lblDuration.Text = appointmentBo.Duration
        lblDurationTotal.Text = appointmentBo.TotalDuration
        'TODO: Remove Unnecessary data
        'lblTime.Text = appointmentBo.Time
        'TODO: Save Date and Time in memory as DateTime data type & Apply formating when binding to UI
        lblStartDate.Text = appointmentBo.StartDate
        lblEndDate.Text = appointmentBo.EndDate
    End Sub

#End Region

#Region "Update Customer Address"
    ''' <summary>
    ''' Update Customer Address
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateAddress()

        'Update Enable
        If btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges

        Else
            'Save Changes
            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                'If Not isError Then
                Dim objCustomerBL As CustomerBL = New CustomerBL()
                Dim objCustomerBO As CustomerBO = New CustomerBO()

                objCustomerBO.CustomerId = hdnCustomerId.Value
                objCustomerBO.Telephone = txtCustomerTelephone.Text
                objCustomerBO.Mobile = txtCustomerMobile.Text
                objCustomerBO.Email = txtCustomerEmail.Text

                Dim results As ValidationResults = Validation.Validate(objCustomerBO)
                If results.IsValid Then


                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblCustMessage, pnlCustMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblCustMessage, pnlCustMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

                    End If
                Else
                    Dim message = String.Empty
                    For Each result As ValidationResult In results
                        message += result.Message
                        Exit For
                    Next
                    uiMessageHelper.setMessage(lblCustMessage, pnlCustMessage, message, True)
                End If
                'End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

            End If

        End If

    End Sub

#End Region

#Region "Disable update customer button"
    ''' <summary>
    ''' Disable update customer button
    ''' </summary>
    ''' <remarks></remarks>
    Sub disableUpdateCustomerButton()
        btnUpdateCustomerDetails.Enabled = False
    End Sub

#End Region


End Class