﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AvailableMiscOperatives.ascx.vb"
    Inherits="PlannedMaintenance.AvailableMiscOperatives" %>



<asp:Panel ID="pnlAvailableMiscMessage" runat="server" Visible="False">
    <br />
    <asp:Label ID="lblAvailableMiscMessage" runat="server"></asp:Label>
    <br />
</asp:Panel>

<asp:GridView ID="grdAvailableMisc" runat="server" AutoGenerateColumns="False" BorderWidth="1px"
    BorderStyle="Solid" Width="100%" AllowSorting="false" AllowPaging="False" GridLines="None"
    HeaderStyle-CssClass="gridfaults-header" CellSpacing="10" HorizontalAlign="Left"
    ClientIDMode="AutoID" CellPadding="2" BorderColor="#BBBBBB">
    <Columns>
        <asp:TemplateField HeaderText="Operative:" HeaderStyle-ForeColor="Black" ConvertEmptyStringToNull="False"
            HeaderStyle-BorderStyle="None" FooterStyle-VerticalAlign="Middle" HeaderStyle-CssClass="sort-expression">
            <ItemTemplate>
                <asp:Label ID="lblOperative" runat="server" Text='<%#Eval("Operative") %>'></asp:Label>
                <asp:Label ID="lblOperativeId" runat="server" Text='<%#Eval("OperativeId") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblStartTime" runat="server" Text='<%#Eval("StartTime") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDate") %>' Visible="false"></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            <ItemStyle Width="100px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Start date:" HeaderStyle-ForeColor="Black" ConvertEmptyStringToNull="False"
            HeaderStyle-BorderStyle="None" FooterStyle-VerticalAlign="Middle" HeaderStyle-CssClass="sort-expression">
            <ItemTemplate>
                <asp:Label ID="lblStartDate" runat="server" Text='<%#Eval("StartDateTime") %>'></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            <ItemStyle Width="300px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-ForeColor="Black" ConvertEmptyStringToNull="False"
            HeaderStyle-BorderStyle="None" FooterStyle-VerticalAlign="Middle" HeaderStyle-CssClass="sort-expression">
            <ItemTemplate>
                <asp:Button ID="btnSchedule" runat="server" Text="Schedule" />
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            <ItemStyle Width="60px" />
        </asp:TemplateField>
    </Columns>
    <EditRowStyle Wrap="True" />
    <HeaderStyle BorderStyle="Solid" Font-Bold="True" BorderColor="Black" BorderWidth="5px" />
    <RowStyle BorderStyle="None" />
</asp:GridView>


