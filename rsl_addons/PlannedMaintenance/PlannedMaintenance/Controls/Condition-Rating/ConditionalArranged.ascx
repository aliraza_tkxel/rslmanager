﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionalArranged.ascx.vb" Inherits="PlannedMaintenance.ConditionalArranged" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<div style="height: 500px; overflow: auto;">
    <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
        <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
    </asp:Panel>
    <cc2:PagingGridView ID="grdAppointmentsArranged" runat="server" AllowPaging="True"
        AllowSorting="True" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White"
        CellPadding="4" ForeColor="Black" GridLines="None"  PageSize="30" HeaderStyle-CssClass="gridfaults-header"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Ref:" SortExpression="Ref" ItemStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ItemStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="30%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                <ItemTemplate>
                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                <ItemTemplate>
                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Operative:" SortExpression="Operative">
                <ItemTemplate>
                    <asp:Label ID="lblOperative" runat="server" Text='<%# Bind("Operative") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Duration:" SortExpression="Duration">
                <ItemTemplate>
                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment:" SortExpression="Appointment">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="30%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnArrow" runat="server" OnClick="lnkBtnArrow_click" CommandArgument='<%#Eval("RefSort")%>'>
                                <img src="../../Images/arrow.gif" alt="Appointment Arranged" style="border:none;" />
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Width="10%" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#E6E6E6"></RowStyle>    
        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        <HeaderStyle CssClass="gridfaults-header" />
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
    </cc2:PagingGridView>
    <br />
</div>
