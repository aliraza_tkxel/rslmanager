﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class ConditionalToBeArranged
    Inherits UserControlBase

#Region "Events"

#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
        End If
    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Page Index Changing Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdAppointmentToBeArranged.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)

            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentToBeArrangedList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Appointment To Be Arranged List Sorting Event"
    ''' <summary>
    ''' Grid Appointment To Be Arranged List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentToBeArranged_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdAppointmentToBeArranged.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)

            Dim resultDataSet As DataSet = New DataSet()

            populateAppointmentToBeArrangedList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "btn Schedule Click"
    ''' <summary>
    ''' event handler for schedule button.
    ''' get the journal id from the command argument
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSchedule_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument
            Dim scheduleBtn As Button = CType(sender, Button)
            Dim journalId As Integer = CType(scheduleBtn.CommandArgument(), Integer)

            'save the filtered row in new data table and save that data table in session
            If Me.saveDataForScheduling(journalId) = True Then

                'Check whether risk or vulnerability exist against the customer
                'If (checkRiskAndVulnerability(journalId)) Then
                '    mdlPopupRisk.Show()
                'Else
                Me.redirectToConditionalAvailableAppointments()
                'End If

            Else
                ''it means there is an error so show error
                'uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.ComponentTradeDoesNotExist, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Ok Click"
    ''' <summary>
    ''' btn Ok Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnOk_click(sender As Object, e As EventArgs)
        Try
            redirectToConditionalAvailableAppointments()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Assign to Contractor Click"
    ''' <summary>
    ''' Populate Assign to contractor user control.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAssignToContrator_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            uiMessageHelper.resetMessage(lblMessageControl, pnlMessageControl)
            'Get the journalId(PMO) from command arguments
            Dim btnAssignToContractor As Button = CType(sender, Button)
            Dim commandArguments() As String = btnAssignToContractor.CommandArgument.Split(";")
            Dim journalId As Integer = CType(commandArguments(0), Integer)
            Dim propertyId As String = CType(commandArguments(1), String)

            ucAssignToContractor.PopulateControl(propertyId, AssignToContractorType.Conditional, journalId)
            mdlPopupAssignToContractor.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucAssignToContractor.CloseButtonClicked, imgBtnClosePopup.Click
        Try
            mdlPopupAssignToContractor.Hide()

            'In case a work is assigned to contractor re-populate the appointment to be arranged grid as data is changed.
            If ucAssignToContractor.IsSaved Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
                objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

                objPageSortBo.PageNumber = 1
                grdAppointmentToBeArranged.PageIndex = 0
                objPageSortBo.setSortDirection()

                ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                Dim resultDataSet As DataSet = New DataSet()

                populateAppointmentToBeArrangedList(resultDataSet, search, False)

                DirectCast(Parent.FindControl("updPanelScheduleWorks"), UpdatePanel).Update()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()

        ViewState.Item(ViewStateConstants.Search) = String.Empty
        ViewState.Item(ViewStateConstants.SchemeId) = -1

        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        grdAppointmentToBeArranged.DataBind()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Schedule Appointment To Be Arranged"
    ''' <summary>
    ''' Schedule Appointment To Be Arranged
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub scheduleAppointmentToBeArranged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            'Dim propertyId As String = ViewState.Item(ViewStateConstants.PropertyId)
            'Dim customerId As Integer = ViewState.Item(ViewStateConstants.CustomerId)
            'SessionManager.setPropertyId(propertyId)
            'SessionManager.setCustomerId(customerId)

            'Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Jsn + "=" + lblFaultId.Text)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Populate Appointment To Be Arranged List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="search"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Sub populateAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim totalCount As Integer = 0

        setSearchTextViewState(search)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)

        If setSession Then
            setResultDataSetViewState(resultDataSet)
            setPageSortBoViewState(objPageSortBo)
            setVirtualItemCountViewState(totalCount)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        totalCount = objSchedulingBL.getConditionAppointmentsToBeArrangedList(resultDataSet, objPageSortBo, search)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        Me.setResultDataSetViewState(resultDataSet)
        grdAppointmentToBeArranged.DataBind()

        objPageSortBo = getPageSortBoViewState()

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentToBeArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentToBeArranged.PageCount Then
            grdAppointmentToBeArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentToBeArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentToBeArranged.DataSource = getResultDataSetViewState()
            grdAppointmentToBeArranged.DataBind()
        End If

    End Sub

#End Region

#Region "save Data For Scheduling"
    ''' <summary>
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="journalId"></param>
    ''' <remarks></remarks>
    Private Function saveDataForScheduling(ByVal journalId As Integer) As Boolean
        Dim appointmentToBeArrangeDs As DataSet = New DataSet()
        'get the result data set from view state
        appointmentToBeArrangeDs = Me.getResultDataSetViewState()
        Dim query = (From res In appointmentToBeArrangeDs.Tables(0)
                    Where res("PMO") = (journalId)
                    Select res)

        If query.Count() > 0 Then
            'get the row of related to specific journal id and save it in new datatable
            Dim conditionWorksAppointmentBO As New ConditionWorksAppointmentBO
            Dim appointmentInfoDt = query.CopyToDataTable()

            conditionWorksAppointmentBO.JournalId = journalId

            'save PMO
            conditionWorksAppointmentBO.PMO = CType(appointmentInfoDt.Rows(0).Item("PMO"), Integer)
            'save property id and Address
            conditionWorksAppointmentBO.PropertyId = CType(appointmentInfoDt.Rows(0).Item("PropertyId"), String)
            conditionWorksAppointmentBO.PropertyAddress = CType(appointmentInfoDt.Rows(0).Item("Address"), String)

            'save appointment info
            conditionWorksAppointmentBO.WorksRequired = CType(appointmentInfoDt.Rows(0).Item("WorksRequired"), String)
            conditionWorksAppointmentBO.ComponentName = CType(appointmentInfoDt.Rows(0).Item("Component"), String)
            conditionWorksAppointmentBO.AppointmentNotes = conditionWorksAppointmentBO.WorksRequired


            'remove the session that is being used in appointment arranged and appointment to be arranged
            SessionManager.removeConditionWorksAppointmentBo()
            'SessionManager.removePlannedSchedulingBo()

            'save this planned appointment business object in session . This object will be used and populated in next screens
            SessionManager.setConditionWorksAppointmentBo(conditionWorksAppointmentBO)
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "redirect To Available Appointments"
    ''' <summary>
    ''' redirect to avaialble appointments
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub redirectToConditionalAvailableAppointments()
        Response.Redirect(PathConstants.ConditionalAvailableAppointments)
    End Sub
#End Region

    '#Region "Check risk and vulnerability"
    '    ''' <summary>
    '    ''' Check risk and vulnerability
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Function checkRiskAndVulnerability(ByVal journalId As Integer)

    '        Dim riskExist As Boolean = False
    '        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    '        Dim resultDataset As DataSet = New DataSet()
    '        objSchedulingBL.getRiskAndVulnerabilityInfo(resultDataset, journalId)

    '        If (resultDataset.Tables(ApplicationConstants.RiskInfo).Rows.Count > 0) Then
    '            grdRisks.DataSource = resultDataset.Tables(ApplicationConstants.RiskInfo).DefaultView
    '            grdRisks.DataBind()
    '            pnlRisks.Visible = True

    '            riskExist = True
    '        Else
    '            pnlRisks.Visible = False
    '        End If

    '        If (resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).Rows.Count > 0) Then
    '            grdVulnerability.DataSource = resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).DefaultView
    '            grdVulnerability.DataBind()
    '            pnlVulnerability.Visible = True

    '            riskExist = True

    '        Else
    '            pnlVulnerability.Visible = False
    '        End If

    '        Return riskExist

    '    End Function

    '#End Region

#End Region

#Region "View State Functions"

#Region "Page SortBo Get/Set/Remove"

    Public Function getPageSortBoViewState()
        Dim objPageSortBo As PageSortBO = TryCast(ViewState.Item(ViewStateConstants.PageSortBo), PageSortBO)
        If IsNothing(objPageSortBo) Then
            objPageSortBo = New PageSortBO("DESC", "Address", 1, 30)
        End If
        Return objPageSortBo
    End Function

    Public Sub setPageSortBoViewState(pageSortBo As PageSortBO)
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
    End Sub

    Public Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Result DataSet Get/Set/Remove"

    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        Dim resultDataset As DataSet = TryCast(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        If resultDataset Is Nothing Then
            resultDataset = New DataSet()
        End If
        Return resultDataset
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Search Text Get/Set/Remove"

    Private Function getSearchTextViewState()
        Dim seachText As String = TryCast(ViewState.Item(ViewStateConstants.Search), String)
        If IsNothing(seachText) Then
            seachText = String.Empty
        End If
        Return seachText
    End Function

    Private Sub removeSearchTextViewState()
        ViewState.Remove(ViewStateConstants.Search)
    End Sub

    Private Sub setSearchTextViewState(ByRef searchText As String)
        ViewState.Add(ViewStateConstants.Search, searchText)
    End Sub

#End Region

#Region "Virtual Item Count Get/Set/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        Dim virtualItemCount As Integer = -1
        If Not IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            virtualItemCount = CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
        Return virtualItemCount
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#End Region

End Class