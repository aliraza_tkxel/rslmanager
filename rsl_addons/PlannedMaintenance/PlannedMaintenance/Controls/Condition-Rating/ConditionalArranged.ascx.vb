﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class ConditionalArranged
    Inherits UserControlBase

#Region "Events"

#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load Event"

    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Grid Appointments Arranged List Page Index Changing Event"
    ''' <summary>
    ''' Grid Appointments Arranged List Page Index Changing Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentsArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentsArranged.PageIndexChanging

        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdAppointmentsArranged.PageIndex = e.NewPageIndex

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = getSearchTextViewState()


            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentsArrangedList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Appointments Arranged List Sorting Event"
    ''' <summary>
    ''' Grid Appointments Arranged List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAppointmentsArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentsArranged.Sorting

        Try
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdAppointmentsArranged.PageIndex = 0
            objPageSortBo.setSortDirection()

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = getSearchTextViewState()

            Dim resultDataSet As DataSet = New DataSet()
            populateAppointmentsArrangedList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region


#End Region

#Region "Functions"

#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        ViewState.Item(ViewStateConstants.Search) = String.Empty
        ViewState.Item(ViewStateConstants.SchemeId) = -1

        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)

        grdAppointmentsArranged.VirtualItemCount = totalCount
        grdAppointmentsArranged.DataSource = resultDataSet
        grdAppointmentsArranged.DataBind()

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        ' If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then
        If grdAppointmentsArranged.PageCount > 1 Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Arrow button event"
    ''' <summary>
    ''' Arrow button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnArrow_click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim lnkBtnArrow As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnArrow.NamingContainer, GridViewRow)
            Dim lblJsn As Label = DirectCast(row.FindControl("lblJSN"), Label)
            Dim pmo As Integer = Convert.ToInt32(lnkBtnArrow.CommandArgument.ToString())
            'this fuction will save the data in session (on row which user clicked)
            saveSelectedDataInSession(lblJsn.Text, pmo)
            'redirect the page to job sheet
            Response.Redirect(PathConstants.ViewConditionArrangedAppointments + "?" + PathConstants.Jsn + "=" + lblJsn.Text + "&" + PathConstants.Pmo + "=" + Convert.ToString(pmo))

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Populate Appointments Arranged List"
    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="search"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Public Sub populateAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim totalCount As Integer = 0

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)

        setSearchTextViewState(search)

        If setSession Then
            'ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            setResultDataSetViewState(resultDataSet)
            setPageSortBoViewState(objPageSortBo)
            setVirtualItemCountViewState(totalCount)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        Else
            objPageSortBo = getPageSortBoViewState()
        End If

        totalCount = objSchedulingBL.getConditionAppointmentsArrangedList(resultDataSet, objPageSortBo, search)

        grdAppointmentsArranged.VirtualItemCount = totalCount
        grdAppointmentsArranged.DataSource = resultDataSet
        grdAppointmentsArranged.DataBind()

        objPageSortBo = getPageSortBoViewState()
        ' If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then
        If grdAppointmentsArranged.PageCount > 1 Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdAppointmentsArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentsArranged.PageCount Then
            grdAppointmentsArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentsArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentsArranged.DataSource = getResultDataSetViewState()
            grdAppointmentsArranged.DataBind()
        End If

    End Sub

#End Region

#Region "Save Selected Data In Session"
    ''' <summary>
    ''' This function will save the selected data in session (when user clicked on blue arrow)
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <param name="pmo"></param>
    ''' <remarks></remarks>
    Private Sub saveSelectedDataInSession(ByVal jsn As String, ByVal pmo As Integer)

        Dim objConditionSchedulingBo As PlannedSchedulingBO = New PlannedSchedulingBO()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)

        Dim query = (From res In resultDataSet.Tables(0).AsEnumerable Where res.Item("JSN") = jsn Select res)
        objConditionSchedulingBo.AppointmentInfoDt = query.CopyToDataTable()
        SessionManager.setPlannedSchedulingBo(objConditionSchedulingBo)

        Dim objPlannedAppointmentBo As PlannedAppointmentBO = New PlannedAppointmentBO()
        objPlannedAppointmentBo.PMO = pmo
        objPlannedAppointmentBo.PropertyId = CType(objConditionSchedulingBo.AppointmentInfoDt.Rows(0).Item("PropertyId"), String)
        objPlannedAppointmentBo.ComponentId = CType(objConditionSchedulingBo.AppointmentInfoDt.Rows(0).Item("ComponentId"), Integer)
        objPlannedAppointmentBo.Jsn = CType(objConditionSchedulingBo.AppointmentInfoDt.Rows(0).Item("AppointmentId"), Integer)
        SessionManager.setPlannedAppointmentBO(objPlannedAppointmentBo)
    End Sub
#End Region

#End Region

#Region "View State Functions"

#Region "Page SortBo Get/Set/Remove"

    Public Function getPageSortBoViewState()
        Dim objPageSortBo As PageSortBO = TryCast(ViewState.Item(ViewStateConstants.PageSortBo), PageSortBO)
        If IsNothing(objPageSortBo) Then
            objPageSortBo = New PageSortBO("DESC", "Address", 1, 30)
        End If
        Return objPageSortBo
    End Function

    Public Sub setPageSortBoViewState(pageSortBo As PageSortBO)
        ViewState.Add(ViewStateConstants.PageSortBo, pageSortBo)
    End Sub

    Public Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Search Text Get/Set/Remove"

    Private Function getSearchTextViewState()
        Dim seachText As String = TryCast(ViewState.Item(ViewStateConstants.Search), String)
        If IsNothing(seachText) Then
            seachText = String.Empty
        End If
        Return seachText
    End Function

    Private Sub removeSearchTextViewState()
        ViewState.Remove(ViewStateConstants.Search)
    End Sub

    Private Sub setSearchTextViewState(ByRef searchText As String)
        ViewState.Add(ViewStateConstants.Search, searchText)
    End Sub

#End Region

#End Region

End Class