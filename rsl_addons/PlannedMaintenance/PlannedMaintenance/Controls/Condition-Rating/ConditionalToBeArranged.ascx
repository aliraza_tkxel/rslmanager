﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionalToBeArranged.ascx.vb"
    Inherits="PlannedMaintenance.ConditionalToBeArranged" %>
<%@ Register TagName="AssignToContractor" TagPrefix="uc1" Src="~/Controls/Scheduling/AssignToContractor.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div style="height: 500px; overflow: auto;">
    <asp:Panel ID="pnlMessageControl" runat="server" Visible="False">
        <asp:Label ID="lblMessageControl" runat="server"></asp:Label>
    </asp:Panel>
    <cc2:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AllowPaging="True"
        AllowSorting="True" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White"
        CellPadding="4" ForeColor="Black" GridLines="None" PageSize="30" HeaderStyle-CssClass="gridfaults-header"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="PMO:" SortExpression="PMO" ItemStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblPmo" runat="server" Text='<%# Bind("PMO") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                <ItemTemplate>
                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Component" SortExpression="Component">
                <ItemTemplate>
                    <asp:Label ID="lblComponentName" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="BY" SortExpression="BySort">
                <ItemTemplate>
                    <asp:Label ID="lblOperatives" runat="server" Text='<%# Bind("BY") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Works Required" SortExpression="WorksRequired">
                <ItemTemplate>
                    <asp:Label ID="lblWorksRequired" runat="server" Text='<%# Bind("WorksRequired") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button Text="Assign to Contractor" ID="btnAssignToContractor" runat="server"
                        CommandArgument='<%#Eval("PMO").ToString() + ";" + Eval("PropertyId").ToString() %>'
                        OnClick="btnAssignToContrator_Click" UseSubmitBehavior="false" Visible='<%# Boolean.Parse(ConfigurationManager.AppSettings("ShowAssignedToContractorButton")).ToString() %>' />
                    <asp:Button ID="btnSchedule" Text="Schedule" runat="server" CommandArgument='<%#Eval("PMO")%>'
                        OnClick="btnSchedule_Click" UseSubmitBehavior="false"></asp:Button>
                </ItemTemplate>
                <ItemStyle Width="200px" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#E6E6E6"></RowStyle>
        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        <HeaderStyle CssClass="gridfaults-header" />
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
    </cc2:PagingGridView>
    <br />
    <%-- Modal PopUp Assign to Contractor - Start --%>
    <%-- This hidden button is added just to set as Target Control Id for modal popup extender. --%>
    <asp:Button ID="btnHiddenAssigntoContractor" runat="server" Text="" Style="display: none;" />
    <asp:ModalPopupExtender ID="mdlPopupAssignToContractor" runat="server" TargetControlID="btnHiddenAssigntoContractor"
        PopupControlID="pnlAssignToContractor" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlAssignToContractor" runat="server" Style="min-width: 600px; max-width: 700px;
        display: none">
        <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        <uc1:AssignToContractor ID="ucAssignToContractor" runat="server" />
    </asp:Panel>
    <%-- Modal PopUp Assign - End --%>
</div>
