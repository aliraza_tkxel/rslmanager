﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BudgetSpendReportDetail.ascx.vb"
    Inherits="PlannedMaintenance.BudgetSpendReportDetail" %>
<asp:Panel ID="pnlBudgetSpendReportDetail" runat="server" BackColor="White" Width="350px"
    Style="font-weight: normal; font-size: 13px; padding: 10px">
    <div style="width: 100%; font-weight: bold;">
        <b>
            <asp:Label ID="lblHeaderComponent" runat="server">
            </asp:Label>
            &nbsp
            <asp:Label ID="lblHeaderStatus" runat="server">
            </asp:Label>
        </b>
    </div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
        <asp:Label ID="lblErrorMessage" runat="server">
        </asp:Label>
    </asp:Panel>

    <table style="width: 100%;">
        <tr>
            <td>
                <b>Component</b>
            </td>
            <td>
                <asp:Label ID="lblComponent" runat="server" Font-Bold="true">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <b>Year</b>
            </td>
            <td>
                <asp:Label ID="lblYear" runat="server" Font-Bold="true">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="true">
                </asp:Label>
            </td>
            <td>
                <asp:Label ID="lblTotalCost" runat="server" Font-Bold="true">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <b>Properties</b>
            </td>
            <td>
                <asp:Label ID="lblPropertiesCount" runat="server" Font-Bold="true">
                </asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <div style="width: 100%; max-height: 300px; overflow: auto">
        <asp:GridView ID="grdProperties" runat="server" AutoGenerateColumns="False" ShowHeader="True"
            Width="100%" GridLines="None" CellPadding="10" CellSpacing="5">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Due">
                    <ItemTemplate>
                        <asp:Label ID="lblDueDate" runat="server" Text='<%# Bind("DueDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lblCost" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#F2F2F2"></RowStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        </asp:GridView>
    </div>
</asp:Panel>
