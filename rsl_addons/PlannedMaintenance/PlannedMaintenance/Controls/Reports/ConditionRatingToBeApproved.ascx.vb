﻿Imports PL_BusinessLogic
Imports PL_BusinessObject
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Public Class ConditionRatingToBeApproved
    Inherits UserControlBase

#Region "Properties"

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer = -1
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

                'populateConditionRatingReport()
            End If

            pnlReport.Visible = True
            pnlMoreDetail.Visible = False
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Cancel Click"
    ''' <summary>
    ''' Button Cancel Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            mdlPopUpApproveCondition.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Confirm Click"
    ''' <summary>
    ''' Button Confirm Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        Try
            Dim conditionWorksId As Integer = Convert.ToInt32(hdnSelectedConditionId.Value)
            Dim objReportsBL As ReportsBL = New ReportsBL()
            Dim userId As Integer = SessionManager.getPlannedMaintenanceUserId()
            Dim isApproved As Boolean = objReportsBL.approveConditionWorks(conditionWorksId, userId)
            Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
            'uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "2", False)

            If (isApproved) Then
                mdlPopUpApproveCondition.Hide()

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateConditionRatingToBeApprovedReport(resultDataSet, search, False, GetComponentId)

                Dim body As String = populateBodyApprovedConditionWorksEmail(objConditionEmailBo.RequestedBy, objConditionEmailBo.Component, objConditionEmailBo.Address, objConditionEmailBo.Towncity, objConditionEmailBo.Username, objConditionEmailBo.PostCode)
                'uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "4", False)
                If (sendEmail(body)) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApproved, False)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApprovedFailedEmail, False)
                End If
                'updPanelTobeApproved.Update()
                ' updPanelConditionRating.Update()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkFailedToApprove, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Approved Email body"
    Private Function populateBodyApprovedConditionWorksEmail(ByVal requestedBy As String, ByVal Component As String, ByVal address1 As String, ByVal townCity As String, ByVal username As String, ByVal PostCode As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/ConditionRatingApproved.html"))
        body.Append(reader.ReadToEnd.ToString)
        body = body.Replace("{Address}", address1 + ", " + townCity + ", " + PostCode)
        body = body.Replace("{Component}", Component)
        body = body.Replace("{Requestedby}", requestedBy)
        body = body.Replace("{Approvedby}", username)
        Return body.ToString()
    End Function
#End Region


#Region "Populate Rejected Email body"
    Private Function populateBodyRejectedConditionWorksEmail(ByVal requestedBy As String, ByVal Component As String, ByVal address1 As String, ByVal townCity As String, ByVal username As String, ByVal PostCode As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/ConditionRatingRejected.html"))
        body.Append(reader.ReadToEnd.ToString)
        body = body.Replace("{Address}", address1 + ", " + townCity + ", " + PostCode)
        body = body.Replace("{Component}", Component)
        body = body.Replace("{Requestedby}", requestedBy)
        body = body.Replace("{Rejectedby}", username)
        Return body.ToString()
    End Function
#End Region
#Region "Button Reject Click"
    ''' <summary>
    ''' Button Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRejectConditionWorks_Click(sender As Object, e As EventArgs) Handles btnRejectConditionWorks.Click
        Try
            Dim conditionWorksId As Integer = Convert.ToInt32(hdnSelectedConditionId.Value)
            Dim objRejectBO As RejectConditionBO = New RejectConditionBO()
            objRejectBO.ConditionWorkId = conditionWorksId
            objRejectBO.RejectionNotes = txtRejectNotes.Text
            objRejectBO.ReplacementDue = Convert.ToInt32(ddlReplacementDue.SelectedValue)
            objRejectBO.ReasonId = Convert.ToInt32(ddlReason.SelectedValue)
            objRejectBO.UserId = SessionManager.getPlannedMaintenanceUserId()
            validateReject(objRejectBO)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                mdlPopupRejectCondition.Show()
                uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(sender As Object, e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub grdConditionRating_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(sender As Object, e As EventArgs)
        Try
            Dim propertyId As String = DirectCast(sender, LinkButton).CommandArgument
            Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button More Detail Click"
    ''' <summary>
    ''' Link Button More Detail Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnMoreDetail_Click(sender As Object, e As EventArgs)
        Try
            Dim conditionWorkId As Integer = Convert.ToInt32(DirectCast(sender, LinkButton).CommandArgument)
            ucConditionMoreDetail.populateMoreDetail(conditionWorkId)
            pnlReport.Visible = False
            pnlMoreDetail.Visible = True
            'updPanelConditionRating.Update()

            'Response.Redirect(PathConstants.ConditionMoreDetailPath + "?" + PathConstants.Pmo + "=" + journalId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Approve Click"
    ''' <summary>
    ''' Link Button Approve Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnApprove_Click(sender As Object, e As EventArgs)
        Try

            Dim lnkBtnApprove As LinkButton = DirectCast(sender, LinkButton)
            hdnSelectedConditionId.Value = lnkBtnApprove.CommandArgument
            Dim row As GridViewRow = DirectCast(lnkBtnApprove.NamingContainer, GridViewRow)
            Dim lnkBtnAddress As LinkButton = DirectCast(row.FindControl("lnkBtnAddress"), LinkButton)
            'saveSmsBoInSession(row)
            saveEmailBoInSession(row)
            Dim propertyAddress As String = lnkBtnAddress.Text
            showApprovePopup(propertyAddress)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Link Button Reject Click"
    ''' <summary>
    ''' Link Button Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnReject_Click(sender As Object, e As EventArgs)
        Try

            Dim lnkBtnReject As LinkButton = DirectCast(sender, LinkButton)
            hdnSelectedConditionId.Value = lnkBtnReject.CommandArgument
            Dim row As GridViewRow = DirectCast(lnkBtnReject.NamingContainer, GridViewRow)
            saveSmsBoInSession(row)
            saveEmailBoInSession(row)
            showRejectPopup()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Condition More Control Back Button Clicked"
    ''' <summary>
    ''' Condition More Control Back Button Clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBackButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucConditionMoreDetail.BackButtonClicked
        Try

            pnlReport.Visible = True
            pnlMoreDetail.Visible = False
            ' updPanelConditionRating.Update()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdConditionRating_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdConditionRating.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdConditionRating.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingToBeApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingToBeApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateConditionRatingToBeApprovedReport(resultDataSet, search, False, GetComponentId)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Reset Approve Popup"
    ''' <summary>
    ''' Reset Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetApprovePopup()

        uiMessageHelper.resetMessage(lblApproveErrorMessage, pnlApproveErrorMessage)
        lblApprovePropertyAddress.Text = String.Empty

    End Sub

#End Region

#Region "Reset Reject Popup"
    ''' <summary>
    ''' Reset Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetRejectPopup()

        uiMessageHelper.resetMessage(lblRejectErrorMessage, pnlRejectErrorMessage)
        populateReasonDropdown()
        populateReplacementYearDropdown()
        txtRejectNotes.Text = String.Empty

    End Sub

#End Region

#Region "Save SMS BO"
    ''' <summary>
    ''' Save SMS BO
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveSmsBoInSession(ByVal row As GridViewRow)

        Dim hdnAddress1 As HiddenField = DirectCast(row.FindControl("hdnAddress1"), HiddenField)
        Dim hdnTowncity As HiddenField = DirectCast(row.FindControl("hdnTowncity"), HiddenField)
        Dim hdnMobile As HiddenField = DirectCast(row.FindControl("hdnMobile"), HiddenField)

        Dim objSmsBo As ConditionSmsBO = New ConditionSmsBO()
        objSmsBo.Address = hdnAddress1.Value()
        objSmsBo.Towncity = hdnTowncity.Value()
        objSmsBo.Mobile = hdnMobile.Value()
        objSmsBo.Username = SessionManager.getUserFullName()
        SessionManager.setConditionSmsBo(objSmsBo)

    End Sub

#End Region

#Region "Save Email BO"
    ''' <summary>
    ''' Save SMS BO
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveEmailBoInSession(ByVal row As GridViewRow)

        Dim hdnAddress1 As HiddenField = DirectCast(row.FindControl("hdnAddress1"), HiddenField)
        Dim hdnTowncity As HiddenField = DirectCast(row.FindControl("hdnTowncity"), HiddenField)
        Dim hdnEmail As HiddenField = DirectCast(row.FindControl("hdnEmail"), HiddenField)
        Dim hdnRequestedBy As HiddenField = DirectCast(row.FindControl("hdnRequestedBy"), HiddenField)
        Dim Component As Label = DirectCast(row.FindControl("lblComponent"), Label)
        Dim PostCode As Label = DirectCast(row.FindControl("lblPostcode"), Label)
        Dim objEmailBo As ConditionEmailBO = New ConditionEmailBO()
        objEmailBo.Address = hdnAddress1.Value()
        objEmailBo.Towncity = hdnTowncity.Value()
        objEmailBo.Username = SessionManager.getUserFullName()
        objEmailBo.Email = hdnEmail.Value()
        objEmailBo.RequestedBy = hdnRequestedBy.Value()
        objEmailBo.Component = Component.Text
        objEmailBo.PostCode = PostCode.Text
        SessionManager.setConditionEmailBO(objEmailBo)

    End Sub

#End Region

#Region "Validate reject information"
    ''' <summary>
    ''' Validate reject information
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateReject(ByVal objRejectBO As RejectConditionBO)

        Dim results As ValidationResults = Validation.Validate(objRejectBO)

        If results.IsValid Then
            rejectConditionWorks(objRejectBO)
        Else

            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            uiMessageHelper.IsError = True
            uiMessageHelper.message = message

        End If

    End Sub

#End Region

#Region "Reject condition works"
    ''' <summary>
    ''' Reject condition works
    ''' </summary>
    ''' <remarks></remarks>
    Sub rejectConditionWorks(ByVal objRejectBO As RejectConditionBO)

        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim userId As Integer = SessionManager.getPlannedMaintenanceUserId()
        Dim isRejected As Boolean = objReportsBL.rejectConditionWorks(objRejectBO)
        Dim objConditionSmsBo As ConditionSmsBO = SessionManager.getConditionSmsBo()
        Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
        If (isRejected) Then
            mdlPopupRejectCondition.Hide()

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingToBeApprovedReport(resultDataSet, search, False, GetComponentId)

            Dim body As String = GeneralHelper.populateBodyRejectConditionWorksSMS(objConditionSmsBo.Address, objConditionSmsBo.Towncity, objConditionSmsBo.Username)
            Dim emaillBody As String = populateBodyRejectedConditionWorksEmail(objConditionEmailBo.RequestedBy, objConditionEmailBo.Component, objConditionEmailBo.Address, objConditionEmailBo.Towncity, objConditionEmailBo.Username, objConditionEmailBo.PostCode)
            'Dim isSMSSent As Boolean = sendSMS(body)
            Dim isEmailSent As Boolean = sendRejectedEmail(emaillBody)
            If (isEmailSent = True) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejected, False)
            ElseIf (isEmailSent = False) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedEmail, False)
                'ElseIf (isEmailSent = True) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedSms, False)
                'ElseIf (isEmailSent = False) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedSmsEmail, False)
            End If

            '  updPanelConditionRating.Update()

        Else
            uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, UserMessageConstants.ConditionWorkFailedToRejected, True)
        End If

    End Sub

#End Region

#Region "Send SMS"
    Private Function sendSMS(ByVal body As String)
        Dim isMessageSent As Boolean = False

        Try
            Dim objConditionSmsBo As ConditionSmsBO = SessionManager.getConditionSmsBo()
            Dim results As ValidationResults = Validation.Validate(objConditionSmsBo)

            'If results.IsValid Then
            If objConditionSmsBo.Mobile.Length >= 10 Then

                ' Dim smsUrl As String = ResolveClientUrl(ApplicationConstants.SMSURL)


                'Dim javascriptMethod As String = "sendSMS('" + objConditionSmsBo.Mobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(body, objConditionSmsBo.Mobile)
                isMessageSent = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
                isMessageSent = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            isMessageSent = False

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                isMessageSent = False
            End If
        End Try

        Return isMessageSent

    End Function
#End Region

#Region "Send Email"
    Private Function sendEmail(ByVal body As String)
        Dim isEmailSent As Boolean = False

        Try
            Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
            Dim results As ValidationResults = Validation.Validate(objConditionEmailBo)
            If results.IsValid Then
                EmailHelper.sendHtmlFormattedEmailForConditionWorksApproved(objConditionEmailBo.RequestedBy, ApplicationConstants.replyTo, objConditionEmailBo.Email, ApplicationConstants.Subject, body)
                isEmailSent = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
                isEmailSent = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            isEmailSent = False

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                isEmailSent = False
            End If
        End Try
        Return isEmailSent
    End Function
#End Region
#Region "Send Email"
    Private Function sendRejectedEmail(ByVal body As String)
        Dim isEmailSent As Boolean = False

        Try
            Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
            Dim results As ValidationResults = Validation.Validate(objConditionEmailBo)
            If results.IsValid Then
                EmailHelper.sendHtmlFormattedEmailForConditionWorksApproved(objConditionEmailBo.RequestedBy, ApplicationConstants.replyTo, objConditionEmailBo.Email, ApplicationConstants.rejectedSubject, body)
                isEmailSent = True
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
                isEmailSent = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            isEmailSent = False

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
        End Try

        Return isEmailSent

    End Function
#End Region

#Region "Show Approve Popup"
    ''' <summary>
    ''' Show Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showApprovePopup(ByVal propertyAddress As String)

        resetApprovePopup()
        lblApprovePropertyAddress.Text = propertyAddress
        mdlPopUpApproveCondition.Show()

    End Sub

#End Region

#Region "Show Reject Popup"
    ''' <summary>
    ''' Show Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showRejectPopup()

        resetRejectPopup()
        mdlPopupRejectCondition.Show()

    End Sub

#End Region

#Region "Populate replacement year"
    ''' <summary>
    ''' Populate replacement year from 2000 to 2050
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdown()

        ddlReplacementDue.Items.Clear()

        Dim year As Integer = 2000

        While (year <= ApplicationConstants.YearEndRange)
            Dim yearItem As ListItem = New ListItem(Convert.ToString(year), Convert.ToString(year))
            ddlReplacementDue.Items.Add(yearItem)
            year = year + 1
        End While

        Dim item As ListItem = New ListItem("YYYY", "-1")
        ddlReplacementDue.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate reason dropdown"
    ''' <summary>
    ''' Populate reason dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReasonDropdown()

        ddlReason.Items.Clear()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        objReportsBl.getConditionReasons(resultDataset)

        ddlReason.DataSource = resultDataset.Tables(0).DefaultView
        ddlReason.DataValueField = "ReasonId"
        ddlReason.DataTextField = "Reason"
        ddlReason.DataBind()


        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlReason.Items.Insert(0, item)

    End Sub

#End Region



#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingToBeApprovedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean, Optional ByRef componentId As String = "-1")

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        objReportsBl.getConditionRatingList(resultDataSet, objPageSortBo, search, isFullList, componentId)
        grdConditionRating.DataSource = resultDataSet.Tables(ApplicationConstants.ConditionRatingDt)
        grdConditionRating.DataBind()

        Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(ApplicationConstants.ConditionRatingCountDt).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode:"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim locationField As BoundField = New BoundField()
        locationField.HeaderText = "Location:"
        locationField.DataField = "Location"
        grdFullConditionList.Columns.Add(locationField)

        Dim componentField As BoundField = New BoundField()
        componentField.HeaderText = "Component:"
        componentField.DataField = "Component"
        grdFullConditionList.Columns.Add(componentField)

        Dim conditionField As BoundField = New BoundField()
        conditionField.HeaderText = "Condition:"
        conditionField.DataField = "Condition"
        grdFullConditionList.Columns.Add(conditionField)

        Dim dueField As BoundField = New BoundField()
        dueField.HeaderText = "Due:"
        dueField.DataField = "Due"
        grdFullConditionList.Columns.Add(dueField)

        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "By:"
        byField.DataField = "By"
        grdFullConditionList.Columns.Add(byField)

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ""
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getConditionRatingList(resultDataset, objPageSortBo, searchtext, isFullList, GetComponentId)

        grdFullConditionList.DataSource = resultDataset.Tables(ApplicationConstants.ConditionRatingDt)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "ConditionRating_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region


End Class