﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_Utilities

Public Class BudgetSpendReportDetail
    Inherits UserControlBase

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblErrorMessage, pnlErrorMessage)
    End Sub
#End Region

#Region "Bind Row Data"
    ''' <summary>
    ''' Bind Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdProperties_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProperties.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lblCost As Label = DirectCast(e.Row.FindControl("lblCost"), Label)
                lblCost.Text = Double.Parse(lblCost.Text).ToString("c")

                Dim lblDueDate As Label = DirectCast(e.Row.FindControl("lblDueDate"), Label)
                lblDueDate.Text = DateTime.Parse(lblDueDate.Text).ToString("dd/MM/yyyy")

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate budgetSpendReportDetail"
    ''' <summary>
    ''' Populate budgetSpendReportDetail
    ''' </summary>
    ''' <param name="dtProperty"></param>
    ''' <param name="component"></param>
    ''' <param name="status"></param>
    ''' <param name="year"></param>
    ''' <remarks></remarks>
    Sub populateBudgetSpendReportDetail(ByVal dtProperty As DataTable, ByVal component As String, ByVal status As String, ByVal year As String)
        lblHeaderComponent.Text = component
        lblHeaderStatus.Text = IIf(status.Equals(ApplicationConstants.StatusToBeArranged), ApplicationConstants.StatusApproved, status)
        lblComponent.Text = component
        lblStatus.Text = IIf(status.Equals(ApplicationConstants.StatusToBeArranged), ApplicationConstants.StatusApproved, status)
        lblYear.Text = year
        lblPropertiesCount.Text = (From p In dtProperty Select p).Count().ToString()
        lblTotalCost.Text = (Aggregate row As DataRow In dtProperty.AsEnumerable() Into Sum(row.Field(Of Double)("Cost"))).ToString("c")
        grdProperties.DataSource = dtProperty.DefaultView
        grdProperties.DataBind()
    End Sub

#End Region

#End Region

End Class