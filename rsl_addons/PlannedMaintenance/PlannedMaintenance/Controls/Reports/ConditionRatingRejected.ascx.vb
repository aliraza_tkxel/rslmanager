﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports System.Drawing
Public Class ConditionRatingRejected
    Inherits UserControlBase
     
#Region "Properties"


#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

            End If
            pnlReport.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(sender As Object, e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(sender As Object, e As EventArgs)
        Try
            Dim propertyId As String = DirectCast(sender, LinkButton).CommandArgument
            Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Link Button View Click"
    ''' <summary>
    ''' Link Button View Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnView_Click(sender As Object, e As EventArgs)
        Try

            Dim lnkBtnApprove As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnApprove.NamingContainer, GridViewRow)
            Dim reasonLabel As Label = DirectCast(row.FindControl("lblRejectionReason"), Label)
            showViewPopup(reasonLabel.Text, lnkBtnApprove.CommandArgument)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Show View Popup"
    ''' <summary>
    ''' Show View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showViewPopup(ByVal reason As String, ByVal notes As String)

        resetViewPopup()
        lblRejectionReason.Text = reason
        lblRejectionNotes.Text = notes
        mdlPopUpViewCondition.Show()

    End Sub

#End Region

#Region "Reset View Popup"
    ''' <summary>
    ''' Reset View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetViewPopup()

        uiMessageHelper.resetMessage(lblViewErrorMessage, pnlViewErrorMessage)
        lblRejectionReason.Text = String.Empty
        lblRejectionNotes.Text = String.Empty

    End Sub

#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdConditionRating_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdConditionRating.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdConditionRating.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingRejectedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingRejectedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateConditionRatingRejectedReport(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingRejectedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        objReportsBl.getConditionRatingRejectedList(resultDataSet, objPageSortBo, search, isFullList)
        grdConditionRating.DataSource = resultDataSet.Tables(ApplicationConstants.ConditionRatingDt)
        grdConditionRating.DataBind()

        Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(ApplicationConstants.ConditionRatingCountDt).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode:"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim locationField As BoundField = New BoundField()
        locationField.HeaderText = "Location:"
        locationField.DataField = "Location"
        grdFullConditionList.Columns.Add(locationField)

        Dim componentField As BoundField = New BoundField()
        componentField.HeaderText = "Component:"
        componentField.DataField = "Component"
        grdFullConditionList.Columns.Add(componentField)

        Dim conditionField As BoundField = New BoundField()
        conditionField.HeaderText = "Condition:"
        conditionField.DataField = "Condition"
        grdFullConditionList.Columns.Add(conditionField)

        Dim dueField As BoundField = New BoundField()
        dueField.HeaderText = "Due:"
        dueField.DataField = "Due"
        grdFullConditionList.Columns.Add(dueField)

        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "Reported By:"
        byField.DataField = "By"
        grdFullConditionList.Columns.Add(byField)

        Dim rejectedBy As BoundField = New BoundField()
        rejectedBy.HeaderText = "Rejected By:"
        rejectedBy.DataField = "RejectedByAt"
        grdFullConditionList.Columns.Add(rejectedBy)

        Dim rejectedReason As BoundField = New BoundField()
        rejectedReason.HeaderText = "Rejection Reason:"
        rejectedReason.DataField = "RejectionReason"
        grdFullConditionList.Columns.Add(rejectedReason)

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ""
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getConditionRatingRejectedList(resultDataset, objPageSortBo, searchtext, isFullList)

        grdFullConditionList.DataSource = resultDataset.Tables(ApplicationConstants.ConditionRatingDt)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "ConditionRatingRejected_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region


End Class