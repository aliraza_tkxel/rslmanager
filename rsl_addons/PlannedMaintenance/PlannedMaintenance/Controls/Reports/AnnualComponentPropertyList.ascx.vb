﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessObject
Imports AjaxControlToolkit
Imports System.IO


Public Class AnnualComponentPropertyList
    Inherits UserControlBase

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not (IsPostBack) Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Check all checkboxes"
    ''' <summary>
    ''' Check all checkboxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chkSelectAll As CheckBox = DirectCast(sender, CheckBox)
            Dim btnSaveChanges As Button = CType(Me.Parent.FindControl("btnSaveChanges"), Button)
            Dim btnEditParameters As Button = CType(Me.Parent.FindControl("btnEditParameters"), Button)
            Dim updPnlComponentCost As UpdatePanel = CType(Me.Parent.FindControl("updPnlComponentCost"), UpdatePanel)

            If (chkSelectAll.Checked = False) Then

                btnSaveChanges.Visible = False
                btnEditParameters.Visible = True

                updPanelPropertyList.Update()
                updPnlComponentCost.Update()

            End If
    

            For Each row As GridViewRow In grdPropertyList.Rows
                Dim chkPropertyItem As CheckBox = DirectCast(row.FindControl("chkPropertyItem"), CheckBox)
                chkPropertyItem.Checked = chkSelectAll.Checked
                selectDeselectProperty(row, chkSelectAll.Checked)
            Next

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Check single checkboxes"
    ''' <summary>
    ''' Check single checkboxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkPropertyItem_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chkPropertyItem As CheckBox = DirectCast(sender, CheckBox)
            Dim row As GridViewRow = DirectCast(chkPropertyItem.NamingContainer, GridViewRow)
            selectDeselectProperty(row, chkPropertyItem.Checked)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Search Components Annual Property List Sorting"
    ''' <summary>
    ''' Search Components Annual Property List Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdComponentsAnnualPropertyList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPropertyList.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdPropertyList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            populateComponentsAnnualPropertyList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            populateComponentsAnnualPropertyList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)
                populateComponentsAnnualPropertyList()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Bind Row Data"
    ''' <summary>
    ''' Bind Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdPropertyList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPropertyList.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim clndrDue As CalendarExtender = DirectCast(e.Row.FindControl("clndrDue"), CalendarExtender)
                Dim txtDue As TextBox = DirectCast(e.Row.FindControl("txtDue"), TextBox)
                txtDue.Attributes.Add("readonly", "readonly")

                clndrDue.TargetControlID = txtDue.UniqueID

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Button Export to Excel Click"
    ''' <summary>
    ''' Button Export to Excel Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Excel grid Row Data Bound"
    ''' <summary>
    ''' Excel grid Row Data Bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        Try
            If (e.Row.RowType = DataControlRowType.Footer) Then

                Dim lblTitle As New Label
                Dim lblTotalCost As New Label
                lblTitle.Font.Bold = True
                lblTotalCost.Font.Bold = True

                Dim totalCost As String = getTotalCostViewState().ToString()
                lblTotalCost.Text = String.Format("£ {0}", totalCost)
                lblTitle.Text = "Total cost:"

                e.Row.Cells(6).Controls.Add(lblTitle)
                e.Row.Cells(7).Controls.Add(lblTotalCost)
            ElseIf (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.Cells(7).Text = String.Format("£ {0}", e.Row.Cells(7).Text)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Save Changes"
    ''' <summary>
    ''' Save Changes
    ''' </summary>
    ''' <remarks></remarks>
    Function saveChanges() As Boolean

        Dim objReportsBL As ReportsBL = New ReportsBL()

        Dim message As String = validateDueDate()
        If message.Length > 0 Then
            Throw New Exception(UserMessageConstants.InvalidDueDate + " " + message)
        End If
        For Each row As GridViewRow In grdPropertyList.Rows
            Dim chkPropertyItem As CheckBox = DirectCast(row.FindControl("chkPropertyItem"), CheckBox)
            Dim sid As Integer = Convert.ToInt32(DirectCast(row.FindControl("hdnDueSid"), HiddenField).Value())
            Dim dueDate As String = DirectCast(row.FindControl("txtDue"), TextBox).Text
            Dim replacedDate As String = DirectCast(row.FindControl("lblReplaced"), Label).Text
            Dim ref As String = DirectCast(row.FindControl("lblRef"), Label).Text

            If (chkPropertyItem.Checked = True) Then
                Dim lblDue As Label = DirectCast(row.FindControl("lblDue"), Label)
                Dim txtDue As TextBox = DirectCast(row.FindControl("txtDue"), TextBox)

                'If (Date.ParseExact(txtDue.Text, "yyyy", Nothing) < Date.ParseExact(replacedDate, "yyyy", Nothing)) Then
                '    Throw New Exception(UserMessageConstants.InvalidDueDate + " " + ref)
                'End If

                If (Not txtDue.Text.Equals(lblDue.Text)) Then
                    updatePropertyListInSession(sid, dueDate)
                End If
            End If
        Next

        Return True

    End Function

#End Region

#Region "Validate Due Date"
    Public Function validateDueDate()


        Dim message As String = String.Empty
        For Each row As GridViewRow In grdPropertyList.Rows
            Dim chkPropertyItem As CheckBox = DirectCast(row.FindControl("chkPropertyItem"), CheckBox)
            Dim sid As Integer = Convert.ToInt32(DirectCast(row.FindControl("hdnDueSid"), HiddenField).Value())
            Dim dueDate As String = DirectCast(row.FindControl("txtDue"), TextBox).Text
            Dim replacedDate As String = DirectCast(row.FindControl("lblReplaced"), Label).Text
            Dim ref As String = DirectCast(row.FindControl("lblRef"), Label).Text

            If (chkPropertyItem.Checked = True) Then
                Dim lblDue As Label = DirectCast(row.FindControl("lblDue"), Label)
                Dim txtDue As TextBox = DirectCast(row.FindControl("txtDue"), TextBox)

                If (Date.ParseExact(txtDue.Text, "yyyy", Nothing) < Date.ParseExact(replacedDate, "yyyy", Nothing)) Then
                    message += ref + ", "
                End If
            End If
        Next
        If message.Length > 0 Then
            message = message.Substring(0, message.Length - 2)
        End If

        Return message
    End Function

#End Region

#Region "Update property list in session"
    ''' <summary>
    ''' Update property list in session
    ''' </summary>
    ''' <remarks></remarks>
    Sub updatePropertyListInSession(ByVal sid As Integer, ByVal dueDate As String)

        Dim dt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()

        Dim isDataExist As Boolean = False
        Dim rowSid As Integer

        For Each dtr As DataRow In dt.Rows
            rowSid = dtr.Item(ApplicationConstants.SidCol)

            If (rowSid = sid) Then
                dtr.Item(ApplicationConstants.DueDateCol) = dueDate
                isDataExist = True
                Exit For
            End If

        Next

        If (isDataExist = False) Then
            Dim dr = dt.NewRow
            dr(ApplicationConstants.SidCol) = sid
            dr(ApplicationConstants.DueDateCol) = dueDate
            dt.Rows.Add(dr)
        End If
        dt.AcceptChanges()

        SessionManager.setUpdatedPropertyDueDatesDataTable(dt)

    End Sub

#End Region

#Region "Check whether data is changed"
    ''' <summary>
    ''' Check whether data is changed
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function isDataChanged() As Boolean

        For Each row As GridViewRow In grdPropertyList.Rows
            Dim chkPropertyItem As CheckBox = DirectCast(row.FindControl("chkPropertyItem"), CheckBox)
            If (chkPropertyItem.Checked = True) Then
                Dim lblDue As Label = DirectCast(row.FindControl("lblDue"), Label)
                Dim txtDue As TextBox = DirectCast(row.FindControl("txtDue"), TextBox)
                If (Not txtDue.Text.Equals(lblDue.Text)) Then
                    Return True
                End If
            End If
        Next

        Return False

    End Function

#End Region

#Region "Select/Deselect property"
    ''' <summary>
    ''' Select/Deselect property
    ''' </summary>
    ''' <remarks></remarks>
    Sub selectDeselectProperty(ByVal row As GridViewRow, ByVal state As Boolean)

        Dim lblDue As Label = DirectCast(row.FindControl("lblDue"), Label)
        Dim txtDue As TextBox = DirectCast(row.FindControl("txtDue"), TextBox)
        Dim isCheckedExist As Boolean = False
        Dim btnSaveChanges As Button = CType(Me.Parent.FindControl("btnSaveChanges"), Button)
        Dim btnEditParameters As Button = CType(Me.Parent.FindControl("btnEditParameters"), Button)
        Dim updPnlComponentCost As UpdatePanel = CType(Me.Parent.FindControl("updPnlComponentCost"), UpdatePanel)

        If (state = True) Then

            txtDue.Text = lblDue.Text
            txtDue.Visible = True
            lblDue.Visible = False
            btnSaveChanges.Visible = True
            btnEditParameters.Visible = False

        Else
            txtDue.Visible = False
            lblDue.Visible = True

            Dim chkAllProperty As CheckBox = CType(grdPropertyList.HeaderRow.FindControl("chkSelectAll"), CheckBox)
            If (chkAllProperty.Checked = True) Then
                chkAllProperty.Checked = False
            End If

            For Each grdrow As GridViewRow In grdPropertyList.Rows
                Dim chkPropertyItem As CheckBox = DirectCast(grdrow.FindControl("chkPropertyItem"), CheckBox)
                If (chkPropertyItem.Checked = True) Then
                    isCheckedExist = True
                    Exit For
                End If
            Next

            If (isCheckedExist) Then
                btnSaveChanges.Visible = True
                btnEditParameters.Visible = False
            Else
                btnSaveChanges.Visible = False
                btnEditParameters.Visible = True
            End If


        End If

        updPanelPropertyList.Update()
        updPnlComponentCost.Update()

    End Sub

#End Region

#Region "Populate components annual property list"
    ''' <summary>
    ''' Populate components annual property list
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateComponentsAnnualPropertyList(Optional ByVal isReload As Boolean = False)

        Dim objComponentYearSearchBO As ComponentYearSearchBO = SessionManager.getComponentYearSearchBO()
        Dim objUpdatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
        Dim objEditedComponentDt As DataTable = SessionManager.getEditedComponentDataTable()
        Dim totalCount As Integer = 0
        Dim objPageSortBo As PageSortBO

        If isReload = True Then
            objPageSortBo = New PageSortBO("DESC", "Ref", 1, 30)
        Else
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        If (IsNothing(objComponentYearSearchBO) Or IsNothing(objPageSortBo) Or IsNothing(objUpdatedPropertyDt) Or IsNothing(objEditedComponentDt)) Then
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        Else

            Dim resultDs As DataSet = New DataSet()
            Dim propertyListDataView As New DataView()
            Dim objReportsBL As ReportsBL = New ReportsBL()

            totalCount = objReportsBL.getYearlyComponentPropertyList(resultDs, objComponentYearSearchBO, objPageSortBo, objUpdatedPropertyDt, objEditedComponentDt)
            propertyListDataView = resultDs.Tables(ApplicationConstants.PropertyListDt).DefaultView
            grdPropertyList.DataSource = propertyListDataView
            grdPropertyList.DataBind()

            Dim totalCost As String = resultDs.Tables(ApplicationConstants.TotalCostDt).Rows(0)(0).ToString()
            lblTotalCost.Text = "£" + totalCost

            If (totalCount > 0) Then
                pnlTotalCost.Visible = True
            Else
                pnlTotalCost.Visible = False
            End If

            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
            setPageSortBoViewState(objPageSortBo)
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        End If

    End Sub
#End Region

#Region "Page Sort BO"
    ''' <summary>
    ''' Page Sort BO
    ''' </summary>
    ''' <param name="pageSortBO"></param>
    ''' <remarks></remarks>
    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Total Cost"

    Protected Sub setTotalCostViewState(ByRef totalCost As Double)

        If Not IsNothing(ViewState.Item(ViewStateConstants.TotalCost)) Then
            ViewState.Add(ViewStateConstants.TotalCost, totalCost)
        Else
            ViewState.Item(ViewStateConstants.TotalCost) = totalCost
        End If

    End Sub

    Protected Function getTotalCostViewState()

        Dim totalCost As Double = 0
        If Not IsNothing(ViewState.Item(ViewStateConstants.TotalCost)) Then
            totalCost = ViewState.Item(ViewStateConstants.TotalCost)
        End If
        Return totalCost

    End Function

    Protected Sub removeTotalCostViewState()
        ViewState.Remove(ViewStateConstants.TotalCost)
    End Sub

#End Region

#Region "Export to Excel"

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdComponentProperties As New GridView()
        generateGridForExcel(grdComponentProperties)

        Dim objReplacementBL As ReplacementBL = New ReplacementBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Ref", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        Dim objComponentYearSearchBO As ComponentYearSearchBO = SessionManager.getComponentYearSearchBO()
        Dim objUpdatedPropertyDt As DataTable = SessionManager.getUpdatedPropertyDueDatesDataTable()
        Dim objEditedComponentDt As DataTable = SessionManager.getEditedComponentDataTable()

        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim resultDs As DataSet = New DataSet()
        Dim isFullList As Boolean = True

        objReportsBL.getYearlyComponentPropertyList(resultDs, objComponentYearSearchBO, objPageSortBo, objUpdatedPropertyDt, objEditedComponentDt, isFullList)

        Dim totalCost As Double = Convert.ToDouble(resultDs.Tables(ApplicationConstants.TotalCostDt).Rows(0)(0))
        setTotalCostViewState(totalCost)

        grdComponentProperties.DataSource = resultDs.Tables(ApplicationConstants.PropertyListDt)
        grdComponentProperties.DataBind()

        ExportGridToExcel(grdComponentProperties)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim objComponentYearSearchBO As ComponentYearSearchBO = SessionManager.getComponentYearSearchBO()
        Dim fileName As String = String.Format("{0}_CostForecast_{1}_{2}_{3}", objComponentYearSearchBO.ComponentName.Replace(" ", "_"), Convert.ToString(DateTime.Now.Day), Convert.ToString(DateTime.Now.Month), Convert.ToString(DateTime.Now.Year))

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = System.Text.Encoding.Default

        Response.ContentType = "application/vnd.ms-excel"
        Dim swExcel As New StringWriter()
        Dim hwExcel As New HtmlTextWriter(swExcel)

        grdViewObject.RenderControl(hwExcel)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(swExcel.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Generate Grid For Excel"

    Sub generateGridForExcel(ByRef gridView As GridView)

        gridView.AllowPaging = False
        gridView.AutoGenerateColumns = False
        gridView.ShowFooter = True

        AddHandler gridView.RowDataBound, AddressOf grdExcel_RowDataBound
        createBoundField(gridView, "Ref:", "Ref")
        createBoundField(gridView, "Scheme:", "Scheme")
        createBoundField(gridView, "Address:", "Address")
        createBoundField(gridView, "Postcode:", "Postcode")
        createBoundField(gridView, "Component:", "Component")
        createBoundField(gridView, "Replaced:", "Replaced")
        createBoundField(gridView, "Due:", "Due")
        createBoundField(gridView, "Cost:", "Cost")

    End Sub

#End Region

#Region "Create Bound field"

    Sub createBoundField(ByRef gridView As GridView, ByVal headerText As String, ByVal dataField As String)

        Dim boundField As BoundField = New BoundField()
        boundField.HeaderText = headerText
        boundField.DataField = dataField
        gridView.Columns.Add(boundField)

    End Sub

#End Region

#End Region

#End Region


End Class