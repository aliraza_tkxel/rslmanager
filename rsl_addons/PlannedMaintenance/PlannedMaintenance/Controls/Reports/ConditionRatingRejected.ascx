﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionRatingRejected.ascx.vb" Inherits="PlannedMaintenance.ConditionRatingRejected" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:UpdatePanel ID="updPanelRejected" runat="server">
    <ContentTemplate>
        <div style="height:auto; overflow: auto;">
            <asp:Panel ID="pnlReport" runat="server" Visible="true">
                <asp:Panel ID="pnlMessage" runat="server" Visible="True">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div style="height: 600px; overflow: auto; width: 99%; border: 1px solid #ADADAD;">
                    <cc1:PagingGridView ID="grdConditionRating" runat="server" AllowPaging="False" AllowSorting="True"
                    AutoGenerateColumns="False" BackColor="White" BorderColor="#ADADAD" ShowHeaderWhenEmpty="true"
                    BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Both" PageSize="30"
                    Width="100%" CssClass="sort-expression" Style="text-align: center;" PagerSettings-Visible="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAddress" OnClick="lnkBtnAddress_Click" Font-Bold="true"
                                    Font-Underline="true" CommandArgument='<%# Bind("PropertyId") %>' ForeColor="Blue"
                                    runat="server" Text='<%# Bind("Address") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                            <ItemTemplate>
                                <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location" SortExpression="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="250px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Component" SortExpression="Component">
                            <ItemTemplate>
                                <asp:Label ID="lblComponent" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="190px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Condition" SortExpression="Condition">
                            <ItemTemplate>
                                <asp:Label ID="lblCondition" runat="server" Text='<%# Bind("Condition") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Due" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="lblDue" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reported By" SortExpression="By">
                            <ItemTemplate>
                                <asp:Label ID="lblBy" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="110px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rejected By" SortExpression="RejectedByAt">
                            <ItemTemplate>
                                <asp:Label ID="lblRejectedByAt" runat="server" Text='<%# Bind("RejectedByAt") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="110px" />
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason" SortExpression="RejectionReason">
                            <ItemTemplate>
                                <asp:Label ID="lblRejectionReason" runat="server" Text='<%# Bind("RejectionReason") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="110px" />
                        </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkBtnView" OnClick="lnkBtnView_Click"
                                    Font-Bold="true" Font-Underline="true" CommandArgument='<%# Bind("RejectionNotes") %>'
                                    ForeColor="Blue" Text="View"></asp:LinkButton>
                                </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                        </asp:TemplateField>
                        </Columns>
                        <%-- Grid View Styling Start --%>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                        <HeaderStyle BackColor="#FFFFFF" ForeColor="Black" Font-Bold="True" Font-Underline="false"
                            HorizontalAlign="Left"></HeaderStyle>
                        <FooterStyle BackColor="#FFFFFF" ForeColor="Black" Font-Bold="True" Font-Underline="true"
                            HorizontalAlign="Left" />
                        <%-- Grid View Styling End --%>
                        <PagerSettings Mode="NumericFirstLast">
                        </PagerSettings>
                </cc1:PagingGridView>
            </div>
                <%--Pager Template Start--%>
                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="height: 30px;
                color: Black; background-color: #FFFFFF; border-left: 1px solid #ADADAD; border-right: 1px solid #ADADAD;
                width: 99%; vertical-align: middle;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td align="right">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                    CommandArgument="First" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Previous" runat="server" CommandName="Page"
                                                    CommandArgument="Prev" />
                                                &nbsp; &nbsp &nbsp
                                            </td>
                                            <td>
                                                Records:
                                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                to
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                of
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                &nbsp &nbsp Page:
                                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                of
                                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                &nbsp &nbsp
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                    CommandArgument="Next" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                    CommandArgument="Last" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td align="right">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                <asp:TextBox Visible="false" ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                <asp:Button Visible="false" Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                    ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
                <%--Pager Template End--%>
                <asp:Panel ID="pnlExportToExcel" runat="server" HorizontalAlign="Right">
                <div style="width: 99%; float: left; border: 1px solid #ADADAD;">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Panel ID="pnlInner" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnExportToExcel" UseSubmitBehavior="false"  runat="server" Text="Export to XLS" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
                <asp:HiddenField ID="hdnSelectedConditionId" runat="server" />
                <!----------------------------------------------------- View  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelViewPopup" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlViewCondition" CssClass="left" runat="server" BackColor="White"
                        Width="380px" Style="padding: 15px 5px; top: 0; left: 0; bottom: 0; right: 0;
                        border: 1px solid black; -bracket-: hack(; left: 430px !important; top: 100px;
                        );">
                        <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                            <b>Rejection Details </b>
                        </div>
                        <div style="clear: both; height: 1px;">
                        </div>
                        <hr />
                        <asp:ImageButton ID="imgBtnCloseViewCondition" runat="server" Style="position: absolute;
                            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                        <asp:Panel ID="pnlViewErrorMessage" runat="server" Visible="false">
                            <asp:Label ID="lblViewErrorMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                        <div style="margin-left: 10px; margin-right: 10px;">
                            <p>
                                <strong>Reason: </strong>
                                <br />
                                <asp:Label ID="lblRejectionReason"  runat="server">
                                </asp:Label>
                            </p>
                            <p>
                                <strong>Notes: </strong>
                                <br />
                                <asp:Label ID="lblRejectionNotes" runat="server">
                                </asp:Label>
                            </p>
                            <br />
                        </div>
                    </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopUpViewCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblViewPopup" PopupControlID="pnlViewCondition"
                            DropShadow="true" CancelControlID="imgBtnCloseViewCondition" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblViewPopup" runat="server"></asp:Label>
                </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
   </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
