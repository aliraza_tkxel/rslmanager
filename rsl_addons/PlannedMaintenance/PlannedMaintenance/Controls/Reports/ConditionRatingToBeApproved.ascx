﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionRatingToBeApproved.ascx.vb"
    Inherits="PlannedMaintenance.ConditionRatingToBeApproved" %>
    
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/Reports/ConditionMoreDetail.ascx" TagName="ConditionMoreDetail"
    TagPrefix="uc1" %>

<asp:UpdatePanel ID="updPanelTobeApproved" runat="server">
    <ContentTemplate>
        <div style="height:auto; overflow: auto;">
            <asp:Panel ID="pnlReport" runat="server" Visible="true">
                <asp:Panel ID="pnlMessage" runat="server" Visible="True">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                    <cc1:PagingGridView ID="grdConditionRating" runat="server" AutoGenerateColumns="False" AllowSorting="True" PageSize="30"
                    Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard dashboard-border webgrid table table-responsive" EmptyDataText="No Records Found"
                    GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="True" OnRowCreated="grdConditionRating_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="Address" SortExpression="Address">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAddress" OnClick="lnkBtnAddress_Click" Font-Bold="true"
                                    Font-Underline="true" CommandArgument='<%# Bind("PropertyId") %>' ForeColor="Blue"
                                    runat="server" Text='<%# Bind("Address") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                            <ItemTemplate>
                                <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" SortExpression="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Component" SortExpression="Component">
                            <ItemTemplate>
                                <asp:Label ID="lblComponent" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="190px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Condition" SortExpression="Condition">
                            <ItemTemplate>
                                <asp:Label ID="lblCondition" runat="server" Text='<%# Bind("Condition") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="lblDue" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="By" SortExpression="By">
                            <ItemTemplate>
                                <asp:Label ID="lblBy" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="110px" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkBtnMoreDetail" OnClick="lnkBtnMoreDetail_Click"
                                    Font-Bold="true" Font-Underline="true" CommandArgument='<%# Bind("ConditionWorksId") %>'
                                    ForeColor="Blue" Text="More Details"></asp:LinkButton>
                                &nbsp
                                <asp:LinkButton runat="server" ID="lnkBtnApprove" OnClick="lnkBtnApprove_Click" Font-Bold="true"
                                    CommandArgument='<%# Bind("ConditionWorksId") %>' Font-Underline="true" Text="Approve"
                                    ForeColor="Blue"></asp:LinkButton>
                                &nbsp
                                <asp:LinkButton runat="server" ID="lnkBtnReject" OnClick="lnkBtnReject_Click" CommandArgument='<%# Bind("ConditionWorksId") %>'
                                    Font-Bold="true" Font-Underline="true" Text="Reject" ForeColor="Blue"></asp:LinkButton>
                                <asp:HiddenField ID="hdnAddress1" runat="server" Value='<%# Bind("Address1") %>' />
                                <asp:HiddenField ID="hdnTowncity" runat="server" Value='<%# Bind("Towncity") %>' />
                                <asp:HiddenField ID="hdnMobile" runat="server" Value='<%# Bind("Mobile") %>' />
                                <asp:HiddenField ID="hdnEmail" runat="server" Value='<%# Bind("WorkEmail") %>' />
                                <asp:HiddenField ID="hdnRequestedBy" runat="server" Value='<%# Bind("RequestedBy") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="400px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                </cc1:PagingGridView>
                </div>
                <%--Pager Template Start--%>
                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                        <div class="paging-left">
                            <span style="padding-right:10px;">
                                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                    &lt;&lt;First
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                    &lt;Prev
                                </asp:LinkButton>
                            </span>
                            <span style="padding-right:10px;">
                                <b>Page:</b>
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                of
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                            </span>
                            <span style="padding-right:20px;">
                                <b>Result:</b>
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                to
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                of
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </span>
                            <span style="padding-right:10px;">
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                </asp:LinkButton>
                            </span>
                        </div>
                        <div style="width:40%; float:left">
                            <div class="right">
                                <span>
                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                                </span>
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field right" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--Pager Template End--%>
                <asp:Panel ID="pnlExportToExcel" runat="server" HorizontalAlign="Right">
                    <div style="width: 99%; float: left; border: 1px solid #ADADAD;">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlInner" runat="server" HorizontalAlign="Right">
                                        <asp:Button ID="btnExportToExcel" UseSubmitBehavior="false"  runat="server" Text="Export to XLS" />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnSelectedConditionId" runat="server" />
                <!----------------------------------------------------- Approve  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelApprovePopup" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlApproveCondition" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style="padding: 15px 5px; top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                                <b>Approve Condition Works </b>
                            </div>
                            <div style="clear: both; height: 1px;">
                            </div>
                            <hr />
                            <asp:ImageButton ID="imgBtnCloseApproveCondition" runat="server" Style="position: absolute;
                                top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                            <asp:Panel ID="pnlApproveErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblApproveErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <p>
                                    You have chosen to approve the condition works for
                                    <asp:Label ID="lblApprovePropertyAddress" Font-Bold="true" runat="server">
                                    </asp:Label>
                                    , select confirm to add the works to the 'Appointment to be arranged' list!
                                </p>
                                <br />
                                <asp:Panel ID="pnlApproveButtons" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                    <asp:Button ID="btnConfirm" runat="server" Text="Confirm" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopUpApproveCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblApprovePopup" PopupControlID="pnlApproveCondition"
                            DropShadow="true" CancelControlID="imgBtnCloseApproveCondition" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblApprovePopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
                <!----------------------------------------------------- Reject  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelRejectPopup" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlRejectCondition" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style="padding: 15px 5px; top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                                <b>Reject Condition Works </b>
                            </div>
                            <div style="clear: both; height: 1px;">
                            </div>
                            <hr />
                            <asp:ImageButton ID="imgBtnCloseRejectCondition" runat="server" Style="position: absolute;
                                top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                            <asp:Panel ID="pnlRejectErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblRejectErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <p>
                                    You have chosen to reject the reported condition works.
                                    <br />
                                    Please record a reason why you wish to reject the works and enter a new replacement
                                    due date for the component.
                                    <br />
                                </p>
                                <br />
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            Reason*:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReason" Width="150px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Replacement Due:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlReplacementDue" Width="100px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <asp:TextBox ID="txtRejectNotes" Height="70px" Width="93%" TextMode="MultiLine" runat="server"></asp:TextBox>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                    TargetControlID="txtRejectNotes" WatermarkText="Please enter some notes explaining the reason for the rejection."
                                    WatermarkCssClass="descriptionText">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <br />
                                <br />
                                <br />
                                <asp:Panel ID="pnlRejectButtons" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnRejectConditionWorks" runat="server" Text="Reject Condition Works" />
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopupRejectCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblRejectPopup" PopupControlID="pnlRejectCondition"
                            DropShadow="true" CancelControlID="imgBtnCloseRejectCondition" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblRejectPopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!---------------------------------------------------------------------------------------------------------------------->
            </asp:Panel>
            <asp:Panel ID="pnlMoreDetail" runat="server" Visible="false">
                <uc1:ConditionMoreDetail ID="ucConditionMoreDetail" runat="server" />
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
