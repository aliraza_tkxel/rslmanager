﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AnnualComponentPropertyList.ascx.vb"
    Inherits="PlannedMaintenance.AnnualComponentPropertyList" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="updPanelPropertyList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False" CssClass="message">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div id="propertyList">
            <div style="height: 550px; overflow: auto;">
                <cc2:PagingGridView ID="grdPropertyList" runat="server" AllowPaging="False" AllowSorting="True"
                    Font-Underline="false" HeaderStyle-Font-Underline="false" BorderWidth="0px" BorderStyle="Solid"
                    ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"
                    CellPadding="4" ForeColor="Black" GridLines="None" orderby="" PageSize="5" HeaderStyle-CssClass="gridfaults-header"
                    Width="100%" PagerSettings-Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="40px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPropertyItem" runat="server" AutoPostBack="true" OnCheckedChanged="chkPropertyItem_CheckedChanged">
                                </asp:CheckBox>
                                <asp:HiddenField ID="hdnDueSid" runat="server" Value='<%# Bind("DueSID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ref:" SortExpression="Ref" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Font-Bold="true" Font-Underline="true"
                                    ForeColor="#0040FF" Text='<%# Bind("Address") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="25%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                            <ItemTemplate>
                                <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Component:" SortExpression="Component">
                            <ItemTemplate>
                                <asp:Label ID="lblComponent" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Replaced:" SortExpression="Replaced">
                            <ItemTemplate>
                                <asp:Label ID="lblReplaced" runat="server" Text='<%# Bind("Replaced") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due:" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="lblDue" runat="server" Text='<%# Bind("Due") %>'></asp:Label>
                                <asp:TextBox ID="txtDue" runat="server" Visible="false" Text='<%# Bind("Due") %>'
                                    Width="50px"></asp:TextBox>
                                <asp:CalendarExtender ID="clndrDue" TargetControlID="txtDue" Format="yyyy" PopupButtonID="txtDue"
                                    runat="server" OnClientShown="ShowNavigation" />
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cost:" SortExpression="Cost">
                            <ItemTemplate>
                                £<asp:Label ID="lblCost" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridfaults-header" />
                    <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                    <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                </cc2:PagingGridView>
                <br />
            </div>
            <%--Pager Template Start--%>
            <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                background-color: #FAFAD2; border-color: #ADADAD; border-width: 1px; border-style: Solid;
                vertical-align: middle;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td align="left">
                                Records :
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                to
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                of
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td align="center">
                                Page :
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                of
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                            </td>
                            <td style="float: right;">
                                <table style="text-align: right;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                    CommandArgument="First" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                                    CommandArgument="Prev" />
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                    CommandArgument="Next" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                    CommandArgument="Last" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="display: none;">
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" />
                    <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                        ValidationGroup="pageNumber" UseSubmitBehavior="false" /></div>
            </asp:Panel>
            <%--Pager Template End--%>
            <asp:Panel ID="pnlTotalCost" CssClass="totalCost" runat="server" Visible="false">                
                <asp:Label ID="lblTotalCost" CssClass="rightControl" runat="server" Text="" Font-Bold="true"></asp:Label>
                <asp:Label ID="Label1" runat="server" CssClass="rightControl" Text="Total cost:"
                    Font-Bold="true"></asp:Label>
                <asp:Button runat="server" ID="btnExportToExcel" Text="Export to XLS" UseSubmitBehavior="false"  CssClass="rightControl"/>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
           <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>

</asp:UpdatePanel>
