﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionMoreDetail.ascx.vb"
    Inherits="PlannedMaintenance.ConditionMoreDetail1" %>
<div class="headingTitle" style="width: 97%">
    <asp:Image ID="Image1" ImageUrl="~/Images/question.png" runat="server" />
    <b>More Details</b>
</div>
<div style="border: 1px solid black; padding: 36px;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <br />
    </asp:Panel>
    <div id="PropertyDetail">
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label2" runat="server" Text="Scheme:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label3" runat="server" Text="Address:"></asp:Label>
                    </td>
                    <td>
                        <asp:HiddenField ID="hdnPropertyId" runat="server" />
                        <asp:LinkButton ID="lnkBtnAddress" Font-Bold="true" ForeColor="Blue" Font-Underline="true"
                            OnClick="lnkBtnAddress_Click" runat="server"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label1" runat="server" Text="Customer:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label5" runat="server" Text="Telephone:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label4" runat="server" Text="Mobile:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label6" runat="server" Text="Email:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" Font-Bold="true" Font-Underline="true" ForeColor ="Blue"    runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div style="clear: both;">
        <br />
        <br />
    </div>
    <hr />
    <br />
    <br />
    <div id="ConditionDetail">
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label7" runat="server" Text="Location:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label9" runat="server" Text="Component:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblComponent" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label8" runat="server" Text="Status:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label10" runat="server" Text="Condition Updated:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblConditionUpdated" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label11" runat="server" Text="Condition Updated By:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblConditionUpdatedBy" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label12" runat="server" Text="Last Survey:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLastSurvey" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label22" runat="server" Text="Last Surveyed By:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLastSurveyedBy" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label24" runat="server" Text="Approval:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblApproval" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label26" runat="server" Text="Appointment:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblAppointment" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label14" runat="server" Text="Planned Maintenance Component?:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblPlannedMaintenanceComponent" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label16" runat="server" Text="Attribute Replacement Cycle:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReplacementCycle" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label18" runat="server" Text="Replacement Due:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReplacementDue" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label13" runat="server" Text="Works required:"></asp:Label>
                    </td>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd" colspan="2">
                        <asp:TextBox ID="txtWorksRequired" Font-Italic="true" TextMode="MultiLine" Width="82%" Height="110px"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div style="clear: both;">
    </div>
    <br />
    <br />
    <hr />
    <br />
    <br />
    <br />
    <asp:Button ID="btnBackToConditionReport" runat="server" Text="< Back To Condition Report" />
</div>
