﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports PL_BusinessObject
Imports AjaxControlToolkit

Public Class ConditionMoreDetail1
    Inherits UserControlBase


#Region "Control Events"

    Public Event BackButtonClicked As EventHandler

#End Region

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not (IsPostBack) Then
                setControlProperty()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Link Button Property Address Event"
    ''' <summary>
    ''' Link Button Property Address Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnAddress.Click
        Try

            Dim propertyId As String = hdnPropertyId.Value()
            Dim url As String = PathConstants.PropertyRecordPath + propertyId
            Dim winStr As String = "window.open('" & url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "script", winStr, True)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Back Button Event"
    ''' <summary>
    ''' Back Button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBackToConditionReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToConditionReport.Click
        RaiseEvent BackButtonClicked(Me, New EventArgs)
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Set control property"
    ''' <summary>
    ''' Set control property
    ''' </summary>
    ''' <remarks></remarks>
    Sub setControlProperty()

        txtWorksRequired.Attributes.Add("readonly", "readonly")

    End Sub

#End Region

#Region "Reset Controls"
    ''' <summary>
    ''' Reset Controls
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetControls()

        lblScheme.Text = String.Empty
        lnkBtnAddress.Text = String.Empty
        lblTowncity.Text = String.Empty
        lblCounty.Text = String.Empty
        lblPostcode.Text = String.Empty

        lblCustomer.Text = ApplicationConstants.NotAvailable
        lblTelephone.Text = ApplicationConstants.NotAvailable
        lblMobile.Text = ApplicationConstants.NotAvailable
        lblEmail.Text = ApplicationConstants.NotAvailable

        lblLocation.Text = String.Empty
        lblComponent.Text = String.Empty
        lblStatus.Text = String.Empty
        lblConditionUpdated.Text = String.Empty
        lblConditionUpdatedBy.Text = String.Empty

        lblLastSurvey.Text = String.Empty
        lblLastSurveyedBy.Text = String.Empty
        lblApproval.Text = String.Empty
        lblAppointment.Text = String.Empty

        lblPlannedMaintenanceComponent.Text = String.Empty
        lblReplacementCycle.Text = String.Empty
        lblReplacementDue.Text = String.Empty

        txtWorksRequired.Text = String.Empty


    End Sub

#End Region

#Region "Populate More Detail"
    ''' <summary>
    ''' Populate More Detail
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateMoreDetail(ByVal conditionWorkId As Integer)

        Me.resetControls()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim strLastSurvey As String = String.Empty
        Dim strSurveyBy As String = String.Empty
        objReportsBl.getConditionMoreDetail(resultDataset, conditionWorkId)

        Dim propertyCustomerDt As DataTable = resultDataset.Tables(ApplicationConstants.PropertyCustomerDetailDt)
        Dim conditionDetailDt As DataTable = resultDataset.Tables(ApplicationConstants.ConditionDetailDt)

        If (propertyCustomerDt.Rows.Count > 0) Then

            lblScheme.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.SchemeCol)
            lnkBtnAddress.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.PropertyAddressCol)
            lblTowncity.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.TownCityCol)
            hdnPropertyId.Value = propertyCustomerDt.Rows(0)(ApplicationConstants.PropertyIdCol)
            lblCounty.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.CountyCol)
            lblPostcode.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.PostcodeCol)

            lblCustomer.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.CustomerCol)
            lblTelephone.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.TelephoneCol)
            lblMobile.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.MobileCol)
            lblEmail.Text = propertyCustomerDt.Rows(0)(ApplicationConstants.EmailCol)
        End If

        If (conditionDetailDt.Rows.Count > 0) Then

            lblLocation.Text = conditionDetailDt.Rows(0)(ApplicationConstants.LocationCol)
            lblComponent.Text = conditionDetailDt.Rows(0)(ApplicationConstants.ComponentCol)
            lblStatus.Text = conditionDetailDt.Rows(0)(ApplicationConstants.StatusCol)            
            lblConditionUpdatedBy.Text = conditionDetailDt.Rows(0)(ApplicationConstants.ConditionUpdatedByCol)

            If (IsDBNull(conditionDetailDt.Rows(0)(ApplicationConstants.ConditionUpdatedCol))) Then
                lblConditionUpdated.Text = ApplicationConstants.NotAvailable
            Else
                lblConditionUpdated.Text = GeneralHelper.getDateWithWeekdayFormat(conditionDetailDt.Rows(0)(ApplicationConstants.ConditionUpdatedCol))
            End If

            If (IsDBNull(conditionDetailDt.Rows(0)(ApplicationConstants.LastSurveyCol))) Then
                strLastSurvey = ApplicationConstants.NotAvailable
            Else
                strLastSurvey = GeneralHelper.getDateWithWeekdayFormat(conditionDetailDt.Rows(0)(ApplicationConstants.LastSurveyCol))
            End If
            If (IsDBNull(conditionDetailDt.Rows(0)(ApplicationConstants.LastSurveyedByCol))) Then
                strSurveyBy = ApplicationConstants.NotAvailable
            Else
                strSurveyBy = conditionDetailDt.Rows(0)(ApplicationConstants.LastSurveyedByCol)
            End If

            lblLastSurvey.Text = strLastSurvey
            lblLastSurveyedBy.Text = strSurveyBy
            lblApproval.Text = conditionDetailDt.Rows(0)(ApplicationConstants.ApprovalCol)
            lblAppointment.Text = conditionDetailDt.Rows(0)(ApplicationConstants.AppointmentCol)

            lblPlannedMaintenanceComponent.Text = conditionDetailDt.Rows(0)(ApplicationConstants.PlannedComponentCol)
            lblReplacementCycle.Text = conditionDetailDt.Rows(0)(ApplicationConstants.ReplacementCycleCol)
            lblReplacementDue.Text = conditionDetailDt.Rows(0)(ApplicationConstants.ReplacementDueCol)

            txtWorksRequired.Text = conditionDetailDt.Rows(0)(ApplicationConstants.WorksRequiredCol)

        Else
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If


    End Sub

#End Region

#End Region


End Class