﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports PL_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports System.Drawing


Public Class ConditionRatingApproved
    Inherits UserControlBase
    
#Region "Properties"

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer = -1
            If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
                componentid = Request.QueryString(PathConstants.ComponentId)
            End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            End If
            pnlReport.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(sender As Object, e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(sender As Object, e As EventArgs)
        Try
            Dim propertyId As String = DirectCast(sender, LinkButton).CommandArgument
            Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button More Detail Click"
    ''' <summary>
    ''' Link Button More Detail Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnViewAppointment_Click(sender As Object, e As EventArgs)
        Try
            Dim parameters() As String = DirectCast(sender, LinkButton).CommandArgument.Split(";")
            Dim ref As String = parameters(0)
            Dim statusTitle As String = parameters(1)
            SessionManager.setConditionRatingAptSearch("PMO" + ref)

            Select statusTitle
                Case "Condition to be Arranged"
                    Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentToBeArrangedTab)
                Case "Condition Arranged"
                    Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
                Case "To be Arranged"
                    Response.Redirect(PathConstants.ScheduleWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentToBeArrangedTab)
                Case "Arranged"
                    Response.Redirect(PathConstants.ScheduleWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
                Case "Assigned To Contractor"
                    Response.Redirect(PathConstants.AssignedToContractor)
                Case "Cancelled"
                    Response.Redirect(PathConstants.CancelledApointmentReport)
                Case "Completed"
                    statusTitle = "This work has been completed successfully."
                    showViewPopup(statusTitle)
                Case Else
                    showViewPopup("Information is not available right now.")
            End Select

            'updPanelConditionRating.Update()
            'Response.Redirect(PathConstants.ConditionMoreDetailPath + "?" + PathConstants.Pmo + "=" + journalId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Show View Popup"
    ''' <summary>
    ''' Show View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showViewPopup(ByVal status As String)

        resetViewPopup()
        lblStatus.Text = status
        mdlPopUpViewCondition.Show()

    End Sub

#End Region

#Region "Reset View Popup"
    ''' <summary>
    ''' Reset View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetViewPopup()

        uiMessageHelper.resetMessage(lblViewErrorMessage, pnlViewErrorMessage)
        lblStatus.Text = String.Empty

    End Sub

#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdConditionRating_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdConditionRating.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdConditionRating.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateConditionRatingApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateConditionRatingApprovedReport(resultDataSet, search, False, GetComponentId)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateConditionRatingApprovedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean, Optional ByRef componentId As String = "-1")

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        objReportsBl.getConditionRatingApprovedList(resultDataSet, objPageSortBo, search, isFullList, componentId)
        grdConditionRating.DataSource = resultDataSet.Tables(ApplicationConstants.ConditionRatingDt)
        grdConditionRating.DataBind()

        Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(ApplicationConstants.ConditionRatingCountDt).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode:"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim locationField As BoundField = New BoundField()
        locationField.HeaderText = "Location:"
        locationField.DataField = "Location"
        grdFullConditionList.Columns.Add(locationField)

        Dim componentField As BoundField = New BoundField()
        componentField.HeaderText = "Component:"
        componentField.DataField = "Component"
        grdFullConditionList.Columns.Add(componentField)

        Dim conditionField As BoundField = New BoundField()
        conditionField.HeaderText = "Condition:"
        conditionField.DataField = "Condition"
        grdFullConditionList.Columns.Add(conditionField)

        Dim dueField As BoundField = New BoundField()
        dueField.HeaderText = "Due:"
        dueField.DataField = "Due"
        grdFullConditionList.Columns.Add(dueField)

        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "Reported By:"
        byField.DataField = "By"
        grdFullConditionList.Columns.Add(byField)

        Dim approvedBy As BoundField = New BoundField()
        approvedBy.HeaderText = "Approved By:"
        approvedBy.DataField = "ApprovedByAt"
        grdFullConditionList.Columns.Add(approvedBy)

        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ""
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getConditionRatingApprovedList(resultDataset, objPageSortBo, searchtext, isFullList, GetComponentId)

        grdFullConditionList.DataSource = resultDataset.Tables(ApplicationConstants.ConditionRatingDt)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "ConditionRatingApproved_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region


End Class