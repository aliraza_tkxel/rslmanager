﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class OperativeAppointmentsListControl
    Inherits UserControlBase


#Region "Properties"

#End Region

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate List with Inprogress appointments of an operative"
    ''' <summary>
    ''' Populate List with Inprogress appointments of an operative
    ''' </summary>
    ''' <param name="componentId"></param>
    ''' <param name="operativeId"></param>
    ''' <remarks></remarks>
    Sub populateInprogressAppointments(ByVal componentId As Integer, ByVal operativeId As Integer)

        ViewState(ViewStateConstants.OperativeId) = operativeId
        ViewState(ViewStateConstants.ComponentId) = componentId

        Dim dsInprogressAppointments As DataSet = New DataSet()
        Dim appointmentDataView As New DataView()
        Dim objDashboardBl As DashboardBL = New DashboardBL()

        objDashboardBl.getInprogressAppointments(dsInprogressAppointments, componentId, operativeId)
        appointmentDataView = dsInprogressAppointments.Tables(0).DefaultView
        grdOperativeAppointments.DataSource = appointmentDataView
        grdOperativeAppointments.DataBind()

    End Sub
#End Region

#End Region

   
End Class