﻿Imports PL_BusinessLogic
Imports PL_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class MapControl
    Inherits System.Web.UI.UserControl

#Region "Properties"

#End Region

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region


#End Region

#Region "Functions"

    '#Region "Populate List with appointments of an operative"
    '    ''' <summary>
    '    ''' Populate List with appointments of an operative
    '    ''' </summary>
    '    ''' <param name="operativeId"></param>
    '    ''' <remarks></remarks>
    '    Sub populateMarkerData(ByVal operativeId As Integer)

    '        ViewState(ViewStateConstants.OperativeId) = operativeId
    '        Dim dsOperativeAppointments As DataSet = New DataSet()
    '        Dim appointmentDataView As New DataView()
    '        objDashboardBl.getTodaysAppointmentsOfOperative(dsOperativeAppointments, operativeId)

    '    End Sub

    '#End Region

    '#Region "Populate operative appointments grid"
    '    ''' <summary>
    '    ''' Populate operative appointments grid
    '    ''' </summary>
    '    ''' <param name="operativeId"></param>
    '    ''' <param name="sortExp"></param>
    '    ''' <param name="sortDir"></param>
    '    ''' <remarks></remarks>
    '    Sub populateOperativeAppointments(ByVal operativeId As Integer, ByVal sortExp As String, ByVal sortDir As String)

    '        Dim dsOperativeAppointments As DataSet = New DataSet()
    '        Dim appointmentDataView As New DataView()
    '        objDashboardBl.getTodaysAppointmentsOfOperative(dsOperativeAppointments, operativeId)
    '        appointmentDataView = dsOperativeAppointments.Tables(0).DefaultView

    '    End Sub
    '#End Region

    '#Region "Update In Progress Number"
    '    ''' <summary>
    '    ''' Update In Progress Number
    '    ''' </summary>
    '    ''' <param name="operativeId"></param>
    '    ''' <returns></returns>
    '    ''' <remarks></remarks>
    '    <System.Web.Services.WebMethod()> _
    '    Public Shared Function populateMarker(ByVal operativeId As Integer) As DataTable
    '        Dim objDashboardBl As DashboardBL = New DashboardBL()
    '        Dim dsOperativeAppointments As DataSet = New DataSet()
    '        Dim appointmentDataView As New DataView()
    '        objDashboardBl.getTodaysAppointmentsOfOperative(dsOperativeAppointments, operativeId)
    '        Return dsOperativeAppointments.Tables(0)

    '    End Function
    '#End Region

#End Region



   
End Class