﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MapControl.ascx.vb"
    Inherits="PlannedMaintenance.MapControl" %>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

    function pageLoad() {
        loadDefaultMap(52.630886, 1.297355, -1);
        populateMarker();
    }

    function addMarkers(markersArray) {

        var markers;
        var myLatlng;
        var locationArray = [];

        if (markersArray.length > 0) {
      
            //  Setting default location
            var mapOptions = {
                center: new google.maps.LatLng(52.630886, 1.297355),
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            var bounds = new google.maps.LatLngBounds();
            var geocoder = new google.maps.Geocoder();

            // Getting location and setting marker
            for (j = 0; j < markersArray.length; j++) {
                var adrs = markersArray[j].address + " " + markersArray[j].towncity + " " + markersArray[j].county + " " + markersArray[j].postcode
                geocoder.geocode({ 'address': adrs }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat();
                        var lng = results[0].geometry.location.lng();
                        myLatlng = new google.maps.LatLng(lat, lng);

                        if (myLatlng != '') {
                            locationArray.push(myLatlng);
                            setMarker(map, locationArray, markersArray);                                                                 
                        }
                    }
                });

            }

        } else {
            loadDefaultMap(52.630886, 1.297355, -1);
        }
    }

    function setMarker(map, locationArray, markers) {

        var infoWindow = new google.maps.InfoWindow();
        for (k = 0; k < markers.length; k++) {

            var data = markers[k];
            var icon = '';
            var Time = markers[k].appointments[0].startTime.split(':');

            var marker;

            if (Time[0] > 00 && Time[0] < 12) {
                icon = '../../Images/blue.png';
            }
            else {
                icon = '../../Images/red.png';
            }

            marker = new google.maps.Marker({
                position: locationArray[k],
                map: map,
                title: markers[k].address + " " + markers[k].towncity + " " + markers[k].county,
                icon: icon,
                animation: google.maps.Animation.DROP
            });

            (function (marker, data) {
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    var content = '<div style="width:200;font-size:11px;">';
                    content = content + data.address + "," + data.towncity + "<br>";
                    content = content + "<b>" + data.postcode + "</b><br>";

                    content = content + '<div id="bodyContent" style="max-height:200px;overflow:auto;font-size:11px;">';
                    var appointments = data.appointments

                    for (l = 0; l < appointments.length; l++) {
                        var appointment = appointments[l];
                        content = content + "<hr>";
                        content = content + "<b> JSN" + appointment.jsn + "</b><br>";
                        content = content + "<b>" + appointment.operative + "</b><span style='float:right;font-size:11px;'>" + appointment.duration + "</span><br>";
                        content = content + appointment.componentName + " <span style='float:right;font-size:11px;'>" + appointment.startTime + "</span><br>";
                        content = content + appointment.appointmentDate;
                    }


                    content = content + '</div></div>';
                    infoWindow.setOptions({ maxWidth: 230 });
                    infoWindow.setContent(content);
                    infoWindow.open(map, marker);
                });
            })(marker, data);

        }
    }

    function loadDefaultMap(lat, lng, index) {
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    }

    function infoCallback(infoWindow, marker, map) {
        return function () {
            infoWindow.open(map, marker);
        };
    };
    
</script>
<asp:UpdatePanel ID="updPanelMap" runat="server">
    <ContentTemplate>
        <div id="dvMap" style="width: 100%; height: 400px; float: left; margin-bottom:2px;"></div>
    </ContentTemplate>
</asp:UpdatePanel>
