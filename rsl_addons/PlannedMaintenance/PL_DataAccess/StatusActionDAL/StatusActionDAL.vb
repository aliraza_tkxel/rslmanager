﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject

Namespace PL_DataAccess
    Public Class StatusActionDAL
        Inherits BaseDAL


#Region "Functions"

#Region "Status And Action"

#Region "get All Statuses"
        ''' <summary>
        ''' This function 'll fetch all the statuses 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllStatuses(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAllStauses)
        End Sub
#End Region

#Region "Add Status"
        ''' <summary>
        ''' This funciton will be used to add the status and it will return all the statuses
        ''' </summary>        
        ''' <param name="statusTitle"></param>
        ''' <param name="selectedRanking"></param>
        ''' ''' <param name="allStatusDs">This will contain the updated statuses</param>
        ''' <remarks></remarks>
        Public Function addStatus(ByVal statusTitle As String, ByVal selectedRanking As Integer, ByRef allStatusDs As DataSet) As Boolean
            Dim outParameterList As ParameterList = New ParameterList()
            Dim inParameterList As ParameterList = New ParameterList()
            Dim CreatedByParam As Integer = SessionManager.getUserEmployeeId()
            Dim modifiedByParam As Integer = 0
            Dim rankTobeUpdated As ParameterBO = New ParameterBO("Ranking", selectedRanking, DbType.Int32)
            inParameterList.Add(rankTobeUpdated)
            Dim newTitle As ParameterBO = New ParameterBO("Title", statusTitle, DbType.String)
            inParameterList.Add(newTitle)
            Dim CreatedBy As ParameterBO = New ParameterBO("CreatedBy", CreatedByParam, DbType.Int32)
            inParameterList.Add(CreatedBy)
            Dim ModifiedBy As ParameterBO = New ParameterBO("modifiedBy", modifiedByParam, DbType.Int32)
            inParameterList.Add(ModifiedBy)

            Dim isSaved As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParameterList.Add(isSaved)

            'MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.EditStatus)
            MyBase.LoadDataSet(allStatusDs, inParameterList, outParameterList, SpNameConstants.AddStatus)
            Return CType(outParameterList.Item(0).Value, Boolean)
        End Function
#End Region

#Region "edit Status"
        ''' <summary>
        ''' this function 'll update the status
        ''' </summary>
        ''' <param name="statusId"></param>        
        ''' <param name="statusTitle"></param>
        ''' <param name="selectedRanking"></param>
        ''' <param name="modifiedBy"></param>
        ''' <remarks></remarks>
        Public Function editStatus(ByVal statusId As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer, ByVal modifiedBy As Integer, ByRef allStatusDs As DataSet) As Boolean
            Dim inParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("StatusId", statusId, DbType.Int32)
            inParameterList.Add(StatusIdParam)

            Dim statusTitleParam As ParameterBO = New ParameterBO("Title", statusTitle, DbType.String)
            inParameterList.Add(statusTitleParam)

            Dim selectedRankingParam As ParameterBO = New ParameterBO("Ranking", selectedRanking, DbType.Int32)
            inParameterList.Add(selectedRankingParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", modifiedBy, DbType.Int32)
            inParameterList.Add(modifiedByParam)

            Dim isSaved As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParameterList.Add(isSaved)

            MyBase.LoadDataSet(allStatusDs, inParameterList, outParameterList, SpNameConstants.EditStatus)
            Return CType(outParameterList.Item(0).Value, Boolean)
        End Function
#End Region

#Region "add Action"
        ''' <summary>
        ''' This function 'll add the status in database
        ''' </summary>
        ''' <param name="StatusId"></param>
        ''' <param name="Title"></param>
        ''' <param name="ranking"></param>
        ''' <param name="CreatedBy"></param>
        ''' <remarks></remarks>
        Public Function addAction(ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal CreatedBy As Integer, ByRef allActionDs As DataSet)
            Dim inParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", StatusId, DbType.Int32)
            inParameterList.Add(StatusIdParam)

            Dim TitleParam As ParameterBO = New ParameterBO("title", Title, DbType.String)
            inParameterList.Add(TitleParam)

            Dim RankingParam As ParameterBO = New ParameterBO("ranking", ranking, DbType.Int32)
            inParameterList.Add(RankingParam)

            Dim CreatedByParam As ParameterBO = New ParameterBO("createdBy", CreatedBy, DbType.Int32)
            inParameterList.Add(CreatedByParam)

            Dim isSaved As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParameterList.Add(isSaved)

            MyBase.LoadDataSet(allActionDs, inParameterList, outParameterList, SpNameConstants.AddAction)
            Return CType(outParameterList.Item(0).Value, Boolean)
        End Function

#End Region

#Region "edit Action"
        ''' <summary>
        ''' This functino 'll update the action in database
        ''' </summary>
        ''' <param name="ActionId"></param>
        ''' <param name="StatusId"></param>
        ''' <param name="Title"></param>
        ''' <param name="ranking"></param>
        ''' <param name="modifiedBy"></param>
        ''' <remarks></remarks>
        Public Function editAction(ByVal ActionId As Integer, ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal modifiedBy As Integer, ByRef allActionDs As DataSet)
            Dim inParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", StatusId, DbType.Int32)
            inParameterList.Add(StatusIdParam)

            Dim ActionIdParam As ParameterBO = New ParameterBO("actionId", ActionId, DbType.Int32)
            inParameterList.Add(ActionIdParam)

            Dim statusTitleParam As ParameterBO = New ParameterBO("title", Title, DbType.String)
            inParameterList.Add(statusTitleParam)

            Dim selectedRankingParam As ParameterBO = New ParameterBO("ranking", ranking, DbType.Int32)
            inParameterList.Add(selectedRankingParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("modifiedBy", modifiedBy, DbType.Int32)
            inParameterList.Add(modifiedByParam)

            Dim isSaved As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParameterList.Add(isSaved)

            MyBase.LoadDataSet(allActionDs, inParameterList, outParameterList, SpNameConstants.EditAction)
            Return CType(outParameterList.Item(0).Value, Boolean)
        End Function

#End Region

#Region "Get All Actions"
        ''' <summary>
        ''' This funciton 'll fetch all the actions
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAllActions(ByRef resultDataSet As DataSet)
            Dim parameterlist As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterlist, SpNameConstants.GetAllActions)
        End Sub
#End Region

#Region "Get Action by StatusId"
        ''' <summary>
        ''' This function 'll return the action based on status id
        ''' </summary>
        ''' <param name="statusId"></param>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getActionsByStatusId(ByVal statusId As Integer, ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterList.Add(statusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetActionsByStatusId)
        End Sub
#End Region

#Region "get Action Ranking By StatusId"
        ''' <summary>
        ''' This fuction 'll get the rankings of action on the basis of status id
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="statusId"></param>
        ''' <remarks></remarks>
        Sub getActionRankingByStatusId(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            Dim parameterlist As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterlist.Add(StatusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterlist, SpNameConstants.GetActionRankingByStatusId)
        End Sub
#End Region

#Region "get Letters By Status Id"
        ''' <summary>
        ''' This function 'll fetch the letters on the basis of status id
        ''' </summary>
        ''' <param name="statusId"></param>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getLettersByStatusId(ByVal statusId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterList.Add(StatusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getLettersByStatusId)
        End Sub
#End Region





#End Region

#End Region


    End Class
End Namespace