﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject

Namespace PL_DataAccess
    Public Class JobSheetDAL : Inherits BaseDAL

#Region "Get Job Sheed Summary Detail By JSN(Job Sheet Number)"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            Dim inParametersList As New ParameterList
            Dim jsnparameter As ParameterBO = New ParameterBO("JSN", jsn, DbType.String)

            inParametersList.Add(jsnparameter)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.getJobSheetSummaryByJSN)

            ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)
        End Sub

#End Region

#Region "Get Status Look Up For Job Sheet Summary Update"

        Sub getPlannedStatusLookUp(ByRef dsPlannedStatus As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(dsPlannedStatus, parametersList, outParametersList, SpNameConstants.getPlannedStatusLookUp)
        End Sub

#End Region

#Region "Save/Set Appointment Stauts in Database with Notes"

        Sub setAppointmentJobSheetStatusUpdate(ByRef objJobSheetBO As JobSheetBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.setAppointmentJobSheetStatus

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters AppointmentJSNId
            Dim AppointmentJSNId As ParameterBO = New ParameterBO("AppointmentJSNId", objJobSheetBO.AppointmentJSNId, DbType.Int32)
            inParamList.Add(AppointmentJSNId)

            '' Creating input ParameterBO objects and passing prameters OrderId
            Dim OrderId As ParameterBO = New ParameterBO("OrderId", objJobSheetBO.OrderId, DbType.Int32)
            inParamList.Add(OrderId)

            '' Creating input ParameterBO objects and passing prameters AppointmentStatusID
            Dim AppointmentStatusID As ParameterBO = New ParameterBO("AppointmentStatusId ", objJobSheetBO.AppointmentStatus, DbType.Int32)
            inParamList.Add(AppointmentStatusID)

            '' Creating input ParameterBO objects and passing prameters Notes
            Dim Notes As ParameterBO = New ParameterBO("Notes ", objJobSheetBO.AppointmentNotes, DbType.String)
            inParamList.Add(Notes)

            '' Creating input ParameterBO objects and passing prameters LastActionDate
            Dim LastActionDate As ParameterBO = New ParameterBO("LastActionDate", objJobSheetBO.LastActionDateTime, DbType.DateTime)
            inParamList.Add(LastActionDate)

            '' Creating input ParameterBO objects and passing prameters LastActionUserID
            Dim LastActionUserID As ParameterBO = New ParameterBO("LastActionUserID", objJobSheetBO.UserID, DbType.Int32)
            inParamList.Add(LastActionUserID)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                objJobSheetBO.IsFlagStatus = False
                objJobSheetBO.UserMsg = "Unable to save appointment job sheet status in database"
            Else
                objJobSheetBO.IsFlagStatus = True
            End If

        End Sub

#End Region

    End Class
End Namespace



