﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Data.SqlClient

Namespace PL_DataAccess
    Public Class ComponentsDAL : Inherits BaseDAL
#Region "Functions"

#Region "Get Components"
        ''' <summary>
        ''' Get Components
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getComponentsForLifeCycle(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO)
            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()
            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)
            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, outParametersList, SpNameConstants.GetComponentsForLifeCycle)
            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region


#Region "Check component trade exist"
        ''' <summary>
        ''' Check component trade exist
        ''' </summary>
        ''' <param name="tradeId"></param>
        ''' <param name="duration"></param>
        ''' <param name="componentId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function checkComponentExist(ByVal tradeId As Integer, ByVal duration As Double, ByVal componentId As Integer)
            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", tradeId, DbType.Int32)
            parameterList.Add(tradeIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", duration, DbType.Double)
            parameterList.Add(durationParam)

            Dim checkParam As ParameterBO = New ParameterBO("check", 0, DbType.Int32)
            outParametersList.Add(checkParam)

            MyBase.SelectRecord(parameterList, outParametersList, SpNameConstants.CheckCompTradeExist)
            Return outParametersList.Item(0).Value

        End Function
#End Region

#Region "get Component Detail"

        ''' <summary>
        ''' get Component detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentID"></param>
        ''' <remarks></remarks>
        Sub getComponentdetail(ByRef resultDataSet As DataSet, ByRef componentID As Integer)

            Dim parameterList As ParameterList = New ParameterList()



            Dim paraComponentID As ParameterBO = New ParameterBO("componentID", componentID, DbType.Int32)
            parameterList.Add(paraComponentID)

            Dim dtComponentDetail As DataTable = New DataTable()
            dtComponentDetail.TableName = "Componentdetail"
            resultDataSet.Tables.Add(dtComponentDetail)

            Dim dtOperativesDetail As DataTable = New DataTable()
            dtOperativesDetail.TableName = "OperativesDetail"
            resultDataSet.Tables.Add(dtOperativesDetail)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetComponentsByID)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtComponentDetail, dtOperativesDetail)
            ' Return outParametersList.Item(0).Value.ToString()
            ' MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.SearchSalesAccount)

        End Sub
#End Region
#Region "get Lookup Values"
        ''' <summary>
        ''' get Lookup Values
        ''' </summary>
        ''' <param name="lskLookUp"></param>
        ''' <param name="spName"></param>
        ''' <remarks></remarks>
        Private Sub getLookupValuesBySpNameNoFilter(ByRef lskLookUp As List(Of LookUpBO), ByVal spName As String)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, spName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookUpBO(id, name)
                lskLookUp.Add(objLookUp)
            End While
        End Sub
#End Region

#Region "get Trade LookUp Values All"
        ''' <summary>
        ''' get Trade LookUp Values All
        ''' </summary>
        ''' <param name="lstLookUp"></param>
        ''' <remarks></remarks>
        Sub getTradeLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getTradeLookup
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub
#End Region

#Region "Update Components"

        Public Sub UpdateComponent(ByRef objComponentBO As ComponentsBO, ByVal tradesToInsert As DataTable, ByVal tradesToDelete As DataTable, ByVal tradesOrderDt As DataTable)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentID As ParameterBO = New ParameterBO("COMPONENTID", objComponentBO.ComponentID, DbType.Int32)
            parameterList.Add(componentID)

            Dim componentName As ParameterBO = New ParameterBO("COMPONENTNAME", objComponentBO.ComponentName, DbType.String)
            parameterList.Add(componentName)

            Dim isAccount As ParameterBO = New ParameterBO("ISACCOUNTING", objComponentBO.IsAccount, DbType.Boolean)
            parameterList.Add(isAccount)

            Dim cycle As ParameterBO = New ParameterBO("CYCLE", objComponentBO.Cycle, DbType.Int32)
            parameterList.Add(cycle)

            Dim frequency As ParameterBO = New ParameterBO("FREQUENCY", objComponentBO.Frequency, DbType.String)
            parameterList.Add(frequency)

            Dim AnnualBudgetCost As ParameterBO = New ParameterBO("ANNUALBUDGETCOST", objComponentBO.AnnualBudgetCost, DbType.Decimal)
            parameterList.Add(AnnualBudgetCost)

            Dim meterialCost As ParameterBO = New ParameterBO("MATERIALCOST", objComponentBO.MeterialCost, DbType.Decimal)
            parameterList.Add(meterialCost)

            Dim labourCost As ParameterBO = New ParameterBO("LABOURCOST", objComponentBO.LabourCost, DbType.Decimal)
            parameterList.Add(labourCost)

            Dim editedBy As ParameterBO = New ParameterBO("EDITEDBY", objComponentBO.EditedBy, DbType.Int32)
            parameterList.Add(editedBy)

            Dim editedOn As ParameterBO = New ParameterBO("EDITEDON", objComponentBO.EditedOn, DbType.DateTime)
            parameterList.Add(editedOn)

            Dim trades As ParameterBO = New ParameterBO("@TradesInsert", tradesToInsert, SqlDbType.Structured)
            parameterList.Add(trades)

            Dim deleteTrades As ParameterBO = New ParameterBO("@TradesDelete", tradesToDelete, SqlDbType.Structured)
            parameterList.Add(deleteTrades)

            Dim orderTrades As ParameterBO = New ParameterBO("@TradesOrder", tradesOrderDt, SqlDbType.Structured)
            parameterList.Add(orderTrades)

            Dim outParamList As ParameterList = New ParameterList()

            'Dim isUpdatedParam As ParameterBO = New ParameterBO("isUpdated", 0, DbType.Int32)
            'outParametersList.Add(isUpdatedParam)

            'MyBase.SaveRecord(parameterList, outParamList, SpNameConstants.UpdateComponent)

            'MyBase.LoadDataSet(resultDataSet, parameterList, outParametersList, SpNameConstants.GetComponentsForLifeCycle)
            'Return outParametersList.Item(0).Value.ToString()

            MyBase.SaveRecord(parameterList, outParamList, SpNameConstants.UpdateComponent)
            'If (outParametersList.Item(0).Value = 0) Then
            '    Throw New Exception(UserMessageConstants.ComponentTradeDeletionFailed)
            'End If

            'Return outParametersList.Item(0).Value.ToString()

        End Sub
#End Region

#End Region

    End Class
End Namespace
