﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject

Namespace PL_DataAccess
    Public Class DashboardDAL : Inherits BaseDAL

#Region "Functions"

#Region "Replacement Due (Current Year)"
        ''' <summary>
        ''' Returns the replacement due in current year
        ''' </summary>
        ''' <param name="componentId"></param>
        ''' <param name="currentYear"></param>
        ''' <param name="yearOperator"></param>
        ''' <remarks></remarks>
        Function getReplacementDueCount(ByVal componentId As Integer, ByVal currentYear As Integer, ByVal yearOperator As Integer) As Integer
            Dim parameterList As New ParameterList()

            Dim componentIdParam As New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim replacementYearParam As New ParameterBO("replacementYear", currentYear, DbType.String)
            parameterList.Add(replacementYearParam)

            Dim yearOperatorParam As New ParameterBO("yearOperator", yearOperator, DbType.Int32)
            parameterList.Add(yearOperatorParam)

            Dim pageSizeParam As New ParameterBO("pageSize", 0, DbType.Int32)
            parameterList.Add(pageSizeParam)

            Dim lDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetReplacementList)

            Dim totalCount As Integer = 0
            'Call next result two times to move to third result set(data table)
            lDataReader.NextResult()
            lDataReader.NextResult()
            If (lDataReader.Read()) Then
                totalCount = lDataReader.GetInt32(lDataReader.GetOrdinal("TotalCount"))
            End If

            Return totalCount
        End Function
#End Region

#Region "Replacement Due (Not scheduled)"
        ''' <summary>
        ''' Returns Replacement Due (Not scheduled) count.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function getReplacementDueNotScheduleCount(ByVal componentId As Integer) As Integer
            Dim inParameterList As New ParameterList

            Dim componentIdParam As New ParameterBO("componentId", componentId, DbType.Int32)
            inParameterList.Add(componentIdParam)

            Dim pageSizeParam As New ParameterBO("pageSize", 0, DbType.Int32)
            inParameterList.Add(pageSizeParam)

            Dim outParamterList As New ParameterList
            Dim outTotalCount As New ParameterBO("totalCount", 0, DbType.Int32)
            outParamterList.Add(outTotalCount)

            MyBase.SaveRecord(inParameterList, outParamterList, SpNameConstants.GetAppointmentsToBeArrangedList)

            Return outTotalCount.Value
        End Function
#End Region

#Region "Replacement Due (Scheduled)"
        ''' <summary>
        ''' Returns Replacement Due (Scheduled) count.
        ''' </summary>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Function getReplacementDueScheduleCount(ByVal componentId As Integer) As Integer
            Dim inParameterList As New ParameterList

            Dim componentIdParam As New ParameterBO("componentId", componentId, DbType.Int32)
            inParameterList.Add(componentIdParam)

            Dim pageSizeParam As New ParameterBO("pageSize", 0, DbType.Int32)
            inParameterList.Add(pageSizeParam)

            Dim outParamterList As New ParameterList
            Dim outTotalCount As New ParameterBO("totalCount", 0, DbType.Int32)
            outParamterList.Add(outTotalCount)

            MyBase.SaveRecord(inParameterList, outParamterList, SpNameConstants.GetAppointmentsArrangedList)

            Return outTotalCount.Value
        End Function
#End Region

#Region "NoEntry Count"
        ''' <summary>
        ''' Returns NoEntry count.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getNoEntryCount(ByRef resultDataSet As DataSet, ByVal componentId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetNoEntryCount)
        End Sub
#End Region

#Region "Get Inprogress count"
        ''' <summary>
        ''' Returns Inprogress count.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getInprogressCount(ByRef resultDataSet As DataSet, ByVal componentId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetInprogressCount)
        End Sub
#End Region

#Region "Get Inprogress appointments"
        ''' <summary>
        ''' Returns inprogress appointments.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getInprogressAppointments(ByRef resultDataSet As DataSet, ByVal componentId As Integer, ByVal operativeId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parameterList.Add(operativeIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetInprogressAppointments)
        End Sub
#End Region

#Region "Get all components"
        ''' <summary>
        ''' Get all components
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllComponents(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllComponents)

        End Sub
#End Region

#Region "Get Inprogress Appointments Operatives"
        ''' <summary>
        ''' Get Inprogress Appointments Operatives
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getInprogressAppointmentOperatives(ByRef resultDataSet As DataSet, ByVal componentId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetInprogressAppointmentOperatives)

        End Sub
#End Region

#Region "Get RSLModules"
        ''' <summary>
        ''' Get RSLModules
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByRef userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRSLModules As DataTable = New DataTable()
            dtRSLModules.TableName = "RSLModules"
            resultDataSet.Tables.Add(dtRSLModules)

            Dim userIdParam As ParameterBO = New ParameterBO("USERID", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim orderParam As ParameterBO = New ParameterBO("OrderASC", 1, DbType.Int32)
            parametersList.Add(orderParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRSLModulesList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRSLModules)

        End Sub
#End Region

#Region "Misc And Adaptation Count"
        ''' <summary>
        ''' Returns Adaptation Not Scheduled count.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAdaptationCount(ByRef resultDataSet As DataSet, ByVal componentId As Integer, ByVal adaptationStatus As String, ByVal appointmentType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim adaptationStatusParam As ParameterBO = New ParameterBO("adaptationStatus", adaptationStatus, DbType.String)
            parameterList.Add(adaptationStatusParam)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("appointmentType", appointmentType, DbType.String)
            parameterList.Add(appointmentTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetMiscAndAdaptationCount)
        End Sub
#End Region

#Region " Get Condition Works Approval Required Count "
        ''' <summary>
        ''' Get Condition Works Approval Required Count
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getConditionWorksApprovalRequiredCount(ByRef resultDataSet As DataSet, ByVal componentId As Integer)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtConditionRatingList As DataTable = New DataTable()
            dtConditionRatingList.TableName = ApplicationConstants.ConditionRatingDt
            resultDataSet.Tables.Add(dtConditionRatingList)

            Dim dtTotalCount As DataTable = New DataTable()
            dtTotalCount.TableName = ApplicationConstants.ConditionRatingCountDt
            resultDataSet.Tables.Add(dtTotalCount)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", 0, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", 1, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", "Address", DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", "DESC", DbType.String)
            parameterList.Add(sortOrder)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", "", DbType.String)
            parameterList.Add(searchTextParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", False, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionRatingList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConditionRatingList, dtTotalCount)

        End Sub
#End Region

#Region " Get Condition Works Approved Count "
        ''' <summary>
        ''' Get Condition Works Approved Count
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getConditionWorksApprovedCount(ByRef resultDataSet As DataSet, ByVal componentId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetConditionWorksApprovedCount)
        End Sub
#End Region

#End Region

    End Class
End Namespace





