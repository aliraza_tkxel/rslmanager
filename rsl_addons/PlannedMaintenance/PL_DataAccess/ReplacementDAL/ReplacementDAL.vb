﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject

Namespace PL_DataAccess
    Public Class ReplacementDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get all components"
        ''' <summary>
        ''' Get all components
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllComponents(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllComponents)

        End Sub
#End Region

#Region "Get all schemes"
        ''' <summary>
        ''' Get all schemes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllSchemes(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllSchemes)

        End Sub
#End Region

#Region "Get schemes against replacement year and component"
        ''' <summary>
        ''' Get schemes against replacement year and componenet
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getSchemesAgainstReplacementYearAndComponent(ByRef resultDataSet As DataSet, ByVal replacementYear As Integer, ByVal componentId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim replacementYearParam As ParameterBO = New ParameterBO("replacementYear", replacementYear, DbType.Int32)
            parameterList.Add(replacementYearParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetSchemesAgainstReplacementYearAndComponent)

        End Sub
#End Region

#Region "Get all Appointment Statuses"
        ''' <summary>
        ''' Get all Appointment Statuses
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllAppointmentStatuses(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllAppointmentStatuses)

        End Sub
#End Region

#Region "Get Replacement List"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="componentId"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="aptStatusId"></param>
        ''' <remarks></remarks>
        Public Sub getReplacementList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal componentId As Integer, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal aptStatusId As Integer, ByVal yearOperator As Integer, ByVal isFullList As Boolean, ByVal searchText As String)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtReplacementList As DataTable = New DataTable()
            Dim dtTotalCost As DataTable = New DataTable()
            Dim dtTotalCount As DataTable = New DataTable()

            dtReplacementList.TableName = ApplicationConstants.ReplacementListDataTable
            dtTotalCost.TableName = ApplicationConstants.ReplacementTotalCostDataTable
            dtTotalCount.TableName = ApplicationConstants.ReplacementTotalCountDataTable

            resultDataSet.Tables.Add(dtReplacementList)
            resultDataSet.Tables.Add(dtTotalCost)
            resultDataSet.Tables.Add(dtTotalCount)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim replacementYearParam As ParameterBO = New ParameterBO("replacementYear", replacementYear, DbType.String)
            parameterList.Add(replacementYearParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parameterList.Add(schemeIdParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim aptStatusIdParam As ParameterBO = New ParameterBO("aptStatusId", aptStatusId, DbType.Int32)
            parameterList.Add(aptStatusIdParam)

            Dim yearOperatorParam As ParameterBO = New ParameterBO("yearOperator", yearOperator, DbType.Int32)
            parameterList.Add(yearOperatorParam)

            Dim searchtextParam As ParameterBO = New ParameterBO("@searchedText", searchText, DbType.String)
            parameterList.Add(searchtextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetReplacementList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReplacementList, dtTotalCost, dtTotalCount)

        End Sub
#End Region

#Region "Schedule selected properties"
        ''' <summary>
        ''' Schedule selected properties
        ''' </summary>
        ''' <param name="selectedProperties"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub scheduleSelectedProperties(ByVal selectedProperties As DataTable, ByVal userId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim userIdParam As ParameterBO = New ParameterBO("USERID", userId, DbType.Int32)
            parameterList.Add(userIdParam)

            Dim selectedPropertiesParam As ParameterBO = New ParameterBO("@SelectedReplacementItems", selectedProperties, SqlDbType.Structured)
            parameterList.Add(selectedPropertiesParam)

            Dim outParamList As ParameterList = New ParameterList()

            MyBase.SaveRecord(parameterList, outParamList, SpNameConstants.ScheduleReplacementItem)

        End Sub
#End Region

#Region "Get Property Detail"
        ''' <summary>
        ''' Get Property Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Sub getPropertyDetail(ByRef resultDataSet As DataSet, ByVal propertyId As String)

            Dim parameterList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetPropertyDetail)

        End Sub
#End Region

#Region "Get All trades"
        ''' <summary>
        ''' Get All trades
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="componentId"></param>
        ''' <remarks></remarks>
        Sub getAllTrades(ByRef resultDataSet As DataSet, ByVal componentId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllTrades)

        End Sub
#End Region

#Region "Get Inspection Arranged Status"
        ''' <summary>
        ''' Get Actions by statusId
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getInspectionArrangedActions(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.InspectionArrangedActions)

        End Sub
#End Region

#Region "Get Employee By Trade Id"
        ''' <summary>
        ''' This function 'll return the employees based on trade id
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getEmployeeByTradeId(ByRef resultDataSet As DataSet, ByVal tradeId As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", tradeId, DbType.Int32)
            parameterList.Add(tradeIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetEmpoloyeeByTradeId)

        End Sub
#End Region

#Region "Get All Employees"
        ''' <summary>
        ''' This function 'll return all the employees 
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getAllEmployees(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()            

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAllEmpoloyees)

        End Sub
#End Region

#Region "Get Letter by actionId"
        ''' <summary>
        ''' Get Letter by actionId
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="actionId"></param>
        ''' <remarks></remarks>
        Sub getLetterByActionId(ByRef resultDataSet As DataSet, ByVal actionId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", actionId, DbType.Int32)
            parameterList.Add(actionIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetLetterByActionId)

        End Sub
#End Region

#Region "Arrange Inspection"
        ''' <summary>
        ''' Arrange Inspection
        ''' </summary>
        ''' <param name="objArrangeInspectionBO"></param>
        ''' <param name="savedLetterDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function arrangeInspection(ByVal objArrangeInspectionBO As ArrangeInspectionBO, ByRef savedLetterDt As DataTable) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", objArrangeInspectionBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("ComponentId", objArrangeInspectionBO.ComponentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", objArrangeInspectionBO.StatusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", objArrangeInspectionBO.ActionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objArrangeInspectionBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim isLetterAttachedParam As ParameterBO = New ParameterBO("isLetterAttached", objArrangeInspectionBO.IsLetterAttached, DbType.Boolean)
            parametersList.Add(isLetterAttachedParam)

            Dim isDocumentAttachedParam As ParameterBO = New ParameterBO("isDocumentAttached", objArrangeInspectionBO.IsDocumentAttached, DbType.Boolean)
            parametersList.Add(isDocumentAttachedParam)

            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objArrangeInspectionBO.TenancyId, DbType.Int32)
            parametersList.Add(tenancyIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", Convert.ToDateTime(objArrangeInspectionBO.AppointmentDate).Date, DbType.Date)
            parametersList.Add(appointmentDateParam)

            Dim appointmentStartTimeParam As ParameterBO = New ParameterBO("appointmentStartTime", objArrangeInspectionBO.AppointmentStartTime, DbType.String)
            parametersList.Add(appointmentStartTimeParam)

            Dim appointmentEndTimeParam As ParameterBO = New ParameterBO("appointmentEndTime", objArrangeInspectionBO.AppointmentEndTime, DbType.String)
            parametersList.Add(appointmentEndTimeParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", objArrangeInspectionBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim inspectionIdParam As ParameterBO = New ParameterBO("inspectionId", objArrangeInspectionBO.InspectionId, DbType.Int32)
            parametersList.Add(inspectionIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", -1, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim journalHistoryIdFinalParam As ParameterBO = New ParameterBO("journalHistoryIdFinal", -1, DbType.Int32)
            outParametersList.Add(journalHistoryIdFinalParam)

            'This procedure will arrange inspection and saved in journal history and it 'll update the record in journal 
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.ArrangeInspection)

            Dim journalHistoryId As Integer = CType(outParametersList.Item(1).Value, Int32)

            'Now we 'll use this journal history id to save the letters
            If (journalHistoryId > 0) Then
                If (Not IsNothing(objArrangeInspectionBO.LetterList)) Then
                    ' Dim row As DataRow
                    ' For Each row In savedLetterDt.Rows()

                    Dim letterParamList As ParameterList = New ParameterList()
                    Dim outLetterParamList As ParameterList = New ParameterList()

                    Dim journalHistIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                    letterParamList.Add(journalHistIdParam)

                    'Dim titleParam As ParameterBO = New ParameterBO("title", row.Item(ApplicationConstants.LetterTextTitleColumn), DbType.String)
                    'letterParamList.Add(titleParam)

                    'Dim bodyParam As ParameterBO = New ParameterBO("body", row.Item(ApplicationConstants.LetterBodyColumn), DbType.String)
                    'letterParamList.Add(bodyParam)

                    'Dim stLetterIdParam As ParameterBO = New ParameterBO("standardLetterId", CType(row.Item(ApplicationConstants.StandardLetterId), Integer), DbType.Int32)
                    'letterParamList.Add(stLetterIdParam)

                    'Dim teamIdParam As ParameterBO = New ParameterBO("teamId", CType(row.Item(ApplicationConstants.TeamIdColumn), Integer), DbType.Int32)
                    'letterParamList.Add(teamIdParam)

                    'Dim resourceIdParam As ParameterBO = New ParameterBO("resourceId", CType(row.Item(ApplicationConstants.FromResourceIdColumn), Integer), DbType.Int32)
                    'letterParamList.Add(resourceIdParam)

                    'Dim signOffParam As ParameterBO = New ParameterBO("signOffId", CType(row.Item(ApplicationConstants.SignOffCodeColumn), Integer), DbType.Int32)
                    'letterParamList.Add(signOffParam)

                    'Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", CType(row.Item(ApplicationConstants.RentBalanceColumn), Double), DbType.Double)
                    'letterParamList.Add(rentBalanceParam)

                    'Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", CType(row.Item(ApplicationConstants.RentChargeColumn), Double), DbType.Double)
                    'letterParamList.Add(rentChargeParam)

                    'Dim dateParam As ParameterBO = New ParameterBO("todayDate", CType(row.Item(ApplicationConstants.TodayDateColumn), Date), DbType.Date)
                    'letterParamList.Add(dateParam)
                    Dim editedLetter As ParameterBO = New ParameterBO("@SaveEditedLetter", savedLetterDt, SqlDbType.Structured)
                    letterParamList.Add(editedLetter)

                    MyBase.SelectRecord(letterParamList, outLetterParamList, SpNameConstants.SaveEditedLetter)
                    ' Next
                End If

                If (Not IsNothing(objArrangeInspectionBO.DocList)) Then
                    For Each item As String In objArrangeInspectionBO.DocList
                        'Save the documents against journal history id
                        Dim docParamList As ParameterList = New ParameterList()
                        Dim outDocParamList As ParameterList = New ParameterList()

                        Dim journalHistIdDocParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        docParamList.Add(journalHistIdDocParam)

                        Dim docNameParam As ParameterBO = New ParameterBO("documentName", item, DbType.String)
                        docParamList.Add(docNameParam)

                        'Dim docPathParam As ParameterBO = New ParameterBO("documentPath", objArrangeInspectionBO.DocumentPath, DbType.String)
                        'docParamList.Add(docPathParam)

                        MyBase.SelectRecord(docParamList, outDocParamList, SpNameConstants.SaveDocuments)
                    Next
                End If


            End If

            Return journalHistoryId.ToString()
        End Function

#End Region

#Region "Get Operative Info"
        ''' <summary>
        ''' Get Operative Info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getOperativeInformation(ByRef resultDataSet As DataSet, ByVal operativeId As String)

            Dim parameterList As ParameterList = New ParameterList()

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.String)
            parameterList.Add(operativeIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetOperativeInformation)

        End Sub
#End Region

#Region "Get Inspection Information"
        ''' <summary>
        ''' Get Inspection Information
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="inspectionId"></param>
        ''' <remarks></remarks>
        Public Sub getInspectionInfo(ByRef resultDataSet As DataSet, ByRef inspectionId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtInspectionInfo As DataTable = New DataTable()
            dtInspectionInfo.TableName = ApplicationConstants.InspectionInfoDataTable
            resultDataSet.Tables.Add(dtInspectionInfo)

            Dim dtLetter As DataTable = New DataTable()
            dtLetter.TableName = ApplicationConstants.LetterDataTable
            resultDataSet.Tables.Add(dtLetter)

            Dim dtDocuments As DataTable = New DataTable()
            dtDocuments.TableName = ApplicationConstants.DocumentsDataTable
            resultDataSet.Tables.Add(dtDocuments)

            Dim inspectionIdParam As ParameterBO = New ParameterBO("inspectionId", inspectionId, DbType.Int32)
            parametersList.Add(inspectionIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetInspectionInfo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtInspectionInfo, dtLetter, dtDocuments)

        End Sub
#End Region



#End Region

    End Class
End Namespace
