﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Globalization


Namespace PL_DataAccess
    Public Class ReportsDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get All Schemes"
        Public Sub getAllSchemes(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim dtAllSchemes As DataTable = New DataTable()

            dtAllSchemes.TableName = "Schemes"

            resultDataSet.Tables.Add(dtAllSchemes)

            
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetAllSchemes)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAllSchemes)

        End Sub
#End Region

#Region " Get All Fiscal Years "
        Public Sub getAllFiscalYears(ByRef resultDataSet As DataSet)
            Try
                Dim lResultDataReader As IDataReader
                Dim parameterList As ParameterList = New ParameterList()
                Dim dtAllFiscalYears As DataTable = New DataTable()

                If (IsDBNull(dtAllFiscalYears.TableName) Or Trim(dtAllFiscalYears.TableName) = "") Then
                    dtAllFiscalYears.TableName = "FiscalYears"
                End If
                resultDataSet.Tables.Add(dtAllFiscalYears)
                lResultDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetAllFiscalYears)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAllFiscalYears)
            Catch ex As Exception

            End Try
        End Sub
#End Region


#Region "get Cost Component Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <remarks></remarks>

        Sub getCostComponentData(ByRef resultDataset As DataSet, ByVal searchedText As String, ByVal selectedScheme As Integer, ByVal startYear As String, ByVal flag As Boolean, ByRef editedParameterDt As DataTable, ByRef updatedPropertyDt As DataTable)

            Dim parameterList As ParameterList = New ParameterList()

            'Dim startYear As String = DateTime.Now.Year.ToString()
            Dim paramFlag As ParameterBO = New ParameterBO("flag", flag, DbType.Boolean)
            parameterList.Add(paramFlag)

            Dim paramStartYear As ParameterBO = New ParameterBO("startYear", startYear, DbType.String)
            parameterList.Add(paramStartYear)

            Dim ParamSelectedScheme As ParameterBO = New ParameterBO("selectedScheme", selectedScheme, DbType.Int32)
            parameterList.Add(ParamSelectedScheme)

            Dim paramSearchtext As ParameterBO = New ParameterBO("searchText", searchedText, DbType.String)
            parameterList.Add(paramSearchtext)

            Dim editedComponentsDtParam As ParameterBO = New ParameterBO("@editedComponentsDt", editedParameterDt, SqlDbType.Structured)
            parameterList.Add(editedComponentsDtParam)

            Dim updatedPropertyDtParam As ParameterBO = New ParameterBO("@dueDateDt", updatedPropertyDt, SqlDbType.Structured)
            parameterList.Add(updatedPropertyDtParam)

            Dim dtCostComponent As DataTable = New DataTable()

            dtCostComponent.TableName = "CostComponent"

            resultDataset.Tables.Add(dtCostComponent)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetComponentCostData)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtCostComponent)
        End Sub
#End Region

#Region "get Component Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <remarks></remarks>
        Sub getComponentData(ByVal resultDataset As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtAllComponents As DataTable = New DataTable()

            dtAllComponents.TableName = "Components"

            resultDataset.Tables.Add(dtAllComponents)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetAllComponentsDetails)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAllComponents)
        End Sub
#End Region

#Region "Save Parameters Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="objComponentBO"></param>
        ''' <remarks></remarks>
        Sub saveParametersData(ByRef resultDataSet As DataSet, ByVal objComponentBO As ComponentsBO, ByVal selectedScheme As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim dtAllComponentsReport As DataTable = New DataTable()

            dtAllComponentsReport.TableName = "ComponentReport"

            resultDataSet.Tables.Add(dtAllComponentsReport)

            Dim paramComponentID As ParameterBO = New ParameterBO("ComponentId", objComponentBO.ComponentID, DbType.Int32)
            parameterList.Add(paramComponentID)

            Dim paramCycle As ParameterBO = New ParameterBO("Cycle", objComponentBO.Cycle, DbType.Int32)
            parameterList.Add(paramCycle)

            Dim paramFrequency As ParameterBO = New ParameterBO("Frequency", objComponentBO.Frequency, DbType.String)
            parameterList.Add(paramFrequency)

            Dim paramMeterialCost As ParameterBO = New ParameterBO("MaterialCost", objComponentBO.MeterialCost, DbType.Decimal)
            parameterList.Add(paramMeterialCost)

            Dim paramLabourCost As ParameterBO = New ParameterBO("LabourCost", objComponentBO.LabourCost, DbType.Decimal)
            parameterList.Add(paramLabourCost)

            Dim paramEditedBy As ParameterBO = New ParameterBO("EditedBy", objComponentBO.EditedBy, DbType.Decimal)
            parameterList.Add(paramEditedBy)

            Dim paramSelectedScheme As ParameterBO = New ParameterBO("selectedScheme", selectedScheme, DbType.Int32)
            parameterList.Add(paramSelectedScheme)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.UpdateComponentData)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAllComponentsReport)
        End Sub
#End Region

#Region "Get Yearly Component Property List"

        ''' <summary>
        ''' Returns Yearly Component Property List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objComponentYearSearchBO"></param>
        ''' <remarks></remarks>
        Public Function getYearlyComponentPropertyList(ByRef resultDataSet As DataSet, ByVal objComponentYearSearchBO As ComponentYearSearchBO, ByVal objPageSortBO As PageSortBO, ByVal objUpdatedPropertyDt As DataTable, ByVal objEditedComponentDt As DataTable, ByVal isFullList As Boolean)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyListDt As DataTable = New DataTable()
            propertyListDt.TableName = ApplicationConstants.PropertyListDt
            Dim totalCostDt As DataTable = New DataTable()
            totalCostDt.TableName = ApplicationConstants.TotalCostDt
            Dim totalCountDt As DataTable = New DataTable()
            totalCountDt.TableName = ApplicationConstants.TotalCountDt

            resultDataSet.Tables.Add(propertyListDt)
            resultDataSet.Tables.Add(totalCostDt)
            resultDataSet.Tables.Add(totalCountDt)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", objComponentYearSearchBO.ComponentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim operatorParam As ParameterBO = New ParameterBO("operator", objComponentYearSearchBO.YearOperator, DbType.Int32)
            parametersList.Add(operatorParam)

            Dim yearParam As ParameterBO = New ParameterBO("year", objComponentYearSearchBO.DueYear, DbType.String)
            parametersList.Add(yearParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objComponentYearSearchBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parametersList.Add(isFullListParam)

            Dim dueDateDtParam As ParameterBO = New ParameterBO("@dueDateDt", objUpdatedPropertyDt, SqlDbType.Structured)
            parametersList.Add(dueDateDtParam)

            Dim editedComponentsDtParam As ParameterBO = New ParameterBO("@editedComponentsDt", objEditedComponentDt, SqlDbType.Structured)
            parametersList.Add(editedComponentsDtParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetYearlyComponentPropertyList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, propertyListDt, totalCostDt, totalCountDt)
            Return totalCountDt.Rows(0)(0)

        End Function
#End Region

#Region "Save due dates"
        ''' <summary>
        ''' Save due dates
        ''' </summary>
        ''' <param name="dueDateDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveDuedates(ByVal dueDateDt As DataTable)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim dueDateDtParam As ParameterBO = New ParameterBO("@dueDateDt", dueDateDt, SqlDbType.Structured)
            parametersList.Add(dueDateDtParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.UpdatePropertyReplacementDueDate)
            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "Get Assigned to Contractor List"

        Function getAssignedToContractorReport(ByRef resultDataSet As DataSet, ByRef objPageSortBo As Object, ByRef searchText As String) As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAssignedToContractorReport)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Condition Rating List"
        ''' <summary>
        ''' Get Condition Rating List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean, Optional ByRef componentId As String = "-1")

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtConditionRatingList As DataTable = New DataTable()
            dtConditionRatingList.TableName = ApplicationConstants.ConditionRatingDt
            resultDataSet.Tables.Add(dtConditionRatingList)

            Dim dtTotalCount As DataTable = New DataTable()
            dtTotalCount.TableName = ApplicationConstants.ConditionRatingCountDt
            resultDataSet.Tables.Add(dtTotalCount)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parameterList.Add(searchTextParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionRatingList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConditionRatingList, dtTotalCount)

        End Sub
#End Region




#Region "Get Condition Rating Approved List"
        ''' <summary>
        ''' Get Condition Rating Approved List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingApprovedList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean, Optional ByRef componentId As String = "-1")

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtConditionRatingList As DataTable = New DataTable()
            dtConditionRatingList.TableName = ApplicationConstants.ConditionRatingDt
            resultDataSet.Tables.Add(dtConditionRatingList)

            Dim dtTotalCount As DataTable = New DataTable()
            dtTotalCount.TableName = ApplicationConstants.ConditionRatingCountDt
            resultDataSet.Tables.Add(dtTotalCount)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parameterList.Add(searchTextParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionRatingApprovedList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConditionRatingList, dtTotalCount)

        End Sub
#End Region



#Region "Get Condition Rating Rejected List"
        ''' <summary>
        ''' Get Condition Rating Rejected List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <param name="isFullList"></param>
        ''' <remarks></remarks>
        Public Sub getConditionRatingRejectedList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal isFullList As Boolean)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtConditionRatingList As DataTable = New DataTable()
            dtConditionRatingList.TableName = ApplicationConstants.ConditionRatingDt
            resultDataSet.Tables.Add(dtConditionRatingList)

            Dim dtTotalCount As DataTable = New DataTable()
            dtTotalCount.TableName = ApplicationConstants.ConditionRatingCountDt
            resultDataSet.Tables.Add(dtTotalCount)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parameterList.Add(searchTextParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionRatingRejectedList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConditionRatingList, dtTotalCount)

        End Sub
#End Region

#Region "Get Condition More Detail"
        ''' <summary>
        ''' Get Condition More Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getConditionMoreDetail(ByRef resultDataSet As DataSet, ByVal conditionWorkId As Integer)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtPropertyCustomerDetail As DataTable = New DataTable()
            dtPropertyCustomerDetail.TableName = ApplicationConstants.PropertyCustomerDetailDt
            resultDataSet.Tables.Add(dtPropertyCustomerDetail)

            Dim dtConditionDetail As DataTable = New DataTable()
            dtConditionDetail.TableName = ApplicationConstants.ConditionDetailDt
            resultDataSet.Tables.Add(dtConditionDetail)

            Dim conditionWorkIdParam As ParameterBO = New ParameterBO("conditionWorkId", conditionWorkId, DbType.Int32)
            parameterList.Add(conditionWorkIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionMoreDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyCustomerDetail, dtConditionDetail)

        End Sub
#End Region

#Region "Get Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getConditionReasons(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim dtReason As DataTable = New DataTable()
            dtReason.TableName = ApplicationConstants.ReasonDt
            resultDataSet.Tables.Add(dtReason)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetConditionReasons)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReason)

        End Sub
#End Region


#Region "Get Cancel Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getCancelReasons(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim dtReason As DataTable = New DataTable()
            dtReason.TableName = ApplicationConstants.ReasonDt
            resultDataSet.Tables.Add(dtReason)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.getCancelReasons)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReason)

        End Sub
#End Region


#Region "Approve Condition Works"
        ''' <summary>
        ''' Approve Condition Works
        ''' </summary>
        ''' <param name="conditionWorkId"></param>
        ''' <remarks></remarks>
        Public Function approveConditionWorks(ByVal conditionWorkId As Integer, ByVal userId As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim conditionWorkIdParam As ParameterBO = New ParameterBO("conditionWorkId", conditionWorkId, DbType.Int32)
            parameterList.Add(conditionWorkIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parameterList.Add(userIdParam)

            Dim isApprovedParam As ParameterBO = New ParameterBO("isApproved", False, DbType.Boolean)
            outParametersList.Add(isApprovedParam)

            outParametersList = MyBase.SelectRecord(parameterList, outParametersList, SpNameConstants.ApproveConditionWorks)

            Return CType(outParametersList.Item(0).Value, Boolean)

        End Function
#End Region

#Region "Reject Condition Works"
        ''' <summary>
        ''' Reject Condition Works
        ''' </summary>
        ''' <param name="objRejectBO"></param>
        ''' <remarks></remarks>
        Public Function rejectConditionWorks(ByVal objRejectBO As RejectConditionBO)

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim conditionWorkIdParam As ParameterBO = New ParameterBO("conditionWorkId", objRejectBO.ConditionWorkId, DbType.Int32)
            parameterList.Add(conditionWorkIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objRejectBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)

            Dim reasonIdParam As ParameterBO = New ParameterBO("reasonId", objRejectBO.ReasonId, DbType.Int32)
            parameterList.Add(reasonIdParam)

            Dim rejectionNotesParam As ParameterBO = New ParameterBO("rejectionNotes", objRejectBO.RejectionNotes, DbType.String)
            parameterList.Add(rejectionNotesParam)

            Dim replacementDueParam As ParameterBO = New ParameterBO("replacementDue", Convert.ToString(objRejectBO.ReplacementDue), DbType.String)
            parameterList.Add(replacementDueParam)

            Dim isRejectedParam As ParameterBO = New ParameterBO("isRejected", False, DbType.Boolean)
            outParametersList.Add(isRejectedParam)

            outParametersList = MyBase.SelectRecord(parameterList, outParametersList, SpNameConstants.RejectConditionWorks)

            Return CType(outParametersList.Item(0).Value, Boolean)

        End Function
#End Region

#Region "Get budget spend report"
        ''' <summary>
        ''' Get budget spend report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReport(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal searchText As String, ByVal isFullList As Boolean)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtBudgetSpendReport As DataTable = New DataTable()
            dtBudgetSpendReport.TableName = ApplicationConstants.BudgetSpendReportDt
            resultDataSet.Tables.Add(dtBudgetSpendReport)

            Dim dtBudgetSpendReportCount As DataTable = New DataTable()
            dtBudgetSpendReportCount.TableName = ApplicationConstants.BudgetSpendReportCountDt
            resultDataSet.Tables.Add(dtBudgetSpendReportCount)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim replacementYearParam As ParameterBO = New ParameterBO("replacementYear", replacementYear, DbType.String)
            parameterList.Add(replacementYearParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parameterList.Add(searchTextParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parameterList.Add(schemeIdParam)

            Dim isFullListParam As ParameterBO = New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parameterList.Add(isFullListParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetBudgetSpendReport)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtBudgetSpendReport, dtBudgetSpendReportCount)

        End Sub
#End Region

#Region "Get budget spend report detail"
        ''' <summary>
        ''' Get budget spend report detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReportDetail(ByRef resultDataSet As DataSet, ByVal replacementYear As String, ByVal status As String, ByVal schemeId As Integer, ByVal componentId As Integer)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtBudgetSpendReportDetail As DataTable = New DataTable()
            dtBudgetSpendReportDetail.TableName = ApplicationConstants.BudgetSpendReportDetailDt
            resultDataSet.Tables.Add(dtBudgetSpendReportDetail)

            Dim replacementYearParam As ParameterBO = New ParameterBO("replacementYear", replacementYear, DbType.String)
            parameterList.Add(replacementYearParam)

            Dim statusParam As ParameterBO = New ParameterBO("status", status, DbType.String)
            parameterList.Add(statusParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parameterList.Add(schemeIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parameterList.Add(componentIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetBudgetSpendReportDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtBudgetSpendReportDetail)

        End Sub
#End Region

#Region "Get budget spend report excel detail"
        ''' <summary>
        ''' Get budget spend report excel detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="replacementYear"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getBudgetSpendReportExcelDetail(ByRef resultDataSet As DataSet, ByVal replacementYear As String, ByVal schemeId As Integer, ByVal searchText As String)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtForecast As DataTable = New DataTable()
            dtForecast.TableName = ApplicationConstants.ForecastDt
            resultDataSet.Tables.Add(dtForecast)

            Dim dtApproved As DataTable = New DataTable()
            dtApproved.TableName = ApplicationConstants.ApprovedDt
            resultDataSet.Tables.Add(dtApproved)

            Dim dtArranged As DataTable = New DataTable()
            dtArranged.TableName = ApplicationConstants.ArrangedDt
            resultDataSet.Tables.Add(dtArranged)

            Dim dtCompleted As DataTable = New DataTable()
            dtCompleted.TableName = ApplicationConstants.CompletedDt
            resultDataSet.Tables.Add(dtCompleted)

            Dim dtComponent As DataTable = New DataTable()
            dtComponent.TableName = ApplicationConstants.ComponentDt
            resultDataSet.Tables.Add(dtComponent)

            Dim replacementYearParam As ParameterBO = New ParameterBO("replacementYear", replacementYear, DbType.String)
            parameterList.Add(replacementYearParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parameterList.Add(searchTextParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parameterList.Add(schemeIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetBudgetSpendReportExcelDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtForecast, dtApproved, dtArranged, dtCompleted, dtComponent)

        End Sub
#End Region


#End Region

        Function getCompletedWorksList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal search As String, ByVal PlannedType As Integer) As Integer


            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", search, DbType.String)
            parametersList.Add(searchTextParam)

            Dim PlannedTypeParam As ParameterBO = New ParameterBO("PlannedType", PlannedType, DbType.Int32)
            parametersList.Add(PlannedTypeParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParamList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParamList, SpNameConstants.GetCOMPLETEDWORKSLIST)
            Return outParamList.Item(0).Value.ToString()


        End Function

    End Class
End Namespace
