﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports PL_Utilities
Imports PL_BusinessObject

Namespace PL_DataAccess
    Public Class SchedulingDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Appointment to be Arranged List"
        ''' <summary>
        ''' Get Appointment to be Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer,
                                                       ByVal searchText As String, ByVal componentId As Integer, Optional ByRef type As String = "") As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim compomentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.String)
            parametersList.Add(compomentIdParam)

            Dim typeParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAppointmentsToBeArrangedList)
            Return Convert.ToInt32(outParametersList.Item(0).Value.ToString())

        End Function

#End Region

#Region "Get Appointments Arranged List"
        ''' <summary>
        ''' Get Appointments Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer,
                                                    ByVal searchText As String, ByVal componentId As Integer, ByVal appointmentDate As String,
                                                    Optional ByRef type As String = "")
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim compomentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.String)
            parametersList.Add(compomentIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", appointmentDate, DbType.String)
            parametersList.Add(appointmentDateParam)

            Dim typeParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAppointmentsArrangedList)
            Return outParametersList.Item(0).Value.ToString()


        End Function

#End Region

#Region "Get Appointments Cancelled List"
        ''' <summary>
        ''' Get Appointment Cancelled List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function getAppointmentCancelledList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal searchText As String, ByVal componentId As Integer) As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim compomentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.String)
            parametersList.Add(compomentIdParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAppointmentsCancelledList)
            Return Convert.ToInt32(outParametersList.Item(0).Value.ToString())

        End Function

#End Region

#Region "Cancel Appointment to be Arranged List"
        ''' <summary>
        ''' Get Appointment to be Arranged List
        ''' </summary>
        ''' <param name="pmo"></param>
        ''' <param name="reasonId"></param>
        ''' <param name="reason"></param>
        ''' <param name="userId"></param>
        ''' <param name="prevDue"></param>
        ''' <param name="newDue"></param>
        ''' <param name="itemId"></param>
        ''' <param name="propertyId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function cancelAppointmentToBeArrangedList(ByVal pmo As Integer, ByVal reasonId As Integer, ByVal reason As String, ByVal userId As Integer,
                                                          ByVal prevDue As DateTime, ByVal newDue As DateTime, ByVal itemId As Integer, ByVal propertyId As String,
                                                          ByVal componentId As Integer, ByVal type As String) As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim reasonIdParam As ParameterBO = New ParameterBO("reasonId", reasonId, DbType.Int32)
            parametersList.Add(reasonIdParam)

            Dim reasonParam As ParameterBO = New ParameterBO("reason", reason, DbType.String)
            parametersList.Add(reasonParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parametersList.Add(itemIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim propertyIdParam As ParameterBO
            Dim schemeIdParam As ParameterBO
            Dim blockIdParam As ParameterBO

            If (type = ApplicationConstants.propertyType) Then
                propertyIdParam = New ParameterBO("PropertyId", propertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            ElseIf (type = ApplicationConstants.schemeType) Then
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(propertyId), DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            Else
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", Convert.ToInt32(propertyId), DbType.Int32)
                parametersList.Add(blockIdParam)
            End If
            Dim prevDueParam As ParameterBO
            If prevDue = "#12:00:00 AM#" Then
                prevDueParam = New ParameterBO("prevDue", DBNull.Value, DbType.DateTime2)
            Else
                prevDueParam = New ParameterBO("prevDue", prevDue, DbType.DateTime2)
            End If
            parametersList.Add(prevDueParam)

            Dim newDueParam As ParameterBO = New ParameterBO("newDue", newDue, DbType.DateTime2)
            parametersList.Add(newDueParam)

            Dim typeParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeParam)

            Dim isCancelledParam As ParameterBO = New ParameterBO("isCancelled", 0, DbType.Int32)
            outParametersList.Add(isCancelledParam)
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.CancelAppointmentToBeArrange)

            Return CType(outParametersList.Item(0).Value, Integer)

        End Function

#End Region

#Region "Get Property Search Result"
        ''' <summary>
        ''' Get Property Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <remarks></remarks>
        Public Sub getPropertySearchResult(ByRef resultDataSet As DataSet, ByRef searchText As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyResult As DataTable = New DataTable()

            dtPropertyResult.TableName = "PropertySearchResult"

            resultDataSet.Tables.Add(dtPropertyResult)

            Dim dtSchemeResult As DataTable = New DataTable()

            dtSchemeResult.TableName = "SchemeSearchResult"

            resultDataSet.Tables.Add(dtSchemeResult)

            Dim dtBlockResult As DataTable = New DataTable()

            dtBlockResult.TableName = "BlockSearchResult"

            resultDataSet.Tables.Add(dtBlockResult)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSearchProperty)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult, dtSchemeResult, dtBlockResult)

        End Sub

#End Region

#Region "Get Location Adaptations"
        ''' <summary>
        ''' Get Location Adaptations
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="parameterID"></param>
        ''' <remarks></remarks>
        Public Sub getLocationAdaptations(ByRef resultDataSet As DataSet, ByRef parameterID As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAdaptationResult As DataTable = New DataTable()

            dtAdaptationResult.TableName = "LocationAdaptations"

            resultDataSet.Tables.Add(dtAdaptationResult)

            Dim locationParam As ParameterBO = New ParameterBO("parameterID", parameterID, DbType.Int16)
            parametersList.Add(locationParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetLocationAdaptations)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAdaptationResult)

        End Sub

#End Region

#Region "Get Property Customer Detail"
        ''' <summary>
        ''' Get Property Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyCustomerDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyResult As DataTable = New DataTable()

            dtPropertyResult.TableName = "PropertyCustomerDetail"

            resultDataSet.Tables.Add(dtPropertyResult)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult)

        End Sub
#End Region

#Region "Get Scheme Detail"
        ''' <summary>
        ''' Get Scheme Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Public Sub getSchemeDetail(ByRef resultDataSet As DataSet, ByVal schemeId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtSchemeResult As DataTable = New DataTable()

            dtSchemeResult.TableName = "SchemeDetail"

            resultDataSet.Tables.Add(dtSchemeResult)

            Dim propertyIdParam As ParameterBO = New ParameterBO("schemeId", Convert.ToInt32(schemeId), DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSchemeDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtSchemeResult)

        End Sub
#End Region

#Region "Get Block Detail"
        ''' <summary>
        ''' Get Block Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="blockId"></param>
        ''' <remarks></remarks>
        Public Sub getBlockDetail(ByRef resultDataSet As DataSet, ByVal blockId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtBlockResult As DataTable = New DataTable()

            dtBlockResult.TableName = "BlockDetail"

            resultDataSet.Tables.Add(dtBlockResult)

            Dim propertyIdParam As ParameterBO = New ParameterBO("blockId", Convert.ToInt32(blockId), DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetBlockDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtBlockResult)

        End Sub
#End Region

#Region "Get Address By Type"
        ''' <summary>
        ''' Get Address By Type
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="Id"></param>
        ''' <param name="type"></param>
        ''' <remarks></remarks>
        Public Sub getAddressByType(ByRef resultDataSet As DataSet, ByVal Id As String, ByVal type As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAddressResult As DataTable = New DataTable()

            dtAddressResult.TableName = "AddressDetail"

            resultDataSet.Tables.Add(dtAddressResult)

            Dim idParam As ParameterBO = New ParameterBO("id", Id, DbType.String)
            parametersList.Add(idParam)

            Dim typeParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAddressByType)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAddressResult)

        End Sub
#End Region

#Region "Get risk and vulnerability info"
        ''' <summary>
        ''' Get risk and vulnerability info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getRiskAndVulnerabilityInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRiskResult As DataTable = New DataTable()
            dtRiskResult.TableName = ApplicationConstants.RiskInfo
            resultDataSet.Tables.Add(dtRiskResult)

            Dim dtVulnerabilityResult As DataTable = New DataTable()
            dtVulnerabilityResult.TableName = ApplicationConstants.VulnerabilityInfo
            resultDataSet.Tables.Add(dtVulnerabilityResult)

            Dim propertyIdParam As ParameterBO = New ParameterBO("@journalId", journalId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRiskAndVulnerabilityInfo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRiskResult, dtVulnerabilityResult)

        End Sub
#End Region

#Region "Get Confirm Appointments"
        ''' <summary>
        ''' This function 'll get pending component trade's appointment
        ''' </summary>        
        ''' <param name="journalId"></param>        
        ''' <remarks></remarks>
        Public Sub getConfirmTradeAppointments(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal journalId As Integer, Optional ByVal type As String = ApplicationConstants.propertyType)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO
            Dim schemeIdParam As ParameterBO
            Dim blockIdParam As ParameterBO

            If (type = ApplicationConstants.propertyType) Then
                propertyIdParam = New ParameterBO("propertyId", propertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            ElseIf (type = ApplicationConstants.schemeType) Then
                propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(propertyId), DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            Else
                propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", Convert.ToInt32(propertyId), DbType.Int32)
                parametersList.Add(blockIdParam)
            End If

            Dim typeIdParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetConfirmAppointments)
        End Sub


#End Region



#Region "Get Misc Confirm Appointments"
        ''' <summary>
        ''' This function 'll get pending component trade's appointment
        ''' </summary>        
        ''' <param name="journalId"></param>        
        ''' <remarks></remarks>
        Public Sub getMiscConfirmTradeAppointments(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal journalId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetMiscConfirmAppointments)
        End Sub


#End Region

#Region "Get Component Trades"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>        
        ''' <param name="journalId"></param>        
        ''' <remarks></remarks>
        Public Sub getComponentTrades(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetComponentTrades)
        End Sub


#End Region

#Region "Get Misc Component Trades"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>        
        ''' <param name="journalId"></param>        
        ''' <remarks></remarks>
        Public Sub getMiscComponentTrades(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetMiscComponentTrades)
        End Sub


#End Region


#Region "Get Misc Component Details"
        ''' <summary>
        ''' This function 'll get the trades against component
        ''' </summary>        
        ''' <param name="journalId"></param>        
        ''' <remarks></remarks>
        Public Sub getMiscComponentDetails(ByRef resultDataSet As DataSet, ByVal journalId As Integer, ByVal tradeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", tradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetMiscComponentDetails)
        End Sub


#End Region

#Region "Schedule Misc Work appointment"
        ''' <summary>
        ''' Schedule Misc Work appointment
        ''' </summary>
        ''' <param name="objMiscAppointmentBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function scheduleMiscWorkAppointment(ByVal objMiscAppointmentBO As MiscAppointmentBO, ByRef pmo As Integer, ByVal fromScheduling As Boolean)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO
            Dim schemeIdParam As ParameterBO
            Dim blockIdParam As ParameterBO

            If (objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                propertyIdParam = New ParameterBO("PropertyId", objMiscAppointmentBO.PropertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            ElseIf (objMiscAppointmentBO.Type = ApplicationConstants.schemeType) Then
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(objMiscAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            Else
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", Convert.ToInt32(objMiscAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(blockIdParam)
            End If

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objMiscAppointmentBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objMiscAppointmentBO.TradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)

            Dim parameterIdParam As ParameterBO = New ParameterBO("parameterId", objMiscAppointmentBO.LocationId, DbType.Int32)
            parametersList.Add(parameterIdParam)

            Dim parameterValueIdParam As ParameterBO = New ParameterBO("parameterValueId", objMiscAppointmentBO.AdaptationId, DbType.Int32)
            parametersList.Add(parameterValueIdParam)

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", objMiscAppointmentBO.CustomerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", objMiscAppointmentBO.AppointmentNotes, DbType.String)
            parametersList.Add(appointmentNotesParam)

            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objMiscAppointmentBO.TenancyId, DbType.Int32)
            parametersList.Add(tenancyIdParam)

            Dim appointmentTypeIdParam As ParameterBO = New ParameterBO("appointmentTypeId", objMiscAppointmentBO.AppointmentTypeId, DbType.Int32)
            parametersList.Add(appointmentTypeIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", objMiscAppointmentBO.StartDate, DbType.Date)
            parametersList.Add(appointmentDateParam)

            Dim appointmentEndDateParam As ParameterBO = New ParameterBO("appointmentEndDate", objMiscAppointmentBO.EndDate, DbType.Date)
            parametersList.Add(appointmentEndDateParam)

            Dim startTimeParam As ParameterBO = New ParameterBO("startTime", objMiscAppointmentBO.StartTime, DbType.String)
            parametersList.Add(startTimeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", objMiscAppointmentBO.EndTime, DbType.String)
            parametersList.Add(endTimeParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", objMiscAppointmentBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim pmoParam As ParameterBO = New ParameterBO("PMO", objMiscAppointmentBO.PMO, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim fromSchedulingParam As ParameterBO = New ParameterBO("fromScheduling", fromScheduling, DbType.Boolean)
            parametersList.Add(fromSchedulingParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objMiscAppointmentBO.Duration, DbType.Int32)
            parametersList.Add(durationParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", 0, DbType.Int32)
            outParametersList.Add(journalIdOutParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.ScheduleMiscWorkAppointment)
            pmo = outParametersList.Item(1).Value

            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Available Operatives"
        ''' <summary>
        ''' This function 'll get the list of operatives that are available
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tradeIds"></param>
        ''' <param name="propertyId"></param>
        ''' <param name="startDate"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet, ByVal tradeIds As String, ByVal propertyId As String, ByVal startDate As Date)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim employeeDt As DataTable = New DataTable()
            employeeDt.TableName = ApplicationConstants.AvailableOperatives
            Dim leavesDt As DataTable = New DataTable()
            leavesDt.TableName = ApplicationConstants.OperativesLeaves
            Dim appointmentsDt As DataTable = New DataTable()
            appointmentsDt.TableName = ApplicationConstants.OperativesAppointments

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim tradeIdsParam As ParameterBO = New ParameterBO("tradeIds", tradeIds, DbType.String)
            parametersList.Add(tradeIdsParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", startDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAvailableOperatives)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)
        End Sub
#End Region

#Region "Get Operatives Leaves And Appointments -"
        ''' <summary>
        ''' This function 'll return the leaves and appointments of operative
        ''' </summary>
        ''' <param name="resultDataSet"></param>        
        ''' <remarks></remarks>
        Public Sub getOperativesLeavesAndAppointments(ByRef resultDataSet As DataSet, ByVal startDate As Date)
            'Public Sub getOperativesLeavesAndAppointments(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal startDate As Date)

            Dim parameterList As ParameterList = New ParameterList()

            Dim leavesDt As DataTable = New DataTable()
            leavesDt.TableName = ApplicationConstants.OperativesLeaves
            Dim appointmentsDt As DataTable = New DataTable()
            appointmentsDt.TableName = ApplicationConstants.OperativesAppointments
            Dim availableOperativeDt As DataTable = New DataTable()
            availableOperativeDt.TableName = ApplicationConstants.AvailableOperatives

            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)
            resultDataSet.Tables.Add(availableOperativeDt)

            'Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            'parameterList.Add(operativeIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", startDate, DbType.Date)
            parameterList.Add(startDateParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.PLANNED_GetOperativesLeavesAndAppointmentsReplacementList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, leavesDt, appointmentsDt, availableOperativeDt)

        End Sub


        Public Sub getOperativesLeavesAndAppointments(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal startDate As Date)

            Dim parameterList As ParameterList = New ParameterList()

            Dim leavesDt As DataTable = New DataTable()
            leavesDt.TableName = ApplicationConstants.OperativesLeaves
            Dim appointmentsDt As DataTable = New DataTable()
            appointmentsDt.TableName = ApplicationConstants.OperativesAppointments

            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parameterList.Add(operativeIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", startDate, DbType.Date)
            parameterList.Add(startDateParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetOperativesLeavesAndAppointments)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, leavesDt, appointmentsDt)

        End Sub
#End Region

#Region "Get Arranged Appointments Detail"
        ''' <summary>
        ''' Get Arranged Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getArrangedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPlannedAppointmentDetail As DataTable = New DataTable()
            dtPlannedAppointmentDetail.TableName = "PlannedAppointmentsDetail"
            resultDataSet.Tables.Add(dtPlannedAppointmentDetail)

            Dim dtPropertyDetail As DataTable = New DataTable()
            dtPropertyDetail.TableName = "PropertyDetail"
            resultDataSet.Tables.Add(dtPropertyDetail)

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetArrangedAppointmentsDetailByPmo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPlannedAppointmentDetail, dtPropertyDetail)

        End Sub

#End Region

#Region "Get Cancelled Appointments Detail"
        ''' <summary>
        ''' Get Cancelled Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getCancelledAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPlannedAppointmentDetail As DataTable = New DataTable()
            dtPlannedAppointmentDetail.TableName = "PlannedAppointmentsDetail"
            resultDataSet.Tables.Add(dtPlannedAppointmentDetail)

            Dim dtPropertyDetail As DataTable = New DataTable()
            dtPropertyDetail.TableName = "PropertyDetail"
            resultDataSet.Tables.Add(dtPropertyDetail)

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetCancelledAppointmentsDetailByPmo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPlannedAppointmentDetail, dtPropertyDetail)

        End Sub

#End Region

#Region "Get Email Appointments Detail"
        ''' <summary>
        ''' Get Cancelled Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getEmailAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPlannedAppointmentDetail As DataTable = New DataTable()
            dtPlannedAppointmentDetail.TableName = "PlannedAppointmentsDetail"
            resultDataSet.Tables.Add(dtPlannedAppointmentDetail)

            Dim dtPropertyDetail As DataTable = New DataTable()
            dtPropertyDetail.TableName = "PropertyDetail"
            resultDataSet.Tables.Add(dtPropertyDetail)

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.PLANNED_EmailDetailByPmo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPlannedAppointmentDetail, dtPropertyDetail)

        End Sub

#End Region

#Region "Get Condition Arranged Appointments Detail"
        ''' <summary>
        ''' Get Condition Arranged Appointments Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getConditionArrangedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtConditionAppointmentDetail As DataTable = New DataTable()
            dtConditionAppointmentDetail.TableName = "ConditionAppointmentsDetail"
            resultDataSet.Tables.Add(dtConditionAppointmentDetail)

            Dim dtPropertyDetail As DataTable = New DataTable()
            dtPropertyDetail.TableName = "PropertyDetail"
            resultDataSet.Tables.Add(dtPropertyDetail)

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetConditionArrangedAppointmentsDetailByPmo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConditionAppointmentDetail, dtPropertyDetail)

        End Sub

#End Region

#Region "Schedule Planned Work appointment"
        ''' <summary>
        ''' Schedule Planned Work appointment
        ''' </summary>
        ''' <param name="objPlannedAppointmentBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function schedulePlannedWorkAppointment(ByVal objPlannedAppointmentBO As PlannedAppointmentBO, ByRef appointmentId As Integer)
            'TODO:check in stored procedure that appointment is already booked against this operative or not.
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", objPlannedAppointmentBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPlannedAppointmentBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objPlannedAppointmentBO.TradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)

            Dim compTradeIdParam As ParameterBO = New ParameterBO("compTradeId", objPlannedAppointmentBO.CompTradeId, DbType.Int32)
            parametersList.Add(compTradeIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", objPlannedAppointmentBO.PMO, DbType.Int32)
            parametersList.Add(journalIdParam)

            'Dim isPendingParam As ParameterBO = New ParameterBO("isPending", objPlannedAppointmentBO.IsPending, DbType.Int32)
            'parametersList.Add(isPendingParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", objPlannedAppointmentBO.ComponentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", objPlannedAppointmentBO.CustomerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", objPlannedAppointmentBO.AppointmentNotes, DbType.String)
            parametersList.Add(appointmentNotesParam)

            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objPlannedAppointmentBO.TenancyId, DbType.Int32)
            parametersList.Add(tenancyIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objPlannedAppointmentBO.StartDate.Date, DbType.Date)
            parametersList.Add(startDateParam)

            Dim endDateParam As ParameterBO = New ParameterBO("endDate", objPlannedAppointmentBO.EndDate.Date, DbType.Date)
            parametersList.Add(endDateParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objPlannedAppointmentBO.Duration, DbType.Double)
            parametersList.Add(durationParam)

            Dim startTimeParam As ParameterBO = New ParameterBO("startTime", objPlannedAppointmentBO.StartTime, DbType.String)
            parametersList.Add(startTimeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", objPlannedAppointmentBO.EndTime, DbType.String)
            parametersList.Add(endTimeParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", objPlannedAppointmentBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            Dim appointmentIdOutParam As ParameterBO = New ParameterBO("appointmentIdOut", 0, DbType.Int32)
            outParametersList.Add(appointmentIdOutParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SchedulePlannedWorkAppointment)
            appointmentId = outParametersList.Item(1).Value

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "Update Planned Work appointment"
        ''' <summary>
        ''' Update Planned Work appointment
        ''' </summary>
        ''' <param name="objPlannedAppointmentBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function updatePlannedWorkAppointment(ByVal objPlannedAppointmentBO As PlannedAppointmentBO)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO
            Dim schemeIdParam As ParameterBO
            Dim blockIdParam As ParameterBO

            If (objPlannedAppointmentBO.Type = ApplicationConstants.propertyType) Then
                propertyIdParam = New ParameterBO("propertyId", objPlannedAppointmentBO.PropertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            ElseIf (objPlannedAppointmentBO.Type = ApplicationConstants.schemeType) Then
                propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(objPlannedAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            Else
                propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", Convert.ToInt32(objPlannedAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(blockIdParam)
            End If


            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPlannedAppointmentBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", objPlannedAppointmentBO.PMO, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", objPlannedAppointmentBO.ComponentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", objPlannedAppointmentBO.CustomerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", objPlannedAppointmentBO.AppointmentNotes, DbType.String)
            parametersList.Add(appointmentNotesParam)

            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objPlannedAppointmentBO.TenancyId, DbType.Int32)
            parametersList.Add(tenancyIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objPlannedAppointmentBO.StartDate.Date, DbType.Date)
            parametersList.Add(startDateParam)

            Dim endDateParam As ParameterBO = New ParameterBO("endDate", objPlannedAppointmentBO.EndDate.Date, DbType.Date)
            parametersList.Add(endDateParam)

            Dim startTimeParam As ParameterBO = New ParameterBO("startTime", objPlannedAppointmentBO.StartTime, DbType.String)
            parametersList.Add(startTimeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", objPlannedAppointmentBO.EndTime, DbType.String)
            parametersList.Add(endTimeParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objPlannedAppointmentBO.Duration, DbType.Double)
            parametersList.Add(durationParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", objPlannedAppointmentBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim apponintmentIdParam As ParameterBO = New ParameterBO("appointmentId", objPlannedAppointmentBO.AppointmentId, DbType.Int32)
            parametersList.Add(apponintmentIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.UpdatePlannedWorkAppointment)

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "Delete Appointment"
        ''' <summary>
        ''' This function 'll delete the appointment 
        ''' </summary>        
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function deleteAppointment(ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteAppointment)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "Delete Misc Appointment"
        ''' <summary>
        ''' This function 'll delete the appointment 
        ''' </summary>        
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function deleteMiscAppointment(ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteMiscAppointment)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "Remove trade from scheduling"
        ''' <summary>
        ''' This function 'll remove trade from scheduling
        ''' </summary>        
        ''' <param name="compTradeId"></param>        
        ''' <remarks></remarks>
        Public Function removeTradeFromScheduling(ByVal pmo As Integer, ByVal compTradeId As Integer, ByVal duration As Double)


            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim compTradeIdParam As ParameterBO = New ParameterBO("compTradeId", compTradeId, DbType.Int32)
            parametersList.Add(compTradeIdParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", duration, DbType.Double)
            parametersList.Add(durationParam)

            Dim isRemovedParam As ParameterBO = New ParameterBO("isRemoved", 0, DbType.Boolean)
            outParametersList.Add(isRemovedParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.RemoveTradeFromScheduling)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region


#Region "Remove misc trade from scheduling"
        ''' <summary>
        ''' This function 'll remove trade from scheduling
        ''' </summary>        
        ''' <param name="compTradeId"></param>        
        ''' <remarks></remarks>
        Public Function removeMiscTradeFromScheduling(ByVal pmo As Integer, ByVal compTradeId As Integer, ByVal duration As Double)


            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim compTradeIdParam As ParameterBO = New ParameterBO("miscTradeId", compTradeId, DbType.Int32)
            parametersList.Add(compTradeIdParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", duration, DbType.Double)
            parametersList.Add(durationParam)

            Dim isRemovedParam As ParameterBO = New ParameterBO("isRemoved", 0, DbType.Boolean)
            outParametersList.Add(isRemovedParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.RemoveMiscTradeFromScheduling)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#Region "Get Removed trades from scheduling"
        ''' <summary>
        ''' Get Removed trades from scheduling
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getRemovedTradesFromScheduling(ByRef resultDataset As DataSet, ByVal pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            MyBase.LoadDataSet(resultDataset, parametersList, outParametersList, SpNameConstants.GetRemovedTradesFromScheduling)

        End Sub
#End Region


#Region "Get Removed misc trades from scheduling"
        ''' <summary>
        ''' Get Removed trades from scheduling
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="pmo"></param>
        ''' <remarks></remarks>
        Public Sub getRemovedMiscTradesFromScheduling(ByRef resultDataset As DataSet, ByVal pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            MyBase.LoadDataSet(resultDataset, parametersList, outParametersList, SpNameConstants.GetRemovedMiscTradesFromScheduling)

        End Sub
#End Region


#Region "Set Pending Bit For Appointment"
        ''' <summary>
        ''' This function 'll make the is pending = 0 against the appointment 
        ''' </summary>        
        ''' <param name="appointmentId"></param>        
        ''' <remarks></remarks>
        Public Function setPendingBitForAppointment(ByVal appointmentId As Integer, ByVal isPending As Boolean)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim isPendingParam As ParameterBO = New ParameterBO("isPending", isPending, DbType.Int32)
            parametersList.Add(isPendingParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SetPendingBitForAppointment)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "Schedule Confirmed Planned Appointments"
        ''' <summary>
        ''' This function 'll schedule the appointments that have been confirmed
        ''' </summary>        
        ''' <param name="appointmentIds"></param>        
        ''' <remarks></remarks>
        Public Function scheduleConfirmedPlannedAppointments(ByVal appointmentIds As String, ByVal propertyId As String, ByVal journalId As Integer, ByVal componentId As Integer, ByVal createdBy As Integer)
            'TODO:check in stored procedure that appointment is already booked against this operative or not.
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdsParam As ParameterBO = New ParameterBO("appointmentIds", appointmentIds, DbType.String)
            parametersList.Add(appointmentIdsParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim componentIdParam As ParameterBO = New ParameterBO("componentId", componentId, DbType.Int32)
            parametersList.Add(componentIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", createdBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ScheduleConfirmedPlannedAppointments)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Update appointment notes"

        ''' <summary>
        ''' Update appointment notes
        ''' </summary>
        ''' <param name="customerNotes"></param>
        ''' <param name="jobsheetNotes"></param>
        ''' <param name="appointmentId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function updatePlannedAppointmentsNotes(ByVal customerNotes As String, ByVal jobsheetNotes As String, ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", customerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim jobsheetNotesParam As ParameterBO = New ParameterBO("jobsheetNotes", jobsheetNotes, DbType.String)
            parametersList.Add(jobsheetNotesParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.UpdatePlannedAppointmentNotes)
            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "Cancel appointment"
        ''' <summary>
        ''' Cancel appointment
        ''' </summary>
        ''' <param name="pmo"></param>
        ''' <param name="reason"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function cancelAppointment(ByVal pmo As Integer, ByVal reason As String, ByVal appontmntstatus As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim jobsheetNotesParam As ParameterBO = New ParameterBO("reason", reason, DbType.String)
            parametersList.Add(jobsheetNotesParam)

            Dim appointmentStatus As ParameterBO = New ParameterBO("appointmentStatus", appontmntstatus, DbType.String)
            parametersList.Add(appointmentStatus)

            Dim isCancelledParam As ParameterBO = New ParameterBO("isCancelled", 0, DbType.Int32)
            outParametersList.Add(isCancelledParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.CancelArrangedAppointment)
            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "Get associated PMOs"
        ''' <summary>
        ''' Get associated PMOs
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAssociatedPmos(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef type As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAssociatedPmos As DataTable = New DataTable()
            dtAssociatedPmos.TableName = "AssociatedPmos"
            resultDataSet.Tables.Add(dtAssociatedPmos)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim typeParam As ParameterBO = New ParameterBO("type", type, DbType.String)
            parametersList.Add(typeParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAssociatedPmos)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAssociatedPmos)

        End Sub
#End Region

#Region "Get associated appointments"
        ''' <summary>
        ''' Get associated PMOs
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAssociatedAppointmentList(ByRef resultDataSet As DataSet, ByVal journalId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAssociatedPmos As DataTable = New DataTable()
            dtAssociatedPmos.TableName = "AssociatedAppointments"
            resultDataSet.Tables.Add(dtAssociatedPmos)

            Dim propertyIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAssociatedAppointmentList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAssociatedPmos)

        End Sub
#End Region

#Region "Get Condition Appointment to be Arranged List"

        ''' <summary>
        ''' Get Condition Work's Appointments to be Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        Public Function getConditionAppointmentsToBeArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetConditionalToBeArrangedList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Locations"
        ''' <summary>
        ''' Get Locations
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getLocations(ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtLocations As DataTable = New DataTable()
            dtLocations.TableName = "Locations"
            resultDataSet.Tables.Add(dtLocations)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetLocations)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtLocations)

        End Sub
#End Region

#Region "Get Appointment types"
        ''' <summary>
        ''' Get Appointment types
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAppointmentTypes(ByRef resultDataSet As DataSet, ByVal isActive As Boolean)

            Dim parametersList As ParameterList = New ParameterList()

            Dim paramIsActive As ParameterBO = New ParameterBO("isActive", isActive, DbType.Boolean)
            parametersList.Add(paramIsActive)

            Dim dtAppointmentTypes As DataTable = New DataTable()
            dtAppointmentTypes.TableName = "AppointmentTypes"
            resultDataSet.Tables.Add(dtAppointmentTypes)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAppointmentTypes)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentTypes)

        End Sub
#End Region

#Region "Get Condition Appointments Arranged List"

        ''' <summary>
        ''' Get Condition Appointments Arranged List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <param name="searchText"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getConditionAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetConditionalArrangedList)
            Return outParametersList.Item(0).Value.ToString()


        End Function

#End Region

#Region "Schedule Condition Works Appointment"
        ''' <summary>
        ''' Schedule Condition Works Appointment
        ''' </summary>
        ''' <param name="objConditionWorksAppointmentBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function scheduleConditionWorksAppointment(ByVal objConditionWorksAppointmentBO As ConditionWorksAppointmentBO, ByRef appointmentId As Integer)
            'TODO:check in stored procedure that appointment is already booked against this operative or not.
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objConditionWorksAppointmentBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", objConditionWorksAppointmentBO.PMO, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objConditionWorksAppointmentBO.TenancyId, DbType.Int32)
            parametersList.Add(tenancyIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objConditionWorksAppointmentBO.TradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objConditionWorksAppointmentBO.StartDate.Date, DbType.Date)
            parametersList.Add(startDateParam)

            Dim endDateParam As ParameterBO = New ParameterBO("endDate", objConditionWorksAppointmentBO.EndDate.Date, DbType.Date)
            parametersList.Add(endDateParam)

            Dim startTimeParam As ParameterBO = New ParameterBO("startTime", objConditionWorksAppointmentBO.StartTime, DbType.String)
            parametersList.Add(startTimeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", objConditionWorksAppointmentBO.EndTime, DbType.String)
            parametersList.Add(endTimeParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", objConditionWorksAppointmentBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", objConditionWorksAppointmentBO.CustomerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", objConditionWorksAppointmentBO.AppointmentNotes, DbType.String)
            parametersList.Add(appointmentNotesParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objConditionWorksAppointmentBO.Duration, DbType.Int32)
            parametersList.Add(durationParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            Dim appointmentIdOutParam As ParameterBO = New ParameterBO("appointmentIdOut", 0, DbType.Int32)
            outParametersList.Add(appointmentIdOutParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.ScheduleConditionWorksAppointment)
            appointmentId = outParametersList.Item(1).Value

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "Schedule Confirmed Planned Appointments"
        ''' <summary>
        ''' This function 'll confirm scheduled Condition appointments. (i.e set ispending to false)
        ''' </summary>        
        ''' <param name="journalId"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Function ConfirmedCoditionAppointments(ByVal journalId As Integer, ByVal userId As Integer)
            'TODO:check in stored procedure that appointment is already booked against this operative or not.
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ConfirmConditionWorksAppointment)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Delete Removed Trade"
        ''' <summary>
        ''' This function 'll delete the removed trade
        ''' </summary>        
        ''' <param name="componentTradeId"></param>        
        ''' <remarks></remarks>
        Public Function deleteRemovedTrade(ByVal componentTradeId As Integer, ByVal pmo As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim componentTradeIdParam As ParameterBO = New ParameterBO("componentTradeId", componentTradeId, DbType.Int32)
            parametersList.Add(componentTradeIdParam)

            Dim pmoParam As ParameterBO = New ParameterBO("pmo", pmo, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim isDeletedParam As ParameterBO = New ParameterBO("isDeleted", 0, DbType.Boolean)
            outParametersList.Add(isDeletedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteRemovedTrades)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region


#Region "Add Appointment to Scheduling"
        ''' <summary>
        ''' Add Appointment to Scheduling
        ''' </summary>
        ''' <param name="objMiscAppointmentBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function addAppointmentToScheduling(ByVal objMiscAppointmentBO As MiscAppointmentBO, ByRef pmo As Integer, ByVal tradesDt As DataTable)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO
            Dim schemeIdParam As ParameterBO
            Dim blockIdParam As ParameterBO

            If (objMiscAppointmentBO.Type = ApplicationConstants.propertyType) Then
                propertyIdParam = New ParameterBO("PropertyId", objMiscAppointmentBO.PropertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            ElseIf (objMiscAppointmentBO.Type = ApplicationConstants.schemeType) Then
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(objMiscAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
                parametersList.Add(blockIdParam)
            Else
                propertyIdParam = New ParameterBO("PropertyId", DBNull.Value, DbType.String)
                parametersList.Add(propertyIdParam)

                schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
                parametersList.Add(schemeIdParam)

                blockIdParam = New ParameterBO("blockId", Convert.ToInt32(objMiscAppointmentBO.PropertyId), DbType.Int32)
                parametersList.Add(blockIdParam)
            End If

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objMiscAppointmentBO.UserId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim parameterIdParam As ParameterBO = New ParameterBO("parameterId", objMiscAppointmentBO.LocationId, DbType.Int32)
            parametersList.Add(parameterIdParam)

            Dim parameterValueIdParam As ParameterBO = New ParameterBO("parameterValueId", objMiscAppointmentBO.AdaptationId, DbType.Int32)
            parametersList.Add(parameterValueIdParam)

            Dim customerNotesParam As ParameterBO = New ParameterBO("customerNotes", objMiscAppointmentBO.CustomerNotes, DbType.String)
            parametersList.Add(customerNotesParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", objMiscAppointmentBO.AppointmentNotes, DbType.String)
            parametersList.Add(appointmentNotesParam)

            Dim appointmentTypeIdParam As ParameterBO = New ParameterBO("appointmentTypeId", objMiscAppointmentBO.AppointmentTypeId, DbType.Int32)
            parametersList.Add(appointmentTypeIdParam)


            Dim pmoParam As ParameterBO = New ParameterBO("PMO", objMiscAppointmentBO.PMO, DbType.Int32)
            parametersList.Add(pmoParam)

            Dim locationParam As ParameterBO = New ParameterBO("locationName", objMiscAppointmentBO.LocationName, DbType.String)
            parametersList.Add(locationParam)

            Dim adaptationParam As ParameterBO = New ParameterBO("adaptationName", objMiscAppointmentBO.AdaptationName, DbType.String)
            parametersList.Add(adaptationParam)

            Dim tradesDtParam As New ParameterBO("tradesDt", tradesDt, SqlDbType.Structured)
            parametersList.Add(tradesDtParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", 0, DbType.Int32)
            outParametersList.Add(journalIdOutParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.ScheduleAdhocAppointment)
            pmo = outParametersList.Item(1).Value

            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get operative working hours"
        ''' <summary>
        ''' get operative working hours
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub getOperativeWorkingHours(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal ofcCoreStartTime As String, ByVal ofcCoreEndTime As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim workingHourDataTable As DataTable = New DataTable(ApplicationConstants.WorkingHourDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(tempFaultIdsParam)

            Dim ofcCoreStartTimeParam As ParameterBO = New ParameterBO("defaultStartTime", ofcCoreStartTime, DbType.String)
            parametersList.Add(ofcCoreStartTimeParam)

            Dim ofcCoreEndTimeParam As ParameterBO = New ParameterBO("defaultEndTime", ofcCoreEndTime, DbType.String)
            parametersList.Add(ofcCoreEndTimeParam)

            resultDataSet.Tables.Add(workingHourDataTable)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetOperativeWorkingHours)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, workingHourDataTable)
        End Sub
#End Region

#End Region

    End Class
End Namespace



