USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC	[dbo].[AS_GetCP12DocumentByLGSRHISTORYID] @LGSRHISTORYID = 15
-- Author:		<Aamir Waheed>
-- Create date: <October, 15 2013>
-- Description:	<This Stored Proceedure gets CP12Document against a LGSRHISTORYID>
-- Web Page: PropertyRecord.aspx.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetCP12DocumentByLGSRHISTORYID]

	@LGSRHISTORYID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here 
    
    SELECT PROPERTYID, CP12NUMBER, DOCUMENTTYPE, CP12DOCUMENT 
	FROM P_LGSR_HISTORY
	WHERE LGSRHISTORYID = @LGSRHISTORYID
END