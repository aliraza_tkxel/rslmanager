  
-- =============================================  
-- Author:  <Author,,Ali Raza>  
-- Create date: <Create Date,30 June, 2014>  
-- Description: <Description,,>  
--EXEC dbo.[AS_GetRefurbishmentInformaiton]  
--@propertyId='A010580008'  
-- =============================================  
CREATE PROCEDURE dbo.[AS_GetRefurbishmentInformaiton]  
 -- Add the parameters for the stored procedure here  
@propertyId as varchar(50)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 SELECT PROPERTYID,CONVERT(nvarchar(20), REFURBISHMENT_DATE, 103) as REFURBISHMENT_DATE,e.FIRSTNAME+', '+e.LASTNAME as USERNAME,NOTES FROM P_REFURBISHMENT_HISTORY  
 INNER JOIN E__EMPLOYEE e on e.EMPLOYEEID = USERID   
  WHERE PROPERTYID=@propertyId  ORDER BY REFURBISHMENT_DATE desc    
END  