USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectContractorDropDownValues]    Script Date: 09/23/2015 16:20:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noor Muhammad
-- Create date: 04/01/2015
-- Description:	To get contractors having at lease one Defect contract, as drop down values for assgin work to contractor.
-- History:          23/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
ALTER PROCEDURE [dbo].[DF_GetDefectContractorDropDownValues] 
	-- Add the parameters for the stored procedure here	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID AND RTRIM(LTRIM(AoW.DESCRIPTION)) LIKE '%Gas%'
	ORDER BY NAME ASC
	
	
END
