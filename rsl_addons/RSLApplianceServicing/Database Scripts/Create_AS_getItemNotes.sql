USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_getItemNotes]    Script Date: 11/01/2013 11:32:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_getItemNotes 'BHA0000987' ,1  
-- Author:  <Ali Raza>  
-- Create date: <10/30/2013>  
-- Description: <Get the property Notes against Item Id>  
-- WebPage: PropertRecord.aspx => Attributes Tab  
-- =============================================  
Create PROCEDURE [dbo].[AS_getItemNotes] (  
@proerptyId Varchar(100),  
@itemId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT [SID],PROPERTYID,ItemId,Notes, CONVERT(varchar,CreatedOn,103 )   CreatedOn,LEFT(e.FIRSTNAME, 1)+ LEFT(e.LASTNAME, 1)  CreatedBy  
 FROM PA_PROPERTY_ITEM_NOTES
 INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy 
 WHERE PROPERTYID = @proerptyId AND ItemId = @itemId  
END  