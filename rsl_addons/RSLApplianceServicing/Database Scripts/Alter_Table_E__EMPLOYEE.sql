-- Author:		<Ahmed Mehmood>
-- Create date: <30 April 2013>
-- Description:	<This script will create the new field (EmployeeSignaturePath) in E__EMPLOYEE>
ALTER TABLE [dbo].[E__EMPLOYEE] add [SIGNATUREPATH] [nvarchar](250) NULL
