SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC	[dbo].[AS_GetCountNoEntry]
--		@StatusTitle = N'No Entry',
--		@PatchId = 18,
--		@DevelopmentId = 1 
-- Author:		<Hussain Ali>
-- Create date: <31/10/2012>
-- Description:	<This stored procedure gets the count of all 'No Entries'>
-- Webpage: dashboard.aspx

-- =============================================
IF OBJECT_ID('dbo.[AS_GetCountNoEntry]') IS NULL 
	EXEC('CREATE PROCEDURE dbo.[AS_GetCountNoEntry] AS SET NOCOUNT ON;') 
GO
ALTER PROCEDURE [dbo].[AS_GetCountNoEntry]
	-- Add the parameters for the stored procedure here
	@PatchId		int,
	@DevelopmentId	int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	SET @fromClause		= 'FROM 
								 AS_APPOINTMENTS 
								INNER JOIN	AS_JOURNAL on AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID 
								INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId 
								INNER JOIN P__PROPERTY	ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
								INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID								
								LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID FROM P_LGSR)as P_LGSR ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1 
									AND P__PROPERTY.STATUS NOT IN (9,5,6) 
									AND P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)
									AND P__PROPERTY.FUELTYPE = 1
									AND AS_JOURNAL.IsCurrent = 1
									AND AS_JOURNAL.StatusId = 3'
									
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)

END




