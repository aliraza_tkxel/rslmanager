/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	15 May 2014
--  Description:	Added 'TOIL Recorded' and 'Time Off in Lieu' in E_NATURE
--					
 '==============================================================================*/

UPDATE	E_NATURE
SET		DESCRIPTION = 'TOIL Recorded'
		, EMPLOYEEONLY = 0
WHERE	DESCRIPTION = 'TOIL'

INSERT	INTO [E_NATURE] ([ITEMID],[DESCRIPTION],[EMPLOYEEONLY],[APPROVALREQUIRED])
VALUES	(1,'Time Off in Lieu',1,1)
