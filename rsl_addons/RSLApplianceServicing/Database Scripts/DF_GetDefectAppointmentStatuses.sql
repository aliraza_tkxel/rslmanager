USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectAppointmentStatuses]    Script Date: 09/08/2015 19:25:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Defect Status List
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetDefectAppointmentStatuses]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT STATUSID AS StatusId, TITLE AS Title From PDR_STATUS WHERE TITLE NOT LIKE '%to be arranged%'
	ORDER BY Title ASC 
END
