-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <22/05/2013>
-- Description:	<Add isInspected Column to keep track for detector inspection.>
-- =============================================

ALTER TABLE P_Smoke_Inspection
ALTER COLUMN DateStamp smalldatetime NULL

ALTER TABLE P_Smoke_Inspection
ADD IsInspected bit NULL

ALTER TABLE P_CO2_Inspection
ALTER COLUMN DateStamp smalldatetime NULL

ALTER TABLE P_CO2_Inspection
ADD IsInspected bit NULL