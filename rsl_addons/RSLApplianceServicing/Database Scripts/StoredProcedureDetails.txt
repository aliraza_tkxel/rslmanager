----------------------------WEEK 4 ---------------

Alter Proceedure:
-----------------
AS_ScheduledAppointmentsDetail
AS_AppointmentsCalendarDetail
AS_GetUsers
AS_Status 
AS_GetAction
AS_AppointmentsToBeArranged
AS_AppointmentArranged

CreateProcedures:
-----------------

AS_GetEditUserInpectionTypeInfo
AS_GetEditUserPatchInfo
AS_GetEditUsers
AS_UpdateDevelopmentId
AS_UpdateInspectionType
AS_UpdatePatchId
AS_UpdateUserData
AS_PropertyLetters
AS_PropertyActivities

-----------------------------------------WEEK 5-------------------------------
Alter Proceedure:
-----------------
AS_GetAction
AS_AppointmentsToBeArranged
AS_AppointmentArranged
----------------
AS_Status 
AS_GetAction
AS_GetUsers

Create:
--------
AS_AddUpdateStatus
AS_AddAction
--------------
AS_AddSavedLetter
AS_AddStandardLetter
AS_GetStandardLetterById

-----------------------------------------Week 6 --------------------------------

Alter Procedure:
----------------
AS_GetUsers
AS_AppointmentsCalendarDetail
AS_AppointmentEngineerInfo
AS_AppointmentCalendarEngineerInfo
AS_Status
AS_PropertyActivities
AS_PropertyActivities
AS_GetLettersByActionId

Create Table:
-------------
Table_AS_Documents

Alter Table
-----------
ALter_Table_AS_JournalHistory
Table_AS_SavedLetters (please note that this SQL Query would first drop the table and then insert it)
Table_AS_Pages(please note that this SQL Query would first drop the table and then insert it)

Create Procedures:
----------------------
AS_GetEngineersLeaveInformation
AS_PropertyDocuments
AS_GetLettersByActionId
AS_DeleteStandardLetterTemplate
AS_GetFromUserByTeamID
AS_GetLetters
AS_GetSignOff
AS_GetTeams
AS_ScheduledAppointments
AS_UpdateStandardLetterTemplate
AS_AddSavedLetter(Run Table Script before )
AS_GetPages
AS_GetPagesByParentId
AS_SearchAppointmentDetail
AS_GetPropertyAddress
AS_SavePropertyActivity
AS_SaveUserRights
AS_GetEmployeeIdByName
AS_GetPageByEmployeeId
AS_EditStatus

-----------------------------------------Week 7 --------------------------------
Alter Procedures:
-----------------
AS_SavePropertyActivity
AS_QuickFindUsers {Sent already as fixation of Resources}
AS_AddUpdateStatus {Sent already as fixation of Resources}
AS_SearchedAppointmentDetail

Create Procedure:
-----------------
AS_SaveEditedLetter
AS_SaveDocuments
AS_MeterLocations
AS_Locations
AS_ItemByAreaId
AS_getPropertyGasInfo
AS_DeleteUserPages {Sent already as fixation of Resources}
AS_AreaByLocationId
AS_DeleteUserInspectionType 
AS_PropertyAppliances
AS_GetSavedLetterById
AS_GetCountAppointmentsArranged
AS_GetCountAppointmentsToBeArranged
AS_GetCountExpired
AS_GetCountExpiresIn56Days
AS_GetCountLegalProceedings
AS_GetCountNoEntry

Alter Table Script:
-------------------
Alter_Table_AS_SavedLetter
Alter_Table_AS_Documents


--------------------------------------WEEK 8----------------------------------------

Alter Procedures:
----------------------
AS_getPropertyGasInfo
AS_AppointmentEngineerInfo
AS_ScheduledAppointmentsDetail
AS_PropertyAppliances
AS_AppointmentArranged
AS_AppointmentsToBeArranged

Create Procedures:
------------------------
AS_SavePropertyAppliance  
AS_updatePropertyGasInfo 
AS_SaveAppointment 
AS_SaveDefect 
AS_GetPropertyAppliances 
AS_GetAppliancesDefects 
AS_GetDefects 
AS_GetFuelType 
AS_GetApplianceType 
AS_GetMake 
AS_GetFlueType 
AS_GetApplianceLocations 
AS_AddCategory 
AS_getPropertyAppliancesByApplianceId 
AS_GetDefectCategory 
AS_GetAppointmentsToBeArranged 
AS_GetExpired 
AS_GetLegalProceedings
AS_GetNoEntries

Insert Script
-------------
Insert_Script_AS_LoopCode-As_LookupType



--------------------------------------WEEK 9--------------------------------------

Create Procedures:
------------------
AS_AmendAppointment
AS_GetSelectedAppointmentInfo
AS_getItemDetails
AS_GetParametersByAreaId
AS_getPropertyDefectDetails
AS_getPropertyImages
AS_SavePhotograph
AS_AddDefectImage
AS_getDefectImages
AS_EditAction
AS_GetActionRankingByStatusId

Alter Procedures:
-----------------
AS_AppointmentArranged
AS_AppointmentsToBeArranged
AS_AppointmentEngineerInfo
AS_ScheduledAppointmentsDetail
AS_QuickFindUsers
AS_GetUsers
AS_SavePropertyAppliance
AS_GetEmployeeIdByName
AS_AddAction
AS_EditAction
AS_AppointmentsToBeArranged
AS_AppointmentArranged

Alter Tables:
-------------------
Alter_Table_PA_PROPERTY_ITEM_IMAGES


------------------------------------------Week 10--------------------------------------
Alter procedure:
---------------------
AS_AppointmentsToBeArranged
AS_GetAppointmentsToBeArranged
AS_GetExpired
AS_GetLegalProceedings
AS_GetNoEntries
Alter_Table_PA_PROPERTY_ITEM_IMAGES
AS_AddUpdateStatus
AS_EditStatus
AS_GetPropertyAppliances
AS_AppointmentCalendarEngineerInfo
AS_AppointmentEngineerInfo
AS_QuickFindUsers
AS_GetCountAppointmentsArranged
AS_SaveUser
AS_GetCountExpiresIn56Days
AS_GetCountExpired
AS_GetCountNoEntry
AS_GetCountLegalProceedings
AS_GetCountAppointmentsToBeArranged
AS_GetExpired
AS_GetAppointmentsToBeArranged
AS_getPropertyGasInfo
AS_updatePropertyGasInfo
------------------------------------------------------------Release 10------------------------------------
Create Procedure:
-------------------------
AS_getPropertyDefectImages
AS_GetCountMIData 
AS_getPropertyDefectImages
AS_GetPrintActivityDetails
AS_GetEmployeeById
AS_GetExpiredCertificateInfo
AS_GetPropertyType
AS_Get6MonthExpiry
AS_GetCountMIData
--------------------------------
AS_GetApplianceCaseDetail

Alter Procedure:
----------------------
AS_AddAction
AS_PropertyActivities
AS_PropertyAppliances
AS_AppointmentsToBeArranged
AS_AppointmentsArranged

------------------------------------------------------------Release 10 new------------------------------------

Aleter Procedure:
AS_Get6MonthExpiry
AS_GetExpired
AS_GetCountExpired
AS_AppointmentsCalendarDetail
AS_GetEngineersLeaveInformation
AS_GetNoEntries
AS_GetLegalProceedings
AS_GetExpired
AS_GetCountExpired
AS_GetAppointmentsToBeArranged
AS_PropertyAppliances
AS_SavePropertyAppliance
AS_UpdateUser
AS_AppointmentsCalendarDetail
AS_ScheduledAppointmentsDetail
AS_DeleteUser
AS_updatePropertyGasInfoAS_SaveDocuments
AS_PropertyActivities
AS_GetSavedLetterById
AS_updatePropertyGasInfo
AS_SavePhotograph
AS_UpdateUser
AS_SaveUserRights
AS_SaveUser
AS_EditAction
AS_AppointmentsArranged.sql
AS_AppointmentsToBeArranged.sql
AS_GetPropertyDocument.sql
AS_PropertyAppliances.sql
P__Property-FullTextIndexCreation.sql
AS_GetCountExpired
AS_GetExpired
AS_GetLetters
AS_GetActionByStatusId
AS_getPropertyGasInfo
AS_SaveDefect
AS_getPropertyAppliancesByApplianceId
AS_AddUpdateStatus
AS_EditStatus
AS_EditAction
AS_SaveDefect

create procedure:
AS_GetLastLogin
AS_PropertyRbRc
AS_GetPropertyDocument


Alter Table
-----------
Alter_Table_Gs_Property_Appliance
Alter_Table_AS_Documents_Add_Path

------------------------------------------------------------Bugs Fix------------------------------------

Create Procedure:
-------------------------
1- AS_GetFromTelEmailPrintLetterByEmployeeID
2- AS_GetCP12DocumentByLGSRID

Alter Procedure:
----------------------
1- AS_AppointmentsToBeArranged
2- AS_QuickFindUsers
3- AS_GetCustomerAndPropertyRbRc
4- AS_GetNoEntries
5- AS_PropertyActivities
6- AS_GetCountNoEntry
7- AS_GetPrintActivityDetails

------------------------------------------------------------UAT-----------------------------------------
Create Procedure:
-------------------------


-------------------------
Alter Procedure:

1- AS_GetExpired
2- AS_AppointmentsToBeArranged
3- AS_AppointmentsArranged
4- AS_AppointmentsCalendarDetail.sql
5- AS_AppointmentsMonthlyCalendarDetail.sql

-----------------------------------------------------------Appliances CRs Release # 2
Alter Table
-------------------------
1- Alter_Table_E__EMPLOYEE

-------------------------
Alter Procedures
------------------------
1. AS_AppointmentsCalendarDetail
2. AS_SaveUser
3. AS_ScheduledAppointmentsDetail
4. AS_GetEditUsers
5. AS_UpdateUserData

-----------------------------------------------------------Appliances CRs UAT - May 
1. AS_ScheduledAppointmentsDetail
2. AS_GetFromTelEmailPrintLetterByEmployeeID
3. AS_GetCustomerAndPropertyRbRc
4. AS_GetJointTenantsInfoByTenancyID
5. AS_AppointmentsToBeArranged

Alter Table
-----------
1. AlterTableDetector.sql
2. Alter_Tables_Add_Field_isInspected

Create Table
------------
1. co2 inspection.sql
2. Smoke Inspection.sql



-------------------------------------------------------Joint Tenant AND VOID PROPERTY
CREATE Procedure
-----------------
1.AS_GetJointTenantsInfoByTenancyID

ALTER Procedure
----------------
1. AS_AppointmentsToBeArranged
2. AS_ScheduledAppointmentsDetail
3. AS_AppointmentsBeArranged
4. AlterTableProperty2Appointment

-------------------------------------------------------Increase length of Serial Number in Appliance
Alter Table
-----------
1. ALTER_GS_PROPERTY_APPLIANCE_INCREASE_SerialNumber_LENGTH

ALTER Procedure
----------------
1. AS_SavePropertyAppliance

