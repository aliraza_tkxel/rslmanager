USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_updatePropertyGasInfo]    Script Date: 12/13/2012 16:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_updatePropertyGasInfo
--@propertyId ='A010060001',
--@issuedDate = '12/09/2012',
--@renewalDate = '12/09/2013',
--@certificateNumber = '121212',
--@meterLocationId = 1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/02/2012>
-- Description:	<Description,,Amend Gas Info against Property Id>
-- Web Page: PropertyRecord.aspx => DetailsTab.ascx
-- =============================================
ALTER PROCEDURE [dbo].[AS_updatePropertyGasInfo](
@propertyId VarChar(10),
@issuedDate SMALLDATETIME,
@renewalDate SMALLDATETIME,
@certificateNumber Varchar (100),
@meterLocationId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @checkRecordinPA_PROPERTY_ATTRIBUTES int
	Declare @checkRecordinP_LGSR int
	
	SET NOCOUNT ON;
	SELECT @checkRecordinPA_PROPERTY_ATTRIBUTES = COUNT(*) FROM PA_PROPERTY_ATTRIBUTES WHERE PA_PROPERTY_ATTRIBUTES.PROPERTYID = @propertyId
    SELECT @checkRecordinP_LGSR = COUNT(*) FROM P_LGSR WHERE P_LGSR.PROPERTYID = @propertyId
    -- Insert statements for procedure here
    if @checkRecordinPA_PROPERTY_ATTRIBUTES > 0
		BEGIN
			Update PA_PROPERTY_ATTRIBUTES SET VALUEID = @meterLocationId WHERE PROPERTYID = @propertyId
		END
	ELSE 
		BEGIN
			INSERT INTO PA_ITEM_PARAMETER(ItemId,ParameterId) 
				Values (46,63)
			INSERT INTO PA_PROPERTY_ATTRIBUTES (PROPERTYID,VALUEID,PARAMETERVALUE,ITEMPARAMID,UPDATEDON)
			   VALUES(@propertyId,@meterLocationId,'Gas',@@IDENTITY,GETDATE())
		END
	 if @checkRecordinP_LGSR > 0
		BEGIN
			Update P_LGSR SET ISSUEDATE = @issuedDate, CP12NUMBER = @certificateNumber WHERE PROPERTYID = @propertyId
		END
	ELSE 
		BEGIN
			INSERT INTO P_LGSR (PROPERTYID,ISSUEDATE,CP12NUMBER)
			VALUES (@propertyId,@issuedDate,@certificateNumber)
		END
END
