USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetCountExpired]    Script Date: 12/12/2012 12:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 31/10/2012
-- Description:	This stored procedure returns the count of all "Expired" properties
-- Usage: Dashboard
-- Exec [dbo].[AS_GetCountExpired]
--		@PatchId = 18,
--		@DevelopmentId = 1

-- Modified Date: <24 June 2013, Aamir Waheed>
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetCountExpired]
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
		-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	
	-- Start - Changes by Aamir Waheed on 24 June 2013
	
	SET @selectClause	= 'SELECT 
								COUNT(P__PROPERTY.PROPERTYID) AS Number'
	
	--SET @selectClause	= 'SELECT 
	--							COUNT(DISTINCT P_LGSR.PROPERTYID) AS Number'
								
	-- End - Changes by Aamir Waheed on 24 June 2013								
	
	SET @fromClause		= 'FROM 
								(SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR
								INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
								INNER JOIN  AS_JOURNAL ON P__PROPERTY.PROPERTYID  = AS_JOURNAL.PROPERTYID
								INNER JOIN AS_Status ON AS_STATUS.StatusId = AS_JOURNAL.STATUSID
								INNER JOIN C_TENANCY on C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
								INNER JOIN C_CUSTOMERTENANCY on C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
								INNER JOIN C_ADDRESS on C_ADDRESS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + ' DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) < 0' + CHAR(10)
	SET @whereClause = @whereClause + 'AND C_ADDRESS.ISDEFAULT = 1  															
									   AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
									   AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)' + CHAR(10)
	
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
   
END
