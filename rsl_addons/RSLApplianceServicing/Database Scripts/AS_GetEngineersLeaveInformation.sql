USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-- =============================================
Exec AS_GetEngineersLeaveInformation
	@startDate='20130101',
	@returnDate ='20130930',
	@employeeIds ='34,70,84,615'
-- Author:		<Aqib Javed>
-- Create date: <14/10/2012>
-- Description:	<This store procedure shall provide the engineer's leaves information based on appointments>
-- Webpage : WeeklyCalendar.aspx, MonthlyCalendar.aspx
-- Modified date: <5th July 2013, Aamir Waheed>
-- =============================================*/
ALTER PROCEDURE [dbo].[AS_GetEngineersLeaveInformation] @startDate DATETIME,
@returnDate DATETIME,
@employeeIds NVARCHAR(250)
AS
BEGIN
	--Getting Employee Leaves Information
	SELECT DISTINCT
		J.EMPLOYEEID										AS EMPLOYEEID
		,COALESCE(N.DESCRIPTION, A.REASON, E_ABSENCEREASON.DESCRIPTION, 'N/A')
		+ CASE
			WHEN DATEPART(HOUR, A.STARTDATE) > 0
				AND DATEPART(HOUR, A.RETURNDATE) > 0
				THEN '<br /> ' + CONVERT(NVARCHAR(5), A.STARTDATE, 108) + ' - '
					+ CONVERT(NVARCHAR(5), A.RETURNDATE, 108)
			ELSE ''
		END													AS REASON
		,CONVERT(DATE, A.STARTDATE)							AS StartDate
		,CONVERT(DATE, COALESCE(A.RETURNDATE, GETDATE()))	AS EndDate
	FROM
		E_JOURNAL J
			-- This join is added to just bring Max History Id of every Journal
			INNER JOIN
				(
					SELECT
						MAX(ABSENCEHISTORYID)	AS MaxAbsenceHistoryId
						,JOURNALID
					FROM
						E_ABSENCE
					GROUP BY
						JOURNALID
				) AS MaxAbsence ON MaxAbsence.JOURNALID = J.JOURNALID
			INNER JOIN E_ABSENCE A ON MaxAbsence.MaxAbsenceHistoryId = A.ABSENCEHISTORYID
			INNER JOIN E_STATUS ON A.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			LEFT JOIN E_ABSENCEREASON ON A.REASONID = E_ABSENCEREASON.SID
			INNER JOIN E_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID
			INNER JOIN dbo.SPLIT_STRING(@employeeIds, ',') Employees ON Employees.COLUMN1 = J.EMPLOYEEID
	WHERE
		(
		-- To filter for planned i.e annual leaves etc. where approval is needed
		(E_STATUS.ITEMSTATUSID = 5
				AND J.ITEMNATUREID >= 2)
			OR
		-- To filter for sickness leaves. where approval is not needed
		(J.ITEMNATUREID = 1
				AND E_STATUS.ITEMSTATUSID <> 20)
		)

		AND ((CONVERT(DATE,A.STARTDATE) >= CONVERT(DATE,@startDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) <= CONVERT(DATE,@returnDate))
			OR (CONVERT(DATE,A.STARTDATE) <= CONVERT(DATE,@returnDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) >= CONVERT(DATE,@startDate))
			OR (CONVERT(DATE,A.STARTDATE) <= CONVERT(DATE,@startDate)
				AND CONVERT(DATE,COALESCE(A.RETURNDATE, GETDATE())) >= CONVERT(DATE,@returnDate)))
	ORDER BY StartDate
END