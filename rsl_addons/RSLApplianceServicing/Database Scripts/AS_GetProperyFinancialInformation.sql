USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetProperyFinancialInformation]    Script Date: 07/07/2014 15:08:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetProperyFinancialInformation]
-- Author:		<Aqib Javed>
-- Create date: <18-Apr-2014>
-- Description:	<This procedure 'll get the Rent Frequency,Funding Authority, Charge and Rent Type information >
-- Web Page: FinancialTab.ascx
ALTER PROCEDURE [dbo].[AS_GetProperyFinancialInformation] 
@propertyId varchar(50)		
AS
BEGIN

	Select FUNDINGAUTHORITYID, [DESCRIPTION] FROM P_FUNDINGAUTHORITY order by [DESCRIPTION]
	Select CHARGEID, [DESCRIPTION] from P_CHARGE order by [DESCRIPTION]	
	--Add Yield Column for Ticket #6727 
	Select RENTFREQUENCY,FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,YIELD,
	Convert(varchar(20),ChargeValueDate,103) as ChargeValueDate,
	Convert(varchar(20),OMVSTDate,103) as OMVSTDate,
	Convert(varchar(20),EUVDate,103) as EUVDate,
	Convert(varchar(20),OMVDate,103) as OMVDate,
	Convert(varchar(20),InsuranceValuedate,103) as InsuranceValuedate,
	Convert(varchar(20),YieldDate,103) as YieldDate
	,MarketRent,AffordableRent,SocialRent, 
	Convert(varchar(20),MarketRentDate,103) as MarketRentDate,
	Convert(varchar(20),AffordableRentDate,103) as AffordableRentDate,
	Convert(varchar(20),SocialRentDate,103) as SocialRentDate
	from P_Financial where PropertyId=@propertyId
END