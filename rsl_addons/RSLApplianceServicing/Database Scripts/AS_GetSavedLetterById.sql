USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetSavedLetterById]    Script Date: 12/13/2012 13:05:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC [dbo].[AS_GetSavedLetterById]
--		@SavedLetterId='37'
-- Author:		Hussain Ali
-- Create date: 23/10/2012
-- Description:	This SP returns the Saved Letter and all assosiated information based on Saved Letter ID
-- Useage: savePDFLetter.aspx

-- =============================================
ALTER PROCEDURE [dbo].[AS_GetSavedLetterById]
	-- Add the parameters for the stored procedure here
	@SavedLetterId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select 
		C__CUSTOMER.TENANCY,		
		ISNULL(C__CUSTOMER.FIRSTNAME ,'')+' '+ISNULL(C__CUSTOMER.LASTNAME ,'') as TenantName,
		P__PROPERTY.HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,		
		AS_JOURNAL.PROPERTYID, 
		AS_SAVEDLETTERS.LETTERTITLE, 
		AS_SAVEDLETTERS.LETTERCONTENT,
		AS_SAVEDLETTERS.RentBalance,
		AS_SAVEDLETTERS.RentCharge,
		AS_SAVEDLETTERS.TodayDate,
		AS_LookupCode.CodeName,		
		ISNULL(E__EMPLOYEE.FIRSTNAME,'')+' '+ISNULL(E__EMPLOYEE.LASTNAME,'')as EmployeeName,
		E_Team.TEAMNAME,
		ISNULL(E_CONTACT.WORKDD,'') as WORKDD,				
		ISNULL(E_CONTACT.WORKEMAIL,'') as WORKEMAIL				
		
	From
		AS_SAVEDLETTERS
		INNER JOIN AS_LookupCode ON AS_LookupCode.LookupCodeId=AS_SAVEDLETTERS.SignOffLookupCode
		INNER JOIN E_TEAM ON E_TEAM.TEAMID=AS_SAVEDLETTERS.TEAMID
		INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = AS_SAVEDLETTERS.FromResourceId		
		INNER JOIN AS_JOURNALHISTORY ON AS_JOURNALHISTORY.JOURNALHISTORYID = AS_SAVEDLETTERS.JOURNALHISTORYID
		INNER JOIN AS_JOURNAL ON AS_JOURNAL.JOURNALID = AS_JOURNALHISTORY.JOURNALID  
		INNER JOIN C__CUSTOMER ON C__CUSTOMER.PROPERTY = AS_JOURNAL.PROPERTYID
		INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = AS_JOURNAL.PROPERTYID
		INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID
	WHERE
		AS_SAVEDLETTERS.SAVEDLETTERID = @SavedLetterId
END

