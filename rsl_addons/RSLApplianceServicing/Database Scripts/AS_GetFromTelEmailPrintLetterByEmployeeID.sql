USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--	EXEC AS_GetFromTelEmailPrintLetterByEmployeeID
		@EmployeeID = 34
-- 	Author:		Aamir Waheed
-- 	Create date: 26 March 2013
-- 	Description:	To Get Employee Direct Dial Contact and Email by Employee ID
-- 	Web Page: PrintLetter.aspx
-- =============================================*/
ALTER PROCEDURE [dbo].[AS_GetFromTelEmailPrintLetterByEmployeeID]
	
	@EmployeeID int
AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT
		ISNULL(E_CONTACT.WORKDD, 'N/A') AS FromDirectDial,		
		ISNULL(E_CONTACT.WORKEMAIL, 'N/A') AS FromEmail,
		ISNULL(E_JOBDETAILS.JOBTITLE, 'Job Title N/A') AS FromJobTitle
		
	FROM
		E__EMPLOYEE
		INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
		INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
		
	WHERE
		E__EMPLOYEE.EMPLOYEEID = @EmployeeID
		
END
