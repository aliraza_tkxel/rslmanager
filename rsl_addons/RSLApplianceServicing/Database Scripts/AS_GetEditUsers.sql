USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetEidtUsers]    Script Date: 10/19/2012 17:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetEidtUsers @employeeId = @employeeId
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01-10-2012>
-- Description:	<Description,,On Edit User page get the User info>
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetEidtUsers](
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT AS_USER.UserTypeID,ISNULL(E_JOBDETAILS.GSRENo,0) AS GSRENo , (E__EMPLOYEE.FIRSTNAME+' '+ E__EMPLOYEE.LASTNAME) AS Name , ISNULL(E__EMPLOYEE.SIGNATUREPATH,'') SignaturePath
	 FROM AS_USER INNER JOIN
	 E__EMPLOYEE on AS_USER.EmployeeId = E__EMPLOYEE.EMPLOYEEID INNER JOIN
	 E_JOBDETAILS on E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	 Where AS_USER.EMPLOYEEID = @employeeId
END
