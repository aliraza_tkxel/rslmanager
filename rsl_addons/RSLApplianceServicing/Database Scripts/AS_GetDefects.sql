-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetDefects
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/06/2012>
-- Description:	<Description,,Get Defects >
-- Web Page: PropertyRecord.aspx => Add Defect Tab
-- =============================================
CREATE PROCEDURE AS_GetDefects
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM P_DEFECTS
END
GO
