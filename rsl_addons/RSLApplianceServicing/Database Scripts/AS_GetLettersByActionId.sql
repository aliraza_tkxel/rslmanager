USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetLettersByActionId]    Script Date: 10/19/2012 21:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetLettersByStatusId @statusId = 0
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,16/10/2012>
-- Description:	<Description,,Get Letters against Action Id>
-- Web Page: PropertyRecord.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetLettersByActionId](
	-- Add the parameters for the stored procedure here
	@actionId int = -1
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	  SELECT StandardLetterId,Title from AS_StandardLetters WHERE ActionId = @actionId

END