USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetStatusHistory]    Script Date: 08/27/2015 12:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:          Raja Aneeq

-- Create date:      25/08/2015

-- Description:      Status History of Property on Property Record page

-- History:          

-- =============================================
--exec AS_GetStatusHistory 'A011000003'
ALTER PROCEDURE [dbo].[AS_GetStatusHistory]
@propertyId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
		SELECT 
		CASE (s.NEWSTATUS) 
		WHEN 1
		THEN
		'Void' 
		WHEN 2
		THEN
		'Let'
		ELSE
		'N/A'
		END
		AS STATUS,
		ISNULL(Convert(Varchar,S.PREVIOUSTIMESTAMP,103),'N/A') AS FROMDATE,
		ISNULL(Convert(Varchar,S.CTIMESTAMP,103),'OnGoing') AS TODATE,

		CASE(s.NEWSTATUS) 
		WHEN 1
		THEN
		DATEDIFF(d,S.PREVIOUSTIMESTAMP,S.CTIMESTAMP) END AS NUMBOFDAYSVOID,
		
		CASE(S.NEWSTATUS) 
		WHEN   2 
		THEN
		
	DATEDIFF(d,S.PREVIOUSTIMESTAMP,S.CTIMESTAMP)END  AS NUMBOFDAYSLET,
	ISNULL(F.RENT,0)AS RENT	
	FROM P_STATUSCHANGEHISTORY S
	LEFT JOIN P_FINANCIAL F   
	ON  F.PROPERTYID = S.PROPERTYID 
	AND 
	
	(S.PREVIOUSTIMESTAMP between F.DATERENTSET AND  F.RENTEFFECTIVE)
	AND
	(S.CTIMESTAMP between F.DATERENTSET AND  F.RENTEFFECTIVE) 
	
	WHERE  S.PROPERTYID = @propertyId--'A011000003'
	
	 ORDER BY S.PREVIOUSTIMESTAMP ASC
END
