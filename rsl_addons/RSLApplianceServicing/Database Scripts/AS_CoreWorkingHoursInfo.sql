USE [RSLBHALive]
GO


CREATE TYPE [dbo].[AS_CoreWorkingHoursInfo] AS TABLE(

	[DayId] [int] NOT NULL,
	[StartTime] [nvarchar](10) NULL,
	[EndTime] [nvarchar](10) NULL
)
GO


