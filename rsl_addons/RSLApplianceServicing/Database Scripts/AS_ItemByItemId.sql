-- =============================================      
-- Exec AS_SubItemByItemId @areaId=1      
-- Author: <Ali Raza>      
-- Create date: <17/04/2014>       
-- Web Page: PropertyRecord.aspx      
-- =============================================      
Create PROCEDURE [dbo].[AS_ItemByItemId](      
@itemId int      
)      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
 SELECT *       
 From PA_ITEM      
 Where ItemId= @itemId and IsActive = 1  
 Order BY ItemSOrder ASC      
END 