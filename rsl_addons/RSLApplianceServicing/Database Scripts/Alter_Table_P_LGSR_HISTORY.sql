-- Author:		<Behroz Sikander>
-- Create date: <18 December 2012>
-- Description:	<This script will create the new fields in P_LGSR_HISTORY>
ALTER TABLE [dbo].[P_LGSR_HISTORY] add [ReceivedOnBehalf] [varchar](20) NULL
ALTER TABLE [dbo].[P_LGSR_HISTORY] add [ReceivedDate] [smalldatetime] NULL
ALTER TABLE [dbo].[P_LGSR_HISTORY] add [TENANTHANDED] [bit] NULL
 