USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetRentHistory]    Script Date: 03/25/2015 12:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --=============================================    
-- EXEC AS_GetRentHistory  @propertyId = 'A530010002'   
--@propertyId = 'A530010002'    
-- Author:  <Ali Raza>    
-- Create date: <8-Dec-2014>   
-- Modification Details: Added filter for Asbestos Element  
-- Description: <Get Property current rent Tab Information>    
-- WebPage: PropertyRecord.aspx > Document Tab    
-- =============================================    
ALTER PROCEDURE [dbo].[AS_GetRentHistory]    
 @propertyId varchar(50),    
 @pageSize int = 30,    
 @pageNumber int = 1,    
 @totalCount int = 0 output    
     
AS    
BEGIN    
    
 DECLARE @SelectClause varchar(2000),    
        @fromClause   varchar(3000),    
        @whereClause  varchar(3000),             
        @orderClause  varchar(200),     
        @mainSelectQuery varchar(MAX),            
        @rowNumberQuery varchar(MAX),    
        @finalQuery varchar(MAX),    
        @sortColumn varchar(100) = 'SID ', 
        @sortOrder varchar (5) = 'DESC',    
        -- used to add in conditions in WhereClause based on search criteria provided    
        @searchCriteria varchar(1500),  
        @sortRecord varchar(150) = ' Order By SID Desc',  
            
        --variables for paging    
        @offset int,    
  @limit int    
  --Paging Formula    
  SET @offset = 1 + (@pageNumber - 1) * @pageSize    
  SET @limit = (@offset + @pageSize) - 1     
    
  --========================================================================================             
  -- Begin building SELECT clause    
  -- Insert statements for procedure here    
    
  SET @selectClause = 'SELECT DATERENTSET as RentSet,
                              TOTALRENT as TotalRent,
                              C_TENANCYTYPE.[DESCRIPTION] as Type,
                              PFTIMESTAMP as Updated,
                              ISNULL( LEFT(e.FIRSTNAME, 1) + '' '' + e.LASTNAME,''-'' ) AS [By],
                              SID,
                              RENTEFFECTIVE,
                              RENT as [Rent],
                              SERVICES as [SERVICES],
                              COUNCILTAX as [COUNCILTAX],
                              WATERRATES as [WATERRATES],
                              INELIGSERV as [INELIGSERV],
                              SUPPORTEDSERVICES as [SUPPORTEDSERVICES],
                              GARAGE as [GARAGE], 
                              ISNULL( e.FIRSTNAME + '' '' + e.LASTNAME,''-'' ) AS [ByFullName],
                              RENTFREQUENCY as RentFrequency,
                              fre.[DESCRIPTION] as RentFrequencyDesc
                              
                              '
    
  -- End building SELECT clause    
  --========================================================================================            
    
    
  --========================================================================================        
  -- Begin building FROM clause    
  SET @fromClause = CHAR(10) + ' FROM P_FINANCIAL_HISTORY
					INNER JOIN C_TENANCYTYPE ON TENANCYTYPEID = RENTTYPE 
					LEFT JOIN E__EMPLOYEE e ON e.EMPLOYEEID = PFUSERID 
					LEFT JOIN P_RENTFREQUENCY fre ON fre.FID = RENTFREQUENCY 
					'    
  -- End building From clause    
  --========================================================================================    
      
  --========================================================================================    
  -- Begin building SearchCriteria clause    
  -- These conditions will be added into where clause based on search criteria provided    
    
  SET @searchCriteria = ' IsValuation = 0 '   
  SET @searchCriteria += ' And PropertyId =''' + @propertyId +''''     
    
  -- End building SearchCriteria clause       
  --========================================================================================    
    
  --========================================================================================    
  -- Begin building WHERE clause    
    
  -- This Where clause contains subquery to exclude already displayed records         
     
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria    
    
  -- End building WHERE clause    
  --========================================================================================    
    
  --========================================================================================        
  -- Begin building OrderBy clause      
    
  -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias    
    
  SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder    
    
  -- End building OrderBy clause    
  --========================================================================================    
    
  --========================================================================================    
  -- Begin building the main select Query    
    
  SET @mainSelectQuery = @selectClause + @fromClause + @whereClause --+ @orderClause    
     
  -- End building the main select Query    
  --========================================================================================                                       
    
  --========================================================================================    
  -- Begin building the row number query    
    
  SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row     
          FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'    
    
  -- End building the row number query    
  --========================================================================================    
    
  --========================================================================================    
  -- Begin building the final query     
    
  SET @finalQuery = ' SELECT *    
       FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result     
       WHERE    
       Result.row between' + CHAR(10) + CONVERT(varchar(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(varchar(10), @limit+1)    
        
      
  -- End building the final query    
  --========================================================================================             
     
  --========================================================================================    
  -- Begin - Execute the Query     
  print(@finalQuery)    
  EXEC (@finalQuery+@sortRecord)    
  --EXEC(@mainSelectQuery)    
                             
  -- End - Execute the Query     
  --========================================================================================             
      
  --========================================================================================    
  -- Begin building Count Query     
      
  Declare @selectCount nvarchar(2000),     
  @parameterDef NVARCHAR(500)    
    
  SET @parameterDef = '@totalCount int OUTPUT';    
  SET @selectCount = 'SELECT  @totalCount = COUNT(PropertyId) ' + @fromClause + @whereClause    
    
  --print @selectCount    
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;    
        
  -- End building the Count Query    
  --========================================================================================    
    
END