SELECT * FROM AS_Pages

DECLARE @ParentPageId INT = (SELECT Pageid FROM AS_Pages WHERE PageName = 'Reports')

IF NOT EXISTS (SELECT Pageid FROM AS_Pages WHERE PageName = 'Issued Certificates')
BEGIN
	INSERT INTO AS_Pages 
		VALUES	('Issued Certificates', 1, @ParentPageId)
END

IF NOT EXISTS (SELECT Pageid FROM AS_Pages WHERE PageName = 'Print Certificates')
BEGIN
	INSERT INTO AS_Pages 
		VALUES	('Print Certificates', 1, @ParentPageId)
END

SELECT * FROM AS_Pages