SET IDENTITY_INSERT [dbo].[P_ServicingType] ON
INSERT [dbo].[P_ServicingType] ([ServicingTypeID], [Description]) VALUES (1, N'Gas')
INSERT [dbo].[P_ServicingType] ([ServicingTypeID], [Description]) VALUES (2, N'Oil')
INSERT [dbo].[P_ServicingType] ([ServicingTypeID], [Description]) VALUES (3, N'M&E Servicing')
INSERT [dbo].[P_ServicingType] ([ServicingTypeID], [Description]) VALUES (4, N'PAT Testing')
SET IDENTITY_INSERT [dbo].[P_ServicingType] OFF