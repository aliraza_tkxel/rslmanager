USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveItemNotes]    Script Date: 11/01/2013 11:32:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	AS_SaveItemNotes
--		@propertyId = N'A010060001',
--		@itemId = 1,
--		@note = N'New',
--		@createdBy = 1
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,,10/30/2013>
-- Description:	<Description,,Save the Note for property and Item>
-- WebPage: PropertyRecord.aspx => Attributes Tab
-- =============================================
Create PROCEDURE [dbo].[AS_SaveItemNotes] (
@propertyId varchar(500),
@itemId int,
@note varchar(500),
@createdBy int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO PA_PROPERTY_ITEM_NOTES 
	([PROPERTYID],[ItemId],[Notes] ,[CreatedOn],[CreatedBy])
	VALUES
	(@propertyId,@itemId,@note ,GETDATE(),@createdBy)
END
