USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetExpired]    Script Date: 12/28/2012 16:52:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Modified By:		<Noor Muhammad>
-- Create date: 31/10/2012
-- Modified date: <12/11/2012>
-- Description:	This stored procedure returns the "Expired" properties
-- Usage: Dashboard
-- Exec [dbo].[AS_GetExpired]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetExpired]
	@PatchId		int,
	@DevelopmentId	int,
	-- parameters for sorting and paging		
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Address',
	@sortOrder varchar (5) = 'ASC',
	@totalCount int=0 output
AS
BEGIN

	declare @offset int
	declare @limit int

	set @offset = 1+(@pageNumber-1) * @pageSize
	set @limit = (@offset + @pageSize)-1	

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@orderClause  varchar(100),	
			@whereClause	varchar(8000),			
			@mainSelectQuery varchar(5000),        
			@rowNumberQuery varchar(5500),
			@finalQuery varchar(6000)         
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT top ('+convert(varchar(10),@limit)+') 
							P__PROPERTY.PROPERTYID,
							ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '', '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') as Address,
							P__PROPERTY.HOUSENUMBER as HOUSENUMBER,
							P__PROPERTY.ADDRESS1 as ADDRESS1,
							P__PROPERTY.ADDRESS2 as ADDRESS2,
							CONVERT(VARCHAR(10),DATEADD(YEAR,1,P_LGSR.ISSUEDATE),103) AS TEL,
							CONVERT(VARCHAR(10),DATEADD(YEAR,1,P_LGSR.ISSUEDATE),102) AS ExpiryDateSort,
							AS_Status.Title as StatusTitle '
	
	SET @fromClause		= 'FROM 								
								(SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR
								INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
								INNER JOIN  AS_JOURNAL ON P__PROPERTY.PROPERTYID  = AS_JOURNAL.PROPERTYID
								INNER JOIN AS_Status ON AS_STATUS.StatusId = AS_JOURNAL.STATUSID
								INNER JOIN C_TENANCY on C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
								INNER JOIN C_CUSTOMERTENANCY on C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
								INNER JOIN C_ADDRESS on C_ADDRESS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	SET @whereClause = @whereClause + 'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) < 0' + CHAR(10)	
	SET @whereClause = @whereClause + 'AND C_ADDRESS.ISDEFAULT = 1  															
									   AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
									   AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)' + CHAR(10)
	--SET @whereClause = @whereClause + 'AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE > GETDATE())' + CHAR(10)
	--SET @whereClause = @whereClause + 'AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE> GETDATE())' + CHAR(10)
	
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 	
 	
 	--========================================================================================    
	-- Begin building OrderBy clause		

	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
	SET @sortColumn = CHAR(10)+ 'Address2, HouseNumber'		
	END		


	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
	--========================================================================================
	-- Begin building the main select Query

	Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause

	-- End building the main select Query
	--========================================================================================			
	
	
	--========================================================================================
	-- Begin building the row number query

	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
							FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

	-- End building the row number query
	--========================================================================================

	--========================================================================================
	-- Begin building the final query 

	Set @finalQuery  =' SELECT *
						FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
						WHERE
						Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

	-- End building the final query
	--========================================================================================									

	--========================================================================================
	-- Begin - Execute the Query 
	print(@finalQuery)
	EXEC (@finalQuery)																									
	-- End - Execute the Query 
	--========================================================================================									

	--========================================================================================
	-- Begin building Count Query 

	Declare @selectCount nvarchar(2000), 
	@parameterDef NVARCHAR(500)

	SET @parameterDef = '@totalCount int OUTPUT';
	SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

	--print @selectCount
	EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		
	-- End building the Count Query
	--========================================================================================	 	 	
	
	
   
END
