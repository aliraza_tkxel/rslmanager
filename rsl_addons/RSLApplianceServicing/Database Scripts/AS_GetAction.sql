USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetAction]    Script Date: 11/20/2012 20:41:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetAction
-- Author:		<Salman Nazir>
-- Create date: <23/09/2012>
-- Description:	<This Stored Proceedure gets Actions on Status Page >
-- Web Page: Status.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetAction]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ActionId ,StatusId  ,Title, Ranking,IsEditable
	FROM AS_Action
END
