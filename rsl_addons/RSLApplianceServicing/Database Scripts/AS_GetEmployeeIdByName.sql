USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetEmployeeIdByName]    Script Date: 11/20/2012 19:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetEmployeeIdByName 	@firstName = 'Billy', @lastName = 'Newton'
-- Author:		<Salman Nazir>
-- Create date: <20/09/2012>
-- Description:	<This Stored Proceedure get the Employee Id>
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetEmployeeIdByName](
@firstName varchar(100),
@lastName varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.EMPLOYEEID,ISNULL(E_JOBDETAILS.GSRENo,0) AS GSRENo
	FROM E__EMPLOYEE INNER JOIN
			E_JOBDETAILS on E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	Where ltrim(rtrim(FIRSTNAME)) = ltrim(rtrim(@firstName)) AND ltrim(rtrim(LASTNAME)) = ltrim(rtrim(@lastName))
END
