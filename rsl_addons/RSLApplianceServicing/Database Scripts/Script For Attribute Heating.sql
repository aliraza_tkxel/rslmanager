BEGIN TRANSACTION
	BEGIN TRY  
Update PA_ITEM SET ItemName='Appliances' Where ItemName='Appliance Servicing'
Update PA_ITEM SET ItemName='Heating & Water Supply' Where ItemName='Hot Water Supply'

UPDATE PA_PARAMETER Set IsActive=0 where ParameterName = 'Secondary heating fuel'
UPDATE PA_PARAMETER Set IsActive=0 where ParameterName = 'Heating System'
Declare @parameterId int
select @parameterId =ParameterID from PA_PARAMETER where ParameterName = 'Emitter Type'

INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail, Sorder, IsActive)Values(@parameterId,'HVAC',6,1)
INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail, Sorder, IsActive)Values(@parameterId,'Warm Air',7,1)


-- Internal > Services > Heating
declare @itemid int;
declare @paramid int;
SELECT @itemid = ItemID from PA_ITEM where ItemName = 'Heating'
  
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Serial No','String','TextBox',0,5,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('GC Number','String','TextBox',0,6,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Boiler Location','String','TextBox',0,7,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)


insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Original Install Date','Date','Date',1,8,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Landlord Appliance','String','Checkboxes',0,9,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values

(@paramid,'',0,1)

insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Flue Type','String','Dropdown',0,10,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'FL',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'OF',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'RS',2,1)

Update PA_ITEM_PARAMETER set ItemId = @itemid where ParameterId =(Select ParameterId from PA_PARAMETER where ParameterName = 'Meter Location')
Update PA_PARAMETER SET ParameterSorder = 10 Where  ParameterName = 'Meter Location' 
--And ItemId = (Select ItemId From PA_ITEM where ItemName LIKE '%Appliance%')
Update PA_Parameter SET ParameterName='CP12 Issued' where ParameterName='Certificate Issued'
Update PA_Parameter SET ParameterName='CP12 Renewal' where ParameterName='Certificate Renewal'

Update PA_ITEM_PARAMETER set ItemId = @itemid, IsActive=0 where ParameterId =(Select ParameterId from PA_PARAMETER where ParameterName = 'Certificate Issued')
And ItemId = (Select ItemId From PA_ITEM where ItemName LIKE '%Appliance%')
Update PA_ITEM_PARAMETER set ItemId = @itemid,IsActive=0 where ParameterId =(Select ParameterId from PA_PARAMETER where ParameterName = 'Certificate Renewal')
And ItemId = (Select ItemId From PA_ITEM where ItemName LIKE '%Appliance%')

Update PA_PARAMETER set ParameterName = 'Boiler Condition Rating' where ParameterID = (select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER --where ParameterId=133
inner join PA_ITem on PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId 
inner join PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID  
where ItemName  LIKE '%Appliance%' And ParameterName = 'Condition Rating')

Update PA_ITEM_PARAMETER SET ItemId = @itemid
WHERE ItemParamID = (select PA_ITEM_PARAMETER.ItemParamID from PA_ITEM_PARAMETER 
inner join PA_ITem on PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId 
inner join PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID  
where ItemName  LIKE '%Appliance%' And ParameterName LIKE '%Condition Rating%' )


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 