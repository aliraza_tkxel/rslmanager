-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetActionRankingByStatusId @statusId = 1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/20/2012>
-- Description:	<Description,,get Action ranking against StatusId>
-- WebPage: Status.aspx
-- =============================================
CREATE PROCEDURE AS_GetActionRankingByStatusId(
@statusId int
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ActionId ,StatusId  ,Title, Ranking,IsEditable
	FROM AS_Action
	WHERE StatusId = @statusId
END
GO
