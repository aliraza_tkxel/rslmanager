
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_Locations
-- Author:		<Salman Nazir>
-- Create date: <25/10/2012>
-- Description:	<Get All locations for Attributes Tree>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE AS_Locations
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	From PA_LOCATION
END
GO
