USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PLANNEDCOMPONENTTRADE]    Script Date: 05/12/2014 12:06:15 ******/
CREATE TYPE [dbo].[AS_OutOfHoursInfo] AS TABLE(

	[OutOfHoursId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[StartTime] [nvarchar](10) NULL,
	[EndTime] [nvarchar](10) NULL,
	[GeneralStartDate] [nvarchar](100) NULL,
	[GeneralEndDate] [nvarchar](100) NULL
)
GO


