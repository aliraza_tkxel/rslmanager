-- =============================================  
--EXEC AS_SaveItemNotes  
--  @propertyId = N'A010060001',  
--  @itemId = 1,  
--  @note = N'New',  
--  @createdBy = 1  
-- Author:  <Author,,Ali Raza>  
-- Create date: <Create Date,,10/30/2013>  
-- Description: <Description,,Save the Note for property and Item>  
-- WebPage: PropertyRecord.aspx => Attributes Tab  
-- =============================================  
Alter PROCEDURE [dbo].[AS_SaveItemNotes] (  
@propertyId varchar(500),  
@schemeId int=null,   
@blockId int=null,   
@itemId int,  
@note varchar(500),
@showInScheduling bit=1,
@showOnApp bit=0,  
@createdBy int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 INSERT INTO PA_PROPERTY_ITEM_NOTES   
 ([PROPERTYID],[ItemId],[Notes] ,[CreatedOn],[CreatedBy],SchemeId,BlockId,ShowInScheduling,ShowOnApp)  
 VALUES  
 (@propertyId,@itemId,@note ,GETDATE(),@createdBy,@schemeId,@blockId,@showInScheduling,@showOnApp)  
END  