/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	21-4-2014
--  Description:	isActive is used to populate ACTIVE Appliance Type
--					
 '==============================================================================*/

ALTER TABLE GS_PROPERTY_APPLIANCE ADD LASTREPLACED SMALLDATETIME NULL