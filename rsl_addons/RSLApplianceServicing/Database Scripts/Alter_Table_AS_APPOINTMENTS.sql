-- Author:		<Behroz Sikander>
-- Create date: <18 December 2012>
-- Description:	<This script will create the new fields in AS_APPOINTMENTS>
ALTER TABLE [dbo].[AS_APPOINTMENTS]	add [APPOINTMENTALERT] [varchar](100) NULL
ALTER TABLE [dbo].[AS_APPOINTMENTS]	add [APPOINTMENTCALENDER] [varchar](100) NULL
ALTER TABLE [dbo].[AS_APPOINTMENTS]	add [SURVEYOURSTATUS] [varchar](50) NULL
ALTER TABLE [dbo].[AS_APPOINTMENTS]	add [APPOINTMENTSTATUS] [varchar](50) NULL
ALTER TABLE [dbo].[AS_APPOINTMENTS] add [SURVEYTYPE] [varchar](50) NULL
