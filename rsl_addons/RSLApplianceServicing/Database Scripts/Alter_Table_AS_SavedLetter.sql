-- Author:		<Noor Muhammad>
-- Create date: <03 Oct 2012>
-- Description:	<This script will create the new fields in as_savedLetters>
alter table AS_SavedLetters add RentBalance float null
alter table AS_SavedLetters add RentCharge float null
alter table AS_SavedLetters add TodayDate date null
alter table AS_SavedLetters add CreatedDate datetime null
alter table AS_SavedLetters add ModifiedDate datetime null