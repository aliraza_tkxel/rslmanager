/* =================================================================================    
    Page Description: Property Summary Page => Accomodation.
 
    Author: Muhammad
    Creation Date: Dec-13-2014

    Change History:

    Version		Date			By					Description
    =======     ============    ========            ==========================
    v1.0         Dec-13-2014    Aamir Waheed        Added datebase stored procedure to get proprety Rooms Dimensions
  =================================================================================*/

CREATE PROCEDURE P_GetRoomsDimensionsByPropertyId
	@propertyId NVARCHAR(40) = ''
AS
BEGIN
	SELECT
		PPD.PropertyId
		,ISNULL(PPD.RoomName,'N/A') AS RoomName
		,ISNULL(PPD.RoomWidth,0) AS RoomWidth	
		,ISNULL(PPD.RoomLength,0) AS RoomLength
		,ISNULL(PPD.RoomWidth,0) * ISNULL(RoomLength,0) AS Area
	FROM PA_PropertyDimension AS PPD	
	WHERE PropertyId = @propertyId
	ORDER BY RoomName ASC
END