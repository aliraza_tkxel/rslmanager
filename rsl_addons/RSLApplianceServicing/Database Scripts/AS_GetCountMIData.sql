SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec [dbo].[AS_GetCountMIData]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- Author:		Hussain Ali
-- Create date: 28/11/2012
-- Description:	This SP returns the MI data for the dashboard
-- Webpage: dashboard.aspx

-- =============================================
CREATE PROCEDURE [dbo].[AS_GetCountMIData]
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	-- Declaring the variables to be used in this query
	DECLARE @propertiesQuery	varchar(8000),
			@boilerQuery		varchar(8000),
			@noCertificates		varchar(8000),
			@dueIn8To12Weeks	varchar(8000),
			@whereClause		varchar(8000),
			@query				varchar(8000)	        

	-- Initializing the variables
	SET @whereClause		= CHAR(10)
	SET @propertiesQuery	= CHAR(10)
	SET @boilerQuery		= CHAR(10)
	SET @noCertificates		= CHAR(10)
	SET @dueIn8To12Weeks	= CHAR(10)
	
	-- Setting the where clause to cater for the patch and development id
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END

	-- Initializing the sub query to fetch the count of all properties
	SET @propertiesQuery = 'Select
								COUNT(DISTINCT P__PROPERTY.PROPERTYID) AS Properties
							FROM
								P__PROPERTY
							WHERE
								1 = 1' + CHAR(10)+ @whereClause 
	
	-- Initializing the sub query to fetch the count of all boilers
	SET @boilerQuery = 'Select
							COUNT(DISTINCT P__PROPERTY.PROPERTYID) AS Properties
						FROM
							P__PROPERTY
							INNER JOIN P_PROPERTYTYPE on P_PROPERTYTYPE.PROPERTYTYPEID = P__PROPERTY.PROPERTYTYPE
						WHERE
							P_PROPERTYTYPE.DESCRIPTION = ''Boiler''' + CHAR(10)+ @whereClause 
							
	-- Initializing the sub query to fetch the count of all properties with no certificates
	SET @noCertificates = 'Select
								COUNT(DISTINCT P__PROPERTY.PROPERTYID) AS Properties
							FROM
								P__PROPERTY 								
							WHERE
								P__PROPERTY.PROPERTYID NOT IN (SELECT PROPERTYID from P_LGSR)' + CHAR(10)+ @whereClause 						
								
	-- Initializing the sub query to fetch the count of all properties with certificates due to expire in 8 to 12 weeks
	--SET @dueIn8To12Weeks = 'Select
	--							COUNT(DISTINCT P__PROPERTY.PROPERTYID) AS Properties
	--						FROM
	--							P__PROPERTY, (SELECT PROPERTYID,P_LGSR.ISSUEDATE from P_LGSR)as P_LGSR							
	--						WHERE
	--							P__PROPERTY.PROPERTYID NOT IN (SELECT PROPERTYID from P_LGSR)
	--							AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 56
	--							AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 84' + CHAR(10)+ @whereClause 													
	SET @dueIn8To12Weeks = 'SELECT 
								COUNT(DISTINCT P_LGSR.PROPERTYID) AS Number
							FROM
								(SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR
								INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
								INNER JOIN AS_JOURNAL on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
								INNER JOIN AS_Status on AS_JOURNAL.STATUSID = AS_Status.StatusId
								INNER JOIN C_TENANCY on C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
								INNER JOIN C_CUSTOMERTENANCY on C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
								INNER JOIN C_ADDRESS on C_ADDRESS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
							WHERE
								DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) >= 56
								AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 84
								AND C_ADDRESS.ISDEFAULT = 1  															
								AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
								AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)' + CHAR(10)+ @whereClause 													
	
	
	-- Building the Query	
	SET @query = 'Select (' + @propertiesQuery + ') as Properties, (' + @boilerQuery + ') as Boilers, (' + @dueIn8To12Weeks + ') as NoCertificates, (' + @dueIn8To12Weeks + ') as DueIn8To12Weeks'
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
END
GO
