
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_PatchLocation 
-- Author:		<Author,Salman Nazir>
-- Create date: <Create Date,,26/09/2012>
-- Description:	<Description,,This Stored Proceedure fetch the Patch Location and shows in the drop down on Add User Page>
-- WebPage: Resources.aspx
-- =============================================
ALTER PROCEDURE AS_PatchLocation 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From E_PATCH
END
GO
