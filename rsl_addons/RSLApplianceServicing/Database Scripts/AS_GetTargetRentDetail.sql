USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyRentType]    Script Date: 03/25/2015 11:27:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
--EXEC [dbo].[AS_GetTargetRentDetail]  
-- @propertyId='BHA0000018'  
-- Author:  <Ali Raza>  
-- Create date: <25-Mar-2015>  
-- Description: <This procedure 'll get the target Rent detail and Associate Financial information >  
-- Web Page: TargetRentTab.ascx  
Create PROCEDURE [dbo].[AS_GetTargetRentDetail]   
 @propertyId varchar(100)    
AS  
BEGIN  
 Select TENANCYTYPEID, [DESCRIPTION] from C_TENANCYTYPE order by TENANCYTYPEID  
 Select * from P_FINANCIAL_TARGET where propertyId=@propertyId  
 Select FID, [DESCRIPTION] from P_RENTFREQUENCY order by [DESCRIPTION]
END