-- Author:		<Noor Muhammad>
-- Create date: <01 Oct 2012>
-- Description:	<This script will create the new table for documents>

USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[AS_Documents]    Script Date: 10/16/2012 11:44:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AS_Documents]') AND type in (N'U'))
DROP TABLE [dbo].[AS_Documents]
GO

/****** Object:  Table [dbo].[AS_Documents]    Script Date: 10/16/2012 11:45:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AS_Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [nvarchar](50) NULL,
	[JournalHistoryId] [int] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_AS_Documents] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


