UPDATE PA_PARAMETER Set IsActive=0 Where ParameterName='Heating'
UPDATE PA_PARAMETER Set IsActive=0 Where ParameterName='Bedrooms'
UPDATE PA_PARAMETER Set ShowInAccomodation=1 where ParameterID=(SELECT PA_ITEM_PARAMETER.ParameterId from PA_ITEM_PARAMETER
									 INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
									 INNER JOIN PA_ITEM on PA_ITEM_PARAMETER.ItemId= PA_ITEM.ItemID
									 Where ParameterName='Quantity' And ItemName='Bedrooms')

UPDATE PA_PARAMETER Set ShowInAccomodation=1 where ParameterName='Heating Fuel'
UPDATE PA_PARAMETER Set ShowInAccomodation=1 where ParameterName='Heating Type'
UPDATE PA_PARAMETER Set ShowInAccomodation=1 where ParameterName='Hot Water Source'


