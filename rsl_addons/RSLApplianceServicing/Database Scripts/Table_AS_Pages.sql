--Salman Nazir 
--Date 10/22/2012
-- Description: Alter the table AS_Pages and add field ParentPageId 
-- Web Page: Access.aspx 

/****** Object:  ForeignKey [FK_AS_User_Pages_AS_Pages]    Script Date: 10/22/2012 18:27:20 ******/
ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_Pages]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_User]    Script Date: 10/22/2012 18:27:20 ******/
ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_User]
GO
/****** Object:  Table [dbo].[AS_User_Pages]    Script Date: 10/22/2012 18:27:20 ******/
--ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_Pages]
GO
--ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_User]
GO
DROP TABLE [dbo].[AS_User_Pages]
GO
/****** Object:  Table [dbo].[AS_Pages]    Script Date: 10/22/2012 18:27:19 ******/
DROP TABLE [dbo].[AS_Pages]
GO
/****** Object:  Table [dbo].[AS_Pages]    Script Date: 10/22/2012 18:27:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_Pages](
	[PageId] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[ParentPageId] [int] NULL,
 CONSTRAINT [PK_AS_Pages] PRIMARY KEY CLUSTERED 
(
	[PageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_Pages] ON
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (1, N'Dashboard', 1, 0)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (2, N'Resources', 1, 0)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (3, N'Scheduling', 1, 0)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (4, N'Reports', 1, 0)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (5, N'Calendar', 1, 0)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (6, N'Users', 1, 2)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (7, N'Status', 1, 2)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (8, N'Letter', 1, 2)
INSERT [dbo].[AS_Pages] ([PageId], [PageName], [IsActive], [ParentPageId]) VALUES (9, N'Access', 1, 2)
SET IDENTITY_INSERT [dbo].[AS_Pages] OFF
/****** Object:  Table [dbo].[AS_User_Pages]    Script Date: 10/22/2012 18:27:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_User_Pages](
	[UserPagesId] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AS_User_Pages] PRIMARY KEY CLUSTERED 
(
	[UserPagesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_Pages]    Script Date: 10/22/2012 18:27:20 ******/
ALTER TABLE [dbo].[AS_User_Pages]  WITH CHECK ADD  CONSTRAINT [FK_AS_User_Pages_AS_Pages] FOREIGN KEY([PageId])
REFERENCES [dbo].[AS_Pages] ([PageId])
GO
ALTER TABLE [dbo].[AS_User_Pages] CHECK CONSTRAINT [FK_AS_User_Pages_AS_Pages]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_User]    Script Date: 10/22/2012 18:27:20 ******/
ALTER TABLE [dbo].[AS_User_Pages]  WITH CHECK ADD  CONSTRAINT [FK_AS_User_Pages_AS_User] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_User_Pages] CHECK CONSTRAINT [FK_AS_User_Pages_AS_User]
GO
