USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePropertyFinancialInformation]    Script Date: 07/07/2014 15:07:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_SavePropertyFinancialInformation
 --   @rentFrequency = 1,
	--@nominatingBody='Aqib Javed',
	--@fundingAuthority =2,
	--@charge =1,
	--@chargeValue=12.00,
	--@OMVSt=11.00,
	--@EUV=10.00,
	--@insuranceValue=9.00,
	--@OMV=8.00,
	--@propertyId='BHA0000020',
	--@userId=605
-- Author:		<Aqib Javed>
-- Create date: <25-4-2014>
-- Description:	<This store procedure will save the information of Asbestos and Risk>
-- Web Page: FinancialTab.ascx
-- =============================================
ALTER PROCEDURE [dbo].[AS_SavePropertyFinancialInformation]
	
	@nominatingBody varchar(150),
	@fundingAuthority int,
	@charge int,
	@chargeValue Money,
	@OMVSt Money,
	@EUV Money,
	@insuranceValue Money,
	@OMV Money,
	@propertyId varchar(100),
	@userId int,
	@Yield Money,
	@ChargeValueDate datetime=null,
	@OMVSTDate datetime=null,
	@EUVDate datetime=null,
	@OMVDate datetime=null,
	@InsuranceValuedate datetime=null,
	@YieldDate datetime=null,
	@MarketRent Money,
	@AffordableRent Money,
	@SocialRent Money,
	@MarketRentDate datetime=null,
	@AffordableRentDate datetime=null,
	@SocialRentDate datetime=null
	
AS
BEGIN
Declare @isRecordExist int
Select @isRecordExist =count(PROPERTYID)  from P_Financial where PROPERTYID =@propertyId
if (@isRecordExist<=0)
Begin
     Insert into P_FINANCIAL(FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,PROPERTYID,PFUSERID,YIELD,ChargeValueDate,
     OMVSTDate,EUVDate,OMVDate,InsuranceValuedate,YieldDate,AffordableRent,MarketRent,SocialRent,AffordableRentDate,MarketRentDate,SocialRentDate)
     Values(@fundingAuthority,@nominatingBody,@insuranceValue,@chargeValue,@OMVSt,@EUV,@OMV,@charge,@propertyId,@userId,@Yield,@ChargeValueDate,
     @OMVSTDate,@EUVDate,@OMVDate,@InsuranceValuedate,@YieldDate,@AffordableRent,@MarketRent,@SocialRent,@AffordableRentDate,@MarketRentDate,@SocialRentDate)     
End            
Else
 Begin
     Update P_FINANCIAL set 
           
            FUNDINGAUTHORITY=@fundingAuthority,
            NOMINATINGBODY=@nominatingBody,
            INSURANCEVALUE=@insuranceValue,
			CHARGEVALUE=@chargeValue,
			OMVST=@OMVSt,
			EUV=@EUV,
			OMV=@OMV,
			CHARGE=@charge,
		    PFUSERID=@userId,
		    Yield = @Yield,
		    ChargeValueDate=@ChargeValueDate,
		    OMVSTDate=@OMVSTDate,
		    EUVDate=@EUVDate,
		    OMVDate=@OMVDate,
		    InsuranceValuedate=@InsuranceValuedate,
		    YieldDate=@YieldDate
		    ,AffordableRent = @AffordableRent
		    ,MarketRent = @MarketRent
		    ,SocialRent = @SocialRent
		    ,AffordableRentDate = @AffordableRentDate
		    ,MarketRentDate = @MarketRentDate
		    ,SocialRentDate = @SocialRentDate
			where PROPERTYID=@propertyId
End  

Insert into P_FINANCIAL_HISTORY(FUNDINGAUTHORITY,NOMINATINGBODY,INSURANCEVALUE,CHARGEVALUE,OMVST,EUV,OMV,CHARGE,PROPERTYID,PFUSERID,YIELD,IsValuation,
ChargeValueDate,OMVSTDate,EUVDate,OMVDate,InsuranceValuedate,YieldDate,AffordableRent,MarketRent,SocialRent,AffordableRentDate,MarketRentDate,SocialRentDate)
     Values(@fundingAuthority,@nominatingBody,@insuranceValue,@chargeValue,@OMVSt,@EUV,@OMV,@charge,@propertyId,@userId,@Yield,1,
     @ChargeValueDate,@OMVSTDate,@EUVDate,@OMVDate,@InsuranceValuedate,@YieldDate,@AffordableRent,@MarketRent,@SocialRent,@AffordableRentDate,@MarketRentDate,@SocialRentDate
     )       
                
END	