-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@documentName nvarchar(50),
--		@documentPath nvarchar(100)

--EXEC	@return_value = [dbo].[AS_GetPropertyDocument]
--		@documentId = 13,
--		@journalHistoryId = 57,
--		@documentName = @documentName OUTPUT,
--		@documentPath = @documentPath OUTPUT

--SELECT	@documentName as N'@documentName',
--		@documentPath as N'@documentPath'

--SELECT	'Return Value' = @return_value

-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,13-Dec-2012>
-- Description:	<Description,,This procdure will get the document against property id and document id>
-- =============================================
CREATE PROCEDURE AS_GetPropertyDocument(
	@documentId int,
	@journalHistoryId int,
	@documentName nvarchar(50) output,
	@documentPath nvarchar(100) output		
)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @documentName = DocumentName,@documentPath = DocumentPath 
	From AS_Documents 
	WHERE AS_Documents.DocumentId = @documentId
	AND  AS_Documents.JournalHistoryId = @journalHistoryId
END
GO
