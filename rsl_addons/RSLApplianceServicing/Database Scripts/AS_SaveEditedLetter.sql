USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveEditedLetter]    Script Date: 10/23/2012 17:00:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[AS_SaveEditedLetter]
--		@journalHistoryId = 12,
--		@title = N'Letter',
--		@body = N'Body',
--		@standardLetterId = 1,
--		@teamId = 1,
--		@resourceId = 1,
--		@signOffId = 1,
--		@rentBalance = 0,
--		@rentCharge = 0,
--		@todayDate = NULL

--SELECT	'Return Value' = @return_value

-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,, 23 Oct, 2012>
-- Description:	<Description,, This stored procedure will save the reccord in as_savedletter with journalhistory id>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveEditedLetter] 
	-- Add the parameters for the stored procedure here
	@journalHistoryId int,
	@title varchar(max),
	@body nvarchar(MAX),
	@standardLetterId int,
	@teamId int,
	@resourceId int,
	@signOffId int,		
	@rentBalance float,
	@rentCharge float,
	@todayDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [RSLBHALive].[dbo].[AS_SAVEDLETTERS]
           ([JOURNALHISTORYID]
           ,[LETTERCONTENT]
           ,[LETTERID]
           ,[LETTERTITLE]
           ,[TEAMID]
           ,[FromResourceId]
           ,[SignOffLookupCode]
           ,[RentBalance]
           ,[RentCharge]
           ,[TodayDate]
           ,[CreatedDate]
           ,[ModifiedDate]
           )
     VALUES
           (@journalHistoryId 
           ,@body 
           ,@standardLetterId
           ,@title 
           ,@teamId
           ,@resourceId
           ,@signOffId 
           ,@rentBalance
           ,@rentCharge
           ,@todayDate
           ,CURRENT_TIMESTAMP 
           ,CURRENT_TIMESTAMP 
           )
END
