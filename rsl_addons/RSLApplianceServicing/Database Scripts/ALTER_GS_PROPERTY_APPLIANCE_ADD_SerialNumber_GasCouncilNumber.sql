-- =============================================
-- Author:		<Aamir Waheed>
-- Create date: <08/05/2013>
-- Description:	<Add new fields SerialNumber and GasCouncilNumber >
-- Web Page: PropertyRecord.aspx
-- Modified : <Aamir Waheed, 08/May/2013>
-- =============================================

ALTER TABLE GS_PROPERTY_APPLIANCE
ADD SerialNumber NVARCHAR(10) NULL
	,GasCouncilNumber NVARCHAR(10) NULL 
