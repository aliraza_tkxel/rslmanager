-- Author:		<Noor Muhammad>
-- Create date: <23 Oct 2012>
-- Description:	<This script will change the fields type in as_documents>

ALTER TABLE dbo.AS_Documents
ALTER COLUMN CreatedDate datetime

ALTER TABLE dbo.AS_Documents
ALTER COLUMN ModifiedDate datetime