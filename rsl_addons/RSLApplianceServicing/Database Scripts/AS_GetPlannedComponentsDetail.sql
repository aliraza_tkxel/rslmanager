USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetPlannedComponentsDetail]
--		@propertyId = N'A010060001'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,, 4 August, 2014>
-- Description:	<Description,,This procedure 'll get the Planned Components Detail >
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetPlannedComponentsDetail] 
	@propertyId as varchar(20)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT	PC.COMPONENTNAME AS Component
			,ISNULL(CONVERT(VARCHAR(10),YEAR(PID.LastDone)),'-') AS LastReplaced
			,ISNULL(CONVERT(VARCHAR(10),YEAR(PID.DueDate)),'-') AS Due
	FROM	PLANNED_COMPONENT AS PC
			LEFT JOIN ( SELECT	PLANNED_COMPONENTID,MAX(SID) AS SUB_SID
						FROM	PA_PROPERTY_ITEM_DATES
						WHERE   PROPERTYID = @propertyId
						GROUP BY PLANNED_COMPONENTID )  AS PID_SUB ON PC.COMPONENTID = PID_SUB.PLANNED_COMPONENTID
			LEFT JOIN	PA_PROPERTY_ITEM_DATES PID ON PID_SUB.SUB_SID = PID.SID
	ORDER BY  PC.SOrder  asc
	
	
END
