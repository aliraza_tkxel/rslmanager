/*
	Author: Aamir Waheed
	Date: 2nd Feb 2015
*/

--======================================================================
/*
 * Set values in this section to select parameter
 *  and insert value detail
 */
--======================================================================
-- Variable to select parameter(s) by name. (use % to find one or more characters at start, at end or in between phrase)
DECLARE @ParameterName VARCHAR(50) = '%Condition Rating'

-- Variables to set value details to insert
DECLARE @ValueDetail VARCHAR(50) = 'Not Applicable'
DECLARE @SortOrder INT = 0 -- Set @InsertAtEnd to 0 if you want to insert the value at a specfic sort order.
DECLARE @InsertAtEnd BIT = 1 -- Set @InsertAtEnd to 1 if you want to insert a value at end of list in sort order.
DECLARE @IsActive BIT = 1

--======================================================================

DECLARE @ParameterId INT = 0
DECLARE ParametersCursor Cursor FOR SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName LIKE @ParameterName
OPEN ParametersCursor

FETCH NEXT FROM ParametersCursor INTO @ParameterID
WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'ParameterId: ' + CONVERT(NVARCHAR,@ParameterId)
	IF @InsertAtEnd = 1
	BEGIN
		SELECT @SortOrder = MAX(Sorder) FROM PA_PARAMETER_VALUE WHERE ParameterID = @ParameterId
		SELECT @SortOrder = ISNULL(@SortOrder,0) + 1
	END
	ELSE
	BEGIN
		UPDATE PA_PARAMETER_VALUE SET
			Sorder = CASE WHEN Sorder > Sorder + 1 THEN Sorder ELSE Sorder + 1 END
		WHERE ParameterID = @ParameterId AND Sorder >= @SortOrder
	END
	-- Insert New values for the parameter.
	IF NOT EXISTS (SELECT ValueID FROM PA_PARAMETER_VALUE WHERE ParameterID = @ParameterId AND ValueDetail = @ValueDetail)
	BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
		VALUES (@ParameterId,@ValueDetail,@SortOrder,@IsActive)
	END
	-- Update existing values for sort order and active status.
	ELSE
	BEGIN
		UPDATE PA_PARAMETER_VALUE SET
				Sorder = @SortOrder
				,IsActive = @IsActive
		WHERE ParameterID = @ParameterId AND ValueDetail = @ValueDetail
	END

FETCH NEXT FROM ParametersCursor INTO @ParameterID
END

CLOSE ParametersCursor
DEALLOCATE ParametersCursor