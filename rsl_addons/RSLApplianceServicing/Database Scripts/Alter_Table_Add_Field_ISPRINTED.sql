-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <26/08/2013>
-- Description:	<Add ISPRINTED Column to P_LGSR keep track of printed CP12 documents.>
-- =============================================

ALTER TABLE P_LGSR
ADD ISPRINTED bit NULL