USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetAttribute]    Script Date: 12/03/2015 16:27:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 03/12/2015
-- Description:		 property attribute notes 
-- History:          03/12/2015 Ali : Get the Property attribute notes to display on Fuel scheduling popup.

-- =============================================
/*    Execution Command:
    
    Exec AS_GetAttributeNotesByPropertyID 'BHA0000727'
  =================================================================================*/
ALTER PROCEDURE [dbo].[AS_GetAttributeNotesByPropertyID]
	-- Add the parameters for the stored procedure here
	@propertyId varchar(20) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select  A.AreaName + ISNULL(' : '+ParentItem.ItemName,'')+ ISNULL(' : '+I.ItemName,'') as AttributeName
		,N.Notes as AttributeNotes
	from 
		PA_PROPERTY_ITEM_NOTES N
		INNER JOIN P__PROPERTY P on N.PropertyId=P.PropertyId
		Inner Join PA_ITEM I ON N.ItemId=I.ItemId  And I.IsActive=1
		OUTER APPLY (Select PItem.ItemName from PA_ITEM PItem 
		Inner Join PA_ITEM Item ON PItem.ItemId=Item.ParentItemId  And Item.IsActive=1
		where Item.ItemId = I.ItemId
		) AS ParentItem
		INNER JOIN PA_AREA A ON I.AreaId=A.AreaId
where P.PropertyID=@propertyId AND N.ShowInScheduling=1

END
