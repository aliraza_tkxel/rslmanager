SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Trigger Description: Add a history entry when a meter is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */

CREATE TRIGGER dbo.TR_P_METER_INSERT_HISTORY_ENTRY
   ON  dbo.P_METER
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
	INSERT INTO P_METER_HISTORY (
					MeterId
					,PropertyId
					,MeterTypeId
					,Location
					,Manufacturer
					,SerialNumber
					,Installed
					,Reading
					,ReadingDate
					,Passed
					,Notes
					,IsDisconnected
					,CTimeStamp
				)
		SELECT
			MeterId
			,PropertyId
			,MeterTypeId
			,Location
			,Manufacturer
			,SerialNumber
			,Installed
			,Reading
			,ReadingDate
			,Passed
			,Notes
			,IsDisconnected
			,@CTimeStamp
		FROM
			INSERTED

END
GO