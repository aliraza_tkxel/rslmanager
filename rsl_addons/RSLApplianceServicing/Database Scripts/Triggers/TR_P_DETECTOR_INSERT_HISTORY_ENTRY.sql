SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Trigger Description: Add a history entry then a detector is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */

CREATE TRIGGER dbo.TR_P_DETECTOR_INSERT_HISTORY_ENTRY
   ON  dbo.P_DETECTOR
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
INSERT INTO P_DETECTOR_HISTORY (
				DetectorId
				,PropertyId
				,DetectorTypeId
				,Location
				,Manufacturer
				,SerialNumber
				,PowerSource
				,InstalledDate
				,InstalledBy
				,IsLandlordsDetector
				,TestedDate
				,TestedBy
				,BatteryReplaced
				,Passed
				,Notes
				,CTimeStamp
			)
	SELECT
		DetectorId
		,PropertyId
		,DetectorTypeId
		,Location
		,Manufacturer
		,SerialNumber
		,PowerSource
		,InstalledDate
		,InstalledBy
		,IsLandlordsDetector
		,TestedDate
		,TestedBy
		,BatteryReplaced
		,Passed
		,Notes
		,@CTimeStamp
	FROM
		INSERTED

END
GO