/* =================================================================================    
    Trigger Description: Add a history entry when a meter is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */
CREATE TRIGGER dbo.TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY
   ON  dbo.P_PROPERTY_APPLIANCE_DEFECTS
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
	INSERT INTO dbo.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY (
					PropertyDefectId
					,PropertyId
					,CategoryId
					,JournalId
					,IsDefectIdentified
					,DefectNotes
					,IsActionTaken
					,ActionNotes
					,IsWarningIssued
					,ApplianceId
					,SerialNumber
					,IsWarningFixed
					,DefectDate
					,CreatedBy
					,DetectorTypeId
					,PhotoNotes
					,GasCouncilNumber
					,IsDisconnected
					,IsPartsrequired
					,IsPartsOrdered
					,PartsOrderedBy
					,PartsDue
					,PartsDescription
					,PartsLocation
					,IsTwoPersonsJob
					,ReasonFor2ndPerson
					,Duration
					,Priority
					,TradeId
					,IsCustomerHaveHeating
					,IsHeatersLeft
					,NumberOfHeatersLeft
					,IsCustomerHaveHotWater
					,DefectJobSheetStatus
					,ApplianceDefectAppointmentJournalId
					,NoEntryNotes
					,CancelNotes
					,CreatedDate
					,ModifiedBy
					,ModifiedDate
					,CTimeStamp
				)
		SELECT
			PropertyDefectId
			,PropertyId
			,CategoryId
			,JournalId
			,IsDefectIdentified
			,DefectNotes
			,IsActionTaken
			,ActionNotes
			,IsWarningIssued
			,ApplianceId
			,SerialNumber
			,IsWarningFixed
			,DefectDate
			,CreatedBy
			,DetectorTypeId
			,PhotoNotes
			,GasCouncilNumber
			,IsDisconnected
			,IsPartsrequired
			,IsPartsOrdered
			,PartsOrderedBy
			,PartsDue
			,PartsDescription
			,PartsLocation
			,IsTwoPersonsJob
			,ReasonFor2ndPerson
			,Duration
			,Priority
			,TradeId
			,IsCustomerHaveHeating
			,IsHeatersLeft
			,NumberOfHeatersLeft
			,IsCustomerHaveHotWater
			,DefectJobSheetStatus
			,ApplianceDefectAppointmentJournalId
			,NoEntryNotes
			,CancelNotes
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,@CTimeStamp
		FROM
			INSERTED
END
GO