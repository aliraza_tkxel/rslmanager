-- =============================================  
-- Exec AS_Locations  
-- Author:  <Muhammad Awais>  
-- Create date: <04-Apr-2014>  
-- Description: <Get All models>  
-- Web Page: Appliance.ascx  
-- =============================================  
Alter PROCEDURE [dbo].[AS_GetModelsList]  
(@prefix varchar(100) )  

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT ModelID as id, Model as title   
 From GS_ApplianceModel  where Model like'%'+@prefix+'%'  
END  