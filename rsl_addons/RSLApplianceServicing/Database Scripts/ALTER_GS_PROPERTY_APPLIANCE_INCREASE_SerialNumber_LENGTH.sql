-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <15/08/2013>
-- Description:	<Increase the size of characters for Serial Number.>
-- =============================================

ALTER TABLE GS_PROPERTY_APPLIANCE
ALTER COLUMN SerialNumber nvarchar(50) NULL