-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,16-Jan-2015>
-- Description:	<Description,,NoEntry Counts on ME Dashboard >
-- EXEC PDR_GetCountNoEntry -1,-1
-- =============================================
CREATE PROCEDURE PDR_GetCountNoEntry
@schemeId int =-1,
@blockId int =-1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @statusId int
	
	Select @statusId = STATUSID from PDR_STATUS Where TITLE = 'NoEntry'

Select COUNT(A.APPOINTMENTID) from 
			PDR_APPOINTMENT_HISTORY A
			INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
			INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
			INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID
			
			Where S.STATUSID = @statusId
			OR MS.SchemeId = @schemeId
			OR MS.BlockId = @blockId
	
END
GO
