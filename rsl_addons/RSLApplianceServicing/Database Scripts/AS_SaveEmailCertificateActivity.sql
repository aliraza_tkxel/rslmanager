USE [RSLBHALive1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =============================================
EXEC	[AS_SaveEmailCertificateActivity]
		@propertyId = N'A010060001',
		@createdBy = 615,
		@createDate = '2013-09-01'
		
-- Author:		<Author,,Aamir Waheed>
-- Create date: <Create Date,,01/09/2013>
-- Description:	<Description,,Save Activity against property when a CP12 is emailed >
-- Web Page: PrintCertificates.aspx
-- =============================================*/
ALTER PROCEDURE [dbo].[AS_SaveEmailCertificateActivity]
	@propertyId varchar(50),
	@createdBy int,	
	@createDate date
	
AS
BEGIN
	
	SET NOCOUNT ON;			
   	--Update the Journal
   	DECLARE @inspectionTypeId int,
   			@statusId int,
			@actionId int,
			@notes nvarchar(1000) = NULL,		
			@isLetterAttached bit = 0,
			@isDocumentAttached bit = 0,
			@journalHistoryID INT
			
	
			
	SELECT TOP 1 @inspectionTypeId = INSPECTIONTYPEID, @statusId = STATUSID FROM AS_JOURNALHISTORY
	WHERE PROPERTYID = @propertyId
	ORDER BY JOURNALHISTORYID DESC
	
	IF EXISTS (SELECT ActionId FROM AS_Action WHERE Title = 'Certificate Emailed' AND StatusId = @statusId)
	BEGIN
		SELECT @actionId = ActionId FROM AS_Action WHERE Title = 'Certificate Emailed' AND StatusId = @statusId
	END
	ELSE
	BEGIN
		DECLARE @StatusName NVARCHAR(100) 
		SELECT @StatusName = AS_STATUS.Title FROM AS_Status WHERE StatusId = @statusId
		DECLARE @ErrorMsg NVARCHAR(2048) =  'The action "Certificate Emailed" do not exists for Status "' + @StatusName 
								+ '", kindy add an action as "Certificate Emailed" for status "' + @StatusName  + '", in Resources area.'
								+ ' The record entry for "Certificate Emailed" is not added to property activities. '
		RAISERROR (@ErrorMsg, 11,1);
		RETURN
	END
	
	EXEC [dbo].[AS_SavePropertyActivity] @propertyID, @createdBy, @inspectionTypeId, @createDate, @statusId, @actionId, NULL, 0, 0, @journalHistoryID
	
END