USE [RSLBHALive1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =============================================
EXEC	[AS_SavePrintCertificateActivity]
		@propertyId = N'A010060001',
		@createdBy = 615,
		@createDate = '2013-09-01'
		
-- Author:		<Author,,Aamir Waheed>
-- Create date: <Create Date,,01/09/2013>
-- Description:	<Description,,Get tenants agains a property to email certificate.>
-- Web Page: PrintCertificates.aspx
-- =============================================*/
CREATE PROCEDURE [dbo].[AS_GetTenantsByPropertyId]
	@propertyId varchar(50)	
AS
BEGIN
	
	SET NOCOUNT ON;			
   	--Update the Journal
   
   SELECT C.CUSTOMERID AS CustomerId,
		ISNULL(NULLIF(ISNULL(G_TITLE.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '')
			 + ISNULL(' ' + C.MIDDLENAME, '') + ISNULL(' ' + C.LASTNAME, ''), ''), 'N/A') AS CustomerName,
		NULLIF(CA.EMAIL, '') AS Email
	FROM C_TENANCY T
		INNER JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID AND (T.ENDDATE IS NULL OR T.ENDDATE >= GETDATE())
			AND (CT.ENDDATE IS NULL OR CT.ENDDATE >= GETDATE())
		INNER JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID
		INNER JOIN G_TITLE ON C.TITLE = G_TITLE.TITLEID
		INNER JOIN C_ADDRESS CA ON C.CUSTOMERID = CA.CUSTOMERID AND CA.ISDEFAULT = 1
	WHERE T.PROPERTYID = @propertyId    
	
END