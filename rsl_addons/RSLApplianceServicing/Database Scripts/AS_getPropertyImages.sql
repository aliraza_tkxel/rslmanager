-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_getPropertyImages @proertyId ='A010060001' ,@itemId=1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/14/2012>
-- Description:	<Description,,Get the property Images against Item Id>
-- WebPage: PropertRecord.aspx => Attributes Tab
-- =============================================
CREATE PROCEDURE AS_getPropertyImages (
@proerptyId Varchar(100),
@itemId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM PA_PROPERTY_ITEM_IMAGES
	WHERE PROPERTYID = @proerptyId AND ItemId = @itemId
END
GO
