-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_AddCategory @categoryTitle = 'NewCategory'
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/06/2012>
-- Description:	<Description,,Add Category >
-- Web Page: PropertyRecord.aspx => Add Category PopUp
-- =============================================
CREATE PROCEDURE AS_AddCategory (
@categoryTitle varchar(500)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO P_DEFECTS_CATEGORY
	(Description)
	Values
	(@categoryTitle)
END
GO
