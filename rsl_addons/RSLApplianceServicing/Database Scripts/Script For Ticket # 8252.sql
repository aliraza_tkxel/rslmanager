BEGIN TRANSACTION 

BEGIN try 
    DECLARE @journalId  INT, 
            @propertyId NVARCHAR(150) 
    ---Declare Cursor variable   
    DECLARE @ItemToFetchCursor CURSOR 

    --Initialize cursor   
    SET @ItemToFetchCursor = CURSOR fast_forward 
    FOR SELECT journalid, 
               propertyid 
        FROM   (SELECT Max(journalid) AS journalId, 
                       Count(*)       AS TotalCount, 
                       propertyid 
                FROM   as_journal 
                WHERE  iscurrent = 1 
                GROUP  BY propertyid) AS result 
        WHERE  totalcount > 1; 

    --Open cursor   
    OPEN @ItemToFetchCursor 

    ---fetch row from cursor   
    FETCH next FROM @ItemToFetchCursor INTO @journalId, @propertyId 

    ---Iterate cursor to get record row by row   
    WHILE @@FETCH_STATUS = 0 
      BEGIN 
          UPDATE as_journal 
          SET    iscurrent = 0 
          WHERE  propertyid = @propertyId 

          UPDATE as_journal 
          SET    iscurrent = 1 
          WHERE  journalid = @journalId 

          FETCH next FROM @ItemToFetchCursor INTO @journalId, @propertyId 
      END 

    --close & deallocate cursor     
    CLOSE @ItemToFetchCursor 

    DEALLOCATE @ItemToFetchCursor 
END try 

BEGIN catch 
    IF @@TRANCOUNT > 0 
      BEGIN 
          PRINT 'Some thing went wrong with this transaction' 

          ROLLBACK TRANSACTION; 
      END 

    DECLARE @ErrorMessage NVARCHAR(4000); 
    DECLARE @ErrorSeverity INT; 
    DECLARE @ErrorState INT; 

    SELECT @ErrorMessage = Error_message(), 
           @ErrorSeverity = Error_severity(), 
           @ErrorState = Error_state(); 

    -- Use RAISERROR inside the CATCH block to return  
    -- error information about the original error that  
    -- caused execution to jump to the CATCH block. 
    RAISERROR (@ErrorMessage,-- Message text. 
               @ErrorSeverity,-- Severity. 
               @ErrorState -- State. 
    ); 
END catch 

IF @@TRANCOUNT > 0 
  BEGIN 
      PRINT 'Transaction completed successfully' 

      COMMIT TRANSACTION; 
  END 