/****** Object:  StoredProcedure [dbo].[AS_SearchedAppointmentDetail]    Script Date: 11/14/2012 18:25:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec: AS_GetSSelectedAppointmentInfo
--@appointmentId = 63
-- Author:		<Aqib Javed>
-- Create date: <10/17/2012>
-- Description:	<This store procedure shall provide the appointments details of select appointment >
-- Webpage : FuelScheduling.aspx
-- Web Control :AppointmentArranged.ascx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetSSelectedAppointmentInfo] 
@appointmentId int
AS
BEGIN
    Select Distinct AS_JOURNAL.JOURNALID, AS_JOURNAL.StatusId,AS_JOURNAL.INSPECTIONTYPEID,AS_JOURNAL.ACTIONID,AS_APPOINTMENTS.APPOINTMENTID,AS_APPOINTMENTS.APPOINTMENTDATE,AS_APPOINTMENTS.APPOINTMENTSTARTTIME,AS_APPOINTMENTS.APPOINTMENTENDTIME,AS_APPOINTMENTS.ASSIGNEDTO       from AS_APPOINTMENTS 
    INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
    WHERE AS_APPOINTMENTS.APPOINTMENTID =@appointmentId 
	 
	 
END
