-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <19/06/2013>
-- Description:	<Change CustomerId and TenancyId to nullable.>
-- =============================================

ALTER TABLE PS_Property2Appointment
ALTER COLUMN CustomerId int NULL

ALTER TABLE PS_Property2Appointment
ALTER COLUMN TenancyId int NULL