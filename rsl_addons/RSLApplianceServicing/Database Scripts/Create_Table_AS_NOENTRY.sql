USE [RSLBHALive1]
GO

/****** Object:  Table [dbo].[AS_NoEntry]    Script Date: 01/14/2013 10:53:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AS_NoEntry](
	[NoEntryID] [int] IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NOT NULL,
	[isCardLeft] [bit] NOT NULL,
	[RecordedDate] [smalldatetime] NOT NULL,
	[RecordedBy] [int] NOT NULL,
 CONSTRAINT [PK_AS_NoEntry] PRIMARY KEY CLUSTERED 
(
	[NoEntryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


