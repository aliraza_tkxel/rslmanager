USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_Get6MonthExpiry]    Script Date: 12/03/2012 15:19:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 29/11/2012
-- Description:	This SP returns the certificate exipration count in the next six months. 
-- Usage: dashboard.aspx
-- Exec [dbo].[AS_Get6MonthExpiry]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- =============================================
ALTER PROCEDURE [dbo].[AS_Get6MonthExpiry]
	-- Parameters for the stored procedure here
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Declaring the variables to be used in this query
	DECLARE @tempTableCreateQuery	varchar (8000),
			@tempTableInsertQuery	varchar	(8000),
			@tempTableSelectQuery	varchar	(8000),
			@tempQueryPartOne		varchar	(8000),
			@tempQueryPartTwo		varchar (8000),
			@whereClause			varchar	(8000),			
			@query					varchar	(8000)
	
	-- Initializing the variables
	SET @tempTableCreateQuery	= CHAR(10)
	SET @tempTableInsertQuery	= CHAR(10)
	SET @tempTableSelectQuery	= CHAR(10)
	SET @tempQueryPartOne		= CHAR(10)
	SET @tempQueryPartTwo		= CHAR(10)
	SET @whereClause			= CHAR(10)
	SET @query					= CHAR(10)
	
	-- Setting the where clause to cater for the patch and development id
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
	
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
    
    -- Initializing the counter queries (these are stored in temp varaibles
	
	SET @tempQueryPartOne = @tempQueryPartOne + 'INSERT INTO @CertificateExpiryTempTable (monName, monValue)
													(Select ' + CHAR(10)
	
	SET @tempQueryPartTwo = @tempQueryPartTwo + 'FROM
													(SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR) As P_LGSR
													INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
													INNER JOIN AS_JOURNAL on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
													INNER JOIN AS_Status on AS_JOURNAL.STATUSID = AS_Status.StatusId
													INNER JOIN C_TENANCY on C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
													INNER JOIN C_CUSTOMERTENANCY on C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
													INNER JOIN C_ADDRESS on C_ADDRESS.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID
												WHERE
												C_ADDRESS.ISDEFAULT = 1  															
												AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
												AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)' + CHAR(10) + @whereClause + CHAR(10) + CHAR(10)
    
    -- Creating the temporary table. 
    SET @tempTableCreateQuery = @tempTableCreateQuery +  'DECLARE @CertificateExpiryTempTable TABLE (monName char(3), monValue int)'
    
    -- Creating the queries to fetch the relevant counts
    
    -- First Month
    SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,0,CURRENT_TIMESTAMP),100),' + 
								-- 'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=0 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<30) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')' + CHAR(10)
								
	
	-- Second Month							
	SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,1,CURRENT_TIMESTAMP),100),' + 
								--'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=30 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<60) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')'+ CHAR(10)
    
    -- Third Month
	SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,2,CURRENT_TIMESTAMP),100),' + 
								--'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=60 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<90) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')'+ CHAR(10)

	-- Fourth Month
	SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,3,CURRENT_TIMESTAMP),100),' + 
								--'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=90 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<120) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo  + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')'+ CHAR(10)

	-- Fifth Month
	SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,4,CURRENT_TIMESTAMP),100),' + 
								--'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=120 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<150) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')'+ CHAR(10)

	-- Sixth Month
	SET @tempTableInsertQuery = @tempTableInsertQuery + @tempQueryPartOne + 
								'CONVERT(VARCHAR(3), DATEADD(mm,5,CURRENT_TIMESTAMP),100),' + 
								--'COUNT (CASE WHEN (DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))>=150 AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE))<180) THEN 1 ELSE 0 END)' + 
								'COUNT(DISTINCT P_LGSR.PROPERTYID)' + 
								@tempQueryPartTwo + ' AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 30 AND ' + 
								'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + ')'+ CHAR(10)
    
    
    -- Creating the select temprary table
    SET @tempTableSelectQuery = @tempTableSelectQuery + 'SELECT * FROM @CertificateExpiryTempTable'
    
    -- Building the Query	
	SET @query = @tempTableCreateQuery + CHAR(10) + @tempTableInsertQuery + CHAR(10) + @tempTableSelectQuery
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
    
END
