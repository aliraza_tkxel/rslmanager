USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_InspectionTypes]    Script Date: 08/05/2013 16:48:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
 --EXEC  AS_InspectionTypesByDesc
 --@description = 'Appliance Servicing'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,05/08/2013>
-- Description:	<Description,,This Stored Procedure fetches the any specific entry of p_insepection type on the basis of description.
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_InspectionTypesByDesc]
	@description varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT InspectionTypeId 
	FROM P_INSPECTIONTYPE WHERE description = @description
END
