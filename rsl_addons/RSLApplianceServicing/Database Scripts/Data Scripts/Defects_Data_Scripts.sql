USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[P_PowerSourceType]    Script Date: 07/07/2015 07:10:33 ******/
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (1, N'Mains', 1, 1)
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (2, N'Battery', 1, 2)
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (3, N'Both', 1, 3)
/****** Object:  Table [dbo].[P_MeterType]    Script Date: 07/07/2015 07:10:33 ******/
INSERT [dbo].[P_MeterType] ([MeterTypeId], [MeterType], [IsActive], [sortOrder]) VALUES (1, N'Electric Meter', 1, 1)
INSERT [dbo].[P_MeterType] ([MeterTypeId], [MeterType], [IsActive], [sortOrder]) VALUES (2, N'Gas Meter', 1, 2)
/****** Object:  Table [dbo].[P_DEFECTS_PRIORITY]    Script Date: 07/07/2015 07:10:33 ******/
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='High') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (1, N'High', 7, 1, 1, 1)
END
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='Medium') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (2, N'Medium', 24, 1, 1, 2)
END
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='Low') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (3, N'Low', 60, 1, 1, 3)
END