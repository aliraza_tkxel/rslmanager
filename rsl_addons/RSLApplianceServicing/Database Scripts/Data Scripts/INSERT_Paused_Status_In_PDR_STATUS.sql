/* =============================================================================
	Author:			Aamir Waheed
	DATE CREATED:	09 September 2015
	Description:	Script to add Paused status in PDR_STATUS
					
 '==============================================================================*/

BEGIN TRANSACTION
	BEGIN TRY
DECLARE @newStatus NCHAR(100) = 'Paused'
IF NOT EXISTS (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = @newStatus)
BEGIN
	INSERT INTO PDR_STATUS(TITLE,CREATIONDATE)
	VALUES(@newStatus,GETDATE())
END

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 BEGIN
	PRINT 'Some thing went wrong with this transaction'

	ROLLBACK TRANSACTION;
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT
		@ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
	@ErrorSeverity, -- Severity.
	@ErrorState -- State.
	);
END CATCH
  
	 
IF @@TRANCOUNT >0
BEGIN
	PRINT 'Transaction completed successfully'

	COMMIT TRANSACTION;
END