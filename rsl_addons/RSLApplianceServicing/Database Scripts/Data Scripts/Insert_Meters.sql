
/* =============================================================================
	Author:			Aamir Waheed
	DATE CREATED:	12 August 2015
	Description:	Script to add/update/move attributes for meters (Gas/ Electric)
					
 '==============================================================================*/

BEGIN TRANSACTION
	BEGIN TRY

DECLARE @ServicesAreaId INT
DECLARE @MetersItemId INT
DECLARE @GasMeterItemId INT
DECLARE @ElectricMeterItemId INT

/* Get Areaid for Services
====================================================== */
SELECT @ServicesAreaId = AreaId
FROM PA_AREA
WHERE AreaName = 'Services'

PRINT '@ServicesAreaId = ' + CONVERT(NVARCHAR, @ServicesAreaId)

/* =================================================== */

/* Insert Meters (Parent Item) or Get Id if exists
====================================================== */

SELECT @MetersItemId = ItemID
	FROM PA_ITEM
	WHERE AreaID = @ServicesAreaId
	AND ItemName = 'Meters'	

IF @MetersItemId IS NULL
BEGIN

	UPDATE PA_ITEM SET
		ItemSorder = ItemSorder + 1
	WHERE AreaID = 8
	AND ParentItemId IS NULL
	AND ItemSorder >= 1

	INSERT INTO PA_ITEM(AreaID, ItemName, ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView)
	VALUES (@ServicesAreaId, 'Meters', 1, 1, NULL, 0, 0)
	
	SELECT @MetersItemId = SCOPE_IDENTITY()
END

PRINT '@MetersItemId = ' + CONVERT(NVARCHAR, @MetersItemId)
/* =================================================== */

/* Insert Electric Meter or Get Id if exists
====================================================== */

SELECT @ElectricMeterItemId = I.ItemID FROM PA_ITEM I
	WHERE 
	I.AreaID = @ServicesAreaId
	AND I.ParentItemId = @MetersItemId
	AND I.ItemName = 'Electric'

IF @ElectricMeterItemId IS NULL
BEGIN
	INSERT INTO PA_ITEM(AreaID, ItemName, ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView)
	VALUES(@ServicesAreaId, 'Electric', 1, 1, @MetersItemId, 0, 0)
	
	SELECT @ElectricMeterItemId = SCOPE_IDENTITY()
END

/* =================================================== */

/* Insert Gas Meter or Get Id if exists
====================================================== */

SELECT @GasMeterItemId = I.ItemID FROM PA_ITEM I
	WHERE 
	I.AreaID = @ServicesAreaId
	AND I.ParentItemId = @MetersItemId
	AND I.ItemName = 'Gas'

IF @GasMeterItemId IS NULL
BEGIN
	INSERT INTO PA_ITEM(AreaID, ItemName, ItemSorder, IsActive, ParentItemId, ShowInApp, ShowListView)
	VALUES(@ServicesAreaId, 'Gas', 2, 1, @MetersItemId, 0, 0)
	
	SELECT @GasMeterItemId = SCOPE_IDENTITY()
END

/* =================================================== */

/* Insert Parameters and Related Values or Get Id if exists
	and attach it to meter item(s) e.g Gas Meter, Electic meter
====================================================== */

DECLARE @ParameterId INT
DECLARE @ItemParameterId INT
DECLARE @ParameterSortOrder INT = 0
DECLARE @HeatingItemId INT
DECLARE @ElectricsItemId INT

/* Get Heating Item Id
====================================================== */
	SELECT @HeatingItemId = ItemID
	FROM PA_ITEM
	WHERE ItemName = 'Heating'
	AND AreaID = @ServicesAreaId

/* =================================================== */

/* Get Heating Item Id
====================================================== */
	SELECT @ElectricsItemId	= ItemID
	FROM PA_ITEM
	WHERE ItemName = 'Electrics'
	AND AreaID = @ServicesAreaId

/* =================================================== */

-- Set (increase) sort order for Next Parameter
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
/* Insert Gas Meter Type or attach existing
	-- Note: (If exists) Logic is to get meter type
	 from heating and attach it tos meter item.
====================================================== */

SELECT @ParameterId = ParameterID
FROM PA_PARAMETER
WHERE ParameterName = 'Gas Meter Type'
	AND ControlType = 'Dropdown'
	AND DataType = 'String'

IF @ParameterId IS NULL
BEGIN	
	INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
	VALUES ('Meter Type','String','Dropdown',0,@ParameterSortOrder,1,1)

	SET @ParameterId = SCOPE_IDENTITY()
	
	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@GasMeterItemId,@ParameterId,1)	

	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Coriolis',0,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Diaphragm/bellows',1,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Orifice',2,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Rotary',3,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Turbine',4,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@ParameterId,'Ultrasonic flow',5,1)
		
END
ELSE
BEGIN
	
	UPDATE PA_PARAMETER
		SET ParameterName = 'Meter Type' -- Changing 'Gas Meter Type' to 'Meter Type'
		, ParameterSorder = @ParameterSortOrder
		, IsActive = 1
	WHERE ParameterID = @ParameterId
	
	UPDATE PA_ITEM_PARAMETER
		SET IsActive = 0
	WHERE ParameterId = @ParameterId
	AND ItemId = @HeatingItemId
	
	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@GasMeterItemId,@ParameterId,1)
	
END

/* =================================================== */

/* Insert Electric Meter Type or attach existing
	-- Note: (If exists) Logic is to get meter type
	 from heating and attach it to meter item.
====================================================== */

SELECT @ParameterId = ParameterID
FROM PA_PARAMETER
WHERE ParameterName = 'Electric Meter Type'
	AND ControlType = 'Dropdown'
	AND DataType = 'String'

IF @ParameterId IS NULL
BEGIN
	
	INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
	VALUES ('Meter Type','String','Dropdown',0,@ParameterSortOrder,1,1)

	SET @ParameterId = SCOPE_IDENTITY()

	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@ElectricMeterItemId,@ParameterId,1)	
	
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Standard Meter',0,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Variable Rate-Economy 7',1,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Pre-Payment',2,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Smart Meters',3,1)
	INSERT INTO PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) VALUES(@ParameterId,'Economy 10',4,1)
		
END
ELSE
BEGIN
	
	UPDATE PA_PARAMETER
		SET ParameterName = 'Meter Type' -- Changing 'Electic Meter Type' to 'Meter Type'
		, ParameterSorder = @ParameterSortOrder
		, IsActive = 1
	WHERE ParameterID = @ParameterId
	
	UPDATE PA_ITEM_PARAMETER
		SET IsActive = 0
	WHERE ParameterId = @ParameterId
	AND ItemId = @HeatingItemId
	
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @ElectricMeterItemId AND ParameterId = @ParameterId)
	BEGIN
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@ElectricMeterItemId,@ParameterId,1)
	END
	
END

/* =================================================== */

/* Insert Meter Location or attach existing
	-- Note:  (if exists) Logic is to get meter location 
	from heating and attach it to meter item.
====================================================== */

SELECT @ParameterId = P.ParameterId
FROM PA_PARAMETER P
INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
WHERE ParameterName = 'Meter Location'
	AND ControlType = 'Textbox'
	AND DataType = 'String'
	
-- Set (increase) sort order for Next Parameter
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
IF @ParameterId IS NULL
BEGIN
	
	INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
	VALUES ('Location','String','Textbox',0,@ParameterSortOrder,1,1)

	SET @ParameterId = SCOPE_IDENTITY()
	
	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@GasMeterItemId,@ParameterId,1),(@ElectricMeterItemId, @ParameterId, 1)	

END
ELSE
BEGIN
	
	UPDATE PA_PARAMETER
		SET ParameterName = 'Location' -- Changing 'Meter Location' to 'Location'
		, ParameterSorder = @ParameterSortOrder
		, IsActive = 1
	WHERE ParameterID = @ParameterId
	
	UPDATE PA_ITEM_PARAMETER
		SET IsActive = 0
	WHERE ParameterId = @ParameterId
	AND ItemId IN (@HeatingItemId, @ElectricsItemId)
		
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @GasMeterItemId AND ParameterId = @ParameterId)
	BEGIN 
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@GasMeterItemId,@ParameterId,1)
	END
	
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @ElectricMeterItemId AND ParameterId = @ParameterId)
	BEGIN
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@ElectricMeterItemId, @ParameterId, 1)
	END
		
END

/* =================================================== */

/* Insert Meter Reading or attach existing
	-- Note:  (if exists) Logic is to get meter reading 
	from heating and attach it to meter item.
====================================================== */

SELECT @ParameterId = P.ParameterID
FROM PA_PARAMETER P
INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
WHERE ParameterName = 'Gas Meter Reading'
	AND ControlType = 'Textbox'
	AND DataType = 'String'
	
-- Set (increase) sort order for Next Parameter
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
IF @ParameterId IS NULL
BEGIN
	
	INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
	VALUES ('Reading','String','Textbox',0,@ParameterSortOrder,1,1)

	SET @ParameterId = SCOPE_IDENTITY()
	
	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@GasMeterItemId,@ParameterId,1),(@ElectricMeterItemId, @ParameterId, 1)	

END
ELSE
BEGIN
	
	UPDATE PA_PARAMETER
		SET ParameterName = 'Reading' -- Changing 'Gas Meter Reading' to 'Reading'
		, ParameterSorder = @ParameterSortOrder
		, IsActive = 1
	WHERE ParameterID = @ParameterId
	
	UPDATE PA_ITEM_PARAMETER
		SET IsActive = 0
	WHERE ParameterId = @ParameterId
	AND ItemId IN (@HeatingItemId, @ElectricsItemId)
		
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @GasMeterItemId AND ParameterId = @ParameterId)
	BEGIN 
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@GasMeterItemId,@ParameterId,1)
	END
	
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @ElectricMeterItemId AND ParameterId = @ParameterId)
	BEGIN
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@ElectricMeterItemId, @ParameterId, 1)
	END

END

/* =================================================== */

/* detach (if exists) Electiric Meter From Heating
====================================================== */

SELECT @ParameterId = P.ParameterID
FROM PA_PARAMETER P
INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
WHERE ParameterName = 'Electric Meter Reading'
	AND ControlType = 'Textbox'
	AND DataType = 'String'
	
IF @ParameterId IS NOT NULL
BEGIN
	
	-- detach from heating item
	UPDATE IP
		SET IsActive = 0
	FROM PA_PARAMETER P
	INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
	WHERE P.ParameterID = @ParameterId
	
	-- set IsActive to false 'Electric Meter Reading'
	UPDATE P
		SET IsActive = 0
	FROM PA_PARAMETER P
	INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
	WHERE P.ParameterID = @ParameterId	

END

/* =================================================== */

/* Insert Meter Reading Date or attach existing
	Note: (if exists) Logic is to get meter reading date 
	from heating and attach it to meter item.
====================================================== */

SELECT @ParameterId = P.ParameterID
FROM PA_PARAMETER P
INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
WHERE ParameterName = 'Gas Meter Reading Date'
	AND ControlType = 'Date'
	AND DataType = 'Date'
	
-- Set (increase) sort order for Next Parameter
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
IF @ParameterId IS NULL
BEGIN
	
	INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
	VALUES ('Reading Date','Date','Date',1,@ParameterSortOrder,1,1)

	SET @ParameterId = SCOPE_IDENTITY()
	
	INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
	VALUES (@GasMeterItemId,@ParameterId,1),(@ElectricMeterItemId, @ParameterId, 1)	

END
ELSE
BEGIN
	
	UPDATE PA_PARAMETER
		SET ParameterName = 'Reading Date' -- Changing 'Gas Meter Reading Date' to 'Reading Date'
		, ParameterSorder = @ParameterSortOrder
		, IsActive = 1
	WHERE ParameterID = @ParameterId
	
	UPDATE PA_ITEM_PARAMETER
		SET IsActive = 0
	WHERE ParameterId = @ParameterId
	AND ItemId IN (@HeatingItemId, @ElectricsItemId)
		
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @GasMeterItemId AND ParameterId = @ParameterId)
	BEGIN 
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@GasMeterItemId,@ParameterId,1)
	END
	
	IF NOT EXISTS (SELECT * FROM PA_ITEM_PARAMETER WHERE ItemId = @ElectricMeterItemId AND ParameterId = @ParameterId)
	BEGIN
		INSERT INTO [PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
		VALUES (@ElectricMeterItemId, @ParameterId, 1)
	END

END

/* =================================================== */

/* detach (if exists) Electiric Meter From Heating
====================================================== */

SELECT @ParameterId  = P.ParameterID
FROM PA_PARAMETER P
INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
WHERE ParameterName = 'Electric Meter Reading Date'
	AND ControlType = 'Date'
	AND DataType = 'Date'
	
IF @ParameterId IS NOT NULL
BEGIN
	
	-- set IsActive to false 'Electric Meter Reading Date'
	UPDATE P
		SET IsActive = 0
	FROM PA_PARAMETER P
	INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
	WHERE P.ParameterID = @ParameterId
	
	-- detach from heating item
	UPDATE IP
		SET IsActive = 0
	FROM PA_PARAMETER P
	INNER JOIN PA_ITEM_PARAMETER IP ON P.ParameterID = IP.ParameterId AND IP.ItemId = @HeatingItemId
	WHERE P.ParameterID = @ParameterId

END

/* =================================================== */

/* Add manufacturer parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Next Parameter
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Manufacturer', 'String', 'TextBox', 0, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)

/* =================================================== */

/* Add Serial Number parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Reading Date (Meter Reading Date)
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Serial Number', 'String', 'TextBox', 0, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)

/* =================================================== */

/* Add Installed(Date) parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Reading Date (Meter Reading Date)
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Installed', 'Date', 'Date', 1, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)

/* =================================================== */

/* Add Capped parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Capped
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Capped', 'String', 'Radiobutton', 0, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)
		
INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail,Sorder, IsActive)
VALUES	(@ParameterId, 'Yes', 1, 1)
		,(@ParameterId, 'No', 2, 1)

/* =================================================== */

/* Add Last Inspected(Date) parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Last Inspected (Date)
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Last Inspected', 'Date', 'Date', 1, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)

/* =================================================== */

/* Add Passed parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Passed
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Passed', 'String', 'Radiobutton', 0, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)
		
INSERT INTO PA_PARAMETER_VALUE(ParameterID, ValueDetail,Sorder, IsActive)
VALUES	(@ParameterId, 'Yes', 1, 1)
		,(@ParameterId, 'No', 2, 1)
		
/* =================================================== */

/* Add Passed parameter and attached to meters
====================================================== */
-- Set (increase) sort order for Passed
SELECT @ParameterSortOrder = @ParameterSortOrder + 1
INSERT INTO [PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
VALUES ('Notes', 'String', 'TextArea', 0, @ParameterSortOrder,1,1)

SELECT @ParameterId = SCOPE_IDENTITY()

INSERT INTO PA_ITEM_PARAMETER (ItemId, ParameterId, IsActive)
VALUES (@GasMeterItemId, @ParameterId, 1)
		,(@ElectricMeterItemId, @ParameterId, 1)
		
/* =================================================== */


/* End meters parameter and values insert
  =================================================== */

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 BEGIN
	PRINT 'Some thing went wrong with this transaction'

	ROLLBACK TRANSACTION;
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT
		@ErrorMessage = ERROR_MESSAGE()
		,@ErrorSeverity = ERROR_SEVERITY()
		,@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
	@ErrorSeverity, -- Severity.
	@ErrorState -- State.
	);
END CATCH
  
	 
IF @@TRANCOUNT >0
BEGIN
	PRINT 'Transaction completed successfully'

	COMMIT TRANSACTION;
END