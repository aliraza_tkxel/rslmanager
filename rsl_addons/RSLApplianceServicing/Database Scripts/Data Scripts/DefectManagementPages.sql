/*
	Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Inser appliance defect management pages for navigation and access rights.
*/

/*
SELECT
	ROW_NUMBER() OVER(ORDER BY P.DESCRIPTION) As No
	,MD.DESCRIPTION	AS Module
	,MN.DESCRIPTION	AS Menu
	,PP.DESCRIPTION	AS ParentPage
	,P.DESCRIPTION	AS Page
	,P.PAGE
	,P.*
FROM
	AC_PAGES P
		LEFT JOIN AC_PAGES PP ON P.ParentPage = PP.PAGEID
		INNER JOIN AC_MENUS MN ON P.MENUID = MN.MENUID
		INNER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
WHERE 1 = 1
	AND PP.DESCRIPTION LIKE '%Scheduling%'
	AND MN.DESCRIPTION LIKE '%Servicing%'
*/

BEGIN TRANSACTION
BEGIN TRY

-- Get servicing Menu Id
DECLARE @servicingMenuId INT
SELECT  @servicingMenuId = MN.MENUID
FROM AC_MENUS MN
WHERE MN.DESCRIPTION = 'Servicing'

PRINT '@servicingMenuId: ' + CONVERT(NVARCHAR,@servicingMenuId)

-- Get Scheduling Parent Page Id
DECLARE @schedulingPageId INT
SELECT  @schedulingPageId = P.PAGEID
FROM AC_PAGES  P
WHERE P.DESCRIPTION = 'Scheduling'
AND P.MENUID = @servicingMenuId

PRINT '@schedulingPageId: ' + CONVERT(NVARCHAR,@schedulingPageId)

-- Get Reports Parent Page Id
DECLARE @reportsPageId INT
SELECT  @reportsPageId = P.PAGEID
FROM AC_PAGES P
WHERE P.DESCRIPTION = 'Reports'
AND P.MENUID = @servicingMenuId

PRINT '@reportsPageId: ' + CONVERT(NVARCHAR,@reportsPageId)
-- Insert DefetScheduling.aspx (Level-2)
IF Not EXISTS(Select 1 from AC_PAGES where DESCRIPTION='Defects')
Begin
	DECLARE @DefectPageId INT
	INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
	VALUES('Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Bridge.aspx?pg=defectScheduling', 4, 2, @schedulingPageId , '~/../RSLApplianceServicing/Views/Scheduling/DefectScheduling.aspx', 1)
	SELECT @DefectPageId = SCOPE_IDENTITY()
END

-- Insert Defect Scheduling Supporting Page (Level-3)
IF Not EXISTS(Select 1 from AC_PAGES where DESCRIPTION='Defect Basket')
Begin
	INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
	VALUES('Defect Basket', @servicingMenuId, 1, 0, '~/../RSLApplianceServicing/Views/Scheduling/DefectBasket.aspx', 1, 3, @DefectPageId , '', 0)
	,('Defect Appointments', @servicingMenuId, 1, 0, '~/../RSLApplianceServicing/Views/Scheduling/DefectAvailableAppointments.aspx', 2, 3, @DefectPageId , '', 0)
	,('Defect Job Sheet', @servicingMenuId, 1, 0, '~/../RSLApplianceServicing/Views/Scheduling/DefectJobSheetSummary.aspx', 3, 3, @DefectPageId , '', 0)
END
-- Insert Defect Report Pages (Level-2)
IF Not EXISTS(Select 1 from AC_PAGES where DESCRIPTION='Appliance Defects')
Begin
	INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
	VALUES('Appliance Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/ApplianceDefectReport.aspx', 4, 2, @reportsPageId , '', 1)
	,('Disconnected Appliance', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/DisconnectedApplianceReport.aspx', 5, 2, @reportsPageId , '', 1)
	,('Cancelled Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/CancelledDefectReport.aspx', 6, 2, @reportsPageId , '', 1)
	,('Defects History', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/DefectHistoryReport.aspx', 7, 2, @reportsPageId , '', 1)
END
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);

END CATCH