SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_SavePatchID @employeeId=161,	@patchId = 2
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,28/09/2012>
-- Description:	<Description,,Save the patch drop values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE AS_SavePatchID
	-- Add the parameters for the stored procedure here
	@employeeId int,
	@patchId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO AS_UserPatchDevelopment (EmployeeId,PatchId,IsActive) VALUES
						(@employeeId,@patchId,1)
END
GO
