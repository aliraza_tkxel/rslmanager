USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_ItemDetails]    Script Date: 11/01/2013 11:37:07 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'AS_ItemDetails' AND ss.name = N'dbo')
DROP TYPE [dbo].[AS_ItemDetails]
GO

USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_ItemDetails]    Script Date: 11/01/2013 11:37:07 ******/
CREATE TYPE [dbo].[AS_ItemDetails] AS TABLE(
	[ItemParamId] [int] NULL,
	[ParameterValue] [nvarchar](100) NULL,
	[ValueId] [int] NULL,
	[IsCheckBoxSelected] [bit] NULL
)
GO


