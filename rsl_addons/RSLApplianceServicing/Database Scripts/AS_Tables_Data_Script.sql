USE [RSLBHALive]
GO
/****** Object:  ForeignKey [FK_AS_ACTION_AS_STATUS]    Script Date: 09/19/2012 16:32:56 ******/
ALTER TABLE [dbo].[AS_Action] DROP CONSTRAINT [FK_AS_ACTION_AS_STATUS]
GO
/****** Object:  ForeignKey [FK_AS_ACTIONHistory_AS_STATUS]    Script Date: 09/19/2012 16:32:56 ******/
ALTER TABLE [dbo].[AS_ActionHistory] DROP CONSTRAINT [FK_AS_ACTIONHistory_AS_STATUS]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTS_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS] DROP CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTS_AS_JOURNALHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS] DROP CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNALHISTORY]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS] DROP CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_JOURNALHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS] DROP CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNALHISTORY]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_APPOINTMENT]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTSHISTORY] DROP CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_APPOINTMENT]
GO
/****** Object:  ForeignKey [FK_AS_JOURNAL_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNAL] DROP CONSTRAINT [FK_AS_JOURNAL_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNAL_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNAL] DROP CONSTRAINT [FK_AS_JOURNAL_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY] DROP CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_ACTIONHISTORY_ActionHistoryId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY] DROP CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTIONHISTORY_ActionHistoryId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY] DROP CONSTRAINT [FK_AS_JOURNALHISTORY_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY] DROP CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_STATUSHISTORY_StatusHistoryId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY] DROP CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUSHISTORY_StatusHistoryId]
GO
/****** Object:  ForeignKey [FK_AS_LookupCode_AM_LookupType]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_LookupCode] DROP CONSTRAINT [FK_AS_LookupCode_AM_LookupType]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters] DROP CONSTRAINT [FK_AS_StandardLetters_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_LookupCode_SignOff]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters] DROP CONSTRAINT [FK_AS_StandardLetters_AS_LookupCode_SignOff]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters] DROP CONSTRAINT [FK_AS_StandardLetters_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_USER_CreatedBy]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters] DROP CONSTRAINT [FK_AS_StandardLetters_AS_USER_CreatedBy]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_USER_ModifiedBy]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters] DROP CONSTRAINT [FK_AS_StandardLetters_AS_USER_ModifiedBy]
GO
/****** Object:  ForeignKey [FK_AS_Status_P_INSPECTIONTYPE]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_Status] DROP CONSTRAINT [FK_AS_Status_P_INSPECTIONTYPE]
GO
/****** Object:  ForeignKey [FK_AS_StatusHistory_AS_Status]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StatusHistory] DROP CONSTRAINT [FK_AS_StatusHistory_AS_Status]
GO
/****** Object:  ForeignKey [FK_AS_USER_AS_USERTYPE]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER] DROP CONSTRAINT [FK_AS_USER_AS_USERTYPE]
GO
/****** Object:  ForeignKey [FK_AS_USER_INSPECTIONTYPE_AS_USER]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE] DROP CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_AS_USER]
GO
/****** Object:  ForeignKey [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE] DROP CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_Pages]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_Pages]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_User]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_User_Pages] DROP CONSTRAINT [FK_AS_User_Pages_AS_User]
GO
/****** Object:  ForeignKey [FK_AS_UserPatchDevelopment_AS_User]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_UserPatchDevelopment] DROP CONSTRAINT [FK_AS_UserPatchDevelopment_AS_User]
GO


GO
DROP TABLE [dbo].[AS_APPOINTMENTSHISTORY]

GO



GO
DROP TABLE [dbo].[AS_APPOINTMENTS]

GO

GO



GO


GO

GO
DROP TABLE [dbo].[AS_ActionHistory]


GO
DROP TABLE [dbo].[AS_JOURNAL]

GO

GO

GO


GO


GO

GO
DROP TABLE [dbo].[AS_StatusHistory]

GO



GO



GO
DROP TABLE [dbo].[AS_UserPatchDevelopment]

GO
DROP TABLE [dbo].[AS_Action]
GO
/****** Object:  Table [dbo].[AS_LookupCode]    Script Date: 09/19/2012 16:32:57 ******/

GO
DROP TABLE [dbo].[AS_LookupCode]
GO
/****** Object:  Table [dbo].[AS_USER]    Script Date: 09/19/2012 16:32:58 ******/

GO
DROP TABLE [dbo].[AS_USER]
GO
/****** Object:  Table [dbo].[AS_Status]    Script Date: 09/19/2012 16:32:57 ******/

GO
DROP TABLE [dbo].[AS_Status]
GO
/****** Object:  Table [dbo].[AS_USERTYPE]    Script Date: 09/19/2012 16:32:58 ******/
DROP TABLE [dbo].[AS_USERTYPE]
GO
/****** Object:  Table [dbo].[AS_LookupType]    Script Date: 09/19/2012 16:32:57 ******/
DROP TABLE [dbo].[AS_LookupType]
GO
/****** Object:  Table [dbo].[AS_Pages]    Script Date: 09/19/2012 16:32:57 ******/
DROP TABLE [dbo].[AS_Pages]
GO
/****** Object:  Table [dbo].[AS_Pages]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_Pages](
	[PageId] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AS_Pages] PRIMARY KEY CLUSTERED 
(
	[PageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_LookupType]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_LookupType](
	[LookupTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](800) NOT NULL,
 CONSTRAINT [PK_AS_LookupType] PRIMARY KEY CLUSTERED 
(
	[LookupTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_USERTYPE]    Script Date: 09/19/2012 16:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_USERTYPE](
	[UserTypeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_AS_USERTYPE] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_USERTYPE] ON
INSERT [dbo].[AS_USERTYPE] ([UserTypeID], [Description]) VALUES (1, N'Manager')
INSERT [dbo].[AS_USERTYPE] ([UserTypeID], [Description]) VALUES (2, N'Scheduler')
INSERT [dbo].[AS_USERTYPE] ([UserTypeID], [Description]) VALUES (3, N'Gas Engineer')
INSERT [dbo].[AS_USERTYPE] ([UserTypeID], [Description]) VALUES (4, N'Repair Operative')
SET IDENTITY_INSERT [dbo].[AS_USERTYPE] OFF
/****** Object:  Table [dbo].[AS_Status]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Ranking] [smallint] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[InspectionTypeID] [smallint] NULL,
	[IsEditable] [bit] NULL,
 CONSTRAINT [PK_AS_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_Status] ON
INSERT [dbo].[AS_Status] ([StatusId], [Title], [Ranking], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [InspectionTypeID], [IsEditable]) VALUES (1, N'Appointment to be arranged', 1, CAST(0xA0C902A5 AS SmallDateTime), 615, NULL, NULL, 1, 0)
INSERT [dbo].[AS_Status] ([StatusId], [Title], [Ranking], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [InspectionTypeID], [IsEditable]) VALUES (2, N'Arranged', 1, CAST(0xA0C902A5 AS SmallDateTime), 615, NULL, NULL, 1, 0)
INSERT [dbo].[AS_Status] ([StatusId], [Title], [Ranking], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [InspectionTypeID], [IsEditable]) VALUES (3, N'No Entry', 1, CAST(0xA0C902A5 AS SmallDateTime), 615, NULL, NULL, 1, 0)
INSERT [dbo].[AS_Status] ([StatusId], [Title], [Ranking], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [InspectionTypeID], [IsEditable]) VALUES (4, N'Legal Proceedings', 1, CAST(0xA0C902A5 AS SmallDateTime), 615, NULL, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[AS_Status] OFF
/****** Object:  Table [dbo].[AS_USER]    Script Date: 09/19/2012 16:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_USER](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeID] [smallint] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_AS_USER] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_AS_USER_EMPLOYEEID] UNIQUE NONCLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_USER] ON
INSERT [dbo].[AS_USER] ([UserId], [UserTypeID], [EmployeeId], [IsActive], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, 615, 1, CAST(0xA0C902A5 AS SmallDateTime), 615, NULL, NULL)
SET IDENTITY_INSERT [dbo].[AS_USER] OFF
/****** Object:  Table [dbo].[AS_LookupCode]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_LookupCode](
	[LookupCodeId] [int] IDENTITY(1,1) NOT NULL,
	[LookupTypeId] [int] NOT NULL,
	[CodeName] [nvarchar](150) NOT NULL,
	[Code] [nvarchar](150) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_AS_LookupCode] PRIMARY KEY CLUSTERED 
(
	[LookupCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_Action]    Script Date: 09/19/2012 16:32:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_Action](
	[ActionId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Ranking] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[IsEditable] [bit] NULL,
 CONSTRAINT [PK_AS_Action] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_Action] ON
INSERT [dbo].[AS_Action] ([ActionId], [StatusId], [Title], [Ranking], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [IsEditable]) VALUES (2, 1, N'Letter', 1, CAST(0x0000A0C900B9F190 AS DateTime), CAST(0x0000A0C900B9F190 AS DateTime), 615, 615, 0)
SET IDENTITY_INSERT [dbo].[AS_Action] OFF
/****** Object:  Table [dbo].[AS_UserPatchDevelopment]    Script Date: 09/19/2012 16:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_UserPatchDevelopment](
	[UserPatchDevelopmentId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[PatchId] [int] NULL,
	[DevelopmentId] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AS_UserPatchDevelopment] PRIMARY KEY CLUSTERED 
(
	[UserPatchDevelopmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_User_Pages]    Script Date: 09/19/2012 16:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_User_Pages](
	[UserPagesId] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AS_User_Pages] PRIMARY KEY CLUSTERED 
(
	[UserPagesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_USER_INSPECTIONTYPE]    Script Date: 09/19/2012 16:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_USER_INSPECTIONTYPE](
	[UserInspectionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[InspectionTypeID] [smallint] NULL,
 CONSTRAINT [PK_AS_USER_INSPECTIONTYPE] PRIMARY KEY CLUSTERED 
(
	[UserInspectionTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_StatusHistory]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_StatusHistory](
	[StatusHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Ranking] [smallint] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[InspectionTypeID] [smallint] NULL,
	[IsEditable] [bit] NULL,
	[Ctimestamp] [smalldatetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_AS_StatusHistory] PRIMARY KEY CLUSTERED 
(
	[StatusHistoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_StandardLetters]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AS_StandardLetters](
	[StandardLetterId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[ActionId] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Body] [varchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[SignOffLookupCode] [int] NULL,
	[TeamId] [int] NULL,
	[FromResourceId] [int] NULL,
	[IsPrinted] [bit] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_AS_StandardLetters] PRIMARY KEY CLUSTERED 
(
	[StandardLetterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_JOURNAL](
	[JOURNALID] [int] IDENTITY(1,1) NOT NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[STATUSID] [int] NULL,
	[ACTIONID] [int] NULL,
	[INSPECTIONTYPEID] [smallint] NULL,
	[CREATIONDATE] [smalldatetime] NULL,
	[CREATEDBY] [int] NULL,
	[ISCURRENT] [bit] NULL,
 CONSTRAINT [PK_AS_JOURNAL] PRIMARY KEY CLUSTERED 
(
	[JOURNALID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AS_JOURNAL] ON
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (1, N'A010060001', 1, 2, 1, CAST(0x9F7902A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (2, N'M010040000', 1, 2, 1, CAST(0x9FCB02A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (3, N'M080340011', 1, 2, 1, CAST(0xA00802A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (4, N'F080200006', 1, 2, 1, CAST(0xA02702A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (5, N'BHA0001129', 1, 2, 1, CAST(0xA04502A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (6, N'BHA0000960', 1, 2, 1, CAST(0xA06402A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (7, N'D510250003', 1, 2, 1, CAST(0xA08202A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (8, N'C270040007', 1, 2, 1, CAST(0xA0A102A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (9, N'BHA0001657', 1, 2, 1, CAST(0xA0C002A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (10, N'C300930009', 1, 2, 1, CAST(0xA0DE02A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (11, N'E070300005', 1, 2, 1, CAST(0xA0FD02A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (12, N'C180160009', 1, 2, 1, CAST(0xA11B02A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (13, N'C01005000A', 1, 2, 1, CAST(0xA13A02A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (14, N'BHA0001092', 1, 2, 1, CAST(0xA15902A5 AS SmallDateTime), 615, 1)
INSERT [dbo].[AS_JOURNAL] ([JOURNALID], [PROPERTYID], [STATUSID], [ACTIONID], [INSPECTIONTYPEID], [CREATIONDATE], [CREATEDBY], [ISCURRENT]) VALUES (15, N'A130010008', 1, 2, 1, CAST(0xA17502A5 AS SmallDateTime), 615, 1)
SET IDENTITY_INSERT [dbo].[AS_JOURNAL] OFF
/****** Object:  Table [dbo].[AS_ActionHistory]    Script Date: 09/19/2012 16:32:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_ActionHistory](
	[ActionHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ActionId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Ranking] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[IsEditable] [bit] NULL,
	[Ctimestamp] [smalldatetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_AS_ActionHistory] PRIMARY KEY CLUSTERED 
(
	[ActionHistoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_JOURNALHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_JOURNALHISTORY](
	[JOURNALHISTORYID] [bigint] IDENTITY(1,1) NOT NULL,
	[JOURNALID] [int] NULL,
	[PROPERTYID] [nvarchar](20) NULL,
	[STATUSID] [int] NULL,
	[ACTIONID] [int] NULL,
	[INSPECTIONTYPEID] [smallint] NULL,
	[CREATIONDATE] [smalldatetime] NULL,
	[CREATEDBY] [int] NULL,
	[NOTES] [nvarchar](1000) NULL,
	[ISLETTERATTACHED] [bit] NULL,
	[StatusHistoryId] [int] NULL,
	[ActionHistoryId] [int] NULL,
 CONSTRAINT [PK_AS_JOURNALHISTORYid] PRIMARY KEY CLUSTERED 
(
	[JOURNALHISTORYID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_APPOINTMENTS]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_APPOINTMENTS](
	[APPOINTMENTID] [int] IDENTITY(1,1) NOT NULL,
	[JSGNUMBER] [nvarchar](10) NULL,
	[TENANCYID] [int] NULL,
	[JournalId] [int] NOT NULL,
	[JOURNALHISTORYID] [bigint] NOT NULL,
	[APPOINTMENTDATE] [smalldatetime] NULL,
	[APPOINTMENTSHIFT] [nvarchar](5) NULL,
	[APPOINTMENTSTARTTIME] [nvarchar](10) NULL,
	[APPOINTMENTENDTIME] [nvarchar](10) NULL,
	[ASSIGNEDTO] [int] NOT NULL,
	[CREATEDBY] [int] NULL,
	[LOGGEDDATE] [smalldatetime] NULL,
	[NOTES] [nvarchar](1000) NULL,
 CONSTRAINT [PK_AS_APPOINTMENTS] PRIMARY KEY CLUSTERED 
(
	[APPOINTMENTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AS_APPOINTMENTSHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AS_APPOINTMENTSHISTORY](
	[APPOINTMENTHISTORYID] [int] IDENTITY(1,1) NOT NULL,
	[APPOINTMENTID] [int] NOT NULL,
	[JSGNUMBER] [nvarchar](10) NULL,
	[TENANCYID] [int] NULL,
	[JournalId] [int] NOT NULL,
	[JOURNALHISTORYID] [bigint] NOT NULL,
	[APPOINTMENTDATE] [smalldatetime] NULL,
	[APPOINTMENTSHIFT] [nvarchar](5) NULL,
	[APPOINTMENTSTARTTIME] [nvarchar](10) NULL,
	[APPOINTMENTENDTIME] [nvarchar](10) NULL,
	[ASSIGNEDTO] [int] NOT NULL,
	[CREATEDBY] [int] NULL,
	[LOGGEDDATE] [smalldatetime] NULL,
	[NOTES] [nvarchar](1000) NULL,
 CONSTRAINT [PK_AS_APPOINTMENTSHISTORY] PRIMARY KEY CLUSTERED 
(
	[APPOINTMENTHISTORYID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_AS_ACTION_AS_STATUS]    Script Date: 09/19/2012 16:32:56 ******/
ALTER TABLE [dbo].[AS_Action]  WITH CHECK ADD  CONSTRAINT [FK_AS_ACTION_AS_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[AS_Status] ([StatusId])
GO
ALTER TABLE [dbo].[AS_Action] CHECK CONSTRAINT [FK_AS_ACTION_AS_STATUS]
GO
/****** Object:  ForeignKey [FK_AS_ACTIONHistory_AS_STATUS]    Script Date: 09/19/2012 16:32:56 ******/
ALTER TABLE [dbo].[AS_ActionHistory]  WITH CHECK ADD  CONSTRAINT [FK_AS_ACTIONHistory_AS_STATUS] FOREIGN KEY([ActionId])
REFERENCES [dbo].[AS_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AS_ActionHistory] CHECK CONSTRAINT [FK_AS_ACTIONHistory_AS_STATUS]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTS_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNAL] FOREIGN KEY([JournalId])
REFERENCES [dbo].[AS_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[AS_APPOINTMENTS] CHECK CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTS_AS_JOURNALHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNALHISTORY] FOREIGN KEY([JOURNALHISTORYID])
REFERENCES [dbo].[AS_JOURNALHISTORY] ([JOURNALHISTORYID])
GO
ALTER TABLE [dbo].[AS_APPOINTMENTS] CHECK CONSTRAINT [FK_AS_APPOINTMENTS_AS_JOURNALHISTORY]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNAL] FOREIGN KEY([JournalId])
REFERENCES [dbo].[AS_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[AS_APPOINTMENTS] CHECK CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_JOURNALHISTORY]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNALHISTORY] FOREIGN KEY([JOURNALHISTORYID])
REFERENCES [dbo].[AS_JOURNALHISTORY] ([JOURNALHISTORYID])
GO
ALTER TABLE [dbo].[AS_APPOINTMENTS] CHECK CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_JOURNALHISTORY]
GO
/****** Object:  ForeignKey [FK_AS_APPOINTMENTSHISTORY_AS_APPOINTMENT]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_APPOINTMENTSHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_APPOINTMENT] FOREIGN KEY([APPOINTMENTID])
REFERENCES [dbo].[AS_APPOINTMENTS] ([APPOINTMENTID])
GO
ALTER TABLE [dbo].[AS_APPOINTMENTSHISTORY] CHECK CONSTRAINT [FK_AS_APPOINTMENTSHISTORY_AS_APPOINTMENT]
GO
/****** Object:  ForeignKey [FK_AS_JOURNAL_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNAL]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNAL_AS_ACTION_ActionId] FOREIGN KEY([ACTIONID])
REFERENCES [dbo].[AS_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AS_JOURNAL] CHECK CONSTRAINT [FK_AS_JOURNAL_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNAL_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNAL]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNAL_AS_STATUS_StatusId] FOREIGN KEY([STATUSID])
REFERENCES [dbo].[AS_Status] ([StatusId])
GO
ALTER TABLE [dbo].[AS_JOURNAL] CHECK CONSTRAINT [FK_AS_JOURNAL_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTION_ActionId] FOREIGN KEY([ACTIONID])
REFERENCES [dbo].[AS_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AS_JOURNALHISTORY] CHECK CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_ACTIONHISTORY_ActionHistoryId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTIONHISTORY_ActionHistoryId] FOREIGN KEY([ActionHistoryId])
REFERENCES [dbo].[AS_ActionHistory] ([ActionHistoryId])
GO
ALTER TABLE [dbo].[AS_JOURNALHISTORY] CHECK CONSTRAINT [FK_AS_JOURNALHISTORY_AS_ACTIONHISTORY_ActionHistoryId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_JOURNAL]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNALHISTORY_AS_JOURNAL] FOREIGN KEY([JOURNALID])
REFERENCES [dbo].[AS_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[AS_JOURNALHISTORY] CHECK CONSTRAINT [FK_AS_JOURNALHISTORY_AS_JOURNAL]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUS_StatusId] FOREIGN KEY([STATUSID])
REFERENCES [dbo].[AS_Status] ([StatusId])
GO
ALTER TABLE [dbo].[AS_JOURNALHISTORY] CHECK CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_JOURNALHISTORY_AS_STATUSHISTORY_StatusHistoryId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_JOURNALHISTORY]  WITH CHECK ADD  CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUSHISTORY_StatusHistoryId] FOREIGN KEY([StatusHistoryId])
REFERENCES [dbo].[AS_StatusHistory] ([StatusHistoryId])
GO
ALTER TABLE [dbo].[AS_JOURNALHISTORY] CHECK CONSTRAINT [FK_AS_JOURNALHISTORY_AS_STATUSHISTORY_StatusHistoryId]
GO
/****** Object:  ForeignKey [FK_AS_LookupCode_AM_LookupType]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_LookupCode]  WITH CHECK ADD  CONSTRAINT [FK_AS_LookupCode_AM_LookupType] FOREIGN KEY([LookupTypeId])
REFERENCES [dbo].[AS_LookupType] ([LookupTypeId])
GO
ALTER TABLE [dbo].[AS_LookupCode] CHECK CONSTRAINT [FK_AS_LookupCode_AM_LookupType]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_ACTION_ActionId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_ACTION_ActionId] FOREIGN KEY([ActionId])
REFERENCES [dbo].[AS_Action] ([ActionId])
GO
ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_ACTION_ActionId]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_LookupCode_SignOff]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_LookupCode_SignOff] FOREIGN KEY([SignOffLookupCode])
REFERENCES [dbo].[AS_LookupCode] ([LookupCodeId])
GO
ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_LookupCode_SignOff]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_STATUS_StatusId]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_STATUS_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[AS_Status] ([StatusId])
GO
ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_STATUS_StatusId]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_USER_CreatedBy]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_USER_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_USER_CreatedBy]
GO
/****** Object:  ForeignKey [FK_AS_StandardLetters_AS_USER_ModifiedBy]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StandardLetters]  WITH CHECK ADD  CONSTRAINT [FK_AS_StandardLetters_AS_USER_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_StandardLetters] CHECK CONSTRAINT [FK_AS_StandardLetters_AS_USER_ModifiedBy]
GO
/****** Object:  ForeignKey [FK_AS_Status_P_INSPECTIONTYPE]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_Status]  WITH CHECK ADD  CONSTRAINT [FK_AS_Status_P_INSPECTIONTYPE] FOREIGN KEY([InspectionTypeID])
REFERENCES [dbo].[P_INSPECTIONTYPE] ([InspectionTypeID])
GO
ALTER TABLE [dbo].[AS_Status] CHECK CONSTRAINT [FK_AS_Status_P_INSPECTIONTYPE]
GO
/****** Object:  ForeignKey [FK_AS_StatusHistory_AS_Status]    Script Date: 09/19/2012 16:32:57 ******/
ALTER TABLE [dbo].[AS_StatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_AS_StatusHistory_AS_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[AS_Status] ([StatusId])
GO
ALTER TABLE [dbo].[AS_StatusHistory] CHECK CONSTRAINT [FK_AS_StatusHistory_AS_Status]
GO
/****** Object:  ForeignKey [FK_AS_USER_AS_USERTYPE]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER]  WITH CHECK ADD  CONSTRAINT [FK_AS_USER_AS_USERTYPE] FOREIGN KEY([UserTypeID])
REFERENCES [dbo].[AS_USERTYPE] ([UserTypeID])
GO
ALTER TABLE [dbo].[AS_USER] CHECK CONSTRAINT [FK_AS_USER_AS_USERTYPE]
GO
/****** Object:  ForeignKey [FK_AS_USER_INSPECTIONTYPE_AS_USER]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE]  WITH CHECK ADD  CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_AS_USER] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE] CHECK CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_AS_USER]
GO
/****** Object:  ForeignKey [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE]  WITH CHECK ADD  CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE] FOREIGN KEY([InspectionTypeID])
REFERENCES [dbo].[P_INSPECTIONTYPE] ([InspectionTypeID])
GO
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE] CHECK CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_Pages]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_User_Pages]  WITH CHECK ADD  CONSTRAINT [FK_AS_User_Pages_AS_Pages] FOREIGN KEY([PageId])
REFERENCES [dbo].[AS_Pages] ([PageId])
GO
ALTER TABLE [dbo].[AS_User_Pages] CHECK CONSTRAINT [FK_AS_User_Pages_AS_Pages]
GO
/****** Object:  ForeignKey [FK_AS_User_Pages_AS_User]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_User_Pages]  WITH CHECK ADD  CONSTRAINT [FK_AS_User_Pages_AS_User] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_User_Pages] CHECK CONSTRAINT [FK_AS_User_Pages_AS_User]
GO
/****** Object:  ForeignKey [FK_AS_UserPatchDevelopment_AS_User]    Script Date: 09/19/2012 16:32:58 ******/
ALTER TABLE [dbo].[AS_UserPatchDevelopment]  WITH CHECK ADD  CONSTRAINT [FK_AS_UserPatchDevelopment_AS_User] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[AS_USER] ([EmployeeId])
GO
ALTER TABLE [dbo].[AS_UserPatchDevelopment] CHECK CONSTRAINT [FK_AS_UserPatchDevelopment_AS_User]
GO
