USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetAllAppointments]    Script Date: 12/07/2015 15:40:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* EXEC AS_GetAllAppointments 'E010500010'


version				Author				Created Date			Description													Page
-------				------				------------			-----------
1.0					Ahmed Mehmood		11/30/2013				Return appointments of Stock , GAS,and Fault Locator		PropertyRecord.aspx 

1.1					Raja Aneeq			12/7/2015				Modified SP for getting appointments Defect					PropertyRecord.aspx 

*/
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetAllAppointments]
	@propertyId varchar(20)
AS
BEGIN

DECLARE		@StockSelectClause NVARCHAR(MAX),
			@StockFromClause NVARCHAR(MAX),
			@StockWhereClause NVARCHAR(MAX),
	
			@ApplianceSelectClause NVARCHAR(MAX),
			@ApplianceFromClause NVARCHAR(MAX),
			@ApplianceWhereClause NVARCHAR(MAX),
			
			@FaultSelectClause NVARCHAR(MAX),
			@FaultFromClause NVARCHAR(MAX),
			@FaultWhereClause NVARCHAR(MAX),
			
			
			@DefectSelectClause NVARCHAR(MAX),
			@DefectFromClause NVARCHAR(MAX),
			@DefectWhereClause NVARCHAR(MAX),
		
			@PlannedSelectClause NVARCHAR(MAX),
			@PlannedFromClause NVARCHAR(MAX),
			@PlannedWhereClause NVARCHAR(MAX),
		
			@unionClause varchar(100),
			@mainSelectQuery varchar(MAX),
					
			@orderClause  varchar(500),
			@sortColumn VARCHAR(300),
			@sortOrder VARCHAR(10)


	--===========================================================================================
	--							STOCK APPOINTMENTS
	--===========================================================================================
				
	SET @StockSelectClause = 'SELECT DISTINCT
		''Stock Survey'' as AppointmentType
		,CONVERT(VARCHAR(11), PS_Appointment.AppointStartDateTime, 106) as AppointmentDate
		,CONVERT(VARCHAR(5),PS_Appointment.AppointStartDateTime,108) AppointmentTime
		, PS_Appointment.AppointStartDateTime SortTime
		,PS_Appointment.AppointId AppointmentId '
						+ CHAR(10)
						
	SET @StockFromClause = 'FROM	PS_Appointment
		INNER JOIN PS_Property2Appointment ON PS_Appointment.AppointId = PS_Property2Appointment.AppointId '
						+ CHAR(10)
						

	SET @StockWhereClause = 'WHERE PS_Appointment.AppointProgStatus <> ''Finished''
		AND	PS_Property2Appointment.PropertyId  = '''+@propertyId+''''
		 
						+ CHAR(10)

		
	--===========================================================================================
	--							APPLIANCES APPOINTMENTS
	--===========================================================================================
				
	SET @ApplianceSelectClause = 'SELECT  DISTINCT	''Gas Service'' as AppointmentType
		,CONVERT(VARCHAR(11), AS_APPOINTMENTS.APPOINTMENTDATE , 106) as AppointmentDate
		,convert(char(5),cast(AS_APPOINTMENTS.APPOINTMENTSTARTTIME+'' ''+ AS_APPOINTMENTS.APPOINTMENTSHIFT as datetime),108) AppointmentTime
		,AS_APPOINTMENTS.AppointmentDate +'' ''+convert(char(5),cast(AS_APPOINTMENTS.APPOINTMENTSTARTTIME+'' ''+ AS_APPOINTMENTS.APPOINTMENTSHIFT as datetime),108) SortTime
		,AS_APPOINTMENTS.AppointmentId '
						+ CHAR(10)
						
	SET @ApplianceFromClause = 'FROM	AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID '
						+ CHAR(10)
						

	SET @ApplianceWhereClause = 'WHERE
		AS_APPOINTMENTS.APPOINTMENTSTATUS  <> ''Finished''
		AND	AS_JOURNAL.PropertyId  = '''+@propertyId+''''
						+ CHAR(10)
	
	--===========================================================================================
	--							FAULT APPOINTMENTS
	--===========================================================================================
				
	SET @FaultSelectClause = 'SELECT DISTINCT ''Fault Repair'' as AppointmentType
		,CONVERT(VARCHAR(11), FL_CO_APPOINTMENT.APPOINTMENTDATE , 106) as AppointmentDate
		,CONVERT(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentTime
		,CONVERT(DATETIME,FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108)) AS SortTime
		,FL_CO_APPOINTMENT.AppointmentId '
						+ CHAR(10)
						
	SET @FaultFromClause = 'FROM FL_CO_APPOINTMENT
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId 
		INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT_STATUS ON  FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID '
						+ CHAR(10)
						

	SET @FaultWhereClause = 'WHERE	FL_FAULT_STATUS.Description = ''Appointment Arranged''
		AND FL_FAULT_LOG.PROPERTYID  = '''+@propertyId+''''
						+ CHAR(10)
	--===========================================================================================
	--							DEFECT APPOINTMENTS
	--===========================================================================================
	
	
		SET @DefectSelectClause = 'SELECT DISTINCT  ''Defect Appointment''  AS AppointmentType	
		,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
		+ '' ''+ CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)	AS AppointmentDate																																												
		,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)AS AppointmentTime
		
		,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME	AS SortTime
		,A.APPOINTMENTID AS AppointmentId ' + CHAR(10)
						
						
	SET @DefectFromClause = '	FROM																	
	P_PROPERTY_APPLIANCE_DEFECTS AD
	INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
	INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
	INNER JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
	INNER JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
	INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
	INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
	INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
	INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
	INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID	 ' + CHAR(10)
						
	SET @DefectWhereClause =  ' WHERE ISNULL(M.isPending,1) = 0
							   AND DS.TITLE IN (''Arranged'')  AND			
							   AD.PropertyId = '''+@propertyId+'''' + CHAR(10)
	 
						
	
	
	--===========================================================================================
	--							PLANNED APPOINTMENTS
	--===========================================================================================
				
	SET @PlannedSelectClause = 'SELECT DISTINCT 
	ISNULL(PLANNED_APPOINTMENT_TYPE.Planned_Appointment_Type,''Planned'')+''-''+ 
	CASE isMiscAppointment WHEN 1    
    THEN ISNULL(PLANNED_COMPONENT.COMPONENTNAME + ''(misc)'', ''Miscellaneous'')    
    ELSE ISNULL(PLANNED_COMPONENT.COMPONENTNAME, ''N/A'')    END  as AppointmentType  
	,CONVERT(VARCHAR(11), APPOINTMENTDATE , 106)  as AppointmentDate
	,APPOINTMENTSTARTTIME	AS AppointmentTime
	,CONVERT(DATETIME,APPOINTMENTDATE +'' '' + convert(char(5),cast(APPOINTMENTSTARTTIME as datetime),108)) AS SortTime
	,PLANNED_APPOINTMENTS.appointmentid
	'
						+ CHAR(10)
						
	SET @PlannedFromClause = 'FROM
		PLANNED_APPOINTMENTS
			INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId
			INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID
			INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
			LEFT JOIN PLANNED_APPOINTMENT_TYPE ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = PLANNED_APPOINTMENT_TYPE.Planned_Appointment_TypeId  
			 LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID   '
						+ CHAR(10)
						

	SET @PlannedWhereClause = 'WHERE
		 (PLANNED_STATUS.TITLE <> ''Cancelled''
			AND APPOINTMENTSTATUS NOT IN (''Complete'', ''Cancelled'')
		)
AND	PLANNED_JOURNAL.PROPERTYID = '''+@propertyId+''''
						+ CHAR(10)
		--========================================================================================
		--Set union Clause
		SET @unionClause=CHAR(10)+CHAR(9) +'UNION ALL'+CHAR(10)+CHAR(9)  
		--========================================================================================	
		SET @sortColumn = CHAR(10)+CHAR(9) +'SortTime DESC'+CHAR(10)+CHAR(9) 
		
		--Set ORDER Clause
		SET @OrderClause = 'order by ' + @sortColumn + CHAR(10)
		--========================================================================================	

		Set @mainSelectQuery = 	@StockSelectClause + @StockFromClause + @StockWhereClause 
								+ @unionClause +
								@ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause
								+ @unionClause +
								@FaultSelectClause + @FaultFromClause + @FaultWhereClause
								+ @unionClause +
								
								@DefectSelectClause + @DefectFromClause + @DefectWhereClause
								+ @unionClause +
								 
								 @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause
								+ @OrderClause		
	
	PRINT @mainSelectQuery	
	EXEC (@mainSelectQuery)

			 
END