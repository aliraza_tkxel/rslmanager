USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetActionByStatusId]    Script Date: 12/16/2012 23:48:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Hussain Ali>
-- Create date: <08/10/2012>
-- Description:	<This Stored Proceedure gets Actions against a status id >
-- Web Page: Status.aspx
-- EXEC	[dbo].[AS_GetActionByStatusId]
--		@StatusId = 1
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetActionByStatusId]

	@StatusId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ActionId ,StatusId  ,Title, Ranking
	FROM AS_Action
	WHERE AS_Action.StatusId = @StatusId
	Order By Ranking
	
	
END
