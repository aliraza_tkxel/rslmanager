USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Ahmed Mehmood>
-- Create date: <Create Date,14/05/2014>
-- Description:	<Description, Save Core Working Hours Information>
--Last modified Date:14/05/2014
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveCoreWorkingHoursInfo]
	-- Add the parameters for the stored procedure here
	@employeeId int
	,@editedBy int
	,@coreWorkingHourInfo as AS_CoreWorkingHoursInfo readonly
	,@isSaved int = 0 out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	
	
	BEGIN TRANSACTION;
	BEGIN TRY
	
	
	CREATE TABLE #tmp_CoreWorkingHourInfo
	(
		[DayId] [int] NOT NULL,
		[StartTime] [nvarchar](10) NULL,
		[EndTime] [nvarchar](10) NULL
	)
	
	INSERT INTO #tmp_CoreWorkingHourInfo
    SELECT * FROM @coreWorkingHourInfo
	
	IF EXISTS (	SELECT	1 
				FROM	E_CORE_WORKING_HOURS
				WHERE E_CORE_WORKING_HOURS.EMPLOYEEID = @employeeId)
		BEGIN 						
			UPDATE	E_CORE_WORKING_HOURS 
			SET		E_CORE_WORKING_HOURS.STARTTIME = #tmp_CoreWorkingHourInfo.StartTime 
					,E_CORE_WORKING_HOURS.ENDTIME = #tmp_CoreWorkingHourInfo.EndTime
					,E_CORE_WORKING_HOURS.CREATEDBY = @editedBy
					,E_CORE_WORKING_HOURS.CREATEDDATE = GETDATE()
			FROM    E_CORE_WORKING_HOURS
					INNER JOIN #tmp_CoreWorkingHourInfo 
					ON E_CORE_WORKING_HOURS.EMPLOYEEID = @employeeId
					AND E_CORE_WORKING_HOURS.DAYID = #tmp_CoreWorkingHourInfo.DayId 		
		END
	
	ELSE
		BEGIN
				INSERT INTO [E_CORE_WORKING_HOURS] 
				([EMPLOYEEID],[DAYID],[STARTTIME],[ENDTIME],[CREATEDDATE],[CREATEDBY])
				SELECT	@employeeId
						, #tmp_CoreWorkingHourInfo.DayId  
						, #tmp_CoreWorkingHourInfo.StartTime 
						, #tmp_CoreWorkingHourInfo.EndTime 
						, GETDATE()
						, @editedBy
				FROM	#tmp_CoreWorkingHourInfo
		END
				

	
	
	DROP TABLE #tmp_CoreWorkingHourInfo
	   
        
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isSaved = 0		
		END
		
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		 @ErrorSeverity, -- Severity.
		 @ErrorState -- State.
		 );
	END CATCH;

	IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
	SET @isSaved = 1
	END

END