USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AmendPropertyItemDetail]    Script Date: 11/01/2013 11:19:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,10/29/2013>
-- Description:	<Description,Insert and update data in PA_PROPERTY_ATTRIBUTES and PA_PROPERTY_ITEM_DATES table>
-- =============================================
Create PROCEDURE [dbo].[AS_AmendPropertyItemDetail] 
	-- Add the parameters for the stored procedure here
	@PropertyId varchar(100)
	,@ItemId int
	,@UpdatedBy int	
	,@ItemDetail as AS_ItemDetails readonly
	,@ItemDates as AS_ItemDates readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
---Begin Iterate 	@ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES
--=================================================================================	
---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES
   DECLARE @minItemParamId INT ,@attributeId INT ,@itemParamId INT , @parameterValue nvarchar(100),@valueId INT,@isCheckBoxSelected bit,@Count INT,@ControlType nvarchar(50)
   ---Declare Cursor variable
   DECLARE @ItemToInsertCursor CURSOR
   --Initialize cursor
   Set @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT ItemParamId , ParameterValue,ValueId,IsCheckBoxSelected  FROM  @ItemDetail;
   --Open cursor
   OPEN @ItemToInsertCursor
 ---fetch row from cursor
   FETCH NEXT FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,@valueId, @isCheckBoxSelected
  ---Iterate cursor to get record row by row
		WHILE @@FETCH_STATUS = 0
		BEGIN
		--- Select Control type  of parameter		
			 SELECT @ControlType=ControlType  from PA_PARAMETER
			INNER JOIN PA_ITEM_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID 
			WHERE ItemParamID=@itemParamId
			
			IF @ControlType='TextBox'
			BEGIN
			SET @valueId=NULL
			--SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId IS NULL
			 SELECT @attributeId=ATTRIBUTEID FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId  AND ValueId IS NULL
			END
			ELSE IF @ControlType='CheckBoxes'
			BEGIN
			
			--SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId AND ValueId =@valueId
			SELECT @attributeId=ATTRIBUTEID FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId  AND ValueId =@valueId
			END
			ELSE
			BEGIN
			--SELECT  @Count=Count(ATTRIBUTEID) FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId 
			 SELECT @attributeId=ATTRIBUTEID FROM PA_PROPERTY_ATTRIBUTES WHERE PROPERTYID = @PropertyId AND ITEMPARAMID = @itemParamId  

			 END
	-- if record exist update that row otherwise insert a new record 		
	IF @attributeId > 0
	   BEGIN
	  
	    UPDATE PA_PROPERTY_ATTRIBUTES SET ITEMPARAMID= @itemParamId,PARAMETERVALUE=@parameterValue,VALUEID=@valueId,
	    UPDATEDON=GETDATE(),UPDATEDBY= @UpdatedBy, IsCheckBoxSelected= @isCheckBoxSelected WHERE ATTRIBUTEID=@attributeId        
	   END
	
	 ELSE
	   BEGIN
	   INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID,ITEMPARAMID, PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY,IsCheckBoxSelected)
	   VALUES(@PropertyId,@itemParamId,@parameterValue,@valueId,GETDATE(),@UpdatedBy, @isCheckBoxSelected)       
	   END 
		
		FETCH NEXT FROM @ItemToInsertCursor INTO @itemParamId, @parameterValue,@valueId, @isCheckBoxSelected
		END
--close & deallocate cursor		
CLOSE @ItemToInsertCursor
DEALLOCATE @ItemToInsertCursor
 

--Strart Iterate ItemDates to insert or update data in 
--=====================================================================================

---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES
   DECLARE @minParamId INT ,@sid INT ,@parameterId INT ,@lastDone date,@dueDate date
   
   SET @minParamId =(SELECT MIN(ParameterId) FROM  @ItemDates) 
 WHILE @minParamId IS NOT NULL
 BEGIN 
 SELECT @parameterId=ParameterId, @lastDone = LastDone , @dueDate  = DueDate  FROM  @ItemDates  WHERE ParameterId = @minParamId
 IF @lastDone = '' OR @lastDone IS NULL
 SET @lastDone=NULL
 IF @dueDate = '' OR @dueDate IS NULL
 SET @dueDate=NULL
 
  IF EXISTS(SELECT [SID] FROM PA_PROPERTY_ITEM_DATES WHERE PROPERTYID = @PropertyId AND ItemId=@ItemId AND ParameterId= @parameterId )   
	BEGIN

	 SELECT @sid=[SID] FROM PA_PROPERTY_ITEM_DATES WHERE PROPERTYID = @PropertyId AND ItemId=@ItemId AND ParameterId= @parameterId
	 	IF @lastDone IS NOT NULL 
	 	BEGIN
	 		 UPDATE PA_PROPERTY_ITEM_DATES SET LastDone=CONVERT(Datetime, @lastDone,120), UPDATEDON=GETDATE(), UPDATEDBY = @UpdatedBy  
	 WHERE [SID] = @sid  
	 	END
	 	IF @dueDate IS NOT NULL 
	 	BEGIN
	 		 UPDATE PA_PROPERTY_ITEM_DATES SET DueDate= CONVERT(Datetime, @dueDate,120), UPDATEDON=GETDATE(), UPDATEDBY = @UpdatedBy  
	 WHERE [SID] = @sid  
	 	END
     
	END
  ELSE
	BEGIN
	IF @lastDone IS NOT NULL 
	 	BEGIN
	INSERT INTO PA_PROPERTY_ITEM_DATES(PROPERTYID, ItemId,LastDone,UPDATEDON,UPDATEDBY,ParameterId )
	VALUES( @PropertyId,@ItemId, CONVERT(Datetime, @lastDone,120),GETDATE(),  @UpdatedBy,@parameterId)    
	END 
		IF @dueDate IS NOT NULL 
	 	BEGIN
	INSERT INTO PA_PROPERTY_ITEM_DATES(PROPERTYID, ItemId,DueDate,UPDATEDON,UPDATEDBY,ParameterId )
	VALUES( @PropertyId,@ItemId, CONVERT(Datetime, @dueDate,120),GETDATE(),  @UpdatedBy,@parameterId)    
	END 
	END 
 
 SET @minParamId =(SELECT MIN(ParameterId)FROM @ItemDates WHERE ParameterId > @minParamId)
 END  
END

