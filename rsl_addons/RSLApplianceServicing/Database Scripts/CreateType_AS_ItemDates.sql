USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_ItemDates]    Script Date: 11/01/2013 11:36:29 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'AS_ItemDates' AND ss.name = N'dbo')
DROP TYPE [dbo].[AS_ItemDates]
GO

USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_ItemDates]    Script Date: 11/01/2013 11:36:29 ******/
CREATE TYPE [dbo].[AS_ItemDates] AS TABLE(
	[ParameterId] [int] NULL,
	[LastDone] [nvarchar](100) NULL,
	[DueDate] [nvarchar](100) NULL,
	[ParameterName] [nvarchar](100) NULL
)
GO


