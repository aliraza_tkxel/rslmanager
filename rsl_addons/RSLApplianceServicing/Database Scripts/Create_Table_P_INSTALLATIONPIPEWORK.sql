USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_InstallationPipeWork]    Script Date: 12/18/2012 12:09:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[P_InstallationPipeWork](
	[PipeWorkID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](20) NOT NULL,
	[EmergencyControl] [varchar](5) NOT NULL,
	[VisualInspection] [varchar](5) NOT NULL,
	[GasTightnessTest] [varchar](5) NOT NULL,
	[EquipotentialBonding] [varchar](5) NOT NULL,
	[DateStamp] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_P_InstallationPipeWork] PRIMARY KEY CLUSTERED 
(
	[PipeWorkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[P_InstallationPipeWork]  WITH CHECK ADD  CONSTRAINT [FK_P_InstallationPipeWork_P__PROPERTY] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
GO

ALTER TABLE [dbo].[P_InstallationPipeWork] CHECK CONSTRAINT [FK_P_InstallationPipeWork_P__PROPERTY]
GO


