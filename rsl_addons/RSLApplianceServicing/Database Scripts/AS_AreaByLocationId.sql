
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_AreaByLocationId @locationId=1
-- Author:	<Salman Nazir>
-- Create date: <25/10/2012>
-- Description:	<Get Area by location id for Attributes Tree>
-- Webpage: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE AS_AreaByLocationId(
@locationId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	From PA_AREA
	Where LocationId = @locationId
END
GO
