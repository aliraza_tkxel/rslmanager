/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	7 January 2015
--  Description:	Script to ParameterValue length from 100 to 1000
 '==============================================================================*/

EXEC sys.sp_rename 'dbo.AS_ItemDetails', 'zAS_ItemDetails';
GO
CREATE TYPE [dbo].[AS_ItemDetails] AS TABLE(
	[ItemParamId] [int] NULL,
	[ParameterValue] [nvarchar](1000) NULL,
	[ValueId] [int] NULL,
	[IsCheckBoxSelected] [bit] NULL
)
GO
DECLARE @Name NVARCHAR(776);

DECLARE REF_CURSOR CURSOR FOR
SELECT referencing_schema_name + '.' + referencing_entity_name
FROM sys.dm_sql_referencing_entities('dbo.AS_ItemDetails', 'TYPE');

OPEN REF_CURSOR;

FETCH NEXT FROM REF_CURSOR INTO @Name;
WHILE (@@FETCH_STATUS = 0)
BEGIN
    EXEC sys.sp_refreshsqlmodule @name = @Name;
    FETCH NEXT FROM REF_CURSOR INTO @Name;
END;

CLOSE REF_CURSOR;
DEALLOCATE REF_CURSOR;
GO
DROP TYPE dbo.zAS_ItemDetails;
GO