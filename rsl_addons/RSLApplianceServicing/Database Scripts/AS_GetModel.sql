USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetModel
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,02/11/2012>
-- Description:	<Description,,Get all models of appliance>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetModel]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ModelID as id, Model as title
	FROM GS_ApplianceModel 
END
