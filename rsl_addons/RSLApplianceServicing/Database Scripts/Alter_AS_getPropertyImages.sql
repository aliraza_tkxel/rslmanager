USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_getPropertyImages]    Script Date: 11/01/2013 11:33:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_getPropertyImages @proertyId ='A010060001' ,@itemId=1  
-- Author:  <Ali Raza>  
-- Create date: <10/31/2013>  
-- Description: <Get the property Images against Item Id>  
-- WebPage: PropertRecord.aspx => Attributes Tab  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_getPropertyImages] (  
@proerptyId Varchar(100),  
@itemId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT [SID],PROPERTYID,ItemId,i.ImagePath,ImageName,CONVERT(varchar,CreatedOn,103 ) CreatedOn,LEFT(e.FIRSTNAME, 1)+' '+ e.LASTNAME  CreatedBy,i.Title  
 FROM PA_PROPERTY_ITEM_IMAGES i 
  INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy   
 WHERE PROPERTYID = @proerptyId AND ItemId = @itemId  
END  