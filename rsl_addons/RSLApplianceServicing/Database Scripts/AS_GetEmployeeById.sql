USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetEmployeeById]    Script Date: 11/30/2012 11:21:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetEmployeeById 	@employeeId = 615
-- Author:		<Noor Muhammad>
-- Create date: <29/11/2012>
-- Description:	<This Stored Proceedure get the Employee by id >
-- Web Page: Bridge.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetEmployeeById](
	@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId
	,E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName
	,AS_USER.UserId as UserId, AS_USER.IsActive as IsActive
	, AS_Pages.PageId
	,AS_Pages.PageName as PageName
	,AS_USERTYPE.Description as UserType
	FROM E__EMPLOYEE 
	INNER JOIN AS_USER ON E__EMPLOYEE.EMPLOYEEID = AS_USER.EmployeeId
	INNER JOIN AS_User_Pages ON E__EMPLOYEE.EMPLOYEEID = AS_User_Pages.EmployeeId
	INNER JOIN AS_Pages ON AS_Pages.PageId = AS_User_Pages.PageId
	INNER JOIN AS_USERTYPE ON AS_USER.UserTypeId = AS_USERTYPE.UserTypeId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId AND AS_Pages.IsActive = 1 AND AS_User_Pages.IsActive = 1
END
