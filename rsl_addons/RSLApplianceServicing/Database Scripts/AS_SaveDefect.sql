-- =============================================  
-- EXEC AS_GetPropertyAppliances @propertyId = A010060001  
-- @ApplianceId = 1,  
-- @CategoryId = 1,  
-- @DefectDate = NULL,  
-- @DefectIdentified = 0,  
-- @DefectIdentifiedNotes = NULL,  
-- @PhotoNotes = NULL,  
-- @RemedialAction = 0,  
-- @RemedialActionNotes = NULL  
-- Author:  <Salman Nazir>  
-- Create date: <11/06/2012>  
-- Description: <Get Appliances against Property Id >  
-- Web Page: PropertyRecord.aspx => Add Defect PopUp  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_SaveDefect]  
 -- Add the parameters for the stored procedure here
 @propertyDefectId INT 
 ,@propertyId NVARCHAR(20)
 ,@CategoryId INT 
 ,@DefectDate SMALLDATETIME = null
 ,@isDefectIdentified BIT=null
 ,@DefectIdentifiedNotes NVARCHAR(200)= null
 ,@isRemedialActionTaken BIT= null
 ,@remedialActionNotes NVARCHAR(200)= null
 ,@isWarningIssued BIT= null
 ,@ApplianceId INT= null
 ,@serialNumber NVARCHAR(20)= null
 ,@GcNumber NVARCHAR(9)= null
 ,@isWarningFixed BIT= null
 ,@isApplianceDisconnected BIT= null
 ,@isPartsRequired BIT= null
 ,@isPartsOrdered BIT= null
 ,@partsOrderedBy INT= null
 ,@partsDueDate DATE= null
 ,@partsDescription NVARCHAR(1000)= null
 ,@partsLocation NVARCHAR(200)= null
 ,@isTwoPersonsJob BIT= null
 ,@reasonForSecondPerson NVARCHAR(1000)= null
 ,@estimatedDuration DECIMAL(9,2)= null
 ,@priorityId INT= null
 ,@tradeId INT= null
 ,@filePath VARCHAR(500)= null 
 ,@photoName VARCHAR(100)= null
 ,@PhotoNotes VARCHAR(1000)= null
 ,@userId INT= null
 
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @Now DATETIME2 = GETDATE()

IF NOT EXISTS
(
	SELECT
		PropertyDefectId
	FROM
		P_PROPERTY_APPLIANCE_DEFECTS
	WHERE PropertyDefectId = @propertyDefectId
) 
BEGIN

DECLARE @JournalId INT
-- Insert statements for procedure here  
SELECT
	@JournalId = JournalId
FROM
	AS_JOURNAL
WHERE
	AS_JOURNAL.PROPERTYID = @propertyId
	AND ISCURRENT = 1

INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS (
				[PropertyId]
				,[CategoryId]
				,[JournalId]
				,[IsDefectIdentified]
				,[DefectNotes]
				,[IsActionTaken]
				,[ActionNotes]
				,[IsWarningIssued]
				,[ApplianceId]
				,[SerialNumber]
				,[IsWarningFixed]
				,[DefectDate]
				,[CreatedBy]
				,[PhotoNotes]
				,[GasCouncilNumber]
				,[IsDisconnected]
				,[IsPartsrequired]
				,[IsPartsOrdered]
				,[PartsOrderedBy]
				,[PartsDue]
				,[PartsDescription]
				,[PartsLocation]
				,[IsTwoPersonsJob]
				,[ReasonFor2ndPerson]
				,[Duration]
				,[Priority]
				,[TradeId]
				,CreatedDate
				,DateCreated
				)           
	VALUES (
			@propertyId, @categoryId, @JournalId
			,@isDefectIdentified, @DefectIdentifiedNotes
			,@isRemedialActionTaken, @remedialActionNotes
			,@isWarningIssued
			,@ApplianceId
			,@serialNumber
			,@isWarningFixed
			,@DefectDate
			,@userId
			,@PhotoNotes
			,@GcNumber
			,@isApplianceDisconnected
			,@isPartsRequired , @isPartsOrdered
			,@partsOrderedBy ,@partsDueDate
			,@partsDescription ,@partsLocation
			,@isTwoPersonsJob, @reasonForSecondPerson
			,@estimatedDuration ,@priorityId
			,@tradeId
			,@Now
			,@Now
		)
		IF  @photoName <> ''
		BEGIN
			INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES (
							ImageTitle
							,ImagePath
							,PropertyDefectId
						)
				VALUES (
						@photoName, @filePath, SCOPE_IDENTITY()
					)
		END			
END
ELSE
BEGIN

	UPDATE P_PROPERTY_APPLIANCE_DEFECTS
		SET IsPartsrequired = @isPartsRequired
			,IsPartsOrdered = @isPartsOrdered
			,PartsDue = @partsDueDate
			,PartsDescription = @partsDescription
			,PartsLocation = @partsLocation
			,IsTwoPersonsJob = @isTwoPersonsJob
			,ReasonFor2ndPerson = @reasonForSecondPerson
			,Duration = @estimatedDuration
			,[Priority] = @priorityId
			,TradeId = @tradeId
			,ModifiedBy = @userId
			,ModifiedDate = @Now
			
	WHERE PropertyDefectId = @propertyDefectId
	
END

END