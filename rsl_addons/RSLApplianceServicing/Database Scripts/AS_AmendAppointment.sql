USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AmendAppointment]    Script Date: 12/11/2012 17:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- DECLARE	@return_value int,
--		@journalHistoryId int

--EXEC	@return_value = [dbo].[AS_AmendAppointment]
--		@propertyId = 1,
--		@journalId = 1,
--		@createdBy = 615,
--		@inspectionTypeId = 1,
--		@appointmentDate = '2012-11-03',
--		@statusId = 2,
--		@actionId = 5,
--		@notes = N'This is demo information',
--		@isLetterAttached = false,
--		@isDocumentAttached = false,
--		@assignedTo = 63,
--		@appStartTime = '11:00',
--		@appStartTime = '13:00',
--		@shift = 'AM',
--		@tenancyId = 10065,
--		@appointmentId = 65,
--		@journalHistoryId = @journalHistoryId OUTPUT 
-- Author:		<Aqib Javed>
-- Create date: <14/11/2012>
-- Description:	<This SP shall amend existing appointment information from Appointment Arranged Screen>
-- Webpage: FuelScheduling.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_AmendAppointment] 
	@journalId int,
	@propertyId varchar(50),
	@createdBy int,
	@inspectionTypeId int,
	@appointmentDate date,
	@statusId int,
	@actionId int,
	@notes nvarchar(1000),		
	@isLetterAttached bit,
	@isDocumentAttached bit,
	@appStartTime varchar(10),
	@appEndTime varchar(10),
	@assignedTo int,
	@shift varchar(5),
	--@tenancyId int,
	@appointmentId int,
	@journalHistoryId int out
AS
BEGIN
   Declare @UserId int
   Declare @tenancyId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
   	--Update the Journal
	UPDATE [dbo].[AS_JOURNAL]
	SET[PROPERTYID] = @propertyId,
	   [STATUSID] = @statusId
	  ,[ACTIONID] = @actionId
	  ,[INSPECTIONTYPEID] = @inspectionTypeId
	  ,[CREATIONDATE] = GETDATE()
	  ,[CREATEDBY] = @createdBy	
	WHERE JOURNALID = @journalId AND isCurrent = 1
	
	--Insert Record into Journal History
	INSERT INTO [dbo].[AS_JOURNALHISTORY]
           ([JOURNALID]
           ,[PROPERTYID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[INSPECTIONTYPEID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]           
           ,[IsDocumentAttached])
     VALUES
           (@journalId
           ,@propertyId
           ,@statusId
           ,@actionId
           ,@inspectionTypeId
           ,GETDATE() 
           ,@createdBy
           ,@notes
           ,@isLetterAttached
		   ,@isDocumentAttached)     

--Getting JournalHistory Id		   
   SELECT @journalHistoryId =  @@identity  
   Declare @statusHistoryId int
   
   SELECT @statusHistoryId = MAX(StatusHistoryId) FROM AS_StatusHistory WHERE StatusId=@statusId
   
	Declare @actionHistoryId int
	SELECT @actionHistoryId = MAX(ActionHistoryId) FROM AS_ActionHistory  where ActionId=@actionId
   
    --Update Journal History Again With Status History and Action History Id
	UPDATE [AS_JOURNALHISTORY]
	SET [StatusHistoryId] = @statusHistoryId
      ,[ActionHistoryId] = @actionHistoryId
      
	WHERE JOURNALHISTORYID = @journalHistoryId
 
   --Insert Record in Appointment 
	Declare @jsgNumber int
	SELECT @jsgNumber = MAX(JSGNUMBER) FROM AS_APPOINTMENTS
	SET @jsgNumber=@jsgNumber+1
	
	--Change - by Behroz - 14/02/2013 - Start
	DECLARE @AppStatus varchar(50)
	select @AppStatus = APPOINTMENTSTATUS from AS_APPOINTMENTS where appointmentid = @appointmentId	
	
	IF (@statusId = 1 or @statusId = 2 or @statusId = 4)
	BEGIN
		IF @AppStatus = 'InProgress'
			BEGIN
				SET @AppStatus = 'InProgress'
			END
		ELSE
			BEGIN
		SET @AppStatus = 'NotStarted'
	END
	END
	ELSE IF(@statusId = 3 or @statusId = 5)
	BEGIN
		SET @AppStatus = 'Finished'
	END	
	--Change - by Behroz - 14/02/2013 - End
	
	Update [dbo].[AS_APPOINTMENTS] SET
            [JOURNALHISTORYID]=@journalHistoryId
           ,[APPOINTMENTDATE] = @appointmentDate
           ,[APPOINTMENTSHIFT]= @shift
           ,[APPOINTMENTSTARTTIME]=@appStartTime
           ,[APPOINTMENTENDTIME]=@appEndTime
           ,[ASSIGNEDTO]= @assignedTo
           ,[CREATEDBY] = @createdBy
           ,[LOGGEDDATE] = GETDATE()
           ,[NOTES]=@notes
           --Change - by Behroz - 05/02/2013 - Start
           ,[APPOINTMENTSTATUS] = @AppStatus
           --Change - by Behroz - 05/02/2013 - End
           WHERE APPOINTMENTID = @appointmentId  
     
   -- Getting Appointment Id
SELECT @tenancyId=tenancyId from AS_APPOINTMENTS  WHERE APPOINTMENTID = @appointmentId  
-- Inserting into AS_APPOINTMENTSHISTORY Table	 
	INSERT INTO [dbo].[AS_APPOINTMENTSHISTORY]
           (
            [APPOINTMENTID],
            [JSGNUMBER],
            [TENANCYID]
           ,[JournalId]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTDATE]
           ,[APPOINTMENTSHIFT]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[NOTES]
           --Change - by Behroz - 05/02/2013 - Start
           ,[APPOINTMENTSTATUS]
           --Change - by Behroz - 05/02/2013 - End
           )
     VALUES
           (
           @AppointmentId,
           @jsgNumber
           ,@tenancyId
           ,@journalId
           ,@journalHistoryId
           ,@appointmentDate
           ,@shift
           ,@appStartTime
           ,@appEndTime
           ,@assignedTo
           ,@createdBy
           ,GETDATE() 
           ,@notes
           --Change - by Behroz - 05/02/2013 - Start
           ,@AppStatus
           --Change - by Behroz - 05/02/2013 - End
           )   
END

