
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 16/10/2012
-- Description:	This SP would update the letter based on the Standard Letter ID
-- Useage: Createletter.aspx
-- Exec: 
-- =============================================
CREATE PROCEDURE [dbo].[AS_UpdateStandardLetterTemplate]
	-- Add the parameters for the stored procedure here
	@LetterId			int,
	@StatusId			int,
	@ActionId			int,
	@Title				varchar(50),
	@Code				varchar(50),
	@Body				varchar(MAX),
	@ModifiedBy			int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    UPDATE 
		AS_StandardLetters   
	SET 
		StatusId		=	@StatusId,
		ActionId		=	@ActionId,
		Title			=	@Title,
		Code			=	@Code,
		Body			=	@Body,
		ModifiedBy		=	@ModifiedBy,
		ModifiedDate	=	GETDATE()
	Where 
		StandardLetterId = @LetterId 
   
END
GO
