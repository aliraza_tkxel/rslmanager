SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_DeleteUserInspectionType @employeeId = 199 
-- Author:		<Salman Nazir>
-- Create date: <02/11/2012>
-- Description:	<Delete the InspectionType values against user>
-- WEb Page: Resources.aspx
-- =============================================
CREATE PROCEDURE AS_DeleteUserInspectionType
(
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM AS_USER_INSPECTIONTYPE
	WHERE EmployeeId = @employeeId
END
GO
