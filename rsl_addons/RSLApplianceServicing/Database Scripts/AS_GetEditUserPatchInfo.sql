SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetEditUserPatchInfo @employeeId = @employeeId
-- Author:		<Salman Nazir>
-- Create date: <10/1/2012>
-- Description:	<Description,,On Edit User page get the user Patch info and Scheme Info>
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE AS_GetEditUserPatchInfo(
@employeeId int 
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT 
		ISNULL(AS_UserPatchDevelopment.SCHEMEID,'')AS SchemeId
		,ISNULL(AS_UserPatchDevelopment.PatchId, ' ')AS PatchId
		,ISNULL(P_SCHEME.SCHEMENAME, ' ') AS SchemeName
		,ISNULL(E_PATCH.LOCATION,' ') As Location
		From AS_UserPatchDevelopment 
		Left JOIN E_PATCH on AS_UserPatchDevelopment.PatchId = E_PATCH.PatchId
		LEFT JOIN P_SCHEME ON AS_UserPatchDevelopment.SCHEMEID = P_SCHEME.SCHEMEID 
		Where AS_UserPatchDevelopment.EmployeeId = @employeeId
END
GO

