USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 31/10/2012
-- Modified Date: <13 Jun 2013, Aamir Waheed>
-- Description:	This stored procedure returns the count of all "Appointments Arranged" properties
-- Usage: Dashboard
-- Exec [dbo].[AS_GetCountAppointmentsArranged]
--		@PatchId = 18,
--		@DevelopmentId = 1
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetCountAppointmentsArranged]
	@PatchId		int,
	@DevelopmentId	int
AS
BEGIN
	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT COUNT(P__PROPERTY.propertyid) AS Number'
	
	SET @fromClause		= 'FROM AS_APPOINTMENTS 
							INNER JOIN	AS_JOURNAL on AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID 
							INNER JOIN	AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId 
							INNER JOIN E__EMPLOYEE on AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 
							INNER JOIN P__PROPERTY on AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
							INNER JOIN dbo.P_DEVELOPMENT D ON D.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID	
							INNER JOIN P_FUELTYPE  ON P__PROPERTY.FUELTYPE = P_FUELTYPE.FUELTYPEID 
							LEFT JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
								AND (dbo.C_TENANCY.ENDDATE IS NULL OR dbo.C_TENANCY.ENDDATE > GETDATE())
							LEFT JOIN (SELECT P_LGSR.ISSUEDATE,PROPERTYID from P_LGSR)as P_LGSR
								ON P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	SET @whereClause = @whereClause + '1=1
								AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56
								AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0 
								AND (AS_JOURNAL.STATUSID = 2  OR AS_JOURNAL.STATUSID = 3 OR AS_JOURNAL.STATUSID = 4)
								AND dbo.P__PROPERTY.FUELTYPE = 1
								AND dbo.P__PROPERTY.PROPERTYTYPE NOT IN (6,8,9)'
	
	--SET @whereClause = @whereClause + 'DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) <= 56' + CHAR(10)
	--SET @whereClause = @whereClause + 'AND DATEDIFF(DAY,CURRENT_TIMESTAMP,DATEADD(YEAR,1,P_LGSR.ISSUEDATE)) > 0' + CHAR(10)
	--SET @whereClause = @whereClause + 'AND C_ADDRESS.ISDEFAULT = 1  															
	--								   AND C_CUSTOMERTENANCY.ENDDATE IS NULL 
	--								   AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (SELECT MIN(CUSTOMERTENANCYID)FROM C_CUSTOMERTENANCY WHERE TENANCYID=C_TENANCY.TENANCYID AND C_TENANCY.ENDDATE IS NULL)' + CHAR(10)
	
	--SET @whereClause = @whereClause + 'AND AS_Status.Title = ''Arranged'' ' + CHAR(10)
	
	if (@PatchId <> -1 OR @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END
		
	if (@PatchId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.PATCH = ' + CONVERT(varchar, @PatchId) + CHAR(10)
 	END
 	
 	if (@PatchId <> -1 AND @DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AND' + CHAR(10)
 	END

	if (@DevelopmentId <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'P__PROPERTY.DEVELOPMENTID = ' + CONVERT(varchar, @DevelopmentId) + CHAR(10)
 	END
 	
 
 	
 	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause + @whereClause
	
	-- Printing the query for debugging
	PRINT @query 
	
	-- Executing the query
    EXEC (@query)
    
END
