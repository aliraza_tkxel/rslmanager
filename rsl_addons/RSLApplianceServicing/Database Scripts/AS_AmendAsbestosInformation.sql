-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdullah Saeed
-- Create date: July 7,2014
-- Description:	Amend Asbestos Information
-- =============================================
CREATE PROCEDURE [dbo].[AS_AmendAsbestosInformation]  
	-- Add the parameters for the stored procedure here
	
	@propasbLevelId varchar(50),
	@asbestosLevelId int,  
	@asbestosElementId int,  
	@riskId varchar(50),  
	@addedDate date,  
	@removedDate varchar(50),  
	@notes varchar(250)  
	  
 
AS
BEGIN
if @removedDate IS NOT NULL OR @removedDate != ''
BEGIN
UPDATE P_PROPERTY_ASBESTOS_RISKLEVEL
		SET ASBRISKLEVELID = @asbestosLevelId, ASBESTOSID = @asbestosElementId, DateAdded = @addedDate, DateRemoved = @removedDate, Notes = @notes
		WHERE PROPASBLEVELID = @propasbLevelId
END
ELSE
BEGIN

UPDATE P_PROPERTY_ASBESTOS_RISKLEVEL
		SET ASBRISKLEVELID = @asbestosLevelId, ASBESTOSID = @asbestosElementId, DateAdded = @addedDate, Notes = @notes
		WHERE PROPASBLEVELID = @propasbLevelId
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

END

UPDATE P_PROPERTY_ASBESTOS_RISK
		SET ASBRISKID = @riskId
		WHERE PROPASBLEVELID = @propasbLevelId

END
