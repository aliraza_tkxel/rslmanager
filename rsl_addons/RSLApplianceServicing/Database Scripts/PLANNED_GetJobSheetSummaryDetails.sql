USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PLANNED_GetJobSheetSummaryDetails]
	-- Add the parameters for the stored procedure here
	@JOURNALHISTORYID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
DECLARE @propertyId VARCHAR(50)
-- Insert statements for procedure here
--=============================================================
-- Appointment Info Details
--============================================================
SELECT
	PA.APPOINTMENTID											AS JSN
	,ISNULL(PAH.APPOINTMENTDATE, ' ')							AS StartDate
	,ISNULL(PAH.APPOINTMENTENDDATE, ' ')						AS EndDate
	,COALESCE(SS.TITLE, PAH.APPOINTMENTSTATUS, ' ')				AS InterimStatus
	,PA.JournalId												AS PMO
	,PJH.PROPERTYID												AS PROPERTYID
	,PJH.JOURNALHISTORYID										AS JOURNALHISTORYID
	,PAH.COMPTRADEID											AS COMPTRADEID
	,ISNULL(PC.COMPONENTNAME, ' ')								AS Component
	,ISNULL(T.Description, ' ')									AS Trade
	,PAt.Planned_Appointment_Type								AS Planned_Appointment_Type
	,ISNULL(PMT.DURATION, ' ')									AS Duration
	,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, ' ')				AS Operative
	,PAH.isMiscAppointment										AS isMiscAppointment
	,ISNULL(PAH.CUSTOMERNOTES, ' ')								AS CustomerNotes
	,COALESCE(JSNotes.JobSheetNotes, PAH.APPOINTMENTNOTES, ' ')	AS JobSheetNotes
	,ISNULL(PAH.APPOINTMENTSTARTTIME, ' ')						AS StartTime
	,ISNULL(PAH.APPOINTMENTENDTIME, ' ')						AS EndTime
	,ROW_NUMBER() OVER (ORDER BY PA.APPOINTMENTID)				AS Row
FROM
	PLANNED_JOURNAL_HISTORY PJH
	INNER JOIN PLANNED_APPOINTMENTS PA
		ON PJH.JOURNALID = PA.JournalId
	INNER JOIN PLANNED_APPOINTMENTS_HISTORY PAH
		ON PAH.APPOINTMENTID = PA.APPOINTMENTID
	INNER JOIN
	(
		SELECT
			MAX(APPOINTMENTHISTORYID)	AS [MAXAPPOINTMENTHISTORYID]
			,APPOINTMENTID
		FROM
			PLANNED_APPOINTMENTS_HISTORY
		GROUP BY
			APPOINTMENTID
	) MPAH
		ON MPAH.MAXAPPOINTMENTHISTORYID = PAH.APPOINTMENTHISTORYID
	LEFT JOIN PLANNED_COMPONENT_TRADE PCT
		ON PCT.COMPTRADEID = PAH.COMPTRADEID
	LEFT JOIN PLANNED_COMPONENT PC
		ON PC.COMPONENTID = PCT.COMPONENTID
	LEFT JOIN G_TRADE T
		ON T.TradeId = PCT.TRADEID
	LEFT JOIN PLANNED_MISC_TRADE PMT
		ON PMT.TradeId = T.TradeId
		AND PMT.AppointmentId = PA.APPOINTMENTID
	LEFT JOIN Planned_Appointment_Type PAT
		ON PAT.Planned_Appointment_TypeId = PA.Planned_Appointment_TypeId
	LEFT JOIN PLANNED_SUBSTATUS SS
		ON SS.SUBSTATUSID = PAH.JOURNALSUBSTATUS
	LEFT JOIN E__EMPLOYEE E
		ON E.EMPLOYEEID = PAH.ASSIGNEDTO
	LEFT JOIN
	(
		SELECT
			*
		FROM
			(
				SELECT
					APPOINTMENTHISTORYID				AS APPOINTMENTHISTORYID
					,APPOINTMENTID						AS APPOINTMENTID
					,APPOINTMENTNOTES					AS JobSheetNotes
					,ROW_NUMBER() OVER
					(PARTITION BY APPOINTMENTID
					ORDER BY APPOINTMENTHISTORYID DESC)	AS RN
				FROM
					PLANNED_APPOINTMENTS_HISTORY
			) Results
		WHERE
			RN = 2
	) JSNotes
		ON JSNotes.APPOINTMENTID = PAH.APPOINTMENTID

WHERE
	PJH.JOURNALHISTORYID = @JOURNALHISTORYID
ORDER BY
	PAH.LOGGEDDATE DESC

SELECT
	@propertyId = PROPERTYID
FROM
	PLANNED_JOURNAL_HISTORY
WHERE
	JOURNALHISTORYID = @JOURNALHISTORYID
--==================================================================
-- Property & Customer Info
--==================================================================
EXEC PLANNED_GetPropertyDetail @propertyId

END