
-- =============================================  
-- Exec AS_ItemByAreaId @areaId=1  
-- Author: <Salman Nazir>  
-- Create date: <25/10/2012>  
-- Description: <Get Items by Area id for Attributes Tree>  
-- Web Page: PropertyRecord.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_ItemByAreaId](  
@areaId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT *   
 From PA_ITEM  
 Where AreaID = @areaId and IsActive = 1  and ParentItemId is null
 ORDER BY ItemSorder ASC 
END  