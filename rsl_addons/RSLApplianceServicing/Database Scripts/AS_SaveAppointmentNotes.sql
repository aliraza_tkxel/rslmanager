USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[AS_SaveAppointmentNotes]
--		@appointmentId = 1,
--		@notes = N'abc'

--SELECT	'Return Value' = @return_value

-- Author:		<Ahmed Mehmood>
-- Create date: <14/1/2016>
-- Description:	<This procedure saves appointment notes>
-- Web Page: AppointmentArranged.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_SaveAppointmentNotes](
@appointmentId int
,@notes nvarchar(1000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE	AS_APPOINTMENTS
	SET		AS_APPOINTMENTS.NOTES = @notes
	WHERE	APPOINTMENTID = @appointmentId
END


