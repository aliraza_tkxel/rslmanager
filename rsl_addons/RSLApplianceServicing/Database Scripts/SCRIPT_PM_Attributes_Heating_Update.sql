/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	24 November 2014
--  Description:	Scripts related to changes mentioned by Peter in email 
--	'PM Attributes Heating Update' Date: Fri, Nov 21, 2014 at 9:34 PM
--					
 '==============================================================================*/


BEGIN TRANSACTION
	BEGIN TRY  
	
--======================================================
--(1) CHANGE 'Heating & Water Supply' TO 'Water Supply'
--======================================================
UPDATE	PA_ITEM 
SET		ItemName='Water Supply' 
WHERE	ItemName='Heating & Water Supply'
		AND ParentItemId IS NOT NULL
		
--======================================================
--(2) MOVE 'Boiler Manufacturer' FROM 'Water Supply' TO 'Heating'
--======================================================
UPDATE	PA_ITEM_PARAMETER 
SET		ItemId = dbo.FN_GetItemId('Heating')
WHERE	ItemId = dbo.FN_GetItemId('Water Supply')
		AND ParameterId = DBO.FN_GetParameterId('Boiler Manufacturer') 
		
UPDATE	PA_PARAMETER
SET		ParameterSorder = 5
WHERE	ParameterName = 'Boiler Manufacturer'

--======================================================
--(3) HIDE 'Condition Rating' FROM 'Heating'
--======================================================
UPDATE	PA_ITEM_PARAMETER
SET		IsActive = 0
FROM	PA_ITEM_PARAMETER 
		INNER JOIN PA_ITEM [I] ON PA_ITEM_PARAMETER.ItemId = I.ItemID 
		INNER JOIN PA_PARAMETER [PP] ON PA_ITEM_PARAMETER.ParameterId = PP.ParameterID  
WHERE	I.ItemName = 'Heating'
		AND PP.ParameterName = 'Condition Rating'
		
--======================================================
--(4) HIDE 'Water heater type' FROM 'Water Supply'
--======================================================
UPDATE	PA_ITEM_PARAMETER
SET		IsActive = 0
FROM	PA_ITEM_PARAMETER 
		INNER JOIN PA_ITEM [I] ON PA_ITEM_PARAMETER.ItemId = I.ItemID 
		INNER JOIN PA_PARAMETER [PP] ON PA_ITEM_PARAMETER.ParameterId = PP.ParameterID  
WHERE	I.ItemName = 'Water Supply'
		AND PP.ParameterName = 'Water heater type'

--======================================================
--(5) HIDE 'Water heater type' FROM 'Water Supply'
--======================================================
DECLARE @TYPEID INT
SELECT	@TYPEID = PA_ITEM_PARAMETER.ParameterId  
FROM	PA_ITEM_PARAMETER 
		INNER JOIN PA_ITEM [I] ON PA_ITEM_PARAMETER.ItemId = I.ItemID 
		INNER JOIN PA_PARAMETER [PP] ON PA_ITEM_PARAMETER.ParameterId = PP.ParameterID  
WHERE	I.ItemName = 'Water Supply'
		AND PP.ParameterName = 'Type'
		
UPDATE	PA_PARAMETER
SET		ParameterName = 'Hot Water Source'
WHERE	ParameterID = @TYPEID
	
--======================================================	
--(6)	Remove 'Internals => Services => Heating & Water Supply => Water Supply => Fuel Type'
--====================================================== 
UPDATE	PA_ITEM_PARAMETER
SET		IsActive = 0
FROM	PA_ITEM_PARAMETER 
		INNER JOIN PA_ITEM [I] ON PA_ITEM_PARAMETER.ItemId = I.ItemID 
		INNER JOIN PA_PARAMETER [PP] ON PA_ITEM_PARAMETER.ParameterId = PP.ParameterID  
WHERE	I.ItemName = 'Water Supply'
		AND PP.ParameterName = 'Fuel type'
		
--======================================================	
--(7) MOVE 'Hot Water Source', 'Life cycle' and 'Component Accounting' down to below 'Stop Cock Location'	
--======================================================
UPDATE	PA_PARAMETER
SET		ParameterSorder = 7
WHERE	ParameterName = 'Hot Water Source'

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 