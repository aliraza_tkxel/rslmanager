create table #tempAttribute
(
pid varchar(30) ,
 SmokeDetector int null,
 Co2Detector int null
)
 DECLARE @pid nvarchar(100),
 @SmokeDetector nvarchar(150), @Co2Detector nvarchar(150)
	
INSERT INTO #tempAttribute select PROPERTYID,SmokeDetector,Co2Detector from P_ATTRIBUTES WHERE (SmokeDetector IS NOT NULL And SmokeDetector > 0) OR(Co2Detector IS NOT NULL And Co2Detector > 0) 
--Select * from #tempAttribute 
DECLARE @ItemToFetchCursor CURSOR  
   --Initialize cursor  
   Set @ItemToFetchCursor = CURSOR FAST_FORWARD FOR SELECT  pid,SmokeDetector,Co2Detector  FROM  #tempAttribute
  
   --Open cursor  
   OPEN @ItemToFetchCursor  
 ---fetch row from cursor  
   FETCH NEXT FROM @ItemToFetchCursor INTO @pid, @SmokeDetector,@Co2Detector
  ---Iterate cursor to get record row by row  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   Declare  @smokeitemParamId INT=0 ,  @coItemParamId INT=0 
  
   
  SELECT @smokeitemParamId = ItemParamID FROM PA_ITEM_PARAMETER 
  INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
  INNER JOIN  PA_ITEM on PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
 where PA_PARAMETER.ParameterName = 'Quantity' And PA_ITEM.ItemName = 'Smoke'
 
   SELECT @coItemParamId = ItemParamID FROM PA_ITEM_PARAMETER 
  INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
  INNER JOIN  PA_ITEM on PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
 where PA_PARAMETER.ParameterName = 'Quantity' And PA_ITEM.ItemName = 'CO'
				IF @smokeitemParamId > 0
						BEGIN
						PRINT ' smokeitemParamId == '+convert(varchar, IsNUll(@smokeitemParamId,0))+'==SmokeDetector =='+convert(varchar, IsNUll(@SmokeDetector,0))+'++++++++++++++'
 
							INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID, ITEMPARAMID, PARAMETERVALUE,  UPDATEDON, UPDATEDBY, IsCheckBoxSelected)
							VALUES(@pid,@smokeitemParamId,@SmokeDetector,GETDATE(),943,0) 
						END
				IF @coItemParamId > 0
						BEGIN
						PRINT ' coItemParamId == '+convert(varchar, IsNUll(@coItemParamId,0))+'== Co2Detector =='+convert(varchar, IsNUll(@Co2Detector,0))+'++++++++++++++'
 
							INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID, ITEMPARAMID, PARAMETERVALUE,  UPDATEDON, UPDATEDBY, IsCheckBoxSelected)
							VALUES(@pid,@coItemParamId,@Co2Detector,GETDATE(),943,0) 
						END
 
 
    
     FETCH NEXT FROM @ItemToFetchCursor INTO @pid, @SmokeDetector,@Co2Detector
  END  
--close & deallocate cursor    
CLOSE @ItemToFetchCursor  
DEALLOCATE @ItemToFetchCursor  	
DROP TABLE #tempAttribute	