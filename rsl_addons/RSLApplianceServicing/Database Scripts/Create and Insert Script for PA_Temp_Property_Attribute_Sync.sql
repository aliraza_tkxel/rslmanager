USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[PA_Temp_Property_Attribute_Sync]    Script Date: 10/28/2014 16:34:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PA_Temp_Property_Attribute_Sync](
	[PropertyAttribute] [nvarchar](200) NOT NULL,
	[Location] [nvarchar](200) NOT NULL,
	[Area] [nvarchar](200) NOT NULL,
	[Item] [nvarchar](200) NOT NULL,
	[ParameterName] [nvarchar](200) NOT NULL,
	[DataType] [nvarchar](200) NOT NULL,
	[ControlType] [nvarchar](200) NOT NULL,
	[TABLES] [nvarchar](200) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'FLOORAREA', N'Main Construction', N'Dwelling ', N'Structure', N'Floor Area(sq m)', N'float', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'MAXPEOPLE', N'Main Construction', N'Dwelling ', N'Structure', N'Max People', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'BEDROOMS', N'Internals', N'Accommodation', N'Bedrooms', N'Bedrooms', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'BEDROOMSITTINGROOM', N'Internals', N'Accommodation', N'Bedrooms', N'Bedrooms/Sitting Rooms', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'KITCHEN', N'Internals', N'Accommodation', N'Kitchen', N'Kitchen', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'KITCHENDINER', N'Internals', N'Accommodation', N'Kitchen', N'Kitchen/Dinner', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'LIVINGROOM', N'Internals', N'Accommodation', N'Living Room/Dining Room', N'Living Room', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'LIVINGROOMDINER', N'Internals', N'Accommodation', N'Living Room/Dining Room', N'Living Room/Dinner', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'DININGROOM', N'Internals', N'Accommodation', N'Living Room/Dining Room', N'Dinning Room', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'WC', N'Internals', N'Accommodation', N'Bathroom', N'WC', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'BATHROOM', N'Internals', N'Accommodation', N'Bathroom', N'Bathroom', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'BATHROOMWC', N'Internals', N'Accommodation', N'Bathroom', N'Bathroom/WC', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SHOWER', N'Internals', N'Accommodation', N'Bathroom', N'Shower', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SHOWERWC', N'Internals', N'Accommodation', N'Bathroom', N'Shower/WC', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'BOXROOMUTILITYROOM', N'Internals', N'Accommodation', N'Kitchen', N'Box/Utility Room', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SHAREDKITCHENLIVING', N'Internals', N'Accommodation', N'Kitchen', N'Shared Kitchen/Living', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SHAREDWC', N'Internals', N'Accommodation', N'Bathroom', N'Shared WC', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SHAREDSHOWERWC', N'Internals', N'Accommodation', N'Bathroom', N'Shared Shower/WC', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'HEATING', N'Internals', N'Services', N'Heating', N'Heating', N'Integer', N'Dropdown', N'P_HEATING')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'PARKING', N'Externals', N'Dwelling ', N'Drive/Paths', N'Parking', N'Integer', N'Dropdown', N'Yes/No')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'GARDEN', N'Externals', N'Dwelling ', N'Garden', N'Garden', N'Integer', N'Dropdown', N'P_GARDEN')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'FURNISHING', N'Main Construction', N'Dwelling ', N'Structure', N'Furnishing', N'Integer', N'Dropdown', N'P_FURNISHING')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'PETS', N'Main Construction', N'Dwelling ', N'Structure', N'Pets', N'Integer', N'Dropdown', N'Yes/No')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'SECURITY', N'Main Construction', N'Dwelling ', N'Structure', N'Security', N'Integer', N'Dropdown', N'P_SECURITY')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'KITCHENDINERLOUNGE', N'Internals', N'Accommodation', N'Kitchen', N'Kitchen/Dinner/Lounge', N'Integer', N'TextBox', N'NA')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'ACCESSIBILITYSTANDARDS', N'Main Construction', N'Dwelling ', N'Structure', N'W/Chair Access', N'Integer', N'Dropdown', N'P_ACCESSIBILITYSTANDARDS')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'FrontPorch', N'Internals', N'Accommodation', N'Hallway', N'Front Porch', N'Integer', N'Dropdown', N'Yes/No')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'ConservatoryUPVC', N'Internals', N'Accommodation', N'Living Room/Dining Room', N'Conservatory (UPVC)', N'Integer', N'Dropdown', N'Yes/No')
INSERT [dbo].[PA_Temp_Property_Attribute_Sync] ([PropertyAttribute], [Location], [Area], [Item], [ParameterName], [DataType], [ControlType], [TABLES]) VALUES (N'Garage', N'Externals', N'Dwelling ', N'Garage', N'Garage', N'Integer', N'Dropdown', N'Yes/No')
