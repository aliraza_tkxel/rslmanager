USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_UpdatePrintedStatus]    Script Date: 09/01/2013 16:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <27/08/2013>
-- Description:	<Update ISPRINTED status in P_LGSR table>
-- Web Page:    <CP12Document.aspx>
-- EXEC	@return_value = [dbo].[AS_UpdatePrintedStatus]
--		@LGSRID = 1,
--		@result = @result OUTPUT
-- =====================================================================
CREATE PROCEDURE [dbo].[AS_UpdatePrintedStatus] 
	-- Add the parameters for the stored procedure here
	@LGSRID int,	
	@result bit OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE P_LGSR
	SET ISPRINTED  = 1
	WHERE LGSRID = @LGSRID
	
	SET @result = 1		 
END

