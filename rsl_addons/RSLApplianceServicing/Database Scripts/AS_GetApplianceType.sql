USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetApplianceType]    Script Date: 04/21/2014 18:53:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetApplianceType
-- Author:		<Noor Muhammad>
-- Create date: <02/11/2012>
-- Description:	<Get all appliance types>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetApplianceType]
(@prefix varchar(100) )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT APPLIANCETYPEID as id, APPLIANCETYPE as title
	FROM GS_Appliance_Type
	WHERE ISACTIVE = 1 And APPLIANCETYPE like '%'+@prefix+'%' 
	order by APPLIANCETYPE asc
END
