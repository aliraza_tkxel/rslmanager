USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetPlannedAppointments]
--		@propertyId = N'A010060001'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,, 30 Nov, 2013>
-- Description:	<Description,,This procedure 'll get the asbestos information >
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPlannedAppointments] 
	@propertyId as varchar(20)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT	PLANNED_APPOINTMENTS.APPOINTMENTID ,ISNULL(PLANNED_COMPONENT.COMPONENTNAME,'Miscellaneous works') + ' Replacement' as Component 
		, CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE  , 106) AppointmentDate
	FROM	PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
		INNER JOIN PLANNED_STATUS ON  PLANNED_JOURNAL.STATUSID =  PLANNED_STATUS.STATUSID
		LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
	WHERE	PLANNED_JOURNAL.PROPERTYID = @propertyId AND PLANNED_STATUS.TITLE = 'Arranged'

	ORDER BY  PLANNED_APPOINTMENTS.APPOINTMENTDATE DESC
		
END
