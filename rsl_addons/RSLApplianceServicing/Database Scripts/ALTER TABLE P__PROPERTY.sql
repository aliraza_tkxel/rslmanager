/*
   10/02/20151:47:01 AM
   User: sa
   Server: Dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.P__PROPERTY
	DROP CONSTRAINT FK_P__PROPERTY_P_DEVELOPMENT
GO
ALTER TABLE dbo.P_DEVELOPMENT SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_DEVELOPMENT', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_DEVELOPMENT', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_DEVELOPMENT', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_DEVELOPMENT SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_DEVELOPMENT', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_DEVELOPMENT', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_DEVELOPMENT', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P__PROPERTY WITH NOCHECK ADD CONSTRAINT
	FK_P__PROPERTY_PDR_DEVELOPMENT FOREIGN KEY
	(
	DEVELOPMENTID
	) REFERENCES dbo.PDR_DEVELOPMENT
	(
	DEVELOPMENTID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P__PROPERTY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'CONTROL') as Contr_Per 