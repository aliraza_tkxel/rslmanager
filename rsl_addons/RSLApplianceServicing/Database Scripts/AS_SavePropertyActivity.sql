USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePropertyActivity]    Script Date: 10/23/2012 17:36:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- DECLARE	@return_value int,
--		@journalHistoryId int

--EXEC	@return_value = [dbo].[AS_SavePropertyActivity]
--		@propertyId = N'A010060001',
--		@createdBy = 615,
--		@inspectionTypeId = 1,
--		@createDate = '2012-11-03',
--		@statusId = 2,
--		@actionId = 5,
--		@notes = N'hello',
--		@isLetterAttached = false,
--		@isDocumentAttached = false,
--		@journalHistoryId = @journalHistoryId OUTPUT

--SELECT	@journalHistoryId as N'@journalHistoryId'
--SELECT	'Return Value' = @return_value

-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,09/10/2012>
-- Description:	<Description,,Save Activity against property >
-- Web Page: PropertyRecord.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_SavePropertyActivity]
	@propertyId varchar(50),
	@createdBy int,
	@inspectionTypeId int,
	@createDate date,
	@statusId int,
	@actionId int,
	@notes nvarchar(1000),		
	@isLetterAttached bit,
	@isDocumentAttached bit,
	@journalHistoryId int out
AS
BEGIN
	
      Declare @UserId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
   	--Update the Journal
	UPDATE [dbo].[AS_JOURNAL]
	SET [PROPERTYID] = @propertyId
	  ,[STATUSID] = @statusId
	  ,[ACTIONID] = @actionId
	  ,[INSPECTIONTYPEID] = @inspectionTypeId
	  ,[CREATIONDATE] = @createDate
	  ,[CREATEDBY] = @createdBy	
	WHERE propertyid = @propertyid AND isCurrent = 1
	
	Declare @journalId int
	
	SELECT @journalId = JournalId  FROM AS_JOURNAL WHERE propertyid = @propertyid AND isCurrent = 1
	
	--Insert Record into Journal History
	INSERT INTO [dbo].[AS_JOURNALHISTORY]
           ([JOURNALID]
           ,[PROPERTYID]
           ,[STATUSID]
           ,[ACTIONID]
           ,[INSPECTIONTYPEID]
           ,[CREATIONDATE]
           ,[CREATEDBY]
           ,[NOTES]
           ,[ISLETTERATTACHED]           
           ,[IsDocumentAttached])
     VALUES
           (@journalId
           ,@propertyId
           ,@statusId
           ,@actionId
           ,@inspectionTypeId
           ,@createDate
           ,@createdBy
           ,@notes
           ,@isLetterAttached
		   ,@isDocumentAttached)     
   SELECT @journalHistoryId =  @@identity  
   
   
	Declare @statusHistoryId int
	SELECT @statusHistoryId = MAX(StatusHistoryId) FROM AS_StatusHistory WHERE StatusId=@statusId
   
	Declare @actionHistoryId int
	SELECT @actionHistoryId = MAX(ActionHistoryId) FROM AS_ActionHistory  where ActionId=@actionId
   
    --Update Journal Again With Status History and Action History Id
	UPDATE [AS_JOURNALHISTORY]
	SET [StatusHistoryId] = @statusHistoryId
      ,[ActionHistoryId] = @actionHistoryId
      
	WHERE JOURNALHISTORYID = @journalHistoryId



   
	
END