/****** Object:  StoredProcedure [dbo].[AS_GetDefectCategory]    Script Date: 11/08/2012 16:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetDefects
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/06/2012>
-- Description:	<Description,,Get Defects >
-- Web Page: PropertyRecord.aspx => Add Defect Tab
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetDefectCategory]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM P_DEFECTS_CATEGORY
END
