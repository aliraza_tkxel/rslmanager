--=============================================
-- EXEC AS_DeletePropertyDocumentsByDocumentId
-- @documentId = 4
-- Author:		<Aamir Waheed>
-- Create date: <14-Dec-2013>
-- Description:	<Delete Property Document By Document Id>
-- WebPage: PropertyRecord.aspx > Document Tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_DeletePropertyDocumentsByDocumentId]

	@documentId int,
	@documentPath nvarchar(1000) OUTPUT
	
AS
BEGIN
	--  Get path of document to delete it from hard disk.
	SELECT @documentPath = DocumentPath FROM P_Documents WHERE DocumentId = @documentId
	
	-- Delete the document record from database.	
	DELETE FROM P_Documents	WHERE DocumentId = @documentId

END