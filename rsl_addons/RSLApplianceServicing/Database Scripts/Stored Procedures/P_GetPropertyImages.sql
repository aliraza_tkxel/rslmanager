USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetPropertyImages]    Script Date: 10/02/2015 19:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC P_GetPropertyImages
--@propertyId = 'BHA0000119'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,, 17 Sep,2013>
-- Description:	<Description,,This stored procedure returns all the images against the property>
--Modified by: Raja Aneeq
--modification date: 2/10/2015
-- =============================================
ALTER PROCEDURE [dbo].[P_GetPropertyImages] 
	@propertyId as varchar(20)
	,@count int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 SELECT 
		PA_PROPERTY_ITEM_IMAGES.ImagePath 
		,ImageName
		,'Stock' as Type
		,COnvert(varchar(10),CreatedOn,103) as CreatedOn
		,(ISNULL(E__EMPLOYEE.FirstNAME,'')+' '+ISNULL(E__EMPLOYEE.LastName,'')) as CreatedBy
		,PA_PROPERTY_ITEM_IMAGES.Title
	FROM
		PA_PROPERTY_ITEM_IMAGES
		INNER JOIN E__EMPLOYEE on PA_PROPERTY_ITEM_IMAGES.CreatedBy = E__EMPLOYEE.EmployeeId
		INNER JOIN P__PROPERTY ON P__PROPERTY.PropertyPicId = PA_PROPERTY_ITEM_IMAGES.SID
		
	WHERE 
		PA_PROPERTY_ITEM_IMAGES.PROPERTYID=@propertyId
			
    SET @count =	(SELECT COUNT(*) from PA_PROPERTY_ITEM_IMAGES
					INNER JOIN P__PROPERTY 
					ON P__PROPERTY.PropertyPicId = PA_PROPERTY_ITEM_IMAGES.SID
					AND PA_PROPERTY_ITEM_IMAGES.PROPERTYID=@propertyId)
     	
									
    
    
    	
	print @count
	----UNION ALL 				
	--	SELECT distinct
	--	ImagePath as ImagePath
	--	,ImageTitle as ImageName
	--	,'Gas' as Type
	--FROM
	--	P_PROPERTY_APPLIANCE_DEFECTS_IMAGES		
	--	INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.FaultId = P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.propertydefectid
	--	WHERE FL_FAULT_LOG.propertyId= @propertyId	
END
