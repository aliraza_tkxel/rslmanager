USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  10/09/2015
	Description: Get Defect Arranged List to show in defect scheduling.
    Change History:    
    
Execution Command:
----------------------------------------------------

DECLARE	@totalCount int

EXEC	[dbo].[DF_GetDefectsArrangedList] @sortColumn = 'Address',
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_GetDefectsArrangedList](
		
		@searchText varchar(5000)='',		
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'AppointmentDateSort',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 
								AND ISNULL(M.isPending,1) = 0
								AND DS.TITLE IN (''Arranged'', ''No Entry'', ''Paused'', ''In Progress'') '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
		END
				
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
											INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
											INNER JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
											INNER JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
											INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
											INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
											INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
											INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
											INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID											
									'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')
									 RIGHT(''00000'' + CONVERT(NVARCHAR, AD.PropertyDefectId), 5)				AS JSD
									,P.HOUSENUMBER + '' '' + P.ADDRESS1 + '', '' + P.TOWNCITY					AS Address
									,P.POSTCODE																	AS Postcode
									,AT.APPLIANCETYPE															AS Appliance
									,DC.Description																AS Defect
									,CASE
										DS.TITLE
										WHEN ''No Entry''
											THEN RTRIM(DS.TITLE) + ''('' + CONVERT(NVARCHAR,
												(
													SELECT
														COUNT(ADH.IDENTITYCOL)
													FROM
														P_PROPERTY_APPLIANCE_DEFECTS_HISTORY ADH
													WHERE
														ADH.PropertyDefectId = AD.PropertyDefectId
														AND ADH.DefectJobSheetStatus = DS.STATUSID
												)
												) + '')''
										ELSE DS.TITLE
									END																			AS DefectStatus
									,E.FIRSTNAME + '' '' + E.LASTNAME											AS Engineer
									,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)
									+ '' - '' + CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTENDTIME), 108)	AS AppointmentTimes
									,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
									+ '' '' + CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)					AS AppointmentDate
									,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME							AS AppointmentDateSort
									,AD.PropertyDefectId														AS DefectId
									,A.APPOINTMENTID															AS AppointmentId
									,P.HouseNumber																AS HouseNumber									
							'
							
			--============================Group Clause==========================================
			SET @GroupClause = CHAR(10) + ' '
					
			
			--============================Order Clause==========================================
			
			IF (@sortColumn = 'Address') BEGIN
				SET @sortColumn = CHAR(10) + ' CAST(SUBSTRING(HouseNumber, 1,CASE when patindex(''%[^0-9]%'',HouseNumber) > 0 THEN PATINDEX(''%[^0-9]%'',HouseNumber) - 1 ELSE LEN(HouseNumber) END) AS INT) '
			END
							
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @GroupClause + @orderClause 
			--PRINT @mainSelectQuery
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn + CHAR(10) + @sortOrder +CHAR(10) + ') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			-- PRINT @rowNumberQuery
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			PRINT @finalQuery
			--============================ Exec Final Query =================================
						
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= ' SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================

END