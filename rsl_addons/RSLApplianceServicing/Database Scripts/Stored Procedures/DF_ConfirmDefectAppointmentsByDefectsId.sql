USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Confirm Defect Appointments.	
    Change History:    

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_ConfirmDefectAppointmentsByDefectsId](
		 @defectIds NVARCHAR(4000),		 
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
		
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE MS
	SET isPending = 0
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
INNER JOIN PDR_MSAT MS ON PJ.MSATID = MS.MSATId
INNER JOIN dbo.SPLIT_STRING(@defectIds,',') Defects ON AD.PropertyDefectId = Defects.COLUMN1
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END