USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Rearrange Defect Appointment and make associated defect availalbe for rearrange.
    Change History:    

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_RearrangeDefectAppointment](
		 @appointmentId INT,		 
		 @userId INT,
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
		
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE AD
	SET DefectJobSheetStatus = NULL
		,ApplianceDefectAppointmentJournalId = NULL
		,ModifiedBy = @userId
		,ModifiedDate = GETDATE()
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END