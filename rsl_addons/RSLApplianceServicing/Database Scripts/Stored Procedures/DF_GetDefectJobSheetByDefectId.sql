USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  10/09/2015
	Description: Get Job Sheet Details (Arranged Job Sheet)
    Change History:    
    
Execution Command:
----------------------------------------------------

EXEC	[dbo].[DF_GetDefectJobSheetByDefectId] @defectId = '6185',

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_GetDefectJobSheetByDefectId](
		 @defectId INT
)
AS
BEGIN
	DECLARE @propertyId NVARCHAR(20)
			,@JournalId INT
			,@defectApppointmentJournalId INT

SELECT
	@propertyId = PropertyId
	,@JournalId = JournalId
	,@DefectApppointmentJournalId = ApplianceDefectAppointmentJournalId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS
WHERE
	PropertyDefectId = @defectId


-- Get Defects by @defectApppointmentJournalId
SELECT
	AD.PropertyDefectId	AS [DefectId:]
	,AT.APPLIANCETYPE	AS [Appliance:]
	,T.Description		AS [Trade:]
	,CASE
		AD.IsTwoPersonsJob
		WHEN 1
			THEN 'Yes'
		ELSE 'No'
	END					AS [2 Person:]
	,CASE
		AD.IsDisconnected
		WHEN 1
			THEN 'Yes'
		WHEN 0
			THEN 'No'
		ELSE 'N/A'
	END					AS [Disconnected:]
	,CONVERT(NVARCHAR, CAST(AD.Duration AS INT)) + CASE
		WHEN AD.Duration > 1
			THEN ' Hours'
		ELSE ' Hour'
	END					AS [Duration:]
	,AD.PartsDue		AS [Parts Due:]
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN GS_PROPERTY_APPLIANCE A ON AD.ApplianceId = A.PROPERTYAPPLIANCEID
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		INNER JOIN G_TRADE T ON AD.TradeId = T.TradeId
WHERE
	AD.ApplianceDefectAppointmentJournalId = @defectApppointmentJournalId

-- Get Defect Appointment Details
SELECT DISTINCT
	AD.JournalId										AS InspectionRef
	,E.FIRSTNAME + ' ' + E.LASTNAME						AS Operative
	,A.DURATION											AS Duration
	,T.Description										AS Trade
	,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME	AS StartDateTime
	,A.APPOINTMENTENDDATE + A.APPOINTMENTENDTIME		AS EndDateTime
	,A.APPOINTMENTNOTES									AS AppointmentNotes
	,A.CUSTOMERNOTES									AS JobSheetNotes
	,A.APPOINTMENTID									AS AppointmentId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
		INNER JOIN G_TRADE T ON A.TRADEID = T.TradeId
		INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID
WHERE
	AD.ApplianceDefectAppointmentJournalId = @defectApppointmentJournalId

-- Get Property and Tenant Details
EXEC PLANNED_GetPropertyDetail @propertyId

END