USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_PopulateAllDropDownList]    Script Date: 10/16/2015 21:01:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Populate All DropDown List
 
    Author: Ali Raza
    Creation Date: Dec-17-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-17-2014      Ali Raza           Populate All DropDown List
    v1.1         Oct-6-2015     Raja Aneeq          Exec a new SP PDR_GetTenureSelectionType
    Execution Command:
    
    Exec PDR_PopulateAllDropDownList 
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_PopulateAllDropDownList]

AS
BEGIN
--get Property Templates
Exec PDR_GetPropertyTemplate

--get Property Development
Exec PDR_GetDevelopment
--get Property Stock type
Exec PDR_GetPropertyStockType
--get Property Ownership
Exec PDR_GetPropertyOwnerShip
--get Property Status
Exec PDR_GetPropertyStatus
--get Property Type
Exec PDR_GetPropertyType
--get Property Dwelling Type
Exec PDR_GetPropertyDwellingType

--get Property Level
Exec PDR_GetPropertyLevel

--get Property Asset Type
Exec PDR_GetPropertyAssetType

--get Property NROSH Asset Type1
Exec PDR_GetPropertyNROSHAssetTypeMain


--get Tenure Type for dropdown
Exec PDR_GetTenureSelectionType

END
