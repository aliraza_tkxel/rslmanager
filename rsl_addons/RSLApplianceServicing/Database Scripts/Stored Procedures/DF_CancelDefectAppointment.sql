USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Cancel Defect Appointment and update cancel notes
    Change History:    

----------------------------------------------------
*/

ALTER PROCEDURE [dbo].[DF_CancelDefectAppointment](
		 @appointmentId INT,
		 @cancelReasonNotes NVARCHAR(1000),
		 @userId INT,
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
	DECLARE @Now DATETIME2 = GETDATE()
	
	DECLARE @CancelledStatusId INT
	DECLARE @CancelledStatus NVARCHAR(200) = 'Cancelled'
	SELECT @CancelledStatusId = STATUSID
	FROM PDR_STATUS
	WHERE RTRIM(TITLE) = @CancelledStatus
	
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE AD
	SET DefectJobSheetStatus = @CancelledStatusId
		,CancelNotes = @cancelReasonNotes
		,ModifiedBy = @userId
		,ModifiedDate = @Now
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

-- Update journal Entry and get histroy identifier
UPDATE PJ
	SET STATUSID = @CancelledStatusId		
FROM PDR_JOURNAL PJ
INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

DECLARE @JournalHistoryId INT
SELECT @JournalHistoryId = MAX(A.JOURNALHISTORYID)
FROM PDR_APPOINTMENTS A
INNER JOIN PDR_JOURNAL_HISTORY PJH	ON A.JOURNALID = PJH.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

-- Update Appointment
UPDATE PDR_APPOINTMENTS
	SET APPOINTMENTSTATUS = @CancelledStatus
		,JOURNALHISTORYID = @JournalHistoryId		
WHERE APPOINTMENTID = @appointmentId

-- Insert Entry into Cancel table
INSERT INTO PDR_CANCELLED_JOBS(AppointmentId, RecordedOn, Notes)
VALUES (@appointmentId, @Now, @cancelReasonNotes)
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END