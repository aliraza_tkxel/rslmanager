USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyDetailByPropertyId]    Script Date: 10/16/2015 21:46:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Detail By PropertyId for Template
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Detail By PropertyId for Template
    v1.1		 July-23-2015	  Salman Nazir	   Add two new columns 'TitleNumber' and 'DeedLocation'   
    
    Execution Command:
    ==================
    Exec PDR_GetPropertyDetailByPropertyId
  =================================================================================*/

ALTER PROCEDURE [dbo].[PDR_GetPropertyDetailByPropertyId]
(@PropertyId varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT PROPERTYID,PATCH,DEVELOPMENTID,BLOCKID,HOUSENUMBER,FLATNUMBER,ADDRESS1,ADDRESS2,ADDRESS3,TOWNCITY,COUNTY,POSTCODE,STATUS,SUBSTATUS,AVDATE,PROPERTYTYPE, ASSETTYPE,STOCKTYPE,
PROPERTYLEVEL,DATEBUILT,VIEWINGDATE,LETTINGDATE,RIGHTTOBUY,UNITDESC,PRACTICALCOMPLETIONDATE, HOUSINGOFFICER, DEFECTSPERIODEND,DATETIMESTAMP,SAP,OWNERSHIP,MINPURCHASE,PURCHASELEVEL,
DWELLINGTYPE, NROSHASSETTYPEMAIN, NROSHASSETTYPESUB, FUELTYPE, PropertyPicId, SCHEMEID,PROPERTYVALUE,IsTemplate ,TemplateName,PhaseId,LeaseStart,LeaseEnd,FloodingRisk,DateAcquired,
viewingcommencement,Valuationdate,GroundRent,LandLord,RentReview,TitleNumber,DeedLocation
FROM P__PROPERTY P 
Where P.PROPERTYID = @PropertyId
END
