USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetTenureSelectionType]    Script Date: 10/16/2015 21:15:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
   

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-6-2015      Raja Aneeq           Get Tenure Type for dropdown
    
    Execution Command:
    
    Exec PDR_GetTenureSelectionType
  =================================================================================*/

ALTER PROCEDURE [dbo].[PDR_GetTenureSelectionType]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT TENURETYPEID,TENURETYPE FROM P_TENURESELECTION 
	
END
