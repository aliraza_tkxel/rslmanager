USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  04/08/2015
	Description: Schedule Defect Appointment
    Change History:    

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_ScheduleAppointment](
		@journalId INT
		,@appointmentStartDateTime DATETIME
		,@appointmentEndDateTime DATETIME
		,@operativeId INT
		,@schedularId INT
		,@appointmentNotes NVARCHAR(1000)
		,@jobSheetNotes NVARCHAR(1000)
		,@tradeId INT
		,@duration float
		,@defectIds NVARCHAR(500)
		
		-- Output Parameters to return values
		,@appointmentId INT OUTPUT
		,@saveStatus BIT OUTPUT
) 
AS
BEGIN

DECLARE @currentTimeStamp DATETIME2 = GETDATE()
DECLARE @Today DATE = @currentTimeStamp

SET NOCOUNT ON;
	
BEGIN TRANSACTION
BEGIN TRY

-- Get Appointment Type Id (MSATTypeId) from PDR_MSATType by giving MSATTypeName
DECLARE @appointmentTypeId INT
SELECT @appointmentTypeId = MSATTypeId FROM PDR_MSATType WHERE MSATTypeName = 'Appliance Defect'

-- Get JournalStatusId by Status sting from PDR_STATUS
DECLARE @JournalStatusId INT
SELECT @JournalStatusId = STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'

-- Get Tenancy Detail
DECLARE @TenancyId INT
SELECT @TenancyId = T.TENANCYID
FROM AS_JOURNAL AJ
LEFT JOIN C_TENANCY T ON AJ.PROPERTYID = T.PROPERTYID AND (T.ENDDATE IS NULL OR T.ENDDATE >= @Today)
WHERE AJ.JOURNALID = @journalId

-- Insert an entry in PDR_MSAT and PRD_Journal and get its key(s)
DECLARE @MSATId INT
INSERT INTO PDR_MSAT(PropertyId, MSATTypeId, IsActive, TenancyId, isPending)
SELECT PROPERTYID AS PropertyId, @appointmentTypeId AS MSATTypeId, 1 AS ISACTIVE, @TenancyId AS TenancyId, 1 As isPending
FROM AS_JOURNAL WHERE JOURNALID = @journalId
SELECT @MSATId = SCOPE_IDENTITY()

-- --------
DECLARE @ApplianceDefectAppointmentJournalId INT
INSERT INTO PDR_JOURNAL(MSATID, STATUSID, CREATIONDATE, CREATEDBY)
VALUES (@MSATId, @JournalStatusId, @currentTimeStamp, @schedularId)
SELECT @ApplianceDefectAppointmentJournalId = SCOPE_IDENTITY()

DECLARE @pdrJournalHistoryId BIGINT
SELECT @pdrJournalHistoryId = MAX(JOURNALHISTORYID) FROM PDR_JOURNAL_HISTORY WHERE JOURNALID = @ApplianceDefectAppointmentJournalId

-- Insert an entry in PDR_Appointment and get its key for output

INSERT INTO PDR_APPOINTMENTS(TENANCYID, JOURNALID, JOURNALHISTORYID, APPOINTMENTSTARTDATE, APPOINTMENTENDDATE
		, APPOINTMENTSTARTTIME, APPOINTMENTENDTIME
		, ASSIGNEDTO, CREATEDBY, LOGGEDDATE, APPOINTMENTNOTES, CUSTOMERNOTES
		, APPOINTMENTSTATUS, TRADEID, DURATION)
VALUES(@TenancyId, @ApplianceDefectAppointmentJournalId, @pdrJournalHistoryId, CONVERT(DATE,@appointmentStartDateTime), CONVERT(DATE, @appointmentEndDateTime)
		, LTRIM(REPLACE(REPLACE(SUBSTRING(CONVERT(NVARCHAR, @appointmentStartDateTime, 0),13,7),'AM',' AM'), 'PM',' PM')), LTRIM(REPLACE(REPLACE(SUBSTRING(CONVERT(NVARCHAR, @appointmentEndDateTime, 0),13,7),'AM',' AM'), 'PM',' PM'))
		, @operativeId, @schedularId, @currentTimeStamp, @appointmentNotes, @jobSheetNotes
		, 'NotStarted', @tradeId, @duration)

SELECT @appointmentId = SCOPE_IDENTITY()

-- Update Defects (Defect Job Sheets)
UPDATE P_PROPERTY_APPLIANCE_DEFECTS
	SET DefectJobSheetStatus = @JournalStatusId
		,ApplianceDefectAppointmentJournalId = @ApplianceDefectAppointmentJournalId
WHERE PropertyDefectId IN (SELECT COLUMN1 FROM dbo.SPLIT_STRING(@defectIds, ','))

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END
END