USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[SavePropertyImage]    Script Date: 10/02/2015 19:12:03 ******/
--Created By : Raja Aneeq
--created date 2/10/2015
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SavePropertyImage]

	@imagename nvarchar(100),
	@propertyId nvarchar(100),
	@imagePath nvarchar(1000),
	@createdBy nvarchar(100),
	@isDefault Bit
	AS
	BEGIN
	insert into PA_PROPERTY_ITEM_IMAGES (PROPERTYID,ImagePath,ImageName,CreatedOn,CreatedBy,Isdefault)
	VALUES(@propertyId,@imagePath,@imagename,GETDATE(),@createdBy,@isDefault)
	
	update P__PROPERTY set PropertyPicId = SCOPE_IDENTITY() where  PROPERTYID=@propertyId

	END





