USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_Statuses]    Script Date: 10/11/2012 12:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Salman Nazir>
-- Create date: <23/09/2012>
-- Description:	<This Stored Proceedure shows the statuses on the Resoucres Page, Status Listing >
-- =============================================
ALTER PROCEDURE [dbo].[AS_Statuses]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AS_Status.Title,AS_Status.Ranking,AS_Status.StatusId,InspectionTypeID,IsEditable FROM AS_Status
	Order BY Ranking
END
