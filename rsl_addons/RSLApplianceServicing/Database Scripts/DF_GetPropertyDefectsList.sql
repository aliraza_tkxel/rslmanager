-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Property Defects List
-- History:          11/09/2015 Noor : Created the stored procedure
--                   11/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------

EXEC	@return_value = [dbo].[DF_GetPropertyDefectsList]
		@propertyId = N'A010140003'

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/
Create PROCEDURE DF_GetPropertyDefectsList
	-- Add the parameters for the stored procedure here
	@propertyId varchar(20) = '' 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
    P_DEFECTS_CATEGORY.Description AS Category 
	,GS_APPLIANCE_TYPE.APPLIANCETYPE AS ApplianceType
	,P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy AS RecordedBy
	,P_PROPERTY_APPLIANCE_DEFECTS.IsActionTaken As IsActionTaken
	,P_PROPERTY_APPLIANCE_DEFECTS.IsDisconnected As IsDiconnected
	,P_PROPERTY_APPLIANCE_DEFECTS.IsPartsOrdered As IsPartsOrdered
	,'N/A' As PartsFitted
	FROM P_PROPERTY_APPLIANCE_DEFECTS
	INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	INNER JOIN GS_APPLIANCE_TYPE ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
	WHERE P_PROPERTY_APPLIANCE_DEFECTS.PropertyId = @propertyId
END
GO
