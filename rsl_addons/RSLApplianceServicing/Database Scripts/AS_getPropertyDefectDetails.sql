USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_getPropertyDefectDetails]    Script Date: 09/14/2015 16:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_getPropertyDefectDetails  @propertyDefectId = 3186  
-- Author:  <Salman Nazir>  
-- Create date: <13/11/2012> 
-- Updated by: <Raja Aneeq>
-- Update date: <14/9/2015> 
-- Description: <This Store Procedure shall get the details of a Defect>  
-- WebPage: PropertyRecord.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_getPropertyDefectDetails](  
@propertyDefectId int  
)  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;

-- Insert statements for procedure here  
SELECT
	PropertyDefectId								AS PropertyDefectId
	,AD.PropertyId									AS PropertyId
	,AD.CategoryId									AS CategoryId
	,DC.Description									AS DefectCategory
	,DefectDate										AS DefectDate
	,JournalId										AS JournalId
	,ISNULL(IsDefectIdentified, 0)					AS IsDefectIdentified
	,ISNULL(DefectNotes, '')						AS DefectNotes
	,ISNULL(IsActionTaken, 0)						AS IsRemedialActionTaken
	,ISNULL(ActionNotes, '')						AS RemedialActionNotes
	,ISNULL(IsWarningIssued, 0)						AS IsWarningIssued
	,ApplianceId									AS ApplianceId
	,ISNULL(AD.SerialNumber, '')					AS SerialNumber
	,ISNULL(AD.GasCouncilNumber, '')					AS GcNumber
	,ISNULL(IsWarningFixed, 0)						AS IsWarningFixed
	,AD.IsDisconnected								AS IsApplianceDisconnected
	,AD.IsPartsrequired								AS IsPartsRequired
	,AD.IsPartsOrdered								AS IsPartsOrdered
	,AD.PartsOrderedBy								AS PartsOrderedBy
	,ISNULL(POB.FIRSTNAME + ' ' + POB.LASTNAME, '')	AS PartsOrderedByName
	,AD.PartsDue									AS PartsDueDate
	,ISNULL(AD.PartsDescription,'')					AS PartsDescription
	,ISNULL(AD.PartsLocation,'')					AS PartsLocation
	,ISNULL(AD.IsTwoPersonsJob, 0)					AS IsTwoPersonsJob
	,ISNULL(AD.ReasonFor2ndPerson,'')				AS ReasonForSecondPerson
	,AD.Duration									AS EstimatedDuration
	,AD.Priority									AS PriorityId
	,ISNULL(DP.PriorityName,'N/A')					AS PriorityName
	,AD.TradeId										AS TradeId
	,ISNULL(T.Description,'N/A')					AS Trade
	,ISNULL(PhotoNotes,'')							AS PhotoNotes
	,ISNULL(AT.APPLIANCETYPE,'')					AS APPLIANCE
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN GS_PROPERTY_APPLIANCE A ON AD.ApplianceId = A.PROPERTYAPPLIANCEID
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		LEFT JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
		LEFT JOIN E__EMPLOYEE POB ON AD.PartsOrderedBy = POB.EMPLOYEEID
		LEFT JOIN G_TRADE T ON AD.TradeId = T.TradeId
		LEFT JOIN P_DEFECTS_PRIORITY DP ON AD.Priority = DP.PriorityID
WHERE
	PropertyDefectId = @propertyDefectId
END