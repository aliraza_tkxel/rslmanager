USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyDocumentInformationById]    Script Date: 05/15/2015 20:01:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

--=============================================
-- EXEC AS_GetLatestEpcDocument
-- @documentId = 4
-- Author:		<Noor Muhamamd>
-- Create date: <24-Apr-2015>
-- Description:	<Get Latest EPC document with title certificate of property>
-- WebPage: PropertyRecord.aspx > Document Tab
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetLatestEpcDocument]
	
AS
BEGIN

	--Note:
	--document type id = 4 = EPC 
	--document subtype id = 12 = Certificate
	-- Get Document Complete Information to show document on client side.
	SELECT	top 1 DocumentId, PropertyId, DocumentName, DocumentPath, DocumentType, Keywords, CreatedDate, DocumentSize, DocumentFormat ,DocumentTypeId,DocumentSubTypeId
	FROM	P_Documents
	WHERE documentTypeId = 4 AND DocumentSubTypeId = 12	
	ORDER BY CREATEDDATE DESC
END

