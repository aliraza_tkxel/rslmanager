USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetSignOff]    Script Date: 10/15/2012 16:41:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 15/10/2012
-- Description:	This SP displays all the Sign Off values. 
-- Useage: View Letter
-- EXEC [dbo].[AS_GetSignOff] 
-- IMPORTANT: Please make suer that there's a entry in the AS_LookupType which has the TypeName as "SignOff"
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetSignOff]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		AS_LookupCode.LookupCodeId, 
		AS_LookupCode.CodeName 	
	FROM 
		AS_LookupCode INNER JOIN
		AS_LookupType on AS_LookupCode.LookupTypeId=AS_LookupType.LookupTypeId
	WHERE 
		AS_LookupType.TypeName = 'SignOff'
END
