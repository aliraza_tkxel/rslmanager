BEGIN TRANSACTION
	BEGIN TRY  
-- Internal > Services > Smoke/CO
DECLARE @itemid int;
DECLARE @parItemId int;
INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Sundry',5,1,0,0)

set @parItemId = SCOPE_IDENTITY()
 
 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Furniture',1,1,@parItemId,0,1)


 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Office Equipment',2,1,@parItemId,0,1)

INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Gardening Tools',3,1,@parItemId,0,1)

 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Furnishings',4,1,@parItemId,0,1)

 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Catering equipment',5,1,@parItemId,0,1)

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 