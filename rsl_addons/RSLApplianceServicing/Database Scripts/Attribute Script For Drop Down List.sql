/* ===========================================================================
--  Author:			Ali Raza
--  DATE CREATED:	26 Sep,2014
--  Description:	Insertion and Moving attribute to PA_PROPERTY_ATTRIBUTE table scripts
--					
 '==============================================================================*/
	BEGIN TRANSACTION
	BEGIN TRY    

 DECLARE @itemName nvarchar(100),
 @attribute nvarchar(150), @location nvarchar(150),  @area nvarchar(150),  @item nvarchar(150), @parameterName nvarchar(150),
    @dataType nvarchar(150), @controlType nvarchar(150),  @table nvarchar(150)   
	
	  ---Declare Cursor variable  
   DECLARE @ItemToFetchCursor CURSOR  
   --Initialize cursor  
   Set @ItemToFetchCursor = CURSOR FAST_FORWARD FOR SELECT  PropertyAttribute,Location,Area,Item,ParameterName,DataType,ControlType,TABLES
  FROM  PA_Temp_Property_Attribute_Sync
  Where ControlType='Dropdown';  
   --Open cursor  
   OPEN @ItemToFetchCursor  
 ---fetch row from cursor  
   FETCH NEXT FROM @ItemToFetchCursor INTO @attribute, @location,@area,@item,@parameterName,@dataType,@controlType,@table
  ---Iterate cursor to get record row by row  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
  --Declare variables for set values of internal queries
  Declare @sql nvarchar(500),@areaId INT=0, @itemId INT=0,@parameterSOrder INT=0, @parameterId INT =0,@itemParamId INT=0 ,
   @propertyId nvarchar(150), @attributeValue nvarchar(150)  ,@valueDetail nvarchar(150), @parameterValueId INT=0
  --Select ItemId and AreaId based on  location,Area and Item text.
 Select @itemId=ItemId ,@areaId =PA_AREA.AreaID FROM PA_ITEM
  INNER JOIN PA_AREA ON PA_ITEM.AreaID=PA_AREA.AreaID
  INNER JOIN PA_LOCATION ON PA_AREA.LocationId=PA_LOCATION.LocationID
  where PA_LOCATION.LocationName=convert(varchar, @location)  AND PA_AREA.AreaName=convert(varchar, @area)  AND PA_ITEM.ItemName=convert(varchar, @item)
--If itemId not exist, then get the area id based on location and area text. 
  IF (@itemId=0)
  BEGIN
  Select @areaId =PA_AREA.AreaID FROM PA_AREA  
  INNER JOIN PA_LOCATION ON PA_AREA.LocationId=PA_LOCATION.LocationID
  where PA_LOCATION.LocationName=convert(varchar, @location)  AND PA_AREA.AreaName=convert(varchar, @area) -- AND PA_ITEM.ItemName=convert(varchar, @item)
 
  PRINT '+++++'+convert(varchar, @areaId)+'+++++++++'+convert(varchar, @controlType)+'++++++++++++++'
  --insert the item in database and get the itemId
		 INSERT INTO PA_ITEM(AreaID,ItemName,ItemSorder,IsActive,ShowInApp)
			  VALUES(@areaId, @item,10,1,0)
		 SET @itemId = Scope_identity()
  END
  --ELSE
  --BEGIN
  
  PRINT '+++++'+convert(varchar, @areaId)+'+++++++++'+convert(varchar, @controlType)+'++++++++++++++'
  --get parameter Sort Order and add 1 with existing sort order
	SET @parameterSOrder=(select Top 1 ParameterSorder from PA_ITEM_PARAMETER
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId=PA_PARAMETER.ParameterId
	where ItemID=@itemId ORDER BY ParameterSorder DESC)
	SET @parameterSOrder = @parameterSOrder + 1
-- Insert data in PA_PARAMETER and get the inserted parameter id to insert in PA_ITEM_PARAMETER with ItemId 
		INSERT INTO PA_PARAMETER( ParameterName, DataType, ControlType, IsDate, ParameterSorder,IsActive,ShowInApp)
		VALUES(@parameterName,@dataType,@controlType,0,@parameterSOrder,1,0)
		
		 SET @parameterId = Scope_identity()
		
--Insert data in PA_ITEM_PARAMETER and get inserted ItemParamId to insert in PA_PROPERTY_ATTRIBUTES		    
		INSERT INTO PA_ITEM_PARAMETER( ItemId , ParameterId, IsActive )
		VALUES(@itemId, @parameterId, 1) 
		 SET @itemParamId = Scope_identity()
		 
	If @table='P_HEATING'
		BEGIN
			INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
			SELECT @parameterId,DESCRIPTION,HEATINGID,1 FROM P_HEATING
		END
	ELSE IF @table='P_GARDEN'
		BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
			SELECT @parameterId,DESCRIPTION,GARDENID,1 FROM P_GARDEN
		END
		 
	ELSE IF @table='P_FURNISHING'
		BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
			SELECT @parameterId,DESCRIPTION,FURNISHINGID,1 FROM P_FURNISHING
		END	 
	ELSE IF @table='P_SECURITY'
		BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
			SELECT @parameterId,DESCRIPTION,SECURITYID,1 FROM P_SECURITY
		END	 
	ELSE IF @table='P_ACCESSIBILITYSTANDARDS'
		BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
			SELECT @parameterId,DESCRIPTION,SID,1 FROM P_ACCESSIBILITYSTANDARDS
		END	 	 
	ELSE IF @table='Yes/No'
		BEGIN
		INSERT INTO PA_PARAMETER_VALUE(ParameterID,ValueDetail,Sorder,IsActive)
		VALUES(@parameterId,'Yes',1,1),(@parameterId,'No',2,1)		
		END	 	 
		 
 --Create temp table to insert propertyId And Attribute to migrate data from P_ATTRIBUTES to PA_PROPERTY_ATTRIBUTES
		 CREATE TABLE #Attributes(
				attribute nvarchar(100)
				,PropertyId nvarchar(100)
								
			)
		  --fetch record from P_ATTRIBUTE
		SET @sql ='INSERT INTO #Attributes(attribute,PropertyId)select '+@attribute+', PropertyId from P_ATTRIBUTES '
		PRINT @sql
		EXECUTE (@sql)
						
				---Declare Cursor variable  
				DECLARE @ItemToInsertCursor CURSOR  
				--Initialize cursor  
				Set @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT  attribute,PropertyId
				FROM  #Attributes;  
				--Open cursor  
				OPEN @ItemToInsertCursor  
				---fetch row from cursor  
				FETCH NEXT FROM @ItemToInsertCursor INTO @attributeValue, @propertyId
				---Iterate cursor to get record row by row  
					WHILE @@FETCH_STATUS = 0  
					BEGIN 
					set @parameterValueId = 0 
					If @table='P_HEATING'
						BEGIN							
							SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
							ValueDetail=(SELECT DESCRIPTION FROM P_HEATING WHERE HEATINGID=@attributeValue )
						END
					ELSE IF @table='P_GARDEN'
						BEGIN
							SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
							ValueDetail=(SELECT DESCRIPTION FROM P_GARDEN WHERE GARDENID=@attributeValue )
						
						END

					ELSE IF @table='P_FURNISHING'
						BEGIN
							SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
							ValueDetail=(SELECT DESCRIPTION FROM P_FURNISHING WHERE FURNISHINGID=@attributeValue )						
						END	 
					ELSE IF @table='P_SECURITY'
						BEGIN
						SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
							ValueDetail=(SELECT DESCRIPTION FROM P_SECURITY WHERE SECURITYID=@attributeValue )	
						
						END	 
					ELSE IF @table='P_ACCESSIBILITYSTANDARDS'
						BEGIN
						SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
							ValueDetail=(SELECT DESCRIPTION FROM P_ACCESSIBILITYSTANDARDS WHERE SID=@attributeValue )
						END	 	 
					ELSE IF @table='Yes/No'
						BEGIN
						IF @attributeValue='1'
							BEGIN
							SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
								ValueDetail='Yes'
							END
						ELSE IF @attributeValue='0'
							BEGIN 
							SELECT @parameterValueId=VALUEID,@valueDetail = ValueDetail FROM PA_PARAMETER_VALUE WHERE ParameterID=@parameterId AND 
								ValueDetail='No'						
							END
						
						END	
						print @table 	 
						print @parameterValueId
						IF @parameterValueId > 0
						BEGIN
							INSERT INTO PA_PROPERTY_ATTRIBUTES(PROPERTYID, ITEMPARAMID, PARAMETERVALUE, VALUEID, UPDATEDON, UPDATEDBY, IsCheckBoxSelected)
							VALUES(@propertyId,@itemParamId,@valueDetail,@parameterValueId,GETDATE(),943,0) 
						END

					FETCH NEXT FROM @ItemToInsertCursor INTO @attributeValue, @propertyId
					END  
				--close & deallocate cursor    
				CLOSE @ItemToInsertCursor  
				DEALLOCATE @ItemToInsertCursor  	
	DROP TABLE #Attributes		
  --END
  
   FETCH NEXT FROM @ItemToFetchCursor INTO @attribute, @location,@area,@item,@parameterName,@dataType,@controlType,@table
  END  
--close & deallocate cursor    
CLOSE @ItemToFetchCursor  
DEALLOCATE @ItemToFetchCursor  	


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 