SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC  AS_InspectionTypes
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,28/09/2012>
-- Description:	<Description,,This Stored Procedure shows the checkboxes on Add User Panel in Resources Page>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE AS_InspectionTypes
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM P_INSPECTIONTYPE
END
GO
