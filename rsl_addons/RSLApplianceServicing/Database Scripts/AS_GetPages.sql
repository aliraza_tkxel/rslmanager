-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetPages
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,10/17/2012>
-- Description:	<Description,,Get all pages on Access Right Page>
-- Web Page: Access.aspx
-- =============================================
CREATE PROCEDURE AS_GetPages
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM AS_Pages Where AS_Pages.ParentPageID = 0
END
GO
