SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Trigger Description: Add a history entry then a detector is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */

CREATE TRIGGER dbo.TR_P_DETECTOR_INSERT_HISTORY_ENTRY
   ON  dbo.P_DETECTOR
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
INSERT INTO P_DETECTOR_HISTORY (
				DetectorId
				,PropertyId
				,DetectorTypeId
				,Location
				,Manufacturer
				,SerialNumber
				,PowerSource
				,InstalledDate
				,InstalledBy
				,IsLandlordsDetector
				,TestedDate
				,TestedBy
				,BatteryReplaced
				,Passed
				,Notes
				,CTimeStamp
			)
	SELECT
		DetectorId
		,PropertyId
		,DetectorTypeId
		,Location
		,Manufacturer
		,SerialNumber
		,PowerSource
		,InstalledDate
		,InstalledBy
		,IsLandlordsDetector
		,TestedDate
		,TestedBy
		,BatteryReplaced
		,Passed
		,Notes
		,@CTimeStamp
	FROM
		INSERTED

END
GO









SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Trigger Description: Add a history entry when a meter is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */

CREATE TRIGGER dbo.TR_P_METER_INSERT_HISTORY_ENTRY
   ON  dbo.P_METER
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
	INSERT INTO P_METER_HISTORY (
					MeterId
					,PropertyId
					,MeterTypeId
					,Location
					,Manufacturer
					,SerialNumber
					,Installed
					,Reading
					,ReadingDate
					,Passed
					,Notes
					,IsDisconnected
					,CTimeStamp
				)
		SELECT
			MeterId
			,PropertyId
			,MeterTypeId
			,Location
			,Manufacturer
			,SerialNumber
			,Installed
			,Reading
			,ReadingDate
			,Passed
			,Notes
			,IsDisconnected
			,@CTimeStamp
		FROM
			INSERTED

END
GO




/* =================================================================================    
    Trigger Description: Add a history entry when a meter is added or updated.    
 
    Author:			Aamir Waheed
    Creation Date:	23/07/2015
    
    Change History:

    Version     Date            By                Description
    =======     ============    ========          ===========================
    v1.0         23/07/2015     Aamir Waheed      Created trigger to save histroy entry.
    
*================================================================================= */
CREATE TRIGGER dbo.TR_P_PROPERTY_APPLIANCE_DEFECTS_INSERT_HISTORY_ENTRY
   ON  dbo.P_PROPERTY_APPLIANCE_DEFECTS
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @CTimeStamp DATETIME2 = GETDATE()
	INSERT INTO dbo.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY (
					PropertyDefectId
					,PropertyId
					,CategoryId
					,JournalId
					,IsDefectIdentified
					,DefectNotes
					,IsActionTaken
					,ActionNotes
					,IsWarningIssued
					,ApplianceId
					,SerialNumber
					,IsWarningFixed
					,DefectDate
					,CreatedBy
					,DetectorTypeId
					,PhotoNotes
					,GasCouncilNumber
					,IsDisconnected
					,IsPartsrequired
					,IsPartsOrdered
					,PartsOrderedBy
					,PartsDue
					,PartsDescription
					,PartsLocation
					,IsTwoPersonsJob
					,ReasonFor2ndPerson
					,Duration
					,Priority
					,TradeId
					,IsCustomerHaveHeating
					,IsHeatersLeft
					,NumberOfHeatersLeft
					,IsCustomerHaveHotWater
					,DefectJobSheetStatus
					,ApplianceDefectAppointmentJournalId
					,NoEntryNotes
					,CancelNotes
					,CreatedDate
					,ModifiedBy
					,ModifiedDate
					,CTimeStamp
				)
		SELECT
			PropertyDefectId
			,PropertyId
			,CategoryId
			,JournalId
			,IsDefectIdentified
			,DefectNotes
			,IsActionTaken
			,ActionNotes
			,IsWarningIssued
			,ApplianceId
			,SerialNumber
			,IsWarningFixed
			,DefectDate
			,CreatedBy
			,DetectorTypeId
			,PhotoNotes
			,GasCouncilNumber
			,IsDisconnected
			,IsPartsrequired
			,IsPartsOrdered
			,PartsOrderedBy
			,PartsDue
			,PartsDescription
			,PartsLocation
			,IsTwoPersonsJob
			,ReasonFor2ndPerson
			,Duration
			,Priority
			,TradeId
			,IsCustomerHaveHeating
			,IsHeatersLeft
			,NumberOfHeatersLeft
			,IsCustomerHaveHotWater
			,DefectJobSheetStatus
			,ApplianceDefectAppointmentJournalId
			,NoEntryNotes
			,CancelNotes
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,@CTimeStamp
		FROM
			INSERTED
END
GO