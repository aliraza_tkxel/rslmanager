USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_RearrangeDefectAppointment]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Rearrange Defect Appointment and make associated defect availalbe for rearrange.
    Change History:    

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_RearrangeDefectAppointment](
		 @appointmentId INT,		 
		 @userId INT,
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
		
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE AD
	SET DefectJobSheetStatus = NULL
		,ApplianceDefectAppointmentJournalId = NULL
		,ModifiedBy = @userId
		,ModifiedDate = GETDATE()
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectHistoryReport]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      11/09/2015
-- Description:      Get Defect History Report
-- History:          11/09/2015 Noor : Created the stored procedure
--                   11/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetDefectHistoryReport]
		@searchText = N'a',
		@schemeId = NULL,
		@applianceType = 'All',	
		@year = 2015,
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'SchemeName',
		@sortOrder = N'ASC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value


----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDefectHistoryReport](
		
		@searchText varchar(5000)='',
		@schemeId INT = -1,
		@applianceType varchar(500) = 'All',
		@year INT = -1,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'SchemeName',
		@sortOrder varchar (5) = 'ASC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int		

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'						
		END
		
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
		
		IF @applianceType != 'All' AND @applianceType != '-2'
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
		END
		
		IF @year = -1
		BEGIN
			SET @year = YEAR(getdate())
		END							
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P__PROPERTY
										INNER JOIN P_STATUS on P__PROPERTY.STATUS = P_STATUS.STATUSID
										INNER JOIN (
														SELECT PropertyId,DefectDate,ApplianceId,PropertyDefectId 
														FROM
														(
															Select PropertyId,DefectDate,ApplianceId,PropertyDefectId,  
															ROW_NUMBER() OVER(PARTITION BY ApplianceId ORDER BY DefectDate DESC)
															As Record From P_PROPERTY_APPLIANCE_DEFECTS
														) AS t
														Where t.Record = 1
													) 
										AS P_PROPERTY_APPLIANCE_DEFECTS 
										ON 
										P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId
										
										INNER JOIN (
														SELECT PROPERTYAPPLIANCEID,SchemeId,APPLIANCETYPEID 
														FROM GS_PROPERTY_APPLIANCE
													) 
										AS GS_PROPERTY_APPLIANCE 
										ON 
										P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
										
										INNER JOIN (
														SELECT P_LGSR.PROPERTYID,P_LGSR.ISSUEDATE 
														FROM P_LGSR
													) 
										AS P_LGSR 
										ON 
										P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID
										INNER JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
										INNER JOIN P_SCHEME ON GS_PROPERTY_APPLIANCE.SchemeId = P_SCHEME.SCHEMEID'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 
							+ CHAR(10) +'AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year)		
							+ CHAR(10) +'AND STATUS IN (
															SELECT 
																P_STATUS.STATUSID																
															FROM 
																P_STATUS 
															WHERE 
																P_STATUS.DESCRIPTION = ''Available to rent'' OR P_STATUS.DESCRIPTION = ''Let''
														)'
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')									  
									 P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS PostCode
									,P__PROPERTY.PropertyId	AS PropertyId 
									,P_SCHEME.SCHEMENAME AS SchemeName																					
									,(	SELECT count(*) 
										FROM 
											P_PROPERTY_APPLIANCE_DEFECTS 
										WHERE 
											P_PROPERTY_APPLIANCE_DEFECTS.PropertyId = P__PROPERTY.PropertyId
									) AS NumberOfDefects
									,P_PROPERTY_APPLIANCE_DEFECTS.DefectDate AS LastInspectedDate
									,DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS CertificateExpiryDate
									,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId'
							
			--============================Group Clause==========================================
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			--SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(4000), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetCancelledDefectsReport]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Cancelled Defects Report
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------


DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetCancelledDefectsReport]
		@searchText = '',
		@schemeId = 1,
		@applianceType = 'All',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'SchemeName',
		@sortOrder = N'ASC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetCancelledDefectsReport](
		
		@searchText varchar(5000)='',
		@schemeId INT = -1,
		@applianceType varchar(500) = 'All',
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'SchemeName',
		@sortOrder varchar (5) = 'ASC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'						
		END
		
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
		
		IF @applianceType != 'All' AND @applianceType != '-2'
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
		END
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P_PROPERTY_APPLIANCE_DEFECTS
										INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
										INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
										INNER JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
										INNER JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
										INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID	
										INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
										INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId
										LEFT JOIN P_SCHEME ON P__PROPERTY.SchemeId = P_SCHEME.SCHEMEID
										INNER JOIN PDR_CANCELLED_JOBS ON PDR_APPOINTMENTS.APPOINTMENTID = PDR_CANCELLED_JOBS.AppointmentId'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')									  
									 P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS PostCode
									,P__PROPERTY.PropertyId	AS PropertyId 
									,P_SCHEME.SCHEMENAME AS SchemeName
									,GS_PROPERTY_APPLIANCE.SchemeId AS SchemeId
									,GS_APPLIANCE_TYPE.APPLIANCETYPE AS ApplianceType
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) AS AppointmentDate 
									,P_PROPERTY_APPLIANCE_DEFECTS.CancelNotes AS CancelNotes
									,PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId
									,(SELECT PDR_APPOINTMENTS.ASSIGNEDTO FROM PDR_APPOINTMENTS WHERE PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID) AS CancelledBy
									,PDR_CANCELLED_JOBS.RecordedOn AS CancelledDate'
							
			--============================Group Clause==========================================
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(4000), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectJobSheetByDefectId]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  10/09/2015
	Description: Get Job Sheet Details (Arranged Job Sheet)
    Change History:    
    
Execution Command:
----------------------------------------------------

EXEC	[dbo].[DF_GetDefectJobSheetByDefectId] @defectId = '6185',

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDefectJobSheetByDefectId](
		 @defectId INT
)
AS
BEGIN
	DECLARE @propertyId NVARCHAR(20)
			,@JournalId INT
			,@defectApppointmentJournalId INT

SELECT
	@propertyId = PropertyId
	,@JournalId = JournalId
	,@DefectApppointmentJournalId = ApplianceDefectAppointmentJournalId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS
WHERE
	PropertyDefectId = @defectId


-- Get Defects by @defectApppointmentJournalId
SELECT
	AD.PropertyDefectId	AS [DefectId:]
	,AT.APPLIANCETYPE	AS [Appliance:]
	,T.Description		AS [Trade:]
	,CASE
		AD.IsTwoPersonsJob
		WHEN 1
			THEN 'Yes'
		ELSE 'No'
	END					AS [2 Person:]
	,CASE
		AD.IsDisconnected
		WHEN 1
			THEN 'Yes'
		WHEN 0
			THEN 'No'
		ELSE 'N/A'
	END					AS [Disconnected:]
	,CONVERT(NVARCHAR, CAST(AD.Duration AS INT)) + CASE
		WHEN AD.Duration > 1
			THEN ' Hours'
		ELSE ' Hour'
	END					AS [Duration:]
	,AD.PartsDue		AS [Parts Due:]
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN GS_PROPERTY_APPLIANCE A ON AD.ApplianceId = A.PROPERTYAPPLIANCEID
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		INNER JOIN G_TRADE T ON AD.TradeId = T.TradeId
WHERE
	AD.ApplianceDefectAppointmentJournalId = @defectApppointmentJournalId

-- Get Defect Appointment Details
SELECT DISTINCT
	AD.JournalId										AS InspectionRef
	,E.FIRSTNAME + ' ' + E.LASTNAME						AS Operative
	,A.DURATION											AS Duration
	,T.Description										AS Trade
	,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME	AS StartDateTime
	,A.APPOINTMENTENDDATE + A.APPOINTMENTENDTIME		AS EndDateTime
	,A.APPOINTMENTNOTES									AS AppointmentNotes
	,A.CUSTOMERNOTES									AS JobSheetNotes
	,A.APPOINTMENTID									AS AppointmentId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
		INNER JOIN G_TRADE T ON A.TRADEID = T.TradeId
		INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID
WHERE
	AD.ApplianceDefectAppointmentJournalId = @defectApppointmentJournalId

-- Get Property and Tenant Details
EXEC PLANNED_GetPropertyDetail @propertyId

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetApplianceDefectReport]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Appliance Defect Report
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetApplianceDefectReport]
		@searchText = 'com',
		@appointmentStatusId = 2,
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'JSD',
		@sortOrder = N'DESC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetApplianceDefectReport](
		
		@searchText varchar(5000)='',
		@appointmentStatusId INT = -1,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'JSD',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND ((ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR CONVERT(NVARCHAR,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId) LIKE ''%' + @searchText + '%'' )'
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR GS_APPLIANCE_TYPE.APPLIANCETYPE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P_DEFECTS_CATEGORY.Description LIKE ''%' + @searchText + '%'' )'
		END
		
		IF @appointmentStatusId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND PDR_STATUS.StatusId = ' + CONVERT(VARCHAR, @appointmentStatusId)
		END
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P_PROPERTY_APPLIANCE_DEFECTS
										INNER JOIN P_DEFECTS_CATEGORY ON P_DEFECTS_CATEGORY.CategoryId = P_PROPERTY_APPLIANCE_DEFECTS.CategoryId
										INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
										INNER JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID
										INNER JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
										INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
										INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
										INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
										INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
										INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')
									  P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId AS JSD
									,P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS POSTCODE
									,P__PROPERTY.PropertyId	AS PROPERTYID									
									,GS_APPLIANCE_TYPE.APPLIANCETYPE AS ApplianceType
									,P_DEFECTS_CATEGORY.Description AS Category
									,LEFT (E__EMPLOYEE.FIRSTNAME,1) +NCHAR(2)+ E__EMPLOYEE.LASTNAME AS RecordedBy
									,LEFT(CONVERT(VARCHAR(5),PDR_APPOINTMENTS.APPOINTMENTSTARTTIME,114),5)
									+'' ''+ 
									LEFT(DATENAME(WEEKDAY, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(3),DATEPART(DAY,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE)) 
									+'' ''+ 
									LEFT(DATENAME(MONTH, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),3) 
									+'' ''+ 
									CONVERT(VARCHAR(4),DATEPART(YEAR,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE))
									+'' (''+ 
									 RTRIM(LTRIM(PDR_STATUS.TITLE)) ++'') '' As AppointmentStatus
									,PDR_APPOINTMENTS.APPOINTMENTSTARTDATE As AppointmentStartDate 
									,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME As AppointmentStartTime 
									,PDR_STATUS.TITLE AS AppointmentStatusTitle'
							
			--============================Group Clause==========================================
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(4000), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDisconnectedAppliancesReport]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Disconnected Appliances Report
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetDisconnectedAppliancesReport]
		@searchText = N'a',
		@schemeId = NULL,
		@applianceType = 'All',
		@pageSize = 30,
		@pageNumber = 1,
		@sortColumn = N'SchemeName',
		@sortOrder = N'ASC',
		@getOnlyCount = 0,
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value


----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDisconnectedAppliancesReport](
		
		@searchText varchar(5000)='',
		@schemeId INT = -1,
		@applianceType varchar(500) = 'All',
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'SchemeName',
		@sortOrder varchar (5) = 'ASC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'					
		END
		
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
		
		IF @applianceType != 'All' AND @applianceType != '-2'
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'
		END
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P_PROPERTY_APPLIANCE_DEFECTS	
										INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID	
										INNER JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID	
										INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId
										INNER JOIN GS_MANUFACTURER ON GS_PROPERTY_APPLIANCE.MANUFACTURERID = GS_MANUFACTURER.MANUFACTURERID
										INNER JOIN GS_ApplianceModel ON GS_PROPERTY_APPLIANCE.MODELID = GS_ApplianceModel.ModelID
										INNER JOIN P_SCHEME ON P__PROPERTY.SchemeId = P_SCHEME.SCHEMEID'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria + CHAR(10)+' AND P_PROPERTY_APPLIANCE_DEFECTS.IsDisconnected = 1'		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT DISTINCT TOP ('+convert(VARCHAR(10),@limit)+')									  
									 P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''')	AS Address
									,P__PROPERTY.POSTCODE AS PostCode
									,P__PROPERTY.PropertyId	AS PropertyId 
									,P_SCHEME.SCHEMENAME AS SchemeName
									,GS_PROPERTY_APPLIANCE.SchemeId AS SchemeId
									,GS_APPLIANCE_TYPE.APPLIANCETYPE AS ApplianceType
									,GS_APPLIANCE_TYPE.APPLIANCETYPEID as ApplianceTypeId
									,GS_MANUFACTURER.MANUFACTURER As Manufacturer
									,GS_ApplianceModel.Model As Model
									,GS_PROPERTY_APPLIANCE.DATEINSTALLED As DateInstalled
									,P_PROPERTY_APPLIANCE_DEFECTS.DefectDate As DisconnectedDate'
							
			--============================Group Clause==========================================
			--SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
			--										, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '								
			--============================Order Clause==========================================		
								
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--=================================	Where Clause ================================
			
			--SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			
			--============================ Exec Final Query =================================
			IF(@getOnlyCount=0)
			BEGIN
				print(@finalQuery)
				EXEC (@finalQuery)
			END
			
			--========================================================================================
			-- Begin building Count Query 
			
			Declare @selectCount nvarchar(4000), 
			@parameterDef NVARCHAR(500)
			
			SET @parameterDef = '@totalCount int OUTPUT';
			SET @selectCount= 'SELECT @totalCount =  count(DISTINCT GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID) ' + @fromClause + @whereClause
			
			--print @selectCount
			EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
		END					
			-- End building the Count Query
			--========================================================================================	

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectsToBeArrangedList]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  04/08/2015
	Description: Get Defect To Be Arranged List for defect scheduling.
    Change History:    
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_GetDefectsToBeArrangedList]
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDefectsToBeArrangedList](
		
		@searchText varchar(5000)='',
		@defectCategoryId INT = -1,
		@schemeId int = -1,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'DefectDate',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 AND DC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'')
								AND (ApplianceId <> 0 AND ApplianceId IS NOT NULL)
								AND AD.IsActionTaken = 0 
								AND PendingDefectsCount > 0
								 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
		END
		
		IF @defectCategoryId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND AD.CategoryId = ' + CONVERT(VARCHAR, @defectCategoryId)
		END
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
											CROSS APPLY (SELECT COUNT(CASE WHEN iD.ApplianceDefectAppointmentJournalId IS NULL THEN 1 ELSE NULL END) PendingDefectsCount
													 FROM P_PROPERTY_APPLIANCE_DEFECTS iD
													 INNER JOIN P_DEFECTS_CATEGORY iDC ON iD.CategoryId = iDC.CategoryId
													 WHERE iD.JournalId = AD.JournalId
														AND iDC.Description IN (''RIDDOR'',''Immediately Dangerous'',''At Risk'',''Not to Current Standards'')
														AND (iD.ApplianceId <> 0 AND iD.ApplianceId IS NOT NULL)
														AND AD.IsActionTaken = 0 
													 ) DefectAppointment
									'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria + ' '		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = '
									 AD.JournalId																			AS JournalId
									,P.HOUSENUMBER + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.TOWNCITY, '''')	AS Address
									,P.POSTCODE																				AS POSTCODE
									,AD.PropertyId																			AS PROPERTYID
									,MIN(AD.PartsDue)																		AS PartsDue
									,MIN(AD.DefectDate)																		AS DefectDate
									,SUM(CASE
										AD.IsHeatersLeft
										WHEN 1
											THEN 1
										ELSE 0
									END)																					AS isHeatersLeft
									,SUM(CASE
										AD.IsCustomerHaveHeating
										WHEN 1
											THEN 1
										ELSE 0
									END)																					AS isHeatingAvailable
									,SUM(CASE
										AD.IsDisconnected
										WHEN 1
											THEN 1
										ELSE 0
									END)																					AS isCapped
									,ISNULL(P.PATCH,-1)																		AS PatchId
									,P.COUNTY																				AS COUNTY
									,P.TOWNCITY																				AS TOWNCITY
							'
							
			--============================Group Clause==========================================
			SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY
													, P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '
					
			
			--============================Order Clause==========================================		
							
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--===============================Main Query ====================================
			Set @mainSelectQuery ='SELECT TOP ('+convert(VARCHAR(10),@limit)+')'+ @selectClause +@fromClause + @whereClause + @GroupClause + @orderClause 
			--PRINT @mainSelectQuery
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			-- PRINT @rowNumberQuery
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			 PRINT @finalQuery
			--============================ Exec Final Query =================================
						
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= ' SELECT @totalCount =  count(*) from (Select '+ @selectClause + @fromClause + @whereClause + @GroupClause+') as Record' 
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectsArrangedList]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  10/09/2015
	Description: Get Defect Arranged List to show in defect scheduling.
    Change History:    
    
Execution Command:
----------------------------------------------------

DECLARE	@totalCount int

EXEC	[dbo].[DF_GetDefectsArrangedList] @sortColumn = 'Address',
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDefectsArrangedList](
		
		@searchText varchar(5000)='',		
		@schemeId int = -1,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'AppointmentDateSort',
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int=0 output
)
AS
BEGIN
DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @GroupClause  varchar(1000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1 = 1 
								AND ISNULL(M.isPending,1) = 0
								AND DS.TITLE IN (''Arranged'', ''No Entry'', ''Paused'', ''In Progress'') '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ((ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') + ISNULL('', ''+P.TOWNCITY, '''') LIKE ''%' + @searchText + '%'')
																	OR P.POSTCODE LIKE ''%' + @searchText + '%'' '
			SET @searchCriteria = @searchCriteria + CHAR(10) + '	OR CONVERT(NVARCHAR,AD.JournalId) LIKE ''%' + @searchText + '%'' )'
		END
		IF @schemeId != -1
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P.SchemeId = ' + CONVERT(VARCHAR, @schemeId)
		END
				
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM
										P_PROPERTY_APPLIANCE_DEFECTS AD
											INNER JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
											INNER JOIN PDR_STATUS DS ON AD.DefectJobSheetStatus = DS.STATUSID
											INNER JOIN GS_PROPERTY_APPLIANCE PA ON AD.ApplianceId = PA.PROPERTYAPPLIANCEID
											INNER JOIN GS_APPLIANCE_TYPE AT ON PA.APPLIANCETYPEID = AT.APPLIANCETYPEID
											INNER JOIN P__PROPERTY P ON AD.PropertyId = P.PROPERTYID
											INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
											INNER JOIN PDR_MSAT M ON PJ.MSATID = M.MSATId
											INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
											INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO = E.EMPLOYEEID											
									'
									
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria 		
		
		IF(@getOnlyCount=0)
		BEGIN
			
			--=======================Select Clause=============================================
				SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')
									 RIGHT(''00000'' + CONVERT(NVARCHAR, AD.PropertyDefectId), 5)				AS JSD
									,P.HOUSENUMBER + '' '' + P.ADDRESS1 + '', '' + P.TOWNCITY					AS Address
									,P.POSTCODE																	AS Postcode
									,AT.APPLIANCETYPE															AS Appliance
									,DC.Description																AS Defect
									,CASE
										DS.TITLE
										WHEN ''No Entry''
											THEN RTRIM(DS.TITLE) + ''('' + CONVERT(NVARCHAR,
												(
													SELECT
														COUNT(ADH.IDENTITYCOL)
													FROM
														P_PROPERTY_APPLIANCE_DEFECTS_HISTORY ADH
													WHERE
														ADH.PropertyDefectId = AD.PropertyDefectId
														AND ADH.DefectJobSheetStatus = DS.STATUSID
												)
												) + '')''
										ELSE DS.TITLE
									END																			AS DefectStatus
									,E.FIRSTNAME + '' '' + E.LASTNAME											AS Engineer
									,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108)
									+ '' - '' + CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTENDTIME), 108)	AS AppointmentTimes
									,LEFT(DATENAME(WEEKDAY, APPOINTMENTSTARTDATE), 3)
									+ '' '' + CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 106)					AS AppointmentDate
									,A.APPOINTMENTSTARTDATE + A.APPOINTMENTSTARTTIME							AS AppointmentDateSort
									,AD.PropertyDefectId														AS DefectId
									,A.APPOINTMENTID															AS AppointmentId
									,P.HouseNumber																AS HouseNumber									
									,P.ADDRESS1																	AS PrimaryAddress									
							'
							
			--============================Group Clause==========================================
			SET @GroupClause = CHAR(10) + ' '
					
			
			--============================Order Clause==========================================
			
			IF (@sortColumn = 'Address') BEGIN
				SET @sortColumn = CHAR(10) + ' CAST(SUBSTRING(HouseNumber, 1,CASE when patindex(''%[^0-9]%'',HouseNumber) > 0 THEN PATINDEX(''%[^0-9]%'',HouseNumber) - 1 ELSE LEN(HouseNumber) END) AS INT) '
			END
							
			SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
			
			--===============================Main Query ====================================
			Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @GroupClause + @orderClause 
			--PRINT @mainSelectQuery
			
			--=============================== Row Number Query =============================
			Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn + CHAR(10) + @sortOrder +CHAR(10) + ') as row	
									FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
			-- PRINT @rowNumberQuery
			--============================== Final Query ===================================
			Set @finalQuery  =' SELECT *
								FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
								WHERE
								Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
			PRINT @finalQuery
			--============================ Exec Final Query =================================
						
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= ' SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetApplianceInfo]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC DF_GetApplianceInfo  
-- @defectId =1212  
-- Author:  <Noor Muhammad>  
-- Create date: <28/09/2015>  
-- Description: <Get Appliance information>  
-- History:          28/09/2015 Noor : Created the stored procedure
--                   28/09/2015 Name : Description of Work
-- =============================================  
  
CREATE PROCEDURE [dbo].[DF_GetApplianceInfo]  
  
 @defectId INT   
  
AS  
BEGIN   

SELECT 
	GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID AS ApplianceId  
	,GS_APPLIANCE_TYPE.APPLIANCETYPE COLLATE Latin1_General_CI_AI + ' : ' + P_DEFECTS_CATEGORY.Description COLLATE Latin1_General_CI_AI AS ApplianceAndCategory  	
	,P_PROPERTY_APPLIANCE_DEFECTS.DefectNotes AS WorksRequired  
	,ISNULL(GS_PROPERTY_APPLIANCE.NetCost,0) AS NetCost  
	,ISNULL(GS_PROPERTY_APPLIANCE.Vat,0) AS Vat  
	,ISNULL(GS_PROPERTY_APPLIANCE.Gross,0) AS Gross  
	,ISNULL(GS_PROPERTY_APPLIANCE.VatRateId ,-1 ) AS VatRateId  
	,P_PROPERTY_APPLIANCE_DEFECTS.PropertyId  AS PropertyId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS
	INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	INNER JOIN GS_APPLIANCE_TYPE ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	INNER JOIN P_DEFECTS_CATEGORY  ON P_PROPERTY_APPLIANCE_DEFECTS.CategoryId = P_DEFECTS_CATEGORY.CategoryId
WHERE 
	P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId = @defectId	   
  
END
GO
/****** Object:  StoredProcedure [dbo].[DF_CancelDefectAppointment]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Cancel Defect Appointment and update cancel notes
    Change History:    

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_CancelDefectAppointment](
		 @appointmentId INT,
		 @cancelReasonNotes NVARCHAR(1000),
		 @userId INT,
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
	DECLARE @Now DATETIME2 = GETDATE()
	
	DECLARE @CancelledStatusId INT
	DECLARE @CancelledStatus NVARCHAR(200) = 'Cancelled'
	SELECT @CancelledStatusId = STATUSID
	FROM PDR_STATUS
	WHERE RTRIM(TITLE) = @CancelledStatus
	
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE AD
	SET DefectJobSheetStatus = @CancelledStatusId
		,CancelNotes = @cancelReasonNotes
		,ModifiedBy = @userId
		,ModifiedDate = @Now
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_APPOINTMENTS A ON AD.ApplianceDefectAppointmentJournalId = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

-- Update journal Entry and get histroy identifier
UPDATE PJ
	SET STATUSID = @CancelledStatusId		
FROM PDR_JOURNAL PJ
INNER JOIN PDR_APPOINTMENTS A ON PJ.JOURNALID = A.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

DECLARE @JournalHistoryId INT
SELECT @JournalHistoryId = MAX(A.JOURNALHISTORYID)
FROM PDR_APPOINTMENTS A
INNER JOIN PDR_JOURNAL_HISTORY PJH	ON A.JOURNALID = PJH.JOURNALID
WHERE A.APPOINTMENTID = @appointmentId

-- Update Appointment
UPDATE PDR_APPOINTMENTS
	SET APPOINTMENTSTATUS = @CancelledStatus
		,JOURNALHISTORYID = @JournalHistoryId		
WHERE APPOINTMENTID = @appointmentId

-- Insert Entry into Cancel table
INSERT INTO PDR_CANCELLED_JOBS(AppointmentId, RecordedOn, Notes)
VALUES (@appointmentId, @Now, @cancelReasonNotes)
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAppliancesDefectsByJournalId]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  05/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         05/08/2015     Aamir Waheed       Created:DF_GetAppliancesDefectsByJournalId
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetAppliancesDefectsByJournalId]
						@journalId = 6494,
						@defectCategoryId = -1

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetAppliancesDefectsByJournalId](
		
		@journalId INT,
		@defectCategoryId INT = -1
)
AS
BEGIN

Declare @assingnedToContractorStatusId int 
SET @assingnedToContractorStatusId = (SELECT PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Assigned To Contractor')
SELECT
	D.PropertyDefectId									AS DefectId
	,AT.APPLIANCETYPE									AS Appliance
	,DC.Description										AS DefectCategory
	,ISNULL(DP.PriorityName, 'N/A')						AS DefectPriority
	,ISNULL(T.Description, 'N/A')						AS DefectTrade
	,ISNULL(D.IsTwoPersonsJob, 0)						AS isTwoPersonsJob
	,ISNULL(D.IsDisconnected, 0)						AS isApplianceDisconnected
	,CONVERT(NVARCHAR(5), CONVERT(TIME, A.APPOINTMENTSTARTTIME), 108) + ' '
	+ CONVERT(NVARCHAR, A.APPOINTMENTSTARTDATE, 103)	AS AppointmentDate
	,D.PartsDue											AS DefectPartsDue
	,E.EMPLOYEEID										AS EMPLOYEEID
	,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')			AS EMPLOYEENAME
	,ISNULL(D.Duration, 0)								AS Duration
	,T.TradeId											AS TradeId
	,CASE
		WHEN D.ApplianceDefectAppointmentJournalId IS NULL
			THEN 0
		ELSE 1
	END													AS isAppointmentCreated
	,CASE
		WHEN J.STATUSID = @assingnedToContractorStatusId
			THEN 1
		ELSE 0
	END													As isAssignedToContractor
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AS D
		INNER JOIN GS_PROPERTY_APPLIANCE AS APP ON D.ApplianceId = APP.PROPERTYAPPLIANCEID
		INNER JOIN GS_APPLIANCE_TYPE AS AT ON APP.APPLIANCETYPEID = AT.APPLIANCETYPEID
		INNER JOIN P_DEFECTS_CATEGORY AS DC ON D.CategoryId = DC.CategoryId
		LEFT JOIN P_DEFECTS_PRIORITY AS DP ON D.Priority = DP.PriorityID
		LEFT JOIN G_TRADE AS T ON D.TradeId = T.TradeId
		LEFT JOIN E__EMPLOYEE AS E ON D.CreatedBy = E.EMPLOYEEID
		LEFT JOIN PDR_JOURNAL J ON D.ApplianceDefectAppointmentJournalId = J.JOURNALID 
		LEFT JOIN PDR_MSAT M ON J.MSATID = M.MSATId AND M.isPending = 1 
		LEFT JOIN PDR_APPOINTMENTS A ON J.JOURNALID = A.JOURNALID
WHERE
	D.JournalId = @journalId
	AND (@defectCategoryId = -1 OR D.CategoryId = @defectCategoryId)
	AND D.IsActionTaken = 0
	AND DC.Description IN ('RIDDOR', 'Immediately Dangerous', 'At Risk', 'Not to Current Standards')
END
GO
/****** Object:  StoredProcedure [dbo].[DF_ConfirmDefectAppointmentsByDefectsId]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectJobSheetSummary.aspx
 
    Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Confirm Defect Appointments.	
    Change History:    

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_ConfirmDefectAppointmentsByDefectsId](
		 @defectIds NVARCHAR(4000),		 
		 
		 -- OUTPUT Parameters
		 @saveStatus BIT OUTPUT
)
AS
BEGIN
		
BEGIN TRANSACTION
BEGIN TRY

-- Update Defects
UPDATE MS
	SET isPending = 0
FROM P_PROPERTY_APPLIANCE_DEFECTS AD
INNER JOIN PDR_JOURNAL PJ ON AD.ApplianceDefectAppointmentJournalId = PJ.JOURNALID
INNER JOIN PDR_MSAT MS ON PJ.MSATID = MS.MSATId
INNER JOIN dbo.SPLIT_STRING(@defectIds,',') Defects ON AD.PropertyDefectId = Defects.COLUMN1
	
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END

END
GO
/****** Object:  StoredProcedure [dbo].[DF_AssignWorkToContractor]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Noor Muhammad>
-- Create date: <29/09/2015>
-- Description:	Assign Work to Contractor, Defect work
-- History:     29/09/2015 Noor : Created the stored procedure
--              29/09/2015 Name : Description of Work

-- =============================================
CREATE PROCEDURE [dbo].[DF_AssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@contactId INT,
	@contractorId INT,
	@userId int,
	@estimate SMALLMONEY,
	@estimateRef NVARCHAR(200),
	@poStatus INT,
	@propertyId varchar(20),
	@journalId INT ,
	@contractorWorksDetail AS DF_AssingToContractorWorksRequired READONLY,
	@defectNotes nvarchar(200),
	@customerId int,
	@isSavedOut BIT = 0 OUTPUT,
	@poStatusIdOut int = 0 OUTPUT
	

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRANSACTION
BEGIN TRY

-- =====================================================
-- General Purpose Variable
-- =====================================================

-- To save same time stamp in all records 
DECLARE @currentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusHistoryId int = NULL
DECLARE @newStatusId int = NULL
-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @active bit = 1
, @poType int = (	SELECT	POTYPEID
					FROM F_POTYPE
					WHERE POTYPENAME = 'Appliance Defect') -- 2 = 'Repair'

, @purchaseOrderId int
, @blockId INT = NULL
, @schemeId INT = NULL
, @developmentId INT = NULL

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
POTYPE, POSTATUS, GASSERVICINGYESNO, POTIMESTAMP, DEVELOPMENTID, BLOCKID)
	VALUES (UPPER('Gas Servicing'), @currentDateTime, @defectNotes , @userId, @contractorId, @active, @poType, @poStatus, 1, @currentDateTime, @developmentId, @blockId)

-- To get Identity Value of Purchase Order.
SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Old table(s)
-- Insert new P_WORKORDER
-- =====================================================
DECLARE @title nvarchar(50) = 'Assigned To Contractor'
DECLARE @woStatus INT = 2 -- 'Assigned To Contractor
If (@poStatus = 0) SET @woStatus = 12 --Pending
DECLARE @birthNature INT = 2 --'Day To Day Repair'
DECLARE @birthEntity INT = 1 --'FOR Property'
DECLARE @birthModule INT = 1 --'Customer Module'

DECLARE @today DATE = CAST(@currentDateTime AS DATE)
DECLARE @tenancyId INT
SELECT @tenancyId = MAX(TENANCYID) FROM C_TENANCY T
	WHERE T.PROPERTYID = @propertyId AND (T.ENDDATE IS NULL OR T.ENDDATE >= @today)



INSERT INTO P_WORKORDER (ORDERID, TITLE, CUSTOMERID, PROPERTYID, TENANCYID, WOSTATUS, CREATIONDATE, BIRTH_MODULE
						,BIRTH_ENTITY, BIRTH_NATURE, DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId)
		VALUES(@purchaseOrderId,@title, @customerId, @propertyId, @tenancyId,@woStatus,@currentDateTime,@birthModule
				, @birthEntity, @birthNature, @developmentId, @blockId, 0, @schemeId)

DECLARE @woId INT = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================
DECLARE @defectContractorId int
INSERT INTO PDR_CONTRACTOR_WORK (JournalId, ContractorId, AssignedDate, AssignedBy, Estimate,EstimateRef
,ContactId,PurchaseORDERID)
	VALUES (@journalId,@contractorId,@currentDateTime,@userId, @estimate, @estimateRef, @contactId,@purchaseOrderId )

SET @defectContractorId = SCOPE_IDENTITY()


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	worksRequired, netCost, vatType, vat,
	gross, piStatus, expenditureId, costCenterId, budgetHeadId
FROM @contractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE @worksRequired nvarchar(4000),
@netCost smallmoney,
@vatType int,
@vat smallmoney,
@gross smallmoney,
@piStatus int,
@expenditureId int,
@costCenterId int,
@budgetHeadId int

-- Variable used within loop
DECLARE @purchaseItemTITLE nvarchar(100) = @defectNotes -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @worksRequired, @netCost, @vatType, @vat,
		@gross, @piStatus, @expenditureId, @costCenterId, @budgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN

			-- =====================================================
			-- Old table(s)
			-- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER
			-- =====================================================

			DECLARE @itemId INT = 1 --'PROPERTY'
			DECLARE @status INT = 2 --'ASSIGNED
			If (@poStatus = 0) SET @status = 12 --Pending


			INSERT INTO C_JOURNAL ( CUSTOMERID, TENANCYID, PROPERTYID, ITEMID,ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)
			VALUES(@customerId, @tenancyId, @propertyId, @itemId, @birthNature, @status, @currentDateTime,@title)

			DECLARE @cJournalId INT = SCOPE_IDENTITY()

			-- =====================================================
			-- Old table(s)
			--INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER
			-- =====================================================

			DECLARE @itemActionId INT = 2 --'ASSIGNED TO CONTRACTOR'

			If (@poStatus = 0) SET @itemActionId = 12 --Pending

			DECLARE @propertyAddress NVARCHAR(400)

			SELECT	@propertyAddress = ISNULL(FLATNUMBER+',','')+ISNULL(HOUSENUMBER+',','')+ISNULL(ADDRESS1+',','')+ISNULL(ADDRESS2+',','')+ISNULL(ADDRESS3+',','')+ISNULL(TOWNCITY+',','')+ISNULL(POSTCODE,'')
			FROM	P__PROPERTY
			WHERE	PROPERTYID = @propertyId

			--ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE 
			--AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT

			INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE,LASTACTIONUSER, ITEMDETAILID,
								  CONTRACTORID, TITLE, NOTES,SCOPEID)	
			VALUES(@cJournalId, @status, @itemActionId, @currentDateTime, @userId,NULL,@contractorId,'FOR'+@propertyAddress,@title,NULL)

			DECLARE @repairHistoryId INT = SCOPE_IDENTITY()

			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
			NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
				VALUES (@purchaseOrderId, @expenditureId, @purchaseItemTITLE, @worksRequired, 
				@currentDateTime,  @netCost, @vatType, @vat, @gross, @userId, @active, @poType, @poStatus)

			DECLARE @orderItemId int = SCOPE_IDENTITY()

			-- =====================================================
			--INSERT VALUE IN P_WOTOREPAIR for each work required
			-- =====================================================

			INSERT INTO P_WOTOREPAIR(WOID, JOURNALID, ORDERITEMID)
			VALUES(@woId,@cJournalId,@orderItemId)

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================		
			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
				(
					[PDRContractorId],[ServiceRequired],[NetCost],
					[VatId],[Vat],[Gross],[ExpenditureId],
					[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID]
				)
			VALUES			
				(
					@defectContractorId, @worksRequired, @netCost, 
					@vatType, @vat, @gross,@expenditureId,
					@costCenterId,@budgetHeadId, @orderItemId
				)					

		-- Fetch record for next loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @worksRequired, @netCost, @vatType, @vat,
		@gross, @piStatus, @expenditureId, @costCenterId, @budgetHeadId
		END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

-- =====================================================
-- If PO is queue set work order status to 12 (queued) as set in Portfolio Work Order
-- =====================================================

IF @poStatus = 0
 BEGIN
  UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = @woId
 END
ELSE
 BEGIN
	-- - - -  - - - - - - -  -
	-- AUTO ACCEPT REPAIRS
	-- - ---- - - - -  - - - -
	-- we check to see if the org has an auto accept functionality =  1
	-- if so then we auto accept the repair
  EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = @PurchaseOrderId, @SUPPLIERID =  @contractorId
END


CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor
SET @poStatusIdOut =  @poStatus


END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSavedOut = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSavedOut = 1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectManagementLoopkupsData]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: DefectAvailableAppointments.aspx
 
    Author: Aamir Waheed
    Creation Date:  26/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         02/09/2015     Aamir Waheed       Created:DF_GetDefectManagementLoopkupsData, to get lookup values for defect management user control
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetDefectManagementLoopkupsData]
						@PropertyId = ''

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetDefectManagementLoopkupsData]
	@PropertyId NVARCHAR(20)
AS
BEGIN

-- 1- Get Defect Categories
SELECT
	CategoryId
	,[Description]	AS CategoryName
FROM
	P_DEFECTS_CATEGORY

-- 2- Get Property Appliances by PropertyId
SELECT
	A.PROPERTYAPPLIANCEID
	,AT.APPLIANCETYPE	AS APPLIANCE
FROM
	GS_PROPERTY_APPLIANCE A
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
WHERE
	A.PROPERTYID = @PropertyId

-- 3- Get Employees List (for Parts order by)
SELECT
	E.EMPLOYEEID
	,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')	AS EmployeeName
FROM
	E__EMPLOYEE E
		INNER JOIN E_JOBDETAILS JD ON E.EMPLOYEEID = JD.EMPLOYEEID
			AND
			JD.ACTIVE = 1

-- 4- Get Defect Priorities List.
SELECT
	P.PriorityID
	,P.PriorityName
FROM
	P_DEFECTS_PRIORITY P

-- 5- Get Trades List
SELECT
	T.TradeId
	,T.[Description]	AS Trade
FROM
	G_TRADE T

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAllSchemes]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      09/09/2015
-- Description:      Get All Schemes
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetAllSchemes]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SCHEMEID as Id, SCHEMENAME as Title From P_SCHEME 
	WHERE SCHEMENAME !=''
	ORDER BY SCHEMENAME ASC
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAllApplainceTypes]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      09/09/2015
-- Description:      Get All Applaince Types
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetAllApplainceTypes]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT GS_APPLIANCE_TYPE.APPLIANCETYPEID AS Id, GS_APPLIANCE_TYPE.APPLIANCETYPE AS Title FROM GS_APPLIANCE_TYPE
	WHERE APPLIANCETYPE IS NOT NULL
	ORDER BY GS_APPLIANCE_TYPE.APPLIANCETYPE ASC
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectAppointmentStatuses]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      08/09/2015
-- Description:      Get Defect Status List
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetDefectAppointmentStatuses]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT STATUSID AS StatusId, TITLE AS Title From PDR_STATUS WHERE TITLE NOT LIKE '%to be arranged%'
	ORDER BY Title ASC 
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetDefectContractorDropDownValues]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noor Muhammad
-- Create date: 04/01/2015
-- Description:	To get contractors having at lease one Defect contract, as drop down values for assgin work to contractor.
-- History:          23/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetDefectContractorDropDownValues] 
	-- Add the parameters for the stored procedure here	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID AND RTRIM(LTRIM(AoW.DESCRIPTION)) LIKE '%Defect%'
	ORDER BY NAME ASC
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[DF_SaveContractorAppointmentDetails]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================
-- EXEC [dbo].[DF_SaveContractorAppointmentDetails]

	--@ORGID =1270,
	--@DefectId =1487,
	--@UserId =760
-- Author:		<Noor Muhammad>
-- Create date: <29/09/2015>
-- Description:	<Saves all appointment information of defect>
-- History:          29/09/2015 Noor : Created the stored procedure
--                   29/09/2015 Name : Description of Work
-- =====================================================
CREATE PROCEDURE [dbo].[DF_SaveContractorAppointmentDetails]
( 		
	@applianceId int,
	@defectId int,
	@propertyId nvarchar(20),	
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@contactId int, 
	@contractorId int,
	@poStatus int,
	@ContractorWorksDetail AS DF_AssingToContractorWorksRequired READONLY,		
	@isSavedOut INT OUTPUT,
	@journalIdOut int = 0 output,
	@poStatusIdOut int = 0 OUTPUT
	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY
		-------- Declaring variables -------------------------------------------------------
		------------------------------------------------------------------------------------
		Declare @tenancyId int,
		@msatTypeId int,
		@appointmentStatusId int,
		@msatId int,
		@defectNotes nvarchar(200),
		@customerId int

		-------- Populating variables ------------------------------------------------------
		------------------------------------------------------------------------------------
		if @contractorId= -1
			set @contractorId=NULL
		
		-- Get Tenancy Detail

		SELECT	@tenancyId = CT.TenancyId, @customerId = CT.CUSTOMERID
		FROM	C_TENANCY T INNER JOIN C_CUSTOMERTENANCY CT ON 
				( T.TENANCYID = CT.TENANCYID ) AND 
				( T.ENDDATE >= CONVERT(date, GETDATE() ) OR 
				 T.ENDDATE IS NULL ) AND
				( CT.ENDDATE >= CONVERT(date, GETDATE() )	OR 
				 CT.ENDDATE IS NULL )
		WHERE	( T.PROPERTYID = @PropertyId ) AND 
				( CT.CUSTOMERTENANCYID = (	SELECT max( CUSTOMERTENANCYID ) 
											FROM C_CUSTOMERTENANCY C_CT 
											WHERE C_CT.TENANCYID = CT.TENANCYID ) )
		
		--SELECT MSAT TYPE ID
		SELECT @msatTypeId = msatTypeId
		From PDR_MSATType WHERE PDR_MsatType.MsatTypeName like 'Appliance Defect%'
		
		--Select Status Id
		SELECT @appointmentStatusId = PDR_Status.StatusId
		FROM PDR_Status WHERE Title = 'Assigned To Contractor'
		
		--Get Defect  Notes
		SELECT @defectNotes = DefectNotes FROM P_PROPERTY_APPLIANCE_DEFECTS WHERE PropertyDefectId = @defectId

		-------- Insertion into Tables -----------------------------------------------------
		------------------------------------------------------------------------------------
		--Insert into PDR_MSAT
		INSERT INTO [PDR_MSAT]([PropertyId] ,[MSATTypeId],[TenancyId], [CustomerId])
		 VALUES(@propertyId ,@msatTypeId,@tenancyId, @customerId)
	           
		SET @msatId = SCOPE_IDENTITY()           

		--Insert into PDR_Journal
		INSERT INTO [PDR_JOURNAL]([MSATID],[STATUSID],[CREATIONDATE],[CREATEDBY])
		 VALUES(@msatId,@appointmentStatusId,getdate(),@userId)
		
		SET @journalIdOut  = SCOPE_IDENTITY()    
		   
		--update Into [P_PROPERTY_APPLIANCE_DEFECTS]
		UPDATE [P_PROPERTY_APPLIANCE_DEFECTS]
		SET 
		   [ApplianceDefectAppointmentJournalId] = @journalIdOut     
		  ,[ModifiedBy] = @userId
		  ,[ModifiedDate] = getdate()
		WHERE PropertyDefectId = @defectId
		
		DECLARE @isAssignToContractorSaved bit = 0 		
		-- Execute the stored procedure "Assign Defect Work To Contractor"
		EXEC DF_AssignWorkToContractor 
						@contactId,
						@contractorId,
						@userId,
						@Estimate,
						@EstimateRef,
						@poStatus,
						@propertyId,
						@journalIdOut,
						@contractorWorksDetail,
						@defectNotes,
						@customerId,
						@isAssignToContractorSaved OUTPUT,
						@poStatusIdOut OUTPUT;
									
	END TRY
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isSavedOut = 0        
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
					@ErrorSeverity, -- Severity.
					@ErrorState -- State.
				);
	END CATCH;

	IF @@TRANCOUNT > 0
		BEGIN  
			COMMIT TRANSACTION;  
			SET @isSavedOut = 1
		END
    
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetContractorDetailforEmail]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noor Muhammad
-- Create date: 01/10/2015
-- Description:	To get detail to send email to contractor, details are: contractor details, property details, tenant details, tenant risk details
-- History:     29/09/2015 Noor : Created the stored procedure
--              29/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetContractorDetailforEmail]
	@journalId INT
	,@propertyId NVARCHAR(20)
	,@employeeId INT
	,@ContractorId INT		
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS [ContractorContactName],
	ISNULL(C.WORKEMAIL, '') AS [Email]
FROM E__EMPLOYEE E
	INNER JOIN E_CONTACT C
	ON E.EMPLOYEEID = C.EMPLOYEEID

WHERE E.EMPLOYEEID = @employeeId

--=================================================
--Get Property Detail(s)
--=================================================
SELECT
	ISNULL(P.PROPERTYID,'') PROPERTYID,
	ISNULL(HOUSENUMBER,'') HOUSENUMBER,
	ISNULL(FLATNUMBER,'') FLATNUMBER,
	ISNULL(ADDRESS1,'') ADDRESS1,
	ISNULL(ADDRESS2,'') ADDRESS2,
	ISNULL(ADDRESS3,'') ADDRESS3,
	ISNULL(TOWNCITY,'') TOWNCITY,
	ISNULL(COUNTY,'') COUNTY,
	ISNULL(POSTCODE,'') POSTCODE,
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, '') + ISNULL(' ' + TOWNCITY, '')
	+ ISNULL(', ' + COUNTY, '') + ISNULL(', ' + POSTCODE, ''), ''), 'N/A') AS [FullAddress],
	ISNULL(NULLIF(ISNULL('Flat No:' + FLATNUMBER + ', ', '') + ISNULL(HOUSENUMBER, '') + ISNULL(' ' + ADDRESS1, '')
	+ ISNULL(' ' + ADDRESS2, '') + ISNULL(' ' + ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress],
	 CASE WHEN EXISTS(Select ASBESTOSID from P_PROPERTY_ASBESTOS_RISKLEVEL ARL WHERE ARL.PROPERTYID = P.PROPERTYID ) Then 
	 1 
	 ELSE
	 0
	 END AS AsbestosExists
FROM P__PROPERTY P
WHERE P.PROPERTYID = @propertyId

--=================================================
-- Get CustomerId to get Tenant Detail(s) and Risk
--=================================================

CREATE TABLE #Customers(CUSTOMERID INT)

INSERT INTO #Customers(CUSTOMERID)
SELECT CT.CUSTOMERID
FROM C_TENANCY T
	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID 
	AND (CT.ENDDATE IS NULL OR CT.ENDDATE > CAST(GETDATE() AS DATE))
	AND (T.ENDDATE IS NULL OR T.ENDDATE > CAST(GETDATE() AS DATE))
WHERE T.PROPERTYID = @propertyId
ORDER BY CT.CUSTOMERTENANCYID ASC

--=================================================
--Get Customer/Tenant Detail(s)
--=================================================

SELECT TOP 1
	ISNULL(C.CUSTOMERID,'') AS [CUSTOMERID],
	ISNULL(FIRSTNAME, '') AS [FIRSTNAME],
	ISNULL(MIDDLENAME, '') AS [MIDDLENAME],
	ISNULL(LASTNAME, '') AS [LASTNAME],
	ISNULL(TEL, '') AS [TEL],
	ISNULL(T.DESCRIPTION, '') [TITLE],
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '') + ISNULL(' ' + C.MIDDLENAME, '')
	+ ISNULL(' ' + C.LASTNAME, '') + ISNULL(', Tel:' + A.TEL, ''), ''), 'N/A') AS ContactDetail,
	ISNULL(NULLIF(ISNULL(T.DESCRIPTION + '. ', '') + ISNULL(C.FIRSTNAME, '')
	+ ISNULL(' ' + C.LASTNAME, ''), ''), 'N/A') [FullName]
FROM #Customers TC
	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = TC.CUSTOMERID
	INNER JOIN G_TITLE T ON C.TITLE = T.TITLEID
	INNER JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1
	
--=================================================
-- Create temp tables to get Customer/Tenant Risk and Vulnerability Detail(s)
--=================================================

CREATE TABLE #RISKS (CATDESC NVARCHAR(60), SUBCATDESC NVARCHAR(60))
CREATE TABLE #VULNERABILITY (CATDESC NVARCHAR(50), SUBCATDESC NVARCHAR(140))

DECLARE @CustomerId INT = -1

SELECT @CustomerId = MIN(CUSTOMERID) FROM #Customers WHERE CUSTOMERID > @CustomerId
WHILE (@CustomerId IS NOT NULL)
BEGIN

--=================================================
--Fill Customer/Tenant Risk Detail(s) in temp tables
--=================================================
INSERT INTO #RISKS
SELECT	CATDESC, SUBCATDESC
FROM	RISK_CATS_SUBCATS(@CustomerId)

--=================================================
--Fill Customer/Tenant Vulnerability Detail(s)  in temp table
--=================================================
DECLARE @VULNERABILITYHISTORYID int = 0

SELECT  @VULNERABILITYHISTORYID = VULNERABILITYHISTORYID
FROM	C_JOURNAL J
		INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID
WHERE	CUSTOMERID = @customerId
		AND ITEMNATUREID = 61
		AND CV.ITEMSTATUSID <> 14
		AND CV.VULNERABILITYHISTORYID = (	SELECT MAX(VULNERABILITYHISTORYID)
											FROM C_VULNERABILITY IN_CV
											WHERE IN_CV.JOURNALID = J.JOURNALID)
  
INSERT INTO #VULNERABILITY
EXECUTE VULNERABILITY_CAT_SUBCAT @VULNERABILITYHISTORYID  


SELECT	@CustomerId = MIN(CUSTOMERID) 
FROM	#Customers 
WHERE	CUSTOMERID > @CustomerId

END

--=================================================
--Get Customer/Tenant Risk Detail(s)
--=================================================
SELECT	DISTINCT * 
FROM	#RISKS

--=================================================
--Get Customer/Tenant Vulnerability Detail(s)
--=================================================
SELECT	DISTINCT * 
FROM	#VULNERABILITY

--================================================
-- Get Ordered Person Details 
--================================================

SELECT 
ISNULL( C.WORKDD,'N/A') AS DDial,ISNULL(C.WORKEMAIL, 'N/A') AS EMAIL  ,PCW.PurchaseORDERID AS OrderId, 
(ISNULL(E.FIRSTNAME,' ') + ISNULL(' ' + E.LASTNAME,' ')) AS OrderedBy
FROM PDR_CONTRACTOR_WORK PCW
INNER JOIN E_CONTACT C ON C.EMPLOYEEID = PCW.AssignedBy
INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID =   PCW.AssignedBy
WHERE PCW.PDRContractorId = @ContractorId


--================================================
-- Get Property Absestos Detail.
--================================================

 SELECT DISTINCT RL.ASBRISKLEVELDESCRIPTION,A.RISKDESCRIPTION
 FROM P__PROPERTY P
 INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ARL ON P.PROPERTYID = ARL.PROPERTYID  
 INNER JOIN P_PROPERTY_ASBESTOS_RISK AR ON ARL.PROPASBLEVELID = AR.PROPASBLEVELID  
 INNER JOIN P_ASBESTOS A ON ARL.ASBESTOSID = A.ASBESTOSID
 INNER JOIN P_ASBRISKLEVEL RL ON ARL.ASBRISKLEVELID = RL.ASBRISKLEVELID  
 WHERE P.PROPERTYID = @propertyId  

--=================================================
--Remove temp tables
--=================================================

DROP TABLE #RISKS
DROP TABLE #VULNERABILITY
DROP TABLE #Customers

END
GO
/****** Object:  StoredProcedure [dbo].[DF_ScheduleAppointment]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  04/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         01/09/2015     Aamir Waheed       Created:DF_ScheduleAppointment
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int,
		@totalCount int

EXEC	@return_value = [dbo].[DF_ScheduleAppointment]
		@totalCount = @totalCount OUTPUT

SELECT	@totalCount as N'@totalCount'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_ScheduleAppointment](
		@journalId INT
		,@appointmentStartDateTime DATETIME
		,@appointmentEndDateTime DATETIME
		,@operativeId INT
		,@schedularId INT
		,@appointmentNotes NVARCHAR(2000)
		,@jobSheetNotes NVARCHAR(2000)
		,@tradeId INT
		,@duration float
		,@defectIds NVARCHAR(500)
		
		-- Output Parameters to return values
		,@appointmentId INT OUTPUT
		,@saveStatus BIT OUTPUT
) 
AS
BEGIN

DECLARE @currentTimeStamp DATETIME2 = GETDATE()
DECLARE @Today DATE = @currentTimeStamp

SET NOCOUNT ON;
	
BEGIN TRANSACTION
BEGIN TRY

-- Get Appointment Type Id (MSATTypeId) from PDR_MSATType by giving MSATTypeName
DECLARE @appointmentTypeId INT
SELECT @appointmentTypeId = MSATTypeId FROM PDR_MSATType WHERE MSATTypeName = 'Appliance Defect'

-- Get JournalStatusId by Status sting from PDR_STATUS
DECLARE @JournalStatusId INT
SELECT @JournalStatusId = STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'

-- Get Tenancy Detail
DECLARE @TenancyId INT
SELECT @TenancyId = T.TENANCYID
FROM AS_JOURNAL AJ
LEFT JOIN C_TENANCY T ON AJ.PROPERTYID = T.PROPERTYID AND (T.ENDDATE IS NULL OR T.ENDDATE >= @Today)
WHERE AJ.JOURNALID = @journalId

-- Insert an entry in PDR_MSAT and PRD_Journal and get its key(s)
DECLARE @MSATId INT
INSERT INTO PDR_MSAT(PropertyId, MSATTypeId, IsActive, TenancyId)
SELECT PROPERTYID AS PropertyId, @appointmentTypeId AS MSATTypeId, 1 AS ISACTIVE, @TenancyId AS TenancyId
FROM AS_JOURNAL WHERE JOURNALID = @journalId
SELECT @MSATId = SCOPE_IDENTITY()

-- --------
DECLARE @ApplianceDefectAppointmentJournalId INT
INSERT INTO PDR_JOURNAL(MSATID, STATUSID, CREATIONDATE, CREATEDBY)
VALUES (@MSATId, @JournalStatusId, @currentTimeStamp, @schedularId)
SELECT @ApplianceDefectAppointmentJournalId = SCOPE_IDENTITY()

DECLARE @pdrJournalHistoryId BIGINT
SELECT @pdrJournalHistoryId = MAX(JOURNALHISTORYID) FROM PDR_JOURNAL_HISTORY WHERE JOURNALID = @ApplianceDefectAppointmentJournalId

-- Insert an entry in PDR_Appointment and get its key for output

INSERT INTO PDR_APPOINTMENTS(TENANCYID, JOURNALID, JOURNALHISTORYID, APPOINTMENTSTARTDATE, APPOINTMENTENDDATE
		, APPOINTMENTSTARTTIME, APPOINTMENTENDTIME
		, ASSIGNEDTO, CREATEDBY, LOGGEDDATE, APPOINTMENTNOTES, CUSTOMERNOTES
		, APPOINTMENTSTATUS, TRADEID, DURATION)
VALUES(@TenancyId, @ApplianceDefectAppointmentJournalId, @pdrJournalHistoryId, CONVERT(DATE,@appointmentStartDateTime), CONVERT(DATE, @appointmentEndDateTime)
		, LTRIM(REPLACE(REPLACE(SUBSTRING(CONVERT(NVARCHAR, @appointmentStartDateTime, 0),13,7),'AM',' AM'), 'PM',' PM')), LTRIM(REPLACE(REPLACE(SUBSTRING(CONVERT(NVARCHAR, @appointmentEndDateTime, 0),13,7),'AM',' AM'), 'PM',' PM'))
		, @operativeId, @schedularId, @currentTimeStamp, @appointmentNotes, @jobSheetNotes
		, 'NotStarted', @tradeId, @duration)

SELECT @appointmentId = SCOPE_IDENTITY()

-- Update Defects (Defect Job Sheets)
UPDATE P_PROPERTY_APPLIANCE_DEFECTS
	SET DefectJobSheetStatus = @JournalStatusId
		,ApplianceDefectAppointmentJournalId = @ApplianceDefectAppointmentJournalId
WHERE PropertyDefectId IN (SELECT COLUMN1 FROM dbo.SPLIT_STRING(@defectIds, ','))

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @saveStatus = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;
		SET @saveStatus = 1
	END
END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetRiskAndVulnerabilityInfoByJournalId]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  DefectScheduling.aspx
 
    Author: Aamir Waheed
    Creation Date:  04/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         11/08/2015     Aamir Waheed       Created:DF_GetRiskAndVulnerabilityInfoByJournalId
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetRiskAndVulnerabilityInfoByJournalId]
		@JournalId = 1277

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetRiskAndVulnerabilityInfoByJournalId](
	@JournalId INT
)
AS
BEGIN
--=================================================
-- Get CustomerId to get Tenant Detail(s) and Risk
--=================================================

DECLARE @Customers TABLE(CUSTOMERID INT)

INSERT INTO @Customers(CUSTOMERID)
SELECT CT.CUSTOMERID
FROM C_TENANCY T
	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID 
	AND (CT.ENDDATE IS NULL OR CT.ENDDATE > CAST(GETDATE() AS DATE))
	AND (T.ENDDATE IS NULL OR T.ENDDATE > CAST(GETDATE() AS DATE))
	INNER JOIN AS_JOURNAL AJ ON T.PROPERTYID = AJ.PROPERTYID
WHERE AJ.JOURNALID = @JournalId
ORDER BY CT.CUSTOMERTENANCYID ASC

--=================================================
-- Create temp tables to get Customer/Tenant Risk and Vulnerability Detail(s)
--=================================================

DECLARE @RISKS TABLE(CATDESC NVARCHAR(60), SUBCATDESC NVARCHAR(60))
DECLARE @VULNERABILITY TABLE(CATDESC NVARCHAR(50), SUBCATDESC NVARCHAR(140))

DECLARE @CustomerId INT = NULL
SELECT @CustomerId = MIN(CUSTOMERID) FROM @Customers WHERE CUSTOMERID > @CustomerId
WHILE (@CustomerId IS NOT NULL)
BEGIN

--=================================================
--Fill Customer/Tenant Risk Detail(s) in temp tables
--=================================================
INSERT INTO @RISKS
SELECT	CATDESC, SUBCATDESC
FROM	RISK_CATS_SUBCATS(@CustomerId)

--=================================================
--Fill Customer/Tenant Vulnerability Detail(s)  in temp table
--=================================================
DECLARE @VULNERABILITYHISTORYID int = 0

SELECT  @VULNERABILITYHISTORYID = VULNERABILITYHISTORYID
FROM	C_JOURNAL J
		INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID
WHERE	CUSTOMERID = @customerId
		AND ITEMNATUREID = 61
		AND CV.ITEMSTATUSID <> 14
		AND CV.VULNERABILITYHISTORYID = (	SELECT MAX(VULNERABILITYHISTORYID)
											FROM C_VULNERABILITY IN_CV
											WHERE IN_CV.JOURNALID = J.JOURNALID)
  
INSERT INTO @VULNERABILITY
EXECUTE VULNERABILITY_CAT_SUBCAT @VULNERABILITYHISTORYID  


SELECT	@CustomerId = MIN(CUSTOMERID) 
FROM	@Customers 
WHERE	CUSTOMERID > @CustomerId

END

--=================================================
--Get Customer/Tenant Risk Detail(s)
--=================================================
SELECT	DISTINCT * 
FROM	@RISKS

--=================================================
--Get Customer/Tenant Vulnerability Detail(s)
--=================================================
SELECT	DISTINCT * 
FROM	@VULNERABILITY

END
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAvailableOperatives]    Script Date: 10/07/2015 10:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: DefectAvailableAppointments.aspx
 
    Author: Aamir Waheed
    Creation Date:  26/08/2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         26/08/2015     Aamir Waheed       Created:DF_GetAvailableOperatives, to get available operative
    
Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[DF_GetAvailableOperatives]
						@tradeIds = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18'

SELECT	'Return Value' = @return_value

----------------------------------------------------
*/

CREATE PROCEDURE [dbo].[DF_GetAvailableOperatives]
	-- Add the parameters for the stored procedure here  
	@tradeIds		AS VARCHAR(MAX)	
AS
BEGIN

SET NOCOUNT ON;

--=================================================================================================================  
------------------------------------------------------ Step 0------------------------------------------------------
-- Declare table variable for available operatives and other local variables

DECLARE @AvailableOperatives AS TABLE(EmployeeId INT
, FirstName NVARCHAR(100)
, LastName NVARCHAR(200)
, FullName NVARCHAR(400)
, IsGasSafe BIT
, IsOftec BIT
, PatchId INT
, PatchName NVARCHAR(20)
, Distance NVARCHAR(10)
, TradeID INT)

DECLARE @Today DATE = CONVERT(DATE, GETDATE())

--=================================================================================================================  
------------------------------------------------------ Step 1------------------------------------------------------  
--This query fetches the employees which matches with the following criteria  
--Trade of employee is same as trade(s) of defect(s) (or provided trades)
-- Optional: User type is 'Fuel Servicing'

INSERT INTO @AvailableOperatives (
				EmployeeId
				,FirstName
				,LastName
				,FullName
				,IsGasSafe
				,IsOftec
				,PatchId
				,PatchName
				,Distance
				,TradeID
			)
	SELECT
		E.EMPLOYEEID
		,E.FIRSTNAME
		,E.LASTNAME
		,E.FIRSTNAME + ' ' + E.LASTNAME
		,ISNULL(JD.IsGasSafe, 0)
		,ISNULL(JD.IsOftec, 0)
		,ISNULL(JD.PATCH, -1)
		,P.LOCATION
		,0
		,ET.TradeId
	FROM
		E__EMPLOYEE E
			INNER JOIN E_JOBDETAILS JD ON E.EMPLOYEEID = JD.EMPLOYEEID
				AND
				(JD.ACTIVE = 1)
			LEFT JOIN E_PATCH P ON JD.PATCH = P.PATCHID
			INNER JOIN E_TRADE ET ON E.EMPLOYEEID = ET.EmpId

			-- To filter operatives for a specifiec inspection type (uncomment two joins below)
			--INNER JOIN AS_USER_INSPECTIONTYPE UIT ON E.EMPLOYEEID = UIT.EmployeeId  
			--INNER JOIN P_INSPECTIONTYPE IT ON UIT.InspectionTypeID = IT.InspectionTypeID -- AND (IT.Description  = 'Fuel Servicing' )

			-- Filter to skip out operative(s) on sick leave (and not returned to work).
			OUTER APPLY
			(
				SELECT
					EMPLOYEEID
				FROM
					E_JOURNAL EJ
						CROSS APPLY
						(
							SELECT
								MAX(ABSENCEHISTORYID) MaxABSENCEHISTORYID
							FROM
								E_ABSENCE iA
							WHERE
								iA.JOURNALID = EJ.JOURNALID
						) MaxAbsence
						INNER JOIN E_ABSENCE A ON EJ.JOURNALID = A.JOURNALID
							AND
							A.ABSENCEHISTORYID = MaxAbsence.MaxABSENCEHISTORYID
				WHERE
					ITEMNATUREID = 1
					AND A.RETURNDATE IS NULL
					AND A.ITEMSTATUSID = 1
					AND EJ.EMPLOYEEID = E.EMPLOYEEID
			) SickEmp

	WHERE
		1 = 1
		AND ET.TradeId IN
		(
			SELECT
				COLUMN1
			FROM
				dbo.SPLIT_STRING(@tradeIds, ',')
		)
		-- Filter to skip out operative(s) on sick leave (and not returned to work).
		AND SickEmp.EMPLOYEEID IS NULL

---------------------------------------------------------  

SELECT
	EmployeeId
	,FirstName
	,LastName
	,FullName
	,IsGasSafe
	,IsOftec
	,PatchId
	,PatchName
	,Distance
	,TradeID
FROM
	@AvailableOperatives
--=================================================================================================================  
------------------------------------------------------ Step 2------------------------------------------------------        
--This query selects the leaves of employees i.e the employess which we get in step1  
--M : morning - 08:00 AM - 12:00 PM  
--A : mean after noon - 01:00 PM - 05:00 PM  
--F : Single full day  
--F-F : Multiple full days  
--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM  
--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days  


SELECT
	A.STARTDATE		AS StartDate
	,A.RETURNDATE	AS EndDate
	,EJ.employeeid	AS OperativeId
	,A.HolType
	,A.duration
	,CASE
		WHEN (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
			.STARTDATE)))
			THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
		WHEN HolType = ''
			THEN '08:00 AM'
		WHEN HolType = 'M'
			THEN '08:00 AM'
		WHEN HolType = 'A'
			THEN '01:00 PM'
		WHEN HolType = 'F'
			THEN '00:00 AM'
		WHEN HolType = 'F-F'
			THEN '00:00 AM'
		WHEN HolType = 'F-M'
			THEN '00:00 AM'
		WHEN HolType = 'A-F'
			THEN '01:00 PM'
	END				AS StartTime
	,CASE
		WHEN
			(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
			.RETURNDATE)))
			THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
		WHEN HolType = ''
			THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
		WHEN HolType = 'M'
			THEN '12:00 PM'
		WHEN HolType = 'A'
			THEN '05:00 PM'
		WHEN HolType = 'F'
			THEN '11:59 PM'
		WHEN HolType = 'F-F'
			THEN '11:59 PM'
		WHEN HolType = 'F-M'
			THEN '12:00 PM'
		WHEN HolType = 'A-F'
			THEN '11:59 PM'
	END				AS EndTime
	,CASE
		WHEN
			(A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE
			)))
			THEN DATEDIFF(mi, '1970-01-01', A.STARTDATE)
		WHEN HolType = ''
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
		WHEN HolType = 'M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
		WHEN HolType = 'A'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
		WHEN HolType = 'F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
		WHEN HolType = 'F-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
		WHEN HolType = 'F-M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
		WHEN HolType = 'A-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
	END				AS StartTimeInMin
	,CASE
		WHEN
			(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE
			)))
			THEN DATEDIFF(mi, '1970-01-01', A.RETURNDATE)
		WHEN HolType = ''
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00', 103))
		WHEN HolType = 'M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
		WHEN HolType = 'F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		WHEN HolType = 'F-M'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
		WHEN HolType = 'A-F'
			THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
	END				AS EndTimeInMin
FROM
	E_JOURNAL EJ
		INNER JOIN @AvailableOperatives AO ON EJ.employeeid = AO.employeeid
		CROSS APPLY
		(
			SELECT
				MAX(ABSENCEHISTORYID) MaxABSENCEHISTORYID
			FROM
				E_ABSENCE iA
			WHERE
				iA.JOURNALID = EJ.JOURNALID
		) MaxAbsence
		INNER JOIN E_ABSENCE A ON EJ.JOURNALID = A.JOURNALID
			AND
			A.ABSENCEHISTORYID = MaxAbsence.MaxABSENCEHISTORYID
WHERE
	(
	-- To filter for planned i.e annual leaves etc. where approval is needed  
	(A.ITEMSTATUSID = 5
	AND EJ.ITEMNATUREID >= 2
	)
	OR
	-- To filter for sickness leaves. where the operative is now returned to work.  
	(ITEMNATUREID = 1
	AND A.ITEMSTATUSID = 2
	AND A.RETURNDATE IS NOT NULL)
	)
	AND A.RETURNDATE >= @Today
-- Check for bank holidays	
UNION ALL SELECT DISTINCT
	CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS StartDate
	,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS EndDate
	,AO.EmployeeId																																		AS OperativeId
	,'F'																																				AS HolType
	,1																																					AS duration
	,'00:00 AM'																																			AS StartTime
	,'11:59 PM'																																			AS EndTime
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
	,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
FROM
	G_BANKHOLIDAYS bh
		CROSS JOIN @AvailableOperatives AO
WHERE
	bh.BHDATE >= @Today
	AND bh.BHA = 1

--=================================================================================================================  
------------------------------------------------------ Step 3------------------------------------------------------        
--This query selects the appointments of employees i.e the employess which we get in step1  
SELECT
	A.AppointmentDate
	,A.OperativeID
	,A.Time																				AS StartTime
	,A.EndTime																			AS EndTime
	,DATEDIFF(SECOND, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10),
	A.AppointmentDate, 103) + ' ' + Time, 103))											AS StartTimeInSec
	,DATEDIFF(SECOND, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10),
	COALESCE(A.AppointmentEndDate, A.AppointmentDate), 103) + ' ' + A.EndTime, 103))	AS EndTimeInSec
	,CASE
		WHEN FL.PROPERTYID IS NOT NULL
			THEN ISNULL(P.POSTCODE, '')
		WHEN FL.BLOCKID IS NOT NULL
			THEN ISNULL(P_BLOCK.POSTCODE, '')
		WHEN FL.SCHEMEID IS NOT NULL
			THEN ISNULL(PDR_DEVELOPMENT.PostCode, '')
		ELSE ''
	END																					AS PostCode

	,CASE
		WHEN FL.PROPERTYID IS NOT NULL
			THEN ISNULL(P.HouseNumber + ' ', '')
				+ ISNULL(P.ADDRESS1, '')
				+ ISNULL(', ' + P.ADDRESS2, '')
				+ ISNULL(', ' + P.ADDRESS3, '')
		WHEN FL.BLOCKID IS NOT NULL
			THEN ISNULL(P_BLOCK.ADDRESS1, '')
		WHEN FL.SCHEMEID IS NOT NULL
			THEN ISNULL(P_SCHEME.SCHEMENAME + ', ', '')
		ELSE ''
	END																					AS Address
	,CASE
		WHEN FL.PROPERTYID IS NOT NULL
			THEN ISNULL(P.TownCity, '')
		WHEN FL.BLOCKID IS NOT NULL
			THEN ISNULL(P_BLOCK.TOWNCITY, '')
		WHEN FL.SCHEMEID IS NOT NULL
			THEN ISNULL(PDR_DEVELOPMENT.TOWN, '')
		ELSE ''
	END																					AS TownCity
	,CASE
		WHEN FL.PROPERTYID IS NOT NULL
			THEN ISNULL(P.COUNTY, '')
		WHEN FL.BLOCKID IS NOT NULL
			THEN ISNULL(P_BLOCK.COUNTY, '')
		WHEN FL.SCHEMEID IS NOT NULL
			THEN ISNULL(PDR_DEVELOPMENT.COUNTY, '')
		ELSE ''
	END																					AS County
	,'Fault Appointment'																AS AppointmentType
FROM
	FL_CO_Appointment A
		INNER JOIN FL_FAULT_APPOINTMENT FA ON A.AppointmentID = FA.AppointmentId
		CROSS APPLY
		(
			SELECT
				COUNT(iFA.FaultLogId) AS [NotPausedCount]
			FROM
				FL_FAULT_APPOINTMENT iFA
					INNER JOIN FL_FAULT_LOG iFL ON iFA.FaultLogId = iFL.FaultLogId
					INNER JOIN FL_FAULT_STATUS FS ON iFL.StatusID = FS.FaultStatusID
						AND
						FS.Description NOT IN ('Cancelled', 'Complete')
					OUTER APPLY
					(
						SELECT
							MAX(FaultLogHistoryID) FaultLogHistoryIDMax
						FROM
							FL_FAULT_LOG_HISTORY
						WHERE
							FaultLogID = iFL.FaultLogID
					) FLHMAX
					LEFT JOIN FL_FAULT_LOG_HISTORY FLH ON iFL.FaultLogID = FLH.FaultLogId
						AND
						FLH.FaultLogHistoryID = FLHMAX.FaultLogHistoryIDMax
					LEFT JOIN FL_FAULT_PAUSED FP ON FLH.FaultLogHistoryID = FP.FaultLogHistoryID
						AND
						FP.Reason = 'Parts Required'
			WHERE
				FP.PauseID IS NULL
				AND iFA.AppointmentId = A.AppointmentID
		) FilteredAppointments
		INNER JOIN FL_FAULT_LOG FL ON FA.faultlogid = FL.FaultLogID
		LEFT JOIN P__PROPERTY P ON FL.PROPERTYID = P.PROPERTYID
		LEFT JOIN P_SCHEME ON FL.SCHEMEID = P_SCHEME.SCHEMEID
		LEFT JOIN P_BLOCK ON FL.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
		INNER JOIN @AvailableOperatives AO ON A.OperativeID = AO.employeeid
WHERE
	A.AppointmentDate >= @Today
	AND FilteredAppointments.NotPausedCount > 0

-----------------------------------------------------------------------------------------------------------------------    
--Appliance Appointments     
UNION ALL SELECT
DISTINCT
	AS_APPOINTMENTS.AppointmentDate																																	AS AppointmentDate
	--,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate    
	,AS_APPOINTMENTS.ASSIGNEDTO																																		AS OperativeId
	,APPOINTMENTSTARTTIME																																			AS StartTime
	,APPOINTMENTENDTIME																																				AS EndTime
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), AS_APPOINTMENTS.AppointmentDate, 103) + ' ' + APPOINTMENTENDTIME, 103))						AS EndTimeInSec
	,p__property.postcode																																			AS PostCode
	,ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')	AS Address
	,P__PROPERTY.TownCity																																			AS TownCity
	,P__PROPERTY.County																																				AS County
	,'Gas Appointment'																																				AS AppointmentType
FROM
	AS_APPOINTMENTS
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
			AND
			(DBO.C_TENANCY.ENDDATE IS NULL
				OR DBO.C_TENANCY.ENDDATE > GETDATE())
		INNER JOIN @AvailableOperatives AO ON AS_APPOINTMENTS.ASSIGNEDTO = AO.employeeid
WHERE
	AS_JOURNAL.IsCurrent = 1
	AND AS_APPOINTMENTS.AppointmentDate >= @Today
	AND (AS_Status.Title <> 'Cancelled'
	AND APPOINTMENTSTATUS <> 'Complete')
-----------------------------------------------------------------------------------------------------------------------    
--Planned Appointments    
UNION ALL SELECT
DISTINCT
	APPOINTMENTDATE																																					AS AppointmentDate
	--,APPOINTMENTENDDATE as AppointmentEndDate    
	,ASSIGNEDTO																																						AS OperativeId
	,APPOINTMENTSTARTTIME																																			AS StartTime
	,APPOINTMENTENDTIME																																				AS EndTime
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))										AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))									AS EndTimeInSec
	,p__property.postcode																																			AS PostCode
	,ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')	AS Address
	,P__PROPERTY.TownCity																																			AS TownCity
	,P__PROPERTY.County																																				AS County
	,'Planned Appointment'																																			AS AppointmentType
FROM
	PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId
		INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN @AvailableOperatives AO ON PLANNED_APPOINTMENTS.ASSIGNEDTO = AO.employeeid
WHERE
	(
	AppointmentDate >= @Today
	OR AppointmentEndDate >= @Today
	)
	AND (PLANNED_STATUS.TITLE <> 'Cancelled'
	AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	)
-- PDR Appointments	
UNION ALL SELECT DISTINCT
	APPOINTMENTSTARTDATE																												AS AppointmentDate
	,ASSIGNEDTO																															AS OperativeId
	,APPOINTMENTSTARTTIME																												AS StartTime
	,APPOINTMENTENDTIME																													AS EndTime
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTSTARTDATE, 103) + ' ' + APPOINTMENTSTARTTIME, 103))	AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), APPOINTMENTENDDATE, 103) + ' ' + APPOINTMENTENDTIME, 103))		AS EndTimeInSec
	,COALESCE(p__property.postcode, P_SCHEME.SCHEMECODE, P_BLOCK.POSTCODE)																AS PostCode
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ISNULL(P_SCHEME.SCHEMENAME, '')
		ELSE ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
			+ ISNULL(P_BLOCK.ADDRESS1, '')
			+ ISNULL(', ' + P_BLOCK.ADDRESS2, '')
			+ ISNULL(' ' + P_BLOCK.ADDRESS3, '')
	END																																	AS Address
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN P__PROPERTY.TOWNCITY
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ''
		ELSE P_BLOCK.TOWNCITY
	END																																	AS TownCity
	,CASE
		WHEN PDR_MSAT.propertyid IS NOT NULL
			THEN P__PROPERTY.COUNTY
		WHEN PDR_MSAT.SCHEMEID IS NOT NULL
			THEN ''
		ELSE P_BLOCK.COUNTY
	END																																	AS County
	,PDR_MSATType.MSATTypeName + ' Appointment'																							AS AppointmentType
FROM
	PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
		LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID
		LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
		INNER JOIN @AvailableOperatives AO ON PDR_APPOINTMENTS.ASSIGNEDTO = AO.employeeid
WHERE
	(
	APPOINTMENTSTARTDATE >= @Today
	OR AppointmentEndDate >= @Today
	)
	AND (PDR_STATUS.TITLE <> 'Cancelled'
	AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled')
	)

END
GO


-- ============================================= 
-- Author:          Ali Raza 
-- Create date:      08/10/2015 
-- Description:      Save Detectors 
-- History:          08/10/2015 AR : Save detectors for property 
-- ============================================= 
CREATE PROCEDURE Df_savedetectors (@propertyId          NVARCHAR(20), 
                                  @detectorType        NVARCHAR(10), 
                                  @Location            NVARCHAR(400), 
                                  @Manufacturer        NVARCHAR(200), 
                                  @SerialNumber        NVARCHAR(200), 
                                  @PowerSource         INT = NULL, 
                                  @InstalledDate       DATETIME=NULL, 
                                  @InstalledBy         INT = NULL, 
                                  @IsLandlordsDetector BIT = NULL, 
                                  @TestedDate          DATETIME = NULL, 
                                  @BatteryReplaced     DATETIME = NULL, 
                                  @Passed              BIT = NULL, 
                                  @Notes               NVARCHAR(2000), 
                                  @saveStatus          BIT output) 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from 
      -- interfering with SELECT statements. 
      SET nocount ON; 

      BEGIN TRANSACTION 

      BEGIN try 
          DECLARE @DetectorTypeId INT 

          SELECT @DetectorTypeId = detectortypeid 
          FROM   as_detectortype 
          WHERE  as_detectortype.detectortype LIKE '%' + @detectorType + '%' 

          INSERT INTO p_detector 
                      (PropertyId, 
                       DetectorTypeId, 
                       Location, 
                       Manufacturer, 
                       SerialNumber, 
                       PowerSource, 
                       InstalledDate, 
                       InstalledBy, 
                       IsLandlordsDetector, 
                       TestedDate, 
                       BatteryReplaced, 
                       Passed, 
                       Notes) 
          VALUES      (@propertyId, 
                       @DetectorTypeId, 
                       @Location, 
                       @Manufacturer, 
                       @SerialNumber, 
                       @PowerSource, 
                       @InstalledDate, 
                       @InstalledBy, 
                       @IsLandlordsDetector, 
                       @TestedDate, 
                       @BatteryReplaced, 
                       @Passed, 
                       @Notes) 
      END try 

      BEGIN catch 
          IF @@TRANCOUNT > 0 
            BEGIN 
                ROLLBACK TRANSACTION; 

                SET @saveStatus = 0 
            END 

          DECLARE @ErrorMessage NVARCHAR(4000); 
          DECLARE @ErrorSeverity INT; 
          DECLARE @ErrorState INT; 

          SELECT @ErrorMessage = Error_message(), 
                 @ErrorSeverity = Error_severity(), 
                 @ErrorState = Error_state(); 

          -- Use RAISERROR inside the CATCH block to return  
          -- error information about the original error that  
          -- caused execution to jump to the CATCH block. 
          RAISERROR (@ErrorMessage,-- Message text. 
                     @ErrorSeverity,-- Severity. 
                     @ErrorState -- State. 
          ); 
      END catch; 

      IF @@TRANCOUNT > 0 
        BEGIN 
            COMMIT TRANSACTION; 

            SET @saveStatus = 1 
        END 
  END 
  
  
  
-- =============================================
-- Author:          Ali Raza
-- Create date:      08/10/2015
-- Description:      Detectors list
-- History:          08/10/2015 AR : get detectors list by propertyId

--EXEC DF_GetDetectorByPropertyId 'BHA0000727','Smoke'
-- =============================================
CREATE PROCEDURE DF_GetDetectorByPropertyId
	(
	@propertyId nvarchar(20),
	@detectorType nvarchar(10)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE @propertyId nvarchar(20)='C390100007',@detectorType nvarchar(10)='CO'
SELECT
DetectorId,PropertyId,AS_DetectorType.DetectorTypeId,Location,Manufacturer,SerialNumber,PowerSource
 ,CONVERT(NVARCHAR, InstalledDate, 103) as Installed
 ,InstalledBy
 ,IsLandlordsDetector
 ,CONVERT(NVARCHAR, InstalledDate, 103)as TestedDate
 ,TestedBy
 ,CONVERT(NVARCHAR, BatteryReplaced, 103)as BatteryReplaced
 ,Passed
 ,Notes 
 ,AS_DetectorType.DetectorType
 ,E.FIRSTNAME + ISNULL(' ' + E.LASTNAME, '')			AS InstalledByPerson
 ,P_PowerSourceType.PowerType as [Power]
 FROM
  P_DETECTOR  
  Left JOIN P_PowerSourceType ON P_DETECTOR.PowerSource = P_PowerSourceType.PowerTypeId And IsActive=1 
  INNER JOIN  AS_DetectorType ON P_DETECTOR.DetectorTypeId = AS_DetectorType.DetectorTypeId
  LEFT JOIN E__EMPLOYEE AS E ON P_DETECTOR.InstalledBy = E.EMPLOYEEID
  where PropertyId =   @propertyId AND AS_DetectorType.DetectorType like '%'+@detectorType+'%'  
  
Select PowerTypeId, PowerType from P_PowerSourceType WHERE IsActive=1
END
GO
