USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_PropertyActivities]    Script Date: 09/17/2015 14:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
/* =============================================              
 --EXEC AS_PropertyActivities @propertyId ='B060080001'              
 -- ,@actionType = -1              
 -- ,@sortColumn = 'JournalHistoryId'              
 -- ,@sortOrder = 'DESC'             
-- Author:  <Noor Muhammad>              
-- Create date: <10/1/2012>              
-- Description: <This stored procedure returns the all the activities against the property>              
-- Parameters:               
  --@propertyId varchar(50),              
  --@actionType int = -1,                 
                 
  ---- column name on which sorting is performed              
  --@sortColumn varchar(50) = 'JournalHistoryId',              
  --@sortOrder varchar (5) = 'DESC'              
-- Webpage :PropertyRecord.aspx              
-- Modified By: <April 20, 2103, Aamir Waheed>              
-- Modified By: Abdullah Saeed <August 15,2014>            
-- ============================================= */      
ALTER PROCEDURE [dbo].[AS_PropertyActivities]  
(               
  @propertyId varchar(50),              
  @actionType int = -1,                  
  -- column name on which sorting is performed              
  @sortColumn varchar(50) = 'CreateDate',              
  @sortOrder varchar (5) = 'DESC'              
                
)              
AS              
BEGIN      
              
              
 DECLARE @PlannedSelectClause NVARCHAR(MAX),              
   @PlannedFromClause NVARCHAR(MAX),              
   @PlannedWhereClause NVARCHAR(MAX),              
   @PlannedOrderClause NVARCHAR(MAX),             
   @PdrSelectClause NVARCHAR(MAX),            
   @PdrFromClause NVARCHAR(MAX),            
   @PdrWhereClause NVARCHAR(MAX),            
   @PdrOrderClause NVARCHAR(MAX),       
               
   @StockConditionSelectClause NVARCHAR(MAX),            
   @StockConditionFromClause NVARCHAR(MAX),            
   @StockConditionWhereClause NVARCHAR(MAX),             
                 
   @ConditionSelectClause NVARCHAR(MAX),              
   @ConditionFromClause NVARCHAR(MAX),              
   @ConditionWhereClause NVARCHAR(MAX),              
   @ConditionOrderClause NVARCHAR(MAX),             
                 
   @unionClause varchar(100),              
   @mainSelectQuery Nvarchar(MAX),            
               
   @ApplianceSelectClause NVARCHAR(MAX),              
   @ApplianceFromClause NVARCHAR(MAX),              
   @ApplianceWhereClause NVARCHAR(MAX),              
   @ApplianceOrderClause NVARCHAR(MAX) ,   
   
   @ApplianceDefectSelectClause NVARCHAR(MAX),          
   @ApplianceDefectFromClause NVARCHAR(MAX),          
   @ApplianceDefectWhereClause NVARCHAR(MAX),
   @ApplianceDefectOrderClause varchar(100),
   
                  
   @orderClause  varchar(100),              
   @FaultSelectClause NVARCHAR(MAX),            
   @FaultFromClause NVARCHAR(MAX),            
   @FaultWhereClause NVARCHAR(MAX)  ,  
     
   @GasElectricSelectClause NVARCHAR(MAX),            
   @GasElectricFromClause NVARCHAR(MAX),            
   @GasElectricWhereClause NVARCHAR(MAX),            
   @GasElectricOrderClause NVARCHAR(MAX),  
     
     
   @PaintPackSelectClause NVARCHAR(MAX),            
   @PaintPackFromClause NVARCHAR(MAX),            
   @PaintPackWhereClause NVARCHAR(MAX),            
   @PaintPackOrderClause NVARCHAR(MAX),  
        
   @Inspection varchar(100)  
    
--===========================================================================================              
--       INSPECTION DESCRIPTION  
--===========================================================================================      
    
  SELECT @Inspection = P.Description FROM P_INSPECTIONTYPE P WHERE P.InspectionTypeID = @actionType  
    
--===========================================================================================            
--       CONDITION ACTIVITIES              
--===========================================================================================              
      
SET @ConditionSelectClause = '            
  SELECT [PCW].ConditionWorksHistoryId AS JournalHistoryId            
  ,ISNULL(COALESCE(PC.COMPONENTNAME,[I].ItemName) +'' - ''+ PV.ValueDetail,''N/A'') AS Status            
  , CASE  WHEN [PLA].Title =''Rejected'' THEN    
  [PLA].Title + '' - '' + [PCWR].[RejectionNotes]    
 ELSE    
  ISNULL([PLA].Title,''N/A'')     
 END AS Action    
  ,''Condition Works'' AS InspectionType              
  ,CONVERT(varchar(10), [PCW].CreatedDate , 103) AS CreateDate            
  ,SUBSTRING(E.FIRSTNAME,1,1) + '' '' +E.LASTNAME AS Name             
  ,0 AS IsLetterAttached              
  ,0 AS IsDocumentAttached              
  ,''None'' as Document              
  ,0 as CP12DocumentID              
  ,[PCW].CreatedDate AS CREATIONDATE    
  ,(''JSN'' + CONVERT(VARCHAR,A.APPOINTMENTID) ) AS REF     
  ,Convert( NVarchar, A.APPOINTMENTDATE, 103 ) As AppointmentDate     
  ,(IsNULL( OP.FIRSTNAME,'''') + '' '' +Isnull(OP.LASTNAME,'''' ))  As OPERATIVENAME    
  ,T.Description As  OPERATIVETRADE   
  ,[PCW].ConditionWorksId As JournalId   
  ' + CHAR(10)      
      
SET @ConditionFromClause = ' FROM PLANNED_CONDITIONWORKS_HISTORY  [PCW]             
  INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId             
  INNER JOIN PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID             
  INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID              
  INNER JOIN PA_ITEM [I] ON [I].ItemId = [IP].ItemId             
  INNER JOIN PA_PARAMETER_VALUE PV ON [PCW].VALUEID = PV.ValueID                
    
  LEFT JOIN PLANNED_CONDITIONWORKS CW ON CW.ConditionWorksId = PCW.ConditionWorksId    
  LEFT JOIN PLANNED_JOURNAL J ON CW.JournalId = J.JOURNALID    
  LEFT JOIN PLANNED_APPOINTMENTS A ON A.JournalId = J.JOURNALID    
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = A.ASSIGNEDTO    
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID    
  LEFT JOIN G_TRADE T ON T.TradeId = MT.TradeId     
    
  LEFT JOIN E__EMPLOYEE E ON [PCW].CreatedBy = E.EMPLOYEEID     
  LEFT JOIN PA_PROPERTY_ITEM_DATES [PID] ON [PID].ItemId = [IP].ItemID AND [PID].PROPERTYID = [ATT].PROPERTYID AND ([PID].ParameterId = [IP].ParameterId OR [PID].ParameterId IS NULL)            
  LEFT JOIN PLANNED_COMPONENT [PC] ON [PC].COMPONENTID = [PCW].COMPONENTID    
  LEFT JOIN PLANNED_CONDITIONWORKS_REJECTED [PCWR] ON [PCWR].ConditionWorksId = [PCW].ConditionWorksId '    
        
+ CHAR(10)      
      
      
SET @ConditionWhereClause = 'WHERE [ATT].PROPERTYID = ''' + @propertyId + ''''      
+ CHAR(10)      
      
  PRINT '--===================================================================='  
  --PRINT @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause  
--===========================================================================================              
--       PLANNED MAINTENANCE ACTIVITIES              
--===========================================================================================              
     
SET @PlannedSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId              
  ,ISNULL(S.TITLE, ''N/A'')     
  + CASE WHEN S.TITLE = ''Inspection Arranged'' THEN ISNULL('' ('' + SUBSTRING(OP.FIRSTNAME, 1, 1) + '' '' + OP.LASTNAME    
  + '' '' + CONVERT(NVARCHAR,PIA.APPOINTMENTDATE,103)     
  + '' '' + PIA.APPOINTMENTTIME + '')'','''') ELSE '''' END AS Status              
  ,ISNULL(ACT.Title ,''N/A'' ) AS Action              
  ,''Planned - ''+ COALESCE(C.COMPONENTNAME,AT.Planned_Appointment_Type,''N/A'') AS InspectionType              
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate              
  ,ISNULL(E.FIRSTNAME,'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name              
  ,ISNULL(IsLetterAttached, 0) AS IsLetterAttached              
  ,ISNULL(IsDocumentAttached, 0) AS IsDocumentAttached              
  ,''None'' as Document              
  ,0 as CP12DocumentID              
  ,PJH.CREATIONDATE AS CREATIONDATE    
  ,( ''JSN'' + Convert( NVarchar, A.APPOINTMENTID ) ) AS REF     
  ,Convert( NVarchar, A.APPOINTMENTDATE, 103 ) As AppointmentDate     
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME    
  ,T.Description As OPERATIVETRADE   
  ,PJH.JOURNALID As JournalId   
  ' + CHAR(10)      
      
SET @PlannedFromClause = ' FROM PLANNED_JOURNAL_HISTORY PJH        
  LEFT JOIN PLANNED_COMPONENT C ON PJH.COMPONENTID = C.COMPONENTID               
  INNER JOIN PLANNED_STATUS S ON  PJH.STATUSID = S.STATUSID               
  LEFT JOIN PLANNED_Action ACT ON  PJH.ACTIONID = ACT.ActionId               
  LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID        
  LEFT JOIN PLANNED_APPOINTMENTS_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID        
  LEFT JOIN PLANNED_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID        
  LEFT JOIN Planned_Appointment_Type AT ON AT.Planned_Appointment_TypeId = A.Planned_Appointment_TypeId     
  LEFT JOIN PLANNED_INSPECTION_APPOINTMENTS PIA ON PIA.JournalId = PJH.JOURNALID    
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PIA.ASSIGNEDTO OR OP.EMPLOYEEID = AH.ASSIGNEDTO      
  LEFT JOIN PLANNED_COMPONENT_TRADE CT ON CT.COMPTRADEID = A.COMPTRADEID    
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID    
  LEFT JOIN G_TRADE T ON T.TRADEID = CT.TRADEID OR T.TradeId = MT.TradeId '    
+ CHAR(10)      
      
SET @PlannedWhereClause = 'WHERE PJH.PROPERTYID = ''' + @propertyId + ''''      
+ CHAR(10)      
      PRINT '--===================================================================='  
  ---PRINT @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause  
    
--===========================================================================================            
--       PDR ACTIVITIES            
--===========================================================================================            
  
SET @PdrSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId            
  ,ISNULL(S.TITLE, ''N/A'')  AS Status            
  ,''N/A''  AS Action            
  ,MSATTYPE.MSATTypeName +'' - ''+ ISNULL(PL.LocationName +'' > ''+ PA.AreaName + '' > '' + PT.ItemName,''N/A'') AS InspectionType            
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate            
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name            
  ,0 AS IsLetterAttached            
  ,0 AS IsDocumentAttached            
  ,''None'' as Document            
  ,0 as CP12DocumentID            
  ,PJH.CREATIONDATE AS CREATIONDATE     
  ,( ''JSN'' + Convert( NVarchar, A.APPOINTMENTID ) ) AS REF     
  ,Convert( NVarchar, A.APPOINTMENTSTARTDATE, 103 ) As AppointmentDate     
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME    
  ,T.Description As OPERATIVETRADE  
  ,PJH.JOURNALID As JournalId   
   ' + CHAR(10)    
    
SET @PdrFromClause = ' FROM PDR_JOURNAL_HISTORY PJH  
 INNER JOIN PDR_MSAT MSAT ON PJH.MSATID = MSAT.MSATId  
 INNER JOIN PDR_MSATType MSATTYPE ON MSAT.MSATTypeId = MSATTYPE.MSATTypeId   
 INNER JOIN PDR_STATUS S ON  PJH.STATUSID = S.STATUSID    
 INNER JOIN PA_ITEM PT ON MSAT.ItemId = PT.ItemID  
 INNER JOIN PA_AREA PA ON  PT.AreaID = PA.AreaID  
 INNER JOIN PA_LOCATION PL ON PA.LocationId = PL.LocationID          
 LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID  
 LEFT JOIN PDR_APPOINTMENT_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID        
 LEFT JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID    
 LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO   
 LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID  
     ' + CHAR(10)    
    
    
SET @PdrWhereClause = 'WHERE MSAT.PROPERTYID = ''' + @propertyId + ''''    
  
+ CHAR(10)    
  
IF @actionType != -1  
BEGIN  
 SET @PdrWhereClause = @PdrWhereClause+ ' AND  MSATTYPE.MSATTypeName = '''+ @Inspection + ''''    
END  
      
    PRINT '--===================================================================='  
    --PRINT @PdrSelectClause + @PdrFromClause + @PdrWhereClause  
--===========================================================================================              
--       APPLIANCES ACTIVITIES              
--===========================================================================================         
      
SET @ApplianceSelectClause = 'SELECT DISTINCT              
      AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId              
      ,ISNULL(as_status.title, ''N/A'') as Status              
      ,ISNULL(as_action.title, ''N/A'') as Action              
      ,ISNULL(P_INSPECTIONTYPE.description, ''N/A'') AS InspectionType              
      ,Convert(varchar(10),AS_JOURNALHISTORY.creationdate,103) as CreateDate              
      ,ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name              
      ,ISNULL(IsLetterAttached, 0) AS IsLetterAttached              
      ,ISNULL(IsDocumentAttached, 0) AS IsDocumentAttached   
      ,CASE WHEN P_LGSR_HISTORY.CP12DOCUMENT IS NOT NULL AND as_status.title = ''CP12 issued''  THEN ''cp12.pdf'' ELSE ''None'' END Document   
      ,CASE WHEN P_LGSR_HISTORY.CP12DOCUMENT IS NULL THEN 0 ELSE P_LGSR_HISTORY.LGSRID END AS CP12DocumentID              
      ,AS_JOURNALHISTORY.CREATIONDATE AS CREATIONDATE    
      , ''JSG'' + convert(NVARCHAR, AS_APPOINTMENTSHISTORY.JSGNUMBER ) AS REF     
      ,( Convert( NVarchar, AS_APPOINTMENTSHISTORY.APPOINTMENTDATE, 103 ) + '' '' + AS_APPOINTMENTSHISTORY.APPOINTMENTSTARTTIME + ''-'' + AS_APPOINTMENTSHISTORY.APPOINTMENTENDTIME ) As AppointmentDate     
      ,( IsNULL(SUBSTRING( OP.FIRSTNAME,1,1), '''') + '' '' + isnull( OP.LASTNAME, '''' ) ) As OPERATIVENAME     
      ,''N/A'' As OPERATIVETRADE   
      ,AS_JOURNALHISTORY.JOURNALID As JournalId     
      ' + CHAR(10)      
      
SET @ApplianceFromClause = 'FROM AS_JOURNALHISTORY  
INNER JOIN P_INSPECTIONTYPE ON AS_JOURNALHISTORY.INSPECTIONTYPEID = P_INSPECTIONTYPE.INSPECTIONTYPEID  
INNER JOIN as_status ON as_status.statusid = AS_JOURNALHISTORY.statusid  
LEFT JOIN AS_Action ON AS_Action.actionid = AS_JOURNALHISTORY.actionid  
INNER JOIN e__employee ON e__employee.employeeid = AS_JOURNALHISTORY.createdby  
LEFT JOIN (SELECT LGSRHISTORYID AS LGSRID, PROPERTYID,CP12DOCUMENT,JOURNALID  
FROM P_LGSR_HISTORY WHERE CP12DOCUMENT IS NOT NULL) AS P_LGSR_HISTORY  
 ON P_LGSR_HISTORY.JOURNALID = AS_JOURNALHISTORY.JOURNALID  
LEFT JOIN AS_APPOINTMENTSHISTORY ON AS_JOURNALHISTORY.JOURNALHISTORYID = AS_APPOINTMENTSHISTORY.JOURNALHISTORYID  
LEFT JOIN E__EMPLOYEE OP ON AS_APPOINTMENTSHISTORY.ASSIGNEDTO = OP.EMPLOYEEID  
'      
+ CHAR(10)      
      
      
SET @ApplianceWhereClause = 'WHERE (' + CONVERT(NVARCHAR, @actionType) + '= -1 ' + ' OR as_journalhistory.inspectiontypeid = ' + CONVERT(NVARCHAR, @actionType) + ')              
       AND AS_JOURNALHISTORY.PROPERTYID= ''' + @propertyId + ''''      
+ CHAR(10)      
      
  PRINT '--===================================================================='  
    PRINT @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause  
    
--===========================================================================================            
--       APPLIANCES DEFECT ACTIVITIES            
--===========================================================================================       
    
SET @ApplianceDefectSelectClause = 'SELECT 
		P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId as PropertyDefectId   	 
	  ,CASE WHEN PDR_STATUS.Title = ''Completed'' THEN ''Defect: Completed'' ELSE ISNULL(''Defect Category :''+P_DEFECTS_CATEGORY.Description, ''N/A'') END AS Status                      
      ,CASE WHEN PDR_STATUS.Title IS NOT NULL THEN PDR_STATUS.Title ELSE ''Appointment To Be Arranged'' END as Action              
      ,ISNULL(P_INSPECTIONTYPE.description, ''N/A'') AS InspectionType              
      ,Convert(varchar(10),P_PROPERTY_APPLIANCE_DEFECTS.DefectDate,103) as CreateDate              
       ,ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name               
      ,0 AS IsLetterAttached              
      ,0 AS IsDocumentAttached   
      ,''None'' AS Document   
      ,0 CP12DocumentID              
      ,AS_JOURNAL.CREATIONDATE AS CREATIONDATE    
      ,''N/A'' AS REF           
      ,( Convert( NVarchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103 ) + '' '' + PDR_APPOINTMENTS.APPOINTMENTSTARTTIME + ''-'' + PDR_APPOINTMENTS.APPOINTMENTENDTIME ) As AppointmentDate     
      ,( IsNULL(SUBSTRING( OP.FIRSTNAME,1,1), '''') + '' '' + isnull( OP.LASTNAME, '''' ) ) As OPERATIVENAME      
      ,''N/A'' As OPERATIVETRADE
      ,AS_JOURNAL.JOURNALID As JournalId ' + CHAR(10)    
    
SET @ApplianceDefectFromClause = CHAR(10)+'FROM P_PROPERTY_APPLIANCE_DEFECTS 
	INNER JOIN AS_JOURNAL ON AS_JOURNAL.JOURNALID =P_PROPERTY_APPLIANCE_DEFECTS.JournalId	
	INNER JOIN P_DEFECTS_CATEGORY ON P_PROPERTY_APPLIANCE_DEFECTS.CategoryId = P_DEFECTS_CATEGORY.CategoryId
	INNER JOIN P_INSPECTIONTYPE ON AS_JOURNAL.INSPECTIONTYPEID = P_INSPECTIONTYPE.INSPECTIONTYPEID 
	INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = P_PROPERTY_APPLIANCE_DEFECTS.CreatedBy
	LEFT JOIN PDR_JOURNAL ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceDefectAppointmentJournalId = PDR_JOURNAL.JOURNALID
	LEFT JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JOURNALID
	LEFT JOIN PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
	LEFT JOIN E__EMPLOYEE OP ON PDR_APPOINTMENTS.ASSIGNEDTO = OP.EMPLOYEEID'+ CHAR(10)    
    
    SET @ApplianceDefectOrderClause = CHAR(10)+ 'Order By P_PROPERTY_APPLIANCE_DEFECTS.DefectDate DESC'
    
SET @ApplianceDefectWhereClause = 'WHERE (' + CONVERT(NVARCHAR, @actionType) + '= -1 ' + ' OR P_INSPECTIONTYPE.INSPECTIONTYPEID = ' + CONVERT(NVARCHAR, @actionType) + ')            
       AND P_PROPERTY_APPLIANCE_DEFECTS.PROPERTYID= ''' + @propertyId + ''''
       + CHAR(10)    
                    
    
  PRINT '--===================================================================='
    PRINT @ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
          
--===========================================================================================            
--       FAULT APPOINTMENTS            
--===========================================================================================            
      
SET @FaultSelectClause = 'SELECT DISTINCT FL_FAULT_LOG_HISTORY.FaultLogHistoryID as JournalHistoryId              
        ,ISNULL(FL_FAULT_STATUS.[Description],''N/A'' ) AS Status              
        ,''N/A''  AS Action              
        ,''Fault Repair'' AS InspectionType              
        ,Convert(varchar(10),SubmitDate ,103) as CreateDate              
        ,ISNULL(E__EMPLOYEE.FIRSTNAME,'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'' '')  AS Name              
        ,0 AS IsLetterAttached              
        ,0 AS IsDocumentAttached              
        ,''None'' as Document              
        ,0 as CP12DocumentID              
        ,SubmitDate AS CREATIONDATE     
        ,FL_FAULT_LOG.JobSheetNumber  AS REF     
        ,Convert( NVarchar, FL_CO_APPOINTMENT.AppointmentDate, 103 ) As AppointmentDate     
        ,( ISNULL(SUBSTRING(E__EMPLOYEE.FIRSTNAME,1,1),'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'' '') ) As OPERATIVENAME    
        ,T.Description As OPERATIVETRADE   
         ,FL_FAULT_LOG_HISTORY.JournalID As JournalId   
        ' + CHAR(10)      
    
SET @FaultFromClause = 'from FL_FAULT_LOG_HISTORY    
      Inner Join FL_FAULT_LOG On FL_FAULT_LOG.FaultLogID = FL_FAULT_LOG_HISTORY.FaultLogID             
      LEFT join FL_FAULT_APPOINTMENT on FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID             
      INNER JOIN FL_FAULT_STATUS on FL_FAULT_LOG_HISTORY.FaultStatusID = FL_FAULT_STATUS.FaultStatusID            
      INNER JOIN E__EMPLOYEE On EMPLOYEEID = UserId       
      LEFT JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentID    
      LEFT JOIN E__EMPLOYEE OP On OP.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID    
      LEFT JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeID = FL_FAULT_LOG.FaultTradeID    
      LEFT JOIN G_TRADE T ON FL_FAULT_TRADE.TradeId = T.TradeId    
      ' + CHAR(10)      
    
SET @FaultWhereClause = 'where  FL_FAULT_STATUS.FaultStatusID <>  13 AND FL_FAULT_LOG.PROPERTYID =''' + @propertyId + ''''      
+ CHAR(10)      
    
    PRINT '--===================================================================='  
   -- PRINT @FaultSelectClause + @FaultFromClause + @FaultWhereClause  
    
--===========================================================================================              
--       STOCK CONDITION    
--===========================================================================================              
SET @StockConditionSelectClause = 'select             
      0 as JournalHistoryId,            
      ''Inspection Completed'' As Status,            
      ''N/A'' as Action,            
      ''Stock Condition'' As InspectionType,            
      CONVERT(CHAR(10), PA_Property_Inspection_Record.CreatedDate, 103) As ''CreateDate'',            
      ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name  ,            
      0 AS IsLetterAttached,             
      CASE WHEN PA_Property_Inspection_Record.InspectionDocument IS NOT NULL Then 1 Else 0 END AS IsDocumentAttached,             
      ''None''as Document,            
      PA_Property_Inspection_Record.InspectionId  as CP12DocumentID,            
      PA_Property_Inspection_Record.CreatedDate as CREATIONDATE     
      ,''N/A'' AS REF     
      ,Convert( char, PA_Property_Inspection_Record.inspectionDate, 103 ) As AppointmentDate     
      ,( ISNULL(SUBSTRING( E__EMPLOYEE.FIRSTNAME,1,1),'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'''') ) As OPERATIVENAME    
       ,''N/A'' As OPERATIVETRADE   
       ,InspectionId As JournalId         
      ' + CHAR(10)      
      
SET @StockConditionFromClause = ' from PA_Property_Inspection_Record ' + CHAR(10) +      
   'LEFT JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = PA_Property_Inspection_Record.createdBy     
    LEFT JOIN PS_Appointment2Survey ON PS_Appointment2Survey.SurveyId = PA_Property_Inspection_Record.SurveyId    
    LEFT JOIN PS_Appointment ON PS_Appointment2Survey.AppointId = PS_Appointment.AppointId    
    LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PS_Appointment.SurveyourUserName    
   '      
    
SET @StockConditionWhereClause = 'where PA_Property_Inspection_Record.PropertyId=''' + @propertyId + '''' + CHAR(10)   
  
  PRINT '--===================================================================='  
  --PRINT @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause  
   
   
   
--===========================================================================================            
--       Gas Electric ACTIVITIES            
--===========================================================================================            
  
SET @GasElectricSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId            
  ,ISNULL(S.TITLE, ''N/A'')  AS Status            
  ,''N/A''  AS Action            
  ,MSATTYPE.MSATTypeName  AS InspectionType            
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate            
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name            
  ,0 AS IsLetterAttached            
  ,0 AS IsDocumentAttached            
  ,''None'' as Document            
  ,0 as CP12DocumentID            
  ,PJH.CREATIONDATE AS CREATIONDATE     
  ,( ''JSV'' + Convert( NVarchar, PJH.JournalID ) ) AS REF     
  ,Convert( NVarchar, A.APPOINTMENTSTARTDATE, 103 ) As AppointmentDate     
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME    
  ,T.Description As OPERATIVETRADE  
  ,PJH.JOURNALID As JournalId  
   ' + CHAR(10)    
    
SET @GasElectricFromClause = ' FROM PDR_JOURNAL_HISTORY PJH  
 INNER JOIN PDR_MSAT MSAT ON PJH.MSATID = MSAT.MSATId  
 INNER JOIN PDR_MSATType MSATTYPE ON MSAT.MSATTypeId = MSATTYPE.MSATTypeId   
 INNER JOIN PDR_STATUS S ON  PJH.STATUSID = S.STATUSID           
 LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID  
 LEFT JOIN PDR_APPOINTMENT_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID        
 LEFT JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID    
 LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO   
 LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID   
     ' + CHAR(10)    
    
    
SET @GasElectricWhereClause = 'WHERE MSAT.PROPERTYID = ''' + @propertyId + ''''  + CHAR(10)    
SET @GasElectricWhereClause = @GasElectricWhereClause+ ' AND ( MSATTYPE.MSATTypeName Like ''%Gas Check%'''    
SET @GasElectricWhereClause = @GasElectricWhereClause+ ' OR  MSATTYPE.MSATTypeName Like ''%Electric Check%'')'    
      
    PRINT '--===================================================================='  
   -- PRINT @GasElectricSelectClause + @GasElectricFromClause + @GasElectricWhereClause   
   
--===========================================================================================            
--       Paint Pack ACTIVITIES            
--===========================================================================================            
  
SET @PaintPackSelectClause = 'SELECT  P.PaintPackId as JournalHistoryId            
  ,ISNULL(PS.TITLE, ''N/A'')  AS Status            
  ,''N/A''  AS Action            
  ,''Void Paint Pack''  AS InspectionType            
  ,Convert(varchar(10),P.CreatedDate ,103) as CreateDate            
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name            
  ,0 AS IsLetterAttached            
  ,0 AS IsDocumentAttached            
  ,''None'' as Document            
  ,0 as CP12DocumentID            
  ,P.CreatedDate AS CREATIONDATE     
  ,'''' AS REF     
  ,'''' As AppointmentDate     
  ,'''' As OPERATIVENAME    
  ,''''As OPERATIVETRADE  
  ,P.InspectionJournalId As JournalId  
   ' + CHAR(10)    
    
SET @PaintPackFromClause = ' FROM PDR_JOURNAL J  
  INNER JOIN V_PaintPack P ON J.JOURNALID=P.InspectionJournalId  
  INNER JOIN V_PaintStatus PS ON P.StatusId=PS.StatusId   
  INNER JOIN PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId  
  INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId    
  INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID     
  INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.CreatedBy   
     ' + CHAR(10)    
    
    
SET @PaintPackWhereClause = 'WHERE PDR_MSAT.PROPERTYID = ''' + @propertyId + ''''  + CHAR(10)    
--==============================================================================================          
    
--Set union Clause            
SET @unionClause = CHAR(10) + CHAR(9) + 'UNION ALL' + CHAR(10) + CHAR(9)    
--========================================================================================              
--Set ORDER Clause      
SET @OrderClause = 'order by ' + @sortColumn + ' ' + @sortOrder + CHAR(10)   
IF @sortColumn = 'CREATIONDATE'  
 Begin  
  SET @OrderClause = @OrderClause + ', JournalHistoryId ' + @sortOrder + CHAR(10)   
 END  
--========================================================================================   
     
/* @actionType    
 -1 => All (Default)    
 1 => Appliance Servicing    
 2 => Reactive    
 3 => Stock    
 4 => Void    
 5 => Planned    
 6 => Condition Works   
 7 => M&E Servicing  
 8 => Cyclic Maintenance  
 9 => PAT Testing   
*/    
    
IF (@actionType = 1)     
  SET @mainSelectQuery = @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause 
						+@unionClause
						+@ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause
						+@OrderClause      
ELSE IF (@actionType = 2)    
 SET @mainSelectQuery = @FaultSelectClause + @FaultFromClause + @FaultWhereClause + @OrderClause    
ELSE IF (@actionType = 3)    
 SET @mainSelectQuery = @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause + @OrderClause    
ELSE IF (@actionType = 5)    
 SET @mainSelectQuery = @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause + @OrderClause    
ELSE IF (@actionType = 6)    
 SET @mainSelectQuery = @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause + @OrderClause    
ELSE IF (@actionType = 7 or @actionType = 8 or @actionType = 9)    
 SET @mainSelectQuery = @PdrSelectClause + @PdrFromClause + @PdrWhereClause + @OrderClause   
ELSE SET @mainSelectQuery = @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause    
 + @unionClause    
 + @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause  
 + @unionClause
 + @ApplianceDefectSelectClause + @ApplianceDefectFromClause + @ApplianceDefectWhereClause   
 + @unionClause    
 + @FaultSelectClause + @FaultFromClause + @FaultWhereClause    
 + @unionClause    
 + @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause    
 + @unionClause    
 + @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause    
 + @unionClause    
 + @PdrSelectClause + @PdrFromClause + @PdrWhereClause   
 + @unionClause    
 + @GasElectricSelectClause + @GasElectricFromClause + @GasElectricWhereClause   
 + @unionClause    
 + @PaintPackSelectClause + @PaintPackFromClause + @PaintPackWhereClause   
     
 + @OrderClause    
   
   PRINT '===================================================================='   
PRINT @mainSelectQuery    
EXEC (@mainSelectQuery)    
    
END




-- =============================================  
-- EXEC AS_getPropertyDefectDetails  @propertyDefectId = 3186  
-- Author:  <Salman Nazir>  
-- Create date: <13/11/2012> 
-- Updated by: <Raja Aneeq>
-- Update date: <14/9/2015> 
-- Description: <This Store Procedure shall get the details of a Defect>  
-- WebPage: PropertyRecord.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_getPropertyDefectDetails](  
@propertyDefectId int  
)  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;

-- Insert statements for procedure here  
SELECT
	PropertyDefectId								AS PropertyDefectId
	,AD.PropertyId									AS PropertyId
	,AD.CategoryId									AS CategoryId
	,DC.Description									AS DefectCategory
	,DefectDate										AS DefectDate
	,JournalId										AS JournalId
	,ISNULL(IsDefectIdentified, 0)					AS IsDefectIdentified
	,ISNULL(DefectNotes, '')						AS DefectNotes
	,ISNULL(IsActionTaken, 0)						AS IsRemedialActionTaken
	,ISNULL(ActionNotes, '')						AS RemedialActionNotes
	,ISNULL(IsWarningIssued, 0)						AS IsWarningIssued
	,ApplianceId									AS ApplianceId
	,ISNULL(AD.SerialNumber, '')					AS SerialNumber
	,ISNULL(AD.GasCouncilNumber, '')					AS GcNumber
	,ISNULL(IsWarningFixed, 0)						AS IsWarningFixed
	,AD.IsDisconnected								AS IsApplianceDisconnected
	,AD.IsPartsrequired								AS IsPartsRequired
	,AD.IsPartsOrdered								AS IsPartsOrdered
	,AD.PartsOrderedBy								AS PartsOrderedBy
	,ISNULL(POB.FIRSTNAME + ' ' + POB.LASTNAME, '')	AS PartsOrderedByName
	,AD.PartsDue									AS PartsDueDate
	,ISNULL(AD.PartsDescription,'')					AS PartsDescription
	,ISNULL(AD.PartsLocation,'')					AS PartsLocation
	,ISNULL(AD.IsTwoPersonsJob, 0)					AS IsTwoPersonsJob
	,ISNULL(AD.ReasonFor2ndPerson,'')				AS ReasonForSecondPerson
	,AD.Duration									AS EstimatedDuration
	,AD.Priority									AS PriorityId
	,ISNULL(DP.PriorityName,'N/A')					AS PriorityName
	,AD.TradeId										AS TradeId
	,ISNULL(T.Description,'N/A')					AS Trade
	,ISNULL(PhotoNotes,'')							AS PhotoNotes
	,ISNULL(AT.APPLIANCETYPE,'')					AS APPLIANCE
FROM
	P_PROPERTY_APPLIANCE_DEFECTS AD
		INNER JOIN GS_PROPERTY_APPLIANCE A ON AD.ApplianceId = A.PROPERTYAPPLIANCEID
		INNER JOIN GS_APPLIANCE_TYPE AT ON A.APPLIANCETYPEID = AT.APPLIANCETYPEID
		LEFT JOIN P_DEFECTS_CATEGORY DC ON AD.CategoryId = DC.CategoryId
		LEFT JOIN E__EMPLOYEE POB ON AD.PartsOrderedBy = POB.EMPLOYEEID
		LEFT JOIN G_TRADE T ON AD.TradeId = T.TradeId
		LEFT JOIN P_DEFECTS_PRIORITY DP ON AD.Priority = DP.PriorityID
WHERE
	PropertyDefectId = @propertyDefectId
END

-- =============================================  
-- EXEC AS_GetPropertyAppliances @propertyId = A010060001  
-- @ApplianceId = 1,  
-- @CategoryId = 1,  
-- @DefectDate = NULL,  
-- @DefectIdentified = 0,  
-- @DefectIdentifiedNotes = NULL,  
-- @PhotoNotes = NULL,  
-- @RemedialAction = 0,  
-- @RemedialActionNotes = NULL  
-- Author:  <Salman Nazir>  
-- Create date: <11/06/2012>  
-- Description: <Get Appliances against Property Id >  
-- Web Page: PropertyRecord.aspx => Add Defect PopUp  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_SaveDefect]  
 -- Add the parameters for the stored procedure here
 @propertyDefectId INT 
 ,@propertyId NVARCHAR(20)
 ,@CategoryId INT 
 ,@DefectDate SMALLDATETIME
 ,@isDefectIdentified BIT
 ,@DefectIdentifiedNotes NVARCHAR(200)
 ,@isRemedialActionTaken BIT
 ,@remedialActionNotes NVARCHAR(200)
 ,@isWarningIssued BIT
 ,@ApplianceId INT
 ,@serialNumber NVARCHAR(20)
 ,@GcNumber NVARCHAR(9)
 ,@isWarningFixed BIT
 ,@isApplianceDisconnected BIT
 ,@isPartsRequired BIT
 ,@isPartsOrdered BIT
 ,@partsOrderedBy INT
 ,@partsDueDate DATE
 ,@partsDescription NVARCHAR(1000)
 ,@partsLocation NVARCHAR(200)
 ,@isTwoPersonsJob BIT
 ,@reasonForSecondPerson NVARCHAR(1000)
 ,@estimatedDuration DECIMAL(9,2)
 ,@priorityId INT
 ,@tradeId INT
 ,@filePath VARCHAR(500) 
 ,@photoName VARCHAR(100)
 ,@PhotoNotes VARCHAR(1000)
 ,@userId INT
 
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

DECLARE @Now DATETIME2 = GETDATE()

IF NOT EXISTS
(
	SELECT
		PropertyDefectId
	FROM
		P_PROPERTY_APPLIANCE_DEFECTS
	WHERE PropertyDefectId = @propertyDefectId
) 
BEGIN

DECLARE @JournalId INT
-- Insert statements for procedure here  
SELECT
	@JournalId = JournalId
FROM
	AS_JOURNAL
WHERE
	AS_JOURNAL.PROPERTYID = @propertyId
	AND ISCURRENT = 1

INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS (
				[PropertyId]
				,[CategoryId]
				,[JournalId]
				,[IsDefectIdentified]
				,[DefectNotes]
				,[IsActionTaken]
				,[ActionNotes]
				,[IsWarningIssued]
				,[ApplianceId]
				,[SerialNumber]
				,[IsWarningFixed]
				,[DefectDate]
				,[CreatedBy]
				,[PhotoNotes]
				,[GasCouncilNumber]
				,[IsDisconnected]
				,[IsPartsrequired]
				,[IsPartsOrdered]
				,[PartsOrderedBy]
				,[PartsDue]
				,[PartsDescription]
				,[PartsLocation]
				,[IsTwoPersonsJob]
				,[ReasonFor2ndPerson]
				,[Duration]
				,[Priority]
				,[TradeId]
				,CreatedDate
				)           
	VALUES (
			@propertyId, @categoryId, @JournalId
			,@isDefectIdentified, @DefectIdentifiedNotes
			,@isRemedialActionTaken, @remedialActionNotes
			,@isWarningIssued
			,@ApplianceId
			,@serialNumber
			,@isWarningFixed
			,@DefectDate
			,@userId
			,@PhotoNotes
			,@GcNumber
			,@isApplianceDisconnected
			,@isPartsRequired , @isPartsOrdered
			,@partsOrderedBy ,@partsDueDate
			,@partsDescription ,@partsLocation
			,@isTwoPersonsJob, @reasonForSecondPerson
			,@estimatedDuration ,@priorityId
			,@tradeId
			,@Now
		)
		IF  @photoName <> ''
		BEGIN
			INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES (
							ImageTitle
							,ImagePath
							,PropertyDefectId
						)
				VALUES (
						@photoName, @filePath, SCOPE_IDENTITY()
					)
		END			
END
ELSE
	BEGIN

		UPDATE P_PROPERTY_APPLIANCE_DEFECTS
			SET IsPartsrequired = @isPartsRequired
				,IsPartsOrdered = @isPartsOrdered
				,PartsDue = @partsDueDate
				,PartsDescription = @partsDescription
				,PartsLocation = @partsLocation
				,IsTwoPersonsJob = @isTwoPersonsJob
				,ReasonFor2ndPerson = @reasonForSecondPerson
				,Duration = @estimatedDuration
				,[Priority] = @priorityId
				,TradeId = @tradeId
				,ModifiedBy = @userId
				,ModifiedDate = @Now
				
		WHERE PropertyDefectId = @propertyDefectId
		
	END

END

