/*
	Author: Aamir Waheed
    Creation Date:  12/09/2015
	Description: Inser appliance defect management pages for navigation and access rights.
*/

/*
SELECT
	ROW_NUMBER() OVER(ORDER BY P.DESCRIPTION) As No
	,MD.DESCRIPTION	AS Module
	,MN.DESCRIPTION	AS Menu
	,PP.DESCRIPTION	AS ParentPage
	,P.DESCRIPTION	AS Page
	,P.PAGE
	,P.*
FROM
	AC_PAGES P
		LEFT JOIN AC_PAGES PP ON P.ParentPage = PP.PAGEID
		INNER JOIN AC_MENUS MN ON P.MENUID = MN.MENUID
		INNER JOIN AC_MODULES MD ON MN.MODULEID = MD.MODULEID
WHERE 1 = 1
	AND PP.DESCRIPTION LIKE '%Scheduling%'
	AND MN.DESCRIPTION LIKE '%Servicing%'
*/

BEGIN TRANSACTION
BEGIN TRY

-- Get servicing Menu Id
DECLARE @servicingMenuId INT
SELECT  @servicingMenuId = MN.MENUID
FROM AC_MENUS MN
WHERE MN.DESCRIPTION = 'Servicing'

PRINT '@servicingMenuId: ' + CONVERT(NVARCHAR,@servicingMenuId)

-- Get Scheduling Parent Page Id
DECLARE @schedulingPageId INT
SELECT  @schedulingPageId = P.PAGEID
FROM AC_PAGES  P
WHERE P.DESCRIPTION = 'Scheduling'
AND P.MENUID = @servicingMenuId

PRINT '@schedulingPageId: ' + CONVERT(NVARCHAR,@schedulingPageId)

-- Get Reports Parent Page Id
DECLARE @reportsPageId INT
SELECT  @reportsPageId = P.PAGEID
FROM AC_PAGES P
WHERE P.DESCRIPTION = 'Reports'
AND P.MENUID = @servicingMenuId

PRINT '@reportsPageId: ' + CONVERT(NVARCHAR,@reportsPageId)

-- Insert DefetScheduling.aspx (Level-2)
DECLARE @DefectPageId INT
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES('Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Bridge.aspx?pg=defectScheduling', 4, 2, @schedulingPageId , '~/../RSLApplianceServicing/Views/Scheduling/DefectScheduling.aspx', 1)
SELECT @DefectPageId = SCOPE_IDENTITY()

-- Insert Defect Scheduling Supporting Page (Level-3)
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES('Defect Basket', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Scheduling/DefectBasket.aspx', 1, 3, @DefectPageId , '', 0)
,('Defect Appointments', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Scheduling/DefectAvailableAppointments.aspx', 2, 3, @DefectPageId , '', 0)
,('Defect Job Sheet', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Scheduling/DefectJobSheetSummary.aspx', 3, 3, @DefectPageId , '', 0)

-- Insert Defect Report Pages (Level-2)
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES('Appliance Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/ApplianceDefectReport.aspx', 4, 2, @reportsPageId , '', 1)
,('Disconnected Appliance', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/CancelledDefectReport.aspx', 5, 2, @reportsPageId , '', 1)
,('Cancelled Defects', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/CancelledDefectReport.aspx', 6, 2, @reportsPageId , '', 1)
,('Defects History', @servicingMenuId, 1, 1, '~/../RSLApplianceServicing/Views/Reports/Defects/DefectHistoryReport.aspx', 7, 2, @reportsPageId , '', 1)



/****** Object:  Table [dbo].[P_PowerSourceType]    Script Date: 07/07/2015 07:10:33 ******/
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (1, N'Mains', 1, 1)
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (2, N'Battery', 1, 2)
INSERT [dbo].[P_PowerSourceType] ([PowerTypeId], [PowerType], [IsActive], [sortOrder]) VALUES (3, N'Both', 1, 3)
/****** Object:  Table [dbo].[P_MeterType]    Script Date: 07/07/2015 07:10:33 ******/
INSERT [dbo].[P_MeterType] ([MeterTypeId], [MeterType], [IsActive], [sortOrder]) VALUES (1, N'Electric Meter', 1, 1)
INSERT [dbo].[P_MeterType] ([MeterTypeId], [MeterType], [IsActive], [sortOrder]) VALUES (2, N'Gas Meter', 1, 2)
/****** Object:  Table [dbo].[P_DEFECTS_PRIORITY]    Script Date: 07/07/2015 07:10:33 ******/
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='High') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (1, N'High', 7, 1, 1, 1)
END
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='Medium') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (2, N'Medium', 24, 1, 1, 2)
END
IF NOT EXISTS(Select * from P_DEFECTS_PRIORITY where PriorityName='Low') 
BEGIN
	INSERT [dbo].[P_DEFECTS_PRIORITY] ([PriorityID], [PriorityName], [ResponseDuration], [IsDay], [IsActive], [sortOrder]) VALUES (3, N'Low', 60, 1, 1, 3)
END


-- Insert 'Appliance Defect'

INSERT INTO [PDR_MSATType]
           ([MSATTypeId]
           ,[MSATTypeName]
           ,[IsActive])
     VALUES
           (11
           ,'Appliance Defect'
           ,1)
GO


DECLARE @newStatus NCHAR(100) = 'Paused'
IF NOT EXISTS (SELECT STATUSID FROM PDR_STATUS WHERE TITLE = @newStatus)
BEGIN
	INSERT INTO PDR_STATUS(TITLE,CREATIONDATE)
	VALUES(@newStatus,GETDATE())
END


IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);

END CATCH