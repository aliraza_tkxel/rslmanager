Brief explanation for "Appliance Defects Management Functional Design Specification FD1.3" Deployment.

Please get the database backup befor deployment

All scripts related to deployment exist in following svn Path
 "\BHA.rslManager\rsl_addons\RSLApplianceServicing\Database Scripts\Gas Defect Deployment\"

Please execute the script in following order

Step 1 Execute DDL object scripts: 
1). Create Gas Defect Tables.sql
2). Alter Tables for Gas Defect.sql
3). DataType_DF_AssingToContractorWorksRequired.sql
4). Triggers for Gas Defect.sql
5). Create Stored Procedures for Gas Defect.sql
6). Alter SP for Gas Defect.sql


Step 2 Execute DML object scripts:
1). Defects_Data_Scripts.sql


Step 3 Build the following Projects:

1). RSLApplianceServicing
2). StockConditionOfflineAPI