

BEGIN TRANSACTION
 
ALTER TABLE dbo.PDR_MSAT ADD isPending bit NULL

ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES ADD isBefore bit NULL


ALTER TABLE dbo.GS_PROPERTY_APPLIANCE ADD IsDisconnected bit NULL
COMMIT

/* =================================================================================    
    Table Description: This table contains defects logged for appliances during gas
				survey and/or after defect appointment/job sheet update.
    
    Update: Added new attribues and related constrants to implement update defects
    management process.
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_STATUS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_JOURNAL SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.G_TRADE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.G_TRADE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.G_TRADE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.G_TRADE', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_DEFECTS_PRIORITY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_DEFECTS_PRIORITY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_DEFECTS_PRIORITY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_DEFECTS_PRIORITY', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS ADD
	GasCouncilNumber nvarchar(20) NULL,
	IsDisconnected bit NULL,
	IsPartsrequired bit NULL,
	IsPartsOrdered bit NULL,
	PartsOrderedBy int NULL,
	PartsDue date NULL,
	PartsDescription nvarchar(1000) NULL,
	PartsLocation nvarchar(200) NULL,
	IsTwoPersonsJob bit NULL,
	ReasonFor2ndPerson nvarchar(1000) NULL,
	Duration decimal(9, 2) NULL,
	Priority int NULL,
	TradeId int NULL,
	IsCustomerHaveHeating bit NULL,
	IsHeatersLeft bit NULL,
	NumberOfHeatersLeft tinyint NULL,
	IsCustomerHaveHotWater bit NULL,
	DefectJobSheetStatus int NULL,
	ApplianceDefectAppointmentJournalId int NULL,
	NoEntryNotes nvarchar(1000) NULL,
	CancelNotes nvarchar(1000) NULL,
	CreatedDate datetime2(3) NULL,
	ModifiedBy int NULL,
	ModifiedDate datetime2(3) NULL
GO
ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS ADD CONSTRAINT
	FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_STATUS FOREIGN KEY
	(
	DefectJobSheetStatus
	) REFERENCES dbo.PDR_STATUS
	(
	STATUSID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS ADD CONSTRAINT
	FK_P_PROPERTY_APPLIANCE_DEFECTS_P_DEFECTS_PRIORITY FOREIGN KEY
	(
	Priority
	) REFERENCES dbo.P_DEFECTS_PRIORITY
	(
	PriorityID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS ADD CONSTRAINT
	FK_P_PROPERTY_APPLIANCE_DEFECTS_G_TRADE FOREIGN KEY
	(
	TradeId
	) REFERENCES dbo.G_TRADE
	(
	TradeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS ADD CONSTRAINT
	FK_P_PROPERTY_APPLIANCE_DEFECTS_PDR_JOURNAL FOREIGN KEY
	(
	ApplianceDefectAppointmentJournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS', 'Object', 'CONTROL') as Contr_Per 