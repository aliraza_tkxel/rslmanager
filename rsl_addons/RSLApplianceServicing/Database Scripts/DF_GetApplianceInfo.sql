USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetApplainceInfo]    Script Date: 09/28/2015 12:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- EXEC DF_GetApplianceInfo  
-- @defectId =1212  
-- Author:  <Noor Muhammad>  
-- Create date: <28/09/2015>  
-- Description: <Get Appliance information>  
-- History:          28/09/2015 Noor : Created the stored procedure
--                   28/09/2015 Name : Description of Work
-- =============================================  
  
CREATE PROCEDURE [dbo].[DF_GetApplianceInfo]  
  
 @defectId INT   
  
AS  
BEGIN   

SELECT 
	GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID AS ApplianceId  
	,GS_APPLIANCE_TYPE.APPLIANCETYPE COLLATE Latin1_General_CI_AI + ' : ' + P_DEFECTS_CATEGORY.Description COLLATE Latin1_General_CI_AI AS ApplianceAndCategory  	
	,P_PROPERTY_APPLIANCE_DEFECTS.DefectNotes AS WorksRequired  
	,ISNULL(GS_PROPERTY_APPLIANCE.NetCost,0) AS NetCost  
	,ISNULL(GS_PROPERTY_APPLIANCE.Vat,0) AS Vat  
	,ISNULL(GS_PROPERTY_APPLIANCE.Gross,0) AS Gross  
	,ISNULL(GS_PROPERTY_APPLIANCE.VatRateId ,-1 ) AS VatRateId  
	,P_PROPERTY_APPLIANCE_DEFECTS.PropertyId  AS PropertyId
FROM
	P_PROPERTY_APPLIANCE_DEFECTS
	INNER JOIN GS_PROPERTY_APPLIANCE ON P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID
	INNER JOIN GS_APPLIANCE_TYPE ON GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	INNER JOIN P_DEFECTS_CATEGORY  ON P_PROPERTY_APPLIANCE_DEFECTS.CategoryId = P_DEFECTS_CATEGORY.CategoryId
WHERE 
	P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId = @defectId	   
  
END  