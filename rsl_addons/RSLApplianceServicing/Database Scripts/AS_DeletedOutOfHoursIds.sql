USE [RSLBHALive]
GO

CREATE TYPE [dbo].[AS_DeletedOutOfHoursIds] AS TABLE(

	[OutOfHoursId] [int] NOT NULL
)
GO