/*
	Use this script to check and add a page in AC_PAGES
	by setting the variable values in below section
*/

DECLARE	@Module NVARCHAR(100) = 'Property'
		,@Menu NVARCHAR(200) = 'Search'
		,@PageDescription NVARCHAR(200) = 'Accommodation'
		,@Active INT = 1
		,@Link INT = 1
		,@Page NVARCHAR(600) = '~/Controls/Property/Accommodation.ascx'
		,@OrderText NVARCHAR(100) = 4
		,@AccessLevel INT = 2
		,@ParentPageDescription NVARCHAR(200) = 'Search'
		,@ParentPage NVARCHAR(200) = '~/../PropertyModule/Views/Search/SearchProperty.aspx'
		,@BridgeActualPage NVARCHAR(300) = ''
		,@DisplayInTree BIT = 1


IF	NOT EXISTS
	(
		SELECT
			P.PAGEID
		FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.DESCRIPTION = @Menu
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
	)
BEGIN

	BEGIN TRANSACTION
	BEGIN TRY

		DECLARE	@MenuId INT = NULL
				,@ParentPageId INT = NULL

		SELECT
			@ParentPageId = PP.PAGEID
			,@MenuId = MN.MENUID
		FROM
			AC_MENUS MN
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
				LEFT JOIN AC_PAGES PP ON PP.MENUID = MN.MENUID
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
		WHERE
			MN.DESCRIPTION = @Menu

		UPDATE
			AC_PAGES
		SET
			ORDERTEXT = ORDERTEXT + 1
		WHERE
			MENUID = @MENUID
			AND ParentPage = @ParentPageId
			AND AccessLevel = @AccessLevel
			AND ISNUMERIC(@OrderText) = 1
			AND ORDERTEXT IS NOT NULL
			AND ISNUMERIC(ORDERTEXT) = 1
			AND ORDERTEXT >= @OrderText

		INSERT INTO AC_PAGES (
						DESCRIPTION
						,MENUID
						,ACTIVE
						,LINK
						,PAGE
						,ORDERTEXT
						,AccessLevel
						,ParentPage
						,BridgeActualPage
						,DisplayInTree
					)
			VALUES (
					@PageDescription
					,@MenuId
					,@Active
					,@Link
					,@Page
					,@OrderText
					,@AccessLevel
					,@ParentPageId
					,NULL
					,@DisplayInTree
				)
		-- if successful - COMMIT the work
		PRINT 'Page ' + @PageDescription + ' added successfully'

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	-- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage

	-- in case of an error, ROLLBACK the transaction    
	ROLLBACK TRANSACTION
END CATCH
END