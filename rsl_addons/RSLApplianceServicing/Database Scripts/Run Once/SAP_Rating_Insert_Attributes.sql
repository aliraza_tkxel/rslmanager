BEGIN TRANSACTION T1;
	BEGIN TRY
		DECLARE @ParameterName VARCHAR(50) = 'SAP Rating'
				,@DataType VARCHAR(50) = 'Integer'
				,@ControlType VARCHAR(50) = 'TextBox'
				,@IsDate BIT = 0
				,@Active BIT = 1
				,@ShowInApp BIT = 1
				
		IF NOT EXISTS (SELECT ParameterID FROM PA_PARAMETER WHERE ParameterName = @ParameterName AND DataType = @DataType AND ControlType = @ControlType )
		BEGIN		
		
			DECLARE @SOrder AS INTEGER
			DECLARE @ParameterId AS INTEGER
		
			SET @SOrder = ( SELECT DISTINCT PARAMETERSORDER FROM PA_PARAMETER WHERE lower( ParameterName ) LIKE '%build date%' ) + 1
		
			INSERT INTO PA_PARAMETER VALUES ( @ParameterName, @DataType, @ControlType, @IsDate, @SOrder, @Active, @ShowInApp )
		
		SET @ParameterId  = SCOPE_IDENTITY()
				
		DECLARE @ItemId INT
		
		SELECT @ItemId = I.ItemID FROM PA_ITEM I
		INNER JOIN PA_AREA A ON A.AreaID = I.AreaID
		WHERE ItemName = 'Structure'
			AND A.AreaName = 'Dwelling'		
		
		INSERT INTO PA_ITEM_PARAMETER VALUES ( @ItemId, @ParameterId, @Active)
		
		END
		 COMMIT TRANSACTION T1
		
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION T1;
	END CATCH;