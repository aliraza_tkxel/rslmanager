/*
	Use this script to check and add/Update a page in AC_PAGES
	by setting the variable values in below section
*/

/*
	SCRIPT and Logic to remove weekly calendar from navigation and access control tree.
*/

DECLARE	@Module NVARCHAR(100) = 'Property'
		,@Menu NVARCHAR(200) = 'Servicing'
		,@PageDescription NVARCHAR(200) = 'Calendar'
		,@Active INT = 0
		,@Link INT = 1
		,@Page NVARCHAR(600) = '~/../RSLApplianceServicing/Bridge.aspx?pg=weeklycalendar'
		,@OrderText NVARCHAR(100) = 1
		,@AccessLevel INT = 1
		,@DisplayInTree BIT = 0
		,@ParentPageDescription NVARCHAR(200) = NULL
		,@ParentPage NVARCHAR(200) = NULL
		,@BridgeActualPage NVARCHAR(300) = '~/../RSLApplianceServicing/Views/Calendar/WeeklyCalendar.aspx'

BEGIN TRANSACTION
BEGIN TRY

DECLARE	@MenuId INT = NULL
				,@ParentPageId INT = NULL

		SELECT
			@ParentPageId = PP.PAGEID
			,@MenuId = MN.MENUID
		FROM
			AC_MENUS MN
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
				LEFT JOIN AC_PAGES PP ON PP.MENUID = MN.MENUID
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
		WHERE
			MN.DESCRIPTION = @Menu
			
PRINT 'MENU ID: ' + ISNULL(CONVERT(NVARCHAR,@MenuId),'NULL')
PRINT 'Parent Page ID: ' + ISNULL(CONVERT(NVARCHAR,@ParentPageId),'NULL')

IF @MenuId IS NOT NULL
BEGIN
IF	NOT EXISTS
	(
		SELECT
			P.PAGEID
		FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
	)
BEGIN
		PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" do not exists, Inserting '
		
		INSERT INTO AC_PAGES (
						DESCRIPTION
						,MENUID
						,ACTIVE
						,LINK
						,PAGE
						,ORDERTEXT
						,AccessLevel
						,ParentPage
						,BridgeActualPage
						,DisplayInTree
					)
			VALUES (
					@PageDescription
					,@MenuId
					,@Active
					,@Link
					,@Page
					,@OrderText
					,@AccessLevel
					,@ParentPageId
					,NULL
					,@DisplayInTree
				)		
		PRINT 'Page ' + @PageDescription + ' added successfully'
END
ELSE
BEGIN
	PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" already exists, Updating '
	
	UPDATE P
		SET P.ACTIVE = @Active
			,P.LINK = @Link
			,P.PAGE = @Page
			,P.ORDERTEXT = @OrderText
			,P.DisplayInTree = @DisplayInTree
	FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel

END

END
ELSE
	PRINT 'Menu "' + @Menu + '" does not exists in module "' + @Module + '", please verify this menu is correct.' 
	-- if successful - COMMIT the work
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	-- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage

	-- in case of an error, ROLLBACK the transaction    
	ROLLBACK TRANSACTION
END CATCH

--====================================================================================================================

/*
	SCRIPT and Logic to remove monthly calendar from navigation and access control tree.
*/

SELECT	@Module = 'Property'
		,@Menu = 'Servicing'
		,@PageDescription  = 'Monthly Calendar'
		,@Active = 0
		,@Link = 1
		,@Page  = '~/../RSLApplianceServicing/Bridge.aspx?pg=weeklycalendar'
		,@OrderText  = 1
		,@AccessLevel = 2
		,@DisplayInTree = 0
		,@ParentPageDescription  = 'Calendar'
		,@ParentPage  = '~/../RSLApplianceServicing/Bridge.aspx?pg=weeklycalendar'
		,@BridgeActualPage  = NULL

BEGIN TRANSACTION
BEGIN TRY

SELECT	@MenuId  = NULL
				,@ParentPageId  = NULL

		SELECT
			@ParentPageId = PP.PAGEID
			,@MenuId = MN.MENUID
		FROM
			AC_MENUS MN
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
				LEFT JOIN AC_PAGES PP ON PP.MENUID = MN.MENUID
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
		WHERE
			MN.DESCRIPTION = @Menu
			
PRINT 'MENU ID: ' + ISNULL(CONVERT(NVARCHAR,@MenuId),'NULL')
PRINT 'Parent Page ID: ' + ISNULL(CONVERT(NVARCHAR,@ParentPageId),'NULL')

IF @MenuId IS NOT NULL
BEGIN
IF	NOT EXISTS
	(
		SELECT
			P.PAGEID
		FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
	)
BEGIN
		PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" do not exists, Inserting '
		
		INSERT INTO AC_PAGES (
						DESCRIPTION
						,MENUID
						,ACTIVE
						,LINK
						,PAGE
						,ORDERTEXT
						,AccessLevel
						,ParentPage
						,BridgeActualPage
						,DisplayInTree
					)
			VALUES (
					@PageDescription
					,@MenuId
					,@Active
					,@Link
					,@Page
					,@OrderText
					,@AccessLevel
					,@ParentPageId
					,@BridgeActualPage
					,@DisplayInTree
				)		
		PRINT 'Page ' + @PageDescription + ' added successfully'
END
ELSE
BEGIN
	PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" already exists, Updating '
	
	UPDATE P
		SET P.ACTIVE = @Active
			,P.LINK = @Link
			,P.PAGE = @Page
			,P.ORDERTEXT = @OrderText
			,P.DisplayInTree = @DisplayInTree
			,P.BridgeActualPage = @BridgeActualPage
	FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
END

END
ELSE
	PRINT 'Menu "' + @Menu + '" does not exists in module "' + @Module + '", please verify this menu is correct.' 
	-- if successful - COMMIT the work
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	-- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage

	-- in case of an error, ROLLBACK the transaction    
	ROLLBACK TRANSACTION
END CATCH

--====================================================================================================================

/*
	SCRIPT and Logic to remove calendar search from navigation and access control tree.
*/

SELECT	@Module = 'Property'
		,@Menu = 'Servicing'
		,@PageDescription  = 'Calendar Search'
		,@Active = 0
		,@Link = 0
		,@Page  = '~/../RSLApplianceServicing/Views/Calendar/CalendarSearch.aspx'
		,@OrderText  = 2
		,@AccessLevel = 2
		,@DisplayInTree = 0
		,@ParentPageDescription  = 'Calendar'
		,@ParentPage  = '~/../RSLApplianceServicing/Bridge.aspx?pg=weeklycalendar'
		,@BridgeActualPage  = NULL

BEGIN TRANSACTION
BEGIN TRY

SELECT	@MenuId  = NULL
				,@ParentPageId  = NULL

		SELECT
			@ParentPageId = PP.PAGEID
			,@MenuId = MN.MENUID
		FROM
			AC_MENUS MN
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
				LEFT JOIN AC_PAGES PP ON PP.MENUID = MN.MENUID
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
		WHERE
			MN.DESCRIPTION = @Menu
			
PRINT 'MENU ID: ' + ISNULL(CONVERT(NVARCHAR,@MenuId),'NULL')
PRINT 'Parent Page ID: ' + ISNULL(CONVERT(NVARCHAR,@ParentPageId),'NULL')

IF @MenuId IS NOT NULL
BEGIN
IF	NOT EXISTS
	(
		SELECT
			P.PAGEID
		FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
	)
BEGIN
		PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" do not exists, Inserting '
		
		INSERT INTO AC_PAGES (
						DESCRIPTION
						,MENUID
						,ACTIVE
						,LINK
						,PAGE
						,ORDERTEXT
						,AccessLevel
						,ParentPage
						,BridgeActualPage
						,DisplayInTree
					)
			VALUES (
					@PageDescription
					,@MenuId
					,@Active
					,@Link
					,@Page
					,@OrderText
					,@AccessLevel
					,@ParentPageId
					,@BridgeActualPage
					,@DisplayInTree
				)		
		PRINT 'Page ' + @PageDescription + ' added successfully'
END
ELSE
BEGIN
	PRINT 'Page: ' + @Module + ' > ' + @Menu + ' > ' + ISNULL(@ParentPageDescription + ' > ','') + '"' + @PageDescription + '" already exists, Updating '
	
	UPDATE P
		SET P.ACTIVE = @Active
			,P.LINK = @Link
			,P.PAGE = @Page
			,P.ORDERTEXT = @OrderText
			,P.DisplayInTree = @DisplayInTree
			,P.BridgeActualPage = @BridgeActualPage
	FROM
			AC_PAGES P
				LEFT JOIN AC_PAGES PP ON PP.PAGEID = P.ParentPage
					AND
					PP.DESCRIPTION = @ParentPageDescription
					AND
					PP.PAGE = @ParentPage
				INNER JOIN AC_MENUS MN ON MN.MENUID = P.MENUID
					AND
					MN.MENUID = @MenuId
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			P.DESCRIPTION = @PageDescription
			AND P.AccessLevel = @AccessLevel
END

END
ELSE
	PRINT 'Menu "' + @Menu + '" does not exists in module "' + @Module + '", please verify this menu is correct.' 
	-- if successful - COMMIT the work
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	-- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage

	-- in case of an error, ROLLBACK the transaction    
	ROLLBACK TRANSACTION
END CATCH