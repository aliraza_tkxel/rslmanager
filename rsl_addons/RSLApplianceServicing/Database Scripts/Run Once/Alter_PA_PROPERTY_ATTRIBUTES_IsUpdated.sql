/*
Author: Aamir Waheed
Date: 30 Jan 2014
*/

--=========================================================================
--====== Add IsUpdated Column in PA_PROPERTY_ATTRIBUTES
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'PA_PROPERTY_ATTRIBUTES'
		AND COLUMN_NAME = 'IsUpdated'
)
BEGIN
	PRINT 'Not Exists'
	
--==============================================================

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE dbo.PA_PROPERTY_ATTRIBUTES ADD
	IsUpdated bit NOT NULL CONSTRAINT DF_PA_PROPERTY_ATTRIBUTES_IsUpdated DEFAULT 0

ALTER TABLE dbo.PA_PROPERTY_ATTRIBUTES SET (LOCK_ESCALATION = TABLE)

COMMIT
select Has_Perms_By_Name(N'dbo.PA_PROPERTY_ATTRIBUTES', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PA_PROPERTY_ATTRIBUTES', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PA_PROPERTY_ATTRIBUTES', 'Object', 'CONTROL') as Contr_Per 

--==============================================================
 PRINT 'Column inserted successfully.'
END
ELSE
	PRINT 'Column already exists.'