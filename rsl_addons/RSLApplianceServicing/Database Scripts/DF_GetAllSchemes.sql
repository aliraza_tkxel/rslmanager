USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_GetAllApplainceTypes]    Script Date: 09/09/2015 15:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      09/09/2015
-- Description:      Get All Schemes
-- History:          08/09/2015 Noor : Created the stored procedure
--                   08/09/2015 Name : Description of Work
-- =============================================
CREATE PROCEDURE [dbo].[DF_GetAllSchemes]


AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SCHEMEID as Id, SCHEMENAME as Title From P_SCHEME 
	WHERE SCHEMENAME !=''
	ORDER BY SCHEMENAME ASC
END
