USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GET_DOCUMENT_SUBTYPES]    Script Date: 08/11/2014 17:14:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Simon Rogers
-- Create date: 1st August 2014
-- Description:	Retrieve Document Types
-- =============================================
CREATE PROCEDURE [dbo].[P_GET_DOCUMENT_SUBTYPES]
	-- Add the parameters for the stored procedure here
	@DocumentTypeId INT ,
	@ShowActiveOnly BIT = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @ShowActiveOnly = 1
		SELECT -1 as DocumentSubtypeId, 0 AS DocumentTypeId, 'Please select' AS Title, 1 AS [Active], 0 AS [Order]
		UNION
		SELECT DocumentSubtypeId, DocumentTypeId, Title, [Active], [Order]
		FROM dbo.P_Documents_Subtype dt
		WHERE [Active] = 1 AND DocumentTypeId = @DocumentTypeId
		ORDER BY [Order], [Title]
	
	ELSE
		SELECT -1 as DocumentSubtypeId, 0 AS Parent, 'Please select' AS Title, 1 AS [Active], 0 AS [Order]
		UNION
		SELECT DocumentSubtypeId, DocumentTypeId, Title, [Active], [Order]
		FROM dbo.P_Documents_Subtype dt
		WHERE DocumentTypeId = @DocumentTypeId
		ORDER BY [Order], [Title]
	
	
END


