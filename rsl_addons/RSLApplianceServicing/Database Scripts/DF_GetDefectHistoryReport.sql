/* =================================================================================      
-- Author:           Noor Muhammad  
-- Create date:      11/09/2015  
-- Description:      Get Defect History Report  
-- History:          11/09/2015 Noor : Created the stored procedure  
--                   11/09/2015 Name : Description of Work  
					 14/12/2015 Ali: Update the Join for P_Scheme.
      
Execution Command:  
----------------------------------------------------  
  
DECLARE @return_value int,  
  @totalCount int  
  
EXEC @return_value = [dbo].[DF_GetDefectHistoryReport]  
  @searchText = N'a',  
  @schemeId = NULL,  
  @applianceType = 'All',   
  @year = 2015,  
  @pageSize = 30,  
  @pageNumber = 1,  
  @sortColumn = N'SchemeName',  
  @sortOrder = N'ASC',  
  @getOnlyCount = 0,  
  @totalCount = @totalCount OUTPUT  
  
SELECT @totalCount as N'@totalCount'  
  
SELECT 'Return Value' = @return_value  
  
  
----------------------------------------------------  
*/  
  
ALTER PROCEDURE [dbo].[DF_GetDefectHistoryReport](  
    
  @searchText varchar(5000)='',  
  @schemeId INT = -1,  
  @applianceType varchar(500) = 'All',  
  @year INT = -1,  
    
  --Parameters which would help in sorting and paging  
  @pageSize int = 30,  
  @pageNumber int = 1,  
  @sortColumn varchar(50) = 'SchemeName',  
  @sortOrder varchar (5) = 'ASC',  
  @getOnlyCount bit=0,  
  @totalCount int=0 output  
)  
AS  
BEGIN  
DECLARE   
   
  @SelectClause varchar(3000),  
        @fromClause   varchar(3000),  
        @GroupClause  varchar(1000),  
        @whereClause  varchar(2000),           
        @orderClause  varchar(2000),   
        @mainSelectQuery varchar(7000),          
        @rowNumberQuery varchar(7000),  
        @finalQuery varchar(7000),  
        -- used to add in conditions in WhereClause based on search criteria provided  
        @searchCriteria varchar(5000),  
          
        --variables for paging  
        @offset int,  
  @limit int    
  
  --Paging Formula  
  SET @offset = 1+(@pageNumber-1) * @pageSize  
  SET @limit = (@offset + @pageSize)-1  
    
  
  --=====================Search Criteria===============================  
  SET @searchCriteria = ' 1 = 1 '  
    
  IF(@searchText != '' OR @searchText != NULL)  
  BEGIN        
   SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND (ISNULL(P__PROPERTY.HouseNumber, '''') + ISNULL('' ''+P__PROPERTY.ADDRESS1, '''')  + ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''  
   SET @searchCriteria = @searchCriteria + CHAR(10) + 'OR P__PROPERTY.POSTCODE LIKE ''%' + @searchText + '%'' )'        
  END  
    
  IF @schemeId != -1  
  BEGIN  
   SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND P_SCHEME.SchemeId = ' + CONVERT(VARCHAR, @schemeId)  
  END  
    
  IF @applianceType != 'All' AND @applianceType != '-2'  
  BEGIN  
   SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND LOWER(GS_APPLIANCE_TYPE.APPLIANCETYPE) LIKE LOWER(''%'+@applianceType+'%'')'  
  END  
    
  IF @year = -1  
  BEGIN  
   SET @year = YEAR(getdate())  
  END         
    
  --============================From Clause============================================  
  SET @fromClause = CHAR(10) +'FROM  
          P__PROPERTY  
          INNER JOIN P_STATUS on P__PROPERTY.STATUS = P_STATUS.STATUSID  
          INNER JOIN (  
              SELECT PropertyId,DefectDate,ApplianceId,PropertyDefectId                FROM               (                Select PropertyId,DefectDate,ApplianceId,PropertyDefectId,ROW_NUMBER() OVER(PARTITION BY ApplianceId ORDER BY 
              DefectDate DESC)                As Record From P_PROPERTY_APPLIANCE_DEFECTS               ) AS t               Where t.Record = 1  
             )   
          AS P_PROPERTY_APPLIANCE_DEFECTS   
          ON   
          P__PROPERTY.PROPERTYID = P_PROPERTY_APPLIANCE_DEFECTS.PropertyId  
            
          INNER JOIN (  
              SELECT PROPERTYAPPLIANCEID,SchemeId,APPLIANCETYPEID   
              FROM GS_PROPERTY_APPLIANCE  
             )   
          AS GS_PROPERTY_APPLIANCE   
          ON   
          P_PROPERTY_APPLIANCE_DEFECTS.ApplianceId = GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID  
            
          INNER JOIN (  
              SELECT P_LGSR.PROPERTYID,P_LGSR.ISSUEDATE   
              FROM P_LGSR  
             )   
          AS P_LGSR   
          ON   
          P__PROPERTY.PROPERTYID = P_LGSR.PROPERTYID  
          INNER JOIN GS_APPLIANCE_TYPE ON GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID  
          INNER JOIN P_SCHEME ON GS_PROPERTY_APPLIANCE.SchemeId = P_SCHEME.SCHEMEID OR P__Property.SchemeId =  P_SCHEME.SCHEMEID'  
           
  --================================= Where Clause ================================  
    
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria   
       + CHAR(10) +'AND DATEPART(yyyy, P_PROPERTY_APPLIANCE_DEFECTS.DefectDate)= ' + CONVERT(varchar(4),@year)    
       + CHAR(10) +'AND STATUS IN (  
               SELECT   
                P_STATUS.STATUSID                  
               FROM   
                P_STATUS   
               WHERE   
                P_STATUS.DESCRIPTION = ''Available to rent'' OR P_STATUS.DESCRIPTION = ''Let''  
              )'  
    
  IF(@getOnlyCount=0)  
  BEGIN  
     
   --=======================Select Clause=============================================  
    SET @SelectClause = 'SELECT TOP ('+convert(VARCHAR(10),@limit)+')             
          P__PROPERTY.HOUSENUMBER + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''') AS Address  
         ,P__PROPERTY.POSTCODE AS PostCode  
         ,P__PROPERTY.PropertyId AS PropertyId   
         ,P_SCHEME.SCHEMENAME AS SchemeName                       
         ,( SELECT count(*)   
          FROM   
           P_PROPERTY_APPLIANCE_DEFECTS   
          WHERE   
           P_PROPERTY_APPLIANCE_DEFECTS.PropertyId = P__PROPERTY.PropertyId  
         ) AS NumberOfDefects  
         ,P_PROPERTY_APPLIANCE_DEFECTS.DefectDate AS LastInspectedDate  
         ,DATEADD(YEAR,1,P_LGSR.ISSUEDATE) AS CertificateExpiryDate  
         ,P_PROPERTY_APPLIANCE_DEFECTS.PropertyDefectId'  
         
   --============================Group Clause==========================================  
   --SET @GroupClause = CHAR(10) + ' GROUP BY JournalId, P.HOUSENUMBER, P.ADDRESS1, P.TOWNCITY  
   --          , P.POSTCODE, AD.PropertyId, P.PATCH, P.COUNTY '          
   --============================Order Clause==========================================    
          
   SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  
     
   --================================= Where Clause ================================  
     
   --SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria   
     
   --===============================Main Query ====================================  
   Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause   
     
   --=============================== Row Number Query =============================  
   Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row   
         FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'  
     
   --============================== Final Query ===================================  
   Set @finalQuery  =' SELECT *  
        FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result   
        WHERE  
        Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)      
     
   --============================ Exec Final Query =================================  
   IF(@getOnlyCount=0)  
   BEGIN  
    print(@finalQuery)  
    EXEC (@finalQuery)  
   END  
     
   --========================================================================================  
   -- Begin building Count Query   
     
   Declare @selectCount nvarchar(4000),   
   @parameterDef NVARCHAR(500)  
     
   SET @parameterDef = '@totalCount int OUTPUT';  
   SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause  
     
   --print @selectCount  
   EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  
  END       
   -- End building the Count Query  
   --========================================================================================   
  
END  