-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_AddDefectImage @propertyDefectId = 1,
		--@imagePath = N'C;\Appliances\Documents',
		--@imageName = N'i.jpg'
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/16/2012>
-- Description:	<Description,,get all defect images>
-- WebPage: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE AS_AddDefectImage
@propertyDefectId int,
@imagePath varchar(1000),
@imageName varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
	([PropertyDefectId],[ImageTitle],[ImagePath])
	VALUES
	(@propertyDefectId,@imageName,@imagePath)
END
GO
