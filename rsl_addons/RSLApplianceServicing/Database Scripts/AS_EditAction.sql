USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_EditAction]    Script Date: 12/17/2012 06:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC	[AS_EditAction]
		--@StatusId = 1,
		--@ActionId = 5,
		--@Title = N'New',
		--@Ranking = 2,
		--@ModifiedBy = 1
-- Author:		<Salman Nazir>
-- Create date: <20/11/2012>
-- Description:	<Edit Action's value from table>
-- WebPage: Status.aspx => Edit Action
-- =============================================
ALTER PROCEDURE [dbo].[AS_EditAction]
	(
	@StatusId int,
	@ActionId int,
	@Title varchar(500),
	@Ranking int,
	@ModifiedBy int
	)
AS
BEGIN
Declare @checkOldRanking int
Declare @countRecord int
Declare @actionTableId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT @actionTableId = AS_Action.ActionId 
    FROM AS_Action
    WHERE Ranking = @Ranking AND StatusId = @StatusId
    

    
    SELECT @checkOldRanking = AS_Action.Ranking FROM AS_Action WHERE ActionId = @ActionId   
    SELECT @countRecord = COUNT(*) FROM AS_Action WHERE StatusId = @StatusId
    
    if @actionTableId > 0 AND @actionTableId = @ActionId
		BEGIN
		
   			Update AS_Action SET Title = @Title, Ranking = @Ranking, ModifiedBy = @ModifiedBy
			WHERE ActionId = @ActionId AND StatusId = @StatusId
			
			INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
			Values(@ActionId,@StatusId,@Title,@Ranking,1,@ModifiedBy,GETDATE(),GETDATE())
		END
	ELSE
		BEGIN
			UPDATE AS_Action SET Ranking = @checkOldRanking WHERE ActionId = @actionTableId
			Declare @ActionTitle varchar(500)
			Declare @ActionStatusId int			
			Declare @isEditable smallint

			SELECT @ActionTitle=Title,@ActionStatusId=StatusId,@isEditable=IsEditable FROM AS_Action WHERE ActionId = @actionTableId
			INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
			Values(@actionTableId,@ActionStatusId,@ActionTitle,@countRecord+1,@isEditable,@ModifiedBy,GETDATE(),GETDATE())
			
			
			
			
			Update AS_Action SET Title = @Title, Ranking = @Ranking, ModifiedBy = @ModifiedBy
			WHERE ActionId = @ActionId AND StatusId = @StatusId
			INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
			Values(@ActionId,@StatusId,@Title,@Ranking,@isEditable,@ModifiedBy,GETDATE(),GETDATE())
			
		END
END
