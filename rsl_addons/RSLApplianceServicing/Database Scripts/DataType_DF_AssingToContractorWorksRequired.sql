USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[DF_AssingToContractorWorksRequired]    Script Date: 10/01/2015 15:48:34 ******/
IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'DF_AssingToContractorWorksRequired' AND ss.name = N'dbo')
DROP TYPE [dbo].[DF_AssingToContractorWorksRequired]
GO

USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[DF_AssingToContractorWorksRequired]    Script Date: 10/01/2015 15:48:34 ******/
CREATE TYPE [dbo].[DF_AssingToContractorWorksRequired] AS TABLE(
	[WorksRequired] [nvarchar](4000) NOT NULL,
	[NetCost] [smallmoney] NOT NULL,
	[VatType] [int] NOT NULL,
	[VAT] [smallmoney] NOT NULL,
	[GROSS] [smallmoney] NOT NULL,
	[PIStatus] [int] NOT NULL,
	[ExpenditureId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[BudgetHeadId] [int] NOT NULL
)
GO


