alter table GS_PROPERTY_APPLIANCE drop constraint DF_GS_PROPERTY_APPLIANCE_APPLIANCETYPEID
alter table GS_PROPERTY_APPLIANCE drop constraint DF_GS_PROPERTY_APPLIANCE_LOCATIONID
alter table GS_PROPERTY_APPLIANCE drop constraint DF_GS_PROPERTY_APPLIANCE_MANUFACTURERID
alter table GS_PROPERTY_APPLIANCE drop constraint DF_GS_PROPERTY_APPLIANCE_MODELID
IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'tr_GS_PROPERTY_APPLIANCE' AND type = 'TR')
   DROP TRIGGER tr_GS_PROPERTY_APPLIANCE
GO