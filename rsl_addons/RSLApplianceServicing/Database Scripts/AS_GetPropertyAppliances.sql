USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyAppliances]    Script Date: 11/22/2012 20:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetPropertyAppliances @propertyId = A010060001
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/06/2012>
-- Description:	<Description,,Get Appliances against Property Id >
-- Web Page: PropertyRecord.aspx => Add Defect PopUp
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetPropertyAppliances](
@propertyId Varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct( GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID),APPLIANCETYPE
	FROM GS_PROPERTY_APPLIANCE INNER JOIN
	GS_APPLIANCE_TYPE on GS_PROPERTY_APPLIANCE.APPLIANCETYPEID = GS_APPLIANCE_TYPE.APPLIANCETYPEID
	Where PROPERTYID = @propertyId
END
