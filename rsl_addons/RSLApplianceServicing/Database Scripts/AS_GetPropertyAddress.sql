USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetEditUserPatchInfo]    Script Date: 10/19/2012 15:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Copy the blow lines to execute
--		DECLARE	@return_value int,
--		@propertyAddress varchar(100)

--		EXEC	@return_value = [dbo].[AS_GetPropertyAddress]
--				@propertyId = N'A010060001',
--				@propertyAddress = @propertyAddress OUTPUT

--		SELECT	@propertyAddress as N'@propertyAddress'

--		SELECT	'Return Value' = @return_value

-- Author:		<Noor Muhammad>
-- Create date: <19/10/2012>
-- Description:	<This procedure selects the property address against property id>
-- Web Page: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetPropertyAddress](
@propertyId varchar(20),
@propertyAddress varchar(100) out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT @propertyAddress = ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  
		From P__PROPERTY 
		Where P__PROPERTY.PROPERTYID = @propertyId
END
