BEGIN TRANSACTION
	BEGIN TRY 
	
Update PA_PARAMETER set ParameterSorder =  ParameterSorder * 10	
DECLARE @itemid int;
DECLARE @paramSortOrder int,@paramid int

SELECT @itemid = ItemID from PA_ITEM where ItemName = 'Heating'
	
Select @paramSortOrder = ParameterSorder from PA_PARAMETER	where ParameterName='Original Install Date'

insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp,ShowInAccomodation)
values('Warranty Expiry','Date','Date',1,@paramSortOrder + 1,1,1,0)


set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values(@itemid,@paramid,1)

	END TRY 
BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 	