/* =================================================================================    
    Table Description:  This is a lookup table, for meter type. It is created
    to hold meter type(s). These meter type(s) are used to select type a meter
    for a property.
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.P_MeterType
	(
	MeterTypeId int NOT NULL,
	MeterType nvarchar(100) NOT NULL,
	IsActive bit NOT NULL,
	sortOrder int NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'It is default to 1 or active'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'P_MeterType', N'COLUMN', N'IsActive'

SET @v = N'It may be used for custom sorting of records.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'P_MeterType', N'COLUMN', N'sortOrder'

ALTER TABLE dbo.P_MeterType ADD CONSTRAINT
	DF_P_MeterType_isActive DEFAULT 1 FOR IsActive

ALTER TABLE dbo.P_MeterType ADD CONSTRAINT
	PK_P_MeterType PRIMARY KEY CLUSTERED 
	(
	MeterTypeId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


ALTER TABLE dbo.P_MeterType SET (LOCK_ESCALATION = TABLE)

COMMIT
select Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'CONTROL') as Contr_Per 