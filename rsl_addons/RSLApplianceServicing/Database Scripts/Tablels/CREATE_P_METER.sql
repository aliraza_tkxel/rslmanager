/* =================================================================================    
    Table Description:  This table is to hold meter data, and meter's test/reading data.
    A meter have different attribute including meter type.
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_MeterType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_MeterType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.P_METER
	(
	MeterId int NOT NULL IDENTITY (1, 1),
	PropertyId nvarchar(40) NOT NULL,
	MeterTypeId int NOT NULL,
	Location nvarchar(200) NOT NULL,
	Manufacturer nvarchar(100) NULL,
	SerialNumber nvarchar(100) NULL,
	Installed date NOT NULL,
	Reading decimal(19, 4) NULL,
	ReadingDate date NULL,
	Passed bit NULL,
	Notes nvarchar(1000) NULL,
	IsDisconnected bit NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.P_METER ADD CONSTRAINT
	PK_P_METER PRIMARY KEY CLUSTERED 
	(
	MeterId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.P_METER ADD CONSTRAINT
	FK_P_METER_P_MeterType FOREIGN KEY
	(
	MeterTypeId
	) REFERENCES dbo.P_MeterType
	(
	MeterTypeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_METER SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_METER', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_METER', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_METER', 'Object', 'CONTROL') as Contr_Per 