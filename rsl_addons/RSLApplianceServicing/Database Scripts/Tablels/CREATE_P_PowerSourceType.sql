/* =================================================================================    
    Table Description:  This is a lookup table, for power souce type. It is created
    to hold power source type(s). These power source types are to be used to mention
    a power source for dectectors.
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.P_PowerSourceType
	(
	PowerTypeId int NOT NULL,
	PowerType nvarchar(100) NOT NULL,
	IsActive bit NOT NULL,
	sortOrder int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.P_PowerSourceType ADD CONSTRAINT
	DF_P_PowerSourceType_isActive DEFAULT 1 FOR IsActive

ALTER TABLE dbo.P_PowerSourceType ADD CONSTRAINT
	PK_P_PowerSourceType PRIMARY KEY CLUSTERED 
	(
	PowerTypeId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


ALTER TABLE dbo.P_PowerSourceType SET (LOCK_ESCALATION = TABLE)

COMMIT
select Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'CONTROL') as Contr_Per 