/* =================================================================================    
    Table Description:  This table is created to record the histroy of changes made
    in P_DETECTOR.
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.E__EMPLOYEE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.E__EMPLOYEE', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AS_JOURNAL SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AS_JOURNAL', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AS_JOURNAL', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AS_JOURNAL', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_PowerSourceType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_PowerSourceType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AS_DetectorType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AS_DetectorType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AS_DetectorType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AS_DetectorType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.P_DETECTOR SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_DETECTOR', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_DETECTOR', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_DETECTOR', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.P_DETECTOR_HISTORY
	(
	DetectorHistoryId int NOT NULL IDENTITY (1, 1),
	DetectorId int NOT NULL,
	PropertyId nvarchar(40) NOT NULL,
	DetectorTypeId int NOT NULL,
	Location nvarchar(200) NOT NULL,
	Manufacturer nvarchar(100) NULL,
	SerialNumber nvarchar(100) NULL,
	PowerSource int NULL,
	InstalledDate date NOT NULL,
	InstalledBy INT NOT NULL,
	IsLandlordsDetector bit NOT NULL,
	TestedDate date NULL,
	TestedBy INT NULL,
	BatteryReplaced date NULL,
	Passed bit NULL,
	Notes nvarchar(1000) NULL,
	JournalId INT NULL,
	CTimeStamp datetime2(7) NOT NULL CONSTRAINT DF_P_DETECTOR_HISTORY_CTimeStamp DEFAULT getdate()
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	PK_P_DETECTOR_HISTORY PRIMARY KEY CLUSTERED 
	(
	DetectorHistoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_P_DETECTOR FOREIGN KEY
	(
	DetectorId
	) REFERENCES dbo.P_DETECTOR
	(
	DetectorId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_AS_DetectorType FOREIGN KEY
	(
	DetectorTypeId
	) REFERENCES dbo.AS_DetectorType
	(
	DetectorTypeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_P_PowerSourceType FOREIGN KEY
	(
	PowerSource
	) REFERENCES dbo.P_PowerSourceType
	(
	PowerTypeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_AS_JOURNAL FOREIGN KEY
	(
	JournalId
	) REFERENCES dbo.AS_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_E__EMPLOYEE FOREIGN KEY
	(
	InstalledBy
	) REFERENCES dbo.E__EMPLOYEE
	(
	EMPLOYEEID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY ADD CONSTRAINT
	FK_P_DETECTOR_HISTORY_E__EMPLOYEE1 FOREIGN KEY
	(
	TestedBy
	) REFERENCES dbo.E__EMPLOYEE
	(
	EMPLOYEEID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.P_DETECTOR_HISTORY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_DETECTOR_HISTORY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_DETECTOR_HISTORY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_DETECTOR_HISTORY', 'Object', 'CONTROL') as Contr_Per 