/*
   Friday, September 18, 201510:02:49 PM
   User: tkxel
   Server: 10.0.1.75
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
IF Not EXISTS(SELECT 1 FROM sys.columns 
            WHERE Name = N'IsDisconnected' AND Object_ID = Object_ID(N'GS_PROPERTY_APPLIANCE'))
BEGIN
  ALTER TABLE dbo.GS_PROPERTY_APPLIANCE ADD
	IsDisconnected bit NULL
END

IF Not EXISTS(SELECT 1 FROM sys.columns 
            WHERE Name = N'DisconnectedDate' AND Object_ID = Object_ID(N'GS_PROPERTY_APPLIANCE'))
BEGIN
ALTER TABLE dbo.GS_PROPERTY_APPLIANCE ADD
	DisconnectedDate datetime2(2) NULL
	
END	
GO
ALTER TABLE dbo.GS_PROPERTY_APPLIANCE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.GS_PROPERTY_APPLIANCE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.GS_PROPERTY_APPLIANCE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.GS_PROPERTY_APPLIANCE', 'Object', 'CONTROL') as Contr_Per 