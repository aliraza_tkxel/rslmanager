/* =================================================================================    
    Table Description:  This is a lookup table, for priority. It is created
    to hold priority and related duration and type of duration day(s)/hour(s).
 
    Author: Aamir Waheed
    Creation Date:  07/07/2015
    
*================================================================================= */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[P_DEFECTS_PRIORITY]
	(
	PriorityID int NOT NULL,
	PriorityName nvarchar(50) NOT NULL,
	ResponseDuration smallint NOT NULL,
	IsDay bit NOT NULL,
	IsActive bit NOT NULL,
	sortOrder int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[P_DEFECTS_PRIORITY] ADD CONSTRAINT
	DF_P_DEFECTS_PRIORITY_IsDay DEFAULT 1 FOR IsDay

ALTER TABLE dbo.[P_DEFECTS_PRIORITY] ADD CONSTRAINT
	DF_P_DEFECTS_PRIORITY_IsActive DEFAULT 1 FOR IsActive

ALTER TABLE dbo.[P_DEFECTS_PRIORITY] ADD CONSTRAINT
	[PK_P_DEFECTS_PRIORITY] PRIMARY KEY CLUSTERED 
	(
	PriorityID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


ALTER TABLE dbo.[P_DEFECTS_PRIORITY] SET (LOCK_ESCALATION = TABLE)

COMMIT
select Has_Perms_By_Name(N'dbo.[P_DEFECTS_PRIORITY]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[P_DEFECTS_PRIORITY]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[P_DEFECTS_PRIORITY]', 'Object', 'CONTROL') as Contr_Per 