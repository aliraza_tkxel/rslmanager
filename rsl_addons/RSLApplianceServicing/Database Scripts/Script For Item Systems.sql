BEGIN TRANSACTION
	BEGIN TRY  
-- Internal > Services > Smoke/CO
DECLARE @itemid int;
DECLARE @coitemid int;
DECLARE @paramid int;
DECLARE @parItemId int;
INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Systems',4,1,0,0)

set @parItemId = SCOPE_IDENTITY()
 
 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'Smoke',1,1,@parItemId,0,0)

set @itemid = SCOPE_IDENTITY()

 INSERT INTO [PA_ITEM] (AreaID,ItemName ,ItemSorder,IsActive,ParentItemId,ShowInApp,ShowListView) values
((select top 1 AreaID from PA_AREA where AreaName='Services' and LocationId=3),'CO',1,1,@parItemId,0,0)

set @coitemid = SCOPE_IDENTITY()
  ---- for Smoke Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Type','String','Dropdown',0,0,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@itemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Ionization',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Optical',1,1)
---- for CO Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Type','String','Dropdown',0,0,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@coitemid,@paramid,1)

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Opto Chemical',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Biomimetic',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Electrochemical',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Semiconductor',3,1)
-- for Quantity
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Quantity','String','TextBox',0,1,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@itemid,@paramid,1)
insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@coitemid,@paramid,1)

-- for Date Installed
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Date Installed','Date','Date',1,2,1,1)

set @paramid = SCOPE_IDENTITY()

INSERT INTO [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@itemid,@paramid,1)
INSERT INTO [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@coitemid,@paramid,1)
-- for Notes
INSERT INTO [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive, ShowInApp)
values
('Notes','String','TextArea',0,3,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@itemid,@paramid,1)
insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)values(@coitemid,@paramid,1)
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 