-- Stored Procedure

CREATE PROCEDURE [dbo].[Find_Text_In_SP]
@StringToSearch varchar(100) ='P_ATTRIBUTES'
AS 
   SET @StringToSearch = '%' + Lower( @StringToSearch ) + '%'
   SELECT Distinct SO.Name
     FROM sysobjects SO (NOLOCK)INNER JOIN syscomments SC (NOLOCK) on 
               SO.Id = SC.ID AND
               SO.Type = 'P' AND 
               Lower( SC.Text ) LIKE @stringtosearch
      ORDER BY SO.Name


GO