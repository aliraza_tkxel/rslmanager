USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPrintCertificateReport]    Script Date: 11/17/2015 16:25:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <22/08/2013>
-- Description:	<This stored procedure fetches the Print Certificate Report>
-- Web Page:    <PrintCertificates.aspx>
-- EXEC	@return_value = [dbo].[AS_GetPrintCertificateReport]
--		@fromDate = N'2013/07/01',
--		@toDate = N'2013/07/31',
--		@searchText = N'Adnan',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Ref',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT   
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetPrintCertificateReport] 
	-- Add the parameters for the stored procedure here
	@fromDate DateTime,
	@toDate DateTime,
	@searchText nvarchar(200),
	--Parameters which would help in sorting and paging
	@pageSize int = 30,
	@pageNumber int = 1,
	@sortColumn varchar(50) = 'Ref',
	@sortOrder varchar (5) = 'DESC',
	@totalCount int = 0 output
AS
BEGIN

    -- Insert statements for procedure here
	DECLARE @Certificate int,
	@Defects int,
	@DefectID int,
	@JournalID int,
	@PropertyID nvarchar(20),
	@SingleWarning bit,
	@Warning nvarchar(5),
	
	-- Declared for the paging part
	@SelectClause varchar(2000),
    @fromClause   varchar(1500),            
    @orderClause  varchar(100),	
    @mainSelectQuery varchar(5500),        
    @rowNumberQuery varchar(6000),
    @finalQuery varchar(6500),
    -- used to add in conditions in WhereClause based on search criteria provided
    @searchCriteria varchar(1500),
        
    --variables for paging
    @offset int,
	@limit int	

	SET @Warning = 'No'
	
	--Paging Formula
	SET @offset = 1+(@pageNumber-1) * @pageSize
	SET @limit = (@offset + @pageSize)-1
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT P__PROPERTY.PROPERTYID AS Ref,
	P__PROPERTY.HOUSENUMBER + ' ' + P__PROPERTY.ADDRESS1 + ', ' + P__PROPERTY.TOWNCITY AS 'Address',
	P__PROPERTY.HOUSENUMBER AS HOUSENUMBER, P__PROPERTY.ADDRESS1 AS ADDRESS1, P__PROPERTY.TOWNCITY AS TOWNCITY,
	P_LGSR.LGSRID AS 'Certificate',
	P_LGSR.CP12NUMBER AS CP12NUMBER,
	CONVERT(nvarchar(10),P_LGSR.ISSUEDATE, 103) AS Issued,
	--'CONVERT(nvarchar(5),P_LGSR.ISSUEDATE, 108) + ' ' + CONVERT(nvarchar(10),P_LGSR.ISSUEDATE, 103) AS Issued,
	P_LGSR.ISSUEDATE AS ISSUEDATE,E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME AS 'By',
	E__EMPLOYEE.FIRSTNAME AS FIRSTNAME, E__EMPLOYEE.LASTNAME AS LASTNAME, AS_JOURNALHISTORY.JOURNALID As JournalID,
	'No' As WarningNote, 0 As Defects
	INTO #TempIssuedCertificateReport
	FROM AS_JOURNALHISTORY
	JOIN AS_Status ON AS_JOURNALHISTORY.STATUSID = AS_Status.StatusId
	JOIN P_LGSR ON AS_JOURNALHISTORY.PROPERTYID = P_LGSR.PROPERTYID AND AS_JOURNALHISTORY.JOURNALID = P_LGSR.JOURNALID
	JOIN P__PROPERTY ON AS_JOURNALHISTORY.PROPERTYID = P__PROPERTY.PROPERTYID
	JOIN E__EMPLOYEE ON P_LGSR.CP12ISSUEDBY = E__EMPLOYEE.EMPLOYEEID
	WHERE AS_Status.Title = 'CP12 issued' AND
	((E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME) LIKE '%'+@searchText+'%' OR 
	(P__PROPERTY.HOUSENUMBER + ' ' + P__PROPERTY.ADDRESS1 + ', ' + P__PROPERTY.TOWNCITY) LIKE '%'+@searchText+'%')
	AND P_LGSR.ISSUEDATE >= @FromDate AND P_LGSR.ISSUEDATE <= @ToDate 

	ALTER TABLE #TempIssuedCertificateReport
	ALTER COLUMN WarningNote nvarchar(5)
		
	ALTER TABLE #TempIssuedCertificateReport
	ALTER COLUMN Defects int

	SELECT @Certificate = MIN(Certificate)
	FROM #TempIssuedCertificateReport

	WHILE @Certificate IS NOT NULL
	BEGIN

		SELECT @PropertyID = Ref, @JournalID = JournalID
		FROM #TempIssuedCertificateReport
		WHERE Certificate = @Certificate
		
		SELECT @Defects = COUNT(PropertyDefectId)
		FROM P_PROPERTY_APPLIANCE_DEFECTS
		WHERE PropertyId = @PropertyID AND JournalId = @JournalID AND DefectDate >= @FromDate
		AND DefectDate <= @ToDate
		
		SELECT PropertyDefectId as ID, IsWarningIssued as Warning
		INTO #TempDefects
		FROM P_PROPERTY_APPLIANCE_DEFECTS
		WHERE PropertyId = @PropertyID AND JournalId = @JournalID 
				AND DefectDate >= @FromDate	AND DefectDate <= @ToDate
				AND CategoryId NOT IN (SELECT CategoryId FROM P_DEFECTS_CATEGORY WHERE Description = 'General comment')
		
		SELECT @DefectID = MIN(ID)
		FROM #TempDefects
		
		WHILE @DefectID IS NOT NULL
		BEGIN
		
			SELECT @SingleWarning = Warning 
			FROM #TempDefects
			WHERE ID = @DefectID
			
			IF @SingleWarning = 1
			BEGIN
				SET @Warning = 'Yes'
				BREAK
			END
		
			SELECT @DefectID = MIN(ID)
			FROM #TempDefects
			WHERE ID > @DefectID
		END
		
		drop table #TempDefects	
		
		UPDATE #TempIssuedCertificateReport
		SET Defects = @Defects, WarningNote = @Warning
		WHERE Certificate = @Certificate
		
		SET @Warning = 'No'
		
		print CONVERT(nvarchar(10),@Defects)
		
		SELECT @Certificate = MIN(Certificate)
		FROM #TempIssuedCertificateReport
		WHERE Certificate > @Certificate
	END
	
	--========================================================================================	        
	-- Begin building SELECT clause
	-- Insert statements for procedure here
		
	SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') *'
	-- End building SELECT clause
	--======================================================================================== 							
				
	--========================================================================================    
	-- Begin building FROM clause
	SET @fromClause =	  CHAR(10) +'FROM #TempIssuedCertificateReport'
	-- End building From clause
	--======================================================================================== 														  
		
	--========================================================================================    
	-- Begin building OrderBy clause		
		
	-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
	IF(@sortColumn = 'Address')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'HOUSENUMBER,ADDRESS1,TOWNCITY' 		
	END
		
	IF(@sortColumn = 'By')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'FIRSTNAME,LASTNAME' 		
	END
		
	IF(@sortColumn = 'Issued')
	BEGIN
		SET @sortColumn = CHAR(10)+ 'ISSUEDATE' 		
	END			
		
	SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
	-- End building OrderBy clause
	--========================================================================================								
			
	--========================================================================================
	-- Begin building the main select Query
		
	Set @mainSelectQuery = @selectClause +@fromClause + @orderClause 
		
	-- End building the main select Query
	--========================================================================================																																			
	
	--========================================================================================
	-- Begin building the row number query
		
	Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
