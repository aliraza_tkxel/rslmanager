USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetMake
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,02/11/2012>
-- Description:	<Description,,Get all make types>
-- Web Page: PropertyRecrod.aspx
-- Control Page: Appliance.ascx
-- =============================================
Alter PROCEDURE [dbo].[AS_GetMake]
(@prefix varchar(100) )  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT MANUFACTURERID as id, MANUFACTURER as title
	FROM GS_MANUFACTURER Where MANUFACTURER like'%'+@prefix+'%'  
END
