USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[DF_AssignDefectWorkToContractor]    Script Date: 09/28/2015 19:13:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Noor Muhammad>
-- Create date: <29/09/2015>
-- Description:	Assign Work to Contractor, Defect work
-- History:     29/09/2015 Noor : Created the stored procedure
--              29/09/2015 Name : Description of Work

-- =============================================
ALTER PROCEDURE [dbo].[DF_AssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@contactId INT,
	@contractorId INT,
	@userId int,
	@estimate SMALLMONEY,
	@estimateRef NVARCHAR(200),
	@poStatus INT,
	@propertyId varchar(20),
	@journalId INT ,
	@contractorWorksDetail AS DF_AssingToContractorWorksRequired READONLY,
	@defectNotes nvarchar(200),
	@customerId int,
	@isSavedOut BIT = 0 OUTPUT,
	@poStatusIdOut int = 0 OUTPUT
	

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRANSACTION
BEGIN TRY

-- =====================================================
-- General Purpose Variable
-- =====================================================

-- To save same time stamp in all records 
DECLARE @currentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusHistoryId int = NULL
DECLARE @newStatusId int = NULL
-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @active bit = 1
, @poType int = (	SELECT	POTYPEID
					FROM F_POTYPE
					WHERE POTYPENAME = 'Appliance Defect') -- 2 = 'Repair'

, @purchaseOrderId int
, @blockId INT = NULL
, @schemeId INT = NULL
, @developmentId INT = NULL

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
POTYPE, POSTATUS, GASSERVICINGYESNO, POTIMESTAMP, DEVELOPMENTID, BLOCKID)
	VALUES (UPPER('Gas Servicing'), @currentDateTime, @defectNotes , @userId, @contractorId, @active, @poType, @poStatus, 1, @currentDateTime, @developmentId, @blockId)

-- To get Identity Value of Purchase Order.
SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Old table(s)
-- Insert new P_WORKORDER
-- =====================================================
DECLARE @title nvarchar(50) = 'Assigned To Contractor'
DECLARE @woStatus INT = 2 -- 'Assigned To Contractor
If (@poStatus = 0) SET @woStatus = 12 --Pending
DECLARE @birthNature INT = 2 --'Day To Day Repair'
DECLARE @birthEntity INT = 1 --'FOR Property'
DECLARE @birthModule INT = 1 --'Customer Module'

DECLARE @today DATE = CAST(@currentDateTime AS DATE)
DECLARE @tenancyId INT
SELECT @tenancyId = MAX(TENANCYID) FROM C_TENANCY T
	WHERE T.PROPERTYID = @propertyId AND (T.ENDDATE IS NULL OR T.ENDDATE >= @today)



INSERT INTO P_WORKORDER (ORDERID, TITLE, CUSTOMERID, PROPERTYID, TENANCYID, WOSTATUS, CREATIONDATE, BIRTH_MODULE
						,BIRTH_ENTITY, BIRTH_NATURE, DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId)
		VALUES(@purchaseOrderId,@title, @customerId, @propertyId, @tenancyId,@woStatus,@currentDateTime,@birthModule
				, @birthEntity, @birthNature, @developmentId, @blockId, 0, @schemeId)

DECLARE @woId INT = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================
DECLARE @defectContractorId int
INSERT INTO PDR_CONTRACTOR_WORK (JournalId, ContractorId, AssignedDate, AssignedBy, Estimate,EstimateRef
,ContactId,PurchaseORDERID)
	VALUES (@journalId,@contractorId,@currentDateTime,@userId, @estimate, @estimateRef, @contactId,@purchaseOrderId )

SET @defectContractorId = SCOPE_IDENTITY()


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	worksRequired, netCost, vatType, vat,
	gross, piStatus, expenditureId, costCenterId, budgetHeadId
FROM @contractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE @worksRequired nvarchar(4000),
@netCost smallmoney,
@vatType int,
@vat smallmoney,
@gross smallmoney,
@piStatus int,
@expenditureId int,
@costCenterId int,
@budgetHeadId int

-- Variable used within loop
DECLARE @purchaseItemTITLE nvarchar(100) = @defectNotes -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @worksRequired, @netCost, @vatType, @vat,
		@gross, @piStatus, @expenditureId, @costCenterId, @budgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN

			-- =====================================================
			-- Old table(s)
			-- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER
			-- =====================================================

			DECLARE @itemId INT = 1 --'PROPERTY'
			DECLARE @status INT = 2 --'ASSIGNED
			If (@poStatus = 0) SET @status = 12 --Pending


			INSERT INTO C_JOURNAL ( CUSTOMERID, TENANCYID, PROPERTYID, ITEMID,ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)
			VALUES(@customerId, @tenancyId, @propertyId, @itemId, @birthNature, @status, @currentDateTime,@title)

			DECLARE @cJournalId INT = SCOPE_IDENTITY()

			-- =====================================================
			-- Old table(s)
			--INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER
			-- =====================================================

			DECLARE @itemActionId INT = 2 --'ASSIGNED TO CONTRACTOR'

			If (@poStatus = 0) SET @itemActionId = 12 --Pending

			DECLARE @propertyAddress NVARCHAR(400)

			SELECT	@propertyAddress = ISNULL(FLATNUMBER+',','')+ISNULL(HOUSENUMBER+',','')+ISNULL(ADDRESS1+',','')+ISNULL(ADDRESS2+',','')+ISNULL(ADDRESS3+',','')+ISNULL(TOWNCITY+',','')+ISNULL(POSTCODE,'')
			FROM	P__PROPERTY
			WHERE	PROPERTYID = @propertyId

			--ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE 
			--AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT

			INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE,LASTACTIONUSER, ITEMDETAILID,
								  CONTRACTORID, TITLE, NOTES,SCOPEID)	
			VALUES(@cJournalId, @status, @itemActionId, @currentDateTime, @userId,NULL,@contractorId,'FOR'+@propertyAddress,@title,NULL)

			DECLARE @repairHistoryId INT = SCOPE_IDENTITY()

			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
			NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
				VALUES (@purchaseOrderId, @expenditureId, @purchaseItemTITLE, @worksRequired, 
				@currentDateTime,  @netCost, @vatType, @vat, @gross, @userId, @active, @poType, @poStatus)

			DECLARE @orderItemId int = SCOPE_IDENTITY()

			-- =====================================================
			--INSERT VALUE IN P_WOTOREPAIR for each work required
			-- =====================================================

			INSERT INTO P_WOTOREPAIR(WOID, JOURNALID, ORDERITEMID)
			VALUES(@woId,@cJournalId,@orderItemId)

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================		
			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
				(
					[PDRContractorId],[ServiceRequired],[NetCost],
					[VatId],[Vat],[Gross],[ExpenditureId],
					[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID]
				)
			VALUES			
				(
					@defectContractorId, @worksRequired, @netCost, 
					@vatType, @vat, @gross,@expenditureId,
					@costCenterId,@budgetHeadId, @orderItemId
				)					

		-- Fetch record for next loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @worksRequired, @netCost, @vatType, @vat,
		@gross, @piStatus, @expenditureId, @costCenterId, @budgetHeadId
		END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

-- =====================================================
-- If PO is queue set work order status to 12 (queued) as set in Portfolio Work Order
-- =====================================================

IF @poStatus = 0
 BEGIN
  UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = @woId
 END
ELSE
 BEGIN
	-- - - -  - - - - - - -  -
	-- AUTO ACCEPT REPAIRS
	-- - ---- - - - -  - - - -
	-- we check to see if the org has an auto accept functionality =  1
	-- if so then we auto accept the repair
  EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = @PurchaseOrderId, @SUPPLIERID =  @contractorId
END


CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor
SET @poStatusIdOut =  @poStatus


END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSavedOut = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSavedOut = 1
	END
END
