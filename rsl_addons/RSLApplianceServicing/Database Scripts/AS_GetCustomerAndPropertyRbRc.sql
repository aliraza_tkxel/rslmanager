USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC AS_GetCustomerAndPropertyRbRc 
	--@propertyId = 'A010060001'
	--@rentBalance OUTPUT,
	--@rentCharge OUTPUT
-- Author:		<Author,,NoorMuhammad>
-- Create date: <Create Date,,25/Mar/2013>
-- Description:	<Description,,AS_GetCustomerAndPropertyRbRc >
-- Web Page: EditLetter.aspx => Get the customer info and property information 
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetCustomerAndPropertyRbRc](
	@propertyId varchar(12),
	@rentBalance float output,
	@rentCharge float output
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	Select 
		C_TENANCY.TENANCYID as Tenancy,
		-- TenantName, In case of multiple tenants it will show multiple names.
		ISNULL(STUFF((
				SELECT
					' & ' + ISNULL (G_TITLE.DESCRIPTION + '.','')+ ISNULL(' ' + C__CUSTOMER.FIRSTNAME ,'') + ISNULL(' ' + C__CUSTOMER.LASTNAME ,'')
				FROM  C_CUSTOMERTENANCY
					INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID		
					INNER JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
				WHERE 1 = 1
					AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE >= CONVERT(DATE,GETDATE()))
					AND C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
				ORDER BY C__CUSTOMER.FIRSTNAME DESC
				FOR XML PATH('')),1,(LEN(' &amp; ') + 1),''),'N/A') TenantName,
		
		
		
		ISNULL(P__PROPERTY.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,
				
		
		ISNULL(STUFF((
					SELECT
						' & ' + ISNULL (G_TITLE.DESCRIPTION  + '.','') + ISNULL(' ' + SUBSTRING(C__CUSTOMER.FIRSTNAME,1,1) ,'') + ISNULL(' '+ C__CUSTOMER.LASTNAME ,'')
					FROM  C_CUSTOMERTENANCY
						INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID		
						INNER JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
					WHERE 1 = 1
						AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE >= CONVERT(DATE,GETDATE()))
						AND C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID
					ORDER BY C__CUSTOMER.FIRSTNAME DESC
					FOR XML PATH('')),1,(LEN(' &amp; ') + 1),''),'N/A') TenantNameSalutation
		
		
		
From		
	P__PROPERTY 
		INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID AND (C_TENANCY.ENDDATE IS NULL OR C_TENANCY.ENDDATE >= CONVERT(DATE, GETDATE()))
		
WHERE 1 = 1		
		AND	P__PROPERTY.PROPERTYID = @propertyId
			
ORDER BY
	TENANCY
	
		
		EXEC AS_PropertyRbRc @propertyId, @rentBalance output, @rentCharge output			

END
