/****** Object:  StoredProcedure [dbo].[AS_AddSavedLetter]    Script Date: 10/16/2012 12:09:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 10/10/2012
-- Description:	This SP would save a SAVED letter into the databse. 
-- Useage: View letter
-- EXEC	@return_value = [dbo].[AS_AddSavedLetter]
--		@JournalHistoryId = 1,
--		@LetterBody = N'This is the letter body',
--		@LetterTitle = N'This is the letter title',
--		@LetterId = 1 
-- =============================================
CREATE PROCEDURE [dbo].[AS_AddSavedLetter] 
	-- Add the parameters for the stored procedure here
	@JournalHistoryId	int,
	@LetterBody			varchar(MAX),
	@LetterTitle		varchar(MAX),
	@LetterId			int,
	@TeamId				int,
	@FromResourceId		int,
	@SignOffLookupCode	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [RSLBHALive].[dbo].[AS_SAVEDLETTERS]
           ([JOURNALHISTORYID]
           ,[LETTERCONTENT]
           ,[LETTERID]
           ,[LETTERTITLE]
           ,[TEAMID]
           ,[FromResourceId]
           ,[SignOffLookupCode])
    VALUES
           (@JournalHistoryId
           ,@LetterBody
           ,@LetterId
           ,@LetterTitle
           ,@TeamId
           ,@FromResourceId
           ,@SignOffLookupCode)
END
