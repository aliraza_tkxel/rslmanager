USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[AS_GetAppointmentDetail]
--		@appointmentId = 1

--SELECT	'Return Value' = @return_value

-- Author:		<Ahmed Mehmood>
-- Create date: <14/1/2016>
-- Description:	<This procedure gets the detail of appointment>
-- Web Page: AppointmentArranged.aspx
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetAppointmentDetail](
@appointmentId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT	ISNULL(P__PROPERTY.HouseNumber,'') + ISNULL(' '+P__PROPERTY.ADDRESS1,'') + ISNULL(', '+P__PROPERTY.TOWNCITY,'')  AS location
				,DATENAME(WEEKDAY, AS_APPOINTMENTS.APPOINTMENTDATE) + ' '+ DATENAME(DAY, AS_APPOINTMENTS.APPOINTMENTDATE) + ' ' + DATENAME(MONTH, AS_APPOINTMENTS.APPOINTMENTDATE) + ' ' + CAST(YEAR(AS_APPOINTMENTS.APPOINTMENTDATE) AS VARCHAR(4)) AS appointmentDate 
				,ISNULL(AS_APPOINTMENTS.APPOINTMENTSTARTTIME,'n/a') AS startTime
				,ISNULL(AS_APPOINTMENTS.APPOINTMENTENDTIME,'n/a') AS endTime
				,E__EMPLOYEE.FIRSTNAME +' '+ E__EMPLOYEE.LASTNAME AS operative
				,ISNULL(AS_APPOINTMENTS.NOTES,'') AS notes
				,AS_APPOINTMENTS.ASSIGNEDTO AS operativeId
		From	AS_APPOINTMENTS
				INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
				INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
				INNER JOIN E__EMPLOYEE ON AS_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		Where	AS_APPOINTMENTS.APPOINTMENTID = @appointmentId
END


