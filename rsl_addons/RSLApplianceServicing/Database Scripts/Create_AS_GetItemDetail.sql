USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetItemDetail]    Script Date: 11/01/2013 11:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Ali Raza>  
-- Create date: <Create Date,10/22/2013>  
-- Description: <Description,Get the detail of tree leaf node>  
--EXEC [dbo].[AS_GetItemDetail]  
--  @itemId = 1,  
--  @PropertyId = N'BHA0000987'  
-- =============================================  
Create PROCEDURE [dbo].[AS_GetItemDetail]  
 -- Add the parameters for the stored procedure here  
 --@areaId int,    
 @itemId int,  
 @propertyId nvarchar(20)    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
--=================================================================  
  --get Parameters  
 select ItemParamID,PA_ITEM.ItemID,PA_PARAMETER.ParameterID,ParameterName,DataType,ControlType,IsDate,ParameterSorder from PA_ITEM_PARAMETER   
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID   
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId   
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID   
 where PA_ITEM.ItemID=@itemId --AND PA_AREA.AreaID =@areaId  
   
 --=================================================================  
  ---get parameter values  
  --=================================================================  
 SELECT ValueID, ParameterID, ValueDetail, Sorder FROM PA_PARAMETER_VALUE where ParameterID IN ( select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER   
inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID   
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId   
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID   
 where PA_ITEM.ItemID=@itemId ) --AND PA_AREA.AreaID =@areaId)   
   
 --=================================================================  
 ---get pre inserted values   
 --=================================================================  
SELECT PROPERTYID,ATTRIBUTEID,ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY ,IsCheckBoxSelected FROM PA_PROPERTY_ATTRIBUTES  
  WHERE PROPERTYID=  @propertyId AND ITEMPARAMID IN  ( select ItemParamID from PA_ITEM_PARAMETER   
 INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID   
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId   
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID   
where PA_ITEM.ItemID=@itemId ) --AND PA_AREA.AreaID =@areaId)    
   
 --=================================================================  
  ---get Last Replaced Dates  
  --=================================================================  
  SELECT [SID],PROPERTYID,ItemId,LastDone,ParameterId from PA_PROPERTY_ITEM_DATES  
   where PROPERTYID=@propertyId AND ItemId =@itemId AND ParameterID IN ( select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER   
inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID   
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId   
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID   
 where PA_ITEM.ItemID=@itemId ) 
  --=================================================================  
  ---get Replacement Due
    SELECT [SID],PROPERTYID,ItemId,DueDate,ParameterId from PA_PROPERTY_ITEM_DATES  
   where PROPERTYID=@propertyId AND ItemId =@itemId AND ParameterID IN ( select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER   
inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID   
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId   
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID   
 where PA_ITEM.ItemID=@itemId ) 
 --=================================================================  
END  