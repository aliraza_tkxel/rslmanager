USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_AddAction]    Script Date: 11/27/2012 15:35:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_AddAction
-- Author:		<Salman Nazir>
-- Create date: <23/09/2012>
-- Description:	<This Stored Proceedure Add Actions on Status Page >
-- Web Page: Status.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_AddAction](
@statusId int,
@ranking int,
@title varchar(1000),
@createdBy int
)
AS
BEGIN
Declare @checkRecord int
Declare @countRecord int
Declare @actionTableId int
Declare @lastActionId int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
    -- Insert statements for procedure here
    SELECT @actionTableId = AS_Action.ActionId
    FROM AS_Action
    WHERE Ranking = @ranking AND StatusId = @statusId
    
    SELECT @countRecord = COUNT(*) FROM AS_Action WHERE StatusId = @statusId
    
    
    if @actionTableId > 0
		BEGIN
			BEGIN Tran	        
				UPDATE AS_Action SET Ranking = @countRecord+1 WHERE ActionId = @actionTableId
				Declare @ActionTitle varchar(500)
				Declare @ActionStatusId int
				Declare @isEditable smallint

				SELECT @ActionTitle=Title,@ActionStatusId=StatusId,@isEditable=IsEditable FROM AS_Action WHERE ActionId = @actionTableId
				INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				Values(@actionTableId,@ActionStatusId,@ActionTitle,@countRecord+1,@isEditable,@createdBy,GETDATE(),GETDATE())
				
				
				
				INSERT INTO AS_Action (StatusId,Title,Ranking,CreatedBy,IsEditable,CreatedDate ) 
					VALUES (@statusId,@title,@ranking,@createdBy,1,GETDATE())	
					
				SELECT @lastActionId=ActionId from AS_Action WHERE StatusId = @statusId AND Ranking = @ranking
				INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				VALUES (@lastActionId,@statusId,@title,@ranking,1,1,GETDATE(),GETDATE())							
			COMMIT	
		END
	ELSE
	
		BEGIN
			INSERT INTO AS_Action (StatusId,Title,Ranking,CreatedBy,IsEditable,CreatedDate ) 
				VALUES (@statusId,@title,@ranking,@createdBy,1,GETDATE())	
				SELECT @lastActionId=ActionId from AS_Action WHERE StatusId = @statusId AND Ranking = @ranking
			INSERT INTO AS_ActionHistory(ActionId,StatusId,Title,Ranking,IsEditable,CreatedBy,CreatedDate,Ctimestamp)
				VALUES (@lastActionId,@statusId,@title,@ranking,1,1,GETDATE(),GETDATE())		
		END			
END
