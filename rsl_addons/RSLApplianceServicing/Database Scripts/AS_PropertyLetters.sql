SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC AS_PropertyLetters
		--@journalHistoryId=1
		
-- Author:		<Noor Muhammad>
-- Create date: <10/3/2012>
-- Description:	<This stored procedure returns the all the letters against the property history>
-- Parameters:	
		--@journalHistoryId int = 0
-- Webpage :PropertyRecord.aspx		
		
-- =============================================
CREATE PROCEDURE [dbo].[AS_PropertyLetters]
( 
		@journalHistoryId bigint = 0
		
)
AS
BEGIN
SELECT 
AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId
,AS_SAVEDLETTERS.LetterId as StandardLetterId
,AS_SAVEDLETTERS.SavedLetterId as LetterHistoryId
,AS_StandardLetters.Title as LetterTitle

FROM AS_JOURNALHISTORY 

INNER JOIN AS_SAVEDLETTERS ON  AS_SAVEDLETTERS.JOURNALHISTORYID = AS_JOURNALHISTORY.JOURNALHISTORYID
INNER JOIN AS_StandardLetters on AS_StandardLetters.StandardLetterId = AS_SAVEDLETTERS.LETTERID
Where AS_JOURNALHISTORY.JournalHistoryId =@journalHistoryId

END

