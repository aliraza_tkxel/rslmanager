USE [RSLBHALive]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM]') AND parent_object_id = OBJECT_ID(N'[dbo].[PLANNED_COMPONENT_ITEM]'))
ALTER TABLE [dbo].[PLANNED_COMPONENT_ITEM] DROP CONSTRAINT [FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM]
GO

USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PLANNED_COMPONENT_ITEM]    Script Date: 03/18/2014 16:06:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PLANNED_COMPONENT_ITEM]') AND type in (N'U'))
DROP TABLE [dbo].[PLANNED_COMPONENT_ITEM]
GO

USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PLANNED_COMPONENT_ITEM]    Script Date: 03/18/2014 16:06:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PLANNED_COMPONENT_ITEM](
	[COMPONENTITEMID] [int] IDENTITY(1,1) NOT NULL,
	[COMPONENTID] [smallint] NOT NULL,
	[ITEMID] [int] NOT NULL,
	[PARAMETERID] [int] NULL,
	[VALUEID] [int] NULL,
	[ISACTIVE] [bit] NULL,
 CONSTRAINT [PK_PLANNED_COMPONENT_ITEM] PRIMARY KEY CLUSTERED 
(
	[COMPONENTITEMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PLANNED_COMPONENT_ITEM]  WITH CHECK ADD  CONSTRAINT [FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM] FOREIGN KEY([COMPONENTID])
REFERENCES [dbo].[PLANNED_COMPONENT] ([COMPONENTID])
GO

ALTER TABLE [dbo].[PLANNED_COMPONENT_ITEM] CHECK CONSTRAINT [FK_PLANNED_COMPONENT_PLANNED_COMPONENT_ITEM]
GO


