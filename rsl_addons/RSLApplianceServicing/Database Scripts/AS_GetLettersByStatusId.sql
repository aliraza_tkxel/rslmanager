-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetLettersByStatusId @statusId = 0
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,09/10/2012>
-- Description:	<Description,,Get Letters against Status Id>
-- Web Page: Status.aspx
-- =============================================
CREATE PROCEDURE AS_GetLettersByStatusId(
	-- Add the parameters for the stored procedure here
	@statusId int = -1
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	  SELECT StandardLetterId,Title from AS_StandardLetters WHERE StatusId = @statusId OR @statusId = -1

END
GO
