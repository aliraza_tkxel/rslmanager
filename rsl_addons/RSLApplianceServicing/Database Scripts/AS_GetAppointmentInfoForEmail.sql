USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC	[dbo].[AS_GetAppointmentInfoForEmail]
--		@journalHistoryId = 76576
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,, 19 August, 2014>
-- Description:	<Description,,This procedure 'll get appointment info for Email >
-- =============================================
CREATE PROCEDURE [dbo].[AS_GetAppointmentInfoForEmail] 
	@journalHistoryId as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT	C.FIRSTNAME +' '+ C.LASTNAME AS	CustomerName
			, ISNULL(AA.APPOINTMENTSTARTTIME,'') AS AppointmentTime
			,ISNULL(AA.APPOINTMENTDATE,'') AS AppointmentDate
			,ISNULL(AA.JSGNUMBER,'') AS JSG
			,ISNULL(CD.EMAIL,'') AS Email
	FROM	AS_APPOINTMENTS AA
			INNER JOIN C_CUSTOMERTENANCY CC ON AA.TENANCYID = CC.TENANCYID
			INNER JOIN C__CUSTOMER C ON CC.CUSTOMERID = C.CUSTOMERID
			INNER JOIN C_ADDRESS CD ON  C.CUSTOMERID = CD.CUSTOMERID
	WHERE	AA.JOURNALHISTORYID = @journalHistoryId	
			AND CD.ISDEFAULT =1
		
END
