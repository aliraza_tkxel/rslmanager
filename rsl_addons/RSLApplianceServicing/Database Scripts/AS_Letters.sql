SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetLetters
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<This Stored Proceedure shows the Standard Letters on the Resources Page , Letters Listing>
-- Web Page: Resources.aspx
-- =============================================
CREATE PROCEDURE AS_GetLetters
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AS_StandardLetters.Title FROM AS_StandardLetters
END
GO
