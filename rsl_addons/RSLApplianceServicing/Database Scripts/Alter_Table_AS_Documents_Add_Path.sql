-- Author:		<Noor Muhammad>
-- Create date: <03 Oct 2012>
-- Description:	<This script will create the new fields in as_documents>
alter table AS_Documents add DocumentPath nvarchar(100) null
update AS_Documents set DocumentPath  = 'C:\AppliancesDocs\Documents\'