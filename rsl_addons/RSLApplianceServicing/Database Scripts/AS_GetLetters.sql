USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetLetters]    Script Date: 12/16/2012 20:57:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetLetters
-- Author:		Hussain Ali
-- Create date: 12/10/2012
-- Description:	<This Stored Proceedure shows the Standard Letters on the Resources Page , Letters Listing>
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetLetters]
	@StatusID	int = -1,
	@ActionID	int = -1,
	@Title		varchar(max) = null,
	@Code		varchar(max) = null
AS
BEGIN

	-- Declaring the variables to be used in this query
	DECLARE @selectClause	varchar(8000),
			@fromClause		varchar(8000),
			@whereClause	varchar(8000),
			@query			varchar(8000)	        
			
	-- Initalizing the variables to be used in this query
	SET @selectClause	= 'SELECT 
								AS_StandardLetters.StandardLetterId,
								AS_StandardLetters.Code, 
								AS_Status.Title as StatusTitle, 
								AS_Action.Title as ActionTitle, 
								AS_StandardLetters.Title, 
								AS_StandardLetters.IsActive,
								CONVERT(varchar(10), AS_StandardLetters.ModifiedDate, 103) as ModifiedDate,
								ISNULL(E__EMPLOYEE.FIRSTNAME,'''') + ''' + ' ' + ''' + ISNULL(E__EMPLOYEE.LASTNAME,'''') as Name'
	
	SET @fromClause		= 'FROM 
								AS_StandardLetters INNER JOIN
								AS_Status ON AS_Status.StatusId=AS_StandardLetters.StatusId INNER JOIN 
								AS_Action ON AS_Action.ActionId=AS_StandardLetters.ActionId INNER JOIN
								E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = AS_StandardLetters.ModifiedBy'
	
	SET @whereClause	= 'WHERE'
	
	-- Adding white spaces between the elements
	SET @selectClause	= @selectClause + CHAR(10)
	SET @fromClause		= @fromClause	+ CHAR(10)
	SET @whereClause	= @whereClause  + CHAR(10)	
	
	-- Filling in the where classes
	
	if (@StatusID <> -1)
	BEGIN
		SET @whereClause = @whereClause + 'AS_StandardLetters.StatusId = ' + CONVERT(varchar, @StatusID) + ' AND' + CHAR(10)
 	END
 	
 	if (@ActionID != -1)
	BEGIN
		SET @whereClause = @whereClause + 'AS_StandardLetters.ActionId = ' + CONVERT(varchar, @ActionID) + ' AND' + CHAR(10)
 	END
 	
 	if (@Title IS NOT NULL)
	BEGIN
		SET @whereClause = @whereClause + 'AS_StandardLetters.Title LIKE ''%' + @Title + '%'' AND' + CHAR(10)
 	END
 	
 	if (@Code IS NOT NULL)
	BEGIN
		SET @whereClause = @whereClause + 'AS_StandardLetters.Code LIKE ''%' + @Code + '%'' AND' + CHAR(10)
 	END
	
	SET @whereClause = @whereClause + 'AS_StandardLetters.IsActive = 1' + CHAR(10)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Building the Query	
	SET @query = @selectClause + @fromClause
	
	--if ((@StatusID != -1) OR (@ActionID != -1) OR (@Title IS NOT NULL) OR (@Code IS NOT NULL))
	--Begin
		SET @query = @query + @whereClause
	--End	
	
	-- Printing the query for debugging
	print @query 
	
	-- Executing the query
    EXEC (@query)
	
END
