-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Hussain Ali
-- Create date: 15/10/2012
-- Description:	This SP displays all the Teams that are active
-- Useage: View Letter
-- EXEC: [dbo].[AS_GetTeams]
-- =============================================

CREATE PROCEDURE [dbo].[AS_GetTeams]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		E_Team.TEAMID, 
		E_Team.TEAMNAME 
	FROM
		E_Team 
	WHERE 
		E_TEAM.ACTIVE = 1
END
GO
