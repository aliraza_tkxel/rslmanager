USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SaveUser]    Script Date: 12/16/2012 22:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,27/09/2012>
-- Description:	<Description,,Save Users when we add on the screen add user from Resources Page>
-- Web Page: Resources.aspx
-- =============================================
ALTER PROCEDURE [dbo].[AS_SaveUser]
	@employeeId int,
	@userTypeId int,
	@createdBy int,
	@SignatureFileName varchar(250),
	@userCount int out
AS
BEGIN
	Declare @UserId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	
	UPDATE E__EMPLOYEE 
	SET E__EMPLOYEE.SIGNATUREPATH =@SignatureFileName 
	WHERE E__EMPLOYEE.EMPLOYEEID = @employeeId
	
	SELECT @userCount = COUNT(*) FROM AS_USER WHere EmployeeId =@employeeId 
	if @userCount = 0
		BEGIN
		INSERT INTO AS_USER (UserTypeID,EmployeeId,IsActive,CreatedDate,CreatedBy) VALUES 
							(@userTypeId,@employeeId,1,GETDATE(),@createdBy)
		END
	ELSE
		BEGIN
				SELECT @userCount = COUNT(*) FROM AS_USER WHere EmployeeId =@employeeId AND IsActive = 0
				IF @userCount > 0
					BEGIN
						SET @userCount = 0
						 Update AS_USER Set IsActive = 1,UserTypeID=@userTypeId Where EmployeeId = @employeeId
					END
				ELSE
					BEGIN
						SET @userCount = 1	
					END	
		END
 END
