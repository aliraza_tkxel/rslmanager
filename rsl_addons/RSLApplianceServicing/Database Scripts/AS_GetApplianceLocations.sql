-- =============================================    
-- EXEC AS_GetApplianceLocations    
-- Author:  <Author,,Noor Muhammad>    
-- Create date: <Create Date,,05/11/2012>    
-- Description: <Description,,Get all in house locations of property>    
-- Web Page: PropertyRecrod.aspx    
-- Control Page: Appliance.ascx    
-- =============================================    
ALTER PROCEDURE [dbo].[AS_GetApplianceLocations]    
(@prefix varchar(100) )  
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
 SELECT LocationID as id , Location as title     
 From GS_LOCATION where LOCATION like'%'+@prefix+'%'  order by LocationID ASC     
END 