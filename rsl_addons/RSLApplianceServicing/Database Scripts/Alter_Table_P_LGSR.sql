-- Author:		<Behroz Sikander>
-- Create date: <18 December 2012>
-- Description:	<This script will create the new fields in P_LGSR>
ALTER TABLE [dbo].[P_LGSR] add [ReceivedOnBehalf] [varchar](20) NULL
ALTER TABLE [dbo].[P_LGSR] add [ReceivedDate] [smalldatetime] NULL
ALTER TABLE [dbo].[P_LGSR] add [TENANTHANDED] [bit] NULL
