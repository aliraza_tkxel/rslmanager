USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_MSATDetail]    Script Date: 03/19/2015 16:58:46 ******/
CREATE TYPE [dbo].[AS_MSATDetail] AS TABLE(
	[IsRequired] [BIT] NULL,
	[LastDate] [DATETIME] NULL,
	[Cycle] [INT] NULL,
	[CycleTypeId] [INT] NULL,
	[NextDate] [DATETIME] NULL,
	[AnnualApportionment] [FLOAT] NULL,
	[MSATTypeId] [INT] NULL
)
GO


