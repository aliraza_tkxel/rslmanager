USE [RSLBHALive1]
GO

/****** Object:  Table [dbo].[P_Documents]    Script Date: 12/23/2013 12:28:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[P_Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [varchar](50) NULL,
	[DocumentName] [varchar](50) NULL,
	[DocumentPath] [varchar](50) NULL,
	[DocumentType] [varchar](50) NULL,
	[Keywords] [varchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[DocumentSize] [varchar](50) NULL,
	[DocumentFormat] [varchar](50) NULL,
 CONSTRAINT [PK_P_Documents] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[P_Documents] ADD  CONSTRAINT [DF_P_Documents_createdDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

