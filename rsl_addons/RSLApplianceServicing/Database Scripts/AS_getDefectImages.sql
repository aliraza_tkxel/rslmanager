-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_getDefectImages @propertyDefectId = 1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/16/2012>
-- Description:	<Description,,get all defect images>
-- WebPage: PropertyRecord.aspx
-- =============================================
CREATE PROCEDURE AS_getDefectImages
	@propertyDefectId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
	WHERE PropertyDefectId = @propertyDefectId
END
GO
