USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
  
-- EXEC P_GetRSLModulesList  
-- @USERID =34,  
-- @OrderASC = 1  
-- Author:  <Ahmed Mehmood>  
-- Create date: <21/06/2013>  
-- Description: <Returns user specific module list in Property Managment Module>  
-- Web Page: PropertyModuleMaster.master  
-- Modified: <Aamir Waheed, 10/02/2014>   
  
-- =============================================  
  
ALTER PROCEDURE [dbo].[P_GetRSLModulesList](  
@USERID INT,  
@OrderASC INT  
)  
AS

/* MODULEID  
-- ModuleId = 16 --Proerty/PropertyModule  
*/

SET NOCOUNT ON
IF @OrderASC = 1 BEGIN
SELECT DISTINCT
	M.MODULEID							AS MODULEID
	,DESCRIPTION						AS [DESCRIPTION]
	,'/' + M.DIRECTORY + '/' + M.PAGE	AS THEPATH
	,[IMAGE]							AS [IMAGE]
	,IMAGEOPEN							AS IMAGEOPEN
	,IMAGEMOUSEOVER						AS IMAGEMOUSEOVER
	,IMAGETAG							AS IMAGETAG
	,IMAGETITLE							AS IMAGETITLE
	,ORDERTEXT							AS ORDERTEXT
FROM
	AC_MODULES M
		INNER JOIN AC_MODULES_ACCESS ON AC_MODULES_ACCESS.ModuleId = M.MODULEID
		INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
WHERE
	(M.ACTIVE = 1 AND DISPLAY = 1 AND EMPLOYEEID = @USERID)
	OR (ALLACCESS = 1 AND DISPLAY = 1) UNION ALL SELECT
	0												AS MODULEID
	,'Whiteboard'									AS [DESCRIPTION]
	,'/BHAIntranet/custompages/MyWhiteboard.aspx'	AS THEPATH
	,''												AS [IMAGE]
	,''												AS IMAGEOPEN
	,''												AS IMAGEMOUSEOVER
	,''												AS IMAGETAG
	,''												AS IMAGETITLE
	,'1'											AS ORDERTEXT

ORDER BY ORDERTEXT ASC
END ELSE BEGIN
SELECT DISTINCT
	M.MODULEID							AS MODULEID
	,DESCRIPTION						AS [DESCRIPTION]
	,'/' + M.DIRECTORY + '/' + M.PAGE	AS THEPATH
	,[IMAGE]							AS [IMAGE]
	,IMAGEOPEN							AS IMAGEOPEN
	,IMAGEMOUSEOVER						AS IMAGEMOUSEOVER
	,IMAGETAG							AS IMAGETAG
	,IMAGETITLE							AS IMAGETITLE
	,ORDERTEXT							AS ORDERTEXT
FROM
	AC_MODULES M
		INNER JOIN AC_MODULES_ACCESS ON AC_MODULES_ACCESS.ModuleId = M.MODULEID
		INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
WHERE
	(M.ACTIVE = 1 AND DISPLAY = 1 AND EMPLOYEEID = @USERID)
	OR (ALLACCESS = 1 AND DISPLAY = 1) UNION ALL SELECT
	0												AS MODULEID
	,'Whiteboard'									AS [DESCRIPTION]
	,'/BHAIntranet/custompages/MyWhiteboard.aspx'	AS THEPATH
	,''												AS [IMAGE]
	,''												AS IMAGEOPEN
	,''												AS IMAGEMOUSEOVER
	,''												AS IMAGETAG
	,''												AS IMAGETITLE
	,'0'											AS ORDERTEXT

ORDER BY ORDERTEXT DESC
END