USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_ScheduledAppointmentsDetail]    Script Date: 06/17/2013 15:53:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Aqib Javed
-- Create date:      23/09/2012
-- Description:      Performance Summary
-- History:          23/09/2012 Aqib :This Stored Proceedure shows the Appointment details on the Calendar Page
--                   13/07/2015 Ali Raza : Added All void appointments union
-- EXEC AS_ScheduledAppointmentsDetail @startDate='31/12/2013',@endDate='04/01/2013'
-- =============================================
ALTER PROCEDURE [dbo].[AS_ScheduledAppointmentsDetail]
@startDate date,
@endDate date	
AS
BEGIN

	-----------------------------------SELECT Gas / Appliance Servicing Appointments -----------
	--------------------------------------------------------------------------------------------------------
	SELECT DISTINCT 
		AS_APPOINTMENTS.APPOINTMENTSHIFT AS APPOINTMENTSHIFT,
		CONVERT(varchar, APPOINTMENTDATE,103) as AppointmentDate, 
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
		AS_APPOINTMENTS.ASSIGNEDTO AS ASSIGNEDTO,
		P__PROPERTY.POSTCODE AS PostCode,
		AS_Status.Title AS Title,
		AS_APPOINTMENTS.APPOINTMENTSTARTTIME AS APPOINTMENTSTARTTIME,
		AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENTENDTIME,
		AS_APPOINTMENTS.APPOINTMENTID AS APPOINTMENTID,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,CONVERT(varchar, APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Gas' as AppointmentType,
		C_TENANCY.TENANCYID AS TenancyId,
		P_STATUS.[DESCRIPTION] AS PropertyStatus


		FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		WHERE AS_JOURNAL.IsCurrent = 1
		AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate and @endDate
		-- Added Condition to filter only the appointment of Active gas operatives
		AND AS_APPOINTMENTS.ASSIGNEDTO IN ( SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1 )
	
	-----------------------------------UNION SELECT Reactive Repair Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
    
	SELECT DISTINCT substring(Time,6,len(Time)) as APPOINTMENTSHIFT
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as AppointmentDate
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') 
				+' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,FL_CO_APPOINTMENT.OperativeID as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,FL_FAULT_STATUS.[Description] as Title
		,FL_CO_APPOINTMENT.[Time] as APPOINTMENTSTARTTIME
		,FL_CO_APPOINTMENT.EndTime as APPOINTMENTENDTIME
		,FL_CO_APPOINTMENT.AppointmentID
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + [Time],103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Reactive Repair' as AppointmentType	
		,C_TENANCY.TENANCYID
		,P_STATUS.[DESCRIPTION] 
		from FL_CO_APPOINTMENT 
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId 
		INNER JOIN	FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
		INNER JOIN	FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID  = FL_FAULT_STATUS.FaultStatusID 
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID=P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		
	WHERE 

		FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND FL_FAULT_STATUS.[Description]!='Cancelled'
		-- Added Condition to filter only the appointment of Active gas operatives
		AND FL_CO_APPOINTMENT.OPERATIVEID IN (SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1)
		
	
	-----------------------------------UNION SELECT Stock ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
	
	SELECT 
		RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),2) as APPOINTMENTSHIFT
		,PS_Appointment.AppointStartDateTime as AppointmentDate		
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,PS_Appointment.CreatedBy as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,'' as Title
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),8) as APPOINTMENTSTARTTIME
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointEndDateTime,100),8) as APPOINTMENTENDTIME		
		,PS_Appointment.AppointId as APPOINTMENTID
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as EndTimeInSec	
		,CONVERT(varchar(20), AppointStartDateTime,103) as ValidateAppointmentDate	
		,PS_Appointment.AppointType as AppointmentType
		,C_TENANCY.TENANCYID  	
		,P_STATUS.[DESCRIPTION]  
	FROM PS_Appointment 
		INNER JOIN PS_Property2Appointment ON PS_Appointment.AppointId = PS_Property2Appointment.AppointId		
		INNER JOIN P__PROPERTY ON PS_Property2Appointment.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	
	
	WHERE 
		(PS_Appointment.AppointType = 'Stock')	
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		-- Added Condition to filter only the appointment of Active gas operatives
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND PS_Appointment.SurveyourUserName IN (	SELECT DISTINCT PS_Appointment.SurveyourUserName 
													FROM PS_Appointment 
														INNER JOIN AC_LOGINS ON PS_Appointment.SurveyourUserName = AC_LOGINS.LOGIN 
														INNER JOIN AS_USER ON AC_LOGINS.EMPLOYEEID = AS_USER.EMPLOYEEID 
													WHERE AS_USER.UserTypeID = 3 AND AS_USER.IsActive  = 1 )
													
		
		
		-----------------------------------UNION SELECT  Void Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL	
		
		SELECT DISTINCT 
		RIGHT(Convert(VARCHAR(20), convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103),100),2) as APPOINTMENTSHIFT,
		CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103) as AppointmentDate, 
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
		PDR_APPOINTMENTS.ASSIGNEDTO AS ASSIGNEDTO,
		P__PROPERTY.POSTCODE AS PostCode,
		PDR_Status.TITLE AS Title,
		PDR_APPOINTMENTS.APPOINTMENTSTARTTIME AS APPOINTMENTSTARTTIME,
		PDR_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENTENDTIME,
		PDR_APPOINTMENTS.APPOINTMENTID AS APPOINTMENTID,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTENDDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,CONVERT(varchar, APPOINTMENTSTARTDATE,103) as ValidateAppointmentDate
		,PDR_MSATTYPE.MSATTypeName as AppointmentType,
		C_TENANCY.TENANCYID AS TenancyId,
		P_STATUS.[DESCRIPTION] AS PropertyStatus

		
		       FROM     PDR_APPOINTMENTS
                                INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
                                INNER JOIN	PDR_Status ON PDR_JOURNAL.StatusId = PDR_Status.StatusId AND PDR_STATUS.TITLE Not In ('Cancelled','To be Arranged')
								INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID
								INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
								LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
								LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
								LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
								LEFT JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
								INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID
                                --LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                --                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                --                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                --                           )
                                --LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                --                              AND CG.ID IN (
                                --                              SELECT
                                --                              MAX(ID) ID
                                --                              FROM
                                --                              C_CUSTOMER_NAMES_GROUPED
                                --                              GROUP BY I )
                       WHERE    		PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PDR_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE <= CONVERT(DATE, @startDate, 103) AND PDR_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103))
       
		
		
		-- Order By Caluse for all record from UNION(S) ALL
		ORDER BY StartTimeInSec ASC
	
	
END
