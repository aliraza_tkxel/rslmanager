SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC: [dbo].[AS_GetStandardLetterByID]
--			@StandardLetterID=1
-- Author:		Hussain Ali
-- Create date: 15/10/2012
-- Description:	This SP returns a standard letter based on an ID
-- Useage: view Letter
-- =============================================
ALTER PROCEDURE [dbo].[AS_GetStandardLetterByID]
	-- Add the parameters for the stored procedure here
	@StandardLetterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		AS_StandardLetters.StandardLetterId,
		AS_StandardLetters.StatusId,
		AS_StandardLetters.ActionId,
		AS_StandardLetters.Title,
		AS_StandardLetters.Code,
		AS_StandardLetters.Body
	FROM
		AS_StandardLetters
	WHERE
		AS_StandardLetters.StandardLetterId=@StandardLetterID
			
END
GO
