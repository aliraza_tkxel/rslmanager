-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC AS_GetParametersByAreaId @itemId =1
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,11/13/2012>
-- Description:	<Description,,get the details of the Tree leaf Node>
-- WebPage: PropertyRecord.aspx=>Attributes tab
-- =============================================
CREATE PROCEDURE AS_GetParametersByAreaId (
@areaId int,
@itemId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT ItemId,PA_PARAMETER.*
  FROM PA_ITEM_PARAMETER INNER JOIN
  PA_PARAMETER on PA_PARAMETER.ParameterID = PA_ITEM_PARAMETER.ParameterId 
  WHERE  PA_ITEM_PARAMETER.ItemId = @areaId 
END
GO
