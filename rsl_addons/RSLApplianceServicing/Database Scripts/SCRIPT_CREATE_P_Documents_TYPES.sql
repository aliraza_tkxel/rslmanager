USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_Documents_SubType]    Script Date: 08/11/2014 17:16:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[P_Documents_SubType](
	[DocumentSubtypeId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentTypeId] [int] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK__P_Docume__DD7012646023A6E2] PRIMARY KEY CLUSTERED 
(
	[DocumentSubtypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[P_Documents_SubType]  WITH NOCHECK ADD  CONSTRAINT [p_documents_subtype_parent_FK] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[P_Documents_Type] ([DocumentTypeId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[P_Documents_SubType] CHECK CONSTRAINT [p_documents_subtype_parent_FK]
GO

ALTER TABLE [dbo].[P_Documents_SubType] ADD  CONSTRAINT [DF_P_Documents_Subtype_active]  DEFAULT ((1)) FOR [Active]
GO

ALTER TABLE [dbo].[P_Documents_SubType] ADD  CONSTRAINT [DF_P_Documents_Subtype_order]  DEFAULT ((1000)) FOR [Order]
GO



CREATE TABLE [dbo].[P_Documents_Type](
	[DocumentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK__P_Docume__DD7012645C5315FE] PRIMARY KEY CLUSTERED 
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[P_Documents_Type] ADD  CONSTRAINT [DF_P_Documents_Type_active]  DEFAULT ((1)) FOR [Active]
GO

ALTER TABLE [dbo].[P_Documents_Type] ADD  CONSTRAINT [DF_P_Documents_Type_order]  DEFAULT ((1000)) FOR [Order]
GO



