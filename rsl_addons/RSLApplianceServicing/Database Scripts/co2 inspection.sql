USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_CO2_Inspection]    Script Date: 05/21/2013 15:13:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[P_CO2_Inspection](
	[DectectorId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](20) NOT NULL,
	[DetectorTest] [nvarchar](10),
	[DateStamp] [smalldatetime] NOT NULL,
	[Journalid] [int] NULL,
 CONSTRAINT [PK_P_CO2_Inspection] PRIMARY KEY CLUSTERED 
(
	[DectectorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[P_CO2_Inspection]  WITH CHECK ADD  CONSTRAINT [FK_P_CO2_InspectionP__PROPERTY] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
GO


