USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[AS_LookupType]    Script Date: 11/07/2012 12:47:53 ******/
SET IDENTITY_INSERT [dbo].[AS_LookupType] ON
INSERT [dbo].[AS_LookupType] ([LookupTypeId], [TypeName], [Description]) VALUES (2, N'SignOff', N'Yours faithfully, Yours Sincerely, Kind regards')
SET IDENTITY_INSERT [dbo].[AS_LookupType] OFF
/****** Object:  Table [dbo].[AS_LookupCode]    Script Date: 11/07/2012 12:47:53 ******/
SET IDENTITY_INSERT [dbo].[AS_LookupCode] ON
INSERT [dbo].[AS_LookupCode] ([LookupCodeId], [LookupTypeId], [CodeName], [Code], [CreatedBy], [CreatedDate]) VALUES (2, 2, N'Yours faithfully', NULL, NULL, NULL)
INSERT [dbo].[AS_LookupCode] ([LookupCodeId], [LookupTypeId], [CodeName], [Code], [CreatedBy], [CreatedDate]) VALUES (4, 2, N'Yours Sincerely', NULL, NULL, NULL)
INSERT [dbo].[AS_LookupCode] ([LookupCodeId], [LookupTypeId], [CodeName], [Code], [CreatedBy], [CreatedDate]) VALUES (6, 2, N' Kind regards', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[AS_LookupCode] OFF
