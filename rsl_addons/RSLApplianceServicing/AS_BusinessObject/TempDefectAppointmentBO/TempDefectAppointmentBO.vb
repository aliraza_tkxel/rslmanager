﻿Imports System.Globalization

Public Class TempDefectAppointmentBO

    Public Property DueDate As Date

    Public Property TotalDuration As Double

    Public Property TradeId As Integer

    Public Property IsPatchSelected As Boolean

    Public Property IsOperativeCheckSelected As Boolean

    Public Property DisplayCount As Integer

    Public Property CompleteDueDate As Date

    Public Property OperativeId As Integer = -1
    Public Property OperativeName As String = String.Empty

    Public Property IsFaultOftec As Boolean = False

    Public Property IsFaultGasSafe As Boolean = False
    Public Property StartSelectedDate As Date = Now.Date
    Public tempDefectDtBo As TempDefectDtBO = New TempDefectDtBO
    Public tempAppointmentDtBo As TempAppointmentDtBO = New TempAppointmentDtBO

#Region "add Row In Appointment List"

    Public Sub addRowInAppointmentList(ByVal operativeId As Integer, ByVal operativeName As String, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date, ByVal patch As String, ByVal distanceFrom As Double, Optional ByVal distanceTo As Double = 0)

        tempAppointmentDtBo.operativeId = operativeId
        tempAppointmentDtBo.operative = operativeName
        tempAppointmentDtBo.startDateTime = convertDateToCustomString(appointmentStartDateTime)
        tempAppointmentDtBo.patch = patch
        tempAppointmentDtBo.endDateTime = convertDateToCustomString(appointmentEndDateTime)
        tempAppointmentDtBo.appointmentStartDate = appointmentStartDateTime
        tempAppointmentDtBo.appointmentEndDate = appointmentEndDateTime
        tempAppointmentDtBo.distanceFrom = distanceFrom
        tempAppointmentDtBo.distanceTo = distanceTo
        tempAppointmentDtBo.lastColumn = "--"
        tempAppointmentDtBo.lastAdded = True
        tempAppointmentDtBo.addNewDataRow()

    End Sub

#End Region

#Region "convert Date Time In to Date Time String"

    ''' <summary>
    ''' This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
    ''' </summary>
    ''' <param name="aDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function convertDateToCustomString(ByVal aDate As Date)
        Return (aDate.ToString("HH:mm dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")))
    End Function

#End Region

End Class