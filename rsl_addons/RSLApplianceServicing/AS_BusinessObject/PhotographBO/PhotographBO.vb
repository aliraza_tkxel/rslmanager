﻿Imports System

Public Class PhotographBO

#Region "Attributes"
    Private _propertyId As String
    Private _title As String
    Private _uploadDate As DateTime
    Private _imageName As String
    Private _filePath As String
    Private _createdBy As Integer
    Private _itemId As Integer
    Private _isDefaultImage As Boolean
#End Region

#Region "Construtor"

    Public Sub New()
        _propertyId = String.Empty
        _title = String.Empty
        _uploadDate = DateTime.Now
        _imageName = String.Empty
        _filePath = String.Empty
        _createdBy = 0
        _itemId = 0
        _isDefaultImage = False
    End Sub

#End Region

#Region "Properties"
    ' Get / Set property for _propertyId
    Public Property PropertyId() As String

        Get
            Return _propertyId
        End Get

        Set(ByVal value As String)
            _propertyId = value
        End Set

    End Property
    ' Get / Set property for _title
    Public Property Title() As String

        Get
            Return _title
        End Get

        Set(ByVal value As String)
            _title = value
        End Set

    End Property
    ' Get / Set property for _uploadDate
    Public Property UploadDate() As DateTime

        Get
            Return _uploadDate
        End Get

        Set(ByVal value As DateTime)
            _uploadDate = value
        End Set

    End Property
    ' Get / Set property for _imageName
    Public Property ImageName() As String

        Get
            Return _imageName
        End Get

        Set(ByVal value As String)
            _imageName = value
        End Set

    End Property
    ' Get / Set property for _filePath
    Public Property FilePath() As String

        Get
            Return _filePath
        End Get

        Set(ByVal value As String)
            _filePath = value
        End Set

    End Property
    ' Get / Set property for _createdBy
    Public Property CreatedBy() As Integer

        Get
            Return _createdBy
        End Get

        Set(ByVal value As Integer)
            _createdBy = value
        End Set

    End Property
    ' Get / Set property for _itemId
    Public Property ItemId() As Integer

        Get
            Return _itemId
        End Get

        Set(ByVal value As Integer)
            _itemId = value
        End Set

    End Property

    ' Get / Set property for _isDefaultImage
    Public Property IsDefaultImage() As Boolean

        Get
            Return _isDefaultImage
        End Get

        Set(ByVal value As Boolean)
            _isDefaultImage = value
        End Set

    End Property
    Public Property HeatingMappingId As Nullable(Of Integer)
#End Region


End Class
