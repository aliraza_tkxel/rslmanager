﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators

Namespace AS_BusinessObject
    Public Class ChangeDefectStatus : Inherits BaseBO
        Private _defectId As Integer
        Private _userId As Integer
        Private _rejectionReasonId As Integer?
        Private _rejectionReasonNotes As String
        Private _status As String

        Public Sub New()
            _defectId = 0
            _userId = 0
            _rejectionReasonId = 0
            _rejectionReasonNotes = String.Empty
            _status = String.Empty


        End Sub

        Public Property DefectId() As Integer

            Get
                Return _defectId
            End Get

            Set(ByVal value As Integer)
                _defectId = value
            End Set

        End Property

        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property
        <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="Please select reason.")>
        Public Property RejectionReasonId() As Integer

            Get
                Return _rejectionReasonId
            End Get

            Set(ByVal value As Integer)
                _rejectionReasonId = value
            End Set

        End Property
        <StringLengthValidator(1, 4000, MessageTemplate:="Please enter rejection notes.")> _
         Public Property RejectionReasonNotes() As String

            Get
                Return _rejectionReasonNotes
            End Get

            Set(ByVal value As String)
                _rejectionReasonNotes = value
            End Set

        End Property

        Public Property Status() As String

            Get
                Return _status
            End Get

            Set(ByVal value As String)
                _status = value
            End Set

        End Property

    End Class
End Namespace
