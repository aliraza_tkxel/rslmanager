﻿Imports System
Public Class LetterBO

#Region "Attributes"

    Private _letterID As Integer
    Private _statusID As Integer
    Private _actionID As Integer
    Private _title As String
    Private _code As String
    Private _isAlternativeServicing As Boolean
    Private _body As String
    Private _createdBy As Integer
    Private _modifiedBy As Integer

#End Region

#Region "Construtor"

    Public Sub New(ByVal statusId As Integer, ByVal actionId As Integer, ByVal title As String, ByVal code As String, ByVal isAlternativeServicing As Boolean, ByVal body As String, ByVal createdBy As Integer, ByVal modifiedBy As Integer)
        _statusID = statusId
        _actionID = actionId
        _title = title
        _code = code
        _isAlternativeServicing = isAlternativeServicing
        _body = body
        _createdBy = createdBy
        _modifiedBy = modifiedBy
    End Sub

    Public Sub New(ByVal letterId As Integer, ByVal statusId As Integer, ByVal actionId As Integer, ByVal title As String, ByVal code As String, ByVal isAlternativeServicing As Boolean, ByVal body As String, ByVal createdBy As Integer, ByVal modifiedBy As Integer)
        _letterID = letterId
        _statusID = statusId
        _actionID = actionId
        _title = title
        _code = code
        _isAlternativeServicing = isAlternativeServicing
        _body = body
        _createdBy = createdBy
        _modifiedBy = modifiedBy
    End Sub

#End Region

#Region "Properties"

    Public Property LetterID() As Integer
        Get
            Return _letterID
        End Get
        Set(ByVal value As Integer)
            _letterID = value
        End Set
    End Property

    Public Property StatusID() As Integer
        Get
            Return _statusID
        End Get
        Set(ByVal value As Integer)
            _statusID = value
        End Set
    End Property

    Public Property ActionID() As Integer
        Get
            Return _actionID
        End Get
        Set(ByVal value As Integer)
            _actionID = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    Public Property IsAlternativeServicing() As String
        Get
            Return _isAlternativeServicing
        End Get
        Set(ByVal value As String)
            _isAlternativeServicing = value
        End Set
    End Property

    Public Property Body() As String
        Get
            Return _body
        End Get
        Set(ByVal value As String)
            _body = value
        End Set
    End Property

    Public Property CreatedBy() As Integer
        Get
            Return _createdBy
        End Get
        Set(ByVal value As Integer)
            _createdBy = value
        End Set
    End Property

    Public Property ModifiedBy() As Integer
        Get
            Return _modifiedBy
        End Get
        Set(ByVal value As Integer)
            _modifiedBy = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
