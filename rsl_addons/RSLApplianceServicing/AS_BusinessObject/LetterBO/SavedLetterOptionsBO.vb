﻿Imports System

Public Class SavedLetterOptionsBO

#Region "Attributes"

    Private _rentCharge As Integer
    Private _currentRentBalance As String
    Private _todaysDate As Date

#End Region

#Region "Construtor"

    Public Sub New(ByVal rentCharge As Integer, ByVal currentRentBalance As Integer, ByVal todaysDate As Date)
        _rentCharge = rentCharge
        _currentRentBalance = currentRentBalance
        _todaysDate = todaysDate
    End Sub

#End Region

#Region "Properties"

    Public Property RentCharge() As Integer
        Get
            Return _rentCharge
        End Get
        Set(ByVal value As Integer)
            _rentCharge = value
        End Set
    End Property

    Public Property CurrentRentBalance() As Integer
        Get
            Return _currentRentBalance
        End Get
        Set(ByVal value As Integer)
            _currentRentBalance = value
        End Set
    End Property

    Public Property TodaysDate() As Date
        Get
            Return _todaysDate
        End Get
        Set(ByVal value As Date)
            _todaysDate = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
