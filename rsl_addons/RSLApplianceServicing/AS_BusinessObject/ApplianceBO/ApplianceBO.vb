﻿Public Class ApplianceBO
    Private _propertyId As String
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property
    Private _existingApplianceId As Integer
    Public Property ExistingApplianceId() As Integer
        Get
            Return _existingApplianceId
        End Get
        Set(ByVal value As Integer)
            _existingApplianceId = value
        End Set
    End Property
    Private _location As String
    Public Property Location() As String
        Get
            Return _location
        End Get
        Set(ByVal value As String)
            _location = value
        End Set
    End Property
    Private _locationId As Integer
    Public Property LocationId() As Integer
        Get
            Return _locationId
        End Get
        Set(ByVal value As Integer)
            _locationId = value
        End Set
    End Property

    Private _fuel As String
    Public Property Fuel() As Integer
        Get
            Return _fuel
        End Get
        Set(ByVal value As Integer)
            _fuel = value
        End Set
    End Property
    Private _type As String
    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    Private _typeId As Integer
    Public Property TypeId() As Integer
        Get
            Return _typeId
        End Get
        Set(ByVal value As Integer)
            _typeId = value
        End Set
    End Property

    Private _make As String
    Public Property Make() As String
        Get
            Return _make
        End Get
        Set(ByVal value As String)
            _make = value
        End Set
    End Property

    Private _makeId As Integer
    Public Property MakeId() As Integer
        Get
            Return _makeId
        End Get
        Set(ByVal value As Integer)
            _makeId = value
        End Set
    End Property



    Private _model As String
    Public Property Model() As String
        Get
            Return _model
        End Get
        Set(ByVal value As String)
            _model = value
        End Set
    End Property

    Private _modelId As Integer
    Public Property ModelId() As Integer
        Get
            Return _modelId
        End Get
        Set(ByVal value As Integer)
            _modelId = value
        End Set
    End Property

    Private _flueType As Integer
    Public Property FlueType() As Integer
        Get
            Return _flueType
        End Get
        Set(ByVal value As Integer)
            _flueType = value
        End Set
    End Property

    Private _itemId As Integer
    Public Property ItemId() As Integer
        Get
            Return _itemId
        End Get
        Set(ByVal value As Integer)
            _itemId = value
        End Set
    End Property

    Private _item As String
    Public Property Item() As String
        Get
            Return _item
        End Get
        Set(ByVal value As String)
            _item = value
        End Set
    End Property
    Private _quantity As String
    Public Property Quantity() As String
        Get
            Return _quantity
        End Get
        Set(ByVal value As String)
            _quantity = value
        End Set
    End Property
    Private _dimensions As String
    Public Property Dimensions() As String
        Get
            Return _dimensions
        End Get
        Set(ByVal value As String)
            _dimensions = value
        End Set
    End Property

    Private _lifeSpan As Integer
    Public Property LifeSpan() As Integer
        Get
            Return _lifeSpan
        End Get
        Set(ByVal value As Integer)
            _lifeSpan = value
        End Set
    End Property

    Private _purchaseCost As Decimal
    Public Property PurchaseCost() As Decimal
        Get
            Return _purchaseCost
        End Get
        Set(ByVal value As Decimal)
            _purchaseCost = value
        End Set
    End Property


    Private _datepurchased As Date
    Public Property Datepurchased() As Date
        Get
            Return _datepurchased
        End Get
        Set(ByVal value As Date)
            _datepurchased = value
        End Set
    End Property

    Private _dateRemoved As Date
    Public Property DateRemoved() As Date
        Get
            Return _dateRemoved
        End Get
        Set(ByVal value As Date)
            _dateRemoved = value
        End Set
    End Property


    Private _installed As Date
    Public Property Installed() As Date
        Get
            Return _installed
        End Get
        Set(ByVal value As Date)
            _installed = value
        End Set
    End Property

    Private isLandLordAppliance As Boolean
    Public Property IsLandLoardAppliance() As Boolean
        Get
            Return isLandLordAppliance
        End Get
        Set(ByVal value As Boolean)
            isLandLordAppliance = value
        End Set
    End Property

    'Start - Changes by Aamir Wahed on 07 May 2013
    'To Implement Additional Field in Add Appliance, Serial Number, Gas Council Number, Replacement Due Field(s)

    Private _serialNumber As String
    Public Property SerialNumber() As String
        Get
            Return _serialNumber
        End Get
        Set(ByVal value As String)
            _serialNumber = value
        End Set
    End Property

    Private _notes As String
    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property

    Private _gasCouncilNumber As String
    Public Property GasCouncilNumber() As String
        Get
            Return _gasCouncilNumber
        End Get
        Set(ByVal value As String)
            _gasCouncilNumber = value
        End Set
    End Property

    Private _replacementDue As Date
    Public Property ReplacementDue() As Date
        Get
            Return _replacementDue
        End Get
        Set(ByVal value As Date)
            _replacementDue = value
        End Set
    End Property

    Private _lastReplaced As Date
    Public Property LastReplaced() As Date
        Get
            Return _lastReplaced
        End Get
        Set(ByVal value As Date)
            _lastReplaced = value
        End Set
    End Property

    Public Sub New()
        Me.Location = String.Empty
        Me.Model = String.Empty
        Me.Make = String.Empty
        Me.Type = String.Empty
        Me.PurchaseCost = Nothing


    End Sub

    'End - Changes by Aamir Wahed on 07 May 2013



End Class
Public Class LookUpBO
#Region "Attributes"

    Private _value As Integer
    Private _name As String

#End Region

#Region "Constructors"

    Public Sub New(ByVal val As Integer, ByVal name As String)

        Me.LookUpValue = val
        Me.LookUpName = name

    End Sub

#End Region

#Region "Properties"

    Public Property LookUpValue() As Integer
        Get
            Return _value
        End Get
        Set(ByVal value As Integer)
            _value = value
        End Set
    End Property

    Public Property LookUpName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

#End Region

End Class