﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators

Public Class ServiceRequiredBo

    Public Property WorkDetailId As Integer

    Public Property ServiceRequired As String

    <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Net Cost must be a positive number.")>
    Public Property NetCost As Decimal

    <NotNullValidator(MessageTemplate:="Please Select Vat.")>
    <TypeConversionValidator(GetType(Integer), MessageTemplate:="Please Select Vat.")>
    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="Please Select Vat.")>
    Public Property VatIdDDLValue As Integer

    <TypeConversionValidator(GetType(Decimal), MessageTemplate:="VAT must be a number.")>
    <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="VAT must be a positive number.")>
    Public Property Vat As Decimal

    <TypeConversionValidator(GetType(Decimal), MessageTemplate:="Total must be a number.")>
    <RangeValidator(GetType(Decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate:="Total must be a positive number.")>
    Public Property Total As Decimal

    <TypeConversionValidator(GetType(Integer), MessageTemplate:="You must select a valid expenditure from list.")>
    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="You must select a valid expenditure from list.")>
    Public Property ExpenditureId As Integer

    <TypeConversionValidator(GetType(Integer), MessageTemplate:="You must select a valid Budget Head from list.")>
    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="You must select a valid Budget Head from list.")>
    Public Property BudgetHeadId As Integer

    <TypeConversionValidator(GetType(Integer), MessageTemplate:="You must select a valid Cost Center from list.")>
    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="You must select a valid Cost Center from list.")>
    Public Property CostCenterId As Integer

    Public Property IsPending As Boolean

    Public Property VatRate As Decimal

End Class
