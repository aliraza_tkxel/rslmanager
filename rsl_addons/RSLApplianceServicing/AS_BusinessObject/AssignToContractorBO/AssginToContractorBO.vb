﻿Public Class AssignToContractorBO
#Region "Properties"

    Public Property CostCenterId As Integer
    Public Property BudgetHeadId As Integer
    Public Property ExpenditureId As Integer
    Public Property ContractorId As Integer
    Public Property ContactId As Integer
    Public Property WorksRequired As DataTable
    Public Property Estimate As Decimal
    Public Property EstimateRef As String
    Public Property DefaultNetCost As Double
    Public Property DefaultVatId As Integer
    Public Property DefaultVat As Double
    Public Property DefaultGross As Double



    Public Property AreaId As Integer
    Public Property AttributeId As Integer
    Public Property UserId As Integer
   
    Public Property AssignWorkType As Integer
    Public Property EmpolyeeId As Integer
    Public Property POStatus As Integer

    Public Property SchemeId As Integer
    Public Property BlockId As Integer
    Public Property DefectContractorId As Integer
    Public Property JournalId As Integer

    Public Property PropertyId As String
    Public Property OrderId As Integer
    Public Property ApplianceId As Integer
    Public Property ReportedApplianceAndCategory As String
    Public Property DefectWorksRequired As String
 
    Public Property DefectId As Integer
    Public Property jsn As String
    Public Property PoCurrentStatus As Integer
   
    Public Property RepairWorksRequired As String
    Public Property FuelServicingWorksRequired As String
    Public Property RequestType As String
    Public Property AppointmentType As String
#End Region
End Class