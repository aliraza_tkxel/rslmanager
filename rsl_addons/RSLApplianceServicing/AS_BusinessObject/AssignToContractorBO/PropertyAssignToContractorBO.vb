﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data

Public Class PropertyAssignToContractorBo
    Public Sub New()
        PdrContractorId = -1
        JournalId = -1
        CostCenterId = -1
        BudgetHeadId = -1
        ExpenditureId = -1
        ContractorId = -1
        ContractorName = String.Empty
        ContactId = -1
        ServiceRequired = String.Empty
        LifeCycle = String.Empty
        ServiceDue = String.Empty
        Estimate = 0D
        EstimateRef = String.Empty
        ContractStartDate = String.Empty
        ContractEndDate = String.Empty
        EmpolyeeId = -1
        POStatus = -1
        SchemeId = -1
        BlockId = -1
        UserId = -1
        MsatType = String.Empty
    End Sub

    Public Property PdrContractorId As Integer
    Public Property AttributeTypeId As Integer?
    Public Property ProvisionId As Integer?
    Public Property ItemId As Integer?
    Public Property JournalId As Integer
    Public Property CostCenterId As Integer
    Public Property BudgetHeadId As Integer
    Public Property ExpenditureId As Integer
    Public Property ContractorId As Integer
    Public Property ContractorName As String
    Public Property ContactId As Integer
    Public Property POName As String
    Public Property ServiceRequired As String
    Public Property LifeCycle As String
    Public Property ServiceDue As String
    Public Property Estimate As Decimal
    Public Property EstimateRef As String
    Public Property ContractStartDate As String
    Public Property ContractEndDate As String
    Public Property EmpolyeeId As Integer
    Public Property POStatus As Integer
    Public Property SchemeId As Integer?
    Public Property BlockId As Integer?
    Public Property PropertyId As String
    Public Property ServiceRequiredDt As DataTable
    Public Property UserId As Integer
    Public Property MsatType As String
    Public Property RequiredWorkIds As String
    Public Property PaintPackId As Integer
    Public Property InspectionJournalId As Integer
    Public Property OrderId As Integer
    Public Property IncInSChge As Boolean?
    Public Property PropertyApportionment As Decimal?
End Class

