﻿Imports System


Public Class ReportBO
#Region "Attributes"

    Private _fuelType As Integer
    Private _propertyType As Integer
    Private _patch As Integer
    Private _scheme As Integer
    Private _stage As Integer
    Private _certificateType As Integer
    Private _uprn As String
    Private _startDate As String
    Private _endDate As String
    Private _heatingTypeId As Integer

#End Region

#Region "Construtor"
    Public Sub New()
        _fuelType = 0
        _propertyType = 0
        _patch = 0
        _scheme = 0
        _stage = 0
        _certificateType = 0
        _uprn = String.Empty
        _startDate = String.Empty
        _endDate = String.Empty '
        _heatingTypeId = 0
    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _startDate
    Public Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    ' Get / Set property for _endDate
    Public Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property

    ' Get / Set property for _fuelType
    Public Property FuelType() As Integer

        Get
            Return _fuelType
        End Get

        Set(ByVal value As Integer)
            _fuelType = value
        End Set

    End Property
    ' Get / Set property for _heatingTypeId
    Public Property HeatingType() As Integer

        Get
            Return _heatingTypeId
        End Get

        Set(ByVal value As Integer)
            _heatingTypeId = value
        End Set

    End Property
    ' Get / Set property for _propertyType
    Public Property PropertyType() As Integer

        Get
            Return _propertyType
        End Get

        Set(ByVal value As Integer)
            _propertyType = value
        End Set

    End Property
    ' Get / Set property for _patch
    Public Property Patch() As Integer

        Get
            Return _patch
        End Get

        Set(ByVal value As Integer)
            _patch = value
        End Set

    End Property
    ' Get / Set property for _periodFilter
    Public Property Scheme() As Integer

        Get
            Return _scheme
        End Get

        Set(ByVal value As Integer)
            _scheme = value
        End Set

    End Property
    ' Get / Set property for _stage
    Public Property Stage() As Integer

        Get
            Return _stage
        End Get

        Set(ByVal value As Integer)
            _stage = value
        End Set

    End Property
    ' Get / Set property for _uprn
    Public Property UPRN() As String

        Get
            Return _uprn
        End Get

        Set(ByVal value As String)
            _uprn = value
        End Set

    End Property
    ' Get / Set certificate type
    Public Property CertificateType() As String

        Get
            Return _certificateType
        End Get

        Set(ByVal value As String)
            _certificateType = value
        End Set

    End Property
#End Region
End Class
