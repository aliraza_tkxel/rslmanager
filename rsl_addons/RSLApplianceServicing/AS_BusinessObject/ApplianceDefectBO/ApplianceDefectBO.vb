﻿Imports System

<Serializable()>
Public Class ApplianceDefectBO

#Region "Construtor(s)"

#End Region

#Region "Properties"

    ' Get / Set property for _propertyDefectId
    Public Property PropertyDefectId As Integer = -1

    ' Get / Set property for _propertyId
    Public Property PropertyId() As String = String.Empty

    ' Get / Set property for _requestType
    Public Property RequestType() As String = String.Empty

    ' Get / Set property for _categoryId
    Public Property CategoryId() As Integer = 0

    ' Get / Set property for _categoryId
    Public Property HeatingMappingId() As Integer = 0

    ' Get / Set property for _categoryDescription
    Public Property CategoryDescription() As String = String.Empty

    ' Get / Set property for _defectDate
    Public Property DefectDate() As DateTime = Now

    ' Get / Set property for _defectIdentified
    Public Property IsDefectIdentified() As Boolean = False

    ' Get / Set property for _defectIdentifiedNotes
    Public Property DefectIdentifiedNotes() As String = String.Empty

    ' Get / Set property for _remedialAction
    Public Property IsRemedialActionTaken() As Boolean = False

    ' Get / Set property for _remedialActionNotes
    Public Property RemedialActionNotes() As String = String.Empty

    ' Get / Set property for _isWarningIssued
    Public Property IsWarningNoteIssued() As Boolean = False

    ' Get / Set property for _applianceId
    Public Property ApplianceId() As Integer = 0

    ' Get / Set property for _serialNumber
    Public Property SerialNumber() As String = String.Empty

    ' Get / Set property for _serialNumber
    Public Property GcNumber() As String = String.Empty

    ' Get / Set property for _filePath
    Public Property IsWarningTagFixed() As Boolean = False

    ' Get / Set property for _isApplianceDisconnected
    Public Property IsAppliancedDisconnected As Nullable(Of Boolean) = Nothing

    ' Get / Set property for _isPartsRequired
    Public Property IsPartsRequired As Nullable(Of Boolean) = Nothing

    ' Get / Set property for _isPartsOrdered
    Public Property IsPartsOrdered As Nullable(Of Boolean) = Nothing

    ' Get / Set property for _partsOrderedBy
    Public Property PartsOrderedBy As Nullable(Of Integer) = Nothing

    ' Get / Set property for _partsOrderedBy
    Public Property PartsDueDate As Nullable(Of Date) = Nothing

    ' Get / Set property for _partsDescription
    Public Property PartsDescription() As String = String.Empty

    ' Get / Set property for _partsLocation
    Public Property PartsLocation() As String = String.Empty

    ' Get / Set property for _isTwoPersonsJob
    Public Property IsTwoPersonsJob() As Boolean = False

    ' Get / Set property for _reasonForSecondPerson
    Public Property ReasonForSecondPerson() As String = String.Empty

    ' Get / Set property for _estimatedDuration
    Public Property EstimatedDuration() As Nullable(Of Decimal) = Nothing

    ' Get / Set property for _priorityId
    Public Property PriorityId() As Nullable(Of Integer) = Nothing

    ' Get / Set property for _estimatedDuration
    Public Property TradeId() As Nullable(Of Integer) = Nothing

    ' Get / Set property for _filePath
    Public Property FilePath() As String = String.Empty

    ' Get / Set property for _photoName
    Public Property PhotoName() As String = String.Empty

    ' Get / Set property for _photoNotes
    Public Property PhotosNotes() As String = String.Empty

    ' Get / Set property for _journalId
    Public Property JournalId() As Integer = -1

    ' Get / Set property for _partsOrderedByName
    Public Property PartsOrderedByName As String = String.Empty

    ' Get / Set property for _journalId
    Public Property Trade() As String = String.Empty

    ' Get / Set property for _priorityName
    Public Property PriorityName As String = String.Empty

    ' Get / Set property for _appliance
    Public Property Appliance As String = String.Empty

    ' Get / Set property for _userId
    Public Property UserId As Integer = -1

    ' Get / Set property for _boiler
    Public Property Boiler As String = String.Empty

#End Region

End Class
