﻿Imports System

Public Class ActionBO

#Region "Attributes"

    Private _actionID As Integer
    Private _title As String

#End Region

#Region "Construtor"

    Public Sub New(ByVal id As Integer, ByVal name As String)
        _actionID = id
        _title = name
    End Sub

#End Region

#Region "Properties"


    Public Property ActionID() As Integer
        Get
            Return _actionID
        End Get
        Set(ByVal value As Integer)
            _actionID = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
