﻿

Public Class VatBo
    Inherits DropDownBO

    Public Sub New(ByVal id As Integer, ByVal name As String, ByVal vatRate As Decimal)
        MyBase.New(id, name)
        _vatRate = vatRate
    End Sub

    Private _vatRate As Decimal

    Public ReadOnly Property VatRate As Decimal
        Get
            Return _vatRate
        End Get
    End Property
End Class
