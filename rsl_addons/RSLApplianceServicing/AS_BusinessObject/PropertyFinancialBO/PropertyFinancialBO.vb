﻿Public Class PropertyFinancialBO
#Region "Private"
    Private _rentFrequency As Integer
    Private _nominatingBody As String
    Private _fundingAuthority As Integer
    Private _charge As Integer
    Private _chargeValue As Decimal
    Private _oMVSt As Decimal
    Private _eUV As Decimal
    Private _insuranceValue As Decimal
    Private _oMV As Decimal
    Private _propertyId As String
    Private _userId As Integer
    Private _Yield As Decimal
    Private _chargeValueDate As Nullable(Of DateTime)
    Private _oMVSTDate As Nullable(Of DateTime)
    Private _eUVDate As Nullable(Of DateTime)
    Private _oMVDate As Nullable(Of DateTime)
    Private _insuranceValuedate As Nullable(Of DateTime)
    Private _yieldDate As Nullable(Of DateTime)
    'New Rental Information - 27th July 2015 
    Private _marketRent As Decimal
    Private _affordableRent As Decimal
    Private _socialRent As Decimal
    Private _marketRentDate As Nullable(Of DateTime)
    Private _affordableRentDate As Nullable(Of DateTime)
    Private _socialRentDate As Nullable(Of DateTime)
    'End here
#End Region
#Region "Public Properties"

    'Property Property Rent Frequency 
    Public Property RentFequency() As Integer
        Get
            Return _rentFrequency
        End Get
        Set(value As Integer)
            _rentFrequency = value
        End Set
    End Property
    'Property Property Nominating Body
    Public Property NominatingBody() As String
        Get
            Return _nominatingBody
        End Get
        Set(value As String)
            _nominatingBody = value
        End Set
    End Property
    'Property Property Funding Authority
    Public Property FundingAuthority() As Integer
        Get
            Return _fundingAuthority
        End Get
        Set(value As Integer)
            _fundingAuthority = value
        End Set
    End Property
    'Property Property Charge
    Public Property Charge() As Integer
        Get
            Return _charge
        End Get
        Set(value As Integer)
            _charge = value
        End Set
    End Property
    'Property Property Charge Value
    Public Property ChargeValue() As Decimal
        Get
            Return _chargeValue
        End Get
        Set(value As Decimal)
            _chargeValue = value
        End Set
    End Property
    'Property Property OMVSt
    Public Property OMVSt() As Decimal
        Get
            Return _oMVSt
        End Get
        Set(value As Decimal)
            _oMVSt = value
        End Set
    End Property
    'Property Property EUV
    Public Property EUV() As Decimal
        Get
            Return _eUV
        End Get
        Set(value As Decimal)
            _eUV = value
        End Set
    End Property
    'Property Property Insurance Value
    Public Property InsuranceValue() As Decimal
        Get
            Return _insuranceValue
        End Get
        Set(value As Decimal)
            _insuranceValue = value
        End Set
    End Property
    'Property Property OMV
    Public Property OMV() As Decimal
        Get
            Return _oMV
        End Get
        Set(value As Decimal)
            _oMV = value
        End Set
    End Property
    'User Id
    Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(value As Integer)
            _userId = value
        End Set
    End Property
    'Property Id
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(value As String)
            _propertyId = value
        End Set
    End Property


    Public Property Yield() As Decimal
        Get
            Return _Yield
        End Get
        Set(ByVal value As Decimal)
            _Yield = value
        End Set
    End Property


    Public Property ChargeValueDate() As Nullable(Of DateTime)
        Get
            Return _chargeValueDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _chargeValueDate = value
        End Set
    End Property


    Public Property OMVSTDate() As Nullable(Of DateTime)
        Get
            Return _oMVSTDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _oMVSTDate = value
        End Set
    End Property

    Public Property EUVDate() As Nullable(Of DateTime)
        Get
            Return _eUVDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _eUVDate = value
        End Set
    End Property


    Public Property OMVDate() As Nullable(Of DateTime)
        Get
            Return _oMVDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _oMVDate = value
        End Set
    End Property

    Public Property InsuranceValueDate() As Nullable(Of DateTime)
        Get
            Return _insuranceValuedate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _insuranceValuedate = value
        End Set
    End Property


    Public Property YieldDate() As Nullable(Of DateTime)
        Get
            Return _yieldDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _yieldDate = value
        End Set
    End Property

    Public Property MarketRent() As Decimal
        Get
            Return _marketRent
        End Get
        Set(ByVal value As Decimal)
            _marketRent = value
        End Set
    End Property

    Public Property MarketRentDate() As Nullable(Of DateTime)
        Get
            Return _marketRentDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _marketRentDate = value
        End Set
    End Property

    Public Property AffordableRent() As Decimal
        Get
            Return _affordableRent
        End Get
        Set(ByVal value As Decimal)
            _affordableRent = value
        End Set
    End Property

    Public Property AffordableRentDate() As Nullable(Of DateTime)
        Get
            Return _affordableRentDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _affordableRentDate = value
        End Set
    End Property

    Public Property SocialRent() As Decimal
        Get
            Return _socialRent
        End Get
        Set(ByVal value As Decimal)
            _socialRent = value
        End Set
    End Property

    Public Property SocialRentDate() As Nullable(Of DateTime)
        Get
            Return _socialRentDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _socialRentDate = value
        End Set
    End Property
#End Region

    Public Sub New()
        ChargeValueDate = Nothing
        OMVSTDate = Nothing
        EUVDate = Nothing
        OMVDate = Nothing
        InsuranceValueDate = Nothing
        YieldDate = Nothing
        MarketRentDate = Nothing
        AffordableRentDate = Nothing
        SocialRentDate = Nothing
    End Sub
End Class
