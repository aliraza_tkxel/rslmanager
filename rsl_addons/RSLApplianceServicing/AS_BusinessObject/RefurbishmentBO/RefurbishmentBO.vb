﻿Public Class RefurbishmentBO
#Region "Private"
    Private _refurbishmentDate As Date
    Private _notes As String
    Private _propertyId As String
    Private _userId As Integer
#End Region
#Region "Public Properties"
   
    'Property Asbestos Added Date
    Public Property RefurbishmentDate() As Date
        Get
            Return _refurbishmentDate
        End Get
        Set(value As Date)
            _refurbishmentDate = value
        End Set
    End Property
    'Property Asbestos Note
    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(value As String)
            _notes = value
        End Set
    End Property
    'Property Id
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(value As String)
            _propertyId = value
        End Set
    End Property
    'User Id
    Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(value As Integer)
            _userId = value
        End Set
    End Property
#End Region
End Class
