﻿Imports System

Public Class StatusBO

#Region "Attributes"
    
    Private _statusID As Integer
    Private _description As String

#End Region

#Region "Construtor"

    Public Sub New(ByVal id As Integer, ByVal name As String)
        _statusID = id
        _description = name
    End Sub

#End Region

#Region "Properties"


    Public Property StatusID() As Integer
        Get
            Return _statusID
        End Get
        Set(ByVal value As Integer)
            _statusID = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
