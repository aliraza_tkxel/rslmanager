﻿Imports System

Namespace AS_BusinessObject
    Public Class CalendarAppointmentsBO : Inherits BaseBO
#Region "Attributes"
        Private _shift As String
        Private _customername As String
        Private _cellno As String
        Private _address As String
        Private _appointmentdate As DateTime
        'Scheduling Appointments
        Private _empid As Integer
        Private _empname As String
        Private _status As Integer
        'Search Schedule Appointments
        Private _patchid As Integer
        Private _patchname As String
        Private _schemename As String
        Private _schemecode As String
        Private _title As String
        Private _appointmentid As Integer
        Private _appointmentdateonly As DateTime
        ''Monthly Calendar 
        'Private _startdate As DateTime
        'Private _enddate As DateTime
#End Region

#Region "Construtor"

        Public Sub New()
            _shift = String.Empty
            _customername = String.Empty
            _cellno = String.Empty
            _address = String.Empty
            _appointmentdate = Nothing


        End Sub

#End Region

#Region "Appointments Engineers"
        ' Get / Set property for _id
        Public Property EmpolyeeId() As Integer

            Get
                Return _empid
            End Get

            Set(ByVal value As Integer)
                _empid = value
            End Set

        End Property
        ' Get / Set property for _empname
        Public Property EmployeeName() As String

            Get
                Return _empname
            End Get

            Set(ByVal value As String)
                _empname = value
            End Set

        End Property
#End Region

#Region "Appointments Details"
        ' Get / Set property for _shift
        Public Property Shift() As Integer

            Get
                Return _shift
            End Get

            Set(ByVal value As Integer)
                _shift = value
            End Set

        End Property
        ' Get / Set property for _customername
        Public Property CustomerName() As String

            Get
                Return _customername
            End Get

            Set(ByVal value As String)
                _customername = value
            End Set

        End Property
        ' Get / Set property for _cellno
        Public Property CellNo() As String

            Get
                Return _cellno
            End Get

            Set(ByVal value As String)
                _cellno = value
            End Set

        End Property
        ' Get / Set property for _address
        Public Property Address() As String

            Get
                Return _address
            End Get

            Set(ByVal value As String)
                _address = value
            End Set

        End Property
        ' Get / Set property for _appointmentdate
        Public Property AppointmentDate() As DateTime

            Get
                Return _appointmentdate
            End Get

            Set(ByVal value As DateTime)
                _appointmentdate = value
            End Set

        End Property
        ' Get / Set property for _status
        Public Property Status() As Integer

            Get
                Return _status
            End Get

            Set(ByVal value As Integer)
                _status = value
            End Set

        End Property
#End Region

#Region "Search Appointments Details"
        ' Get / Set property for _patchid
        Public Property PatchId() As Integer

            Get
                Return _patchid
            End Get

            Set(ByVal value As Integer)
                _patchid = value
            End Set

        End Property
        ' Get / Set property for _patchname
        Public Property PatchName() As String

            Get
                Return _patchname
            End Get

            Set(ByVal value As String)
                _patchname = value
            End Set

        End Property
        ' Get / Set property for _schemename
        Public Property SchemeName() As String

            Get
                Return _schemename
            End Get

            Set(ByVal value As String)
                _schemename = value
            End Set

        End Property
        ' Get / Set property for _schemecode
        Public Property SchemeCode() As String

            Get
                Return _schemecode
            End Get

            Set(ByVal value As String)
                _schemecode = value
            End Set

        End Property
        ' Get / Set property for _title
        Public Property Title() As String

            Get
                Return _title
            End Get

            Set(ByVal value As String)
                _title = value
            End Set

        End Property
        ' Get / Set property for _appointmentid
        Public Property AppointmentId() As Integer

            Get
                Return _appointmentid
            End Get

            Set(ByVal value As Integer)
                _appointmentid = value
            End Set

        End Property
        ' Get / Set property for _appointmentdateonly
        Public Property AppointmentDateOnly() As DateTime

            Get
                Return _appointmentdateonly
            End Get

            Set(ByVal value As DateTime)
                _appointmentdateonly = value
            End Set

        End Property
        '' Get / Set property for _startdate
        'Public Property StartDate() As DateTime

        '    Get
        '        Return _startdate
        '    End Get

        '    Set(ByVal value As DateTime)
        '        _startdate = value
        '    End Set

        'End Property
        '' Get / Set property for _enddate
        'Public Property EndDate() As DateTime

        '    Get
        '        Return _enddate
        '    End Get

        '    Set(ByVal value As DateTime)
        '        _enddate = value
        '    End Set

        'End Property
#End Region

    End Class
End Namespace
