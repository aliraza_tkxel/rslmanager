﻿Imports System.Globalization

Public Class AlternativeAppointmentDtBO
    Implements IDtBO

#Region "Attributes"


    Private _expiryDate As Date
    Public Property ExpiryDate() As Date
        Get
            Return _expiryDate
        End Get
        Set(ByVal value As Date)
            _expiryDate = value
        End Set
    End Property

    Private _isPatchSelected As Boolean
    Public Property IsPatchSelected() As Boolean
        Get
            Return _isPatchSelected
        End Get
        Set(ByVal value As Boolean)
            _isPatchSelected = value
        End Set
    End Property

    Private _isDateSelected As Boolean
    Public Property IsDateSelected() As Boolean
        Get
            Return _isDateSelected
        End Get
        Set(ByVal value As Boolean)
            _isDateSelected = value
        End Set
    End Property

    Private _displayCount As Integer
    Public Property DisplayCount() As Integer
        Get
            Return _displayCount
        End Get
        Set(ByVal value As Integer)
            _displayCount = value
        End Set
    End Property

    Private _fullExpiryDate As Date
    Public Property FullExpiryDate() As Date
        Get
            Return _fullExpiryDate
        End Get
        Set(ByVal value As Date)
            _fullExpiryDate = value
        End Set
    End Property

    Private _appointmentDuration As String
    Public Property AppointmentDuration() As String
        Get
            Return _appointmentDuration
        End Get
        Set(ByVal value As String)
            _appointmentDuration = value
        End Set
    End Property

    Private _fuelType As String
    Public Property FuelType() As String
        Get
            Return _fuelType
        End Get
        Set(ByVal value As String)
            _fuelType = value
        End Set
    End Property

    Private _appointmentStatus As String
    Public Property AppointmentStatus() As String
        Get
            Return _appointmentStatus
        End Get
        Set(ByVal value As String)
            _appointmentStatus = value
        End Set
    End Property

    Private _FromDate As String
    Public Property FromDate() As String
        Get
            Return _FromDate
        End Get
        Set(ByVal value As String)
            _FromDate = value
        End Set
    End Property

    Private _durationHrs As Double
    Public Property DurationHrs() As Double
        Get
            Return _durationHrs
        End Get
        Set(ByVal value As Double)
            _durationHrs = value
        End Set
    End Property

    Private _durationDays As Double
    Public Property DurationDays() As Double
        Get
            Return _durationDays
        End Get
        Set(ByVal value As Double)
            _durationDays = value
        End Set
    End Property

    Private _propertyId As String
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property


    Public operative As String = String.Empty
    Public patch As String = String.Empty
    Public time As String = String.Empty
    Public appointmentDateString As String = String.Empty
    Public distance As String = String.Empty
    Public lastColumn As String = String.Empty
    Public operativeId As Integer = 0
    Public appointmentdate As Date
    Public appointmentEnddate As Date
    Public aptStartTime As String
    Public aptEndTime As String
    Public appointmentEndDateString As String = String.Empty
    Public dt As DataTable
    Private dr As DataRow

    Public Const FuelTypeColName As String = "Fuel Type:"
    Public Const DurationColName As String = "Duration:"
    Public Const TradeColName As String = "Trade:"
    Public Const StatusColName As String = "Status:"
    Public Const IsPatchColName As String = "Patch:"
    Public Const IsExpiryDateColName As String = "IsExpiryDate"
    Public Const FullExpiryDateColName As String = "Expiry Date" '
    Public Const DurationHrsColName As String = "DurationHrs"
    Public Const DurationDaysColName As String = "DurationDays"
    Public Const PropertyIdColName As String = "PropertyId"

#End Region

#Region "Constructor"
    Sub New()
        Me.createDataTable()
    End Sub
#End Region

#Region "Copy Constructor"
    'The main purpose copy constrctor is to make a new copy of object and 
    'then save to session to avoid effect of changes made after saving in session.
    Sub New(ByVal sourceAppointmentSlotsDtBO As AlternativeAppointmentDtBO)
        dt = sourceAppointmentSlotsDtBO.dt.Copy()
        ExpiryDate = sourceAppointmentSlotsDtBO.ExpiryDate
        IsPatchSelected = sourceAppointmentSlotsDtBO.IsPatchSelected
        IsDateSelected = sourceAppointmentSlotsDtBO.IsDateSelected
        DisplayCount = sourceAppointmentSlotsDtBO.DisplayCount
        FullExpiryDate = sourceAppointmentSlotsDtBO.FullExpiryDate
        AppointmentDuration = sourceAppointmentSlotsDtBO.AppointmentDuration
        FuelType = sourceAppointmentSlotsDtBO.FuelType
        DurationHrs = sourceAppointmentSlotsDtBO.DurationHrs
        DurationDays = sourceAppointmentSlotsDtBO.DurationDays
        PropertyId = sourceAppointmentSlotsDtBO.PropertyId
    End Sub
#End Region

#Region "create Data Table"
    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(FuelTypeColName)
        dt.Columns.Add(DurationColName)
        dt.Columns.Add(TradeColName)
        dt.Columns.Add(StatusColName)
        dt.Columns.Add(IsPatchColName)
        dt.Columns.Add(FullExpiryDateColName)
        dt.Columns.Add(DurationHrsColName)
        dt.Columns.Add(DurationDaysColName)
        dt.Columns.Add(IsExpiryDateColName)
        dt.Columns.Add(PropertyIdColName)
    End Sub
#End Region

#Region "add New Data Row"
    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(FuelTypeColName) = FuelType
        dr(DurationColName) = AppointmentDuration
        dr(TradeColName) = "Renewable Qualified"
        dr(StatusColName) = AppointmentStatus
        dr(IsPatchColName) = IsPatchSelected
        dr(FullExpiryDateColName) = FullExpiryDate
        dr(DurationHrsColName) = DurationHrs
        dr(DurationDaysColName) = DurationDays
        dr(IsExpiryDateColName) = IsDateSelected
        dr(PropertyIdColName) = PropertyId
        dt.Rows.Add(dr)
    End Sub
#End Region

#Region "add Row In Appointment List"
    Public Sub addRowInAppointmentList(ByVal operativeId As Integer, ByVal operativeName As String, ByVal operativePatch As String, ByVal aptStartTime As DateTime, ByVal aptEndTime As DateTime, ByVal apptDate As Date, ByVal apptEndDate As Date, ByVal distance As String)

        Dim calendarStartDate As New DateTime(1970, 1, 1)

        Me.operativeId = operativeId
        Me.operative = operativeName
        Me.patch = operativePatch
        Me.time = aptStartTime.ToString("HH:mm") + " - " + aptEndTime.ToString("HH:mm")
        Me.appointmentDateString = apptDate.ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"))
        Me.appointmentdate = CType(apptDate.ToLongDateString() + " " + aptStartTime.ToString("HH:mm"), Date).ToString("dd/MM/yyyy HH:mm")
        Me.appointmentEnddate = CType(apptEndDate.ToLongDateString() + " " + aptEndTime.ToString("HH:mm"), Date).ToString("dd/MM/yyyy HH:mm")
        Me.distance = distance
        Me.aptStartTime = aptStartTime.ToString("HH:mm")
        Me.aptEndTime = aptEndTime.ToString("HH:mm")
        Me.lastColumn = "--"
        Me.addNewDataRow()
    End Sub
#End Region
End Class
