﻿Public Class AlternativeAppointmentSlotsDtBO
    Implements IDtBO

#Region "Attributes"

    Private _isPatchSelected As Boolean
    Public Property IsPatchSelected() As Boolean
        Get
            Return _isPatchSelected
        End Get
        Set(ByVal value As Boolean)
            _isPatchSelected = value
        End Set
    End Property

    Public operative As String = String.Empty
    Public startDateString As String = String.Empty
    Public endDateString As String = String.Empty
    Public lastColumn As String = "--"
    Public operativeId As Integer = 0
    Public startTime As String = String.Empty
    Public endTime As String = String.Empty
    Public propertyId As String = String.Empty
    Public distance As Double = 0.0
    Public patch As String = String.Empty
    Public lastAdded As Boolean = True

    Public dt As DataTable
    Private dr As DataRow

    Public Const OperativeColName As String = "Operative:"
    Public Const PatchColName As String = "Patch:"
    Public Const StartDateStringColName As String = "Start date:"
    Public Const EndDateStringColName As String = "End date:"
    Public Const DistanceColName As String = "Distance:"

    Public Const LastColName As String = " "
    Public Const OperativeIdColName As String = "OperativeId"
    Public Const StartTimeColName As String = "StartTime"
    Public Const EndTimeColName As String = "EndTime"
    Public Const PropertyIdColName As String = "PropertyId"
    Public Const lastAddedColName As String = "LastAdded:"

#End Region

#Region "Constructor"
    Sub New()
        Me.createDataTable()
    End Sub
#End Region


#Region "create Data Table"
    Public Sub createDataTable() Implements IDtBO.createDataTable

        dt = New DataTable()
        dt.Columns.Add(OperativeColName)
        dt.Columns.Add(PatchColName)
        dt.Columns.Add(StartDateStringColName)
        dt.Columns.Add(EndDateStringColName)
        dt.Columns.Add(DistanceColName)
        dt.Columns.Add(LastColName)
        dt.Columns.Add(OperativeIdColName)
        dt.Columns.Add(StartTimeColName)
        dt.Columns.Add(EndTimeColName)
        dt.Columns.Add(PropertyIdColName)
        dt.Columns.Add(lastAddedColName)

    End Sub
#End Region

#Region "add New Data Row"
    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow

        dr = dt.NewRow()
        dr(OperativeColName) = operative
        dr(PatchColName) = patch
        dr(StartDateStringColName) = startDateString
        dr(EndDateStringColName) = endDateString
        dr(DistanceColName) = distance
        dr(LastColName) = lastColumn
        dr(OperativeIdColName) = operativeId
        dr(StartTimeColName) = startTime
        dr(EndTimeColName) = endTime
        dr(PropertyIdColName) = propertyId
        dr(lastAddedColName) = lastAdded
        dt.Rows.Add(dr)

    End Sub
#End Region

End Class
