﻿Imports System.Globalization

Public Class AppointmentSlotsDtBO
    Implements IDtBO

#Region "Attributes"


    Private _expiryDate As Date
    Public Property ExpiryDate() As Date
        Get
            Return _expiryDate
        End Get
        Set(ByVal value As Date)
            _expiryDate = value
        End Set
    End Property

    Private _isPatchSelected As Boolean
    Public Property IsPatchSelected() As Boolean
        Get
            Return _isPatchSelected
        End Get
        Set(ByVal value As Boolean)
            _isPatchSelected = value
        End Set
    End Property

    Private _isDateSelected As Boolean
    Public Property IsDateSelected() As Boolean
        Get
            Return _isDateSelected
        End Get
        Set(ByVal value As Boolean)
            _isDateSelected = value
        End Set
    End Property

    Private _displayCount As Integer
    Public Property DisplayCount() As Integer
        Get
            Return _displayCount
        End Get
        Set(ByVal value As Integer)
            _displayCount = value
        End Set
    End Property

    Private _fullExpiryDate As Date
    Public Property FullExpiryDate() As Date
        Get
            Return _fullExpiryDate
        End Get
        Set(ByVal value As Date)
            _fullExpiryDate = value
        End Set
    End Property

    Private _appointmentDuration As String
    Public Property AppointmentDuration() As String
        Get
            Return _appointmentDuration
        End Get
        Set(ByVal value As String)
            _appointmentDuration = value
        End Set
    End Property

    Private _appointmentStatus As String
    Public Property AppointmentStatus() As String
        Get
            Return _appointmentStatus
        End Get
        Set(ByVal value As String)
            _appointmentStatus = value
        End Set
    End Property

    Private _FromDate As String
    Public Property FromDate() As String
        Get
            Return _FromDate
        End Get
        Set(ByVal value As String)
            _FromDate = value
        End Set
    End Property

    Private _durationHrs As Double
    Public Property DurationHrs() As Double
        Get
            Return _durationHrs
        End Get
        Set(ByVal value As Double)
            _durationHrs = value
        End Set
    End Property

    Private _durationDays As Double
    Public Property DurationDays() As Double
        Get
            Return _durationDays
        End Get
        Set(ByVal value As Double)
            _durationDays = value
        End Set
    End Property



    Public operative As String = String.Empty
    Public patch As String = String.Empty
    Public time As String = String.Empty
    Public appointmentDateString As String = String.Empty
    Public distance As String = String.Empty
    Public lastColumn As String = String.Empty
    Public operativeId As Integer = 0
    Public appointmentdate As Date
    Public appointmentEnddate As Date
    Public aptStartTime As String
    Public aptEndTime As String
    Public appointmentEndDateString As String = String.Empty
    Public dt As DataTable
    Private dr As DataRow

    Public Const operativeColName As String = "Operative:"
    Public Const patchColName As String = "Patch:"
    Public Const timeColName As String = "Time:"
    Public Const dateStringColName As String = "Date:"
    Public Const distanceColName As String = "Distance:"
    Public Const lastColName As String = " "
    Public Const operativeIdColName As String = "OperativeId"
    Public Const appointmentDateColName As String = "AppointmentDate"
    Public Const aptStartTimeColName As String = "AppointmentStartTime"
    Public Const aptEndTimeColName As String = "AppointmentEndTime"
    Public Const appointmentEndDateColName As String = "AppointmentEndDate"
#End Region

#Region "Constructor"
    Sub New()
        Me.createDataTable()
    End Sub
#End Region

#Region "Copy Constructor"
    'The main purpose copy constrctor is to make a new copy of object and 
    'then save to session to avoid effect of changes made after saving in session.
    Sub New(ByVal sourceAppointmentSlotsDtBO As AppointmentSlotsDtBO)
        dt = sourceAppointmentSlotsDtBO.dt.Copy()
        ExpiryDate = sourceAppointmentSlotsDtBO.ExpiryDate
        IsPatchSelected = sourceAppointmentSlotsDtBO.IsPatchSelected
        IsDateSelected = sourceAppointmentSlotsDtBO.IsDateSelected
        DisplayCount = sourceAppointmentSlotsDtBO.DisplayCount
        FullExpiryDate = sourceAppointmentSlotsDtBO.FullExpiryDate
        AppointmentDuration = sourceAppointmentSlotsDtBO.AppointmentDuration
    End Sub
#End Region

#Region "create Data Table"
    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(operativeColName)
        dt.Columns.Add(patchColName)
        dt.Columns.Add(timeColName)
        dt.Columns.Add(dateStringColName)
        dt.Columns.Add(distanceColName)
        dt.Columns.Add(lastColName)
        dt.Columns.Add(operativeIdColName)
        dt.Columns.Add(appointmentDateColName)
        dt.Columns.Add(aptStartTimeColName)
        dt.Columns.Add(aptEndTimeColName)
        dt.Columns.Add(appointmentEndDateColName)
    End Sub
#End Region

#Region "add New Data Row"
    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(operativeColName) = operative
        dr(patchColName) = patch
        dr(timeColName) = time
        dr(dateStringColName) = appointmentDateString
        dr(distanceColName) = distance
        dr(lastColName) = lastColumn
        dr(operativeIdColName) = operativeId
        dr(appointmentDateColName) = appointmentdate
        dr(aptStartTimeColName) = aptStartTime
        dr(aptEndTimeColName) = aptEndTime
        dr(appointmentEndDateColName) = appointmentEnddate
        dt.Rows.Add(dr)
    End Sub
#End Region

#Region "add Row In Appointment List"
    Public Sub addRowInAppointmentList(ByVal operativeId As Integer, ByVal operativeName As String, ByVal operativePatch As String, ByVal aptStartTime As DateTime, ByVal aptEndTime As DateTime, ByVal apptDate As Date, ByVal apptEndDate As Date, ByVal distance As String)

        Dim calendarStartDate As New DateTime(1970, 1, 1)

        Me.operativeId = operativeId
        Me.operative = operativeName
        Me.patch = operativePatch
        Me.time = aptStartTime.ToString("HH:mm") + " - " + aptEndTime.ToString("HH:mm")
        Me.appointmentDateString = apptDate.ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"))
        Me.appointmentdate = CType(apptDate.ToLongDateString() + " " + aptStartTime.ToString("HH:mm"), Date).ToString("dd/MM/yyyy HH:mm")
        Me.appointmentEnddate = CType(apptEndDate.ToLongDateString() + " " + aptEndTime.ToString("HH:mm"), Date).ToString("dd/MM/yyyy HH:mm")
        Me.distance = distance
        Me.aptStartTime = aptStartTime.ToString("HH:mm")
        Me.aptEndTime = aptEndTime.ToString("HH:mm")
        Me.lastColumn = "--"
        Me.addNewDataRow()
    End Sub
#End Region
End Class
