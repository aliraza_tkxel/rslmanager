﻿Public Class OilAlternativeSchedullingSlotHeader
    Private _appointmentDuration As String
    Public Property AppointmentDuration() As String
        Get
            Return _appointmentDuration
        End Get
        Set(ByVal value As String)
            _appointmentDuration = value
        End Set
    End Property

    Private _fuelType As String
    Public Property FuelType() As String
        Get
            Return _fuelType
        End Get
        Set(ByVal value As String)
            _fuelType = value
        End Set
    End Property

    Private _appointmentStatus As String
    Public Property AppointmentStatus() As String
        Get
            Return _appointmentStatus
        End Get
        Set(ByVal value As String)
            _appointmentStatus = value
        End Set
    End Property

    Private _trade As String
    Public Property Trade() As String
        Get
            Return _trade
        End Get
        Set(ByVal value As String)
            _trade = value
        End Set
    End Property

    Private _tradeId As Integer
    Public Property TradeId() As Integer
        Get
            Return _tradeId
        End Get
        Set(ByVal value As Integer)
            _tradeId = value
        End Set
    End Property

    Private _heatingTypeId As Integer
    Public Property HeatingTypeId() As Integer
        Get
            Return _heatingTypeId
        End Get
        Set(ByVal value As Integer)
            _heatingTypeId = value
        End Set
    End Property

End Class
