﻿Public Class SelectedPropertySchedulingBO
    Private _propertyAddress As String
    Public Property PropertyAddress() As String
        Get
            Return _propertyAddress
        End Get
        Set(ByVal value As String)
            _propertyAddress = value
        End Set
    End Property

    Private _postCode As String
    Public Property PostCode() As String
        Get
            Return _postCode
        End Get
        Set(ByVal value As String)
            _postCode = value
        End Set
    End Property

    Private _boilers As String
    Public Property Boilers() As String
        Get
            Return _boilers
        End Get
        Set(ByVal value As String)
            _boilers = value
        End Set
    End Property

    Private _appointmentType As String
    Public Property AppointmentType() As String
        Get
            Return _appointmentType
        End Get
        Set(ByVal value As String)
            _appointmentType = value
        End Set
    End Property

    Private _tenancyId? As Integer
    Public Property TenancyId() As Integer
        Get
            Return _tenancyId
        End Get
        Set(ByVal value As Integer)
            _tenancyId = value
        End Set
    End Property

    Private _duration? As Integer
    Public Property Duration() As Integer
        Get
            Return _duration
        End Get
        Set(ByVal value As Integer)
            _duration = value
        End Set
    End Property

    Private _telephone As String
    Public Property Telephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    Private _patchId As Integer
    Public Property PatchId() As Integer
        Get
            Return _patchId
        End Get
        Set(ByVal value As Integer)
            _patchId = value
        End Set
    End Property

    Private _isPatchSelected As Boolean
    Public Property IsPatchSelected() As Boolean
        Get
            Return _isPatchSelected
        End Get
        Set(ByVal value As Boolean)
            _isPatchSelected = value
        End Set
    End Property

    Private _isDateSelected As Boolean
    Public Property IsDateSelected() As Boolean
        Get
            Return _isDateSelected
        End Get
        Set(ByVal value As Boolean)
            _isDateSelected = value
        End Set
    End Property

    Private _ceritificateExpiryDate As Nullable(Of Date)
    Public Property CertificateExpiryDate() As Nullable(Of Date)
        Get
            Return _ceritificateExpiryDate
        End Get
        Set(ByVal value As Nullable(Of Date))
            _ceritificateExpiryDate = value
        End Set
    End Property

    Private _expiryInDays As String
    Public Property ExpiryInDays() As String
        Get
            Return _expiryInDays
        End Get
        Set(ByVal value As String)
            _expiryInDays = value
        End Set
    End Property

    Private _propertyId As String
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property

    Private _fuelType As String
    Public Property FuelType() As String
        Get
            Return _fuelType
        End Get
        Set(ByVal value As String)
            _fuelType = value
        End Set
    End Property

    Private _journalId As Integer
    Public Property JournalId() As Integer
        Get
            Return _journalId
        End Get
        Set(ByVal value As Integer)
            _journalId = value
        End Set
    End Property

    Private _schemeId As Integer
    Public Property SchemeId() As Integer
        Get
            Return _schemeId
        End Get
        Set(ByVal value As Integer)
            _schemeId = value
        End Set
    End Property

    Private _blockId As Integer
    Public Property BlockId() As Integer
        Get
            Return _blockId
        End Get
        Set(ByVal value As Integer)
            _blockId = value
        End Set
    End Property

    Private _appointmentDate As Date
    Public Property AppointmentDate() As Date
        Get
            Return _appointmentDate
        End Get
        Set(ByVal value As Date)
            _appointmentDate = value
        End Set
    End Property

    Private _appointmentEndDate As Date
    Public Property AppointmentEndDate() As Date
        Get
            Return _appointmentEndDate
        End Get
        Set(ByVal value As Date)
            _appointmentEndDate = value
        End Set
    End Property

    Private _appointmentStartTime As Date
    Public Property AppointmentStartTime() As Date
        Get
            Return _appointmentStartTime
        End Get
        Set(ByVal value As Date)
            _appointmentStartTime = value
        End Set
    End Property

    Private _appointmentEndTime As Date
    Public Property AppointmentEndTime() As Date
        Get
            Return _appointmentEndTime
        End Get
        Set(ByVal value As Date)
            _appointmentEndTime = value
        End Set
    End Property

    Private _operativeId As Integer
    Public Property OperativeId() As Integer
        Get
            Return _operativeId
        End Get
        Set(ByVal value As Integer)
            _operativeId = value
        End Set
    End Property

    Private _operativeName As String
    Public Property OperativeName() As String
        Get
            Return _operativeName
        End Get
        Set(ByVal value As String)
            _operativeName = value
        End Set
    End Property

    Private _appointmentId As Integer
    Public Property AppointmentId() As Integer
        Get
            Return _appointmentId
        End Get
        Set(ByVal value As Integer)
            _appointmentId = value
        End Set
    End Property

    Private _propertyStatus As String
    Public Property PropertyStatus() As String
        Get
            Return _propertyStatus
        End Get
        Set(ByVal value As String)
            _propertyStatus = value
        End Set
    End Property



    Private _CustomerName As String
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal value As String)
            _CustomerName = value
        End Set
    End Property

    Private _CustomerEmail As String
    Public Property CustomerEmail() As String
        Get
            Return _CustomerEmail
        End Get
        Set(ByVal value As String)
            _CustomerEmail = value
        End Set
    End Property

    Private _CustomerEmailStatus As String
    Public Property CustomerEmailStatus() As String
        Get
            Return _CustomerEmailStatus
        End Get
        Set(ByVal value As String)
            _CustomerEmailStatus = value
        End Set
    End Property

    Private _HouseNumber As String
    Public Property HouseNumber() As String
        Get
            Return _HouseNumber
        End Get
        Set(ByVal value As String)
            _HouseNumber = value
        End Set
    End Property

    Private _Address1 As String
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal value As String)
            _Address1 = value
        End Set
    End Property

    Private _Address2 As String
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal value As String)
            _Address2 = value
        End Set
    End Property

    Private _Address3 As String
    Public Property Address3() As String
        Get
            Return _Address3
        End Get
        Set(ByVal value As String)
            _Address3 = value
        End Set
    End Property

    Private _TownCity As String
    Public Property TownCity() As String
        Get
            Return _TownCity
        End Get
        Set(ByVal value As String)
            _TownCity = value
        End Set
    End Property

    Private _County As String
    Public Property County() As String
        Get
            Return _County
        End Get
        Set(ByVal value As String)
            _County = value
        End Set
    End Property


End Class
