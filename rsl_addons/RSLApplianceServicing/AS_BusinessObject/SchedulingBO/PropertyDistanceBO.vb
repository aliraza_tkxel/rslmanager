﻿Public Class PropertyDistanceBO
    Private _fromPropertyAddress As String
    Public Property FromPropertyAddress() As String
        Get
            Return _fromPropertyAddress
        End Get
        Set(ByVal value As String)
            _fromPropertyAddress = value
        End Set
    End Property

    Private _toPropertyAddress As String
    Public Property ToPropertyAddress() As String
        Get
            Return _toPropertyAddress
        End Get
        Set(ByVal value As String)
            _toPropertyAddress = value
        End Set
    End Property

    Private _distance As String
    Public Property Distance() As String
        Get
            Return _distance
        End Get
        Set(ByVal value As String)
            _distance = value
        End Set
    End Property


    Public dr As DataRow

    Public fromAddress As String = String.Empty
    Public toAddress As String = String.Empty
    Public distanceString As String = String.Empty


    Public Const fromAddressColName As String = "FromAddress:"
    Public Const toAddressColName As String = "ToAddress:"
    Public Const distanceStringColName As String = "distanceString:"




    Public Sub addNewDataRow(dt As DataTable)
        dr = dt.NewRow()
        dr(fromAddressColName) = fromAddress
        dr(toAddressColName) = toAddress
        dr(distanceStringColName) = distanceString

        dt.Rows.Add(dr)
    End Sub

    Public Sub createDataTable(dt As DataTable)
        dt.Columns.Add(fromAddressColName)
        dt.Columns.Add(toAddressColName)
        dt.Columns.Add(distanceStringColName)

    End Sub


End Class
