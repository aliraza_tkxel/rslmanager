﻿Public Class AlternativeSchedullingSlot
    Private _startSelectedDate As Date
    Public Property StartSelectedDate() As Date
        Get
            Return _startSelectedDate
        End Get
        Set(ByVal value As Date)
            _startSelectedDate = value
        End Set
    End Property

    Private _displayCount As Integer
    Public Property DisplayCount() As Integer
        Get
            Return _displayCount
        End Get
        Set(ByVal value As Integer)
            _displayCount = value
        End Set
    End Property
    Public alternativeAppointmentDtBO As AlternativeAppointmentDtBO = New AlternativeAppointmentDtBO
    Public alternativeAppointmentSlotsDtBO As AlternativeAppointmentSlotsDtBO = New AlternativeAppointmentSlotsDtBO
End Class
