﻿Imports System

Namespace AS_BusinessObject
    Public Class ScheduleAppointmentsBO : Inherits BaseBO
#Region "Attributes"
        Private _duration? As Integer
        Private _id As Integer
        Private _address As String
        Private _postcode As String
        Private _fuel As String
        Private _type As String

        Private _appointmentType As String

        Private _status As String
        Private _days As Integer
        Private _expirydate As DateTime
        Private _isFlagStatus As Boolean
        Private _isVoid As Integer
        'Scheduling Appointments
        Private _empid As Integer
        Private _empname As String

        'Add Appointments
        Private _appointmentdate As DateTime
        Private _appointmentenddate As DateTime
        Private _journelid As Integer
        Private _schemeId As String
        Private _blockId As String

        Private _schemeBlockId As Integer

        Private _inspectionTypeId As Integer
        Private _statusId As Integer
        Private _actionId As Integer
        Private _letterId As Integer
        Private _letterList As List(Of Integer)
        Private _docList As List(Of String)
        Private _notes As String
        Private _propertyId As String
        Private _createdBy As Integer
        Private _isLetterAttached As Boolean
        Private _isDocumentAttached As Boolean
        Private _shift As String
        Private _appstarttime As String
        Private _appendtime As String
        Private _assignedto As Integer
        'Start - Changes by Aamir Waheed on 17 June 2013
        'To make _tenancyid nullable.
        Private _tenancyid As Nullable(Of Integer)
        'Private _tenancyid As Integer
        'End - Changes by Aamir Waheed on 17 June 2013
        Private _appointmentid As Integer
        'Employees Leave Management
        Private _startdate As DateTime
        Private _returndate As DateTime
        Private _employeeids As String
        Private _documentPath As String

#End Region

#Region "Construtor"

        Public Sub New()
            _id = -1
            _address = String.Empty
            _postcode = String.Empty
            _fuel = String.Empty
            _type = String.Empty
            _appointmentType = String.Empty
            _status = String.Empty
            _days = -1
            _expirydate = Nothing
            _isVoid = 0
        End Sub

#End Region

#Region "Properties"
        ' Get / Set property for _id
        Public Property Id() As Integer

            Get
                Return _id
            End Get

            Set(ByVal value As Integer)
                _id = value
            End Set

        End Property
        ' Get / Set property for _address
        Public Property Address() As String

            Get
                Return _address
            End Get

            Set(ByVal value As String)
                _address = value
            End Set

        End Property
        ' Get / Set property for _postcode
        Public Property Postcode() As String

            Get
                Return _postcode
            End Get

            Set(ByVal value As String)
                _postcode = value
            End Set

        End Property
        ' Get / Set property for _fuel
        Public Property Fuel() As String

            Get
                Return _fuel
            End Get

            Set(ByVal value As String)
                _fuel = value
            End Set

        End Property
        ' Get / Set property for _type
        Public Property Type() As String

            Get
                Return _type
            End Get

            Set(ByVal value As String)
                _type = value
            End Set

        End Property

        ' Get / Set property for _appointmentType
        Public Property AppointmentType() As String

            Get
                Return _appointmentType

            End Get

            Set(ByVal value As String)
                _appointmentType = value
            End Set

        End Property

        ' Get / Set property for _status
        Public Property Status() As String

            Get
                Return _status
            End Get

            Set(ByVal value As String)
                _status = value
            End Set

        End Property

        ' Get / Set property for _isVoid
        Public Property IsVoid() As Integer

            Get
                Return _isVoid
            End Get

            Set(ByVal value As Integer)
                _isVoid = value
            End Set

        End Property

        ' Get / Set property for _days
        Public Property Days() As Integer

            Get
                Return _days
            End Get

            Set(ByVal value As Integer)
                _days = value
            End Set

        End Property
        ' Get / Set property for _expiraydate
        Public Property Expiraydate() As DateTime

            Get
                Return _expirydate
            End Get

            Set(ByVal value As DateTime)
                _expirydate = value
            End Set

        End Property


        Public Property Duration() As Integer
            Get
                Return _duration
            End Get
            Set(ByVal value As Integer)
                _duration = value
            End Set
        End Property
#End Region

#Region "Scheduling Appointments"
        ' Get / Set property for _id
        Public Property EmpolyeeId() As Integer

            Get
                Return _empid
            End Get

            Set(ByVal value As Integer)
                _empid = value
            End Set

        End Property
        ' Get / Set property for _address
        Public Property EmployeeName() As String

            Get
                Return _empname
            End Get

            Set(ByVal value As String)
                _empname = value
            End Set

        End Property
#Region "Get Employees Leaves Info"
        ' Get / Set property for _employeeids
        Public Property EmpolyeeIds() As String

            Get
                Return _employeeids
            End Get

            Set(ByVal value As String)
                _employeeids = value
            End Set

        End Property
        ' Get / Set property for _startdate
        Public Property StartDate() As Date
            Get
                Return _startdate
            End Get
            Set(ByVal value As Date)
                _startdate = value
            End Set
        End Property
        ' Get / Set property for _returndate
        Public Property ReturnDate() As Date
            Get
                Return _returndate
            End Get
            Set(ByVal value As Date)
                _returndate = value
            End Set
        End Property

        Public Property DocumentPath() As String
            Get
                Return _documentPath
            End Get
            Set(ByVal value As String)
                _documentPath = value
            End Set
        End Property
#End Region
#End Region

#Region "Add Appointments"
        Public Property AppointmentDate() As Date
            Get
                Return _appointmentdate
            End Get
            Set(ByVal value As Date)
                _appointmentdate = value
            End Set
        End Property

        Public Property AppointmentEndDate() As Date
            Get
                Return _appointmentenddate
            End Get
            Set(ByVal value As Date)
                _appointmentenddate = value
            End Set
        End Property

        Public Property SchemeId() As String
            Get
                Return _schemeId
            End Get
            Set(ByVal value As String)
                _schemeId = value
            End Set
        End Property

        Public Property BlockId() As String
            Get
                Return _blockId
            End Get
            Set(ByVal value As String)
                _blockId = value
            End Set
        End Property

        Public Property JournelId() As Integer
            Get
                Return _journelid
            End Get
            Set(ByVal value As Integer)
                _journelid = value
            End Set
        End Property

        Public Property SchemeBlockId() As Integer
            Get
                Return _schemeBlockId
            End Get
            Set(ByVal value As Integer)
                _schemeBlockId = value
            End Set
        End Property

        Public Property InspectionTypeId() As Integer
            Get
                Return _inspectionTypeId
            End Get
            Set(ByVal value As Integer)
                _inspectionTypeId = value
            End Set
        End Property
        Public Property StatusId() As Integer
            Get
                Return _statusId
            End Get
            Set(ByVal value As Integer)
                _statusId = value
            End Set
        End Property
        Public Property ActionId() As Integer
            Get
                Return _actionId
            End Get
            Set(ByVal value As Integer)
                _actionId = value
            End Set
        End Property
        Public Property LetterId() As Integer
            Get
                Return _letterId
            End Get
            Set(ByVal value As Integer)
                _letterId = value
            End Set
        End Property
        Public Property LetterList() As List(Of Integer)
            Get
                Return _letterList
            End Get
            Set(ByVal value As List(Of Integer))
                _letterList = value
            End Set
        End Property
        Public Property DocList() As List(Of String)
            Get
                Return _docList
            End Get
            Set(ByVal value As List(Of String))
                _docList = value
            End Set
        End Property
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property
        Public Property CreatedBy() As String
            Get
                Return _createdBy
            End Get
            Set(ByVal value As String)
                _createdBy = value
            End Set
        End Property
        Public Property IsLetterAttached() As Boolean
            Get
                Return _isLetterAttached
            End Get
            Set(ByVal value As Boolean)
                _isLetterAttached = value
            End Set
        End Property
        Public Property IsDocumentAttached() As Boolean
            Get
                Return _isDocumentAttached
            End Get
            Set(ByVal value As Boolean)
                _isDocumentAttached = value
            End Set
        End Property
        Public Property AppStartTime() As String
            Get
                Return _appstarttime
            End Get
            Set(ByVal value As String)
                _appstarttime = value
            End Set
        End Property
        Public Property AppEndTime() As String
            Get
                Return _appendtime
            End Get
            Set(ByVal value As String)
                _appendtime = value
            End Set
        End Property
        Public Property AssignedTo() As Integer
            Get
                Return _assignedto
            End Get
            Set(ByVal value As Integer)
                _assignedto = value
            End Set
        End Property
        Public Property Shift() As String
            Get
                Return _shift
            End Get
            Set(ByVal value As String)
                _shift = value
            End Set
        End Property

        'Start - Changes by Aamir Waheed on 17 June 2013
        'To make _tenancyid nullable.

        Public Property TenancyId() As Nullable(Of Integer)
            Get
                Return _tenancyid
            End Get
            Set(ByVal value As Nullable(Of Integer))
                _tenancyid = value
            End Set
        End Property

        'Public Property TenancyId() As Integer
        '    Get
        '        Return _tenancyid
        '    End Get
        '    Set(ByVal value As Integer)
        '        _tenancyid = value
        '    End Set
        'End Property

        'End - Changes by Aamir Waheed on 17 June 2013


        Public Property AppointmentId() As Integer
            Get
                Return _appointmentid
            End Get
            Set(ByVal value As Integer)
                _appointmentid = value
            End Set
        End Property

        Private _CustomerEmail As String
        Public Property CustomerEmail() As String
            Get
                Return _CustomerEmail
            End Get
            Set(ByVal value As String)
                _CustomerEmail = value
            End Set
        End Property

        Private _CustomerEmailStatus As String
        Public Property CustomerEmailStatus() As String
            Get
                Return _CustomerEmailStatus
            End Get
            Set(ByVal value As String)
                _CustomerEmailStatus = value
            End Set
        End Property
#End Region

#Region "Functions"


#End Region



    End Class
End Namespace
