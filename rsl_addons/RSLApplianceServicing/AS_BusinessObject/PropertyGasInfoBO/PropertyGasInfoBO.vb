﻿Imports System


Public Class PropertyGasInfoBO
#Region "Attributes"

    Private _propertyId As String
    Private _issuedDate As DateTime
    Private _renewalDate As DateTime
    Private _certificateNumber As String
    Private _meterLocationId As Integer


#End Region

#Region "Construtor"

    Public Sub New()
        _propertyId = String.Empty
        _issuedDate = DateTime.Now
        _renewalDate = DateTime.Now
        _certificateNumber = String.Empty
        _meterLocationId = 0
    End Sub

#End Region

#Region "Properties"
    ' Get / Set property for _propertyId
    Public Property PropertyId() As String

        Get
            Return _propertyId
        End Get

        Set(ByVal value As String)
            _propertyId = value
        End Set

    End Property
    ' Get / Set property for _issuedDate
    Public Property IssuedDate() As DateTime

        Get
            Return _issuedDate
        End Get

        Set(ByVal value As DateTime)
            _issuedDate = value
        End Set

    End Property
    ' Get / Set property for _renewalDate
    Public Property RenewalDate() As DateTime

        Get
            Return _renewalDate
        End Get

        Set(ByVal value As DateTime)
            _renewalDate = value
        End Set

    End Property
    ' Get / Set property for _certificateNumber
    Public Property CertificateNumber() As String

        Get
            Return _certificateNumber
        End Get

        Set(ByVal value As String)
            _certificateNumber = value
        End Set

    End Property
    ' Get / Set property for _meterLocationId
    Public Property MeterLocationId() As Integer

        Get
            Return _meterLocationId
        End Get

        Set(ByVal value As Integer)
            _meterLocationId = value
        End Set

    End Property
#End Region
End Class
