﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Namespace AS_BusinessObject

    Public Class CustomerBO : Inherits BaseBO

#Region "Properties"

        Public Property Id() As Integer = -1

        Public Property TitleValue() As Integer = -1

        Public Property FirstName() As String = String.Empty

        Public Property MiddleName() As String = String.Empty

        Public Property LastName() As String = String.Empty

        Public Property Tenancy() As String = String.Empty

        Public Property Password() As String = String.Empty

        Public Property DateOfBirth() As DateTime = Nothing

        Public ReadOnly Property DateOfBirthDDMMYY() As String
            Get
                Return Me.DateOfBirth.Day.ToString + "/" + Me.DateOfBirth.Month.ToString + "/" + Me.DateOfBirth.Year.ToString
            End Get
        End Property

#Region "Wrapper Properties"

        Public Property CustomerId As Integer
            Get
                Return Id
            End Get
            Set(value As Integer)
                Id = value
            End Set
        End Property

#End Region

#Region "Properties return concatinated rusult of multiple attributes"
        ' Get property ,Customer Name
        Public ReadOnly Property CustomerName() As String
            Get
                Return Me.LastName & " " & Me.FirstName
            End Get

        End Property

#End Region

#Region "Properties with validation attribues"

        <RegexValidator("^\+?\d+(-\d+)*$", MessageTemplate:="Invalid Telephone number.")> _
        <StringLengthValidator(0, 18, MessageTemplate:="Invalid Telephone number.")> _
        Public Property Telephone() As String

        ' Get / Set property for _mobile
        <RegexValidator("^\+?\d+(-\d+)*$", MessageTemplate:="Invalid Mobile number.")> _
        <StringLengthValidator(0, 18, MessageTemplate:="Invalid Mobile number.")> _
        Public Property Mobile() As String

        ' Get / Set email for _email
        <RegexValidator("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$", MessageTemplate:="Invalid Email address.")> _
        Public Property Email() As String

#End Region

#End Region

#Region "Functions"

        Public Function Compare(ByRef custBO As CustomerBO) As Boolean

            If _email.Equals(custBO.Email) And _password.Equals(custBO.Password) Then
                Return True
            End If

            Return False

        End Function

#End Region

    End Class

End Namespace