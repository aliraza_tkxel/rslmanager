﻿Imports System

Namespace AS_BusinessObject
    Public Class CustomerDetailsBO : Inherits CustomerBO

#Region "Attributes"

        Private _accountBalance As Decimal
        Private _gasServicingAppointmentDate As DateTime

        Private _gender As String
        Private _niNumber As String
        Private _occupation As String
        Private _takeHomePay As Integer
        Private _maritalStatus As String
        Private _ethnicity As String
        Private _religion As String
        Private _sexualOrientation As String
        Private _mainLanguage As String
        Private _communication As String
        Private _preferCommunicationStyle As String
        Private _disability As String
        Private _noOfOccupantsAbove18 As Integer
        Private _noOfOccupantsBelow18 As Integer


#End Region

#Region "Construtor"

        Public Sub New()

            _accountBalance = -1
            _gasServicingAppointmentDate = Nothing


            _gender = String.Empty
            _niNumber = String.Empty
            _occupation = String.Empty
            _takeHomePay = -1
            _maritalStatus = String.Empty
            _ethnicity = String.Empty
            _religion = String.Empty
            _sexualOrientation = String.Empty
            _mainLanguage = String.Empty
            _communication = String.Empty
            _preferCommunicationStyle = String.Empty
            _disability = String.Empty
            _noOfOccupantsAbove18 = -1
            _noOfOccupantsBelow18 = -1

        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _accountBalance
        Public Property AccountBalance() As Decimal

            Get
                Return _accountBalance
            End Get

            Set(ByVal value As Decimal)
                _accountBalance = value
            End Set

        End Property


        ' Get / Set property for _gasServicingAppointmentDate
        Public Property GasServicingAppointmentDate() As DateTime

            Get
                Return _gasServicingAppointmentDate
            End Get

            Set(ByVal value As DateTime)
                _gasServicingAppointmentDate = value
            End Set

        End Property

        ' Get / Set property for _gender
        Public Property Gender() As String

            Get
                Return _gender
            End Get

            Set(ByVal value As String)
                _gender = value
            End Set

        End Property


        ' Get / Set property for _niNumber
        Public Property NINumber() As String

            Get
                Return _niNumber
            End Get

            Set(ByVal value As String)
                _niNumber = value
            End Set

        End Property

        ' Get / Set property for _occupation
        Public Property Occupation() As String

            Get
                Return _occupation
            End Get

            Set(ByVal value As String)
                _occupation = value
            End Set

        End Property


        ' Get / Set property for _takeHomePay
        Public Property TakeHomePay() As String

            Get
                Return _takeHomePay
            End Get

            Set(ByVal value As String)
                _takeHomePay = value
            End Set

        End Property

        ' Get / Set property for _maritalStatus
        Public Property MaritalStatus() As String

            Get
                Return _maritalStatus
            End Get

            Set(ByVal value As String)
                _maritalStatus = value
            End Set

        End Property


        ' Get / Set property for _ethnicity
        Public Property Ethnicity() As String

            Get
                Return _ethnicity
            End Get

            Set(ByVal value As String)
                _ethnicity = value
            End Set

        End Property


        ' Get / Set property for _religion
        Public Property Religion() As String

            Get
                Return _religion
            End Get

            Set(ByVal value As String)
                _religion = value
            End Set

        End Property


        ' Get / Set property for _sexualOrientation
        Public Property SexualOrientation() As String

            Get
                Return _sexualOrientation
            End Get

            Set(ByVal value As String)
                _sexualOrientation = value
            End Set

        End Property

        ' Get / Set property for _mainLanguage
        Public Property MainLanguage() As String

            Get
                Return _mainLanguage
            End Get

            Set(ByVal value As String)
                _mainLanguage = value
            End Set

        End Property

        ' Get / Set property for _communication
        Public Property Communication() As String

            Get
                Return _communication
            End Get

            Set(ByVal value As String)
                _communication = value
            End Set

        End Property

        ' Get / Set property for _preferCommunicationStyle
        Public Property PreferCommunicationStyle() As String

            Get
                Return _preferCommunicationStyle
            End Get

            Set(ByVal value As String)
                _preferCommunicationStyle = value
            End Set

        End Property

        ' Get / Set property for _disability
        Public Property Disability() As String

            Get
                Return _disability
            End Get

            Set(ByVal value As String)
                _disability = value
            End Set

        End Property

        ' Get / Set property for _preferCommunicationStyle
        Public Property NoOfOccupantsAbove18() As Integer

            Get
                Return _noOfOccupantsAbove18
            End Get

            Set(ByVal value As Integer)
                _noOfOccupantsAbove18 = value
            End Set

        End Property

        ' Get / Set property for _noOfOccupantsBelow18
        Public Property NoOfOccupantsBelow18() As Integer

            Get
                Return _noOfOccupantsBelow18
            End Get

            Set(ByVal value As Integer)
                _noOfOccupantsBelow18 = value
            End Set

        End Property


#End Region

#Region "Functions"


#End Region

    End Class
End Namespace