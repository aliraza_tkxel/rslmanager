﻿Imports System
Namespace AS_BusinessObject

    Public Class CustomerBO : Inherits BaseBO

#Region "Attributes"

        Private _id As Integer
        Private _titleVal As Integer
        Private _firstName As String
        Private _middleName As String
        Private _lastName As String
        Private _email As String
        Private _password As String
        Private _tenancy As String
        Private _dob As DateTime

#End Region

#Region "Construtor"

        Public Sub New()

            _id = -1
            _titleVal = -1
            _firstName = String.Empty
            _middleName = String.Empty
            _lastName = String.Empty
            _email = String.Empty
            _password = String.Empty
            _tenancy = String.Empty
            ' _address = Nothing
            _dob = Nothing

        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _id
        Public Property Id() As Integer

            Get
                Return _id
            End Get

            Set(ByVal value As Integer)
                _id = value
            End Set

        End Property

        ' Get / Set property for _titleVal
        Public Property TitleValue() As Integer

            Get
                Return _titleVal
            End Get

            Set(ByVal value As Integer)
                _titleVal = value
            End Set

        End Property

        ' Get / Set property for _firstName
        Public Property FirstName() As String

            Get
                Return _firstName
            End Get

            Set(ByVal value As String)
                _firstName = value
            End Set

        End Property

        ' Get / Set property for _middleName
        Public Property MiddleName() As String

            Get
                Return _middleName
            End Get

            Set(ByVal value As String)
                _middleName = value
            End Set

        End Property

        ' Get / Set property for _lastName
        Public Property LastName() As String

            Get
                Return _lastName
            End Get

            Set(ByVal value As String)
                _lastName = value
            End Set

        End Property

        ' Get / Set property for _email
        Public Property Email() As String

            Get
                Return _email
            End Get

            Set(ByVal value As String)
                _email = value
            End Set

        End Property

        ' Get / Set property for _tenancy
        Public Property Tenancy() As String

            Get
                Return _tenancy
            End Get

            Set(ByVal value As String)
                _tenancy = value
            End Set

        End Property


        ' Get / Set property for _password
        Public Property Password() As String

            Get
                Return _password
            End Get

            Set(ByVal value As String)
                _password = value
            End Set

        End Property


        ' Get / Set property for _address
        'Public Property Address() As CustomerAddressBO

        '    Get
        '        Return _address
        '    End Get

        '    Set(ByVal value As CustomerAddressBO)
        '        _address = value
        '    End Set

        'End Property

        ' Get / Set property for _dob
        Public Property DateOfBirth() As DateTime
            Get
                Return _dob
            End Get

            Set(ByVal value As DateTime)
                _dob = value
            End Set

        End Property

        ' Get / Set property for _dob
        Public ReadOnly Property DateOfBirthDDMMYY() As String
            Get
                Return Me.DateOfBirth.Day.ToString + "/" + Me.DateOfBirth.Month.ToString + "/" + Me.DateOfBirth.Year.ToString
            End Get

        End Property



#Region "Properties return concatinated rusult of multiple attributes"
        ' Get property ,Customer Name
        Public ReadOnly Property CustomerName() As String
            Get
                Return Me.LastName & " " & Me.FirstName
            End Get

        End Property

#End Region
#End Region

#Region "Functions"

        Public Function Compare(ByRef custBO As CustomerBO) As Boolean

            If _email.Equals(custBO.Email) And _password.Equals(custBO.Password) Then
                Return True
            End If

            Return False

        End Function

#End Region

    End Class

End Namespace