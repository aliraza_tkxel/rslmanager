﻿Imports System

Public Class JobSheetBO
#Region "Attributes"
    Private _appointmentJSNId As Integer
    Private _appointmentStatus As String
    Private _appointmentNotes As String
    Private _userId As Integer
    Private _orderid As Integer
    Private _lastactiondatetime As DateTime
    Protected _flagStatus As Boolean
    Protected _purchaseOrderItemId As Integer
    Protected _userMsg As String
    Protected _workRequired As String
#End Region

#Region "Construtor"
    Public Sub New()
        _appointmentJSNId = 0
        _appointmentStatus = String.Empty
        _purchaseOrderItemId = 0
        _userId = -1
        _orderid = 0
        _workRequired = String.Empty

        _flagStatus = False
        _userMsg = String.Empty
    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _appointmentid
    Public Property AppointmentJSNId() As Integer

        Get
            Return _appointmentJSNId
        End Get

        Set(ByVal value As Integer)
            _appointmentJSNId = value
        End Set

    End Property

    ' Get / Set property for _appointmentStatus
    Public Property AppointmentStatus() As String

        Get
            Return _appointmentStatus
        End Get

        Set(ByVal value As String)
            _appointmentStatus = value
        End Set

    End Property

    ' Get / Set property for _appointmentNotes
    Public Property AppointmentNotes() As String

        Get
            Return _appointmentNotes
        End Get

        Set(ByVal value As String)
            _appointmentNotes = value
        End Set

    End Property

    'Get/Set property for _lastactiondate
    Public Property LastActionDateTime() As DateTime
        Get
            Return _lastactiondatetime
        End Get
        Set(ByVal value As DateTime)
            _lastactiondatetime = value
        End Set
    End Property

    'Get/Set property for _userId
    Public Property UserID() As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property

    'Get/Set property for _purchaseOrderItemId
    Public Property PurchaseOrderItemId() As Integer
        Get
            Return _purchaseOrderItemId
        End Get
        Set(ByVal value As Integer)
            _purchaseOrderItemId = value
        End Set
    End Property

    ' Get / Set property for _orderid
    Public Property OrderId() As Integer

        Get
            Return _orderid
        End Get

        Set(ByVal value As Integer)
            _orderid = value
        End Set

    End Property


    ' Get/Set property for _flagStatus
    Public Property IsFlagStatus() As Boolean

        Get
            Return _flagStatus
        End Get

        Set(ByVal value As Boolean)
            _flagStatus = value
        End Set

    End Property

    ' Get/Set property for _userMsg
    Public Property UserMsg() As String

        Get
            Return _userMsg
        End Get

        Set(ByVal value As String)
            _userMsg = value
        End Set

    End Property

    ' Get/Set property for _flagStatus
    Public Property WorkRequired() As String

        Get
            Return _workRequired
        End Get

        Set(ByVal value As String)

            _workRequired = value
        End Set

    End Property

#End Region
End Class
