﻿Imports System

Public Class PropertyActivityBO

#Region "Attributes"

    Private _actionId As Integer
    Private _title As String
    Private _inspectionTypeId As Integer
    Private _statusId As Integer
    Private _letterId As Integer
    Private _docList As List(Of String)
    Private _letterList As List(Of Integer)
    Private _notes As String
    Private _actionDate As Date
    Private _propertyId As String
    Private _createdBy As Integer
    Private _isLetterAttached As Boolean
    Private _isDocumentAttached As Boolean
    Private _documentPath As String
    Private _reqType As String

#End Region

#Region "Construtor"

    Public Sub New()

    End Sub

    Public Sub New(ByVal id As Integer, ByVal name As String)
        _actionId = id
        _title = name
    End Sub

#End Region

#Region "Properties"

    Public Property InspectionTypeId() As Integer
        Get
            Return _inspectionTypeId
        End Get
        Set(ByVal value As Integer)
            _inspectionTypeId = value
        End Set
    End Property

    Public Property StatusId() As Integer
        Get
            Return _statusId
        End Get
        Set(ByVal value As Integer)
            _statusId = value
        End Set
    End Property

    Public Property ActionId() As Integer
        Get
            Return _actionId
        End Get
        Set(ByVal value As Integer)
            _actionId = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public Property LetterId() As Integer
        Get
            Return _letterId
        End Get
        Set(ByVal value As Integer)
            _letterId = value
        End Set
    End Property

    Public Property LetterList() As List(Of Integer)
        Get
            Return _letterList
        End Get
        Set(ByVal value As List(Of Integer))
            _letterList = value
        End Set
    End Property

    Public Property DocList() As List(Of String)
        Get
            Return _docList
        End Get
        Set(ByVal value As List(Of String))
            _docList = value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property

    Public Property ActionDate() As Date
        Get
            Return _actionDate
        End Get
        Set(ByVal value As Date)
            _actionDate = value
        End Set
    End Property

    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property


    Public Property CreatedBy() As String
        Get
            Return _createdBy
        End Get
        Set(ByVal value As String)
            _createdBy = value
        End Set
    End Property

    Public Property IsLetterAttached() As Boolean
        Get
            Return _isLetterAttached
        End Get
        Set(ByVal value As Boolean)
            _isLetterAttached = value
        End Set
    End Property

    Public Property IsDocumentAttached() As Boolean
        Get
            Return _isDocumentAttached
        End Get
        Set(ByVal value As Boolean)
            _isDocumentAttached = value
        End Set
    End Property

    Public Property DocumentPath() As String
        Get
            Return _documentPath
        End Get
        Set(ByVal value As String)
            _documentPath = value
        End Set
    End Property

    Public Property ReqTye() As String
        Get
            Return _reqType
        End Get
        Set(ByVal value As String)
            _reqType = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
