﻿Public Class ActivitiesBO
    Private _JournalHistoryId As Integer
    Public Property JournalHistoryId() As Integer
        Get
            Return _JournalHistoryId
        End Get
        Set(ByVal value As Integer)
            _JournalHistoryId = value
        End Set
    End Property

    Private _PropertyDefectId As Integer
    Public Property PropertyDefectId() As Integer
        Get
            Return _PropertyDefectId
        End Get
        Set(ByVal value As Integer)
            _PropertyDefectId = value
        End Set
    End Property

    Private _IsDefectImageExist As Integer
    Public Property IsDefectImageExist() As Integer
        Get
            Return _IsDefectImageExist
        End Get
        Set(ByVal value As Integer)
            _IsDefectImageExist = value
        End Set
    End Property

    Private _JournalId As Integer
    Public Property JournalId() As Integer
        Get
            Return _JournalId
        End Get
        Set(ByVal value As Integer)
            _JournalId = value
        End Set
    End Property


    Private _Status As String
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = value
        End Set
    End Property

    Private _Action As String
    Public Property Action() As String
        Get
            Return _Action
        End Get
        Set(ByVal value As String)
            _Action = value
        End Set
    End Property
    Private _InspectionType As String
    Public Property InspectionType() As String
        Get
            Return _InspectionType
        End Get
        Set(ByVal value As String)
            _InspectionType = value
        End Set
    End Property

    Private _CreateDate As String
    Public Property CreateDate() As String
        Get
            Return _CreateDate
        End Get
        Set(ByVal value As String)
            _CreateDate = value
        End Set
    End Property
    Private _Name As String
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property
    Private _IsLetterAttached As Boolean
    Public Property IsLetterAttached() As Boolean
        Get
            Return _IsLetterAttached
        End Get
        Set(ByVal value As Boolean)
            _IsLetterAttached = value
        End Set
    End Property
    Private _IsDocumentAttached As Boolean
    Public Property IsDocumentAttached() As Boolean
        Get
            Return _IsDocumentAttached
        End Get
        Set(ByVal value As Boolean)
            _IsDocumentAttached = value
        End Set
    End Property
    Private _Document As String
    Public Property Document() As String
        Get
            Return _Document
        End Get
        Set(ByVal value As String)
            _Document = value
        End Set
    End Property
    Private _CP12DocumentID As Integer
    Public Property CP12DocumentID() As Integer
        Get
            Return _CP12DocumentID
        End Get
        Set(ByVal value As Integer)
            _CP12DocumentID = value
        End Set
    End Property
    Private _CREATIONDate As String
    Public Property CREATIONDate() As String
        Get
            Return _CREATIONDate
        End Get
        Set(ByVal value As String)
            _CREATIONDate = value
        End Set
    End Property
    Private _REF As String
    Public Property REF() As String
        Get
            Return _REF
        End Get
        Set(ByVal value As String)
            _REF = value
        End Set
    End Property
    Private _AppointmentDate As String
    Public Property AppointmentDate() As String
        Get
            Return _AppointmentDate
        End Get
        Set(ByVal value As String)
            _AppointmentDate = value
        End Set
    End Property
    Private _OPERATIVENAME As String
    Public Property OPERATIVENAME() As String
        Get
            Return _OPERATIVENAME
        End Get
        Set(ByVal value As String)
            _OPERATIVENAME = value
        End Set
    End Property
    Private _OPERATIVETRADE As String
    Public Property OPERATIVETRADE() As String
        Get
            Return _OPERATIVETRADE
        End Get
        Set(ByVal value As String)
            _OPERATIVETRADE = value
        End Set
    End Property


    Private _AppointmentId As Integer
    Public Property AppointmentId() As Integer
        Get
            Return _AppointmentId
        End Get
        Set(ByVal value As Integer)
            _AppointmentId = value
        End Set
    End Property

    Private _AppointmentNotes As String
    Public Property AppointmentNotes() As String
        Get
            Return _AppointmentNotes
        End Get
        Set(ByVal value As String)
            _AppointmentNotes = value
        End Set
    End Property

    Private _InspectionTypeDescription As String
    Public Property InspectionTypeDescription() As String
        Get
            Return _InspectionTypeDescription
        End Get
        Set(ByVal value As String)
            _InspectionTypeDescription = value
        End Set
    End Property

    'my change
    Private _LGSRHISTORYID As Integer
    Public Property LGSRHISTORYID() As Integer
        Get
            Return _LGSRHISTORYID
        End Get
        Set(ByVal value As Integer)
            _LGSRHISTORYID = value
        End Set
    End Property

    'end my change
End Class
