﻿Public Class PropertyRentBO
#Region "Private"
    Private _rentFrequency As Integer
    Private _rentDateSet As Date
    Private _rentEffectiveTo As Date
    Private _rentType As Integer
    Private _rent As Decimal
    Private _services As Decimal
    Private _councilTax As Decimal
    Private _water As Decimal
    Private _inelig As Decimal
    Private _supp As Decimal
    Private _garage As Decimal
    Private _totalRent As Decimal
    Private _propertyId As String
    Private _userId As Integer
#End Region

#Region "Public Properties"

    'Property Property Rent Frequency 
    Public Property RentFrequency() As Integer
        Get
            Return _rentFrequency
        End Get
        Set(value As Integer)
            _rentFrequency = value
        End Set
    End Property
    'Property Property Rent Date Set 
    Public Property RentDateSet() As Date
        Get
            Return _rentDateSet
        End Get
        Set(ByVal value As Date)
            _rentDateSet = value
        End Set
    End Property
    'Property Property Rent Effective Date
    Public Property RentEffectiveSet() As Date
        Get
            Return _rentEffectiveTo
        End Get
        Set(ByVal value As Date)
            _rentEffectiveTo = value
        End Set
    End Property

    'Property Rent Type
    Public Property RentType() As Integer
        Get
            Return _rentType
        End Get
        Set(ByVal value As Integer)
            _rentType = value
        End Set
    End Property
    'Property Property Rent
    Public Property Rent() As Decimal
        Get
            Return _rent
        End Get
        Set(ByVal value As Decimal)
            _rent = value
        End Set
    End Property
    'Property Services
    Public Property Services() As Decimal
        Get
            Return _services
        End Get
        Set(ByVal value As Decimal)
            _services = value
        End Set
    End Property
    'Property Council Tax
    Public Property CouncilTax() As Decimal
        Get
            Return _councilTax
        End Get
        Set(ByVal value As Decimal)
            _councilTax = value
        End Set
    End Property
    'Property Water
    Public Property Water() As Decimal
        Get
            Return _water
        End Get
        Set(ByVal value As Decimal)
            _water = value
        End Set
    End Property
    'Property Inelig
    Public Property Inelig() As Decimal
        Get
            Return _inelig
        End Get
        Set(ByVal value As Decimal)
            _inelig = value
        End Set
    End Property
    'Property Supp
    Public Property Supp() As Decimal
        Get
            Return _supp
        End Get
        Set(ByVal value As Decimal)
            _supp = value
        End Set
    End Property
    'Property Garage
    Public Property Garage() As Decimal
        Get
            Return _garage
        End Get
        Set(ByVal value As Decimal)
            _garage = value
        End Set
    End Property
    'Property Supp
    Public Property TotalRent() As Decimal
        Get
            Return _totalRent
        End Get
        Set(ByVal value As Decimal)
            _totalRent = value
        End Set
    End Property
    'User Id
    Public Property UserId() As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property
    'Property Id
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property
#End Region
End Class
