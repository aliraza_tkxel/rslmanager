﻿Imports System

Public Class PropertyDocumentBO

#Region "Attributes"

    Private _propertyId As String
    Private _documentPath As String
    Private _documentType As String
    Private _keyword As String
    Private _documentSize As String
    Private _documentFormat As String
    Private _documentName As String
    Private _expiryDate As String
    Private _documentDate As String
    Private _uploadedBy As Integer
    Private _EpcRating As Integer
    Private _CP12Number As String
    Private _CP12Dcoument As Byte()
    Private _documentCategory As String
#End Region

#Region "Properties"

    Public Property DocumentName() As String
        Get
            Return _documentName
        End Get
        Set(ByVal value As String)
            _documentName = value
        End Set
    End Property

    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property
    Public Property DocumentSubTypeId() As Integer
      
    Public Property DocumentTypeId() As Integer
      
    Public Property DocumentPath() As String
        Get
            Return _documentPath
        End Get
        Set(ByVal value As String)
            _documentPath = value
        End Set
    End Property
    Public Property DocumentType() As String
        Get
            Return _documentType
        End Get
        Set(ByVal value As String)
            _documentType = value
        End Set
    End Property
    Public Property Keyword() As String
        Get
            Return _keyword
        End Get
        Set(ByVal value As String)
            _keyword = value
        End Set
    End Property
    Public Property DocumentSize() As String
        Get
            Return _documentSize
        End Get
        Set(ByVal value As String)
            _documentSize = value
        End Set
    End Property
    Public Property DocumentFormat() As String
        Get
            Return _documentFormat
        End Get
        Set(ByVal value As String)
            _documentFormat = value
        End Set
    End Property

    Private _documentId As Integer
    Public Property DocumentId() As Integer
        Get
            Return _documentId
        End Get
        Set(ByVal value As Integer)
            _documentId = value
        End Set
    End Property

    Public Property ExpiryDate() As String
        Get
            Return _expiryDate
        End Get
        Set(ByVal value As String)
            _expiryDate = value
        End Set
    End Property

    Public Property DocumentDate() As String
        Get
            Return _documentDate
        End Get
        Set(ByVal value As String)
            _documentDate = value
        End Set
    End Property

    Public Property UploadedBy() As Integer
        Get
            Return _uploadedBy
        End Get
        Set(ByVal value As Integer)
            _uploadedBy = value
        End Set
    End Property
    Public Property EpcRating() As Integer
        Get
            Return _EpcRating
        End Get
        Set(ByVal value As Integer)
            _EpcRating = value
        End Set
    End Property


    Public Property CP12Number() As String
        Get
            Return _CP12Number
        End Get
        Set(ByVal value As String)
            _CP12Number = value
        End Set
    End Property


    Public Property CP12Dcoument() As Byte()
        Get
            Return _CP12Dcoument
        End Get
        Set(ByVal value As Byte())
            _CP12Dcoument = value
        End Set
    End Property

    Public Property DocumentCategory() As String
        Get
            Return _documentCategory
        End Get
        Set(ByVal value As String)
            _documentCategory = value
        End Set
    End Property



#End Region

End Class
