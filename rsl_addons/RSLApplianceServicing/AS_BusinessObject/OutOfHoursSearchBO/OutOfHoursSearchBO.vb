﻿Imports System

Public Class OutOfHoursSearchBO

#Region "Attributes"

    Private _typeId As Integer
    Private _type As String
    Private _startDate As String
    Private _endDate As String
    Private _from As String
    Private _to As String

#End Region

#Region "Properties"

    Public Property TypeId() As Integer
        Get
            Return _typeId
        End Get
        Set(ByVal value As Integer)
            _typeId = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property

    Public Property FromTime() As String
        Get
            Return _from
        End Get
        Set(ByVal value As String)
            _from = value
        End Set
    End Property

    Public Property ToTime() As String
        Get
            Return _to
        End Get
        Set(ByVal value As String)
            _to = value
        End Set
    End Property

#End Region

End Class

