﻿Public Class TempDefectDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public appliance As String = String.Empty
    Public trade As String = String.Empty
    Public twoPersons As String = String.Empty
    Public disconnected As String = String.Empty
    Public durationString As String = String.Empty
    Public partsDueString As String = String.Empty
    Public tradeId As Integer = 0
    Public defectId As Integer = 0
    Public defectDurationHrs As Double = 0
    Public ReqType As String = String.Empty
    'Public dueDateString As String = String.Empty
    'Public tempFaultId As Integer = 0
    'Public priorityName As String = String.Empty
    'Public completeDueDate As Date


    Public Const applianceColName As String = "Appliance:"
    Public Const tradeColName As String = "Trade:"
    Public Const twoPersonsColName As String = "2 Person:"
    Public Const disconnectedColName As String = "Disconnected:"
    Public Const durationStringColName As String = "Duration:"
    Public Const partsDueColName As String = "Parts Due:"
    Public Const tradeIdColName As String = "TradeID:"
    Public Const DefectIdColName As String = "DefectId:"
    Public Const defectdurationColName As String = "DefectDuration"
    Public Const reqTypeColName As String = "ReqType:"
    'Public Const priorityColName As String = "PriorityName:"
    'Public Const completeDueDateColName As String = "CompleteDueDate:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(applianceColName) = appliance
        dr(tradeColName) = trade
        dr(twoPersonsColName) = twoPersons
        dr(disconnectedColName) = disconnected
        dr(durationStringColName) = durationString
        dr(partsDueColName) = partsDueString
        dr(tradeIdColName) = tradeId
        dr(DefectIdColName) = defectId

        dr(defectdurationColName) = defectDurationHrs
        dr(reqTypeColName) = ReqType
        'dr(completeDueDateColName) = completeDueDate

        dt.Rows.Add(dr)
    End Sub

    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(applianceColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(twoPersonsColName)
        dt.Columns.Add(disconnectedColName)
        dt.Columns.Add(durationStringColName)
        dt.Columns.Add(partsDueColName)
        dt.Columns.Add(tradeIdColName)
        dt.Columns.Add(DefectIdColName)
        dt.Columns.Add(defectdurationColName)
        dt.Columns.Add(reqTypeColName)
        'dt.Columns.Add(completeDueDateColName)

    End Sub
End Class


