﻿
Namespace AS_BusinessObject
    Public Class DefectReportBo
        Private _searchText As String
        Public Property SearchText() As String
            Get
                Return _searchText
            End Get
            Set(ByVal value As String)
                _searchText = value
            End Set
        End Property

        Private _schemeId As Integer
        Public Property SchemeId() As Integer
            Get
                Return _schemeId
            End Get
            Set(ByVal value As Integer)
                _schemeId = value
            End Set
        End Property

        Private _applianceType As String
        Public Property ApplianceType() As String
            Get
                Return _applianceType
            End Get
            Set(ByVal value As String)
                _applianceType = value
            End Set
        End Property

        Private _defectType As String
        Public Property DefectType() As String
            Get
                Return _defectType
            End Get
            Set(ByVal value As String)
                _defectType = value
            End Set
        End Property

        Private _year As Integer
        Public Property Year() As Integer
            Get
                Return _year
            End Get
            Set(ByVal value As Integer)
                _year = value
            End Set
        End Property

        Private _appointmentStatusId As Integer
        Public Property AppointmentStatusId() As Integer
            Get
                Return _appointmentStatusId
            End Get
            Set(ByVal value As Integer)
                _appointmentStatusId = value
            End Set
        End Property


    End Class
End Namespace
