﻿Public Class PropertyDetailsBO

#Region "Attributes"

    Private _refNumber As String
    Private _address As String
    Private _postCode As String
    Private _dev As String
    Private _scheme As String
    Private _status As String
    Private _type As String
#End Region

#Region "Construtor"

    Public Sub New()
        _refNumber = String.Empty
        _address = String.Empty
        _postCode = String.Empty
        _dev = String.Empty
        _scheme = String.Empty
        _status = String.Empty
        _type = String.Empty
    End Sub

#End Region

#Region "Properties"
    ' Get / Set property for _refNumber
    Public Property RefNumber() As String

        Get
            Return _refNumber
        End Get

        Set(ByVal value As String)
            _refNumber = value
        End Set

    End Property
   
    ' Get / Set property for _address
    Public Property Address() As String

        Get
            Return _address
        End Get

        Set(ByVal value As String)
            _address = value
        End Set

    End Property
    ' Get / Set property for _postCode
    Public Property PostCode() As String

        Get
            Return _postCode
        End Get

        Set(ByVal value As String)
            _postCode = value
        End Set

    End Property
    ' Get / Set property for _dev
    Public Property Dev() As String

        Get
            Return _dev
        End Get

        Set(ByVal value As String)
            _dev = value
        End Set

    End Property
    ' Get / Set property for _scheme
    Public Property Scheme() As String

        Get
            Return _scheme
        End Get

        Set(ByVal value As String)
            _scheme = value
        End Set

    End Property
    ' Get / Set property for _status
    Public Property Status() As String

        Get
            Return _status
        End Get

        Set(ByVal value As String)
            _status = value
        End Set

    End Property
    ' Get / Set property for _type
    Public Property Type() As String

        Get
            Return _type
        End Get

        Set(ByVal value As String)
            _type = value
        End Set

    End Property
#End Region

End Class
