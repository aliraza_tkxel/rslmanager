﻿Public Class PropertiesDetailsBO

#Region "Properties"
    Public Property ValueId As Integer
    Public Property UpdatedBy As Nullable(Of Integer)
    Public Property PropertyId As String
    Public Property TemplateId As String
    Public Property DevelopmentName As String
    Public Property DevelopmentId As Integer
    Public Property BlockName As String
    Public Property BlockId As Nullable(Of Integer)
    Public Property PhaseId As Nullable(Of Integer)
    Public Property PhaseName As String
    Public Property House As String
    Public Property Address1 As String
    Public Property Address2 As String
    Public Property Address3 As String
    Public Property Town As String
    Public Property County As String
    Public Property PostCode As String
    Public Property DefectPeriod As Nullable(Of Date)
    Public Property StockType As Nullable(Of Integer)
    Public Property Ownership As Nullable(Of Integer)
    Public Property LeaseStart As Nullable(Of Date)
    Public Property LeaseEnd As Nullable(Of Date)
    Public Property RightToAcquire As Nullable(Of Integer)
    Public Property FloodingRisk As Nullable(Of Boolean)
    Public Property Status As Nullable(Of Integer)
    Public Property SubStatus As Nullable(Of Integer)
    Public Property PropertyType As Nullable(Of Integer)
    Public Property DwellingType As Nullable(Of Integer)
    Public Property Level As Nullable(Of Integer)
    Public Property AssetType As Nullable(Of Integer)
    Public Property AssetTypeMain As Nullable(Of Integer)
    Public Property AssetTypeSub As Nullable(Of Integer)
    Public Property MinPurchase As Nullable(Of Decimal)
    Public Property PurchaseLevel As Nullable(Of Decimal)
    Public Property DateAcquired As Nullable(Of Date)
    Public Property DateBuild As Nullable(Of Date)
    Public Property ViewingCommencement As Nullable(Of Date)


    Public Property GroundRent As String
    Public Property LandLord As String
    Public Property RentReview As Nullable(Of Date)
    Public Property IsTemplate As Nullable(Of Boolean)
    Public Property TemplateName As String
    Public Property TitleNumber As String
    Public Property DeedLocation As String
    Public Property PropertyPicId As Nullable(Of Integer) 'new field
    Public Property ImageName As String 'new field
    Public Property ImagePath As String  'new field
    Public Property CreatedBy As String  'new field
    Public Property IsDefault As Nullable(Of Boolean)   'new field
    Public Property TenureType As String ' new field

#End Region





End Class
