﻿Namespace AS_BusinessObject
    Public Class PropertyAsbestosBO
#Region "Private"
        Private _asbestosLevelId As Integer
        Private _asbestosElementId As Integer
        Private _riskId As String
        Private _addedDate As Date
        Private _removedDate As String
        Private _notes As String
        Private _propertyId As String
        Private _userId As Integer
        Private _propasbLevelId As String
#End Region
#Region "Public Properties"
        'Property Asbestos Level Id
        Public Property AsbestosLevelId() As Integer
            Get
                Return _asbestosLevelId
            End Get
            Set(value As Integer)
                _asbestosLevelId = value
            End Set
        End Property

        'Property Asbestos Element Id
        Public Property AsbestosElementId() As Integer
            Get
                Return _asbestosElementId
            End Get
            Set(value As Integer)
                _asbestosElementId = value
            End Set
        End Property

        'Property Asbestos Risk Id
        Public Property RiskId() As String
            Get
                Return _riskId
            End Get
            Set(value As String)
                _riskId = value
            End Set
        End Property
        'Property Asbestos Added Date
        Public Property AddedDate() As Date
            Get
                Return _addedDate
            End Get
            Set(value As Date)
                _addedDate = value
            End Set
        End Property
        'Property Asbestos Removed Date
        Public Property RemovedDate() As String
            Get
                Return _removedDate
            End Get
            Set(value As String)
                _removedDate = value
            End Set
        End Property
        'Property Asbestos Note
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(value As String)
                _notes = value
            End Set
        End Property
        'Property Id
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(value As String)
                _propertyId = value
            End Set
        End Property
        'User Id
        Public Property UserId() As Integer
            Get
                Return _userId
            End Get
            Set(value As Integer)
                _userId = value
            End Set
        End Property
        'Propasb Level Id
        Public Property PropasbLevelID() As String
            Get
                Return _propasbLevelId
            End Get
            Set(ByVal value As String)
                _propasbLevelId = value
            End Set
        End Property

        Private _riskLevel As Integer
        Public Property RiskLevel() As Integer
            Get
                Return _riskLevel
            End Get
            Set(ByVal value As Integer)
                _riskLevel = value
            End Set
        End Property

        Private _UrgentActionRequired As Boolean
        Public Property UrgentActionRequired() As Boolean
            Get
                Return _UrgentActionRequired
            End Get
            Set(ByVal value As Boolean)
                _UrgentActionRequired = value
            End Set
        End Property



#End Region
    End Class
End Namespace

