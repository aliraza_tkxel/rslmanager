﻿
Public Class CommonAddressBO

    Public Property Street() As String = String.Empty
    
    Public Property Address() As String = String.Empty

    Public Property Area() As String = String.Empty
    
    Public Property City() As String = String.Empty

    Public Property PostCode() As String = String.Empty

    Public Property PatchId() As Integer = -1

    Public Property PatchName() As String = String.Empty

    Public Property PropertyId As String = String.Empty
    Public Property Telephone As String = String.Empty
End Class