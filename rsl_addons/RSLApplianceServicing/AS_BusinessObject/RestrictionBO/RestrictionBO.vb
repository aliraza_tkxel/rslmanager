﻿
Public Class RestrictionBO
    Public Property RestrictionId As Integer
    Public Property PermittedPlanning As String
    Public Property RelevantPlanning As String
    Public Property RelevantTitle As String
    Public Property RestrictionComments As String
    Public Property AccessIssues As String
    Public Property MediaIssues As String
    Public Property ThirdPartyAgreement As String
    Public Property SpFundingArrangements As String
    Public Property IsRegistered As Integer?
    Public Property ManagementDetail As String
    Public Property NonBhaInsuranceDetail As String
    Public Property SchemeId As Integer?
    Public Property BlockId As Integer?
    Public Property PropertyId As String
    Public Property UpdatedBy As Integer
End Class
