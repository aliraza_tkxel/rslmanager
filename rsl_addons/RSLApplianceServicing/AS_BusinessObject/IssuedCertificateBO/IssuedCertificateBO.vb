﻿Namespace AS_BusinessObject
    <Serializable()> _
    Public Class IssuedCertificateBO : Inherits BaseBO

#Region "Attributes"

        Private _fromDate As DateTime
        Private _toDate As DateTime

        Private _searchText As String
        Private _heatingTypeId As Integer

#End Region

#Region "Constructors"

        Sub New()

            'Set the from date to 1st of current mounth and to date end date to last day of current month.
            'Set the from time to 00:00:00 (midnight) and to Time to 23:59:59 (midnight)
            'Set the searchText to the default values that is an Empty String(String.Empty)

            _fromDate = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0)
            _toDate = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 23, 59, 59)
            _searchText = String.Empty
            _heatingTypeId = -1

        End Sub

        Sub New(ByRef paramFromDate As DateTime, ByRef paramToDate As DateTime, Optional ByRef paramsearchText As String = "", Optional ByRef heatingTypeId As Integer = 0)

            'Set the from date to date provided(paramFromDate) and end date to the date provided (paramToDate).
            'Set the from time to 00:00:00 (midnight) and to Time to 23:59:59 (midnight)
            'Set the searchText to the default values that is an Empty String(String.Empty)

            _fromDate = New DateTime(paramFromDate.Year, paramFromDate.Month, paramFromDate.Day, 0, 0, 0)
            _toDate = New DateTime(paramToDate.Year, paramToDate.Month, paramToDate.Day, 23, 59, 59)
            _searchText = paramsearchText
            _heatingTypeId = -1
        End Sub

#End Region

#Region "Properties"

#Region "Get / Set Property fromDate"

        Public Property fromDate() As DateTime
            Get
                Return _fromDate
            End Get
            Set(ByVal value As DateTime)
                'This statment will override the time provided to set value. And set it to 00:00:00 (midnight)
                'To avoid time override kindly unncomment this "_fromDate = value" and comment next line
                '_fromDate = value
                _fromDate = New DateTime(value.Year, value.Month, value.Day, 0, 0, 0)
            End Set
        End Property

#End Region

#Region "Get / Set Propert toDate"

        Public Property toDate() As DateTime
            Get
                Return _toDate
            End Get
            Set(ByVal value As DateTime)
                'This statment will override the time provided to set value. And set it to 23:59:59 (midnight)
                'To avoid time override kindly unncomment this "_toDate = value" and comment next line
                '_toDate = value
                _toDate = New DateTime(value.Year, value.Month, value.Day, 0, 0, 0)
            End Set
        End Property

#End Region

#Region "Get / Set Property searchText"

        Public Property searchText() As String
            Get
                Return _searchText
            End Get
            Set(ByVal value As String)
                _searchText = value
            End Set
        End Property

#End Region
#Region "Get / Set Property heating type id"

        Public Property heatingTypeId() As Integer
            Get
                Return _heatingTypeId

            End Get
            Set(ByVal value As Integer)
                _heatingTypeId = value
            End Set
        End Property

#End Region
#End Region

    End Class
End Namespace