﻿Imports AS_BusinessObject

Public Class AppointmentBO : Inherits BaseBO

#Region "Properties"

    ''' <summary>
    ''' Property to hold operative Id
    ''' </summary>
    ''' <value>Integar: OperativeId</value>
    ''' <returns>Integar: OperativeId</returns>
    ''' <remarks></remarks>
    Public Property OperativeId As Integer

    ''' <summary>
    ''' Property to hold operative name
    ''' </summary>
    ''' <value>String: Operative Name</value>
    ''' <returns>String: Operative Name</returns>
    ''' <remarks></remarks>
    Public Property Operative() As String

    ''' <summary>
    ''' Property to hold Duration
    ''' </summary>
    ''' <value>Double: Duration</value>
    ''' <returns>Double: Duration</returns>
    ''' <remarks></remarks>
    Public Property Duration() As Double

    ''' <summary>
    ''' Property to hold Appointment Start Time
    ''' </summary>
    ''' <value>String: Appointment Start Time</value>
    ''' <returns>String: Appointment Start Time</returns>
    ''' <remarks></remarks>
    Public Property StartTime() As String

    ''' <summary>
    ''' Property to hold Appointment End Time
    ''' </summary>
    ''' <value>String: Appointment End Time</value>
    ''' <returns>String: Appointment End Time</returns>
    ''' <remarks></remarks>
    Public Property EndTime() As String

    ''' <summary>
    ''' Property to hold Appointment Start Date
    ''' </summary>
    ''' <value>DateTime: Appointment Start Date</value>
    ''' <returns>DateTime: Appointment Start Date</returns>
    ''' <remarks></remarks>
    Public Property AppointmentStartDate() As DateTime

    ''' <summary>
    ''' Property to hold Appointment End Time
    ''' </summary>
    ''' <value>DateTime: Appointment End Date</value>
    ''' <returns>DateTime: Appointment End Date</returns>
    ''' <remarks></remarks>
    Public Property AppointmentEndDate As DateTime

    ''' <summary>
    ''' Property to hold Job Sheet Notes
    ''' </summary>
    ''' <value>String: Job Sheet Notes</value>
    ''' <returns>String: Job Sheet Notes</returns>
    ''' <remarks></remarks>
    Public Property JobSheetNotes() As String

    ''' <summary>
    ''' Property to hold Appointment ID
    ''' </summary>
    ''' <value>Integer: AppointmentId</value>
    ''' <returns>Integer: AppointmentId</returns>
    ''' <remarks></remarks>
    Public Property AppointmentId() As Integer

    ' ''' <summary>
    ' ''' Property to hold Fault Log(s) List in a datatable
    ' ''' </summary>
    ' ''' <value>DataTable: Containing FaultLog(s)</value>
    ' ''' <returns>DataTable: Containing FaultLog(s)</returns>
    ' ''' <remarks></remarks>
    'Public Property FaultLogdt As DataTable

    ''' <summary>
    ''' Property to hold IsCalendarAppointment Flag values
    ''' </summary>
    ''' <value>Boolean: IsCalendarAppointment</value>
    ''' <returns>Boolean: IsCalendarAppointment</returns>
    ''' <remarks></remarks>
    Public Property IsCalendarAppointment As Boolean

    ''' <summary>
    ''' Property to hold job sheet notes
    ''' </summary>
    ''' <value>job sheet notes</value>
    ''' <returns>job sheet notes</returns>
    ''' <remarks></remarks>
    Public Property AppointmentNotes() As String

    ''' <summary>
    ''' Property to hold schedular's EmployeeId, it is id of logged in user.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SchedularId() As Integer

    Public Property TradeId As Integer

#End Region

#Region "Enums"

    Public Enum AppointmentParameter
        OperativeId = 0
        Operative = 1
        AppointmentStartDateTime = 2
        AppointmentEndDateTime = 3
        selectedTempFaultIds = 4
        tradeIds = 5
        appointmentBlockId = 6

        'Parameter array has the following indexes
        'zero index = operative id, 
        '1st index = operative name, 
        '2nd index = appointment Start Date time, 
        '3rd index = appointment End Date time,
        '4th index = selected temp fault ids, 
        '5th index = trade ids
        '6th index = appointmentBlockId (In session list)
    End Enum

#End Region

End Class
