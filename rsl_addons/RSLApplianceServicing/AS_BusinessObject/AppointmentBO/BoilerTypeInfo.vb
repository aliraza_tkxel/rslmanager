﻿Imports AS_BusinessObject

Public Class BoilerTypeInfo : Inherits BaseBO

#Region "Attributes"
    Private _appointmentType As String
    Private _boilerCount As String

#End Region

#Region "Construtor"
    Public Sub New()
        _appointmentType = String.Empty
        _boilerCount = String.Empty
    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _faultslist
    Public Property AppointmentType() As String

        Get
            Return _appointmentType
        End Get

        Set(ByVal value As String)
            _appointmentType = value
        End Set

    End Property

    Public Property BoilerCount() As String

        Get
            Return _boilerCount
        End Get

        Set(ByVal value As String)
            _boilerCount = value
        End Set

    End Property

#End Region

End Class
