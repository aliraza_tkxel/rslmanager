﻿Imports AS_BusinessObject

Public Class ServicingType : Inherits BaseBO

#Region "Attributes"
    Private _isAlternative As Boolean
    Private _isOil As Boolean

#End Region

#Region "Construtor"
    Public Sub New(ByVal isOilType As Boolean, ByVal isAlternativeType As Boolean)
        IsAlternative = isAlternativeType
        IsOil = isOilType
    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _faultslist
    Public Property IsAlternative() As Boolean

        Get
            Return _isAlternative
        End Get

        Set(ByVal value As Boolean)
            _isAlternative = value
        End Set

    End Property

    Public Property IsOil() As Boolean

        Get
            Return _isOil
        End Get

        Set(ByVal value As Boolean)
            _isOil = value
        End Set

    End Property

#End Region

End Class
