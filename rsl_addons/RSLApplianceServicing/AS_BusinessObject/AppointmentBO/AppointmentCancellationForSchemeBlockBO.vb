﻿Imports AS_BusinessObject

Public Class AppointmentCancellationForSchemeBlockBO : Inherits BaseBO
#Region "Attributes"
    Private _appointmentType As String
    Private _notes As String
    Private _userId As Integer
    Private _appointmentId As Integer

#End Region

#Region "Construtor"
    Public Sub New()
        _appointmentType = String.Empty
        _notes = String.Empty
        _userId = 0
        _appointmentId = 0
    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _faultslist
    Public Property AppointmentType() As String

        Get
            Return _appointmentType
        End Get

        Set(ByVal value As String)
            _appointmentType = value
        End Set

    End Property
    ' Get / Set property for _notes
    Public Property Notes() As String

        Get
            Return _notes
        End Get

        Set(ByVal value As String)
            _notes = value
        End Set

    End Property
    Public Property userId() As Integer

        Get
            Return _userId
        End Get

        Set(ByVal value As Integer)
            _userId = value
        End Set

    End Property

    Public Property AppointmentId() As Integer

        Get
            Return _appointmentId
        End Get

        Set(ByVal value As Integer)
            _appointmentId = value
        End Set

    End Property

#End Region
End Class
