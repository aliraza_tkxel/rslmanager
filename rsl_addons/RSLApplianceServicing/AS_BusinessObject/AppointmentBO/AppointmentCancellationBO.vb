﻿Imports AS_BusinessObject

Public Class AppointmentCancellationBO : Inherits BaseBO
#Region "Attributes"
    Private _faultslist As String
    Private _notes As String
    Private _userId As Integer

#End Region

#Region "Construtor"
    Public Sub New()
        _faultslist = String.Empty
        _notes = String.Empty
        _userId = 0
    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _faultslist
    Public Property FaultsList() As String

        Get
            Return _faultslist
        End Get

        Set(ByVal value As String)
            _faultslist = value
        End Set

    End Property
    ' Get / Set property for _notes
    Public Property Notes() As String

        Get
            Return _notes
        End Get

        Set(ByVal value As String)
            _notes = value
        End Set

    End Property
    Public Property userId() As Integer

        Get
            Return _userId
        End Get

        Set(ByVal value As Integer)
            _userId = value
        End Set

    End Property

#End Region
End Class
