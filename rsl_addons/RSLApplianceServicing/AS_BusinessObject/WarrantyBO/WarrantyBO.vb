﻿Public Class WarrantyBO

    Private _warrantyType As Integer

    Public Property WarrantyType As Integer
        Get
            Return _warrantyType
        End Get
        Set(ByVal value As Integer)
            _warrantyType = value
        End Set
    End Property

    Private _category As Integer

    Public Property Category As Integer
        Get
            Return _category
        End Get
        Set(ByVal value As Integer)
            _category = value
        End Set
    End Property

    Private _area As Integer?

    Public Property Area As Integer?
        Get
            Return _area
        End Get
        Set(ByVal value As Integer?)
            _area = value
        End Set
    End Property

    Private _item As Integer?

    Public Property Item As Integer?
        Get
            Return _item
        End Get
        Set(ByVal value As Integer?)
            _item = value
        End Set
    End Property

    Private _contractor As Integer

    Public Property Contractor As Integer
        Get
            Return _contractor
        End Get
        Set(ByVal value As Integer)
            _contractor = value
        End Set
    End Property

    Private _expiry As DateTime

    Public Property Expiry As DateTime
        Get
            Return _expiry
        End Get
        Set(ByVal value As DateTime)
            _expiry = value
        End Set
    End Property

    Private _notes As String

    Public Property Notes As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property

    Private _id As String

    Public Property Id As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Private _requestType As String

    Public Property RequestType As String
        Get
            Return _requestType
        End Get
        Set(ByVal value As String)
            _requestType = value
        End Set
    End Property

    Private _warrantyId As Integer

    Public Property WarrantyId As Integer
        Get
            Return _warrantyId
        End Get
        Set(ByVal value As Integer)
            _warrantyId = value
        End Set
    End Property
End Class
