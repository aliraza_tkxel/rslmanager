﻿Namespace AS_BusinessObject
    Public Class ItemNotesBO

#Region "Private"
        Private _notesId As Integer
        Private _propertyId As String
        Private _notes As String
        Private _itemId As Integer
        Private _createdBy As Integer
        Private _showinScheduling As Boolean
        Private _showOnApp As Boolean

#End Region

#Region "Public properties"
        'NotesId
        Public Property NotesId() As Integer
            Get
                Return _notesId
            End Get
            Set(ByVal value As Integer)
                _notesId = value
            End Set
        End Property

        'Property Id
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        'item Id
        Public Property ItemId() As Integer
            Get
                Return _itemId
            End Get
            Set(ByVal value As Integer)
                _itemId = value
            End Set
        End Property

        'item Id
        Public Property CreatedBy() As Integer
            Get
                Return _createdBy
            End Get
            Set(ByVal value As Integer)
                _createdBy = value
            End Set
        End Property

        'Notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        Public Property ShowinScheduling() As Boolean
            Get
                Return _showinScheduling
            End Get
            Set(ByVal value As Boolean)
                _showinScheduling = value
            End Set
        End Property
        Public Property ShowOnApp() As String
            Get
                Return _showOnApp
            End Get
            Set(ByVal value As String)
                _showOnApp = value
            End Set
        End Property
        Public Property HeatingMappingId As Nullable(Of Integer)
#End Region

    End Class
End Namespace
