﻿Namespace AS_BusinessObject
    Public Class DetectorsBO
        Private _loaction As String
        Public Property Location() As String
            Get
                Return _loaction
            End Get
            Set(ByVal value As String)
                _loaction = value
            End Set
        End Property

        Private _Manufacturer As String
        Public Property Manufacturer() As String
            Get
                Return _Manufacturer
            End Get
            Set(ByVal value As String)
                _Manufacturer = value
            End Set
        End Property

        Private _SerialNumber As String
        Public Property SerialNumber() As String
            Get
                Return _SerialNumber
            End Get
            Set(ByVal value As String)
                _SerialNumber = value
            End Set
        End Property
        Private _PowerSource As Integer
        Public Property PowerSource() As Integer
            Get
                Return _PowerSource
            End Get
            Set(ByVal value As Integer)
                _PowerSource = value
            End Set
        End Property
        Public Property InstalledDate As Nullable(Of Date)
        
        Private _IsLandlordsDetector As Boolean
        Public Property IsLandlordsDetector() As Boolean
            Get
                Return _IsLandlordsDetector
            End Get
            Set(ByVal value As Boolean)
                _IsLandlordsDetector = value
            End Set
        End Property
        Public Property LastTestedDate As Nullable(Of Date)
        Public Property BatteryReplaced As Nullable(Of Date)
        Public Property Notes As String
        Private _IsPassed As Boolean
        Public Property IsPassed() As Boolean
            Get
                Return _IsPassed
            End Get
            Set(ByVal value As Boolean)
                _IsPassed = value
            End Set
        End Property
        Private _PropertyId As String
        Public Property PropertyId() As String
            Get
                Return _PropertyId
            End Get
            Set(ByVal value As String)
                _PropertyId = value
            End Set
        End Property

        Private _DetectorType As String
        Public Property DetectorType() As String
            Get
                Return _DetectorType
            End Get
            Set(ByVal value As String)
                _DetectorType = value
            End Set
        End Property












    End Class
End Namespace
