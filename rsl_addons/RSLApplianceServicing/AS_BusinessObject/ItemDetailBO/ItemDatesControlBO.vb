﻿Namespace AS_BusinessObject
    Public Class ItemDatesControlBO

#Region "Private"
        Private _paramId As Integer
        Private _lastDone As String
        Private _dueDate As String
        Private _parameterName As String
        Private _componentId As Integer
#End Region

#Region "Public Properties"
        ' Param ID
        Public Property ParameterId() As Integer
            Get
                Return _paramId
            End Get
            Set(ByVal value As Integer)
                _paramId = value
            End Set
        End Property
        'Last Done
        Public Property LastDone() As String
            Get
                Return _lastDone
            End Get
            Set(ByVal value As String)
                _lastDone = value
            End Set
        End Property

        'Due Date
        Public Property DueDate() As String
            Get
                Return _dueDate
            End Get
            Set(ByVal value As String)
                _dueDate = value
            End Set
        End Property

        'Parameter Name
        Public Property ParameterName() As String
            Get
                Return _parameterName
            End Get
            Set(ByVal value As String)
                _parameterName = value
            End Set
        End Property
        ' component ID
        Public Property ComponentId() As Integer
            Get
                Return _componentId
            End Get
            Set(ByVal value As Integer)
                _componentId = value
            End Set
        End Property

#End Region
#Region "Constructors"

        Sub New()
            _dueDate = String.Empty
            _lastDone = String.Empty
        End Sub
#End Region
    End Class
End Namespace
