﻿Namespace AS_BusinessObject
    Public Class ItemDetailBO

#Region "Private"
        Private _itemParamId As Integer
        Private _parameterValue As String
        Private _valueId As Integer
        Private _isCheckBoxSelected As Boolean

#End Region

#Region "Public Properties"
        'Item Param ID
        Public Property ItemParamId() As Integer
            Get
                Return _itemParamId
            End Get
            Set(ByVal value As Integer)
                _itemParamId = value
            End Set
        End Property
        'Parameter Value
        Public Property ParameterValue() As String
            Get
                Return _parameterValue
            End Get
            Set(ByVal value As String)
                _parameterValue = value
            End Set
        End Property

        'Value ID
        Public Property ValueId() As Integer
            Get
                Return _valueId
            End Get
            Set(ByVal value As Integer)
                _valueId = value
            End Set
        End Property

        'Is Check Box Selected
        Public Property IsCheckBoxSelected() As Boolean
            Get
                Return _isCheckBoxSelected
            End Get
            Set(ByVal value As Boolean)
                _isCheckBoxSelected = value
            End Set
        End Property

#End Region

    End Class
End Namespace
