﻿Namespace AS_BusinessObject
    Public Class ConditionRatingBO
#Region "Private"
        Private _worksRequired As String
        Private _ComponentId As Integer
        Private _ValueId As Integer
#End Region

#Region "Public Properties"
        'Item Param ID
        Public Property ComponentId() As Integer
            Get
                Return _ComponentId
            End Get
            Set(ByVal value As Integer)
                _ComponentId = value
            End Set
        End Property
        'WorksRequired
        Public Property WorksRequired() As String
            Get
                Return _worksRequired
            End Get
            Set(ByVal value As String)
                _worksRequired = value
            End Set
        End Property

        'Value ID
        Public Property ValueId() As Integer
            Get
                Return _valueId
            End Get
            Set(ByVal value As Integer)
                _valueId = value
            End Set
        End Property

       
#End Region
    End Class
End Namespace
