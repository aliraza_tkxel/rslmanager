﻿Namespace AS_BusinessObject
    Public Class ItemAttributeBO
#Region "Private"
        Private _propertyId As String
        Private _itemId As Integer
        Private _updatedBy As Integer

#End Region

#Region "Public properties"
        'Property Id
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        'item Id
        Public Property ItemId() As Integer
            Get
                Return _itemId
            End Get
            Set(ByVal value As Integer)
                _itemId = value
            End Set
        End Property

        'item Id
        Public Property UpdatedBy() As Integer
            Get
                Return _updatedBy
            End Get
            Set(ByVal value As Integer)
                _updatedBy = value
            End Set
        End Property
        Public Property HeatingMappingId As Nullable(Of Integer)
         
#End Region

    End Class
End Namespace

