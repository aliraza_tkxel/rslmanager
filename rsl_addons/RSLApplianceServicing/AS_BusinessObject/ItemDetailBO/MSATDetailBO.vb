﻿Namespace AS_BusinessObject
    Public Class MSATDetailBO

        Public Property IsRequired As Boolean
        Public Property LastDate As Nullable(Of DateTime)
        Public Property Cycle As Nullable(Of Integer)
        Public Property CycleTypeId As Nullable(Of Integer)
        Public Property CycleCost As Nullable(Of Decimal)
        Public Property NextDate As Nullable(Of DateTime)
        Public Property AnnualApportionment As Nullable(Of Decimal)
        Public Property MSATTypeId As Integer

    End Class
End Namespace
