﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities


Namespace AS_BusinessLogic

    Public Class CustomerBLManager

#Region "Functions"

#Region "GetCustomerDetails"

        ' Function gets called to retrieve information to be displayed on customer details page
        Public Function GetCustomerDetails(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailedInfo As Boolean) As Boolean

            Try

                'Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                'Call DAL method to retrieve information
                objCustomerDAL.GetCustomerDetailsInfo(custDetailsBO, True)

                If (custDetailsBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

                Throw

            End Try

        End Function

#End Region

#Region "Update customer address"
        Public Function updateAddress(ByVal objCustomerBO As CustomerBO)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            Return objCustomerDAL.updateAddress(objCustomerBO)

        End Function

#End Region


#End Region

    End Class

End Namespace
