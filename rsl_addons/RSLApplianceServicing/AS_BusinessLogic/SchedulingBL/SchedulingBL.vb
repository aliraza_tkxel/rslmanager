﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities
Imports System.Globalization

Namespace AS_BusinessLogic

    Public Class SchedulingBL
        Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()

#Region "Functions"

#Region "get Appointment To Be Arranged List"
        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal fuelType As Integer, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)

            Try

                Return objSchedulingDAL.getAppointmentToBeArrangedList(resultDataSet, check56Days, searchText, objPageSortBo, fuelType, status, patchId, developmentId)

            Catch ex As Exception

                Throw
            End Try



        End Function

#End Region

#Region "Get Boilers Info of Scheme/Block "
        Public Sub getSchemeBlockBoilersInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer, ByVal appointmentType As String)

            Try

                objSchedulingDAL.getSchemeBlockBoilersInfo(resultDataSet, journalId, appointmentType)

            Catch ex As Exception

                Throw
            End Try


        End Sub

#End Region

#Region "Get Heating Info For Alternative"
        Public Sub GetHeatingInfoForAlternative(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Try
                objSchedulingDAL.GetHeatingInfoForAlternative(resultDataSet, journalId)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

#Region "Get Heating Info For Alternative"
        Public Sub GetHeatingInfoForOil(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Try
                objSchedulingDAL.GetHeatingInfoForOil(resultDataSet, journalId)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region


#Region "get Property schedule status"
        Public Function getPropertyschedulestatus(ByVal propertyId As String, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)

            Try

                Return objSchedulingDAL.getPropertyschedulestatus(propertyId, istimeexceeded, isScheduled, appointmentType)

            Catch ex As Exception

                Throw
            End Try



        End Function

#End Region

#Region "set Property schedule status"
        Public Function setPropertyschedulestatus(ByVal propertyId As String, ByVal lock As Integer, ByRef schedulestatus As Integer, ByVal lockedBy As Integer, ByVal appointmentType As String)

            Try

                Return objSchedulingDAL.setPropertyschedulestatus(propertyId, lock, schedulestatus, lockedBy, appointmentType)

            Catch ex As Exception

                Throw
            End Try

        End Function

#End Region

#Region "Save Gas Servicing Cancellation"
        Public Function saveGasServicingCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Return objSchedulingDAL.saveGasServicingCancellation(appointmentCancellation)

        End Function
#End Region

#Region "Save Gas Servicing Cancellation"
        Public Function saveGasServicingCancellationForSchemeBlock(ByVal appointmentCancellationForSchemeBlock As AppointmentCancellationForSchemeBlockBO)

            Return objSchedulingDAL.saveGasServicingCancellationForSchemeBlock(appointmentCancellationForSchemeBlock)

        End Function
#End Region

#Region "Save Gas Servicing Cancellation for assign to contractor"
        Public Function saveGasServicingCancellationforContractors(ByVal appointmentCancellation As AppointmentCancellationBO)

            Return objSchedulingDAL.saveGasServicingCancellationforContractors(appointmentCancellation)

        End Function
#End Region

#Region "get Arranged Property schedule status"
        Public Function getArrangedPropertyschedulestatus(ByVal propertyId As String, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)

            Try

                Return objSchedulingDAL.getArrangedPropertyschedulestatus(propertyId, istimeexceeded, isScheduled, appointmentType)

            Catch ex As Exception

                Throw
            End Try

        End Function

#End Region

#Region "get Property Locked By"
        Public Function getPropertyLockedBy(ByVal propertyId As String, ByVal appointmentType As String)

            Try

                Return objSchedulingDAL.getPropertyLockedBy(propertyId, appointmentType)

            Catch ex As Exception

                Throw
            End Try

        End Function

#End Region

#Region "get void Appointment To Be Arranged List"
        Public Function getVoidAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Try

                Return objSchedulingDAL.getVoidAppointmentToBeArrangedList(resultDataSet, searchText, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try



        End Function

#End Region


#Region "get Risk List"
        Public Sub getRiskList(ByRef resultDataSet As DataSet, ByVal customerId As Integer)

            Try
                objSchedulingDAL.getRiskList(resultDataSet, customerId)
            Catch ex As Exception
                Throw
            End Try

        End Sub

#End Region

#Region "get abort reason"
        Public Sub getAbortData(ByRef resultDataSet As DataSet, ByVal customerId As Integer)

            Try
                objSchedulingDAL.getAbortReason(resultDataSet, customerId)
            Catch ex As Exception
                Throw
            End Try

        End Sub

#End Region


#Region "get Appointments Arranged List"
        Public Function getAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal fuelType As Integer, ByVal developmentId As Integer)
            Try

                Return objSchedulingDAL.getAppointmentArrangedList(resultDataSet, check56Days, searchedText, objPageSortBo, patchId, schemeId, fuelType, developmentId)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region


#Region "get void Appointments Arranged List"
        Public Function getVoidAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, Optional ByVal status As String = "")
            Try
                Return objSchedulingDAL.getVoidAppointmentArrangedList(resultDataSet, searchedText, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region


#Region "Get Appointment Detail"
        Public Sub getAppointmentDetail(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)
            objSchedulingDAL.getAppointmentDetail(resultDataSet, appointmentId)
        End Sub
#End Region

#Region "Save Appointment Notes"
        Public Sub saveAppointmentNotes(ByVal appointmentId As Integer, ByVal notes As String)
            objSchedulingDAL.saveAppointmentNotes(appointmentId, notes)
        End Sub
#End Region

#Region "get Searched Values"
        Public Sub getSeacrhedValues(ByRef resultDataSet As DataSet, ByVal searchedText As String)
            'Dim objAppointmentToBeArranged As AppointmentToBeArranged = New AppointmentToBeArranged()

            Try
                objSchedulingDAL.getSeacrhedValues(resultDataSet, searchedText)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Engineers Info for Appointment"
        Public Sub getEngineersListForAppointment(ByRef resultDataSet As DataSet)
            Try
                objSchedulingDAL.getEngineersListForAppointment(resultDataSet)
            Catch ex As Exception

                Throw
            End Try

        End Sub
#End Region

#Region "get Scheduled Appointments Details"
        Public Sub getScheduledAppointmentsDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Try
                objSchedulingDAL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, resultDataSet)
            Catch ex As Exception

                Throw New Exception(ex.Message + "start date=" + objScheduleAppointmentsBO.StartDate.ToString() + ", return date=" + objScheduleAppointmentsBO.ReturnDate.ToString(), ex.InnerException)
            End Try
        End Sub
#End Region

#Region "get Engineer Leaves Details"
        Public Sub getEngineerLeavesDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Try
                objSchedulingDAL.getEngineerLeavesDetail(objScheduleAppointmentsBO, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Create New Appointment "
        Public Function saveAppointmentInfo(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef savedLetterDt As DataTable, Optional ByRef appointmentId As Int32 = 0)

            Try
                Return objSchedulingDAL.saveAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt, appointmentId)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Save email and sms status for appointment "
        Public Sub Saveemailsmsstatusforappointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)

            Try
                objSchedulingDAL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region
#Region "Save push noticification status for appointment "
        Public Sub Savepushnoticificationstatusforappointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)

            Try
                objSchedulingDAL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Amend Appointment "
        Public Function AmendAppointmentInfo(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef savedLetterDt As DataTable)

            Try
                Return objSchedulingDAL.AmendAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "get Selected Appointments Details"
        Public Sub getSelectedAppointmentDetails(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)
            Try

                objSchedulingDAL.getSelectedAppointmentDetails(resultDataSet, appointmentId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Tenant(s) Info by TenancyId - Joint Tenancy"
        'Start - Changed By Aamir Waheed on May 28, 2013.
        'To implement joint tenants information.
        Sub GetJointTenantsInfoByTenancyID(ByRef dstenantsInfo As DataSet, ByRef tenancyID As Integer)
            Try

                objSchedulingDAL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            Catch ex As Exception

                Throw
            End Try
        End Sub
        'End - Changed By Aamir Waheed on May 28, 2013.

#End Region

#Region "Integlligent Scheduling Functions"

#Region "get Available Operatives"
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet)
            Try
                objSchedulingDAL.getAvailableOperatives(resultDataSet)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Risk & Vulnerability of Customer"
        Public Function getCustomerRiskVulnerability(ByRef riskVulnerabilityResult As DataSet, ByVal customerId As Integer)
            Try
                Dim riskMessage As String = "\nThe customer has the following Risk \n recorded against their current details \n\n"
                Dim vulnerableMessage As String = "\n The customer has the following Vulnerability \n recorded against their current details \n\n"
                Dim message As String = ""
                objSchedulingDAL.getCustomerRiskVulnerability(riskVulnerabilityResult, customerId)

                'Here will be the code to Extract Risk & Vulnerabilities from DataSet
                Dim riskDataTable, vulnerableDataTable As New DataTable
                riskDataTable = riskVulnerabilityResult.Tables(0)
                vulnerableDataTable = riskVulnerabilityResult.Tables(1)

                'Iterate over Risks
                If (riskDataTable.Rows.Count > 0) Then
                    For Each row As DataRow In riskDataTable.Rows
                        riskMessage = riskMessage + row(ApplicationConstants.RiskCategory).ToString() + " - " + row(ApplicationConstants.RiskSubCategory).ToString() + "\n"
                    Next
                End If

                'Iterate over Vulnerables
                If (vulnerableDataTable.Rows.Count > 0) Then
                    For Each row As DataRow In vulnerableDataTable.Rows
                        vulnerableMessage = vulnerableMessage + row(ApplicationConstants.VulnerabilityCategory).ToString() + " - " + row(ApplicationConstants.VulnerabilitySubCategory).ToString() + "\n"
                    Next
                End If

                'Check if no Risks & Vulnerabilities returned, then filter them out
                If (riskDataTable.Rows.Count > 0) Then
                    message = riskMessage
                End If
                If (vulnerableDataTable.Rows.Count > 0) Then
                    message = message + vulnerableMessage
                End If

                'If message.Equals("") Then
                '    message = "No Risk and Vulnerability againts this customer"
                'End If

                Return message
            Catch ex As Exception
                Throw New ApplicationException("Error in getting Risks & Vulnerabilities againts Customers")
            End Try

        End Function
#End Region
#Region "add More Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and expiry date
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub addMoreAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = Me.applyPatchFilter(operativesDt, appointmentSlotsDtBo, propertySchedulingBo.PatchId)

            ' This function 'll filtered the already created slots based on expiry date
            Me.applyExpiryDateFilter(appointmentSlotsDtBo)

            'fetch the previous operative id so the operatives name should appear in order
            Dim previousOperativeId As Integer = 0
            Dim appointments = appointmentSlotsDtBo.dt.AsEnumerable()
            Dim appResult = (From app In appointments Select app)
            If appResult.Count() > 0 Then
                previousOperativeId = appResult.LastOrDefault.Item(appointmentSlotsDtBo.operativeIdColName)
            End If
            Dim startDate As DateTime = Convert.ToDateTime(appointmentSlotsDtBo.FromDate)
            'This function 'll create the appointment slots based on operatives, appointments, leaves and expiry date
            'Me.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, appointmentSlotsDtBo, propertySchedulingBo, previousOperativeId, startDate)
            Me.createVoidsTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, appointmentSlotsDtBo, propertySchedulingBo, startDate, previousOperativeId)
        End Sub
#End Region

#Region "get Appointments"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="propertySchedulingBo"></param>        
        ''' <remarks></remarks>
        Public Function getAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, Optional ByVal startDate As DateTime = Nothing)
            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            appointmentSlotsDtBo.IsPatchSelected = True
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            If (IsNothing(propertySchedulingBo.CertificateExpiryDate) = True) Then
                appointmentSlotsDtBo.IsDateSelected = False
            Else
                appointmentSlotsDtBo.IsDateSelected = True
                appointmentSlotsDtBo.FullExpiryDate = propertySchedulingBo.CertificateExpiryDate
            End If
            appointmentSlotsDtBo.DisplayCount = 5
            'appointmentSlotsDtBo.AppointmentDuration = GeneralHelper.getAppointmentDurationHours()
            appointmentSlotsDtBo.AppointmentDuration = propertySchedulingBo.Duration

            Dim filteredOperativesDt As DataTable = New DataTable()
            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = Me.applyPatchFilter(operativesDt, appointmentSlotsDtBo, propertySchedulingBo.PatchId)

            'this function 'll create the appointment slot based on leaves, appointments & expiry date
            ' Me.createTempAppointmentSlotss(filteredOperativesDt, leavesDt, appointmentsDt, appointmentSlotsDtBo, propertySchedulingBo, 0, startDate)
            Me.createVoidsTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, appointmentSlotsDtBo, propertySchedulingBo, startDate, 0)
            'save the appointment slots in session 
            'First save a copy of object in session before applying the order function.
            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment 
            orderAppointmentsByDistance(appointmentSlotsDtBo)
            Return appointmentSlotsDtBo
        End Function
#End Region

#Region "apply expiry date Filter"
        ''' <summary>
        ''' This function 'll filtered the already created slots based on certificate expiry date
        ''' </summary>
        ''' <param name="appointmentSlotsDtBo"></param>
        ''' <remarks></remarks>
        Public Sub applyExpiryDateFilter(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO)

            If appointmentSlotsDtBo.IsDateSelected = True Then
                Dim fullExpiryDate As Date = New Date()
                fullExpiryDate = CType(appointmentSlotsDtBo.FullExpiryDate.ToLongDateString() + " " + "23:59", Date)
                Dim appointments = appointmentSlotsDtBo.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app.Item(appointmentSlotsDtBo.appointmentDateColName) <= fullExpiryDate Select app)

                If (appResult.Count() > 0) Then
                    appointmentSlotsDtBo.dt = appResult.CopyToDataTable()
                Else
                    appointmentSlotsDtBo.dt.Clear()
                End If
            End If
        End Sub
#End Region

#Region "apply Patch Filter"
        ''' <summary>
        ''' If patch check box is selected this function 'll sort out the operatives who have same selected property's patch 
        ''' otherwise if patch is unchecked it 'll return all the operatvies
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="appointmentSlotsDtBo"></param>
        ''' <param name="patchId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function applyPatchFilter(ByVal operativesDt As DataTable, ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal patchId As Integer)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'if patch is selected then filter the operatives which have the same patch as selected property's patch
            If appointmentSlotsDtBo.IsPatchSelected = True Then
                '(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = String.Format("PatchId = {0} OR PatchId =  -1 ", patchId.ToString())
                filteredOperativesDt = operativesDv.ToTable()

                'get the ids of filtered operatives
                Dim employeeIds As String = String.Empty
                For Each dr As DataRow In filteredOperativesDt.Rows
                    employeeIds = employeeIds + dr.Item("EmployeeId").ToString() + ","
                Next
                If employeeIds.Length() > 1 Then
                    employeeIds = employeeIds.Substring(0, employeeIds.Length() - 1)

                    'select the appointments based on operatives which are filtered above
                    Dim tempAptDv As DataView = New DataView()
                    tempAptDv = appointmentSlotsDtBo.dt.AsDataView()
                    tempAptDv.RowFilter = appointmentSlotsDtBo.operativeIdColName + " IN (" + employeeIds + ")"
                    appointmentSlotsDtBo.dt = tempAptDv.ToTable()
                Else
                    appointmentSlotsDtBo.dt.Clear()
                End If
            Else
                filteredOperativesDt = operativesDv.ToTable()
            End If

            Return filteredOperativesDt
        End Function
#End Region

#Region "create Temp Appointment Slots"
        ''' <summary>
        ''' 'This function 'll create the appointment slots based on operatives, appointments, leaves and certificate expiry date
        ''' Rules that are implemented in this function are:
        ''' Appointment slots should be start creating - just one hour ahead of current time in today's date.
        ''' 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="appointmentSlotsDtBo"></param>
        ''' <param name="propertySchedulingBo"></param>
        ''' <param name="previousOperativeId"></param>
        ''' <remarks></remarks>
        Public Sub createTempAppointmentSlots(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, Optional ByVal previousOperativeId As Integer = 0, Optional ByVal fromDate As DateTime = Nothing)
            Dim startDate As Date = getStartingDate()
            If Not fromDate = Nothing Then
                startDate = fromDate
            End If

            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim operativePatch As String = String.Empty
            Dim operativePostCode As String = String.Empty
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(startDate) 'requirement is that : Appointment slots should be start creating - just one hour ahead of current time in today's date.
            Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
            Dim displayedTimeSlotCount As Integer = 1
            Dim runningDate As Date
            Dim aptStartTime As New Date
            Dim aptEndTime As New Date
            Dim runningHour As Double = 0
            Dim expiryDateCheckCount As Hashtable = New Hashtable()

            runningDate = startDate
            While (displayedTimeSlotCount <= 5)
                Dim uniqueCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop                
                If operativesDt.Rows.Count = 0 Then
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    uniqueCounter += 1
                    operativeId = Convert.ToString(operativeDr.Item("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr.Item("FullName"))
                    operativePatch = Convert.ToString(operativeDr.Item("PatchName"))
                    operativePostCode = Convert.ToString(operativeDr("PostCode"))

                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 
                    If (previousOperativeId <> 0) Then

                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For
                        End If

                    End If

                    'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                    runningDate = CType(startDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00", Date)
                    runningHour = 0

                    'skip the weekend if it exists
                    Me.skipWeekend(runningDate, runningHour)
                    Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)

                    'this condition 'll check either expiry date check has been executed against all the operatives
                    If (expiryDateCheckCount.Count() = operativesDt.Rows.Count And appointmentSlotsDtBo.IsDateSelected = True) Then
                        Exit While
                    End If

                    'get the old time slots that have been displayed / added lately
                    Dim slots = appointmentSlotsDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots Where app(appointmentSlotsDtBo.operativeIdColName) = operativeId Select app)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        runningDate = CType(slotsResult.Last.Item(appointmentSlotsDtBo.appointmentDateColName), Date)
                        Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)

                        'go to next day because current day slots have finished
                        'Also set the running hour to zero for the nex day 
                        If (Me.changeDayIfTimeEnd(runningDate, runningHour, dayEndHour)) Then
                            'the changeDayIfTimeEnd has increased the day by 1 day now set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                            runningDate = CType(runningDate.ToLongDateString() + " " + GeneralHelper.getDayStartHour().ToString() + ":00", Date)
                            Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)
                        End If

                        'this condition 'll enforce that if expiry date check box is selected then we can only display time slots 
                        'upto the due date <=0
                        If appointmentSlotsDtBo.IsDateSelected = True And (DateTime.Compare(appointmentSlotsDtBo.FullExpiryDate, CType(runningDate.ToShortDateString(), Date)) <= 0) Then
                            'this variable 'll check either due date check has been executed against all the operatives
                            If ((Not (expiryDateCheckCount.ContainsValue(operativeId))) And appointmentSlotsDtBo.IsDateSelected = True) Then
                                If expiryDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                                    expiryDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                                Else
                                    expiryDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                                End If
                            End If
                            Continue For
                        End If

                    End If

                    Dim slotCheckCounter As Integer = 1
                    'this loop 'll create the appointment slots but it 'll not end until and unless it 'll be sure
                    'that no appointment or leave exist in the new created appointment time slot
                    While (slotCheckCounter > 0)
                        'this slotCheckCounter = 0 , means that we are asuming time slot is valid i.e no appointment or leave exist during this assumed time slot
                        'if leave or appointment 'll exist then this slotCheckCounter 'll be incremented by 1 and one more while loop cycle 'll be executed.
                        slotCheckCounter = 0
                        uniqueCounter += 1
                        'check the leaves if exist or not
                        Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, runningDate, aptStartTime, aptEndTime)
                        'if leave exist get the new date time but still its possiblity that leave ''ll exist in the next date time so 'll also be checked again.
                        If leaveResult.Count() > 0 Then
                            runningDate = CType(leaveResult.Last().Item("EndDate"), Date)
                            runningDate = CType(runningDate.ToLongDateString() + " " + leaveResult.Last().Item("EndTime").ToString(), Date)
                            Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)

                            'check if time slot exceeeds the end time of working hour
                            If changeDayIfTimeEnd(runningDate, runningHour, dayEndHour) = True Then
                                'the changeDayIfTimeEnd has increased the day by 1 day now set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)                            
                                runningDate = CType(runningDate.ToLongDateString() + " " + GeneralHelper.getDayStartHour().ToString() + ":00", Date)
                                Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)
                            End If

                            'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot 
                            'that no appointment or leave exist during this time slot
                            slotCheckCounter += 1
                        End If

                        'if appoinment exist in this time slot get the new date time but still its possiblity that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this 'll be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, runningDate, aptStartTime, aptEndTime)
                        If appResult.Count() > 0 Then
                            runningDate = CType(appResult.Last().Item("AppointmentDate"), Date)
                            runningDate = GeneralHelper.unixTimeStampToDateTime(appResult.Last.Item("EndTimeInSec").ToString())
                            'runningDate = CType(runningDate.ToLongDateString() + " " + appResult.Last.Item("EndTime").ToString(), Date)
                            Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)

                            'check if time slot exceeeds the end time of working hour
                            If changeDayIfTimeEnd(runningDate, runningHour, dayEndHour) = True Then
                                'the changeDayIfTimeEnd has increased the day by 1 day now set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)                            
                                runningDate = CType(runningDate.ToLongDateString() + " " + GeneralHelper.getDayStartHour().ToString() + ":00", Date)
                                Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)
                            End If

                            'this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot 
                            'that no appointment or leave exist during this time slot
                            slotCheckCounter += 1
                        End If

                        'check if time slot exceeeds the end time of working hour
                        If changeDayIfTimeEnd(runningDate, runningHour, dayEndHour) = True Then
                            'the changeDayIfTimeEnd has increased the day by 1 day now set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)                            
                            runningDate = CType(runningDate.ToLongDateString() + " " + GeneralHelper.getDayStartHour().ToString() + ":00", Date)
                            Me.setAppointmentStartEndTime(runningDate, aptStartTime, aptEndTime, runningHour, appointmentSlotsDtBo.AppointmentDuration)
                            slotCheckCounter += 1
                        End If

                        'this condition 'll enforce that if due date check box is selected then we can only display time slots 
                        'upto the due date
                        If appointmentSlotsDtBo.IsDateSelected = True And (DateTime.Compare(appointmentSlotsDtBo.FullExpiryDate, CType(runningDate.ToShortDateString(), Date)) <= 0) Then
                            'this variable 'll check either expiry date check has been executed against all the operatives
                            If ((Not (expiryDateCheckCount.ContainsValue(operativeId))) And appointmentSlotsDtBo.IsDateSelected = True) Then
                                If expiryDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                                    expiryDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                                Else
                                    expiryDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                                End If
                            End If
                            Continue For
                        End If

                    End While ' while loop end

                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    Dim distance As Double = getDistanceFromProperty(appointmentsDt, operativeId, runningDate, aptStartTime, propertySchedulingBo, operativePostCode)

                    'add this time slot in appointment list
                    ' appointmentSlotsDtBo.addRowInAppointmentList(operativeId, operativeName, operativePatch, aptStartTime, aptEndTime, runningDate, distance)

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > 5 Then
                        Exit While
                    End If
                Next
            End While 'outer while loop end
        End Sub




#End Region

#Region "get Starting day Date"
        ''' <summary>
        ''' This funciton 'll be used to reset the starting date.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getStartingDate()
            Return Date.Now.AddHours(2)
        End Function
#End Region

#Region "Skip Weekend"
        ''' <summary>
        ''' This function 'll skip the weekend (saturday and/or sunday)
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="runningHour"></param>
        ''' <remarks></remarks>
        Private Sub skipWeekend(ByRef runningDate As Date, ByRef runningHour As Double)
            If runningDate.DayOfWeek = DayOfWeek.Saturday Then
                runningDate = runningDate.AddDays(2)
                runningHour = 0
            ElseIf runningDate.DayOfWeek = DayOfWeek.Sunday Then
                runningDate = runningDate.AddDays(1)
                runningHour = 0
            End If
        End Sub
#End Region

#Region "set appointment start end time"
        Private Sub setAppointmentStartEndTime(ByRef runningDate As Date, ByRef aptStartTime As Date, ByRef aptEndTime As Date, ByRef runningHour As Double, ByVal appointmentDuration As Double)
            aptStartTime = CType(runningDate.ToString("HH") + ":" + runningDate.ToString("mm"), Date)
            runningHour = CType(aptStartTime.ToString("HH"), Double) + (CType(aptStartTime.ToString("mm"), Double) / 60)
            runningHour = runningHour + appointmentDuration
            aptEndTime = CType(runningDate.AddHours(appointmentDuration).ToString("HH") + ":" + runningDate.AddHours(appointmentDuration).ToString("mm"), Date)
        End Sub
#End Region

#Region "change Day If Time End"
        ''' <summary>
        ''' 'go to next day because current day slots have finished Also set the running hour to zero for the nex day 
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="runningHour"></param>
        ''' <param name="dayEndHour"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function changeDayIfTimeEnd(ByRef runningDate As Date, ByRef runningHour As Double, ByVal dayEndHour As Double) As Boolean
            If (runningHour > dayEndHour) Then
                'go to next day because current day slots have finished
                'Also set the running hour to zero for the nex day 
                runningDate = runningDate.AddDays(1)
                runningHour = 0
                'skip the weekend if it exists
                Me.skipWeekend(runningDate, runningHour)
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "get Operative Distance From Property"
        ''' <summary>
        ''' This appointment 'll calculate the distance between operatives last appointment slot and running appointment slot (running means the slot that is being calculated at run time)
        ''' The rule would work like this
        ''' The distance will be calculated via the selected property and the last appointment (in order of time) prior to the available slot on the day 
        ''' Examples:
        ''' Case 1
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 11:00 - 12:00
        ''' Lets assume that Carl had 1 appointment from 9:00 - 10:00 at Property 1. Similarly he has another appointment from 10:00 - 11:00 at Property 2. The distance ail be calculated between selected property and Property2. The reason we chose Property 2 is because Carl will be here prior to his free slot 11:00 - 12:00
        ''' Case2
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 9:00 - 11:00
        ''' Lets assume that Carl had no appointments on this day. In case there is no prior booked appointment before the available appointment slot, we shall display 0 in the distance column
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="runningDate"></param>
        ''' <param name="aptStartTime"></param>
        ''' <remarks></remarks>
        Private Function getDistanceFromProperty(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal operativePostCode As String) As Double
            'Distance
            Dim distance As Double = 0

            'get the end time of running time slot (date + start time of appointment) 
            'running appointment date time means the appointment time which is being calculated in loop
            Dim runningAptDateTime As Date
            runningAptDateTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startDate As New DateTime(1970, 1, 1)
            Dim runningAptTimeInSec As Integer
            ' we are creating this variable because we want to compare the dates from db and from calcuated date with time as 00:00:00
            ' in order to do the right comparison
            Dim runningDateAlias As Date = New Date()

            runningAptTimeInSec = (runningAptDateTime - startDate).TotalSeconds
            'creating the alias for comparison with 00:00:00 time
            runningDateAlias = CType(runningDate.ToLongDateString() + " " + "00:00:00", Date)


            Dim appointments = appointmentsDt.AsEnumerable()
            Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("EndTimeInSec") <= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("EndTimeInSec"), Integer) Descending)

            'if operative has any appointment prior to the running appointment slot then 
            'find the distance between customer property address and property address of operatives last arranged appointment on the same day
            If appResult.Count() > 0 Then
                'get the property address of running time slot
                Dim fromPropertyAddress As String
                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = appResult.First().Item("Address") + "+" + appResult.First().Item("TownCity") + "+" + appResult.First().Item("County")
                fromPropertyAddress = fromPropertyAddress.Replace(" ", "+")

                Dim toPropertyAddress As String
                toPropertyAddress = propertySchedulingBo.PropertyAddress.Replace(" ", "+")

                ''get the distance from session if this calcuation was done previously
                Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                propertyAddressDictionary = SessionManager.getPropertyDistance()
                Dim propertyResult = (From ps In propertyAddressDictionary Where ps.Value.FromPropertyAddress = fromPropertyAddress And ps.Value.ToPropertyAddress = toPropertyAddress Select ps)

                'if we have found the distance of property address from session then we have save the call of map api
                If propertyResult.Count > 0 Then
                    distance = propertyResult.First.Value.Distance
                Else
                    'we didnt' find the property distance from session then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress)

                    'save the property's distance in session so that we can save map api call in future calculations
                    Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                    'this 'll be used to create the unique index of dictionary
                    Dim uniqueKey As String = GeneralHelper.getUniqueKey(0)

                    propertyDistance.FromPropertyAddress = fromPropertyAddress
                    propertyDistance.ToPropertyAddress = propertySchedulingBo.PropertyAddress
                    propertyDistance.Distance = distance
                    If (propertyAddressDictionary.ContainsKey(uniqueKey) = False) Then
                        propertyAddressDictionary.Add(uniqueKey, propertyDistance)
                    End If

                    'set the dictionary of property distances  in session
                    SessionManager.setPropertyDistance(propertyAddressDictionary)
                End If
            Else

                'get the property address of running time slot
                Dim fromPropertyAddress As String
                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = operativePostCode.Replace(" ", "+")

                Dim toPropertyAddress As String

                If propertySchedulingBo.PostCode.Length > 0 Then
                    toPropertyAddress = propertySchedulingBo.PostCode.Replace(" ", "+")

                Else
                    toPropertyAddress = propertySchedulingBo.PropertyAddress.Replace(" ", "+")
                End If

                ''get the distance from session if this calcuation was done previously
                Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                propertyAddressDictionary = SessionManager.getPropertyDistance()
                Dim propertyResult = (From ps In propertyAddressDictionary Where ps.Value.FromPropertyAddress = fromPropertyAddress And ps.Value.ToPropertyAddress = toPropertyAddress Select ps)

                'if we have found the distance of property address from session then we have save the call of map api
                If propertyResult.Count > 0 Then
                    distance = propertyResult.First.Value.Distance
                Else
                    'we didnt' find the property distance from session then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress)

                    'save the property's distance in session so that we can save map api call in future calculations
                    Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                    'this 'll be used to create the unique index of dictionary
                    Dim uniqueKey As String = GeneralHelper.getUniqueKey(0)

                    propertyDistance.FromPropertyAddress = fromPropertyAddress
                    propertyDistance.ToPropertyAddress = toPropertyAddress
                    propertyDistance.Distance = distance
                    If (propertyAddressDictionary.ContainsKey(uniqueKey) = False) Then
                        propertyAddressDictionary.Add(uniqueKey, propertyDistance)
                    End If

                    'set the dictionary of property distances  in session
                    SessionManager.setPropertyDistance(propertyAddressDictionary)
                End If

            End If

            Return distance
        End Function
#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="aptStartTime"></param>
        ''' <param name="aptEndTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Public Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal aptEndTime As Date)

            aptStartTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            aptEndTime = CType(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(aptStartTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(aptEndTime)

            Dim leaves = leavesDt.AsEnumerable()

            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin = app("StartTimeInMin") And lEndTimeInMin < app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin = app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin > app("StartTimeInMin") And lEndTimeInMin < app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
            Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="aptStartTime "></param>
        ''' <param name="aptEndTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Public Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal aptEndTime As Date)

            aptStartTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            aptEndTime = CType(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(aptStartTime)
            endTimeInSec = GeneralHelper.convertDateInSec(aptEndTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso ( _
                                                               (startTimeInSec = app("StartTimeInSec") And endTimeInSec < app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("EndTimeInSec") And endTimeInSec = app("EndTimeInSec")) _
                                                            Or (startTimeInSec > app("StartTimeInSec") And endTimeInSec < app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("StartTimeInSec") And endTimeInSec > app("EndTimeInSec")) _
                                                            Or (startTimeInSec < app("StartTimeInSec") And endTimeInSec > app("StartTimeInSec")) _
                                                            Or (startTimeInSec < app("EndTimeInSec") And endTimeInSec > app("EndTimeInSec"))) _
                            Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region

#Region "Order Temp Appointments By Distance"
        ''' <summary>
        ''' 'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment 
        ''' </summary>
        ''' <param name="appointmentSlotsDtBo"></param>
        ''' <remarks></remarks>
        Public Sub orderAppointmentsByDistance(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO)
            Dim appointments = appointmentSlotsDtBo.dt.AsEnumerable
            Dim appResult = (From app In appointments Select app _
                             Order By CType(app(appointmentSlotsDtBo.distanceColName), Double) Ascending _
                             , Date.ParseExact(app(appointmentSlotsDtBo.dateStringColName), "dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")) Ascending
                             , app(appointmentSlotsDtBo.timeColName) Ascending)
            If appResult.Count() > 0 Then
                appointmentSlotsDtBo.dt = appResult.CopyToDataTable()
            End If

        End Sub

#End Region

#End Region

#Region "Get Appointment Info For Email"
        Sub getAppointmentInfoForEmail(ByRef resultDataSet As DataSet, ByVal journalHistoryId As Integer)
            objSchedulingDAL.getAppointmentInfoForEmail(resultDataSet, journalHistoryId)
        End Sub
#End Region

#Region "Populate Job Sheet Summary Page"
        Sub populateJobSheet(ByVal journalHistoryId As Integer, ByRef resultDataset As DataSet)
            Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()
            objSchedulingDAL.populateJobSheet(journalHistoryId, resultDataset)

        End Sub

#End Region

#Region "Get risk and vulnerability info"
        ''' <summary>
        ''' Get risk and vulnerability info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getRiskAndVulnerabilityInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim objSchedulingDal As SchedulingDAL = New SchedulingDAL()
            objSchedulingDal.getRiskAndVulnerabilityInfo(resultDataSet, journalId)

        End Sub
#End Region

#Region "get Defect Available Operatives"

        ''' <summary>
        ''' This function 'll get the available operatives against faults
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tradeIds"></param>
        ''' <remarks></remarks>
        Public Sub getDefectAvailableOperatives(ByRef resultDataSet As DataSet, ByRef tradeIds As String)

            objSchedulingDAL.getDefectAvailableOperatives(resultDataSet, tradeIds)

        End Sub

#End Region

#Region "Get Property Customer Detail"
        ''' <summary>
        ''' Get Property Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyCustomerDetail(ByRef resultDataSet As DataSet, ByVal propertyId As String)
            objSchedulingDAL.getPropertyCustomerDetail(resultDataSet, propertyId)
        End Sub
#End Region

#Region "Get Scheme/Block Customer Detail"
        ''' <summary>
        ''' Get Schemw/Block Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getSchemeBlockDetail(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal ReuestType As String)
            objSchedulingDAL.getScemeBlockDetail(resultDataSet, propertyId, ReuestType)
        End Sub
#End Region

#Region "Schedule Defect Appointment"

        Function scheduleDefectAppointment(ByVal journalId As Integer, ByVal objAppointment As AppointmentBO, ByVal tempDefectsdt As DataTable) As Boolean
            Return objSchedulingDAL.scheduleDefectAppointment(journalId, objAppointment, tempDefectsdt)
        End Function

#End Region


#Region "Get Property Attribute notes"
        ''' <summary>
        ''' Get Property Attribute notes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAttributeNotesByPropertyId(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            objSchedulingDAL.getAttributeNotesByPropertyId(resultDataSet, propertyId)


        End Sub
#End Region

#Region "Get Scheme/Block Attribute notes"
        ''' <summary>
        ''' Get Property Attribute notes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="blockId"></param>
        ''' <remarks></remarks>
        Public Sub GetAttributeNotesBySchemeBlockId(ByRef resultDataSet As DataSet, ByRef schemeId As Int32, ByRef blockId As Int32)
            objSchedulingDAL.GetAttributeNotesBySchemeBlockId(resultDataSet, schemeId, blockId)
        End Sub
#End Region

#Region "Intelligent Scheduling Algorithm with core working and extended working hours"


#Region "create Voids TempAppointment Slots"
        Public Sub createVoidsTempAppointmentSlots(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempAppointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal selectedStartDate As DateTime, _
         Optional ByVal previousOperativeId As Integer = 0)

            'If selectedStartDate = Nothing Then
            '    selectedStartDate = DateTime.Now
            'End If
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim operativePostCode As String = String.Empty
            Dim patchName As String = String.Empty
            Dim dueDateCheckCount As New Hashtable()
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(selectedStartDate)
            Dim dayEndHour As Double = GeneralHelper.getDayEndHour()


            Dim displayedTimeSlotCount As Integer = 1
            Dim sDate As DateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
            Dim appointmentStartDateTime As System.DateTime = Nothing
            Dim appointmentEndDate As System.DateTime = Nothing
            'System.DateTime aptEndTime = new System.DateTime();
            'int isGasOftecOperativeCount = 0;
            'int timeSlotsToDisplay = GeneralHelper.getTimeSlotToDisplay;
            'bool isMultipleDaysAppointmentAllowed = GeneralHelper.isMultipleDaysAppointmentAllowed;

            Dim operativeEligibility As New Dictionary(Of Integer, Boolean)()
            Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
            Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + (If(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString()))).ToString("HH:mm")
            Dim skipOperative As Boolean = False

            Dim appointmentDueDate As DateTime = Convert.ToDateTime(DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")

            'Just to initlize start date and end date.
            appointmentStartDateTime = selectedStartDate
            appointmentEndDate = appointmentStartDateTime

            'Calculate Time slots given in count
            ' Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            While displayedTimeSlotCount <= tempAppointmentSlotsDtBo.DisplayCount
                Dim uniqueCounter As Integer = 0
                Dim operativeCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop

                If operativesDt.Rows.Count = 0 Then
                    ' TODO: might not be correct. Was : Exit While
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    skipOperative = False
                    uniqueCounter += 1
                    operativeCounter += 1
                    operativeId = Convert.ToInt32(operativeDr("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr("FullName"))
                    operativePostCode = Convert.ToString(operativeDr("PostCode"))
                    patchName = Convert.ToString(operativeDr("PatchName"))
                    Dim operativeWorkingHours As New DataSet()
                    objSchedulingDAL.getOperativeWorkingHours(operativeWorkingHours, operativeId, ofcCoreStartTime, ofcCoreEndTime)
                    SessionManager.setOperativeworkingHourDs(operativeWorkingHours)


                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 

                    If (previousOperativeId <> 0) Then
                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For

                        End If
                    End If

                    'if syetem does not get any slot for an operative then system will check in maximum days which Defined in web.cong file.
                    ' Other wise system will goes to infinit loop. 
                    appointmentDueDate = Convert.ToDateTime(System.DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")


                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempAppointmentSlotsDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots _
                                       Where app(AppointmentSlotsDtBO.operativeIdColName) = operativeId _
                                       Select app _
                                       Order By CType(app(AppointmentSlotsDtBO.appointmentEndDateColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = Convert.ToDateTime(slotsResult.Last().Field(Of String)(AppointmentSlotsDtBO.appointmentEndDateColName))
                    Else
                        'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        'appointmentStartDateTime = sDate;
                        appointmentStartDateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
                    End If

                    Me.setVoidAppointmentStartEndTime(appointmentStartDateTime, appointmentEndDate, tempAppointmentSlotsDtBo.AppointmentDuration, skipOperative, leavesDt, appointmentsDt, _
                     operativeId, tempAppointmentSlotsDtBo, appointmentDueDate, dueDateCheckCount, uniqueCounter)

                    If (skipOperative = True) Then
                        'Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        'If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        'this condition 'll check either due date check has been executed against all the operatives
                        If (dueDateCheckCount.Count = operativesDt.Rows.Count) Then
                            'break; // TODO: might not be correct. Was : 
                            Return
                        Else
                            Continue For
                        End If
                    End If

                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    Dim distance As Double = getDistanceFromProperty(appointmentsDt, operativeId, appointmentStartDateTime, GeneralHelper.getHrsAndMinString(appointmentStartDateTime), propertySchedulingBo, operativePostCode)

                    'add this time slot in appointment list
                    tempAppointmentSlotsDtBo.addRowInAppointmentList(operativeId, operativeName, patchName, appointmentStartDateTime, appointmentEndDate, appointmentStartDateTime, appointmentEndDate, distance)
                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    'double distanceFrom = getPropertyDistance(appointmentsDt, operativeId, appointmentStartDateTime, customerBo);
                    'double distanceTo = getPropertyDistance(appointmentsDt, operativeId, appointmentEndDate, customerBo, isToNext: true);

                    'update the lastAdded column in tempfaultBO.dt
                    'updateLastAdded(ref tempFaultAptBo);
                    'add this time slot in appointment list
                    'tempFaultAptBo.operativeId = operativeId
                    'tempFaultAptBo.operative = operativeName
                    'tempFaultAptBo.aptStartTime = GeneralHelper.getHrsAndMinString(appointmentStartDateTime)
                    'tempFaultAptBo.aptEndTime = GeneralHelper.getHrsAndMinString(appointmentEndDate)
                    'tempFaultAptBo.appointmentDateString = GeneralHelper.convertDateToCustomString(appointmentStartDateTime)
                    ''tempFaultAptBo.appointmentEndDate = GeneralHelper.convertDateToCustomString(appointmentEndDate)
                    'tempFaultAptBo.lastAdded = True
                    'updateLastAdded(tempFaultAptBo)
                    'tempFaultAptBo.addNewDataRow()
                    'tempFaultAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom, distanceTo);

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > tempAppointmentSlotsDtBo.DisplayCount Then
                        ' TODO: might not be correct. Was : Exit While
                        Exit For
                    End If
                Next
            End While
            'Calculate Time slots loop ends here
        End Sub

#End Region

#Region "update Last added column in tempTradeAptBo data table"
        ''' <summary>
        ''' 'update Last added column in tempTradeAptBo datatable 
        ''' </summary>
        ''' <param name="tempTradeAptBo"></param>
        ''' <remarks></remarks>
        Public Sub updateLastAdded(ByRef tempTradeAptBo As TempAppointmentDtBO)
            Dim appointments = tempTradeAptBo.dt.AsEnumerable()
            Dim appResult = (From app In appointments Where Convert.ToBoolean(app(TempAppointmentDtBO.lastAddedColName)) = True Select app)
            If appResult.Count() > 0 Then
                appResult.First()(TempAppointmentDtBO.lastAddedColName) = False
            End If

        End Sub

#End Region

#Region "is Opertive Available"
        Private Function isOpertiveAvailable(ByVal operativeWorkingHourDt As DataTable, ByVal dateToCheck As DateTime, ByRef dueDateCheckCount As Hashtable, ByVal operativeId As Integer, ByVal uniqueCounter As Integer, ByVal appointmentDueDate As DateTime) As Boolean

            '1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            Dim operativeAvailable As Boolean = checkDateIsGreaterThanDueDateAndAddToSkipList(uniqueCounter, dueDateCheckCount, dateToCheck, operativeId, appointmentDueDate) _
                                                OrElse Not checkOperativeAvailability(dateToCheck, operativeWorkingHourDt)

            Return operativeAvailable
        End Function

#End Region

#Region "if Date Is Greater Than Due Date"

        ''' <summary>
        ''' This function 'll check if running date is greater than due date of fault. If its true then add the entry in hash table and this (current) operative 'll not process any more for appointments
        ''' The count of total entries in hash table 'll help us to check that all operatives has been processed against the fault due date
        ''' </summary>
        ''' <param name="uniqueCounter"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="operativeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkDateIsGreaterThanDueDateAndAddToSkipList(ByRef uniqueCounter As Integer, ByRef dueDateCheckCount As Hashtable, ByVal dateToCheck As Date, ByVal operativeId As Integer, ByVal appointmentDueDate As DateTime) As Boolean
            Dim operativeSkipped As Boolean = False

            'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            If CType(dateToCheck.ToShortDateString(), Date) > appointmentDueDate Then
                operativeSkipped = True
                'this variable 'll check either due date check has been executed against all the operatives
                If Not (dueDateCheckCount.ContainsValue(operativeId)) Then
                    If dueDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                    Else
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                    End If
                End If
            End If
            Return operativeSkipped
        End Function

#End Region

#Region "Check operative availability"
        ''' <summary>
        ''' This function checks whether operative is fully absent in core and extended hours. If yes then this 
        ''' operative will be skipped.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="operativeHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkOperativeAvailability(ByVal runningDate As DateTime, ByVal operativeHoursDt As DataTable)

            Dim isAvailable As Boolean = True
            Dim workingHours = operativeHoursDt.AsEnumerable

            Dim coreWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) = "NA" _
                                            And app(ApplicationConstants.EndDateCol) = "NA" _
                                            And app(ApplicationConstants.StartTimeCol) <> "" _
                                            Select app)

            Dim extendedWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) <> "NA" _
                                            Select app)

            If (extendedWorkingHoursResult.Count > 0) Then
                extendedWorkingHoursResult = (From app In extendedWorkingHoursResult.CopyToDataTable().AsEnumerable() _
                                           Where Convert.ToDateTime(app(ApplicationConstants.StartDateCol)) >= runningDate _
                                           Select app)
            End If

            If coreWorkingHoursResult.Count = 0 And extendedWorkingHoursResult.Count = 0 Then
                isAvailable = False
            End If

            Return isAvailable

        End Function

#End Region

#Region "Get operatives available time slots for given date/day in ascending order of StartTime column"

        Private Function getOperativeAvailableSlotsByDateTime(ByVal operativeWorkingHourDt As DataTable, ByVal dateToFilter As Date) As DataTable
            'Check one or more working hours slots from the operative data table.
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                           Where optHour(ApplicationConstants.StartDateCol) = dateToFilter.ToString("dd/MM/yyyy") _
                                                 Or ((optHour(ApplicationConstants.WeekDayCol).ToString() = dateToFilter.DayOfWeek.ToString) _
                                                And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                                And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                                And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                ) _
                                           Select optHour _
                                           Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > dateToFilter.TimeOfDay))
            ''And If(optHour(ApplicationConstants.EndTimeCol) = "", False, CType(optHour(ApplicationConstants.EndTimeCol), TimeSpan) >= dateToFilter.TimeOfDay)

            ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            Dim operativeWorkingHourForCurrentDayDt = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable, operativeWorkingHourDt.Clone)
            Return operativeWorkingHourForCurrentDayDt
        End Function

#End Region

#Region "set Appointment Start and  End Time"
        ''' <summary>
        ''' This function calculates the appointment start time and appointment end time. We are assuming that end date 'll be used to calculate the new start date and end date
        ''' </summary>
        ''' <param name="appointmentStartDate"></param>
        ''' <param name="appointmentEndDate"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="skipOperative"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="tempAppointmentSlotsDtBo"></param>
        ''' <param name="appointmentDueDate"></param>
        ''' <param name="dueDateCheckCount"></param>
        ''' <param name="uniqueCounter"></param>
        ''' <remarks></remarks>
        Private Sub setVoidAppointmentStartEndTime(ByRef appointmentStartDate As DateTime, ByRef appointmentEndDate As DateTime, ByVal workDuration As Double, ByRef skipOperative As Boolean, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, _
         ByVal operativeId As Integer, ByVal tempAppointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal appointmentDueDate As DateTime, ByRef dueDateCheckCount As Hashtable, ByRef uniqueCounter As Integer, Optional ByVal isMultipleDaysAppointmentAllowed As Boolean = False)
            Dim isAppointmentCreated As Boolean = False
            Dim isExtendedApptVerified As Boolean = False
            'Get Operative core and extended working hours from session
            Dim operativeWorkingHourDs As DataSet = SessionManager.getOperativeworkingHourDs
            Dim operativeWorkingHourDt As DataTable = operativeWorkingHourDs.Tables(0)

            Dim currentRunningDate As System.DateTime = appointmentStartDate
            Dim loggingTime As Integer = 0

            'Do Until: appointment is created or the operative is skipped
            Do
                'Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future) 

                If isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, appointmentDueDate) Then

                    skipOperative = True
                Else
                    'Get operatives running day available time in ascending order of StartTime column
                    '  Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate)


                    Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsForMultipleDays(operativeWorkingHourDt, appointmentStartDate, appointmentEndDate, workDuration, appointmentDueDate, loggingTime)
                    ' Check if there is a slot 
                    If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
                        Dim nextBlockStartTime As String = operativeWorkingHourForCurrentDayDt.Rows(0)(ApplicationConstants.StartTimeCol)
                        Dim nextBlockStartDateTime As Date = CType(appointmentStartDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                        If nextBlockStartDateTime > appointmentStartDate Then
                            appointmentStartDate = nextBlockStartDateTime
                        End If

                        'At the begining of slot creation set appointment end date time to same as start date time and then calculate
                        ' the end date time base on duration and operative working hours.
                        appointmentEndDate = appointmentStartDate

                        'Merge adjacent hours, treat each row as block
                        'operativeWorkingHourDt = Me.mergeAdjacentHours(operativeWorkingHourDt)

                        'This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
                        Dim dayCounter As Integer = 0
                        'Repeat For End Date Time Until the full remainingDuration is consumed to zero
                        Dim remainDuration As Double = workDuration
                        While remainDuration > 0 AndAlso Not isAppointmentCreated
                            dayCounter += 1

                            ' Iterate through each working core/on call/extended to use it for appointment duration.
                            For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                                nextBlockStartTime = operativeWorkingHoursBlock(ApplicationConstants.StartTimeCol)
                                nextBlockStartDateTime = CType(appointmentEndDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                                loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)
                                'Set appointmentEndDate to start time of next block.
                                'At the begining of each block set appointment end date time to the start of next block's start date in case
                                ' in case it is less than next block start time.
                                If nextBlockStartDateTime > appointmentEndDate Then
                                    appointmentEndDate = nextBlockStartDateTime
                                End If

                                Dim blockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeCol)
                                Dim blockEndDateTime As Date = CType(appointmentEndDate.ToLongDateString() + " " + blockEndTime, Date)

                                'calculate the remaining time/lapse of the block
                                Dim lapse As Double = blockEndDateTime.TimeOfDay.Subtract(appointmentEndDate.TimeOfDay).TotalHours
                                'subtract the total lapse/remaining time of the block from the remaining trade duration
                                Dim tDuration As Double = remainDuration - lapse

                                'if duration is less than zero than it means now we have to calculate the end hour of appointment otherwise we have to go to one day ahead to calculate the end time
                                If (tDuration <= 0) Then
                                    'Dim absoluteDuration As Double = Math.Abs(tDuration)
                                    appointmentEndDate = appointmentEndDate.AddHours(remainDuration)
                                    'In this case appointment is created successfully, exit the blocks loop and check for leaves and existing appointments
                                    isAppointmentCreated = True
                                ElseIf tDuration > 0 Then
                                    'we have to go to next block to calculate the end date/time, so setting appointment End Date = BlockEndDateTiem
                                    appointmentEndDate = blockEndDateTime
                                End If
                                remainDuration -= lapse
                                If isAppointmentCreated Then
                                    Exit For
                                End If
                            Next

                            'In case appointment is created or operative is skipped by reaching due date: exit while and apply next iteration or add operative to skip list.
                            If isAppointmentCreated OrElse isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, appointmentDueDate) Then
                                Exit While
                            End If

                            'In case appointment is not created by above process change the day and set appointment end datetime at start of next day.
                            changeDay(appointmentEndDate, "00:00")

                            'In case appointment is not create and all slots (Core Working Hours, On Call hours and out of office hours) are 
                            'Check for case: is it is allowed to create multiple days appointment: If now exit while and start over for next day
                            ' in case due date is reached operative will be skipped by skipOperative Process.
                            If (Not isMultipleDaysAppointmentAllowed) Then
                                appointmentStartDate = appointmentEndDate
                                Exit While
                            End If

                            'Only in case remainDuration is more than zero then we need to appointment working hours for next day, other wise the loop will simply exit.                            
                            operativeWorkingHourForCurrentDayDt = If(remainDuration > 0, _
                                                                     getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentEndDate), Nothing)
                        End While

                    Else
                        'Change day 
                        changeDay(appointmentStartDate, "00:00")
                    End If
                End If

                If isAppointmentCreated AndAlso Not skipOperative Then
                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate)
                    'if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    If leaveResult.Count() > 0 Then
                        appointmentStartDate = Convert.ToDateTime(leaveResult.Last().Field(Of DateTime)("EndDate"))
                        appointmentStartDate = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Field(Of String)("EndTime"))
                        isAppointmentCreated = False
                    Else
                        'if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this will be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate)
                        If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            isAppointmentCreated = False

                        End If
                    End If

                End If
            Loop While Not ((isAppointmentCreated OrElse skipOperative))
        End Sub

#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="leavesDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appoitmentEndDateTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appoitmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(appointmentStartDateTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(appoitmentEndDateTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
             Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime "></param>
        ''' <param name="appointmentEndDateTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                            Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region


#Region "Day calculation algorithm."

        Private Function getOperativeAvailableSlotsForMultipleDays(ByVal operativeWorkingHourDt As DataTable, ByVal dateToFilter As DateTime, ByRef appointmentEndDate As DateTime, ByVal workDuration As Double, _
                                                                   ByVal appointmentDueDate As DateTime, ByRef loggingTime As Integer) As DataTable

            'Check one or more working hours slots from the operative data table.
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                           Where optHour(ApplicationConstants.StartDateCol) = dateToFilter.ToString("dd/MM/yyyy") _
                                                 Or ((optHour(ApplicationConstants.WeekDayCol).ToString() = dateToFilter.DayOfWeek.ToString) _
                                                And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                                And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                                And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                ) _
                                           Select optHour _
                                           Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > dateToFilter.TimeOfDay))
            ''And If(optHour(ApplicationConstants.EndTimeCol) = "", False, CType(optHour(ApplicationConstants.EndTimeCol), TimeSpan) >= dateToFilter.TimeOfDay)

            ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            Dim operativeWorkingHourForCurrentDayDt = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable, operativeWorkingHourDt.Clone)
            Return operativeWorkingHourForCurrentDayDt

            'Dim totalWorkingHour As Double = 0
            'Dim hourDiff As Double = 0
            'Dim isAppointmentCreated As Boolean = True
            'Dim appointmentStartDate As DateTime = dateToFilter
            'Dim isStartDateSelected As Boolean = False
            'Dim isExtendedIncluded As Boolean = True
            ''extended ot Oncall hours included foe specific day ot not.
            'Dim remainingDuration As Double = workDuration
            'Do

            '    Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
            '                                 Where optHour(ApplicationConstants.StartDateCol) = appointmentStartDate.ToString("dd/MM/yyyy") _
            '                              Or ((optHour(ApplicationConstants.WeekDayCol).ToString() = appointmentStartDate.DayOfWeek.ToString) _
            '                             And optHour(ApplicationConstants.StartDateCol) = "NA" _
            '                             And optHour(ApplicationConstants.EndDateCol) = "NA" _
            '                             And optHour(ApplicationConstants.StartTimeCol) <> "" _
            '                             ) _
            '                                 Select optHour _
            '                                 Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
            '                                  .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > appointmentStartDate.TimeOfDay))

            '    ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            '    Dim operativeWorkingHourForCurrentDayDt As DataTable = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())

            '    If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
            '        Dim nextBlockStartTime As String = operativeWorkingHourForCurrentDayDt.Rows(0)(ApplicationConstants.StartTimeCol)
            '        Dim nextBlockStartDateTime As Date = CType(appointmentStartDate.ToLongDateString() + " " + nextBlockStartTime, Date)
            '        If nextBlockStartDateTime > appointmentStartDate Then
            '            appointmentStartDate = nextBlockStartDateTime
            '        End If

            '        'At the begining of slot creation set appointment end date time to same as start date time and then calculate
            '        ' the end date time base on duration and operative working hours.
            '        appointmentEndDate = appointmentStartDate

            '        'Merge adjacent hours, treat each row as block
            '        'operativeWorkingHourDt = Me.mergeAdjacentHours(operativeWorkingHourDt)

            '        'This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
            '        Dim dayCounter As Integer = 0
            '        'Repeat For End Date Time Until the full remainingDuration is consumed to zero
            '        Dim remainDuration As Double = workDuration


            '        'Iterate result set including core, Extended and on call hours.
            '        For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
            '            'get operative day start hour for specific day
            '            nextBlockStartTime = operativeWorkingHoursBlock(ApplicationConstants.StartTimeColumn).ToString()
            '            nextBlockStartDateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockStartTime)
            '            ' get operative day end hour for specific day
            '            Dim nextBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeColumn).ToString()
            '            Dim nextBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockEndTime)
            '            loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)

            '            If nextBlockEndDateTime > dateToFilter AndAlso (operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString() = "NA" OrElse isExtendedIncluded) Then

            '                If Not isStartDateSelected AndAlso nextBlockStartDateTime >= appointmentStartDate Then
            '                    isStartDateSelected = True
            '                    dateToFilter = nextBlockStartDateTime
            '                    appointmentStartDate = nextBlockStartDateTime
            '                ElseIf Not isStartDateSelected AndAlso dateToFilter > nextBlockStartDateTime Then
            '                    isStartDateSelected = True
            '                ElseIf nextBlockStartDateTime >= appointmentStartDate Then
            '                    If dateToFilter.Date = nextBlockStartDateTime.Date Then
            '                        appointmentStartDate = dateToFilter
            '                    ElseIf dateToFilter.Date < nextBlockStartDateTime.Date Then
            '                        Dim midnightHour As Double = Convert.ToDouble(nextBlockStartDateTime.Subtract(dateToFilter).TotalHours)
            '                        If midnightHour = 1 Then
            '                            appointmentEndDate = nextBlockStartDateTime
            '                            appointmentEndDate = CType(nextBlockStartDateTime.ToLongDateString() + " 23:59", Date)
            '                            Exit Do

            '                        End If
            '                    Else
            '                        appointmentStartDate = nextBlockStartDateTime
            '                    End If




            '                End If
            '                Dim operativeWorkingDayHour As Double = Convert.ToDouble(nextBlockEndDateTime.Subtract(appointmentStartDate).TotalHours)
            '                appointmentEndDate = nextBlockEndDateTime



            '                If totalWorkingHour + operativeWorkingDayHour > workDuration Then
            '                    hourDiff = (totalWorkingHour + operativeWorkingDayHour) - workDuration
            '                    appointmentEndDate = nextBlockEndDateTime.AddHours(-hourDiff)
            '                    appointmentEndDate = appointmentStartDate.AddHours(remainingDuration)
            '                End If

            '                If hourDiff > 0 Then
            '                    totalWorkingHour = totalWorkingHour + operativeWorkingDayHour - hourDiff

            '                ElseIf hourDiff <= 0 AndAlso isStartDateSelected Then
            '                    totalWorkingHour = totalWorkingHour + operativeWorkingDayHour

            '                End If
            '                If dateToFilter.Date <> appointmentEndDate.Date Then
            '                    'changeDay(appointmentStartDate, "00:00")
            '                    appointmentStartDate = dateToFilter
            '                    isStartDateSelected = False
            '                    totalWorkingHour = 0
            '                    Continue For
            '                End If
            '                If totalWorkingHour = workDuration Then
            '                    Exit For
            '                End If
            '            End If

            '        Next
            '    End If
            '    changeDay(appointmentStartDate, "00:00")
            '    'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '    If Convert.ToDateTime(dateToFilter.ToShortDateString()) > appointmentDueDate Then
            '        isAppointmentCreated = False
            '        Exit Do
            '    End If

            '    remainingDuration = workDuration - totalWorkingHour
            'Loop While Not (totalWorkingHour = workDuration)


            'Return isAppointmentCreated
            ' return operativeWorkingHourForCurrentDayDt;
        End Function
#End Region

#Region "Change day"

        ''' <summary>
        ''' This function will change day
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub changeDay(ByRef runningDate As Date, ByVal nextDayStartTime As String)
            runningDate = runningDate.AddDays(1)
            runningDate = CType(runningDate.ToLongDateString() + " " + nextDayStartTime, Date)
        End Sub

#End Region




#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetailForContractor(ByVal journalId As Integer, ByVal purchaseOrderId As Integer, ByRef detailsForEmailDS As DataSet)
            objSchedulingDAL.getEmailDetailForContractor(journalId, purchaseOrderId, detailsForEmailDS)
        End Sub

#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetailForOriginator(ByVal journalId As Integer, ByVal purchaseOrderId As Integer, ByRef detailsForEmailDS As DataSet)
            objSchedulingDAL.getEmailDetailForOriginator(journalId, purchaseOrderId, detailsForEmailDS)
        End Sub

#End Region

#Region "Get Property Feul Types"

        Sub GetPropertyFeulTypes(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            objSchedulingDAL.GetPropertyFeulTypes(propertyId, resultDataSet)
        End Sub

#End Region

#Region "Get Scheduling Property Locked By"
        Public Function GetSchedulingPropertyLockedBy(ByVal journalId As Int32, ByVal appointmentType As String)

            Try
                Return objSchedulingDAL.GetSchedulingPropertyLockedBy(journalId, appointmentType)
            Catch ex As Exception
                Throw
            End Try

        End Function
#End Region

#Region "Set Scheduling Property Status"
        Public Function SetSchedulingPropertyStatus(ByVal journalId As Int32, ByVal lock As Integer, ByRef schedulestatus As Integer, ByVal lockedBy As Integer, ByVal appointmentType As String)
            Try
                Return objSchedulingDAL.SetSchedulingPropertyStatus(journalId, lock, schedulestatus, lockedBy, appointmentType)
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

#Region "get Property schedule status"
        Public Function GetSchedulingPropertyStatus(ByVal journalId As Int32, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Try
                Return objSchedulingDAL.GetSchedulingPropertyStatus(journalId, istimeexceeded, isScheduled, appointmentType)
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

#Region "get Arranged Property schedule status"
        Public Function GetArrangedSchedulingPropertyStatus(ByVal journalId As Int32, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Try
                Return objSchedulingDAL.GetArrangedSchedulingPropertyStatus(journalId, istimeexceeded, isScheduled, appointmentType)
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

#End Region
    End Class

End Namespace

