﻿Imports System
Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic

    Public Class AlternativeSchedulingBL

        Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()

#Region "Functions"

#Region "Integlligent Scheduling Functions"

#Region "get Available Operatives For Alternative/Oil Fuel"
        Public Sub GetAvailableOperativesForAlternativeOilFuel(ByRef resultDataSet As DataSet, ByVal fuelServicingType As String)
            Try
                objSchedulingDAL.GetAvailableOperativesForAlternativeOilFuel(resultDataSet, fuelServicingType)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Alternative Appointments By PropertyId"
        Public Sub GetAlternativeAppointmentsByPropertyId(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            Try
                objSchedulingDAL.GetAlternativeAppointmentsByPropertyId(propertyId, resultDataSet)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

#Region "get Appointments Arranged List"
        Public Function GetAlternativeAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal fuelType As Integer, ByVal developmentId As Integer)
            Try

                Return objSchedulingDAL.GetAlternativeAppointmentsArrangedList(resultDataSet, check56Days, searchedText, objPageSortBo, patchId, schemeId, fuelType, developmentId)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "Get Alternative Appointment To Be Arranged List"
        Public Function GetAlternativeAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal alternativeFuelType As Integer, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)
            Try
                Return objSchedulingDAL.GetAlternativeAppointmentToBeArrangedList(resultDataSet, check56Days, searchText, objPageSortBo, alternativeFuelType, status, patchId, developmentId)
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

#Region "Get To Be Arranged Appointments For Alternative"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetToBeAppointmentsForAlternative(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal startDate As Date)

            Dim dt = New DataTable()
            Dim lstAlternativeSchedullingSlotHeader = New List(Of OilAlternativeSchedullingSlotHeader)()
            lstAlternativeSchedullingSlotHeader = SessionManager.GetAlternativeSchedulingHeaderBoList()

            If (lstAlternativeSchedullingSlotHeader.Any()) Then
                dt.Columns.Add("Fuel")
                dt.Columns.Add("Duration")
                dt.Columns.Add("Trade")
                dt.Columns.Add("Status")
                dt.Columns.Add("HeatingTypeId")
                dt.Columns.Add("TradeId")

                For Each item As OilAlternativeSchedullingSlotHeader In lstAlternativeSchedullingSlotHeader
                    Dim row = dt.NewRow()
                    row("Duration") = item.AppointmentDuration
                    row("Status") = item.AppointmentStatus
                    row("Trade") = item.Trade
                    row("Fuel") = item.FuelType
                    row("HeatingTypeId") = item.HeatingTypeId
                    row("TradeId") = item.TradeId
                    dt.Rows.Add(row)
                Next
            Else
                Dim resultSet = New DataSet()
                GetAlternativeAppointmentsHeaderByProperty(propertySchedulingBo.PropertyId, resultSet)

                For Each row As DataRow In resultSet.Tables(0).Rows

                    Dim alternativeSchedullingSlotHeader = New OilAlternativeSchedullingSlotHeader()
                    alternativeSchedullingSlotHeader.AppointmentDuration = row("Duration")
                    alternativeSchedullingSlotHeader.AppointmentStatus = row("Status")
                    alternativeSchedullingSlotHeader.Trade = row("Trade")
                    alternativeSchedullingSlotHeader.FuelType = row("Fuel")
                    alternativeSchedullingSlotHeader.HeatingTypeId = row("HeatingTypeId")
                    alternativeSchedullingSlotHeader.TradeId = row("TradeId")
                    lstAlternativeSchedullingSlotHeader.Add(alternativeSchedullingSlotHeader)

                Next
                dt = resultSet.Tables(0)
            End If

            propertySchedulingBo.Duration = lstAlternativeSchedullingSlotHeader.Sum(Function(item) item.AppointmentDuration)

            SessionManager.SetAlternativeSchedulingHeaderBoList(lstAlternativeSchedullingSlotHeader)

            Dim tempAlternativeSchedullingSlot As AlternativeSchedullingSlot = New AlternativeSchedullingSlot()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.PropertyId = propertySchedulingBo.PropertyId
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FuelType = propertySchedulingBo.FuelType

            'save the start date 
            tempAlternativeSchedullingSlot.StartSelectedDate = startDate
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.aptStartTime = startDate

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            If (IsNothing(propertySchedulingBo.CertificateExpiryDate) = True) Then
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = False
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = True
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FullExpiryDate = propertySchedulingBo.CertificateExpiryDate
            End If
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DisplayCount = 5
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.getAppointmentDurationHours()

            '-------------
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.appendHourLabel(Convert.ToDouble(propertySchedulingBo.Duration))
            If propertySchedulingBo.PropertyStatus = ApplicationConstants.ArrangedStatus Then
                'in case of appointments with status "arranged" or others
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ArrangedStatus
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ToBeArrangedStatus
            End If

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs = CType(propertySchedulingBo.Duration, Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationDays = CType(propertySchedulingBo.Duration, Double) / GeneralHelper.getAppointmentDurationHours()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.dt = dt
            'tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.addNewDataRow()
            'changes(here)

            Dim filteredOperativesDt As DataTable = New DataTable()
            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = ApplyPatchFilterForAlternative(operativesDt, tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, propertySchedulingBo.PatchId)

            'this function 'll create the appointment slot based on leaves, appointments & due date

            Me.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, appointmentsDt, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                                          GeneralHelper.getAppointmentCreationLimitForSingleRequest(), propertySchedulingBo)

            ' ''this function 'll sort the appointments by appointment date and distance
            OrderAlternativeAppointmentsByDistance(tempAlternativeSchedullingSlot)
            SessionManager.SetSelectedPropertySchedulingBo(propertySchedulingBo)
            Return tempAlternativeSchedullingSlot
        End Function
#End Region

#Region "Get Arranged Appointments For Alternative"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetArrangedAppointmentsForAlternative(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal startDate As Date, ByVal journalId As Integer, ByVal appointmentId As Integer)

            Dim dt = New DataTable()
            Dim alternativeSchedullingSlotHeaderLst = New List(Of OilAlternativeSchedullingSlotHeader)()
            alternativeSchedullingSlotHeaderLst = SessionManager.GetAlternativeSchedulingHeaderBoList()

            If (alternativeSchedullingSlotHeaderLst.Any()) Then
                dt.Columns.Add("FuelType")
                dt.Columns.Add("Duration")
                dt.Columns.Add("Trade")
                dt.Columns.Add("Status")
                dt.Columns.Add("HeatingTypeId")
                dt.Columns.Add("TradeId")
                For Each item As OilAlternativeSchedullingSlotHeader In alternativeSchedullingSlotHeaderLst
                    Dim row = dt.NewRow()
                    row("Duration") = item.AppointmentDuration
                    row("Status") = item.AppointmentStatus
                    row("Trade") = item.Trade
                    row("FuelType") = item.FuelType
                    row("HeatingTypeId") = item.HeatingTypeId
                    row("TradeId") = item.TradeId
                    dt.Rows.Add(row)
                Next
            Else
                Dim resultSet = New DataSet()
                objSchedulingDAL.GetHeatingAppointmentDuration(journalId, appointmentId, resultSet)

                For Each row As DataRow In resultSet.Tables(0).Rows

                    Dim alternativeSchedullingSlotHeader = New OilAlternativeSchedullingSlotHeader()
                    alternativeSchedullingSlotHeader.AppointmentDuration = row("Duration")
                    alternativeSchedullingSlotHeader.AppointmentStatus = row("Status")
                    alternativeSchedullingSlotHeader.Trade = row("Trade")
                    alternativeSchedullingSlotHeader.FuelType = row("FuelType")
                    alternativeSchedullingSlotHeader.HeatingTypeId = row("HeatingTypeId")
                    alternativeSchedullingSlotHeader.TradeId = row("TradeId")
                    alternativeSchedullingSlotHeaderLst.Add(alternativeSchedullingSlotHeader)

                Next
                dt = resultSet.Tables(0)
            End If

            SessionManager.SetAlternativeSchedulingHeaderBoList(alternativeSchedullingSlotHeaderLst)
            propertySchedulingBo.Duration = alternativeSchedullingSlotHeaderLst.Sum(Function(item) item.AppointmentDuration)

            Dim tempAlternativeSchedullingSlot As AlternativeSchedullingSlot = New AlternativeSchedullingSlot()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.PropertyId = propertySchedulingBo.PropertyId
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FuelType = propertySchedulingBo.FuelType

            'save the start date 
            tempAlternativeSchedullingSlot.StartSelectedDate = startDate
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.aptStartTime = startDate

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            If (IsNothing(propertySchedulingBo.CertificateExpiryDate) = True) Then
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = False
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = True
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FullExpiryDate = propertySchedulingBo.CertificateExpiryDate
            End If
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DisplayCount = 5
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.getAppointmentDurationHours()

            '-------------
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.appendHourLabel(Convert.ToDouble(propertySchedulingBo.Duration))
            If propertySchedulingBo.PropertyStatus = ApplicationConstants.ArrangedStatus Then
                'in case of appointments with status "arranged" or others
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ArrangedStatus
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ToBeArrangedStatus
            End If

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs = CType(propertySchedulingBo.Duration, Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationDays = CType(propertySchedulingBo.Duration, Double) / GeneralHelper.getAppointmentDurationHours()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.dt = dt

            'tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.addNewDataRow()
            'changes(here)

            Dim filteredOperativesDt As DataTable = New DataTable()
            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = ApplyPatchFilterForAlternative(operativesDt, tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, propertySchedulingBo.PatchId)

            'this function 'll create the appointment slot based on leaves, appointments & due date

            Me.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, appointmentsDt, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                                          GeneralHelper.getAppointmentCreationLimitForSingleRequest(), propertySchedulingBo)

            ' ''this function 'll sort the appointments by appointment date and distance
            OrderAlternativeAppointmentsByDistance(tempAlternativeSchedullingSlot)
            SessionManager.SetSelectedPropertySchedulingBo(propertySchedulingBo)
            Return tempAlternativeSchedullingSlot
        End Function
#End Region

#Region "apply Patch Filter For Alternative"
        ''' <summary>
        ''' If patch check box is selected this function 'll sort out the operatives who have same selected property's patch 
        ''' otherwise if patch is unchecked it 'll return all the operatvies
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="alternativeAppointmentSlotsDtBO"></param>
        ''' <param name="patchId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function ApplyPatchFilterForAlternative(ByVal operativesDt As DataTable, ByRef alternativeAppointmentSlotsDtBO As AlternativeAppointmentSlotsDtBO, ByVal patchId As Integer)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'if patch is selected then filter the operatives which have the same patch as selected property's patch
            If alternativeAppointmentSlotsDtBO.IsPatchSelected = True Then
                '(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = String.Format("PatchId = {0} OR PatchId =  -1 ", patchId.ToString())
                filteredOperativesDt = operativesDv.ToTable()

                'get the ids of filtered operatives
                Dim employeeIds As String = String.Empty
                For Each dr As DataRow In filteredOperativesDt.Rows
                    employeeIds = employeeIds + dr.Item("EmployeeId").ToString() + ","
                Next
                If employeeIds.Length() > 1 Then
                    employeeIds = employeeIds.Substring(0, employeeIds.Length() - 1)

                    'select the appointments based on operatives which are filtered above
                    Dim tempAptDv As DataView = New DataView()
                    tempAptDv = alternativeAppointmentSlotsDtBO.dt.AsDataView()
                    tempAptDv.RowFilter = AppointmentSlotsDtBO.operativeIdColName + " IN (" + employeeIds + ")"
                    alternativeAppointmentSlotsDtBO.dt = tempAptDv.ToTable()
                Else
                    alternativeAppointmentSlotsDtBO.dt.Clear()
                End If
            Else
                filteredOperativesDt = operativesDv.ToTable()
            End If

            Return filteredOperativesDt
        End Function
#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="aptStartTime"></param>
        ''' <param name="aptEndTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Public Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal aptEndTime As Date)

            aptStartTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)
            aptEndTime = CType(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(aptStartTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(aptEndTime)

            Dim leaves = leavesDt.AsEnumerable()

            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin = app("StartTimeInMin") And lEndTimeInMin < app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin = app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin > app("StartTimeInMin") And lEndTimeInMin < app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
            Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "Order Temp Alternative Appointments By Distance"
        ''' <summary>
        ''' 'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment 
        ''' </summary>
        ''' <param name="alternativeSchedullingSlot"></param>
        ''' <remarks></remarks>
        Public Sub OrderAlternativeAppointmentsByDistance(ByRef alternativeSchedullingSlot As AlternativeSchedullingSlot)
            Dim appointments = alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.AsEnumerable
            Dim appResult = (From app In appointments Select app _
                             Order By CType(app(AlternativeAppointmentSlotsDtBO.DistanceColName), Double) Ascending _
                             , CType(app(AlternativeAppointmentSlotsDtBO.StartDateStringColName), Date) Ascending)
            If appResult.Any() Then
                alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt = appResult.CopyToDataTable()
            End If

        End Sub
#End Region

#End Region

#Region "Intelligent Scheduling Algorithm with core working and extended working hours"


#Region "update Last added column in tempAlternativeAptBo data table"
        ''' <summary>
        ''' 'update Last added column in tempAlternativeAptBo datatable 
        ''' </summary>
        ''' <param name="tempAlternativeAptBo"></param>
        ''' <remarks></remarks>
        Private Sub UpdateLastAddedForAlternative(ByRef tempAlternativeAptBo As AlternativeAppointmentSlotsDtBO)
            Dim appointments = tempAlternativeAptBo.dt.AsEnumerable()
            Dim appResult = (From app In appointments Where Convert.ToBoolean(app(TempAppointmentDtBO.lastAddedColName)) = True Select app)
            If appResult.Any() Then
                appResult.First()(TempAppointmentDtBO.lastAddedColName) = False
            End If
        End Sub
#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="leavesDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appoitmentEndDateTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appoitmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(appointmentStartDateTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(appoitmentEndDateTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
             Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime "></param>
        ''' <param name="appointmentEndDateTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                            Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region

#Region "create Alternative TempAppointment Slots"

        Public Sub CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempAppointmentDtBo As AlternativeAppointmentSlotsDtBO, ByVal selectedStartDate As Date, ByVal tradeDurationHrs As Double, ByVal requiredResultCount As Integer, ByVal selectedPropertySchedulingBo As SelectedPropertySchedulingBO, Optional ByVal previousOperativeId As Integer = 0)

            If tradeDurationHrs <= 0 Then
                Exit Sub
            End If
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim patchName As String = String.Empty
            Dim operativePostCode As String = String.Empty
            Dim dueDateCheckCount As New Hashtable()
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(selectedStartDate)
            Dim dayEndHour As Double = GeneralHelper.getDayEndHour()

            Dim displayedTimeSlotCount As Integer = 1
            Dim sDate As DateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
            Dim appointmentStartDateTime As System.DateTime = Nothing
            Dim appointmentEndDate As System.DateTime = Nothing
            'System.DateTime aptEndTime = new System.DateTime();

            Dim operativeEligibility As New Dictionary(Of Integer, Boolean)()
            Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
            Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + (If(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString()))).ToString("HH:mm")
            Dim skipOperative As Boolean = False

            Dim appointmentDueDate As DateTime = Convert.ToDateTime(DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")

            'Just to initlize start date and end date.
            appointmentStartDateTime = selectedStartDate
            appointmentEndDate = appointmentStartDateTime

            'Calculate Time slots given in count
            ' Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            While displayedTimeSlotCount <= requiredResultCount
                Dim uniqueCounter As Integer = 0
                Dim operativeCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop

                If operativesDt.Rows.Count = 0 Then
                    ' TODO: might not be correct. Was : Exit While
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    skipOperative = False
                    uniqueCounter += 1
                    operativeCounter += 1
                    operativeId = Convert.ToInt32(operativeDr("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr("FullName"))
                    operativePostCode = Convert.ToString(operativeDr("PostCode"))
                    patchName = Convert.ToString(operativeDr("PatchName"))
                    Dim operativeWorkingHours As New DataSet()
                    Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()

                    objSchedulingDAL.getOperativeWorkingHours(operativeWorkingHours, operativeId, ofcCoreStartTime, ofcCoreEndTime)
                    SessionManager.setOperativeworkingHourDs(operativeWorkingHours)

                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 

                    If (previousOperativeId <> 0) Then
                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For

                        End If
                    End If

                    'if syetem does not get any slot for an operative then system will check in maximum days which Defined in web.cong file.
                    ' Other wise system will goes to infinit loop. 
                    appointmentDueDate = Convert.ToDateTime(System.DateTime.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59")


                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempAppointmentDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots _
                                       Where app(tempAppointmentDtBo.OperativeIdColName) = operativeId _
                                       Select app _
                                       Order By CType(app(tempAppointmentDtBo.EndDateStringColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Any() Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = Convert.ToDateTime(slotsResult.Last().Field(Of String)(tempAppointmentDtBo.EndDateStringColName))
                    Else
                        'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        'appointmentStartDateTime = sDate;
                        appointmentStartDateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00")
                    End If

                    'Set appointment start time and appointment end time according to Core, Extended and On call Hour

                    Me.SetAlternativeVoidAppointmentStartEndTime(appointmentStartDateTime, appointmentEndDate, tradeDurationHrs, skipOperative, leavesDt, appointmentsDt, _
                     operativeId, appointmentDueDate, dueDateCheckCount, uniqueCounter, tempAppointmentDtBo)

                    If (skipOperative = True) Then
                        'Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        'If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        'this condition 'll check either due date check has been executed against all the operatives
                        If (dueDateCheckCount.Count = operativesDt.Rows.Count) Then
                            'break; // TODO: might not be correct. Was : 
                            Return
                        Else
                            Continue For
                        End If
                    End If

                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    Dim distance As Double = GetDistanceFromProperty(appointmentsDt, operativeId, appointmentStartDateTime, GeneralHelper.getHrsAndMinString(appointmentStartDateTime), selectedPropertySchedulingBo, operativePostCode)

                    'update the lastAdded column in tempfaultBO.dt
                    'updateLastAdded(ref tempFaultAptBo);
                    'add this time slot in appointment list
                    tempAppointmentDtBo.distance = distance
                    tempAppointmentDtBo.operativeId = operativeId
                    tempAppointmentDtBo.operative = operativeName
                    'tempAppointmentDtBo.patch = Convert.ToString(operativeDr("PatchId"))
                    tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(appointmentStartDateTime)
                    tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(appointmentEndDate)
                    tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(appointmentStartDateTime)
                    tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(appointmentEndDate)
                    tempAppointmentDtBo.lastAdded = True
                    UpdateLastAddedForAlternative(tempAppointmentDtBo)
                    tempAppointmentDtBo.addNewDataRow()
                    'tempFaultAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom, distanceTo);

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > requiredResultCount Then
                        ' TODO: might not be correct. Was : Exit While
                        Exit For
                    End If
                Next
            End While
            'Calculate Time slots loop ends here
        End Sub

#End Region

#Region "Day calculation algorithm for alternative servicing."

        Private Function GetAlternativeOperativeAvailableSlotsForMultipleDays(ByVal operativeWorkingHourDt As DataTable, ByRef dateToFilter As DateTime, ByRef appointmentEndDate As DateTime, ByVal workDuration As Double, ByVal appointmentDueDate As DateTime, _
                                                                   ByRef loggingTime As Integer) As Boolean
            Dim totalWorkingHour As Double = 0
            Dim hourDiff As Double = 0
            Dim isAppointmentCreated As Boolean = True
            Dim appointmentStartDate As DateTime = dateToFilter
            Dim isStartDateSelected As Boolean = False
            Dim isExtendedIncluded As Boolean = False
            'extended ot Oncall hours included foe specific day ot not.
            Dim remainingDuration As Double = workDuration
            Do

                Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                                           Where ((optHour(ApplicationConstants.WeekDayCol).ToString() = appointmentStartDate.DayOfWeek.ToString) _
                                                                And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                                                And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                                                And optHour(ApplicationConstants.StartTimeCol) <> "") _
                                                           Select optHour _
                                                           Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > appointmentStartDate.TimeOfDay))

                ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
                Dim operativeWorkingHourForCurrentDayDt As DataTable = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())

                If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
                    'Iterate result set including core, Extended and on call hours.
                    For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                        'get operative day start hour for specific day
                        Dim nextBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.StartTimeColumn).ToString()
                        Dim nextBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockStartTime)
                        loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)
                        ' get operative day end hour for specific day
                        Dim nextBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeColumn).ToString()
                        Dim nextBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(appointmentStartDate.ToLongDateString() + " ") & nextBlockEndTime)

                        'If operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString() <> "NA" Then
                        '    Dim extendedBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString()
                        '    Dim ExtendedBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.StartDateColumn)).ToLongDateString() + " ") & nextBlockStartTime)
                        '    Dim extendedBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString()
                        '    Dim ExtendedBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn)).ToLongDateString() + " ") & nextBlockEndTime)
                        '    If nextBlockStartDateTime.ToString("dd/MM/yyyy") = ExtendedBlockStartDateTime.ToString("dd/MM/yyyy") Then
                        '        isExtendedIncluded = True

                        '    End If
                        'End If
                        'If nextBlockEndDateTime > dateToFilter AndAlso (operativeWorkingHoursBlock(ApplicationConstants.EndDateColumn).ToString() = "NA" OrElse isExtendedIncluded) Then
                        If nextBlockEndDateTime > dateToFilter Then

                            If Not isStartDateSelected AndAlso nextBlockStartDateTime >= appointmentStartDate Then
                                isStartDateSelected = True
                                dateToFilter = nextBlockStartDateTime
                                appointmentStartDate = nextBlockStartDateTime

                            ElseIf Not isStartDateSelected AndAlso dateToFilter > nextBlockStartDateTime Then
                                isStartDateSelected = True
                            ElseIf nextBlockStartDateTime >= appointmentStartDate Then
                                appointmentStartDate = nextBlockStartDateTime
                            End If
                            Dim operativeWorkingDayHour As Double = Convert.ToDouble(nextBlockEndDateTime.Subtract(appointmentStartDate).TotalHours)
                            appointmentEndDate = nextBlockEndDateTime



                            If totalWorkingHour + operativeWorkingDayHour > workDuration Then
                                hourDiff = (totalWorkingHour + operativeWorkingDayHour) - workDuration
                                appointmentEndDate = nextBlockEndDateTime.AddHours(-hourDiff)
                                appointmentEndDate = appointmentStartDate.AddHours(remainingDuration)
                            End If

                            If hourDiff > 0 Then
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour - hourDiff

                            ElseIf hourDiff <= 0 AndAlso isStartDateSelected Then
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour

                            End If

                            If totalWorkingHour = workDuration Then
                                Exit For
                            End If
                        End If

                    Next
                End If
                ChangeDay(appointmentStartDate, "00:00")
                'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
                If Convert.ToDateTime(dateToFilter.ToShortDateString()) > appointmentDueDate Then
                    isAppointmentCreated = False
                    Exit Do
                End If

                remainingDuration = workDuration - totalWorkingHour
            Loop While Not (totalWorkingHour = workDuration)


            Return isAppointmentCreated
            ' return operativeWorkingHourForCurrentDayDt;
        End Function
#End Region


        ''' <summary>
        ''' This function calculates the appointment start time and appointment end time. We are assuming that end date 'll be used to calculate the new start date and end date
        ''' </summary>
        ''' <param name="appointmentStartDate"></param>
        ''' <param name="appointmentEndDate"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="skipOperative"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentDueDate"></param>
        ''' <param name="dueDateCheckCount"></param>
        ''' <param name="uniqueCounter"></param>
        ''' <remarks></remarks>

        Private Sub SetAlternativeVoidAppointmentStartEndTime(ByRef appointmentStartDate As DateTime, ByRef appointmentEndDate As DateTime, ByVal workDuration As Double, ByRef skipOperative As Boolean, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, _
            ByVal operativeId As Integer, ByVal appointmentDueDate As DateTime, ByRef dueDateCheckCount As Hashtable, ByRef uniqueCounter As Integer, ByVal tempTradeAptBo As AlternativeAppointmentSlotsDtBO)

            Dim isAppointmentCreated As Boolean = False
            Dim isExtendedApptVerified As Boolean = False
            'Get Operative core and extended working hours from session
            Dim operativeWorkingHourDs As DataSet = SessionManager.getOperativeworkingHourDs
            Dim operativeWorkingHourDt As DataTable = operativeWorkingHourDs.Tables(0)

            Dim currentRunningDate As System.DateTime = appointmentStartDate

            Dim loggingTime As Integer = 0
            'Do Until: appointment is created or the operative is skipped
            Do
                'Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future) 

                If IsOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, appointmentDueDate) Then

                    skipOperative = True
                Else
                    'Get operatives running day available time in ascending order of StartTime column
                    '  Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate)

                    isAppointmentCreated = GetAlternativeOperativeAvailableSlotsForMultipleDays(operativeWorkingHourDt, appointmentStartDate, appointmentEndDate, workDuration, appointmentDueDate, loggingTime)
                End If

                If isAppointmentCreated AndAlso Not skipOperative Then
                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate)
                    'if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    If leaveResult.Any() Then
                        appointmentStartDate = Convert.ToDateTime(leaveResult.Last().Field(Of DateTime)("EndDate"))
                        appointmentStartDate = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Field(Of String)("EndTime"))
                        isAppointmentCreated = False
                    Else
                        'if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this will be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate)
                        If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                            'appointmentStartDate = Convert.ToDateTime(appResult.Last().Field(Of DateTime)("AppointmentEndDate"))
                            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            isAppointmentCreated = False
                            ' Comment this area because Extended and Oncall hour should not be available in planned appointment 
                            'If isExtendedApptVerified = False Then

                            '    If appResult.Last().Field(Of Integer)("CreationDate") < loggingTime Then

                            '        Dim isExtendedHoursExist As Boolean = isExtendedIncludedInAppointment(operativeWorkingHourDt, appointmentsDt, appointmentStartDate, appResult.Last().Field(Of Integer)("CreationDate"), workDuration, operativeId, loggingTime, tempTradeAptBo)
                            '        If isExtendedHoursExist = False Then
                            '            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            '        Else
                            '            isExtendedApptVerified = True
                            '        End If

                            '    Else
                            '        appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                            '    End If
                            '    isAppointmentCreated = False

                            'End If
                        End If
                    End If

                End If
            Loop While Not ((isAppointmentCreated OrElse skipOperative))
        End Sub

#End Region

#Region "Is Extended Hours included in Appointment"
        ''' <summary>
        ''' Is Extended Hours included in Appointment of Multiple days.
        ''' </summary>
        ''' <param name="operativeWorkingHourDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="dateToFilter"></param>
        ''' <param name="creationDateInSecond"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="loggingTime"></param>
        ''' <param name="alternativeAppointmentDtBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedIncludedInAppointment(ByVal operativeWorkingHourDt As DataTable, ByVal appointmentsDt As DataTable, ByRef dateToFilter As DateTime, ByVal creationDateInSecond As Integer, _
                                                    ByVal workDuration As Double, ByVal operativeId As Integer, ByVal loggingTime As Integer, ByVal alternativeAppointmentDtBO As AlternativeAppointmentSlotsDtBO)
            Dim appointmentStartDate As DateTime = dateToFilter
            Dim result As Boolean = False
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                                          Where optHour(ApplicationConstants.CreationDateCol) > creationDateInSecond _
                                                               And optHour(ApplicationConstants.StartDateCol) <> "NA" _
                                                               And optHour(ApplicationConstants.EndDateCol) <> "NA" _
                                                               And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                          Select optHour _
                                                          Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending)

            Dim operativeWorkingHourForCurrentDayDt As DataTable = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())
            If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then

                For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                    Dim extendedBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.StartTimeColumn).ToString()
                    Dim extendedBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.StartDateCol)).ToLongDateString() + " ") & extendedBlockStartTime)
                    dateToFilter = extendedBlockStartDateTime
                    result = True
                    ' get operative day end hour for specific day
                    Dim extendedBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeColumn).ToString()
                    Dim extendedBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.EndDateCol)).ToLongDateString() + " ") & extendedBlockEndTime)

                    Dim isTimeSlotAlreadyAdded As Boolean = False
                    isTimeSlotAlreadyAdded = checkTimeSlotDisplayed(alternativeAppointmentDtBO, dateToFilter, extendedBlockEndDateTime, operativeId)


                    Dim apptEndDateTime As DateTime = dateToFilter.AddHours(workDuration)
                    Dim apptEndDateTimeInSec As Integer
                    Dim extendedBlockEndDateTimeInSec As Integer
                    apptEndDateTimeInSec = GeneralHelper.convertDateInSec(apptEndDateTime)
                    extendedBlockEndDateTimeInSec = GeneralHelper.convertDateInSec(extendedBlockEndDateTime)
                    If apptEndDateTimeInSec > extendedBlockEndDateTimeInSec Then
                        result = False
                        Continue For
                    Else

                        Dim isExtendedApptExist As Boolean = True
                        isExtendedApptExist = isExtendedAppointmentExist(appointmentsDt, operativeId, dateToFilter, apptEndDateTime, loggingTime)
                        If isExtendedApptExist = False Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return result

        End Function

#End Region

#Region "is Extended Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given Extended start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appointmentEndDateTime"></param>
        ''' <param name="loggingTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByRef appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date, ByVal loggingTime As Integer)

            ''çonvert time into seconds for comparison

            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer
            Dim result As Boolean = False
            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                                                        AndAlso app("CreationDate") > loggingTime _
                            Select app Order By app("EndTimeInSec") Ascending)
            If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                appointmentStartDateTime = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                result = True
            End If

            Return result
        End Function
#End Region


#Region "Check Time Slot already displayed in Gridview"
        ''' <summary>
        ''' Check Time Slot already displayed in Gridview
        ''' </summary>
        ''' <param name="alternativeAppointmentDtBO"></param>
        ''' <remarks></remarks>
        Public Function checkTimeSlotDisplayed(ByVal alternativeAppointmentDtBO As AlternativeAppointmentSlotsDtBO, ByRef appointmentStartDate As DateTime, ByVal extendedBlockEndDateTime As DateTime, ByVal operativeId As Integer)
            Dim result As Boolean = False
            Dim runningDate As DateTime = appointmentStartDate


            Dim appointments = alternativeAppointmentDtBO.dt.AsEnumerable
            Dim appResult = (From app In appointments _
                           Where app(alternativeAppointmentDtBO.StartDateStringColName) >= runningDate _
                          AndAlso app(alternativeAppointmentDtBO.EndDateStringColName) <= extendedBlockEndDateTime _
                           And app(TempAppointmentDtBO.IdColName) = operativeId Select app Order By app(alternativeAppointmentDtBO.StartDateStringColName) Ascending)
            If appResult.Count() > 0 Then
                appointmentStartDate = appResult.Last.Item(alternativeAppointmentDtBO.EndDateStringColName)
                result = True
            End If
            Return result
        End Function

#End Region

#Region "Get Operative Distance From Property"
        ''' <summary>
        ''' This appointment 'll calculate the distance between operatives last appointment slot and running appointment slot (running means the slot that is being calculated at run time)
        ''' The rule would work like this
        ''' The distance will be calculated via the selected property and the last appointment (in order of time) prior to the available slot on the day 
        ''' Examples:
        ''' Case 1
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 11:00 - 12:00
        ''' Lets assume that Carl had 1 appointment from 9:00 - 10:00 at Property 1. Similarly he has another appointment from 10:00 - 11:00 at Property 2. The distance ail be calculated between selected property and Property2. The reason we chose Property 2 is because Carl will be here prior to his free slot 11:00 - 12:00
        ''' Case2
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 9:00 - 11:00
        ''' Lets assume that Carl had no appointments on this day. In case there is no prior booked appointment before the available appointment slot, we shall display 0 in the distance column
        ''' </summary>
        ''' <param name="operativeId"></param>
        ''' <param name="runningDate"></param>
        ''' <param name="aptStartTime"></param>
        ''' <remarks></remarks>
        Private Function GetDistanceFromProperty(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal aptStartTime As Date, ByVal selectedPropertySchedulingBo As SelectedPropertySchedulingBO, ByVal operativePostCode As String) As Double
            'Distance
            Dim distance As Double = 0

            'get the end time of running time slot (date + start time of appointment) 
            'running appointment date time means the appointment time which is being calculated in loop
            Dim runningAptDateTime As Date
            runningAptDateTime = CType(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString(), Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startDate As New DateTime(1970, 1, 1)
            Dim runningAptTimeInSec As Integer
            ' we are creating this variable because we want to compare the dates from db and from calcuated date with time as 00:00:00
            ' in order to do the right comparison
            Dim runningDateAlias As Date = New Date()

            runningAptTimeInSec = (runningAptDateTime - startDate).TotalSeconds
            'creating the alias for comparison with 00:00:00 time
            runningDateAlias = CType(runningDate.ToLongDateString() + " " + "00:00:00", Date)


            Dim appointments = appointmentsDt.AsEnumerable()
            Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("EndTimeInSec") <= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("EndTimeInSec"), Integer) Descending)

            'if operative has any appointment prior to the running appointment slot then 
            'find the distance between customer property address and property address of operatives last arranged appointment on the same day
            If appResult.Any() Then
                'get the property address of running time slot
                Dim fromPropertyAddress As String
                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = appResult.First().Item("Address") + "+" + appResult.First().Item("TownCity") + "+" + appResult.First().Item("County")
                fromPropertyAddress = fromPropertyAddress.Replace(" ", "+")

                Dim toPropertyAddress As String
                toPropertyAddress = selectedPropertySchedulingBo.PropertyAddress.Replace(" ", "+")

                ''get the distance from session if this calcuation was done previously
                Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                propertyAddressDictionary = SessionManager.getPropertyDistance()
                Dim propertyResult = (From ps In propertyAddressDictionary Where ps.Value.FromPropertyAddress = fromPropertyAddress And ps.Value.ToPropertyAddress = toPropertyAddress Select ps)

                'if we have found the distance of property address from session then we have save the call of map api
                If propertyResult.Count > 0 Then
                    distance = propertyResult.First.Value.Distance
                Else
                    'we didnt' find the property distance from session then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress)

                    'save the property's distance in session so that we can save map api call in future calculations
                    Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                    'this 'll be used to create the unique index of dictionary
                    Dim uniqueKey As String = GeneralHelper.getUniqueKey(0)

                    propertyDistance.FromPropertyAddress = fromPropertyAddress
                    propertyDistance.ToPropertyAddress = selectedPropertySchedulingBo.PropertyAddress
                    propertyDistance.Distance = distance
                    If (propertyAddressDictionary.ContainsKey(uniqueKey) = False) Then
                        propertyAddressDictionary.Add(uniqueKey, propertyDistance)
                    End If

                    'set the dictionary of property distances  in session
                    SessionManager.setPropertyDistance(propertyAddressDictionary)
                End If
            Else

                'get the property address of running time slot
                Dim fromPropertyAddress As String
                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = operativePostCode.Replace(" ", "+")

                Dim toPropertyAddress As String

                If selectedPropertySchedulingBo.PostCode.Length > 0 Then
                    toPropertyAddress = selectedPropertySchedulingBo.PostCode.Replace(" ", "+")

                Else
                    toPropertyAddress = selectedPropertySchedulingBo.PropertyAddress.Replace(" ", "+")
                End If

                ''get the distance from session if this calcuation was done previously
                Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                propertyAddressDictionary = SessionManager.getPropertyDistance()
                Dim propertyResult = (From ps In propertyAddressDictionary Where ps.Value.FromPropertyAddress = fromPropertyAddress And ps.Value.ToPropertyAddress = toPropertyAddress Select ps)

                'if we have found the distance of property address from session then we have save the call of map api
                If propertyResult.Count > 0 Then
                    distance = propertyResult.First.Value.Distance
                Else
                    'we didnt' find the property distance from session then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress)

                    'save the property's distance in session so that we can save map api call in future calculations
                    Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                    'this 'll be used to create the unique index of dictionary
                    Dim uniqueKey As String = GeneralHelper.getUniqueKey(0)

                    propertyDistance.FromPropertyAddress = fromPropertyAddress
                    propertyDistance.ToPropertyAddress = toPropertyAddress
                    propertyDistance.Distance = distance
                    If (propertyAddressDictionary.ContainsKey(uniqueKey) = False) Then
                        propertyAddressDictionary.Add(uniqueKey, propertyDistance)
                    End If

                    'set the dictionary of property distances  in session
                    SessionManager.setPropertyDistance(propertyAddressDictionary)
                End If

            End If

            Return distance
        End Function
#End Region

#Region "Change day"

        ''' <summary>
        ''' This function will change day
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ChangeDay(ByRef runningDate As Date, ByVal nextDayStartTime As String)
            runningDate = runningDate.AddDays(1)
            runningDate = CType(runningDate.ToLongDateString() + " " + nextDayStartTime, Date)
        End Sub

#End Region

#Region "is Opertive Available"
        Private Function IsOpertiveAvailable(ByVal operativeWorkingHourDt As DataTable, ByVal dateToCheck As DateTime, ByRef dueDateCheckCount As Hashtable, ByVal operativeId As Integer, ByVal uniqueCounter As Integer, ByVal appointmentDueDate As DateTime) As Boolean

            '1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            Dim operativeAvailable As Boolean = CheckDateIsGreaterThanDueDateAndAddToSkipList(uniqueCounter, dueDateCheckCount, dateToCheck, operativeId, appointmentDueDate) _
                                                OrElse Not CheckOperativeAvailability(dateToCheck, operativeWorkingHourDt)

            Return operativeAvailable
        End Function

#End Region

#Region "if Date Is Greater Than Due Date"

        ''' <summary>
        ''' This function 'll check if running date is greater than due date of fault. If its true then add the entry in hash table and this (current) operative 'll not process any more for appointments
        ''' The count of total entries in hash table 'll help us to check that all operatives has been processed against the fault due date
        ''' </summary>
        ''' <param name="uniqueCounter"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="operativeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CheckDateIsGreaterThanDueDateAndAddToSkipList(ByRef uniqueCounter As Integer, ByRef dueDateCheckCount As Hashtable, ByVal dateToCheck As Date, ByVal operativeId As Integer, ByVal appointmentDueDate As DateTime) As Boolean
            Dim operativeSkipped As Boolean = False

            'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            If CType(dateToCheck.ToShortDateString(), Date) > appointmentDueDate Then
                operativeSkipped = True
                'this variable 'll check either due date check has been executed against all the operatives
                If Not (dueDateCheckCount.ContainsValue(operativeId)) Then
                    If dueDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                    Else
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                    End If
                End If
            End If
            Return operativeSkipped
        End Function

#End Region

#Region "Check operative availability"
        ''' <summary>
        ''' This function checks whether operative is fully absent in core and extended hours. If yes then this 
        ''' operative will be skipped.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="operativeHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CheckOperativeAvailability(ByVal runningDate As DateTime, ByVal operativeHoursDt As DataTable)

            Dim isAvailable As Boolean = True
            Dim workingHours = operativeHoursDt.AsEnumerable

            Dim coreWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) = "NA" _
                                            And app(ApplicationConstants.EndDateCol) = "NA" _
                                            And app(ApplicationConstants.StartTimeCol) <> "" _
                                            Select app)

            Dim extendedWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) <> "NA" _
                                            Select app)

            If (extendedWorkingHoursResult.Count > 0) Then
                extendedWorkingHoursResult = (From app In extendedWorkingHoursResult.CopyToDataTable().AsEnumerable() _
                                           Where Convert.ToDateTime(app(ApplicationConstants.StartDateCol)) >= runningDate _
                                           Select app)
            End If

            If coreWorkingHoursResult.Count = 0 And extendedWorkingHoursResult.Count = 0 Then
                isAvailable = False
            End If

            Return isAvailable

        End Function

#End Region

#Region "add More Temp Trade Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="alternativeSchedullingSlot"></param>
        ''' <param name="startDate"></param>
        ''' <param name="startAgain">This 'll help to identify that user wants to fetch the appointments onward from the selected date</param>
        ''' <remarks></remarks>
        Public Sub AddMoreTempAltrnativeAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef alternativeSchedullingSlot As AlternativeSchedullingSlot, ByVal startDate As Date, ByVal selectedPropertySchedulingBO As SelectedPropertySchedulingBO, Optional ByVal startAgain As Boolean = False)
            ' this id 'll retain the operative id, against which the slots were created
            Dim previousOperativeId As Integer = 0
            'retreive the trade id 
            Dim filteredOperativesDt As DataTable = operativesDt
            'This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
            ' This function 'll furthur help us to minimize the time to create appointment slots
            Dim filteredAppointmentsDt As DataTable = appointmentsDt
            filteredOperativesDt = ApplyPatchFilterForAlternative(operativesDt, alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, selectedPropertySchedulingBO.PatchId)

            If startAgain = False Then
                'fetch the previous operative id so the operatives name should appear in order                
                Dim appointments = alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app(alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.lastAddedColName) = True Select app)
                If appResult.Any() Then
                    previousOperativeId = appResult.LastOrDefault.Item(alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.OperativeIdColName)
                    'This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                    'as we have previous operative id , so it 'll continue creating the appointment slots after exsiting slots
                    Me.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt, _
                                                  alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                  alternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                                  GeneralHelper.getAppointmentCreationLimitForSingleRequest(), selectedPropertySchedulingBO, previousOperativeId)
                End If
            Else
                'This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                ' This 'll restart appointments slot from the start date
                alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.Clear()
                Me.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, filteredAppointmentsDt, _
                                                alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                alternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                              GeneralHelper.getAppointmentCreationLimitForSingleRequest(), selectedPropertySchedulingBO)
            End If
        End Sub
#End Region

#Region "Get Alternative Appointments Header By PropertyId"

        Private Sub GetAlternativeAppointmentsHeaderByProperty(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            objSchedulingDAL.GetAlternativeAppointmentsHeaderByProperty(propertyId, resultDataSet)
        End Sub

#End Region

#Region "Get Heating Appointment Duration"

        Sub GetHeatingAppointmentDuration(ByVal journalId As Int32, ByVal appointmentId As Int32, ByRef resultDataSet As DataSet)
            objSchedulingDAL.GetHeatingAppointmentDuration(journalId, appointmentId, resultDataSet)
        End Sub

#End Region

#Region "Set Heating Appointment Duration"

        Public Function SetHeatingAppointmentDuration(ByVal appointmentId As Int32, ByVal appointmentHeatingFuelDuration As DataTable)
            Return objSchedulingDAL.SetHeatingAppointmentDuration(appointmentId, appointmentHeatingFuelDuration)
        End Function

#End Region

#Region "Set Heating Appointment Duration"

        Public Function UpdateHeatingAppointmentDuration(ByVal appointmentId As Int32, ByVal appointmentHeatingFuelDuration As DataTable)
            Return objSchedulingDAL.UpdateHeatingAppointmentDuration(appointmentId, appointmentHeatingFuelDuration)
        End Function

#End Region


#End Region
    End Class

End Namespace

