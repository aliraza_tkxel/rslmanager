﻿Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class OilSchedulingBL
        Dim objSchedulingDAL As SchedulingDAL = New SchedulingDAL()
        Dim alternativeSchedulingBL As AlternativeSchedulingBL = New AlternativeSchedulingBL()

#Region "Functions"

#Region "Integlligent Scheduling Functions"

#Region "get Appointments Arranged List"
        Public Function GetOilAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal developmentId As Integer)
            Try

                Return objSchedulingDAL.GetOilAppointmentsArrangedList(resultDataSet, check56Days, searchedText, objPageSortBo, patchId, schemeId, developmentId)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "Get Oil Appointment To Be Arranged List"
        Public Function GetOilAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)
            Try
                Return objSchedulingDAL.GetOilAppointmentToBeArrangedList(resultDataSet, check56Days, searchText, objPageSortBo, status, patchId, developmentId)
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

#Region "Get Arranged Appointments For Oil"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetArrangedAppointmentsForOil(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal startDate As Date, ByVal journalId As Integer, ByVal appointmentId As Integer)

            Dim dt = New DataTable()
            Dim alternativeSchedullingSlotHeaderLst = New List(Of OilAlternativeSchedullingSlotHeader)()
            alternativeSchedullingSlotHeaderLst = SessionManager.GetAlternativeSchedulingHeaderBoList()

            If (alternativeSchedullingSlotHeaderLst.Any()) Then
                dt.Columns.Add("FuelType")
                dt.Columns.Add("Duration")
                dt.Columns.Add("Trade")
                dt.Columns.Add("Status")
                dt.Columns.Add("HeatingTypeId")
                dt.Columns.Add("TradeId")

                For Each item As OilAlternativeSchedullingSlotHeader In alternativeSchedullingSlotHeaderLst
                    Dim row = dt.NewRow()
                    row("Duration") = item.AppointmentDuration
                    row("Status") = item.AppointmentStatus
                    row("Trade") = item.Trade
                    row("FuelType") = item.FuelType
                    row("HeatingTypeId") = item.HeatingTypeId
                    row("TradeId") = item.TradeId
                    dt.Rows.Add(row)
                Next
            Else
                Dim resultSet = New DataSet()
                objSchedulingDAL.GetHeatingAppointmentDuration(journalId, appointmentId, resultSet)

                For Each row As DataRow In resultSet.Tables(0).Rows

                    Dim alternativeSchedullingSlotHeader = New OilAlternativeSchedullingSlotHeader()
                    alternativeSchedullingSlotHeader.AppointmentDuration = row("Duration")
                    alternativeSchedullingSlotHeader.AppointmentStatus = row("Status")
                    alternativeSchedullingSlotHeader.Trade = row("Trade")
                    alternativeSchedullingSlotHeader.FuelType = row("FuelType")
                    alternativeSchedullingSlotHeader.HeatingTypeId = row("HeatingTypeId")
                    alternativeSchedullingSlotHeader.TradeId = row("TradeId")
                    alternativeSchedullingSlotHeaderLst.Add(alternativeSchedullingSlotHeader)

                Next
                dt = resultSet.Tables(0)
            End If

            SessionManager.SetAlternativeSchedulingHeaderBoList(alternativeSchedullingSlotHeaderLst)
            propertySchedulingBo.Duration = alternativeSchedullingSlotHeaderLst.FirstOrDefault().AppointmentDuration

            Dim tempAlternativeSchedullingSlot As AlternativeSchedullingSlot = New AlternativeSchedullingSlot()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.PropertyId = propertySchedulingBo.PropertyId
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FuelType = propertySchedulingBo.FuelType

            'save the start date 
            tempAlternativeSchedullingSlot.StartSelectedDate = startDate
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.aptStartTime = startDate

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            If (IsNothing(propertySchedulingBo.CertificateExpiryDate) = True) Then
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = False
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = True
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FullExpiryDate = propertySchedulingBo.CertificateExpiryDate
            End If
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DisplayCount = 5
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.getAppointmentDurationHours()

            '-------------
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.appendHourLabel(Convert.ToDouble(propertySchedulingBo.Duration))
            If propertySchedulingBo.PropertyStatus = ApplicationConstants.ArrangedStatus Then
                'in case of appointments with status "arranged" or others
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ArrangedStatus
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ToBeArrangedStatus
            End If

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs = CType(propertySchedulingBo.Duration, Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationDays = CType(propertySchedulingBo.Duration, Double) / GeneralHelper.getAppointmentDurationHours()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.dt = dt
            'tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.addNewDataRow()
            'changes(here)

            Dim filteredOperativesDt As DataTable = New DataTable()
            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = ApplyPatchFilterForAlternative(operativesDt, tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, propertySchedulingBo.PatchId)

            'this function 'll create the appointment slot based on leaves, appointments & due date

            alternativeSchedulingBL.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, appointmentsDt, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                                          GeneralHelper.getAppointmentCreationLimitForSingleRequest(), propertySchedulingBo)

            ' ''this function 'll sort the appointments by appointment date and distance
            alternativeSchedulingBL.OrderAlternativeAppointmentsByDistance(tempAlternativeSchedullingSlot)
            SessionManager.SetSelectedPropertySchedulingBo(propertySchedulingBo)
            Return tempAlternativeSchedullingSlot
        End Function
#End Region

#Region "Get To Be Arranged Appointments For Oil"
        ''' <summary>
        ''' This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetToBeArrangedAppointmentsForOil(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal startDate As Date)

            Dim dt As DataTable = New DataTable()

            Dim lstAlternativeSchedullingSlotHeader = New List(Of OilAlternativeSchedullingSlotHeader)()
            lstAlternativeSchedullingSlotHeader = SessionManager.GetAlternativeSchedulingHeaderBoList()

            If (lstAlternativeSchedullingSlotHeader.Any()) Then
                dt.Columns.Add("Fuel")
                dt.Columns.Add("Duration")
                dt.Columns.Add("Trade")
                dt.Columns.Add("Status")
                dt.Columns.Add("HeatingTypeId")
                dt.Columns.Add("TradeId")

                For Each item As OilAlternativeSchedullingSlotHeader In lstAlternativeSchedullingSlotHeader
                    Dim row = dt.NewRow()
                    row("Duration") = item.AppointmentDuration
                    row("Status") = item.AppointmentStatus
                    row("Trade") = item.Trade
                    row("Fuel") = item.FuelType
                    row("HeatingTypeId") = item.HeatingTypeId
                    row("TradeId") = item.TradeId
                    dt.Rows.Add(row)
                Next
            Else
                Dim resultSet = New DataSet()
                GetOilAppointmentsHeaderByProperty(propertySchedulingBo.PropertyId, resultSet)

                For Each row As DataRow In resultSet.Tables(0).Rows

                    Dim alternativeSchedullingSlotHeader = New OilAlternativeSchedullingSlotHeader()
                    alternativeSchedullingSlotHeader.AppointmentDuration = row("Duration")
                    alternativeSchedullingSlotHeader.AppointmentStatus = row("Status")
                    alternativeSchedullingSlotHeader.Trade = row("Trade")
                    alternativeSchedullingSlotHeader.FuelType = row("Fuel")
                    alternativeSchedullingSlotHeader.HeatingTypeId = row("HeatingTypeId")
                    alternativeSchedullingSlotHeader.TradeId = row("TradeId")
                    lstAlternativeSchedullingSlotHeader.Add(alternativeSchedullingSlotHeader)

                Next
                dt = resultSet.Tables(0)
            End If

            SessionManager.SetAlternativeSchedulingHeaderBoList(lstAlternativeSchedullingSlotHeader)
            propertySchedulingBo.Duration = lstAlternativeSchedullingSlotHeader.FirstOrDefault().AppointmentDuration

            Dim tempAlternativeSchedullingSlot As AlternativeSchedullingSlot = New AlternativeSchedullingSlot()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.PropertyId = propertySchedulingBo.PropertyId
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FuelType = propertySchedulingBo.FuelType

            'save the start date 
            tempAlternativeSchedullingSlot.StartSelectedDate = startDate
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.aptStartTime = startDate

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.IsPatchSelected = propertySchedulingBo.IsPatchSelected
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            If (IsNothing(propertySchedulingBo.CertificateExpiryDate) = True) Then
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = False
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.IsDateSelected = True
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.FullExpiryDate = propertySchedulingBo.CertificateExpiryDate
            End If
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DisplayCount = 5
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.getAppointmentDurationHours()

            '-------------
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentDuration = GeneralHelper.appendHourLabel(Convert.ToDouble(propertySchedulingBo.Duration))
            If propertySchedulingBo.PropertyStatus = ApplicationConstants.ArrangedStatus Then
                'in case of appointments with status "arranged" or others
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ArrangedStatus
            Else
                tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.AppointmentStatus = ApplicationConstants.ToBeArrangedStatus
            End If

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs = CType(propertySchedulingBo.Duration, Double) ' * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationDays = CType(propertySchedulingBo.Duration, Double) / GeneralHelper.getAppointmentDurationHours()

            tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.dt = dt

            'changes(here)

            Dim filteredOperativesDt As DataTable = New DataTable()
            'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it 'll return all the operatvies
            filteredOperativesDt = ApplyPatchFilterForAlternative(operativesDt, tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, propertySchedulingBo.PatchId)

            'this function 'll create the appointment slot based on leaves, appointments & due date

            alternativeSchedulingBL.CreateAlternativeAppointmentSlotsWithCoreAndExtendedHours(filteredOperativesDt, leavesDt, appointmentsDt, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentSlotsDtBO, startDate, _
                                                          tempAlternativeSchedullingSlot.alternativeAppointmentDtBO.DurationHrs, _
                                                          GeneralHelper.getAppointmentCreationLimitForSingleRequest(), propertySchedulingBo)

            ' ''this function 'll sort the appointments by appointment date and distance
            alternativeSchedulingBL.OrderAlternativeAppointmentsByDistance(tempAlternativeSchedullingSlot)
            SessionManager.SetSelectedPropertySchedulingBo(propertySchedulingBo)

            Return tempAlternativeSchedullingSlot
        End Function
#End Region

#Region "apply Patch Filter For Alternative"
        ''' <summary>
        ''' If patch check box is selected this function 'll sort out the operatives who have same selected property's patch 
        ''' otherwise if patch is unchecked it 'll return all the operatvies
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="alternativeAppointmentSlotsDtBO"></param>
        ''' <param name="patchId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function ApplyPatchFilterForAlternative(ByVal operativesDt As DataTable, ByRef alternativeAppointmentSlotsDtBO As AlternativeAppointmentSlotsDtBO, ByVal patchId As Integer)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'if patch is selected then filter the operatives which have the same patch as selected property's patch
            If alternativeAppointmentSlotsDtBO.IsPatchSelected = True Then
                '(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = String.Format("PatchId = {0} OR PatchId =  -1 ", patchId.ToString())
                filteredOperativesDt = operativesDv.ToTable()

                'get the ids of filtered operatives
                Dim employeeIds As String = String.Empty
                For Each dr As DataRow In filteredOperativesDt.Rows
                    employeeIds = employeeIds + dr.Item("EmployeeId").ToString() + ","
                Next
                If employeeIds.Length() > 1 Then
                    employeeIds = employeeIds.Substring(0, employeeIds.Length() - 1)

                    'select the appointments based on operatives which are filtered above
                    Dim tempAptDv As DataView = New DataView()
                    tempAptDv = alternativeAppointmentSlotsDtBO.dt.AsDataView()
                    tempAptDv.RowFilter = AppointmentSlotsDtBO.operativeIdColName + " IN (" + employeeIds + ")"
                    alternativeAppointmentSlotsDtBO.dt = tempAptDv.ToTable()
                Else
                    alternativeAppointmentSlotsDtBO.dt.Clear()
                End If
            Else
                filteredOperativesDt = operativesDv.ToTable()
            End If

            Return filteredOperativesDt
        End Function
#End Region

#Region "Get Oil Appointments Header By PropertyId"

        Private Sub GetOilAppointmentsHeaderByProperty(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            objSchedulingDAL.GetOilAppointmentsHeaderByProperty(propertyId, resultDataSet)
        End Sub

#End Region

#End Region

#End Region
    End Class
End Namespace