﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities
Imports System.Globalization

Namespace AS_BusinessLogic

    Public Class DefectSchedulingBL

        Dim appointmentDueDate As Date

#Region "Defect Managements - Integlligent Scheduling Functions"

#Region "get Temp Defect Appointments"
        ''' <summary>
        ''' This function 'll group the faults by trade and 'll fill up the list of tempFaultAppointmentBO
        ''' </summary>
        ''' <param name="selectedDefectsDt"></param>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="groupBy"></param>
        ''' <param name="customerBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getTempDefectsAppointments(ByVal selectedDefectsDt As DataTable, ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal groupBy As Boolean, ByVal customerBo As CommonAddressBO, ByVal selectedStartDate As Date) As List(Of TempDefectAppointmentBO)

            Dim tempDefectAptBo As TempDefectAppointmentBO = New TempDefectAppointmentBO()
            Dim tempDefectAptBoList As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)

            'these two variables 'll help to identify either the fault belongs to same appointment or next appointment
            Dim currentTradeId As Integer = 0
            Dim nextTradeId As Integer = 0
            Dim faultCount As Integer = 0

            'Dim isFaultGasSafe As Boolean = False
            'Dim isFaultOftec As Boolean = False

            'this flag 'll tell either this fault should be added in new list of faults
            Dim addFaultInList As Boolean = True
            'this flag 'll tell either this appointment should be added in new list of appointments
            Dim addAppointmentInList As Boolean = True
            Dim faultDuration As Double = 0


            'Loop through faults           
            For Each selectedDefectDr As DataRow In selectedDefectsDt.Rows
                'save fault count. This 'll help us to check either we should access next row or not
                faultCount = faultCount + 1
                currentTradeId = Convert.ToInt32(selectedDefectDr.Item("TradeId"))
                'currentFaultTradeId = tempFaultDr.Item("FaultTradeId")

                'Check that either fault is gas safe or not this 'll help us to determine the operatives.
                'In case of gas safe only operatives with gas safe status 'll be displayed
                'If CType(selectedDefectDr.Item("isGasSafe"), Boolean) = True Then
                '    isFaultGasSafe = True
                'End If
                'Check that either fault is oftec or not. This will help us to determine the operatives.
                'In case of oftec , only operatives with oftec status will be displayed
                'If CType(selectedDefectDr.Item("isOftec"), Boolean) = True Then
                '    isFaultOftec = True
                'End If

                'sum up the fault duration of all faults in one group
                faultDuration += If(IsDBNull(selectedDefectDr.Item("Duration")) = True, 0, selectedDefectDr.Item("Duration"))

                If (faultCount) <= selectedDefectsDt.Rows.Count() - 1 Then
                    nextTradeId = selectedDefectsDt.Rows(faultCount).Item("TradeId")
                Else
                    nextTradeId = currentTradeId
                End If

                'Below mentioned IF BLOCK shows decides either to show operative or not based on trade id                            
                If (groupBy = True And nextTradeId = currentTradeId) And (faultCount = selectedDefectsDt.Rows.Count()) Then
                    'If its last row then add fault of current row in new fault list
                    'and also add the appointment of current row in new appointment list
                    addFaultInList = True
                    addAppointmentInList = True
                ElseIf (groupBy = True And nextTradeId = currentTradeId) Then
                    addFaultInList = True
                    addAppointmentInList = False
                ElseIf (groupBy = True And nextTradeId <> currentTradeId) Then
                    addFaultInList = True
                    addAppointmentInList = True
                ElseIf groupBy = False Then
                    addFaultInList = True
                    addAppointmentInList = True
                End If

                If addFaultInList = True Then

                    tempDefectAptBo.tempDefectDtBo.appliance = selectedDefectDr.Item("Appliance").ToString()
                    tempDefectAptBo.tempDefectDtBo.trade = selectedDefectDr.Item("DefectTrade").ToString()
                    tempDefectAptBo.tempDefectDtBo.twoPersons = GeneralHelper.getYesNoStringFromValue(selectedDefectDr.Item("isTwoPersonsJob").ToString())
                    tempDefectAptBo.tempDefectDtBo.disconnected = GeneralHelper.getYesNoStringFromValue(selectedDefectDr.Item("isApplianceDisconnected").ToString())
                    tempDefectAptBo.tempDefectDtBo.durationString = If(IsDBNull(selectedDefectDr.Item("Duration")) = True, "0 hour", GeneralHelper.getSingularOrPluralPostFixForNumericValues(selectedDefectDr.Item("Duration"), "hour", "hours"))
                    tempDefectAptBo.tempDefectDtBo.defectDurationHrs = If(IsDBNull(selectedDefectDr.Item("Duration")) = True, 1, selectedDefectDr.Item("Duration"))

                    tempDefectAptBo.tempDefectDtBo.partsDueString = If(IsDBNull(selectedDefectDr.Item("DefectPartsDue")), "N/A", selectedDefectDr.Item("DefectPartsDue"))
                    tempDefectAptBo.tempDefectDtBo.tradeId = selectedDefectDr.Item("TradeID").ToString()
                    tempDefectAptBo.tempDefectDtBo.defectId = selectedDefectDr.Item("DefectId").ToString()
                    tempDefectAptBo.tempDefectDtBo.ReqType = selectedDefectDr.Item("AppointmentType").ToString()

                    tempDefectAptBo.tempDefectDtBo.addNewDataRow()
                End If

                If addAppointmentInList = True Then
                    tempDefectAptBo.TotalDuration = faultDuration
                    If Not IsDBNull(selectedDefectDr.Item("EMPLOYEEID")) Then
                        tempDefectAptBo.OperativeId = selectedDefectDr.Item("EMPLOYEEID")
                    Else
                        tempDefectAptBo.OperativeId = 0
                    End If
                    If Not IsDBNull(selectedDefectDr.Item("EMPLOYEEID")) Then
                        tempDefectAptBo.OperativeName = selectedDefectDr.Item("EMPLOYEENAME")
                    Else
                        tempDefectAptBo.OperativeName = "N/A"
                    End If

                    'selectedDefectDr.Item("EmployeeId")
                    'tempDefectAptBo.IsFaultOftec = isFaultOftec
                    tempDefectAptBo.TradeId = currentTradeId
                    tempDefectAptBo.IsPatchSelected = True
                    tempDefectAptBo.IsOperativeCheckSelected = True
                    tempDefectAptBo.DisplayCount = 5
                    'tempDefectAptBo.CompleteDueDate = selectedDefectDr.Item("CompleteDueDate").ToString()
                    faultDuration = 0
                    'isFaultGasSafe = False
                    'isFaultOftec = False
                    Dim filteredOperativesDt As DataTable = operativesDt.Copy()

                    If tempDefectAptBo.IsOperativeCheckSelected AndAlso tempDefectAptBo.OperativeId > 0 Then
                        filteredOperativesDt = applylOperativeFilter(filteredOperativesDt, tempDefectAptBo)
                    End If

                    'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
                    'otherwise if patch is unchecked it 'll return all the operatives
                    filteredOperativesDt = Me.applyPatchFilter(filteredOperativesDt, tempDefectAptBo, customerBo.PatchId)

                    'Filter for operatives that have same trade as of current fault (operative_filter = fault_filter)
                    'Provide the already filtered operatives for patchid to filter more for tradeid.
                    'Note: Return result into same data table(variable) there is no need to use a data table(variable).
                    filteredOperativesDt = applyTradeFilter(filteredOperativesDt, currentTradeId)

                    'this function will calculate/decide the due date of faults (if appointment has multiple faults) based on priority of faults
                    'Me.calculateDueDate(tempFaultAptBo)

                    'this function will create the appointment slot based on leaves, appointments & due date
                    Me.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, tempDefectAptBo, customerBo, selectedStartDate)

                    'this function will sort the appointments by distance calculated between operative's last appointment and current calculated appointment 
                    orderAppointmentsByDistance(tempDefectAptBo)

                    'once appointment is added in list then create a new item 
                    'for new list of faults and appointment against them
                    tempDefectAptBoList.Add(tempDefectAptBo)
                    tempDefectAptBo = New TempDefectAppointmentBO()
                End If
            Next

            Return tempDefectAptBoList
        End Function
#End Region

#Region "add More Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub addMoreAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal customerBo As CommonAddressBO, ByRef tempDefectAptBo As TempDefectAppointmentBO, ByVal selectedDate As Date, Optional ByVal startAgain As Boolean = False)

            Dim filteredOperativesDt As DataTable = operativesDt.Copy()


            'If patch check box is selected this function will sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it will return all the operatives
            filteredOperativesDt = Me.applyPatchFilter(filteredOperativesDt, tempDefectAptBo, customerBo.PatchId)

            'Filter for operatives that have same trade as of current fault (operative_filter = fault_filter)
            'Provide the already filtered operatives for patchid to filter more for tradeid.
            'Note: Return result into same data table(variable) there is no need to use a data table(variable).
            filteredOperativesDt = applyTradeFilter(filteredOperativesDt, tempDefectAptBo.TradeId)

            'if Operative check box is selected then function will filter the Datatable w.r.t operativeId
            If tempDefectAptBo.IsOperativeCheckSelected AndAlso tempDefectAptBo.OperativeId > 0 Then
                filteredOperativesDt = applylOperativeFilter(filteredOperativesDt, tempDefectAptBo)
            End If
            If startAgain = False Then
                'fetch the previous operative id so the operatives name should appear in order
                Dim previousOperativeId As Integer = 0
                Dim appointments = tempDefectAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
                If appResult.Count() > 0 Then
                    previousOperativeId = appResult.LastOrDefault.Item(TempAppointmentDtBO.operativeIdColName)
                End If

                'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
                Me.createTempAppointmentSlots(filteredOperativesDt, appointmentsDt, leavesDt, tempDefectAptBo, customerBo, selectedDate, previousOperativeId)
            Else
                'This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                ' This 'll restart appointments slot from the start date
                tempDefectAptBo.tempAppointmentDtBo.dt.Clear()
                Me.createTempAppointmentSlots(filteredOperativesDt, appointmentsDt, leavesDt, tempDefectAptBo, customerBo, selectedDate)

            End If
        End Sub
#End Region

#Region "apply Patch Filter"
        ''' <summary>
        ''' If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
        ''' otherwise if patch is unchecked it 'll return all the operatvies
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="tempDefectAptBo"></param>
        ''' <param name="patchId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function applyPatchFilter(ByVal operativesDt As DataTable, ByRef tempDefectAptBo As TempDefectAppointmentBO, ByVal patchId As Integer)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'if patch is selected then filter the operatives which have the same patch as customer's property patch
            If tempDefectAptBo.IsPatchSelected = True Then
                '(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = String.Format("PatchId = {0} OR PatchId =  -1 ", patchId.ToString())
                filteredOperativesDt = operativesDv.ToTable()

                'get the ids of filtered operatives
                Dim employeeIds As String = String.Empty
                For Each dr As DataRow In filteredOperativesDt.Rows
                    employeeIds = employeeIds + dr.Item("EmployeeId").ToString() + ","
                Next
                If employeeIds.Length() > 1 Then
                    employeeIds = employeeIds.Substring(0, employeeIds.Length() - 1)

                    'select the appointments based on operatives which are filtered above
                    Dim tempAptDv As DataView = New DataView()
                    tempAptDv = tempDefectAptBo.tempAppointmentDtBo.dt.AsDataView()
                    tempAptDv.RowFilter = TempAppointmentDtBO.operativeIdColName + " IN (" + employeeIds + ")"
                    tempDefectAptBo.tempAppointmentDtBo.dt = tempAptDv.ToTable()
                Else
                    tempDefectAptBo.tempAppointmentDtBo.dt.Clear()
                End If
            Else
                filteredOperativesDt = operativesDv.ToTable()
            End If

            Return filteredOperativesDt
        End Function
#End Region

#Region "apply Trade Filter"

        ''' <summary>
        ''' This function is used to filter the available operate for a particular trade, that filter is applied based of tradeid provided.
        ''' In case the provided tradeid is not in operatives list it will return all records.
        ''' </summary>
        ''' <param name="operativesDt">Provide the data table to fiilter for trade, it must has a TradeID column</param>
        ''' <param name="tradeId">Provide Fault TradeId to filter Operative for that trade only</param>
        ''' <returns>The fuction returns filter operatives by trade as a datatable.</returns>
        ''' <remarks>Although this is not a generic function but with a little modification it can be changed to generic function</remarks>
        Private Function applyTradeFilter(ByVal operativesDt As DataTable, ByVal tradeId As Integer) As DataTable
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'Filter the operatives which have the same trade as temp faults trade.

            operativesDv.RowFilter = "TradeID =  " + tradeId.ToString()
            filteredOperativesDt = operativesDv.ToTable()

            Return filteredOperativesDt
        End Function

#End Region

#Region "Apply Operative Filter"

        Public Function applylOperativeFilter(operativesDt As DataTable, tempDefectAptBo As TempDefectAppointmentBO) As DataTable
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'Filter the operatives which have the same trade as temp faults trade.
            If tempDefectAptBo.IsOperativeCheckSelected Then
                operativesDv.RowFilter = "EmployeeId =  " + tempDefectAptBo.OperativeId.ToString()
            End If

            filteredOperativesDt = operativesDv.ToTable()

            Return filteredOperativesDt
        End Function

#End Region

#Region "create Temp Appointment Slots"

        ''' <summary>
        ''' 'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tempDefectAptBo"></param>
        ''' <param name="customerBo"></param>
        ''' <param name="previousOperativeId"></param>
        ''' <remarks></remarks>
        Private Sub createTempAppointmentSlots(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempDefectAptBo As TempDefectAppointmentBO, ByVal customerBo As CommonAddressBO, ByVal startDate As Date, Optional ByVal previousOperativeId As Integer = 0)
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim operativePostCode As String = String.Empty
            Dim patchName As String = String.Empty
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(startDate)
            Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
            Dim displayedTimeSlotCount As Integer = 1
            Dim appointmentStartDateTime As Date
            Dim appointmentEndDate As Date
            Dim aptEndTime As New Date
            Dim isGasOftecOperativeCount As Integer = 0
            Dim dueDateCheckCount As Hashtable = New Hashtable()
            Dim timeSlotsToDisplay As Integer = GeneralHelper.getTimeSlotToDisplay
            Dim isMultipleDaysAppointmentAllowed As Boolean = True ' GeneralHelper.isMultipleDaysAppointmentAllowed

            Dim operativeEligibility As New Dictionary(Of Integer, Boolean)
            Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
            Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + IIf(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString())).ToString("HH:mm")
            Dim skipOperative As Boolean

            'Just to initlize start date and end date.
            appointmentStartDateTime = startDate
            appointmentEndDate = appointmentStartDateTime

            'Calculate Time slots given in count
            ' Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            While (displayedTimeSlotCount <= timeSlotsToDisplay AndAlso (tempDefectAptBo.TotalDuration <= ApplicationConstants.MaximumDayWorkingHours OrElse isMultipleDaysAppointmentAllowed))
                Dim uniqueCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop

                If operativesDt.Rows.Count = 0 Then
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    skipOperative = False
                    uniqueCounter += 1
                    operativeId = Convert.ToString(operativeDr.Item("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr.Item("FullName"))
                    operativePostCode = Convert.ToString(operativeDr("PostCode"))
                    patchName = Convert.ToString(operativeDr.Item("PatchName"))
                    Dim operativeWorkingHourDs As DataSet = New DataSet()
                    Dim objFaultAppointmentDal As SchedulingDAL = New SchedulingDAL
                    objFaultAppointmentDal.getOperativeWorkingHours(operativeWorkingHourDs, operativeId, ofcCoreStartTime, ofcCoreEndTime)
                    SessionManager.setOperativeworkingHourDs(operativeWorkingHourDs)

                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 
                    If (previousOperativeId <> 0) Then

                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For
                        End If

                    End If

                    appointmentDueDate = CType(Date.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59", Date)

                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempDefectAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots _
                                       Where app(TempAppointmentDtBO.operativeIdColName) = operativeId _
                                       Select app _
                                       Order By CType(app(TempAppointmentDtBO.appointmentEndDateColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = CType(slotsResult.Last.Item(TempAppointmentDtBO.appointmentEndDateColName), Date)
                    Else
                        'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        appointmentStartDateTime = CType(startDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00", Date)
                    End If

                    Me.setAppointmentStartEndTime(appointmentStartDateTime, appointmentEndDate, tempDefectAptBo.TotalDuration, skipOperative _
                                              , leavesDt, appointmentsDt, operativeId, uniqueCounter, dueDateCheckCount, tempDefectAptBo, isMultipleDaysAppointmentAllowed)

                    If (skipOperative = True) Then
                        'Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        'If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        'this condition 'll check either due date check has been executed against all the operatives
                        If (dueDateCheckCount.Count() = operativesDt.Rows.Count) Then
                            Exit While
                        Else
                            Continue For
                        End If
                    End If

                    'this condition means that not single operative in the list is eligilble to fix this fault
                    If isGasOftecOperativeCount = operativesDt.Rows.Count Then
                        Exit While
                    End If
                    Dim operativeEligibilityCount As Integer = 0
                    Dim operativeEligibilityResult As Boolean
                    If operativeEligibility.ContainsKey(operativeId) Then
                        operativeEligibilityResult = operativeEligibility.Item(operativeId)
                        If operativeEligibilityResult = False Then
                            '  operativeCounter = operativeCounter + 1
                            Continue For
                        End If
                    Else

                        'check if fault is gas safe then operative should be gas safe . if this condition is true then operative is not eligible to fix this fault
                        If tempDefectAptBo.IsFaultGasSafe = True And CType(operativeDr.Item("isGasSafe"), Boolean) = False Then
                            isGasOftecOperativeCount = isGasOftecOperativeCount + 1
                            operativeEligibilityCount = operativeEligibilityCount + 1
                            ' Continue For
                        End If

                        'check if fault is oftec then operative should be oftec. if this condition is true then operative is not eligible to fix this fault
                        If tempDefectAptBo.IsFaultOftec = True And CType(operativeDr.Item("isOftec"), Boolean) = False Then
                            isGasOftecOperativeCount = isGasOftecOperativeCount + 1
                            operativeEligibilityCount = operativeEligibilityCount + 1
                            ' Continue For
                        End If
                        If operativeEligibilityCount > 0 Then
                            operativeEligibility.Add(operativeId, False)
                            Continue For
                        Else
                            operativeEligibility.Add(operativeId, True)
                        End If
                    End If

                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    Dim distanceFrom As Double = getPropertyDistance(appointmentsDt, operativeId, appointmentStartDateTime, customerBo, operativePostCode)
                    'Dim distanceTo As Double = getPropertyDistance(appointmentsDt, operativeId, appointmentEndDate, customerBo, operativePostCode, isToNext:=True)

                    'update the lastAdded column in tempfaultBO.dt
                    updateLastAdded(tempDefectAptBo)
                    'add this time slot in appointment list
                    tempDefectAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom)

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > timeSlotsToDisplay Then
                        Exit While
                    End If
                Next
            End While 'Calculate Time slots loop ends here
        End Sub

#End Region

#Region "get starting date"
        ''' <summary>
        ''' This funciton 'll be used to reset the starting date.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getStartingDate()
            Return Date.Now.AddHours(1)
        End Function
#End Region

#Region "set appointment start end time"

        Private Sub setAppointmentStartEndTime(ByRef appointmentStartDate As Date, ByRef appointmentEndDate As Date _
                                               , ByVal faultDuration As Double, ByRef skipOperative As Boolean, ByVal leavesDt As DataTable _
                                               , ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByRef uniqueCounter As Integer, _
                                               ByRef dueDateCheckCount As Hashtable, ByVal tempFaultAptBo As TempDefectAppointmentBO, Optional ByVal isMultipleDaysAppointmentAllowed As Boolean = False)

            Dim isAppointmentCreated As Boolean = False
            Dim isExtendedApptVerified As Boolean = False
            'Get Operative core and extended working hours from session
            Dim operativeWorkingHourDs As DataSet = SessionManager.getOperativeworkingHourDs()
            Dim operativeWorkingHourDt As DataTable = operativeWorkingHourDs.Tables(0)

            Dim currentRunningDate As Date = appointmentStartDate
            Dim loggingTime As Integer = 0

            'Do Until: appointment is created or the operative is skipped
            Do
                'Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future)                
                If isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, skipOperative) Then
                    skipOperative = True
                Else

                    'Get operatives running day available time in ascending order of StartTime column
                    Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate)

                    ' Check if there is a slot 
                    If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
                        Dim nextBlockStartTime As String = operativeWorkingHourForCurrentDayDt.Rows(0)(ApplicationConstants.StartTimeCol)
                        Dim nextBlockStartDateTime As Date = CType(appointmentStartDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                        If nextBlockStartDateTime > appointmentStartDate Then
                            appointmentStartDate = nextBlockStartDateTime
                        End If

                        'At the begining of slot creation set appointment end date time to same as start date time and then calculate
                        ' the end date time base on duration and operative working hours.
                        appointmentEndDate = appointmentStartDate

                        'Merge adjacent hours, treat each row as block
                        'operativeWorkingHourDt = Me.mergeAdjacentHours(operativeWorkingHourDt)

                        'This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
                        Dim dayCounter As Integer = 0
                        'Repeat For End Date Time Until the full remainingDuration is consumed to zero
                        Dim remainDuration As Double = faultDuration
                        While remainDuration > 0 AndAlso Not isAppointmentCreated
                            dayCounter += 1

                            ' Iterate through each working core/on call/extended to use it for appointment duration.
                            For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                                nextBlockStartTime = operativeWorkingHoursBlock(ApplicationConstants.StartTimeCol)
                                nextBlockStartDateTime = CType(appointmentEndDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                                loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)
                                'Set appointmentEndDate to start time of next block.
                                'At the begining of each block set appointment end date time to the start of next block's start date in case
                                ' in case it is less than next block start time.
                                If nextBlockStartDateTime > appointmentEndDate Then
                                    appointmentEndDate = nextBlockStartDateTime
                                End If

                                Dim blockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeCol)
                                Dim blockEndDateTime As Date = CType(appointmentEndDate.ToLongDateString() + " " + blockEndTime, Date)

                                'calculate the remaining time/lapse of the block
                                Dim lapse As Double = blockEndDateTime.TimeOfDay.Subtract(appointmentEndDate.TimeOfDay).TotalHours
                                'subtract the total lapse/remaining time of the block from the remaining trade duration
                                Dim tDuration As Double = remainDuration - lapse

                                'if duration is less than zero than it means now we have to calculate the end hour of appointment otherwise we have to go to one day ahead to calculate the end time
                                If (tDuration <= 0) Then
                                    'Dim absoluteDuration As Double = Math.Abs(tDuration)
                                    appointmentEndDate = appointmentEndDate.AddHours(remainDuration)
                                    'In this case appointment is created successfully, exit the blocks loop and check for leaves and existing appointments
                                    isAppointmentCreated = True
                                ElseIf tDuration > 0 Then
                                    'we have to go to next block to calculate the end date/time, so setting appointment End Date = BlockEndDateTiem
                                    appointmentEndDate = blockEndDateTime
                                End If
                                remainDuration -= lapse
                                If isAppointmentCreated Then
                                    Exit For
                                End If
                            Next

                            'In case appointment is created or operative is skipped by reaching due date: exit while and apply next iteration or add operative to skip list.
                            If isAppointmentCreated OrElse isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, skipOperative) Then
                                Exit While
                            End If

                            'In case appointment is not created by above process change the day and set appointment end datetime at start of next day.
                            changeDay(appointmentEndDate, "00:00")

                            'In case appointment is not create and all slots (Core Working Hours, On Call hours and out of office hours) are 
                            'Check for case: is it is allowed to create multiple days appointment: If now exit while and start over for next day
                            ' in case due date is reached operative will be skipped by skipOperative Process.
                            If (Not isMultipleDaysAppointmentAllowed) Then
                                appointmentStartDate = appointmentEndDate
                                Exit While
                            End If

                            'Only in case remainDuration is more than zero then we need to appointment working hours for next day, other wise the loop will simply exit.                            
                            operativeWorkingHourForCurrentDayDt = If(remainDuration > 0, _
                                                                     getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentEndDate), Nothing)
                        End While

                    Else
                        'Change day 
                        changeDay(appointmentStartDate, "00:00")
                    End If
                End If

                If isAppointmentCreated AndAlso Not skipOperative Then
                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate)
                    'if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    If leaveResult.Count() > 0 Then
                        appointmentStartDate = CType(leaveResult.Last().Item("EndDate"), Date)
                        appointmentStartDate = CType(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Item("EndTime").ToString(), Date)
                        isAppointmentCreated = False
                    Else
                        'if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this will be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate)
                        If Not IsNothing(appResult) And appResult.Count() > 0 Then
                            'appointmentStartDate = CType(appResult.Last().Item("AppointmentDate"), Date)
                            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(appResult.Last.Item("EndTimeInSec").ToString())
                            isAppointmentCreated = False

                        End If
                    End If
                End If

            Loop Until (isAppointmentCreated OrElse skipOperative)
        End Sub

#End Region

#Region "Get operatives available time slots for given date/day in ascending order of StartTime column"

        Private Function getOperativeAvailableSlotsByDateTime(ByVal operativeWorkingHourDt As DataTable, ByVal dateToFilter As Date) As DataTable
            'Check one or more working hours slots from the operative data table.
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                           Where ((optHour(ApplicationConstants.WeekDayCol).ToString() = dateToFilter.DayOfWeek.ToString) _
                                                And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                                And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                                And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                ) _
                                           Select optHour _
                                           Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > dateToFilter.TimeOfDay))
            ''And If(optHour(ApplicationConstants.EndTimeCol) = "", False, CType(optHour(ApplicationConstants.EndTimeCol), TimeSpan) >= dateToFilter.TimeOfDay)

            ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            Dim operativeWorkingHourForCurrentDayDt = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable, operativeWorkingHourDt.Clone)
            Return operativeWorkingHourForCurrentDayDt
        End Function

#End Region

#Region "Change day"

        ''' <summary>
        ''' This function will change day
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub changeDay(ByRef runningDate As Date, ByVal nextDayStartTime As String)
            runningDate = runningDate.AddDays(1)
            runningDate = CType(runningDate.ToLongDateString() + " " + nextDayStartTime, Date)
        End Sub

#End Region

#Region "is Operative Available - Check operative availability from due date and workingHours"

        ''' <summary>
        ''' 1- Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
        ''' 2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
        ''' </summary>
        ''' <param name="operativeWorkingHourDt"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="dueDateCheckCount"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="uniqueCounter"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isOpertiveAvailable(ByVal operativeWorkingHourDt As DataTable, ByVal dateToCheck As Date, ByRef dueDateCheckCount As Hashtable, ByVal operativeId As Integer, ByVal uniqueCounter As Integer, ByRef skipOperative As Boolean) As Boolean

            '1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            Dim operativeAvailable As Boolean = checkDateIsGreaterThanDueDateAndAddToSkipList(uniqueCounter, dueDateCheckCount, dateToCheck, operativeId) _
                                    OrElse Not checkOperativeAvailability(dateToCheck, operativeWorkingHourDt)

            Return operativeAvailable
        End Function

#End Region

#Region "if Date Is Greater Than Due Date"

        ''' <summary>
        ''' This function 'll check if running date is greater than due date of fault. If its true then add the entry in hash table and this (current) operative 'll not process any more for appointments
        ''' The count of total entries in hash table 'll help us to check that all operatives has been processed against the fault due date
        ''' </summary>
        ''' <param name="uniqueCounter"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="operativeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkDateIsGreaterThanDueDateAndAddToSkipList(ByRef uniqueCounter As Integer, ByRef dueDateCheckCount As Hashtable, ByVal dateToCheck As Date, ByVal operativeId As Integer) As Boolean
            Dim operativeSkipped As Boolean = False

            'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            If CType(dateToCheck.ToShortDateString(), Date) > appointmentDueDate Then
                operativeSkipped = True
                'this variable 'll check either due date check has been executed against all the operatives
                If Not (dueDateCheckCount.ContainsValue(operativeId)) Then
                    If dueDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                    Else
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                    End If
                End If
            End If
            Return operativeSkipped
        End Function

#End Region

#Region "Check operative availability"
        ''' <summary>
        ''' This function checks whether operative is fully absent in core and extended hours. If yes then this 
        ''' operative will be skipped.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="operativeHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkOperativeAvailability(ByVal runningDate As DateTime, ByVal operativeHoursDt As DataTable)

            Dim isAvailable As Boolean = True
            Dim workingHours = operativeHoursDt.AsEnumerable

            Dim coreWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) = "NA" _
                                            And app(ApplicationConstants.EndDateCol) = "NA" _
                                            And app(ApplicationConstants.StartTimeCol) <> "" _
                                            Select app)

            Dim extendedWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) <> "NA" _
                                            Select app)

            If (extendedWorkingHoursResult.Count > 0) Then
                extendedWorkingHoursResult = (From app In extendedWorkingHoursResult.CopyToDataTable().AsEnumerable() _
                                           Where Convert.ToDateTime(app(ApplicationConstants.StartDateCol)) >= runningDate _
                                           Select app)
            End If

            If coreWorkingHoursResult.Count = 0 And extendedWorkingHoursResult.Count = 0 Then
                isAvailable = False
            End If

            Return isAvailable

        End Function

#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="leavesDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appoitmentEndDateTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appoitmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(appointmentStartDateTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(appoitmentEndDateTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
             Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime "></param>
        ''' <param name="appointmentEndDateTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Private Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                            Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region

#Region "get Operative Distance From Property"
        ''' <summary>
        ''' This appointment 'll calculate the distance between operatives last appointment slot and running appointment slot (running means the slot that is being calculated at run time)
        ''' The rule would work like this
        ''' The distance will be calculated via the selected property and the last appointment (in order of time) prior to the available slot on the day 
        ''' Examples:
        ''' Case 1
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 11:00 - 12:00
        ''' Lets assume that Carl had 1 appointment from 9:00 - 10:00 at Property 1. Similarly he has another appointment from 10:00 - 11:00 at Property 2. The distance ail be calculated between selected property and Property2. The reason we chose Property 2 is because Carl will be here prior to his free slot 11:00 - 12:00
        ''' Case2
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 9:00 - 11:00
        ''' Lets assume that Carl had no appointments on this day. In case there is no prior booked appointment before the available appointment slot, we shall display 0 in the distance column
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentDate"></param>
        ''' <remarks></remarks>
        Private Function getPropertyDistance(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentDate As Date, ByVal customerBo As CommonAddressBO, ByVal operativePostCode As String, Optional ByVal isToNext As Boolean = False) As Double
            'Distance
            Dim distance As Double = 0
            Dim iscalculated As Boolean = False
            Dim isNoGoogleError As Boolean = False


            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startDate As New DateTime(1970, 1, 1)
            Dim runningAptTimeInSec As Integer
            ' we are creating this variable because we want to compare the dates from db and from calcuated date with time as 00:00:00
            ' in order to do the right comparison
            Dim runningDateAlias As Date = New Date()

            runningAptTimeInSec = (appointmentDate - startDate).TotalSeconds
            'creating the alias for comparison with 00:00:00 time
            runningDateAlias = CType(appointmentDate.ToLongDateString() + " " + "00:00:00", Date)

            Dim fromPropertyAddress As String
            Dim toPropertyAddress As String

            Dim appointments = appointmentsDt.AsEnumerable()
            Dim appResult As OrderedEnumerableRowCollection(Of DataRow)

            If Not isToNext Then
                appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("EndTimeInSec") <= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("EndTimeInSec"), Integer) Descending)
            Else
                appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("StartTimeInSec") >= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("StartTimeInSec"), Integer) Ascending)
            End If

            'if operative has any appointment prior to the running appointment slot then 
            'find the distance between customer property address and property address of operatives last arranged appointment on the same day
            If appResult.Count() > 0 Then
                'get the property address of running time slot

                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = String.Join("+", New String() {appResult.First().Item("Address"),
                                                                     appResult.First().Item("TownCity"),
                                                                     appResult.First().Item("County"),
                                                                     appResult.First().Item("PostCode")})

                fromPropertyAddress = fromPropertyAddress.Replace(" ", "+")


                toPropertyAddress = String.Join("+", New String() {customerBo.Street,
                                                                   customerBo.City,
                                                                   customerBo.Area,
                                                                   customerBo.PostCode})
                toPropertyAddress = toPropertyAddress.Replace(" ", "+")


                ''get the distance from session if this calcuation was done previously
                'Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                ' propertyAddressDictionary = SessionManager.getPropertyDistance()


                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                If ApplicationConstants.propertyDistance.Columns.Count = 0 Then

                    propertyDistance.createDataTable(ApplicationConstants.propertyDistance)

                Else
                    'Filter the data according to fromPropertyAddress and toPropertyAddress
                    Dim propertyResult = (From ps In ApplicationConstants.propertyDistance Where ps.Item(PropertyDistanceBO.fromAddressColName).ToString() = fromPropertyAddress And ps.Item(PropertyDistanceBO.toAddressColName).ToString() = toPropertyAddress Select ps)

                    If propertyResult.Count > 0 Then
                        distance = propertyResult.First.Item(PropertyDistanceBO.distanceStringColName)
                        iscalculated = True
                    End If

                End If
                If iscalculated = False Then
                    'we didnt' find the property distance from datatable then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isNoGoogleError)
                    If isNoGoogleError Then
                        'Save the property's distance in share datatable so that we can save map api call in future calculations
                        propertyDistance.fromAddress = fromPropertyAddress
                        propertyDistance.toAddress = toPropertyAddress
                        propertyDistance.distanceString = distance
                        'Add a new row in data table
                        propertyDistance.addNewDataRow(ApplicationConstants.propertyDistance)
                    End If
                End If



            Else

                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = operativePostCode.Replace(" ", "+")

                If customerBo.PostCode.Length > 0 Then
                    toPropertyAddress = customerBo.PostCode.Replace(" ", "+")

                Else

                    toPropertyAddress = String.Join("+", New String() {customerBo.Street,
                                                                     customerBo.City,
                                                                     customerBo.Area,
                                                                     customerBo.PostCode})
                    toPropertyAddress = toPropertyAddress.Replace(" ", "+")
                End If


                ''get the distance from session if this calcuation was done previously
                'Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                ' propertyAddressDictionary = SessionManager.getPropertyDistance()


                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                If ApplicationConstants.propertyDistance.Columns.Count = 0 Then

                    propertyDistance.createDataTable(ApplicationConstants.propertyDistance)

                Else
                    'Filter the data according to fromPropertyAddress and toPropertyAddress
                    Dim propertyResult = (From ps In ApplicationConstants.propertyDistance Where ps.Item(PropertyDistanceBO.fromAddressColName).ToString() = fromPropertyAddress And ps.Item(PropertyDistanceBO.toAddressColName).ToString() = toPropertyAddress Select ps)

                    If propertyResult.Count > 0 Then
                        distance = propertyResult.First.Item(PropertyDistanceBO.distanceStringColName)
                        iscalculated = True
                    End If

                End If
                If iscalculated = False Then
                    'we didnt' find the property distance from datatable then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isNoGoogleError)
                    If isNoGoogleError Then
                        'Save the property's distance in share datatable so that we can save map api call in future calculations
                        propertyDistance.fromAddress = fromPropertyAddress
                        propertyDistance.toAddress = toPropertyAddress
                        propertyDistance.distanceString = distance
                        'Add a new row in data table
                        propertyDistance.addNewDataRow(ApplicationConstants.propertyDistance)
                    End If
                End If

                End If

                Return distance
        End Function
#End Region

#Region "update Last added column in tempFaultAptBo datatable"
        ''' <summary>
        ''' 'update Last added column in tempFaultAptBo datatable 
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Private Sub updateLastAdded(ByRef tempFaultAptBo As TempDefectAppointmentBO)
            Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable
            Dim appResult = (From app In appointments _
                           Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
            If appResult.Count() > 0 Then
                appResult.First.Item(TempAppointmentDtBO.lastAddedColName) = False

            End If

        End Sub

#End Region

#Region "Order Temp Appointments By Distance"
        ''' <summary>
        ''' 'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment 
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Public Sub orderAppointmentsByDistance(ByRef tempFaultAptBo As TempDefectAppointmentBO)
            Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable
            'Ordert The appointment, First By Distance then By Date and Then By Start Time.
            Dim appResult = (From app In appointments _
                             Select app _
                             Order By CType(app(TempAppointmentDtBO.distanceFromColName), Double) + CType(app(TempAppointmentDtBO.distanceToColName), Double) Ascending _
                             , CType(app(TempAppointmentDtBO.appointmentStartDateColName), Date) Ascending _
                             , app(TempAppointmentDtBO.startDateColName) Ascending)
            If appResult.Count() > 0 Then
                tempFaultAptBo.tempAppointmentDtBo.dt = appResult.CopyToDataTable()
            End If

        End Sub

#End Region




#End Region

    End Class

End Namespace
