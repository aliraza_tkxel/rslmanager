﻿Imports System
Imports AS_BusinessObject
Imports System.Data.SqlClient
Imports AS_DataAccess
Imports System.Web
Imports System.Web.UI.WebControls
Imports AS_Utilities


Namespace AS_BusinessLogic

    Public Class UsersBL
#Region "Attributes"
        Dim objUserDAL As UsersDAL = New UsersDAL()
#End Region

#Region "Functions"

#Region "get Users Details"
        Public Sub getUsers(ByRef resultDataSet As DataSet)
            Try

                objUserDAL.getUsers(resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

        Public Sub getUserTypes(ByVal index As Int32, ByVal userTypeList As List(Of UserTypeBO))
            objUserDAL.getUserTypes(index, userTypeList)
        End Sub
        Public Sub getPatchLocations(ByVal index As Int32, ByVal userTypeList As List(Of UserTypeBO))
            objUserDAL.getPatchLocations(index, userTypeList)
        End Sub
        Public Sub getSchemeNames(ByVal selectedValue As Int32, ByVal userTypeList As List(Of UserTypeBO))
            objUserDAL.getSchemeNames(selectedValue, userTypeList)
        End Sub


#End Region

#Region "Quick Find User"
        Public Sub getQuickFindUsers(ByVal firstname As String, ByVal lastname As String, ByRef names As DataSet)
            objUserDAL.getQuickFindUsers(firstname, lastname, names)
        End Sub
#End Region

#Region "Get Inspection Types"
        Public Sub getInspectionTypes(ByRef resultDataSet As DataSet)
            objUserDAL.getInspectionTypes(resultDataSet)
        End Sub
#End Region

#Region "Get All Trades"
        Public Sub getAllTrades(ByRef resultDataSet As DataSet)
            objUserDAL.getTrades(resultDataSet)
        End Sub
#End Region


#Region "Save Selected Trades"
        Public Sub saveSelectedTrades(ByVal empId As String, ByVal tradeIds As String, ByVal userId As String, ByRef resultDataSet As DataSet)
            objUserDAL.saveTrades(empId, tradeIds, userId, resultDataSet)
        End Sub
#End Region

#Region "Get Emp Trades"
        Public Sub GetEmpTrades(ByVal empId As String, ByRef resultDataSet As DataSet)
            objUserDAL.GetEmpTrades(empId, resultDataSet)
        End Sub
#End Region

#Region "Save oftec gas safe and regNo"
        Public Sub saveJobDetails(ByVal gasSafe As Integer, ByVal ofTec As Integer, ByVal userId As Integer, ByVal empId As Integer, ByVal regNo As String, ByRef resultDataSet As DataSet)
            objUserDAL.saveOftecGassafe(ofTec, gasSafe, regNo, userId, empId, resultDataSet)
        End Sub
#End Region

#Region "Get Inspection Types By Desc"
        Public Sub getInspectionTypesByDesc(ByRef resultDataSet As DataSet, ByVal description As String)
            objUserDAL.getInspectionTypesByDesc(resultDataSet, description)
        End Sub
#End Region

#Region "Edit User"
        Public Sub EditUser(ByVal employeeId As Integer, ByRef edittUserDataSet As DataSet, ByRef edittUserPatchDataSet As DataSet, ByRef edittUserInspectionDataSet As DataSet)
            objUserDAL.EditUser(employeeId, edittUserDataSet, edittUserPatchDataSet, edittUserInspectionDataSet)
        End Sub
#End Region

        Sub UpdateResource(ByVal UserId As Object, ByVal value As String, ByRef resultDataset As DataSet)
            objUserDAL.UpdateResource(UserId, value, resultDataset)
        End Sub

        Sub deleteUser(ByVal EmployeeId As Integer, ByRef resultDataset As DataSet)
            objUserDAL.deleteUser(EmployeeId, resultDataset)
        End Sub

        Function saveUser(ByVal CreatedBy As Integer, ByRef resultDataSet As DataSet, ByVal EmployeeId As Integer, ByVal SeletedTypeValue As Integer, ByVal chkBoxInspectionList As UI.WebControls.CheckBoxList, ByVal signatureFileName As String)
            Dim userCount As Integer = 0
            userCount = objUserDAL.saveUser(CreatedBy, EmployeeId, SeletedTypeValue, resultDataSet, signatureFileName)

            'For Each scheme As Integer In userViewStatSchemeList
            '    objUserDAL.saveUserViewStatSchemeList(scheme, EmployeeId)
            'Next

            'For Each patchid As Integer In userViewStatPatchList
            '    objUserDAL.saveUserViewStatPatchList(patchid, EmployeeId)
            'Next
            If userCount > 0 Then
                Return userCount

            Else
                Dim itemCount As Integer

                itemCount = chkBoxInspectionList.Items.Count

                For i = 0 To (itemCount - 1)
                    If chkBoxInspectionList.Items(i).Selected Then
                        objUserDAL.saveChkBoxInspectionList(chkBoxInspectionList.Items(i).Value, EmployeeId)

                    End If
                Next i

                Return userCount
            End If
        End Function

        Sub updateUser(ByVal ModifiedBy As Integer, ByRef resultDataSet As DataSet, ByVal EmployeeId As Integer, ByVal signaturePath As String, ByVal userViewStatSchemeList As List(Of Integer), ByVal userViewStatPatchList As List(Of Integer), ByVal SeletedTypeValue As Integer, ByVal chkBoxInspectionList As UI.WebControls.CheckBoxList)

            objUserDAL.updateUser(ModifiedBy, EmployeeId, signaturePath, SeletedTypeValue, resultDataSet)

            If Not userViewStatSchemeList Is Nothing Then

                For Each scheme As Integer In userViewStatSchemeList
                    objUserDAL.updadteUserViewStatSchemeList(scheme, EmployeeId)
                Next

            End If
            If Not userViewStatPatchList Is Nothing Then

                For Each patchid As Integer In userViewStatPatchList
                    objUserDAL.updateUserViewStatPatchList(patchid, EmployeeId)
                Next

            End If
            objUserDAL.deleteUserInspectionType(EmployeeId)
            Dim itemCount As Integer

            itemCount = chkBoxInspectionList.Items.Count
            For i = 0 To (itemCount - 1)
                If chkBoxInspectionList.Items(i).Selected Then
                    objUserDAL.updateChkBoxInspectionList(chkBoxInspectionList.Items(i).Value, EmployeeId)

                End If
            Next i
        End Sub

        Sub getEmployeeIdByName(ByVal firstName As String, ByVal lastName As String, ByRef userInfoDataSet As DataSet)
            objUserDAL.getEmployeeIdByName(firstName, lastName, userInfoDataSet)
        End Sub

        Function getEmployeeById(ByVal employeeId As Integer) As DataSet
            Return objUserDAL.getEmployeeById(employeeId)
        End Function

        Function GetPropertyEmployeeEmail(employeeId As String) As String
            Return objUserDAL.GetPropertyEmployeeEmail(employeeId)
        End Function

        Function GetEmployeeWorkMobile(employeeId As String) As String
            Return objUserDAL.GetEmployeeWorkMobile(employeeId)
        End Function

#End Region

    End Class

End Namespace
