﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class PropertyBL

#Region "Attributes"
        Dim objPropertDAL As PropertyDAL = New PropertyDAL()
#End Region

#Region "Functions"

#Region "Get Appliance Locations"
        Public Function getPropertyDocuments(ByRef resultDataSet As DataSet, ByVal objPropertyDocumentBO As PropertyDocumentBO, ByRef objPageSortBo As PageSortBO, Optional ByVal isSerachClick As Boolean = False)
            Try

                Return objPropertDAL.getPropertyDocuments(resultDataSet, objPropertyDocumentBO, objPageSortBo, isSerachClick)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "Save Uploaded Document"

        Public Sub savePropertyDocumentUpload(ByVal objPropertyDocumentBO As PropertyDocumentBO)

            Try
                objPropertDAL.savePropertyDocumentUpload(objPropertyDocumentBO)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Delete Uploaded Document Id and get Document file path"

        Public Sub deleteDocumentByIdAndGetPath(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            objPropertDAL.deleteDocumentByIDandGetPath(objPropertyDocumentBO)
        End Sub

#End Region

#Region "Get Uploaded Document Information by Id"

        Public Sub getPropertyDocumentInformationById(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            objPropertDAL.getPropertyDocumentInformationById(objPropertyDocumentBO)
        End Sub

#End Region

#Region "Get Latest Epc Document"

        Public Sub getLatestEpcDocument(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            objPropertDAL.getLatestEpcDocument(objPropertyDocumentBO)
        End Sub

#End Region

#Region "Property Activities List"
        Public Sub propertyActivitiesList(ByRef resultDataSet As DataSet, ByRef sortDirection As String, ByRef sortExpression As String, ByRef actionType As Integer, ByRef propertyId As String)

            Try
                objPropertDAL.propertyActivitiesList(resultDataSet, sortDirection, sortExpression, actionType, propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Property Letter List"
        Public Sub propertyLetterList(ByRef resultDataSet As DataSet, ByRef journalHistoryId As Int64)

            Try
                objPropertDAL.propertyLetterList(resultDataSet, journalHistoryId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Property Document List"
        Public Sub propertyDocumentList(ByRef resultDataSet As DataSet, ByRef journalHistoryId As Int64)

            Try
                objPropertDAL.propertyDocumentList(resultDataSet, journalHistoryId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "save Property Activity "
        Public Function savePropertyActivity(ByVal objPropActivityBo As PropertyActivityBO, ByRef savedLetterDt As DataTable)

            Try
                Dim rentBalance As Double = 0
                Dim rentCharge As Double = 0
                objPropertDAL.getPropertyRbRc(objPropActivityBo.PropertyId, rentBalance, rentCharge)
                Return objPropertDAL.savePropertyActivity(objPropActivityBo, savedLetterDt, rentBalance, rentCharge)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "get Property Address "
        Public Function getPropertyAddress(ByRef propertyId) As String
            Try
                Return objPropertDAL.getPropertyAddress(propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Property Appliances List"
        Public Function propertyAppliancesList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal propertyId As String, ByRef itemId As Integer)

            Try
                Return objPropertDAL.propertyAppliancesList(resultDataSet, objPageSortBo, propertyId, itemId)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Get Appliance Locations"
        Public Sub getApplianceLocations(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Try

                objPropertDAL.getApplianceLocations(resultDataSet, prefix)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Fuel"
        Public Sub GetFuel(ByRef resultDataSet As DataSet, Optional ByVal certificateNameSelectedValue As Integer = 0)
            Try
                objPropertDAL.GetFuel(resultDataSet, certificateNameSelectedValue)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Certificates"
        Public Sub GetCertificates(ByRef resultDataSet As DataSet, Optional ByVal fuelTypeSelectedValue As Integer = -1)
            Try
                objPropertDAL.GetCertificateNames(resultDataSet, fuelTypeSelectedValue)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Alternative Fuel Type"
        Public Sub GetAlternativeFuelType(ByRef resultDataSet As DataSet)
            Try
                objPropertDAL.GetAlternativeFuelType(resultDataSet)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

#Region "Get Status"
        Public Sub getStatus(ByRef resultDataSet As DataSet)
            Try

                objPropertDAL.getStatus(resultDataSet)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Appliance Type"
        Public Sub getApplianceType(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Try

                objPropertDAL.getApplianceType(resultDataSet, prefix)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Make"
        Public Sub getMake(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Try

                objPropertDAL.getMake(resultDataSet, prefix)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Flue Type"
        Public Sub getFlueType(ByRef resultDataSet As DataSet)
            Try

                objPropertDAL.getFlueType(resultDataSet)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "save Property Appliance "
        Public Function savePropertyAppliance(ByVal objApplianceBo As ApplianceBO) As Integer

            Try
                Return objPropertDAL.savePropertyAppliance(objApplianceBo)

            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Delete Property Appliance "
        Public Function deletePropertyAppliance(ByVal applianceId As Integer)
            Return objPropertDAL.deletePropertyAppliance(applianceId)
        End Function
#End Region

#Region "Attributes Area"

#Region "get Locations"
        Public Sub getLocations(ByRef resultDataSet As DataSet)
            objPropertDAL.getLocations(resultDataSet)
        End Sub
#End Region

#Region "get Areas by Location Id"
        Sub getAreasByLocationId(ByVal locationId As Integer, ByVal resultDataSet As DataSet)
            objPropertDAL.getAreasByLocationId(locationId, resultDataSet)
        End Sub
#End Region

#Region "get Items by Area Id"
        Sub getItemsByAreaId(ByVal areaId As Integer, ByVal resultDataSet As DataSet)
            objPropertDAL.getItemsByAreaId(areaId, resultDataSet)
        End Sub
#End Region

#Region "get Sub Items by ItemId"
        Sub getItemsByItemId(ByVal ItemId As Integer, ByVal resultDataSet As DataSet)
            objPropertDAL.getItemsByItemId(ItemId, resultDataSet)
        End Sub
#End Region
#Region "get Sub Items by ItemId"
        Sub getSubItemsByItemId(ByVal ItemId As Integer, ByVal resultDataSet As DataSet)
            objPropertDAL.getSubItemsByItemId(ItemId, resultDataSet)
        End Sub
#End Region

#Region "load ddl Meter Location"
        Sub loadddlMeterLocation(ByRef meterLocationDataSet As DataSet)
            objPropertDAL.loadddlMeterLocation(meterLocationDataSet)
        End Sub
#End Region

#Region "get Property Gas Info"
        Sub getPropertyGasInfo(ByRef ObjPropertyGasInfoBO As PropertyGasInfoBO)
            objPropertDAL.getPropertyGasInfo(ObjPropertyGasInfoBO)
        End Sub
#End Region

#Region "update Property Gas Info"
        Sub updatePropertyGasInfo(ByRef objPropertyGasInfoBO As PropertyGasInfoBO)
            objPropertDAL.updatePropertyGasInfo(objPropertyGasInfoBO)
        End Sub
#End Region

#Region "load Category DDL"
        Sub loadCategoryDDL(ByRef resultDataSet As DataSet)
            objPropertDAL.loadCategoryDDL(resultDataSet)
        End Sub
#End Region

#Region "load Property Appliances DDL"
        Sub loadPropertyAppliancesDDL(ByRef propertyId As String, ByRef resultDataSet As DataSet)
            objPropertDAL.loadPropertyAppliancesDDL(propertyId, resultDataSet)
        End Sub
#End Region

#Region "save Defect"
        Sub saveDefect(ByVal objDefectBO As ApplianceDefectBO)
            objPropertDAL.saveDefect(objDefectBO)
        End Sub
#End Region

#Region "save Defect For Scheme/Block"
        Sub saveDefectForSchemeBlock(ByVal objDefectBO As ApplianceDefectBO)
            objPropertDAL.saveDefectForSchemeBlock(objDefectBO)
        End Sub
#End Region

#Region "save Category"
        Sub addCategory(ByVal categoryTitle As String)
            objPropertDAL.addCategory(categoryTitle)
        End Sub
#End Region

#Region " get Property Appliances"
        Sub getPropertyAppliances(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            objPropertDAL.getPropertyAppliances(propertyId, resultDataSet)
        End Sub
#End Region

#Region "get Property Defect Details"
        Sub getPropertyDefectDetails(ByVal propertyDefectId As String, ByRef objApplianceDefectBO As ApplianceDefectBO)
            objPropertDAL.getPropertyDefectDetails(propertyDefectId, objApplianceDefectBO)
        End Sub
#End Region

#Region "get Property Document"
        Public Sub getPropertyDocument(ByRef documentId As Integer, ByRef journalHistoryId As Integer, ByRef documentName As String, ByRef documentPath As String)
            Try
                objPropertDAL.getPropertyDocument(documentId, journalHistoryId, documentName, documentPath)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#End Region

#Region "Get CP12Document By LGSRID"

        Sub getCP12DocumentByLGSRID(ByRef dataSet As DataSet, ByRef LGSRID As Integer)
            objPropertDAL.getCP12DocumentByLGSRID(dataSet, LGSRID)
        End Sub

#End Region

#Region "Get PropertyInspection Document by InspectionId"
        Sub getInspectionRecordByInspectionId(ByRef dataSet As DataSet, ByRef Document As Integer)
            objPropertDAL.getInspectionRecordByInspectionId(dataSet, Document)
        End Sub
#End Region

#Region "Save Print Certificate Activity"
        Sub savePrintCertificateActivity(ByRef objPropertyActivityBO As PropertyActivityBO)
            objPropertDAL.savePrintCertificateActivity(objPropertyActivityBO)
        End Sub
#End Region

#Region "Get Tenants By PropertyId"
        Sub getTenantsByPropertyId(ByVal tenantsDS As DataSet, ByVal propertyId As String)
            objPropertDAL.getTenantsByPropertyId(tenantsDS, propertyId)
        End Sub
#End Region

#Region "Save Email Certificate Activity"

        Sub saveEmailCertificateActivity(ByRef objPropertyActivityBO As PropertyActivityBO)
            objPropertDAL.saveEmailCertificateActivity(objPropertyActivityBO)
        End Sub

#End Region

#Region "get Parameters"
        Sub getParameters(ByVal itemId As Integer, ByVal areaId As Integer, ByRef parameterDataSet As DataSet)
            objPropertDAL.getParameters(itemId, areaId, parameterDataSet)
        End Sub
#End Region

#Region "get Item detail"
        Sub getItemDetail(ByVal itemId As Integer, ByVal propertyId As String, ByRef resultDataSet As DataSet)
            objPropertDAL.getItemDetail(itemId, propertyId, resultDataSet)
        End Sub
#End Region

#Region "get Property Images"
        Sub getPropertyImages(ByVal proerptyId As String, ByVal itemId As Integer, ByRef photosDataSet As DataSet, ByVal heatingMappingId As Integer)
            objPropertDAL.getPropertyImages(proerptyId, itemId, photosDataSet, heatingMappingId)
        End Sub
#End Region

#Region "save Photograph"
        Sub savePhotograph(ByRef objPhotographBO As PhotographBO)
            objPropertDAL.savePhotograph(objPhotographBO)
        End Sub
#End Region

#Region "save EPC Photograph"
        Sub saveEPCPhotograph(ByRef objPhotographBO As PhotographBO)
            objPropertDAL.saveEPCPhotograph(objPhotographBO)
        End Sub
#End Region


#Region "get Property Defect Images"
        Sub getPropertyDefectImages(ByVal proerptyId As String, ByRef photosDataSet As DataSet)
            objPropertDAL.getPropertyDefectImages(proerptyId, photosDataSet)
        End Sub
#End Region

#Region "get Item Details"
        Sub getItemDetails(ByVal parameterId As Integer, ByRef loadParameterDDL As DataSet)
            objPropertDAL.getItemDetails(parameterId, loadParameterDDL)
        End Sub
#End Region

#Region "get Appliance Case Details"
        Sub getApplianceCaseDetails(ByRef loadApplianceData As DataSet, ByVal propertyId As String)
            objPropertDAL.getApplicanceCaseDetails(loadApplianceData, propertyId)
        End Sub
#End Region

#Region "load Property Details"
        Sub loadPropertyDetails(ByVal propertyId As String, ByRef objProertyDetails As PropertyDetailsBO)
            objPropertDAL.loadPropertyDetails(propertyId, objProertyDetails)
        End Sub
#End Region

#Region "Property Summary Tab Functions"
#Region "Get Tenancy/Property detail"
        ''' <summary>
        ''' Get Tenancy/Property detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyPropertyDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getTenancyPropertyDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Accommodation Summary"
        ''' <summary>
        ''' Get Accommodation Summary
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAccommodationSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getAccommodationSummary(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Property Address"
        ''' <summary>
        ''' Get Property Address
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyAddress(ByRef propertyId As String) As String

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            Return objPropertyDal.getPropertyAddress(propertyId)

        End Function

#End Region

#Region "Get Tenancy History"
        ''' <summary>
        ''' Get Tenancy History
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getTenancyHistory(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Asbestos Detail"
        ''' <summary>
        ''' Get Asbestos Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAsbestosDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getAsbestosDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Planned Appointments Detail"
        ''' <summary>
        ''' Get Planned Appointments Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPlannedComponentsDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getPlannedComponentsDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Property Condition Rating Detail"
        ''' <summary>
        ''' Get Property Condition Rating Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPropertyConditionRatingDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getPropertyConditionRatingDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Planned Appointments Detail"
        ''' <summary>
        ''' Get Planned Appointments Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPlannedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getPlannedAppointmentsDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Stock/Gas/Fault Appointments Detail"
        ''' <summary>
        ''' Get Stock/Gas/Fault Appointments Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getStockGasFaultAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getStockGasFaultAppointments(resultDataSet, propertyId)

        End Sub

#End Region


#Region "Get Reported Faults/Defects"
        ''' <summary>
        ''' Get Reported Faults/Defects
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getReportedFaultsDefects(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByVal actionType As Integer)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getReportedFaultsDefects(resultDataSet, propertyId, actionType)

        End Sub

#End Region

#Region "Get Job Sheet Details"
        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim objPropertyDAL As PropertyDAL = New PropertyDAL()
            objPropertyDAL.getJobSheetDetails(resultDataSet, jobSheetNumber)


        End Sub
#End Region

#Region "Get Property Images"
        ''' <summary>
        ''' Get Property Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            Return objPropertyDal.getPropertyImages(resultDataSet, propertyId)

        End Function

#End Region






#Region "Get Property EPC Images"
        ''' <summary>
        ''' Get Property EPC Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyEPCImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            Return objPropertyDal.getPropertyEPCImages(resultDataSet, propertyId)

        End Function

#End Region


#End Region

#Region "Get CP12Document By LGSRHISTORYID"

        Sub getCP12DocumentByLGSRHISTORYID(ByVal dataSet As DataSet, ByVal LGSRHISTORYID As Integer)
            objPropertDAL.getCP12DocumentByLGSRHISTORYID(dataSet, LGSRHISTORYID)
        End Sub

#End Region

#Region "Get Planned Inspection Document"

        Sub getPlannedInspectionDocument(ByVal dataSet As DataSet, ByVal JournalHistoryId As Integer)
            objPropertDAL.getPlannedInspectionDocument(dataSet, JournalHistoryId)
        End Sub

#End Region

#Region "Amend Item Detail"

        Public Sub amendItemDetail(ByRef objItemAttributeBo As ItemAttributeBO, ByVal itemDetailDt As DataTable, ByVal itemDatesDt As DataTable, ByVal conditionRatingDt As DataTable, ByVal MSATDetailDt As DataTable)
            objPropertDAL.AmendItemDetail(objItemAttributeBo, itemDetailDt, itemDatesDt, conditionRatingDt, MSATDetailDt)
        End Sub
        Sub deleteHeating(ByVal heatingMappingId As Integer)
            objPropertDAL.deleteHeating(heatingMappingId)
        End Sub
#End Region

#Region "save Item Notes"
        Sub SaveItemNotes(ByRef objItemNotesBo As ItemNotesBO)
            objPropertDAL.saveItemNotes(objItemNotesBo)
        End Sub
#End Region

#Region "Update Item Notes"
        Sub UpdateItemNotes(ByRef objItemNotesBo As ItemNotesBO)
            objPropertDAL.UpdateItemNotes(objItemNotesBo)
        End Sub
#End Region

#Region "get Item Notes"
        Sub GetItemNotes(ByVal proerptyId As String, ByVal itemId As Integer, ByRef notesDataSet As DataSet, Optional ByVal heatingMappingId As Integer = 0)
            objPropertDAL.GetItemNotes(proerptyId, itemId, notesDataSet, heatingMappingId)
        End Sub
#End Region

#Region "Delete Item Notes"
        Sub DeleteItemNotes(ByVal itemNotesId As Integer)
            objPropertDAL.DeleteItemNotes(itemNotesId)
        End Sub
#End Region

#Region "Item Notes Trail"
        Sub GetItemNotesTrail(ByVal itemNotesId As Integer, ByRef notesDataSet As DataSet)
            objPropertDAL.GetItemNotesTrail(itemNotesId, notesDataSet)
        End Sub
#End Region

#Region "get Models List "
        Public Sub getModelsList(ByRef resultDataset As DataSet, ByVal prefix As String)
            Try
                objPropertDAL.getModelsList(resultDataset, prefix)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Financial Tab"
#Region "Get Property Financial Tab Drop down Lists (Frequency,Funding Authority and Charge) Information"
        Public Sub getPropertyFinancialTabDropdownInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Try
                objPropertDAL.getPropertyFinancialTabDropdownInformation(resultDataSet, propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Get Property Rent Type and Financial Information"
        Public Sub getRentTypeAndFinancialInfomration(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Try
                objPropertDAL.getRentTypeAndFinancialInfomration(resultDataSet, propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Get Property Rent Type and Financial Information"
        Public Sub getTargetRentDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Try
                objPropertDAL.getTargetRentDetail(resultDataSet, propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region
#Region "get Rent History"
        Public Function getRentHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Try

                Return objPropertDAL.getRentHistory(resultDataSet, propertyId, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "get Valuation History"
        Public Function getValuationHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Try

                Return objPropertDAL.getValuationHistory(resultDataSet, propertyId, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "save Property Financial Information"
        Sub savePropertyFinancialInformation(ByRef objPropertyFinancialBO As PropertyFinancialBO)
            objPropertDAL.savePropertyFinancialInformation(objPropertyFinancialBO)
        End Sub
#End Region

#Region "save Property Current Rent Information"
        Sub savePropertyCurrentRentInformation(ByRef objPropertyRentBO As PropertyRentBO)
            objPropertDAL.savePropertyCurrentRentInformation(objPropertyRentBO)
        End Sub
#End Region

#Region "save Property Target Rent Information"
        Sub savePropertyTargetRentInformation(ByRef objPropertyRentBO As PropertyRentBO)
            objPropertDAL.savePropertyTargetRentInformation(objPropertyRentBO)
        End Sub
#End Region
#End Region

#Region "Health & Safety Tab"
#Region "Get Property Asbestos (Level and Elements) & Risk Information "
        Public Sub getPropertyAsbestosAndRisk(ByRef resultDataSet As DataSet)

            Try
                objPropertDAL.getPropertyAsbestosAndRisk(resultDataSet)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Get Property Asbestos (Level and Elements) & Risk Information, Added, Removed and Notes"
        Public Sub getPropertyAsbestosRiskAndOtherInfo(ByRef resultDataSet As DataSet, ByRef asbestosID As Integer)
            Try
                objPropertDAL.getPropertyAsbestosRiskAndOtherInfo(resultDataSet, asbestosID)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "save Asbestos Information"
        Sub saveAsbestos(ByRef objPropertyAsbestosBO As PropertyAsbestosBO)
            objPropertDAL.saveAsbestos(objPropertyAsbestosBO)
        End Sub
#End Region

#Region "Amend Asbestos Information"
        Sub amendAsbestos(ByRef objPropertyAsbestosBO As PropertyAsbestosBO)
            objPropertDAL.amendAsbestos(objPropertyAsbestosBO)
        End Sub
#End Region


#Region "save Refurbishment Information"
        Public Function saveRefurbishment(ByRef objRefurbishmentBO As RefurbishmentBO) As String
            Return objPropertDAL.saveRefurbishment(objRefurbishmentBO)
        End Function
#End Region

#Region "Display Health & Safety Details"
        Public Function getHealthAndSafetyTabInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Try

                Return objPropertDAL.getHealthAndSafetyTabInformation(resultDataSet, propertyId, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "Display refurbishment detail"
        Public Sub getRefurbishmentInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String)
            Try
                objPropertDAL.getRefurbishmentInformation(resultDataSet, propertyId)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

        Public Sub GetEmployeeEmailForAsbestosEmailNotifications(ByRef propertyId As String, ByRef employeeEmailDataSet As DataSet)
            objPropertDAL.GetEmployeeEmailForAsbestosEmailNotifications(propertyId, employeeEmailDataSet)
        End Sub
#End Region


#Region "Get Attribute Summary"
        ''' <summary>
        ''' Get Tenancy History
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAttributeSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getAttributeSummary(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Property Rooms Dimensions by PropertyId"
        Function getRoomsDimensions(propertyId As String, Optional dataset As DataSet = Nothing) As DataSet
            Dim objPropertyDal As New PropertyDAL()
            Return objPropertyDal.getRoomsDimensions(propertyId, dataset)
        End Function
#End Region

#Region "Property Data Restructure"
#Region "Get Properties List"
        ''' <summary>
        ''' Get Properties List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getPropertiesList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO)

            Return objPropertDAL.getPropertiesList(resultDataSet, search, objPageSortBo)

        End Function

#End Region



#Region "get Property Template"
        Public Sub getPropertyTemplateForAllDropDown(ByRef resultDataSet As DataSet)
            Try

                objPropertDAL.getPropertyTemplateForAllDropDown(resultDataSet)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get getProperty Template by propertyID "
        Public Sub getPropertyDetailByPropertyId(ByRef resultDataSet As DataSet, ByVal propertyId As String)
            Try

                objPropertDAL.getPropertyDetailByPropertyId(resultDataSet, propertyId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Blocks By BlockID"
        Public Sub getBlocksByBlockId(ByRef resultDataSet As DataSet, ByVal blockId As Integer)
            Try

                objPropertDAL.getBlocksByBlockId(resultDataSet, blockId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Blocks By DevelopmentID"
        Public Sub getBlocksByDevelopmentId(ByRef resultDataSet As DataSet, ByVal devId As Integer)
            Try

                objPropertDAL.getBlocksByDevelopmentId(resultDataSet, devId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region
#Region "get Phase List"
        Public Sub getPhaseList(ByRef resultDataSet As DataSet, ByVal devId As Integer)
            Try
                objPropertDAL.getPhaseList(resultDataSet, devId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Phase List"
        Public Sub getPhaseDetail(ByRef resultDataSet As DataSet, ByVal phaseId As Integer)
            Try
                objPropertDAL.getPhaseDetail(resultDataSet, phaseId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region
#Region "get Property Sub Status"
        Public Sub getPropertySubStatus(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            Try
                objPropertDAL.getPropertySubStatus(resultDataSet, statusId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Property NROSH Asset Type Sub"
        Public Sub getPropertyNROSHAssetTypeSub(ByRef resultDataSet As DataSet, ByVal sId As Integer)
            Try
                objPropertDAL.getPropertyNROSHAssetTypeSub(resultDataSet, sId)

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region


#Region "save Property Detail"
        Function SavePropertyDetail(ByRef objPropertyDetailsBO As PropertiesDetailsBO, ByVal complDate As Date, ByVal handDate As Date) As String
            Return objPropertDAL.SavePropertyDetail(objPropertyDetailsBO, complDate, handDate)
        End Function
#End Region

#Region "load cycle DDL"
        Sub loadCycleDdl(ByRef resultDataSet As DataSet)
            objPropertDAL.loadCycleDdl(resultDataSet)
        End Sub
#End Region



#End Region

#Region "Get Warranties List"
        Sub getWarrantiesList(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal objPageSortBO As PageSortBO)
            objPropertDAL.getWarrantiesList(resultDataSet, propertyId, objPageSortBO)
        End Sub
#End Region

#Region "get Warranty Types"


        Sub getWarrantyTypes(ByRef warrantyTypeDataSet As DataSet)
            objPropertDAL.getWarrantyTypes(warrantyTypeDataSet)
        End Sub
#End Region

#Region "get category DropdownValue"
        Sub getcategoryDropdownValue(ByRef forDataSet As DataSet)
            objPropertDAL.getcategoryDropdownValue(forDataSet)

        End Sub
#End Region

#Region "get area DropdownValues by category"
        Sub getAreaDropdownValueByCategoryId(ByVal categoryId As Integer, ByRef forDataSet As DataSet)
            objPropertDAL.getAreaDropdownValueByCategoryId(categoryId, forDataSet)

        End Sub
#End Region

#Region "get item DropdownValue by areaId"
        Sub getItemDropdownValueByAreaId(ByVal areaId As Integer, ByRef forDataSet As DataSet)
            objPropertDAL.getItemDropdownValueByAreaId(areaId, forDataSet)

        End Sub
#End Region

#Region "get Contractors"
        Sub getContractors(ByRef contractorDataSet As DataSet)
            objPropertDAL.getContractors(contractorDataSet)

        End Sub
#End Region

#Region "save Warranty"
        Sub saveWarranty(ByVal objWarrantyBO As WarrantyBO)
            objPropertDAL.saveWarranty(objWarrantyBO)

        End Sub
#End Region

#Region "Get Warranty Data"
        Sub getWarrantyData(ByRef warrantyDataSet As DataSet, ByVal warranty As Integer)
            objPropertDAL.getWarrantyData(warrantyDataSet, warranty)
        End Sub
#End Region

#Region "Delete Warranty"
        Sub deleteWarranty(ByVal warranty As Integer)
            objPropertDAL.deleteWarranty(warranty)
        End Sub
#End Region


#Region "Get contractor jobsheet detail"

        Public Sub getContractorJobsheetDetail(ByVal faultLogHistoryId As Int32, ByRef resultDataSet As DataSet)

            objPropertDAL.getContractorJobsheetDetail(faultLogHistoryId, resultDataSet)

        End Sub

#End Region

#Region "Get Appliance Cancel Notes"

        Public Function getApplianceCancelNotes(ByVal JournalId As Int32)

            Return objPropertDAL.getApplianceCancelNotes(JournalId)

        End Function

#End Region

#Region "Get Property Status History"
        Function getStatusHistory(ByVal propertyId As String) As DataSet
            Return objPropertDAL.getStatusHistory(propertyId)
        End Function
#End Region




#End Region

#Region "Save Property Image"
        Sub SavePropertyImage(ByRef objPropertyDetailsBO As PropertiesDetailsBO)

            objPropertDAL.SavePropertyImage(objPropertyDetailsBO)
        End Sub
#End Region

        Sub getPropertyCustomerEmail(ByRef resultDataSet As DataSet, ByVal letterHistoryId As String)
            objPropertDAL.getPropertyCustomerEmail(resultDataSet, letterHistoryId)
        End Sub

        Sub UpdatePropertyCustomerEmail(customerEmail As String, appointmentId As String, emailStatus As String)
            objPropertDAL.UpdatePropertyCustomerEmail(customerEmail, appointmentId, emailStatus)
        End Sub

        Public Function SaveRestriction(ByRef objRestrictionBO As RestrictionBO) As Boolean
            Return objPropertDAL.SaveRestriction(objRestrictionBO)
        End Function

        Public Sub getRestrictions(ByRef dataSet As DataSet, ByVal propertyId As String)
            objPropertDAL.getRestrictions(dataSet, propertyId)
        End Sub

        Public Sub getNHBCWarranties(ByRef dataSet As DataSet, ByVal propertyId As String)
            objPropertDAL.getNHBCWarranties(dataSet, propertyId)
        End Sub

        Public Sub getLandRegistrationOptions(ByRef dataSet As DataSet)
            objPropertDAL.getLandRegistrationOptions(dataSet)
        End Sub

    End Class
End Namespace

