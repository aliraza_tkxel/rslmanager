﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities


Namespace AS_BusinessLogic



    Public Class CalendarBL
        Dim objCalendarDAL As CalendarDAL = New CalendarDAL()
#Region "Functions"

#Region "get Engineers Info for Appointment"
        Public Sub getEngineersForCalendarAppointment(ByRef resultDataSet As DataSet)
            Try
                objCalendarDAL.getEngineersForCalendarAppointment(resultDataSet)
            Catch ex As Exception

                Throw
            End Try

        End Sub
#End Region

#Region "get Appointments Details for Weekly Calendar"
        Public Sub getAppointmentsWeeklyCalendarDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Try
                objCalendarDAL.getAppointmentsWeeklyCalendarDetail(objScheduleAppointmentsBO, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Appointments Details for Monthly Calendar"
        Public Sub getAppointmentsMonthlyCalendarDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Try
                objCalendarDAL.getAppointmentsMonthlyCalendarDetail(objScheduleAppointmentsBO, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Searched Appointments Details"
        Public Sub getSearchedAppointmentsDetail(ByRef resultDataSet As DataSet)
            Try
                objCalendarDAL.getSearchedAppointmentsDetail(resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "get Employee Core Working Hours"
        Sub getEmployeeCoreWorkingHours(ByRef resultDataSet As DataSet, ByVal employeeIds As String)
            objCalendarDAL.getEmployeeCoreWorkingHours(resultDataSet, employeeIds)
        End Sub
#End Region

#End Region

    End Class

End Namespace

