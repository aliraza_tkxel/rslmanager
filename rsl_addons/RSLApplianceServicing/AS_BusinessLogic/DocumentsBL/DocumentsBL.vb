﻿
Imports AS_Utilities

Public Class DocumentsBL

#Region "get Document Types"

    Public Sub getDocumentTypesList(ByRef resultDataSet As DataSet, ByVal ShowActiveOnly As Boolean, ByVal reportFor As String, ByVal CategoryId As Integer)

        Try
            Dim documentTypeDL As DocumentTypeDAL = New DocumentTypeDAL
            documentTypeDL.getDocumentTypes(resultDataSet, True, reportFor, CategoryId)

        Catch ex As Exception

            Throw
        End Try
    End Sub

#End Region
#Region "get Categories"

    Public Sub getCategories(ByRef resultDataSet As DataSet, ByRef reportFor As String)

        Try
            Dim documentTypeDL As DocumentTypeDAL = New DocumentTypeDAL
            documentTypeDL.getCategoryTypes(resultDataSet, reportFor)

        Catch ex As Exception

            Throw
        End Try
    End Sub

#End Region


#Region "get Document Subtypes"

    Public Sub getDocumentSubtypesList(ByRef resultDataSet As DataSet, ByVal DocumentTypeId As Int16, Optional ByVal reportFor As String = "")

        Try
            Dim documentTypeDL As DocumentTypeDAL = New DocumentTypeDAL
            documentTypeDL.getDocumentSubtypes(resultDataSet, True, DocumentTypeId, reportFor)

        Catch ex As Exception

            Throw
        End Try
    End Sub

#End Region


#Region "get EPC Category types"

    Public Sub EpcCategoryTypeList(ByRef resultDataSet As DataSet)

        Try
            Dim documentTypeDL As DocumentTypeDAL = New DocumentTypeDAL
            documentTypeDL.getEpcCategoryType(resultDataSet)

        Catch ex As Exception

            Throw
        End Try
    End Sub

#End Region


#Region "get Document type and subtype"

    Public Sub getDocumentTypeAndSubtypeList(ByRef resultDataSet As DataSet, ByVal Type As Int16, ByVal DocumentSubtypeId As Int16)

        Try
            Dim documentTypeDL As DocumentTypeDAL = New DocumentTypeDAL
            documentTypeDL.getDocumentTypeAndSubtype(resultDataSet, Type, DocumentSubtypeId)

        Catch ex As Exception

            Throw
        End Try
    End Sub

#End Region

End Class
