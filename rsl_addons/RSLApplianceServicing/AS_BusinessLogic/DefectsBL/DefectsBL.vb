﻿Imports AS_DataAccess
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_BusinessLogic

    Public Class DefectsBL

#Region "Get Defects Appointment To Be Arranged List"

        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal defectCategoryId As Integer, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)
            Dim defetsDal As New DefectsDAL()
            Return defetsDal.getAppointmentToBeArrangedList(resultDataSet, defectCategoryId, searchText, objPageSortBo)
        End Function

#End Region

#Region "Get Defects Appointment List"

        Public Function getAppointmentArrangedList(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)
            Dim defetsDal As New DefectsDAL()
            Return defetsDal.getAppointmentArrangedList(resultDataSet, searchText, objPageSortBo)
        End Function

#End Region

#Region "Get Defects List By Journal Id"

        Public Sub GetDefectsByJournalId(ByRef resultDataset As DataSet, ByVal journalId As Integer, ByVal defectCategoryId As Integer)
            Dim defetsDal As New DefectsDAL()
            defetsDal.GetDefectsByJournalId(resultDataset, journalId, defectCategoryId)
        End Sub

#End Region

#Region "Get Defect Management Look up values"

        Public Sub getDefectManagementLookupValues(ByRef dataset As DataSet, ByVal propertyId As String)
            Dim defectDal As New DefectsDAL
            defectDal.getDefectManagementLookupValues(dataset, propertyId)
        End Sub

#End Region

#Region "get All Schemes"
        Public Sub getAllSchemes(ByRef resultDataSet As DataSet)
            Dim defetsDal As New DefectsDAL()
            defetsDal.getAllSchemes(resultDataSet)
        End Sub
#End Region

#Region "get All Appliance Types"
        Public Function getAllApplianceTypes(ByVal defectType As String)
            Dim defetsDal As New DefectsDAL()
            Dim resultDt As DataTable = defetsDal.getAllApplianceTypes()
            If defectType <> ApplicationConstants.DropDownDefaultValue Then

                Dim filterApplianceQuery = From app In resultDt.AsEnumerable() _
                            Where app.Field(Of String)(ApplicationConstants.DefectTypeColumn) = defectType _
                            Select app

                resultDt = filterApplianceQuery.CopyToDataTable()

            End If

            If resultDt.Rows.Count > 0 Then
                resultDt = GeneralHelper.RemoveDuplicateRows(resultDt, ApplicationConstants.DefectTitleColumn)
                resultDt = resultDt.AsEnumerable.OrderBy(Function(r) r.Field(Of String)(ApplicationConstants.DefectTitleColumn)).CopyToDataTable()
            End If

            Return resultDt
        End Function
#End Region

#Region "Get Job Sheet Details by defectId"

        Sub getJobSheetDetails(jobSheetDataSet As DataSet, defectId As Integer)
            Dim objDefectDAL As New DefectsDAL
            objDefectDAL.getJobSheetDetails(jobSheetDataSet, defectId)
        End Sub

#End Region

#Region "Cancel Defect Appointmnet"

        Function cancelDefectAppointment(ByVal appointmentId As Integer, ByVal cancelReasonNotes As String, ByVal userId As Integer) As Boolean
            Dim objDefectDal As New DefectsDAL
            Return objDefectDal.cancelDefectAppointment(appointmentId, cancelReasonNotes, userId)
        End Function

#End Region

#Region "Rearrange Defect Appointment"

        Public Function rearrangeDefectAppointment(ByVal appointmentId As Integer, ByVal userId As Integer)
            Dim objDefectDal As New DefectsDAL
            Return objDefectDal.rearrangeDefectAppointment(appointmentId, userId)
        End Function

#End Region

#Region "Confirm Defect Appointments By DefectIds (Comma Seprated)"

        Public Sub confirmDefectAppointmentsByDefectIds(defectIds As String)
            Dim objDefectDal As New DefectsDAL
            objDefectDal.confirmDefectAppointmentsByDefectIds(defectIds)
        End Sub

#End Region

#Region "Get Detector By PropertyId"

        Public Function getDetectorByPropertyId(PropertyId As String, detectorType As String)
            Dim defetsDal As New DefectsDAL()
            Return defetsDal.getDetectorByPropertyId(PropertyId, detectorType)

        End Function

#End Region


#Region "Rearrange Defect Appointment"

        Public Function saveDetectors(objDetectorBo As DetectorsBO, smokeDetectorType As String, noOfSmokeDetectors As String)
            Dim defetsDal As New DefectsDAL()
            Return defetsDal.saveDetectors(objDetectorBo, smokeDetectorType, noOfSmokeDetectors)
        End Function
#End Region

    End Class

End Namespace