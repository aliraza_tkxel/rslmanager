﻿Imports System.Web
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_DataAccess
Imports System.Collections.Specialized


Namespace AS_BusinessLogic


    Public Class DashboardBL
        Dim objDashboardDAL As DashboardDAL = New DashboardDAL()

#Region "Counts"
        Public Sub getCounts(ByRef fuelTypeSelected As String, ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultHashTable As Hashtable)
            Try
                Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
                Dim objAlternativeSchedulingBL As AlternativeSchedulingBL = New AlternativeSchedulingBL()
                Dim objOilSchedulingBL As OilSchedulingBL = New OilSchedulingBL()
                Dim pageSort As PageSortBO = New PageSortBO("DESC", "JOURNALID", 1, 10)
                Dim totalCount As Integer

                'Getting the count of the "No Entries"
                Dim rsNoEntry As DataSet = New DataSet()
                objDashboardDAL.getCountNoEntry(patchId, developmentId, rsNoEntry, fuelTypeSelected)
                resultHashTable.Add(ApplicationConstants.HashTableKeyNoEntry, rsNoEntry.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Legal Proceedings"
                Dim rsLegalProceedings As DataSet = New DataSet()
                objDashboardDAL.getCountLegalProceedings(patchId, developmentId, rsLegalProceedings, fuelTypeSelected)
                resultHashTable.Add(ApplicationConstants.HashTableKeyLegalProceedings, rsLegalProceedings.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Expired"
                Dim rsExpired As DataSet = New DataSet()
                objDashboardDAL.getCountExpired(patchId, developmentId, rsExpired, fuelTypeSelected)
                resultHashTable.Add(ApplicationConstants.HashTableKeyExpired, rsExpired.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Expires in 56 Days"
                Dim rsExpiresIn56Days As DataSet = New DataSet()
                objDashboardDAL.getCountExpiresIn56Days(patchId, developmentId, rsExpiresIn56Days, fuelTypeSelected)
                resultHashTable.Add(ApplicationConstants.HashTableKeyExpiresIn56Days, rsExpiresIn56Days.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Appointments Arranged"
                Dim rsAppointmentsArranged As DataSet = New DataSet()

                If fuelTypeSelected.Equals("Gas") Then
                    totalCount = objSchedulingBL.getAppointmentsArrangedList(rsAppointmentsArranged, False, String.Empty, pageSort, patchId, -1, 172, developmentId)
                ElseIf (fuelTypeSelected.Equals("Oil")) Then
                    totalCount = objOilSchedulingBL.GetOilAppointmentsArrangedList(rsAppointmentsArranged, False, String.Empty, pageSort, patchId, -1, developmentId)
                Else
                    totalCount = objAlternativeSchedulingBL.GetAlternativeAppointmentsArrangedList(rsAppointmentsArranged, False, String.Empty, pageSort, patchId, -1, -1, developmentId)
                End If
                objDashboardDAL.getCountAppointmentsArranged(patchId, developmentId, rsAppointmentsArranged)

                resultHashTable.Add(ApplicationConstants.HashTableKeyAppointmentsArranged, totalCount)
                'resultHashTable.Add(ApplicationConstants.HashTableKeyAppointmentsArranged, rsAppointmentsArranged.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Appointments to be Arranged"
                Dim rsAppointmentsToBeArranged As DataSet = New DataSet() ' Separate call for Alternative and Gas
                If fuelTypeSelected.Equals("Gas") Then
                    totalCount = objSchedulingBL.getAppointmentToBeArrangedList(rsAppointmentsToBeArranged, False, String.Empty, pageSort, 172, String.Empty, patchId, developmentId)
                ElseIf (fuelTypeSelected.Equals("Oil")) Then
                    totalCount = objOilSchedulingBL.GetOilAppointmentToBeArrangedList(rsAppointmentsToBeArranged, False, String.Empty, pageSort, String.Empty, patchId, developmentId)
                Else
                    totalCount = objAlternativeSchedulingBL.GetAlternativeAppointmentToBeArrangedList(rsAppointmentsToBeArranged, False, String.Empty, pageSort, -1, String.Empty, patchId, developmentId)
                End If


                resultHashTable.Add(ApplicationConstants.HashTableKeyAppointmentsToBeArranged, totalCount)

                'objDashboardDAL.getCountAppointmentsToBeArranged(patchId, schemeId, rsAppointmentsToBeArranged)
                'resultHashTable.Add(ApplicationConstants.HashTableKeyAppointmentsToBeArranged, rsAppointmentsToBeArranged.Tables(0).Rows(0).Item(0))


                'Getting the count of the "Void Appointments Arranged"
                Dim rsVoidAppointmentsArranged As DataSet = New DataSet()
                objDashboardDAL.getCountVoidAppointmentsArranged(fuelTypeSelected, patchId, developmentId, rsVoidAppointmentsArranged)
                resultHashTable.Add(ApplicationConstants.HashTableKeyVoidAppointmentsArranged, rsVoidAppointmentsArranged.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Void Appointments to be Arranged"
                Dim rsVoidAppointmentsToBeArranged As DataSet = New DataSet()
                objDashboardDAL.getCountVoidAppointmentsToBeArranged(fuelTypeSelected, patchId, developmentId, rsVoidAppointmentsToBeArranged)
                resultHashTable.Add(ApplicationConstants.HashTableKeyVoidAppointmentsToBeArranged, rsVoidAppointmentsToBeArranged.Tables(0).Rows(0).Item(0))

                'Getting the count of the "Void Properties"
                Dim rsVoidProperties As DataSet = New DataSet()
                objDashboardDAL.getCountVoidProperties(fuelTypeSelected, patchId, developmentId, rsVoidProperties)
                resultHashTable.Add(ApplicationConstants.HashTableKeyVoidProperties, rsVoidProperties.Tables(0).Rows(0).Item(0))

                'Getting the Defect Appointment count of the "Appointments to be Arranged"
                Dim rsDefectsAppointmentsToBeArranged As DataSet = New DataSet()
                Dim objDefectsDal As DefectsDAL = New DefectsDAL()
                Dim defectsAppointmentToBeArrangedCount As Integer = objDefectsDal.getAppointmentToBeArrangedListCount(rsDefectsAppointmentsToBeArranged, developmentId)
                resultHashTable.Add(ApplicationConstants.HashTableKeyDefectAppointmentsToBeArranged, defectsAppointmentToBeArrangedCount)

                'Getting the Defect Appointment count of the "Appointments Arranged"
                Dim rsDefectsAppointmentsArranged As DataSet = New DataSet()
                Dim defectsAppointmentArrangedCount As Integer = objDefectsDal.getAppointmentArrangedListCount(rsDefectsAppointmentsArranged, developmentId)
                resultHashTable.Add(ApplicationConstants.HashTableKeyDefectAppointmentsArranged, defectsAppointmentArrangedCount)

                'Getting the Disconnected / capped appliances count"
                Dim objReportDal As ReportDAL = New ReportDAL()
                Dim rsDefectsCappedAppliances As DataSet = New DataSet()
                Dim defectsCappedAppliancesCount As Integer = objReportDal.getDisconnectedAppliancesCount(rsDefectsCappedAppliances, developmentId)
                resultHashTable.Add(ApplicationConstants.HashTableKeyDefectCappedAppliances, defectsCappedAppliancesCount)

                'Getting the Defects requiring approval count"
                Dim rsDefectsRequiringApproval As DataSet = New DataSet()
                objReportDal.getDefectsRequiringApprovalCount(rsDefectsRequiringApproval)
                resultHashTable.Add(ApplicationConstants.HashTableKeyDefectsRequiringApproval, rsDefectsRequiringApproval.Tables(0).Rows(0).Item(0))

            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Management Info Counts"

        Public Sub getCountMIData(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultSet As DataSet, ByVal fuelType As String)
            Try
                objDashboardDAL.getCountMIData(patchId, developmentId, resultSet, fuelType)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Certificate Expiry"

        Public Sub getCertificateExpiry(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultSet As DataSet, ByRef fuelType As String)
            Try
                objDashboardDAL.getCertificateExpiry(patchId, developmentId, resultSet, fuelType)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Inspection Status"

        Public Function getInspectionStatus(ByVal inspectionStatus As Integer, ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim retrunValue As String = 0
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim objAlternativeSchedulingBL As AlternativeSchedulingBL = New AlternativeSchedulingBL()
            Dim objOilSchedulingBL As OilSchedulingBL = New OilSchedulingBL()
            Dim pageSort As PageSortBO = New PageSortBO("DESC", "JOURNALID", 1, 10)
            Try
                If (inspectionStatus = ApplicationConstants.InspectionStatusDropDownNoEntries) Then
                    retrunValue = objDashboardDAL.getNoEntries(patchId, developmentId, objPageSortBo, resultSet, FuelType)
                ElseIf (inspectionStatus = ApplicationConstants.InspectionStatusDropDownLegalProceedings) Then
                    retrunValue = objDashboardDAL.getLegalProceedings(patchId, developmentId, objPageSortBo, resultSet, FuelType)
                ElseIf (inspectionStatus = ApplicationConstants.InspectionStatusDropDownExpired) Then
                    retrunValue = objDashboardDAL.getExpired(patchId, developmentId, objPageSortBo, resultSet, FuelType)
                ElseIf (inspectionStatus = ApplicationConstants.InspectionStatusDropDownAppointmentsToBeArranges) Then
                    If FuelType.Equals("Gas") Then
                        retrunValue = objSchedulingBL.getAppointmentToBeArrangedList(resultSet, False, String.Empty, pageSort, 172, String.Empty, patchId, developmentId)
                    ElseIf (FuelType.Equals("Oil")) Then
                        retrunValue = objOilSchedulingBL.GetOilAppointmentToBeArrangedList(resultSet, False, String.Empty, pageSort, String.Empty, patchId, developmentId)
                    Else
                        retrunValue = objAlternativeSchedulingBL.GetAlternativeAppointmentToBeArrangedList(resultSet, False, String.Empty, pageSort, -1, String.Empty, patchId, developmentId)
                    End If
                ElseIf (inspectionStatus = ApplicationConstants.InspectionStatusDropDownAppointmentsAborted) Then
                    retrunValue = objDashboardDAL.getAbortAppointments(patchId, developmentId, objPageSortBo, resultSet, FuelType)
                End If

                Return retrunValue
            Catch ex As Exception

                Throw
            End Try

        End Function

#End Region

#Region "Last Login Date"

        Public Sub getLastLoginDate(ByVal employeeId As Integer, ByRef resultSet As DataSet)

            Try
                objDashboardDAL.getLastLoginDate(employeeId, resultSet)

            Catch ex As Exception

                Throw
            End Try

        End Sub


#End Region

#Region "Get RSLModules"
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByVal userId As String)
            objDashboardDAL.getRSLModules(resultDataSet, Convert.ToInt32(userId))
        End Sub
#End Region

#Region "Get Property Menus List"
        ''' <summary>
        ''' Get Property Menus List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyMenuList(ByRef resultDataSet As DataSet, ByVal userId As String)
            objDashboardDAL.getPropertyMenuList(resultDataSet, Convert.ToInt32(userId))
        End Sub
#End Region

#Region "Get Property Page List"
        ''' <summary>
        ''' Get Property Page List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="employeeId"></param>
        ''' <param name="selectedMenu"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)
            objDashboardDAL.getPropertyPageList(resultDataSet, employeeId, selectedMenu)
        End Sub
#End Region

#Region "Check Page Access"
        ''' <summary>
        ''' Check Page Access
        ''' </summary>
        ''' <remarks></remarks>
        Function checkPageAccess(ByVal menu As String)

            Dim userId As Integer = SessionManager.getUserEmployeeId()
            Dim isAccessGranted As Boolean = False
            Dim pageId As Integer

            Dim resultDataset As DataSet = New DataSet
            getPropertyPageList(resultDataset, userId, menu)

            Dim accessGrantedModulesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedModulesDt)
            Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
            Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
            Dim randomPageDt As DataTable = resultDataset.Tables(ApplicationConstants.RandomPageDt)

            If (checkPageExist(accessGrantedPagesDt, pageId)) Then
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, accessGrantedPagesDt, pageId)
            ElseIf (checkPageExist(randomPageDt, pageId)) Then
                randomPageDt.Merge(accessGrantedPagesDt)
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, randomPageDt, pageId)
            End If

            Return isAccessGranted

        End Function

#End Region

#Region "Check Page Exist"
        ''' <summary>
        ''' Check Page Exist
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageExist(ByVal pagesDt As DataTable, ByRef pageId As Integer)

            Dim pageFound As Boolean = False
            Dim pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1)
            Dim pageQueryString As NameValueCollection = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())

            Dim query = (From dataRow In pagesDt _
                       Where dataRow.Field(Of String)(ApplicationConstants.GrantPageFileNameCol).ToLower() = pageFileName.ToLower() _
                       Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol) Descending _
                       Select dataRow)
            Dim resultPageDt As DataTable = pagesDt.Clone()


            If query.Count > 0 Then
                resultPageDt = query.CopyToDataTable()

                For Each row As DataRow In resultPageDt.Rows
                    Dim fileName As String = row(ApplicationConstants.GrantPageFileNameCol)
                    Dim url As String = row(ApplicationConstants.GrantPageQueryStringCol)
                    Dim queryString As NameValueCollection = HttpUtility.ParseQueryString(url)

                    If (queryString.Count = 0 And pageQueryString.Count = 0) Then
                        pageFound = True
                        pageId = row(ApplicationConstants.GrantPageIdCol)
                        Exit For
                    Else
                        Dim matchCount As Integer = 0
                        For Each key As String In queryString.AllKeys
                            If (queryString(key).Equals(pageQueryString(key))) Then
                                matchCount = matchCount + 1
                            End If
                        Next key

                        If (queryString.Count = matchCount) Then
                            pageId = row(ApplicationConstants.GrantPageIdCol)
                            pageFound = True
                            Exit For
                        End If

                    End If

                Next

            End If

            Return pageFound

        End Function

#End Region

#Region "Check Page Hierarchy Access Rights"
        ''' <summary>
        ''' Check Page Hierarchy Access Rights
        ''' First check in pages levels e.g (level 3 , level 2 , level 1)
        ''' Second check in menu
        ''' Third check in modules
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageHierarchyAccessRights(ByVal modulesDt As DataTable, ByVal menusDt As DataTable, ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim isAccessGranted As Boolean = False

            If (checkInPageLevelsAccessRights(pagesDt, pageId)) Then

                Dim pageExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
                Dim pageRows() As DataRow = pagesDt.Select(pageExpression)
                Dim linkedMenuId As Integer = pageRows(0)(ApplicationConstants.GrantPageMenuIdCol)
                Dim linkedModuleId As Integer = pageRows(0)(ApplicationConstants.GrantPageModuleIdCol)

                Dim menuExpression As String = ApplicationConstants.GrantMenuMenuIdCol + " = " + Convert.ToString(linkedMenuId)
                Dim menuRows() As DataRow = menusDt.Select(menuExpression)

                If (menuRows.Count > 0) Then

                    Dim moduleExpression As String = ApplicationConstants.GrantModulesModuleIdCol + " = " + Convert.ToString(linkedModuleId)
                    Dim moduleRows() As DataRow = modulesDt.Select(moduleExpression)

                    If (moduleRows.Count > 0) Then
                        isAccessGranted = True
                    End If

                End If

            End If

            Return isAccessGranted
        End Function

#End Region

#Region "Check In Page Levels Access Rights"
        ''' <summary>
        ''' Check In Page Levels Access Rights from bottom to top
        ''' </summary>
        ''' <param name="pagesDt"></param>
        ''' <param name="pageId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkInPageLevelsAccessRights(ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim parentPageId As Integer
            Dim expression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
            Dim rows() As DataRow = pagesDt.Select(expression)

            If (IsDBNull(rows(0)(ApplicationConstants.GrantPageParentPageCol)) Or _
                IsNothing(rows(0)(ApplicationConstants.GrantPageParentPageCol))) Then
                Return True
            Else
                parentPageId = rows(0)(ApplicationConstants.GrantPageParentPageCol)
                Dim parentExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(parentPageId)
                Dim parentRow() As DataRow = pagesDt.Select(parentExpression)

                If (parentRow.Count > 0) Then
                    Return checkInPageLevelsAccessRights(pagesDt, parentPageId)
                Else
                    Return False
                End If

            End If

        End Function

#End Region

#Region "Get Servicing Types"
        Sub getServicingTypes(ByRef resultDataSet As DataSet)
            objDashboardDAL.getServicingTypes(resultDataSet)
        End Sub
#End Region

#Region "Load ME Dashboard Dropdowns"


        Sub populateMESchemeDropdown(ByRef resultDataSet As DataSet)
            objDashboardDAL.populateMESchemeDropdown(resultDataSet)
        End Sub

        Sub populateBlockDropdown(ByRef resultDataSet As DataSet, ByVal schemeId As Integer)
            objDashboardDAL.populateBlockDropdown(resultDataSet, schemeId)

        End Sub
#End Region

#Region " Get ME Dashboard Counts"


        Sub getMECounts(ByVal selectedMEScheme As Integer, ByVal selectedMEBlock As Integer, ByRef MECountHashtable As Hashtable)

            'Getting the count of the "No Entries" on ME Dashboard
            Dim rsNoEntry As DataSet = New DataSet()
            objDashboardDAL.getMECountNoEntry(selectedMEScheme, selectedMEBlock, rsNoEntry)
            MECountHashtable.Add(ApplicationConstants.MEHashTableKeyNoEntry, rsNoEntry.Tables(0).Rows(0).Item(0))

            'Getting the count of the "Overdue" on ME Dashboard
            Dim rsOverdue As DataSet = New DataSet()
            objDashboardDAL.getMECountOverDue(selectedMEScheme, selectedMEBlock, rsOverdue)
            MECountHashtable.Add(ApplicationConstants.MEHashTableKeyOverdue, rsOverdue.Tables(0).Rows(0).Item(0))

            'Getting the count of the "Appointment Arranged" on ME Dashboard
            Dim rsAppointmentArranged As DataSet = New DataSet()
            objDashboardDAL.getMECountAppointmentArranged(selectedMEScheme, selectedMEBlock, rsAppointmentArranged)
            MECountHashtable.Add(ApplicationConstants.MEHashTableKeyAppointmentArranged, rsAppointmentArranged.Tables(0).Rows(0).Item(0))

            'Getting the count of the "Appointment To Be Arranged" on ME Dashboard
            Dim rsAppointmentToBeArranged As DataSet = New DataSet()
            objDashboardDAL.getMECountAppointmentToBeArranged(selectedMEScheme, selectedMEBlock, rsAppointmentToBeArranged)
            MECountHashtable.Add(ApplicationConstants.MEHashTableKeyAppointmentToBeArranged, rsAppointmentToBeArranged.Tables(0).Rows(0).Item(0))
        End Sub
#End Region

        Function getServicingStatus(ByVal servicingTypeSelected As Integer, ByVal selectedScheme As Integer, ByVal selectedBlock As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet) As Integer
            Return objDashboardDAL.getServicingStatus(servicingTypeSelected, selectedScheme, selectedBlock, objPageSortBo, resultSet)
        End Function


    End Class

End Namespace