﻿Imports System.Web
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports AS_BusinessObject
Imports System.Data.SqlClient
Imports System.Globalization


Namespace AS_BusinessLogic

    Public Class JobSheetBL

#Region "Get Job Sheed Summary Detail By JSN(Job Sheed Number)"

        Sub getJobSheetSummaryByJsnAC(ByRef ds As DataSet, ByRef jsn As String)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.getJobSheetSummaryByJsnAC(ds, jsn)
        End Sub

#End Region
#Region "Get Job Sheed Summary Detail By JSNAC"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.getJobSheetSummaryByJsn(ds, jsn)
        End Sub

#End Region
#Region "Get Status Look Up For Job Sheet Summary Update"

        Sub getPlannedStatusLookUp(ByRef dsPlannedStatus As DataSet)
            Dim objJobSheetDAL As JobSheetDAL = New JobSheetDAL()
            objJobSheetDAL.getPlannedStatusLookUp(dsPlannedStatus)
        End Sub

#End Region
    End Class

End Namespace
