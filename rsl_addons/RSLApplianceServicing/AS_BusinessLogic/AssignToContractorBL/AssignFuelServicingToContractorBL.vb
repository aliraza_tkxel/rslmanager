﻿Imports AS_DataAccess

Namespace AS_BusinessLogic

End Namespace


Public Class AssignFuelServicingToContractorBL


#Region "Get Contractor Having Fuel Servicing Contract"

    Sub GetFeulServicingContractors(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        objAssignFuelServicingToContractorDAL.GetFeulServicingContractors(dropDownList)
    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByRef dropDownList As List(Of DropDownBO), ByRef ContractorId As Integer)
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        objAssignFuelServicingToContractorDAL.GetContactDropDownValuesbyContractorId(dropDownList, ContractorId)
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

    Sub getVatDropDownValues(ByRef vatBoList As List(Of ContractorVatBO))
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        objAssignFuelServicingToContractorDAL.GetVatDropDownValues(vatBoList)
    End Sub

    Sub getVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        objAssignFuelServicingToContractorDAL.GetVatDropDownValues(dropDownList)
    End Sub

#End Region

#Region "Assign Work to Contractor"

    Function assignToContractor(ByRef assignToContractorBo As AssignToContractorBO) As Boolean
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        Return objAssignFuelServicingToContractorDAL.assignToContractor(assignToContractorBo)
    End Function

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

    Sub SetExpenditureValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        objAssignFuelServicingToContractorDAL.SetExpenditureValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)
    End Sub

#End Region
   

#Region "Get Details for email"

    Sub getdetailsForEmail(ByRef assignToContractorBo As AssignToContractorBO, ByRef detailsForEmailDS As DataSet)
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL

        objAssignFuelServicingToContractorDAL.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
    End Sub

#End Region

#Region "get Budget Holder By OrderId"
    Sub getBudgetHolderByOrderId(ByRef resultDs As DataSet, ByRef orderId As Integer)
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL

        objAssignFuelServicingToContractorDAL.getBudgetHolderByOrderId(resultDs, orderId)
    End Sub
#End Region



#Region "Get Max cost to be entered For Assign Fuel Servicing Contrator"
    Public Function GetMaxCost(ByRef expenditureId As Integer)
        Dim objAssignFuelServicingToContractorDAL As New AssignFuelServicingToContractorDAL
        Return objAssignFuelServicingToContractorDAL.GetMaxCost(expenditureId)
    End Function
#End Region

End Class
