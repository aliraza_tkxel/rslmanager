﻿
Public Class PropertyAssignToContractorBL
    Private objAssignToContractor As PropertyAssignToContractorDAL


    Public Sub New()
        objAssignToContractor = New PropertyAssignToContractorDAL()
    End Sub

    Public Sub getContactEmailDetail(ByRef contactId As Integer, ByRef detailsForEmailDS As DataSet)
        objAssignToContractor.getContactEmailDetail(contactId, detailsForEmailDS)
    End Sub

    Public Sub GetCostCentreDropDownVales(ByRef dropDownList As List(Of DropDownBO))
        objAssignToContractor.GetCostCentreDropDownVales(dropDownList)
    End Sub

    Public Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef dropDownList As List(Of DropDownBO), ByRef CostCentreId As Integer)
        objAssignToContractor.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, CostCentreId)
    End Sub

    Public Sub GetExpenditureDropDownValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        objAssignToContractor.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)
    End Sub

    Public Sub GetMeServicingContractors(ByRef dropDownList As List(Of DropDownBO), ByVal objAssignToContractorBo As PropertyAssignToContractorBo)
        objAssignToContractor.GetMeContractors(dropDownList, objAssignToContractorBo)
    End Sub

    Public Sub GetContactDropDownValuesbyContractorId(ByRef dropDownList As List(Of DropDownBO), ByRef resultDataset As DataSet, ByVal objAssignToContractorBo As PropertyAssignToContractorBo)
        objAssignToContractor.GetContactDropDownValuesbyContractorId(dropDownList, resultDataset, objAssignToContractorBo)
    End Sub

    Public Sub GetContactDropDownValuesbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByVal contractorId As Integer)
        objAssignToContractor.GetContactDropDownValuesbyContractorId(dropDownBoList, contractorId)
    End Sub

    Public Sub getVatDropDownValues(ByRef vatBoList As List(Of VatBo))
        objAssignToContractor.GetVatDropDownValues(vatBoList)
    End Sub

    Public Sub getVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        objAssignToContractor.GetVatDropDownValues(dropDownList)
    End Sub

    Public Function assignToContractor(ByVal assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Return objAssignToContractor.assignToContractor(assignToContractorBo)
    End Function

    Public Function assignToContractorForPropertyPO(ByVal assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Return objAssignToContractor.assignToContractorForPropertyPO(assignToContractorBo)
    End Function

    Public Sub GetAreaDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        objAssignToContractor.GetAreaDropDownValues(dropDownList)
    End Sub

    Public Sub getdetailsForEmail(ByRef assignToContractorBo As PropertyAssignToContractorBo, ByRef detailsForEmailDS As DataSet)
        objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
    End Sub

    Public Sub getPropertyDetailsForEmail(ByRef assignToContractorBo As PropertyAssignToContractorBo, ByRef detailsForEmailDS As DataSet)
        objAssignToContractor.getPropertyDetailsForEmail(assignToContractorBo, detailsForEmailDS)
    End Sub

    Public Sub getAssignToContractorDetailByJournalId(ByRef resultDataSet As DataSet, ByVal journalId As Integer)
        objAssignToContractor.getAssignToContractorDetailByJournalId(resultDataSet, journalId)
    End Sub

    Public Sub getReactiveRepairContractors(ByRef dropDownList As List(Of DropDownBO))
        objAssignToContractor.getReactiveRepairContractors(dropDownList)
    End Sub

    Public Sub getAllContractors(ByRef dropDownList As List(Of DropDownBO))
        objAssignToContractor.getAllContractors(dropDownList)
    End Sub

    Public Function voidWorkAssignToContractor(ByRef assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Return objAssignToContractor.voidWorkAssignToContractor(assignToContractorBo)
    End Function

    Public Sub GetContactDropDownValuesAndDetailsbyContractorId(ByRef dropDownList As List(Of DropDownBO), ByVal objAssignToContractorBo As PropertyAssignToContractorBo)
        objAssignToContractor.GetContactDropDownValuesAndDetailsbyContractorId(dropDownList, objAssignToContractorBo)
    End Sub

    Public Function voidPaintPackAssignToContractor(ByRef assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Return objAssignToContractor.voidPaintPackAssignToContractor(assignToContractorBo)
    End Function

    Public Sub getBudgetHolderByOrderId(ByRef resultDs As DataSet, ByVal orderId As Integer, ByVal expenditureId As Integer)
        objAssignToContractor.getBudgetHolderByOrderId(resultDs, orderId, expenditureId)
    End Sub

    Public Sub GetAttributesSubItemsDropdownValuesByAreaId(ByRef dropDownList As List(Of DropDownBO), ByVal areaId As Integer)
        objAssignToContractor.GetAttributesSubItemsDropdownValuesByAreaId(dropDownList, areaId)
    End Sub

    Public Function GetItemMSATDetailByItemId(ByVal itemId As Integer, ByVal schemeId As Integer?, ByVal blockId As Integer?) As DataSet
        Return objAssignToContractor.GetItemMSATDetailByItemId(itemId, schemeId, blockId)
    End Function
End Class
