﻿
Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class ReportBL

#Region "Attributes"
        Dim objReportDAL As ReportDAL = New ReportDAL()
#End Region

#Region "Functions"

#Region "get Certificate Expiry Report Statistics"
        Public Function getCertificateExpiryReportStats(ByRef resultDataSet As DataSet, ByRef objReportBO As ReportBO, ByRef objPageSortBo As PageSortBO)
            Try
                Return objReportDAL.getCertificateExpiryReportStats(resultDataSet, objReportBO, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "get Certificate Expiry Report Information"
        Public Function getCertificateExpiryReportInfo(ByRef resultDataSet As DataSet, ByRef objReportBO As ReportBO, ByRef objPageSortBo As PageSortBO)
            Try
                Return objReportDAL.getCertificateExpiryReportInfo(resultDataSet, objReportBO, objPageSortBo)

            Catch ex As Exception

                Throw
            End Try
        End Function
#End Region

#Region "Get Property Type"
        Public Sub getPropertyType(ByRef resultDataSet As DataSet)
            Try
                objReportDAL.getPropertyType(resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region

#Region "Get Issued Certificate Report"

        Public Function getIssuedCertificateReport(ByRef objIssuedCertificateBO As IssuedCertificateBO, ByRef objPageSortBO As PageSortBO, ByRef resultDS As DataSet)
            Try
                Return objReportDAL.getIssuedCertificateReport(objIssuedCertificateBO, objPageSortBO, resultDS)
            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Get Print Certificate Report"

        Public Function getPrintCertificateReport(ByRef objPrintCertificateBO As IssuedCertificateBO, ByRef objPageSortBO As PageSortBO, ByRef resultDS As DataSet)
            Try
                Return objReportDAL.getPrintCertificateReport(objPrintCertificateBO, objPageSortBO, resultDS)
            Catch ex As Exception

                Throw
            End Try
        End Function

#End Region

#Region "Set CP12 Document as Printed"

        Public Sub setCP12DocumentPrinted(ByRef id As Integer)
            Try
                objReportDAL.setCP12DocumentPrinted(id)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Get Appliance Defect Report"

        Public Function getApplianceDefectReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getApplianceDefectReport(resultDataSet, objDefectReportBo, objPageSortBo)
        End Function
#End Region

#Region "Get Defect To Be Approved Report"

        Public Function getDefectToBeApprovedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getDefectToBeApprovedReport(resultDataSet, searchText, objPageSortBo)
        End Function
#End Region

#Region "Get Defect More Detail"
        ''' <summary>
        ''' Get Defect More Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getDefectMoreDetail(ByRef resultDataSet As DataSet, ByVal defectId As Integer, ByVal appointmentType As String)

            Dim objReportsDal As New ReportDAL
            objReportsDal.getDefectMoreDetail(resultDataSet, defectId, appointmentType)

        End Sub
#End Region

#Region "Get Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getDefecteasons(ByRef resultDataSet As DataSet)

            Dim reportDal As New ReportDAL()
            reportDal.getDefectReasons(resultDataSet)

        End Sub
#End Region

#Region "Get Defect  Approved Report"

        Public Function getDefectApprovedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getDefectApprovedReport(resultDataSet, searchText, objPageSortBo)
        End Function
#End Region

#Region "Get Defect Rejected Report"

        Public Function getDefectRejectedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getDefectRejectedReport(resultDataSet, searchText, objPageSortBo)
        End Function
#End Region

#Region "Change Defect Status"

        Public Function changeDefectStatus(ByRef cds As ChangeDefectStatus)
            Dim reportDal As New ReportDAL()
            Return reportDal.changeDefectStatus(cds)
        End Function
#End Region

#Region "Get Appliance Defect Report"

        Public Function getDisconnectedAppliances(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getDisconnectedAppliances(resultDataSet, objDefectReportBo, objPageSortBo)
        End Function
#End Region

#Region "Get Cancelled Defects Report"

        Public Function getCancelledDefectsReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getCancelledDefectsReport(resultDataSet, objDefectReportBo, objPageSortBo)
        End Function
#End Region

#Region "Get Defect History Report"

        Public Function getDefectHistoryReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)
            Dim reportDal As New ReportDAL()
            Return reportDal.getDefectHistoryReport(resultDataSet, objDefectReportBo, objPageSortBo)
        End Function
#End Region

#Region "Get Property Defect List"

        Public Sub getPropertyDefectList(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal year As Int32, ByVal defectType As String, ByVal applianceType As String)
            Dim reportDal As New ReportDAL()
            reportDal.getPropertyDefectList(resultDataSet, propertyId, year, defectType, applianceType)
        End Sub
#End Region

#Region "Get Scheme/Block Defect List"

        Public Sub getSchemeBlockDefectList(ByRef resultDataSet As DataSet, ByVal propertyId As Integer, ByVal reqType As String, ByVal year As Int32, ByVal defectType As String, ByVal applianceType As String)
            Dim reportDal As New ReportDAL()
            reportDal.getSchemeBlockDefectList(resultDataSet, propertyId, reqType, year, defectType, applianceType)
        End Sub
#End Region


#Region "Get Assigned to Contractor List"

        Function getAssignedToContractorReport(ByRef resultDataSet As DataSet, ByVal objPageSortBo As Object, ByVal searchText As String) As Integer
            Dim objReportsDal As New ReportDAL
            Return objReportsDal.getAssignedToContractorReport(resultDataSet, objPageSortBo, searchText)
        End Function

#End Region


#Region "Get Job Sheed Summary Detail By Journal Id"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As Integer)

            Dim objReportDAL As ReportDAL = New ReportDAL()
            objReportDAL.getJobSheetSummaryByJournalId(ds, jsn)
        End Sub

#End Region

        
#Region "Get Job Sheet Summary By Contractor Work Id"

        Sub getJobSheetSummaryForSchemeBlockByJournalId(ByRef ds As DataSet, ByRef journalId As Integer)

            Dim objReportDAL As ReportDAL = New ReportDAL()
            objReportDAL.getJobSheetSummaryForSchemeBlockByJournalId(ds, journalId)
        End Sub

#End Region

#Region "Save/Update Appointment Stauts in Database with Notes"

        Sub setAppointmentJobSheetStatusUpdate(ByRef objJobSheetBO As JobSheetBO)
            Dim objReportDAL As ReportDAL = New ReportDAL()
            objReportDAL.setAppointmentJobSheetStatusUpdate(objJobSheetBO)
        End Sub

#End Region

#End Region

    End Class
End Namespace

