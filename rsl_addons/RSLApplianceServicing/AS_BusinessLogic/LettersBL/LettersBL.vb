﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic

    Public Class LettersBL

#Region "Attributes"
        Dim objLettersDAL As LettersDAL = New LettersDAL()
        Dim objStatusDAL As StatusDAL = New StatusDAL()

#End Region

#Region "Functions"

#Region "Get Letters"
        Public Sub getLetters(ByRef resultDataSet As DataSet)
            Try
                objLettersDAL.getLetters(resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

        Public Sub getLettersForSearch(ByRef letterBO As LetterBO, ByRef resultDataSet As DataSet)
            Try
                objLettersDAL.getLettersForSearch(letterBO, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

        Public Sub getLetterById(ByVal letterID As Integer, ByRef resultDataSet As DataSet)
            Try
                objLettersDAL.getLetterById(letterID, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

        Public Function getSavedLetterById(ByVal letterId As Integer, ByRef resultSet As DataSet) As SavedLetterPDFBO
            Try
                objLettersDAL.getSavedLetterById(letterId, resultSet)

                Dim tenancyRefValue As String = resultSet.Tables(0).Rows(0).Item("TENANCY")
                Dim letterDateValue As Date = New Date()
                Dim tenantNameValue As String = resultSet.Tables(0).Rows(0).Item("TenantName")
                Dim houseNumberValue As String = resultSet.Tables(0).Rows(0).Item("HOUSENUMBER")
                Dim addressLine1Value As String = resultSet.Tables(0).Rows(0).Item("ADDRESS1")
                Dim addressLine2Value As String = resultSet.Tables(0).Rows(0).Item("ADDRESS2")
                Dim townCityValue As String = resultSet.Tables(0).Rows(0).Item("TOWNCITY")
                Dim countyValue As String = resultSet.Tables(0).Rows(0).Item("COUNTY")
                Dim postCodeValue As String = resultSet.Tables(0).Rows(0).Item("POSTCODE")
                Dim letterTitleValue As String = resultSet.Tables(0).Rows(0).Item("LETTERTITLE")
                Dim letterBodyValue As String = resultSet.Tables(0).Rows(0).Item("LETTERCONTENT")
                Dim signOffValue As String = resultSet.Tables(0).Rows(0).Item("CodeName")
                Dim fromResourceValue As String = resultSet.Tables(0).Rows(0).Item("EmployeeName")
                Dim teamValue As String = resultSet.Tables(0).Rows(0).Item("TEAMNAME")
                Dim fromEmailValue As String = resultSet.Tables(0).Rows(0).Item("WORKEMAIL")
                Dim fromDirectDialValue As String = resultSet.Tables(0).Rows(0).Item("WORKDD")
                Dim rentBalance As Double = resultSet.Tables(0).Rows(0).Item("RentBalance")
                Dim rentCharge As Double = resultSet.Tables(0).Rows(0).Item("RentCharge")
                Dim todayDate As Date = resultSet.Tables(0).Rows(0).Item("TodayDate")

                letterBodyValue = GeneralHelper.repalceRcRBTd(letterBodyValue, rentBalance, rentCharge, todayDate)

                'Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO("A3002320", New Date(), "Hussain Ali", "21", "Street 15", "Sector D, Askari X", "Lahore", "Punjab", "54400", "You have been awarded a new house", "Congratulations<br /> You have been given a new house. Congratulations !!", "Its been a real pleasure", "Altaf Hussain", "Askari X Management", "hussain@tkxel.com", "0332-8400486")
                Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO(tenancyRefValue, todayDate, tenantNameValue, houseNumberValue, addressLine1Value, addressLine2Value, townCityValue, countyValue, postCodeValue, letterTitleValue, letterBodyValue, signOffValue, fromResourceValue, teamValue, fromEmailValue, fromDirectDialValue)

                Return savedLetterPDFBO

            Catch ex As Exception

                Throw ex
            End Try
        End Function

#Region "get Customer And Property Rb Rc"
        Public Sub getCustomerAndPropertyRbRc(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef rentBalance As Double, ByRef rentCharge As Double)
            objLettersDAL.getCustomerAndPropertyRbRc(resultDataSet, propertyId, rentBalance, rentCharge)
        End Sub
#End Region
#End Region


#Region "Create new Letter Template"

#Region "Get Status"

        Public Sub getStatus(ByRef resultDataSet As DataSet)
            Try
                objStatusDAL.getStatus(resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#Region "Add Standard Letter Template"

        Public Sub addStandardLetterTemplate(ByRef letterBO As LetterBO, ByRef resultSet As DataSet)

            Try
                objLettersDAL.addStandardLetter(letterBO, resultSet)
            Catch ex As Exception

                Throw
            End Try

        End Sub

#End Region

#Region "update Standard Letter Template"

        Public Sub updateStandardLetterTemplate(ByRef letterBO As LetterBO, ByRef resultSet As DataSet)

            Try
                objLettersDAL.updateStandardLetter(letterBO, resultSet)
            Catch ex As Exception

                Throw
            End Try

        End Sub

#End Region

#Region "Delete Standard Letter Template"

        Public Sub deleteStandardLetter(ByVal letterID As Integer, ByRef resultDataSet As DataSet)
            Try
                objLettersDAL.deleteStandardLetter(letterID, resultDataSet)
            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region


#Region "Add Saved Letter"
        Public Sub addSavedLetter(ByRef savedLetterBO As SavedLetterBO, ByRef savedLetterOptionsBO As SavedLetterOptionsBO, ByRef resultSet As DataSet)
            Try
                ' first get the letter. 
                Dim letterResultSet As DataSet = New DataSet()
                objLettersDAL.getLetterById(savedLetterBO.LetterId, letterResultSet)

                Dim rawBodyText As String = letterResultSet.Tables(0).Rows(0).Item(1)

                savedLetterBO.LetterBody = rawBodyText

                Me.addCustomSavedLetter(savedLetterBO, savedLetterOptionsBO, resultSet)

            Catch ex As Exception

                Throw
            End Try

        End Sub

        Public Sub addCustomSavedLetter(ByRef savedLetterBO As SavedLetterBO, ByRef savedLetterOptionsBO As SavedLetterOptionsBO, ByRef resultSet As DataSet)
            Try
                ' modify the letter
                Dim rawBodyText As String = savedLetterBO.LetterBody

                If (Not savedLetterOptionsBO.RentCharge = -1) Then
                    rawBodyText = rawBodyText.Replace("[RC]", savedLetterOptionsBO.RentCharge.ToString)
                End If

                If (Not savedLetterOptionsBO.CurrentRentBalance = -1) Then
                    rawBodyText = rawBodyText.Replace("[RB]", savedLetterOptionsBO.CurrentRentBalance.ToString)
                End If

                If (IsNothing(savedLetterOptionsBO.TodaysDate)) Then
                    Dim dateString As String = savedLetterOptionsBO.TodaysDate.Day + " " + savedLetterOptionsBO.TodaysDate.Date.ToString + " " + savedLetterOptionsBO.TodaysDate.Month.ToString + ", " + savedLetterOptionsBO.TodaysDate.Year.ToString
                    rawBodyText = rawBodyText.Replace("[TD]", dateString)
                End If

                savedLetterBO.LetterBody = rawBodyText

                ' save the Saved Letter
                objLettersDAL.addSavedLetter(savedLetterBO, resultSet)

            Catch ex As Exception

                Throw
            End Try

        End Sub

#End Region

#End Region


#Region "Get SignOFF Types, Teams and From"
        Public Sub getSignOffTypes(ByRef signOffList As List(Of DropDownBO))
            Try
                objLettersDAL.getSignOffTypes(signOffList)
            Catch ex As Exception

                Throw
            End Try
        End Sub


        Public Sub getTeams(ByRef teamList As List(Of DropDownBO))
            Try
                objLettersDAL.getTeams(teamList)
            Catch ex As Exception

                Throw
            End Try
        End Sub

        Public Sub getFromUserByTeamId(ByVal teamID As Integer, ByRef userList As List(Of DropDownBO))

            Try
                objLettersDAL.getFromUserByTeamID(teamID, userList)
            Catch ex As Exception

                Throw
            End Try
        End Sub
#End Region


#Region "Employee(From) Direct Dial and Email Address By Employee ID (Print Letter)"

        Sub getFromTelEmailPrintLetterByEmployeeID(ByRef printLetterBo As PrintLetterBO)
            objLettersDAL.getFromTelEmailPrintLetterByEmployeeID(printLetterBo)
        End Sub

#End Region

#End Region

        

    End Class

End Namespace
