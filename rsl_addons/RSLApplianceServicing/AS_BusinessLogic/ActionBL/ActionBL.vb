﻿Imports System
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Namespace AS_BusinessLogic

    Public Class ActionBL
#Region "Attributes"
        Dim objActionDAL As ActionDAL = New ActionDAL()
#End Region

#Region "Functions"

#Region "Get Action by Status ID"
        
        Public Sub getActionByStatusId(ByVal statusId As Integer, ByRef statusList As List(Of ActionBO))
            Try

                objActionDAL.getActionByStatusId(statusId, statusList)

            Catch ex As Exception

                Throw
            End Try
        End Sub

#End Region

#End Region

    End Class
End Namespace

