﻿
Public Class MaintenanceBL


    Private objMaintenance As MaintenanceDAL

    Public Sub New()
        objMaintenance = New MaintenanceDAL
    End Sub

    Public Sub getMaintenanceTypes(ByRef resultDataSet As DataSet)
        objMaintenance.getMaintenanceTypes(resultDataSet)
    End Sub

    Public Sub getBlocksBySchemeId(ByVal schemeId As Integer, ByRef resultDataSet As DataSet)
        objMaintenance.getBlocksBySchemeId(schemeId, resultDataSet)
    End Sub

    Public Sub getPropertiesBySchemeId(ByVal schemeId As Integer, ByRef resultDataSet As DataSet)
        objMaintenance.getPropertiesBySchemeId(schemeId, resultDataSet)
    End Sub

    Public Sub getPropertiesByBlockId(ByVal blockId As Integer, ByRef resultDataSet As DataSet)
        objMaintenance.getPropertiesByBlockId(blockId, resultDataSet)
    End Sub

End Class
