﻿Imports System.Web
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_DataAccess


Namespace AS_BusinessLogic


    Public Class ResourcesBL
        Dim objResourcesDAL As ResourceDAL = New ResourceDAL()

#Region "Get Actions By Status Id"
        Public Sub getLettersByStatusId(ByVal statusId As Int32, ByRef resultDataSet As DataSet)
            objResourcesDAL.getLettersByStatusId(statusId, resultDataSet)
        End Sub
#End Region

#Region "Get Actions By Action Id"
        Public Sub getLettersByActionId(ByRef actionId As Int32, ByRef resultDataSet As DataSet)
            objResourcesDAL.getLettersByActionId(actionId, resultDataSet)
        End Sub
#End Region

#Region "Get Alternative Actions By Action Id"
        Public Sub GetAlternativeLettersByActionId(ByRef actionId As Int32, ByRef resultDataSet As DataSet)
            objResourcesDAL.GetAlternativeLettersByActionId(actionId, resultDataSet)
        End Sub
#End Region

#Region "Edit Status"
        Public Sub editStatus(ByVal statusId As Integer, ByVal inspectionType As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer, ByVal modifiedBy As Integer)
            objResourcesDAL.editStatus(statusId, inspectionType, statusTitle, selectedRanking, modifiedBy)

        End Sub
#End Region

#Region "Add Action"
        Public Sub AddAction(ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal CreatedBy As Integer)
            objResourcesDAL.AddAction(StatusId, Title, ranking, CreatedBy)
        End Sub
#End Region

#Region "Edit Action"
        Sub editAction(ByVal ActionId As Integer, ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal modifiedBy As Integer)
            objResourcesDAL.editAction(ActionId, StatusId, Title, ranking, modifiedBy)
        End Sub

#End Region

#Region "Access Right"


        Sub getPages(ByVal resultDataSet As DataSet)
            objResourcesDAL.getPages(resultDataSet)
        End Sub

        Sub getPagesbyParentId(ByVal ParentId As Integer, ByVal resultDataSet As DataSet)
            objResourcesDAL.getPagesbyParentId(ParentId, resultDataSet)
        End Sub

        Sub saveUserAccessRight(ByVal EmployeeId As Integer, ByVal CheckedNodesList As List(Of Integer))
            objResourcesDAL.deleteUserPages(EmployeeId)
            For Each pageId As Integer In CheckedNodesList
                objResourcesDAL.saveUserAccessRight(EmployeeId, pageId)
            Next

        End Sub

        Sub GetPageByEmployeeId(ByVal EmployeeId As Integer, ByVal UserPageDataSet As DataSet)
            objResourcesDAL.GetPageByEmployeeId(EmployeeId, UserPageDataSet)
        End Sub

#End Region

#Region "Core And Out Of Hours Info"
        ''' <summary>
        ''' Core And Out Of Hours Info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Sub getCoreAndOutOfHoursInfo(ByRef resultDataSet As DataSet, ByVal employeeId As Integer)

            objResourcesDAL.getCoreAndOutOfHoursInfo(resultDataSet, employeeId)

        End Sub

#End Region

#Region "Save Out Of Hours Info"
        ''' <summary>
        ''' Save Out Of Hours Info
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <param name="newOutOfHoursDt"></param>
        ''' <param name="deletedOutOfHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function saveOutOfHoursInfo(ByVal employeeId As Integer, ByVal editedBy As Integer, ByVal newOutOfHoursDt As DataTable, ByVal deletedOutOfHoursDt As DataTable)

            Return objResourcesDAL.saveOutOfHoursInfo(employeeId, editedBy, newOutOfHoursDt, deletedOutOfHoursDt)

        End Function
#End Region

#Region "Save Core Working Hours Info"
        ''' <summary>
        ''' Save Core Working Hours Info
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <param name="editedBy"></param>
        ''' <param name="coreWorkingHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function saveCoreWorkingHoursInfo(ByVal employeeId As Integer, ByVal editedBy As Integer, ByVal coreWorkingHoursDt As DataTable)
            Return objResourcesDAL.saveCoreWorkingHoursInfo(employeeId, editedBy, coreWorkingHoursDt)
        End Function

#End Region
    

    End Class

End Namespace