﻿Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AjaxControlToolkit
Imports System.Globalization

Namespace AS_BusinessLogic
    Public Class DatesControlBL
        Implements IControlBL

#Region "Functions"
#Region "Create controls"
        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            'Dim loadParameterDDL As DataTable = New DataTable()
            Dim txtDate As TextBox = New TextBox()
            'Set control styles
            txtDate.ID = ApplicationConstants.txtDate + parameterId.ToString()
            txtDate.CssClass = ApplicationConstants.textBoxStyle
            'get dataset from session
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            ' filter the Parameters datatable w.r.t parameterID
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)
            'Populate control with existing inserted values
            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                populateHeatingControl(parameterId, txtDate, heatingParamId, itemParamId)
            Else
                populateControl(parameterId, txtDate)
            End If
            Dim lblDate As Label = New Label()
            Dim tr As TableRow = New TableRow()
            tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblDate.ID = ApplicationConstants.lblDate + parameterId.ToString()
            lblDate.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            ''Add Calendar Extender to date control
            Dim calendarExtender As CalendarExtender = New CalendarExtender()
            calendarExtender.TargetControlID = txtDate.ID
            calendarExtender.ID = ApplicationConstants.extender + parameterId.ToString()
            calendarExtender.Format = "dd/MM/yyyy"
            'txtDate.ReadOnly = True
            td1.Controls.Add(lblDate)
            td2.Controls.Add(txtDate)
            td2.Controls.Add(calendarExtender)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region

#Region " Populate Controls"
        Public Sub populateControl(ByVal parameterId As Integer, ByRef control As Web.UI.Control) Implements IControlBL.populateControl
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId Select ps)
            Dim itemParamId As Integer = attributeResult.First.Item("ItemParamID")
            Dim isEventAdded As Boolean = False
            If attributeResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                Dim replacementResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() Select ps)
                If replacementResult.Count() > 0 Then
                    isEventAdded = True
                End If

                If attributeResult.First.Item("ParameterName").ToString().Contains("Due") Then

                    If attributeResult.First.Item("ParameterName").ToString().Contains("Rewire Due") Then
                        If replacementResult.Count() > 0 Then
                            txtParameter.Enabled = False
                        End If
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString()
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "Last Rewired" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("DueDate")
                            End If

                        End If

                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Cur Due") Then
                        If replacementResult.Count() > 0 Then
                            txtParameter.Enabled = False
                        End If
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.curDue.ToString()
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "CUR Last Replaced" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("DueDate")
                            End If

                        End If

                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Upgrade Due") Then
                        If replacementResult.Count() > 0 Then
                            txtParameter.Enabled = False
                        End If
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString()
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "Electrical Upgrade Last Done" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("DueDate")
                            End If

                        End If

                    ElseIf attributeResult.First.Item("ParameterName").ToString().ToUpper() = "Replacement Due".ToUpper() Then
                        If replacementResult.Count() > 0 Then
                            txtParameter.Enabled = False
                        End If
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID") Is DBNull.Value Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("DueDate")
                            End If

                        End If

                    Else
                        Dim ReplacementDueResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID").ToString() = parameterId.ToString() Select ps)
                        If ReplacementDueResult.Count > 0 Then
                            If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                                txtParameter.Text = ReplacementDueResult.First.Item("DueDate")
                            End If

                        End If
                    End If
                Else
                    If attributeResult.First.Item("ParameterName").ToString().Contains("Last Rewired") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString()
                        AddHandler txtParameter.TextChanged, AddressOf componentParameter_TextChanged
                        txtParameter.AutoPostBack = True
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "Last Rewired" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("LastDone")
                            End If
                        End If
                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("CUR Last Replaced") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.curLastReplaced.ToString()
                        AddHandler txtParameter.TextChanged, AddressOf componentParameter_TextChanged
                        txtParameter.AutoPostBack = True
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "CUR Last Replaced" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("LastDone")
                            End If
                        End If
                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Upgrade Last Done") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString()
                        AddHandler txtParameter.TextChanged, AddressOf componentParameter_TextChanged
                        txtParameter.AutoPostBack = True
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName") = "Electrical Upgrade Last Done" Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("LastDone")
                            End If
                        End If
                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Original Install Date") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString()
                        AddHandler txtParameter.TextChanged, AddressOf parameter_TextChanged
                        txtParameter.AutoPostBack = True
                        Dim result = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName").ToString() = "Original Install Date" Select ps)
                        If result.Count > 0 Then
                            If Not IsDBNull(result.First.Item("DueDate")) Then
                                txtParameter.Text = result.First.Item("DueDate")
                            End If
                        End If
                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Gasket Set Replacement") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()

                        Dim result = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName").ToString() = "Gasket Set Replacement" Select ps)
                        If result.Count > 0 Then
                            If Not IsDBNull(result.First.Item("DueDate")) Then
                                txtParameter.Text = result.First.Item("DueDate")
                            End If
                        End If

                    ElseIf attributeResult.First.Item("ParameterName").ToString().ToUpper() = "Last Replaced".ToUpper() Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()
                        If isEventAdded Then
                            AddHandler txtParameter.TextChanged, AddressOf componentParameter_TextChanged
                            txtParameter.AutoPostBack = True
                        End If
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID") Is DBNull.Value Select ps)
                        If ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("LastDone")
                            End If
                        End If
                    Else
                        Dim ReplacementDueResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID").ToString() = parameterId.ToString() Select ps)
                        If ReplacementDueResult.Count > 0 Then
                            If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                                txtParameter.Text = ReplacementDueResult.First.Item("DueDate")
                            End If

                        End If
                    End If
                End If


            End If
        End Sub
#End Region

#Region "Component txtDate text changed event"
        ''' <summary>
        ''' txtDate text changed event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub componentParameter_TextChanged(sender As Object, e As EventArgs)
            Dim txt As TextBox = DirectCast(sender, TextBox)
            Dim txtComtrolId = txt.ID
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim ItemLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
            ' if component is mapped with item only
            If ItemLifeCycleResult.Count() > 0 Then
                If txtComtrolId = ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString() Then
                    Dim txtReplacementDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                    Dim hdnLifeCycle As HiddenField = CType(sender.FindControl(ApplicationConstants.hdnLifeCycle + SessionManager.getTreeItemId().ToString()), HiddenField)
                    txtReplacementDue.Text = Convert.ToDateTime(txt.Text).AddMonths(hdnLifeCycle.Value).ToShortDateString()
                End If
                'if component is mapped with item and parameter i.e Electric item
            ElseIf SessionManager.getAttributrTypeId() = 2 Then
                If txtComtrolId = ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString() Then
                    Dim txtRewiredDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString()), TextBox)
                    Dim hdnLifeCycle As HiddenField = CType(sender.FindControl(ApplicationConstants.hdnLifeCycle + ApplicationConstants.rewiredDue.ToString()), HiddenField)
                    Dim lastRewiredDate As Date
                    If txt.Text <> String.Empty AndAlso Date.TryParseExact(txt.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, lastRewiredDate) Then
                        txtRewiredDue.Text = lastRewiredDate.AddMonths(hdnLifeCycle.Value).ToShortDateString()
                    Else
                        txtRewiredDue.Text = String.Empty
                    End If
                ElseIf txtComtrolId = ApplicationConstants.txtDate + ApplicationConstants.curLastReplaced.ToString() Then
                    Dim txtCurDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curDue.ToString()), TextBox)
                    Dim hdnLifeCycle As HiddenField = CType(sender.FindControl(ApplicationConstants.hdnLifeCycle + ApplicationConstants.curDue.ToString()), HiddenField)
                    Dim curLastReplacedDate As Date
                    If txt.Text <> String.Empty AndAlso Date.TryParseExact(txt.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, curLastReplacedDate) Then
                        txtCurDue.Text = curLastReplacedDate.AddMonths(hdnLifeCycle.Value).ToShortDateString()
                    Else
                        txtCurDue.Text = String.Empty
                    End If
                ElseIf txtComtrolId = ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString() Then
                    Dim txtUpgradeDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString()), TextBox)
                    Dim hdnLifeCycle As HiddenField = CType(sender.FindControl(ApplicationConstants.hdnLifeCycle + ApplicationConstants.upgradeDue.ToString()), HiddenField)
                    Dim lastUpgradedDate As Date
                    If txt.Text <> String.Empty AndAlso Date.TryParseExact(txt.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, lastUpgradedDate) Then
                        txtUpgradeDue.Text = lastUpgradedDate.AddMonths(hdnLifeCycle.Value).ToShortDateString()
                    Else
                        txtUpgradeDue.Text = String.Empty
                    End If
                End If
                'if component is mapped with item,parameter,parametervalue,subparameter and subparametervalue i'e Heating and water supply
            ElseIf SessionManager.getAttributrTypeId() = 3 Then
                Dim txtReplacementDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                If SessionManager.getComponentCycle() > 0 Then
                    Dim txtLastReplaced As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                    If txtLastReplaced.Text <> "" Then
                        txtReplacementDue.Text = Convert.ToDateTime(txtLastReplaced.Text).AddMonths(SessionManager.getComponentCycle()).ToShortDateString()
                        txtReplacementDue.Enabled = False
                    End If
                Else
                    txtReplacementDue.Enabled = True
                End If
            End If

        End Sub
#End Region

#Region "txtDate text changed event"
        ''' <summary>
        ''' txtDate text changed event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub parameter_TextChanged(sender As Object, e As EventArgs)
            Dim txt As TextBox = DirectCast(sender, TextBox)
            Dim txtComtrolId = txt.ID

            If txtComtrolId = ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString() Then
                Dim txtGasketSetReplacement As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()), TextBox)
                If txtGasketSetReplacement IsNot Nothing Then

                    Dim gasketSetReplacementDate As Date
                    If txt.Text <> String.Empty AndAlso Date.TryParseExact(txt.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, gasketSetReplacementDate) Then
                        txtGasketSetReplacement.Text = gasketSetReplacementDate.AddYears(ApplicationConstants.GasketSetReplacementDefaultYears).ToShortDateString()
                    Else
                        txtGasketSetReplacement.Text = String.Empty
                    End If
                End If
            End If

        End Sub
#End Region


#Region " Populate HeatingControls"
        Public Sub populateHeatingControl(ByVal parameterId As Integer, ByRef control As Web.UI.Control, ByVal heatingMappingId As Integer, ByVal itemParamId As Integer)
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId AndAlso ps.Item("ItemParamID").ToString() = itemParamId Select ps)
            'Dim itemParamId As Integer = attributeResult.First.Item("ItemParamID")
            Dim isEventAdded As Boolean = False
            If attributeResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                Dim replacementResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() Select ps)
                If replacementResult.Count() > 0 Then
                    isEventAdded = True
                End If

                If attributeResult.First.Item("ParameterName").ToString().Contains("Due") Then

                    If attributeResult.First.Item("ParameterName").ToString().ToUpper() = "Replacement Due".ToUpper() Then
                        If replacementResult.Count() > 0 Then
                            txtParameter.Enabled = False
                        End If
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID") Is DBNull.Value AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("DueDate")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("DueDate")
                            End If

                        End If

                    Else
                        Dim ReplacementDueResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID").ToString() = parameterId.ToString() AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso ReplacementDueResult.Count > 0 Then
                            If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                                txtParameter.Text = ReplacementDueResult.First.Item("DueDate")
                            End If

                        End If
                    End If
                Else
                    If attributeResult.First.Item("ParameterName").ToString().Contains("Original Install Date") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString()
                        AddHandler txtParameter.TextChanged, AddressOf parameter_TextChanged
                        txtParameter.AutoPostBack = True
                        Dim result = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName").ToString() = "Original Install Date" AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso result.Count > 0 Then
                            If Not IsDBNull(result.First.Item("DueDate")) Then
                                txtParameter.Text = result.First.Item("DueDate")
                            End If
                        End If
                    ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Gasket Set Replacement") Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()

                        Dim result = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() AndAlso Not IsDBNull(ps.Item("ParameterName")) AndAlso ps.Item("ParameterName").ToString() = "Gasket Set Replacement" AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso result.Count > 0 Then
                            If Not IsDBNull(result.First.Item("DueDate")) Then
                                txtParameter.Text = result.First.Item("DueDate")
                            End If
                        End If

                    ElseIf attributeResult.First.Item("ParameterName").ToString().ToUpper() = "Last Replaced".ToUpper() Then
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()
                        If isEventAdded Then
                            AddHandler txtParameter.TextChanged, AddressOf componentParameter_TextChanged
                            txtParameter.AutoPostBack = True
                        End If
                        Dim ItemDatesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID") Is DBNull.Value AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso ItemDatesResult.Count > 0 Then
                            If Not IsDBNull(ItemDatesResult.First.Item("LastDone")) Then
                                txtParameter.Text = ItemDatesResult.First.Item("LastDone")
                            End If
                        End If
                    Else
                        Dim ReplacementDueResult = (From ps In parameterDataSet.Tables(ApplicationConstants.lastReplaced) Where ps.Item("ItemID").ToString() = SessionManager.getTreeItemId() And ps.Item("ParameterID").ToString() = parameterId.ToString() AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                        If heatingMappingId > 0 AndAlso ReplacementDueResult.Count > 0 Then
                            If Not IsDBNull(ReplacementDueResult.First.Item("DueDate")) Then
                                txtParameter.Text = ReplacementDueResult.First.Item("DueDate")
                            End If

                        End If
                    End If
                End If


            End If
        End Sub
#End Region
#End Region
    End Class
End Namespace
