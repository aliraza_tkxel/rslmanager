﻿Imports AS_Utilities
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Web

Namespace AS_BusinessLogic
    Public Class RadioButtonControlBL
        Implements IControlBL

#Region "Functions"
#Region "Create controls"
        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            Dim loadParameterRdbtn As DataTable = New DataTable()
            Dim rdBtn As RadioButtonList = New RadioButtonList()
            'Load Parameter values data table
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterRdbtn = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            'convert Parameter values data table into data view            
            Dim loadParameterDv As DataView = New DataView()
            loadParameterDv = loadParameterRdbtn.AsDataView()
            'Filter the Parameters data table w.r.t parameterId
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)



            If attributeResult.First.Item("ParameterName").ToString() = "Condition Rating" Or
                attributeResult.First.Item("ParameterName").ToString() = "Boiler Condition Rating" Then
                rdBtn.CssClass = ApplicationConstants.dynamicRadioButtonBoilerConditionRating
            Else
                rdBtn.CssClass = ApplicationConstants.dynamicRadioButton
            End If


            rdBtn.RepeatDirection = RepeatDirection.Horizontal
            'set control styles
            rdBtn.ID = ApplicationConstants.rdBtn + parameterId.ToString()

            'rdBtn.AutoPostBack = True
            'AddHandler rdBtn.SelectedIndexChanged, AddressOf parameter_SelectedIndexChanged


            'Filter the Parameter values which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString()
            loadParameterRdbtn = loadParameterDv.ToTable()
            'Bind the data source
            rdBtn.DataSource = loadParameterRdbtn
            rdBtn.DataValueField = "ValueID"
            rdBtn.DataTextField = "ValueDetail"
            rdBtn.DataBind()
            'Populate the check box list with existing inserted values
            populateControl(attributeResult.First.Item("ItemParamID"), rdBtn)
            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                If heatingParamId > 0 Then
                    populateHeatingControl(attributeResult.First.Item("ItemParamID"), rdBtn, heatingParamId)
                End If

            Else
                populateControl(attributeResult.First.Item("ItemParamID"), rdBtn)
            End If
            Dim worksRequiredId = ApplicationConstants.Tr + "_WorksRequired_" + rdBtn.ClientID
            SessionManager.setWorksRequiredRowId(worksRequiredId)
            For Each item As ListItem In rdBtn.Items
                If item.Text = ApplicationConstants.conditionUnsatisfactory Or item.Text = ApplicationConstants.conditionPotentiallyUnsatisfactory Then
                    item.Attributes.Add("onclick", "ShowWorksRequired(this,'" + worksRequiredId + "');")
                Else
                    item.Attributes.Add("onclick", "HideWorksRequired(this,'" + worksRequiredId + "');")

                End If
            Next
            Dim lblRadioBtn As Label = New Label()
            Dim tr As TableRow = New TableRow()
            tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            lblRadioBtn.ID = ApplicationConstants.lblRadioBtn + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblRadioBtn.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            td1.Controls.Add(lblRadioBtn)
            td2.Controls.Add(rdBtn)
            td2.Attributes.Add("colspan", "2")
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region
#Region " Populate Controls"
        Public Sub populateControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control) Implements IControlBL.populateControl
            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim chklist As RadioButtonList = CType(control, RadioButtonList)

            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)
            If preInsertedResult.Count > 0 Then
                For Each lst As DataRow In preInsertedResult
                    For Each item As ListItem In chklist.Items
                        If item.Value = lst.Item("VALUEID") Then
                            item.Selected = lst.Item("VALUEID")

                            If item.Text = ApplicationConstants.conditionUnsatisfactory Or item.Text = ApplicationConstants.conditionPotentiallyUnsatisfactory Then

                                SessionManager.setIsWorkSatisfactory(False)
                            Else
                                SessionManager.setIsWorkSatisfactory(True)
                            End If
                        End If
                    Next
                Next
            Else
                SessionManager.setIsWorkSatisfactory(True)
            End If
        End Sub
#End Region


#Region " Populate Controls"
        Public Sub populateHeatingControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control, ByVal heatingMappingId As Integer)
            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim chklist As RadioButtonList = CType(control, RadioButtonList)

            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
            If preInsertedResult.Count > 0 Then
                For Each lst As DataRow In preInsertedResult
                    For Each item As ListItem In chklist.Items
                        If item.Value = lst.Item("VALUEID") Then
                            item.Selected = lst.Item("VALUEID")

                            If item.Text = ApplicationConstants.conditionUnsatisfactory Or item.Text = ApplicationConstants.conditionPotentiallyUnsatisfactory Then

                                SessionManager.setIsWorkSatisfactory(False)
                            Else
                                SessionManager.setIsWorkSatisfactory(True)
                            End If
                        End If
                    Next
                Next
            Else
                SessionManager.setIsWorkSatisfactory(True)
            End If
        End Sub
#End Region
#End Region
    End Class
End Namespace