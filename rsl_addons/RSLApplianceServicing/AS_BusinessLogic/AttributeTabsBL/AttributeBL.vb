﻿Public Class AttributeBL

End Class


Public Class AttributesBL


    Private objAttrRpo As AttributesDAL

    Public Sub New()
        objAttrRpo = New AttributesDAL()
    End Sub

    Public Function getItemDetail(ByVal itemId As Integer, ByVal propertyId As String, Optional ByVal childAttributeMappingId As Integer? = Nothing) As DataSet
        Return objAttrRpo.getItemDetail(itemId, propertyId, childAttributeMappingId)
    End Function


End Class
