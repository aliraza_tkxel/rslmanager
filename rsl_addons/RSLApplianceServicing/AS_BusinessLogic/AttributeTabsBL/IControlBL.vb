﻿Imports System.Web.UI.WebControls
Imports System.Web.UI

Namespace AS_BusinessLogic
    Public Interface IControlBL
        Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow
        Sub populateControl(ByVal itemParamId As Integer, ByRef control As Control)

    End Interface
End Namespace
