﻿Imports System.Web.UI.WebControls
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class ControlFactoryBL

#Region "create dynamic control"
        Public Shared Function createDynaicControls(ByRef parameterDataSet As DataSet) As Table
            Dim tblOtherDetails As Table = New Table()
            SessionManager.setIsAccountingMapped(0)
            SessionManager.setIsLifeCycleMapped(0)

            Dim controlType As String
            Dim parameterId As Integer
            Dim showInApp As Boolean
            Dim parameterName As String
            Dim itemParamId As Integer
            Dim showInAppIndex As Integer = -1
            Dim notShowInApp As Integer = 0
            If parameterDataSet.Tables("Parameters").Rows.Count > 0 Then
                For Each dr As DataRow In parameterDataSet.Tables(ApplicationConstants.parameters).Rows
                    controlType = dr.Item("ControlType")
                    parameterId = dr.Item("ParameterID")
                    showInApp = dr.Item("ShowInApp")
                    parameterName = dr.Item("ParameterName")
                    itemParamId = dr.Item("ItemParamId")
                    If showInApp = False Then
                        showInAppIndex = showInAppIndex + 1
                    Else
                        notShowInApp = notShowInApp + 1
                    End If
                    If controlType.ToUpper() = "Dropdown".ToUpper() Then
                        Dim lifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("VALUEID") IsNot DBNull.Value Or ps.Item("SubParameter").ToString() = parameterId.ToString() Select ps)
                        If lifeCycleResult.Count() > 0 Then
                            SessionManager.setValueLifeCycle(True)
                            SessionManager.setIsDdlChangeEvent(True)
                            SessionManager.setAttributrTypeId(3)
                        Else
                            SessionManager.setValueLifeCycle(False)
                            SessionManager.setIsDdlChangeEvent(False)
                        End If
                        Dim tr As TableRow = New TableRow()
                        Dim dropDownBlObj As DropDownControlBL = New DropDownControlBL()
                        If showInApp = False AndAlso notShowInApp > 0 Then
                            Dim trShowInApp As TableRow = tblOtherDetails.Rows(showInAppIndex)
                            Dim td3 As TableCell = New TableCell()
                            Dim tblShowInApp As Table = New Table()
                            Dim trInnerShowInApp As TableRow = New TableRow()
                            trInnerShowInApp = dropDownBlObj.createControl(parameterId, itemParamId)
                            trInnerShowInApp.Style.Add("display", "table-row")
                            tblShowInApp.Rows.Add(trInnerShowInApp)
                            td3.Controls.Add(tblShowInApp)
                            trShowInApp.Cells.Add(td3)
                            trShowInApp.Style.Add("display", "table-row")

                        Else
                            tr = dropDownBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)
                        End If

                        If lifeCycleResult.Count() > 0 Then
                            Dim tblrow As TableRow = New TableRow()
                            tblrow = dropDownBlObj.calculateValueBaseLifeCycle(parameterId)
                            tblOtherDetails.Rows.Add(tblrow)

                            Dim tblDataRow As TableRow = New TableRow()
                            tblDataRow = dropDownBlObj.createComponentAccountingControl(parameterId)
                            tblOtherDetails.Rows.Add(tblDataRow)
                        End If

                    ElseIf controlType.ToUpper() = "Date".ToUpper() Then
                        Dim tr As TableRow = New TableRow()
                        Dim datesBlObj As DatesControlBL = New DatesControlBL()
                        tr = datesBlObj.createControl(parameterId, itemParamId)
                        tblOtherDetails.Rows.Add(tr)

                    ElseIf controlType.ToUpper() = "Textbox".ToUpper() Then

                        Dim tr As TableRow = New TableRow()
                        Dim textBoxBlObj As TextBoxControlBL = New TextBoxControlBL()
                        If showInApp = False AndAlso notShowInApp > 0 Then
                            Dim trShowInApp As TableRow = tblOtherDetails.Rows(showInAppIndex)
                            Dim td3 As TableCell = New TableCell()
                            Dim tblShowInApp As Table = New Table()
                            Dim trInnerShowInApp As TableRow = New TableRow()
                            trInnerShowInApp = textBoxBlObj.createControl(parameterId, itemParamId)
                            trInnerShowInApp.Style.Add("display", "table-row")
                            tblShowInApp.Rows.Add(trInnerShowInApp)
                            td3.Controls.Add(tblShowInApp)
                            trShowInApp.Cells.Add(td3)
                            trShowInApp.Style.Add("display", "table-row")
                        Else
                            tr = textBoxBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)
                        End If

                    ElseIf controlType.ToUpper() = "TextArea".ToUpper() Then

                        Dim tr As TableRow = New TableRow()
                        Dim textBoxBlObj As TextAreaControlBL = New TextAreaControlBL()
                        If parameterName = "Works Required" Then
                            tr = textBoxBlObj.createWorksRequiredControl(parameterId)
                        Else
                            tr = textBoxBlObj.createControl(parameterId, itemParamId)
                        End If

                        tblOtherDetails.Rows.Add(tr)
                    ElseIf controlType.ToUpper() = "Checkboxes".ToUpper() Then

                        If (dr("ParameterName").ToString().ToUpper = ApplicationConstants.landlordAppliance.ToUpper) Then
                            Dim cp12Issued As TableRow
                            Dim cp12Nubmer As TableRow
                            Dim cp12Renewal As TableRow
                            If (parameterDataSet.Tables(ApplicationConstants.cP12Information).Rows.Count > 0) Then
                                With parameterDataSet.Tables(ApplicationConstants.cP12Information).Rows(0)
                                    cp12Issued = createLabelRowByTitleValue(ApplicationConstants.cp12Issued, .Item(ApplicationConstants.cp12Issued))
                                    cp12Nubmer = createLabelRowByTitleValue(ApplicationConstants.cp12Number, .Item(ApplicationConstants.cp12Number))
                                    cp12Renewal = createLabelRowByTitleValue(ApplicationConstants.cp12Renewal, .Item(ApplicationConstants.cp12Renewal))
                                End With
                            Else
                                cp12Issued = createLabelRowByTitleValue(ApplicationConstants.cp12Issued)
                                cp12Nubmer = createLabelRowByTitleValue(ApplicationConstants.cp12Number)
                                cp12Renewal = createLabelRowByTitleValue(ApplicationConstants.cp12Renewal)
                            End If

                            tblOtherDetails.Rows.Add(cp12Issued)
                            tblOtherDetails.Rows.Add(cp12Nubmer)
                            tblOtherDetails.Rows.Add(cp12Renewal)
                        End If

                        Dim tr As TableRow = New TableRow()
                        Dim checkBoxBlObj As CheckBoxControlBL = New CheckBoxControlBL()
                        tr = checkBoxBlObj.createControl(parameterId, itemParamId)
                        tblOtherDetails.Rows.Add(tr)
                    ElseIf controlType.ToUpper() = "Radiobutton".ToUpper() Then

                        Dim tr As TableRow = New TableRow()
                        Dim radioButtonBlObj As RadioButtonControlBL = New RadioButtonControlBL()
                        tr = radioButtonBlObj.createControl(parameterId, itemParamId)
                        tblOtherDetails.Rows.Add(tr)

                    End If

                    Dim parameterLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("PARAMETERID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                    If parameterLifeCycleResult.Count() > 0 Then
                        SessionManager.setAttributrTypeId(2)
                        Dim tblrow As TableRow = New TableRow()
                        tblrow = calculateParamBaseLifeCycle(parameterId)
                        tblOtherDetails.Rows.Add(tblrow)

                        Dim tblDataRow As TableRow = New TableRow()
                        tblDataRow = createAccountingControlParameter(parameterId)
                        tblOtherDetails.Rows.Add(tblDataRow)
                    End If


                Next

            End If
            Dim ItemLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
            If ItemLifeCycleResult.Count() > 0 Then

                Dim tblrow As TableRow = New TableRow()
                tblrow = calculateItemBaseLifeCycle()
                tblOtherDetails.Rows.Add(tblrow)

                Dim tblDataRow As TableRow = New TableRow()
                tblDataRow = createAccountingControlItem()
                tblOtherDetails.Rows.Add(tblDataRow)
                SessionManager.setAttributrTypeId(1)
            End If
            Dim inspectedTblDataRow As TableRow = New TableRow()
            inspectedTblDataRow = CreateLastInspected()
            tblOtherDetails.Rows.Add(inspectedTblDataRow)


            Return tblOtherDetails

        End Function
#End Region

#Region "calculate Parameter base Life Cycle"
        Private Shared Function calculateParamBaseLifeCycle(ByVal parameterId As Integer) As TableRow
            'Dim valueId As Integer = SessionManager.getValueId()
            Dim loadParameterDdl As DataTable = New DataTable()
            'create drop down list dynamically  
            Dim parameter As Label = New Label()
            'set style of control
            parameter.ID = ApplicationConstants.lblLifeCycle + parameterId.ToString()
            parameter.CssClass = ApplicationConstants.ddlStyle
            SessionManager.setControlId(parameter.ID)
            ' parameter.Attributes.Add("ParameterID", valueId.ToString())
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            Dim tr As TableRow = New TableRow()
            ' tr.ID = ApplicationConstants.Tr + valueId.ToString()
            Dim componentResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("ITEMID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId Select ps)
            If componentResult.Count() > 0 Then
                parameter.Text = componentResult.First.Item("LIFECYCLE")
                Dim lblCycle As Label = New Label()
                Dim hdnLifeCyle As HiddenField = New HiddenField
                If attributeResult.First.Item("ParameterName").ToString().Contains("Rewire Due") Then
                    hdnLifeCyle.ID = ApplicationConstants.hdnLifeCycle + ApplicationConstants.rewiredDue.ToString()
                ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Upgrade Due") Then
                    hdnLifeCyle.ID = ApplicationConstants.hdnLifeCycle + ApplicationConstants.upgradeDue.ToString()
                ElseIf attributeResult.First.Item("ParameterName").ToString().Contains("Cur Due") Then
                    hdnLifeCyle.ID = ApplicationConstants.hdnLifeCycle + ApplicationConstants.curDue.ToString()
                End If

                hdnLifeCyle.Value = componentResult.First.Item("CYCLE")
                Dim td1 As TableCell = New TableCell()
                ' td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Right
                lblCycle.ID = ApplicationConstants.lblCycle + parameterId.ToString()
                Dim td2 As TableCell = New TableCell()
                ' td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblCycle.Text = "Life Cycle: "
                td1.Controls.Add(lblCycle)
                td2.Controls.Add(parameter)
                td2.Controls.Add(hdnLifeCyle)
                tr.Cells.Add(td1)
                tr.Cells.Add(td2)
                tr.Height = "30"
            End If


            Return tr
        End Function
#End Region

#Region "Create Is Accounting List controls for Parameter"
        Public Shared Function createAccountingControlParameter(ByVal parameterId As Integer) As TableRow


            Dim rdBtn As RadioButtonList = New RadioButtonList()
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButton
            rdBtn.RepeatDirection = RepeatDirection.Horizontal
            'set control styles
            rdBtn.ID = ApplicationConstants.rdBtn + parameterId.ToString()
            SessionManager.setAccountingControlId(rdBtn.ID)
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            'Filter the Parameters datatable w.r.t parameterId
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("ITEMID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)

            'Filter the Parametervalues which have the same ParameterID.          
            rdBtn.Items.Add(New ListItem("Yes", "True"))
            rdBtn.Items.Add(New ListItem("No", "False"))

            For Each item As ListItem In rdBtn.Items
                If item.Value = attributeResult.First.Item("ISACCOUNTING") Then
                    item.Selected = True
                End If
            Next
            rdBtn.Enabled = False

            'Populate the check box list with existing inserted values
            'populateControl(attributeResult.First.Item("ItemParamID"), rdBtn)
            Dim lblRadioBtn As Label = New Label()
            Dim tr As TableRow = New TableRow()
            'tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            ' td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            lblRadioBtn.ID = ApplicationConstants.lblRadioBtn + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            ' td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblRadioBtn.Text = "Component Accounting: "
            'Changes in dynamic control
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButtonComponentAccounting

            td2.Attributes.Add("colspan", "2")
            td1.Controls.Add(lblRadioBtn)
            td2.Controls.Add(rdBtn)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region

#Region "calculate Item base Life Cycle"
        Private Shared Function calculateItemBaseLifeCycle() As TableRow
            Dim valueId As Integer = SessionManager.getValueId()
            Dim loadParameterDdl As DataTable = New DataTable()
            'create drop down list dynamically  
            Dim parameter As Label = New Label()
            'set style of control
            parameter.ID = ApplicationConstants.lblLifeCycle + SessionManager.getTreeItemId().ToString()
            parameter.CssClass = ApplicationConstants.ddlStyle
            SessionManager.setControlId(parameter.ID)
            parameter.Attributes.Add("ParameterID", valueId.ToString())
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            Dim tr As TableRow = New TableRow()
            '  tr.ID = ApplicationConstants.Tr + valueId.ToString()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
            If attributeResult.Count() > 0 Then
                parameter.Text = attributeResult.First.Item("LIFECYCLE")
                Dim lblCycle As Label = New Label()
                Dim hdnLifeCyle As HiddenField = New HiddenField
                hdnLifeCyle.ID = ApplicationConstants.hdnLifeCycle + SessionManager.getTreeItemId().ToString()
                hdnLifeCyle.Value = attributeResult.First.Item("CYCLE")
                Dim td1 As TableCell = New TableCell()
                td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Right
                lblCycle.ID = ApplicationConstants.lblCycle + valueId.ToString()
                Dim td2 As TableCell = New TableCell()
                td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblCycle.Text = "Life Cycle: "
                td1.Controls.Add(lblCycle)
                td2.Controls.Add(parameter)
                td2.Controls.Add(hdnLifeCyle)
                tr.Cells.Add(td1)
                tr.Cells.Add(td2)
                tr.Height = "30"
            End If


            Return tr
        End Function
#End Region

#Region "Create Is Accounting List controls for Item"
        Public Shared Function createAccountingControlItem() As TableRow
            Dim valueId As Integer = SessionManager.getValueId()

            Dim rdBtn As RadioButtonList = New RadioButtonList()
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButton
            rdBtn.RepeatDirection = RepeatDirection.Horizontal
            'set control styles
            rdBtn.ID = ApplicationConstants.rdBtn + SessionManager.getTreeItemId().ToString()
            SessionManager.setAccountingControlId(rdBtn.ID)
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            'Filter the Parameters datatable w.r.t parameterId
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)

            'Filter the Parametervalues which have the same ParameterID.          
            rdBtn.Items.Add(New ListItem("Yes", "True"))
            rdBtn.Items.Add(New ListItem("No", "False"))

            For Each item As ListItem In rdBtn.Items
                If item.Value = attributeResult.First.Item("ISACCOUNTING") Then
                    item.Selected = True
                End If
            Next
            rdBtn.Enabled = False
            'populateControl(attributeResult.First.Item("ItemParamID"), rdBtn)
            Dim lblRadioBtn As Label = New Label()
            Dim tr As TableRow = New TableRow()
            'tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            ' td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            lblRadioBtn.ID = ApplicationConstants.lblRadioBtn + valueId.ToString()
            Dim td2 As TableCell = New TableCell()
            ' td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblRadioBtn.Text = "Component Accounting: "
            'Changes in dynamic control
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButtonComponentAccounting
            td2.Attributes.Add("colspan", "2")
            td1.Controls.Add(lblRadioBtn)
            td2.Controls.Add(rdBtn)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region

#Region "calculate Item base Life Cycle"
        Private Shared Function CreateLastInspected() As TableRow

            Dim loadParameterDdl As DataTable = New DataTable()
            'create drop down list dynamically  
            Dim parameter As Label = New Label()
            'set style of control
            parameter.ID = ApplicationConstants.lblInspected + SessionManager.getTreeItemId().ToString()
            parameter.CssClass = ApplicationConstants.ddlStyle
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            Dim tr As TableRow = New TableRow()
            '  tr.ID = ApplicationConstants.Tr + valueId.ToString()
            Dim inspectedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("LastInspected") IsNot DBNull.Value Select ps)

            If inspectedResult.Count > 0 Then
                parameter.Text = inspectedResult.First.Item("LastInspected")
                Dim lblInspected As Label = New Label()

                Dim td1 As TableCell = New TableCell()
                ' td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Right
                lblInspected.ID = ApplicationConstants.lblInspected
                Dim td2 As TableCell = New TableCell()
                'td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblInspected.Text = "Last Inspected: "
                td1.Controls.Add(lblInspected)
                td2.Controls.Add(parameter)
                tr.Cells.Add(td1)
                tr.Cells.Add(td2)
                tr.Height = "30"
            End If


            Return tr
        End Function
#End Region

#Region "Create Label row by row title and row value"

        Public Shared Function createLabelRowByTitleValue(ByVal rowTitle As String, Optional ByVal rowValue As String = "N/A") As TableRow
            'create drop down list dynamically  
            Dim parameter As Label = New Label()
            'set style of control
            parameter.CssClass = ApplicationConstants.ddlStyle

            Dim tr As TableRow = New TableRow()
            tr.Height = "30"

            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            td1.Text = rowTitle + ":"
            tr.Cells.Add(td1)

            Dim td2 As TableCell = New TableCell()
            parameter.Text = rowValue
            td2.Controls.Add(parameter)
            tr.Cells.Add(td2)

            Return tr
        End Function

#End Region

#Region "create dynamic control for heating"
        Public Shared Function createHeatingDynaicControls(ByRef parameterDataSet As DataSet, ByVal heatingMapping As String) As Table
            Dim tblOtherDetails As Table = New Table()
            SessionManager.setIsAccountingMapped(0)
            SessionManager.setIsLifeCycleMapped(0)
            SessionManager.removepreviousValueId()
            'SessionManager.removeIsMapped()
            'SessionManager.removeComponentCycle()

            Dim heatingType As Integer = 0
            Dim heatingMappingId As Integer = 0
            Dim heatArr() As String
            heatArr = heatingMapping.Split("|")
            heatingMappingId = heatArr(0)
            heatingType = heatArr(1)
            SessionManager.setHeatingMappingId(heatingMappingId)
            SessionManager.setheatingMapping(heatingMapping)
            SessionManager.setHeatingFuelId(heatingType)
            Dim controlType As String
            Dim parameterId As Integer
            Dim showInApp As Boolean
            Dim parameterName As String
            Dim itemParamId As Integer

            If parameterDataSet.Tables("Parameters").Rows.Count > 0 Then
                Dim parameterTableRS As DataTable = parameterDataSet.Tables(ApplicationConstants.parameters)
                Dim heatingFuel As DataRow = parameterDataSet.Tables(ApplicationConstants.parameters).Rows(0)
                controlType = heatingFuel.Item("ControlType")
                parameterId = heatingFuel.Item("ParameterID")
                showInApp = heatingFuel.Item("ShowInApp")
                parameterName = heatingFuel.Item("ParameterName")
                itemParamId = heatingFuel.Item("ItemParamId")
                If controlType.ToUpper() = "Dropdown".ToUpper() Then
                    Dim lifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("VALUEID") IsNot DBNull.Value Or ps.Item("SubParameter").ToString() = parameterId.ToString() Select ps)
                    If lifeCycleResult.Count() > 0 Then
                        SessionManager.setValueLifeCycle(True)
                        SessionManager.setIsDdlChangeEvent(True)
                        SessionManager.setAttributrTypeId(3)
                    Else
                        SessionManager.setValueLifeCycle(False)
                        SessionManager.setIsDdlChangeEvent(False)
                    End If
                    Dim tr As TableRow = New TableRow()
                    Dim dropDownBlObj As DropDownControlBL = New DropDownControlBL()

                    tr = dropDownBlObj.createControl(parameterId, itemParamId)
                    tblOtherDetails.Rows.Add(tr)


                    If lifeCycleResult.Count() > 0 Then
                        Dim tblrow As TableRow = New TableRow()
                        tblrow = dropDownBlObj.calculateValueBaseLifeCycle(parameterId)
                        tblOtherDetails.Rows.Add(tblrow)

                        Dim tblDataRow As TableRow = New TableRow()
                        tblDataRow = dropDownBlObj.createComponentAccountingControl(parameterId)
                        tblOtherDetails.Rows.Add(tblDataRow)
                    End If
                End If

            End If

          
            If heatingType > 0 Then
                Dim filteredDV As DataView = parameterDataSet.Tables(ApplicationConstants.parameters).AsDataView()
                filteredDV.RowFilter = "ParameterValueId =  " + heatingType.ToString()
                Dim filteredDT As DataTable = filteredDV.ToTable()



                If filteredDT.Rows.Count > 0 Then
                    For Each dr As DataRow In filteredDT.Rows
                        controlType = dr.Item("ControlType")
                        parameterId = dr.Item("ParameterID")
                        showInApp = dr.Item("ShowInApp")
                        parameterName = dr.Item("ParameterName")
                        itemParamId = dr.Item("ItemParamId")

                        If controlType.ToUpper() = "Dropdown".ToUpper() Then
                            Dim lifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("VALUEID") IsNot DBNull.Value Or ps.Item("SubParameter").ToString() = parameterId.ToString() Select ps)
                            If lifeCycleResult.Count() > 0 Then
                                SessionManager.setValueLifeCycle(True)
                                SessionManager.setIsDdlChangeEvent(True)
                                SessionManager.setAttributrTypeId(3)
                            Else
                                SessionManager.setValueLifeCycle(False)
                                SessionManager.setIsDdlChangeEvent(False)
                            End If
                            Dim tr As TableRow = New TableRow()
                            Dim dropDownBlObj As DropDownControlBL = New DropDownControlBL()

                            tr = dropDownBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)


                            If lifeCycleResult.Count() > 0 Then
                                Dim tblrow As TableRow = New TableRow()
                                tblrow = dropDownBlObj.calculateValueBaseLifeCycle(parameterId)
                                tblOtherDetails.Rows.Add(tblrow)

                                Dim tblDataRow As TableRow = New TableRow()
                                tblDataRow = dropDownBlObj.createComponentAccountingControl(parameterId)
                                tblOtherDetails.Rows.Add(tblDataRow)
                            End If

                        ElseIf controlType.ToUpper() = "Date".ToUpper() Then
                            Dim tr As TableRow = New TableRow()
                            Dim datesBlObj As DatesControlBL = New DatesControlBL()
                            tr = datesBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)

                        ElseIf controlType.ToUpper() = "Textbox".ToUpper() Then

                            Dim tr As TableRow = New TableRow()
                            Dim textBoxBlObj As TextBoxControlBL = New TextBoxControlBL()

                            tr = textBoxBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)


                        ElseIf controlType.ToUpper() = "TextArea".ToUpper() Then

                            Dim tr As TableRow = New TableRow()
                            Dim textBoxBlObj As TextAreaControlBL = New TextAreaControlBL()
                            If parameterName = "Works Required" Then
                                tr = textBoxBlObj.createWorksRequiredControl(parameterId)
                            Else
                                tr = textBoxBlObj.createControl(parameterId, itemParamId)
                            End If

                            tblOtherDetails.Rows.Add(tr)
                        ElseIf controlType.ToUpper() = "Checkboxes".ToUpper() Then

                            If (dr("ParameterName").ToString().ToUpper = ApplicationConstants.landlordAppliance.ToUpper) Then
                                Dim dtLGSRResult = (From ps In parameterDataSet.Tables(ApplicationConstants.dtLGSR) Where ps.Item("HeatingType").ToString() = heatingType Select ps)
                                If dtLGSRResult.Count() > 0 Then
                                    Dim certificateName As TableRow
                                    Dim cp12Issued As TableRow
                                    Dim cp12Nubmer As TableRow
                                    Dim cp12Renewal As TableRow
                                    Dim cP12InformationResult = (From ps In parameterDataSet.Tables(ApplicationConstants.cP12Information) Where ps.Item("HeatingMappingId") = heatingMappingId Select ps)
                                    If (heatingMappingId > 0 AndAlso cP12InformationResult.Count > 0) Then
                                        Dim dtCP12 As DataTable = cP12InformationResult.CopyToDataTable()
                                        With dtCP12.Rows(0)
                                            certificateName = createLabelRowByTitleValue(ApplicationConstants.CertificateName, dtLGSRResult.FirstOrDefault().Item("Title"))
                                            cp12Issued = createLabelRowByTitleValue(ApplicationConstants.cp12Issued, .Item(ApplicationConstants.cp12Issued))
                                            cp12Nubmer = createLabelRowByTitleValue(ApplicationConstants.cp12Number, .Item(ApplicationConstants.cp12Number))
                                            cp12Renewal = createLabelRowByTitleValue(ApplicationConstants.cp12Renewal, .Item(ApplicationConstants.cp12Renewal))
                                        End With
                                    Else
                                        certificateName = createLabelRowByTitleValue(ApplicationConstants.CertificateName, dtLGSRResult.FirstOrDefault().Item("Title"))
                                        cp12Issued = createLabelRowByTitleValue(ApplicationConstants.cp12Issued)
                                        cp12Nubmer = createLabelRowByTitleValue(ApplicationConstants.cp12Number)
                                        cp12Renewal = createLabelRowByTitleValue(ApplicationConstants.cp12Renewal)
                                    End If
                                    tblOtherDetails.Rows.Add(certificateName)
                                    tblOtherDetails.Rows.Add(cp12Issued)
                                    tblOtherDetails.Rows.Add(cp12Nubmer)
                                    tblOtherDetails.Rows.Add(cp12Renewal)
                                End If
                            End If
                            Dim tr As TableRow = New TableRow()
                            Dim checkBoxBlObj As CheckBoxControlBL = New CheckBoxControlBL()
                            tr = checkBoxBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)
                        ElseIf controlType.ToUpper() = "Radiobutton".ToUpper() Then

                            Dim tr As TableRow = New TableRow()
                            Dim radioButtonBlObj As RadioButtonControlBL = New RadioButtonControlBL()
                            tr = radioButtonBlObj.createControl(parameterId, itemParamId)
                            tblOtherDetails.Rows.Add(tr)

                        End If

                        Dim parameterLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("PARAMETERID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                        If parameterLifeCycleResult.Count() > 0 Then
                            SessionManager.setAttributrTypeId(2)
                            Dim tblrow As TableRow = New TableRow()
                            tblrow = calculateParamBaseLifeCycle(parameterId)
                            tblOtherDetails.Rows.Add(tblrow)

                            Dim tblDataRow As TableRow = New TableRow()
                            tblDataRow = createAccountingControlParameter(parameterId)
                            tblOtherDetails.Rows.Add(tblDataRow)
                        End If


                    Next

                End If
                Dim ItemLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                If ItemLifeCycleResult.Count() > 0 Then

                    Dim tblrow As TableRow = New TableRow()
                    tblrow = calculateItemBaseLifeCycle()
                    tblOtherDetails.Rows.Add(tblrow)

                    Dim tblDataRow As TableRow = New TableRow()
                    tblDataRow = createAccountingControlItem()
                    tblOtherDetails.Rows.Add(tblDataRow)
                    SessionManager.setAttributrTypeId(1)
                End If
                Dim inspectedTblDataRow As TableRow = New TableRow()
                inspectedTblDataRow = CreateLastInspected()
                tblOtherDetails.Rows.Add(inspectedTblDataRow)
            End If

            Return tblOtherDetails

        End Function
#End Region



    End Class
End Namespace