﻿Imports AS_Utilities
Imports System.Web.UI.WebControls

Namespace AS_BusinessLogic
    Public Class CheckBoxControlBL
        Implements IControlBL


#Region "Functions"
#Region "Create controls"

        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            Dim loadParameterDdl As DataTable = New DataTable()
            Dim chkList As CheckBoxList = New CheckBoxList()
            'set control styles
            chkList.ID = ApplicationConstants.chkList + parameterId.ToString()
            chkList.CssClass = ApplicationConstants.dynamicRadioButton
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            'convert Parametervalues data table into dataview            
            Dim loadParameterDv As DataView = New DataView()
            loadParameterDv = loadParameterDdl.AsDataView()
            'Filter the Parameters datatable w.r.t parameterId
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)

            'Filter the Parametervalues which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString()
            loadParameterDdl = loadParameterDv.ToTable()
            'Bind the datasource
            chkList.DataSource = loadParameterDdl
            chkList.DataValueField = "ValueID"
            chkList.DataTextField = "ValueDetail"
            chkList.DataBind()
            'Populate the check box list with existing inserted values
            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                If heatingParamId > 0 Then
                    populateHeatingControl(attributeResult.First.Item("ItemParamID"), chkList, heatingParamId)
                End If
            Else
                populateControl(attributeResult.First.Item("ItemParamID"), chkList)
            End If
            Dim lblChkBox As Label = New Label()
            Dim tr As TableRow = New TableRow()
            tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            lblChkBox.ID = ApplicationConstants.lblChkBox + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            'set dafault value of landlord appliance
            If attributeResult.First.Item("ParameterName").ToString() = "Landlord Appliance" Then
                SetLandlordApplianceDefaultValue(attributeResult.First.Item("ItemParamID"), attributeResult.First.Item("ParameterID"), chkList)
            End If

            lblChkBox.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            If attributeResult.First.Item("ParameterName").ToString() = "Condition Rating" Or
                attributeResult.First.Item("ParameterName").ToString() = "Boiler Condition Rating" Then
                chkList.CssClass = ApplicationConstants.dynamicRadioButtonBoilerConditionRating
            Else
                chkList.CssClass = ApplicationConstants.dynamicRadioButton
            End If
            td1.Controls.Add(lblChkBox)
            td2.Controls.Add(chkList)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function

#End Region

#Region " Populate Controls"
        Public Sub populateControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control) Implements IControlBL.populateControl

            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim chklist As CheckBoxList = CType(control, CheckBoxList)
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)
            For Each lst As DataRow In preInsertedResult

                If lst.Item("IsCheckBoxSelected") = True Then
                    chklist.Items.FindByValue(lst.Item("VALUEID")).Selected = True
                    'tem.Selected = lst.Item("VALUEID")
                    'Else
                    '    chklist.Items.FindByValue(lst.Item("VALUEID")).Selected = True
                End If
                'For Each item As ListItem In chklist.Items
                '    If item.Value = lst.Item("VALUEID") AndAlso Not IsDBNull(lst.Item("IsCheckBoxSelected")) = True Then
                '        item.Selected = lst.Item("VALUEID")
                '    End If
                'Next
            Next


        End Sub
#End Region

#Region "Set default values of landlords Appliance"
        Public Sub SetLandlordApplianceDefaultValue(ByVal itemParamId As Integer, ByVal ParameterId As Integer, ByRef control As Web.UI.Control)
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim chklist As CheckBoxList = CType(control, CheckBoxList)
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)
            If preInsertedResult.Count = 0 Then
                chklist.Items(0).Selected = True
            End If
        End Sub
#End Region

#Region " Populate Heating Controls"
        Public Sub populateHeatingControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control, ByVal heatingMappingId As Integer)

            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim chklist As CheckBoxList = CType(control, CheckBoxList)
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
            For Each lst As DataRow In preInsertedResult

                If lst.Item("IsCheckBoxSelected") = True Then
                    chklist.Items.FindByValue(lst.Item("VALUEID")).Selected = True
                    'tem.Selected = lst.Item("VALUEID")
                    'Else
                    '    chklist.Items.FindByValue(lst.Item("VALUEID")).Selected = True
                End If
                'For Each item As ListItem In chklist.Items
                '    If item.Value = lst.Item("VALUEID") AndAlso Not IsDBNull(lst.Item("IsCheckBoxSelected")) = True Then
                '        item.Selected = lst.Item("VALUEID")
                '    End If
                'Next
            Next


        End Sub
#End Region
#End Region

    End Class
End Namespace
