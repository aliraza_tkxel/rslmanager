﻿
Public Class ProvisionsBL


    Private objProvisionsRepo As ProvisionsDAL

    Public Sub New()
        objProvisionsRepo = New ProvisionsDAL
    End Sub

    Public Function GetProvisionsData(ByVal schemeId As Integer?, ByVal blockId As Integer?) As DataSet
        Return objProvisionsRepo.GetProvisionsData(schemeId, blockId)
    End Function

    Public Function GetProvisionsDataById(ByVal provisionId As Integer) As DataSet
        Return objProvisionsRepo.GetProvisionsDataById(provisionId)
    End Function

End Class

