﻿Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class DropDownControlBL
        Implements IControlBL

#Region "Functions"
#Region "Create controls"
        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            Dim loadParameterDdl As DataTable = New DataTable()
            'create drop down list dynamically  
            Dim parameter As DropDownList = New DropDownList()
            'set style of control
            parameter.ID = ApplicationConstants.ddlParameter + parameterId.ToString()
            parameter.CssClass = ApplicationConstants.ddlStyle
            parameter.Attributes.Add("ParameterID", parameterId.ToString())
            ' Dim parameterId As Integer = dr.Item("ParameterID")
            'Load Parameter values data table
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)

            'convert Parameter values data table into data view            
            Dim loadParameterDv As DataView = New DataView()
            loadParameterDv = loadParameterDdl.AsDataView()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)

            ' Dim lifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId And ps.Item("VALUEID").ToString() IsNot Nothing Select ps)

            If SessionManager.getIsDdlChangeEvent = True AndAlso attributeResult.First.Item("ParameterName").ToString() <> "Heating Fuel" Then
                parameter.AutoPostBack = True
                AddHandler parameter.SelectedIndexChanged, AddressOf parameter_SelectedIndexChanged
                SessionManager.setValueLifeCycle(True)

            End If
            If attributeResult.First.Item("ParameterName").ToString() = "Heating Fuel" Then
                loadParameterDv.Sort = "ValueDetail ASC"
            End If
            'Filter the Parameter values which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString()
            loadParameterDdl = loadParameterDv.ToTable()
            parameter.DataSource = loadParameterDdl
            parameter.DataValueField = "ValueID"
            parameter.DataTextField = "ValueDetail"
            'parameter.SelectedIndex = 1
            parameter.DataBind()
            'parameter.Items.Insert(0, New ListItem("Please Select", "-1"))
            If attributeResult.First.Item("ParameterName").ToString() = "Manufacturer" Then
                reloadManufacturer(parameter)
            End If

            If SessionManager.getValueLifeCycle() = True Then
                SessionManager.setValueId(parameter.SelectedValue())
            End If
            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                If heatingParamId > 0 Then
                    populateHeatingControl(attributeResult.First.Item("ItemParamID"), parameter, heatingParamId)
                End If
            Else
                populateControl(attributeResult.First.Item("ItemParamID"), parameter)
            End If

            If attributeResult.First.Item("ParameterName").ToString() = "Heating Fuel" Then
                parameter.Attributes.Add("onchange", "heatingFuelChanged(this);")
                If SessionManager.getHeatingFuelId() > 0 Then
                    parameter.SelectedValue = SessionManager.getHeatingFuelId().ToString()
                    ' parameter_SelectedIndexChanged(parameter, Nothing)
                End If
                If SessionManager.getHeatingMappingId() > 0 Then
                    parameter.Enabled = False
                Else
                    parameter.Enabled = True
                End If
            End If

            If attributeResult.First.Item("ParameterName").ToString() = "Heating Fuel" And SessionManager.getpreviousValueId() = Nothing Then
                SessionManager.setpreviousValueId(parameter.SelectedValue)
            End If
            Dim lblDdl As Label = New Label()
            Dim tr As TableRow = New TableRow()
            '  tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            ' td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            td1.HorizontalAlign = HorizontalAlign.Right
            lblDdl.ID = ApplicationConstants.lblDdl + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            'td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblDdl.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            td1.Controls.Add(lblDdl)
            td1.Attributes.Add("Width", "150")
            td2.Controls.Add(parameter)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region
#Region " Populate Controls"
        Public Sub populateControl(ByVal itemParamId As Integer, ByRef control As Control) Implements IControlBL.populateControl
            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim ddllist As DropDownList = CType(control, DropDownList)
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)
            If preInsertedResult.Count > 0 Then
                ddllist.SelectedValue = preInsertedResult.First.Item("VALUEID").ToString()
                If SessionManager.getValueLifeCycle() = True Then
                    SessionManager.setValueId(preInsertedResult.First.Item("VALUEID"))

                    Dim isMappedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = preInsertedResult.First.Item("VALUEID") And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable

                    If isMappedResult.Count > 0 Then

                        SessionManager.setIsMapped(False)
                        If SessionManager.getpreviousValueId() Is Nothing Then
                            SessionManager.setpreviousValueId(preInsertedResult.First.Item("VALUEID"))
                        End If

                    Else
                        Dim previousValueId = SessionManager.getpreviousValueId()
                        If previousValueId > 0 Then
                            Dim valId As Integer = preInsertedResult.First.Item("VALUEID")
                            Dim loadComponentDv As DataView = New DataView()
                            Dim dtLoadComponent As DataTable = New DataTable()
                            dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                            loadComponentDv = dtLoadComponent.AsDataView()
                            loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valId.ToString()
                            dtLoadComponent = loadComponentDv.ToTable()
                            If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                                Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valId Select ps)
                                If ipreviousResult.Count > 0 Then
                                    SessionManager.setIsMapped(True)
                                End If
                            End If
                        Else
                            Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = preInsertedResult.First.Item("VALUEID") And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                            If iResult.Count > 0 Then
                                SessionManager.setIsMapped(True)
                            Else
                                SessionManager.setIsMapped(False)
                            End If
                        End If


                    End If
                Else
                    SessionManager.removeValueId()
                End If
            Else
                ddllist.SelectedValue = "-1"
                SessionManager.setIsMapped(False)

            End If
            SessionManager.setIsDdlChangeEvent(False)
            SessionManager.setValueLifeCycle(False)


        End Sub
#End Region

#Region "calculate Value base Life Cycle"
        Public Function calculateValueBaseLifeCycle(ByVal parameterId As Integer) As TableRow
            Dim valueId As Integer = SessionManager.getValueId()
            Dim loadParameterDdl As DataTable = New DataTable()
            'create drop down list dynamically  
            Dim parameter As Label = New Label()
            Dim tr As TableRow = New TableRow()
            tr.ID = ApplicationConstants.Tr + "_LIFECYCLE_" + parameterId.ToString()

            'set style of control
            parameter.ID = ApplicationConstants.lblLifeCycle + parameterId.ToString()
            parameter.CssClass = ApplicationConstants.ddlStyle
            SessionManager.setControlId(parameter.ID)
            'parameter.Attributes.Add("ParameterID", valueId.ToString())
            Dim lblCycle As Label = New Label()
            'Load Parameter values datatable
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            loadParameterDdl = parameterDataSet.Tables(ApplicationConstants.parametervalues)
            '  Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId Select ps)
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable
            Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)

            If attributeResult.Count > 0 Then
                '  If attributeResult.Count > 0 Then
                parameter.Text = attributeResult.First.Item("LIFECYCLE")
                tr.Style.Add("display", "None")
            ElseIf iResult.Count > 0 Then

                parameter.Text = iResult.First.Item("LIFECYCLE")
                If SessionManager.getIsMapped() = True Then
                    tr.Style.Add("display", "table-row")
                    Dim cycle As Integer = iResult.First.Item("CYCLE")
                    SessionManager.setComponentCycle(cycle)
                Else
                    tr.Style.Add("display", "None")
                End If


            Else
                Dim previousValueId = SessionManager.getpreviousValueId()
                If previousValueId > 0 Then
                    Dim loadComponentDv As DataView = New DataView()
                    Dim dtLoadComponent As DataTable = New DataTable()
                    dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                    loadComponentDv = dtLoadComponent.AsDataView()
                    loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                    dtLoadComponent = loadComponentDv.ToTable()
                    If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                        Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                        If ipreviousResult.Count > 0 Then
                            parameter.Text = ipreviousResult.First.Item("LIFECYCLE")
                            If SessionManager.getIsMapped() = True Then
                                tr.Style.Add("display", "table-row")
                                Dim cycle As Integer = ipreviousResult.First.Item("CYCLE")
                                SessionManager.setComponentCycle(cycle)


                            Else
                                tr.Style.Add("display", "None")
                            End If
                        Else
                            tr.Style.Add("display", "None")
                        End If
                    ElseIf parameterId = 65 Then
                        Dim iPreviousResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = previousValueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)

                        If iPreviousResult.Count > 0 Then
                            parameter.Text = iPreviousResult.First.Item("LIFECYCLE")
                            SessionManager.setIsMapped(True)
                            tr.Style.Add("display", "table-row")
                            Dim cycle As Integer = iPreviousResult.First.Item("CYCLE")
                            SessionManager.setComponentCycle(cycle)

                        Else
                            tr.Style.Add("display", "None")
                        End If
                    Else
                        tr.Style.Add("display", "None")
                    End If
                Else

                    tr.Style.Add("display", "None")
                End If

            End If
            Dim td1 As TableCell = New TableCell()
            'td1.ID = ApplicationConstants.Td1 + valueId.ToString()
            td1.HorizontalAlign = HorizontalAlign.Right
            'lblCycle.ID = ApplicationConstants.lblCycle + valueId.ToString()
            Dim td2 As TableCell = New TableCell()
            'td2.ID = ApplicationConstants.Td2 + valueId.ToString()
            lblCycle.Text = "Life Cycle: "
            td1.Controls.Add(lblCycle)
            td2.Controls.Add(parameter)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"

            Return tr
        End Function
#End Region

#Region "Drop down selected index changed event"
        Public Sub parameter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try

                Dim valueId As String = CType(sender, DropDownList).SelectedValue
                Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
                If valueId > 0 Then

                    Dim previousValueId As String = SessionManager.getpreviousValueId()
                    SessionManager.setValueId(valueId)
                    Dim parameterResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parametervalues) Where ps.Item("VALUEID") = valueId Select ps)
                    Dim txtReplacementDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                    Dim lifecyclerowId As String = parameterResult.First.Item("PARAMETERID")
                    Dim lbllifecycle As Label = CType(sender.FindControl(ApplicationConstants.lblLifeCycle + lifecyclerowId.ToString()), Label)
                    Dim rbtnList As RadioButtonList = CType(sender.FindControl(ApplicationConstants.rdBtn + lifecyclerowId.ToString()), RadioButtonList)
                    Dim lifeCycleRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_LIFECYCLE_" + lifecyclerowId.ToString()), TableRow)
                    Dim componentAccountingRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_Accounting_" + lifecyclerowId.ToString()), TableRow)
                    'Implement CR "when the user selects an option 
                    'within the Heating Fuel drop down list, the ‘Heating Type’ changes to ‘Please Select’."

                    Dim attributePrimaryHeatingResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = lifecyclerowId Select ps)
                    If attributePrimaryHeatingResult.Count() > 0 Then
                        Dim ControlType As String = attributePrimaryHeatingResult.First().Item("ControlType")
                        Dim parameterName As String = attributePrimaryHeatingResult.First().Item("ParameterName")
                        If ControlType.ToUpper() = "Dropdown".ToUpper() AndAlso parameterName.ToUpper = "Heating Fuel".ToUpper Then
                            Dim attributeHeatingResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterName").ToString().ToUpper = "Heating Type".ToUpper Select ps)
                            If (attributeHeatingResult.Count > 0) Then
                                Dim boilerTypeId As Integer = attributeHeatingResult.First().Item("ParameterID")
                                Dim ddlParameter As DropDownList = CType(sender.FindControl(ApplicationConstants.ddlParameter + boilerTypeId.ToString()), DropDownList)
                                ddlParameter.SelectedIndex = 0
                                Dim boilerTypeLifeCycleRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_LIFECYCLE_" + boilerTypeId.ToString()), TableRow)
                                Dim boilerTypeComponentAccountingRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_Accounting_" + boilerTypeId.ToString()), TableRow)
                                boilerTypeLifeCycleRow.Style.Add("display", "none")
                                boilerTypeComponentAccountingRow.Style.Add("display", "none")
                            End If
                        End If
                    End If

                    Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable
                    Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                    rbtnList.Items.Clear()
                    rbtnList.Items.Add(New ListItem("Yes", "True"))
                    rbtnList.Items.Add(New ListItem("No", "False"))
                    If attributeResult.Count > 0 Then
                        lbllifecycle.Text = attributeResult.First.Item("LIFECYCLE")

                        SessionManager.setAccountingControlId(rbtnList.ID)

                        For Each item As ListItem In rbtnList.Items
                            If item.Value = attributeResult.First.Item("ISACCOUNTING") Then
                                item.Selected = True
                            End If
                        Next
                        lifeCycleRow.Style.Add("display", "none")
                        componentAccountingRow.Style.Add("display", "none")
                        SessionManager.setpreviousValueId(valueId)
                    ElseIf iResult.Count > 0 Then
                        SessionManager.setIsMapped(True)
                        lbllifecycle.Text = iResult.First.Item("LIFECYCLE")
                        lifeCycleRow.Style.Add("display", "table-row")
                        componentAccountingRow.Style.Add("display", "table-row")
                        SessionManager.setpreviousValueId(valueId)
                        For Each item As ListItem In rbtnList.Items
                            If item.Value = iResult.First.Item("ISACCOUNTING") Then
                                item.Selected = True
                            End If
                        Next

                        Dim cycle As Integer = iResult.First.Item("CYCLE")
                        SessionManager.setComponentCycle(cycle)
                        ' Dim txtReplacementDue As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                        Dim txtLastReplaced As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                        If txtLastReplaced.Text <> "" Then
                            txtReplacementDue.Text = Convert.ToDateTime(txtLastReplaced.Text).AddMonths(cycle).ToShortDateString()
                            txtReplacementDue.Enabled = False
                        End If
                    Else
                        If (IsNothing(previousValueId)) Then
                            previousValueId = 0
                        End If
                        ' Dim previousValueId = SessionManager.getpreviousValueId()
                        Dim loadComponentDv As DataView = New DataView()
                        Dim dtLoadComponent As DataTable = New DataTable()
                        dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                        loadComponentDv = dtLoadComponent.AsDataView()
                        loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                        dtLoadComponent = loadComponentDv.ToTable()
                        If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                            Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                            If ipreviousResult.Count > 0 Then
                                SessionManager.setIsMapped(True)
                                lbllifecycle.Text = ipreviousResult.First.Item("LIFECYCLE")
                                For Each item As ListItem In rbtnList.Items
                                    If item.Value = ipreviousResult.First.Item("ISACCOUNTING") Then
                                        item.Selected = True
                                    End If
                                Next
                                lifeCycleRow.Style.Add("display", "table-row")
                                componentAccountingRow.Style.Add("display", "table-row")
                                Dim cycle As Integer = ipreviousResult.First.Item("CYCLE")
                                SessionManager.setComponentCycle(cycle)

                                Dim txtLastReplaced As TextBox = CType(sender.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                                If txtLastReplaced IsNot Nothing AndAlso txtLastReplaced.Text <> "" Then
                                    txtReplacementDue.Text = Convert.ToDateTime(txtLastReplaced.Text).AddMonths(cycle).ToShortDateString()
                                    txtReplacementDue.Enabled = False
                                End If

                            Else
                                SessionManager.setIsMapped(False)
                                lifeCycleRow.Style.Add("display", "None")
                                componentAccountingRow.Style.Add("display", "none")
                                If SessionManager.getComponentCycle() = Nothing Then
                                    txtReplacementDue.Enabled = True
                                End If

                            End If
                        Else
                            If (Not (SessionManager.getIsMapped())) Then
                                lifeCycleRow.Style.Add("display", "None")
                                componentAccountingRow.Style.Add("display", "none")
                                'If SessionManager.getComponentCycle() = Nothing Then
                                txtReplacementDue.Enabled = True
                                'End If
                            End If

                        End If
                    End If
                Else
                    CType(sender, DropDownList).SelectedIndex = 1

                    Dim valId As String = CType(sender, DropDownList).SelectedValue
                    Dim parameterResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parametervalues) Where ps.Item("VALUEID") = valId Select ps)

                    Dim lifecyclerowId As String = parameterResult.First.Item("PARAMETERID")
                    Dim lifeCycleRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_LIFECYCLE_" + lifecyclerowId.ToString()), TableRow)
                    Dim componentAccountingRow As TableRow = CType(sender.FindControl(ApplicationConstants.Tr + "_Accounting_" + lifecyclerowId.ToString()), TableRow)
                    lifeCycleRow.Style.Add("display", "None")
                    componentAccountingRow.Style.Add("display", "none")
                    CType(sender, DropDownList).SelectedIndex = -1
                    SessionManager.removeComponentCycle()
                End If
                
            Catch ex As Exception
                Throw
            End Try

        End Sub
#End Region

#Region "create Component Accounting Control"
        Public Function createComponentAccountingControl(ByVal parameterId As Integer) As TableRow
            Dim valueId As Integer = SessionManager.getValueId()

            Dim rdBtn As RadioButtonList = New RadioButtonList()
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButton
            rdBtn.RepeatDirection = RepeatDirection.Horizontal
            'set control styles
            Dim tr As TableRow = New TableRow()
            tr.ID = ApplicationConstants.Tr + "_Accounting_" + parameterId.ToString()
            rdBtn.ID = ApplicationConstants.rdBtn + parameterId.ToString()
            SessionManager.setAccountingControlId(rdBtn.ID)

            'Load Parameter values data table
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            'Filter the Parameters data table w.r.t parameterId
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable
            Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)

            'Filter the Parameter values which have the same ParameterID.          

            If attributeResult.Count > 0 Then
                rdBtn.Items.Add(New ListItem("Yes", "True"))
                rdBtn.Items.Add(New ListItem("No", "False"))

                For Each item As ListItem In rdBtn.Items
                    If item.Value = attributeResult.First.Item("ISACCOUNTING") Then
                        item.Selected = True
                    End If
                Next
                rdBtn.Enabled = False
                tr.Style.Add("display", "None")
            ElseIf iResult.Count > 0 Then

                rdBtn.Items.Add(New ListItem("Yes", "True"))
                rdBtn.Items.Add(New ListItem("No", "False"))

                For Each item As ListItem In rdBtn.Items
                    If item.Value = iResult.First.Item("ISACCOUNTING") Then
                        item.Selected = True
                    End If
                Next
                If SessionManager.getIsMapped() = True Then
                    tr.Style.Add("display", "table-row")
                Else
                    tr.Style.Add("display", "None")
                End If


            Else
                Dim previousValueId = SessionManager.getpreviousValueId()
                If previousValueId > 0 Then


                    Dim loadComponentDv As DataView = New DataView()
                    Dim dtLoadComponent As DataTable = New DataTable()
                    dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                    loadComponentDv = dtLoadComponent.AsDataView()
                    loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                    dtLoadComponent = loadComponentDv.ToTable()
                    If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                        Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                        If ipreviousResult.Count > 0 Then
                            '  ApplicationConstants.isMapped = True
                            rdBtn.Items.Add(New ListItem("Yes", "True"))
                            rdBtn.Items.Add(New ListItem("No", "False"))

                            For Each item As ListItem In rdBtn.Items
                                If item.Value = ipreviousResult.First.Item("ISACCOUNTING") Then
                                    item.Selected = True
                                End If
                            Next
                            If SessionManager.getIsMapped() = True Then
                                tr.Style.Add("display", "table-row")
                            Else
                                tr.Style.Add("display", "None")
                            End If
                        Else
                            tr.Style.Add("display", "None")
                        End If
                    ElseIf parameterId = 65 Then
                        Dim iPreviousResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = previousValueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)

                        If iPreviousResult.Count > 0 Then
                            'Parameter.Text = iPreviousResult.First.Item("LIFECYCLE")
                            rdBtn.Items.Add(New ListItem("Yes", "True"))
                            rdBtn.Items.Add(New ListItem("No", "False"))

                            For Each item As ListItem In rdBtn.Items
                                If item.Value = iPreviousResult.First.Item("ISACCOUNTING") Then
                                    item.Selected = True
                                End If
                            Next

                        Else
                            tr.Style.Add("display", "None")
                        End If
                    Else
                        tr.Style.Add("display", "None")
                    End If
                Else
                    tr.Style.Add("display", "None")
                End If


            End If
            'If ApplicationConstants.isMapped = False Then
            '    tr.Style.Add("display", "None")

            'End If
            rdBtn.Enabled = False
            Dim lblRadioBtn As Label = New Label()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            lblRadioBtn.ID = ApplicationConstants.lblRadioBtn + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            lblRadioBtn.Text = "Component Accounting: "

            'Changes in dynamic control
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButtonComponentAccounting
            td2.Attributes.Add("colspan", "2")
            td1.Controls.Add(lblRadioBtn)
            td2.Controls.Add(rdBtn)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region




#Region " Populate Controls for Heating"
        Public Sub populateHeatingControl(ByVal itemParamId As Integer, ByRef control As Control, ByVal heatingMappingId As Integer)
            ' Dim sa As textBo
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim ddllist As DropDownList = CType(control, DropDownList)
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID") = itemParamId AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)
            If preInsertedResult.Count > 0 Then

                ddllist.SelectedValue = preInsertedResult.First.Item("VALUEID").ToString()
                If ddllist.SelectedItem.Text <> "Please Select" Then


                    If SessionManager.getValueLifeCycle() = True Then
                        SessionManager.setValueId(preInsertedResult.First.Item("VALUEID"))

                        Dim isMappedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = preInsertedResult.First.Item("VALUEID") And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable

                        If isMappedResult.Count > 0 Then

                            SessionManager.setIsMapped(False)
                            If SessionManager.getpreviousValueId() Is Nothing Then
                                SessionManager.setpreviousValueId(preInsertedResult.First.Item("VALUEID"))
                            End If

                        Else
                            Dim previousValueId = SessionManager.getpreviousValueId()
                            If previousValueId > 0 Then
                                Dim valId As Integer = preInsertedResult.First.Item("VALUEID")
                                Dim loadComponentDv As DataView = New DataView()
                                Dim dtLoadComponent As DataTable = New DataTable()
                                dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                                loadComponentDv = dtLoadComponent.AsDataView()
                                loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valId.ToString()
                                dtLoadComponent = loadComponentDv.ToTable()
                                If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                                    Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valId Select ps)
                                    If ipreviousResult.Count > 0 Then
                                        SessionManager.setIsMapped(True)
                                    End If
                                End If
                            Else
                                Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = preInsertedResult.First.Item("VALUEID") And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                                If iResult.Count > 0 Then
                                    SessionManager.setIsMapped(True)
                                Else
                                    SessionManager.setIsMapped(False)
                                End If
                            End If


                        End If
                    Else
                        SessionManager.removeValueId()
                    End If
                End If
            Else
                ddllist.SelectedValue = "-1"
                'SessionManager.setIsMapped(False)


            End If
            SessionManager.setIsDdlChangeEvent(False)
            SessionManager.setValueLifeCycle(False)


        End Sub
#End Region

        Public Sub reloadManufacturer(ByRef ddlManufacturer As DropDownList)

            Dim HeatingFuelId As String = SessionManager.getHeatingFuelId()
            Dim selectedHeatingType As String

            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim heatingFuelResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parametervalues) Where ps.Item("ValueId").ToString() = HeatingFuelId.ToString() Select ps)
            selectedHeatingType = heatingFuelResult.FirstOrDefault().Item("ValueDetail").ToString()
            Dim manufacturerResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterName").ToString() = "Manufacturer" Select ps)
            If manufacturerResult.Count > 0 Then
                Dim manufacturerId As String = manufacturerResult.First.Item("PARAMETERID")

                ' Dim selectedHeatingType As String = CType(sender, DropDownList).SelectedItem.Text
                If selectedHeatingType = "Mains Gas" Or selectedHeatingType = "Oil" Then
                    Dim manufacturerValuesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parametervalues) Where ps.Item("PARAMETERID").ToString() = manufacturerId.ToString() And ps.Item("IsAlterNativeHeating").ToString() = "" Select ps)
                    If manufacturerValuesResult.Count > 0 Then
                        ' Dim ddlManufacturer As DropDownList = CType(sender.FindControl(ApplicationConstants.ddlParameter + manufacturerId.ToString()), DropDownList)
                        Dim parameterValues As DataTable = manufacturerValuesResult.CopyToDataTable()
                        ddlManufacturer.DataSource = parameterValues
                        ddlManufacturer.DataValueField = "ValueID"
                        ddlManufacturer.DataTextField = "ValueDetail"
                        ddlManufacturer.DataBind()
                        ddlManufacturer.SelectedIndex = 0
                    End If
                Else
                    Dim manufacturerValuesResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parametervalues) Where ps.Item("PARAMETERID").ToString() = manufacturerId.ToString() And ps.Item("IsAlterNativeHeating").ToString() = "True" Select ps)
                    If manufacturerValuesResult.Count > 0 Then
                        ' Dim ddlManufacturer As DropDownList = CType(sender.FindControl(ApplicationConstants.ddlParameter + manufacturerId.ToString()), DropDownList)
                        Dim parameterValues As DataTable = manufacturerValuesResult.CopyToDataTable()
                        ddlManufacturer.DataSource = parameterValues
                        ddlManufacturer.DataValueField = "ValueID"
                        ddlManufacturer.DataTextField = "ValueDetail"
                        ddlManufacturer.DataBind()
                        ddlManufacturer.SelectedIndex = 0
                    End If
                End If
            End If
        End Sub





#End Region
    End Class
End Namespace