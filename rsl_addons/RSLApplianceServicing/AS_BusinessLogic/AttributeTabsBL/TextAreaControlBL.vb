﻿Imports System.Web.UI.WebControls
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class TextAreaControlBL
        Implements IControlBL

#Region "Functions"

#Region "Create controls"

        ''' <summary>
        ''' Create dynamic text box 
        ''' </summary>
        ''' <param name="parameterId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            'Dim loadParameterDDL As DataTable = New DataTable()
            Dim txtArea As TextBox = New TextBox()
            txtArea.ID = ApplicationConstants.txtArea + parameterId.ToString()
            txtArea.CssClass = ApplicationConstants.textBoxStyle
            txtArea.TextMode = TextBoxMode.MultiLine
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)
            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                If heatingParamId > 0 Then
                    populateHeatingControl(attributeResult.First.Item("ItemParamID"), txtArea, heatingParamId)
                End If

            Else
                populateControl(attributeResult.First.Item("ItemParamID"), txtArea)
            End If


            Dim lblTextBox As Label = New Label()
            Dim tr As TableRow = New TableRow()
            'tr.Attributes.Add("ID", SessionManager.getWorksRequiredRowId())
            ' SessionManager.setWorksRequiredRowId(tr.ID.ToString())
            ' tr.Attributes.Add("runat", "server")
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            'td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            'td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblTextBox.ID = ApplicationConstants.lblTextBox + parameterId.ToString()
            lblTextBox.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            td1.Controls.Add(lblTextBox)
            td2.Controls.Add(txtArea)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            'If SessionManager.getIsWorkSatisfactory() = True Then
            '    tr.Style.Add("display", "none")
            'End If

            Return tr
        End Function

#End Region

#Region "Create controls"
        ''' <summary>
        ''' Create dynamic text box 
        ''' </summary>
        ''' <param name="parameterId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function createWorksRequiredControl(ByVal parameterId As Integer) As TableRow
            'Dim loadParameterDDL As DataTable = New DataTable()
            Dim txtArea As TextBox = New TextBox()
            txtArea.ID = ApplicationConstants.txtArea + parameterId.ToString()
            txtArea.CssClass = ApplicationConstants.textBoxStyle
            txtArea.TextMode = TextBoxMode.MultiLine
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId Select ps)

            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                populateHeatingControl(attributeResult.First.Item("ItemParamID"), txtArea, heatingParamId)
            Else
                populateControl(attributeResult.First.Item("ItemParamID"), txtArea)
            End If


            Dim lblTextBox As Label = New Label()
            Dim tr As TableRow = New TableRow()
            tr.Attributes.Add("ID", SessionManager.getWorksRequiredRowId())
            ' SessionManager.setWorksRequiredRowId(tr.ID.ToString())
            ' tr.Attributes.Add("runat", "server")
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblTextBox.ID = ApplicationConstants.lblTextBox + parameterId.ToString()
            lblTextBox.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            td1.Controls.Add(lblTextBox)
            td2.Controls.Add(txtArea)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            If SessionManager.getIsWorkSatisfactory() = True Then
                tr.Style.Add("display", "none")
            End If

            Return tr
        End Function
#End Region

#Region " Populate Controls"
        Public Sub populateControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control) Implements IControlBL.populateControl
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)

            If preInsertedResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                txtParameter.Text = preInsertedResult.First.Item("ParameterValue").ToString()
            End If

        End Sub
#End Region


#Region " Populate Controls"
        Public Sub populateHeatingControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control, ByVal heatingMappingId As Integer)
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)

            If preInsertedResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                txtParameter.Text = preInsertedResult.First.Item("ParameterValue").ToString()
            End If

        End Sub
#End Region

#End Region
    End Class
End Namespace
