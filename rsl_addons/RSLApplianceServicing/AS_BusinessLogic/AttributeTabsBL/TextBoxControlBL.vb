﻿Imports System.Web.UI.WebControls
Imports AS_Utilities

Namespace AS_BusinessLogic
    Public Class TextBoxControlBL
        Implements IControlBL

#Region "Functions"
#Region "Create controls"
        ''' <summary>
        ''' Create dynamic textbox 
        ''' </summary>
        ''' <param name="parameterId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function createControl(ByVal parameterId As Integer, ByVal itemParamId As Integer) As TableRow Implements IControlBL.createControl
            'Dim loadParameterDDL As DataTable = New DataTable()
            Dim txtBox As TextBox = New TextBox()
            txtBox.ID = ApplicationConstants.txt + parameterId.ToString()
            txtBox.CssClass = ApplicationConstants.textBoxStyle
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ItemParamId").ToString() = itemParamId Select ps)

            If SessionManager.getTreeItemName() = "Heating" Then
                Dim heatingParamId = SessionManager.getHeatingMappingId()
                If heatingParamId > 0 Then
                    populateHeatingControl(attributeResult.First.Item("ItemParamID"), txtBox, heatingParamId)
                End If

            Else
                populateControl(attributeResult.First.Item("ItemParamID"), txtBox)
            End If


            If attributeResult.First.Item("DataType").ToString().ToLower() = "Integer".ToLower() Then
                txtBox.Attributes.Add("onkeyup", "NumberOnly(this);")
            End If

            Dim lblTextBox As Label = New Label()
            Dim tr As TableRow = New TableRow()
            'tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            Dim td1 As TableCell = New TableCell()
            td1.HorizontalAlign = HorizontalAlign.Right
            'td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            Dim td2 As TableCell = New TableCell()
            'td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblTextBox.ID = ApplicationConstants.lblTextBox + parameterId.ToString()
            lblTextBox.Text = attributeResult.First.Item("ParameterName").ToString() + ": "
            td1.Controls.Add(lblTextBox)
            td1.Attributes.Add("Width", "150")
            td2.Controls.Add(txtBox)
            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tr.Height = "30"
            Return tr
        End Function
#End Region
#Region " Populate Controls"
        Public Sub populateControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control) Implements IControlBL.populateControl
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId Select ps)

            If preInsertedResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                txtParameter.Text = preInsertedResult.First.Item("ParameterValue").ToString()
            End If

        End Sub
#End Region



#Region " Populate Heating Controls"
        Public Sub populateHeatingControl(ByVal itemParamId As Integer, ByRef control As Web.UI.Control, ByVal heatingMappingId As Integer)
            Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
            Dim preInsertedResult = (From ps In parameterDataSet.Tables(ApplicationConstants.preinsertedvalues) Where ps.Item("ItemParamID").ToString() = itemParamId AndAlso ps.Item("HeatingMappingId") = heatingMappingId Select ps)

            If preInsertedResult.Count > 0 Then
                Dim txtParameter As TextBox = CType(control, TextBox)
                txtParameter.Text = preInsertedResult.First.Item("ParameterValue").ToString()
            End If

        End Sub
#End Region
#End Region
    End Class
End Namespace
