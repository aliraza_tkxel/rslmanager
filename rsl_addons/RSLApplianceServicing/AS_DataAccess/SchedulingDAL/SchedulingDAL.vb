﻿Imports System
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess
    Public Class SchedulingDAL : Inherits BaseDAL

#Region "Functions"

#Region "get Appointment To Be Arranged List"
        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal fuelType As Integer, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentToBeArranged As String = SpNameConstants.GetAppointmentToBeArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            'Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", fuelType, DbType.Int32)
            'parametersList.Add(fuelTypeParam)

            Dim statusParam As ParameterBO = New ParameterBO("status", status, DbType.String)
            parametersList.Add(statusParam)

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentToBeArranged)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Alternative Appointment To Be Arranged List"
        Public Function GetAlternativeAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal alternativeFuelType As Integer, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentToBeArranged As String = SpNameConstants.AlternativeAppointmentsToBeArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("alternativeFuelType", alternativeFuelType, DbType.Int32)
            parametersList.Add(fuelTypeParam)

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim statusParam As ParameterBO = New ParameterBO("status", status, DbType.String)
            parametersList.Add(statusParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentToBeArranged)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Oil Appointment To Be Arranged List"
        Public Function GetOilAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchText As String, ByRef objPageSortBo As PageSortBO, ByVal status As String, ByVal patchId As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentToBeArranged As String = SpNameConstants.OilAppointmentsToBeArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim statusParam As ParameterBO = New ParameterBO("status", status, DbType.String)
            parametersList.Add(statusParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentToBeArranged)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Heating Info For Alternative"
        Public Sub GetHeatingInfoForAlternative(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim spGetHeatingInfoForAlternative As String = SpNameConstants.GetHeatingInfoForAlternative

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetHeatingInfoForAlternative)
        End Sub
#End Region

#Region "Get Heating Info For Oil"
        Public Sub GetHeatingInfoForOil(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim spGetHeatingInfoForOil As String = SpNameConstants.GetHeatingInfoForOil

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetHeatingInfoForOil)
        End Sub
#End Region


#Region "Get Boilers Detail of Block or Scheme"
        Public Sub getSchemeBlockBoilersInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer, ByVal appointmentType As String)

            Dim spGetSchemeBlockBoilersInfo As String = SpNameConstants.GetSchemeBlockBoilersInfo

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("Id", journalId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetSchemeBlockBoilersInfo)
        End Sub
#End Region

#Region "Get Alternative Appointments By PropertyId"
        Public Sub GetAlternativeAppointmentsByPropertyId(ByVal propertyId As String, ByRef resultDataSet As DataSet)

            Dim spGetAlternativeAppointmentsByProperty As String = SpNameConstants.GetAlternativeAppointmentsByProperty

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetAlternativeAppointmentsByProperty)
        End Sub

#End Region

#Region "get Property schedule status"
        Public Function getPropertyschedulestatus(ByVal propertyId As String, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Dim spGetPropertyScheduleStatus As String = SpNameConstants.GetPropertyScheduleStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pId As ParameterBO = New ParameterBO("pId", propertyId, DbType.String)
            parametersList.Add(pId)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim TimeExceeded As ParameterBO = New ParameterBO("isTimeExceeded", 0, DbType.Int32)
            outParametersList.Add(TimeExceeded)
            Dim scheduled As ParameterBO = New ParameterBO("isScheduled", 0, DbType.Int32)
            outParametersList.Add(scheduled)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.SelectRecord(parametersList, outParametersList, spGetPropertyScheduleStatus)

            If (outParametersList.Item(1).Value = 1) Then
                istimeexceeded = True
            End If
            If (outParametersList.Item(2).Value = 1) Then
                isScheduled = 1
            Else
                isScheduled = 0
            End If
            Return outParametersList.Item(0).Value


        End Function

#End Region

#Region "get Arranged Property schedule status"
        Public Function getArrangedPropertyschedulestatus(ByVal propertyId As String, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Dim spGetPropertyScheduleStatus As String = SpNameConstants.GetArrangedPropertyScheduleStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pId As ParameterBO = New ParameterBO("pId", propertyId, DbType.String)
            parametersList.Add(pId)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim TimeExceeded As ParameterBO = New ParameterBO("isTimeExceeded", 0, DbType.Int32)
            outParametersList.Add(TimeExceeded)
            Dim scheduled As ParameterBO = New ParameterBO("isScheduled", 0, DbType.Int32)
            outParametersList.Add(scheduled)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.SelectRecord(parametersList, outParametersList, spGetPropertyScheduleStatus)

            If (outParametersList.Item(1).Value = 1) Then
                istimeexceeded = True
            End If
            If (outParametersList.Item(2).Value = 1) Then
                isScheduled = 1
            Else
                isScheduled = 0
            End If
            Return outParametersList.Item(0).Value


        End Function

#End Region

#Region "set Property schedule status"
        Public Function setPropertyschedulestatus(ByVal propertyId As String, ByVal status As Integer, ByRef schedulestatusProp As Integer, ByVal lockedBy As Integer, ByVal appointmentType As String)


            Dim spGetPropertyScheduleStatus As String = SpNameConstants.SetPropertyScheduleStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pId As ParameterBO = New ParameterBO("pId", propertyId, DbType.String)
            parametersList.Add(pId)

            Dim status1 As ParameterBO = New ParameterBO("status", status, DbType.Int32)
            parametersList.Add(status1)
            Dim ischeduled As ParameterBO = New ParameterBO("scheduled", schedulestatusProp, DbType.Int32)
            parametersList.Add(ischeduled)
            Dim lockby As ParameterBO = New ParameterBO("lockedBy", lockedBy, DbType.Int32)
            parametersList.Add(lockby)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim lock As ParameterBO = New ParameterBO("lock", 0, DbType.Int32)
            outParametersList.Add(lock)

            MyBase.SelectRecord(parametersList, outParametersList, spGetPropertyScheduleStatus)
            schedulestatusProp = outParametersList.Item(0).Value
            Return outParametersList.Item(1).Value


        End Function

#End Region

#Region "get Property Locked By"
        Public Function getPropertyLockedBy(ByVal propertyId As String, ByVal appointmentType As String)

            Dim spGetPropertyLockedBy As String = SpNameConstants.GetPropertyLockedBy

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pId As ParameterBO = New ParameterBO("pId", propertyId, DbType.String)
            parametersList.Add(pId)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            Dim LockedUser As ParameterBO = New ParameterBO("LockedUser", 0, DbType.Int32)
            outParametersList.Add(LockedUser)

            MyBase.SelectRecord(parametersList, outParametersList, spGetPropertyLockedBy)

            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "get void Appointment To Be Arranged List"
        Public Function getVoidAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim spGetAppointmentToBeArranged As String = SpNameConstants.GetVoidAppointmentToBeArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentToBeArranged)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "get Risk List"
        Public Sub getRiskList(ByRef resultDataSet As DataSet, ByVal customerId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim customerIdParam As ParameterBO = New ParameterBO("@customerId", customerId, DbType.Int32)
            parametersList.Add(customerIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetRiskList)

        End Sub

#End Region

#Region "get abort reason"
        Public Sub getAbortReason(ByRef resultDataSet As DataSet, ByVal journalId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim customerIdParam As ParameterBO = New ParameterBO("@journalId", journalId, DbType.Int32)
            parametersList.Add(customerIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAbortReason)

        End Sub

#End Region

#Region "get Appointments Arranged"
        Public Function getAppointmentArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal fuelType As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentArranged As String = SpNameConstants.GetAppointmentsArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchedText, DbType.String)
            parametersList.Add(searchedValueParam)
            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)
            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)
            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            'Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", fuelType, DbType.Int32)
            'parametersList.Add(fuelTypeParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentArranged)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get Alternative Appointments Arranged"
        Public Function GetAlternativeAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal fuelType As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentArranged As String = SpNameConstants.AlternativeAppointmentsArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchedText, DbType.String)
            parametersList.Add(searchedValueParam)
            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)
            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", fuelType, DbType.Int32)
            parametersList.Add(fuelTypeParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentArranged)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get Oil Appointments Arranged"
        Public Function GetOilAppointmentsArrangedList(ByRef resultDataSet As DataSet, ByVal check56Days As Boolean, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO, ByVal patchId As Integer, ByVal schemeId As Integer, ByVal developmentId As Integer)

            Dim spGetAppointmentArranged As String = SpNameConstants.OilAppointmentsArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchedText, DbType.String)
            parametersList.Add(searchedValueParam)
            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)
            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", patchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", developmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentArranged)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get Void Appointments Arranged"
        Public Function getVoidAppointmentArrangedList(ByRef resultDataSet As DataSet, ByVal searchedText As String, ByRef objPageSortBo As PageSortBO)

            Dim spGetAppointmentArranged As String = SpNameConstants.GetVoidAppointmentsArranged

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchedText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetAppointmentArranged)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Appointment Detail"
        Public Sub getAppointmentDetail(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            Dim spGetAppointmentDetail As String = SpNameConstants.GetAppointmentDetail

            Dim parametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetAppointmentDetail)

        End Sub
#End Region

#Region "Save Appointment Notes"
        Public Sub saveAppointmentNotes(ByVal appointmentId As Integer, ByVal notes As String)

            Dim spSaveAppointmentNotes As String = SpNameConstants.SaveAppointmentNotes

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", notes, DbType.String)
            parametersList.Add(notesParam)

            MyBase.SelectRecord(parametersList, outParametersList, spSaveAppointmentNotes)

        End Sub
#End Region

#Region " get Searched Values"
        Sub getSeacrhedValues(ByVal resultDataSet As DataSet, ByVal searchedText As String)

            Dim spGetAppointmentToBeArranged As String = SpNameConstants.GetAppointmentToBeArranged
            Dim offset As Int32 = 0
            Dim noOfRows As Int32 = 10
            Dim address As String = String.Empty
            Dim postCode As String = String.Empty
            Dim patchId As Integer = 0
            Dim schemeId As Int32 = 0
            Dim dueDate As DateTime = DateTime.Now
            Dim fuelType As String = String.Empty
            Dim check56Days As Boolean = 0
            Dim sortExpression As String = String.Empty
            Dim sortDirection As String = String.Empty
            searchedText = "Garage"

            Dim parametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchedText, DbType.String)
            parametersList.Add(searchedValueParam)
            Dim check56DaysParam As ParameterBO = New ParameterBO("check56Days", check56Days, DbType.Boolean)
            parametersList.Add(check56DaysParam)
            Dim offsetParam As ParameterBO = New ParameterBO("offset", offset, DbType.Int32)
            parametersList.Add(offsetParam)
            Dim pageNumberParam As ParameterBO = New ParameterBO("noOfRows", noOfRows, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim addressParam As ParameterBO = New ParameterBO("address", address, DbType.String)
            parametersList.Add(addressParam)
            Dim postCodeParam As ParameterBO = New ParameterBO("postCode", postCode, DbType.String)
            parametersList.Add(postCodeParam)
            Dim patchParam As ParameterBO = New ParameterBO("patch", patchId, DbType.Int32)
            parametersList.Add(patchParam)

            Dim schemeParam As ParameterBO = New ParameterBO("scheme", schemeId, DbType.Int32)
            parametersList.Add(schemeParam)
            Dim dueDateParam As ParameterBO = New ParameterBO("dueDate", dueDate, DbType.Date)
            parametersList.Add(dueDateParam)
            Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", fuelType, DbType.String)
            parametersList.Add(fuelTypeParam)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", sortExpression, DbType.String)
            parametersList.Add(sortColumn)
            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", sortDirection, DbType.String)
            parametersList.Add(sortOrder)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetAppointmentToBeArranged)

        End Sub
#End Region

#Region "get Engineers Info for Appointment"
        Sub getEngineersListForAppointment(ByRef resultDataSet)
            Dim empId As Int32 = 0
            Dim empName As String = String.Empty
            Dim journelId As Int32 = 0
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentEngineersInfo)
        End Sub
#End Region

#Region "get Scheduled Appointments Details"
        Sub getScheduledAppointmentsDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim shift As String = String.Empty
            Dim appointmentDate As Date = Date.Now
            Dim custName As String = String.Empty
            Dim cellNo As String = String.Empty
            Dim address As String = String.Empty
            Dim zipCode As String = String.Empty
            Dim appointmentStartTime As String = String.Empty
            Dim appointmentEndTime As String = String.Empty
            Dim parametersList As ParameterList = New ParameterList()
            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objScheduleAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim returnDateParam As ParameterBO = New ParameterBO("endDate", objScheduleAppointmentsBO.ReturnDate, DbType.Date)
            parametersList.Add(returnDateParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetScheduledAppointmentsDetail)
        End Sub
#End Region

#Region "get Engineer Leaves Details"
        Sub getEngineerLeavesDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objScheduleAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim returnDateParam As ParameterBO = New ParameterBO("returnDate", objScheduleAppointmentsBO.ReturnDate, DbType.Date)
            parametersList.Add(returnDateParam)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeIds", objScheduleAppointmentsBO.EmpolyeeIds, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetEngineersLeaveInformation)
        End Sub
#End Region

#Region "Create New Appointment"

        Public Function saveAppointmentInfo(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef savedLetterDt As DataTable, ByRef appointmentId As Int32) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", objScheduleAppointmentsBO.JournelId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim isVoidParam As ParameterBO = New ParameterBO("isVoid", objScheduleAppointmentsBO.IsVoid, DbType.Int32)
            parametersList.Add(isVoidParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objScheduleAppointmentsBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objScheduleAppointmentsBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As ParameterBO = New ParameterBO("blockId", objScheduleAppointmentsBO.BlockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objScheduleAppointmentsBO.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim insepctionTypeIdParam As ParameterBO = New ParameterBO("inspectionTypeId", objScheduleAppointmentsBO.InspectionTypeId, DbType.Int32)
            parametersList.Add(insepctionTypeIdParam)

            Dim createDateParam As ParameterBO = New ParameterBO("appointmentDate", objScheduleAppointmentsBO.AppointmentDate, DbType.Date)
            parametersList.Add(createDateParam)

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", objScheduleAppointmentsBO.StatusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", objScheduleAppointmentsBO.ActionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objScheduleAppointmentsBO.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim isLetterAttachParam As ParameterBO = New ParameterBO("isLetterAttached", objScheduleAppointmentsBO.IsLetterAttached, DbType.Boolean)
            parametersList.Add(isLetterAttachParam)

            Dim isDocAttachParam As ParameterBO = New ParameterBO("isDocumentAttached", objScheduleAppointmentsBO.IsDocumentAttached, DbType.Boolean)
            parametersList.Add(isDocAttachParam)

            Dim appStartTime As ParameterBO = New ParameterBO("appStartTime", objScheduleAppointmentsBO.AppStartTime, DbType.String)
            parametersList.Add(appStartTime)

            Dim appEndTime As ParameterBO = New ParameterBO("appEndTime", objScheduleAppointmentsBO.AppEndTime, DbType.String)
            parametersList.Add(appEndTime)

            Dim assignedToParam As ParameterBO = New ParameterBO("assignedTo", objScheduleAppointmentsBO.AssignedTo, DbType.Int32)
            parametersList.Add(assignedToParam)

            Dim shiftParam As ParameterBO = New ParameterBO("shift", objScheduleAppointmentsBO.Shift, DbType.String)
            parametersList.Add(shiftParam)

            If (String.IsNullOrEmpty(objScheduleAppointmentsBO.PropertyId)) Then
                Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", DBNull.Value, DbType.Int32)
                parametersList.Add(tenancyIdParam)
            Else
                Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objScheduleAppointmentsBO.TenancyId, DbType.Int32)
                parametersList.Add(tenancyIdParam)
            End If

            Dim durationParam As ParameterBO = New ParameterBO("duration", objScheduleAppointmentsBO.Duration, DbType.Int32)
            parametersList.Add(durationParam)

            Dim customerEmailParam As ParameterBO = New ParameterBO("customerEmail", objScheduleAppointmentsBO.CustomerEmail, DbType.String)
            parametersList.Add(customerEmailParam)

            Dim customerEmailStatusParam As ParameterBO = New ParameterBO("customerEmailStatus", objScheduleAppointmentsBO.CustomerEmailStatus, DbType.String)
            parametersList.Add(customerEmailStatusParam)

            Dim appointmentEndDateParam As ParameterBO = New ParameterBO("appointmentEndDate", objScheduleAppointmentsBO.AppointmentEndDate, DbType.Date)
            parametersList.Add(appointmentEndDateParam)

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", 0, DbType.Int32)
            outParametersList.Add(journalHistoryIdParam)

            Dim apptIdIdParam As ParameterBO = New ParameterBO("apptId", 0, DbType.Int32)
            outParametersList.Add(apptIdIdParam)

            'This procedure will save the record in journal history and it 'll update the record in journal 
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveAppointment)

            Dim journalHistoryId As Integer = CType(outParametersList.Item(0).Value, Int32)
            appointmentId = CType(outParametersList.Item(1).Value, Int32)

            'Now we 'll use this journal history id to save the letters
            If (journalHistoryId > 0) Then
                If (Not IsNothing(objScheduleAppointmentsBO.LetterList)) Then
                    Dim row As DataRow
                    For Each row In savedLetterDt.Rows()
                        Dim letterParamList As ParameterList = New ParameterList()
                        Dim outLetterParamList As ParameterList = New ParameterList()

                        Dim journalHistIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        letterParamList.Add(journalHistIdParam)

                        Dim titleParam As ParameterBO = New ParameterBO("title", row.Item(ApplicationConstants.LetterTextTitleColumn), DbType.String)
                        letterParamList.Add(titleParam)

                        Dim bodyParam As ParameterBO = New ParameterBO("body", row.Item(ApplicationConstants.LetterBodyColumn), DbType.String)
                        letterParamList.Add(bodyParam)

                        Dim stLetterIdParam As ParameterBO = New ParameterBO("standardLetterId", CType(row.Item(ApplicationConstants.StandardLetterId), Integer), DbType.Int32)
                        letterParamList.Add(stLetterIdParam)

                        Dim teamIdParam As ParameterBO = New ParameterBO("teamId", CType(row.Item(ApplicationConstants.TeamIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(teamIdParam)

                        Dim resourceIdParam As ParameterBO = New ParameterBO("resourceId", CType(row.Item(ApplicationConstants.FromResourceIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(resourceIdParam)

                        Dim signOffParam As ParameterBO = New ParameterBO("signOffId", CType(row.Item(ApplicationConstants.SignOffCodeColumn), Integer), DbType.Int32)
                        letterParamList.Add(signOffParam)

                        Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", CType(row.Item(ApplicationConstants.RentBalanceColumn), Double), DbType.Double)
                        letterParamList.Add(rentBalanceParam)

                        Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", CType(row.Item(ApplicationConstants.RentChargeColumn), Double), DbType.Double)
                        letterParamList.Add(rentChargeParam)

                        Dim dateParam As ParameterBO = New ParameterBO("todayDate", CType(row.Item(ApplicationConstants.TodayDateColumn), Date), DbType.Date)
                        letterParamList.Add(dateParam)

                        MyBase.SelectRecord(letterParamList, outLetterParamList, SpNameConstants.SaveEditedLetter)
                    Next
                End If

                If (Not IsNothing(objScheduleAppointmentsBO.DocList)) Then
                    For Each item As String In objScheduleAppointmentsBO.DocList
                        'Save the documents against journal history id
                        Dim docParamList As ParameterList = New ParameterList()
                        Dim outDocParamList As ParameterList = New ParameterList()

                        Dim journalHistIdDocParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        docParamList.Add(journalHistIdDocParam)

                        Dim docNameParam As ParameterBO = New ParameterBO("documentName", item, DbType.String)
                        docParamList.Add(docNameParam)

                        Dim docPathParam As ParameterBO = New ParameterBO("documentPath", objScheduleAppointmentsBO.DocumentPath, DbType.String)
                        docParamList.Add(docPathParam)

                        MyBase.SelectRecord(docParamList, outDocParamList, SpNameConstants.SaveDocuments)
                    Next
                End If


            End If

            Return journalHistoryId.ToString()
        End Function

#End Region

#Region "Save email and sms status for appointment "
        Public Sub Saveemailsmsstatusforappointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
            Dim spSaveSmsAndEmailStatus As String = SpNameConstants.SaveSmsAndEmailStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
            parametersList.Add(journalHistoryIdParam)
            Dim smsStatusParam As ParameterBO = New ParameterBO("smsStatus", smsStatus, DbType.String)
            parametersList.Add(smsStatusParam)
            Dim smsDescriptionParam As ParameterBO = New ParameterBO("smsDescription", smsDescription, DbType.String)
            parametersList.Add(smsDescriptionParam)
            Dim emailStatusParam As ParameterBO = New ParameterBO("emailStatus", emailStatus, DbType.String)
            parametersList.Add(emailStatusParam)
            Dim emailDescriptionParam As ParameterBO = New ParameterBO("emailDescription", emailDescription, DbType.String)
            parametersList.Add(emailDescriptionParam)
            MyBase.SelectRecord(parametersList, outParametersList, spSaveSmsAndEmailStatus)

        End Sub

#End Region

#Region "Save push noticification status for appointment "
        Public Sub Savepushnoticificationstatusforappointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
            Dim spPushNoticificationStatus As String = SpNameConstants.SavePushNoticificationStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
            parametersList.Add(journalHistoryIdParam)
            Dim smsStatusParam As ParameterBO = New ParameterBO("pushnoticificationStatus", pushnoticificationStatus, DbType.String)
            parametersList.Add(smsStatusParam)
            Dim smsDescriptionParam As ParameterBO = New ParameterBO("pushnoticificationDescription", pushnoticificationDescription, DbType.String)
            parametersList.Add(smsDescriptionParam)
            MyBase.SelectRecord(parametersList, outParametersList, spPushNoticificationStatus)

        End Sub

#End Region

#Region "Amend Appointment"

        Public Function AmendAppointmentInfo(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef savedLetterDt As DataTable) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", objScheduleAppointmentsBO.JournelId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objScheduleAppointmentsBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objScheduleAppointmentsBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As ParameterBO = New ParameterBO("blockId", objScheduleAppointmentsBO.BlockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objScheduleAppointmentsBO.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim insepctionTypeIdParam As ParameterBO = New ParameterBO("inspectionTypeId", objScheduleAppointmentsBO.InspectionTypeId, DbType.Int32)
            parametersList.Add(insepctionTypeIdParam)

            Dim createDateParam As ParameterBO = New ParameterBO("appointmentDate", objScheduleAppointmentsBO.AppointmentDate, DbType.Date)
            parametersList.Add(createDateParam)

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", objScheduleAppointmentsBO.StatusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", objScheduleAppointmentsBO.ActionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objScheduleAppointmentsBO.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim isLetterAttachParam As ParameterBO = New ParameterBO("isLetterAttached", objScheduleAppointmentsBO.IsLetterAttached, DbType.Boolean)
            parametersList.Add(isLetterAttachParam)

            Dim isDocAttachParam As ParameterBO = New ParameterBO("isDocumentAttached", objScheduleAppointmentsBO.IsDocumentAttached, DbType.Boolean)
            parametersList.Add(isDocAttachParam)

            Dim appStartTime As ParameterBO = New ParameterBO("appStartTime", objScheduleAppointmentsBO.AppStartTime, DbType.String)
            parametersList.Add(appStartTime)

            Dim appEndTime As ParameterBO = New ParameterBO("appEndTime", objScheduleAppointmentsBO.AppEndTime, DbType.String)
            parametersList.Add(appEndTime)

            Dim assignedToParam As ParameterBO = New ParameterBO("assignedTo", objScheduleAppointmentsBO.AssignedTo, DbType.Int32)
            parametersList.Add(assignedToParam)

            Dim shiftParam As ParameterBO = New ParameterBO("shift", objScheduleAppointmentsBO.Shift, DbType.String)
            parametersList.Add(shiftParam)

            'Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyId", objScheduleAppointmentsBO.TenancyId, DbType.Int32)
            'parametersList.Add(tenancyIdParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", objScheduleAppointmentsBO.AppointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim customerEmailParam As ParameterBO = New ParameterBO("customerEmail", objScheduleAppointmentsBO.CustomerEmail, DbType.String)
            parametersList.Add(customerEmailParam)

            Dim customerEmailStatusParam As ParameterBO = New ParameterBO("customerEmailStatus", objScheduleAppointmentsBO.CustomerEmailStatus, DbType.String)
            parametersList.Add(customerEmailStatusParam)

            Dim appointmentEndDateParam As ParameterBO = New ParameterBO("appointmentEndDate", objScheduleAppointmentsBO.AppointmentEndDate, DbType.Date)
            parametersList.Add(appointmentEndDateParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objScheduleAppointmentsBO.Duration, DbType.Int32)
            parametersList.Add(durationParam)

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", 0, DbType.Int32)
            outParametersList.Add(journalHistoryIdParam)

            'This procedure will save the record in journal history and it 'll update the record in journal 
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.AmendAppointment)

            Dim journalHistoryId As Integer = CType(outParametersList.Item(0).Value, Int32)

            'Now we 'll use this journal history id to save the letters
            If (journalHistoryId > 0) Then
                If (Not IsNothing(objScheduleAppointmentsBO.LetterList)) Then
                    Dim row As DataRow
                    For Each row In savedLetterDt.Rows()
                        Dim letterParamList As ParameterList = New ParameterList()
                        Dim outLetterParamList As ParameterList = New ParameterList()

                        Dim journalHistIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        letterParamList.Add(journalHistIdParam)

                        Dim titleParam As ParameterBO = New ParameterBO("title", row.Item(ApplicationConstants.LetterTextTitleColumn), DbType.String)
                        letterParamList.Add(titleParam)

                        Dim bodyParam As ParameterBO = New ParameterBO("body", row.Item(ApplicationConstants.LetterBodyColumn), DbType.String)
                        letterParamList.Add(bodyParam)

                        Dim stLetterIdParam As ParameterBO = New ParameterBO("standardLetterId", CType(row.Item(ApplicationConstants.StandardLetterId), Integer), DbType.Int32)
                        letterParamList.Add(stLetterIdParam)

                        Dim teamIdParam As ParameterBO = New ParameterBO("teamId", CType(row.Item(ApplicationConstants.TeamIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(teamIdParam)

                        Dim resourceIdParam As ParameterBO = New ParameterBO("resourceId", CType(row.Item(ApplicationConstants.FromResourceIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(resourceIdParam)

                        Dim signOffParam As ParameterBO = New ParameterBO("signOffId", CType(row.Item(ApplicationConstants.SignOffCodeColumn), Integer), DbType.Int32)
                        letterParamList.Add(signOffParam)

                        Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", CType(row.Item(ApplicationConstants.RentBalanceColumn), Double), DbType.Double)
                        letterParamList.Add(rentBalanceParam)

                        Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", CType(row.Item(ApplicationConstants.RentChargeColumn), Double), DbType.Double)
                        letterParamList.Add(rentChargeParam)

                        Dim dateParam As ParameterBO = New ParameterBO("todayDate", CType(row.Item(ApplicationConstants.TodayDateColumn), Date), DbType.Date)
                        letterParamList.Add(dateParam)

                        MyBase.SelectRecord(letterParamList, outLetterParamList, SpNameConstants.SaveEditedLetter)
                    Next
                End If

                If (Not IsNothing(objScheduleAppointmentsBO.DocList)) Then
                    For Each item As String In objScheduleAppointmentsBO.DocList
                        'Save the documents against journal history id
                        Dim docParamList As ParameterList = New ParameterList()
                        Dim outDocParamList As ParameterList = New ParameterList()

                        Dim journalHistIdDocParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        docParamList.Add(journalHistIdDocParam)

                        Dim docNameParam As ParameterBO = New ParameterBO("documentName", item, DbType.String)
                        docParamList.Add(docNameParam)

                        Dim docPathParam As ParameterBO = New ParameterBO("documentPath", objScheduleAppointmentsBO.DocumentPath, DbType.String)
                        docParamList.Add(docPathParam)

                        MyBase.SelectRecord(docParamList, outDocParamList, SpNameConstants.SaveDocuments)
                    Next
                End If

            End If

            Return journalHistoryId.ToString()
        End Function

#End Region

#Region "get Selected Appointment Details"
        Sub getSelectedAppointmentDetails(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)
            Dim parametersList As ParameterList = New ParameterList()

            Dim pageNumberParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(pageNumberParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSelectedAppointmentInfo)
        End Sub
#End Region

#Region "Get Tenant(s) Info by TenancyId - Joint Tenancy"
        'Start - Changed By Aamir Waheed on May 28, 2013.
        'To implement joint tenants information.
        Sub GetJointTenantsInfoByTenancyID(ByRef dstenantsInfo As DataSet, ByRef tenancyID As Integer)

            Dim parametersList As New ParameterList()

            Dim tenancyIDParam As New ParameterBO("tenancyID", tenancyID, DbType.Int32)
            parametersList.Add(tenancyIDParam)

            MyBase.LoadDataSet(dstenantsInfo, parametersList, SpNameConstants.GetJointTenantsInfoByTenancyID)

        End Sub
        'End - Changed By Aamir Waheed on May 28, 2013.

#End Region

#Region "Risk & Vulnerability of Customer"
        Public Sub getCustomerRiskVulnerability(ByRef riskVulnerabilityResult As DataSet, ByVal customerId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", customerId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim riskDt As DataTable = New DataTable(ApplicationConstants.RiskDt)
            Dim vulnerabilityDt As DataTable = New DataTable(ApplicationConstants.VulnerabilityDt)

            riskVulnerabilityResult.Tables.Add(riskDt)
            riskVulnerabilityResult.Tables.Add(vulnerabilityDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetCustomerRiskVulnerability)
            riskVulnerabilityResult.Load(lResultDataReader, LoadOption.OverwriteChanges, riskDt, vulnerabilityDt)
        End Sub

#End Region

#Region "Intelligent Scheduling Functions"
#Region "get Available Operatives"
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim employeeDt As DataTable = New DataTable(ApplicationConstants.OperativesDt)
            Dim leavesDt As DataTable = New DataTable(ApplicationConstants.LeavesDt)
            Dim appointmentsDt As DataTable = New DataTable(ApplicationConstants.AppointmentsDt)

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAvailableOperatives)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)
        End Sub
#End Region

#Region "get Available Operatives For Alternative/Oil Fuel"
        Public Sub GetAvailableOperativesForAlternativeOilFuel(ByRef resultDataSet As DataSet, ByVal fuelServicingType As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim employeeDt As DataTable = New DataTable(ApplicationConstants.OperativesDt)
            Dim leavesDt As DataTable = New DataTable(ApplicationConstants.LeavesDt)
            Dim appointmentsDt As DataTable = New DataTable(ApplicationConstants.AppointmentsDt)

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim fuelServicingTypeParam As ParameterBO = New ParameterBO("fuelServicingType", fuelServicingType, DbType.String)
            parametersList.Add(fuelServicingTypeParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAvailableOperativesForAlternativeOilFuel)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)
        End Sub
#End Region

#End Region

#Region "Get Appointment Info For Email"
        Sub getAppointmentInfoForEmail(ByRef resultDataSet As DataSet, ByVal journalHistoryId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
            parametersList.Add(journalHistoryIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentInfoForEmail)
        End Sub
#End Region

#Region "Populate Job Sheet Summary Page"
        Sub populateJobSheet(ByVal journalHistoryId As Integer, ByRef resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JOURNALHISTORYID", journalHistoryId, DbType.Int32)
            parametersList.Add(journalHistoryIdParam)


            Dim dtAppointmentsInfo As DataTable = New DataTable()
            dtAppointmentsInfo.TableName = "AppointmentInfo"
            resultDataset.Tables.Add(dtAppointmentsInfo)

            Dim dtPropertyInfo As DataTable = New DataTable()
            dtPropertyInfo.TableName = "PropertyInfo"
            resultDataset.Tables.Add(dtPropertyInfo)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJobSheetSummaryDetails)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentsInfo, dtPropertyInfo)
        End Sub
#End Region

#Region "Get risk and vulnerability info"

        ''' <summary>
        ''' Get risk and vulnerability info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="journalId"></param>
        ''' <remarks></remarks>
        Public Sub getRiskAndVulnerabilityInfo(ByRef resultDataSet As DataSet, ByVal journalId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRiskResult As DataTable = New DataTable()
            dtRiskResult.TableName = ApplicationConstants.RiskInfo
            resultDataSet.Tables.Add(dtRiskResult)

            Dim dtVulnerabilityResult As DataTable = New DataTable()
            dtVulnerabilityResult.TableName = ApplicationConstants.VulnerabilityInfo
            resultDataSet.Tables.Add(dtVulnerabilityResult)

            Dim propertyIdParam As ParameterBO = New ParameterBO("@journalId", journalId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRiskAndVulnerabilityInfoByJournalId)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRiskResult, dtVulnerabilityResult)

        End Sub

#End Region

#Region "get operative working hours"
        ''' <summary>
        ''' get operative working hours
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub getOperativeWorkingHours(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal ofcCoreStartTime As String, ByVal ofcCoreEndTime As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim workingHourDataTable As DataTable = New DataTable(ApplicationConstants.WorkingHourDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(tempFaultIdsParam)

            Dim ofcCoreStartTimeParam As ParameterBO = New ParameterBO("defaultStartTime", ofcCoreStartTime, DbType.String)
            parametersList.Add(ofcCoreStartTimeParam)

            Dim ofcCoreEndTimeParam As ParameterBO = New ParameterBO("defaultEndTime", ofcCoreEndTime, DbType.String)
            parametersList.Add(ofcCoreEndTimeParam)

            resultDataSet.Tables.Add(workingHourDataTable)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetOperativeWorkingHours)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, workingHourDataTable)
        End Sub
#End Region

#Region "get Defect Available Operatives"

        ''' <summary>
        ''' This function 'll get the available operatives against faults
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tradeIds"></param>
        ''' <remarks></remarks>
        Public Sub getDefectAvailableOperatives(ByRef resultDataSet As DataSet, ByRef tradeIds As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim employeeDt As DataTable = New DataTable()
            Dim leavesDt As DataTable = New DataTable()
            Dim appointmentsDt As DataTable = New DataTable()

            Dim proeprtyIdParam As ParameterBO = New ParameterBO("tradeIds", tradeIds, DbType.String)
            parametersList.Add(proeprtyIdParam)

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetDefectAvailableOperatives)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)

        End Sub

#End Region

#Region "Get Property Customer Detail"
        ''' <summary>
        ''' Get Property Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyCustomerDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As New ParameterList()

            Dim dtPropertyResult As New DataTable()

            dtPropertyResult.TableName = "PropertyCustomerDetail"

            resultDataSet.Tables.Add(dtPropertyResult)

            Dim propertyIdParam As New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult)

        End Sub
#End Region

#Region "Get Scheme/Block Detail"
        ''' <summary>
        ''' Get Property Customer Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getScemeBlockDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef RquestType As String)

            Dim parametersList As New ParameterList()

            Dim dtPropertyResult As New DataTable()

            dtPropertyResult.TableName = "PropertyCustomerDetail"

            resultDataSet.Tables.Add(dtPropertyResult)

            Dim Id As Integer = propertyId

            Dim propertyIdParam As New ParameterBO("Id", Id, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim requestTypeParam As New ParameterBO("RequestType", RquestType, DbType.String)
            parametersList.Add(requestTypeParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSchemeBlockDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult)

        End Sub
#End Region

#Region "Schedule Defect Appointment"

        Function scheduleDefectAppointment(ByVal journalId As Integer, ByVal objAppointment As AppointmentBO, ByVal tempDefectsdt As DataTable) As Boolean
            Dim saveStatus As Boolean = False

            Dim defectIds As String = GeneralHelper.getDelimitedStringForDatatableColumn(tempDefectsdt, TempDefectDtBO.DefectIdColName, ",")

            Dim inParametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            inParametersList.Add(New ParameterBO("journalId", journalId, DbType.Int32))
            inParametersList.Add(New ParameterBO("appointmentStartDateTime", objAppointment.AppointmentStartDate, DbType.DateTime2))
            inParametersList.Add(New ParameterBO("appointmentEndDateTime", objAppointment.AppointmentEndDate, DbType.DateTime2))
            inParametersList.Add(New ParameterBO("operativeId", objAppointment.OperativeId, DbType.Int32))
            inParametersList.Add(New ParameterBO("schedularId", objAppointment.SchedularId, DbType.Int32))
            inParametersList.Add(New ParameterBO("appointmentNotes", objAppointment.AppointmentNotes, DbType.String))
            inParametersList.Add(New ParameterBO("jobSheetNotes", objAppointment.JobSheetNotes, DbType.String))
            inParametersList.Add(New ParameterBO("tradeId", objAppointment.TradeId, DbType.Int32))
            inParametersList.Add(New ParameterBO("duration", objAppointment.Duration, DbType.Double))
            inParametersList.Add(New ParameterBO("defectIds", defectIds, DbType.String))

            Dim saveStatusParam As New ParameterBO("saveStatus", saveStatus, DbType.Boolean)
            outParametersList.Add(saveStatusParam)

            outParametersList.Add(New ParameterBO("appointmentId", objAppointment.AppointmentId, DbType.Int32))

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.ScheduleDefectAppointment)

            saveStatus = saveStatusParam.Value

            Return saveStatus
        End Function

#End Region

#Region "Get Property Attribute notes"
        ''' <summary>
        ''' Get Property Attribute notes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAttributeNotesByPropertyId(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As New ParameterList()
            Dim propertyIdParam As New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAttributeNotesByPropertyID)


        End Sub
#End Region

#Region "Get Scheme/Block Attribute notes"
        ''' <summary>
        ''' Get Property Attribute notes
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <param name="blockId"></param>
        ''' <remarks></remarks>
        Public Sub GetAttributeNotesBySchemeBlockId(ByRef resultDataSet As DataSet, ByRef schemeId As Int32, ByRef blockId As Int32)

            Dim parametersList As New ParameterList()
            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)
            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetHeatingFuelNotes)


        End Sub
#End Region

#End Region

#Region "Save Gas Servicing Cancellation"
        Public Function saveGasServicingCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentCancellation.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", appointmentCancellation.Notes, DbType.String)
            inparametersList.Add(notesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", appointmentCancellation.userId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveGasServicingCancellation)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Save Gas Servicing Cancellation For Scheme/Block"
        Public Function saveGasServicingCancellationForSchemeBlock(ByVal appointmentCancellation As AppointmentCancellationForSchemeBlockBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentCancellation.AppointmentId, DbType.Int32)
            inparametersList.Add(appointmentIdParam)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("appointmentType", appointmentCancellation.AppointmentId, DbType.String)
            inparametersList.Add(appointmentTypeParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", appointmentCancellation.Notes, DbType.String)
            inparametersList.Add(notesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", appointmentCancellation.userId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveGasServicingCancellationForSchemeBlock)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Save Gas Servicing Cancellation for Assign to Contractor"
        Public Function saveGasServicingCancellationforContractors(ByVal appointmentCancellation As AppointmentCancellationBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("JournalId", Convert.ToInt32(appointmentCancellation.FaultsList), DbType.Int32)
            inparametersList.Add(faultsListParam)

            Dim notesParam As ParameterBO = New ParameterBO("UserId", appointmentCancellation.userId, DbType.Int32)
            inparametersList.Add(notesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("notes", appointmentCancellation.Notes, DbType.String)
            inparametersList.Add(userIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("sendEmailOrderID", 0, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.CancelFuelPurchaseOrder)
            If (IsDBNull(outParametersList.ElementAt(0).Value)) Then
                Return 0
            End If
            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetailForContractor(ByVal journalId As Integer, ByVal purchaseOrderId As Integer, ByRef detailsForEmailDS As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim FaultLogId As ParameterBO = New ParameterBO("journalId", journalId.ToString(), DbType.String)
            parametersList.Add(FaultLogId)

            Dim PropertyId As ParameterBO = New ParameterBO("purchaseOrderId", purchaseOrderId, DbType.Int32)
            parametersList.Add(PropertyId)


            Dim datareader = MyBase.SelectRecord(parametersList, SpNameConstants.AS_GetContractorEmailDetail)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.ContractorDetailsDt _
                                   , ApplicationConstants.PropertyDetailsDT _
                                   , ApplicationConstants.BlockDetailsdt _
                                   , ApplicationConstants.FaultInfoDt)

        End Sub

#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetailForOriginator(ByVal journalId As Integer, ByVal purchaseOrderId As Integer, ByRef detailsForEmailDS As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim FaultLogId As ParameterBO = New ParameterBO("journalId", journalId.ToString(), DbType.String)
            parametersList.Add(FaultLogId)

            Dim PropertyId As ParameterBO = New ParameterBO("purchaseOrderId", purchaseOrderId, DbType.Int32)
            parametersList.Add(PropertyId)


            Dim datareader = MyBase.SelectRecord(parametersList, SpNameConstants.AS_GetOriginatorEmailDetail)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.ContractorDetailsDt _
                                   , ApplicationConstants.PropertyDetailsDT _
                                   , ApplicationConstants.BlockDetailsdt _
                                   , ApplicationConstants.FaultInfoDt)

        End Sub

#End Region

#Region "Get Property Feul Types"

        Sub GetPropertyFeulTypes(ByVal propertyId As String, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPropertyFuelTypes)

        End Sub

#End Region

#Region "Get Alternative Appointments Header By Property"

        Sub GetAlternativeAppointmentsHeaderByProperty(ByVal propertyId As String, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAlternativeAppointmentsHeaderByProperty)

        End Sub

#End Region

#Region "Get Oil Appointments Header By Property"

        Sub GetOilAppointmentsHeaderByProperty(ByVal propertyId As String, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetOilAppointmentsHeaderByProperty)

        End Sub

#End Region

#Region "Get Heating Appointment Duration"

        Sub GetHeatingAppointmentDuration(ByVal journalId As Int32, ByVal appointmentId As Int32, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetHeatingAppointmentDuration)

        End Sub

#End Region


#Region "Set Heating Appointment Duration"

        Public Function SetHeatingAppointmentDuration(ByVal appointmentId As Int32, ByVal appointmentHeatingFuelDuration As DataTable)

            Dim parametersList As ParameterList = New ParameterList()

            Dim heatingApptDurationDetailParam As ParameterBO = New ParameterBO("HeatingApptDurationDetail", appointmentHeatingFuelDuration, SqlDbType.Structured)
            parametersList.Add(heatingApptDurationDetailParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("AppointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim outParametersList As ParameterList = New ParameterList()

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SetHeatingAppointmentDuration)

            Return CType(outParametersList.Item(0).Value, Int32)
        End Function

#End Region

#Region "Update Heating Appointment Duration"

        Public Function UpdateHeatingAppointmentDuration(ByVal appointmentId As Int32, ByVal appointmentHeatingFuelDuration As DataTable)

            Dim parametersList As ParameterList = New ParameterList()

            Dim heatingApptDurationDetailParam As ParameterBO = New ParameterBO("HeatingApptDurationDetail", appointmentHeatingFuelDuration, SqlDbType.Structured)
            parametersList.Add(heatingApptDurationDetailParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("AppointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim outParametersList As ParameterList = New ParameterList()

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.UpdateHeatingAppointmentDuration)

            Return CType(outParametersList.Item(0).Value, Int32)
        End Function

#End Region


#Region " Get Scheduling Property Locked By"
        Public Function GetSchedulingPropertyLockedBy(ByVal journalId As Int32, ByVal appointmentType As String)

            Dim spGetSchedulingPropertyLockedBy As String = SpNameConstants.GetSchedulingPropertyLockedBy

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            Dim LockedUser As ParameterBO = New ParameterBO("LockedUser", 0, DbType.Int32)
            outParametersList.Add(LockedUser)

            MyBase.SelectRecord(parametersList, outParametersList, spGetSchedulingPropertyLockedBy)

            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "Set Scheduling Property Status"
        Public Function SetSchedulingPropertyStatus(ByVal journalId As Int32, ByVal status As Integer, ByRef schedulestatusProp As Integer, ByVal lockedBy As Integer, ByVal appointmentType As String)

            Dim spSetSchedulingPropertyStatus As String = SpNameConstants.SetSchedulingPropertyStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)

            Dim status1 As ParameterBO = New ParameterBO("status", status, DbType.Int32)
            parametersList.Add(status1)
            Dim ischeduled As ParameterBO = New ParameterBO("scheduled", schedulestatusProp, DbType.Int32)
            parametersList.Add(ischeduled)
            Dim lockby As ParameterBO = New ParameterBO("lockedBy", lockedBy, DbType.Int32)
            parametersList.Add(lockby)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim lock As ParameterBO = New ParameterBO("lock", 0, DbType.Int32)
            outParametersList.Add(lock)

            MyBase.SelectRecord(parametersList, outParametersList, spSetSchedulingPropertyStatus)
            schedulestatusProp = outParametersList.Item(0).Value
            Return outParametersList.Item(1).Value


        End Function

#End Region

#Region "Get Scheduling Property Status"
        Public Function GetSchedulingPropertyStatus(ByVal journalId As Int32, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Dim spGetSchedulingPropertyStatus As String = SpNameConstants.GetSchedulingPropertyStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim TimeExceeded As ParameterBO = New ParameterBO("isTimeExceeded", 0, DbType.Int32)
            outParametersList.Add(TimeExceeded)
            Dim scheduled As ParameterBO = New ParameterBO("isScheduled", 0, DbType.Int32)
            outParametersList.Add(scheduled)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.SelectRecord(parametersList, outParametersList, spGetSchedulingPropertyStatus)

            If (outParametersList.Item(1).Value = 1) Then
                istimeexceeded = True
            End If
            If (outParametersList.Item(2).Value = 1) Then
                isScheduled = 1
            Else
                isScheduled = 0
            End If
            Return outParametersList.Item(0).Value


        End Function

#End Region

#Region "Get Arranged Scheduling Property Status"
        Public Function GetArrangedSchedulingPropertyStatus(ByVal journalId As Int32, ByRef istimeexceeded As Boolean, ByRef isScheduled As Integer, ByVal appointmentType As String)
            Dim spGetPropertyScheduleStatus As String = SpNameConstants.GetArrangedSchedulingPropertyStatus

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parametersList.Add(journalIdParam)


            Dim scheduleStatus As ParameterBO = New ParameterBO("ScheduleStatus", 0, DbType.Int32)
            outParametersList.Add(scheduleStatus)
            Dim TimeExceeded As ParameterBO = New ParameterBO("isTimeExceeded", 0, DbType.Int32)
            outParametersList.Add(TimeExceeded)
            Dim scheduled As ParameterBO = New ParameterBO("isScheduled", 0, DbType.Int32)
            outParametersList.Add(scheduled)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("AppointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.SelectRecord(parametersList, outParametersList, spGetPropertyScheduleStatus)

            If (outParametersList.Item(1).Value = 1) Then
                istimeexceeded = True
            End If
            If (outParametersList.Item(2).Value = 1) Then
                isScheduled = 1
            Else
                isScheduled = 0
            End If
            Return outParametersList.Item(0).Value


        End Function

#End Region

    End Class
End Namespace
