﻿
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports AS_BusinessObject
Imports AS_Utilities
Imports AS_DataAccess

Public Class DocumentTypeDAL
    Inherits BaseDAL

#Region "Get Document Types"

    Public Sub getCategoryTypes(ByRef resultDataSet As DataSet, ByVal reportFor As String)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        
        Dim reportForParam As ParameterBO = New ParameterBO("reportFor", reportFor, DbType.String)
        parametersList.Add(reportForParam)

        MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetCategoryTypes)

    End Sub

#End Region
#Region "Get Category Types"

    Public Sub getDocumentTypes(ByRef resultDataSet As DataSet, ByVal ShowActiveOnly As Boolean, ByVal reportFor As String, ByVal CategoryId As Integer)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        Dim ShowActiveOnlyParam As ParameterBO = New ParameterBO("ShowActiveOnly", ShowActiveOnly, DbType.Boolean)
        parametersList.Add(ShowActiveOnlyParam)

        Dim reportForParam As ParameterBO = New ParameterBO("reportFor", reportFor, DbType.String)
        parametersList.Add(reportForParam)

        Dim categoryIdParam As ParameterBO = New ParameterBO("categoryId", CategoryId, DbType.Int32)
        parametersList.Add(categoryIdParam)

        MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetTypes)

    End Sub

#End Region


#Region "Get Document SubTypes"

    Public Sub getDocumentSubtypes(ByRef resultDataSet As DataSet, ByVal ShowActiveOnly As Boolean, ByVal DocumentTypeId As Int16, ByVal reportFor As String)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        Dim pShowActiveOnly As ParameterBO = New ParameterBO("ShowActiveOnly", ShowActiveOnly, DbType.Boolean)
        Dim pParent As ParameterBO = New ParameterBO("DocumentTypeId", DocumentTypeId, DbType.Int16)
        Dim reportForParam As ParameterBO = New ParameterBO("reportFor", reportFor, DbType.String)
        parametersList.Add(pShowActiveOnly)
        parametersList.Add(pParent)
        parametersList.Add(reportForParam)

        MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDocumentSubtypes)

    End Sub

#End Region




#Region "Get EPC Category types"

    Public Sub getEpcCategoryType(ByRef resultDataSet As DataSet)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetEpcCategoryTypes)
    End Sub

#End Region


#Region "Get Document Type and Subtype"

    Public Sub getDocumentTypeAndSubtype(ByRef resultDataSet As DataSet, ByVal DocumentTypeId As Int16, ByVal DocumentSubTypeId As Int16)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        Dim PType As ParameterBO = New ParameterBO("Type", DocumentTypeId, DbType.Int16)
        Dim PSubtype As ParameterBO = New ParameterBO("Subtype", DocumentSubTypeId, DbType.Int16)
        parametersList.Add(PType)
        parametersList.Add(PSubtype)

        MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDocumentSubtypes)

    End Sub

#End Region



End Class
