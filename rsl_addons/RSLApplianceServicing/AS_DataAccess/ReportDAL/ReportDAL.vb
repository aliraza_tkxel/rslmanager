﻿Imports System
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess
    Public Class ReportDAL : Inherits BaseDAL
#Region "Attributes"

#End Region

#Region "Functions"

#Region "get Certificate Expiry Report Statistics"

        Public Function getCertificateExpiryReportStats(ByVal resultDataSet As DataSet, ByRef objCertificateExpiryReportBo As ReportBO, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()

            Dim outParametersList As ParameterList = New ParameterList()

            Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", objCertificateExpiryReportBo.FuelType, DbType.Int32)
            parametersList.Add(fuelTypeParam)

            Dim propertyTypeParam As ParameterBO = New ParameterBO("propertyType", objCertificateExpiryReportBo.PropertyType, DbType.Int32)
            parametersList.Add(propertyTypeParam)

            Dim patchParam As ParameterBO = New ParameterBO("patch", objCertificateExpiryReportBo.Patch, DbType.Int32)
            parametersList.Add(patchParam)

            Dim shcemeParam As ParameterBO = New ParameterBO("scheme", objCertificateExpiryReportBo.Scheme, DbType.Int32)
            parametersList.Add(shcemeParam)

            Dim stageParam As ParameterBO = New ParameterBO("stage", objCertificateExpiryReportBo.Stage, DbType.Int32)
            parametersList.Add(stageParam)

            Dim UPRNParam As ParameterBO = New ParameterBO("UPRN", objCertificateExpiryReportBo.UPRN, DbType.String)
            parametersList.Add(UPRNParam)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.String)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.String)
            parametersList.Add(pageNumber)

            Dim certType As ParameterBO = New ParameterBO("certType", objCertificateExpiryReportBo.CertificateType, DbType.String)
            parametersList.Add(certType)

            Dim startDateParam As ParameterBO = New ParameterBO("fromDate", objCertificateExpiryReportBo.StartDate, DbType.String)
            parametersList.Add(startDateParam)

            Dim endDateParam As ParameterBO = New ParameterBO("toDate", objCertificateExpiryReportBo.EndDate, DbType.String)
            parametersList.Add(endDateParam)

            Dim heatingTypeId As ParameterBO = New ParameterBO("heatingTypeId", objCertificateExpiryReportBo.HeatingType, DbType.Int32)
            parametersList.Add(heatingTypeId)

            '---------------        OUTPUT PARAMETERS

            Dim totalCountParam As ParameterBO = New ParameterBO("selectCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            Dim totalCountParamExp As ParameterBO = New ParameterBO("selectCountExpired", 0, DbType.Int32)
            outParametersList.Add(totalCountParamExp)

            Dim totalCountParam2week As ParameterBO = New ParameterBO("selectCount2Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam2week)

            Dim totalCountParam4week As ParameterBO = New ParameterBO("selectCount4Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam4week)

            Dim totalCountParam8week As ParameterBO = New ParameterBO("selectCount8Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam8week)

            Dim totalCountParam12week As ParameterBO = New ParameterBO("selectCount12Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam12week)

            Dim totalCountParam16week As ParameterBO = New ParameterBO("selectCount16Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam16week)

            Dim totalCountParam52week As ParameterBO = New ParameterBO("selectCount52Weeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam52week)

            Dim totalCountParam52pweek As ParameterBO = New ParameterBO("selectCount52PlusWeeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParam52pweek)

            Dim totalCountParamNoWeek As ParameterBO = New ParameterBO("selectCountNoWeeks", 0, DbType.Int32)
            outParametersList.Add(totalCountParamNoWeek)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetExpiredCertificateStats)

            Dim lstOfString(10) As String
            lstOfString(0) = outParametersList.Item(0).Value.ToString()
            lstOfString(1) = outParametersList.Item(1).Value.ToString()
            lstOfString(2) = outParametersList.Item(2).Value.ToString()
            lstOfString(3) = outParametersList.Item(3).Value.ToString()
            lstOfString(4) = outParametersList.Item(4).Value.ToString()
            lstOfString(5) = outParametersList.Item(5).Value.ToString()
            lstOfString(6) = outParametersList.Item(6).Value.ToString()
            lstOfString(7) = outParametersList.Item(7).Value.ToString()
            lstOfString(8) = outParametersList.Item(8).Value.ToString()
            lstOfString(9) = outParametersList.Item(9).Value.ToString()

            Return lstOfString

        End Function
#End Region

#Region "get Certificate Expiry Report Information"

        Function getCertificateExpiryReportInfo(ByVal resultDataSet As DataSet, ByRef objCertificateExpiryReportBo As ReportBO, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim fuelTypeParam As ParameterBO = New ParameterBO("fuelType", objCertificateExpiryReportBo.FuelType, DbType.Int32)
            parametersList.Add(fuelTypeParam)

            Dim propertyTypeParam As ParameterBO = New ParameterBO("propertyType", objCertificateExpiryReportBo.PropertyType, DbType.Int32)
            parametersList.Add(propertyTypeParam)

            Dim patchParam As ParameterBO = New ParameterBO("patch", objCertificateExpiryReportBo.Patch, DbType.Int32)
            parametersList.Add(patchParam)

            Dim shcemeParam As ParameterBO = New ParameterBO("scheme", objCertificateExpiryReportBo.Scheme, DbType.Int32)
            parametersList.Add(shcemeParam)

            Dim stageParam As ParameterBO = New ParameterBO("stage", objCertificateExpiryReportBo.Stage, DbType.Int32)
            parametersList.Add(stageParam)

            Dim UPRNParam As ParameterBO = New ParameterBO("UPRN", objCertificateExpiryReportBo.UPRN, DbType.String)
            parametersList.Add(UPRNParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetExpiredCertificateInfo)

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "get Property Type"
        Public Sub getPropertyType(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPropertyType)
        End Sub
#End Region

#Region "Get IssuedCertificate Report"

        Public Function getIssuedCertificateReport(ByRef objIssuedCertificateBO As IssuedCertificateBO, ByRef objPageSortBO As PageSortBO, ByRef resultDS As DataSet)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            'Add in Parameters
            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", objIssuedCertificateBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", objIssuedCertificateBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim searchTextInParam As ParameterBO = New ParameterBO("searchText", objIssuedCertificateBO.searchText, DbType.String)
            inParametersList.Add(searchTextInParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32)
            inParametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32)
            inParametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String)
            inParametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String)
            inParametersList.Add(sortOrder)

            Dim heatingTypeId As ParameterBO = New ParameterBO("heatingTypeId", objIssuedCertificateBO.heatingTypeId, DbType.Int32)
            inParametersList.Add(heatingTypeId)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            'Call base class function to fill dataset by providing parameters and StoredProcedure Name
            LoadDataSet(resultDS, inParametersList, outParametersList, SpNameConstants.GetIssuedCertificateReport)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get PrintCertificate Report"

        Public Function getPrintCertificateReport(ByRef objPrintCertificateBO As IssuedCertificateBO, ByRef objPageSortBO As PageSortBO, ByRef resultDS As DataSet)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            'Add in Parameters
            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", objPrintCertificateBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", objPrintCertificateBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim searchTextInParam As ParameterBO = New ParameterBO("searchText", objPrintCertificateBO.searchText, DbType.String)
            inParametersList.Add(searchTextInParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32)
            inParametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32)
            inParametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String)
            inParametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String)
            inParametersList.Add(sortOrder)

            Dim heatingTypeId As ParameterBO = New ParameterBO("heatingTypeId", objPrintCertificateBO.heatingTypeId, DbType.Int32)
            inParametersList.Add(heatingTypeId)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            'Call base class function to fill dataset by providing parameters and StoredProcedure Name
            LoadDataSet(resultDS, inParametersList, outParametersList, SpNameConstants.GetPrintCertificateReport)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Set CP12 Document as Printed"

        Public Sub setCP12DocumentPrinted(ByRef id As Integer)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            'Add in Parameters
            Dim cp12Id As ParameterBO = New ParameterBO("LGSRID", id, DbType.Int32)
            inParametersList.Add(cp12Id)

            Dim totalCountParam As ParameterBO = New ParameterBO("result", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.UpdatePrintedStatus)

        End Sub

#End Region

#Region "Get Appliance Defect Report"
        ''' <summary>
        ''' This function would get the data set for appliance defect report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objDefectReportBo"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getApplianceDefectReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", objDefectReportBo.SearchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim appointmentStatusIdParam As ParameterBO = New ParameterBO("appointmentStatusId", objDefectReportBo.AppointmentStatusId, DbType.Int32)
            parametersList.Add(appointmentStatusIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetApplianceDefectReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defect To Be Approved Report"
        ''' <summary>
        ''' This function would get the data set for appliance defect report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDefectToBeApprovedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim getCountOnly As ParameterBO = New ParameterBO("getOnlyCount", 0, DbType.Boolean)
            parametersList.Add(getCountOnly)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectToBeApprovedReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Reason List"
        ''' <summary>
        ''' Get Reason List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getDefectReasons(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim dtReason As DataTable = New DataTable()
            dtReason.TableName = ApplicationConstants.ReasonDt
            resultDataSet.Tables.Add(dtReason)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetRejectionReasons)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReason)

        End Sub
#End Region

#Region "Get Defect More Detail"
        ''' <summary>
        ''' Get Condition More Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getDefectMoreDetail(ByRef resultDataSet As DataSet, ByVal defectId As Integer, ByVal appointmentType As String)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim dtPropertyCustomerDetail As DataTable = New DataTable()
            dtPropertyCustomerDetail.TableName = ApplicationConstants.PropertyCustomerDetailDt
            resultDataSet.Tables.Add(dtPropertyCustomerDetail)

            Dim dtDefectDetail As DataTable = New DataTable()
            dtDefectDetail.TableName = ApplicationConstants.DefectDetailDt
            resultDataSet.Tables.Add(dtDefectDetail)

            Dim defectIdParam As ParameterBO = New ParameterBO("defectId", defectId, DbType.Int32)
            parameterList.Add(defectIdParam)

            Dim appointmentTypeParam As ParameterBO = New ParameterBO("appointmentType", appointmentType, DbType.String)
            parameterList.Add(appointmentTypeParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetDefectMoreDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyCustomerDetail, dtDefectDetail)

        End Sub
#End Region

#Region "Get Defect Approved Report"
        ''' <summary>
        ''' This function would get the data set for appliance defect report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDefectApprovedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim getCountOnly As ParameterBO = New ParameterBO("getOnlyCount", 0, DbType.Boolean)
            parametersList.Add(getCountOnly)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectApprovedReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defect Rejected Report"
        ''' <summary>
        ''' This function would get the data set for appliance defect report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDefectRejectedReport(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim getCountOnly As ParameterBO = New ParameterBO("getOnlyCount", 0, DbType.Boolean)
            parametersList.Add(getCountOnly)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectRejectedReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Change Defect Status"
        ''' <summary>
        ''' This function would changes the status of a defect from approved to rejected or rejected to approved
        ''' </summary>
        ''' <param name="cds"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function changeDefectStatus(ByRef cds As ChangeDefectStatus)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("defectId", cds.DefectId, DbType.Int32)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("userId", cds.UserId, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("rejectionReasonId", cds.RejectionReasonId, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("rejectionReasonNotes", cds.RejectionReasonNotes, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("status", cds.Status, DbType.String)
            parametersList.Add(sortOrder)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ChangeDefectStatus)

            Return True
        End Function

#End Region

#Region "Get Disconnected Appliances Report"
        ''' <summary>
        '''  This function will return disconneced appliances.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objDefectReportBo"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDisconnectedAppliances(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", objDefectReportBo.SearchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objDefectReportBo.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim applianceTypeParam As ParameterBO = New ParameterBO("applianceType", objDefectReportBo.ApplianceType, DbType.String)
            parametersList.Add(applianceTypeParam)

            Dim defectType As ParameterBO = New ParameterBO("defectType", objDefectReportBo.DefectType, DbType.String)
            parametersList.Add(defectType)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDisconnectedAppliances)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Disconnected Appliances Count"
        ''' <summary>
        '''  This function will return disconneced appliances count.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDisconnectedAppliancesCount(ByRef resultDataSet As DataSet, ByVal schemeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)
            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDisconnectedAppliances)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defects Requiring Approval Count"
        ''' <summary>
        '''  This function will return disconneced appliances count.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDefectsRequiringApprovalCount(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.DefectsRequiringApproval)
        End Function

#End Region

#Region "Get Cancelled Defects Report"
        ''' <summary>
        ''' This function will return all the defects with cancelled appointment.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objDefectReportBo"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getCancelledDefectsReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", objDefectReportBo.SearchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objDefectReportBo.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim applianceTypeParam As ParameterBO = New ParameterBO("applianceType", objDefectReportBo.ApplianceType, DbType.String)
            parametersList.Add(applianceTypeParam)

            Dim defectTypeParam As ParameterBO = New ParameterBO("defectType", objDefectReportBo.DefectType, DbType.String)
            parametersList.Add(defectTypeParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetCancelledDefectsReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defect History Report"
        ''' <summary>
        ''' Get Defect History Report
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objDefectReportBo"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getDefectHistoryReport(ByRef resultDataSet As DataSet, ByVal objDefectReportBo As DefectReportBo, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", objDefectReportBo.SearchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objDefectReportBo.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim applianceTypeParam As ParameterBO = New ParameterBO("applianceType", objDefectReportBo.ApplianceType, DbType.String)
            parametersList.Add(applianceTypeParam)

            Dim defectTypeParam As ParameterBO = New ParameterBO("defectType", objDefectReportBo.DefectType, DbType.String)
            parametersList.Add(defectTypeParam)

            Dim yearParam As ParameterBO = New ParameterBO("year", objDefectReportBo.Year, DbType.Int32)
            parametersList.Add(yearParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectHistoryReport)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Property Defect List"
        ''' <summary>
        ''' get Property Defect List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getPropertyDefectList(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal year As Int32, ByVal defectType As String, ByVal applianceType As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdValueParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdValueParam)

            Dim yearParam As ParameterBO = New ParameterBO("year", year, DbType.Int32)
            parametersList.Add(yearParam)

            Dim defectTypeParam As ParameterBO = New ParameterBO("defectType", defectType, DbType.String)
            parametersList.Add(defectTypeParam)

            Dim applianceTypeParam As ParameterBO = New ParameterBO("applianceType", applianceType, DbType.String)
            parametersList.Add(applianceTypeParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPropertyDefectsList)

        End Sub

#End Region

#Region "Get Scheme/Block Defect List"
        ''' <summary>
        ''' get Property Defect List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getSchemeBlockDefectList(ByRef resultDataSet As DataSet, ByVal propertyId As Integer, ByVal reqType As String, ByVal year As Int32, ByVal defectType As String, ByVal applianceType As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdValueParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.Int32)
            parametersList.Add(propertyIdValueParam)

            Dim reqTypeValueParam As ParameterBO = New ParameterBO("requestType", reqType, DbType.String)
            parametersList.Add(reqTypeValueParam)

            Dim yearParam As ParameterBO = New ParameterBO("year", Year, DbType.Int32)
            parametersList.Add(yearParam)

            Dim defectTypeParam As ParameterBO = New ParameterBO("defectType", defectType, DbType.String)
            parametersList.Add(defectTypeParam)

            Dim applianceTypeParam As ParameterBO = New ParameterBO("applianceType", applianceType, DbType.String)
            parametersList.Add(applianceTypeParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetSchemeBlockDefectsList)

        End Sub

#End Region

#Region "Get Assigned to Contractor List"

        Function getAssignedToContractorReport(ByRef resultDataSet As DataSet, ByRef objPageSortBo As Object, ByRef searchText As String) As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetAssignedGasServicingToContractorReport)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Job Sheed Summary Detail By Journal Id"

        Sub getJobSheetSummaryByJournalId(ByRef ds As DataSet, ByRef journalId As Integer)
            Dim inParametersList As New ParameterList

            Try
                Dim jsnparameter As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
                inParametersList.Add(jsnparameter)
                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.getJobSheetSummaryByJSN)
                ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)
            Catch ex As Exception
                Dim a As String = ex.Message

            End Try


        End Sub

#End Region

#Region "Get Job Sheet Summary By Contractor Work Id"

        Sub getJobSheetSummaryForSchemeBlockByJournalId(ByRef ds As DataSet, ByRef journalId As Integer)
            Dim inParametersList As New ParameterList

            Try
                Dim contractorWorkIdparameter As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
                inParametersList.Add(contractorWorkIdparameter)
                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.GetJobSheetSummaryForSchemeBlockByJournalId)
                ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable, ApplicationConstants.JobSheetSummaryBoilerInfoTable)
            Catch ex As Exception
                Dim a As String = ex.Message

            End Try


        End Sub

#End Region

#Region "Save/Set Appointment Stauts in Database with Notes"

        Sub setAppointmentJobSheetStatusUpdate(ByRef objJobSheetBO As JobSheetBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.setAppointmentJobSheetStatus

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim AppointmentJSNId As ParameterBO = New ParameterBO("journalID", objJobSheetBO.AppointmentJSNId, DbType.Int32)
            inParamList.Add(AppointmentJSNId)
            Dim OrderId As ParameterBO = New ParameterBO("OrderId", objJobSheetBO.OrderId, DbType.Int32)
            inParamList.Add(OrderId)
            Dim WorkRequired As ParameterBO = New ParameterBO("workRequire", objJobSheetBO.WorkRequired, DbType.String)
            inParamList.Add(WorkRequired)
            Dim userId As ParameterBO = New ParameterBO("userID", objJobSheetBO.UserID, DbType.Int32)
            inParamList.Add(userId)
            Dim purchaseOrderItemId As ParameterBO = New ParameterBO("purchaseOrderItemId", objJobSheetBO.PurchaseOrderItemId, DbType.Int32)
            inParamList.Add(purchaseOrderItemId)
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                objJobSheetBO.IsFlagStatus = False
                objJobSheetBO.UserMsg = "Unable to update appointment job sheet "
            Else
                objJobSheetBO.IsFlagStatus = True
            End If

        End Sub

#End Region
#End Region

    End Class
End Namespace





