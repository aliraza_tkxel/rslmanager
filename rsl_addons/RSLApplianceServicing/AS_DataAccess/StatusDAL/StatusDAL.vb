﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject


Namespace AS_DataAccess

    Public Class StatusDAL : Inherits BaseDAL

#Region "Functions"

#Region "get Status"
        Public Sub getStatus(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetStaus)
        End Sub

        Public Sub getStatusList(ByRef statusList As List(Of StatusBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetStaus)
            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("StatusID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("StatusID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Title")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Title"))
                End If
                Dim objList As New StatusBO(id, name)
                statusList.Add(objList)
            End While

        End Sub
#End Region

#Region "Add Status "
        Public Sub addStatus(ByVal inspectionType As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer)
            Dim outParameterList As ParameterList = New ParameterList()
            Dim inParameterList As ParameterList = New ParameterList()
            Dim CreatedByParam As Integer = SessionManager.getAppServicingUserId()
            Dim modifiedByParam As Integer = 0
            Dim rankTobeUpdated As ParameterBO = New ParameterBO("Ranking", selectedRanking, DbType.Int32)
            inParameterList.Add(rankTobeUpdated)
            Dim newTitle As ParameterBO = New ParameterBO("Title", statusTitle, DbType.String)
            inParameterList.Add(newTitle)
            Dim CreatedBy As ParameterBO = New ParameterBO("CreatedBy", CreatedByParam, DbType.Int32)
            inParameterList.Add(CreatedBy)
            Dim ModifiedBy As ParameterBO = New ParameterBO("modifiedBy", modifiedByParam, DbType.Int32)
            inParameterList.Add(ModifiedBy)
            Dim InspectionTypeId As ParameterBO = New ParameterBO("InspectionTypeId", inspectionType, DbType.Int32)
            inParameterList.Add(InspectionTypeId)

            MyBase.SaveRecord(inParameterList, outParameterList, SpNameConstants.AddStatus)
        End Sub
#End Region

#Region "Get Action"
        Public Sub getActions(ByRef resultDataSet As DataSet)
            Dim parameterlist As ParameterList = New ParameterList()

            'Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            'parameterlist.Add(StatusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterlist, SpNameConstants.GetAction)
        End Sub
#End Region

#Region "Get Action by StatusId"
        Sub getActionsByStatusId(ByVal statusId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterList.Add(statusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetActionsByStatusID)
        End Sub
#End Region

#Region "get Action Ranking By StatusId"
        Sub getActionRankingByStatusId(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            Dim parameterlist As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterlist.Add(StatusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterlist, SpNameConstants.GetActionRankingByStatusId)
        End Sub
#End Region

#Region "get Defect Appointment Statuses"
        Public Sub getDefectAppointmentStatuses(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetDefectAppointmentStatuses)
        End Sub
#End Region


#End Region






    End Class

End Namespace
