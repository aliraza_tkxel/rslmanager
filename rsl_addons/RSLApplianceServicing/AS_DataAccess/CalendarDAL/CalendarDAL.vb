﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject
Namespace AS_DataAccess
    Public Class CalendarDAL : Inherits BaseDAL
#Region "Functions"

#Region "get Engineers Info for Appointment"
        Sub getEngineersForCalendarAppointment(ByRef resultDataSet)
            Dim empId As Int32 = 0
            Dim empName As String = String.Empty
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentCalendarEngineersInfo)
        End Sub
#End Region

#Region "get Engineers Leaves Information"
        Sub getEngineersLeaveInformation(ByRef resultDataSet)
            Dim empId As Int32 = 0
            Dim empName As String = String.Empty
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentCalendarEngineersInfo)
        End Sub
#End Region

#Region "get Appointments Details for Calendar"
        Sub getAppointmentsWeeklyCalendarDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim shift As String = String.Empty
            Dim appointmentDate As Date = Date.Now
            Dim custName As String = String.Empty
            Dim cellNo As String = String.Empty
            Dim address As String = String.Empty
            Dim status As Int32 = 0
            Dim parametersList As ParameterList = New ParameterList()
            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objScheduleAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim returnDateParam As ParameterBO = New ParameterBO("endDate", objScheduleAppointmentsBO.ReturnDate, DbType.Date)
            parametersList.Add(returnDateParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentsCalendarDetail)
        End Sub
#End Region

#Region "get Employee Core Working Hours"
        Sub getEmployeeCoreWorkingHours(ByRef resultDataSet As DataSet, ByVal employeeIds As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeIds", employeeIds, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetEmployeeCoreWorkingHours)
        End Sub
#End Region

#Region "get Appointments Details for Monthly Calendar"
        Sub getAppointmentsMonthlyCalendarDetail(ByVal objScheduleAppointmentsBO As ScheduleAppointmentsBO, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objScheduleAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim returnDateParam As ParameterBO = New ParameterBO("endDate", objScheduleAppointmentsBO.ReturnDate, DbType.Date)
            parametersList.Add(returnDateParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentsMonthlyCalendarDetail)
        End Sub
#End Region

#Region "get Searched Appointments Details"
        Sub getSearchedAppointmentsDetail(ByRef resultDataSet As DataSet)
            Dim shift As String = String.Empty
            Dim appointmentDate As Date = Date.Now
            Dim appointmentDateOnly As Date = Date.Now
            Dim custName As String = String.Empty
            Dim cellNo As String = String.Empty
            Dim address As String = String.Empty
            Dim status As Int32 = 0
            Dim patchId As Int32 = 0
            Dim patchName As String = String.Empty
            Dim schemeName As String = String.Empty
            Dim schemeCode As String = String.Empty
            Dim Title As String = String.Empty
            Dim appointmentId As Int32 = 0
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSearchedAppointmentDetail)
        End Sub
#End Region

#End Region
    End Class
End Namespace

