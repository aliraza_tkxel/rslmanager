﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess


    Public Class ResourceDAL : Inherits BaseDAL

        Sub getLettersByStatusId(ByVal statusId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parameterList.Add(StatusIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getLettersByStatusId)
        End Sub

        Public Sub getLettersByActionId(ByRef actionId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", actionId, DbType.Int32)
            parameterList.Add(actionIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getLettersByActionId)
        End Sub

        Public Sub GetAlternativeLettersByActionId(ByRef actionId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", actionId, DbType.Int32)
            parameterList.Add(actionIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAlternativeLettersByActionId)
        End Sub

        Public Sub editStatus(ByVal statusId As Integer, ByVal inspectionType As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer, ByVal modifiedBy As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("StatusId", statusId, DbType.Int32)
            parameterList.Add(StatusIdParam)

            Dim inspectionTypeParam As ParameterBO = New ParameterBO("InspectionTypeId", inspectionType, DbType.Int32)
            parameterList.Add(inspectionTypeParam)

            Dim statusTitleParam As ParameterBO = New ParameterBO("Title", statusTitle, DbType.String)
            parameterList.Add(statusTitleParam)

            Dim selectedRankingParam As ParameterBO = New ParameterBO("Ranking", selectedRanking, DbType.Int32)
            parameterList.Add(selectedRankingParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", modifiedBy, DbType.Int32)
            parameterList.Add(modifiedByParam)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.EditStatus)
        End Sub

        Sub AddAction(ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal CreatedBy As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("statusId", StatusId, DbType.Int32)
            parameterList.Add(StatusIdParam)

            Dim TitleParam As ParameterBO = New ParameterBO("title", Title, DbType.String)
            parameterList.Add(TitleParam)

            Dim RankingParam As ParameterBO = New ParameterBO("ranking", ranking, DbType.Int32)
            parameterList.Add(RankingParam)

            Dim CreatedByParam As ParameterBO = New ParameterBO("createdBy", CreatedBy, DbType.Int32)
            parameterList.Add(CreatedByParam)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.AddAction)
        End Sub

        Sub getPages(ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetPages)
        End Sub

        Sub getPagesbyParentId(ByVal ParentId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim parentPageIdParam As ParameterBO = New ParameterBO("parentPageId", ParentId, DbType.Int32)
            parameterList.Add(parentPageIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetPagesByParentId)
        End Sub

        Sub saveUserAccessRight(ByVal EmployeeId As Integer, ByVal pageId As Integer)
            Dim outparameterList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("employeeId", EmployeeId, DbType.Int32)
            parameterList.Add(EmployeeIdParam)

            Dim rigthIdParam As ParameterBO = New ParameterBO("pageId", pageId, DbType.Int32)
            parameterList.Add(rigthIdParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.SaveUserRights)

        End Sub

        Sub GetPageByEmployeeId(ByVal EmployeeId As Integer, ByVal UserPageDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("employeeId", EmployeeId, DbType.Int32)
            parameterList.Add(EmployeeIdParam)

            MyBase.LoadDataSet(UserPageDataSet, parameterList, SpNameConstants.GetPageByEmployeeId)
        End Sub

        Sub editAction(ByVal ActionId As Integer, ByVal StatusId As Integer, ByVal Title As String, ByVal ranking As Integer, ByVal modifiedBy As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("StatusId", StatusId, DbType.Int32)
            parameterList.Add(StatusIdParam)

            Dim ActionIdParam As ParameterBO = New ParameterBO("ActionId", ActionId, DbType.Int32)
            parameterList.Add(ActionIdParam)

            Dim statusTitleParam As ParameterBO = New ParameterBO("Title", Title, DbType.String)
            parameterList.Add(statusTitleParam)

            Dim selectedRankingParam As ParameterBO = New ParameterBO("Ranking", ranking, DbType.Int32)
            parameterList.Add(selectedRankingParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", modifiedBy, DbType.Int32)
            parameterList.Add(modifiedByParam)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.EditAction)
        End Sub

        Sub deleteUserPages(ByVal EmployeeId As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", EmployeeId, DbType.Int32)
            parameterList.Add(employeeIdParam)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.DeleteUserPages)

        End Sub

        Sub getCoreAndOutOfHoursInfo(ByRef resultDataSet As DataSet, ByVal employeeId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim dtCoreHours As DataTable = New DataTable()
            dtCoreHours.TableName = ApplicationConstants.CoreHoursDataTable
            resultDataSet.Tables.Add(dtCoreHours)

            Dim dtEmpCoreHours As DataTable = New DataTable()
            dtEmpCoreHours.TableName = ApplicationConstants.EmpCoreHoursDataTable
            resultDataSet.Tables.Add(dtEmpCoreHours)

            Dim dtOutOfHours As DataTable = New DataTable()
            dtOutOfHours.TableName = ApplicationConstants.OutOfHoursDataTable
            resultDataSet.Tables.Add(dtOutOfHours)

            Dim dtOutOfHoursType As DataTable = New DataTable()
            dtOutOfHoursType.TableName = ApplicationConstants.OutOfHoursTypeDataTable
            resultDataSet.Tables.Add(dtOutOfHoursType)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parameterList.Add(employeeIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetCoreAndOutOfHoursInfo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtCoreHours, dtEmpCoreHours, dtOutOfHours, dtOutOfHoursType)

        End Sub


        Function saveOutOfHoursInfo(ByVal employeeId As Integer, ByVal editedBy As Integer, ByVal newOutOfHoursDt As DataTable, ByVal deletedOutOfHoursDt As DataTable)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("@employeeId", employeeId, DbType.Int32)
            parameterList.Add(employeeIdParam)

            Dim editedByParam As ParameterBO = New ParameterBO("@editedBy", editedBy, DbType.Int32)
            parameterList.Add(editedByParam)

            Dim newOutOfHoursDtParam As ParameterBO = New ParameterBO("@newOutOfHoursDt", newOutOfHoursDt, SqlDbType.Structured)
            parameterList.Add(newOutOfHoursDtParam)

            Dim deletedOutOfHoursDtParam As ParameterBO = New ParameterBO("@deletedOutOfHoursDt", deletedOutOfHoursDt, SqlDbType.Structured)
            parameterList.Add(deletedOutOfHoursDtParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            MyBase.SaveRecord(parameterList, outParametersList, SpNameConstants.SaveOutOfHoursInfo)
            If (outParametersList.Item(0).Value.ToString().Equals("0")) Then
                Return False
            Else
                Return True
            End If

        End Function

        Function saveCoreWorkingHoursInfo(ByVal employeeId As Integer, ByVal editedBy As Integer, ByVal coreWorkingHoursDt As DataTable)

            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("@employeeId", employeeId, DbType.Int32)
            parameterList.Add(employeeIdParam)

            Dim editedByParam As ParameterBO = New ParameterBO("@editedBy", editedBy, DbType.Int32)
            parameterList.Add(editedByParam)

            Dim coreWorkingHoursDtParam As ParameterBO = New ParameterBO("@coreWorkingHourInfo", coreWorkingHoursDt, SqlDbType.Structured)
            parameterList.Add(coreWorkingHoursDtParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Int32)
            outParametersList.Add(isSavedParam)

            MyBase.SaveRecord(parameterList, outParametersList, SpNameConstants.SaveCoreWorkingHoursInfo)
            If (outParametersList.Item(0).Value.ToString().Equals("0")) Then
                Return False
            Else
                Return True
            End If

        End Function


    End Class
End Namespace
