﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject


Namespace AS_DataAccess

    Public Class ActionDAL : Inherits BaseDAL
#Region "Functions"

#Region "Get Action By Status ID"
        Public Sub getActionByStatusId(ByVal statusID As Integer, ByRef actionList As List(Of ActionBO))
            Dim parametersList As ParameterList = New ParameterList()

            Dim StatusIdParam As ParameterBO = New ParameterBO("StatusId", statusID, DbType.Int32)
            parametersList.Add(StatusIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetActionsByStatusID)
            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ActionID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("ActionID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Title")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Title"))
                End If
                Dim objList As New ActionBO(id, name)
                actionList.Add(objList)
            End While
        End Sub

#End Region

#End Region

    End Class

End Namespace
