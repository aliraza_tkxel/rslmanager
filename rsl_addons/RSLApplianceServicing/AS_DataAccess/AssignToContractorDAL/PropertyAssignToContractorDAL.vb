﻿
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Public Class PropertyAssignToContractorDAL
    Inherits BaseDAL


    Public Sub GetCostCentreDropDownVales(ByRef dropDownList As List(Of DropDownBO))
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetCostCentreDropDownValues)
    End Sub

    Public Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef dropDownList As List(Of DropDownBO), ByRef CostCentreId As Integer)
        Dim inParamList As ParameterList = New ParameterList()
        Dim costCentreIdparam As ParameterBO = New ParameterBO("costCentreId", CostCentreId, DbType.Int32)
        inParamList.Add(costCentreIdparam)
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetBudgetHeadDropDownValuesByCostCentreId, inParamList)
    End Sub

    Public Sub GetExpenditureDropDownValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        Dim inParamList As ParameterList = New ParameterList()
        Dim budgetHeadIdparam As ParameterBO = New ParameterBO("HeadId", BudgetHeadId, DbType.Int32)
        inParamList.Add(budgetHeadIdparam)
        Dim employeeIdparam As ParameterBO = New ParameterBO("userId", EmployeeId, DbType.Int32)
        inParamList.Add(employeeIdparam)
        Dim spName As String = SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId
        Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, spName)

        While (myDataReader.Read())
            Dim id As Integer = 0
            Dim description As String = String.Empty
            Dim limit As Decimal = 0.0D
            Dim remaining As Decimal = 0.0D

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LIMIT")) Then
                limit = myDataReader.GetDecimal(myDataReader.GetOrdinal("LIMIT"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("REMAINING")) Then
                remaining = myDataReader.GetDecimal(myDataReader.GetOrdinal("REMAINING"))
            End If

            Dim objExpenditureBo As ExpenditureBO = New ExpenditureBO(id, description, limit, remaining)
            expenditureBOList.Add(objExpenditureBo)
        End While

        myDataReader.Close()
    End Sub

    Public Sub GetMeContractors(ByRef dropDownList As List(Of DropDownBO), ByVal objAssignToContractorBo As PropertyAssignToContractorBo)
        Dim inParamList As ParameterList = New ParameterList()
        Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objAssignToContractorBo.SchemeId, DbType.Int32)
        inParamList.Add(schemeIdParam)
        Dim blockIdParam As ParameterBO = New ParameterBO("blockId", objAssignToContractorBo.BlockId, DbType.Int32)
        inParamList.Add(blockIdParam)
        Dim msatTypeParam As ParameterBO = New ParameterBO("msat", objAssignToContractorBo.MsatType, DbType.String)
        inParamList.Add(msatTypeParam)
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetMeContractorDropDownValues, inParamList)
    End Sub

    Public Sub GetContactDropDownValuesbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByRef resultDataset As DataSet, ByVal objAssignToContractorBo As PropertyAssignToContractorBo)
        Dim inParamList As ParameterList = New ParameterList()
        Dim propertyIdparam As ParameterBO = New ParameterBO("contractorId", objAssignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(propertyIdparam)
        Dim contractorDetailDt As DataTable = New DataTable()
        contractorDetailDt.TableName = ApplicationConstants.ContractorDetailDt
        resultDataset.Tables.Add(contractorDetailDt)
        getDropDownValuesBySpName(dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList)
        Dim areaofworkparam As ParameterBO = New ParameterBO("areaofwork", objAssignToContractorBo.MsatType, DbType.String)
        inParamList.Add(areaofworkparam)
        Dim schemeIdparam As ParameterBO = New ParameterBO("schemeId", objAssignToContractorBo.SchemeId, DbType.Int32)
        inParamList.Add(schemeIdparam)
        Dim blockIdparam As ParameterBO = New ParameterBO("blockId", objAssignToContractorBo.BlockId, DbType.Int32)
        inParamList.Add(blockIdparam)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(inParamList, SpNameConstants.GetContractDetailByContractorId)
        resultDataset.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorDetailDt)
        iResultDataReader.Close()
    End Sub

    Public Sub getContactEmailDetail(ByRef contactId As Integer, ByRef detailsForEmailDS As DataSet)
        Dim inParamList As ParameterList = New ParameterList()
        Dim contactIdParam As ParameterBO = New ParameterBO("employeeId", contactId, DbType.Int32)
        inParamList.Add(contactIdParam)
        Dim datareader As IDataReader = MyBase.SelectRecord(inParamList, SpNameConstants.GetContactEmailDetail)
        detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContactEmailDetailsDt)
    End Sub

    Public Sub GetVatDropDownValues(ByRef vatBoList As List(Of VatBo))
        Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetVatDropDownValues)

        If vatBoList Is Nothing Then
            vatBoList = New List(Of VatBo)()
        End If

        While myDataReader.Read()
            Dim id As Integer = 0
            Dim description As String = String.Empty
            Dim vatRate As Decimal = Nothing

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("vatRate")) Then
                vatRate = myDataReader.GetDecimal(myDataReader.GetOrdinal("vatRate"))
            End If

            Dim objVatBo As VatBo = New VatBo(id, description, vatRate)
            vatBoList.Add(objVatBo)
        End While

        myDataReader.Close()
    End Sub

    Public Sub GetVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetVatDropDownValues)
    End Sub

    Public Sub GetAreaDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetAreaDropDownValues)
    End Sub

    Public Sub GetAttributesSubItemsDropdownValuesByAreaId(ByRef dropDownList As List(Of DropDownBO), ByVal areaId As Integer)
        Dim inParamList As ParameterList = New ParameterList()
        Dim areaIdparam As ParameterBO = New ParameterBO("areaId", areaId, DbType.Int32)
        inParamList.Add(areaIdparam)
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetAttributesSubItemsDropDownValuesByAreaId, inParamList)
    End Sub

    Public Function GetItemMSATDetailByItemId(ByVal itemId As Integer, ByVal schemeId As Integer?, ByVal blockId As Integer?) As DataSet
        Dim parameterList As ParameterList = New ParameterList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
        parameterList.Add(itemIdParam)
        Dim schemeIdParam As ParameterBO = New ParameterBO("SchemeId", schemeId, DbType.Int32)
        parameterList.Add(schemeIdParam)
        Dim blockIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
        parameterList.Add(blockIdParam)
        Dim dtMST As DataTable = New DataTable()
        dtMST.TableName = ApplicationConstants.mst
        resultDataSet.Tables.Add(dtMST)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetItemMSATDetailByItemId)
        resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtMST)
        iResultDataReader.Close()
        Return resultDataSet
    End Function

    Public Function assignToContractor(ByVal assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Dim inParamList As ParameterList = New ParameterList()
        Dim outParamList As ParameterList = New ParameterList()
        Dim pdrContractorIdParam As ParameterBO = New ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32)
        inParamList.Add(pdrContractorIdParam)
        Dim journalIdParam As ParameterBO = New ParameterBO("JournalId", assignToContractorBo.JournalId, DbType.Int32)
        inParamList.Add(journalIdParam)
        Dim contractroIdParam As ParameterBO = New ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(contractroIdParam)
        Dim contactIdParam As ParameterBO = New ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32)
        inParamList.Add(contactIdParam)
        Dim userIdParam As ParameterBO = New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
        inParamList.Add(userIdParam)
        Dim estimateParam As ParameterBO = New ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal)
        inParamList.Add(estimateParam)
        Dim estimateRefParam As ParameterBO = New ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String)
        inParamList.Add(estimateRefParam)
        Dim contractStartDateParam As ParameterBO = New ParameterBO("ContractStartDate", assignToContractorBo.ContractStartDate, DbType.String)
        inParamList.Add(contractStartDateParam)
        Dim contractEndDateParam As ParameterBO = New ParameterBO("ContractEndDate", assignToContractorBo.ContractEndDate, DbType.String)
        inParamList.Add(contractEndDateParam)
        Dim msatTypeParam As ParameterBO = New ParameterBO("MsatType", assignToContractorBo.MsatType, DbType.String)
        inParamList.Add(msatTypeParam)
        Dim poStatusParam As ParameterBO = New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
        inParamList.Add(poStatusParam)
        Dim contractorWorkDetalParam As ParameterBO = New ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured)
        inParamList.Add(contractorWorkDetalParam)
        Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
        outParamList.Add(isSavedParam)
        Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", -1, DbType.Int32)
        outParamList.Add(journalIdOutParam)
        outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.AssignWorkToContractor)
        assignToContractorBo.JournalId = Convert.ToInt32(outParamList(1).Value)
        Dim isSaved As Boolean = False
        isSaved = Convert.ToBoolean(outParamList(0).Value)
        Return isSaved
    End Function

    Public Function assignToContractorForPropertyPO(ByVal assignToContractorBo As PropertyAssignToContractorBo) As Boolean

        Dim inParamList As ParameterList = New ParameterList()
        Dim outParamList As ParameterList = New ParameterList()

        Dim pdrIncServiceChargeParam As ParameterBO = New ParameterBO("incServiceCharge", assignToContractorBo.IncInSChge, DbType.Boolean)
        inParamList.Add(pdrIncServiceChargeParam)
        Dim pdrPropApporParam As ParameterBO = New ParameterBO("propertyApportionment", assignToContractorBo.PropertyApportionment, DbType.Decimal)
        inParamList.Add(pdrPropApporParam)
        Dim pdrPropertyIdParam As ParameterBO = New ParameterBO("propertyId", assignToContractorBo.PropertyId, DbType.String)
        inParamList.Add(pdrPropertyIdParam)
        Dim pdrAttributeTypeIdParam As ParameterBO = New ParameterBO("attributeTypeId", assignToContractorBo.AttributeTypeId, DbType.Int32)
        inParamList.Add(pdrAttributeTypeIdParam)
        Dim pdrProvisionIdParam As ParameterBO = New ParameterBO("provisionId", assignToContractorBo.ProvisionId, DbType.Int32)
        inParamList.Add(pdrProvisionIdParam)
        Dim pdrItemIdParam As ParameterBO = New ParameterBO("itemId", assignToContractorBo.ItemId, DbType.Int32)
        inParamList.Add(pdrItemIdParam)
        Dim pdrContractorIdParam As ParameterBO = New ParameterBO("pdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32)
        inParamList.Add(pdrContractorIdParam)
        Dim contractroIdParam As ParameterBO = New ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(contractroIdParam)
        Dim contactIdParam As ParameterBO = New ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32)
        inParamList.Add(contactIdParam)
        Dim poNameParam As ParameterBO = New ParameterBO("POName", assignToContractorBo.POName, DbType.String)
        inParamList.Add(poNameParam)
        Dim userIdParam As ParameterBO = New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
        inParamList.Add(userIdParam)
        Dim estimateParam As ParameterBO = New ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal)
        inParamList.Add(estimateParam)
        Dim estimateRefParam As ParameterBO = New ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String)
        inParamList.Add(estimateRefParam)
        Dim poStatusParam As ParameterBO = New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
        inParamList.Add(poStatusParam)
        Dim contractStartDateParam As ParameterBO = New ParameterBO("ContractStartDate", assignToContractorBo.ContractStartDate, DbType.String)
        inParamList.Add(contractStartDateParam)
        Dim contractEndDateParam As ParameterBO = New ParameterBO("ContractEndDate", assignToContractorBo.ContractEndDate, DbType.String)
        inParamList.Add(contractEndDateParam)
        Dim contractorWorkDetalParam As ParameterBO = New ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured)
        inParamList.Add(contractorWorkDetalParam)
        Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
        outParamList.Add(isSavedParam)
        Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", -1, DbType.Int32)
        outParamList.Add(journalIdOutParam)
        Dim orderIdOutParam As ParameterBO = New ParameterBO("orderIdOut", -1, DbType.Int32)
        outParamList.Add(orderIdOutParam)
        Dim expenditureOutParam As ParameterBO = New ParameterBO("expenditureOut", -1, DbType.Int32)
        outParamList.Add(expenditureOutParam)

        outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.AssignWorkToContractorForPropertyPO)
        assignToContractorBo.JournalId = Convert.ToInt32(outParamList(1).Value)
        assignToContractorBo.OrderId = Convert.ToInt32(outParamList(2).Value)
        If Not outParamList(3).Value.Equals(DBNull.Value) Then assignToContractorBo.ExpenditureId = Convert.ToInt32(outParamList(3).Value)
        Dim isSaved As Boolean = False
        isSaved = Convert.ToBoolean(outParamList(0).Value)
        Return isSaved
    End Function

    Public Sub getdetailsForEmail(ByRef assignToContractorBo As PropertyAssignToContractorBo, ByRef detailsForEmailDS As DataSet)
        Dim inParamList As ParameterList = New ParameterList()
        Dim contractorIdParam As ParameterBO = New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
        inParamList.Add(contractorIdParam)
        Dim inspectionJournalId As ParameterBO = New ParameterBO("inspectionJournalId", assignToContractorBo.InspectionJournalId, DbType.Int32)
        inParamList.Add(inspectionJournalId)
        Dim empolyeeIdParam As ParameterBO = New ParameterBO("empolyeeId", assignToContractorBo.EmpolyeeId, DbType.Int32)
        inParamList.Add(empolyeeIdParam)
        Dim datareader As IDataReader = MyBase.SelectRecord(inParamList, SpNameConstants.GetDetailforEmailToContractor)
        detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContractorDetailsDt, ApplicationConstants.PropertyDetailsDT, ApplicationConstants.TenantDetailsDt, ApplicationConstants.TenantRiskDetailsDt, ApplicationConstants.TenantVulnerabilityDetailsDt, ApplicationConstants.PurchaseOrderDetailsDt, ApplicationConstants.AppointmentSupplierDetailsDt)
        datareader.Close()
    End Sub

    Public Sub getBudgetHolderByOrderId(ByRef resultDs As DataSet, ByVal orderId As Integer, ByVal expenditureId As Integer)
        Dim parametersList As ParameterList = New ParameterList()
        Dim orderIdParam As ParameterBO = New ParameterBO("orderId", orderId, DbType.Int32)
        parametersList.Add(orderIdParam)
        Dim expenditureIdParam As ParameterBO = New ParameterBO("expenditureId", expenditureId, DbType.Int32)
        parametersList.Add(expenditureIdParam)
        Dim datareader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyBudgetHolderByOrderId)
        resultDs.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContractorDetailsDt)
    End Sub

    Public Sub getPropertyDetailsForEmail(ByRef assignToContractorBo As PropertyAssignToContractorBo, ByRef detailsForEmailDS As DataSet)
        Dim inParamList As ParameterList = New ParameterList()
        Dim contractorIdParam As ParameterBO = New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
        inParamList.Add(contractorIdParam)
        Dim inspectionJournalId As ParameterBO = New ParameterBO("inspectionJournalId", assignToContractorBo.InspectionJournalId, DbType.Int32)
        inParamList.Add(inspectionJournalId)
        Dim empolyeeIdParam As ParameterBO = New ParameterBO("empolyeeId", assignToContractorBo.EmpolyeeId, DbType.Int32)
        inParamList.Add(empolyeeIdParam)
        Dim datareader As IDataReader = MyBase.SelectRecord(inParamList, SpNameConstants.GetPropertyDetailforEmailToContractor)
        detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContractorDetailsDt, ApplicationConstants.PropertyDetailsDT, ApplicationConstants.PurchaseOrderDetailsDt, ApplicationConstants.AppointmentSupplierDetailsDt)
        datareader.Close()
    End Sub

    Private Sub getDropDownValuesBySpName(ByRef dropDownList As List(Of DropDownBO), ByVal spName As String, ByVal Optional paramList As ParameterList = Nothing)
        Dim myDataReader As IDataReader = MyBase.SelectRecord(paramList, spName)

        If dropDownList Is Nothing Then
            dropDownList = New List(Of DropDownBO)()
        End If

        While (myDataReader.Read())
            Dim id As Integer = 0
            Dim description As String = String.Empty

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
            End If

            Dim objDropDownBo As DropDownBO = New DropDownBO(id, description)
            dropDownList.Add(objDropDownBo)
        End While

        myDataReader.Close()
    End Sub

    Public Sub getAssignToContractorDetailByJournalId(ByRef resultDataSet As DataSet, ByVal journalId As Integer)
        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        Dim contractorWorkDt As DataTable = New DataTable()
        contractorWorkDt.TableName = ApplicationConstants.ContractorWorkDt
        Dim contractorWorkDetailDt As DataTable = New DataTable()
        contractorWorkDetailDt.TableName = ApplicationConstants.ContractorWorkDetailDt
        resultDataSet.Tables.Add(contractorWorkDt)
        resultDataSet.Tables.Add(contractorWorkDetailDt)
        Dim journalIdParam As ParameterBO = New ParameterBO("JournalId", journalId, DbType.Int32)
        parametersList.Add(journalIdParam)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAssignToContractorDetailByJournalId)
        resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorWorkDt, contractorWorkDetailDt)
        iResultDataReader.Close()
    End Sub

    Public Sub getReactiveRepairContractors(ByRef dropDownList As List(Of DropDownBO))
        Dim inParamList As ParameterList = New ParameterList()
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetReactiveRepairContractors, inParamList)
    End Sub

    Public Sub getAllContractors(ByRef dropDownList As List(Of DropDownBO))
        Dim inParamList As ParameterList = New ParameterList()
        getDropDownValuesBySpName(dropDownList, SpNameConstants.GetAllContractorsDropDownValues, inParamList)
    End Sub

    Public Function voidWorkAssignToContractor(ByRef assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Dim inParamList As ParameterList = New ParameterList()
        Dim outParamList As ParameterList = New ParameterList()
        Dim pdrContractorIdParam As ParameterBO = New ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32)
        inParamList.Add(pdrContractorIdParam)
        Dim journalIdParam As ParameterBO = New ParameterBO("inspectionJournalId", assignToContractorBo.JournalId, DbType.Int32)
        inParamList.Add(journalIdParam)
        Dim contractroIdParam As ParameterBO = New ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(contractroIdParam)
        Dim contactIdParam As ParameterBO = New ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32)
        inParamList.Add(contactIdParam)
        Dim userIdParam As ParameterBO = New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
        inParamList.Add(userIdParam)
        Dim estimateParam As ParameterBO = New ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal)
        inParamList.Add(estimateParam)
        Dim estimateRefParam As ParameterBO = New ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String)
        inParamList.Add(estimateRefParam)
        Dim poStatusParam As ParameterBO = New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
        inParamList.Add(poStatusParam)
        Dim requiredWorkIdsParam As ParameterBO = New ParameterBO("requiredWorkIds", assignToContractorBo.RequiredWorkIds, DbType.String)
        inParamList.Add(requiredWorkIdsParam)
        Dim contractorWorkDetalParam As ParameterBO = New ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured)
        inParamList.Add(contractorWorkDetalParam)
        Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
        outParamList.Add(isSavedParam)
        Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", -1, DbType.Int32)
        outParamList.Add(journalIdOutParam)
        outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.RequiredWorksAssignWorkToContractor)
        assignToContractorBo.JournalId = Convert.ToInt32(outParamList(1).Value)
        Dim isSaved As Boolean = False
        isSaved = Convert.ToBoolean(outParamList(0).Value)
        Return isSaved
    End Function

    Public Sub GetContactDropDownValuesAndDetailsbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByVal objAssignToContractorBo As PropertyAssignToContractorBo)

        Dim inParamList As ParameterList = New ParameterList()
        Dim contractorIdparam As ParameterBO = New ParameterBO("contractorId", objAssignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(contractorIdparam)
        getDropDownValuesBySpName(dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList)

    End Sub

    Public Sub GetContactDropDownValuesbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByVal contractorId As Integer)
        Dim inParamList As ParameterList = New ParameterList()
        Dim propertyIdparam As ParameterBO = New ParameterBO("contractorId", contractorId, DbType.Int32)
        inParamList.Add(propertyIdparam)
        getDropDownValuesBySpName(dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList)
    End Sub

    Public Function voidPaintPackAssignToContractor(ByRef assignToContractorBo As PropertyAssignToContractorBo) As Boolean
        Dim inParamList As ParameterList = New ParameterList()
        Dim outParamList As ParameterList = New ParameterList()
        Dim pdrContractorIdParam As ParameterBO = New ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32)
        inParamList.Add(pdrContractorIdParam)
        Dim paintPackIdParam As ParameterBO = New ParameterBO("paintPackId", assignToContractorBo.PaintPackId, DbType.Int32)
        inParamList.Add(paintPackIdParam)
        Dim contractroIdParam As ParameterBO = New ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32)
        inParamList.Add(contractroIdParam)
        Dim contactIdParam As ParameterBO = New ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32)
        inParamList.Add(contactIdParam)
        Dim userIdParam As ParameterBO = New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
        inParamList.Add(userIdParam)
        Dim estimateParam As ParameterBO = New ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal)
        inParamList.Add(estimateParam)
        Dim estimateRefParam As ParameterBO = New ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String)
        inParamList.Add(estimateRefParam)
        Dim poStatusParam As ParameterBO = New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
        inParamList.Add(poStatusParam)
        Dim contractorWorkDetalParam As ParameterBO = New ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured)
        inParamList.Add(contractorWorkDetalParam)
        Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
        outParamList.Add(isSavedParam)
        Dim journalIdOutParam As ParameterBO = New ParameterBO("journalIdOut", -1, DbType.Int32)
        outParamList.Add(journalIdOutParam)
        outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.PaintPackAssignWorkToContractor)
        assignToContractorBo.JournalId = Convert.ToInt32(outParamList(1).Value)
        Dim isSaved As Boolean = False
        isSaved = Convert.ToBoolean(outParamList(0).Value)
        Return isSaved
    End Function
End Class

