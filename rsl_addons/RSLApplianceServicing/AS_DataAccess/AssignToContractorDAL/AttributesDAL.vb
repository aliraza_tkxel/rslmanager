﻿
Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Public Class AttributesDAL
Inherits BaseDAL

    Public Function getItemDetail(ByVal itemId As Integer, ByVal propertyId As String, Optional ByVal childAttributeMappingId As Integer? = Nothing) As DataSet
        Dim parameterList As ParameterList = New ParameterList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
        parameterList.Add(itemIdParam)
        Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.Int32)
        parameterList.Add(propertyIdParam)
        Dim childAttributMappingIdParam As ParameterBO = New ParameterBO("ChildAttributeMappingId", childAttributeMappingId, DbType.Int32)
        parameterList.Add(childAttributMappingIdParam)
        Dim dtParameters As DataTable = New DataTable()
        dtParameters.TableName = ApplicationConstants.parameters
        resultDataSet.Tables.Add(dtParameters)
        Dim dtParametervalues As DataTable = New DataTable()
        dtParametervalues.TableName = ApplicationConstants.parametervalues
        resultDataSet.Tables.Add(dtParametervalues)
        Dim dtpreinsertedvalues As DataTable = New DataTable()
        dtpreinsertedvalues.TableName = ApplicationConstants.preinsertedvalues
        resultDataSet.Tables.Add(dtpreinsertedvalues)
        Dim dtLastReplaced As DataTable = New DataTable()
        dtLastReplaced.TableName = ApplicationConstants.lastReplaced
        resultDataSet.Tables.Add(dtLastReplaced)
        Dim dtMST As DataTable = New DataTable()
        dtMST.TableName = ApplicationConstants.mst
        resultDataSet.Tables.Add(dtMST)
        Dim dtPlannedComponent As DataTable = New DataTable()
        dtPlannedComponent.TableName = ApplicationConstants.plannedComponent
        resultDataSet.Tables.Add(dtPlannedComponent)
        Dim dtCyclic As DataTable = New DataTable()
        dtCyclic.TableName = ApplicationConstants.CyclicServices
        resultDataSet.Tables.Add(dtCyclic)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.getItemDetail)
        resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtParameters, dtParametervalues, dtpreinsertedvalues, dtLastReplaced, dtMST, dtPlannedComponent, dtCyclic)
        iResultDataReader.Close()
        Return resultDataSet
    End Function

End Class

