﻿Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Public Class ProvisionsDAL
    Inherits BaseDAL

    Public Function GetProvisionsData(ByVal schemeId As Integer?, ByVal blockId As Integer?) As DataSet
        Dim resultDataSet As DataSet = New DataSet()
        Dim parameterList As ParameterList = New ParameterList()
        Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
        parameterList.Add(schemeIdParam)
        Dim blockIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
        parameterList.Add(blockIdParam)
        Dim dtProvisions As DataTable = New DataTable()
        dtProvisions.TableName = ApplicationConstants.Provisions
        resultDataSet.Tables.Add(dtProvisions)
        Dim dtMST As DataTable = New DataTable()
        dtMST.TableName = ApplicationConstants.mst
        resultDataSet.Tables.Add(dtMST)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetProvisions)
        resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProvisions, dtMST)
        iResultDataReader.Close()
        Return resultDataSet
    End Function

    Public Function GetProvisionsDataById(ByVal provisionId As Integer) As DataSet
        Dim resultDataSet As DataSet = New DataSet()
        Dim parameterList As ParameterList = New ParameterList()
        Dim provisionIdParam As ParameterBO = New ParameterBO("provisionId", provisionId, DbType.Int32)
        parameterList.Add(provisionIdParam)
        Dim dtProvisions As DataTable = New DataTable()
        dtProvisions.TableName = ApplicationConstants.Provisions
        resultDataSet.Tables.Add(dtProvisions)
        Dim dtMST As DataTable = New DataTable()
        dtMST.TableName = ApplicationConstants.mst
        resultDataSet.Tables.Add(dtMST)
        Dim iResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.GetProvisionItemDetail)
        resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProvisions, dtMST)
        iResultDataReader.Close()
        Return resultDataSet
    End Function

End Class
