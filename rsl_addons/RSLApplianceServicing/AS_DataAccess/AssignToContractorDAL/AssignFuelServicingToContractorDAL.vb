﻿Imports AS_DataAccess
Imports AS_Utilities
Imports AS_BusinessObject
Namespace AS_DataAccess


    Public Class AssignFuelServicingToContractorDAL : Inherits BaseDAL
#Region "Get Drop Down Values By Stored Procedure name and optional input Parameters(filters)"

        Private Sub getDropDownValuesBySpName(ByRef dropDownList As List(Of DropDownBO), ByRef spName As String, Optional ByRef paramList As ParameterList = Nothing)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(paramList, spName)

            If dropDownList Is Nothing Then
                dropDownList = New List(Of DropDownBO)
            End If

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If
                Dim objDropDownBo As New DropDownBO(id, description)
                dropDownList.Add(objDropDownBo)
            End While
        End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

        'Sub GetVatDropDownValues(ByRef vatBoList As List(Of ContractorVatBO))

        '    Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetVatDropDownValues)

        '    If vatBoList Is Nothing Then
        '        vatBoList = New List(Of ContractorVatBO)
        '    End If

        '    ''Iterate the dataReader
        '    While (myDataReader.Read)

        '        Dim id As Integer
        '        Dim description As String = String.Empty
        '        Dim vatRate As Decimal

        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
        '            id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
        '        End If

        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
        '            description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
        '        End If

        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("vatRate")) Then
        '            vatRate = myDataReader.GetDecimal(myDataReader.GetOrdinal("vatRate"))
        '        End If

        '        Dim objVatBo As New ContractorVatBO(id, description, vatRate)
        '        vatBoList.Add(objVatBo)
        '    End While
        'End Sub

        'Sub GetVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        '    getDropDownValuesBySpName(dropDownList, SpNameConstants.GetVatDropDownValues)
        'End Sub

#End Region

#Region "Get Contractor Having Fuel Servicing Contract"

        Public Sub GetFeulServicingContractors(ByRef dropDownList As List(Of DropDownBO))
            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetFuelServicingContractorDropDownValues)
        End Sub
#End Region


#Region "Get Contact DropDown Values By ContractorId"

        Sub GetContactDropDownValuesbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByVal ContractorId As Integer)
            Dim inParamList As New ParameterList

            Dim propertyIdparam As ParameterBO = New ParameterBO("contractorId", ContractorId, DbType.Int32)
            inParamList.Add(propertyIdparam)

            getDropDownValuesBySpName(dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList)
        End Sub

#End Region


#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

        Sub GetVatDropDownValues(ByRef vatBoList As List(Of ContractorVatBO))

            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetVatDropDownValues)

            If vatBoList Is Nothing Then
                vatBoList = New List(Of ContractorVatBO)
            End If

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty
                Dim vatRate As Decimal

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("vatRate")) Then
                    vatRate = myDataReader.GetDecimal(myDataReader.GetOrdinal("vatRate"))
                End If

                Dim objVatBo As New ContractorVatBO(id, description, vatRate)
                vatBoList.Add(objVatBo)
            End While
        End Sub

        Sub GetVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetVatDropDownValues)
        End Sub

#End Region



#Region "Assign Work to Contractor - Save Process"

        Public Function assignToContractor(ByRef assignToContractorBo As AssignToContractorBO) As Boolean

            Dim inParamList As New ParameterList
            Dim outParamList As New ParameterList

            Dim PropertyIdParam As New ParameterBO("PropertyId", assignToContractorBo.PropertyId, DbType.String)
            inParamList.Add(PropertyIdParam)

            Dim blockIdParam As New ParameterBO("BlockId", assignToContractorBo.BlockId, DbType.Int32)
            inParamList.Add(blockIdParam)
            Dim schemeIdParam As New ParameterBO("SchemeId", assignToContractorBo.SchemeId, DbType.Int32)
            inParamList.Add(schemeIdParam)

            Dim contactIdParam As New ParameterBO("contactId", assignToContractorBo.ContactId, DbType.Int32)
            inParamList.Add(contactIdParam)
            Dim contractroIdParam As New ParameterBO("contractorId", assignToContractorBo.ContractorId, DbType.Int32)
            inParamList.Add(contractroIdParam)
            Dim userIdParam As New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
            inParamList.Add(userIdParam)
            Dim EstimateParam As New ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Int32)
            inParamList.Add(EstimateParam)
            Dim EstimateRefParam As New ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String)
            inParamList.Add(EstimateRefParam)
            Dim poStatusParam As New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
            inParamList.Add(poStatusParam)
            Dim journalIdParam As New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
            inParamList.Add(journalIdParam)
            Dim contractorWorkDetalParam As New ParameterBO("ContractorWorksDetail", assignToContractorBo.WorksRequired, SqlDbType.Structured)
            inParamList.Add(contractorWorkDetalParam)
            Dim isSavedParam As New ParameterBO("isSaved", 0, DbType.Boolean)
            outParamList.Add(isSavedParam)
            Dim journalIdOutParam As New ParameterBO("journalIdOut", -1, DbType.Int32)
            outParamList.Add(journalIdOutParam)
            Dim PoCurrentStatusOutParam As New ParameterBO("POStatusId", -1, DbType.Int32)
            outParamList.Add(PoCurrentStatusOutParam)


            Dim orderIdOutParam As New ParameterBO("orderId", -1, DbType.Int32)
            outParamList.Add(orderIdOutParam)

            outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.AssignWorkToContractor)
            assignToContractorBo.JournalId = outParamList.Item(1).Value
            assignToContractorBo.PoCurrentStatus = Convert.ToInt32(outParamList.Item(2).Value)
            assignToContractorBo.OrderId = Convert.ToInt32(outParamList.Item(3).Value)
            Dim isSaved As Boolean = False
            isSaved = outParamList.Item(0).Value
            Return isSaved

        End Function

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

        Sub SetExpenditureValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
            Dim inParamList As New ParameterList

            Dim budgetHeadIdparam As ParameterBO = New ParameterBO("HeadId", BudgetHeadId, DbType.Int32)
            inParamList.Add(budgetHeadIdparam)

            Dim employeeIdparam As ParameterBO = New ParameterBO("userId", EmployeeId, DbType.Int32)
            inParamList.Add(employeeIdparam)

            Dim spName As String = SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, spName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty
                Dim limit As Decimal = 0.0
                Dim remaining As Decimal = 0.0

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LIMIT")) Then
                    limit = myDataReader.GetDecimal(myDataReader.GetOrdinal("LIMIT"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("REMAINING")) Then
                    remaining = myDataReader.GetDecimal(myDataReader.GetOrdinal("REMAINING"))
                End If

                Dim objExpenditureBo As New ExpenditureBO(id, description, limit, remaining)
                expenditureBOList.Add(objExpenditureBo)
            End While

        End Sub

#End Region


#Region "Appliance Get Details for email"

        Sub getdetailsForEmail(ByRef assignToContractorBo As AssignToContractorBO, ByRef detailsForEmailDS As DataSet)
            Dim inParamList As New ParameterList

            Dim contractorIdParam As New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
            inParamList.Add(contractorIdParam)

            Dim propertyIdParam As New ParameterBO("propertyId", assignToContractorBo.PropertyId, DbType.String)
            inParamList.Add(propertyIdParam)

            Dim empolyeeIdParam As New ParameterBO("empolyeeId", assignToContractorBo.UserId, DbType.Int32)
            inParamList.Add(empolyeeIdParam)

            Dim contractorsContactIdParam As New ParameterBO("contractorsContactIdParam", assignToContractorBo.ContactId, DbType.Int32)
            inParamList.Add(contractorsContactIdParam)

            Dim plannedContractorIdParam As New ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32)
            inParamList.Add(plannedContractorIdParam)

            Dim datareader = MyBase.SelectRecord(inParamList, SpNameConstants.GetDetailforEmailToApplianceServicingContractor)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.ContractorDetailsDt _
                                   , ApplicationConstants.PropertyDetailsDT _
                                   , ApplicationConstants.TenantDetailsDt _
                                   , ApplicationConstants.TenantRiskDetailsDt _
                                   , ApplicationConstants.TenantVulnerabilityDetailsDt _
                                   , ApplicationConstants.OrderedByDetailsDt _
                                   , ApplicationConstants.AsbestosDt)
        End Sub

#End Region

#Region "get Budget Holder By OrderId"

        Sub getBudgetHolderByOrderId(ByRef resultDs As DataSet, ByRef orderId As Integer)

            Dim parametersList As New ParameterList()

            Dim orderIdParam As New ParameterBO("orderId", orderId, DbType.Int32)
            parametersList.Add(orderIdParam)

            MyBase.LoadDataSet(resultDs, parametersList, SpNameConstants.GetBudgetHolderByOrderId)

        End Sub

#End Region



#Region "Get Max cost to be entered For Assign Fuel Servicing Contrator"

        Public Function GetMaxCost(ByRef expenditureId As Integer)

            Dim inParamList As New ParameterList
            Dim outParamList As New ParameterList
            Dim ExpenditureIdParam As New ParameterBO("ExpenditureId", expenditureId, DbType.Int32)
            inParamList.Add(ExpenditureIdParam)
            Dim MaxCostParam As New ParameterBO("MaxAmount", 0, DbType.Int32)
            outParamList.Add(MaxCostParam)
            outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.GetMaxCostValue)
            Return outParamList.Item(0).Value

        End Function

#End Region






    End Class
End Namespace