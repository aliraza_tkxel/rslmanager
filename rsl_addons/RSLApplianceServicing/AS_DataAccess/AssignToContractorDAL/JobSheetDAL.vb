﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess
    Public Class JobSheetDAL : Inherits BaseDAL

#Region "Get Job Sheed Summary Detail By JSN(Job Sheet Number)"

        Sub getJobSheetSummaryByJsnAC(ByRef ds As DataSet, ByRef jsn As String)
            Dim inParametersList As New ParameterList
            Dim jsnparameter As ParameterBO = New ParameterBO("JSN", jsn, DbType.String)

            inParametersList.Add(jsnparameter)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.getPlannedJobSheetSummaryByJSN_AC)

            ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)
        End Sub

#End Region
#Region "Get Job Sheed Summary Detail By JSNAC"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            Dim inParametersList As New ParameterList
            Dim jsnparameter As ParameterBO = New ParameterBO("JSN", jsn, DbType.String)

            inParametersList.Add(jsnparameter)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.getPlannedJobSheetSummaryByJSN)

            ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)
        End Sub

#End Region
#Region "Get Status Look Up For Job Sheet Summary Update"

        Sub getPlannedStatusLookUp(ByRef dsPlannedStatus As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(dsPlannedStatus, parametersList, outParametersList, SpNameConstants.getPlannedStatusLookUp)
        End Sub

#End Region

    End Class
End Namespace



