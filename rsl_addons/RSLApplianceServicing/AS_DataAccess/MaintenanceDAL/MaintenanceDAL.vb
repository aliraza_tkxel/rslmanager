﻿

Imports AS_BusinessObject
Imports AS_DataAccess
Imports AS_Utilities

Public Class MaintenanceDAL
    Inherits BaseDAL

    Public Sub getMaintenanceTypes(ByRef resultDataSet As DataSet)
        Dim paramList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetAttributeTypeForAssignToContractor)
    End Sub

    Public Sub getBlocksBySchemeId(ByVal schemeId As Integer, ByRef resultDataSet As DataSet)
        Dim paramList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        Dim schemeIdParam As ParameterBO = New ParameterBO("SchemeId", schemeId, DbType.Int32)
        paramList.Add(schemeIdParam)
        LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetBlockListByScheme)
    End Sub

    Public Sub getPropertiesBySchemeId(ByVal schemeId As Integer, ByRef resultDataSet As DataSet)
        Dim paramList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        Dim schemeIdParam As ParameterBO = New ParameterBO("SchemeId", schemeId, DbType.Int32)
        Dim blockIdParam As ParameterBO = New ParameterBO("BlockId", 0, DbType.Int32)
        paramList.Add(schemeIdParam)
        paramList.Add(blockIdParam)
        LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetPropertiesByScheme)
    End Sub

    Public Sub getPropertiesByBlockId(ByVal blockId As Integer, ByRef resultDataSet As DataSet)
        Dim paramList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()
        Dim schemeIdParam As ParameterBO = New ParameterBO("SchemeId", -1, DbType.Int32)
        Dim blockIdParam As ParameterBO = New ParameterBO("BlockId", blockId, DbType.Int32)
        paramList.Add(schemeIdParam)
        paramList.Add(blockIdParam)
        LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetPropertiesByScheme)
    End Sub

End Class

