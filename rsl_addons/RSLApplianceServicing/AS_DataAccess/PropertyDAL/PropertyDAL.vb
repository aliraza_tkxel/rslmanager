﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess
    Public Class PropertyDAL : Inherits BaseDAL
        Dim el As New FLS_Utilities.ErrorLogger
#Region "get Property Documents"
        Public Function getPropertyDocuments(ByRef resultDataSet As DataSet, ByVal objPropertyDocumentBO As PropertyDocumentBO, ByRef objPageSortBo As PageSortBO, Optional ByVal isSerachClick As Boolean = False)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyDocumentBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            If isSerachClick = True Then
                Dim expiryDateParam As ParameterBO = New ParameterBO("expiryDate", If(objPropertyDocumentBO.ExpiryDate = "", DBNull.Value, objPropertyDocumentBO.ExpiryDate), DbType.DateTime)
                parametersList.Add(expiryDateParam)

                Dim documentDateParam As ParameterBO = New ParameterBO("documentDate", If(objPropertyDocumentBO.DocumentDate = "", DBNull.Value, objPropertyDocumentBO.DocumentDate), DbType.DateTime)
                parametersList.Add(documentDateParam)

                Dim documentCategory As ParameterBO = New ParameterBO("documentCategory", objPropertyDocumentBO.DocumentCategory, DbType.String)
                parametersList.Add(documentCategory)

                Dim typeParam As ParameterBO = New ParameterBO("DocumentTypeId", objPropertyDocumentBO.DocumentTypeId, DbType.Int32)
                parametersList.Add(typeParam)

                Dim keywordParam As ParameterBO = New ParameterBO("keyword", objPropertyDocumentBO.Keyword, DbType.String)
                parametersList.Add(keywordParam)
            End If

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPropertyDocumentsInfo)
            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "Save Property Document Upload"

        Public Sub savePropertyDocumentUpload(ByVal objPropertyDocumentBO As PropertyDocumentBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyDocumentBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim documentCategory As ParameterBO = New ParameterBO("documentCategory", objPropertyDocumentBO.DocumentCategory, DbType.String)
            parametersList.Add(documentCategory)

            Dim typeParam As ParameterBO = New ParameterBO("DocumentTypeId", objPropertyDocumentBO.DocumentTypeId, DbType.Int32)
            parametersList.Add(typeParam)

            Dim subtypeParam As ParameterBO = New ParameterBO("DocumentSubTypeId", objPropertyDocumentBO.DocumentSubTypeId, DbType.Int32)
            parametersList.Add(subtypeParam)

            Dim documentPathParam As ParameterBO = New ParameterBO("documentPath", objPropertyDocumentBO.DocumentPath, DbType.String)
            parametersList.Add(documentPathParam)

            Dim keywordParam As ParameterBO = New ParameterBO("keyword", objPropertyDocumentBO.Keyword, DbType.String)
            parametersList.Add(keywordParam)

            Dim sizeParam As ParameterBO = New ParameterBO("documentSize", objPropertyDocumentBO.DocumentSize, DbType.String)
            parametersList.Add(sizeParam)

            Dim formatParam As ParameterBO = New ParameterBO("documentFormat", objPropertyDocumentBO.DocumentFormat, DbType.String)
            parametersList.Add(formatParam)

            Dim documentNameParam As ParameterBO = New ParameterBO("documentName", objPropertyDocumentBO.DocumentName, DbType.String)
            parametersList.Add(documentNameParam)

            Dim uploadedByParam As ParameterBO = New ParameterBO("uploadedBy", objPropertyDocumentBO.UploadedBy, DbType.Int32)
            parametersList.Add(uploadedByParam)
            Dim expiryDateParam As ParameterBO = New ParameterBO("expiryDate", objPropertyDocumentBO.ExpiryDate, DbType.String)
            parametersList.Add(expiryDateParam)
            Dim documentDateParam As ParameterBO = New ParameterBO("documentDate", objPropertyDocumentBO.DocumentDate, DbType.String)
            parametersList.Add(documentDateParam)

            Dim EpcCategoryParam As ParameterBO = New ParameterBO("EpcRating", objPropertyDocumentBO.EpcRating, DbType.Int32)
            parametersList.Add(EpcCategoryParam)

            Dim CP12NumberParam As ParameterBO = New ParameterBO("CP12Number", objPropertyDocumentBO.CP12Number, DbType.String)
            parametersList.Add(CP12NumberParam)

            Dim CP12DocumentParam As ParameterBO = New ParameterBO("cp12Doc", objPropertyDocumentBO.CP12Dcoument, DbType.Binary)
            parametersList.Add(CP12DocumentParam)



            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SavePropertyDocumentInformation)

        End Sub

#End Region

#Region "Delete Uploaded Document by Id and get Document file path"

        Public Sub deleteDocumentByIDandGetPath(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim documentIdParam As ParameterBO = New ParameterBO("documentId", objPropertyDocumentBO.DocumentId, DbType.Int32)
            parametersList.Add(documentIdParam)

            Dim documentPathParam As ParameterBO = New ParameterBO("documentPath", objPropertyDocumentBO.DocumentId, DbType.String)
            outParametersList.Add(documentPathParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeletePropertyDocumentsByDocumentId)
            objPropertyDocumentBO.DocumentPath = outParametersList.Item(0).Value.ToString()

        End Sub

#End Region

#Region "Get Uploaded Document Information by Id"

        Public Sub getPropertyDocumentInformationById(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            Dim parametersList As ParameterList = New ParameterList()

            Dim documentIdParam As ParameterBO = New ParameterBO("documentId", objPropertyDocumentBO.DocumentId, DbType.Int32)
            parametersList.Add(documentIdParam)

            Dim lDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyDocumentInformationById)

            If lDataReader.Read Then
                objPropertyDocumentBO.PropertyId = lDataReader.GetString(lDataReader.GetOrdinal("PropertyId"))
                objPropertyDocumentBO.DocumentTypeId = lDataReader.GetInt32(lDataReader.GetOrdinal("DocumentTypeId"))
                objPropertyDocumentBO.DocumentPath = lDataReader.GetString(lDataReader.GetOrdinal("DocumentName"))
                objPropertyDocumentBO.DocumentSubTypeId = lDataReader.GetInt32(lDataReader.GetOrdinal("DocumentSubTypeId"))
                objPropertyDocumentBO.Keyword = lDataReader.GetString(lDataReader.GetOrdinal("Keywords"))
                'objPropertyDocumentBO.DocumentSize = lDataReader.GetString(lDataReader.GetOrdinal("DocumentSize"))
                'objPropertyDocumentBO.DocumentFormat = lDataReader.GetString(lDataReader.GetOrdinal("DocumentFormat"))
            End If

        End Sub

#End Region

#Region "Get Latest Epc Document"

        Public Sub getLatestEpcDocument(ByVal objPropertyDocumentBO As PropertyDocumentBO)
            Dim parametersList As ParameterList = New ParameterList()

            Dim lDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetLatestEpcDocument)

            If lDataReader.Read Then
                objPropertyDocumentBO.PropertyId = lDataReader.GetString(lDataReader.GetOrdinal("PropertyId"))
                objPropertyDocumentBO.DocumentTypeId = lDataReader.GetInt32(lDataReader.GetOrdinal("DocumentTypeId"))
                objPropertyDocumentBO.DocumentPath = lDataReader.GetString(lDataReader.GetOrdinal("DocumentName"))
                objPropertyDocumentBO.DocumentSubTypeId = lDataReader.GetInt32(lDataReader.GetOrdinal("DocumentSubTypeId"))
                objPropertyDocumentBO.Keyword = lDataReader.GetString(lDataReader.GetOrdinal("Keywords"))
                objPropertyDocumentBO.DocumentSize = lDataReader.GetString(lDataReader.GetOrdinal("DocumentSize"))
                objPropertyDocumentBO.DocumentFormat = lDataReader.GetString(lDataReader.GetOrdinal("DocumentFormat"))
            End If

        End Sub

#End Region

#Region "Property Activities List"

        Public Sub propertyActivitiesList(ByRef resultDataSet As DataSet, ByRef sortDirection As String, ByRef sortExpression As String, ByRef actionType As Integer, ByRef propertyId As String)

            Dim spGetPropertyActivities As String = SpNameConstants.PropertyActivities
            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim actionTypeParam As ParameterBO = New ParameterBO("actionType", actionType, DbType.Int32)
            parametersList.Add(actionTypeParam)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", sortExpression, DbType.String)
            parametersList.Add(sortColumn)
            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", sortDirection, DbType.String)
            parametersList.Add(sortOrder)

            MyBase.LoadDataSet(resultDataSet, parametersList, spGetPropertyActivities)

        End Sub

#End Region

#Region "Property Letter List"

        Public Sub propertyLetterList(ByRef resultDataSet As DataSet, ByRef journalHistoryId As Int64)

            Dim spPropertyLetter As String = SpNameConstants.PropertyLetter
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int64)
            parametersList.Add(journalHistoryIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spPropertyLetter)

        End Sub

#End Region

#Region "Property Document List"

        Public Sub propertyDocumentList(ByRef resultDataSet As DataSet, ByRef journalHistoryId As Int64)

            Dim spPropertyDocuments As String = SpNameConstants.PropertyDocuments
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int64)
            parametersList.Add(journalHistoryIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, spPropertyDocuments)

        End Sub

#End Region

#Region "Save Property Activity"

        Public Function savePropertyActivity(ByVal objPropActivityBo As PropertyActivityBO, ByRef savedLetterDt As DataTable, ByRef rentBalance As Double, ByRef rentCharge As Double) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropActivityBo.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objPropActivityBo.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim insepctionTypeIdParam As ParameterBO = New ParameterBO("inspectionTypeId", objPropActivityBo.InspectionTypeId, DbType.Int32)
            parametersList.Add(insepctionTypeIdParam)

            Dim createDateParam As ParameterBO = New ParameterBO("createDate", objPropActivityBo.ActionDate, DbType.Date)
            parametersList.Add(createDateParam)

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", objPropActivityBo.StatusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", objPropActivityBo.ActionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objPropActivityBo.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim isLetterAttachParam As ParameterBO = New ParameterBO("isLetterAttached", objPropActivityBo.IsLetterAttached, DbType.Boolean)
            parametersList.Add(isLetterAttachParam)

            Dim isDocAttachParam As ParameterBO = New ParameterBO("isDocumentAttached", objPropActivityBo.IsDocumentAttached, DbType.Boolean)
            parametersList.Add(isDocAttachParam)

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", 0, DbType.Int32)
            outParametersList.Add(journalHistoryIdParam)
            'This procedure will save the record in journal history and it 'll update the record in journal 
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SavePropertyActivity)

            Dim journalHistoryId As Integer = CType(outParametersList.Item(0).Value, Int32)

            'Now we 'll use this journal history id to save the letters
            If (journalHistoryId > 0) Then
                If (Not IsNothing(objPropActivityBo.LetterList)) Then
                    Dim row As DataRow
                    For Each row In savedLetterDt.Rows()
                        Dim letterParamList As ParameterList = New ParameterList()
                        Dim outLetterParamList As ParameterList = New ParameterList()

                        Dim journalHistIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        letterParamList.Add(journalHistIdParam)

                        Dim titleParam As ParameterBO = New ParameterBO("title", row.Item(ApplicationConstants.LetterTextTitleColumn), DbType.String)
                        letterParamList.Add(titleParam)

                        Dim bodyParam As ParameterBO = New ParameterBO("body", row.Item(ApplicationConstants.LetterBodyColumn), DbType.String)
                        letterParamList.Add(bodyParam)

                        Dim stLetterIdParam As ParameterBO = New ParameterBO("standardLetterId", CType(row.Item(ApplicationConstants.StandardLetterId), Integer), DbType.Int32)
                        letterParamList.Add(stLetterIdParam)

                        Dim teamIdParam As ParameterBO = New ParameterBO("teamId", CType(row.Item(ApplicationConstants.TeamIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(teamIdParam)

                        Dim resourceIdParam As ParameterBO = New ParameterBO("resourceId", CType(row.Item(ApplicationConstants.FromResourceIdColumn), Integer), DbType.Int32)
                        letterParamList.Add(resourceIdParam)

                        Dim signOffParam As ParameterBO = New ParameterBO("signOffId", CType(row.Item(ApplicationConstants.SignOffCodeColumn), Integer), DbType.Int32)
                        letterParamList.Add(signOffParam)

                        Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", rentBalance, DbType.Double)
                        letterParamList.Add(rentBalanceParam)

                        Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", rentCharge, DbType.Double)
                        letterParamList.Add(rentChargeParam)

                        Dim dateParam As ParameterBO = New ParameterBO("todayDate", CType(row.Item(ApplicationConstants.TodayDateColumn), Date), DbType.Date)
                        letterParamList.Add(dateParam)

                        MyBase.SelectRecord(letterParamList, outLetterParamList, SpNameConstants.SaveEditedLetter)
                    Next
                End If

                If (Not IsNothing(objPropActivityBo.DocList)) Then
                    For Each item As String In objPropActivityBo.DocList
                        'Save the documents against journal history id
                        Dim docParamList As ParameterList = New ParameterList()
                        Dim outDocParamList As ParameterList = New ParameterList()

                        Dim journalHistIdDocParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.Int32)
                        docParamList.Add(journalHistIdDocParam)

                        Dim docNameParam As ParameterBO = New ParameterBO("documentName", item, DbType.String)
                        docParamList.Add(docNameParam)

                        Dim docPathParam As ParameterBO = New ParameterBO("documentPath", objPropActivityBo.DocumentPath, DbType.String)
                        docParamList.Add(docPathParam)

                        MyBase.SelectRecord(docParamList, outDocParamList, SpNameConstants.SaveDocuments)
                    Next
                End If


            End If

            Return journalHistoryId.ToString()
        End Function

#End Region

#Region "get Property RB RC"

        Public Sub getPropertyRbRc(ByVal propertyId As String, ByRef rentBalance As Double, ByRef rentCharge As Double)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", 0, DbType.Double)
            outParametersList.Add(rentBalanceParam)

            Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", 0, DbType.Double)
            outParametersList.Add(rentChargeParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.PropertyRbRc)

            rentBalance = CType(outParametersList.Item(0).Value, Double)
            rentCharge = CType(outParametersList.Item(1).Value, Double)
        End Sub

#End Region

#Region "Get Property Address"

        Public Function getPropertyAddress(ByRef propertyId) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim propertyAddressParam As ParameterBO = New ParameterBO("propertyAddress", String.Empty, DbType.String)
            outParametersList.Add(propertyAddressParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.GetPropertyAddress)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Property Document"

        Public Sub getPropertyDocument(ByRef documentId As Integer, ByRef journalHistoryId As Integer, ByRef documentName As String, ByRef documentPath As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim docIdParam As ParameterBO = New ParameterBO("documentId", documentId, DbType.String)
            parametersList.Add(docIdParam)

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("journalHistoryId", journalHistoryId, DbType.String)
            parametersList.Add(journalHistoryIdParam)

            Dim docNameParam As ParameterBO = New ParameterBO("documentName", documentName, DbType.String)
            outParametersList.Add(docNameParam)

            Dim docPathParam As ParameterBO = New ParameterBO("documentPath", documentPath, DbType.String)
            outParametersList.Add(docPathParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.GetPropertyDocument)

            documentName = outParametersList.Item(0).Value.ToString()
            documentPath = outParametersList.Item(1).Value.ToString()
        End Sub

#End Region

#Region "Property Appliances List"

        Public Function propertyAppliancesList(ByRef resultDataSet As DataSet, ByRef objPageSortBo As PageSortBO, ByVal propertyId As String, ByVal itemId As Integer)

            Dim spGetPropertyAppliances As String = SpNameConstants.PropertyAppliances
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.String)
            parametersList.Add(itemIdParam)
            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberColumn)
            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)
            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)
            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetPropertyAppliances)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "get Appliance Locations"
        Public Sub getApplianceLocations(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim prefixParam As ParameterBO = New ParameterBO("prefix", prefix, DbType.String)
            parameterList.Add(prefixParam)
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetApplianceLocations)
        End Sub
#End Region

#Region "Get Fuel"
        Public Sub GetFuel(ByRef resultDataSet As DataSet, Optional ByVal certificateNameSelectedValue As Integer = 0)
            Dim parametersList As ParameterList = New ParameterList()
            Dim heatingTypeIdParam As ParameterBO = New ParameterBO("heatingTypeId", certificateNameSelectedValue, DbType.Int32)
            parametersList.Add(heatingTypeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFuelType)
        End Sub
#End Region

#Region "Get Certificate Nmaes"
        Public Sub GetCertificateNames(ByRef resultSet As DataSet, Optional ByVal fuelTypeSelectedValue As Integer = -1)
            Dim parametersList As ParameterList = New ParameterList()
            Dim heatingTypeIdParam As ParameterBO = New ParameterBO("heatingTypeId", fuelTypeSelectedValue, DbType.Int32)
            parametersList.Add(heatingTypeIdParam)

            MyBase.LoadDataSet(resultSet, parametersList, SpNameConstants.GetCertificateNames)
        End Sub
#End Region

#Region "Get Alternative Fuel Type"
        Public Sub GetAlternativeFuelType(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFuelTypeForAlternativeFuel)
        End Sub
#End Region

#Region "get Status"
        Public Sub getStatus(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSelectedStatusType)
        End Sub
#End Region

#Region "get Applliance Type"
        Public Sub getApplianceType(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim prefixParam As ParameterBO = New ParameterBO("prefix", prefix, DbType.String)
            parametersList.Add(prefixParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetApplianceType)
        End Sub
#End Region

#Region "get Make"
        Public Sub getMake(ByRef resultDataSet As DataSet, ByVal prefix As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim prefixParam As ParameterBO = New ParameterBO("prefix", prefix, DbType.String)
            parametersList.Add(prefixParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetMake)
        End Sub
#End Region

#Region "get Flue Type"
        Public Sub getFlueType(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFlueType)
        End Sub
#End Region

#Region "Save Property Appliance "

        Public Function savePropertyAppliance(ByVal objApplianceBo As ApplianceBO) As Integer

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objApplianceBo.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim locationIdParam As ParameterBO = New ParameterBO("locationId", objApplianceBo.LocationId, DbType.Int32)
            parametersList.Add(locationIdParam)
            Dim locationParam As ParameterBO = New ParameterBO("location", objApplianceBo.Location, DbType.String)
            parametersList.Add(locationParam)

            Dim typeIdParam As ParameterBO = New ParameterBO("typeId", objApplianceBo.TypeId, DbType.Int32)
            parametersList.Add(typeIdParam)
            Dim typeParam As ParameterBO = New ParameterBO("type", objApplianceBo.Type, DbType.String)
            parametersList.Add(typeParam)


            Dim makeIdParam As ParameterBO = New ParameterBO("makeId", objApplianceBo.MakeId, DbType.Int32)
            parametersList.Add(makeIdParam)
            Dim makeParam As ParameterBO = New ParameterBO("make", objApplianceBo.Make, DbType.String)
            parametersList.Add(makeParam)

            Dim modelIdParam As ParameterBO = New ParameterBO("modelId", objApplianceBo.ModelId, DbType.Int32)
            parametersList.Add(modelIdParam)
            Dim modelParam As ParameterBO = New ParameterBO("model", objApplianceBo.Model, DbType.String)
            parametersList.Add(modelParam)

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", objApplianceBo.ItemId, DbType.Int32)
            parametersList.Add(itemIdParam)
            Dim itemParam As ParameterBO = New ParameterBO("item", objApplianceBo.Item, DbType.String)
            parametersList.Add(itemParam)
            Dim quantityParam As ParameterBO = New ParameterBO("quantity", objApplianceBo.Quantity, DbType.String)
            parametersList.Add(quantityParam)

            Dim dimensionsParam As ParameterBO = New ParameterBO("dimensions", objApplianceBo.Dimensions, DbType.String)
            parametersList.Add(dimensionsParam)

            Dim serialNumberParam As ParameterBO = New ParameterBO("serialNumber", objApplianceBo.SerialNumber, DbType.String)
            parametersList.Add(serialNumberParam)
            Dim purchaseCostParam As ParameterBO = New ParameterBO("purchaseCost", objApplianceBo.PurchaseCost, DbType.Decimal)
            parametersList.Add(purchaseCostParam)
            Dim lifeSpanParam As ParameterBO = New ParameterBO("lifeSpan", objApplianceBo.LifeSpan, DbType.Int32)
            parametersList.Add(lifeSpanParam)

            Dim datePurchasedParam As ParameterBO = New ParameterBO("datePurchased", Nothing, DbType.Date)

            If Not objApplianceBo.Datepurchased = Date.MinValue Then
                datePurchasedParam.Value = objApplianceBo.Datepurchased
            End If

            parametersList.Add(datePurchasedParam)


            Dim dateRemovedParam As ParameterBO = New ParameterBO("dateRemoved", Nothing, DbType.Date)

            If Not objApplianceBo.DateRemoved = Date.MinValue Then
                dateRemovedParam.Value = objApplianceBo.DateRemoved
            End If

            parametersList.Add(dateRemovedParam)



            Dim notesParam As ParameterBO = New ParameterBO("notes", objApplianceBo.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim existingApplianceIdParam As ParameterBO = New ParameterBO("existingApplianceId", objApplianceBo.ExistingApplianceId, DbType.Int32)
            parametersList.Add(existingApplianceIdParam)


            Dim applianceId As ParameterBO = New ParameterBO("applianceId", 0, DbType.Int32)
            outParametersList.Add(applianceId)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SavePropertyAppliance)

            Return CType(outParametersList.Item(0).Value, Int32)
        End Function
#End Region

#Region "Delete Property Appliance "

        Public Function deletePropertyAppliance(ByVal applianceId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim applianceIdParam As ParameterBO = New ParameterBO("applianceId", applianceId, DbType.Int32)
            parametersList.Add(applianceIdParam)

            Dim isDeletedParam As ParameterBO = New ParameterBO("isDeleted", 0, DbType.Boolean)
            outParametersList.Add(isDeletedParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.DeletePropertyAppliance)

            Return CType(outParametersList.Item(0).Value, Boolean)
        End Function
#End Region

#Region "Attributes"

#Region "get Locations"
        Public Sub getLocations(ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getLocations)
        End Sub
#End Region

#Region "get Areas by Location Id"
        Sub getAreasByLocationId(ByVal locationId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim LocationIdParam As ParameterBO = New ParameterBO("locationId", locationId, DbType.Int32)
            parameterList.Add(LocationIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getAreasByLocationId)
        End Sub
#End Region

#Region "get Items by Area Id"
        Sub getItemsByAreaId(ByVal areaId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()


            Dim AreaIdParam As ParameterBO = New ParameterBO("areaId", areaId, DbType.Int32)
            parameterList.Add(AreaIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getItemsByAreaId)
        End Sub
#End Region

#Region "get Items by Area Id"
        Sub getSubItemsByItemId(ByVal itemId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()


            Dim ItemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(ItemIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getSubItemsByItemId)
        End Sub
#End Region

#Region "get Items by Item Id"
        Sub getItemsByItemId(ByVal itemId As Integer, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()


            Dim ItemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(ItemIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getItemByItemId)
        End Sub
#End Region

#Region " load ddl Meter Location"
        Sub loadddlMeterLocation(ByRef meterLocationDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(meterLocationDataSet, parameterList, SpNameConstants.getAllMeterLocations)
        End Sub
#End Region

#Region "get Property Gas Info"
        Sub getPropertyGasInfo(ByRef ObjPropertyGasInfoBO As PropertyGasInfoBO)

            Dim gasInfoDataSet As DataSet = New DataSet()
            Dim parameterList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", ObjPropertyGasInfoBO.PropertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            MyBase.LoadDataSet(gasInfoDataSet, parameterList, SpNameConstants.getPropertyGasInfo)
            If gasInfoDataSet.Tables(0).Rows.Count > 0 Then
                ObjPropertyGasInfoBO.IssuedDate = gasInfoDataSet.Tables(0).Rows(0).Item(0).ToString()
                ObjPropertyGasInfoBO.RenewalDate = gasInfoDataSet.Tables(0).Rows(0).Item(1).ToString()
                ObjPropertyGasInfoBO.CertificateNumber = gasInfoDataSet.Tables(0).Rows(0).Item(2).ToString()
                ObjPropertyGasInfoBO.MeterLocationId = gasInfoDataSet.Tables(0).Rows(0).Item(3)
            End If
        End Sub

#End Region

#Region "update Property Gas Info"
        Sub updatePropertyGasInfo(ByRef objPropertyGasInfoBO As PropertyGasInfoBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim propertyId As ParameterBO = New ParameterBO("propertyId", objPropertyGasInfoBO.PropertyId, DbType.String)
            parameterList.Add(propertyId)

            Dim issuedDate As ParameterBO = New ParameterBO("issuedDate", objPropertyGasInfoBO.IssuedDate, DbType.DateTime)
            parameterList.Add(issuedDate)

            Dim renewalDate As ParameterBO = New ParameterBO("renewalDate", objPropertyGasInfoBO.RenewalDate, DbType.DateTime)
            parameterList.Add(renewalDate)

            Dim certificateNumber As ParameterBO = New ParameterBO("certificateNumber", objPropertyGasInfoBO.CertificateNumber, DbType.String)
            parameterList.Add(certificateNumber)

            Dim meterLocationId As ParameterBO = New ParameterBO("meterLocationId", objPropertyGasInfoBO.MeterLocationId, DbType.String)
            parameterList.Add(meterLocationId)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.updatePropertyGasInfo)
        End Sub
#End Region

#Region " load Category DDL"
        Sub loadCategoryDDL(ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getPropertyCategory)
        End Sub
#End Region

#Region "load Property Appliances DDL"
        Sub loadPropertyAppliancesDDL(ByRef propertyId As String, ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getPropertyAppliances)
        End Sub
#End Region

#Region "save Defect"

        Sub saveDefect(ByVal objDefectBO As ApplianceDefectBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            parameterList.Add(New ParameterBO("propertyDefectId", objDefectBO.PropertyDefectId, DbType.Int32))
            parameterList.Add(New ParameterBO("propertyId", objDefectBO.PropertyId, DbType.String))
            parameterList.Add(New ParameterBO("HeatingMappingId", objDefectBO.HeatingMappingId, DbType.Int32))
            parameterList.Add(New ParameterBO("CategoryId", objDefectBO.CategoryId, DbType.Int32))
            parameterList.Add(New ParameterBO("DefectDate", objDefectBO.DefectDate, DbType.DateTime))
            parameterList.Add(New ParameterBO("isDefectIdentified", objDefectBO.IsDefectIdentified, DbType.Boolean))
            parameterList.Add(New ParameterBO("DefectIdentifiedNotes", objDefectBO.DefectIdentifiedNotes, DbType.String))
            parameterList.Add(New ParameterBO("isRemedialActionTaken", objDefectBO.IsRemedialActionTaken, DbType.Boolean))
            parameterList.Add(New ParameterBO("remedialActionNotes", objDefectBO.RemedialActionNotes, DbType.String))
            parameterList.Add(New ParameterBO("isWarningIssued", objDefectBO.IsWarningNoteIssued, DbType.Boolean))
            parameterList.Add(New ParameterBO("ApplianceId", objDefectBO.ApplianceId, DbType.Int32))
            parameterList.Add(New ParameterBO("serialNumber", objDefectBO.SerialNumber, DbType.String))
            parameterList.Add(New ParameterBO("GcNumber", objDefectBO.GcNumber, DbType.String))
            parameterList.Add(New ParameterBO("isWarningFixed", objDefectBO.IsWarningTagFixed, DbType.Boolean))
            parameterList.Add(New ParameterBO("isApplianceDisconnected", objDefectBO.IsAppliancedDisconnected, DbType.Boolean))
            parameterList.Add(New ParameterBO("isPartsRequired", objDefectBO.IsPartsOrdered, DbType.Boolean))
            parameterList.Add(New ParameterBO("isPartsOrdered", objDefectBO.IsPartsOrdered, DbType.Boolean))
            parameterList.Add(New ParameterBO("partsOrderedBy", objDefectBO.PartsOrderedBy, DbType.Int32))
            parameterList.Add(New ParameterBO("partsDueDate", objDefectBO.PartsDueDate, DbType.Date))
            parameterList.Add(New ParameterBO("partsDescription", objDefectBO.PartsDescription, DbType.String))
            parameterList.Add(New ParameterBO("partsLocation", objDefectBO.PartsLocation, DbType.String))
            parameterList.Add(New ParameterBO("isTwoPersonsJob", objDefectBO.IsTwoPersonsJob, DbType.Boolean))
            parameterList.Add(New ParameterBO("reasonForSecondPerson", objDefectBO.ReasonForSecondPerson, DbType.String))
            parameterList.Add(New ParameterBO("estimatedDuration", objDefectBO.EstimatedDuration, DbType.Decimal))
            parameterList.Add(New ParameterBO("priorityId", objDefectBO.PriorityId, DbType.Int32))
            parameterList.Add(New ParameterBO("tradeId", objDefectBO.TradeId, DbType.Int32))
            parameterList.Add(New ParameterBO("filePath", objDefectBO.FilePath, DbType.String))
            parameterList.Add(New ParameterBO("photoName", objDefectBO.PhotoName, DbType.String))
            parameterList.Add(New ParameterBO("PhotoNotes", objDefectBO.PhotosNotes, DbType.String))
            parameterList.Add(New ParameterBO("userID", objDefectBO.UserId, DbType.Int32))

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.saveDefect)
        End Sub

#End Region

#Region "save Defect for scheme/block"

        Sub saveDefectForSchemeBlock(ByVal objDefectBO As ApplianceDefectBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            If objDefectBO.PropertyId = "" Then
                objDefectBO.PropertyId = "0"
            End If
            Dim PId As Integer = objDefectBO.PropertyId
            parameterList.Add(New ParameterBO("propertyDefectId", objDefectBO.PropertyDefectId, DbType.Int32))
            parameterList.Add(New ParameterBO("id", PId, DbType.Int32))
            parameterList.Add(New ParameterBO("requestType", objDefectBO.RequestType, DbType.String))
            parameterList.Add(New ParameterBO("HeatingMappingId", objDefectBO.HeatingMappingId, DbType.Int32))
            parameterList.Add(New ParameterBO("CategoryId", objDefectBO.CategoryId, DbType.Int32))
            parameterList.Add(New ParameterBO("DefectDate", objDefectBO.DefectDate, DbType.DateTime))
            parameterList.Add(New ParameterBO("isDefectIdentified", objDefectBO.IsDefectIdentified, DbType.Boolean))
            parameterList.Add(New ParameterBO("DefectIdentifiedNotes", objDefectBO.DefectIdentifiedNotes, DbType.String))
            parameterList.Add(New ParameterBO("isRemedialActionTaken", objDefectBO.IsRemedialActionTaken, DbType.Boolean))
            parameterList.Add(New ParameterBO("remedialActionNotes", objDefectBO.RemedialActionNotes, DbType.String))
            parameterList.Add(New ParameterBO("isWarningIssued", objDefectBO.IsWarningNoteIssued, DbType.Boolean))
            parameterList.Add(New ParameterBO("serialNumber", objDefectBO.SerialNumber, DbType.String))
            parameterList.Add(New ParameterBO("GcNumber", objDefectBO.GcNumber, DbType.String))
            parameterList.Add(New ParameterBO("isWarningFixed", objDefectBO.IsWarningTagFixed, DbType.Boolean))
            parameterList.Add(New ParameterBO("isApplianceDisconnected", objDefectBO.IsAppliancedDisconnected, DbType.Boolean))
            parameterList.Add(New ParameterBO("isPartsRequired", objDefectBO.IsPartsOrdered, DbType.Boolean))
            parameterList.Add(New ParameterBO("isPartsOrdered", objDefectBO.IsPartsOrdered, DbType.Boolean))
            parameterList.Add(New ParameterBO("partsOrderedBy", objDefectBO.PartsOrderedBy, DbType.Int32))
            parameterList.Add(New ParameterBO("partsDueDate", objDefectBO.PartsDueDate, DbType.Date))
            parameterList.Add(New ParameterBO("partsDescription", objDefectBO.PartsDescription, DbType.String))
            parameterList.Add(New ParameterBO("partsLocation", objDefectBO.PartsLocation, DbType.String))
            parameterList.Add(New ParameterBO("isTwoPersonsJob", objDefectBO.IsTwoPersonsJob, DbType.Boolean))
            parameterList.Add(New ParameterBO("reasonForSecondPerson", objDefectBO.ReasonForSecondPerson, DbType.String))
            parameterList.Add(New ParameterBO("estimatedDuration", objDefectBO.EstimatedDuration, DbType.Decimal))
            parameterList.Add(New ParameterBO("priorityId", objDefectBO.PriorityId, DbType.Int32))
            parameterList.Add(New ParameterBO("tradeId", objDefectBO.TradeId, DbType.Int32))
            parameterList.Add(New ParameterBO("filePath", objDefectBO.FilePath, DbType.String))
            parameterList.Add(New ParameterBO("photoName", objDefectBO.PhotoName, DbType.String))
            parameterList.Add(New ParameterBO("PhotoNotes", objDefectBO.PhotosNotes, DbType.String))
            parameterList.Add(New ParameterBO("userID", objDefectBO.UserId, DbType.Int32))

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.saveDefectForSchemeBlock)
        End Sub

#End Region

#End Region

#Region "Add Category"


        Sub addCategory(ByVal categoryTitle As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()


            Dim categorTitleParam As ParameterBO = New ParameterBO("categoryTitle", categoryTitle, DbType.String)
            parameterList.Add(categorTitleParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.addCategory)

        End Sub
#End Region

#Region "get Property Appliances"



        Sub getPropertyAppliances(ByVal propertyId As String, ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()
            Dim objAppliaceDefectBO As ApplianceDefectBO = New ApplianceDefectBO()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            'Dim applianceIdParam As ParameterBO = New ParameterBO("applianceId", applianceId, DbType.Int32)
            'parameterList.Add(applianceIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getPropertyAppliancesByApplianceId)

            If resultDataSet.Tables(0).Rows.Count > 0 Then
                objAppliaceDefectBO.DefectDate = resultDataSet.Tables(0).Rows(0).Item(0)
                objAppliaceDefectBO.IsDefectIdentified = resultDataSet.Tables(0).Rows(0).Item(2)
                objAppliaceDefectBO.IsRemedialActionTaken = resultDataSet.Tables(0).Rows(0).Item(3)
                objAppliaceDefectBO.CategoryDescription = resultDataSet.Tables(0).Rows(0).Item(4)

            End If
        End Sub

#End Region

#Region "get Property Defect Details"
        Sub getPropertyDefectDetails(ByVal propertyDefectId As String, ByRef objApplianceDefectBO As ApplianceDefectBO)
            Dim argList As String() = propertyDefectId.Split(",")
            Dim appType As String
            Dim pdID As Integer = argList(0)
            Dim parameterList As ParameterList = New ParameterList()

            parameterList.Add(New ParameterBO("propertyDefectId", pdID, DbType.Int32))

            Dim lDataReader As IDataReader
            If argList.Length < 2 Then
                lDataReader = SelectRecord(parameterList, SpNameConstants.getPropertyDefectDetails)
                appType = "Property"
            Else
                If argList(1) = "Property" Then
                    lDataReader = SelectRecord(parameterList, SpNameConstants.getPropertyDefectDetails)
                    appType = "Property"
                Else
                    lDataReader = SelectRecord(parameterList, SpNameConstants.GetSchemeBlockDefectDetails)
                    appType = "Scheme/Block"
                End If
            End If
            
            If lDataReader.Read() Then
                objApplianceDefectBO.PropertyDefectId = lDataReader("PropertyDefectId")
                If appType = "Property" Then
                    objApplianceDefectBO.PropertyId = lDataReader("PropertyId")
                Else
                    objApplianceDefectBO.PropertyId = lDataReader("Id")
                End If
                'objApplianceDefectBO.PropertyId = lDataReader("PropertyId")
                objApplianceDefectBO.RequestType = lDataReader("RequestType")
                objApplianceDefectBO.HeatingMappingId = lDataReader("HeatingMappingId")
                objApplianceDefectBO.CategoryId = lDataReader("CategoryId")
                objApplianceDefectBO.CategoryDescription = lDataReader("DefectCategory")

                objApplianceDefectBO.DefectDate = Now
                If Not IsDBNull(lDataReader("DefectDate")) Then
                    objApplianceDefectBO.DefectDate = lDataReader("DefectDate")
                End If

                objApplianceDefectBO.JournalId = lDataReader("JournalId").ToString()
                objApplianceDefectBO.IsDefectIdentified = lDataReader("IsDefectIdentified")
                objApplianceDefectBO.DefectIdentifiedNotes = lDataReader("DefectNotes")
                objApplianceDefectBO.IsRemedialActionTaken = lDataReader("IsRemedialActionTaken")
                objApplianceDefectBO.RemedialActionNotes = lDataReader("RemedialActionNotes")
                objApplianceDefectBO.IsWarningNoteIssued = lDataReader("IsWarningIssued")

                objApplianceDefectBO.ApplianceId = 0
                If Not IsDBNull(lDataReader("ApplianceId")) Then
                    objApplianceDefectBO.ApplianceId = lDataReader("ApplianceId")
                End If

                objApplianceDefectBO.Boiler = 0
                If Not IsDBNull(lDataReader("BOILER")) Then
                    objApplianceDefectBO.Boiler = lDataReader("BOILER")
                End If

                objApplianceDefectBO.SerialNumber = lDataReader("SerialNumber")
                objApplianceDefectBO.GcNumber = lDataReader("GcNumber")
                objApplianceDefectBO.IsWarningTagFixed = lDataReader("IsWarningFixed")

                objApplianceDefectBO.IsAppliancedDisconnected = Nothing
                If Not IsDBNull(lDataReader("IsApplianceDisconnected")) Then
                    objApplianceDefectBO.IsAppliancedDisconnected = lDataReader("IsApplianceDisconnected")
                End If

                objApplianceDefectBO.IsPartsRequired = Nothing
                If Not IsDBNull(lDataReader("IsPartsRequired")) Then
                    objApplianceDefectBO.IsPartsRequired = lDataReader("IsPartsRequired")
                End If

                objApplianceDefectBO.IsPartsOrdered = Nothing
                If Not IsDBNull(lDataReader("IsPartsOrdered")) Then
                    objApplianceDefectBO.IsPartsOrdered = lDataReader("IsPartsOrdered")
                End If

                objApplianceDefectBO.PartsOrderedBy = Nothing
                If Not IsDBNull(lDataReader("PartsOrderedBy")) Then
                    objApplianceDefectBO.PartsOrderedBy = lDataReader("PartsOrderedBy")
                End If

                objApplianceDefectBO.PartsOrderedByName = lDataReader("PartsOrderedByName")

                objApplianceDefectBO.PartsDueDate = Nothing
                If Not IsDBNull(lDataReader("PartsDueDate")) Then
                    objApplianceDefectBO.PartsDueDate = lDataReader("PartsDueDate")
                End If

                objApplianceDefectBO.PartsDescription = lDataReader("PartsDescription")
                objApplianceDefectBO.PartsLocation = lDataReader("PartsLocation")
                objApplianceDefectBO.IsTwoPersonsJob = lDataReader("IsTwoPersonsJob")
                objApplianceDefectBO.ReasonForSecondPerson = lDataReader("ReasonForSecondPerson")

                objApplianceDefectBO.EstimatedDuration = Nothing
                If Not IsDBNull(lDataReader("EstimatedDuration")) Then
                    objApplianceDefectBO.EstimatedDuration = lDataReader("EstimatedDuration")
                End If

                objApplianceDefectBO.PriorityId = Nothing
                If Not IsDBNull(lDataReader("PriorityId")) Then
                    objApplianceDefectBO.PriorityId = lDataReader("PriorityId")
                End If

                objApplianceDefectBO.PriorityName = lDataReader("PriorityName")

                objApplianceDefectBO.TradeId = Nothing
                If Not IsDBNull(lDataReader("TradeId")) Then
                    objApplianceDefectBO.TradeId = lDataReader("TradeId")
                End If

                objApplianceDefectBO.Trade = lDataReader("Trade")
                objApplianceDefectBO.PhotosNotes = lDataReader("PhotoNotes")
                objApplianceDefectBO.Appliance = lDataReader("APPLIANCE")
            End If
        End Sub
#End Region

#Region "Get Applicanse Case Details"
        Public Sub getApplicanceCaseDetails(ByRef resultDataSet As DataSet, ByRef propertyId As String)
            Dim spGetApplianceCaseDetails As String = SpNameConstants.getApplianceCaseDetails
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim basicInfoDt As DataTable = New DataTable()
            Dim activitesDt As DataTable = New DataTable()
            Dim appliancesDt As DataTable = New DataTable()
            Dim defectsDt As DataTable = New DataTable()

            basicInfoDt.TableName = "BasicInfo"
            activitesDt.TableName = "Activities"
            appliancesDt.TableName = "Appliances"
            defectsDt.TableName = "Defects"

            resultDataSet.Tables.Add(basicInfoDt)
            resultDataSet.Tables.Add(activitesDt)
            resultDataSet.Tables.Add(appliancesDt)
            resultDataSet.Tables.Add(defectsDt)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, spGetApplianceCaseDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, basicInfoDt, activitesDt, appliancesDt, defectsDt)
        End Sub
#End Region

#Region "Get CP12Document By LGSRID"

        Sub getCP12DocumentByLGSRID(ByVal dataSet As DataSet, ByVal LGSRID As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim LGSRIDParam As ParameterBO = New ParameterBO("LGSRID", LGSRID, DbType.Int32)
            parameterList.Add(LGSRIDParam)

            MyBase.LoadDataSet(dataSet, parameterList, SpNameConstants.GetCP12DocumentByLGSRID)
        End Sub

#End Region

#Region "Get Inspection record Document by InspectionId"
        Sub getInspectionRecordByInspectionId(ByVal dataSet As DataSet, ByVal Document As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim DocumentParam As ParameterBO = New ParameterBO("Document", Document, DbType.Int32)
            parameterList.Add(DocumentParam)
            MyBase.LoadDataSet(dataSet, parameterList, SpNameConstants.GetPropertyInspectionDocumentByInspectionId)
        End Sub
#End Region

#Region "Save Print Certificate Activity"
        Sub savePrintCertificateActivity(ByRef objPropertyActivityBO As PropertyActivityBO)
            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyActivityBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objPropertyActivityBO.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim createDateParam As ParameterBO = New ParameterBO("createDate", objPropertyActivityBO.ActionDate, DbType.Date)
            parametersList.Add(createDateParam)

            Dim reqTypeParam As ParameterBO = New ParameterBO("reqType", objPropertyActivityBO.ReqTye, DbType.String)
            parametersList.Add(reqTypeParam)

            'This procedure will save the record in journal history and it 'll update the record in journal 
            MyBase.SelectRecord(parametersList, SpNameConstants.SavePrintCertificateActivity)
        End Sub
#End Region

#Region "Get Tenants By PropertyId"
        Sub getTenantsByPropertyId(ByRef tenantsDS As DataSet, ByVal propertyId As String)
            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            MyBase.LoadDataSet(tenantsDS, parametersList, SpNameConstants.GetTenantsByPropertyId)
        End Sub
#End Region

#Region "Save Email Certificate Activity"

        Sub saveEmailCertificateActivity(ByRef objPropertyActivityBO As PropertyActivityBO)
            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyActivityBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objPropertyActivityBO.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim createDateParam As ParameterBO = New ParameterBO("createDate", objPropertyActivityBO.ActionDate, DbType.Date)
            parametersList.Add(createDateParam)

            'This procedure will save the record in journal history and it 'll update the record in journal 
            MyBase.SelectRecord(parametersList, SpNameConstants.SaveEmailCertificateActivity)
        End Sub

#End Region

#Region "Get Parameters"
        Sub getParameters(ByVal itemId As Integer, ByVal areaId As Integer, ByVal parameterDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim areaIdParam As ParameterBO = New ParameterBO("areaId", areaId, DbType.Int32)
            parameterList.Add(areaIdParam)

            MyBase.LoadDataSet(parameterDataSet, parameterList, SpNameConstants.GetParametersByAreaId)
        End Sub
#End Region

#Region "get Item Detail"
        Sub getItemDetail(ByVal itemId As Integer, ByVal propertyId As String, ByVal resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim itemIdParam As New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim propertyIdParam As New ParameterBO("propertyId", propertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            Dim dtParameters As New DataTable()
            dtParameters.TableName = ApplicationConstants.parameters
            resultDataSet.Tables.Add(dtParameters)

            Dim dtParametervalues As New DataTable()
            dtParametervalues.TableName = ApplicationConstants.parametervalues
            resultDataSet.Tables.Add(dtParametervalues)

            Dim dtpreinsertedvalues As New DataTable()
            dtpreinsertedvalues.TableName = ApplicationConstants.preinsertedvalues
            resultDataSet.Tables.Add(dtpreinsertedvalues)

            Dim dtLastReplaced As New DataTable()
            dtLastReplaced.TableName = ApplicationConstants.lastReplaced
            resultDataSet.Tables.Add(dtLastReplaced)

            Dim dtMST As DataTable = New DataTable()
            dtMST.TableName = ApplicationConstants.mst
            resultDataSet.Tables.Add(dtMST)


            Dim dtPlannedComponent As New DataTable()
            dtPlannedComponent.TableName = ApplicationConstants.plannedComponent
            resultDataSet.Tables.Add(dtPlannedComponent)

            Dim dtCP12Information As New DataTable()
            dtCP12Information.TableName = ApplicationConstants.cP12Information
            resultDataSet.Tables.Add(dtCP12Information)

            Dim dtHeatingMapping As New DataTable()
            dtHeatingMapping.TableName = ApplicationConstants.dtHeatingMapping
            resultDataSet.Tables.Add(dtHeatingMapping)

            Dim dtLGSR As New DataTable()
            dtLGSR.TableName = ApplicationConstants.dtLGSR
            resultDataSet.Tables.Add(dtLGSR)
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, SpNameConstants.getItemDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtParameters, dtParametervalues, dtpreinsertedvalues, dtLastReplaced, dtMST, dtPlannedComponent, dtCP12Information, dtHeatingMapping, dtLGSR)

            'MyBase.LoadDataSet(parameterDataSet, parameterList, SpNameConstants.GetParametersByAreaId)
        End Sub
#End Region

#Region "Get Property Images"
        Sub getPropertyImages(ByVal proerptyId As String, ByVal itemId As Integer, ByVal photosDataSet As DataSet, ByVal heatingMappingId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("proerptyId", proerptyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            Dim heatingMappingIdParam As ParameterBO = New ParameterBO("heatingMappingId", heatingMappingId, DbType.String)
            parameterList.Add(heatingMappingIdParam)
            MyBase.LoadDataSet(photosDataSet, parameterList, SpNameConstants.getPropertyImages)
        End Sub
#End Region

#Region "Get Property Images"
        Sub getPropertyDefectImages(ByVal proerptyId As String, ByVal photosDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            'Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            'parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", proerptyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            MyBase.LoadDataSet(photosDataSet, parameterList, SpNameConstants.getPropertyDefectImages)
        End Sub
#End Region

#Region "Save Photographs"
        Sub savePhotograph(ByVal objPhotographBO As PhotographBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()


            Dim itemIdParam As ParameterBO
            If (objPhotographBO.ItemId = 0) Then
                itemIdParam = New ParameterBO("itemId", DBNull.Value, DbType.Int32)
            Else
                itemIdParam = New ParameterBO("itemId", objPhotographBO.ItemId, DbType.Int32)
            End If
            parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", objPhotographBO.PropertyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            Dim FilePathParam As ParameterBO = New ParameterBO("imagePath", objPhotographBO.FilePath, DbType.String)
            parameterList.Add(FilePathParam)

            Dim ImageName As ParameterBO = New ParameterBO("imageName", objPhotographBO.ImageName, DbType.String)
            parameterList.Add(ImageName)

            Dim Title As ParameterBO = New ParameterBO("title", objPhotographBO.Title, DbType.String)
            parameterList.Add(Title)

            Dim UploadDate As ParameterBO = New ParameterBO("uploadDate", objPhotographBO.UploadDate, DbType.DateTime)
            parameterList.Add(UploadDate)

            Dim CreatedBy As ParameterBO = New ParameterBO("createdBy", objPhotographBO.CreatedBy, DbType.Int32)
            parameterList.Add(CreatedBy)
            Dim HeatingMappingIdBy As ParameterBO = New ParameterBO("HeatingMappingId", objPhotographBO.HeatingMappingId, DbType.Int32)
            parameterList.Add(HeatingMappingIdBy)

            Dim IsDefaultImage As ParameterBO = New ParameterBO("isDefaultImage", objPhotographBO.IsDefaultImage, DbType.Boolean)
            parameterList.Add(IsDefaultImage)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.savePhotograph)
        End Sub
#End Region

#Region "Save Property Image"
        'ByRef fileName As String, ByRef filePath As String, ByRef propertyId As String, ByVal createdBy As Int32
        Sub SavePropertyImage(ByRef objPropertyDetailsBO As PropertiesDetailsBO)

            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()
            Dim imageNameParam As ParameterBO = New ParameterBO("imagename", objPropertyDetailsBO.ImageName, DbType.String)
            parameterList.Add(imageNameParam)

            Dim imagePathParam As ParameterBO = New ParameterBO("imagePath", objPropertyDetailsBO.ImagePath, DbType.String)
            parameterList.Add(imagePathParam)

            Dim createdByParam As ParameterBO = New ParameterBO("createdBy", objPropertyDetailsBO.CreatedBy, DbType.String)
            parameterList.Add(createdByParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyDetailsBO.PropertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            Dim IsDefaultImage As ParameterBO = New ParameterBO("Isdefault", objPropertyDetailsBO.IsDefault, DbType.Boolean)
            parameterList.Add(IsDefaultImage)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.SavePropertyImage)

        End Sub
#End Region




#Region "Save EPC Photographs"
        Sub saveEPCPhotograph(ByVal objPhotographBO As PhotographBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", objPhotographBO.ItemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", objPhotographBO.PropertyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            Dim FilePathParam As ParameterBO = New ParameterBO("imagePath", objPhotographBO.FilePath, DbType.String)
            parameterList.Add(FilePathParam)

            Dim ImageName As ParameterBO = New ParameterBO("imageName", objPhotographBO.ImageName, DbType.String)
            parameterList.Add(ImageName)

            Dim Title As ParameterBO = New ParameterBO("title", objPhotographBO.Title, DbType.String)
            parameterList.Add(Title)

            Dim UploadDate As ParameterBO = New ParameterBO("uploadDate", objPhotographBO.UploadDate, DbType.DateTime)
            parameterList.Add(UploadDate)

            Dim CreatedBy As ParameterBO = New ParameterBO("createdBy", objPhotographBO.CreatedBy, DbType.Int32)
            parameterList.Add(CreatedBy)

            Dim IsDefaultImage As ParameterBO = New ParameterBO("isDefaultImage", objPhotographBO.IsDefaultImage, DbType.Boolean)
            parameterList.Add(IsDefaultImage)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.saveEPCPhotograph)

        End Sub

#End Region

#Region "Get Item Details"
        Sub getItemDetails(ByVal parameterId As Integer, ByRef loadParameterDDL As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim parameterIdParam As ParameterBO = New ParameterBO("parameterId", parameterId, DbType.String)
            parameterList.Add(parameterIdParam)

            MyBase.LoadDataSet(loadParameterDDL, parameterList, SpNameConstants.getItemDetails)
        End Sub
#End Region

#Region "Load Property Details"
        Sub loadPropertyDetails(ByVal propertyId As String, ByRef objProertyDetails As PropertyDetailsBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim resultDataSet As DataSet = New DataSet()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parameterList.Add(propertyIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetPropertyDetails)

            If resultDataSet.Tables(0).Rows.Count > 0 Then
                objProertyDetails.Address = resultDataSet.Tables(0).Rows(0).Item(0).ToString()
                objProertyDetails.PostCode = resultDataSet.Tables(0).Rows(0).Item(1).ToString()
                objProertyDetails.Dev = resultDataSet.Tables(0).Rows(0).Item(2).ToString()
                objProertyDetails.Scheme = resultDataSet.Tables(0).Rows(0).Item(3).ToString()
                objProertyDetails.Status = resultDataSet.Tables(0).Rows(0).Item(4).ToString()
                objProertyDetails.Type = resultDataSet.Tables(0).Rows(0).Item(5).ToString()


            End If
        End Sub
#End Region

#Region "Property Summary Tab Functions"

#Region "Get Tenancy/Property detail"
        ''' <summary>
        ''' Get Tenancy/Property detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyPropertyDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtTenancyPropertyDetail As DataTable = New DataTable()

            dtTenancyPropertyDetail.TableName = "TenancyPropertyDetail"

            resultDataSet.Tables.Add(dtTenancyPropertyDetail)

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTenancyPropertyDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtTenancyPropertyDetail)

        End Sub

#End Region

#Region "Get Accommodation Summary"
        ''' <summary>
        ''' Get Accommodation Summary
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAccommodationSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAccommodation As DataTable = New DataTable()
            dtAccommodation.TableName = "Accommodation"
            resultDataSet.Tables.Add(dtAccommodation)

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAccommodationSummary)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAccommodation)

        End Sub

#End Region

#Region "Get Tenancy History"
        ''' <summary>
        ''' Get Tenancy History
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtTenancyHistory As DataTable = New DataTable()
            dtTenancyHistory.TableName = "TenancyHistory"
            resultDataSet.Tables.Add(dtTenancyHistory)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTenancyHistory)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtTenancyHistory)

        End Sub

#End Region

#Region "Get Asbestos Detail"
        ''' <summary>
        ''' Get Asbestos Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAsbestosDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAsbestos As DataTable = New DataTable()
            dtAsbestos.TableName = "Asbestos"
            resultDataSet.Tables.Add(dtAsbestos)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAsbestosInformation)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAsbestos)

        End Sub

#End Region

#Region "Get Planned Appointments Detail"
        ''' <summary>
        ''' Get Planned Appointments Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPlannedAppointmentsDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPlanned As DataTable = New DataTable()
            dtPlanned.TableName = "Planned"
            resultDataSet.Tables.Add(dtPlanned)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPlannedAppointments)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPlanned)

        End Sub

#End Region

#Region "Get Planned Components Detail"
        ''' <summary>
        ''' Get Planned Components Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPlannedComponentsDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPlanned As DataTable = New DataTable()
            dtPlanned.TableName = ApplicationConstants.PlannedComponents
            resultDataSet.Tables.Add(dtPlanned)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPlannedComponentsDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPlanned)

        End Sub

#End Region


#Region "Get Property Condition Rating Detail"
        ''' <summary>
        ''' Get Property Condition Rating Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPropertyConditionRatingDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtCondition As DataTable = New DataTable()
            dtCondition.TableName = ApplicationConstants.ConditionRating
            resultDataSet.Tables.Add(dtCondition)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyConditionRatingDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtCondition)

        End Sub

#End Region

#Region "Get stock gas and fault Appointments Detail"
        ''' <summary>
        ''' Get stock gas and fault Appointments Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getStockGasFaultAppointments(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtStockGasFaultAppointments As DataTable = New DataTable()
            dtStockGasFaultAppointments.TableName = "StockGasFaultAppointments"
            resultDataSet.Tables.Add(dtStockGasFaultAppointments)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAllAppointments)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtStockGasFaultAppointments)

        End Sub

#End Region

#Region "Get Reported Faults/Defects"
        ''' <summary>
        ''' Get Reported Faults/Defects
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getReportedFaultsDefects(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByVal actionType As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtReportedFaults As DataTable = New DataTable()
            dtReportedFaults.TableName = "ReportedFaults"
            resultDataSet.Tables.Add(dtReportedFaults)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim parameterIdParam As ParameterBO = New ParameterBO("actionType", actionType, DbType.Int32)
            parametersList.Add(parameterIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultsAndDefects)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReportedFaults)

        End Sub

#End Region

#Region "Get Property Address"
        ''' <summary>
        ''' Get Property Address
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyAddress(ByRef propertyId As String) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("propertyAddress", "", DbType.String)
            outParametersList.Add(resultParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.GetPropertySummaryAddress)
            If (IsDBNull(outParametersList.Item(0).Value)) Then
                Return String.Empty
            Else
                Return CType(outParametersList.Item(0).Value, String)
            End If


        End Function

#End Region

#Region "Get Job Sheet Detail"

        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtFaultAndAppointment As DataTable = New DataTable()
            Dim dtAsbestos As DataTable = New DataTable()
            Dim dtCustomer As DataTable = New DataTable()

            dtFaultAndAppointment.TableName = "FaultAppointment"
            dtAsbestos.TableName = "Asbestos"
            dtCustomer.TableName = "Customer"


            resultDataSet.Tables.Add(dtFaultAndAppointment)
            resultDataSet.Tables.Add(dtAsbestos)
            resultDataSet.Tables.Add(dtCustomer)


            Dim jsn As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(jsn)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJobSheetDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultAndAppointment, dtAsbestos, dtCustomer)


        End Sub

#End Region

#Region "Get Property Images"
        ''' <summary>
        ''' Get Property Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            Dim outParam As ParameterBO = New ParameterBO("count", 0, DbType.Int32)
            outParamList.Add(outParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParamList, SpNameConstants.GetPropertySummaryImages)

            If (Not IsDBNull(outParamList.Item(0).Value)) Then
                Return outParamList.Item(0).Value
            Else
                Return 0
            End If

        End Function

#End Region

#Region "Get Property EPC Images"
        ''' <summary>
        ''' Get Property EPC Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyEPCImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim dtPropertyImages As DataTable = New DataTable()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim outParam As ParameterBO = New ParameterBO("count", "", DbType.Int32)
            outParamList.Add(outParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParamList, SpNameConstants.GetPropertySummaryEPCImages)
            If (Not IsDBNull(outParamList.Item(0).Value)) Then
                Return outParamList.Item(0).Value
            Else
                Return 0
            End If

        End Function

#End Region

#End Region

#Region "Get CP12Document By LGSRHISTORYID"

        Sub getCP12DocumentByLGSRHISTORYID(ByVal dataSet As DataSet, ByVal LGSRHISTORYID As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim LGSRHISTORYIDParam As ParameterBO = New ParameterBO("LGSRHISTORYID", LGSRHISTORYID, DbType.Int32)
            parameterList.Add(LGSRHISTORYIDParam)

            MyBase.LoadDataSet(dataSet, parameterList, SpNameConstants.GetCP12DocumentByLGSRHISTORYID)
        End Sub

#End Region

#Region "Get Planned Inspection Document"

        Sub getPlannedInspectionDocument(ByVal dataSet As DataSet, ByVal JournalHistoryId As Integer)
            Dim parameterList As ParameterList = New ParameterList()

            Dim JournalHistoryIdParam As ParameterBO = New ParameterBO("JournalHistoryId", JournalHistoryId, DbType.Int32)
            parameterList.Add(JournalHistoryIdParam)

            MyBase.LoadDataSet(dataSet, parameterList, SpNameConstants.GetPlannedInspectionDocument)
        End Sub

#End Region

#Region "Amend Item Detail"

        Public Sub AmendItemDetail(ByRef objItemAttributeBo As ItemAttributeBO, ByVal itemDetailDt As DataTable, ByVal itemDatesDt As DataTable, ByVal conditionRatingDt As DataTable, ByVal MSATDetailDt As DataTable)
            Dim outParametersList As ParameterList = New ParameterList()
            Dim parameterList As ParameterList = New ParameterList()
            Dim componentID As ParameterBO = New ParameterBO("PropertyId", objItemAttributeBo.PropertyId, DbType.String)
            parameterList.Add(componentID)
            Dim itemId As ParameterBO = New ParameterBO("ItemId", objItemAttributeBo.ItemId, DbType.Int32)
            parameterList.Add(itemId)

            Dim UpdatedBy As ParameterBO = New ParameterBO("UpdatedBy", objItemAttributeBo.UpdatedBy, DbType.Int32)
            parameterList.Add(UpdatedBy)

            Dim heatingMappingId As ParameterBO = New ParameterBO("HeatingMappingId", objItemAttributeBo.HeatingMappingId, DbType.Int32)
            parameterList.Add(heatingMappingId)

            Dim trades As ParameterBO = New ParameterBO("@ItemDetail", itemDetailDt, SqlDbType.Structured)
            parameterList.Add(trades)

            Dim itemDates As ParameterBO = New ParameterBO("@ItemDates", itemDatesDt, SqlDbType.Structured)
            parameterList.Add(itemDates)


            Dim conditionRating As ParameterBO = New ParameterBO("@ConditionWork", conditionRatingDt, SqlDbType.Structured)
            parameterList.Add(conditionRating)

            Dim MSATDetail As ParameterBO = New ParameterBO("@MSATDetail", MSATDetailDt, SqlDbType.Structured)
            parameterList.Add(MSATDetail)

            Dim outParamList As ParameterList = New ParameterList()
            MyBase.SaveRecord(parameterList, outParamList, SpNameConstants.amendPropertyItemDetail)
        End Sub
        Sub deleteHeating(ByVal heatingMappingId As Integer)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramWarrantyID As ParameterBO = New ParameterBO("heatingMappingId", heatingMappingId, DbType.Int32)
            paramList.Add(paramWarrantyID)
            SaveRecord(paramList, outParametersList, SpNameConstants.DeleteHeating)
        End Sub

#End Region

#Region "Save Item Notes"
        Sub saveItemNotes(ByVal objItemNotesBo As ItemNotesBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", objItemNotesBo.ItemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", objItemNotesBo.PropertyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            Dim noteParam As ParameterBO = New ParameterBO("note", objItemNotesBo.Notes, DbType.String)
            parameterList.Add(noteParam)

            Dim showInSchedulingParam As ParameterBO = New ParameterBO("showInScheduling", objItemNotesBo.ShowinScheduling, DbType.Boolean)
            parameterList.Add(showInSchedulingParam)

            Dim showOnAppParam As ParameterBO = New ParameterBO("showOnApp", objItemNotesBo.ShowOnApp, DbType.Boolean)
            parameterList.Add(showOnAppParam)

            Dim createdByParamId As ParameterBO = New ParameterBO("createdBy", objItemNotesBo.CreatedBy, DbType.Int32)
            parameterList.Add(createdByParamId)

            If (objItemNotesBo.HeatingMappingId) IsNot Nothing Then
                Dim heatingMappingIdParamId As ParameterBO = New ParameterBO("heatingMappingId", objItemNotesBo.HeatingMappingId, DbType.Int32)
                parameterList.Add(heatingMappingIdParamId)
            End If

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.saveItemNotes)
        End Sub
#End Region

#Region "Get Item Notes"
        Sub GetItemNotes(ByVal proerptyId As String, ByVal itemId As Integer, ByVal notesDataSet As DataSet, Optional ByVal heatingMappingId As Integer = 0)
            Dim parameterList As ParameterList = New ParameterList()

            Dim itemIdParam As ParameterBO = New ParameterBO("itemId", itemId, DbType.Int32)
            parameterList.Add(itemIdParam)

            Dim proerptyIdParam As ParameterBO = New ParameterBO("proerptyId", proerptyId, DbType.String)
            parameterList.Add(proerptyIdParam)

            Dim heatingMappingIdParamId As ParameterBO = New ParameterBO("heatingMappingId", heatingMappingId, DbType.Int32)
            parameterList.Add(heatingMappingIdParamId)

            MyBase.LoadDataSet(notesDataSet, parameterList, SpNameConstants.getItemNotes)
        End Sub
#End Region

#Region "Update Item Notes"
        Sub UpdateItemNotes(ByVal objItemNotesBo As ItemNotesBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim notesIdParam As ParameterBO = New ParameterBO("notesId", objItemNotesBo.NotesId, DbType.Int32)
            parameterList.Add(notesIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("note", objItemNotesBo.Notes, DbType.String)
            parameterList.Add(notesParam)

            Dim showInSchedulingParam As ParameterBO = New ParameterBO("showInScheduling", objItemNotesBo.ShowinScheduling, DbType.Boolean)
            parameterList.Add(showInSchedulingParam)

            Dim showOnAppParam As ParameterBO = New ParameterBO("showOnApp", objItemNotesBo.ShowOnApp, DbType.Boolean)
            parameterList.Add(showOnAppParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.UpdateItemNotes)
        End Sub
#End Region

#Region "Delete Item Notes"
        Sub DeleteItemNotes(ByVal itemNotesId As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim itemNotesIdParam As ParameterBO = New ParameterBO("notesId", itemNotesId, DbType.Int32)
            parameterList.Add(itemNotesIdParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.DeleteItemNotes)
        End Sub
#End Region

#Region "Get Item Notes Trail"
        Sub GetItemNotesTrail(ByVal itemNotesId As Integer, ByVal notesDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim itemNotesIdParam As ParameterBO = New ParameterBO("notesId", itemNotesId, DbType.Int32)
            parameterList.Add(itemNotesIdParam)

            MyBase.LoadDataSet(notesDataSet, parameterList, SpNameConstants.GetItemNotesTrail)
        End Sub
#End Region


        Sub propertyDocumentUpload(ByVal p1 As String, ByVal p2 As String, ByVal p3 As String, ByVal p4 As String, ByVal p5 As String)
            Throw New NotImplementedException
        End Sub

#Region "Get Models List"

        Public Sub getModelsList(ByRef resultDataset As DataSet, ByVal prefix As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim prefixParam As ParameterBO = New ParameterBO("prefix", prefix, DbType.String)
            parametersList.Add(prefixParam)
            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.GetModelsList)
        End Sub

#End Region

#Region "Health & Safety Tab"
#Region "get Property Health & Safety Infomraiton"
        Public Function getHealthAndSafetyTabInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)
            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.getHealthAndSafetyTabInformation)
            Return outParametersList.Item(0).Value

        End Function
#End Region

#Region "get getRefurbishmentInformation Information"
        Public Sub getRefurbishmentInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.getRefurbishmentInformaiton)
        End Sub
#End Region
#Region "Get Property Asbestos (Level and Elements) & Risk Information "

        Public Sub getPropertyAsbestosAndRisk(ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getAsbestosAndRiskInformation)
            Dim dtAsbestosLevel As DataTable = New DataTable("AsbestosLevel")
            resultDataSet.Tables.Add(dtAsbestosLevel)
            Dim dtAsbestosElements As DataTable = New DataTable("AsbestosElements")
            resultDataSet.Tables.Add(dtAsbestosElements)
            Dim dtRisk As DataTable = New DataTable("Risk")
            resultDataSet.Tables.Add(dtRisk)
            Dim dtLevel As DataTable = New DataTable("Level")
            resultDataSet.Tables.Add(dtLevel)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAsbestosLevel, dtAsbestosElements, dtRisk, dtLevel)
        End Sub

#End Region

#Region "Get Property Asbestos (Level and Elements) & Risk Information, Added, Removed and Notes"
        Public Sub getPropertyAsbestosRiskAndOtherInfo(ByRef resultDataSet As DataSet, ByRef asbestosId As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim asbestosIDParam As ParameterBO = New ParameterBO("asbestosID", asbestosId, DbType.Int32)
            parameterList.Add(asbestosIDParam)
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getAsbestosRiskAndOtherInfo)
        End Sub

#End Region
#Region "Save Asbestos Information"
        Sub saveAsbestos(ByVal objPropertyAsbestosBO As PropertyAsbestosBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim asbestosLevelId As ParameterBO = New ParameterBO("asbestosLevelId", objPropertyAsbestosBO.AsbestosLevelId, DbType.Int32)
            parameterList.Add(asbestosLevelId)

            Dim asbestosElementId As ParameterBO = New ParameterBO("asbestosElementId", objPropertyAsbestosBO.AsbestosElementId, DbType.Int32)
            parameterList.Add(asbestosElementId)

            Dim RiskIdParam As ParameterBO = New ParameterBO("riskId", objPropertyAsbestosBO.RiskId, DbType.String)
            parameterList.Add(RiskIdParam)

            Dim AddedDateParam As ParameterBO = New ParameterBO("addedDate", objPropertyAsbestosBO.AddedDate, DbType.Date)
            parameterList.Add(AddedDateParam)

            Dim RemovedParam As ParameterBO = New ParameterBO("removedDate", objPropertyAsbestosBO.RemovedDate, DbType.String)
            parameterList.Add(RemovedParam)

            Dim riskLevelParam As ParameterBO = New ParameterBO("riskLevel", objPropertyAsbestosBO.RiskLevel, DbType.Int32)
            parameterList.Add(riskLevelParam)

            Dim urgentActionRequiredParam As ParameterBO = New ParameterBO("UrgentActionRequired", objPropertyAsbestosBO.UrgentActionRequired, DbType.Boolean)
            parameterList.Add(urgentActionRequiredParam)

            Dim NotesParam As ParameterBO = New ParameterBO("notes", objPropertyAsbestosBO.Notes, DbType.String)
            parameterList.Add(NotesParam)

            Dim PropertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyAsbestosBO.PropertyId, DbType.String)
            parameterList.Add(PropertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPropertyAsbestosBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)
            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.saveAsbestosInformation)
        End Sub
#End Region

#Region "Amend Asbestos Information"
        Sub amendAsbestos(ByVal objPropertyAsbestosBO As PropertyAsbestosBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim asbestosLevelId As ParameterBO = New ParameterBO("asbestosLevelId", objPropertyAsbestosBO.AsbestosLevelId, DbType.Int32)
            parameterList.Add(asbestosLevelId)

            Dim asbestosElementId As ParameterBO = New ParameterBO("asbestosElementId", objPropertyAsbestosBO.AsbestosElementId, DbType.Int32)
            parameterList.Add(asbestosElementId)

            Dim RiskIdParam As ParameterBO = New ParameterBO("riskId", objPropertyAsbestosBO.RiskId, DbType.String)
            parameterList.Add(RiskIdParam)

            Dim AddedDateParam As ParameterBO = New ParameterBO("addedDate", objPropertyAsbestosBO.AddedDate, DbType.Date)
            parameterList.Add(AddedDateParam)

            Dim RemovedParam As ParameterBO = New ParameterBO("removedDate", objPropertyAsbestosBO.RemovedDate, DbType.String)
            parameterList.Add(RemovedParam)

            Dim riskLevelParam As ParameterBO = New ParameterBO("riskLevel", objPropertyAsbestosBO.RiskLevel, DbType.Int32)
            parameterList.Add(riskLevelParam)

            Dim urgentActionRequiredParam As ParameterBO = New ParameterBO("UrgentActionRequired", objPropertyAsbestosBO.UrgentActionRequired, DbType.Boolean)
            parameterList.Add(urgentActionRequiredParam)

            Dim NotesParam As ParameterBO = New ParameterBO("notes", objPropertyAsbestosBO.Notes, DbType.String)
            parameterList.Add(NotesParam)

            Dim PropasbLevelId As ParameterBO = New ParameterBO("propasbLevelId", objPropertyAsbestosBO.PropasbLevelID, DbType.String)
            parameterList.Add(PropasbLevelId)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPropertyAsbestosBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)
            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.amendAsbestosInformation)
        End Sub
#End Region

#Region "Save Refurbishment Information"
        Public Function saveRefurbishment(ByVal objRefurbishmentBO As RefurbishmentBO) As String
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim RefurbishmentDateParam As ParameterBO = New ParameterBO("REFURBISHMENT_DATE", objRefurbishmentBO.RefurbishmentDate, DbType.Date)
            parameterList.Add(RefurbishmentDateParam)

            Dim NotesParam As ParameterBO = New ParameterBO("NOTES", objRefurbishmentBO.Notes, DbType.String)
            parameterList.Add(NotesParam)

            Dim PropertyIdParam As ParameterBO = New ParameterBO("PROPERYID", objRefurbishmentBO.PropertyId, DbType.String)
            parameterList.Add(PropertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("USER_ID", objRefurbishmentBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)

            Dim errorMessgaeParam As ParameterBO = New ParameterBO("ErrorMessage", 0, DbType.String)
            outparameterList.Add(errorMessgaeParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.saveRefurbishmentInformaiton)
            Return outparameterList.Item(0).Value.ToString()
        End Function
#End Region

#Region "Get Details for email"

        ''' <summary>
        ''' This function is to get details for email, these details include contractor name, email and other details.
        ''' Contact details of customer/tenant.
        ''' Risk/Vulnerability details of the tenant
        ''' Property Address
        ''' </summary>
        ''' <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        ''' <param name="propertyId">Assign to Contractor Bo containing values used to get details</param>
        ''' <remarks></remarks>
        Sub GetEmployeeEmailForAsbestosEmailNotifications(ByRef propertyId As String, ByRef detailsForEmailDS As DataSet)
            Dim inParamList As New ParameterList

            Dim propertyIdParam As New ParameterBO("propertyId", propertyId, DbType.String)
            inParamList.Add(propertyIdParam)

            Dim datareader = MyBase.SelectRecord(inParamList, SpNameConstants.GetEmployeeEmailForAsbestosEmailNotifications)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.AsbestosDetailsDt)
        End Sub

#End Region
#End Region

#Region "Financial Tab"
#Region "Get Property Financial Tab Drop down Lists (Frequency,Funding Authority and Charge) Information"

        Public Sub getPropertyFinancialTabDropdownInformation(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(proerptyIdParam)
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getProperyFinancialInformation)

            Dim dtFundingAuthority As DataTable = New DataTable("FundingAuthority")
            resultDataSet.Tables.Add(dtFundingAuthority)
            Dim dtCharge As DataTable = New DataTable("Charge")
            resultDataSet.Tables.Add(dtCharge)
            Dim dtRentFinancial As DataTable = New DataTable("RentFinancial")
            resultDataSet.Tables.Add(dtRentFinancial)

            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFundingAuthority, dtCharge, dtRentFinancial)
        End Sub

#End Region

#Region "Get Property Rent Type And Financial Infomration Information "

        Public Sub getRentTypeAndFinancialInfomration(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(proerptyIdParam)
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getPropertyRentType)

            Dim dtRentType As DataTable = New DataTable("RentType")
            resultDataSet.Tables.Add(dtRentType)
            Dim dtRentFinancial As DataTable = New DataTable("RentFinancial")
            resultDataSet.Tables.Add(dtRentFinancial)

            Dim dtRentFrequency As DataTable = New DataTable("RentFrequency")
            resultDataSet.Tables.Add(dtRentFrequency)
            Dim dtRentAnnual As DataTable = New DataTable("RentAnnual")
            resultDataSet.Tables.Add(dtRentAnnual)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRentType, dtRentFinancial, dtRentFrequency, dtRentAnnual)
        End Sub

#End Region

#Region "Get Target Rent Detail "

        Public Sub getTargetRentDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim proerptyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(proerptyIdParam)
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getTargetRentDetail)

            Dim dtRentType As DataTable = New DataTable("RentType")
            resultDataSet.Tables.Add(dtRentType)
            Dim dtRentFinancial As DataTable = New DataTable("RentFinancial")
            resultDataSet.Tables.Add(dtRentFinancial)

            Dim dtRentFrequency As DataTable = New DataTable("RentFrequency")
            resultDataSet.Tables.Add(dtRentFrequency)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRentType, dtRentFinancial, dtRentFrequency)
        End Sub

#End Region
#Region "Save Property Financial Information"
        Sub savePropertyFinancialInformation(ByVal objPropertyFinancialBO As PropertyFinancialBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()



            Dim NominatingBodyParam As ParameterBO = New ParameterBO("nominatingBody", objPropertyFinancialBO.NominatingBody, DbType.String)
            parameterList.Add(NominatingBodyParam)

            Dim FundingAuthorityParam As ParameterBO = New ParameterBO("fundingAuthority", objPropertyFinancialBO.FundingAuthority, DbType.Int32)
            parameterList.Add(FundingAuthorityParam)

            Dim chargeParam As ParameterBO = New ParameterBO("charge", objPropertyFinancialBO.Charge, DbType.Int32)
            parameterList.Add(chargeParam)

            Dim chargeValueParam As ParameterBO = New ParameterBO("chargeValue", objPropertyFinancialBO.ChargeValue, DbType.Decimal)
            parameterList.Add(chargeValueParam)

            Dim OMVStParam As ParameterBO = New ParameterBO("OMVSt", objPropertyFinancialBO.OMVSt, DbType.Decimal)
            parameterList.Add(OMVStParam)

            Dim EUVParam As ParameterBO = New ParameterBO("EUV", objPropertyFinancialBO.EUV, DbType.Decimal)
            parameterList.Add(EUVParam)

            Dim InsuranceValueParam As ParameterBO = New ParameterBO("insuranceValue", objPropertyFinancialBO.InsuranceValue, DbType.Decimal)
            parameterList.Add(InsuranceValueParam)

            Dim OMVParam As ParameterBO = New ParameterBO("OMV", objPropertyFinancialBO.OMV, DbType.Decimal)
            parameterList.Add(OMVParam)

            Dim PropertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyFinancialBO.PropertyId, DbType.String)
            parameterList.Add(PropertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPropertyFinancialBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)

            Dim YieldParam As ParameterBO = New ParameterBO("Yield", objPropertyFinancialBO.Yield, DbType.Decimal)
            parameterList.Add(YieldParam)


            Dim ChargeValueDateParam As ParameterBO = New ParameterBO("ChargeValueDate", objPropertyFinancialBO.ChargeValueDate, DbType.DateTime)
            parameterList.Add(ChargeValueDateParam)

            Dim OMVSTDateParam As ParameterBO = New ParameterBO("OMVSTDate", objPropertyFinancialBO.OMVSTDate, DbType.DateTime)
            parameterList.Add(OMVSTDateParam)

            Dim EUVDateParam As ParameterBO = New ParameterBO("EUVDate", objPropertyFinancialBO.EUVDate, DbType.DateTime)
            parameterList.Add(EUVDateParam)

            Dim OMVDateParam As ParameterBO = New ParameterBO("OMVDate", objPropertyFinancialBO.OMVDate, DbType.DateTime)
            parameterList.Add(OMVDateParam)

            Dim InsuranceValuedateParam As ParameterBO = New ParameterBO("InsuranceValuedate", objPropertyFinancialBO.InsuranceValueDate, DbType.DateTime)
            parameterList.Add(InsuranceValuedateParam)

            Dim YieldDateParam As ParameterBO = New ParameterBO("YieldDate", objPropertyFinancialBO.YieldDate, DbType.DateTime)
            parameterList.Add(YieldDateParam)

            Dim MarketRentParam As ParameterBO = New ParameterBO("MarketRent", objPropertyFinancialBO.MarketRent, DbType.Decimal)
            parameterList.Add(MarketRentParam)

            Dim AffordableRentParam As ParameterBO = New ParameterBO("AffordableRent", objPropertyFinancialBO.AffordableRent, DbType.Decimal)
            parameterList.Add(AffordableRentParam)

            Dim SocialRentParam As ParameterBO = New ParameterBO("SocialRent", objPropertyFinancialBO.SocialRent, DbType.Decimal)
            parameterList.Add(SocialRentParam)

            Dim MarketRentDateParam As ParameterBO = New ParameterBO("MarketRentDate", objPropertyFinancialBO.MarketRentDate, DbType.DateTime)
            parameterList.Add(MarketRentDateParam)

            Dim AffordableRentDateParam As ParameterBO = New ParameterBO("AffordableRentDate", objPropertyFinancialBO.AffordableRentDate, DbType.DateTime)
            parameterList.Add(AffordableRentDateParam)

            Dim SocialRentDateParam As ParameterBO = New ParameterBO("SocialRentDate", objPropertyFinancialBO.SocialRentDate, DbType.DateTime)
            parameterList.Add(SocialRentDateParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.savePropertyFinancialInformation)
        End Sub
#End Region

#Region "Save Property Current Rent Information"
        Sub savePropertyCurrentRentInformation(ByVal objPropertyCurrentRentBO As PropertyRentBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim RentDateSetParam As ParameterBO = New ParameterBO("rentDateSet", objPropertyCurrentRentBO.RentDateSet, DbType.Date)
            parameterList.Add(RentDateSetParam)

            Dim RentEffectiveSetParam As ParameterBO = New ParameterBO("rentEffectiveSet", objPropertyCurrentRentBO.RentEffectiveSet, DbType.Date)
            parameterList.Add(RentEffectiveSetParam)

            Dim RentTypeParam As ParameterBO = New ParameterBO("rentType", objPropertyCurrentRentBO.RentType, DbType.Int32)
            parameterList.Add(RentTypeParam)

            Dim RentFequencyParam As ParameterBO = New ParameterBO("rentFrequency", objPropertyCurrentRentBO.RentFrequency, DbType.Int32)
            parameterList.Add(RentFequencyParam)

            Dim RentParam As ParameterBO = New ParameterBO("rent", objPropertyCurrentRentBO.Rent, DbType.Decimal)
            parameterList.Add(RentParam)

            Dim ServicesParam As ParameterBO = New ParameterBO("services", objPropertyCurrentRentBO.Services, DbType.Decimal)
            parameterList.Add(ServicesParam)

            Dim CouncilTaxParam As ParameterBO = New ParameterBO("councilTax", objPropertyCurrentRentBO.CouncilTax, DbType.Decimal)
            parameterList.Add(CouncilTaxParam)

            Dim WaterParam As ParameterBO = New ParameterBO("water", objPropertyCurrentRentBO.Water, DbType.Decimal)
            parameterList.Add(WaterParam)

            Dim InetligParam As ParameterBO = New ParameterBO("inetlig", objPropertyCurrentRentBO.Inelig, DbType.Decimal)
            parameterList.Add(InetligParam)

            Dim SuppParam As ParameterBO = New ParameterBO("supp", objPropertyCurrentRentBO.Supp, DbType.Decimal)
            parameterList.Add(SuppParam)

            Dim GarageParam As ParameterBO = New ParameterBO("garage", objPropertyCurrentRentBO.Garage, DbType.Decimal)
            parameterList.Add(GarageParam)

            Dim TotalRentParam As ParameterBO = New ParameterBO("totalRent", objPropertyCurrentRentBO.TotalRent, DbType.Decimal)
            parameterList.Add(TotalRentParam)

            Dim PropertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyCurrentRentBO.PropertyId, DbType.String)
            parameterList.Add(PropertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPropertyCurrentRentBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)
            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.savePropertyCurrentRentInformation)
        End Sub
#End Region

#Region "Save Property Target Rent Information"
        Sub savePropertyTargetRentInformation(ByVal objPropertyCurrentRentBO As PropertyRentBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim RentTypeParam As ParameterBO = New ParameterBO("rentType", objPropertyCurrentRentBO.RentType, DbType.Int32)
            parameterList.Add(RentTypeParam)

            Dim RentParam As ParameterBO = New ParameterBO("rent", objPropertyCurrentRentBO.Rent, DbType.Decimal)
            parameterList.Add(RentParam)

            Dim ServicesParam As ParameterBO = New ParameterBO("services", objPropertyCurrentRentBO.Services, DbType.Decimal)
            parameterList.Add(ServicesParam)

            Dim CouncilTaxParam As ParameterBO = New ParameterBO("councilTax", objPropertyCurrentRentBO.CouncilTax, DbType.Decimal)
            parameterList.Add(CouncilTaxParam)

            Dim WaterParam As ParameterBO = New ParameterBO("water", objPropertyCurrentRentBO.Water, DbType.Decimal)
            parameterList.Add(WaterParam)

            Dim InetligParam As ParameterBO = New ParameterBO("inetlig", objPropertyCurrentRentBO.Inelig, DbType.Decimal)
            parameterList.Add(InetligParam)

            Dim SuppParam As ParameterBO = New ParameterBO("supp", objPropertyCurrentRentBO.Supp, DbType.Decimal)
            parameterList.Add(SuppParam)

            Dim GarageParam As ParameterBO = New ParameterBO("garage", objPropertyCurrentRentBO.Garage, DbType.Decimal)
            parameterList.Add(GarageParam)

            Dim TotalRentParam As ParameterBO = New ParameterBO("totalRent", objPropertyCurrentRentBO.TotalRent, DbType.Decimal)
            parameterList.Add(TotalRentParam)

            Dim PropertyIdParam As ParameterBO = New ParameterBO("propertyId", objPropertyCurrentRentBO.PropertyId, DbType.String)
            parameterList.Add(PropertyIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objPropertyCurrentRentBO.UserId, DbType.Int32)
            parameterList.Add(userIdParam)
            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.savePropertyTargetRentInformation)
        End Sub
#End Region

#Region "get Rent History"
        ''' <summary>
        ''' get Rent History
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getRentHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.getRentHistory)
            Return outParametersList.Item(0).Value

        End Function
#End Region

#Region "get Valuation History"
        ''' <summary>
        ''' get Valuation History
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="propertyId"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getValuationHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef objPageSortBo As PageSortBO)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.getValuationHistory)
            Return outParametersList.Item(0).Value

        End Function
#End Region
#End Region

#Region "Get Attribute Summary"
        ''' <summary>
        ''' Get Attribute Summary
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAttributeSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyAttributes As DataTable = New DataTable()
            dtPropertyAttributes.TableName = ApplicationConstants.propertyAttributes
            resultDataSet.Tables.Add(dtPropertyAttributes)

            Dim dtPropertyAttributeValues As DataTable = New DataTable()
            dtPropertyAttributeValues.TableName = ApplicationConstants.propertyAttributeValues
            resultDataSet.Tables.Add(dtPropertyAttributeValues)

            Dim dtPropertyImage As DataTable = New DataTable()
            dtPropertyImage.TableName = ApplicationConstants.propertyImage
            resultDataSet.Tables.Add(dtPropertyImage)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getAttributeSummaryDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyAttributes, dtPropertyAttributeValues, dtPropertyImage)

        End Sub

#End Region

#Region "Get Property Rooms Dimensions by PropertyId"
        Function getRoomsDimensions(ByVal propertyId As String, Optional ByVal dataset As DataSet = Nothing) As DataSet
            If dataset Is Nothing Then
                dataset = New DataSet()
            End If

            Dim parametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            MyBase.LoadDataSet(dataset, parametersList, SpNameConstants.getRoomsDimensionsByPropertyId)
            Return dataset
        End Function
#End Region

#Region "Property Data Restructure"

#Region "Get Properties List"
        ''' <summary>
        ''' Get Properties List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getPropertiesList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("searchText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPropertyList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get getProperty Template "
        Public Sub getPropertyTemplateForAllDropDown(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyTemplate As DataTable = New DataTable()
            dtPropertyTemplate.TableName = ApplicationConstants.propertyTemplate
            resultDataSet.Tables.Add(dtPropertyTemplate)

            Dim dtDevelopment As DataTable = New DataTable()
            dtDevelopment.TableName = ApplicationConstants.development
            resultDataSet.Tables.Add(dtDevelopment)

            Dim dtStockType As DataTable = New DataTable()
            dtStockType.TableName = ApplicationConstants.stocktype
            resultDataSet.Tables.Add(dtStockType)

            Dim dtOwnership As DataTable = New DataTable()
            dtOwnership.TableName = ApplicationConstants.ownership
            resultDataSet.Tables.Add(dtOwnership)

            Dim dtStatus As DataTable = New DataTable()
            dtStatus.TableName = ApplicationConstants.status
            resultDataSet.Tables.Add(dtStatus)

            Dim dtpropertyType As DataTable = New DataTable()
            dtpropertyType.TableName = ApplicationConstants.propertyType
            resultDataSet.Tables.Add(dtpropertyType)

            Dim dtDwellingType As DataTable = New DataTable()
            dtDwellingType.TableName = ApplicationConstants.dwellingType
            resultDataSet.Tables.Add(dtDwellingType)

            Dim dtLevel As DataTable = New DataTable()
            dtLevel.TableName = ApplicationConstants.level
            resultDataSet.Tables.Add(dtLevel)


            Dim dtAssetType As DataTable = New DataTable()
            dtAssetType.TableName = ApplicationConstants.assetType
            resultDataSet.Tables.Add(dtAssetType)

            Dim dtTenureType As DataTable = New DataTable()
            dtTenureType.TableName = ApplicationConstants.TenureSelection
            resultDataSet.Tables.Add(dtTenureType)

            Dim dtAssetTypeMain As DataTable = New DataTable()
            dtAssetTypeMain.TableName = ApplicationConstants.assetTypeMain
            resultDataSet.Tables.Add(dtAssetTypeMain)
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.PopulateAllDropDownList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyTemplate, dtDevelopment, dtStockType, dtOwnership, dtStatus, dtpropertyType, dtDwellingType, dtLevel, dtAssetType, dtAssetTypeMain, dtTenureType)


        End Sub
#End Region
#Region "get getProperty Template by propertyID "
        Public Sub getPropertyDetailByPropertyId(ByRef resultDataSet As DataSet, ByVal propertyId As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPropertyDetailByPropertyId)
        End Sub
#End Region
#Region "get Blocks By DevelopmentID "
        Public Sub getBlocksByDevelopmentId(ByRef resultDataSet As DataSet, ByVal devId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", devId, DbType.Int32)
            parametersList.Add(developmentIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetBlocksByDevelopmentId)
        End Sub
#End Region
#Region "get Blocks By blockId "
        Public Sub getBlocksByBlockId(ByRef resultDataSet As DataSet, ByVal blockId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim developmentIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(developmentIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetBlocksByBlockId)
        End Sub
#End Region

#Region "get Phase List"
        Public Sub getPhaseList(ByRef resultDataSet As DataSet, ByVal devId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim developmentIdParam As ParameterBO = New ParameterBO("developmentId", devId, DbType.Int32)
            parametersList.Add(developmentIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPhaseList)
        End Sub
#End Region

#Region "get Phase List"
        Public Sub getPhaseDetail(ByRef resultDataSet As DataSet, ByVal devId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim developmentIdParam As ParameterBO = New ParameterBO("PhaseId", devId, DbType.Int32)
            parametersList.Add(developmentIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPhase)
        End Sub
#End Region
#Region "get Property Sub Status"
        Public Sub getPropertySubStatus(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parametersList.Add(statusIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPropertySubStatus)
        End Sub
#End Region

#Region "get Property NROSH Asset Type Sub"
        Public Sub getPropertyNROSHAssetTypeSub(ByRef resultDataSet As DataSet, ByVal sId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim statusIdParam As ParameterBO = New ParameterBO("mainCategoryId", sId, DbType.Int32)
            parametersList.Add(statusIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPropertyNROSHAssetTypeSub)
        End Sub
#End Region

#Region "Save Property Detail"

        Public Function SavePropertyDetail(ByRef objPropertyDetailsBO As PropertiesDetailsBO, ByVal complDate As Date, ByVal handDate As Date) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", objPropertyDetailsBO.DevelopmentId, DbType.Int32)
            parametersList.Add(developmentIdParam)
            Dim blockIdParam As ParameterBO = New ParameterBO("BlockId", objPropertyDetailsBO.BlockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim primaryHeatingFuelIdParam As ParameterBO = New ParameterBO("heatingFuelValueId", objPropertyDetailsBO.ValueId, DbType.Int32)
            parametersList.Add(primaryHeatingFuelIdParam)

            Dim updatedByParam As ParameterBO = New ParameterBO("updatedBy", objPropertyDetailsBO.UpdatedBy, DbType.Int32)
            parametersList.Add(updatedByParam)

            Dim phaseIdParam As ParameterBO = New ParameterBO("PhaseId", objPropertyDetailsBO.PhaseId, DbType.Int32)
            parametersList.Add(phaseIdParam)
            Dim houseNumberParam As ParameterBO = New ParameterBO("HouseNumber", objPropertyDetailsBO.House, DbType.String)
            parametersList.Add(houseNumberParam)
            Dim address1Param As ParameterBO = New ParameterBO("Address1", objPropertyDetailsBO.Address1, DbType.String)
            parametersList.Add(address1Param)
            Dim address2Param As ParameterBO = New ParameterBO("Address2", objPropertyDetailsBO.Address2, DbType.String)
            parametersList.Add(address2Param)
            Dim address3Param As ParameterBO = New ParameterBO("Address3", objPropertyDetailsBO.Address3, DbType.String)
            parametersList.Add(address3Param)
            Dim townCityParam As ParameterBO = New ParameterBO("TownCity", objPropertyDetailsBO.Town, DbType.String)
            parametersList.Add(townCityParam)
            Dim countyParam As ParameterBO = New ParameterBO("County", objPropertyDetailsBO.County, DbType.String)
            parametersList.Add(countyParam)
            Dim postCodeParam As ParameterBO = New ParameterBO("PostCode", objPropertyDetailsBO.PostCode, DbType.String)
            parametersList.Add(postCodeParam)
            Dim statusParam As ParameterBO = New ParameterBO("Status", objPropertyDetailsBO.Status, DbType.Int32)
            parametersList.Add(statusParam)
            Dim subStatusParam As ParameterBO = New ParameterBO("SubStatus", objPropertyDetailsBO.SubStatus, DbType.Int32)
            parametersList.Add(subStatusParam)
            Dim propertyTypeParam As ParameterBO = New ParameterBO("PropertyType", objPropertyDetailsBO.PropertyType, DbType.Int32)
            parametersList.Add(propertyTypeParam)
            Dim assetTypeParam As ParameterBO = New ParameterBO("AssetType", objPropertyDetailsBO.AssetType, DbType.Int32)
            parametersList.Add(assetTypeParam)
            Dim StockTypeParam As ParameterBO = New ParameterBO("StockType", objPropertyDetailsBO.StockType, DbType.Int32)
            parametersList.Add(StockTypeParam)
            Dim PropertyLevelParam As ParameterBO = New ParameterBO("PropertyLevel", objPropertyDetailsBO.Level, DbType.Int32)
            parametersList.Add(PropertyLevelParam)
            Dim DateBuiltParam As ParameterBO = New ParameterBO("DateBuilt", objPropertyDetailsBO.DateBuild, DbType.DateTime)
            parametersList.Add(DateBuiltParam)

            Dim RightToBuyParam As ParameterBO = New ParameterBO("RightToBuy", objPropertyDetailsBO.RightToAcquire, DbType.Int32)
            parametersList.Add(RightToBuyParam)
            Dim DefectsPeriodEndParam As ParameterBO = New ParameterBO("DefectsPeriodEnd", objPropertyDetailsBO.DefectPeriod, DbType.Date)
            parametersList.Add(DefectsPeriodEndParam)
            Dim OwnerShipParam As ParameterBO = New ParameterBO("OwnerShip", objPropertyDetailsBO.Ownership, DbType.Int32)
            parametersList.Add(OwnerShipParam)
            Dim MinPurchaseParam As ParameterBO = New ParameterBO("MinPurchase", objPropertyDetailsBO.MinPurchase, DbType.Decimal)
            parametersList.Add(MinPurchaseParam)
            Dim PurchaseLevelParam As ParameterBO = New ParameterBO("PurchaseLevel", objPropertyDetailsBO.PurchaseLevel, DbType.Decimal)
            parametersList.Add(PurchaseLevelParam)
            Dim DwellingTypeParam As ParameterBO = New ParameterBO("DwellingType", objPropertyDetailsBO.DwellingType, DbType.Int32)
            parametersList.Add(DwellingTypeParam)
            Dim NROSHAssetTypeMainParam As ParameterBO = New ParameterBO("NROSHAssetTypeMain", objPropertyDetailsBO.AssetTypeMain, DbType.Int32)
            parametersList.Add(NROSHAssetTypeMainParam)
            Dim NROSHAssetTypeSubParam As ParameterBO = New ParameterBO("NROSHAssetTypeSub", objPropertyDetailsBO.AssetTypeSub, DbType.Int32)
            parametersList.Add(NROSHAssetTypeSubParam)

            Dim IsTemplateParam As ParameterBO = New ParameterBO("IsTemplate", objPropertyDetailsBO.IsTemplate, DbType.Boolean)
            parametersList.Add(IsTemplateParam)
            Dim TemplateNameParam As ParameterBO = New ParameterBO("TemplateName", objPropertyDetailsBO.TemplateName, DbType.String)
            parametersList.Add(TemplateNameParam)
            Dim LeaseStartParam As ParameterBO = New ParameterBO("LeaseStart", objPropertyDetailsBO.LeaseStart, DbType.Date)
            parametersList.Add(LeaseStartParam)
            Dim LeaseEndParam As ParameterBO = New ParameterBO("LeaseEnd", objPropertyDetailsBO.LeaseEnd, DbType.Date)
            parametersList.Add(LeaseEndParam)
            Dim FloodingRiskParam As ParameterBO = New ParameterBO("FloodingRisk", objPropertyDetailsBO.FloodingRisk, DbType.Boolean)
            parametersList.Add(FloodingRiskParam)
            Dim DateAcquiredParam As ParameterBO = New ParameterBO("DateAcquired", objPropertyDetailsBO.DateAcquired, DbType.Date)
            parametersList.Add(DateAcquiredParam)

            Dim ViewingDateParam As ParameterBO = New ParameterBO("viewingcommencement", objPropertyDetailsBO.ViewingCommencement, DbType.Date)
            parametersList.Add(ViewingDateParam)


            Dim GroundRentParam As ParameterBO = New ParameterBO("GroundRent ", objPropertyDetailsBO.GroundRent, DbType.String)
            parametersList.Add(GroundRentParam)
            Dim LandLordParam As ParameterBO = New ParameterBO("LandLord ", objPropertyDetailsBO.LandLord, DbType.String)
            parametersList.Add(LandLordParam)

            Dim RentReviewParam As ParameterBO = New ParameterBO("RentReview ", objPropertyDetailsBO.RentReview, DbType.Date)
            parametersList.Add(RentReviewParam)

            Dim ExistingPropertyIDParam As ParameterBO = New ParameterBO("ExistingPropertyID ", objPropertyDetailsBO.PropertyId, DbType.String)
            parametersList.Add(ExistingPropertyIDParam)

            Dim TitleNumberParam As ParameterBO = New ParameterBO("TitleNumber", objPropertyDetailsBO.TitleNumber, DbType.String)
            parametersList.Add(TitleNumberParam)

            Dim DeedLocationParam As ParameterBO = New ParameterBO("DeedLocation", objPropertyDetailsBO.DeedLocation, DbType.String)
            parametersList.Add(DeedLocationParam)

            Dim TenureTypeParam As ParameterBO = New ParameterBO("TenureType", objPropertyDetailsBO.TenureType, DbType.Int32)
            parametersList.Add(TenureTypeParam)

            If complDate = Date.MinValue Then
                Dim ComplDateParam As ParameterBO = New ParameterBO("actualCompletion", DBNull.Value, DbType.Date)
                parametersList.Add(ComplDateParam)
            Else
                Dim ComplDateParam As ParameterBO = New ParameterBO("actualCompletion", complDate, DbType.Date)
                parametersList.Add(ComplDateParam)
            End If
            If complDate = Date.MinValue Then
                Dim HandDateParam As ParameterBO = New ParameterBO("handoverintoManagement", DBNull.Value, DbType.Date)
                parametersList.Add(HandDateParam)
            Else
                Dim HandDateParam As ParameterBO = New ParameterBO("handoverintoManagement", handDate, DbType.Date)
                parametersList.Add(HandDateParam)
            End If
            Dim propertyAddressParam As ParameterBO = New ParameterBO("PropertyId", String.Empty, DbType.String)
            outParametersList.Add(propertyAddressParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SavePropertyDetail)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region


#Region " load Cycle DDL"
        Sub loadCycleDdl(ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetCycleType)
        End Sub
#End Region
#End Region

#Region "Get Warranties"
        Sub getWarrantiesList(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal objPageSortBO As PageSortBO)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramBlockId As ParameterBO = New ParameterBO("Id", propertyId, DbType.String)
            paramList.Add(paramBlockId)
            Dim pageNumberParam As ParameterBO = New ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32)
            paramList.Add(pageNumberParam)
            Dim pageSizeParam As ParameterBO = New ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32)
            paramList.Add(pageSizeParam)
            Dim SortDirectionParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String)
            paramList.Add(SortDirectionParam)
            Dim SortExpressionParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String)
            paramList.Add(SortExpressionParam)
            Dim requestTypeParam As ParameterBO = New ParameterBO("requestType", "Property", DbType.String)
            paramList.Add(requestTypeParam)
            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)
            LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetWarrantyList)
        End Sub
#End Region

#Region "Load Add Warranties Dropdowns"


        Sub getWarrantyTypes(ByRef warrantyTypeDataSet As DataSet)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            LoadDataSet(warrantyTypeDataSet, paramList, outParametersList, SpNameConstants.GetWarrantyTypes)
        End Sub

        Sub getcategoryDropdownValue(ByRef forDataSet As DataSet)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            LoadDataSet(forDataSet, paramList, outParametersList, SpNameConstants.GetCategoryDropDownValues)
        End Sub

        Sub getAreaDropdownValueByCategoryId(ByVal categoryId As Integer, ByRef forDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim categoryParam As ParameterBO = New ParameterBO("locationId", categoryId, DbType.String)
            parametersList.Add(categoryParam)
            MyBase.LoadDataSet(forDataSet, parametersList, SpNameConstants.GetAreaDropDownValuesByCategory)
        End Sub

        Sub getItemDropdownValueByAreaId(ByVal areaId As Integer, ByRef forDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim areaParam As ParameterBO = New ParameterBO("areaId", areaId, DbType.String)
            parametersList.Add(areaParam)
            MyBase.LoadDataSet(forDataSet, parametersList, SpNameConstants.GetItemDropDownValuesByArea)
        End Sub

        Sub getContractors(ByRef contractorDataSet As DataSet)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            LoadDataSet(contractorDataSet, paramList, outParametersList, SpNameConstants.GetContractors)
        End Sub
#End Region

#Region " Save Warranty"


        Sub saveWarranty(ByVal objWarrantyBO As WarrantyBO)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramWarrantyType As ParameterBO = New ParameterBO("warrantyType", objWarrantyBO.WarrantyType, DbType.Int32)
            paramList.Add(paramWarrantyType)
            Dim paramCategory As ParameterBO = New ParameterBO("Category", objWarrantyBO.Category, DbType.Int32)
            paramList.Add(paramCategory)
            Dim paramArea As ParameterBO = New ParameterBO("Area", objWarrantyBO.Area, DbType.Int32)
            paramList.Add(paramArea)
            Dim paramItem As ParameterBO = New ParameterBO("Item", objWarrantyBO.Item, DbType.Int32)
            paramList.Add(paramItem)
            Dim paramContractor As ParameterBO = New ParameterBO("Contractor", objWarrantyBO.Contractor, DbType.Int32)
            paramList.Add(paramContractor)
            Dim paramExpiry As ParameterBO = New ParameterBO("EXPIRYDATE", objWarrantyBO.Expiry, DbType.DateTime)
            paramList.Add(paramExpiry)
            Dim paramNotes As ParameterBO = New ParameterBO("Notes", objWarrantyBO.Notes, DbType.String)
            paramList.Add(paramNotes)
            Dim paramId As ParameterBO = New ParameterBO("Id", objWarrantyBO.Id, DbType.String)
            paramList.Add(paramId)
            Dim paramWarrantyId As ParameterBO = New ParameterBO("WarrantyId", objWarrantyBO.WarrantyId, DbType.String)
            paramList.Add(paramWarrantyId)
            Dim paramRequestType As ParameterBO = New ParameterBO("RequestType", objWarrantyBO.RequestType, DbType.String)
            paramList.Add(paramRequestType)
            SaveRecord(paramList, outParametersList, SpNameConstants.SaveWarranty)
        End Sub
#End Region

        Sub getWarrantyData(ByRef warrantyDataSet As DataSet, ByVal warrantyId As Integer)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramWarrantyID As ParameterBO = New ParameterBO("warrantyId", warrantyId, DbType.Int32)
            paramList.Add(paramWarrantyID)
            LoadDataSet(warrantyDataSet, paramList, outParametersList, SpNameConstants.GetWarrantyData)
        End Sub

        Sub deleteWarranty(ByVal warrantyId As Integer)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramWarrantyID As ParameterBO = New ParameterBO("warrantyId", warrantyId, DbType.Int32)
            paramList.Add(paramWarrantyID)
            SaveRecord(paramList, outParametersList, SpNameConstants.DeleteWarranty)
        End Sub


#Region "Get contractor jobsheet detail"

        Public Sub getContractorJobsheetDetail(ByVal faultLogHistoryId As Int32, ByRef resultDataSet As DataSet)

            Dim spGetContractorJobSheetSummary As String = SpNameConstants.GetContractorJobSheetSummary
            Dim parametersList As ParameterList = New ParameterList()

            Dim jobSheetSummaryDetailDt As DataTable = New DataTable()
            Dim jobSheetSummaryAsbestosDt As DataTable = New DataTable()
            jobSheetSummaryDetailDt.TableName = ApplicationConstants.jobSheetSummaryDetailTable
            jobSheetSummaryAsbestosDt.TableName = ApplicationConstants.jobSheetSummaryAsbestosTable
            resultDataSet.Tables.Add(jobSheetSummaryDetailDt)
            resultDataSet.Tables.Add(jobSheetSummaryAsbestosDt)

            Dim faultLogHistoryIdParam As ParameterBO = New ParameterBO("FaultLogHistoryId", faultLogHistoryId, DbType.Int32)
            parametersList.Add(faultLogHistoryIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, spGetContractorJobSheetSummary)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, jobSheetSummaryDetailDt, jobSheetSummaryAsbestosDt)

        End Sub

#End Region

#Region "Get Appliance Cancel Notes"

        Public Function getApplianceCancelNotes(ByVal journalId As Int32)

            Dim spGetApplianceCancelNotes As String = SpNameConstants.GetApplianceCancelledNotes

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim Journal As ParameterBO = New ParameterBO("JournalId", journalId, DbType.Int32)
            parametersList.Add(Journal)

            Dim notes As ParameterBO = New ParameterBO("notes", "", DbType.String)
            outParametersList.Add(notes)

            MyBase.SelectRecord(parametersList, outParametersList, spGetApplianceCancelNotes)
            Return outParametersList.Item(0).Value
        End Function

#End Region

#Region "Get Property Status History"

        Function getStatusHistory(ByVal propertyId As String) As DataSet
            Dim resultDataSet As DataSet = New DataSet()
            Dim parametersList As ParameterList = New ParameterList()
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.getStatusHistory)
            Return resultDataSet
        End Function
#End Region

        Sub getPropertyCustomerEmail(ByVal resultDataSet As DataSet, ByVal letterHistoryId As String)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramletterHistoryID As ParameterBO = New ParameterBO("letterHistoryId", letterHistoryId, DbType.Int32)
            paramList.Add(paramletterHistoryID)
            LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetPropertyCustomerEmail)
        End Sub

        Sub UpdatePropertyCustomerEmail(ByVal customerEmail As String, ByVal appointmentId As String, ByVal emailStatus As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim paramCustomerEmail As ParameterBO = New ParameterBO("customerEmail", customerEmail, DbType.String)
            parameterList.Add(paramCustomerEmail)

            Dim paramAppointmentId As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.String)
            parameterList.Add(paramAppointmentId)

            Dim paramEmailStatus As ParameterBO = New ParameterBO("emailStatus", emailStatus, DbType.String)
            parameterList.Add(paramEmailStatus)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.UpdatePropertyCustomerEmail)
        End Sub

#Region "Save Restriction"

        Public Function SaveRestriction(ByRef objRestrictionBO As RestrictionBO) As Boolean
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            Dim paramRestrictionId As ParameterBO = New ParameterBO("RestrictionId", objRestrictionBO.RestrictionId, DbType.Int32)
            inParamList.Add(paramRestrictionId)
            Dim paramPermittedPlanning As ParameterBO = New ParameterBO("PermittedPlanning", objRestrictionBO.PermittedPlanning, DbType.String)
            inParamList.Add(paramPermittedPlanning)
            Dim paramRelevantPlanning As ParameterBO = New ParameterBO("RelevantPlanning", objRestrictionBO.RelevantPlanning, DbType.String)
            inParamList.Add(paramRelevantPlanning)
            Dim paramRelevantTitle As ParameterBO = New ParameterBO("RelevantTitle", objRestrictionBO.RelevantTitle, DbType.String)
            inParamList.Add(paramRelevantTitle)
            Dim paramRestrictionComments As ParameterBO = New ParameterBO("RestrictionComments", objRestrictionBO.RestrictionComments, DbType.String)
            inParamList.Add(paramRestrictionComments)
            Dim paramAccessIssues As ParameterBO = New ParameterBO("AccessIssues", objRestrictionBO.AccessIssues, DbType.String)
            inParamList.Add(paramAccessIssues)
            Dim paramMediaIssues As ParameterBO = New ParameterBO("MediaIssues", objRestrictionBO.MediaIssues, DbType.String)
            inParamList.Add(paramMediaIssues)
            Dim paramThirdPartyAgreement As ParameterBO = New ParameterBO("ThirdPartyAgreement", objRestrictionBO.ThirdPartyAgreement, DbType.String)
            inParamList.Add(paramThirdPartyAgreement)
            Dim paramSpFundingArrangements As ParameterBO = New ParameterBO("SpFundingArrangements", objRestrictionBO.SpFundingArrangements, DbType.String)
            inParamList.Add(paramSpFundingArrangements)
            Dim paramIsRegistered As ParameterBO = New ParameterBO("IsRegistered", objRestrictionBO.IsRegistered, DbType.Int32)
            inParamList.Add(paramIsRegistered)
            Dim paramManagementDetail As ParameterBO = New ParameterBO("ManagementDetail", objRestrictionBO.ManagementDetail, DbType.String)
            inParamList.Add(paramManagementDetail)
            Dim paramNonBhaInsuranceDetail As ParameterBO = New ParameterBO("NonBhaInsuranceDetail", objRestrictionBO.NonBhaInsuranceDetail, DbType.String)
            inParamList.Add(paramNonBhaInsuranceDetail)
            Dim paramSchemeId As ParameterBO = New ParameterBO("PropertyId", objRestrictionBO.PropertyId, DbType.String)
            inParamList.Add(paramSchemeId)
            Dim paramUpdatedBy As ParameterBO = New ParameterBO("UpdatedBy", objRestrictionBO.UpdatedBy, DbType.Int32)
            inParamList.Add(paramUpdatedBy)
            Dim isSavedParam As ParameterBO = New ParameterBO("IsSaved", 0, DbType.Boolean)
            outParamList.Add(isSavedParam)
            Dim restrictionIdOutParam As ParameterBO = New ParameterBO("RestrictionIdOut", -1, DbType.Int32)
            outParamList.Add(restrictionIdOutParam)
            outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.SaveRestrictions)
            objRestrictionBO.RestrictionId = Convert.ToInt32(outParamList(1).Value)
            Dim isSaved As Boolean = False
            isSaved = Convert.ToBoolean(outParamList(0).Value)
            Return isSaved
        End Function

#End Region

#Region "Get Restrictions"
        Public Sub getRestrictions(ByRef dataSet As DataSet, ByVal propertyId As String)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            paramList.Add(propertyIdParam)
            LoadDataSet(dataSet, paramList, outParametersList, SpNameConstants.GetRestrictions)
        End Sub
#End Region

#Region "Get NHBC Warranties"
        Public Sub getNHBCWarranties(ByRef warrantyDataSet As DataSet, ByVal Id As String)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramBlockId As ParameterBO = New ParameterBO("Id", Id, DbType.String)
            paramList.Add(paramBlockId)
            LoadDataSet(warrantyDataSet, paramList, outParametersList, SpNameConstants.GetNHBCWarrantyList)
        End Sub
#End Region

#Region "Get Land Registration Options"
        Public Sub getLandRegistrationOptions(ByRef regDataSet As DataSet)
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            LoadDataSet(regDataSet, paramList, outParametersList, SpNameConstants.GetLandRegistrationOptions)
        End Sub
#End Region

    End Class
End Namespace





