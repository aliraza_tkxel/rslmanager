﻿Imports AS_BusinessObject
Imports AS_Utilities

Namespace AS_DataAccess

    Public Class DefectsDAL : Inherits BaseDAL

#Region "Get Defects Appointment To Be Arranged List"

        Public Function getAppointmentToBeArrangedList(ByRef resultDataSet As DataSet, ByVal defectCategoryId As Integer, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            ' Report Filter Parameter(s)
            parametersList.Add(New ParameterBO("searchText", searchText, DbType.String))
            parametersList.Add(New ParameterBO("defectCategoryId", defectCategoryId, DbType.Int32))

            ' Report Paging and Sorting Paramaeters
            parametersList.Add(New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32))
            parametersList.Add(New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32))
            parametersList.Add(New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String))
            parametersList.Add(New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String))

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectsAppointmentToBeArrangedList)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defects Appointment To Be Arranged List Count"

        Public Function getAppointmentToBeArrangedListCount(ByRef resultDataSet As DataSet, ByVal schemeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            parametersList.Add(New ParameterBO("schemeId", schemeId, DbType.Int32))
            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectsAppointmentToBeArrangedList)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defect Appointment Arranged List"

        Public Function getAppointmentArrangedList(ByRef resultDataSet As DataSet, ByVal searchText As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            ' Report Filter Parameter(s)
            parametersList.Add(New ParameterBO("searchText", searchText, DbType.String))

            ' Report Paging and Sorting Paramaeters
            parametersList.Add(New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32))
            parametersList.Add(New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32))
            parametersList.Add(New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String))
            parametersList.Add(New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String))

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectsArrangedList)

            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defect Appointment Arranged List Count"

        Public Function getAppointmentArrangedListCount(ByRef resultDataSet As DataSet, ByVal schemeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            parametersList.Add(New ParameterBO("schemeId", schemeId, DbType.Int32))
            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetDefectsArrangedList)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region

#Region "Get Defects By Journal Id"

        Public Sub GetDefectsByJournalId(ByRef resultDataset As DataSet, ByVal journalId As Integer, ByVal defectCategoryId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim searchedValueParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim check56DaysParam As ParameterBO = New ParameterBO("defectCategoryId", defectCategoryId, DbType.Int32)
            parametersList.Add(check56DaysParam)

            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.GetAppliancesDefectsByJournalId)

        End Sub

#End Region

#Region "Get Defect Management Look up values"

        Public Sub getDefectManagementLookupValues(ByRef dataset As DataSet, ByVal propertyId As String)
            Dim inParameters As New ParameterList

            inParameters.Add(New ParameterBO("PropertyId", propertyId, DbType.String))

            Dim lDataReader = MyBase.SelectRecord(inParameters, SpNameConstants.GetDefectManagementLoopkupsData)
            dataset.Load(lDataReader, LoadOption.OverwriteChanges, "DefectCategories", "PropertyAppliances", "EmployeesList", "TradesList", "PrioritiesList")
        End Sub

#End Region

#Region "get All Schemes"

        Public Sub getAllSchemes(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAllSchemes)
        End Sub

#End Region

#Region "get All Appliance Types"

        Public Function getAllApplianceTypes()
            Dim resultDataSet As DataSet = New DataSet()
            Dim inParameters As New ParameterList
            MyBase.LoadDataSet(resultDataSet, inParameters, SpNameConstants.GetAllApplainceTypes)
            Return resultDataSet.Tables(0)
        End Function

#End Region

#Region "Get Job Sheet Details by defectId"

        Public Sub getJobSheetDetails(jobSheetDataSet As DataSet, defectId As Integer)

            Dim inParameters As New ParameterList
            inParameters.Add(New ParameterBO("defectId", defectId, DbType.Int32))

            Dim lDataReader = MyBase.SelectRecord(inParameters, SpNameConstants.GetDefectJobSheetByDefectId)
            jobSheetDataSet.Load(lDataReader, LoadOption.OverwriteChanges, "Defects", "AppointmentInfo", "PropertyInfo")

        End Sub

#End Region

#Region "Cancel Defect Appointment"

        Public Function cancelDefectAppointment(ByVal appointmentId As Integer, ByVal cancelReasonNotes As String, ByVal userId As Integer) As Boolean

            Dim inParametersList As New ParameterList()
            inParametersList.Add(New ParameterBO("appointmentId", appointmentId, DbType.Int32))
            inParametersList.Add(New ParameterBO("cancelReasonNotes", cancelReasonNotes, DbType.String))
            inParametersList.Add(New ParameterBO("userId", userId, DbType.Int32))

            Dim outParametersList As New ParameterList()
            Dim saveStatusParam As New ParameterBO("saveStatus", False, DbType.Boolean)
            outParametersList.Add(saveStatusParam)

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.CancelDefectAppointment)

            Return saveStatusParam.Value
        End Function

#End Region

#Region "Rearrange Defect Appointment"

        Public Function rearrangeDefectAppointment(ByVal appointmentId As Integer, ByVal userId As Integer)

            Dim inParametersList As New ParameterList()
            inParametersList.Add(New ParameterBO("appointmentId", appointmentId, DbType.Int32))
            inParametersList.Add(New ParameterBO("userId", SessionManager.getAppServicingUserId(), DbType.Int32))

            Dim outParametersList As New ParameterList()
            Dim saveStatusParam As New ParameterBO("saveStatus", False, DbType.Boolean)
            outParametersList.Add(saveStatusParam)

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.RearrangeDefectAppointment)

            Return saveStatusParam.Value
        End Function

#End Region

#Region "Confirm Defect Appointments By DefectIds (Comma Seprated)"

        Public Sub confirmDefectAppointmentsByDefectIds(defectIds As String)

            Dim inParametersList As New ParameterList()
            inParametersList.Add(New ParameterBO("defectIds", defectIds, DbType.String))

            Dim outParametersList As New ParameterList()
            Dim saveStatusParam As New ParameterBO("saveStatus", False, DbType.Boolean)
            outParametersList.Add(saveStatusParam)

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.ConfirmDefectAppointmentsByDefectsId)

        End Sub

#End Region


#Region "Get Detector By PropertyId"

        Public Function getDetectorByPropertyId(PropertyId As String, detectorType As String)
            Dim jobSheetDataSet As DataSet = New DataSet()
            Dim inParameters As New ParameterList
            inParameters.Add(New ParameterBO("PropertyId", PropertyId, DbType.String))
            inParameters.Add(New ParameterBO("detectorType", detectorType, DbType.String))
            Dim lDataReader = MyBase.SelectRecord(inParameters, SpNameConstants.GetDetectorByPropertyId)
            jobSheetDataSet.Load(lDataReader, LoadOption.OverwriteChanges, "Defects", "PowerSource")
            Return jobSheetDataSet
        End Function

#End Region


#Region "Rearrange Defect Appointment"

        Public Function saveDetectors(objDetectorBo As DetectorsBO, smokeDetectorType As String, noOfSmokeDetectors As String)

            Dim inParametersList As New ParameterList()
            inParametersList.Add(New ParameterBO("propertyId", objDetectorBo.PropertyId, DbType.String))
            inParametersList.Add(New ParameterBO("detectorType", objDetectorBo.DetectorType, DbType.String))
            inParametersList.Add(New ParameterBO("Location", objDetectorBo.Location, DbType.String))
            inParametersList.Add(New ParameterBO("Manufacturer", objDetectorBo.Manufacturer, DbType.String))
            inParametersList.Add(New ParameterBO("SerialNumber", objDetectorBo.SerialNumber, DbType.String))
            inParametersList.Add(New ParameterBO("PowerSource", objDetectorBo.PowerSource, DbType.Int32))
            inParametersList.Add(New ParameterBO("InstalledDate", objDetectorBo.InstalledDate, DbType.DateTime))
            inParametersList.Add(New ParameterBO("InstalledBy", SessionManager.getAppServicingUserId(), DbType.Int32))
            inParametersList.Add(New ParameterBO("IsLandlordsDetector", objDetectorBo.IsLandlordsDetector, DbType.Boolean))
            inParametersList.Add(New ParameterBO("TestedDate", objDetectorBo.LastTestedDate, DbType.DateTime))

            inParametersList.Add(New ParameterBO("BatteryReplaced", objDetectorBo.BatteryReplaced, DbType.DateTime))
            inParametersList.Add(New ParameterBO("Passed", objDetectorBo.IsPassed, DbType.Boolean))
            inParametersList.Add(New ParameterBO("Notes", objDetectorBo.Notes, DbType.String))
            inParametersList.Add(New ParameterBO("smokeDetectorType", smokeDetectorType, DbType.String))
            inParametersList.Add(New ParameterBO("noOfSmokeDetector", noOfSmokeDetectors, DbType.String))
            ' inParametersList.Add(New ParameterBO("userId", SessionManager.getAppServicingUserId(), DbType.Int32))

            Dim outParametersList As New ParameterList()

            Dim saveStatusParam As New ParameterBO("saveStatus", False, DbType.Boolean)
            outParametersList.Add(saveStatusParam)

            MyBase.SaveRecord(inParametersList, outParametersList, SpNameConstants.SaveDetectors)

            Return saveStatusParam.Value
        End Function

#End Region
    End Class

End Namespace
