﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject


Namespace AS_DataAccess

    Public Class LettersDAL : Inherits BaseDAL

        Dim OutParametersList As ParameterList = New ParameterList()

#Region "Functions"

#Region "get Letters"
        Public Sub getLetters(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetLetters)
        End Sub

        Public Sub getLettersForSearch(ByRef letterBO As LetterBO, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim statusIDParam As ParameterBO = New ParameterBO("StatusID", letterBO.StatusID, DbType.Int32)
            parametersList.Add(statusIDParam)

            Dim actionIDParam As ParameterBO = New ParameterBO("ActionID", letterBO.ActionID, DbType.Int32)
            parametersList.Add(actionIDParam)

            Dim titleParam As ParameterBO = New ParameterBO("Title", letterBO.Title, DbType.String)
            parametersList.Add(titleParam)

            Dim codeParam As ParameterBO = New ParameterBO("Code", letterBO.Code, DbType.String)
            parametersList.Add(codeParam)

            Dim isAlternativeServicingParam As ParameterBO = New ParameterBO("isAlternativeServicing", letterBO.IsAlternativeServicing, DbType.String)
            parametersList.Add(isAlternativeServicingParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetLetters)

        End Sub

        Public Sub getLetterById(ByVal letterId As Integer, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim letterIDParam As ParameterBO = New ParameterBO("StandardLetterID", letterId, DbType.Int32)
            parametersList.Add(letterIDParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetLetterById)
        End Sub
#End Region

#Region "Add Standard Letter Template"

        Public Sub addStandardLetter(ByRef letterBO As LetterBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim statusIDParam As ParameterBO = New ParameterBO("StatusID", letterBO.StatusID, DbType.Int32)
            parametersList.Add(statusIDParam)

            Dim actionIDParam As ParameterBO = New ParameterBO("ActionID", letterBO.ActionID, DbType.Int32)
            parametersList.Add(actionIDParam)

            Dim titleParam As ParameterBO = New ParameterBO("Title", letterBO.Title, DbType.String)
            parametersList.Add(titleParam)

            Dim codeParam As ParameterBO = New ParameterBO("Code", letterBO.Code, DbType.String)
            parametersList.Add(codeParam)

            Dim isAlternativeServicingParam As ParameterBO = New ParameterBO("IsAlternativeServicing", letterBO.IsAlternativeServicing, DbType.String)
            parametersList.Add(isAlternativeServicingParam)

            Dim bodyParam As ParameterBO = New ParameterBO("Body", letterBO.Body, DbType.String)
            parametersList.Add(bodyParam)

            Dim createdByParam As ParameterBO = New ParameterBO("CreatedBy", letterBO.CreatedBy, DbType.Int32)
            parametersList.Add(createdByParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", letterBO.ModifiedBy, DbType.Int32)
            parametersList.Add(modifiedByParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.AddStandardLetter)

        End Sub

#End Region

#Region "Update Standard Letter Template"

        Public Sub updateStandardLetter(ByRef letterBO As LetterBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim letterIDParam As ParameterBO = New ParameterBO("LetterId", letterBO.LetterID, DbType.Int32)
            parametersList.Add(letterIDParam)

            Dim statusIDParam As ParameterBO = New ParameterBO("StatusId", letterBO.StatusID, DbType.Int32)
            parametersList.Add(statusIDParam)

            Dim actionIDParam As ParameterBO = New ParameterBO("ActionId", letterBO.ActionID, DbType.Int32)
            parametersList.Add(actionIDParam)

            Dim titleParam As ParameterBO = New ParameterBO("Title", letterBO.Title, DbType.String)
            parametersList.Add(titleParam)

            Dim codeParam As ParameterBO = New ParameterBO("Code", letterBO.Code, DbType.String)
            parametersList.Add(codeParam)

            Dim isAlternativeServicingParam As ParameterBO = New ParameterBO("isAlternativeServicing", letterBO.IsAlternativeServicing, DbType.String)
            parametersList.Add(isAlternativeServicingParam)

            Dim bodyParam As ParameterBO = New ParameterBO("Body", letterBO.Body, DbType.String)
            parametersList.Add(bodyParam)

            Dim modifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", letterBO.ModifiedBy, DbType.Int32)
            parametersList.Add(modifiedByParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.UpdateStandardLetter)

        End Sub

#End Region

#Region "Delete Standard Letter Template"

        Public Sub deleteStandardLetter(ByVal letterID As Integer, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim LetterIdParam As ParameterBO = New ParameterBO("LetterId", letterID, DbType.Int32)
            parametersList.Add(LetterIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.DeleteStandardLetter)

        End Sub

#End Region

#Region "Get Saved Letters"

        Public Sub getSavedLetterById(ByVal letterId As Integer, ByRef resultSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim letterIDParam As ParameterBO = New ParameterBO("SavedLetterId", letterId, DbType.Int32)
            parametersList.Add(letterIDParam)

            MyBase.LoadDataSet(resultSet, parametersList, SpNameConstants.GetSavedLetterById)
        End Sub

#End Region

#Region "Add Saved Letter"

        Public Sub addSavedLetter(ByRef savedLetterBO As SavedLetterBO, ByRef resultset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JournalHistoryId", savedLetterBO.JournalHistoryId, DbType.Int32)
            parametersList.Add(journalHistoryIdParam)

            Dim letterBodyParam As ParameterBO = New ParameterBO("LetterBody", savedLetterBO.LetterBody, DbType.String)
            parametersList.Add(letterBodyParam)

            Dim letterTitleParam As ParameterBO = New ParameterBO("LetterTitle", savedLetterBO.LetterTitle, DbType.String)
            parametersList.Add(letterTitleParam)

            Dim letterIdParam As ParameterBO = New ParameterBO("LetterId", savedLetterBO.LetterId, DbType.Int32)
            parametersList.Add(letterIdParam)

            Dim teamIdParam As ParameterBO = New ParameterBO("TeamId", savedLetterBO.TeamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim fromResourceIdParam As ParameterBO = New ParameterBO("FromResourceId", savedLetterBO.FromResourceId, DbType.Int32)
            parametersList.Add(fromResourceIdParam)

            Dim signOffLookupCodeIdParam As ParameterBO = New ParameterBO("SignOffLookupCode", savedLetterBO.SignOffLookupCodeId, DbType.Int32)
            parametersList.Add(signOffLookupCodeIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.AddSavedLetter)
        End Sub



#End Region

#Region "Get SignOFF Types, Teams and From"

        Public Sub getSignOffTypes(ByRef signOffList As List(Of DropDownBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetSignOffTypes)

            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LookupCodeId")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("LookupCodeId"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CodeName")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("CodeName"))
                End If

                Dim objList As New DropDownBO(id, name)
                signOffList.Add(objList)

            End While
        End Sub


        Public Sub getTeams(ByRef teamList As List(Of DropDownBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetTeams)

            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEAMID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("TEAMID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEAMNAME")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("TEAMNAME"))
                End If

                Dim objList As New DropDownBO(id, name)
                teamList.Add(objList)

            End While
        End Sub

        Public Sub getFromUserByTeamID(ByVal teamID As Integer, ByRef userList As List(Of DropDownBO))

            Dim parametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("TeamID", teamID, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFromUsersFromTeamID)

            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPLOYEEID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("EMPLOYEEID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FIRSTNAME")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("FIRSTNAME"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LASTNAME")) Then
                    name += " "
                    name += myDataReader.GetString(myDataReader.GetOrdinal("LASTNAME"))
                End If

                Dim objList As New DropDownBO(id, name)
                userList.Add(objList)

            End While
        End Sub
#End Region

#Region "Get Customer And Property Rb Rc "
        Public Sub getCustomerAndPropertyRbRc(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef rentBalance As Double, ByRef rentCharge As Double)
            Dim spGetCustomerAndPropertyRbRc As String = SpNameConstants.GetCustomerAndPropertyRbRc

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()


            Dim totalCountParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(totalCountParam)

            Dim rentBalanceParam As ParameterBO = New ParameterBO("rentBalance", 0, DbType.Double)
            outParametersList.Add(rentBalanceParam)

            Dim rentChargeParam As ParameterBO = New ParameterBO("rentCharge", 0, DbType.Double)
            outParametersList.Add(rentChargeParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, spGetCustomerAndPropertyRbRc)

            rentBalance = CType(If(IsDBNull(outParametersList.Item(0).Value), 0.0, outParametersList.Item(0).Value), Double)
            rentCharge = CType(If(IsDBNull(outParametersList.Item(1).Value), 0.0, outParametersList.Item(1).Value), Double)
        End Sub
#End Region

#Region "Employee(From) Direct Dial, Email Address and Job Title By Employee ID (Print Letter) "

        Sub getFromTelEmailPrintLetterByEmployeeID(ByRef printLetterBo As PrintLetterBO)

            Dim parametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("EmployeeID", printLetterBo.EmployeeID, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFromTelEmailPrintLetterByEmployeeID)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FromDirectDial")) Then
                    printLetterBo.FromDirectDial = myDataReader.GetString(myDataReader.GetOrdinal("FromDirectDial"))
                Else
                    printLetterBo.FromDirectDial = "N/A"
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FromEmail")) Then
                    printLetterBo.FromEmail = myDataReader.GetString(myDataReader.GetOrdinal("FromEmail"))
                Else
                    printLetterBo.FromEmail = "N/A"
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FromJobTitle")) Then
                    printLetterBo.FromJobTitle = myDataReader.GetString(myDataReader.GetOrdinal("FromJobTitle"))
                Else
                    printLetterBo.FromJobTitle = "Job title N/A"
                End If

            End If

        End Sub

#End Region

#End Region

    End Class

End Namespace

