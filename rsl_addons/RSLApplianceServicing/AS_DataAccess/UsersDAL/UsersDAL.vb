﻿Imports System
Imports AS_Utilities
Imports AS_BusinessObject


Namespace AS_DataAccess

    Public Class UsersDAL : Inherits BaseDAL
#Region "Functions"


#Region "get Users"
        Public Sub getUsers(ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetUsers)
        End Sub
#End Region

#Region "Get User Types"
        Public Sub getUserTypes(ByVal index As Int32, ByVal userTypeList As List(Of UserTypeBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetUserTypes)
            While (myDataReader.Read)

                Dim id As Integer = 0
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("UserTypeID")) Then
                    id = myDataReader.GetInt16(myDataReader.GetOrdinal("UserTypeID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                Dim objList As New UserTypeBO(id, name)
                userTypeList.Add(objList)
            End While
        End Sub
        Public Sub getPatchLocations(ByVal index As Int32, ByVal userTypeList As List(Of UserTypeBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetPatchLocations)
            Dim id As Integer = 0
            Dim name As String = String.Empty
            id = 0
            name = "Please Select Patch"
            Dim objDefault As New UserTypeBO(id, name)
            userTypeList.Add(objDefault)
            While (myDataReader.Read)
                id = 0
                name = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PATCHID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("PATCHID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Location")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Location"))
                End If
                Dim objList As New UserTypeBO(id, name)
                userTypeList.Add(objList)
            End While
        End Sub
        Public Sub getSchemeNames(ByVal selectedValue As Int32, ByVal userTypeList As List(Of UserTypeBO))
            Dim objParameterList As ParameterList = New ParameterList()

            Dim selectedValueParam As ParameterBO = New ParameterBO("patchid", selectedValue, DbType.Int32)
            objParameterList.Add(selectedValueParam)
            Dim id As Integer = 0
            Dim name As String = String.Empty
            id = 0
            name = "Please Select Scheme"
            Dim objDefault As New UserTypeBO(id, name)
            userTypeList.Add(objDefault)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(objParameterList, SpNameConstants.GetSchemeNames)
            While (myDataReader.Read)

                id = 0
                name = String.Empty
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DEVELOPMENTID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("DEVELOPMENTID"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SCHEMENAME")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("SCHEMENAME"))
                End If
                'id = 0
                'name = "Please Select Scheme"
                Dim objList As New UserTypeBO(id, name)
                userTypeList.Add(objList)
            End While
        End Sub
#End Region

        Sub getQuickFindUsers(ByVal firstname As String, ByVal lastname As String, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim fname As ParameterBO = New ParameterBO("str1", firstname, DbType.String)
            parametersList.Add(fname)

            Dim lname As ParameterBO = New ParameterBO("str2", lastname, DbType.String)
            parametersList.Add(lname)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetQuickFindUsers)

        End Sub

        Public Sub getTrades(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAllTradesLookups)
        End Sub

        Public Sub saveTrades(ByVal empId As String, ByVal tradeIds As String, ByVal userId As String, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()
            Dim _empId As ParameterBO = New ParameterBO("EmpId", empId, DbType.Int32)
            parametersList.Add(_empId)
            Dim _tradeIds As ParameterBO = New ParameterBO("TradeIds", tradeIds, DbType.String)
            parametersList.Add(_tradeIds)
            Dim _userId As ParameterBO = New ParameterBO("UserId", userId, DbType.Int32)
            parametersList.Add(_userId)
            ' MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.SaveSelectedTrades)
            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SaveSelectedTrades)
        End Sub

        Public Sub GetEmpTrades(ByVal empId As String, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim _empId As ParameterBO = New ParameterBO("EmpId", empId, DbType.Int32)
            parametersList.Add(_empId)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetEmpTrades)
        End Sub

        Public Sub saveOftecGassafe(ByVal ofTec As Integer, ByVal gassafe As Integer, ByVal regNo As String, ByVal userId As Integer, ByVal empId As Integer, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()
            Dim _oftec As ParameterBO = New ParameterBO("ofTec", ofTec, DbType.Int32)
            parametersList.Add(_oftec)
            Dim _gasEngineer As ParameterBO = New ParameterBO("GSRENo", regNo, DbType.String)
            parametersList.Add(_gasEngineer)
            Dim _gassafe As ParameterBO = New ParameterBO("gasSafe", gassafe, DbType.Int32)
            parametersList.Add(_gassafe)
            Dim _userId As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(_userId)
            Dim _empId As ParameterBO = New ParameterBO("empId", empId, DbType.Int32)
            parametersList.Add(_empId)
            ' MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.SaveSelectedTrades)
            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SaveoftecGassafe)
        End Sub




        Public Sub getInspectionTypes(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetInspectionTypes)
        End Sub

        Public Sub getInspectionTypesByDesc(ByRef resultDataSet As DataSet, ByVal description As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim descriptionParam As ParameterBO = New ParameterBO("description", description, DbType.String)
            parametersList.Add(descriptionParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetInspectionTypesByDesc)
        End Sub

        Sub UpdateResource(ByVal EmployeeId As Integer, ByVal value As Integer, ByRef resultDataset As DataSet)
            Dim ModifiedBy As Integer = SessionManager.getAppServicingUserId()
            'SessionManager.getSessionVariable(SessionConstants.ConSessionUserId, ModifiedBy)
            'Dim resultDataset As DataSet = New DataSet()
            Dim ModifiedDate As DateTime = DateAndTime.Now
            Dim isActive As Boolean = 1
            Dim parametersList As ParameterList = New ParameterList()

            Dim ModifiedByParam As ParameterBO = New ParameterBO("ModifiedBy", ModifiedBy, DbType.Int32)
            parametersList.Add(ModifiedByParam)
            Dim ModifiedDatedParam As ParameterBO = New ParameterBO("ModifiedDate", ModifiedDate, DbType.DateTime)
            parametersList.Add(ModifiedDatedParam)
            Dim isActiveParam As ParameterBO = New ParameterBO("IsActive", isActive, DbType.Int32)
            parametersList.Add(isActiveParam)
            Dim UserTypeId As ParameterBO = New ParameterBO("UserTypeId", value, DbType.Int32)
            parametersList.Add(UserTypeId)
            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.UpdateResource)
        End Sub

        Sub deleteUser(ByVal EmployeeId As Integer, ByVal resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.DeleteUser)

        End Sub

#Region "Save User Functions"


        Function saveUser(ByVal CreatedBy As Integer, ByVal EmployeeId As Integer, ByVal SeletedTypeValue As Integer, ByRef resultDataSet As DataSet, ByVal signatureFileName As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)
            'Dim GSREnoParam As ParameterBO = New ParameterBO("GSREno", GSREno, DbType.Int32)
            'parametersList.Add(GSREnoParam)
            Dim SeletedTypeValueParam As ParameterBO = New ParameterBO("UserTypeID", SeletedTypeValue, DbType.Int32)
            parametersList.Add(SeletedTypeValueParam)

            Dim CreatedByParam As ParameterBO = New ParameterBO("CreatedBy", CreatedBy, DbType.Int32)
            parametersList.Add(CreatedByParam)

            Dim SignatureFileNameParam As ParameterBO = New ParameterBO("SignatureFileName", signatureFileName, DbType.String)
            parametersList.Add(SignatureFileNameParam)

            Dim countUserParam As ParameterBO = New ParameterBO("userCount", 0, DbType.Int32)
            OutParametersList.Add(countUserParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.saveUser)

            Dim countUser As Integer = CType(OutParametersList.Item(0).Value, Int32)
            Return countUser
        End Function

        Sub saveUserViewStatSchemeList(ByVal SchemeId As Integer, ByVal EmployeeId As Int32)
            Dim OutParametersList As ParameterList = New ParameterList()
            Dim parametersList As ParameterList = New ParameterList()
            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim SchemeIdParam As ParameterBO = New ParameterBO("DevelopmentId", SchemeId, DbType.Int32)
            parametersList.Add(SchemeIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SaveDevelopmentID)
        End Sub

        Sub saveUserViewStatPatchList(ByVal PatchId As Integer, ByVal EmployeeId As Int32)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim PatchIdParam As ParameterBO = New ParameterBO("PatchId", PatchId, DbType.Int32)
            parametersList.Add(PatchIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SavePatchID)
        End Sub

        Sub saveChkBoxInspectionList(ByVal InspectionTypeId As String, ByVal EmployeeId As Int32)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim NewInspectionTypeId As Integer = CType(InspectionTypeId, Integer)
            Dim EmployeeIdParam As ParameterBO = New ParameterBO("employeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim InspectionTypeIdParam As ParameterBO = New ParameterBO("inspectionTypeId", NewInspectionTypeId, DbType.Int32)
            parametersList.Add(InspectionTypeIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SaveUserInspectionType)
        End Sub

#End Region

#Region "Edit Users"
        Sub EditUser(ByVal employeeId As Integer, ByRef edittUserDataSet As DataSet, ByRef edittUserPatchDataSet As DataSet, ByRef edittUserInspectionDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeParam)
            ' Get Edit Users Info
            MyBase.LoadDataSet(edittUserDataSet, parametersList, SpNameConstants.GetEidtUsers)
            ' Get Edit User Patch and Scheme Info
            MyBase.LoadDataSet(edittUserPatchDataSet, parametersList, SpNameConstants.GetEditUserPatchInfo)
            ' Get Edit User Inspection Type Info
            MyBase.LoadDataSet(edittUserInspectionDataSet, parametersList, SpNameConstants.GetEditUserInpectionTypeInfo)



        End Sub
#End Region

#Region "Update Users Functions"


        Sub updateUser(ByVal ModifiedBy As Integer, ByVal EmployeeId As Integer, ByVal signaturePath As String, ByVal SeletedTypeValue As Integer, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim signaturePathParam As ParameterBO = New ParameterBO("SignaturePath", signaturePath, DbType.String)
            parametersList.Add(signaturePathParam)

            'Dim GSREnoParam As ParameterBO = New ParameterBO("GSREno", GSREno, DbType.Int32)
            'parametersList.Add(GSREnoParam)
            Dim SeletedTypeValueParam As ParameterBO = New ParameterBO("UserTypeID", SeletedTypeValue, DbType.Int32)
            parametersList.Add(SeletedTypeValueParam)

            Dim CreatedByParam As ParameterBO = New ParameterBO("modifiedBy", ModifiedBy, DbType.Int32)
            parametersList.Add(CreatedByParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.UpdateUsers)
        End Sub

        Sub updadteUserViewStatSchemeList(ByVal SchemeId As Integer, ByVal EmployeeId As Int32)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim SchemeIdParam As ParameterBO = New ParameterBO("DevelopmentId", SchemeId, DbType.Int32)
            parametersList.Add(SchemeIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.UpdateDevelopmentID)
        End Sub

        Sub updateUserViewStatPatchList(ByVal PatchId As Integer, ByVal EmployeeId As Int32)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim PatchIdParam As ParameterBO = New ParameterBO("PatchId", PatchId, DbType.Int32)
            parametersList.Add(PatchIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.UpdateUserPatchInfo)
        End Sub

        Sub updateChkBoxInspectionList(ByVal InspectionTypeId As String, ByVal EmployeeId As Int32)
            Dim parametersList As ParameterList = New ParameterList()
            Dim OutParametersList As ParameterList = New ParameterList()

            Dim NewInspectionTypeId As Integer = CType(InspectionTypeId, Integer)
            Dim EmployeeIdParam As ParameterBO = New ParameterBO("EmployeeId", EmployeeId, DbType.Int32)
            parametersList.Add(EmployeeIdParam)

            Dim InspectionTypeIdParam As ParameterBO = New ParameterBO("inspectionTypeId", NewInspectionTypeId, DbType.Int32)
            parametersList.Add(InspectionTypeIdParam)

            MyBase.SaveRecord(parametersList, OutParametersList, SpNameConstants.SaveUserInspectionType)
        End Sub
#End Region

#Region "get Employee Id By Name"
        Sub getEmployeeIdByName(ByVal firstName As String, ByVal lastName As String, ByRef userInfoDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim firstNameParam As ParameterBO = New ParameterBO("firstName", firstName, DbType.String)
            parametersList.Add(firstNameParam)

            Dim lastNameParam As ParameterBO = New ParameterBO("lastName", lastName, DbType.String)
            parametersList.Add(lastNameParam)

            MyBase.LoadDataSet(userInfoDataSet, parametersList, SpNameConstants.GetEmployeeIdByName)
        End Sub
#End Region

#End Region

        Sub deleteUserInspectionType(ByVal EmployeeId As Integer)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outparameterList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", EmployeeId, DbType.Int32)
            parameterList.Add(employeeIdParam)

            MyBase.SaveRecord(parameterList, outparameterList, SpNameConstants.DeleteUserInpectionTypeInfo)

        End Sub

        Function getEmployeeById(ByVal employeeId As Int32) As DataSet
            Dim parametersList As ParameterList = New ParameterList()
            Dim userBo As UserBO = New UserBO()
            Dim ds As DataSet = New DataSet()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(ds, parametersList, SpNameConstants.GetEmployeeById)
            Return ds
        End Function

        Function GetPropertyEmployeeEmail(employeeId As String) As String
            Dim parametersList As ParameterList = New ParameterList()
            Dim ds As DataSet = New DataSet()
            Dim employeeEmail As String = String.Empty

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(ds, parametersList, SpNameConstants.GetPropertyEmployeeEmail)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                employeeEmail = ds.Tables(0).Rows(i)(0).ToString()
            Next

            Return employeeEmail
        End Function

        Function GetEmployeeWorkMobile(employeeId As String) As String
            Dim parametersList As ParameterList = New ParameterList()
            Dim ds As DataSet = New DataSet()
            Dim employeeWorkMobile As String = String.Empty

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(ds, parametersList, SpNameConstants.GetEmployeeWorkMobile)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                employeeWorkMobile = ds.Tables(0).Rows(i)(0).ToString()
            Next

            Return employeeWorkMobile
        End Function

    End Class

    
End Namespace
