﻿' Class Name -- CustomerDAL
' Base Class -- BaseDAL
' Summary --    Contains Functionality related to Customer/Tenant
' Methods --    AuthenticateCustomer
' Created By  -- Muanwar Nadeem

'---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities




Imports AS_BusinessObject
Namespace AS_DataAccess

    Public Class CustomerDAL : Inherits BaseDAL

#Region "Functions"

#Region "GetCustomerInfo"

        Public Sub GetCustomerDetailsInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean)
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SpNameConstants.GetCustomerInfo
            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type

            ' ParameterBO for userName
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            ' Adding object to paramList
            inParamList.Add(customerIdParam)

            ' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            'Set BO attributes by reading from data reader
            'If getDetailsInfo Then

            '    'Get Customer information of first record returned
            '    If (myDataReader.Read) Then
            '        Me.FillCustomerDetailsBODetailed(myDataReader, custDetailsBO)
            '    End If

            'Else

            '    'Get Customer information of first record returned
            '    If (myDataReader.Read) Then
            '        Me.FillCustomerDetailsBOBasic(myDataReader, custDetailsBO)
            '    End If

            'End If

            custDetailsBO.IsFlagStatus = True

        End Sub
#End Region

#Region "Update customer address"
        Public Function updateAddress(ByVal objCustomerBO As CustomerBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", objCustomerBO.CustomerId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim telephoneParam As ParameterBO = New ParameterBO("telephone", objCustomerBO.Telephone, DbType.String)
            parametersList.Add(telephoneParam)

            Dim mobileParam As ParameterBO = New ParameterBO("mobile", objCustomerBO.Mobile, DbType.String)
            parametersList.Add(mobileParam)

            Dim emailParam As ParameterBO = New ParameterBO("email", objCustomerBO.Email, DbType.String)
            parametersList.Add(emailParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.AmendCustomerAddress)

            Return outParametersList.ElementAt(0).Value

        End Function

#End Region

#End Region

    End Class

End Namespace



