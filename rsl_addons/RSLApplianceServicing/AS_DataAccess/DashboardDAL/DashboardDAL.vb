﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports AS_Utilities
Imports AS_BusinessObject

Namespace AS_DataAccess
    Public Class DashboardDAL : Inherits BaseDAL

#Region "Functions"

#Region "Counts"
        Sub getCountNoEntry(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim FuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(FuelTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountNoEntries)

        End Sub

        Sub getCountLegalProceedings(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountLegalProceedings)
        End Sub

        Sub getCountExpired(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountExpired)
        End Sub

        Sub getCountExpiresIn56Days(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountExpiresIn56Days)
        End Sub



        Sub getCountAppointmentsArranged(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountAppointmentsArranged)
        End Sub

        Sub getCountVoidAppointmentsArranged(ByRef fuelTypeSelected As String, ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet)

            If (fuelTypeSelected.Equals("Gas")) Then
                fuelTypeSelected = "Mains Gas"
            End If

            Dim parameterList As ParameterList = New ParameterList()

            Dim fuelTypeSelectedParam As ParameterBO = New ParameterBO("fuelType", fuelTypeSelected, DbType.String)
            parameterList.Add(fuelTypeSelectedParam)

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountVoidAppointmentsArranged)
        End Sub

        Sub getCountAppointmentsToBeArranged(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetAppointmentToBeArranged)
        End Sub

        Sub getCountVoidAppointmentsToBeArranged(ByRef fuelTypeSelected As String, ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet)

            If (fuelTypeSelected.Equals("Gas")) Then
                fuelTypeSelected = "Mains Gas"
            End If

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim fuelTypeSelectedParam As ParameterBO = New ParameterBO("fuelType", fuelTypeSelected, DbType.String)
            parameterList.Add(fuelTypeSelectedParam)

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountVoidAppointmentsToBeArranged)


        End Sub



        Sub getCountVoidProperties(ByRef fuelTypeSelected As String, ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet)

            If (fuelTypeSelected.Equals("Gas")) Then
                fuelTypeSelected = "Mains Gas"
            End If

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim fuelTypeSelectedParam As ParameterBO = New ParameterBO("fuelType", fuelTypeSelected, DbType.String)
            parameterList.Add(fuelTypeSelectedParam)

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountVoidProperties)


        End Sub

#End Region

#Region "Management Info Counts"

        Sub getCountMIData(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByVal fuelType As String)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", fuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.CountMIData)
        End Sub

#End Region

#Region "Certificate Expiry"

        Sub getCertificateExpiry(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef resultDataSet As DataSet, ByRef fuelType As String)

            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim isAlternativeParam As ParameterBO = New ParameterBO("FuelType", fuelType, DbType.String)
            parameterList.Add(isAlternativeParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.ChartCertificateExpiry)

        End Sub

#End Region

#Region "Insepction Status"

#Region "get No Entries"
        Public Function getNoEntries(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.InspectionStatusNoEntries)

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region


#Region "get Abort"
        Public Function getAbortAppointments(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.InspectionStatusAbort)

            Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region


#Region "get Legal Proceedings"
        Public Function getLegalProceedings(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.InspectionStatusLegalProceedings)
            Return outParametersList.Item(0).Value.ToString()
        End Function
#End Region

#Region "get Expired"
        Public Function getExpired(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.InspectionStatusCertificatesExpired)
            Return outParametersList.Item(0).Value.ToString()
        End Function
#End Region

#Region "get Appointments To Be Arranged"

        Public Function getAppointmentsToBeArranged(ByVal patchId As Integer, ByVal developmentId As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet, ByRef FuelType As String)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("PatchId", patchId, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("DevelopmentId", developmentId, DbType.Int32)
            parameterList.Add(developmentIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim fuelTypeParam As ParameterBO = New ParameterBO("FuelType", FuelType, DbType.String)
            parameterList.Add(fuelTypeParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)


            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.InspectionStatusAppointmentsToBeArranges)
            Return outParametersList.Item(0).Value.ToString()
        End Function

#End Region
#End Region

#Region "Last Login Date"

        Sub getLastLoginDate(ByVal employeeId As Integer, ByRef resultSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parameterList.Add(patchIdParam)

            MyBase.LoadDataSet(resultSet, parameterList, SpNameConstants.GetLastLoginDate)
        End Sub


#End Region

#Region "Get RSLModules"

        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByRef userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRSLModules As DataTable = New DataTable()
            dtRSLModules.TableName = "RSLModules"
            resultDataSet.Tables.Add(dtRSLModules)

            Dim userIdParam As ParameterBO = New ParameterBO("USERID", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim orderParam As ParameterBO = New ParameterBO("OrderASC", 1, DbType.Int32)
            parametersList.Add(orderParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRSLModulesList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRSLModules)

        End Sub
#End Region

#Region "Get Property Menus List"
        ''' <summary>
        ''' Get Property Menus List
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Sub getPropertyMenuList(ByRef resultDataset As DataSet, ByVal employeeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim menusDt As DataTable = New DataTable()
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt
            resultDataset.Tables.Add(menusDt)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuList)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, menusDt)

        End Sub

#End Region

#Region "Get Property Page List "
        ''' <summary>
        ''' Get Property Page List 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim modulesDt As DataTable = New DataTable()
            modulesDt.TableName = ApplicationConstants.AccessGrantedModulesDt
            resultDataSet.Tables.Add(modulesDt)

            Dim menusDt As DataTable = New DataTable()
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt
            resultDataSet.Tables.Add(menusDt)

            Dim pageDt As DataTable = New DataTable()
            pageDt.TableName = ApplicationConstants.AccessGrantedPagesDt
            resultDataSet.Tables.Add(pageDt)

            Dim randomPageDt As DataTable = New DataTable()
            randomPageDt.TableName = ApplicationConstants.RandomPageDt
            resultDataSet.Tables.Add(randomPageDt)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim menuTextParam As ParameterBO = New ParameterBO("menuText", selectedMenu, DbType.String)
            parametersList.Add(menuTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuPages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, modulesDt, menusDt, pageDt, randomPageDt)
        End Sub
#End Region

#Region "Get Servicing Types"


        Sub getServicingTypes(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.getServicingTypes)
        End Sub
#End Region

#Region "Populate ME Dropddowns"
        ''' <summary>
        ''' Populate ME Dashboard dropdowns
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>

        Sub populateMESchemeDropdown(ByVal resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            Dim paramDevelopmentId As ParameterBO = New ParameterBO("developmentId", -1, DbType.Int32)
            parameterList.Add(paramDevelopmentId)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetScheme)
        End Sub

        Sub populateBlockDropdown(ByVal resultDataSet As DataSet, ByVal schemeId As Integer)

            Dim parameterList As ParameterList = New ParameterList()

            Dim paramschemeId As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parameterList.Add(paramschemeId)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetBlocks)
        End Sub

#End Region

#Region " Get ME Dashboard Count"
        ''' <summary>
        ''' Counts on ME dashboard 
        ''' </summary>
        ''' <param name="selectedMEScheme"></param>
        ''' <param name="selectedMEBlock"></param>
        ''' <param name="rsNoEntry"></param>
        ''' <remarks></remarks>
        Sub getMECountNoEntry(ByVal selectedMEScheme As Integer, ByVal selectedMEBlock As Integer, ByRef rsNoEntry As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("SchemeId", selectedMEScheme, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("BlockId", selectedMEBlock, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(rsNoEntry, parameterList, SpNameConstants.MECountNoEntries)
        End Sub

        Sub getMECountOverDue(ByVal selectedMEScheme As Integer, ByVal selectedMEBlock As Integer, ByRef rsOverdue As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("SchemeId", selectedMEScheme, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("BlockId", selectedMEBlock, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(rsOverdue, parameterList, SpNameConstants.MECountOverdue)
        End Sub

        Sub getMECountAppointmentArranged(ByVal selectedMEScheme As Integer, ByVal selectedMEBlock As Integer, ByRef rsAppointmentArranged As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("SchemeId", selectedMEScheme, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("BlockId", selectedMEBlock, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(rsAppointmentArranged, parameterList, SpNameConstants.MECountAppointmentArranged)
        End Sub

        Sub getMECountAppointmentToBeArranged(ByVal selectedMEScheme As Integer, ByVal selectedMEBlock As Integer, ByRef rsAppointmentToBeArranged As DataSet)
            Dim parameterList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("SchemeId", selectedMEScheme, DbType.Int32)
            parameterList.Add(patchIdParam)

            Dim developmentIdParam As ParameterBO = New ParameterBO("BlockId", selectedMEBlock, DbType.Int32)
            parameterList.Add(developmentIdParam)

            MyBase.LoadDataSet(rsAppointmentToBeArranged, parameterList, SpNameConstants.MECountAppointmentToBeArranged)
        End Sub
#End Region


#End Region

#Region "Get Servicing Status List"
        Function getServicingStatus(ByVal servicingTypeSelected As Integer, ByVal selectedScheme As Integer, ByVal selectedBlock As Integer, ByRef objPageSortBo As PageSortBO, ByRef resultSet As DataSet) As Integer
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim servicingTypeParam As ParameterBO = New ParameterBO("servicingType", servicingTypeSelected, DbType.Int32)
            parameterList.Add(servicingTypeParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", selectedScheme, DbType.Int32)
            parameterList.Add(schemeIdParam)

            Dim blockIdParam As ParameterBO = New ParameterBO("blockId", selectedBlock, DbType.Int32)
            parameterList.Add(blockIdParam)

            Dim pageSizeColumn As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSizeColumn)

            Dim pageNumberColumn As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumberColumn)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parameterList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parameterList.Add(sortOrder)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultSet, parameterList, outParametersList, SpNameConstants.GetServicingList)

            Return Convert.ToInt32(outParametersList.Item(0).Value.ToString())

        End Function
#End Region




    End Class
End Namespace





