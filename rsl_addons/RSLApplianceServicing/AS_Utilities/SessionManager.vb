﻿Imports System
Imports System.Web.HttpContext
Imports System.Web

Namespace AS_Utilities

    Public Class SessionManager

#Region "Methods"

        Public Shared Function setSessionVariable(ByVal sessionVariableKey As String, ByVal sessionVariableValue As Object) As Boolean

            Return Current.Session(sessionVariableKey) = sessionVariableValue

        End Function

        Public Shared Function getSessionVariable(ByVal sessionVariableKey As String, ByVal sessionVariableObject As Object) As Object

            Dim varSessionVariable As Object = CTypeDynamic(Current.Session(sessionVariableKey), sessionVariableObject)

            Return varSessionVariable

        End Function


#End Region

#Region "Set / Get Activity Letter Detail"

#Region "Set Activity Letter Detail"
        Public Shared Sub setActivityLetterDetail(ByRef dt As DataTable)
            Current.Session(SessionConstants.ActivityLetterDataSet) = dt
        End Sub
#End Region

#Region "Get Activity Letter Detail"
        Public Shared Function getActivityLetterDetail() As DataTable
            Return CType(Current.Session(SessionConstants.ActivityLetterDataSet), DataTable)
        End Function
#End Region

#Region "Remove Activity Letter Detail"
        Public Shared Sub removeActivityLetterDetail()
            If (Not IsNothing(Current.Session(SessionConstants.ActivityLetterDataSet))) Then
                Current.Session.Remove(SessionConstants.ActivityLetterDataSet)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Send Letter"

#Region "Set Send Letter"
        Public Shared Sub setSendLetter(ByRef sendLetter As Boolean)
            Current.Session(SessionConstants.SendLetter) = sendLetter
        End Sub
#End Region

#Region "Get Send Letter"
        Public Shared Function getSendLetter() As Boolean
            If (IsNothing(Current.Session(SessionConstants.SendLetter))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SendLetter), Boolean)
            End If
        End Function
#End Region

#Region "Remove Send Letter"
        Public Shared Sub removeSendLetter()
            If (Not IsNothing(Current.Session(SessionConstants.SendLetter))) Then
                Current.Session.Remove(SessionConstants.SendLetter)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Activity Standard Letter Id"
#Region "Set Activity Standard Letter Id"
        Public Shared Sub setActivityStandardLetterId(ByRef standardLetterId As Integer)
            Current.Session(SessionConstants.ActivityStandardLetterId) = standardLetterId
        End Sub
#End Region

#Region "Get Activity Standard Letter Id"
        Public Shared Function getActivityStandardLetterId() As Integer
            If (IsNothing(Current.Session(SessionConstants.ActivityStandardLetterId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.ActivityStandardLetterId), Integer)
            End If

        End Function
#End Region

#Region "remove Activity Standard Letter Id"
        Public Shared Sub removeActivityStandardLetterId()
            If (Not IsNothing(Current.Session(SessionConstants.ActivityStandardLetterId))) Then
                Current.Session.Remove(SessionConstants.ActivityStandardLetterId)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get User Id"

#Region "Set App Servicing User Id"
        Public Shared Sub setAppServicingUserId(ByRef appServicingUserId As Integer)
            Current.Session(SessionConstants.AppServicingUserId) = appServicingUserId
        End Sub
#End Region

#Region "get App Servicing User Id"
        Public Shared Function getAppServicingUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.AppServicingUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.AppServicingUserId), Integer)
            End If
        End Function
#End Region

#Region "remove App Servicing User Id"
        Public Sub removeAppServicingUserId()
            If (Not IsNothing(Current.Session(SessionConstants.AppServicingUserId))) Then
                Current.Session.Remove(SessionConstants.AppServicingUserId)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"
        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub
#End Region


#Region "get User Full Name"
        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return "N/A"
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function
#End Region


#Region "remove User Full Name"
        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get User Type"
        'these fucntions 'll be used to set/get/remove the applinace user type in session
#Region "Set User Type"
        ''' <summary>
        ''' '''this function 'll be used to popuplate the applinace user type in session
        ''' </summary>
        ''' <param name="userType"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserType(userType As String)
            Current.Session(SessionConstants.UserType) = userType
        End Sub
#End Region

#Region "get User Type"
        ''' <summary>
        ''' this condition 'll be used to get the applinace user type in session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserType() As String
            If (IsNothing(Current.Session(SessionConstants.UserType))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.UserType), String)
            End If
        End Function
#End Region

#Region "remove User Type"
        ''' <summary>
        ''' 'this condition 'll be used to remove the applinace user type in session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserType()
            'this condition 'll be used to remove the applinace user type in session
            If (Not IsNothing(Current.Session(SessionConstants.UserType))) Then
                Current.Session.Remove(SessionConstants.UserType)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"
        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub
#End Region


#Region "get User Employee Id"
        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function
#End Region


#Region "remove User Employee Id"
        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get User Page Access"

#Region "Set User Page Access"
        Public Shared Sub setUserPageAccess(ByRef userAccessDcit As Dictionary(Of String, Integer))
            Current.Session(SessionConstants.UserPageAccess) = userAccessDcit
        End Sub
#End Region


#Region "get User User Page Access"
        Public Shared Function getUserPageAccess()
            If (IsNothing(Current.Session(SessionConstants.UserPageAccess))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.UserPageAccess), Dictionary(Of String, Integer))
            End If
        End Function
#End Region


#Region "remove User Page Access"
        Public Shared Sub removeUserPageAccess()
            If (Not IsNothing(Current.Session(SessionConstants.UserPageAccess))) Then
                Current.Session.Remove(SessionConstants.UserPageAccess)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Document Name"

#Region "Set Document Name"
        Public Shared Sub setDocumentUploadName(ByRef documentUploadName As String)
            Current.Session(SessionConstants.DocumentUploadName) = documentUploadName
        End Sub
#End Region


#Region "get Document Upload Name"
        Public Shared Function getDocumentUploadName()
            If (IsNothing(Current.Session(SessionConstants.DocumentUploadName))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.DocumentUploadName), String)
            End If
        End Function
#End Region


#Region "remove Document Name"
        Public Shared Sub removeDocumentUploadName()
            If (Not IsNothing(Current.Session(SessionConstants.DocumentUploadName))) Then
                Current.Session.Remove(SessionConstants.DocumentUploadName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Photo Name"

#Region "Set Photo Name"
        Public Shared Sub setPhotoUploadName(ByRef photoUploadName As String)
            Current.Session(SessionConstants.PhotoUploadName) = photoUploadName
        End Sub
#End Region

#Region "get Photo Upload Name"
        Public Shared Function getPhotoUploadName()
            If (IsNothing(Current.Session(SessionConstants.PhotoUploadName))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.PhotoUploadName), String)
            End If
        End Function
#End Region

#Region "remove Photo Name"
        Public Shared Sub removePhotoUploadName()
            If (Not IsNothing(Current.Session(SessionConstants.PhotoUploadName))) Then
                Current.Session.Remove(SessionConstants.PhotoUploadName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get EPC Photo Name"

#Region "Set EPC Photo Name"
        Public Shared Sub setEPCPhotoUploadName(ByRef epcPhotoUploadName As String)
            Current.Session(SessionConstants.EPCPhotoUploadName) = epcPhotoUploadName
        End Sub
#End Region

#Region "get EPC Photo Upload Name"
        Public Shared Function getEPCPhotoUploadName()
            If (IsNothing(Current.Session(SessionConstants.EPCPhotoUploadName))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.EPCPhotoUploadName), String)
            End If
        End Function
#End Region

#Region "remove EPC Photo Name"
        Public Shared Sub removeEPCPhotoUploadName()
            If (Not IsNothing(Current.Session(SessionConstants.EPCPhotoUploadName))) Then
                Current.Session.Remove(SessionConstants.EPCPhotoUploadName)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Appt Schedule Param"

#Region "Set Appt Schedule Param"
        Public Shared Sub setApptScheduleParam(ByRef apptScheduleParam() As String)
            Current.Session(SessionConstants.ApptScheduleParam) = apptScheduleParam
        End Sub
#End Region

#Region "get Appt Schedule Param"
        Public Shared Function getApptScheduleParam()
            If (IsNothing(Current.Session(SessionConstants.ApptScheduleParam))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.ApptScheduleParam), String())
            End If
        End Function
#End Region

#Region "remove Appt Schedule Param"
        Public Shared Sub removeApptScheduleParam()
            If (Not IsNothing(Current.Session(SessionConstants.ApptScheduleParam))) Then
                Current.Session.Remove(SessionConstants.ApptScheduleParam)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Property Id"
#Region "Set Property Id For Edit Letter"
        Public Shared Sub setPropertyIdForEditLetter(ByRef PropertyId As String)
            Current.Session(SessionConstants.PropertyId) = PropertyId
        End Sub
#End Region

#Region "Get Property Id For Edit Letter"
        Public Shared Function getPropertyIdForEditLetter() As String
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If

        End Function
#End Region

#Region "remove Property Id For Edit Letter"
        Public Shared Sub removePropertyIdForEditLetter()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Print Letter Bo"
#Region "Set Print Letter Bo"
        Public Shared Sub setPrintLetterBo(ByRef printLetterBo As PrintLetterBO)
            Current.Session(SessionConstants.PrintLetterBo) = printLetterBo
        End Sub
#End Region

#Region "Get Print Letter Bo"
        Public Shared Function getPrintLetterBo() As PrintLetterBO
            If (IsNothing(Current.Session(SessionConstants.PrintLetterBo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.PrintLetterBo), PrintLetterBO)
            End If
        End Function
#End Region

#Region "remove Print Letter Bo"
        Public Shared Sub removePrintLetterBo()
            If (Not IsNothing(Current.Session(SessionConstants.PrintLetterBo))) Then
                Current.Session.Remove(SessionConstants.PrintLetterBo)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get DueWithIn56Days"

#Region "Set DueWithIn56Days"

        Public Shared Sub setDueWithIn56Days(ByRef checked As Boolean)
            Current.Session(SessionConstants.DueWithIn56Days) = checked
        End Sub

#End Region

#Region "Get DueWithIn56Days"

        Public Shared Function getDueWithIn56Days() As Boolean
            If (Not IsNothing(Current.Session(SessionConstants.DueWithIn56Days))) Then
                Return CType(Current.Session(SessionConstants.DueWithIn56Days), Boolean)
            Else
                Return True
            End If
        End Function

#End Region

#Region "Remove DueWithIn56Days"
        Public Shared Sub removeDueWithIn56Days()
            Current.Session.Remove(SessionConstants.DueWithIn56Days)
        End Sub
#End Region

#End Region

#Region "Set / Get SearchText"

#Region "Set SearchText"

        Public Shared Sub setSearchText(ByRef SearchText As String)
            Current.Session(SessionConstants.SearchText) = SearchText
        End Sub

#End Region

#Region "Get SearchText"

        Public Shared Function getSearchText() As String
            If (Not IsNothing(Current.Session(SessionConstants.SearchText))) Then
                Return CType(Current.Session(SessionConstants.SearchText), String)
            Else
                Return ""
            End If
        End Function

#End Region

#Region "Remove SearchText"
        Public Shared Sub removeSearchText()
            Current.Session.Remove(SessionConstants.SearchText)
        End Sub
#End Region

#End Region





#Region "Set / Get Termination Date"

#Region "Set Termination Date"

        Public Shared Sub setTerminationDate(ByRef TerminationDate As String)
            Current.Session(SessionConstants.TerminationDate) = TerminationDate
        End Sub

#End Region

#Region "Get Termination Date"

        Public Shared Function getTerminationDate() As String
            If (Not IsNothing(Current.Session(SessionConstants.TerminationDate))) Then
                Return CType(Current.Session(SessionConstants.TerminationDate), String)
            Else
                Return ""
            End If
        End Function

#End Region

#Region "Remove Termination Date"
        Public Shared Sub removeTerminationDate()
            Current.Session.Remove(SessionConstants.TerminationDate)
        End Sub
#End Region

#End Region








#Region "Set / Get Certificate Expiry Date"
#Region "Set Expiry Date"
        Public Shared Sub setCertificateExpiryDate(ByRef expiryDateInSec As Integer)
            Current.Session(SessionConstants.CertificateExpiryDate) = expiryDateInSec
        End Sub
#End Region

#Region "Get Certificate Expiry Date"
        Public Shared Function getCertificateExpiryDate() As Integer
            If (IsNothing(Current.Session(SessionConstants.CertificateExpiryDate))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.CertificateExpiryDate), Integer)
            End If

        End Function
#End Region

#Region "remove Certificate Expiry Date"
        Public Shared Sub removeCertificateExpiryDate()
            If (Not IsNothing(Current.Session(SessionConstants.CertificateExpiryDate))) Then
                Current.Session.Remove(SessionConstants.CertificateExpiryDate)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Appointment to be Arranged Param"
#Region "Set Appointment to be Arranged Param"
        Public Shared Sub setAptbaParam(ByVal apptbaScheduleParam() As String)
            Current.Session(SessionConstants.ApptbaScheduleParam) = apptbaScheduleParam
        End Sub
#End Region

#Region "Get Appointment to be Arranged Param"
        Public Shared Function getAptbaParam() As String()
            If (IsNothing(Current.Session(SessionConstants.ApptbaScheduleParam))) Then
                Dim apptbaSchedultParam() As String = Nothing
                Return apptbaSchedultParam
            Else
                Return CType(Current.Session(SessionConstants.ApptbaScheduleParam), String())
            End If

        End Function
#End Region

#Region "remove Appointment to be Arranged Param"
        Public Shared Sub removeAptbaParam()
            If (Not IsNothing(Current.Session(SessionConstants.ApptbaScheduleParam))) Then
                Current.Session.Remove(SessionConstants.ApptbaScheduleParam)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Appointment Param in Appointment to be Arranged"
#Region "Set Get Appointment Param in Appointment to be Arranged"
        Public Shared Sub setAddAptbaParam(ByVal addAppointmentParam() As String)
            Current.Session(SessionConstants.AddAppointmentParam) = addAppointmentParam
        End Sub
#End Region

#Region "Get Appointment Param in Appointment to be Arranged"
        Public Shared Function getAddAptbaParam() As String()
            If (IsNothing(Current.Session(SessionConstants.AddAppointmentParam))) Then
                Dim addAppointmentParam() As String = Nothing
                Return addAppointmentParam
            Else
                Return CType(Current.Session(SessionConstants.AddAppointmentParam), String())
            End If

        End Function
#End Region

#Region "remove Appointment Param in Appointment to be Arranged"
        Public Shared Sub removeAddAptbaParam()
            If (Not IsNothing(Current.Session(SessionConstants.AddAppointmentParam))) Then
                Current.Session.Remove(SessionConstants.AddAppointmentParam)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Start Date for Appointment to be arranged"

#Region "Set Appoitment to be arranged Start Date"
        Public Shared Sub setAptbaStartDate(ByRef aptbaStartDate As Date)
            Current.Session(SessionConstants.AptbaStartDate) = aptbaStartDate
        End Sub
#End Region


#Region "get Appoitment to be arranged Start Date"
        Public Shared Function getAptbaStartDate() As Date

            If (IsNothing(Current.Session(SessionConstants.AptbaStartDate))) Then
                Return Date.Today
            Else
                Return CType(Current.Session(SessionConstants.AptbaStartDate), Date)
            End If
        End Function
#End Region


#Region "remove Appoitment to be arranged Start Date"
        Public Sub removeAptbaStartDate()
            If (Not IsNothing(Current.Session(SessionConstants.AptbaStartDate))) Then
                Current.Session.Remove(SessionConstants.AptbaStartDate)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get End Date for Appointment to be arranged"

#Region "Set Appoitment to be arranged End Date"
        Public Shared Sub setAptbaEndDate(ByRef aptbaEndDate As Date)
            Current.Session(SessionConstants.AptbaEndDate) = aptbaEndDate
        End Sub
#End Region


#Region "get Appoitment to be arranged End Date"
        Public Shared Function getAptbaEndDate() As Date

            If (IsNothing(Current.Session(SessionConstants.AptbaEndDate))) Then
                Return Date.Today
            Else
                Return CType(Current.Session(SessionConstants.AptbaEndDate), Date)
            End If
        End Function
#End Region


#Region "remove Appoitment to be arranged End Date"
        Public Sub removeAptbaEndDate()
            If (Not IsNothing(Current.Session(SessionConstants.AptbaEndDate))) Then
                Current.Session.Remove(SessionConstants.AptbaEndDate)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get IsSuccess Check for Appointment to be arranged"

#Region "Set Appoitment to be arranged IsSuccess"
        Public Shared Sub setAptbaIsSuccess(ByRef addIsSuccess As Boolean)
            Current.Session(SessionConstants.AddIsSuccess) = addIsSuccess
        End Sub
#End Region


#Region "get Appoitment to be arranged IsSuccess"
        Public Shared Function getAptbaIsSuccess() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AddIsSuccess))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AddIsSuccess), Boolean)
            End If
        End Function
#End Region

#Region "remove Appoitment to be arranged IsSuccess"
        Public Sub removeAptbaIsSuccess()
            If (Not IsNothing(Current.Session(SessionConstants.AddIsSuccess))) Then
                Current.Session.Remove(SessionConstants.AddIsSuccess)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Calendar pageing clicked Check for Appointment to be arranged"

#Region "set Calendar pageing clicked in Appoitment to be arranged"
        Public Shared Sub setAptbaCalendarPageingClicked(ByRef addCalendarPageingClicked As Boolean)
            Current.Session(SessionConstants.AddCalendarPageingClicked) = addCalendarPageingClicked
        End Sub
#End Region


#Region "get Calendar pageing clicked in Appoitment to be arranged"
        Public Shared Function getAptbaCalendarPageingClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AddCalendarPageingClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AddCalendarPageingClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Calendar pageing clicked in Appoitment to be arranged"
        Public Sub removeAptbaCalendarPageingClicked()
            If (Not IsNothing(Current.Session(SessionConstants.AddCalendarPageingClicked))) Then
                Current.Session.Remove(SessionConstants.AddCalendarPageingClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Add Appointment clicked Check for Appointment to be arranged"

#Region "set Add Appointment clicked in Appoitment to be arranged"
        Public Shared Sub setAptbaAppointmentClicked(ByRef addAppointmentClicked As Boolean)
            Current.Session(SessionConstants.AddAppointmentClicked) = addAppointmentClicked
        End Sub
#End Region


#Region "get Add Appointment clicked in Appoitment to be arranged"
        Public Shared Function getAptbaAppointmentClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AddAppointmentClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AddAppointmentClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Add Appointment clicked in Appoitment to be arranged"
        Public Sub removeAppointmentClicked()
            If (Not IsNothing(Current.Session(SessionConstants.AddAppointmentClicked))) Then
                Current.Session.Remove(SessionConstants.AddAppointmentClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Appointment Arranged Param"
#Region "Set Appointment Arranged Param"
        Public Shared Sub setApaParam(ByVal apaScheduleParam() As String)
            Current.Session(SessionConstants.ApaScheduleParam) = apaScheduleParam
        End Sub
#End Region

#Region "Get Appointment Arranged Param"
        Public Shared Function getApaParam() As String()
            If (IsNothing(Current.Session(SessionConstants.ApaScheduleParam))) Then
                Dim ApaScheduleParam() As String = Nothing
                Return ApaScheduleParam
            Else
                Return CType(Current.Session(SessionConstants.ApaScheduleParam), String())
            End If

        End Function
#End Region

#Region "remove Appointment Arranged Param"
        Public Shared Sub removeApaParam()
            If (Not IsNothing(Current.Session(SessionConstants.ApaScheduleParam))) Then
                Current.Session.Remove(SessionConstants.ApaScheduleParam)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Amend Appointment Param in Appointment Arranged"
#Region "Set Get Amend Appointment Param in Appointment Arranged"
        Public Shared Sub setAmendApaParam(ByVal amendAppointmentParam() As String)
            Current.Session(SessionConstants.AmendAppointmentParam) = amendAppointmentParam
        End Sub
#End Region

#Region "Get Amend Appointment Param in Appointment Arranged"
        Public Shared Function getAmendApaParam() As String()
            If (IsNothing(Current.Session(SessionConstants.AmendAppointmentParam))) Then
                Dim amendAppointmentParam() As String = Nothing
                Return amendAppointmentParam
            Else
                Return CType(Current.Session(SessionConstants.AmendAppointmentParam), String())
            End If

        End Function
#End Region

#Region "remove Amend Appointment Param in Appointment Arranged"
        Public Shared Sub removeAmendApaParam()
            If (Not IsNothing(Current.Session(SessionConstants.AmendAppointmentParam))) Then
                Current.Session.Remove(SessionConstants.AmendAppointmentParam)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Start Date for Appointment arranged"

#Region "Set Appoitment arranged Start Date"
        Public Shared Sub setApaStartDate(ByRef apaStartDate As Date)
            Current.Session(SessionConstants.ApaStartDate) = apaStartDate
        End Sub
#End Region


#Region "get Appoitment arranged Start Date"
        Public Shared Function getApaStartDate() As Date

            If (IsNothing(Current.Session(SessionConstants.ApaStartDate))) Then
                Return Date.Today
            Else
                Return CType(Current.Session(SessionConstants.ApaStartDate), Date)
            End If
        End Function
#End Region


#Region "remove Appoitment arranged Start Date"
        Public Sub removeApaStartDate()
            If (Not IsNothing(Current.Session(SessionConstants.ApaStartDate))) Then
                Current.Session.Remove(SessionConstants.ApaStartDate)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get End Date for Appointment arranged"

#Region "Set Appoitment arranged End Date"
        Public Shared Sub setApaEndDate(ByRef apaEndDate As Date)
            Current.Session(SessionConstants.ApaEndDate) = apaEndDate
        End Sub
#End Region
#Region "get Appoitment arranged End Date"
        Public Shared Function getApaEndDate() As Date

            If (IsNothing(Current.Session(SessionConstants.ApaEndDate))) Then
                Return Date.Today
            Else
                Return CType(Current.Session(SessionConstants.ApaEndDate), Date)
            End If
        End Function
#End Region


#Region "remove Appoitment arranged End Date"
        Public Sub removeApaEndDate()
            If (Not IsNothing(Current.Session(SessionConstants.ApaEndDate))) Then
                Current.Session.Remove(SessionConstants.ApaEndDate)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Amend IsSuccess Check for Appointment arranged"

#Region "Set Appoitment arranged Amend IsSuccess"
        Public Shared Sub setApaAmendIsSuccess(ByRef amendIsSuccess As Boolean)
            Current.Session(SessionConstants.AmendIsSuccess) = amendIsSuccess
        End Sub
#End Region


#Region "get Appoitment arranged Amend IsSuccess"
        Public Shared Function getApaAmendIsSuccess() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AmendIsSuccess))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AmendIsSuccess), Boolean)
            End If
        End Function
#End Region

#Region "remove Appoitment arranged Amend IsSuccess"
        Public Sub removeApaAmendIsSuccess()
            If (Not IsNothing(Current.Session(SessionConstants.AmendIsSuccess))) Then
                Current.Session.Remove(SessionConstants.AmendIsSuccess)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Amend Calendar pageing clicked Check for Appointment arranged"

#Region "set Amend Calendar pageing clicked in Appoitment arranged"
        Public Shared Sub setApaAmendCalendarPageingClicked(ByRef amendCalendarPageingClicked As Boolean)
            Current.Session(SessionConstants.AmendCalendarPageingClicked) = amendCalendarPageingClicked
        End Sub
#End Region


#Region "get Amend Calendar pageing clicked in Appoitment arranged"
        Public Shared Function getApaAmendCalendarPageingClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AmendCalendarPageingClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AmendCalendarPageingClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Amend Calendar pageing clicked in Appoitment arranged"
        Public Sub removeApaAmendCalendarPageingClicked()
            If (Not IsNothing(Current.Session(SessionConstants.AmendCalendarPageingClicked))) Then
                Current.Session.Remove(SessionConstants.AmendCalendarPageingClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Amend Appointment clicked Check for Appointment arranged"

#Region "set Amend Appointment clicked in Appoitment arranged"
        Public Shared Sub setApaAmendAppointmentClicked(ByRef amendAppointmentClicked As Boolean)
            Current.Session(SessionConstants.AmendAppointmentClicked) = amendAppointmentClicked
        End Sub
#End Region

#Region "get Amend Appointment clicked in Appoitment arranged"
        Public Shared Function getApaAmendAppointmentClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.AmendAppointmentClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.AmendAppointmentClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Amend Appointment clicked in Appoitment arranged"
        Public Sub removeApaAmendAppointmentClicked()
            If (Not IsNothing(Current.Session(SessionConstants.AmendAppointmentClicked))) Then
                Current.Session.Remove(SessionConstants.AmendAppointmentClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set /Get and Check Calander Weekly Data Table for Appointment Arranged"
#Region "Set Calander Weekly Data Table for Appointment Arranged"

        Public Shared Sub setAmendCalanderWeeklyDataTable(ByRef amendCalnderweeklyDt As DataTable)
            Current.Session(SessionConstants.amendCalanderWeeklyDT) = amendCalnderweeklyDt
        End Sub

#End Region

#Region "Get Calander Weekly Data Table for Appointment Arranged"

        Public Shared Function getAmendCalanderWeeklyDataTable() As DataTable
            If (IsNothing(Current.Session(SessionConstants.amendCalanderWeeklyDT))) Then
                Return New DataTable
            Else
                Return CType(Current.Session(SessionConstants.amendCalanderWeeklyDT), DataTable)
            End If
        End Function

#End Region

#Region "Remove Calander Weekly Data Table for Appointment Arranged"

        Public Shared Sub removeAmendCalanderWeeklyDataTable()
            Current.Session.Remove(SessionConstants.amendCalanderWeeklyDT)
        End Sub

#End Region

#Region "Check if Calander Weekly Data Table exists in session for Appointment Arranged"

        Public Shared Function isAmendCalanderWeeklyDTSessionExists() As Boolean
            Dim status As Boolean = False
            If Not IsNothing(Current.Session(SessionConstants.amendCalanderWeeklyDT)) Then
                status = True
            End If
            Return status
        End Function

#End Region

#End Region

#Region "Set /Get and Check Calander Weekly Data Table for Appointment to be Arranged"

#Region "Set Calander Weekly Data Table for Appointment to be Arranged"

        Public Shared Sub setAddCalanderWeeklyDataTable(ByRef addCalanderWeeklyDT)
            Current.Session(SessionConstants.addCalanderWeeklyDT) = addCalanderWeeklyDT
        End Sub

#End Region

#Region "Get Calander Weekly Data Table for Appointment to be Arranged"

        Public Shared Function getAddCalanderWeeklyDataTable()
            If (IsNothing(Current.Session(SessionConstants.addCalanderWeeklyDT))) Then
                Return New DataTable
            Else
                Return CType(Current.Session(SessionConstants.addCalanderWeeklyDT), DataTable)
            End If
        End Function

#End Region

#Region "Remove Calander Weekly Data Table for Appointment to be Arranged"

        Public Shared Sub removeAddCalanderWeeklyDataTable()
            Current.Session.Remove(SessionConstants.addCalanderWeeklyDT)
        End Sub

#End Region

#Region "Check if Calander Weekly Data Table exists in session for Appointment to be Arranged"

        Public Shared Function isAddCalanderWeeklyDTSessionExists() As Boolean
            Dim status As Boolean = False
            If Not IsNothing(Current.Session(SessionConstants.addCalanderWeeklyDT)) Then
                status = True
            End If
            Return status
        End Function

#End Region

#End Region

#Region "Set / Get Tenancy clicked Check for Appointment arranged"

#Region "set Tenancy clicked in Appoitment arranged"
        Public Shared Sub setTenancyClicked(ByRef TenancyClicked As Boolean)
            Current.Session(SessionConstants.TenancyClicked) = TenancyClicked
        End Sub
#End Region

#Region "get Tenancy clicked in Appoitment arranged"
        Public Shared Function getTenancyClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.TenancyClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.TenancyClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Tenancy clicked in Appoitment arranged"
        Public Sub removeTenancyClicked()
            If (Not IsNothing(Current.Session(SessionConstants.TenancyClicked))) Then
                Current.Session.Remove(SessionConstants.TenancyClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get Tenancy clicked Check for Appointment to be arranged"

#Region "set Tenancy clicked in Appoitment to be arranged"
        Public Shared Sub setAptbaTenancyClicked(ByRef AptbaTenancyClicked As Boolean)
            Current.Session(SessionConstants.aptbaTenancyClicked) = AptbaTenancyClicked
        End Sub
#End Region

#Region "get Tenancy clicked in Appoitment to be arranged"
        Public Shared Function getAptbaTenancyClicked() As Boolean

            If (IsNothing(Current.Session(SessionConstants.aptbaTenancyClicked))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.aptbaTenancyClicked), Boolean)
            End If
        End Function
#End Region

#Region "remove Tenancy clicked in Appoitment to be arranged"
        Public Sub removeAptbaTenancyClicked()
            If (Not IsNothing(Current.Session(SessionConstants.aptbaTenancyClicked))) Then
                Current.Session.Remove(SessionConstants.aptbaTenancyClicked)
            End If

        End Sub
#End Region

#End Region

#Region "Set/Get/Remove weekly calendar (Read only calendar) Start Date and End Date"

#Region "Set/Get/Remove weekly calendar (Read only calendar) Start Date"

#Region "Set weekly calendar (Read only calendar) Start Date"
        Public Shared Sub setWeeklyCalendarStartDate(ByRef weeklyCalendarStartDate As Date)
            Current.Session(SessionConstants.weeklyCalendarStartDate) = weeklyCalendarStartDate
        End Sub
#End Region

#Region "Get weekly calendar (Read only calendar) Start Date"
        Public Shared Function getWeeklyCalendarStartDate() As Date
            If (IsNothing(Current.Session(SessionConstants.weeklyCalendarStartDate))) Then
                Return Date.Today
            Else
                Return CType(Current.Session(SessionConstants.weeklyCalendarStartDate), Date)
            End If
        End Function
#End Region

#Region "Remove weekly calendar (Read only calendar) Start Date"
        Public Shared Sub removeWeeklyCalendarStartDate()
            If Not IsNothing(Current.Session(SessionConstants.weeklyCalendarStartDate)) Then
                Current.Session.Remove(SessionConstants.weeklyCalendarStartDate)
            End If
        End Sub
#End Region

        '#End Region "Set/Get/Remove weekly calendar (Read only calendar) Start Date"
#End Region

#Region "Set/Get/Remove weekly calendar (Read only calendar) End Date"

#Region "Set weekly calendar (Read only calendar) End Date"
        Public Shared Sub setWeeklyCalendarEndDate(ByRef weeklyCalendarEndDate As Date)
            Current.Session(SessionConstants.weeklyCalendarEndDate) = weeklyCalendarEndDate
        End Sub
#End Region

#Region "Get weekly calendar (Read only calendar) End Date"
        Public Shared Function getWeeklyCalendarEndDate()
            If IsNothing(Current.Session(SessionConstants.weeklyCalendarEndDate)) Then
                Return Date.Today.AddDays(7)
            Else
                Return CType(Current.Session(SessionConstants.weeklyCalendarEndDate), Date)
            End If
        End Function
#End Region

#Region "Remove weekly calendar (Read only calendar) End Date"
        Public Shared Sub removeWeeklyCalendarEndtDate()
            If Not IsNothing(Current.Session(SessionConstants.weeklyCalendarEndDate)) Then
                Current.Session.Remove(SessionConstants.weeklyCalendarEndDate)
            End If
        End Sub
#End Region

        '#End Region "Set/Get/Remove weekly calendar (Read only calendar) End Date"
#End Region

        '#End Region "Set/Get/Remove weekly calendar (Read only calendar) Start Date and End Date"
#End Region

#Region "Set/Get/Remove Monthly calendar (Read only calendar) Start Date and End Date"

#Region "Set/Get/Remove Monthly calendar (Read only calendar) Start Date"

#Region "Set Monthly calendar (Read only calendar) Start Date"
        Public Shared Sub setMonthlyCalendarStartDate(ByRef monthlyCalendarStartDate As Date)
            Current.Session(SessionConstants.monthlyCalendarStartDate) = monthlyCalendarStartDate
        End Sub
#End Region

#Region "Get Monthly calendar (Read only calendar) Start Date"
        Public Shared Function getMonthlyCalendarStartDate()
            If (IsNothing(Current.Session(SessionConstants.monthlyCalendarStartDate))) Then
                Return DateSerial(Date.Today.Year, Date.Today.Month, 1)
            Else
                Return CType(Current.Session(SessionConstants.monthlyCalendarStartDate), Date)
            End If
        End Function
#End Region

#Region "Remove Monthly calendar (Read only calendar) Start Date"
        Public Shared Sub removeMonthlyCalendarStartDate()
            If Not IsNothing(Current.Session(SessionConstants.monthlyCalendarStartDate)) Then
                Current.Session.Remove(SessionConstants.monthlyCalendarStartDate)
            End If
        End Sub
#End Region

        '#End Region "Set/Get/Remove Monthly calendar (Read only calendar) Start Date"
#End Region

#Region "Set/Get/Remove Monthly calendar (Read only calendar) End Date"

#Region "Set Monthly calendar (Read only calendar) End Date"
        Public Shared Sub setMonthlyCalendarEndDate(ByRef monthlyCalendarEndDate As Date)
            Current.Session(SessionConstants.monthlyCalendarEndDate) = monthlyCalendarEndDate
        End Sub
#End Region

#Region "Get Monthly calendar (Read only calendar) End Date"
        Public Shared Function getMonthlyCalendarEndDate()
            If (IsNothing(Current.Session(SessionConstants.monthlyCalendarEndDate))) Then
                Return DateSerial(Date.Today.Year, Date.Today.Month, Date.DaysInMonth(Date.Today.Year, Date.Today.Month))
            Else
                Return CType(Current.Session(SessionConstants.monthlyCalendarEndDate), Date)
            End If
        End Function
#End Region

#Region "Remove Monthly calendar (Read only calendar) End Date"
        Public Shared Sub removeMonthlyCalendarEndDate()
            If Not IsNothing(Current.Session(SessionConstants.monthlyCalendarEndDate)) Then
                Current.Session.Remove(SessionConstants.monthlyCalendarEndDate)
            End If
        End Sub
#End Region

        '#End Region "Set/Get/Remove Monthly calendar (Read only calendar) End Date"
#End Region

        '#End Region "Set/Get/Remove Monthly calendar (Read only calendar) Start Date and End Date"
#End Region

#Region "Set/Get/Remove Monthly calendar (Read only calendar) Month Days"

#Region "Set Monthly calendar (Read only calendar) Month Days"
        Public Shared Sub setMonthlyCalendarMonthDays(ByRef monthlyCalendarMonthDays As UInteger)
            Current.Session(SessionConstants.monthlyCalendarMonthDays) = monthlyCalendarMonthDays
        End Sub
#End Region

#Region "Get Monthly calendar (Read only calendar) Month Days"
        Public Shared Function getMonthlyCalendarMonthDays()
            If (IsNothing(Current.Session(SessionConstants.monthlyCalendarMonthDays))) Then
                Return Date.DaysInMonth(Date.Today.Year, Date.Today.Month)
            Else
                Return CType(Current.Session(SessionConstants.monthlyCalendarMonthDays), UInteger)
            End If
        End Function
#End Region

#Region "Remove Monthly calendar (Read only calendar) Month Days"
        Public Shared Sub removeMonthlyCalendarMonthDays(ByRef monthlyCalendarMonthDays As UInteger)
            If Not IsNothing(Current.Session(SessionConstants.monthlyCalendarMonthDays)) Then
                Current.Session.Remove(SessionConstants.monthlyCalendarMonthDays)
            End If
        End Sub
#End Region

        '#End Region "Set/Get/Remove Monthly calendar (Read only calendar) Days"
#End Region

#Region "Set / Get Avaialble Operative Ds"

#Region "Set  Avaialble Operative Ds"

        Public Shared Sub setAvailableOperativesDs(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionConstants.AvailableOperativesDs) = availableOperativesDs
        End Sub

#End Region

#Region "Get  Avaialble Operative Ds"

        Public Shared Function getAvailableOperativesDs()
            If (IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.AvailableOperativesDs), DataSet)
            End If
        End Function

#End Region

#Region "remove  Avaialble Operative Ds"

        Public Shared Sub removeAvailableOperativeDs()
            If (Not IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                Current.Session.Remove(SessionConstants.AvailableOperativesDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Property Distance"

#Region "Set Property Distance"

        Public Shared Sub setPropertyDistance(ByRef propertyDistance As Dictionary(Of String, PropertyDistanceBO))
            Current.Session(SessionConstants.PropertyDistance) = propertyDistance
        End Sub

#End Region

#Region "Get Property Distance"

        Public Shared Function getPropertyDistance() As Dictionary(Of String, PropertyDistanceBO)
            If (IsNothing(Current.Session(SessionConstants.PropertyDistance))) Then
                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                Dim propertyDistanceDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                Return propertyDistanceDictionary
            Else
                Return CType(Current.Session(SessionConstants.PropertyDistance), Dictionary(Of String, PropertyDistanceBO))
            End If
        End Function

#End Region

#Region "Remove Property Distance"

        Public Shared Sub removePropertyDistance()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyDistance))) Then
                Current.Session.Remove(SessionConstants.PropertyDistance)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Appointment Slots"

#Region "Set Appointment Slots"

        Public Shared Sub setAppointmentSlots(ByRef appointmentSlots As AppointmentSlotsDtBO)
            'Make a copy of the AppointmentSlotsDtBO by copy constructor
            'Copy is made to avoid the effects of changes to BO datatable after saving in session.
            Dim sessionAppointmentSlots As New AppointmentSlotsDtBO(appointmentSlots)
            Current.Session(SessionConstants.AppointmentSlots) = sessionAppointmentSlots
        End Sub

#End Region

#Region "Get Appointment Slots"

        Public Shared Function getAppointmentSlots() As AppointmentSlotsDtBO
            If (IsNothing(Current.Session(SessionConstants.AppointmentSlots))) Then
                Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                Return appointmentSlotsDtBo
            Else
                Return CType(Current.Session(SessionConstants.AppointmentSlots), AppointmentSlotsDtBO)
            End If
        End Function

#End Region

#Region "Remove Appointment Slots"

        Public Shared Sub removeAppointmentSlots()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentSlots))) Then
                Current.Session.Remove(SessionConstants.AppointmentSlots)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Selected Property Scheduling Bo"

#Region "Set Selected Property Scheduling Bo"

        Public Shared Sub SetSelectedPropertySchedulingBo(ByRef selPropSchedulingBo As SelectedPropertySchedulingBO)
            Current.Session(SessionConstants.SelectedPropertySchedulingBo) = selPropSchedulingBo
        End Sub

#End Region

#Region "Get Property Scheduling Bo"

        Public Shared Function GetSelectedPropertySchedulingBo() As SelectedPropertySchedulingBO
            If (IsNothing(Current.Session(SessionConstants.SelectedPropertySchedulingBo))) Then
                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                Return selPropSchedulingBo
            Else
                Return CType(Current.Session(SessionConstants.SelectedPropertySchedulingBo), SelectedPropertySchedulingBO)
            End If
        End Function

#End Region

#Region "Remove Selected Property Scheduling Bo"

        Public Shared Sub RemoveSelectedPropertySchedulingBo()
            If (Not IsNothing(Current.Session(SessionConstants.SelectedPropertySchedulingBo))) Then
                Current.Session.Remove(SessionConstants.SelectedPropertySchedulingBo)
            End If
        End Sub
#End Region

#End Region


#Region "Set / Get Alternative Scheduling Header List"

#Region "Set Alternative Scheduling Header List"

        Public Shared Sub SetAlternativeSchedulingHeaderBoList(ByRef alternativeSchedulingHeaderBo As List(Of OilAlternativeSchedullingSlotHeader))
            Current.Session(SessionConstants.AlternativeSchedulingHeaderBoList) = alternativeSchedulingHeaderBo
        End Sub

#End Region

#Region "Get Alternative Scheduling Header List"

        Public Shared Function GetAlternativeSchedulingHeaderBoList() As List(Of OilAlternativeSchedullingSlotHeader)
            If (IsNothing(Current.Session(SessionConstants.AlternativeSchedulingHeaderBoList))) Then
                Dim alternativeSchedulingHeaderBoList = New List(Of OilAlternativeSchedullingSlotHeader)
                Return alternativeSchedulingHeaderBoList
            Else
                Return CType(Current.Session(SessionConstants.AlternativeSchedulingHeaderBoList), List(Of OilAlternativeSchedullingSlotHeader))
            End If
        End Function

#End Region

#Region "Remove Alternative Scheduling Header List"

        Public Shared Sub RemoveAlternativeSchedulingHeaderBoList()
            If (Not IsNothing(Current.Session(SessionConstants.AlternativeSchedulingHeaderBoList))) Then
                Current.Session.Remove(SessionConstants.AlternativeSchedulingHeaderBoList)
            End If
        End Sub
#End Region

#End Region


#Region "Set / Get List of Alternative Scheduling Bo"

#Region "Set Selected Alternative Scheduling Bo"

        Public Shared Sub SetAlternativeSchedulingBo(ByRef selPropSchedulingBo As AlternativeSchedullingSlot)
            Current.Session(SessionConstants.AlternativeSchedulingBo) = selPropSchedulingBo
        End Sub

#End Region

#Region "Get lternative Scheduling Bo"

        Public Shared Function GetAlternativeSchedulingBo() As AlternativeSchedullingSlot
            If (IsNothing(Current.Session(SessionConstants.AlternativeSchedulingBo))) Then
                Dim selPropSchedulingBo As AlternativeSchedullingSlot = New AlternativeSchedullingSlot
                Return selPropSchedulingBo
            Else
                Return CType(Current.Session(SessionConstants.AlternativeSchedulingBo), AlternativeSchedullingSlot)
            End If
        End Function

#End Region

#Region "Remove Alternative Scheduling Bo"

        Public Shared Sub RemoveAlternativeSchedulingBo()
            If (Not IsNothing(Current.Session(SessionConstants.AlternativeSchedulingBo))) Then
                Current.Session.Remove(SessionConstants.AlternativeSchedulingBo)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Property Duration Dictionary"

#Region "Set Property Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to save Property Duration Dictionary in session
        ''' </summary>
        ''' <param name="objPropertyDuration"></param>
        ''' <remarks></remarks>
        Public Shared Sub SetPropertyDurationDict(ByRef objPropertyDuration As Dictionary(Of String, Double))
            Current.Session(SessionConstants.PropertyDurationDict) = objPropertyDuration
        End Sub

#End Region

#Region "Get Property Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to get Property Duration Dictionary from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetPropertyDurationDict() As Dictionary(Of String, Double)
            If (IsNothing(Current.Session(SessionConstants.PropertyDurationDict))) Then
                Return New Dictionary(Of String, Double)
            Else
                Return CType(Current.Session.Item(SessionConstants.PropertyDurationDict), Dictionary(Of String, Double))
            End If
        End Function

#End Region

#Region "Remove Property Duration Dictionary"
        ''' <summary>
        ''' This function shall be used to remove Property Duration Dictionary from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub RemovePropertyDurationDict()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyDurationDict))) Then
                Current.Session.Remove(SessionConstants.PropertyDurationDict)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Appliance Servicing Id"

#Region "Set Appliance Servicing Id"

        Public Shared Sub setApplianceServicingId(ByRef applianceServicingId As Integer)
            Current.Session(SessionConstants.ApplianceServicingId) = applianceServicingId
        End Sub

#End Region

#Region "Get Appliance Servicing Id"

        Public Shared Function getApplianceServicingId() As Integer
            If (IsNothing(Current.Session(SessionConstants.ApplianceServicingId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ApplianceServicingId), Integer)
            End If
        End Function

#End Region

#Region "Remove Appliance Servicing Id"

        Public Shared Sub removeApplianceServicingId()
            If (Not IsNothing(Current.Session(SessionConstants.ApplianceServicingId))) Then
                Current.Session.Remove(SessionConstants.ApplianceServicingId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Property Id"

#Region "Set Property Id"

        Public Shared Sub setPropertyId(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyId()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Number"

#Region "Set Job Sheet Number"

        Public Shared Sub setJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.JobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.JobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Attribute Detail DataSet"
        ''' <summary>
        ''' This key will be used to save the Component detail dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared AttributeDetailDataSet As String = "ConponentDetailDataSet"
#Region "Set Attribute Detail DataSet"
        ''' <summary>
        ''' This function shall be used to save Attribute detail data set in session
        ''' </summary>
        ''' <param name="AttributeDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setAttributeDetailDataSet(ByRef AttributeDataSet As DataSet)
            Current.Session(AttributeDetailDataSet) = AttributeDataSet
        End Sub

#End Region

#Region "Get Attribute Detail DataSet"
        ''' <summary>
        ''' This function shall be used to get Attribute detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAttributeDetailDataSet()
            If (IsNothing(Current.Session(AttributeDetailDataSet))) Then
                Dim recallDataSet As New DataSet
                Return recallDataSet
            Else
                Return CType(Current.Session(AttributeDetailDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Attribute Detail DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Attribute detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeAttributeDetailDataSet()
            If (Not IsNothing(Current.Session(AttributeDetailDataSet))) Then
                Current.Session.Remove(AttributeDetailDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Tree Item Id"

#Region "Set Tree Item Id"

        Public Shared Sub setTreeItemId(ByRef TreeItemId As Integer)
            Current.Session(SessionConstants.treeItemId) = TreeItemId
        End Sub

#End Region

#Region "Get Tree Item Id"

        Public Shared Function getTreeItemId() As Integer
            If (IsNothing(Current.Session(SessionConstants.treeItemId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.treeItemId), Integer)
            End If
        End Function

#End Region

#Region "Remove Tree Item Id"

        Public Shared Sub removeTreeItemId()
            If (Not IsNothing(Current.Session(SessionConstants.treeItemId))) Then
                Current.Session.Remove(SessionConstants.treeItemId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get parameter value Id"

#Region "Set parameter value Id"

        Public Shared Sub setValueId(ByRef valueId As Integer)
            Current.Session(SessionConstants.parameterValueId) = valueId
        End Sub

#End Region

#Region "Get parameter value Id"

        Public Shared Function getValueId() As Integer
            If (IsNothing(Current.Session(SessionConstants.parameterValueId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.parameterValueId), Integer)
            End If
        End Function

#End Region

#Region "Remove parameter value Id"

        Public Shared Sub removeValueId()
            If (Not IsNothing(Current.Session(SessionConstants.parameterValueId))) Then
                Current.Session.Remove(SessionConstants.parameterValueId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get parameter Control Id"

#Region "Set parameter Control Id"

        Public Shared Sub setControlId(ByRef controlId As String)
            Current.Session(SessionConstants.controlId) = controlId
        End Sub

#End Region

#Region "Get parameter value Id"

        Public Shared Function getControlId() As String
            If (IsNothing(Current.Session(SessionConstants.controlId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.controlId), String)
            End If
        End Function

#End Region

#Region "Remove parameter value Id"

        Public Shared Sub removeControlId()
            If (Not IsNothing(Current.Session(SessionConstants.controlId))) Then
                Current.Session.Remove(SessionConstants.controlId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Accounting Control Id"

#Region "Set Accounting Control Id"

        Public Shared Sub setAccountingControlId(ByRef controlId As String)
            Current.Session(SessionConstants.accountingControlId) = controlId
        End Sub

#End Region

#Region "Get parameter value Id"

        Public Shared Function getAccountingControlId() As String
            If (IsNothing(Current.Session(SessionConstants.accountingControlId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.accountingControlId), String)
            End If
        End Function

#End Region

#Region "Remove parameter value Id"

        Public Shared Sub removeAccountingControlId()
            If (Not IsNothing(Current.Session(SessionConstants.accountingControlId))) Then
                Current.Session.Remove(SessionConstants.accountingControlId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get parameter previous Value Id"

#Region "Set previous Value Id"

        Public Shared Sub setpreviousValueId(ByRef lifeCycleRow As String)
            Current.Session(SessionConstants.previousValueId) = lifeCycleRow
        End Sub

#End Region

#Region "Get parameter previous Value Id"

        Public Shared Function getpreviousValueId() As String
            If (IsNothing(Current.Session(SessionConstants.previousValueId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.previousValueId), String)
            End If
        End Function

#End Region

#Region "Remove parameter previous Value Id"

        Public Shared Sub removepreviousValueId()
            If (Not IsNothing(Current.Session(SessionConstants.previousValueId))) Then
                Current.Session.Remove(SessionConstants.previousValueId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Core And Out Of Hours Detail DataSet"
        ''' <summary>
        ''' This key will be used to save the Core And Out Of Hours Detail dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared CoreAndOutOfHoursDataSet As String = "CoreAndOutOfHoursDataSet"

#Region "Set Core And Out Of Hours Detail DataSet"
        ''' <summary>
        ''' This function shall be used to save Core And Out Of Hours Detail data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setCoreAndOutOfHoursDataSet(ByRef ds As DataSet)
            Current.Session(CoreAndOutOfHoursDataSet) = ds
        End Sub

#End Region

#Region "Get Core And Out Of Hours Detail DataSet"
        ''' <summary>
        ''' This function shall be used to get Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getCoreAndOutOfHoursDataSet()
            If (IsNothing(Current.Session(CoreAndOutOfHoursDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(CoreAndOutOfHoursDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Core And Out Of Hours Detail DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeCoreAndOutOfHoursDataSet()
            If (Not IsNothing(Current.Session(CoreAndOutOfHoursDataSet))) Then
                Current.Session.Remove(CoreAndOutOfHoursDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Selected Resource Employee Id"

#Region "Set User Selected Resource Employee Id"
        Public Shared Sub setSelectedResourceEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.SelectedResourceEmployeeId) = employeeId
        End Sub
#End Region


#Region "get User Selected Resource Employee Id"
        Public Shared Function getSelectedResourceEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.SelectedResourceEmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SelectedResourceEmployeeId), Integer)
            End If
        End Function
#End Region


#Region "remove Selected Resource Employee Id"
        Public Shared Sub removeSelectedResourceEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.SelectedResourceEmployeeId))) Then
                Current.Session.Remove(SessionConstants.SelectedResourceEmployeeId)
            End If

        End Sub
#End Region

#End Region

#Region "Out Of Hours Deleted List Set and Remove Method/Function(s)"

#Region "Set Out Of Hours Deleted List"
        ''' <summary>
        ''' This function shall be used to save Out Of Hours Deleted List data set in session
        ''' </summary>
        ''' <param name="deletedList"></param>
        ''' <remarks></remarks>
        Public Shared Sub setOutOfHoursDeletedList(ByRef deletedList As DataTable)
            Current.Session(SessionConstants.OutOfHoursDeletedList) = deletedList
        End Sub

#End Region

#Region "Get Out Of Hours Deleted List"
        ''' <summary>
        ''' This function shall be used to get Out Of Hours Deleted List from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getOutOfHoursDeletedList() As DataTable
            If (IsNothing(Current.Session(SessionConstants.OutOfHoursDeletedList))) Then
                Dim table As New DataTable
                table.Columns.Add(ApplicationConstants.OutOfHoursIdCol, GetType(Int32))
                Return table
            Else
                Return CType(Current.Session(SessionConstants.OutOfHoursDeletedList), DataTable)
            End If
        End Function

#End Region

#Region "remove Out Of Hours Deleted List"
        ''' <summary>
        ''' This function shall be used to remove Out Of Hours Deleted List to Delete List from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeOutOfHoursDeletedList()
            If (Not IsNothing(Current.Session(SessionConstants.OutOfHoursDeletedList))) Then
                Current.Session.Remove(SessionConstants.OutOfHoursDeletedList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Page Access DataSet"
        ''' <summary>
        ''' This key will be used to save the Page Access DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PageAccessDataSet As String = "PageAccessDataSet"

#Region "Set Page Access DataSet"
        ''' <summary>
        ''' This function shall be used Page Access DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPageAccessDataSet(ByRef ds As DataSet)
            Current.Session(PageAccessDataSet) = ds
        End Sub

#End Region

#Region "Get Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to get Page Access DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPageAccessDataSet()
            If (IsNothing(Current.Session(PageAccessDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PageAccessDataSet), DataSet)
            End If
        End Function

#End Region

#Region "Remove Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePageAccessDataSet()
            If (Not IsNothing(Current.Session(PageAccessDataSet))) Then
                Current.Session.Remove(PageAccessDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get works Required Row Id"

#Region "Set works Required Row Id"

        Public Shared Sub setWorksRequiredRowId(ByRef worksRequiredRowId As String)
            Current.Session(SessionConstants.worksRequiredRowId) = worksRequiredRowId
        End Sub

#End Region

#Region "Get works Required Row Id"

        Public Shared Function getWorksRequiredRowId() As String
            If (IsNothing(Current.Session(SessionConstants.worksRequiredRowId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.worksRequiredRowId), String)
            End If
        End Function

#End Region

#Region "Remove works Required Row Id"

        Public Shared Sub removeWorksRequiredRowId()
            If (Not IsNothing(Current.Session(SessionConstants.worksRequiredRowId))) Then
                Current.Session.Remove(SessionConstants.worksRequiredRowId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get ExistingAppointments For Scheduling Maps"

#Region "Get ExistingAppointments For Scheduling Map"
        Public Shared Function getExistingAppointmentsForSchedulingMap() As DataTable
            Return CType(Current.Session(SessionConstants.existingAppointmentForSchedulingMap), DataTable)
        End Function
#End Region

#Region "Set ExistingAppointments For Scheduling Maps"
        Public Shared Sub setExistingAppointmentsForSchedulingMap(dataTable As DataTable)
            Current.Session(SessionConstants.existingAppointmentForSchedulingMap) = dataTable
        End Sub
#End Region

#Region "Remove ExistingAppointments For Scheduling Map(s)"
        Public Shared Sub removeExistingAppointmentsForSchedulingMap()
            Current.Session.Remove(SessionConstants.existingAppointmentForSchedulingMap)
        End Sub
#End Region

#End Region

#Region "Set / Get Component Cycle"

#Region "Set  Appointment Component Id"

        Public Shared Sub setComponentCycle(ByRef cycle As Integer)
            Current.Session(SessionConstants.cycle) = cycle
        End Sub

#End Region

#Region "Get  Component Cycle"

        Public Shared Function getComponentCycle() As Integer
            If (IsNothing(Current.Session(SessionConstants.cycle))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.cycle), Integer)
            End If
        End Function

#End Region

#Region "Remove  Component Cycle"

        Public Shared Sub removeComponentCycle()
            If (Not IsNothing(Current.Session(SessionConstants.cycle))) Then
                Current.Session.Remove(SessionConstants.cycle)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get value Life Cycle"

#Region "Set  value Life Cycle"

        Public Shared Sub setValueLifeCycle(ByRef cycle As Boolean)
            Current.Session(SessionConstants.valueLifeCycle) = cycle
        End Sub

#End Region

#Region "Get value Life Cycle"

        Public Shared Function getValueLifeCycle() As Boolean
            If (IsNothing(Current.Session(SessionConstants.valueLifeCycle))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.valueLifeCycle), Boolean)
            End If
        End Function

#End Region

#Region "Remove  value Life Cycle"

        Public Shared Sub removeValueLifeCycle()
            If (Not IsNothing(Current.Session(SessionConstants.valueLifeCycle))) Then
                Current.Session.Remove(SessionConstants.valueLifeCycle)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get DdlChangeEvent"

#Region "Set  DdlChangeEvent"

        Public Shared Sub setIsDdlChangeEvent(ByRef cycle As Boolean)
            Current.Session(SessionConstants.isDdlChangeEvent) = cycle
        End Sub

#End Region

#Region "Get DdlChangeEvent"

        Public Shared Function getIsDdlChangeEvent() As Boolean
            If (IsNothing(Current.Session(SessionConstants.isDdlChangeEvent))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.isDdlChangeEvent), Boolean)
            End If
        End Function

#End Region

#Region "Remove  DdlChangeEvent"

        Public Shared Sub removeIsDdlChangeEvent()
            If (Not IsNothing(Current.Session(SessionConstants.isDdlChangeEvent))) Then
                Current.Session.Remove(SessionConstants.isDdlChangeEvent)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get plannedComponentId"

#Region "Set  plannedComponentId"

        Public Shared Sub setPlannedComponentId(ByRef plannedComponentId As Integer)
            Current.Session(SessionConstants.plannedComponentId) = plannedComponentId
        End Sub

#End Region

#Region "Get plannedComponentId"

        Public Shared Function getPlannedComponentId() As Integer
            If (IsNothing(Current.Session(SessionConstants.plannedComponentId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.plannedComponentId), Integer)
            End If
        End Function

#End Region

#Region "Remove  plannedComponentId"

        Public Shared Sub removePlannedComponentId()
            If (Not IsNothing(Current.Session(SessionConstants.plannedComponentId))) Then
                Current.Session.Remove(SessionConstants.plannedComponentId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get attributrTypeId"

#Region "Set  attributrTypeId"

        Public Shared Sub setAttributrTypeId(ByRef attributrTypeId As Integer)
            Current.Session(SessionConstants.attributrTypeId) = attributrTypeId
        End Sub

#End Region

#Region "Get attributrTypeId"

        Public Shared Function getAttributrTypeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.attributrTypeId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.attributrTypeId), Integer)
            End If
        End Function

#End Region

#Region "Remove  attributrTypeId"

        Public Shared Sub removeAttributrTypeId()
            If (Not IsNothing(Current.Session(SessionConstants.attributrTypeId))) Then
                Current.Session.Remove(SessionConstants.attributrTypeId)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get is Mapped"

#Region "Set  is Mapped"

        Public Shared Sub setIsMapped(ByRef isMapped As Boolean)
            Current.Session(SessionConstants.isMapped) = isMapped
        End Sub

#End Region

#Region "Get is Mapped"

        Public Shared Function getIsMapped() As Boolean
            If (IsNothing(Current.Session(SessionConstants.isMapped))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.isMapped), Boolean)
            End If
        End Function

#End Region

#Region "Remove  is Mapped"

        Public Shared Sub removeIsMapped()
            If (Not IsNothing(Current.Session(SessionConstants.isMapped))) Then
                Current.Session.Remove(SessionConstants.isMapped)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get isLifeCycleMapped"

#Region "Set  isLifeCycleMapped"

        Public Shared Sub setIsLifeCycleMapped(ByRef isLifeCycleMapped As Integer)
            Current.Session(SessionConstants.isLifeCycleMapped) = isLifeCycleMapped
        End Sub

#End Region

#Region "Get isLifeCycleMapped"

        Public Shared Function getIsLifeCycleMapped() As Integer
            If (IsNothing(Current.Session(SessionConstants.isLifeCycleMapped))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.isLifeCycleMapped), Integer)
            End If
        End Function

#End Region

#Region "Remove  isLifeCycleMapped"

        Public Shared Sub removeIsLifeCycleMapped()
            If (Not IsNothing(Current.Session(SessionConstants.isLifeCycleMapped))) Then
                Current.Session.Remove(SessionConstants.isLifeCycleMapped)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get isAccountingMapped"

#Region "Set  isAccountingMapped"

        Public Shared Sub setIsAccountingMapped(ByRef isAccountingMapped As Integer)
            Current.Session(SessionConstants.isAccountingMapped) = isAccountingMapped
        End Sub

#End Region

#Region "Get isAccountingMapped"

        Public Shared Function getIsAccountingMapped() As Integer
            If (IsNothing(Current.Session(SessionConstants.isAccountingMapped))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.isAccountingMapped), Integer)
            End If
        End Function

#End Region

#Region "Remove  isAccountingMapped"

        Public Shared Sub removeIsAccountingMapped()
            If (Not IsNothing(Current.Session(SessionConstants.isAccountingMapped))) Then
                Current.Session.Remove(SessionConstants.isAccountingMapped)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get isWorkSatisfactory"

#Region "Set isWorkSatisfactory"

        Public Shared Sub setIsWorkSatisfactory(ByRef isWorkSatisfactory As Boolean)
            Current.Session(SessionConstants.isWorkSatisfactory) = isWorkSatisfactory
        End Sub

#End Region

#Region "Get isWorkSatisfactory"

        Public Shared Function getIsWorkSatisfactory() As Boolean
            If (IsNothing(Current.Session(SessionConstants.isWorkSatisfactory))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.isWorkSatisfactory), Boolean)
            End If
        End Function

#End Region

#Region "Remove  isWorkSatisfactory"

        Public Shared Sub removeIsWorkSatisfactory()
            If (Not IsNothing(Current.Session(SessionConstants.isWorkSatisfactory))) Then
                Current.Session.Remove(SessionConstants.isWorkSatisfactory)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Item Detail DataSet"
        ''' <summary>
        ''' This key will be used to save Item Detail dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ItemDetailDataSet As String = "ItemDetailDataSet"

#Region "Set Item Detail DataSet"
        ''' <summary>
        ''' This function shall be used to save Items Detail data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setItemDetailDataSet(ByRef ds As DataSet)
            Current.Session(ItemDetailDataSet) = ds
        End Sub

#End Region

#Region "Get Item Detail DataSet"
        ''' <summary>
        ''' This function shall be used to get Item Detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getItemDetailDataSet()
            If (IsNothing(Current.Session(ItemDetailDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(ItemDetailDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Item Detail DataSet"
        ''' <summary>
        ''' This function shall be used to remove Item Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeItemDetailDataSet()
            If (Not IsNothing(Current.Session(ItemDetailDataSet))) Then
                Current.Session.Remove(ItemDetailDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  tree Item Name "
        ''' <summary>
        ''' This key will be used to save tree Item Name 
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared treeItemName As String = "treeItemName"

#Region "Set tree Item Name "
        ''' <summary>
        ''' This function shall be used to save tree Item Name in session
        ''' </summary>
        ''' <param name="treeItemId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTreeItemName(ByRef treeItemId As String)
            Current.Session(treeItemName) = treeItemId
        End Sub

#End Region

#Region "Get  tree Item Name t"
        ''' <summary>
        ''' This function shall be used to get tree Item Name from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTreeItemName()
            If (IsNothing(Current.Session(treeItemName))) Then
                Return Nothing
            Else
                Return CType(Current.Session(treeItemName), String)
            End If
        End Function

#End Region

#Region "remove  tree Item Name "
        ''' <summary>
        ''' This function shall be used to remove  tree Item Name  from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTreeItemName()
            If (Not IsNothing(Current.Session(treeItemName))) Then
                Current.Session.Remove(treeItemName)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Heating Mapping Id "
        ''' <summary>
        ''' This key will be used to save tree Item Name 
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared heatingMappingId As String = "heatingMappingId"

#Region "Set Heating Mapping Id "
        ''' <summary>
        ''' This function shall be used to save tree Item Name in session
        ''' </summary>
        ''' <param name="heatingMappId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setHeatingMappingId(ByRef heatingMappId As String)
            Current.Session(heatingMappingId) = heatingMappId
        End Sub

#End Region

#Region "Get  Heating Mapping Id"
        ''' <summary>
        ''' This function shall be used to get tree Item Name from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getHeatingMappingId()
            If (IsNothing(Current.Session(heatingMappingId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(heatingMappingId), String)
            End If
        End Function

#End Region

#Region "remove  Heating Mapping Id"
        ''' <summary>
        ''' This function shall be used to remove  tree Item Name  from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeHeatingMappingId()
            If (Not IsNothing(Current.Session(heatingMappingId))) Then
                Current.Session.Remove(heatingMappingId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Heating Mapping  "
        ''' <summary>
        ''' This key will be used to save tree Item Name 
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared heatingMapping As String = "heatingMapping"

#Region "Set Heating Mapping  "
        ''' <summary>
        ''' This function shall be used to save tree Item Name in session
        ''' </summary>
        ''' <param name="heatingMappId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setheatingMapping(ByRef heatingMappId As String)
            Current.Session(heatingMapping) = heatingMappId
        End Sub

#End Region

#Region "Get  Heating Mapping "
        ''' <summary>
        ''' This function shall be used to get tree Item Name from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getheatingMapping()
            If (IsNothing(Current.Session(heatingMapping))) Then
                Return Nothing
            Else
                Return CType(Current.Session(heatingMapping), String)
            End If
        End Function

#End Region

#Region "remove  Heating Mapping Id"
        ''' <summary>
        ''' This function shall be used to remove  tree Item Name  from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeheatingMapping()
            If (Not IsNothing(Current.Session(heatingMapping))) Then
                Current.Session.Remove(heatingMapping)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Heating Fuel"
        ''' <summary>
        ''' This key will be used to save tree Item Name 
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared heatingFuelId As String = "heatingFuelId"

#Region "Set Heating Fuel Id "
        ''' <summary>
        ''' This function shall be used to save tree Item Name in session
        ''' </summary>
        ''' <param name="heatingMappId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setHeatingFuelId(ByRef heatingMappId As String)
            Current.Session(heatingFuelId) = heatingMappId
        End Sub

#End Region

#Region "Get  Heating Fuel Id"
        ''' <summary>
        ''' This function shall be used to get tree Item Name from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getHeatingFuelId()
            If (IsNothing(Current.Session(heatingFuelId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(heatingFuelId), String)
            End If
        End Function

#End Region

#Region "remove  Heating Fuel Id"
        ''' <summary>
        ''' This function shall be used to remove  tree Item Name  from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeHeatingFuelId()
            If (Not IsNothing(Current.Session(heatingFuelId))) Then
                Current.Session.Remove(heatingFuelId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get appliance Detail DataSet"
        ''' <summary>
        ''' This key will be used to save appliance Detail dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared applianceDetailDataSet As String = "applianceDetailDataSet"

#Region "Set appliance Detail DataSet"
        ''' <summary>
        ''' This function shall be used to save appliance Detail data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setApplianceDetailDataSet(ByRef ds As DataSet)
            Current.Session(applianceDetailDataSet) = ds
        End Sub

#End Region

#Region "Get appliance Detail DataSet"
        ''' <summary>
        ''' This function shall be used to get appliance Detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getApplianceDetailDataSet()
            If (IsNothing(Current.Session(applianceDetailDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(applianceDetailDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove appliance Detail DataSet"
        ''' <summary>
        ''' This function shall be used to remove appliance Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeApplianceDetailDataSet()
            If (Not IsNothing(Current.Session(applianceDetailDataSet))) Then
                Current.Session.Remove(applianceDetailDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Properties DetailsBO"
        ''' <summary>
        ''' This key will be used to save Properties DetailsBO
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PropertiesDetailsBO As String = "PropertiesDetailsBO"

#Region "Set Properties DetailsBO"
        ''' <summary>
        ''' This function shall be used to save Properties DetailsBO set in session
        ''' </summary>
        ''' <param name="pDetailsBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPropertiesDetailsBO(ByRef pDetailsBO As PropertiesDetailsBO)
            Current.Session(PropertiesDetailsBO) = pDetailsBO
        End Sub

#End Region

#Region "Get Properties DetailsBO"
        ''' <summary>
        ''' This function shall be used to Properties DetailsBO dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPropertiesDetailsBO()
            If (IsNothing(Current.Session(PropertiesDetailsBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PropertiesDetailsBO), PropertiesDetailsBO)
            End If
        End Function

#End Region

#Region "remove Properties DetailsBO"
        ''' <summary>
        ''' This function shall be used to remove Properties DetailsBO from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePropertiesDetailsBO()
            If (Not IsNothing(Current.Session(PropertiesDetailsBO))) Then
                Current.Session.Remove(PropertiesDetailsBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Rent History DataSet"
        ''' <summary>
        ''' This key will be used to save Rent History dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared RentHistoryDataSet As String = "RentHistoryDataSet"

#Region "Set Rent History DataSet"
        ''' <summary>
        ''' This function shall be used to save Rent History data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setRentHistoryDataSet(ByRef ds As DataSet)
            Current.Session(RentHistoryDataSet) = ds
        End Sub

#End Region

#Region "Get Rent History DataSet"
        ''' <summary>
        ''' This function shall be used to get Rent History dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getRentHistoryDataSet()
            If (IsNothing(Current.Session(RentHistoryDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(RentHistoryDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Rent History DataSet"
        ''' <summary>
        ''' This function shall be used to remove Rent History dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeRentHistoryDataSet()
            If (Not IsNothing(Current.Session(RentHistoryDataSet))) Then
                Current.Session.Remove(RentHistoryDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get planned Maintenance DataSet"
        ''' <summary>
        ''' This key will be used to save planned Maintenance dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared plannedMaintenanceDataSet As String = "plannedMaintenanceDataSet"

#Region "Set planned Maintenance DataSet"
        ''' <summary>
        ''' This function shall be used to save planned Maintenance data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setplannedMaintenanceDataSet(ByRef ds As DataSet)
            Current.Session(plannedMaintenanceDataSet) = ds
        End Sub

#End Region

#Region "Get planned Maintenance DataSet"
        ''' <summary>
        ''' This function shall be used to get Item Detail dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getplannedMaintenanceDataSet()
            If (IsNothing(Current.Session(plannedMaintenanceDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(plannedMaintenanceDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove planned Maintenance DataSet"
        ''' <summary>
        ''' This function shall be used to remove planned Maintenance dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeplannedMaintenanceDataSet()
            If (Not IsNothing(Current.Session(plannedMaintenanceDataSet))) Then
                Current.Session.Remove(plannedMaintenanceDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get condition Rating DataSet"
        ''' <summary>
        ''' This key will be used to save condition Rating dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared conditionRatingDataSet As String = "conditionRatingDataSet"

#Region "Set condition Rating DataSet"
        ''' <summary>
        ''' This function shall be used to save condition Rating data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setconditionRatingDataSet(ByRef ds As DataSet)
            Current.Session(conditionRatingDataSet) = ds
        End Sub

#End Region

#Region "Get conditionRating DataSet"
        ''' <summary>
        ''' This function shall be used to get conditionRating dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getconditionRatingDataSet()
            If (IsNothing(Current.Session(conditionRatingDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(conditionRatingDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove conditionRating DataSet"
        ''' <summary>
        ''' This function shall be used to remove conditionRating dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeconditionRatingDataSet()
            If (Not IsNothing(Current.Session(conditionRatingDataSet))) Then
                Current.Session.Remove(conditionRatingDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Valuation History DataSet"
        ''' <summary>
        ''' This key will be used to save Valuation History dataset
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ValuationHistoryDataSet As String = "ValuationHistoryDataSet"

#Region "Set Valuation History DataSet"
        ''' <summary>
        ''' This function shall be used to save Valuation History data set in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setValuationHistoryDataSet(ByRef ds As DataSet)
            Current.Session(ValuationHistoryDataSet) = ds
        End Sub

#End Region

#Region "Get Valuation History DataSet"
        ''' <summary>
        ''' This function shall be used to get Valuation History dataset from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getValuationHistoryDataSet()
            If (IsNothing(Current.Session(ValuationHistoryDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(ValuationHistoryDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Valuation History DataSet"
        ''' <summary>
        ''' This function shall be used to remove Valuation History dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeValuationHistoryDataSet()
            If (Not IsNothing(Current.Session(ValuationHistoryDataSet))) Then
                Current.Session.Remove(ValuationHistoryDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get ActivitiesBO List"
        ''' <summary>
        ''' This key will be used to save ActivitiesBO List
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ActivitiesBOList As String = "ActivitiesBOList"

#Region "Set ActivitiesBO List"
        ''' <summary>
        ''' This function shall be used to save ActivitiesBO List in session
        ''' </summary>
        ''' <param name="list"></param>
        ''' <remarks></remarks>
        Public Shared Sub setActivitiesBOList(ByRef list As List(Of ActivitiesBO))
            Current.Session(ActivitiesBOList) = list
        End Sub

#End Region

#Region "Get ActivitiesBO List "
        ''' <summary>
        ''' This function shall be used to get ActivitiesBO List from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getActivitiesBOList()
            If (IsNothing(Current.Session(ActivitiesBOList))) Then
                Return Nothing
            Else
                Return CType(Current.Session(ActivitiesBOList), List(Of ActivitiesBO))
            End If
        End Function

#End Region

#Region "remove ActivitiesBO List"
        ''' <summary>
        ''' This function shall be used to remove ActivitiesBO List from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeActivitiesBOList()
            If (Not IsNothing(Current.Session(ActivitiesBOList))) Then
                Current.Session.Remove(ActivitiesBOList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Selected Defect Ref"

#Region "Set Selected Defect Ref"

        Public Shared Sub setSelectDefectInseptionRef(ByRef selectedDefectRef As DataTable)
            Current.Session(SessionConstants.SelectedDefectInspectionRefDetail) = selectedDefectRef
        End Sub

#End Region

#Region "Get Selected Defect Ref"

        Public Shared Function getSelectDefectInseptionRef() As DataTable
            Dim selectedDefectRef As DataTable = Nothing
            If (Not IsNothing(Current.Session(SessionConstants.SelectedDefectInspectionRefDetail))) Then
                selectedDefectRef = CType(Current.Session(SessionConstants.SelectedDefectInspectionRefDetail), DataTable)
            End If
            Return selectedDefectRef
        End Function

#End Region

#Region "Remove Selected Defect Ref"

        Public Shared Sub removeSelectDefectInseptionRef()

            Current.Session.Remove(SessionConstants.SelectedDefectInspectionRefDetail)

        End Sub

#End Region

#End Region

#Region "Set / Get Defect Appointment To Be Arrange List"

#Region "Set Defect Appointment To Be Arrange List"

        Public Shared Sub setDefectToBeArrangedList(ByRef defectToBeArrangedList As DataTable)
            Current.Session(SessionConstants.DefectToBeArrangedList) = defectToBeArrangedList.Copy()
        End Sub

#End Region

#Region "Get Defect Appointment To Be Arrange List"

        Public Shared Function getDefectToBeArrangedList() As DataTable
            Dim defectToBeArrangedList As DataTable = Nothing
            If (Not IsNothing(Current.Session(SessionConstants.DefectToBeArrangedList))) Then
                defectToBeArrangedList = CType(Current.Session(SessionConstants.DefectToBeArrangedList), DataTable)
            End If
            Return defectToBeArrangedList
        End Function

#End Region



#Region "Set / Get Void Appointment Arranged &  To Be Arrange List"

#Region "Set Void Appointment To Be Arrange List"

        Public Shared Sub setVoidToBeArrangedList(ByRef voidToBeArrangedList As DataTable)
            Current.Session(SessionConstants.VoidToBeArrangedList) = voidToBeArrangedList.Copy()
        End Sub

#End Region

#Region "Set Void Appointment Arranged List"

        Public Shared Sub setVoidArrangedList(ByRef voidArrangedList As DataTable)
            Current.Session(SessionConstants.VoidArrangedList) = voidArrangedList.Copy()
        End Sub

#End Region

#Region "Get Void Appointment To Be Arrange List"

        Public Shared Function getVoidToBeArrangedList() As DataTable
            Dim voidToBeArrangedList As DataTable = Nothing
            If (Not IsNothing(Current.Session(SessionConstants.VoidToBeArrangedList))) Then
                voidToBeArrangedList = CType(Current.Session(SessionConstants.VoidToBeArrangedList), DataTable)
            End If
            Return voidToBeArrangedList
        End Function

#End Region


#Region "Get Void Appointment Arranged List"

        Public Shared Function getVoidArrangedList() As DataTable
            Dim voidArrangedList As DataTable = Nothing
            If (Not IsNothing(Current.Session(SessionConstants.VoidArrangedList))) Then
                voidArrangedList = CType(Current.Session(SessionConstants.VoidToBeArrangedList), DataTable)
            End If
            Return voidArrangedList
        End Function

#End Region


#End Region

#Region "Remove Defect Appointment To Be Arrange List"

        Public Shared Sub removeDefectToBeArrangedList()
            Current.Session.Remove(SessionConstants.DefectToBeArrangedList)
        End Sub

#End Region

#End Region

#Region "Set / Get Group By"

#Region "Set Group By"

        Public Shared Sub setGroupBy(ByRef groupBy As Boolean)
            Current.Session(SessionConstants.GroupBy) = groupBy
        End Sub

#End Region

#Region "Get Group By"

        Public Shared Function getGroupBy()
            Dim groupBy As Boolean = False
            If (Not IsNothing(Current.Session(SessionConstants.GroupBy))) Then
                groupBy = CType(Current.Session(SessionConstants.GroupBy), Boolean)
            End If
            Return groupBy
        End Function

#End Region

#Region "remove Group By"

        Public Shared Sub removeGroupBy()
            If (Not IsNothing(Current.Session(SessionConstants.GroupBy))) Then
                Current.Session.Remove(SessionConstants.GroupBy)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Selected Defects Datatable"

#Region "Set Selected Defects Datatable"

        ''' <summary>
        ''' This function shall be used to save Selected Defects Datatable in session
        ''' </summary>
        ''' <param name="selectedDefectsDataTable"></param>
        ''' <remarks></remarks>
        Public Shared Sub setSelectedDefectsDataTable(ByRef selectedDefectsDataTable As DataTable)
            Current.Session(SessionConstants.SelectedDefectsDataTable) = selectedDefectsDataTable
        End Sub

#End Region

#Region "Get Selected Defects Datatable"

        ''' <summary>
        ''' This function shall be used to get Selected Defects Datatable from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getSelectedDefectsDataTable() As DataTable
            Dim selectedDefectsDataTable As DataTable = Nothing
            If Not (IsNothing(Current.Session(SessionConstants.SelectedDefectsDataTable))) Then
                selectedDefectsDataTable = (CType(Current.Session(SessionConstants.SelectedDefectsDataTable), DataTable))
            End If
            Return selectedDefectsDataTable
        End Function

#End Region

#Region "remove Selected Defects Datatable"

        ''' <summary>
        ''' This function shall be used to remove Selected Defects Datatable from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeSelectedDefectsDataTable()
            If (Not IsNothing(Current.Session(SessionConstants.SelectedDefectsDataTable))) Then
                Current.Session.Remove(SessionConstants.SelectedDefectsDataTable)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Operative  working Hour Ds"

#Region "Set Operative working Hour Ds"

        Public Shared Sub setOperativeworkingHourDs(ByRef OperativeworkingHour As DataSet)
            Current.Session(SessionConstants.OperativeworkingHour) = OperativeworkingHour
        End Sub

#End Region

#Region "Get Operative working Hour Ds"

        Public Shared Function getOperativeworkingHourDs() As DataSet
            If (IsNothing(Current.Session(SessionConstants.OperativeworkingHour))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.OperativeworkingHour), DataSet)
            End If
        End Function

#End Region

#Region "remove Operative working Hour Ds"

        Public Shared Sub removeOperativeworkingHourDs()
            If (Not IsNothing(Current.Session(SessionConstants.OperativeworkingHour))) Then
                Current.Session.Remove(SessionConstants.OperativeworkingHour)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Defect Avaialble Operative Ds"

#Region "Set Defect Avaialble Operative Ds"

        Public Shared Sub setDefectAvailableOperativesDs(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionConstants.AvailableOperativesDs) = availableOperativesDs
        End Sub

#End Region

#Region "Get Defect Avaialble Operative Ds"

        Public Shared Function getDefectAvailableOperativesDs() As DataSet
            Dim availableOperativesDs As DataSet = Nothing
            If Not(IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                availableOperativesDs = CType(Current.Session(SessionConstants.AvailableOperativesDs), DataSet)
            End If
            Return availableOperativesDs
        End Function

#End Region

#Region "remove Defect Avaialble Operative Ds"

        Public Shared Sub removeDefectAvailableOperativeDs()
            If (Not IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                Current.Session.Remove(SessionConstants.AvailableOperativesDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Temp Fault Apt Block List"

#Region "Set Temp Defect Apt Block List"

        Public Shared Sub setTempDefectAptBlockList(ByRef faultAptBlockList As List(Of TempDefectAppointmentBO))
            Current.Session(SessionConstants.TempDefectAptBlockList) = faultAptBlockList
        End Sub

#End Region

#Region "Get Temp Defect Apt Block List"

        Public Shared Function getTempDefectAptBlockList() As List(Of TempDefectAppointmentBO)
            Dim defectAptBlockList As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
            If Not (IsNothing(Current.Session(SessionConstants.TempDefectAptBlockList))) Then
                defectAptBlockList = CType(Current.Session(SessionConstants.TempDefectAptBlockList), List(Of TempDefectAppointmentBO))
            End If
            Return defectAptBlockList
        End Function

#End Region

#Region "Remove Defect Fault Apt Block List"

        Public Shared Sub removeTempDefectAptBlockList()
            If (Not IsNothing(Current.Session(SessionConstants.TempDefectAptBlockList))) Then
                Current.Session.Remove(SessionConstants.TempDefectAptBlockList)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Common Address BO Data"

#Region "Set Common Address BO Data"

        Public Shared Sub setCommonAddressBO(ByRef customerData As CommonAddressBO)
            Current.Session(SessionConstants.CommonAddress) = customerData
        End Sub

#End Region

#Region "get Common Address BO Data"

        Public Shared Function getCommonAddressBO() As CommonAddressBO
            Dim customerData As New CommonAddressBO
            If Not (IsNothing(Current.Session(SessionConstants.CommonAddress))) Then
                customerData = CType(Current.Session(SessionConstants.CommonAddress), CommonAddressBO)
            End If
            Return customerData
        End Function

#End Region

#Region "remove Common Address BO Data"

        Public Shared Sub removeCommonAddress()
            If (Not IsNothing(Current.Session(SessionConstants.CommonAddress))) Then
                Current.Session.Remove(SessionConstants.CommonAddress)
            End If
        End Sub

#End Region

#End Region

#Region "Selected temp appointment block for Scheduling "

#Region "Set Selected temp appointment block for Scheduling Data"

        Public Shared Sub setSelectedAppointmentBlockforScheduling(ByVal selectedAppointmentBlock As TempDefectAppointmentBO)
            Current.Session(SessionConstants.SelectedAppointmentBlock) = selectedAppointmentBlock
        End Sub

#End Region

#Region "get Selected temp appointment block for Scheduling Data"

        Public Shared Function getSelectedAppointmentBlockforScheduling() As TempDefectAppointmentBO
            Dim selectedAppointmentBlock As New TempDefectAppointmentBO
            If Not (IsNothing(Current.Session(SessionConstants.SelectedAppointmentBlock))) Then
                selectedAppointmentBlock = CType(Current.Session(SessionConstants.SelectedAppointmentBlock), TempDefectAppointmentBO)
            End If
            Return selectedAppointmentBlock
        End Function

#End Region

#Region "remove Selected temp appointment block for Scheduling Data"

        Public Shared Sub removeSelectedAppointmentBlockforScheduling()
            If (Not IsNothing(Current.Session(SessionConstants.SelectedAppointmentBlock))) Then
                Current.Session.Remove(SessionConstants.SelectedAppointmentBlock)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Appointment Data For Appointment Summary"

#Region "Set Appointment Data For Appointment Summary"

        Public Shared Sub setDefectAppointmentDataForAppointmentSummary(ByRef defectAppointmentData As AppointmentBO)
            Current.Session(SessionConstants.DefectAppointmentData) = defectAppointmentData
        End Sub

#End Region

#Region "get Appointment Data For Appointment Summary"

        Public Shared Function getDefectAppointmentDataForAppointmentSummary()
            Dim defectAppointmentData = New AppointmentBO
            If Not (IsNothing(Current.Session(SessionConstants.DefectAppointmentData))) Then
                defectAppointmentData = CType(Current.Session(SessionConstants.DefectAppointmentData), AppointmentBO)
            End If
            Return defectAppointmentData
        End Function

#End Region

#Region "remove Appointment Data For Appointment Summary"

        Public Shared Sub removeDefectAppointmentDataForAppointmentSummary()
            If (Not IsNothing(Current.Session(SessionConstants.DefectAppointmentData))) Then
                Current.Session.Remove(SessionConstants.DefectAppointmentData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Assign To Contractor BO Data"

#Region "Set  Assign To Contractor BO Data"

        Public Shared Sub setAssignToContractorBO(ByRef contractorBO As AssignToContractorBO)
            Current.Session(SessionConstants.AssignToContractorBo) = contractorBO
        End Sub

#End Region

#Region "get  Assign To Contractor BO Data"

        Public Shared Function getAssignToContractorBO()
            If (IsNothing(Current.Session(SessionConstants.AssignToContractorBo))) Then
                Dim contractorBO As New AssignToContractorBO
                Return contractorBO
            Else
                Return CType(Current.Session(SessionConstants.AssignToContractorBo), AssignToContractorBO)
            End If
        End Function

#End Region

#Region "remove Assign To Contractor BO Data"

        Public Shared Sub removeAssignToContractorBO()
            If (Not IsNothing(Current.Session(SessionConstants.AssignToContractorBo))) Then
                Current.Session.Remove(SessionConstants.AssignToContractorBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Expenditure BO List"

        ''' <summary>
        ''' This key is to manage List of Expenditure BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ExpenditureBOList As String = "ExpenditureBOList"
        ''' <summary>
        ''' To get Expenditure BO list, this is saved in view state to get LIMIT and Remaining LIMIT of the current employee.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getExpenditureBOList() As List(Of ExpenditureBO)
            Dim expenditureBOLst As List(Of ExpenditureBO) = TryCast(Current.Session(ExpenditureBOList), List(Of ExpenditureBO))
            If IsNothing(expenditureBOLst) Then
                expenditureBOLst = New List(Of ExpenditureBO)
            End If
            Return expenditureBOLst
        End Function

        Public Shared Sub removeExpenditureBOList()
            Current.Session.Remove(ExpenditureBOList)
        End Sub

        Public Shared Sub setExpenditureBOList(ByVal expenditureBOLst As List(Of ExpenditureBO))
            Current.Session(ExpenditureBOList) = expenditureBOLst
        End Sub

#End Region

#Region " Set / Get Vat BO List"

        ''' <summary>
        ''' This key is to manage List of Vat BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ContractorVatBoList As String = "ContractorVatBoList"

        ''' <summary>
        ''' To get Vat BO list, this is saved in view state to get Vat rate for the current vat item.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getVatBOList() As List(Of ContractorVatBO)
            Dim VatBoLst As List(Of ContractorVatBO) = TryCast(Current.Session(ContractorVatBoList), List(Of ContractorVatBO))
            If IsNothing(VatBoLst) Then
                VatBoLst = New List(Of ContractorVatBO)
            End If
            Return VatBoLst
        End Function

        Public Shared Sub removeVatBOList()
            Current.Session.Remove(ContractorVatBoList)
        End Sub

        Public Shared Sub setVatBOList(ByRef VatBoLst As List(Of ContractorVatBO))
            Current.Session(ContractorVatBoList) = VatBoLst
        End Sub

#End Region

#Region "Set / Get Power Source DataSet"
        ''' <summary>
        ''' This key will be used to save the PowerSource DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PowerSourceDataSet As String = "PowerSourceDataSet"

#Region "Set PowerSource DataSet"
        ''' <summary>
        ''' This function shall be used PowerSource DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPowerSourceDataSet(ByRef ds As DataSet)
            Current.Session(PowerSourceDataSet) = ds
        End Sub

#End Region

#Region "Get PowerSource DataSet"
        ''' <summary>
        ''' This function shall be used to get PowerSource DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPowerSourceDataSet()
            If (IsNothing(Current.Session(PowerSourceDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PowerSourceDataSet), DataSet)
            End If
        End Function

#End Region

#Region "Remove PowerSource DataSet"
        ''' <summary>
        ''' This function shall be used to remove PowerSource dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePowerSourceDataSet()
            If (Not IsNothing(Current.Session(PowerSourceDataSet))) Then
                Current.Session.Remove(PowerSourceDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get CP12Dcoument"
        ''' <summary>
        ''' This key will be used to save the PowerSource DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared CP12Dcoument As String = "CP12Dcoument"

#Region "Set CP12Dcoument"
        ''' <summary>
        ''' This function shall be used PowerSource DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setCP12Dcoument(ByRef ds As Byte())
            Current.Session(CP12Dcoument) = ds
        End Sub

#End Region

#Region "Get CP12Dcoument"
        ''' <summary>
        ''' This function shall be used to get PowerSource DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getCP12Dcoument()
            If (IsNothing(Current.Session(CP12Dcoument))) Then
                Return Nothing
            Else
                Return CType(Current.Session(CP12Dcoument), Byte())
            End If
        End Function

#End Region

#Region "Remove CP12Dcoument"
        ''' <summary>
        ''' This function shall be used to remove PowerSource dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeCP12Dcoument()
            If (Not IsNothing(Current.Session(CP12Dcoument))) Then
                Current.Session.Remove(CP12Dcoument)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Property Id for Appliance"

#Region "Set Property Id for Appliance"

        Public Shared Sub setPropertyIdForAppliance(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id for Appliance"

        Public Shared Function getPropertyIdForAppliance()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id for Appliance"

        Public Shared Sub removePropertyIdForAppliance()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get Property Id for fuel scheduling"

#Region "Set Property Id"

        Public Shared Sub setPropertyIdForFuelScheduling(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyIdForFuelScheduling()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyIdForFuelScheduling()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get JournalId for Fuel scheduling"

#Region "Set JournalId"

        Public Shared Sub setJournalIdForFuelScheduling(ByRef journalId As Integer)

            Current.Session(SessionConstants.JournalId) = journalId
        End Sub

#End Region

#Region "get journalId"

        Public Shared Function getJournalIdForFuelScheduling()
            If (IsNothing(Current.Session(SessionConstants.JournalId))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JournalId), Integer)

            End If
        End Function

#End Region

#Region "remove journalId"

        Public Shared Sub removejournalIdForFuelScheduling()
            If (Not IsNothing(Current.Session(SessionConstants.JournalId))) Then
                Current.Session.Remove(SessionConstants.JournalId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Summary Ds"
        ''' <summary>
        ''' This key shall be used to get and set the job sheet summary data Set
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared objjobSheetSumamryDs As DataSet ' = "JobSheetSummaryDs"

        ''' <summary>
        ''' This function shall be used to save the job sheet summary dataset in session
        ''' </summary>
        ''' <param name="prmjobSheetSumamryDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setJobSheetSummaryDs(ByRef prmjobSheetSumamryDs As DataSet)
            objjobSheetSumamryDs = prmjobSheetSumamryDs
        End Sub

        ''' <summary>
        ''' This function shall be used to get the job sheet summary dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getJobSheetSummaryDs() As DataSet
            If (IsNothing(objjobSheetSumamryDs)) Then
                Return New DataSet
            Else
                Return objjobSheetSumamryDs
            End If
        End Function

        ''' <summary>
        ''' This function shall be used to remove the job sheet summary dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeJobSheetSummaryDs()
            If (Not IsNothing(objjobSheetSumamryDs)) Then
                objjobSheetSumamryDs.Clear()
            End If
        End Sub

#End Region

#Region "Set / Get ExpenditureId Values"

#Region "Set ExpenditureId Values"
        Public Shared Sub setExpenditureId(ByRef ExpenditureId As Integer)
            Current.Session(SessionConstants.ExpenditureId) = ExpenditureId
        End Sub
#End Region

#Region "get ExpenditureId Values"
        Public Shared Function getExpenditureId() As Integer
            If (IsNothing(Current.Session(SessionConstants.ExpenditureId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.ExpenditureId), Integer)
            End If
        End Function
#End Region

#Region "remove ExpenditureId Values"

        Public Shared Sub removeExpenditureId()
            If (Not IsNothing(Current.Session(SessionConstants.ExpenditureId))) Then
                Current.Session.Remove(SessionConstants.ExpenditureId)
            End If
        End Sub

#End Region
#End Region

#Region "Set / Get HeadId Values"

#Region "Set HeadId Values"
        Public Shared Sub setHeadId(ByRef HeadId As Integer)
            Current.Session(SessionConstants.HeadId) = HeadId
        End Sub
#End Region

#Region "get HeadId Values"
        Public Shared Function getHeadId() As Integer
            If (IsNothing(Current.Session(SessionConstants.HeadId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.HeadId), Integer)
            End If
        End Function
#End Region

#Region "remove HeadId Values"

        Public Shared Sub removeHeadId()
            If (Not IsNothing(Current.Session(SessionConstants.HeadId))) Then
                Current.Session.Remove(SessionConstants.HeadId)
            End If
        End Sub

#End Region
#End Region

#Region "Set / Get Confirmed Gas Appointments"

#Region "Set Confirmed Gas Appointments"

        Public Shared Sub setConfirmedGasAppointment(ByRef confirmedFaults As AppointmentCancellationBO)
            Current.Session(SessionConstants.ConfirmedGasAppointment) = confirmedFaults
        End Sub

#End Region

#Region "Get Confirmed Gas Appointments"

        Public Shared Function getConfirmedGasAppointment() As AppointmentCancellationBO
            If (IsNothing(Current.Session(SessionConstants.ConfirmedGasAppointment))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ConfirmedGasAppointment), AppointmentCancellationBO)
            End If
        End Function

#End Region

#Region "remove Confirmed Gas Appointment"

        Public Shared Sub removeConfirmedGasAppointment()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmedGasAppointment))) Then
                Current.Session.Remove(SessionConstants.ConfirmedGasAppointment)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Appointment Cancellation For Scheme and Block"

#Region "Set Appointment Cancellation For Scheme and Block"

        Public Shared Sub setAppointmentCancellationForSchemeBlock(ByRef appointmentCancellationForSchemeBlock As AppointmentCancellationForSchemeBlockBO)
            Current.Session(SessionConstants.AppointmentCancellationForSchemeBlock) = appointmentCancellationForSchemeBlock
        End Sub

#End Region

#Region "Get Appointment Cancellation For Scheme and Block"

        Public Shared Function getAppointmentCancellationForSchemeBlock() As AppointmentCancellationForSchemeBlockBO
            If (IsNothing(Current.Session(SessionConstants.AppointmentCancellationForSchemeBlock))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.AppointmentCancellationForSchemeBlock), AppointmentCancellationForSchemeBlockBO)
            End If
        End Function

#End Region

#Region "remove Appointment Cancellation For Scheme and Block"

        Public Shared Sub removeAppointmentCancellationForSchemeBlock()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentCancellationForSchemeBlock))) Then
                Current.Session.Remove(SessionConstants.AppointmentCancellationForSchemeBlock)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get Appointment Type For Scheme and Block"

#Region "Set Appointment Type For Scheme and Block"

        Public Shared Sub setBoilerTypeInfoForSchemeBlock(ByRef boilerTypeInfo As BoilerTypeInfo)
            Current.Session(SessionConstants.BoilerTypeInfo) = boilerTypeInfo
        End Sub

#End Region

#Region "Get Appointment Type For Scheme and Block"

        Public Shared Function getBoilerTypeInfoForSchemeBlock() As BoilerTypeInfo
            If (IsNothing(Current.Session(SessionConstants.BoilerTypeInfo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.BoilerTypeInfo), BoilerTypeInfo)
            End If
        End Function

#End Region

#Region "remove Appointment Type For Scheme and Block"

        Public Shared Sub removeBoilerTypeInfoForSchemeBlock()
            If (Not IsNothing(Current.Session(SessionConstants.BoilerTypeInfo))) Then
                Current.Session.Remove(SessionConstants.BoilerTypeInfo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Appointment Type"

#Region "Set Appointment Type Info"

        Public Shared Sub SetAppointmentTypeInfo(ByRef appointmentTypeInfo As String())
            Current.Session(SessionConstants.AppointmentType) = appointmentTypeInfo
        End Sub

#End Region

#Region "Get Appointment Type Info"

        Public Shared Function GetAppointmentTypeInfo() As String()
            If (IsNothing(Current.Session(SessionConstants.AppointmentType))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.AppointmentType), String())
            End If
        End Function

#End Region

#Region "remove Appointment Type Info"

        Public Shared Sub RemoveAppointmentTypeInfo()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentType))) Then
                Current.Session.Remove(SessionConstants.AppointmentType)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Servicing Type For Oil/Alternative"

#Region "Set Servicing Type For Oil/Alternative"

        Public Shared Sub SetServicingTypeForOilAlternativ(ByRef servicingType As ServicingType)
            Current.Session(SessionConstants.ServicingTypeInfo) = servicingType
        End Sub

#End Region

#Region "Get Servicing Type For Oil/Alternative"

        Public Shared Function GetServicingTypeForOilAlternativ() As ServicingType
            If (IsNothing(Current.Session(SessionConstants.ServicingTypeInfo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ServicingTypeInfo), ServicingType)
            End If
        End Function

#End Region

#Region "Remove Servicing Type For Oil/Alternative"

        Public Shared Sub RemoveServicingTypeForOilAlternative()
            If (Not IsNothing(Current.Session(SessionConstants.ServicingTypeInfo))) Then
                Current.Session.Remove(SessionConstants.ServicingTypeInfo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get AssignToContractorBo"

        Public Shared Property AssignToContractorBo As PropertyAssignToContractorBo
            Get

                If Current.Session(SessionConstants.PropertyAssignToContractorBo) Is Nothing Then
                    Dim objAssignToContractorBo As PropertyAssignToContractorBo = New PropertyAssignToContractorBo()
                    Return objAssignToContractorBo
                Else
                    Return CType((Current.Session(SessionConstants.PropertyAssignToContractorBo)), PropertyAssignToContractorBo)
                End If
            End Get
            Set(ByVal value As PropertyAssignToContractorBo)

                If value Is Nothing Then

                    If Current.Session(SessionConstants.PropertyAssignToContractorBo) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyAssignToContractorBo)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyAssignToContractorBo) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get PropertyId"

        Public Shared Property PropertyId As String
            Get

                If Current.Session(SessionConstants.PropertyId) Is Nothing Then
                    Return String.Empty
                Else
                    Return Current.Session(SessionConstants.PropertyId).ToString()
                End If
            End Get
            Set(ByVal value As String)

                If String.IsNullOrEmpty(value) Then

                    If Current.Session(SessionConstants.PropertyId) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyId)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyId) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get Provision DataSet"        

        Public Shared Property ProvisionDataSet As DataSet
            Get

                If Current.Session(SessionConstants.ProvisionDataSet) Is Nothing Then
                    Return Nothing
                Else
                    Return CType((Current.Session(SessionConstants.ProvisionDataSet)), DataSet)
                End If
            End Get
            Set(ByVal value As DataSet)

                If value Is Nothing Then

                    If Current.Session(SessionConstants.ProvisionDataSet) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.ProvisionDataSet)
                    End If
                Else
                    Current.Session(SessionConstants.ProvisionDataSet) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get SchemeName"

        Public Shared Property PropertyName As String
            Get

                If Current.Session(SessionConstants.PropertyName) Is Nothing Then
                    Return Nothing
                Else
                    Return Current.Session(SessionConstants.PropertyName).ToString()
                End If
            End Get
            Set(ByVal value As String)

                If value Is Nothing Then

                    If Current.Session(SessionConstants.PropertyName) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyName)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyName) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get VatBoList"

        Public Shared Property VatBOList As List(Of VatBo)
            Get

                If Current.Session(SessionConstants.VatBoList) Is Nothing Then
                    Dim objVatBOList As List(Of VatBo) = New List(Of VatBo)()
                    Return objVatBOList
                Else
                    Return CType((Current.Session(SessionConstants.VatBoList)), List(Of VatBo))
                End If
            End Get
            Set(ByVal value As List(Of VatBo))

                If value Is Nothing Then

                    If Current.Session(SessionConstants.VatBoList) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.VatBoList)
                    End If
                Else
                    Current.Session(SessionConstants.VatBoList) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get PropertyExpenditureBOList"

        Public Shared Property PropertyExpenditureBOList As List(Of ExpenditureBO)
            Get

                If Current.Session(SessionConstants.PropertyExpenditureBOList) Is Nothing Then
                    Dim objExpenditureBOList As List(Of ExpenditureBO) = New List(Of ExpenditureBO)()
                    Return objExpenditureBOList
                Else
                    Return CType((Current.Session(SessionConstants.PropertyExpenditureBOList)), List(Of ExpenditureBO))
                End If
            End Get
            Set(ByVal value As List(Of ExpenditureBO))

                If value Is Nothing Then

                    If Current.Session(SessionConstants.PropertyExpenditureBOList) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyExpenditureBOList)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyExpenditureBOList) = value
                End If
            End Set
        End Property

#End Region    

#Region "Set / Get PropertyDetail"

        Public Shared Property PropertyDetail As DataSet
            Get
                If Current.Session(SessionConstants.PropertyDetail) Is Nothing Then
                    Return Nothing
                Else
                    Return CType((Current.Session(SessionConstants.PropertyDetail)), DataSet)
                End If
            End Get
            Set(ByVal value As DataSet)

                If value Is Nothing Then

                    If Current.Session(SessionConstants.PropertyDetail) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyDetail)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyDetail) = value
                End If
            End Set
        End Property

#End Region

#Region "Set / Get PropertyDetail"

        Public Shared Property PropertyOwnerShip As DataSet
            Get
                If Current.Session(SessionConstants.PropertyOwnerShip) Is Nothing Then
                    Return Nothing
                Else
                    Return CType((Current.Session(SessionConstants.PropertyOwnerShip)), DataSet)
                End If
            End Get
            Set(ByVal value As DataSet)

                If value Is Nothing Then

                    If Current.Session(SessionConstants.PropertyOwnerShip) IsNot Nothing Then
                        Current.Session.Remove(SessionConstants.PropertyOwnerShip)
                    End If
                Else
                    Current.Session(SessionConstants.PropertyOwnerShip) = value
                End If
            End Set
        End Property

#End Region
    End Class

End Namespace