﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Collections
Imports System.Web.Caching
Imports System.Globalization

Namespace AS_Utilities
    Public Class Validation


        Public Function emptyString(value As String) As Boolean
            If value.Equals(String.Empty) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function lengthString(value As String, length As Integer) As Boolean
            If value.Length > length Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function isNumber(value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.numbersExp)
            If Not rgx.IsMatch(value) Then
                Return False
            Else
                Return True
            End If
        End Function

        Public Shared Function isEmail(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.emailExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function isMobile(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.mobileExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function

#Region "validate date time"
        Public Shared Function validateDateFormate(dateString As String) As Boolean
            Dim dateTime__1 As System.DateTime = DateTime.Now
            Dim valid As Boolean = DateTime.TryParseExact(dateString, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime__1)
            Return valid

        End Function


#End Region
    End Class
End Namespace
