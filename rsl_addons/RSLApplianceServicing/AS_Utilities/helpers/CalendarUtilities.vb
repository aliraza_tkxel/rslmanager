﻿Imports System.Data
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports System.Text
Imports System.Security.Cryptography

Namespace AS_Utilities
    Public Class CalendarUtilities

#Region " Read Google Api settings "

        Public ReadOnly Property GoogleApiEnabled() As Boolean
            Get
                Return CType(ConfigurationManager.AppSettings.Item("GoolgeApiEnabled"), Boolean)
            End Get
        End Property

#End Region

#Region "Create Columns for Datatable "
        Public Sub createColumns(ByVal colName As String, ByRef varWeeklyDT As DataTable)
            Dim Name As DataColumn = New DataColumn(colName)
            Name.DataType = System.Type.GetType("System.String")
            varWeeklyDT.Columns.Add(Name)

        End Sub
#End Region

#Region "Calculate Distance"

        Public Function CalculateDistance(ByVal propertyFrom As String, ByVal propertyTo As String) As String

            'Pass request to google api with orgin and destination details
            Dim distance As String = "0"
            Dim key As String = "_Mk3W_etiTwvijZB441zy0E3iDU="
            Dim url As String = String.Empty

            Try
                Trace.WriteLine(String.Format("propertyFrom - {0}; propertyTo - {1}; {2}", propertyFrom, propertyTo, DateTime.Now.ToShortTimeString))

                url = String.Format("http://maps.googleapis.com/maps/api/distancematrix/xml?origins={0}&destinations={1}&mode=Car&units=imperial&sensor=false&client=gme-broadlandhousing", propertyFrom, propertyTo)

                Trace.WriteLine(String.Format("AS Unsigned url: {0}", url))
           
                url = Sign(url, key)

                Trace.WriteLine(String.Format("AS Signed url: {0}", url))
             
                Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

                Trace.WriteLine(request.RequestUri.OriginalString)

                Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
                'Get response as stream from httpwebresponse
                Dim resStream As New StreamReader(response.GetResponseStream())
                Dim distanceXml As New XmlDocument()
                distanceXml.LoadXml(resStream.ReadToEnd())

                'Get DistanceMatrixResponse element and its values
                Dim xnList As XmlNodeList = distanceXml.SelectNodes("/DistanceMatrixResponse")
                For Each xn As XmlNode In xnList
                    If xn("status").InnerText.ToString() = "OK" Then
                        distance = distanceXml.DocumentElement.SelectSingleNode("/DistanceMatrixResponse/row/element/distance/text").InnerText
                        If (distance.Contains("ft")) Then
                            distance = "0"
                        End If
                    End If
                Next

            Catch ex As Exception

                Trace.WriteLine(ex.ToString())

            End Try

            Trace.WriteLine(distance)
            Return distance.Replace("mi", "")

        End Function

        Private Function Sign(ByVal url As String, ByVal keyString As String) As String

            Dim encoding As ASCIIEncoding = New ASCIIEncoding()

            'URL-safe decoding
            Dim privateKeyBytes As Byte() = Convert.FromBase64String(keyString.Replace("-", "+").Replace("_", "/"))

            Dim objURI As Uri = New Uri(url)
            Dim encodedPathAndQueryBytes As Byte() = encoding.GetBytes(objURI.LocalPath & objURI.Query)

            'compute the hash
            Dim algorithm As HMACSHA1 = New HMACSHA1(privateKeyBytes)
            Dim hash As Byte() = algorithm.ComputeHash(encodedPathAndQueryBytes)

            'convert the bytes to string and make url-safe by replacing '+' and '/' characters
            Dim signature As String = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_")

            'Add the signature to the existing URI.
            Return objURI.Scheme & "://" & objURI.Host & objURI.LocalPath & objURI.Query & "&signature=" & signature

        End Function

#End Region

#Region "Calculate Distance"

        ''' <summary>
        '''  If isNoGoogleError= true system will add a new row in data table againt these properties
        ''' </summary>
        ''' <param name="propertyFrom"></param>
        ''' <param name="propertyTo"></param>
        ''' <param name="isNoGoogleError"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CalculateDistance(ByVal propertyFrom As String, ByVal propertyTo As String, ByRef isNoGoogleError As Boolean) As String
            isNoGoogleError = False

            'Pass request to google api with orgin and destination details

            Dim distance As String = "0"
            Dim key As String = "_Mk3W_etiTwvijZB441zy0E3iDU="

            Dim url As String = String.Empty

            Try

                Trace.WriteLine(String.Format("propertyFrom - {0}; propertyTo - {1}; {2}", propertyFrom, propertyTo, DateTime.Now.ToShortTimeString))

                url = String.Format("http://maps.googleapis.com/maps/api/distancematrix/xml?origins={0}&destinations={1}&mode=Car&units=imperial&sensor=false&client=gme-broadlandhousing", propertyFrom, propertyTo)

                Trace.WriteLine(String.Format("Unsigned url: {0}", url))

                url = Sign(url, key)

                Trace.WriteLine(String.Format("Signed url: {0}", url))

                Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

                Trace.WriteLine(request.RequestUri.OriginalString)

                Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
                'Get response as stream from httpwebresponse
                Dim resStream As New StreamReader(response.GetResponseStream())
                Dim distanceXml As New XmlDocument()
                distanceXml.LoadXml(resStream.ReadToEnd())

                'Get DistanceMatrixResponse element and its values
                Dim xnList As XmlNodeList = distanceXml.SelectNodes("/DistanceMatrixResponse")

                For Each xn As XmlNode In xnList

                    'el.WriteToErrorLog(distanceXml.DocumentElement.InnerXml.ToString(), "XML Returned", "Calculate Distance")
                    If xn("status").InnerText.ToString() = "OK" Then
                        distance = distanceXml.DocumentElement.SelectSingleNode("/DistanceMatrixResponse/row/element/distance/text").InnerText
                        'below are the conditions to convert the distance into miles
                        If (distance.Contains("ft")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("ft", "")) * 0.000189394, 2))
                        ElseIf (distance.Contains("km")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("km", "")) * 0.621371, 2))
                        ElseIf (distance.Contains("mi")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("mi", "")), 2))
                        Else
                            distance = "0"
                        End If
                        isNoGoogleError = True
                        ApplicationConstants.distanceError = True

                    ElseIf xn("status").InnerText.ToString() = "INVALID_REQUEST" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.INVALIDREQUEST
                    ElseIf xn("status").InnerText.ToString() = "MAX_ELEMENTS_EXCEEDED" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.MAXELEMENTSEXCEEDED
                    ElseIf xn("status").InnerText.ToString() = "OVER_QUERY_LIMIT" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.OVERQUERYLIMIT
                    ElseIf xn("status").InnerText.ToString() = "REQUEST_DENIED" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.REQUESTDENIED
                    ElseIf xn("status").InnerText.ToString() = "UNKNOWN_ERROR" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.UNKNOWNERROR

                    End If
                Next

            Catch ex As Exception
                'el.WriteToErrorLog(ex.ToString(), "exception Calculated Distance", "exception Calculate Distance")
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
                Trace.WriteLine(ex.ToString)
            End Try

            Trace.WriteLine(distance)
            Return distance.Replace("mi", "")

        End Function

#End Region

#Region "Formating Date with suffix like 1st, 2nd, 3rd"
        Public Function FormatDayNumberSuffix(ByVal calendarDay As String) As String
            Select Case calendarDay
                Case 1, 21, 31
                    calendarDay = calendarDay + "st"
                Case 2, 22
                    calendarDay = calendarDay + "nd"
                Case 3, 23
                    calendarDay = calendarDay + "rd"
                Case Else
                    calendarDay = calendarDay + "th"
            End Select

            Return calendarDay
        End Function
#End Region

#Region "Calculate Last Monday Of The Month"
        Public Sub calculateMonday(ByVal startDate As Date)

            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), ApplicationConstants.startDate).ToString("dddd") = "Monday") Then
                    ApplicationConstants.startDate = DateAdd("d", -(dayCount), ApplicationConstants.startDate)
                    ApplicationConstants.monthDays = ApplicationConstants.monthDays + dayCount
                    dayCount = 10
                End If
            Next

        End Sub
#End Region

#Region "Caculate Monday of Current Week"
        ''' <summary>
        ''' This function accepts a date as input calculated monday of current month, and returns the calculated date.
        ''' </summary>
        ''' <param name="startDate">Provide a date parameter, in case of week calendar it is start date.</param>
        ''' <returns>This function returns the date of monday of current week.</returns>
        ''' <remarks></remarks>
        Public Function calculateMondayFunction(ByVal startDate As Date)

            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), startDate).ToString("dddd") = "Monday") Then
                    startDate = DateAdd("d", -(dayCount), startDate)
                    dayCount = 10
                End If
            Next

            Return startDate
        End Function

#End Region

#Region "Calculate Last Monday for Appointment to be arranged"
        Public Sub calculateAptbaMonday(ByVal startDate As Date)
            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), startDate).ToString("dddd") = "Monday") Then
                    startDate = DateAdd("d", -(dayCount), startDate)
                    dayCount = 10
                End If
            Next
            SessionManager.setAptbaStartDate(startDate)
        End Sub
#End Region

#Region "Calculate Last Monday for Appointment arranged"
        Public Sub calculateApaMonday(ByVal startDate As Date)
            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), startDate).ToString("dddd") = "Monday") Then
                    startDate = DateAdd("d", -(dayCount), startDate)
                    dayCount = 10
                End If
            Next
            SessionManager.setApaStartDate(startDate)
        End Sub
#End Region

#Region "Calculate Last Sunday of the Month"
        Public Sub calculateSunday()
            Dim endDate As Date = SessionManager.getAptbaEndDate()
            For dayCount = 0 To 10
                If (DateAdd("d", dayCount, ApplicationConstants.endDate).ToString("dddd") = "Sunday") Then
                    'If (DateAdd("d", dayCount, endDate).ToString("dddd") = "Sunday") Then
                    'endDate = DateAdd("d", dayCount, endDate)
                    'ApplicationConstants.monthDays = ApplicationConstants.monthDays + dayCount
                    ApplicationConstants.endDate = DateAdd("d", dayCount, ApplicationConstants.endDate)
                    ApplicationConstants.monthDays = ApplicationConstants.monthDays + dayCount
                    dayCount = 10
                End If
            Next
            'SessionManager.setAptbaEndDate(endDate)
        End Sub
#End Region

#Region "Get Last 50 Years List"
        Public Function GetLast50YearsList() As List(Of Integer)
            Dim yearsList As List(Of Integer) = New List(Of Integer)
            Dim currentYear As Integer
            currentYear = DateTime.Now.Year

            For value As Integer = currentYear To currentYear - 50 Step -1
                yearsList.Add(value)
            Next
            Return yearsList
        End Function
#End Region
    End Class
End Namespace