﻿Imports System.Globalization
Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Web
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports System.Text.RegularExpressions

Namespace AS_Utilities
    Public Class GeneralHelper

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getAssignedToContractorJobSheetAddress(ByVal JI As String) As String
            Return "../Common/JobSheetSummaryAC.aspx?JournalId=" + JI
        End Function

#End Region
#Region "get Document Upload Path"
        Public Shared Function getDocumentUploadPath() As String
            Return ConfigurationManager.AppSettings("DocumentsUploadPath")
        End Function
#End Region

#Region "get Image Upload Path"
        Public Shared Function getImageUploadPath() As String
            Return ConfigurationManager.AppSettings("ImageUploadPath")
        End Function
#End Region

#Region "get Thumbnail Upload Path"
        Public Shared Function getThumbnailUploadPath() As String
            Return ConfigurationManager.AppSettings("ThumbnailUploadPath")
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "repalce Rc RB Td"
        Public Shared Function repalceRcRBTd(ByRef letterBodyValue As String, ByRef rentBalance As Double, ByRef rentCharge As Double, ByRef todayDate As Date)
            letterBodyValue = letterBodyValue.Replace("[RB]", rentBalance)
            letterBodyValue = letterBodyValue.Replace("[RC]", rentCharge)
            letterBodyValue = letterBodyValue.Replace("[TD]", todayDate.ToShortDateString())
            Return letterBodyValue
        End Function
#End Region

#Region "Check if Property status is Let"
        'Start - Changed By Aamir Waheed on May 30, 2013.
        'To check for if the property status is let, to show hide tenant contact icon.

        ''' <summary>
        ''' Returns true/false for the status passed as string.
        ''' If calling from a Grid change database value to string using .ToSring()
        ''' </summary>
        ''' <param name="PropertyStatus">Property status to check, if it is "let"</param>
        ''' <returns>True/False</returns>
        ''' <remarks>It handles the status in any case (lower/upper/mixed case).</remarks>

        Public Shared Function isPropertyStatusLet(ByRef PropertyStatus As String) As Boolean
            Dim isLet As Boolean = False

            If (LCase(PropertyStatus.ToString()) = LCase(ApplicationConstants.PropertyStatusLet)) Then
                isLet = True
            End If

            Return isLet
        End Function
        'End - Changed By Aamir Waheed on May 30, 2013.
#End Region

#Region "Convert UTF8 String to ASCII String"

        Public Shared Function UTF8toASCII(ByVal text As String) As String
            Dim utf8 As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim encodedBytes As [Byte]() = utf8.GetBytes(text)
            Dim convertedBytes As [Byte]() = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, encodedBytes)
            Dim ascii As System.Text.Encoding = System.Text.Encoding.ASCII

            Return ascii.GetString(convertedBytes)
        End Function

#End Region

#Region "get Day Start Hour"
        ''' <summary>
        ''' DayStartHour represents job start time in the morning e.g 8:00 AM
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDayStartHour() As String
            Return CType(ConfigurationManager.AppSettings("DayStartHour"), Double)
        End Function
#End Region

#Region "get Day End Hour"
        ''' <summary>
        ''' 'DayEndHour represents job end time in the evening e.g 5:00 PM
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDayEndHour() As String
            Return CType(ConfigurationManager.AppSettings("DayEndHour"), Double)
        End Function
#End Region

#Region "get Appointment Duration Hours"
        ''' <summary>
        ''' 'Appointment Duration represents total hours to complete the appointment e.g 1 hour
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentDurationHours() As String
            Return CType(ConfigurationManager.AppSettings("AppointmentDurationHours"), Double)
        End Function
#End Region

#Region "Get Today Start Hour"

        Public Shared Function getTodayStartHour(ByVal startDate As Date) As Double
            'if both date are same then add two hours in it
            If DateTime.Compare(startDate.Date, Date.Today) = 0 Then
                startDate = DateTime.Now.AddHours(2)
            Else
                Return CType(ConfigurationManager.AppSettings("DayStartHour"), Double)

                'startDate = GeneralHelper.getDateTimeFromDateAndNumber(startDate, 0)
                'startDate.AddHours(GeneralHelper.getOfficialDayStartHour())
            End If
            Return startDate.Hour
        End Function

#End Region

#Region "get Unique Keys"
        Public Shared Function getUniqueKey(ByVal uniqueCounter As Integer) As String
            'this 'll be used to create the unique index of dictionary
            Dim uniqueKey As String = uniqueCounter.ToString() + SessionManager.getUserEmployeeId().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssffff")
            Return uniqueKey
        End Function
#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalMinutes
            Return timeInSec
        End Function
#End Region

#Region "Convert Date to Us Culture"
        ''' <summary>
        ''' This fucntion 'll convert the date in following format e.g Mon 12th Nov 2012
        ''' </summary>
        ''' <param name="dateToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUsCulture(ByVal dateToConvert As Date) As String
            Return CType(dateToConvert, Date).ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"))
        End Function

#End Region

#Region "Convert Time To 24 Hrs Format"
        ''' <summary>
        ''' This funciton will convert the datetime in 24 hours format
        ''' </summary>
        ''' <param name="timeToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertTimeTo24hrsFormat(ByVal timeToConvert As Date) As String
            Return timeToConvert.ToString("HH:mm")
        End Function
#End Region

#Region "Get Date with weekday format"
        ''' <summary>
        ''' Get Date with weekday format e.g "Tuesday 13th December 2013"
        ''' </summary>
        ''' <param name="aDate">date time</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateWithWeekdayFormat(ByVal aDate As DateTime)
            Dim startDay As String = aDate.DayOfWeek.ToString()
            Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(aDate.ToString("dd")))
            Dim startMonth As String = aDate.ToString("MMMMMMMMMMMMM")
            Dim startYear As String = aDate.ToString("yyyy")
            Return startDay + ", " + startDate + " " + startMonth + " " + startYear
        End Function
#End Region

#Region "Get Ordinal"
        ''' <summary>
        ''' Get Ordinal
        ''' </summary>
        ''' <param name="number"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getOrdinal(number As Integer) As String
            Dim suffix As String = [String].Empty

            Dim ones As Integer = number Mod 10
            Dim tens As Integer = CInt(Math.Floor(number / 10D)) Mod 10

            If tens = 1 Then
                suffix = "th"
            Else
                Select Case ones
                    Case 1
                        suffix = "st"
                        Exit Select

                    Case 2
                        suffix = "nd"
                        Exit Select

                    Case 3
                        suffix = "rd"
                        Exit Select
                    Case Else

                        suffix = "th"
                        Exit Select
                End Select
            End If
            Return [String].Format("{0}{1}", number, suffix)
        End Function

#End Region

#Region " Unix time stamp is seconds past epoch"
        Public Shared Function unixTimeStampToDateTime(unixTimeStamp As Double) As DateTime
            ' Unix time stamp is seconds past epoch
            Dim dtDateTime As System.DateTime = New DateTime(1970, 1, 1, 0, 0, 0, _
             0, System.DateTimeKind.Utc)
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp)
            Return dtDateTime
        End Function
#End Region

#Region "Populate SMS body"

        Public Shared Function populateBodyAppointmentConfirmationSMS(ByVal appointmentTime As String, ByVal appointmentDate As Date) As String
            Dim body As String = String.Empty
            body = ApplicationConstants.SMSmessage
            If CType(Left(appointmentTime, 2), Integer) < 12 Then
                body = body.Replace("{AppointmentTime}", "morning")
                body = body.Replace("{StartTime}", "08:00")
                body = body.Replace("{EndTime}", "12:00")
            Else
                body = body.Replace("{AppointmentTime}", "afternoon")
                body = body.Replace("{StartTime}", "12:00")
                body = body.Replace("{EndTime}", "16:30")
            End If

            body = body.Replace("{AppointmentDate}", appointmentDate.ToString("dddd, d") + GeneralHelper.getDaySuffix(appointmentDate) + appointmentDate.ToString(" MMMM yyyy"))

            Return body
        End Function
#End Region
#Region "Send SMS UPDATED API"
        Public Shared Sub sendSmsUpdatedAPI(ByVal recepientMobile As String, ByVal body As String)

            Dim result As String
            Dim apiKey As String = "resw5bAZdIs-fnUwznzIfzJM66WfRdWSQRXlFtR7V2"
            'string apiKey = "nVv8yYd3mYo-1UXMeTpnYJ4pXqspvYPrgC7HTsYH3s"; //rehan
            Dim username As String = "digitalengagementteam@broadlandgroup.org"
            Dim password As String = "Broadland@1"
            Dim numbers As String = recepientMobile ' in a comma seperated list
            Dim message As String = body
            Dim sender As String = " "

            Dim url As String = "https://api.txtlocal.com/send/?apiKey=" & apiKey & "&numbers=" & numbers & "&message=" & message & "&sender=" & sender
            'refer to parameters to complete correct url string

            Dim myWriter As StreamWriter = Nothing
            Dim objRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)

            objRequest.Method = "POST"
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(url)
            objRequest.ContentType = "application/x-www-form-urlencoded"
            Try
                myWriter = New StreamWriter(objRequest.GetRequestStream())
                myWriter.Write(url)
            Catch e As Exception
                'return e.Message;
            Finally
                myWriter.Close()
            End Try

            Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
            Using sr As New StreamReader(objResponse.GetResponseStream())
                result = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using

        End Sub
#End Region
#Region "Populate SMS body"

        Public Shared Function populateBodyAppointmentNotificationSMS(ByVal appointmentDate As Date, ByVal appointmentStartTime As String) As String
            Dim body As New StringBuilder
            body.Append(ApplicationConstants.SMSToCustomerMessage)
            If CType(Left(appointmentStartTime, 2), Integer) < 12 Then
                body.Replace("{AppointmentShift}", "morning")
            Else
                body.Replace("{AppointmentShift}", "afternoon")
            End If

            body.Replace("{AppointmentTime}", appointmentStartTime)

            body.Replace("{AppointmentDate}", getDateWithWeekdayFormat(appointmentDate))
            Return body.ToString()
        End Function
#End Region

#Region "Send SMS"
        Public Shared Sub sendSMS(ByVal body As String, ByVal recepientMobile As String)
            Dim request As WebRequest = WebRequest.Create(ApplicationConstants.SMSUrl)
            ' Set the Method property of the request to POST.
            request.Method = "POST"
            ' Create POST data and convert it to a byte array.

            Dim strdata As String

            strdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(recepientMobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strdata)
            ' Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded"
            ' Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length
            ' Get the request stream.
            Dim dataStream As Stream = request.GetRequestStream()
            ' Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.
            dataStream.Close()
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            ' Display the status.
            'Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            ' Get the stream containing content returned by the server.f
            dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access.
            Dim reader As New StreamReader(dataStream) ' Read the content.
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Display the content.
            'Console.WriteLine(responseFromServer)
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
        End Sub
#End Region

#Region "Populate Confirmation Email body"
        ''' <summary>
        ''' Populate Confirmation Email body
        ''' </summary>
        ''' <param name="jobSheetNumberList"></param>
        ''' <param name="appointmentTime"></param>
        ''' <param name="appointmentDate"></param>
        ''' <param name="customerName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function populateBodyAppointmentConfirmationEmail(ByVal jobSheetNumberList As String, ByVal appointmentTime As String, ByVal appointmentDate As Date, ByVal customerName As String) As AlternateView
            Dim body As String = String.Empty
            Dim server = HttpContext.Current.Server
            Dim emailtemplatepath = server.MapPath("~/Email/AppointmentConfirmation.htm")

            Dim mailMessage As New MailMessage()

            Dim reader As StreamReader = New StreamReader(emailtemplatepath)
            body = reader.ReadToEnd.ToString
            body = body.Replace("{CustomerName}", customerName)

            If CType(Left(appointmentTime, 2), Integer) < 12 Then
                body = body.Replace("{AppointmentTime}", "morning")
                body = body.Replace("{StartTime}", "08:00")
                body = body.Replace("{EndTime}", "13:00")
            Else
                body = body.Replace("{AppointmentTime}", "afternoon")
                body = body.Replace("{StartTime}", "12:00")
                body = body.Replace("{EndTime}", "17:00")
            End If

            body = body.Replace("{AppointmentDate}", getDateWithWeekdayFormat(appointmentDate))

            Dim logo50Years As LinkedResource = New LinkedResource(server.MapPath("~/Images/50_Years.gif"))
            logo50Years.ContentId = "logo50Years_Id"

            body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

            Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(server.MapPath("~/Images/Braodland_Repairs.gif"))
            logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

            body = body.Replace("{Logo_Broadland_Repairs}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

            Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

            Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body, mimeType)
            alternatevw.LinkedResources.Add(logo50Years)
            alternatevw.LinkedResources.Add(logoBroadLandRepairs)


            Return alternatevw
        End Function
#End Region

#Region "Get daysuffix Like th,st,nd,rd to show (1st, 2nd, 3rd etc.)"
        ''' <summary>
        ''' This function is to get the day suffix like st, nd, rd, th to show it with date like 1st, 2nd, 3rd, 4th etc.        ''' 
        ''' </summary>
        ''' <param name="dateparam">date to get suffix.</param>
        ''' <returns>It return the suffix one of these four, st, nd, rd, th</returns>
        ''' <remarks></remarks>
        Public Shared Function getDaySuffix(ByVal dateparam As Date) As String
            Dim daySuffix = "th"
            If (dateparam.Day = 1 OrElse dateparam.Day = 21 OrElse dateparam.Day = 31) Then
                daySuffix = "st"
            ElseIf (dateparam.Day = 2 OrElse dateparam.Day = 22) Then
                daySuffix = "nd"
            ElseIf (dateparam.Day = 3 OrElse dateparam.Day = 23) Then
                daySuffix = "rd"
            End If

            Return daySuffix
        End Function

#End Region

#Region "Set Grid View Pager"

        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)
            pnlPagination.Visible = False

            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

                'Set Range validation error Message and Maximum Value (Minimum value is 1).
                Dim rangevalidatorPageNumber As RangeValidator = pnlPagination.FindControl("rangevalidatorPageNumber")
                If Not IsNothing(rangevalidatorPageNumber) Then
                    rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString()
                    rangevalidatorPageNumber.MaximumValue = totalPages
                End If
            End If
        End Sub

#End Region

#Region "get Unique File Name"
        ''' <summary>
        ''' Get the unique file name for a given file name, the unique file name is based on time.
        ''' </summary>
        ''' <param name="fileName">File name to get new unique file name.</param>
        ''' <param name="keyWord">Optional: A keyword to be added at start of file name. i.e "defects_"</param>
        ''' <returns>Up to first thirty five (35) characters of file name plus unique string and extension.</returns>
        ''' <remarks></remarks>
        Public Shared Function getUniqueFileName(ByVal fileName As String, Optional ByVal keyWord As String = "")

            Dim fileNameWithoutExtention As String = Path.GetFileNameWithoutExtension(fileName)
            Dim ext = Path.GetExtension(fileName)
            Dim uniqueString As String = DateTime.Now.ToString().GetHashCode().ToString("x")

            If fileNameWithoutExtention.Length > 35 Then
                fileNameWithoutExtention = fileNameWithoutExtention.Substring(0, 35)
            End If
            Dim uniqueFileNameWithoutExtention = fileNameWithoutExtention + uniqueString

            Return keyWord + uniqueFileNameWithoutExtention + ext

        End Function
#End Region

#Region "Populate Official Hours With 30 Min Duration"
        ''' <summary>
        ''' this function 'll populate the drop down with official hours with 30 minute Duration
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub populateCoreHoursWithThirtyMinDuration(ByRef ddl As DropDownList)
            Dim startHour As Double = 0
            Dim endHour As Double = 23
            For i As Double = startHour To endHour

                ddl.Items.Add(New ListItem(i.ToString("00") + ":00", i.ToString("00") + ":00"))
                If i <> endHour Then
                    ddl.Items.Add(New ListItem(i.ToString("00") + ":30", i.ToString("00") + ":30"))
                End If

            Next i

            ddl.Items.Add(New ListItem(Convert.ToString(endHour) + ":30", Convert.ToString(endHour) + ":30"))

            Dim item As ListItem = New ListItem("-", "-1")
            ddl.Items.Insert(0, item)
        End Sub
#End Region

#Region "get Official Day Start Hour"
        Public Shared Function getCoreDayStartHour() As String
            Return ConfigurationManager.AppSettings("CoreDayStartHour")
        End Function
#End Region

#Region "get Official Day End Hour"
        Public Shared Function getCoreDayEndHour() As String
            Return ConfigurationManager.AppSettings("CoreDayEndHour")
        End Function
#End Region

#Region "Get Page Menu"

        ''' <summary>
        ''' "Get Page Menu
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPageMenu(ByVal page As String) As String

            Dim pageDict As Dictionary(Of String, String) = New Dictionary(Of String, String)
            pageDict.Add(PathConstants.CalendarPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.CalendarSearchPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.MonthlyCalendarPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.WeeklyCalendarPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DownloadPage, ApplicationConstants.PropertyModuleMenu)
            pageDict.Add(PathConstants.NotInReleasePage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PrintCP12DocumentPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PrintLetterPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DashboardPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.MainPagePage, ApplicationConstants.PropertyModuleMenu)
            pageDict.Add(PathConstants.PrintActivitiesPage, ApplicationConstants.PropertyModuleMenu)
            pageDict.Add(PathConstants.PrintJobSheetDetailPage, ApplicationConstants.PropertyModuleMenu)
            pageDict.Add(PathConstants.PropertyRecordPage, ApplicationConstants.PropertyModuleMenu)
            pageDict.Add(PathConstants.CertificateExpiryPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.IssuedCertificatesPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PrintCertificatesPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PrintPropertyCasePage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ReportsPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.AccessPage, ApplicationConstants.ResourcesMenu)
            pageDict.Add(PathConstants.CreateLetterPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.EditLetterPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.LettersPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PreviewLetterPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ResourcesPage, ApplicationConstants.ResourcesMenu)
            pageDict.Add(PathConstants.SaveLetterAsPdfPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.StatusPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.StatusActionMainPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.UsersPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ViewLetterPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ViewLettersPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.AdminSchedulingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.FuelSchedulingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.VoidSchedulingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.AccessDeniedPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.PropertiesPage, ApplicationConstants.AdminMenu)
            pageDict.Add(PathConstants.DefectSchedulingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DefectBasketPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DefectAvailableAppointmentsPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DefectJobSheetPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ApplianceDefectReportPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.CancelledDefectReportPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DisconnectedApplianceReport, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DefectsHistoryReport, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.AlternativeServicingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ToBeArrangedAvailableAppointmentsPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.AlternativeArrangedAppointmentsPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.OilSchedulingPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.OilToBeArrangedAvailableAppointmentsPage, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.OilArrangedAppointmentsPage, ApplicationConstants.ServicingMenu)

            'Assign to Contractor Report
            pageDict.Add(PathConstants.AssignedToContractorReport, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.DefectApprovalReport, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ViewPlannedJobSheet, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ViewACPlannedJobSheet, ApplicationConstants.ServicingMenu)
            pageDict.Add(PathConstants.ViewPrintJobSheetSummary, ApplicationConstants.ServicingMenu)
            Dim menuName As String = String.Empty
            If (pageDict.ContainsKey(page)) Then
                menuName = pageDict.Item(page)
            End If

            Return menuName

        End Function

#End Region

#Region "Get Access Denied Page"
        ''' <summary>
        ''' Get Access Denied Page
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAccessDeniedPage(ByVal menuName As String) As String

            Dim menuDict As Dictionary(Of String, String) = New Dictionary(Of String, String)
            menuDict.Add(ApplicationConstants.ServicingMenu, PathConstants.AccessDeniedPath)
            menuDict.Add(ApplicationConstants.ResourcesMenu, PathConstants.ResourcesAccessDeniedPath)
            menuDict.Add(ApplicationConstants.PropertyModuleMenu, PathConstants.PropertyModuleAccessDeniedPath)
            menuDict.Add(ApplicationConstants.AdminMenu, PathConstants.AdminAccessDeniedPath)
            menuDict.Add(ApplicationConstants.MaintenanceMenu, PathConstants.MaintenanceAccessDeniedPath)
            menuDict.Add(ApplicationConstants.ReportsMenu, PathConstants.ReportsAccessDeniedPath)

            Dim accessDeniedPage As String = String.Empty

            If (menuDict.ContainsKey(menuName)) Then
                accessDeniedPage = menuDict.Item(menuName)
            End If

            Return accessDeniedPage

        End Function

#End Region

#Region "append Hour Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendHourLabel(ByVal aNumber As Double)
            If aNumber > 1 Then
                Return aNumber.ToString() + " hours"
            Else
                Return aNumber.ToString() + " hour"
            End If

        End Function
#End Region

#Region "Get Job Sheet Summary Address"
        Public Shared Function getJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummary.aspx?jsn="
        End Function
#End Region


#Region "Get Job Sheet Summary Address"

        Public Shared Function getSbJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/SbJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary  for Voids"

        Public Shared Function getVoidJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JSVJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Planned"

        Public Shared Function getPlannedJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummaryPlanned.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getGasJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/ServicingJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getDefectsJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/DefectsJobSheetSummary.aspx?jsn="
        End Function

#End Region
#Region "Get Job Sheet Summary for Cyclic Maintenance and "

        Public Shared Function getMEJobSheetSummary() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/MaintenanceJobSheetSummary.aspx?jsn="
        End Function

#End Region
#Region "validate date time"
        Public Shared Function validateDateFormate(ByVal dateString As String) As Boolean
            Dim dateTime As Date
            Dim valid As Boolean = Date.TryParseExact(dateString, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
            Return valid

        End Function


#End Region

#Region "Get Push Notification Application Address"
        Public Shared Function getPushNotificationAddress(ByVal absoluteUri As String) As String
            Dim servername As String = absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, Len(absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, absoluteUri.Substring(absoluteUri.IndexOf("//") + 2).IndexOf("/"))))
            Return "https://" + servername + "/StockConditionOfflineAPI/push/NotificationApplianceAndPlanned"
        End Function
#End Region

#Region " Send Push Notifications to Appliance "

        Public Shared Function generatePushNotificationMessage(ByVal appointmentType As String, ByVal appointmentDate As DateTime, ByVal appointmentStartTime As DateTime,
                                                         ByVal appointmentEndTime As DateTime, ByVal propertyAddress As String) As String
            Dim message As String = String.Empty
            Dim appointmentTypeDesc As String = String.Empty

            If (appointmentType.ToLower() = "new") Then
                appointmentTypeDesc = "New Appliance Service Scheduled"
            ElseIf (appointmentType.ToLower() = "existing") Then
                appointmentTypeDesc = "Appliance Service Rearranged"
            ElseIf (appointmentType.ToLower() = "cancelled") Then
                appointmentTypeDesc = "Appliance Service Cancelled"
            End If

            message += "BHG Property Manager" + "\n\n"
            message += appointmentTypeDesc + "\n\n"
            message += appointmentDate.ToString("d MMM yyyy") + " "
            message += appointmentStartTime.ToString("HH:mm") + " to "
            message += appointmentEndTime.ToString("HH:mm") + "\n"
            message += propertyAddress

            message = HttpUtility.UrlEncode(message)

            Return message
        End Function
#End Region


#Region " Send Push Notifications to Appliance "

        Public Shared Function pushNotificationAppliance(ByVal appointmentMessage As String, ByVal operatorId As Integer) As Boolean
            Try

                Dim URL As String = String.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId)
                Dim request As WebRequest = Net.WebRequest.Create(URL)
                request.Credentials = CredentialCache.DefaultCredentials
                Dim response As WebResponse = request.GetResponse()
                Dim dataStream As Stream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)
                Dim responseFromServer As String = reader.ReadToEnd()
                reader.Close()
                response.Close()

                If (responseFromServer.ToString.Equals("true")) Then
                    Return True
                End If

                Return False

            Catch ex As Exception
                Return False
            End Try

        End Function

#End Region

#Region "Get Yes No String From Boolean value (For UI Display)"

        Public Shared Function getYesNoStringFromBooleanValue(ByVal inputValue As Object) As String
            Dim outputString As String = "No"
            Dim parsedInputValue As Boolean = False
            If Not (IsDBNull(inputValue) OrElse IsNothing(inputValue)) Then
                Boolean.TryParse(inputValue, parsedInputValue)
                If parsedInputValue Then
                    outputString = "Yes"
                End If
            End If
            Return outputString
        End Function

#End Region

#Region "Get Yes No String From Inetger value (For UI Display)"

        Public Shared Function getYesNoStringFromValue(ByVal inputValue As Object) As String
            Dim outputString As String = "No"
            Dim parsedInputValue As Integer = 0
            If Not (IsDBNull(inputValue) OrElse IsNothing(inputValue)) AndAlso Integer.TryParse(inputValue, parsedInputValue) AndAlso parsedInputValue > 0 Then
                outputString = "Yes"
            End If
            Return outputString
        End Function

#End Region

#Region "Get Yes No String From Boolean value (For UI Display)"

        Public Shared Function getYesNoStringFromValue(ByVal inputValue As Boolean?, Optional ByVal trueString As String = "Yes", Optional ByVal falseString As String = "No", Optional ByVal nullString As String = "N/A") As String
            Dim outputString As String = nullString
            Dim parsedInputValue As Boolean = False
            If Not (IsDBNull(inputValue) OrElse IsNothing(inputValue)) Then
                Boolean.TryParse(inputValue, parsedInputValue)
                If parsedInputValue Then
                    outputString = trueString
                Else
                    outputString = falseString
                End If
            End If
            Return outputString
        End Function

#End Region

#Region "Get Formated Date or Sting N/A if null"

        Public Shared Function getFormatedDateDefaultString(ByVal inputDate As Object, Optional ByVal formatString As String = "dd/MM/yyyy", Optional ByVal defaultOutput As String = "N/A") As String
            Dim outputString = defaultOutput
            Dim parsedInputDate As Date
            If Not (IsDBNull(inputDate) OrElse IsNothing(inputDate)) AndAlso Date.TryParse(inputDate, parsedInputDate) Then
                outputString = String.Format("{0:" + formatString + "}", parsedInputDate)
            End If
            Return outputString
        End Function

#End Region

#Region "Get Color String Base on Parts Dues Date"

        Public Shared Function getColourForPartsDue(ByVal inputDate As Object, Optional ByVal defaultColour As String = "Black")
            Dim colourString As String = defaultColour
            Dim parsedInputDate As Date
            If Not (IsDBNull(inputDate) OrElse IsNothing(inputDate)) AndAlso Date.TryParse(inputDate, parsedInputDate) Then
                If parsedInputDate < Now.AddDays(7).Date Then
                    colourString = "Red"
                End If

            End If
            Return colourString
        End Function

#End Region

#Region "Get Time Slots to Display"

        ''' <summary>
        ''' Get Time Slots to Display from configuration file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Decimal: Get Time Slots to Display from web.config or 5 as default.</remarks>
        Public Shared Function getTimeSlotToDisplay() As Decimal
            getTimeSlotToDisplay = getDecimalValueFromConfigurationFile("timeSlotToDisplayIntelligenceScheduling", 5)
        End Function

#End Region

#Region "Get Decimal value from Configuration file"

        ''' <summary>
        ''' Get Decimal value from file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Decimal value: in case key/value not found it will return 0.0(default) or provided default value.</remarks>
        Public Shared Function getDecimalValueFromConfigurationFile(ByVal key As String, Optional ByVal defaultValue As Decimal = 0.0) As Decimal
            getDecimalValueFromConfigurationFile = defaultValue
            Dim value As String = ConfigurationManager.AppSettings(key)
            Dim tempParsedValue As Decimal = 0.0
            If Not String.IsNullOrEmpty(value) AndAlso Decimal.TryParse(value, tempParsedValue) Then
                getDecimalValueFromConfigurationFile = tempParsedValue
            End If
        End Function

#End Region

#Region "Get Is Multiple Days Appointment Allowed From configuration File"

        ''' <summary>
        ''' Get boolen value of isMultipleDaysAppointmentAllowed from configuration file i.e web.config 
        ''' </summary>
        ''' <returns>Boolen value (True/False) if Multiple Days Appointment Allowed is added in configuration file, otherwise false which is default in this function </returns>
        ''' <remarks></remarks>
        Public Shared Function isMultipleDaysAppointmentAllowed() As Boolean
            isMultipleDaysAppointmentAllowed = getBoolValueFromConfigurationFile("isMultipleDaysAppointmentAllowed", False)
        End Function

#End Region

#Region "Get Bool value from Configuration file"

        ''' <summary>
        ''' Get bool value from file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Boolen value (True/False): ,in case key/value not found it will return false(default) or provided default value.</remarks>
        Public Shared Function getBoolValueFromConfigurationFile(ByVal key As String, Optional ByVal defaultValue As Boolean = False)
            getBoolValueFromConfigurationFile = defaultValue
            Dim value As String = ConfigurationManager.AppSettings(key)
            Dim tempParsedValue As Boolean = False
            If Not String.IsNullOrEmpty(value) AndAlso Boolean.TryParse(value, tempParsedValue) Then
                getBoolValueFromConfigurationFile = tempParsedValue
            End If
        End Function

#End Region

#Region "Get Today Start Hour"

        Public Shared Function getMaximunLookAhead() As Decimal
            Return getDecimalValueFromConfigurationFile("MaximunLookAhead", 50)
        End Function

#End Region

#Region "Get Date Time From Date And Number"
        ''' <summary>
        ''' This function 'll append the date with number to get date time and return it
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateTimeFromDateAndNumber(ByVal aDate As Date, ByVal aHour As Double) As Date
            Return CType(aDate.Date.ToLongDateString() + " " + aHour.ToString() + ":00", Date)
        End Function
#End Region

#Region "Get delimited string for a datatable column"

        Public Shared Function getDelimitedStringForDatatableColumn(ByRef dt As DataTable, ByVal columnName As String, Optional ByVal delimiter As String = ",") As String
            Dim delimitedString As New StringBuilder()

            If dt.Columns.Contains(columnName) Then
                ' Concate/Append each item and delimiter at the end of string
                For Each row As DataRow In dt.Rows
                    delimitedString.Append(row(columnName)).Append(delimiter)
                Next
            End If

            ' Trim extra delimiter from end of string
            If delimitedString.Length > delimiter.Length Then
                delimitedString.Remove(delimitedString.Length - delimiter.Length, delimiter.Length)
            End If

            Return delimitedString.ToString()
        End Function

#End Region

#Region "Get Singular or Plural Post Fix for Numeric Values"

        ''' <summary>
        ''' Add a singular or plural post fix to the end of a numeric value
        ''' </summary>
        ''' <param name="numericValue"></param>
        ''' <param name="singularPostfix">singular postfix for values 1 or less.</param>
        ''' <param name="pluralPostFix">plural postfix for valus more than 1.
        ''' If it is not provides, an s is added at the end of singularpostfix</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getSingularOrPluralPostFixForNumericValues(ByVal numericValue As Double, ByVal singularPostfix As String, Optional ByVal pluralPostFix As String = Nothing, Optional ByVal nullString As String = "N/A") As String
            Dim returnString As String = nullString
            If Not IsNothing(numericValue) Then
                If numericValue <= 1 Then
                    returnString = String.Format("{0} {1}", numericValue, singularPostfix)
                Else
                    returnString = String.Format("{0} {1}", numericValue, If(pluralPostFix, singularPostfix + "s"))
                End If
            End If
            Return returnString
        End Function

#End Region

#Region "Get Inverse of boolen Value"

        ''' <summary>
        ''' This helper is required to bind inverse vaule in grid view.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getInverseOfBooleanValue(ByVal value As Boolean) As Boolean
            Return Not value
        End Function

#End Region

#Region "Empty Labels Text"

        Public Shared Sub emptyLabelsText(ByRef labelsToEmpty() As Label)
            For Each lbl As Label In labelsToEmpty
                lbl.Text = String.Empty
            Next
        End Sub

#End Region

#Region "Empty Text Boxes Text"

        Public Shared Sub emptyTextBoxesText(ByRef textBoxes() As TextBox)
            For Each txtBox As TextBox In textBoxes
                txtBox.Text = String.Empty
            Next
        End Sub

#End Region

#Region "Change server controls visibility"

        Public Shared Sub changeServerControlsVisibility(ByVal visible As Boolean, ByVal ParamArray serverControls() As WebControl)
            For Each control In serverControls
                control.Visible = visible
            Next
        End Sub

#End Region

#Region "Set Parse Nullable Boolean Value from String"

        Public Shared Sub setParseNullableBooleanValueFromStringOrDefaultToNull(ByRef nullableBoolean As Nullable(Of Boolean), ByRef valueToParse As String)
            Dim tempBoolean As Boolean
            If Boolean.TryParse(valueToParse, tempBoolean) Then
                nullableBoolean = tempBoolean
            End If
        End Sub

#End Region

#Region "Populate Drop Down with duration hours"

        Public Shared Sub PopulateDropDownWithDurationHours(ByRef dropDownToPopulate As DropDownList, Optional ByVal startDuration As Decimal = 1, Optional ByVal endDuraton As Decimal = 23.5, Optional ByVal increment As Decimal = 1, Optional ByVal singularPostFix As String = "hour", Optional ByVal pluralPostFix As String = Nothing)
            dropDownToPopulate.Items.Clear()

            For duration As Decimal = startDuration To endDuraton Step increment
                dropDownToPopulate.Items.Add(New ListItem(getSingularOrPluralPostFixForNumericValues(duration, singularPostFix, pluralPostFix), duration))
            Next
        End Sub

#End Region

#Region "Remove Duplicate Rows From Datatable"
        Public Shared Function RemoveDuplicateRows(dTable As DataTable, colName As String) As DataTable
            Dim hTable As New Hashtable()
            Dim duplicateList As New ArrayList()

            For Each dRow As DataRow In dTable.Rows
                If hTable.Contains(dRow(colName).ToString().ToLower()) Then
                    duplicateList.Add(dRow)
                Else
                    hTable.Add(dRow(colName).ToString().ToLower(), String.Empty)
                End If
            Next

            For Each dpRow As DataRow In duplicateList
                dTable.Rows.Remove(dpRow)
            Next

            Return dTable
        End Function
#End Region

#Region "fetch HEADID and EXPENDITUREID for assign to contractor"

        Public Shared Function GetHeadAndExpenditureId() As String
            Return CType(ConfigurationManager.AppSettings("ExpenditueType"), String)
        End Function

#End Region

#Region "fetch ExpenditureType for assign to contractor under Fuel Servicing"

        Public Shared Function GetExpenditureType_GasServicing() As String
            Return CType(ConfigurationManager.AppSettings("ExpenditueType_GasServicing"), String)
        End Function

#End Region

#Region "get Hours And Minute String"
        ''' <summary>
        ''' This function 'll convert the date into following format : (HH:mm)
        ''' </summary>
        ''' <param name="aDateTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getHrsAndMinString(aDateTime As DateTime) As String
            Return aDateTime.ToString("HH:mm")
        End Function
#End Region
#Region "convert Date Time In to Date Time String"
        ''' <summary>
        ''' This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToCustomString(aDate As System.DateTime) As String
            Return (aDate.ToString("HH:mm dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")))
        End Function
#End Region

#Region "get Appointment Creation Limit For Single Request"
        ''' <summary>
        ''' This function 'll return the appointment creation limit for single request
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentCreationLimitForSingleRequest()
            Return 5
        End Function

#End Region

#Region "Check Email Format valid"

        Public Shared Function isEmail(ByVal value As String) As Boolean
            Dim rgx As Regex = New Regex(RegularExpConstants.emailExp)

            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function

#End Region




    End Class

End Namespace
