﻿Imports System.Net.Mail


Namespace AS_Utilities
    Public Class EmailHelper
        Public Shared Sub sendHtmlFormattedEmail(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

        Public Shared Sub sendEmailwithAttachment(ByRef recepientName As String, ByRef recepientEmail As String, ByRef subject As String, ByRef body As String, ByRef attachment As Attachment)
            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
            mailMessage.Attachments.Add(attachment)

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)
        End Sub
        Public Shared Sub sendEmail(ByRef mailMessage As MailMessage)

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)
        End Sub


    End Class
End Namespace

