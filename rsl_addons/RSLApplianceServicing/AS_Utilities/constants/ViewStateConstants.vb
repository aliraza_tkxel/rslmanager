﻿Imports System

Namespace AS_Utilities


    Public Class ViewStateConstants

#Region "Appointment To Be Arranged"

        Public Const AptbaDataSet As String = "AptbaDataSet"
        Public Const AptbaSearchString As String = "AptbaSearchString"
        Public Const AptbaCheck56Days As String = "AptbaCheck56Days"
        Public Const isAddAppointmentPopup As String = "isAddAppointmentPopup"
        Public Const RefreshListButtonDisplay As String = "RefreshListButtonDisplay"
#End Region

#Region "Appointment Arranged"

        Public Const AptaDataSet As String = "AptaDataSet"
        Public Const AppointmentId As String = "AppointmentId"
        Public Const OperativeId As String = "OperativeId"
        Public Const AptaSearchString As String = "AptaSearchString"
        Public Const AptaCheck56Days As String = "AptaCheck56Days"
        Public Const FuelType As String = "FuelType"
        Public Const AlternativeFuelType As String = "AlternativeFuelType"
        Public Const StatusType As String = "StatusType"
        Public Const AlternativeStatusType As String = "AlternativeStatusType"
        Public Const OilStatusType As String = "AlternativeStatusType"

#End Region

#Region "Page Sort Constants"
        Public Const SortDirection As String = "SortDirection"
        Public Const SortExpression As String = "SortExpression"
        Public Const Ascending As String = "Asc"
        Public Const Descending As String = "Desc"
        Public Const DefaultPageNumber As String = "10"
        Public Const DefaultPageSize As String = "10"
        Public Const PageSortBo As String = "PageSortBo"
        Public Const MEPageSortBo As String = "MEPageSortBo"
        Public Const TotalPageNumbers As String = "TotalPageNumbers"
        Public Const CurrentPageNumber As String = "CurrentPageNumber"
#End Region

#Region "Resources"
        Public Const UserPatchList As String = "UserPatchList"
        Public Const UserPatchListIds As String = "UserPatchListIds"
        Public Const UserSchemeList As String = "UserSchemeList"
        Public Const SaveGSRENo As String = "SaveGSRENo"
        Public Const SaveEmployeeId As String = "SaveEmployeeId"
        Public Const SchemeListIds As String = "SchemeListIds"
        Public Const AccessUserDataSet As String = "AccessUserDataSet"
        Public Const AccessRightEmployeeId As String = "AccessRightEmployeeId"
#End Region

#Region "General "
        Public Const GridDataSet As String = "GridDataSet"
        Public Const PropertyId As String = "PropertyId"
        Public Const ApplianceId As String = "ApplianceId"
        Public Const TotalCount As String = "TotalCount"
        Public Const ResultDataSet As String = "ResultDataSet"
        Public Const VirtualItemCount As String = "VirtualItemCount"
        Public Const Search As String = "Search"
        Public Const ActivitiesListBO As String = "ActivitiesListBO"
        Public Const IsSaved As String = "IsSaved"

#End Region

#Region "Status Constants"


        Public Const RankingCount As String = "RankingCount"
        Public Const StatusDataTable As String = "StatusDataTable"
        Public Const ActionDataTable As String = "ActionDataTable"
        Public Const StatusIdforEdit As String = "StatusIdforEdit"
        Public Const ActionId As String = "ActionId"
        Public Const StatusIdForAddAction As String = "StatusIdForAddAction"
        Public Const AccessTreeEmployeeName As String = "AccessTreeEmployeeName"
#End Region

#Region "Letters & Documents"

        Public Const LetterDocList As String = "LetterDocList"
        Public Const StandardLetterId As String = "standardLetterId"
        Public Const UniqueStringLength As String = "UniqueString"
#End Region

#Region "Certificate Expiry Report"

        Public Const certificateExpiryReportType As String = "certificateExpiryReportType"

#End Region

#Region "Property Summary Tab"
        Public Const FaultsResultDataSet As String = "FaultsResultDataSet"
        Public Const JournalHistoryId As String = "JournalHistoryId"
        Public Const CreationDate As String = "CREATIONDATE"
        Public Shared CurrentIndex As String = "CurrentIndex"
        Public Const TotalJobsheets As String = "TotalJobsheets"

#End Region

#Region "Defect Management"

        Public Const DefectCategoryId As String = "DefectCategoryId"
        Public Const DefectsDataTable As String = "DefectsDataTable"
        Public Const ApplianceDefectBO As String = "ApplianceDefectBO"

#End Region

#Region "Assign To Contractor"
        Public Const WorkRequiredDT As String = "WorkRequiredDT"
        Public Const AssignToContractorWorkType As String = "AssignToContractorWorkType"
#End Region




#Region "Assign To Contractor"
        Public Shared CurrentIndexForAssignGasServicingToContractor As String = "CurrentIndex"
        Public Shared ServiceRequiredDT As String = "ServiceRequiredDT"
#End Region





    End Class
End Namespace