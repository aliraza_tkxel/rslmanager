﻿Namespace AS_Utilities


    Public Class PathConstants

#Region "Page paths"
        Public Shared LetterWindowPath As String = "../../Views/Resources/SaveLetterAsPdf.aspx"
        Public Shared DownloadWindowPath As String = "../../Views/Common/Download.aspx"
        Public Shared WeeklyCalendarPath As String = "WeeklyCalendar.aspx"
        Public Shared MonthlyCalendarPath As String = "MonthlyCalendar.aspx"
        Public Shared SerchCalendarPath As String = "CalendarSearch.aspx"
        Public Shared PhotographPath As String = "../../Photographs/"
        Public Shared LoadAccessTree As Boolean = False
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const BridgePath As String = "~/Bridge.aspx?cmd=rsl"
        Public Const DashboardPath As String = "~/Views/Dashboard/Dashboard.aspx"
        Public Const AccessDeniedPath As String = "~/AccessDenied.aspx"
        Public Const ResourcesAccessDeniedPath As String = "~/Views/Resources/ResourcesAccessDenied.aspx"
        Public Const PropertyModuleAccessDeniedPath As String = "../../../PropertyModule/Views/General/AccessDenied.aspx"
        Public Const MaintenanceAccessDeniedPath As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=accessdenied&mn=Maintenance"
        Public Const AdminAccessDeniedPath As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=accessdenied&mn=Admin"
        Public Const ReportsAccessDeniedPath As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=accessdenied&mn=Reports"
        Public Const ResourcesPath As String = "~/Views/Resources/Resources.aspx"
        Public Const SeparatorImagePath As String = "../Images/PropertyModule/sep.jpg"
        Public Const PropertyRecordPath As String = "~/../RSLApplianceServicing/Bridge.aspx?pid="

        'To go directly to property module if the property id is set.
        Public Const PropertyModule As String = "~/Views/Property/PropertyRecord.aspx"
        Public Const PropertyModuleSearch As String = "../../../PropertyModule/Bridge.aspx"
        Public Const PropertyListing As String = "~/Views/Property/Properties.aspx"


        Public Const StatusActionPath As String = "~/Views/Resources/StatusActionMain.aspx"
        Public Const ViewLettersPath As String = "~/Views/Resources/ViewLetters.aspx"
        Public Const AdminSchedulingPath As String = "~/Views/Scheduling/AdminScheduling.aspx"
        Public Const WeeklyCalendarPagePath As String = "~/Views/Calendar/WeeklyCalendar.aspx"
        Public Const CertificateExpiryPath As String = "~/Views/Reports/CertificateExpiry/CertificateExpiry.aspx"
        Public Const IssuedCertificatesPath As String = "~/Views/Reports/IssuedCertificates/IssuedCertificates.aspx"
        Public Const PrintCertificatesPath As String = "~/Views/Reports/PrintCertificates/PrintCertificates.aspx"
        Public Const FuelServicingPath As String = "~/Views/Scheduling/FuelScheduling.aspx"
        Public Const VoidServicingPath As String = "~/Views/Scheduling/VoidScheduling.aspx"
        Public Const PropertiesPath As String = "~/Views/Property/Properties.aspx"

        Public Const AlternativeServicingPath As String = "~/Views/Scheduling/AlternativeServicing.aspx"
        Public Const ToBeArrangedAvailableAppointments As String = "~/Views/Scheduling/ToBeArrangedAvailableAppointments.aspx"
        Public Const AlternativeArrangedAppointments As String = "~/Views/Scheduling/AlternativeArrangedAppointments.aspx"

        Public Const OilServicingPath As String = "~/Views/Scheduling/OilServicing.aspx"
        Public Const OilToBeArrangedAvailableAppointments As String = "~/Views/Scheduling/OilToBeArrangedAvailableAppointments.aspx"
        Public Const OilArrangedAppointments As String = "~/Views/Scheduling/OilArrangedAppointments.aspx"

        'ME Servicing Pages
        Public Const MEServicingAPTBA As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=meservicing&mn=Servicing"
        Public Const MEServicingAppointmentArranged As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=meservicing&tab=aptArranged&mn=Servicing"
        Public Const MEServicingNoEntries As String = "~/../PropertyDataRestructure/Bridge.aspx?"
        'Assign To Contractor Report
        Public Const AssignToContractorReportPath As String = "~/Views/Reports/AssignedToContractor/AssignedToContractor.aspx"
        Public Const DefectApprovalReportPath As String = "~/Views/Reports/DefectApproval/DefectApproval.aspx"
        Public Const SMSURL As String = "~/../Includes/SMS.asp"
        Public Const PlannedJobSheet As String = "~Views/Common/JobSheetSummary.aspx"
        Public Const SchemeRecordDashboard As String = "~/../PropertyDataRestructure/Bridge.aspx?pg=schemerecord&id="

#End Region

#Region "Page Name"

        Public Const DashdoardPageKey As String = "dashboard"
        Public Const StatusActionPageKey As String = "statusactionmain"
        Public Const ViewLettersPageKey As String = "viewletters"
        Public Const AdminSchedulingPageKey As String = "adminscheduling"
        Public Const WeeklyCalendarPageKey As String = "weeklycalendar"
        Public Const CertificateExpiryPageKey As String = "certificateexpiry"
        Public Const IssuedCertificatesPageKey As String = "issuedcertificates"
        Public Const PrintCertificatesPageKey As String = "printcertificates"
        Public Const FuelServicingPageKey As String = "fuelservicing"

        Public Const AlternativeServicingPageKey As String = "alternativeservicing"
        Public Const OilServicingPageKey As String = "oilservicing"

        Public Const VoidServicingPageKey As String = "voidservicing"
        Public Const PropertiesPageKey As String = "properties"
        Public Const DefectSchedulingPageKey As String = "defectScheduling"
        Public Const AssignToContractorPageKey As String = "atcr"
        Public Const DefectApprovalPageKey As String = "dapr"
        Public Const ViewPlannedJobSheetKey As String = "PlannedJobSheet"
#End Region

#Region "Query String Constants"

        Public Const PropertyID As String = "pid"
        Public Const cmd As String = "cmd"
        Public Const PropertyIds As String = "id"
        Public Const Resources As String = "resources"
        Public Const Yes As String = "yes"
        Public Const JournalHistoryId As String = "JournalHistoryId"
        Public Const JournalId As String = "JournalId"
        Public Const Page As String = "pg"
        Public Const Src As String = "src"
        Public Const NewProperty As String = "new"
        Public Const AddProperty As String = "add"
        Public Const Tab As String = "tab"
        Public Const AppointmentArranged As String = "aptArranged"
        Public Const Status As String = "st"
        Public Const NoEntry As String = "ne"
        Public Const Aborted As String = "ab"
        Public Const PatchId As String = "phid"
        Public Const SchemeId As String = "shid"

#End Region

#Region "Property Image Path"
        Public Const AppliancesImagePath As String = "../../../RSLApplianceServicing/Photographs/Images/"
#End Region

#Region "Page File Name"

        Public Const CalendarPage As String = "Calendar.aspx"
        Public Const CalendarSearchPage As String = "CalendarSearch.aspx"
        Public Const MonthlyCalendarPage As String = "MonthlyCalendar.aspx"
        Public Const WeeklyCalendarPage As String = "WeeklyCalendar.aspx"
        Public Const DownloadPage As String = "Download.aspx"
        Public Const NotInReleasePage As String = "NotInRelease.aspx"
        Public Const PrintCP12DocumentPage As String = "PrintCP12Document.aspx"
        Public Const PrintLetterPage As String = "PrintLetter.aspx"
        Public Const UploadDocumentPage As String = "UploadDocument.aspx"
        Public Const DashboardPage As String = "Dashboard.aspx"
        Public Const MainPagePage As String = "MainPage.aspx"
        Public Const PrintActivitiesPage As String = "PrintActivities.aspx"
        Public Const PrintJobSheetDetailPage As String = "PrintJobSheetDetail.aspx"
        Public Const PropertyRecordPage As String = "PropertyRecord.aspx"
        Public Const CertificateExpiryPage As String = "CertificateExpiry.aspx"
        Public Const IssuedCertificatesPage As String = "IssuedCertificates.aspx"
        Public Const PrintCertificatesPage As String = "PrintCertificates.aspx"
        Public Const PrintPropertyCasePage As String = "PrintPropertyCase.aspx"
        Public Const ReportsPage As String = "Reports.aspx"
        Public Const AccessPage As String = "Access.aspx"
        Public Const CreateLetterPage As String = "CreateLetter.aspx"
        Public Const EditLetterPage As String = "EditLetter.aspx"
        Public Const LettersPage As String = "Letters.aspx"
        Public Const PreviewLetterPage As String = "PreviewLetter.aspx"
        Public Const ResourcesPage As String = "Resources.aspx"
        Public Const SaveLetterAsPdfPage As String = "SaveLetterAsPdf.aspx"
        Public Const StatusPage As String = "Status.aspx"
        Public Const StatusActionMainPage As String = "StatusActionMain.aspx"
        Public Const UsersPage As String = "Users.aspx"
        Public Const ViewLetterPage As String = "ViewLetter.aspx"
        Public Const ViewLettersPage As String = "ViewLetters.aspx"
        Public Const AdminSchedulingPage As String = "AdminScheduling.aspx"
        Public Const FuelSchedulingPage As String = "FuelScheduling.aspx"

        Public Const AlternativeServicingPage As String = "AlternativeServicing.aspx"
        Public Const ToBeArrangedAvailableAppointmentsPage As String = "ToBeArrangedAvailableAppointments.aspx"
        Public Const AlternativeArrangedAppointmentsPage As String = "AlternativeArrangedAppointments.aspx"

        Public Const OilSchedulingPage As String = "OilServicing.aspx"
        Public Const OilToBeArrangedAvailableAppointmentsPage As String = "OilToBeArrangedAvailableAppointments.aspx"
        Public Const OilArrangedAppointmentsPage As String = "OilArrangedAppointments.aspx"

        Public Const VoidSchedulingPage As String = "VoidScheduling.aspx"
        Public Const AccessDeniedPage As String = "AccessDenied.aspx"
        Public Const PropertiesPage As String = "Properties.aspx"
        Public Const DefectSchedulingPage As String = "DefectScheduling.aspx"
        Public Const DefectBasketPage As String = "DefectBasket.aspx"
        Public Const DefectAvailableAppointmentsPage As String = "DefectAvailableAppointments.aspx"
        Public Const DefectJobSheetPage As String = "DefectJobSheetSummary.aspx"
        Public Const ApplianceDefectReportPage As String = "ApplianceDefectReport.aspx"
        Public Const CancelledDefectReportPage As String = "CancelledDefectReport.aspx"
        Public Const DisconnectedApplianceReport As String = "DisconnectedApplianceReport.aspx"
        Public Const DefectsHistoryReport As String = "DefectHistoryReport.aspx"
        'Assign to Contractor Report
        Public Const AssignedToContractorReport As String = "AssignedToContractor.aspx"
        Public Const DefectApprovalReport As String = "DefectApproval.aspx"
        Public Const ViewPlannedJobSheet As String = "JobSheetSummary.aspx"
        Public Const ViewACPlannedJobSheet As String = "JobSheetSummaryAC.aspx"

        Public Const ViewPrintJobSheetSummary As String = "PrintJobSheetSummary.aspx"
#End Region

#Region "Defect Management"

        Public Const ScheduleDefect As String = "~/Views/Scheduling/DefectScheduling.aspx"
        Public Const AvailableProperties As String = "/PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=ap"
        Public Const VoidAppointmentsToBeArranged As String = "~/Views/Scheduling/VoidScheduling.aspx"
        Public Const VoidAppointmentsArranged As String = "~/Views/Scheduling/VoidScheduling.aspx"
        Public Const DefectBasket As String = "~/Views/Scheduling/DefectBasket.aspx"
        Public Const DefectAvailableAppointments As String = "~/Views/Scheduling/DefectAvailableAppointments.aspx"
        Public Const DefectJobSheet As String = "~/Views/Scheduling/DefectJobSheetSummary.aspx"
        Public Const DisconnectedApplianceReportPath As String = "~/Views/Reports/Defects/DisconnectedApplianceReport.aspx"
        Public Const DefectsRequiringApprovalPath As String = "~/Views/Reports/DefectApproval/DefectApproval.aspx"

#End Region

    End Class

End Namespace