﻿Imports System
Namespace AS_Utilities

    Public Class SpNameConstants

#Region "Master Page"

        Public Shared GetPropertyMenuList As String = "P_GetPropertyMenuList"
        Public Shared GetPropertyMenuPages As String = "P_GetPropertyMenuPages"

#End Region

#Region "ViewDetails"

        'Executed for Retrieving data for view screen
        Public Shared GetCustomerInfo As String = "TO_CUSTOMER_GETCUSTOMERINFO"


#End Region

#Region "Schedule Appointments"
        Public Shared GetPropertyFuelTypes As String = "AS_GetPropertyFuelTypes"
        Public Shared GetAlternativeAppointmentsHeaderByProperty As String = "AS_GetAlternativeAppointmentsHeaderByProperty"
        Public Shared GetOilAppointmentsHeaderByProperty As String = "AS_GetOilAppointmentsHeaderByProperty"

        Public Shared GetHeatingAppointmentDuration As String = "AS_GetHeatingAppointmentDuration"
        Public Shared SetHeatingAppointmentDuration As String = "AS_SetHeatingAppointmentDuration"
        Public Shared UpdateHeatingAppointmentDuration As String = "AS_UpdateHeatingAppointmentDuration"

        Public Shared GetHeatingInfoForOil As String = "AS_GetHeatingInfoForOil"
        Public Shared GetHeatingInfoForAlternative As String = "AS_GetHeatingInfoForAlternative"
        Public Shared GetSchemeBlockBoilersInfo As String = "AS_GetSchemeBlockBoilersInfo"
        Public Shared GetAppointmentToBeArranged As String = "AS_AppointmentsToBeArranged"

        Public Shared GetAlternativeAppointmentsByProperty As String = "AS_GetAlternativeAppointmentsByProperty"
        Public Shared AlternativeAppointmentsToBeArranged As String = "AS_AlternativeAppointmentsToBeArranged"
        Public Shared AlternativeAppointmentsArranged As String = "AS_AlternativeAppointmentsArranged"

        Public Shared OilAppointmentsToBeArranged As String = "AS_OilAppointmentsToBeArranged"
        Public Shared OilAppointmentsArranged As String = "AS_OilAppointmentsArranged"

        Public Shared GetVoidAppointmentToBeArranged As String = "AS_VoidAppointmentsToBeArranged"
        Public Shared GetAppointmentsArranged As String = "AS_AppointmentsArranged"
        Public Shared GetVoidAppointmentsArranged As String = "AS_VoidAppointmentsArranged"
        Public Shared GetAppointmentDetail As String = "AS_GetAppointmentDetail"
        Public Shared SaveAppointmentNotes As String = "AS_SaveAppointmentNotes"
        Public Shared SaveSmsAndEmailStatus As String = "AS_SaveSmsAndEmailStatus"
        Public Shared SavePushNoticificationStatus As String = "AS_SavePushNoticificationStatus"
        Public Shared SetPropertyScheduleStatus As String = "AS_SetPropertyScheduleStatus"
        Public Shared GetPropertyScheduleStatus As String = "AS_GetPropertyScheduleStatus"
        Public Shared GetArrangedPropertyScheduleStatus As String = "AS_GetPropertyScheduleStatusArranged"
        Public Shared GetPropertyLockedBy As String = "AS_GetPropertyLockedBy"

        Public Shared GetSchedulingPropertyLockedBy As String = "AS_GetSchedulingPropertyLockedBy"
        Public Shared SetSchedulingPropertyStatus As String = "AS_SetSchedulingPropertyStatus"
        Public Shared GetSchedulingPropertyStatus As String = "AS_GetSchedulingPropertyStatus"
        Public Shared GetArrangedSchedulingPropertyStatus As String = "AS_GetSchedulingPropertyStatusArranged"

        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetSearchedValues As String = "AS_AppointmentArrangedSearchedValues"
        Public Shared GetAppointmentEngineersInfo As String = "AS_AppointmentEngineerInfo"
        Public Shared GetScheduledAppointmentsDetail As String = "AS_ScheduledAppointmentsDetail"
        Public Shared GetAppointmentInfoForEmail As String = "AS_GetAppointmentInfoForEmail"
        Public Shared SaveAppointment As String = "AS_SaveAppointment"

        Public Shared AmendAppointment As String = "AS_AmendAppointment"
        Public Shared GetSelectedAppointmentInfo As String = "AS_GetSelectedAppointmentInfo"

        Public Shared GetJointTenantsInfoByTenancyID As String = "AS_GetJointTenantsInfoByTenancyID"
        Public Shared GetAvailableOperatives As String = "AS_GetAvailableOperatives"

        Public Shared GetAvailableOperativesForAlternativeOilFuel As String = "AS_GetAvailableOperativesForAlternativeOilFuel"

        Public Shared GetCustomerRiskVulnerability As String = "AS_GetCustomerRiskVulnerability"
        Public Shared GetAttributeNotesByPropertyID As String = "AS_GetAttributeNotesByPropertyID"

        Public Shared GetHeatingFuelNotes As String = "AS_GetHeatingFuelNotes"

        Public Shared GetRiskList As String = "AS_GetRiskDetails"
        Public Shared GetAbortReason As String = "AS_GetAbortReason"
        Public Shared GetPropertyCustomerEmail As String = "AS_GetPropertyCustomerEmail"
        Public Shared UpdatePropertyCustomerEmail As String = "AS_UpdatePropertyCustomerEmail"
        Public Shared GetPropertyEmployeeEmail As String = "AS_GetPropertyEmployeeEmail"
        Public Shared GetEmployeeWorkMobile As String = "AS_GetEmployeeWorkMobile"

        Public Shared SaveGasServicingCancellation As String = "AS_GasServicingCancellation"
        Public Shared SaveGasServicingCancellationForSchemeBlock As String = "AS_GasServicingCancellationForSchemeBlock"
        Public Shared AS_GetContractorEmailDetail As String = "AS_GetContractorEmailDetail"
        Public Shared AS_GetOriginatorEmailDetail As String = "AS_GetOriginatorEmailDetail"

#End Region

#Region "Appointments Calendar"
        Public Shared GetAppointmentsCalendarDetail As String = "AS_AppointmentsCalendarDetail"
        Public Shared GetAppointmentCalendarEngineersInfo As String = "AS_AppointmentCalendarEngineerInfo"
        Public Shared GetEngineersLeaveInformation As String = "AS_GetEngineersLeaveInformation"
        Public Shared GetSearchedAppointmentDetail As String = "AS_SearchedAppointmentDetail"
        Public Shared GetAppointmentsMonthlyCalendarDetail As String = "AS_AppointmentsMonthlyCalendarDetail"
        Public Shared GetEmployeeCoreWorkingHours As String = "FL_GetEmployeeCoreWorkingHours"
#End Region

#Region "Resources "
        Public Shared GetUsers As String = "AS_GetUsers"
        Public Shared GetStaus As String = "AS_Statuses"
        Public Shared GetCertificateNames = "AS_CertificateNames"
        Public Shared GetUserTypes As String = "AS_UserTypes"
        Public Shared GetPatchLocations As String = "AS_PatchLocation"
        Public Shared GetSchemeNames As String = "AS_SchemeNames"
        Public Shared GetQuickFindUsers As String = "AS_QuickFindUsers"
        Public Shared GetInspectionTypes As String = "AS_InspectionTypes"
        Public Shared GetInspectionTypesByDesc As String = "AS_InspectionTypesByDesc"
        Public Shared UpdateResource As String = "AS_UpdateUser"
        Public Shared DeleteUser As String = "AS_DeleteUser"
        Public Shared saveUser As String = "AS_SaveUser"
        Public Shared SaveUserInspectionType As String = "AS_SaveUserInspectionType"
        Public Shared SavePatchID As String = "AS_SavePatchID"
        Public Shared SaveDevelopmentID As String = "AS_SaveDevelopmentID"
        Public Shared GetActionsByStatusID As String = "AS_GetActionByStatusId"
        Public Shared GetSignOffTypes As String = "AS_GetSignOff"
        Public Shared GetTeams As String = "AS_GetTeams"
        Public Shared GetFromUsersFromTeamID As String = "AS_GetFromUserByTeamID"
        Public Shared GetFromTelEmailPrintLetterByEmployeeID As String = "AS_GetFromTelEmailPrintLetterByEmployeeID"
        Public Shared GetPages As String = "AS_GetPages"
        Public Shared GetPagesByParentId As String = "AS_GetPagesByParentId"
        Public Shared SaveUserRights As String = "AS_SaveUserRights"
        Public Shared GetPageByEmployeeId As String = "AS_GetPageByEmployeeId"
        Public Shared GetEmployeeIdByName As String = "AS_GetEmployeeIdByName"
        Public Shared EditStatus As String = "AS_EditStatus"
        Public Shared DeleteUserPages As String = "AS_DeleteUserPages"
        Public Shared EditAction As String = "AS_EditAction"
        Public Shared GetCoreAndOutOfHoursInfo As String = "AS_GetCoreAndOutOfHoursInfo"
        Public Shared SaveOutOfHoursInfo As String = "AS_SaveOutOfHoursInfo"
        Public Shared SaveCoreWorkingHoursInfo As String = "AS_SaveCoreWorkingHoursInfo"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetAllTrades As String = "FL_GetFaultTrades"
        Public Shared GetAllTradesLookups As String = "FL_GetTradeLookUpAll"
        Public Shared SaveSelectedTrades As String = "E_Trade_Insert"
        Public Shared GetEmpTrades As String = "AS_GetEmpTrades"
        Public Shared SaveoftecGassafe As String = "E_UpdateOftecGasSafe"
        Public Shared GetEmployeeEmailForAsbestosEmailNotifications As String = "AS_GetEmployeeEmailForAsbestosEmailNotifications"



#Region "Edit User Procedures"
        Public Shared GetEidtUsers As String = "AS_GetEidtUsers"
        Public Shared GetEditUserPatchInfo As String = "AS_GetEditUserPatchInfo"
        Public Shared GetEditUserInpectionTypeInfo As String = "AS_GetEditUserInpectionTypeInfo"

#End Region

#Region "UPdate User Procedures"
        Public Shared UpdateUsers As String = "AS_UpdateUserData"
        Public Shared UpdateUserPatchInfo As String = "AS_UpdatePatchId"
        Public Shared DeleteUserInpectionTypeInfo As String = "AS_DeleteUserInspectionType"
        Public Shared UpdateDevelopmentID As String = "AS_UpdateDevelopmentId"

#End Region

#Region "Status Action Procedures"
        Public Shared AddStatus As String = "AS_AddUpdateStatus"
        Public Shared GetAction As String = "AS_GetAction"
        Public Shared AddAction As String = "AS_AddAction"
        Public Shared GetActionRankingByStatusId As String = "AS_GetActionRankingByStatusId"
#End Region

#End Region

#Region "Property Area"
        Public Shared GetContractorJobSheetSummary As String = "AS_GetContractorJobSheetSummary"
        Public Shared GetApplianceCancelledNotes As String = "AS_GetCancelNotes"
        Public Shared PropertyActivities As String = "AS_PropertyActivities"
        Public Shared PropertyDocuments As String = "AS_PropertyDocuments"
        Public Shared GetPropertyAddress As String = "AS_GetPropertyAddress"
        Public Shared SavePropertyActivity As String = "AS_SavePropertyActivity"
        Public Shared SaveDocuments As String = "AS_SaveDocuments"
        Public Shared SaveDocumentsForSchemeBlock As String = "AS_SaveDocumentsForSchemeBlock"
        Public Shared GetApplianceLocations As String = "AS_GetApplianceLocations"
        Public Shared GetFuelType As String = "AS_GetFuelType"
        Public Shared GetFuelTypeForAlternativeFuel As String = "AS_GetFuelTypeForAlternativeFuel"
        Public Shared GetApplianceType As String = "AS_GetApplianceType"
        Public Shared GetMake As String = "AS_GetMake"
        Public Shared GetModelsList As String = "AS_GetModelsList"
        Public Shared GetFlueType As String = "AS_GetFlueType"
        Public Shared SavePropertyAppliance As String = "AS_SavePropertyAppliance"
        Public Shared DeletePropertyAppliance As String = "AS_DeletePropertyAppliance"
        Public Shared GetPropertyType As String = "AS_GetPropertyType"
        Public Shared PropertyRbRc As String = "AS_PropertyRbRc"
        Public Shared GetPropertyDocument As String = "AS_GetPropertyDocument"
        Public Shared getLocations As String = "AS_Locations"
        Public Shared getAreasByLocationId As String = "AS_AreaByLocationId"
        Public Shared getItemsByAreaId As String = "AS_ItemByAreaId"
        Public Shared getSubItemsByItemId As String = "AS_SubItemByItemId"
        Public Shared getItemByItemId As String = "AS_ItemByItemId"
        Public Shared getAllMeterLocations As String = "AS_MeterLocations"
        Public Shared getPropertyGasInfo As String = "AS_getPropertyGasInfo"
        Public Shared updatePropertyGasInfo As String = "AS_updatePropertyGasInfo"
        Public Shared getPropertyCategory As String = "AS_GetDefectCategory"
        Public Shared getPropertyAppliances As String = "AS_GetPropertyAppliances"
        Public Shared saveDefect As String = "AS_SaveDefect"
        Public Shared saveDefectForSchemeBlock As String = "PDR_SaveDefectForSchemeBlock"
        Public Shared addCategory As String = "AS_AddCategory"
        Public Shared getPropertyAppliancesByApplianceId = "AS_getPropertyAppliancesByApplianceId"
        Public Shared getPropertyDefectDetails As String = "AS_getPropertyDefectDetails"
        Public Shared GetSchemeBlockDefectDetails As String = "PDR_getSchemeBlockDefectDetails"
        Public Shared GetParametersByAreaId As String = "AS_GetParametersByAreaId"
        Public Shared getPropertyImages As String = "AS_getPropertyImages"
        Public Shared savePhotograph As String = "AS_SavePhotograph"
        Public Shared saveEPCPhotograph As String = "AS_SaveEPCPhotograph"
        Public Shared getPropertyDefectImages As String = "AS_getPropertyDefectImages"
        Public Shared getItemDetails As String = "AS_getItemDetails"
        Public Shared getApplianceCaseDetails As String = "AS_GetApplianceCaseDetail"
        Public Shared GetPropertyDetails As String = "AS_GetPrintActivityDetails"
        Public Shared PropertyAppliances As String = "AS_PropertyAppliances"
        Public Shared GetCP12DocumentByLGSRID As String = "AS_GetCP12DocumentByLGSRID"
        Public Shared GetPropertyInspectionDocumentByInspectionId As String = "AS_GetDetailsDocumentByInspectionId"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetSearchPropertyAndScheme As String = "P_GetSearchPropertyAndScheme"
        Public Shared GetTenancyPropertyDetail As String = "P_GetTenancyPropertyDetail"
        Public Shared GetAccommodationSummary As String = "P_GetAccommodationSummary"
        Public Shared GetTenancyHistory As String = "P_GetTenancyHistory"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetAsbestosDetail As String = "P_GetAsbestosInformation"
        Public Shared GetAsbestosInformation As String = "AS_GetAsbestosInformation"
        Public Shared GetPlannedAppointments As String = "AS_GetPlannedAppointments"
        Public Shared GetPlannedComponentsDetail As String = "AS_GetPlannedComponentsDetail"
        Public Shared GetPropertyConditionRatingDetail As String = "AS_GetPropertyConditionRatingDetail"
        Public Shared GetAllAppointments As String = "AS_GetAllAppointments"
        Public Shared GetPropertySummaryImages As String = "P_GetPropertyImages"
        Public Shared GetPropertySummaryEPCImages As String = "P_GetPropertyEPCImages"
        Public Shared GetFaultsAndDefects As String = "AS_GetFaultsAndDefects"
        Public Shared GetPropertySummaryAddress As String = "P_GetPropertyAddress"
        Public Shared GetJobSheetDetail As String = "FL_GetJobSheetDetail"
        Public Shared GetCP12DocumentByLGSRHISTORYID = "AS_GetCP12DocumentByLGSRHISTORYID"
        Public Shared GetPlannedInspectionDocument = "AS_GetPlannedInspectionDocument"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared getAllRoofType As String = "AT_RoofType"
        Public Shared getItemDetail As String = "AS_GetItemDetail"
        Public Shared amendPropertyItemDetail As String = "AS_AmendPropertyItemDetail"
        Public Shared saveItemNotes As String = "AS_SaveItemNotes"
        Public Shared getItemNotes As String = "AS_getItemNotes"
        Public Shared UpdateItemNotes As String = "AS_UpdateItemNotes"
        Public Shared DeleteItemNotes As String = "AS_DeleteItemNotes"
        Public Shared GetItemNotesTrail As String = "AS_GetItemNotesTrail"
        Public Shared GetJobSheetSummaryDetails As String = "PLANNED_GetJobSheetSummaryDetails"
        Public Shared getAttributeSummaryDetail As String = "AS_getAttributeSummaryDetail"
        Public Shared getRoomsDimensionsByPropertyId As String = "P_GetRoomsDimensionsByPropertyId"
        Public Shared GetWarrantyList As String = "PDR_GetWarrantyList"
        Public Shared GetWarrantyTypes As String = "PDR_GetWarrantyTypes"
        Public Shared GetForDropDownValues As String = "PDR_GetAreaItem"
        Public Shared GetCategoryDropDownValues As String = "AS_GetPropertyLocations"
        Public Shared GetAreaDropDownValuesByCategory As String = "AS_GetPropertyAreaByLocationId"
        Public Shared GetItemDropDownValuesByArea As String = "AS_GetPropertyItemByAreaId"
        Public Shared GetContractors As String = "PDR_GetContractor"
        Public Shared SaveWarranty As String = "PDR_SaveWarranty"
        Public Shared GetWarrantyData As String = "PDR_GetWarrantyData"
        Public Shared DeleteWarranty As String = "PDR_DeleteWarranty"
        Public Shared GetSelectedStatusType = "AS_GetSelectedTypeAppointmentsArranged"

        Public Shared SavePropertyImage As String = "SavePropertyImage" 'new SP
        Public Shared DefectsRequiringApproval As String = "DF_GetDefectsToBeApprovedListCount"
        Public Shared DeleteHeating As String = "AS_DeleteHeating"

#End Region

#Region "Dashboard"

        Public Shared CountNoEntries As String = "AS_GetCountNoEntry"
        Public Shared CountLegalProceedings As String = "AS_GetCountLegalProceedings"
        Public Shared CountExpired As String = "AS_GetCountExpired"
        Public Shared CountExpiresIn56Days As String = "AS_GetCountExpiresIn56Days"
        Public Shared CountAppointmentsArranged As String = "AS_GetCountAppointmentsArranged"
        Public Shared CountVoidAppointmentsArranged As String = "AS_GetCountVoidAppointmentsArranged"
        Public Shared CountAppointmentsToBeArranged As String = "AS_GetCountAppointmentsToBeArranged"
        Public Shared CountVoidAppointmentsToBeArranged As String = "AS_GetCountVoidAppointmentsToBeArranged"
        Public Shared CountVoidProperties As String = "AS_GetCountVoidProperties"

        Public Shared CountMIData As String = "AS_GetCountMIData"

        Public Shared ChartCertificateExpiry As String = "AS_Get6MonthExpiry"

        Public Shared InspectionStatusNoEntries As String = "AS_GetNoEntries"
        Public Shared InspectionStatusAbort As String = "AS_GetAbortAppointments"

        Public Shared GetServicingList As String = "AS_GetServicingList"
        Public Shared InspectionStatusLegalProceedings As String = "AS_GetLegalProceedings"
        Public Shared InspectionStatusCertificatesExpired As String = "AS_GetExpired"
        Public Shared InspectionStatusAppointmentsToBeArranges As String = "AS_GetAppointmentsToBeArranged"

        Public Shared GetLastLoginDate As String = "AS_GetLastLogin"
        Public Shared GetRSLModulesList As String = "P_GetRSLModulesList"

        Public Shared getServicingTypes As String = "AS_GetServicingTypes"
        'Public Shared getServicingTypes As String = "AS_GetFuelType"
        Public Shared GetBlocks As String = "PDR_GetBlocks"
        Public Shared GetScheme As String = "PDR_GetDevelopmentScheme"

        Public Shared MECountNoEntries As String = "PDR_GetCountNoEntry"
        Public Shared MECountOverdue As String = "PDR_GetCountMEOverdue"
        Public Shared MECountAppointmentArranged As String = "PDR_GetCountArrangedAppointments"
        Public Shared MECountAppointmentToBeArranged As String = "PDR_GetCountAppointmentToBeArranged"


#End Region

#Region "Login"
        Public Shared GetEmployeeById As String = "AS_GetEmployeeById"
#End Region

#Region "Report"
        Public Shared GetExpiredCertificateInfo As String = "AS_GetExpiredCertificateInfo"
        Public Shared GetExpiredCertificateStats As String = "AS_GetExpiredCertificateStats"
        Public Shared GetIssuedCertificateReport As String = "AS_GetIssuedCertificateReport"
        Public Shared GetPrintCertificateReport As String = "AS_GetPrintCertificateReport"
        Public Shared UpdatePrintedStatus As String = "AS_UpdatePrintedStatus"
        Public Shared SavePrintCertificateActivity As String = "AS_SavePrintCertificateActivity"
        Public Shared GetTenantsByPropertyId As String = "AS_GetTenantsByPropertyId"
        Public Shared SaveEmailCertificateActivity As String = "AS_SaveEmailCertificateActivity"
        Public Const GetDefectMoreDetail As String = "DF_GetDefectMoreDetail"
#End Region

#Region "Letter "
        Public Shared GetLetters As String = "AS_GetLetters"
        Public Shared AddStandardLetter As String = "AS_AddStandardLetter"
        Public Shared AddSavedLetter As String = "AS_AddSavedLetter"
        Public Shared GetLetterById As String = "AS_GetStandardLetterById"
        Public Shared UpdateStandardLetter As String = "AS_UpdateStandardLetterTemplate"
        Public Shared DeleteStandardLetter As String = "AS_DeleteStandardLetterTemplate"
        Public Shared GetSavedLetterById As String = "AS_GetSavedLetterById"
        Public Shared getLettersByStatusId As String = "AS_GetLettersByStatusId"
        Public Shared getLettersByActionId As String = "AS_GetLettersByActionId"
        Public Shared GetAlternativeLettersByActionId As String = "AS_GetAlternativeLettersByActionId"
        Public Shared PropertyLetter As String = "AS_PropertyLetters"
        Public Shared SaveEditedLetter As String = "AS_SaveEditedLetter"

        Public Shared SaveEditedLetterForSchemeBlock As String = "AS_SaveEditedLetterForSchemeBlock"
        Public Shared GetCustomerAndPropertyRbRc As String = "AS_GetCustomerAndPropertyRbRc"
#End Region

#Region "Document Tab"
        Public Shared SavePropertyDocumentInformation As String = "AS_SavePropertyDocumentInformation"
        Public Shared GetPropertyDocumentsInfo As String = "AS_GetPropertyDocumentsInformation"

        Public Shared DeletePropertyDocumentsByDocumentId As String = "AS_DeletePropertyDocumentsByDocumentId"
        Public Shared GetPropertyDocumentInformationById As String = "AS_GetPropertyDocumentInformationById"
        Public Shared GetLatestEpcDocument As String = "AS_GetLatestEpcDocument"
        Public Shared GetDocumentTypes As String = "P_GET_DOCUMENT_TYPES"
        Public Shared GetTypes As String = "PDR_GetDocumentTypes"

        Public Shared GetCategoryTypes As String = "PDR_GetCategories"
        Public Shared GetDocumentSubtypes As String = "P_GET_DOCUMENT_SUBTYPES"
        Public Shared GetEpcCategoryTypes As String = "P_GET_EPC_CATEGORY_TYPES"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetDocumentTypesSubtypes As String = "P_GET_TYPE_SUBTYPE"
#End Region

#Region "Health&Safety Tab"
        Public Shared getAsbestosAndRiskInformation As String = "AS_GetAsbestosAndRiskInformation"
        Public Shared getHealthAndSafetyTabInformation As String = "AS_GetHealthAndSafetyTabInformation"
        Public Shared saveAsbestosInformation As String = "AS_SaveAsbestosInformation"
        Public Shared saveRefurbishmentInformaiton As String = "INSERT_AND_RETRIVE_REFURBISHMENT_HISTORY"
        Public Shared getRefurbishmentInformaiton As String = "AS_GetRefurbishmentInformaiton"
        Public Shared getAsbestosRiskAndOtherInfo As String = "AS_getAsbestosRiskAndOtherInfo"
        Public Shared amendAsbestosInformation As String = "AS_AmendAsbestosInformation"

#End Region

#Region "Financial Tab"
        Public Shared getProperyFinancialInformation As String = "AS_GetProperyFinancialInformation"
        Public Shared getPropertyRentType As String = "AS_GetPropertyRentType"
        Public Shared getTargetRentDetail As String = "AS_GetTargetRentDetail"
        Public Shared savePropertyCurrentRentInformation As String = "AS_SavePropertyCurrentRentInformation"
        Public Shared savePropertyFinancialInformation As String = "AS_SavePropertyFinancialInformation"
        Public Shared savePropertyTargetRentInformation As String = "AS_SavePropertyTargetRentInformation"
        Public Shared getRentHistory As String = "AS_GetRentHistory"
        Public Shared getValuationHistory As String = "AS_GetValuationHistory"
#End Region

#Region "Status History Tab"
        Public Shared getStatusHistory As String = "AS_GetStatusHistory"
#End Region

#Region "Property Data Restructure"
        Public Shared GetPropertyList As String = "PDR_GetPropertyList"
        Public Shared PopulateAllDropDownList As String = "PDR_PopulateAllDropDownList"
        Public Shared GetPhaseList As String = "PDR_GetPhaseByDevelopmentId"
        Public Shared GetBlocksByDevelopmentId As String = "PDR_GetBlocksByDevelopmentID"
        Public Shared GetBlocksByBlockId As String = "PDR_GetBlocksByBlockID"
        Public Shared GetPropertySubStatus As String = "PDR_GetPropertySubStatus"
        Public Shared GetPropertyNROSHAssetTypeSub As String = "PDR_GetPropertyNROSHAssetTypeSub"
        Public Shared GetPropertyDetailByPropertyId As String = "PDR_GetPropertyDetailByPropertyId"
        Public Shared SavePropertyDetail As String = "PDR_SaveProperty"
        Public Shared GetCycleType As String = "PDR_GetCycleType"
        Public Shared GetPhase As String = "PDR_GetPhase"

#End Region

#Region "Defects Management"

        Public Const GetDefectsAppointmentToBeArrangedList As String = "DF_GetDefectsToBeArrangedList"
        Public Const GetDefectsArrangedList As String = "DF_GetDefectsArrangedList"
        Public Const GetAppliancesDefectsByJournalId As String = "DF_GetAppliancesDefectsByJournalId"
        Public Const GetDefectAvailableOperatives As String = "DF_GetAvailableOperatives"
        Public Const GetOperativeWorkingHours As String = "FL_GetOperativeWorkingHours"
        Public Const ScheduleDefectAppointment As String = "DF_ScheduleAppointment"
        Public Const GetDefectManagementLoopkupsData As String = "DF_GetDefectManagementLoopkupsData"

        Public Const GetApplianceDefectReport As String = "DF_GetApplianceDefectReport"
        Public Const GetDefectToBeApprovedReport As String = "DF_GetDefectsToBeApprovedList"
        Public Const GetDefectApprovedReport As String = "DF_GetDefectsApprovedList"
        Public Const GetDefectRejectedReport As String = "DF_GetDefectsRejectedList"
        Public Const ChangeDefectStatus As String = "DF_ChangeDefectStatus"
        Public Const GetRejectionReasons As String = "DF_GetRejectionReasons"
        Public Const GetDisconnectedAppliances As String = "DF_GetDisconnectedAppliancesReport"
        Public Const GetCancelledDefectsReport As String = "DF_GetCancelledDefectsReport"
        Public Const GetDefectAppointmentStatuses As String = "DF_GetDefectAppointmentStatuses"
        Public Const GetAllSchemes As String = "DF_GetAllSchemes"
        Public Const GetAllApplainceTypes As String = "DF_GetAllApplainceTypes"
        Public Const GetDefectJobSheetByDefectId As String = "DF_GetDefectJobSheetByDefectId"
        Public Const CancelDefectAppointment As String = "DF_CancelDefectAppointment"
        Public Const RearrangeDefectAppointment As String = "DF_RearrangeDefectAppointment"
        Public Const ConfirmDefectAppointmentsByDefectsId As String = "DF_ConfirmDefectAppointmentsByDefectsId"
        Public Const GetDefectHistoryReport As String = "DF_GetDefectHistoryReport"
        Public Const GetPropertyDefectsList As String = "DF_GetPropertyDefectsList"
        Public Const GetSchemeBlockDefectsList As String = "DF_GetSchemeBlockDefectsList"
        Public Const GetDetectorByPropertyId As String = "DF_GetDetectorByPropertyId"
        Public Const SaveDetectors As String = "DF_SaveDetectors"
#End Region

#Region "Customers"

        Public Const GetRiskAndVulnerabilityInfoByJournalId As String = "DF_GetRiskAndVulnerabilityInfoByJournalId"
        Public Const GetPropertyDetail As String = "PLANNED_GetPropertyDetail"
        Public Const GetSchemeBlockDetail As String = "AS_GetSchemeBlockDetail"
        Public Const AmendCustomerAddress As String = "FL_AmendCustomerAddress"

#End Region

#Region "Assign To Contractor"
        Public Const GetPlannedContractorDropDownValues As String = "PLANNED_GetPlannedContractorDropDownValues"
        Public Const GetCostCentreDropDownValues As String = "PLANNED_GetCostCentreDropDownValues"
        Public Const GetBudgetHeadDropDownValuesByCostCentreId As String = "PLANNED_GetBudgetHeadDropDownValuesByCostCentreId"
        Public Const GetExpenditureDropDownValuesByBudgetHeadId As String = "PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId"
        Public Const GetContactDropDownValuesbyContractorId As String = "PLANNED_GetContactDropDownValuesbyContractorId"
        Public Const GetDefectContractorDropDownValues As String = "DF_GetDefectContractorDropDownValues"
        Public Const GetVatDropDownValues As String = "PLANNED_GetVatDropDownValues"
        Public Const GetBlockListBySchemeID As String = "PDR_GetBlockListBySchemeID"
        Public Const GetAttribute As String = "FL_GetAttribute"
        Public Const GetArea As String = "FL_GetArea"
        Public Const SaveContractorAppointmentDetails As String = "DF_SaveContractorAppointmentDetails"
        Public Const SaveSbSubcontractorFault As String = "FL_SaveSbSubcontractorAppointmentDetails"
        Public Const GetDetailforEmailToContractor As String = "FL_GetDetailforEmailToContractor"
        Public Const GetContractorDetailforEmail As String = "DF_GetContractorDetailforEmail"
        Public Const GetContactEmailDetail As String = "DF_GetContactEmailDetail"
        Public Const GetReplacementDueForAssignToContractor As String = "FL_GetReplacementDueForAssignToContractor"
        Public Const GetApplianceInfo As String = "DF_GetApplianceInfo"
#End Region
#Region "Job Sheet Summary Sub Contractor Update"

        Public Shared getPlannedJobSheetSummaryByJSN As String = "PLANNED_GetJobSheetSummaryByJSN"
        Public Shared getPlannedJobSheetSummaryByJSN_AC As String = "PLANNED_GetJobSheetSummaryByJSN_AC"
        Public Shared getPlannedStatusLookUp As String = "PLANNED_GetStatusLookUp"

#End Region
#Region "Assign Fuel servicing To Contractor"
        Public Const GetFuelServicingContractorDropDownValues As String = "AS_GetFuelServicingContractors"
        Public Const AssignWorkToContractor As String = "AS_AssignWorkToContractor"
        Public Const AssignWorkToContractorForSchemeBlock As String = "AS_AssignWorkToContractorForSchemeBlock"
        Public Const GetDetailforEmailToApplianceServicingContractor As String = "AS_GetDetailforEmailToContractor"
        Public Const GetAssignedGasServicingToContractorReport As String = "AS_AssignToContractorReport"
        Public Shared getJobSheetSummaryByJSN As String = "AS_GetJobSheetSummaryByJournalId"

        Public Shared GetJobSheetSummaryForSchemeBlockByJournalId As String = "AS_GetJobSheetSummaryForSchemeBlockByJournalId"

        Public Shared setAppointmentJobSheetStatus As String = "AS_UpdateJobSheet"
        Public Shared GetBudgetHolderByOrderId As String = "AS_GetBudgetHolderByOrderId"
        Public Shared GetPropertyBudgetHolderByOrderId As String = "FL_GetBudgetHolderByOrderId"
        Public Shared GetMaxCostValue As String = "AS_GetMaxCostValue"
        Public Shared CancelFuelPurchaseOrder As String = "AS_CancelFuelPurchaseOrder"
#End Region

        Public Shared GetHeadAndExpenditureId As String = "FL_GetHeadIdAndExpenditureId"

#Region "Property Assign To Contractor"

        Public Shared GetMeContractorDropDownValues As String = "PDR_GetMeContractorDropDownValues"
        Public Shared GetContractDetailByContractorId As String = "PDR_GetContractDetailByContractorId"
        Public Shared GetAreaDropDownValues As String = "PDR_GetArea"
        Public Shared GetAttributesSubItemsDropDownValuesByAreaId As String = "PDR_GetAttributesSubItemsByAreaID"
        Public Shared GetItemMSATDetailByItemId As String = "PDR_GetItemMSATDetailByItemId"
        Public Shared AssignWorkToContractorForPropertyPO As String = "PDR_PropertyAssignWorkToContractor"
        Public Shared GetPropertyDetailforEmailToContractor As String = "PDR_GetPropertyDetailforEmailToContractor"
        Public Shared GetAssignToContractorDetailByJournalId As String = "PDR_GetAssignToContractorDetailByJournalId"
        Public Shared GetReactiveRepairContractors As String = "V_GetReactiveRepairContractors"
        Public Shared GetAllContractorsDropDownValues As String = "PDR_GetAllContractors"
        Public Shared RequiredWorksAssignWorkToContractor As String = "V_RequiredWorksAssignWorkToContractor"
        Public Shared GetContractDetailByContractorIdForAssignToContractor As String = "PDR_GetContractDetailByContractorIdForAssignToContractor"
        Public Shared PaintPackAssignWorkToContractor As String = "V_PaintPackAssignWorkToContractor"
        Public Shared GetAttributeTypeForAssignToContractor As String = "PDR_GetAttributeTypeForAssignToContractor"
        Public Shared GetBlockListByScheme As String = "PDR_GetBlockListByScheme"
        Public Shared GetPropertiesByScheme As String = "PDR_GetPropertiesByScheme"
        Public Shared GetProvisionItemDetail As String = "PDR_GetProvisionDetailByProvisionId"
        Public Shared GetProvisions As String = "PDR_GetProvisions"        

#End Region

#Region "Restriction Tab"
        Public Shared SaveRestrictions As String = "PDR_SaveRestrictions"
        Public Shared GetRestrictions As String = "PDR_GetRestrictions"
        Public Shared GetNHBCWarrantyList As String = "PDR_GetNHBCWarrantyList"
        Public Shared GetLandRegistrationOptions As String = "PDR_GetLandRegistrationOptions"

#End Region


    End Class
End Namespace



