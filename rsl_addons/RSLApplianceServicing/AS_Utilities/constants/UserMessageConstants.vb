﻿
Namespace AS_Utilities


    Public Class UserMessageConstants

        Public Shared DuplicateNHBCError As String = "NHBC warranty already exists, Please enter different warranty type"
        Public Shared RecordNotFound As String = "No Records Found."
        Public Shared ProblemLoadingData As String = "Problem loading data."
        Public Shared ErrorDataBaseConnection As String = "Error while connecting with database."
        Public Shared NoRecordFound As String = "No Records Found."
        Public Shared InvalidUrl As String = "Url is invalid."
        Public Shared UserDoesNotExist As String = "You don't have access to appliance servicing. Please contant administrator."
        Public Shared UsersAccountDeactivated As String = "Your access to appliance servicing has been de-activiated. Please contant administrator."
        Public Shared LoginRslManager As String = "Please use the login from rsl manager."
        Public Shared ErrorDocumentUpload As String = "Error occurred while uploading the document.Please try again."
        Public Shared GasEngNotFound As String = "Gas engineer not found."
        Public Shared IncorrectDateInterval As String = "From date should not be later than to date"
        Public Shared InvalidJobSheetNumber As String = "Invalid Job Sheet Number"
        Public Shared InvalidMobile As String = "Invalid Mobile number."
        Public Shared AppointmentSavedSMSError As String = "Appointment saved but unable to send SMS due to : "
        Public Shared SelectAPDF As String = "Upload file must be a pdf"
        Public Shared SelectAFile As String = "Please select a file"
        Public Shared SuccessMessage As String = "Information has been saved successfully"
        Public Shared InvalidDate As String = "Removed Date should be greater than added date"
        Public Shared InvalidRemovedDate As String = "Removed date cannot be less than current date"
        Public Shared InvalidAsbestosLevel As String = "Abestos Level not available"
        Public Shared AsbestosDisable As String = "Abestos cannot be viewed. It has been removed already."
        Public Shared CancellationError As String = "You have already cancelled this appointment."
        Public Shared EnterReason As String = "Please enter a reason for cancellation."
        Public Shared FaultNotCancelled As String = "Appointment could not be cancelled"
        Public Shared FaultCancelled As String = "The appointment has been cancelled successfully"

        Public Shared InvalidAsbestosElement As String = "Asbestos Element does not match with existing elements"
        Public Shared IvalidRiskId As String = "Asbestos RiskID is not present"
        Public Shared RefurbishmentError As String = "Information is not added as refurbishment date already exists"
        Public Shared SelectTypeValidationError As String = "Please select type"
        Public Shared SelectTitleValidationError As String = "Please select title"
        Public Shared SelectCategoryValidationError As String = "Please select Category"
        Public Shared Cp12ValidationError As String = "Please enter CP12 certificate no."
        Public Shared SelectDocumentDateValidationError As String = "Please select Document Date"
        Public Shared SelectExpiryDateValidationError As String = "Please select Expiry Date"
        Public Shared InvalidEmail As String = "Invalid Email address."
        Public Shared InvalidCheracter As String = "Special Character is not allowed in Search Criteria."
        Public Shared NotValidPATDateFormat As String = "Please enter valid date for PAT Testing."
        Public Shared NotValidMaintenanceDateFormat As String = "Please enter valid date for Maintenance Item."
        Public Shared NotValidMEDateFormat As String = "Please enter valid date for M&E Servicing."

        Public Shared EnterTestingCycle As String = "Please enter Testing Cycle for PAT Testing."
        Public Shared EnterMaintenanceCycle As String = "Please enter Maintenance Cycle for Maintenance Item."
        Public Shared EnterServiceCycle As String = "Please enter Service Cycle for M&E Servicing."

        Public Shared SaveDetectorSuccessfully As String = "Detector is saved successfully."
        Public Shared SubStatusError As String = "Please select Sub status."

        Public Shared TanencyTerminated As String = "tanency not exist for this customer"
        Public Shared TanencyRecordNotFound As String = "Tenancy records not found."

        Public Const DefectWorkFailedToApprove As String = "Failed to approved defect works."
        Public Const DefectWorkApproved As String = "Defect works has been successfully approved."
        Public Const DefectWorkRejected As String = "Defect works has been successfully rejected."
        Public Const DefectWorkToBeApproved As String = "Defect work has been successfully moved to 'To Be Approved'."
        Public Const DefectWorkFailedToBeApproved As String = "Failed to move defect work in 'To Be Approved'"

#Region "Email/Sms/Noticifications Status"
        Public Shared EmailNotSent As String = "Email Not Sent"
        Public Shared EmailSending As String = "Email Sending"
        Public Shared EmailSent As String = "Email Sent"
        Public Shared EmailSendingFailed As String = "Email Sending Failed"
        Public Shared SmsNotSent As String = "Sms Not Sent"
        Public Shared SmsSending As String = "Sms Sending"
        Public Shared SmsSent As String = "Sms Sent"
        Public Shared SmsSendingFailed As String = "Sms Sending Failed"
        Public Shared PushNoticificationNotSent As String = "Push Noticification Not Sent"
        Public Shared PushNoticificationSending As String = "Push Noticification Sending"
        Public Shared PushNoticificationSent As String = "Push Noticification Sent"
        Public Shared PushNoticificationSendingFailed As String = "Push Noticification Sending Failed"
        Public Shared EmailDescriptionNotSent As String = "Email not initiated"
        Public Shared EmailDescriptionSending As String = "Sending Email"
        Public Shared EmailDescriptionSent As String = "Email Sent Successfully"
        Public Shared EmailDescriptionSendingFailed As String = "Email sending failed due to "
        Public Shared SmsDescriptionNotSent As String = "Sms not initiated"
        Public Shared SmsDescriptionSending As String = "Sending Sms"
        Public Shared SmsDescriptionSent As String = "Sms Sent Successfully"
        Public Shared PushNoticificationDescriptionNotSent As String = "Push Noticification not initiated"
        Public Shared PushNoticificationDescriptionSending As String = "Sending Push Noticification"
        Public Shared PushNoticificationDescriptionSent As String = "Push Noticification Sent Successfully"
        Public Shared PushNoticificationDescriptionSendingFailed As String = "Push Noticification sending failed"
        Public Shared SmsDescriptionSendingFailed As String = "Sms sending failed due to "
        Public Shared EmailHeading As String = "Email Status: "
        Public Shared SmsHeading As String = "SMS Status: "
        Public Shared PushNoticificationHeading As String = "Push Noticification Status: "
        Public Shared EmailToContractor As String = "Email not sent to contractor."

#End Region

#Region "File / Directory Path"
        Public Shared DirectoryDoesNotExist As String = "This path {0} does not exist on hard drive. Please create this path on drive."
        Public Shared DocumentUploadPathKeyDoesNotExist As String = "Document upload path key does not exist in web.config. Please insert the document upload path key in web.config under app settings"
        Public Shared FileDoesNotExist As String = "This file {0} does not exist on hard drive."
        Public Shared InvalidDocumentType As String = "Invlaid document type."
        Public Shared InvalidImageType As String = "Invlaid image type."
        Public Shared SelectDocument As String = "Please select the document"
        Public Shared FileUploadedSuccessfully As String = "File is uploaded successfully."
        Public Shared FileNotExist As String = "File not exist in Server directory."
#End Region

#Region "Property"
        Public Shared InvalidPropertyId As String = "Property Id is invalid."
        Public Shared InvalidPropertyIdQueryString As String = "Invalid Property Id in query string."
        Public Shared PropertyGasInfoUpdatedSuccessfuly As String = "Property Gas Info Updated successfully"
        Public Shared DefectSavedSuccessfuly As String = "Defect Saved successfully"
        Public Shared CategoryAddedSuccessfuly As String = "Category Added successfully"
        Public Shared SelectAppliance As String = "Please select value from Appliance List"
        Public Shared SelectCategory As String = "Please select value from Category List"
        Public Shared SelectDefectDate As String = "Select a valid date"
        Public Shared SerialNumber As String = "Please enter Serial Number"
        Public Shared PhotographTitle As String = "Please enter photograph title"

        'change start
        Public Shared PropertyIdDoesNotExist As String = "No property id given in the url"
        Public Shared SomeServerError As String = "There is some problem while fetching the data"
        Public Shared ErrorFetchingCustomerInfo As String = "There was some issue while fetching the customer infomation"
        Public Shared NoDataFoundCase As String = "No Data Found"
        Public Shared RenewalDateGreaterThanIssueDate As String = "Renewal Date must be greater than Issue Date"
        Public Shared notValidDateFormat As String = "Please enter valid date."
        Public Shared mandatoryField As String = "All fields are mandatory"
        Public Shared pleaseSelect As String = "Please Select"
        Public Shared ItemAddedSuccessfuly As String = "Record saved successfully."
        Public Shared NotValidNote As String = "Please enter note."
        Public Shared InValidNotesLength As String = "Notes length should be less than 1000 characters."
        Public Shared AppointmentNotesSaved As String = "The appointment notes has been updated and notification have been sent to the Gas Engineer."
        Public Shared AppointmentNotesSavedNotificationFailed As String = "The appointment notes have been updated but it was not possible to send a notification to the Engineer."
        Public Shared FillMandatory As String = "Please fill mandatory field."
        Public Shared SelectDevelopment As String = "Please select Development."
        Public Shared SelectPhase As String = "Please select Phase."
        Public Shared WriteAddress As String = "Please write House Number and Address1."
        Public Shared PropertyAlreadyExist As String = "Property with House/Room Number, Address1 and Town/City Already Exist"
        Public Shared SavePropertyServerError As String = "There is some problem while saving the data"
        Public Shared ValidDateFormat As String = "Please enter valid date for "
        Public Shared InitialMinPurchase As String = "Please enter Initial Min Purchase value"
        Public Shared PurchaseLevel As String = "Please enter Current Purchase Level value"
        Public Shared FileFormatError As String = "Please select a valid image file" 'new message



        'change end
#End Region

#Region "Add Appliance"
        Public Shared SelectLocation As String = "Please Insert Location."
        Public Shared SelectItem As String = "Please Insert Item."
        Public Shared SelectQuantity As String = "Please Insert Quantity."
        Public Shared SelectDimensions As String = "Please Insert Dimensions."
        Public Shared SelectFuel As String = "Please Insert Fuel."
        Public Shared SelectType As String = "Please Insert Type."
        Public Shared SelectMake As String = "Please Insert Make."
        Public Shared SelectSerialNo As String = "Please Insert Serial No."
        Public Shared SelectLifespan As String = "Please Select Life Span."
        Public Shared SelectPurchaseCost As String = "Please Insert Purchase Cost."
        Public Shared SelectDatePurchased As String = "Please Insert Date Purchased."
        Public Shared SelectRemoveDate As String = "Please Insert Date Removed."
        Public Shared SelectNotes As String = "Please Insert Notes."
        Public Shared ModelIsEmpty As String = "Model should not be empty."
        Public Shared SelectFlueType As String = "Select Flue Type."
        Public Shared InstalledDateIsEmpty As String = "Original Install Date should not be empty."
        Public Shared ApplianceSavedSuccessfully As String = "Record is saved successfully"
        Public Shared ApplianceDeletedSuccessfully As String = "Appliance is deleted successfully"
        Public Shared ApplianceDeleteFailed As String = "Failed to delete selected appliance"
#End Region

#Region "Add Activity Popup"
        Public Shared SelectActionDate As String = "Please select date."
        Public Shared SelectActionType As String = "Please select type."
        Public Shared SelectActionStatus As String = "Please select status."
        Public Shared SelectAction As String = "Please select action."
        Public Shared SelectActionLetter As String = "Please select letter."
        Public Shared SelectActionNotes As String = "Please enter notes."
        Public Shared SelectActionLetterDoc As String = "Please add letter or upload any document in the list."
        Public Shared SaveActvitySuccessfully As String = "Activity is saved successfully."
        Public Shared LetterAlreadyExist As String = "You have already added this letter."
        Public Shared LetterAlreadySend As String = "You have already send this letter."
        Public Shared duplicateAppointmentTime As String = "Another appointment already exist in selected Start/End time."
#End Region

#Region "Letters"
        Public Shared NoLettersFound As String = "No letter found."
        Public Shared NoDocumentsFound As String = "No document found."
        Public Shared SelectLetter As String = "Please select the letter."

        Public Shared StandardLetterCreated As String = "Standard Letter template successfully created."
        Public Shared StandardLetterUpdated As String = "Standard Letter template successfully updated."
        Public Shared StandardLetterDeleted As String = "Standard Letter template successfully deleted."
        Public Shared StandardLetterNotCreated As String = "Unable to create Standard Letter template."
        Public Shared StandardLetterNoStatus As String = "Please select the status of the Standard Letter template."
        Public Shared StandardLetterNoAction As String = "Please select the action of the Standard Letter template."
        Public Shared StandardLetterNoTitle As String = "Please enter the title of the Standard Letter template."
        Public Shared StandardLetterNoCode As String = "Please enter the code of the Standard Letter template."
        Public Shared StandardLetterCodeNotNumeric As String = "The Letter code shall only be numeric."
        Public Shared StandardLetterNoBody As String = "Please enter the body of the Standard Letter template."

        Public Shared SavedLetterNoLetterId As String = "Error. Could not find letter."
        Public Shared SavedLetterNoBody As String = "Please enter the body of the letter."
        Public Shared SavedLetterNoTitle As String = "Please enter the title of the letter."
        Public Shared SavedLetterNoSignOFfLookupCode As String = "Please select the Sign Off."
        Public Shared SavedLetterNoTeamId As String = "Please select your team."
        Public Shared SavedLetterNoFromResourceId As String = "Please select the person who be this letter's sender."
        Public Shared SavedLetterSuccess As String = "Letter saved successfully."
        Public Shared SavedLetterFailure As String = "An error occured. Letter was not saved."
        Public Shared InvalidLetterId As String = "Letter Id is invalid."
#End Region

#Region "Status And Actions"
        Public Shared ActionSavedSuccessfuly As String = "Action saved successfully"
        Public Shared StatusSavedSuccessfuly As String = "Status saved successfully"
        Public Shared SaveRightsSuccessfuly As String = "Rights saved successfully"
        Public Shared StatusUpdatedSuccessfuly As String = "Status updated successfully"
        Public Shared EditActionSuccessful As String = "Action edited successfully"
        Public Shared statusLength As String = "Status length is too large"
        Public Shared statusTitle As String = "Status title is required"
        Public Shared ActionError As String = "you can add action only under status"
#End Region

#Region "Users"

        Public Shared UserSavedSuccessfuly As String = "User Saved Successfully"
        Public Shared UserUpdatedSuccessfuly As String = "User Updated successfully"
        Public Shared UserDeletedSuccessfuly As String = "User Deleted successfully"
        Public Shared UserEditedSuccessful As String = "User Edited successfully"
        Public Shared NoUserFind As String = "No User Find"
        Public Shared UserGSREnotExists As String = "GSRE No not exists"
        Public Shared NoEmployeFound As String = "No Employe Found"
        Public Shared chkBoxListProperty As String = "Please select atleast one inspection type"
        Public Shared userAlreadyExists As String = "User already exists"
        Public Shared UserNameNotValid As String = "User name is not valid"
#End Region

#Region "Add Appointment Popup"
        Public Shared SaveAppointmentSuccessfully As String = "The appointment has been scheduled and details have been sent to the Gas Engineer"
        Public Shared SaveAmendAppointmentSuccessfully As String = "The appointment has been scheduled and details have been sent to the Gas Engineer"
        Public Shared AppointmentTime As String = "Appointment start time shall always be less than end time."
#End Region

#Region "CP12 Document Download"

        Public Shared Cp12DocumentNotAvailable As String = "Cp12 Document is not available"

#End Region

#Region "Inspection Document Download"
        Public Shared InspectionDocumentNotAvailable As String = "Inspection Document is not available"
#End Region

#Region "Fuel Scheduling"

        Public Shared NoOperativesExistWithInTime As String = "There are currently no operatives available to attend the appliance inspection prior to the certificate expiry date. Either remove the ‘Patch’ and/or ‘Expiry Date’ filter and click the'Refresh’ button"
        Public Shared AppointmentSavedEmailError As String = "Appointment saved but unable to send email due to : "
        Public Shared AppointmentInformationNotFound As String = "Appointment information not found."
        Public Shared DateNotValid As String = "Date should be greater then or equal to current date."
        Public Shared AppointmentNotesPushNotificationMessage As String = "Notification: The appointment notes for a scheduled appliance service have been amended. Please refresh your appointment list to update your appointment details"
        Public Shared UnableToRemoveAppointment As String = "System is unable to remove the appointment. Please refresh the page & try again."

#End Region

#Region " Tenant Information"

        Public Shared noTenantInformationFound As String = "No Tenant Information Found."

#End Region

#Region "CP12 Document Email"

        Public Shared NoTenantFoundToEmail As String = "No tenant found to send email."
        Public Shared UnableToSendEmailToTenant As String = "Unable to send email to one or more tenants due to wrong Email Address"
        Public Shared EmailSentSucessfully As String = "Certificate has been emailed sucessfully."
        Public Shared LGSREmailSubject As String = "Your current LGSR Certificate"

#End Region

#Region "Resources"
        Public Shared InvalidStartDate As String = "Please enter valid Start date."
        Public Shared InvalidEndDate As String = "Please enter valid End date."
        Public Shared InvalidStartTime As String = "Please enter valid Start time."
        Public Shared InvalidEndTime As String = "Please enter valid End time."
        Public Shared InvalidStartEndDate As String = "Start date and End date are not equal."
        Public Shared InvalidEndStartTime As String = "End date/time must be greater than start date/time."
        Public Shared SelectFutureDate As String = "Please select future date/time."
        Public Shared SavedSuccessfully As String = "User info saved Successfully."
        Public Shared UserAddedSuccessfully As String = "User successfully added."
        Public Shared FailedToSave As String = "Failed to save."

        Public Shared InvalidTimeFor As String = "Invalid time selected for "
        Public Shared InvalidFromTime As String = "Start time must be smaller than End time for "
        Public Shared InvalidSelectedTimeFor As String = "Selected time for "
        Public Shared ExtendedHourAlreadyExist As String = "There is already slot reserved in 'Out of Hours Management' for the selected date."
        Public Shared InvalidSelectedTimeForReason As String = " is part of the employees 'Out of Hours Management', please select a different time"
        Public Shared InvalidSelectedOutOfHours As String = "Selected time is already reserved as 'Out of Hours Management', please select a different time."
        Public Shared TimeReservedInCoreWorkingHours As String = "The selected time is part of the employees core working hours, please select a different time."

#End Region

#Region "Defets Management"

        Public Const selectDefect As String = "Please select defect(s)"
        Public Const NoDefectOperativesAvailable As String = "There are currently no operatives available. Either remove the ‘Patch’ and/or ‘Operative’ filter."
        Public Const InvalidDefectId = "Invalid defect data supplied."
        Public Const AppointmentRearrangedButIssueNavigatingToDefectBasket As String = "Defects appointment rearranged successfully, but unable to navigate to defect basket. Please navigate using menu."
        Public Const SelectedDateIsInPast As String = "Selected date is in past. This should be today's date or date in future."
        Public Shared ErrorNoTradeExist As String = "No Trade Exist Against Selected Defect"
#End Region

#Region "Generic Messages"
        Public Shared RecordNotExist As String = "No record exists"
        Public Shared DistanceError As String = "Unable to get distance."
        Public Shared INVALIDREQUEST As String = "Message from Google: The provided request was invalid."
        Public Shared MAXELEMENTSEXCEEDED As String = "Message from Google: The product of origins and destinations exceeds the per-query limit."
        Public Shared OVERQUERYLIMIT As String = "Message from Google: The service has received too many requests from your application within the allowed time period."
        Public Shared REQUESTDENIED As String = "Message from Google: The service denied use of the Distance Matrix service by your application."
        Public Shared UNKNOWNERROR As String = "Message from Google: The distance Matrix request could not be processed due to a server error. The request may succeed if you try again."
        Public Shared OperativeNotFound As String = "Unable to find operative"
        Public Const ErrorUserUpdate As String = "Error Updating Record"
#End Region

#Region "Assign to Contractor"

        Public Const workRequiredCount = "Please add at least one work required."
        Public Const AssignedToContractor = "Work successfully assigned to Contractor."
        Public Const AssignedToContractorFailed = "Assign work to contractor has not successful."
        Public Const NoexpenditureError = "No expenditure setup for current fiscal year."
        Public Const AssignedToSubcontractor As String = "Defect successfully assigned to Subcontractor."


#End Region


#Region "Assign Gas serviicng to contractor"
        Public Const InvalidPageNumber As String = "Invalid Page Number."
        Public Const CompletedDate As String = "Completed date can't be greater than today."
#End Region


#Region "Restriction Tab"
        Public Const FieldRestrictionCheck As String = "Please add atleast one field to save the restriction record."
        Public Const RestrictionSavedSuccessfully As String = "Restriction saved successfully."
        Public Const RestrictionNotSaved As String = "Failed to save restriction."
#End Region


    End Class
End Namespace

