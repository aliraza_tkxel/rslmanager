﻿Imports System

Namespace AS_Utilities

    Public Class SessionConstants

#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared AppServicingUserId As String = "AppServicingUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared UserPageAccess As String = "UserPageAccess"
        Public Shared UserType As String = "UserType"

#End Region

#Region "Session Lists"
        Public Shared UserPatchList As String = "SESSION_USERPATCHLIST"
        Public Shared PropertyId As String = "PropertyId"
#End Region

#Region "Documents"

        Public Shared DocumentUploadName As String = "DocumentUploadName"
        Public Shared PhotoUploadName As String = "PhotoName"
        Public Shared EPCPhotoUploadName As String = "EPCPhotoName"
        Public Shared SignatureUploadName As String = "SignatureName"
#End Region

#Region "Letter"
        Public Shared EditLetterId As String = "EditLetterId"
        Public Shared PreviewLetterTitle As String = "PreviewLetterTitle"
        Public Shared PreviewLetterBody As String = "PreviewLetterBody"
        Public Shared PreviewLetterStatusId As String = "PreviewLetterStatusId"
        Public Shared PreviewLetterActionId As String = "PreviewLetterActionId"
        Public Shared PreviewLetterCode As String = "PreviewLetterCode"
        Public Shared ActivityLetterDataSet As String = "ActivityLetterDataSet"
        Public Shared ActivityStandardLetterId As String = "ActivityStandardLetterId"
        Public Shared ApptScheduleParam As String
        Public Shared PrintLetterBo As String = "PrintLetterBo"
        Public Shared SendLetter As String = "SendLetter"
        Public Shared IsAlternative As String = "IsAlternative"

#End Region

#Region "Status"
        Public Shared StatusIdforEdit As String = "StatusIdforEdit"
        Public Shared statusIdofAction As String = "statusIdofAction"
        Public Shared StatIdforEditAction As String = "StatIdforEditAction"
        Public Shared ActioIdforEditAction As String = "ActioIdforEditAction"
#End Region

#Region "Access"
        Public Shared AccessRightEmployeeId As String = "AccessRightEmployeeId"
#End Region

#Region "Property"
        Public Shared treeItemId As String = "treeItemId"
        Public Shared treeItemName As String = "treeItemName"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
        Public Shared parameterValueId As String = "parameterValueId"
        Public Shared controlId As String = "ControlId"
        Public Shared accountingControlId As String = "AccountingControlId"
        Public Shared previousValueId As String = "lifeCycleRow"
        Public Shared worksRequiredRowId As String = "worksRequiredRowId"
        Public Shared cycle As String = "componentId"
        Public Shared valueLifeCycle As String = "valueLifeCycle"
        Public Shared isDdlChangeEvent As String = "isDdlChangeEvent"
        Public Shared plannedComponentId As String = "plannedComponentId"
        Public Shared attributrTypeId As String = "attributrTypeId"
        Public Shared isMapped As String = "isMapped"
        Public Shared isLifeCycleMapped As String = "isLifeCycleMapped"
        Public Shared isAccountingMapped As String = "isAccountingMapped"
        Public Shared isWorkSatisfactory As String = "isWorkSatisfactory"
        Public Shared JournalId As String = "JournalId"
#End Region

#Region "Fuel Scheduling"

        Public Shared DueWithIn56Days As String = "DueWithIn56Days"
        Public Shared SearchText As String = "SearchText"
        Public Shared TerminationDate As String = "TerminationDate"
        Public Shared CertificateExpiryDate As String = "ExpiryDate"
        Public Shared AvailableOperativesDs As String = "AvailableOperativeDs"
        Public Shared PropertyDistance As String = "PropertyDistance"
        Public Shared AppointmentSlots As String = "AppointmentSlotsDt"
        Public Shared SelectedPropertySchedulingBo As String = "PropertySchedulingBo"

        Public Shared SelectedPropertySchedulingBoList As String = "PropertySchedulingBoList"
        Public Shared AlternativeSchedulingBo As String = "AlternativeSchedulingBo"
        Public Shared AlternativeSchedulingHeaderBoList As String = "AlternativeSchedulingHeaderBoList"
        Public Const PropertyDurationDict As String = "PropertyDurationDict"

        Public Shared ApplianceServicingId As String = "ApplianceServicingId"
        Public Shared AppointmentCancellationForSchemeBlock As String = "AppointmentCancellationForSchemeBlock"

        Public Shared BoilerTypeInfo As String = "BoilerTypeInfo"
        Public Shared ServicingTypeInfo As String = "ServicingTypeInfo"
        Public Shared AppointmentType As String = "AppointmentTypeInfo"

#End Region

#Region "Scheduling Calendar Appoitnemnt to be arranged"

        Public Shared ApptbaScheduleParam As String = "apptbaScheduleParam"
        Public Shared AddAppointmentParam As String = "addAppointmentParam"
        Public Shared AptbaStartDate As String = "aptbaStartDate"
        Public Shared AptbaEndDate As String = "aptbaEndDate"
        Public Shared AddIsSuccess As String = "addIsSuccess"
        Public Shared AddCalendarPageingClicked As String = "addCalendarPageingClicked"
        Public Shared AddAppointmentClicked As String = "addAppointmentClicked"


#End Region

#Region "Scheduling Calendar Appoitnemnt arranged"

        Public Shared ApaScheduleParam As String = "apaScheduleParam"
        Public Shared AmendAppointmentParam As String = "amendAppointmentParam"
        Public Shared ApaStartDate As String = "apaStartDate"
        Public Shared ApaEndDate As String = "apaEndDate"
        Public Shared AmendIsSuccess As String = "amendIsSuccess"
        Public Shared AmendCalendarPageingClicked As String = "amendCalendarPageingClicked"
        Public Shared AmendAppointmentClicked As String = "amendAppointmentClicked"
        Public Shared TenancyClicked As String = "tenancyClicked"
        Public Shared aptbaTenancyClicked As String = "aptbaTenancyClicked"
        Public Shared amendCalanderWeeklyDT As String = "AmendCalanderWeeklyDT"
        Public Shared addCalanderWeeklyDT As String = "AddCalanderWeeklyDT"

#End Region

#Region "Weekly/Monthly Calendar(s) (Readonly Calendar(s))"

        Public Shared weeklyCalendarStartDate As String = "weeklyCalendarStartDate"
        Public Shared weeklyCalendarEndDate As String = "weeklyCalendarEndDate"

        Public Shared monthlyCalendarStartDate As String = "monthlyCalendarStartDate"
        Public Shared monthlyCalendarEndDate As String = "monthlyCalendarEndDate"

        Public Shared monthlyCalendarMonthDays As String = "monthlyCalendarMonthDays"

#End Region

#Region "Resources"
        Public Shared SelectedResourceEmployeeId As String = "SelectedResourceEmployeeId"
        Public Shared OutOfHoursDeletedList As String = "OutOfHoursDeletedList"
#End Region

#Region "Appointments for Map"
        Public Shared existingAppointmentForSchedulingMap As String = "existingAppointmentForSchedulingMap"
#End Region

#Region "Defect Management"

        Public Const DefectToBeArrangedList As String = "DefectToBeArrangedList"
        Public Const VoidToBeArrangedList As String = "VoidToBeArrangedList"
        Public Const VoidArrangedList As String = "VoidArrangedList"
        Public Const SelectedDefectInspectionRefDetail As String = "SelectedDefectInspectionRefDetail"
        Public Const GroupBy As String = "groupBy"
        Public Shared CommonAddress As String = "CommonAddress"
        Public Const SelectedDefectsDataTable As String = "SelectedDefectsDataTable"

        Public Shared OperativeworkingHour As String = "OperativeworkingHour"
        Public Shared TempDefectAptBlockList As String = "TempDefectAptBlockList"
        Public Shared SelectedAppointmentBlock As String = "SelectedAppointmentBlock"
        Public Shared DefectAppointmentData As String = "DefecctAppointmentData"

#End Region

#Region "Assign To Contractor"
        Public Shared AssignToContractorBo As String = "AssignToContractorBo"
#End Region

        Public Shared ConfirmedGasAppointment As String = "ConfirmedGasAppointmentDv"

        Public Shared ExpenditureId As String = "ExpenditureId"
        Public Shared HeadId As String = "HeadId"

#Region "Property Raise PO"
        Public Shared PropertyAssignToContractorBo As String = "PropertyAssignToContractorBo"
        Public Shared ProvisionDataSet As String = "ProvisionDataSet"
        Public Shared PropertyName As String = "PropertyName"
        Public Shared VatBoList As String = "VatBoList"
        Public Shared PropertyExpenditureBOList As String = "PropertyExpenditureBOList"
        Public Shared PropertyDetail As String = "PropertyDetail"
        Public Shared PropertyOwnerShip As String = "PropertyOwnerShip"


#End Region

    End Class

End Namespace


