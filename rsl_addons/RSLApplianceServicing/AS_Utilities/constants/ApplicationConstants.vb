﻿Imports System
Imports System.Text
Imports System.Configuration
Namespace AS_Utilities
    Public Class ApplicationConstants
#Region "Misc Appointment Types Names"

        Public Const schemeType As String = "Scheme"
        Public Const blockType As String = "Block"

#End Region
#Region "General"
        Public Const DropDownDefaultValue As String = "-1"
        Public Const Title As String = "Title"
        Public Const CertificateNames As String = "CertificateNames"
        Public Const LetterPrefix As String = "SL_"
        Public Const DocPrefix As String = "Doc_"
        Public Const LetterWord As String = "Letter"
        Public Const DocumentWord As String = "Document"
        Public Const DefaultDropDownDataValueField As String = "id"
        Public Const DefaultDropDownDataTextField As String = "title"
        Public Const ManagerUserType As String = "Manager" ' this value should be same as stored in db
        Public Const Id As String = "Id"
        Public Const ApplianceDropDownDefaultValue As String = "-2"
        Public Const GasketSetReplacementDefaultYears As Integer = 5

#End Region

#Region "Master Layout"

        Public Const ConWebBackgroundColor As String = "#e6e6e6"
        Public Const ConWebBackgroundProperty As String = "background-color"

        Public Const MenuCol As String = "Menu"
        Public Const UrlCol As String = "Url"
        Public Const MenuIdCol As String = "MenuId"
        Public Const PageFileNameCol As String = "PageFileName"
        Public Const NoPageSelected As Integer = -1
        Public Const PageFirstLevel As Integer = 1

        Public Const ServicingMenu As String = "Servicing"
        Public Const ResourcesMenu As String = "Operatives"
        Public Const PropertyModuleMenu As String = "Search"
        Public Const MaintenanceMenu As String = "Maintenance"
        Public Const AdminMenu As String = "Admin"
        Public Const ReportsMenu As String = "Reports"

        Public Const MultipleMenus As String = ""
        Public Const ServicingMasterFileName As String = "ASMasterPage.Master"
        Public Const ResourcesMasterFileName As String = "ResourceMaster.Master"
        Public Const PropertyModuleMasterFileName As String = "PropertyModuleMaster.Master"

        Public Const AccessGrantedModulesDt As String = "AccessGrantedModulesDt"
        Public Const AccessGrantedMenusDt As String = "AccessGrantedMenusDt"
        Public Const AccessGrantedPagesDt As String = "AccessGrantedPagesDt"
        Public Const RandomPageDt As String = "RandomPageDt"
        Public Const PropertyCustomerDetailDt As String = "PropertyCustomerDetailDt"
        Public Const DefectDetailDt As String = "DefectDetailDt"

        Public Const GrantModulesModuleIdCol As String = "MODULEID"

        Public Const GrantMenuModuleIdCol As String = "ModuleId"
        Public Const GrantMenuMenuIdCol As String = "MenuId"
        Public Const GrantMenuUrlCol As String = "Url"
        Public Const GrantMenuDescriptionCol As String = "Menu"

        Public Const GrantPageParentPageCol As String = "ParentPage"
        Public Const GrantPageAccessLevelCol As String = "AccessLevel"
        Public Const GrantPageQueryStringCol As String = "PageQueryString"
        Public Const GrantPageFileNameCol As String = "PageFileName"
        Public Const GrantPageUrlCol As String = "PageUrl"
        Public Const GrantPageCoreUrlCol As String = "PageCoreUrl"
        Public Const GrantPagePageNameCol As String = "PageName"
        Public Const GrantPageIdCol As String = "PageId"
        Public Const GrantPageMenuIdCol As String = "MenuId"
        Public Const GrantPageModuleIdCol As String = "ModuleId"
        Public Const GrantOrderTextCol As String = "ORDERTEXT"

        Public Const RandomParentPageCol As String = "ParentPage"
        Public Const RandomAccessLevelCol As String = "AccessLevel"
        Public Const RandomQueryStringCol As String = "PageQueryString"
        Public Const RandomFileNameCol As String = "PageFileName"
        Public Const RandomPageNameCol As String = "PageName"
        Public Const RandomPageIdCol As String = "PageId"

#End Region

#Region "Schedule Calendar"
        Public Shared daysArray() As String = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
        'Public Shared appointmentStatus() As String = {"Appointment to be arranged", "Arranged", "No Entry", "Legal Proceedings"}

        'Start - Changed By Aamir Waheed on April 23, 2013.
        'To Implement CR, Can the drop down selection be reduced to start at 6:00 and ends at 20:00 hours
        'Action : Commented out the old "appointmentTime()" and added "new appointmentTime()" with new values.
        'Public Shared appointmentTime() As String = {"00:00", "00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"}

        Public Shared appointmentTime() As String = {"06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45"}

        'End - Changed By Aamir Waheed on April 23, 2013.

        Public Shared monthDays As UInteger = 0
        Public Shared startDate As Date = Date.Today  'DateSerial(Now.Year(), Now.Month(), 1)
        Public Shared endDate As Date = Date.Today
        Public Shared btnSearchClicked As Boolean = False
        Public Shared filter As String = ""
        Public Shared AddressCol As String = "Address"
        Public Shared ddlDefaultValue() As String
        Public Shared amendAppointmentClicked As Boolean = False
        Public Shared addAppointmentClicked As Boolean = False
        Public Shared amendIsSuccess As Boolean = False
        Public Shared addIsSuccess As Boolean = False
        Public Shared amendCalendarPageingClicked As Boolean = False
        Public Shared addCalendarPageingClicked As Boolean = False
        Public Shared noEntry As String = "No Entry"
        Public Shared aborted As String = "Aborted"
        Public Shared toBeApproved As String = "To Be Approved"
        Public Shared approved As String = "Approved"
        Public Shared rejected As String = "Rejected"
        Public Shared workOnHold As String = "Work on hold"
        Public Shared DefautFuelType As String = "Mains Gas"
        Public Shared DefautAlternativeFuelType As String = "All Alternative Fuel Types"
#End Region

#Region "Inspection Drop Down "

        Public Const InspectionId As String = "InspectionTypeID"
        Public Const InspectionName As String = "Description"
#End Region

#Region "Status Drop Down "

        Public Const StatusId As String = "StatusId"
#End Region
#Region "certificates Drop Down "

        Public Const heatingTypeId As String = "heatingTypeId"
#End Region
#Region "Status Drop Down "

        Public Const ActionId As String = "ActionId"
#End Region
#Region "Fuel type Drop Down "

        Public Const AlternativeFuelType As String = "Alternative Servicing"
#End Region

#Region "Property Type Drop Down "

        Public Const TypeId As String = "TypeId"
        Public Const TypeTitle As String = "Title"
#End Region

#Region "Letters History List / Dropdown"
        Public Const LetterHistoryId As String = "LetterHistoryId"
        Public Const LetterHistoryTitle As String = "LetterTitle"
#End Region

#Region "Letters List / Dropdown"
        Public Const LetterId As String = "StandardLetterId"
        Public Const LetterTitle As String = "Title"
#End Region

#Region "Letters Doc List"
        Public Const LetterDocId As String = "LetterDocId"
        Public Const LetterDocTitle As String = "LetterDocTitle"
#End Region

#Region "Letters Doc Data Table Column Names"
        Public Const LetterDocNameColumn As String = "LetterDocName"
        Public Const LetterDocValueColumn As String = "LetterDocValue"
        Public Const LetterIdColumn As String = "LetterId"
        Public Const LetterDocDeleteColumn As String = "Delete"
#End Region

#Region "Edit Letter Values Data Table Column Names"
        Public Const LetterTextTitleColumn As String = "LetterTextTitle"
        Public Const LetterBodyColumn As String = "LetterBody"
        Public Const StandardLetterId As String = "StandardLetterId"
        Public Const TeamIdColumn As String = "TeamId"
        Public Const FromResourceIdColumn As String = "FromResourceId"
        Public Const SignOffCodeColumn As String = "SignOffCode"
        Public Const RentBalanceColumn As String = "RentBalance"
        Public Const RentChargeColumn As String = "RentCharge"
        Public Const TodayDateColumn As String = "TodayDate"
#End Region

#Region "Download Window"

        Public Shared DowloadWindowScript As String = "window.open('{0}','_blank','directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no')"
#End Region

#Region "Dashboard"

        Public Const HashTableKeyNoEntry As String = "No_Entry"
        Public Const HashTableKeyLegalProceedings As String = "Legal_Proceedings"
        Public Const HashTableKeyExpired As String = "Expired"
        Public Const HashTableKeyExpiresIn56Days As String = "Expires_In_56_Days"
        Public Const HashTableKeyAppointmentsArranged As String = "Appointments_Arranged"
        Public Const HashTableKeyVoidAppointmentsArranged As String = "Void_Appointments_Arranged"
        Public Const HashTableKeyAppointmentsToBeArranged As String = "Appointments_To_Be_Arranged"
        Public Const HashTableKeyVoidAppointmentsToBeArranged As String = "Void_Appointments_To_Be_Arranged"
        Public Const HashTableKeyVoidProperties As String = "Void_Properties"

        Public Const InspectionStatusDropDownNoEntries As Integer = 1
        Public Const InspectionStatusDropDownLegalProceedings As Integer = 2
        Public Const InspectionStatusDropDownExpired As Integer = 3
        Public Const InspectionStatusDropDownAppointmentsToBeArranges As Integer = 4
        Public Const InspectionStatusDropDownAppointmentsAborted As Integer = 5

        Public Const MEHashTableKeyNoEntry As String = "No_Entry"
        Public Const MEHashTableKeyOverdue As String = "OverDue"
        Public Const MEHashTableKeyAppointmentArranged As String = "AppointmentArranged"
        Public Const MEHashTableKeyAppointmentToBeArranged As String = "APTBA"

        Public Const ServicingStatusDropDownNoEntries As Integer = 1
        Public Const ServicingStatusDropDownAppointmentToBeArranged As Integer = 2

        Public Const HashTableKeyDefectAppointmentsToBeArranged As String = "Defects_Appointments_To_Be_Arranged"
        Public Const HashTableKeyDefectAppointmentsArranged As String = "Defects_Appointments_Arranged"
        Public Const HashTableKeyDefectCappedAppliances As String = "Defect_Capped_Applinaces"
        Public Const HashTableKeyDefectsRequiringApproval As String = "Defect_Requiring_Approval"
        Public Const ReasonDt As String = "ReasonDt"

#End Region

#Region "Page Sort"

        Public Const Ascending As String = "ASC"
        Public Const Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Const PageSortBo As String = "PageSortBo"
#End Region

#Region "CSS"
        Public Const TabClickedCssClass As String = "TabClicked"
        Public Const TabInitialCssClass As String = "TabInitial"
#End Region

#Region "Page Names"
        'The value of these should be same as those are in database table AS_Pages
        Public Const DashboardPage As String = "Dashboard"
        Public Const ResourcesPage As String = "Resources"
        Public Const SchedulingPage As String = "Scheduling"
        Public Const AdminSchedulingPage As String = "Admin Calendar"
        Public Const UsersPage As String = "Users"
        Public Const StatusPage As String = "Status"
        Public Const LetterPage As String = "Letter"
        Public Const AccessPage As String = "Access"
        Public Const CalendarPage As String = "Calendar"
        Public Const PropertyModulePage As String = "Property Module"
        'Public Const CertificateExpiry As String = "Certificate Expiry"
        Public Const IssuedCertificates As String = "Issued Certificates"
        Public Const PrintCertificates As String = "Print Certificates"
#End Region

#Region "Status Names"
        Public Const Cp12IssuedStatus As String = "Certificate issued"
        'Start - Changed By Aamir Waheed on May 30, 2013.
        'To check for let status of a property.
        Public Const PropertyStatusLet = "let"
        'End - Changed By Aamir Waheed on May 30, 2013.
#End Region

#Region "Certificate Expiry Report"
        'Start - Changes By Aamir Waheed on 26th June 2013
        'To Implement different reports (report filters like 1-2 weeks etc.)
        'These values below are used as report title at front end.
        Public Const CertificateExpiryReport_AllRecords As String = "All Records (Total Units)"
        Public Const CertificateExpiryReport_NoCertificateData As String = "No Certificate Data"
        Public Const CertificateExpiryReport_Expired As String = "Expired"
        Public Const CertificateExpiryReport_DueIn1_2Weeks As String = "Due in 2 weeks"
        Public Const CertificateExpiryReport_DueIn2_4Week As String = "Due in 2-4 weeks"
        Public Const CertificateExpiryReport_DueIn4_8Weeks As String = "Due in 4-8 weeks"
        Public Const CertificateExpiryReport_DueIn8_12Weeks As String = "Due in 8-12 weeks"
        Public Const CertificateExpiryReport_DueIn12_16Weeks As String = "Due in 12-16 weeks"
        Public Const CertificateExpiryReport_DueIn16_52Weeks As String = "Due in 16-52 weeks"
        Public Const CertificateExpiryReport_DueInMoreThan52Weeks As String = "Due > 52 weeks"
        'End - Changes By Aamir Waheed on 26th June 2013
#End Region

#Region "Intelligent scheduling"
        Public Const OperativesDt As String = "OperativesDt"
        Public Const RiskDt As String = "RiskDt"
        Public Const VulnerabilityDt As String = "VulnerabilityDt"
        Public Const LeavesDt As String = "LeavesDt"
        Public Const AppointmentsDt As String = "AppointmentsDt"

        Public Const MaximumDayWorkingHours As Double = 23.5
        Public Const WeekDayCol As String = "Name"

        Public Const AppointmentStartDateControlId As String = "txtStartDate"
        Public Const StartDateLabelControlId As String = "lblStartDate"
        Public Const CalDateControlId As String = "imgCalDate"
        Public Const EditImageButtonControlId As String = "imgBtnEdit"
        Public Const DoneImageButtonControlId As String = "imgBtnDone"
        Public Const DurationLabelControlId As String = "lblDuration"
        Public Const DurationDropdownControlId As String = "ddlDuration"
        Public Const DurationPanelControlId As String = "pnlDuration"
        Public Const CalExtDateControlId As String = "imgCalExtDate"
        Public Const GoControlId As String = "btnGo"
        Public Const TempAppointmentGridControlId As String = "grdTempAppointments"
        Public Const DefaultSchedulingHour As Integer = 1
#End Region

#Region "Prefix"
        Public Const Tr As String = "tr"
        Public Const Td1 As String = "td1_"
        Public Const Td2 As String = "td2_"
        Public Const lblDdl As String = "lblDdl"
        Public Const lblChkBox As String = "lblChkBox"
        Public Const lblDate As String = "lblDate"
        Public Const extender As String = "extender"
        Public Const lblRadioBtn As String = "lblRadioBtn"
        Public Const lblTextBox As String = "lblTextBox"
        Public Const textBoxStyle As String = "dynamicTextBox"
        Public Const ddlStyle As String = "dynamicDdl"
        Public Const dynamicRadioButton As String = "dynamicRadioButton"
        Public Const dynamicRadioButtonBoilerConditionRating As String = "dynamicRadioButtonBoilerConditionRating"
        Public Const dynamicRadioButtonComponentAccounting As String = "dynamicRadioButtonComponentAccounting"
        Public Const lblCycle As String = "lblCycle"
        Public Const lblInspected As String = "lblInspected"
#End Region

#Region "Control ID's"
        Public Const ddlParameter As String = "ddlParameter"
        Public Const chkList As String = "chkList"
        Public Const txtDate As String = "txtDate"
        Public Const rdBtn As String = "rdBtn"
        Public Const txt As String = "txt"
        Public Const txtArea As String = "txtArea"
        Public Const lblLifeCycle As String = "lblLifeCycle"
        Public Const conditionSatisfactory As String = "Satisfactory"
        Public Const conditionUnsatisfactory As String = "Unsatisfactory"
        Public Const conditionPotentiallyUnsatisfactory As String = "Potentially Unsatisfactory"
        Public Const hdnLifeCycle As String = "hdnLifeCycle"
        Public Const lastRewired As String = "lastRewired"
        Public Const rewiredDue As String = "rewiredDue"
        Public Const upgradeDue As String = "upgradeDue"
        Public Const curDue As String = "curDue"
        Public Const lastUpdrade As String = "lastUpdrade"
        Public Const curLastReplaced As String = "curLastReplaced"
        Public Const installedDate As String = "installedDate"
        Public Const gasketSetReplacement As String = "gasketSetReplacement"

        Public Const TempAssignmentGridControlId As String = "grdTempAppointments"
        Public Const TempGridControlId As String = "grdTemp"

#End Region

#Region "Property Record"
        Public Const ApplianceDefect As String = "Appliance Defect"
        Public Const PlannedActiontype As String = "Planned"
        Public Const FaultActiontype As String = "Fault"
        Public Const PlannedComponents As String = "PlannedComponents"
        Public Const ConditionRating As String = "ConditionRating"
        Public Const ArrangedStatus As String = "Arranged"
        Public Const ToBeArrangedStatus As String = "To be Arranged"
        Public Const InspectionArrangedStatus As String = "Inspection Arranged"
        Public Const ApplianceCancelledStatus As String = "Cancelled"
        Public Const ApplianceCompletedStatus As String = "Completed"
        Public Const ApplianceInspectionTypeDescription As String = "Appliance Servicing"
        Public Const ConditionToBeArrangedStatus As String = "Condition to be Arranged"
        Public Const AssignedToContractorStatus As String = "Assigned To Contractor"
        Public Const DefectTypeStatus As String = "Defect"
        Public Const BoilerDefectType As String = "Boiler"
        Public Const DefaultDefectTrade As String = "Gas (Full)"
        Public Const PropertyLocked As Integer = 1
        Public Const PropertyUnLocked As Integer = 0
        Public Const PropertyTimedout As Integer = 1
        Public Const PropertyNotTimeout As Integer = 0
        Public Const PropertyScheduled As Integer = 1
        Public Const PropertyUnScheduled As Integer = 0
#End Region

#Region "attributes Data table"
        Public Const propertyAttributes As String = "PropertyAttributes"
        Public Const propertyAttributeValues As String = "PropertyAttributeValues"
        Public Const propertyImage As String = "PropertyImage"
        Public Const parameters As String = "Parameters"
        Public Const parametervalues As String = "Parametervalues"
        Public Const preinsertedvalues As String = "preinsertedvalues"
        Public Const lastReplaced As String = "lastReplaced"
        Public Const replacementDue As String = "replacementDue"
        Public Const plannedComponent As String = "plannedComponent"
        Public Const mst As String = "MST"
        Public Const cP12Information As String = "CP12Information"
        Public Const dtHeatingMapping As String = "dtHeatingMapping"
        Public Const dtLGSR As String = "dtLGSR"
        Public Const valueLifeCycle As Boolean = False
        Public Const isDdlChangeEvent As Boolean = False
        Public Const plannedComponentId As Integer = 0
        Public Const attributrTypeId As Integer = 0
        Public Const isMapped As Boolean = False
        Public Const isLifeCycleMapped As Integer = 0
        Public Const isAccountingMapped As Integer = 0
        Public Const isWorkSatisfactory As Boolean = False
        Public Const appliancesItemName As String = "Appliances"
        Public Const CyclicServices As String = "CyclicServices"
#End Region

#Region "SMS"
        Public Shared SMSUrl As String = ConfigurationManager.AppSettings("SMSAddress")
        Public Const SMSUserName As String = "adnan.mirza@broadlandgroup.org"
        Public Const SMSPassword As String = "obp5+Ne"
        Public Const strSMS As String = "sms"
        Public Const SMSCompany As String = "Broadland"
        Public Const SMSmessage As String = "Broadland Repairs will be carrying out a service on your heating system in the {AppointmentTime} of {AppointmentDate} between {StartTime} and {EndTime}. If you are unable to keep this appointment please call a member of the scheduling team on 01603 750136."
        Public Const SMSToCustomerMessage As String = "Broadland Repairs Service will be carrying out your annual heating service in the {AppointmentShift} of {AppointmentDate} at {AppointmentTime}. If you are unable to keep this appointment please call a member of the scheduling team on 01603 750136."
#End Region

#Region "Resources"

        Public Const CoreHoursDataTable As String = "CoreHoursDataTable"
        Public Const EmpCoreHoursDataTable As String = "EmpCoreHoursDataTable"
        Public Const OutOfHoursDataTable As String = "OutOfHoursDataTable"
        Public Const OutOfHoursTypeDataTable As String = "OutOfHoursTypeDataTable"

        Public Const OutOfHoursIdCol As String = "OutOfHoursId"
        Public Const TypeIdCol As String = "TypeId"
        Public Const TypeCol As String = "Type"
        Public Const StartDateCol As String = "StartDate"
        Public Const EndDateCol As String = "EndDate"
        Public Const StartTimeCol As String = "StartTime"
        Public Const EndTimeCol As String = "EndTime"
        Public Const GeneralStartDateCol As String = "GeneralStartDate"
        Public Const GeneralEndDateCol As String = "GeneralEndDate"
        Public Const NewEmployeeId As Integer = -1
        Public Const CreationDateCol As String = "CreationDate"

        Public Const AddUserHeader As String = "Add User"
        Public Const EditUserHeader As String = "Edit User"

        Public Const DayIdCol As String = "DayId"
        Public Const DayNameCol As String = "Name"

#End Region

#Region "Scheduling"

        Public Const CustomerNameCol As String = "CustomerName"
        Public Const AppointmentTimeCol As String = "AppointmentTime"
        Public Const AppointmentDateCol As String = "AppointmentDate"
        Public Const JSGCol As String = "JSG"
        Public Const EmailCol As String = "Email"
        Public Const SchemeCol As String = "Scheme"
        Public Const PropertyAddressCol As String = "Address"
        Public Const PropertyIdCol As String = "PropertyId"
        Public Const TownCityCol As String = "TownCity"
        Public Const CountyCol As String = "County"
        Public Const PostcodeCol As String = "Postcode"

        Public Const CustomerCol As String = "Customer"
        Public Const TelephoneCol As String = "Telephone"
        Public Const MobileCol As String = "Mobile"

#End Region

#Region "Property Module Control Tabs"

        Public Const PropertiesPageRef As String = "properties"
        Public Const PropertyRecordPageRef As String = "propertyrecord"
        Public Const Summary As String = "Summary"
        Public Const Activities As String = "Activities"
        Public Const Attributes As String = "Attributes"
        Public Const Accommodation As String = "Accommodation"
        Public Const Documents As String = "Documents"
        Public Const Financial As String = "Financial"
        Public Const HealthSafety As String = "Health & Safety"
        Public Const Warranty As String = "Warranty"
        Public Const StatusHistory As String = "Status History"
        Public Const Restriction As String = "Restriction"
        Public Const lnkBtnSummaryTab As String = "lnkBtnSummaryTab"
        Public Const lnkBtnActivitiesTab As String = "lnkBtnActivitiesTab"
        Public Const lnkBtnAccommodation As String = "lnkBtnAccommodation"
        Public Const lnkBtnAttributesTab As String = "lnkBtnAttributesTab"
        Public Const lnkBtnDocTab As String = "lnkBtnDocTab"
        Public Const lnkBtnFinancial As String = "lnkBtnFinancial"
        Public Const lnkBtnHealthSafety As String = "lnkBtnHealthSafety"
        Public Const lnkBtnWarranty As String = "lnkBtnWarranty"
        Public Const lnkBtnStatusHistory As String = "lnkBtnStatusHistory"
        Public Const lnkBtnRestriction As String = "lnkBtnRestriction"
#End Region

#Region "Appointment Confirmation Email"

        Public Const SubjectAppointmentConfirmationEmail As String = "Appointment Confirmation - Broadland Repair Service"

#End Region

#Region "Job Sheet Summary"
        Public Const PmoColumn As String = "PMO"
        Public Const ComponentColumn As String = "Component"
        Public Const TradesColumn As String = "Trade"
        Public Const JsnColumn As String = "JSN"
        Public Const JsnSearchColumn As String = "JSNSearch"
        Public Const StatusColumn As String = "Status"
        Public Const OperativeColumn As String = "Operative"
        Public Const StartTimeColumn As String = "StartTime"
        Public Const EndTimeColumn As String = "EndTime"
        Public Const DurationsColumn As String = "Duration"
        Public Const TotalDurationColumn As String = "TotalDuration"
        Public Const StartDateColumn As String = "StartDate"
        Public Const EndDateColumn As String = "EndDate"
        Public Const CustomerNotesColumn As String = "CustomerNotes"
        Public Const JobsheetNotesColumn As String = "JobsheetNotes"
        Public Const IsMiscAppointmentColumn As String = "IsMiscAppointment"
        Public Const PropertyIdColumn As String = "PropertyId"
        Public Const AppointmentIdColumn As String = "AppointmentId"
        Public Const InterimStatusColumn As String = "InterimStatus"
        Public Const NotAvailable As String = "N/A"

        Public Const jobSheetSummaryDetailTable As String = "JobSheetSummaryDetailTable"
        Public Const jobSheetSummaryAsbestosTable As String = "JobSheetSummaryAsbestosTable"
        Public Const JobSheetSummaryBoilerInfoTable As String = "JobSheetSummaryBoilerInfoTable"

#End Region

#Region "Property Data Restructure"
        Public Const propertyTemplate As String = "propertyTemplate"
        Public Const development As String = "development"
        Public Const stocktype As String = "Stocktype"
        Public Const ownership As String = "Ownership"
        Public Const status As String = "Status"
        Public Const dwellingType As String = "DwellingType"
        Public Const level As String = "Level"
        Public Const propertyType As String = "PropertyType"
        Public Const assetType As String = "AssetType"
        Public Const assetTypeMain As String = "AssetTypeMain"
        Public Const TenureSelection As String = "TenureSelection"
#End Region

#Region "Parameter Names"

        Public Const landlordAppliance As String = "Landlord Appliance"
        Public Const CertificateName As String = "CertificateNames"
        Public Const cp12Issued As String = "Certificate Issued"
        Public Const cp12Number As String = "Certificate Number"
        Public Const cp12Renewal As String = "Certificate Renewal"

#End Region

#Region "Customers"

        Public Const RiskInfo As String = "RiskInfo"
        Public Const VulnerabilityInfo As String = "VulnerabilityInfo"
        Public Const UpdateCustomerDetails As String = "Update Customer Details"
        Public Const SaveChanges As String = "Save Changes"

#End Region

#Region "Property distance "

        Public Shared propertyDistance As New DataTable
        Public Shared distanceError As Boolean = True

#End Region

#Region "Data Table Names"

        Public Const WorkingHourDataTable As String = "WorkingHourDataTable"

#End Region



#Region "Assign Work To Contractor"

        Public Const EmailSubject As String = "Purchase Order for Fuel Servicing Work"
        Public Const NoExpenditureSetup As String = "No Expenditures are Setup"
        Public Const WorksRequiredCol As String = "WorksRequired"
        Public Const ApplianceCol As String = "Appliance"
        Public Const CategoryUpdatedCol As String = "CategoryUpdated"
        Public Const CategoryUpdatedByCol As String = "CategoryUpdatedBy"
        Public Const ApprovalCol As String = "Approval"
        Public Const AppointmentCol As String = "Appointment"
        Public Const NotesCol As String = "Notes"
        Public Const StatusCol As String = "Status"
        Public Const DefectInfoDt As String = "DefectInfoDt"
        Public Const FaultCostInfoDt As String = "FaultCostInfo"
        'TODO: These should not be hard coded here.
        Public Const EstateMaintenanceCostId As Int32 = 11
        Public Const DefectBudgetHeadId As Int32 = 25
        'Public Shared GasServicingExpenditureId As Int32 = SessionManager.getExpenditureId()
        Public Const WorkOrderedPoStatusId As Int32 = 1
        Public Const QueuedPoStatusId As Int32 = 0
        Public Const WorkRequiredDT As String = "WorkRequiredDT"
        Public Shared PropertyDetailsDT As String = "PropertyDetailsDT"
        Public Const BlockDetailsdt As String = "BlockDetailsdt"
        Public Const UrgentAsbestosEmailSubject As String = "Urgent Asbestos Issue Raised"

#End Region



#Region "Work Required Data Table column names"

        Public Const NetCostCol As String = "NetCost"
        Public Const VatTypeCol As String = "VatType"
        Public Const VatCol As String = "Vat"
        Public Const GrossCol As String = "Gross"
        Public Const PIStatusCol As String = "PIStatus"
        Public Const ExpenditureIdCol As String = "ExpenditureId"
        Public Const CostCenterIdCol As String = "CostCenterIdId"
        Public Const BudgetHeadIdCol As String = "BudgetHeadId"
        Public Const MaxStringLegthWordRequired As Integer = 4000
        Public Const TenantDetailsDt As String = "TenantDetails"
        Public Const TenantRiskDetailsDt As String = "TenantRiskDetails"
        Public Const TenantVulnerabilityDetailsDt As String = "TenantVulnerabilityDetails"
        Public Const OrderedByDetailsDt As String = "OrderedByDetails"
        Public Const ContractorDetailsDt As String = "ContractorDetails"
        Public Const ContactEmailDetailsDt As String = "ContactEmailDetails"
        Public Const FaultDetailsDt As String = "FaultDetails"
        Public Const AsbestosDetailsDt As String = "AsbestosDetails"
        Public Const FaultInfoDt As String = "FaultInfo"
        Public Const AsbestosDt As String = "Asbestos"
#End Region

#Region "Drop Down Lists"

        ''' <summary>
        ''' These two constant are use to add the "Please Select" Drop down item in a drop down list.
        ''' </summary>
        ''' <remarks>This is a good practice to avoid spelling mistakes etc.</remarks>

        Public Const DropdownContractorDefaultText As String = "-- Please select contractor --"
        Public Const DropdownContactDefaultText As String = "-- Please select contact --"
        Public Const DropDwonDefaultText As String = "Please Select"

        Public Const ddlDefaultDataTextField As String = "description"
        Public Const ddlDefaultDataValueField As String = "id"

        Public Const noContactFound = "No contact found"

#End Region


#Region "Assign work to fuel servicing contractor"
        'Public Const expenditureID As Integer = 28
        Public Const costCenter As Integer = 11
        Public Const budgetHead As Integer = 25

#End Region

#Region "Customer Risk & Vulberability"
        Public Const RiskCategory As String = "CATDESC"
        Public Const RiskSubCategory As String = "SUBCATDESC"
        Public Const VulnerabilityCategory As String = "CATDESC"
        Public Const VulnerabilitySubCategory As String = "SUBCATDESC"
#End Region

#Region "Defect types dropdown values"
        Public Const ApplianceKey As String = "appliance"
        Public Const BoilerKey As String = "boiler"
        Public Const ApplianceValue As String = "Appliances"
        Public Const BoilerValue As String = "Boiler"
#End Region

#Region "label types for Defect add/amend"
        Public Const Appliance As String = "Appliance:"
        Public Const Boiler As String = "Boiler:"
        Public Const DefectTypeColumn As String = "defectType"
        Public Const DefectTitleColumn As String = "Title"
#End Region


        Public Shared AppointmentTypeColumn As String = "AppointmentType"
        Public Shared PlannedAppointment As String = "Planned"
        Public Shared FaultAppointment As String = "Fault"
        Public Shared MiscAppointment As String = "Misc"
        Public Shared ConditionAppointment As String = "Condition"
        Public Shared AdaptationAppointment As String = "Adaptation"
        Public Shared SbFaultAppointment As String = "SbFault"
        Public Shared CyclicMaintenanceAppointment As String = "Cyclic Maintenance"
        Public Shared MEServicingAppointment As String = "M&E Servicing"
        Public Shared GasAppointment As String = "GAS"
        Public Shared DefectAppointment As String = "Defects"

#Region "letter salutation"
        Public Const SignOff As String = "Yours Sincerely"
        Public Const Team As String = "Customer Services"
#End Region

#Region "Assign to Contractor Property Dashboard"

        Public Shared Provisions As String = "Provisions"
        Public Shared NoneValue As Integer = -1
        Public Shared NoAttributeFound As String = "No attribute found"
        Public Shared EmailSubject_SB As String = "Purchase Order for BHG Works"
        Public Shared AssignToContractorStatus As String = "Assigned To Contractor"
        Public Shared CyclicalEmailSubject As String = " Confirmation of Cyclical Services Order:"
        Public Shared ContractorWorkDt As String = "ContractorWork"
        Public Shared ContractorWorkDetailDt As String = "ContractorWorkDetail"
        Public Shared ContractorDetailDt As String = "ContractorDetailDt"
        Public Shared PurchaseOrderDetailsDt As String = "PurchaseOrderDetails"
        Public Shared AppointmentCSVDetailsDt As String = "AppointmentCSVDetails"
        Public Shared AppointmentSupplierDetailsDt As String = "AppointmentSupplierDetailsDt"
        Public Shared PurchaseItemDetailsDt As String = "PurchaseItemDetailsDt"
        Public Shared PdrContractorIdCol As String = "PDRContractorId"
        Public Shared ContractorIdCol As String = "ContractorId"
        Public Shared ContractorNameCol As String = "ContractorName"
        Public Shared ContactIdCol As String = "ContactId"
        Public Shared EstimateCol As String = "Estimate"
        Public Shared EstimateRefCol As String = "EstimateRef"
        Public Shared ContractStartDateCol As String = "ContractStartDate"
        Public Shared ContractEndDateCol As String = "ContractEndDate"
        Public Shared WorkDetailIdCol As String = "WorkDetailId"
        Public Shared ServiceRequiredCol As String = "ServiceRequired"

#End Region

#Region "Restriction Tab"

        Public Shared DropDownDefaultValues As String = "-1"
        Public Shared DropDownAllValue As String = "-2"
        Public Shared NHBCWarrantyText As String = "NHBC Cover"        

#End Region


    End Class

End Namespace