﻿Imports AS_Utilities
Imports System.Web
Imports AS_BusinessLogic

Public Class PageBase
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

#Region "is Session Exist"

    Public Sub isSessionExist()
        Dim userId As Integer = 0

        userId = SessionManager.getAppServicingUserId()
        If (userId = 0) Then
            Response.Redirect(PathConstants.BridgePath)
        End If
    End Sub
#End Region

#Region "Check Page Access"
    ''' <summary>
    ''' Check Page Access
    ''' </summary>
    ''' <remarks></remarks>
    Sub checkPageAccess()

        Dim objDashboard As DashboardBL = New DashboardBL
        Dim pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1)
        Dim menu As String = GeneralHelper.getPageMenu(pageFileName)
        Dim accessDeniedPage As String = GeneralHelper.getAccessDeniedPage(menu)

        If (Not objDashboard.checkPageAccess(menu)) Then
            Response.Redirect(accessDeniedPage)
        End If

    End Sub

#End Region

#Region "Session Time out"
    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
        Me.checkPageAccess()
    End Sub
#End Region

End Class
