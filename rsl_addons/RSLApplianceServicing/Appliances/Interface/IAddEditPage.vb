﻿Interface IAddEditPage
    Sub populateDropDown(ByVal ddl As DropDownList)
    Sub populateDropDowns()
    Sub loadData()
    Sub saveData()
    Function validateData() As Boolean
    Sub resetControls()
End Interface