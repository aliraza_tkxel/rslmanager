﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="Status.aspx.vb" Inherits="Appliances.Status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="statusControl" TagName="StatusControl" Src="~/Controls/Resources/CreateStatus.ascx" %>
<%@ Register TagPrefix="actionControl" TagName="ActionControl" Src="~/Controls/Resources/CreateAction.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <p style="background-color: Black; height: 30px; text-align: justify; font-family: Tahoma;
            font-weight: bold; margin: 0 0 8px; font-size: 15px; padding-left: 6px; padding-top: 5px;">
            <font color="white">Resources</font>
        </p>
        <div style="border: 1px solid gray; height: 603px;">
            <h3 style="border-bottom: 1px solid gray; margin: 2px 0px 2px 9px;">
                Status And Actions</h3>
            <asp:UpdatePanel runat="server" ID="updPnlStatus" UpdateMode="Conditional" style="height: auto"
                ChildrenAsTriggers="true">
                <ContentTemplate>
                    <div style="float: left; width: 30%; border-right: 1px solid gray; height: 580px;
                        overflow: scroll">
                        <asp:TreeView ID="trVwStatus" runat="server" ShowLines="true" OnTreeNodePopulate="trVwStatus_TreeNodePopulate"
                            OnTreeNodeCollapsed="trVwStatus_TreeNodeCollapsed" OnSelectedNodeChanged="trVwStatus_SelectedNodeChanged"
                            ExpandDepth="1" OnDisposed="tree_Disposed" OnLoad="tree_Load" NodeStyle-Font-Bold="true"
                            NodeStyle-ForeColor="Black">
                            <Nodes>
                                <asp:TreeNode PopulateOnDemand="true" Text="Status" Value="100"></asp:TreeNode>
                            </Nodes>
                        </asp:TreeView>
                    </div>
                    <div style="float: left; width: 69%; height: auto;">
                        <asp:Panel runat="server" ID="pnlStatusControl">
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                            <statusControl:StatusControl ID="sControl" runat="server" Visible="false" />
                            <actionControl:ActionControl ID="aControl" runat="server" Visible="false" />
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgressStatus" runat="server" AssociatedUpdatePanelID="updPnlStatus"
                DisplayAfter="5">
                <ProgressTemplate>
                    <div class="loading-image">
                        <img alt="Please Wait" src="../../Images/progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    
</asp:Content>
