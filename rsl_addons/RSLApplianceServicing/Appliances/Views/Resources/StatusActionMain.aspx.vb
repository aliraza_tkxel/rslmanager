﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class StatusActionMain
    Inherits PageBase


#Region "Attributes"
    Public Delegate Sub addUserDelegate(ByVal abc As String)
    Dim objUsersBL As UsersBL = New UsersBL()
    Public ckBoxFileName As New CheckBox()
    Public txtSignature As New TextBox()
#End Region

#Region "Functions"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            Me.isSessionExist()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            MaintainScrollPositionOnPostBack = True
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub lnkBtnName_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim gvr As GridViewRow = (CType(sender, LinkButton)).Parent.Parent
        Dim index As Integer = gvr.RowIndex
        Dim LinkButtonName As LinkButton = New LinkButton()
        Dim ObjAddUser As AddUser = New AddUser()
        Try
            LinkButtonName = CType((grdUsers.Rows(index).FindControl("lnkBtnName")), LinkButton)
            Dim employeeId As Integer = LinkButtonName.CommandArgument

            pnlStatusAndLetters.Visible = False
            updPnlResources.Update()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

    Public Sub editUser_OnClick(ByVal employeeId As Integer)

        Dim edittUserDataSet As DataSet = New DataSet()
        Dim edittUserPatchDataSet As DataSet = New DataSet()
        Dim edittUserInspectionDataSet As DataSet = New DataSet()
        Dim LinkButtonName As LinkButton = New LinkButton()
        Dim txtEmployeeName As TextBox = New TextBox()
        Dim chkBoxList As CheckBoxList = New CheckBoxList()
        Try
            Dim objAddUserPnl As Panel = CType(Me.FindControl("pnlAddUser"), Panel)
            objUsersBL.EditUser(employeeId, edittUserDataSet, edittUserPatchDataSet, edittUserInspectionDataSet)
            chkBoxList = CType(objAddUserPnl.FindControl("chkBoxLst"), CheckBoxList)
            txtEmployeeName = CType(objAddUserPnl.FindControl("txtFind"), TextBox)
            txtEmployeeName.Text = edittUserDataSet.Tables(0).Rows(0)("Name")
            chkBoxList.DataSource = edittUserInspectionDataSet
            chkBoxList.DataBind()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

End Class