﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class Resources
    Inherits PageBase

#Region "Attributes"
    Public Delegate Sub addUserDelegate(ByVal abc As String)
    Dim objUsersBL As UsersBL = New UsersBL()
    Public ckBoxFileName As New CheckBox()
    Public txtSignature As New TextBox()
#End Region

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            MaintainScrollPositionOnPostBack = True
            Me.FindControlsOfUserControl()

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                pnlAddUserControl.Visible = False
                Me.getUsers()
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "pgv Users Row Editing"
    Protected Sub pgvUsers_RowEditing(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim lb As Label = New Label()
            Dim ddl As DropDownList = New DropDownList()
            Dim btnEdit As Button = New Button()
            Dim btnSave As Button = New Button()
            Dim hfl As HiddenField = New HiddenField()

            btnEdit = CType((grdUsers.Rows(index).FindControl("btnEdit")), Button)
            btnSave = CType((grdUsers.Rows(index).FindControl("btnSave")), Button)
            lb = CType((grdUsers.Rows(index).FindControl("lblUserType")), Label)
            ddl = CType((grdUsers.Rows(index).FindControl("ddlUserTypeOptions")), DropDownList)
            hfl = CType(grdUsers.Rows(index).FindControl("hflLookupId"), HiddenField)

            'Session(SessionConstants.EditResourceId) = btnEdit.CommandArgument;
            btnEdit.Visible = False
            btnSave.Visible = True
            lb.Visible = False
            PopulateUserType(index, ddl)
            ddl.EnableViewState = True
            ddl.Visible = True
            ddl.SelectedValue = hfl.Value
            pnlAddUserControl.Visible = False

            btnSave.Focus()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "pgv Users Row Updating"

    Protected Sub pgvUsers_RowUpdating(ByVal sender As Object, ByVal e As EventArgs)

        Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
        Dim index As Integer = gvr.RowIndex
        Dim lb As Label = New Label()
        Dim ddl As DropDownList = New DropDownList()
        Dim btnEdit As Button = New Button()
        Dim btnSave As Button = New Button()
        Dim hfl As HiddenField = New HiddenField()
        Try
            btnEdit = CType((grdUsers.Rows(index).FindControl("btnEdit")), Button)
            btnSave = CType((grdUsers.Rows(index).FindControl("btnSave")), Button)
            lb = CType((grdUsers.Rows(index).FindControl("lblUserType")), Label)
            ddl = grdUsers.Rows(index).FindControl("ddlUserTypeOptions")
            ddl = CType(ddl, DropDownList)
            ViewState(ViewStateConstants.SaveEmployeeId) = btnSave.CommandArgument
            'this text 'll be used to popuplate the applinace user type in session
            Dim selectedText As String
            selectedText = ddl.SelectedItem.Text
            UpdateResource(CType(ddl.SelectedValue, Int32), selectedText)
            btnEdit.Visible = True
            btnSave.Visible = False
            lb.Visible = True
            ddl.Visible = False

            'To stop scrolling to top on edit button click.
            btnEdit.Focus()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "btn Add More Operatives Click"
    Protected Sub btnAddMoreOperatives_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddMoreOperatives.Click
        Try

            pnlAddUserControl.Visible = True
            Dim pnlAddUser As Panel = CType(AddUsers.FindControl("pnlAddUser"), Panel)
            Dim txtBoxFind As TextBox = CType(pnlAddUser.FindControl("txtFind"), TextBox)
            Dim lbltxtFind As Label = CType(pnlAddUser.FindControl("lblErrorMsg"), Label)
            Dim lblHeader As Label = CType(pnlAddUser.FindControl("lblHeader"), Label)
            Dim btnEdit As Button = CType(pnlAddUser.FindControl("btnEdit"), Button)
            Dim btnSave As Button = CType(pnlAddUser.FindControl("btnSave"), Button)
            btnEdit.Visible = False
            btnSave.Visible = True
            lbltxtFind.Text = String.Empty
            txtBoxFind.Text = String.Empty
            lblHeader.Text = ApplicationConstants.AddUserHeader
            AddUsers.loadAllPanels()

            updPnlResources.Update()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Name Click"
    ''' <summary>
    ''' lnk Btn Name Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnName_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim gvr As GridViewRow = (CType(sender, LinkButton)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim linkButtonName As LinkButton = New LinkButton()
            Dim objAddUser As AddUser = New AddUser()

            linkButtonName = CType((grdUsers.Rows(index).FindControl("lnkBtnName")), LinkButton)
            Dim employeeId As Integer = linkButtonName.CommandArgument
            pnlAddUserControl.Visible = True
            AddUsers.editUser(employeeId)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "edit User On Click"
    ''' <summary>
    ''' edit User On Click
    ''' </summary>
    ''' <param name="employeeId"></param>
    ''' <remarks></remarks>
    Public Sub editUser_OnClick(ByVal employeeId As Integer)

        Dim edittUserDataSet As DataSet = New DataSet()
        Dim edittUserPatchDataSet As DataSet = New DataSet()
        Dim edittUserInspectionDataSet As DataSet = New DataSet()
        Dim LinkButtonName As LinkButton = New LinkButton()
        Dim txtEmployeeName As TextBox = New TextBox()
        Dim chkBoxList As CheckBoxList = New CheckBoxList()
        Try
            Dim objAddUserPnl As Panel = CType(Me.FindControl("pnlAddUser"), Panel)
            objUsersBL.EditUser(employeeId, edittUserDataSet, edittUserPatchDataSet, edittUserInspectionDataSet)
            chkBoxList = CType(objAddUserPnl.FindControl("chkBoxLst"), CheckBoxList)
            txtEmployeeName = CType(objAddUserPnl.FindControl("txtFind"), TextBox)
            txtEmployeeName.Text = edittUserDataSet.Tables(0).Rows(0)("Name")
            chkBoxList.DataSource = edittUserInspectionDataSet
            chkBoxList.DataBind()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "get Users"
    Public Sub getUsers()

        Dim resultDataset As DataSet = New DataSet()
        Try
            objUsersBL.getUsers(resultDataset)
            If resultDataset.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Else
                grdUsers.DataSource = resultDataset
                grdUsers.DataBind()

            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate User Type"
    Public Sub PopulateUserType(ByVal index As Int32, ByVal dll As DropDownList)
        'objUsersBL.PopulateUserType()
        Dim codeID As String = "UserTypeID"
        Dim codeName As String = "Description"
        Dim ddl As DropDownList = New DropDownList()
        Dim userTypeList As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        Try
            objUsersBL.getUserTypes(index, userTypeList)
            If userTypeList.Count > 0 Then
                ddl = CType((grdUsers.Rows(index).FindControl("ddlUserTypeOptions")), DropDownList)
                ddl.DataSource = userTypeList
                ddl.DataValueField = codeID
                ddl.DataTextField = codeName
                ddl.DataBind()
                ddl.Items.Add(New ListItem("Remove User", "-1"))
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Update Resource"
    ''' <summary>
    ''' Update Resource
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="selectedText"></param>
    ''' <remarks></remarks>
    Public Sub UpdateResource(ByVal value As Int32, ByVal selectedText As String)
        Dim resultDataset As DataSet = New DataSet()
        Dim EmployeeId As Int32 = ViewState(ViewStateConstants.SaveEmployeeId)
        Try
            If (value > 0) Then
                objUsersBL.UpdateResource(EmployeeId, value, resultDataset)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserUpdatedSuccessfuly, False)
            Else
                objUsersBL.deleteUser(EmployeeId, resultDataset)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDeletedSuccessfuly, False)
            End If
            grdUsers.DataSource = resultDataset
            grdUsers.DataBind()
            updPnlResources.Update()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'this condition 'll be used to popuplate the applinace user type in session
                If EmployeeId = SessionManager.getAppServicingUserId() Then
                    'it means some option is selected which is not delete user          
                    If (value > 0) Then
                        SessionManager.setUserType(selectedText)
                    Else
                        'it means that delete user option has been selected
                        SessionManager.setUserType(Nothing)
                    End If
                End If
            End If
        End Try
    End Sub

#End Region

#Region "Find Controls Of User Control"
    Protected Sub FindControlsOfUserControl()

        ckBoxFileName = DirectCast(AddUsers.FindControl("ckBoxFileName"), CheckBox)
        txtSignature = DirectCast(AddUsers.FindControl("txtSignature"), TextBox)

    End Sub
#End Region

#End Region

End Class