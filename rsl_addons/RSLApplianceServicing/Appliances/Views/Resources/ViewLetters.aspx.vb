﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class ViewLetters
    Inherits PageBase

    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objActionBL As ActionBL = New ActionBL()
    Dim objLetterBL As LettersBL = New LettersBL()


#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Me.isSessionExist()
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Statusddl As DropDownList = New DropDownList()

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblGridMessage, pnlGridMessage)
            If Not (IsPostBack) Then
                Statusddl = CType((pnlTemplateManagement.FindControl("ddlStatus")), DropDownList)
                Me.LoadStatusDDL(Statusddl)
                Me.LoadActions()
                Me.loadActionsDDL(Statusddl)

                Me.GetLetters(-1, -1, Nothing, Nothing, 0)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.setMessage(lblGridMessage, pnlGridMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#Region "Event Handlers"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged

        loadActionsDDL(ddlStatus)

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Me.PerformSearch()
    End Sub

    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.deleteLetter(sender)
    End Sub

#End Region

#Region "Private methods"

    Protected Sub LoadAllLeters()

    End Sub

    Protected Sub LoadStatusDDL(ByRef ddl As DropDownList)
        Dim codeID As String = "StatusID"
        Dim codeName As String = "Description"
        Dim statusList As List(Of StatusBO) = New List(Of StatusBO)()
        objStatusBL.getStatusList(statusList)

        ddl.DataSource = statusList
        ddl.DataValueField = codeID
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub

    Protected Sub loadActionsDDL(ByRef ddlStatus As DropDownList)
        Dim statusSelected As Int32 = CType(ddlStatus.SelectedItem.Value, Int32)

        Dim actionID As String = "ActionID"
        Dim actionName As String = "Title"
        Dim actionList As List(Of ActionBO) = New List(Of ActionBO)()
        objActionBL.getActionByStatusId(statusSelected, actionList)

        Dim ActionsDDL As DropDownList = CType((pnlTemplateManagement.FindControl("ddlAction")), DropDownList)

        ActionsDDL.DataSource = actionList
        ActionsDDL.DataValueField = actionID
        ActionsDDL.DataTextField = actionName
        ActionsDDL.DataBind()

        ActionsDDL.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ActionsDDL.SelectedValue = ApplicationConstants.DropDownDefaultValue

        pnlTemplateManagement.Update()
    End Sub

    Protected Sub LoadActions()
        Dim resultset As DataSet = New DataSet()
        objStatusBL.getActions(resultset)

        ViewState.Add(ViewStateConstants.ActionDataTable, resultset)
    End Sub

    Protected Sub PerformSearch()

        Dim statusSelected As Integer = ddlStatus.SelectedItem.Value
        Dim actionSelected As Integer = ddlAction.SelectedItem.Value

        Dim letterTitle As String = txtLetterTitle.Text
        Dim letterCode As String = txtLetterCode.Text
        Dim isAlternativeServicing As Boolean = chkIsAlternative.Checked

        If (letterTitle = String.Empty) Then
            letterTitle = Nothing
        End If

        If (letterCode = String.Empty) Then
            letterCode = Nothing
        End If

        Me.GetLetters(statusSelected, actionSelected, letterTitle, letterCode, isAlternativeServicing)
    End Sub

    Protected Sub GetLetters(ByVal statusSelected As Integer, ByVal actionSelected As Integer, ByVal letterTitle As String, ByVal letterCode As String, ByVal isAlternativeServicing As Boolean)
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        Dim letterBO As LetterBO = New LetterBO(statusSelected, actionSelected, letterTitle, letterCode, isAlternativeServicing, Nothing, -1, -1)

        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLettersForSearch(letterBO, resultSet)

        If (resultSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblGridMessage, pnlGridMessage, UserMessageConstants.NoRecordFound, True)
            Me.grdViewLetters.Visible = False
        Else
            Me.grdViewLetters.Visible = True
            Me.grdViewLetters.DataSource = resultSet
            Me.grdViewLetters.DataBind()
        End If



    End Sub

    Protected Sub deleteLetter(ByVal sender As Object)
        Dim resultDataSet As DataSet = New DataSet()

        Dim lnkBtnDelete As LinkButton = CType(sender, LinkButton)

        Dim letterID As Integer = CType(lnkBtnDelete.CommandArgument, Integer)

        objLetterBL.deleteStandardLetter(letterID, resultDataSet)

        Me.PerformSearch()

        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterDeleted, False)
    End Sub

#End Region

    Protected Sub grdViewLetters_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdViewLetters.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim lblTitle As Label = DirectCast(e.Row.FindControl("lblTitle"), Label)
                Dim hdnIsAlternativeServicing As Boolean = DirectCast(e.Row.FindControl("hdnIsAlternativeServicing"), CheckBox).Checked
                lblTitle.Text = If(hdnIsAlternativeServicing, lblTitle.Text + " (AH)", lblTitle.Text)
                End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub grdViewLetters_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdViewLetters.SelectedIndexChanged
        Dim linkBtnEdit As LinkButton = CType(sender, LinkButton)
        linkBtnEdit.PostBackUrl = "../../Views/Resources/CreateLetter.aspx?LetterId=33"
    End Sub

End Class