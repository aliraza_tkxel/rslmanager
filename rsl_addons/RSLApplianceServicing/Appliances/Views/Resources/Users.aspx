﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="Users.aspx.vb" Inherits="Appliances.Users1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="status" TagName="Status" Src="~/Controls/Resources/Status.ascx" %>
<%@ Register TagPrefix="letters" TagName="Letters" Src="~/Controls/Resources/Letters.ascx" %>
<%@ Register TagPrefix="addUsers" TagName="AddUsers" Src="~/Controls/Resources/AddUser.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function getEditUser() {
            onClickName(1)
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <table width="100%" id="tblUserHeader">
        <tr>
            <td align="left" style="text-align: left; font-weight: bold; font-size: 12px; width: 20%">
                Users
            </td>
            <td align="right" style="text-align: right; width: 80%">
                <asp:Button ID="btnAddMoreOperatives" runat="server" Text="Add More Operatives" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="updPnlUsers" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="grdUsers" runat="server" AutoGenerateColumns="False" Width="100%"
                Style="overflow: scroll" ShowHeader="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnEdit" CausesValidation="false" CommandArgument='<%#Eval("EMPLOYEEID") %>'
                                OnClick="pgvUsers_RowEditing" runat="server" Text="Edit" />
                            <asp:Button ID="btnSave" CausesValidation="false" CommandArgument='<%#Eval("EMPLOYEEID") %>'
                                OnClick="pgvUsers_RowUpdating" Visible="false" runat="server" Text="Save" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnName" runat="server" Font-Underline="true" Text='<%# Bind("FIRSTNAME") %>'
                                OnClick="lnkBtnName_Click" CommandArgument='<%#Eval("EMPLOYEEID") %>'></asp:LinkButton>
                            <br />
                            <asp:Label ID="lblJobDetail" runat="server" Text='<%# Bind("JOBTITLE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField ID="hflLookupId" runat="server" Value='<%#Eval("UserTypeID") %>' />
                            <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                            <asp:DropDownList ID="ddlUserTypeOptions" Width="105px" Visible="false" EnableViewState="true"
                                runat="server">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressStatus" runat="server" AssociatedUpdatePanelID="updPnlUsers"
        DisplayAfter="5">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
