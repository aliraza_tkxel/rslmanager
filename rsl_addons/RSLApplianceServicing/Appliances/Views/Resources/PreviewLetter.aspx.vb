﻿Imports System.Data
Imports AS_BusinessLogic
Imports AS_Utilities

Public Class PreviewLetter
    Inherits PageBase

    Dim objLetterBL As LettersBL = New LettersBL()
    Dim stdLetterID As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strStdLetterID As String = Request.QueryString("letterID")
        Dim strLetterBody As String = Session(SessionConstants.PreviewLetterBody)
        Dim strLetterTitle As String = Session(SessionConstants.PreviewLetterTitle)
        Dim isAlternative As Boolean = Session(SessionConstants.IsAlternative)

        If (Int32.TryParse(strStdLetterID, stdLetterID)) Then
            Me.stdLetterID = Int32.Parse(strStdLetterID)

            If ((Not String.IsNullOrEmpty(strLetterBody)) And (Not IsNothing(strLetterBody)) And (Not String.IsNullOrEmpty(strLetterTitle)) And (Not IsNothing(strLetterBody))) Then
                Me.lblLetterTitle.Text = If(isAlternative, strLetterTitle + " (AH)", strLetterTitle)
                Me.lblLetterBody.Text = strLetterBody
            Else
                Me.LoadLetter()
            End If

            Me.btnBack.PostBackUrl = "~/Views/Resources/" + Request.QueryString("src") + ".aspx?LetterId=" + strStdLetterID
            Me.btnBack.Visible = True
        Else
            Me.lblLetterTitle.Text = strLetterTitle
            Me.lblLetterBody.Text = strLetterBody

            Me.btnBack.PostBackUrl = "~/Views/Resources/" + Request.QueryString("src") + ".aspx"
            Me.btnBack.Visible = True
        End If

    End Sub

    Protected Sub LoadLetter()
        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLetterById(Me.stdLetterID, resultSet)

        If (Not DBNull.Value.Equals((resultSet.Tables(0).Rows(0).Item("IsAlternativeServicing")))) Then
            Me.lblLetterTitle.Text = If(resultSet.Tables(0).Rows(0).Item("IsAlternativeServicing"), resultSet.Tables(0).Rows(0).Item("Title") + " (AH)", resultSet.Tables(0).Rows(0).Item("Title"))
        Else
            Me.lblLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")
        End If
        Me.lblLetterBody.Text = resultSet.Tables(0).Rows(0).Item("Body")

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim title As String = CType(Me.lblLetterTitle.Text, String)
        Dim body As String = CType(Me.lblLetterBody.Text, String)

        Session.Add(SessionConstants.PreviewLetterTitle, title)
        Session.Add(SessionConstants.PreviewLetterBody, body)
    End Sub
End Class