﻿Imports System.Data
Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class CreateLetter
    Inherits PageBase

    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objActionBL As ActionBL = New ActionBL()
    Dim objLetterBL As LettersBL = New LettersBL()
    Dim stdLetterID As Integer

#Region "Event Handlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.ClientScript.RegisterClientScriptBlock(Type.GetType("System.String"), "openPreview()", "openPreview()", True)

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not (IsPostBack) Then

                Me.loadStatusDDL()
                'Me.LoadActions()
                'Me.loadActionsDDL()

                Dim strSrc As String = Request.QueryString("src")
                If ((Not String.IsNullOrEmpty(strSrc)) And (strSrc = "Resources")) Then
                    Me.btnCancel.PostBackUrl = "~/Views/Resources/Resources.aspx"
                End If

                Dim strStdLetterID As String = Request.QueryString("letterID")
                If (Int32.TryParse(strStdLetterID, stdLetterID)) Then
                    Me.stdLetterID = Int32.Parse(strStdLetterID)
                    Me.LoadLetter()
                    'Me.btnPreviewStandardLetter.Enabled = vbYes
                    'Me.btnPreviewStandardLetter.PostBackUrl = "~/Views/Resources/PreviewLetter.aspx?src=CreateLetter&LetterId=" + strStdLetterID
                End If
                'Me.btnPreviewStandardLetter.PostBackUrl = "~/Views/Resources/PreviewLetter.aspx?src=CreateLetter"

                Dim strLetterBody As String = Session(SessionConstants.PreviewLetterBody)
                Dim strLetterTitle As String = Session(SessionConstants.PreviewLetterTitle)
                Dim strLetterCode As String = Session(SessionConstants.PreviewLetterCode)
                Dim isAlternative As String = Session(SessionConstants.IsAlternative)
                Dim intStatusId As Integer = Session(SessionConstants.PreviewLetterStatusId)
                Dim intActionId As Integer = Session(SessionConstants.PreviewLetterActionId)

                If ((Not String.IsNullOrEmpty(strLetterBody)) And (Not IsNothing(strLetterBody))) Then
                    txtLetterTemplate.Text = strLetterBody
                    Session.Remove(SessionConstants.PreviewLetterBody)

                End If

                If ((Not String.IsNullOrEmpty(strLetterTitle)) And (Not IsNothing(strLetterBody))) Then
                    txtLetterTitle.Text = strLetterTitle
                    Session.Remove(SessionConstants.PreviewLetterTitle)
                End If

                If ((Not String.IsNullOrEmpty(strLetterCode)) And (Not IsNothing(strLetterCode))) Then
                    txtLetterCode.Text = strLetterCode
                    Session.Remove(SessionConstants.PreviewLetterCode)
                End If

                If ((Not String.IsNullOrEmpty(isAlternative)) And (Not IsNothing(isAlternative))) Then
                    chkIsAlternative.Checked = Boolean.Parse(isAlternative)
                    Session.Remove(SessionConstants.IsAlternative)
                End If

                If ((Not String.IsNullOrEmpty(intStatusId)) And (Not IsNothing(intStatusId)) And (Not intStatusId = 0)) Then
                    ddlStatus.SelectedValue = intStatusId
                    Session.Remove(SessionConstants.PreviewLetterStatusId)
                    loadActionsDDL()
                End If

                If ((Not String.IsNullOrEmpty(intActionId)) And (Not IsNothing(intActionId))) Then
                    ddlAction.SelectedValue = intActionId
                    Session.Remove(SessionConstants.PreviewLetterActionId)
                End If

            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            loadActionsDDL()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnAddStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddStandardLetter.Click
        Try
            Dim statusSelected As Int32 = CType(CType((pnlAddLetterTemplate.FindControl("ddlStatus")), DropDownList).SelectedItem.Value, Int32)
            Dim actionSelected As Int32 = CType(CType((pnlAddLetterTemplate.FindControl("ddlAction")), DropDownList).SelectedItem.Value, Int32)

            Dim title As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTitle")), TextBox).Text, String)
            Dim code As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterCode")), TextBox).Text, String)
            Dim isAlternativeServicing As Boolean = CType((pnlAddLetterTemplate.FindControl("chkIsAlternative")), CheckBox).Checked
            Dim body As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTemplate")), TextBox).Text, String)

            Dim strStdLetterID As String = Request.QueryString("letterID")
            If (Int32.TryParse(strStdLetterID, stdLetterID)) Then
                Me.stdLetterID = Int32.Parse(strStdLetterID)
            Else
                Me.stdLetterID = -1
            End If

            If (statusSelected = ApplicationConstants.DropDownDefaultValue) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoStatus, True)
                Return
            ElseIf (actionSelected = ApplicationConstants.DropDownDefaultValue) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoAction, True)
                Return
            ElseIf (IsNothing(title) Or title = String.Empty) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoTitle, True)
                Return
            ElseIf (IsNothing(code) Or code = String.Empty) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoCode, True)
                Return
                'ElseIf (Not Int32.TryParse(code, Nothing)) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterCodeNotNumeric, True)
                '    Return
            ElseIf (body.Length <= 3 Or IsNothing(body) Or body = String.Empty) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNoBody, True)
                Return
            End If

            Dim resultset As DataSet = New DataSet()

            Dim letterBO As LetterBO

            If (Me.stdLetterID = -1) Then
                letterBO = New LetterBO(statusSelected, actionSelected, title, code, isAlternativeServicing, body, 615, 615)
                objLetterBL.addStandardLetterTemplate(letterBO, resultset)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterCreated, False)
            Else
                letterBO = New LetterBO(Me.stdLetterID, statusSelected, actionSelected, title, code, isAlternativeServicing, body, 615, 615)
                objLetterBL.updateStandardLetterTemplate(letterBO, resultset)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterUpdated, False)
            End If

            Me.btnCancel.Text = "Back To List"

            If (Me.stdLetterID = -1) Then
                Me.loadStatusDDL()
                Me.loadActionsDDL()

                Me.txtLetterCode.Text = String.Empty
                Me.txtLetterTemplate.Text = String.Empty
                Me.txtLetterTitle.Text = String.Empty
                chkIsAlternative.Checked = False
            End If

            'If (Not Me.stdLetterID = -1) Then
            'Me.LoadLetter()
            'End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StandardLetterNotCreated, True)
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

    Protected Sub btnPreviewStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPreviewStandardLetter.Click
        Try
            Dim title As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTitle")), TextBox).Text, String)
            Dim body As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterTemplate")), TextBox).Text, String)
            Dim code As String = CType(CType((pnlAddLetterTemplate.FindControl("txtLetterCode")), TextBox).Text, String)

            Dim statusId As Integer = CType(ddlStatus.SelectedValue, Integer)
            Dim actionId As Integer = CType(ddlAction.SelectedValue, Integer)
            Dim isAlternative As Boolean = chkIsAlternative.Checked

            Session.Add(SessionConstants.IsAlternative, isAlternative)
            Session.Add(SessionConstants.PreviewLetterTitle, title)
            Session.Add(SessionConstants.PreviewLetterBody, body)

            Session.Add(SessionConstants.PreviewLetterCode, code)

            Session.Add(SessionConstants.PreviewLetterStatusId, statusId)
            Session.Add(SessionConstants.PreviewLetterActionId, actionId)

            Dim strStdLetterID As String = Request.QueryString("letterID")
            If (Int32.TryParse(strStdLetterID, stdLetterID)) Then
                Me.stdLetterID = Int32.Parse(strStdLetterID)
            End If

            If (Me.stdLetterID = -1) Then
                Response.Redirect("PreviewLetter.aspx?src=CreateLetter", False)
            Else
                Response.Redirect("PreviewLetter.aspx?src=CreateLetter&LetterId=" + strStdLetterID, False)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub saveFile(ByVal sender As Object, ByVal e As AjaxControlToolkit.AjaxFileUploadEventArgs)

        Dim fullPath As String = "/Images/Upload_test/" + e.FileName

        'Save your File
        richTxtComments.AjaxFileUpload.SaveAs(Server.MapPath(fullPath))


        'Tells the HtmlEditorExtender where the file is otherwise it will render as: <img src="" />
        e.PostedUrl = fullPath
    End Sub

#End Region

#Region "Private methods"

    Private Sub LoadLetter()
        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLetterById(Me.stdLetterID, resultSet)

        Dim statusSelected As Integer = resultSet.Tables(0).Rows(0).Item("StatusID")
        Dim actionSelected As Integer = resultSet.Tables(0).Rows(0).Item("ActionID")

        Me.txtLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")
        Me.txtLetterCode.Text = resultSet.Tables(0).Rows(0).Item("Code")

        If (Not DBNull.Value.Equals((resultSet.Tables(0).Rows(0).Item("IsAlternativeServicing")))) Then
            Me.chkIsAlternative.Checked = resultSet.Tables(0).Rows(0).Item("IsAlternativeServicing")
            'Me.txtLetterTitle.Text = If(chkIsAlternative.Checked, txtLetterTitle.Text + " (AH)", txtLetterTitle.Text)
        End If

        Me.txtLetterTemplate.Text = resultSet.Tables(0).Rows(0).Item("Body")

        Dim i As Integer = 0

        For Each dropDownBO As StatusBO In ddlStatus.DataSource

            If (dropDownBO.StatusID = statusSelected) Then
                'ddlStatus.SelectedValue = dropDownBO.StatusID
                ddlStatus.SelectedIndex = i
                ddlStatus.SelectedValue = dropDownBO.StatusID
            End If
            i = i + 1
        Next

        Me.loadActionsDDL()

        i = 0

        For Each dropDownBO As ActionBO In ddlAction.DataSource

            If (dropDownBO.ActionID = actionSelected) Then
                ddlAction.SelectedValue = dropDownBO.ActionID
            End If
            i = i + 1
        Next

    End Sub

    Protected Sub loadStatusDDL()
        Dim codeID As String = "StatusID"
        Dim codeName As String = "Description"
        Dim statusList As List(Of StatusBO) = New List(Of StatusBO)()
        objStatusBL.getStatusList(statusList)

        ddlStatus.DataSource = statusList
        ddlStatus.DataValueField = codeID
        ddlStatus.DataTextField = codeName
        ddlStatus.DataBind()

        ddlStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub

    Protected Sub loadActionsDDL()
        Dim statusSelected As Int32 = CType(ddlStatus.SelectedItem.Value, Int32)

        If (Not statusSelected = -1) Then

            'ddlAction.Items.Clear()
            'ddlAction.SelectedIndex = -1
            'ddlAction.SelectedValue = vbNull
            'ddlAction.ClearSelection()

            Dim actionID As String = "ActionID"
            Dim actionName As String = "Title"
            Dim actionList As List(Of ActionBO) = New List(Of ActionBO)
            objActionBL.getActionByStatusId(statusSelected, actionList)

            ddlAction.DataSource = actionList
            ddlAction.DataValueField = actionID
            ddlAction.DataTextField = actionName
            ddlAction.DataBind()
        End If

        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue

        'pnlAddLetterTemplate.Update()
    End Sub

    Protected Sub LoadActions()
        Dim resultset As DataSet = New DataSet()
        objStatusBL.getActions(resultset)

        ViewState.Add(ViewStateConstants.ActionDataTable, resultset)
    End Sub


#End Region

End Class