﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ResourceMaster.Master"
    CodeBehind="Resources.aspx.vb" Inherits="Appliances.Resources" EnableEventValidation="False" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="users" TagName="Users" Src="~/Controls/Resources/Users.ascx" %>
<%@ Register TagPrefix="addUsers" TagName="AddUsers" Src="~/Controls/Resources/AddUser.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function openUploadWindow() {
            javascript: window.open('../../Views/Common/UploadDocument.aspx?type=signature', '_blank', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
            return false;
        }

        function fireSignatureNameEvent(some) {
            if (document.getElementById('<%=ckBoxFileName.ClientID%>').checked == true) {

                document.getElementById('<%=ckBoxFileName.ClientID%>').checked = false

            } else {

                document.getElementById('<%=ckBoxFileName.ClientID%>').checked = true
            }

            setTimeout('__doPostBack(\'<%=ckBoxFileName.UniqueID%>\',\'\')', 0);
        }        
    </script>
    <style type="text/css">
        .grdUsers
        {
            margin-top: 0px;
        }
        
        #tblAddUser
        {
            margin-top: 6px;
        }
        
        #tblAddUser td
        {
            padding: 8px 0 2px 10px;
        }
        
        #tblAddUser td, #tblAddUser th
        {
            text-align: left;
        }
        #tblUserHeader
        {
            border-bottom: 1px solid black;
            height: 37px;
        }
        #tblUserHeader td
        {
            border: 0px;
            text-align: left;
            font-weight: bold;
            font-size: 12px;
            width: 20%;
            padding: 10px;
        }
        #tblAddUser select
        {
            width: 201px;
        }
        
        .pnlUsers
        {
            float: left;
            /*height: 547px;*/
            overflow: auto;
            width: 41%;
        }
        
        .list-select select
        {
            border-radius: 0px;
            border: 1px solid #b1b1b1;
            height: 25px !important;
            font-size: 12px !important;
            width: 205px !important;
        }
        
        .gridfaults-header
        {
            font-weight: normal;
            font-size: 13px;
            border-bottom: 1px solid #BBB !important;
            border-left: 1px solid #BBB !important;
            border-right: 1px solid #BBB !important;
        }
        .resource-menu
        {
            width: 57%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Resources" class="group">
        <script type="text/JavaScript">
            $.extend($.expr[':'], {
                'containsi': function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase()
                           .indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });
            function filterUsersGrid() {

                var text = $(".UserFilter").val().trim();
                
                // Trim text to avoid any noise just by blank spaces
                //text = $.trim(text);
                
                // Split text into word to search of each word (seprated by space)
                //var data = text.split(" ");
                
                // Get all rows from users users table(grid)
                var tblRows = $(".grdUsers").find("tr");
                if (text == "") {
                    //Show all table rows when text in filter(textbox) is empty string
                    tblRows.show();
                }
                else {
                    // Hide all table rows
                    tblRows.hide();
                    
                    // Filter rows for each word in filter(textbox)
                    //$.each(data, function (i, v) {
                    //    tbl.filter(":containsi('" + v + "')").show();
                    //});
                    
                    // Filter rows for the text menioned in filter (textbox)
                    tblRows.filter(":containsi('" + text + "')").show();
                }
                
            }
            function BindControlEvents() {

                $(".UserFilter").keyup(function () {
                    filterUsersGrid();
                });

                $(".UserFilter").change(function () {
                    filterUsersGrid();
                });
            }

            $(document).ready(function () {
                BindControlEvents();
            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {
                filterUsersGrid();
                BindControlEvents();
            }); 
        </script>
        <asp:UpdatePanel runat="server" ID="updPnlResources" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUsers" ViewStateMode="Enabled" CssClass="pnlUsers">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="portlet">
                        <div class="header">
                            <span class="header-label">Users</span>
                            <div class="field right">
                                <asp:Button ID="btnAddMoreOperatives" CssClass="btn btn-xs btn-blue " style="padding: 2px 20px !important; margin: -3px 10px 0 0;" runat="server" Text="Add More Operatives" UseSubmitBehavior="false" />
                                <asp:TextBox ID="txtSearchUser" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Disabled" 
                                    class="searchbox styleselect-control UserFilter right" PlaceHolder="Search User" runat="server">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="portlet-body" style="padding-bottom:0;">
                            <div style="height: 547px; overflow:auto; padding: 2px; width:100%">
                                <asp:GridView ID="grdUsers" runat="server" CssClass="grdUsers dashboard webgrid table table-responsive" AutoGenerateColumns="False"
                                    ShowHeader="False" BorderWidth="0px" BorderStyle="Solid" Width="100%"
                                    AllowSorting="false" AllowPaging="False" GridLines="None" 
                                    CellSpacing="10" HorizontalAlign="Left" CellPadding="2"
                                    BorderColor="#BBBBBB" EmptyDataText="No Record Found.">
                                    <Columns>
                                        <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ConvertEmptyStringToNull="False" >
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" CausesValidation="false" CommandArgument='<%#Eval("EMPLOYEEID") %>'
                                                    OnClick="pgvUsers_RowEditing" runat="server" Text="Edit" BackColor="White" UseSubmitBehavior="False"
                                                    CssClass="btn btn-xs btn-blue" style="padding: 2px 12px !important;" />
                                                <asp:Button ID="btnSave" CausesValidation="false" CommandArgument='<%#Eval("EMPLOYEEID") %>'
                                                    OnClick="pgvUsers_RowUpdating" Visible="false" runat="server" Text="Save" BackColor="White"
                                                    UseSubmitBehavior="False" CssClass="btn btn-xs btn-green" style="padding: 2px 9px !important;" />
                                            </ItemTemplate>
                                            <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnName" runat="server" ForeColor="Black" Text='<%# Eval("FIRSTNAME") +" "+ Eval("LASTNAME") %>'
                                                    OnClick="lnkBtnName_Click" CommandArgument='<%#Eval("EMPLOYEEID") %>'>hrl</asp:LinkButton>
                                                <br />
                                                <asp:Label ID="lblJobDetail" runat="server" Text='<%# Bind("JOBTITLE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hflLookupId" runat="server" Value='<%#Eval("UserTypeID") %>' />
                                                <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                <div class="list-select">
                                                    <div class="field" style="margin:0">
                                                        <asp:DropDownList ID="ddlUserTypeOptions" Width="105px" Visible="false" EnableViewState="true"
                                                            runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle Wrap="True" />
                                    <RowStyle BorderStyle="None" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div style="text-align: left" class="resource-menu">
                    <asp:Panel ID="pnlAddUserControl" runat="server" Visible="false">
                        <addUsers:AddUsers ID="AddUsers" runat="server" />
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdateProgress ID="updateProgressStatus" runat="server" AssociatedUpdatePanelID="updPnlResources"
        DisplayAfter="5">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
