﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Configuration.Assemblies
Imports iTextSharp.text
Imports System.IO
Imports iTextSharp.text.pdf
Imports System.Configuration
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.Collections
Imports System.Xml
Imports System.Globalization
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Threading



Public Class SaveLetterAsPdf
    Inherits PageBase

    Dim objLetterBL As LettersBL = New LettersBL()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsNothing(Request.QueryString("id")) Then
                Dim strStdLetterId As String = Request.QueryString("id")
                Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO()
                Me.getSavedLetter(strStdLetterId, savedLetterPDFBO)
                Me.saveAsPdf(savedLetterPDFBO)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Function getSavedLetter(ByRef strStdLetterId As String, ByRef SavedLetterPDFBO As SavedLetterPDFBO) As SavedLetterPDFBO

        Dim stdLetterId As Integer = 0
        If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
            Dim resultSet As DataSet = New DataSet()

            SavedLetterPDFBO = objLetterBL.getSavedLetterById(stdLetterId, resultSet)
            Return SavedLetterPDFBO
        Else
            Return Nothing
        End If
    End Function

    Public Sub saveAsPdf(ByRef savedLetterPDFBO As SavedLetterPDFBO)

        Dim createLetter As New CreateLetter()
        Dim f As String() = DateTime.Today.GetDateTimeFormats()

        Dim doc As New Document(iTextSharp.text.PageSize.A4, 56.692913386, 34.015748031, 85.039370079, 0) 'Margins are in points and 72 points equal 1 inch

        Dim filePath As String = Request.PhysicalApplicationPath + "\PDF\Letter1.pdf"
        Dim ms As New MemoryStream()
        Dim writer As PdfWriter = PdfWriter.GetInstance(doc, ms)

        Dim htmlWork As New HTMLWorker(doc)
        doc.Open()

        '===================================================================================

        Dim styles As New StyleSheet()

        Dim stylesList As New Dictionary(Of String, String)

        stylesList.Add("padding", "0")
        stylesList.Add("margin", "0")
        stylesList.Add(HtmlTags.FONTSIZE, "11pt")
        stylesList.Add(HtmlTags.FONTFAMILY, "Arial")

        styles.ApplyStyle("*", stylesList)

        styles.LoadTagStyle("*", stylesList)
        styles.LoadTagStyle(HtmlTags.DIV, stylesList)
        styles.LoadTagStyle(HtmlTags.P, stylesList)
        styles.LoadTagStyle(HtmlTags.BODY, stylesList)

        Dim letterHTML As StringBuilder = New StringBuilder(String.Empty)
        letterHTML.Append("<html><body>")
        letterHTML.Append("<div id='print_content' style='position: absolute; font-family: Arial;font-size: 11pt;'>")
        letterHTML.Append("Tenancy Ref:").Append(savedLetterPDFBO.TenancyRef).Append("<br>")
        letterHTML.Append("<br>")

        letterHTML.Append(savedLetterPDFBO.TenantName).Append("<br>")

        letterHTML.Append(savedLetterPDFBO.HouseNumber).Append(", ").Append(savedLetterPDFBO.AddressLine1).Append("<br>")

        If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.AddressLine2) Then
            letterHTML.Append(savedLetterPDFBO.AddressLine2).Append("<br>")
        End If
        If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.TownCity) Then
            letterHTML.Append(savedLetterPDFBO.TownCity).Append("<br>")
        End If

        If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.CountyValue) Then
            letterHTML.Append(savedLetterPDFBO.CountyValue).Append("<br>")
        End If

        If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.PostCode) Then
            letterHTML.Append(savedLetterPDFBO.PostCode).Append("<br>")
        End If

        letterHTML.Append("<br>")

        letterHTML.Append(savedLetterPDFBO.LetterDate.ToString("dd MMMM yyyy")).Append("<br>")
        letterHTML.Append("<br>")

        letterHTML.Append("Dear ").Append(savedLetterPDFBO.TenantName).Append("<br>")
        letterHTML.Append("<br>")

        letterHTML.Append(savedLetterPDFBO.LetterBody).Append("<br>")
        letterHTML.Append("<br>")

        letterHTML.Append(savedLetterPDFBO.SignOff).Append("<br>")
        letterHTML.Append("<br>")

        letterHTML.Append(savedLetterPDFBO.FromResource).Append("<br>")

        letterHTML.Append(savedLetterPDFBO.Team).Append("<br>")
        '<aTenantNameabel ID='lblResourceJobTitle' runat='server'></asp:Label>
        letterHTML.Append("Direct Dial: ").Append(savedLetterPDFBO.DirectDial).Append("<br>")
        letterHTML.Append("Email: ").Append(savedLetterPDFBO.DirectEmail).Append("<br>")

        letterHTML.Append("</div>")
        letterHTML.Append("</body></html>")
        '======================================================================================================

        'Populating the Letter        
        'doc.Add(New Chunk("Tenancy Ref: " + savedLetterPDFBO.TenancyRef + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine))

        'doc.Add(New Chunk(savedLetterPDFBO.HouseNumber + ", " + savedLetterPDFBO.AddressLine1 + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.AddressLine2 + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.TownCity + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.CountyValue + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.PostCode + Environment.NewLine + Environment.NewLine))

        'doc.Add(New Chunk(Environment.NewLine))

        'doc.Add(New Chunk(savedLetterPDFBO.LetterDate.ToString("dd MMMM yyyy") + Environment.NewLine))

        'doc.Add(New Chunk(Environment.NewLine))

        'doc.Add(New Chunk("Dear " + savedLetterPDFBO.TenantName))

        'doc.Add(New Chunk(Environment.NewLine))

        'doc.Add(New Chunk(Environment.NewLine))

        Dim sr As StringReader = New StringReader(letterHTML.ToString()) 'savedLetterPDFBO.LetterBody

        'Dim parsedList As List(Of iTextSharp.text.IElement) = HTMLWorker.ParseToList(sr, styles)

        htmlWork.SetStyleSheet(styles)
        htmlWork.Parse(sr)

        'For k As Integer = 0 To parsedList.Count - 1
        '    Dim element As IElement = DirectCast(parsedList(k), IElement)
        '    For Each chunk As Chunk In element.Chunks
        '        chunk.setLineHeight(11)
        '        Dim para As New Paragraph(chunk)
        '        para.SetLeading(0, 0)
        '        para.SpacingAfter = 0
        '        para.SpacingBefore = 0
        '        doc.Add(para)
        '    Next
        '    'If element.Chunks.Count = 0 Then
        '    '    doc.Add(New Chunk(Environment.NewLine))
        '    'End If
        '    'doc.Add(element)
        'Next

        'doc.Add(New Chunk(Environment.NewLine + Environment.NewLine))

        'doc.Add(New Chunk(savedLetterPDFBO.SignOff + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.FromResource + Environment.NewLine))
        'doc.Add(New Chunk(savedLetterPDFBO.Team + Environment.NewLine))
        'doc.Add(New Chunk("Direct Dial: " + savedLetterPDFBO.DirectDial + Environment.NewLine))
        'doc.Add(New Chunk("Email: " + savedLetterPDFBO.DirectEmail + Environment.NewLine))

        doc.Close()

        Response.Clear()
        Response.AppendHeader("content-disposition", "attachment; filename= Letter1.pdf")
        Response.ContentType = "application/pdf"
        Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length)
        Response.OutputStream.Flush()

        File.Delete(filePath)

        HttpContext.Current.ApplicationInstance.CompleteRequest()


    End Sub


End Class