﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master" CodeBehind="Resources.aspx.vb" Inherits="Appliances.Resources" EnableEventValidation="False" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Letters" class="group">
        <p style="background-color:Black;height: 22px;text-align:justify;font-family:Tahoma;font-weight:bold; margin: 0 0 6px;font-size: 15px;" ><font color="white">Letters</font></p>
        <asp:TextBox
        ID="txtComments"
        TextMode="MultiLine"
        Columns="60"
        Rows="8"
        runat="server" />
 
<asp:HtmlEditorExtender ID="HtmlEditorExtender1"
        TargetControlID="txtComments"
        EnableSanitization="false"
        runat="server" />
    </div>
</asp:Content>
