﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class Status
    Inherits PageBase
#Region "Attributes"
    Dim objStatusBL As StatusBL = New StatusBL()
    Dim ActionUpdate As Boolean = False
    Dim ActionFlag As Boolean = False
#End Region

#Region "Functions"

    Private Event loadEditStatusEvent()

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not Request.QueryString("Id") = "" And IsPostBack = False Then
                Dim StatusId As Integer = CType(Request.QueryString("Id"), Integer)
                'Show the Content of Status in edit mode
                'RaiseEvent trVwStatus_SelectedNodeChanged(sender, e)
                loadStatus(StatusId)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
    Public Sub loadEditStatus() Handles Me.loadEditStatusEvent

    End Sub

#Region "TreeNode Populate"
    Protected Sub trVwStatus_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwStatus.TreeNodePopulate
        Try
            If e.Node.ChildNodes.Count = 0 Then
                Select Case (e.Node.Depth)
                    Case 0
                        PopulateNodes(e.Node)

                    Case 1
                        PopulateActions(e.Node)
                End Select
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Nodes"
    Public Sub PopulateNodes(ByVal node As TreeNode)
        Dim count As Integer = 1
        Dim resultDataSet As DataSet = New DataSet()
        Try
            objStatusBL.getStatus(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                ViewState(ViewStateConstants.StatusDataTable) = resultDataSet
                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("Title")
                    newNode.Value = row("StatusId")
                    newNode.Target = "status"
                    node.ChildNodes.Add(newNode)
                Next
                node.ChildNodes.Add(New TreeNode("Add New Status", "-12"))
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Actions"
    Public Sub PopulateActions(ByVal node As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Dim statusDataSet As DataSet = New DataSet()
        Try

            Dim statusId As Integer = node.Value
            objStatusBL.getStatus(statusDataSet)
            If statusDataSet.Tables(0).Rows.Count > 0 Then
                objStatusBL.getActionsByStatusId(statusId, resultDataSet)
                If resultDataSet.Tables(0).Rows.Count > 0 Then
                    ViewState(ViewStateConstants.ActionDataTable) = resultDataSet
                    For Each row As DataRow In resultDataSet.Tables(0).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        newNode.PopulateOnDemand = True
                        newNode.Text = row("Title")
                        newNode.Value = row("ActionId")
                        newNode.Target = "action"
                        node.ChildNodes.Add(newNode)
                    Next

                End If
                Dim Addnode As TreeNode = New TreeNode()
                Addnode.Value = "-11"
                Addnode.Text = "Add New Action"
                node.ChildNodes.Add(Addnode)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Tree Load"
    Protected Sub tree_Load(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.Load

    End Sub
#End Region

#Region "Tree Disposed"
    Protected Sub tree_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.Disposed

    End Sub
#End Region

#Region "Selected Node Changed"
    Protected Sub trVwStatus_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles trVwStatus.SelectedNodeChanged
        Dim StatusId As Integer = trVwStatus.SelectedNode.Value
        'ViewState(ViewStateConstants.StatusIdforEdit) = StatusId
        SessionManager.setSessionVariable(SessionConstants.StatusIdforEdit, StatusId)
        Session.Add(SessionConstants.StatusIdforEdit, StatusId)
        Dim StatusControl As Control = CType(pnlStatusControl.FindControl("sControl"), Control)
        Dim ActionControl As Control = CType(pnlStatusControl.FindControl("aControl"), Control)
        Dim pnlActionControl As Panel = CType(ActionControl.FindControl("pnlActionControl"), Panel)
        pnlActionControl.Visible = True
        Dim txtBoxTitle As TextBox = CType(StatusControl.FindControl("txtBoxTitle"), TextBox)
        'Status Controls
        'Dim ddlInspectionType As DropDownList = CType(StatusControl.FindControl("ddlType"), DropDownList)
        Dim txtBoxStatus As Label = CType(StatusControl.FindControl("lblStatus"), Label)
        Dim ddlStatusRanking As DropDownList = CType(StatusControl.FindControl("ddlRanking"), DropDownList)
        Dim btnStatusSave As Button = CType(StatusControl.FindControl("btnSave"), Button)
        Dim btnStatusEdit As Button = CType(StatusControl.FindControl("btnEdit"), Button)
        Dim lblNewStatus As Label = CType(StatusControl.FindControl("lblNewStatus"), Label)
        Dim lblEditStatus As Label = CType(StatusControl.FindControl("lblEditStatus"), Label)
        'Action Controls
        Dim ddlActionRanking As DropDownList = CType(ActionControl.FindControl("ddlRanking"), DropDownList)
        Dim lblNewAction As Label = CType(ActionControl.FindControl("lblNewAction"), Label)
        Dim lblEditAction As Label = CType(ActionControl.FindControl("lblEditAction"), Label)
        Dim txtBoxActionTitle As TextBox = CType(ActionControl.FindControl("txtBoxTitle"), TextBox)
        Dim btnSaveAction As Button = CType(ActionControl.FindControl("btnSave"), Button)
        Dim btnEditAction As Button = CType(ActionControl.FindControl("btnEdit"), Button)

        Try

            If StatusId = -11 Then
                ' Add Action Here
                Dim lblStatusTitle As Label = CType(ActionControl.FindControl("lblStatusTitle"), Label)
                lblStatusTitle.Text = trVwStatus.SelectedNode.Parent.Text
                Dim statusIdofAction As Integer = trVwStatus.SelectedNode.Parent.Value
                Session.Add(SessionConstants.statusIdofAction, statusIdofAction)
                LoadRankingDDL(statusIdofAction, False)
                lblNewAction.Visible = True
                lblEditAction.Visible = False
                txtBoxActionTitle.ReadOnly = False
                txtBoxActionTitle.Text = String.Empty
                'ddlActionRanking.SelectedIndex = 0
                btnSaveAction.Visible = True
                btnEditAction.Visible = False
                StatusControl.Visible = False
                ActionControl.Visible = True
                ActionUpdate = True
                ActionFlag = True
            Else
                If StatusId = -12 Then
                    ' Add Status
                    btnStatusEdit.Visible = False
                    btnStatusSave.Visible = True
                    txtBoxTitle.ReadOnly = False
                    txtBoxTitle.Text = String.Empty
                    ddlStatusRanking.SelectedIndex = 0
                    txtBoxStatus.Text = "Gas Inpection"
                    lblEditStatus.Visible = False
                    lblNewStatus.Visible = True
                    StatusControl.Visible = True
                    ActionControl.Visible = False
                    ActionUpdate = False
                    ActionFlag = True
                Else
                    ' Edit Status Here
                    Dim resultDataTable As DataTable = New DataTable()
                    If trVwStatus.SelectedNode.Target = "status" Then
                        txtBoxTitle.Text = trVwStatus.SelectedNode.Text
                        Dim StatusDataSet As DataSet = New DataSet()
                        objStatusBL.getStatus(StatusDataSet)
                        resultDataTable = StatusDataSet.Tables(0)

                        'query the data set using this will return the data row
                        Dim customerQuery = (From dataRow In resultDataTable _
                            Where _
                                dataRow.Field(Of Integer)("StatusId") = StatusId _
                            Select dataRow).ToList()

                        Dim a As Int32 = 0

                        'Query the data table
                        Dim datatable As DataTable = customerQuery.CopyToDataTable()


                        'set the popup with values of tenant 
                        Dim isEditable As Boolean = CType(IIf(IsDBNull(datatable.Rows(0).Item(4)) = True, "", datatable.Rows(0).Item(4)), String)

                        'Dim InspectionTypeId As Integer = CType(IIf(IsDBNull(datatable.Rows(0).Item(3)) = True, "", datatable.Rows(0).Item(3)), String)
                        txtBoxStatus.Text = "Gas Inpection"
                        Dim Rank As Integer = CType(IIf(IsDBNull(datatable.Rows(0).Item(1)) = True, "", datatable.Rows(0).Item(1)), String)
                        ddlStatusRanking.SelectedIndex = Rank - 1
                        lblNewStatus.Visible = False
                        lblEditStatus.Visible = True
                        btnStatusSave.Visible = False
                        btnStatusEdit.Visible = True
                        If isEditable = False Then
                            txtBoxTitle.ReadOnly = True
                            btnStatusSave.Visible = False
                            btnStatusEdit.Visible = False
                        Else
                            txtBoxTitle.ReadOnly = False
                        End If
                        StatusControl.Visible = True
                        ActionControl.Visible = False
                    End If
                    If trVwStatus.SelectedNode.Target = "action" Then
                        Dim lblStatusTitle As Label = CType(ActionControl.FindControl("lblStatusTitle"), Label)
                        lblStatusTitle.Text = trVwStatus.SelectedNode.Parent.Text
                        Dim ActioId As Integer = trVwStatus.SelectedNode.Value
                        Dim StatId As Integer = trVwStatus.SelectedNode.Parent.Value
                        Session.Add(SessionConstants.StatIdforEditAction, StatId)
                        Session.Add(SessionConstants.ActioIdforEditAction, ActioId)
                        LoadRankingDDL(StatId, True)
                        ViewState(ViewStateConstants.ActionId) = ActioId
                        txtBoxActionTitle.Text = trVwStatus.SelectedNode.Text
                        Dim ActionDataSet As DataSet = New DataSet()
                        objStatusBL.getActions(ActionDataSet)
                        resultDataTable = ActionDataSet.Tables(0)

                        'query the data set using this will return the data row
                        Dim customerQuery = (From dataRow In resultDataTable _
                            Where _
                                dataRow.Field(Of Integer)("ActionId") = ActioId _
                            Select dataRow).ToList()

                        Dim a As Int32 = 0

                        'Query the data table
                        Dim datatable As DataTable = customerQuery.CopyToDataTable()
                        Dim Rank As String = CType(IIf(IsDBNull(datatable.Rows(0).Item(3)) = True, "", datatable.Rows(0).Item(3)), String)
                        'If Rank > 1 Then
                        '    ddlActionRanking.SelectedIndex = Rank - 1
                        'Else
                        For Each i As ListItem In ddlActionRanking.Items
                            If i.Text = Rank Then
                                i.Selected = True
                            End If
                        Next

                        'End If
                        Dim isEditable As Boolean = CType(IIf(IsDBNull(datatable.Rows(0).Item(4)) = True, "", datatable.Rows(0).Item(4)), String)
                        If isEditable = False Then
                            txtBoxActionTitle.ReadOnly = True
                            btnSaveAction.Visible = False
                            btnEditAction.Visible = False
                        Else
                            txtBoxActionTitle.ReadOnly = False
                            btnSaveAction.Visible = False
                            btnEditAction.Visible = True
                        End If

                        lblNewAction.Visible = False
                        lblEditAction.Visible = True
                        StatusControl.Visible = False
                        ActionControl.Visible = True
                    End If

                End If
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "TreeNode Collapsed"
    Protected Sub trVwStatus_TreeNodeCollapsed(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs)
        Try
            If ActionFlag Then
                If e.Node.ChildNodes.Count > 0 Then
                    e.Node.ChildNodes.Clear()
                End If
                If Not ActionUpdate Then
                    PopulateNodes(e.Node)
                    For Each node As TreeNode In e.Node.ChildNodes
                        If Not node.Text = "Add New Status" Then
                            PopulateActions(node)
                        End If
                    Next
                Else
                    PopulateActions(e.Node)
                End If
                e.Node.Collapse()
                e.Node.Expand()
            Else
                e.Node.CollapseAll()
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Reload TreeView"
    Public Sub reloadTreeView(ByVal node As TreeNode)
        Try
            node.Parent.Collapse()
            node.Parent.Expand()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Load Ranking DDL"
    Protected Sub LoadRankingDDL(ByVal statusId As Integer, ByVal checkEdit As Boolean)
        Dim resultDataSet As DataSet = New DataSet()
        Try
            Dim ActionControl As Control = CType(pnlStatusControl.FindControl("aControl"), Control)
            Dim ddlActionRanking As DropDownList = CType(ActionControl.FindControl("ddlRanking"), DropDownList)
            ddlActionRanking.Items.Clear()
            objStatusBL.getActionRankingByStatusId(resultDataSet, statusId)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlActionRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next
            If checkEdit = False Then
                ddlActionRanking.Items.Add(New ListItem((resultDataSet.Tables(0).Rows.Count + 1).ToString(), "-1"))
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Load Status"
    Protected Sub loadStatus(ByVal StatusId As Integer)
        Dim StatusControl As Control = CType(pnlStatusControl.FindControl("sControl"), Control)
        Dim ActionControl As Control = CType(pnlStatusControl.FindControl("aControl"), Control)
        Dim pnlActionControl As Panel = CType(ActionControl.FindControl("pnlActionControl"), Panel)
        Dim txtBoxTitle As TextBox = CType(StatusControl.FindControl("txtBoxTitle"), TextBox)
        'Status Controls
        'Dim ddlInspectionType As DropDownList = CType(StatusControl.FindControl("ddlType"), DropDownList)
        Dim txtBoxStatus As Label = CType(StatusControl.FindControl("lblStatus"), Label)
        Dim ddlStatusRanking As DropDownList = CType(StatusControl.FindControl("ddlRanking"), DropDownList)
        Dim btnStatusSave As Button = CType(StatusControl.FindControl("btnSave"), Button)
        Dim btnStatusEdit As Button = CType(StatusControl.FindControl("btnEdit"), Button)
        Dim lblNewStatus As Label = CType(StatusControl.FindControl("lblNewStatus"), Label)
        Dim lblEditStatus As Label = CType(StatusControl.FindControl("lblEditStatus"), Label)
        loadStatusRanking(ddlStatusRanking)
        Dim resultDataTable As DataTable = New DataTable()
        Dim StatusDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(StatusDataSet)
        resultDataTable = StatusDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("StatusId") = StatusId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()

        Dim isEditable As Boolean = CType(IIf(IsDBNull(datatable.Rows(0).Item(4)) = True, "", datatable.Rows(0).Item(4)), String)
        txtBoxTitle.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item(0)) = True, "", datatable.Rows(0).Item(0)), String)
        'Dim InspectionTypeId As Integer = CType(IIf(IsDBNull(datatable.Rows(0).Item(3)) = True, "", datatable.Rows(0).Item(3)), String)
        txtBoxStatus.Text = "Gas Inpection"
        Dim Rank As Integer = CType(IIf(IsDBNull(datatable.Rows(0).Item(1)) = True, "", datatable.Rows(0).Item(1)), String)
        ddlStatusRanking.SelectedIndex = Rank

        lblNewStatus.Visible = False
        lblEditStatus.Visible = True
        btnStatusSave.Visible = False
        btnStatusEdit.Visible = True
        If isEditable = False Then
            txtBoxTitle.ReadOnly = True
            btnStatusSave.Visible = False
            btnStatusEdit.Visible = False
        Else
            txtBoxTitle.ReadOnly = False
        End If
        StatusControl.Visible = True
        ActionControl.Visible = False
    End Sub
#End Region

    Public Sub loadStatusRanking(ByRef ddlRanking As DropDownList)
        Dim resultDataSet As DataSet = New DataSet()
        Dim InspectionDataSet As DataSet = New DataSet()
        Try
            ddlRanking.Items.Clear()
            objStatusBL.getStatus(resultDataSet)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

End Class