﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master" CodeBehind="EditLetter.aspx.vb" Inherits="Appliances.EditLetter" validateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="EditLetterTag" Src="~/Controls/Resources/EditLetterControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">

    function openPrintLetterWindow() {
        window.open('../../Views/Common/PrintLetter.aspx?id=' + <%=Me.stdLetterId %>, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no');
    }        
    function insertText(text) {
        document.getElementById('<%=richTxtComments.ClientID%>_ExtenderContentEditable').innerHTML += text;
    }

    function closePopup(param) {
        //window.opener.document.getElementById('ContentPlaceHolder1_tabProperty_TabPanel2_Activities_ckBoxRefreshDataSet').checked = true
        if (param == 0) {
            window.opener.fireCheckboxEventForAddApp();
            window.close();
        }
        else if(param == 1) {
            window.opener.fireLetterUploadCheckboxEvent();
        }

        else if(param == 2) {
            window.opener.fireLetterUploadCheckboxEvent();
            window.close();
        }
    }

    
</script>
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<uc1:EditLetterTag ID="EditLetterId" runat="server" />
</asp:Content>
