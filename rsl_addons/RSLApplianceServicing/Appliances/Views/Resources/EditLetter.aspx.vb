﻿Imports AjaxControlToolkit

Public Class EditLetter
    Inherits PageBase

    Public txtLetterTitle As TextBox = New TextBox()
    Public lblLetterTitle As Label = New Label()
    Public lblLetterBody As Label = New Label()
    Public richTxtComments As HtmlEditorExtender = New HtmlEditorExtender()
    Public stdLetterId As Integer

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request.QueryString("id")) Then
            stdLetterId = CType(Request.QueryString("id"), Integer)
        End If

        Me.FindControlsOfUserControl()
    End Sub

#Region "Find Contorls"
    Protected Sub FindControlsOfUserControl()
        txtLetterTitle = DirectCast(EditLetterId.FindControl("txtLetterTitle"), TextBox)       
        richTxtComments = DirectCast(EditLetterId.FindControl("richTxtComments"), HtmlEditorExtender)

    End Sub
#End Region

End Class