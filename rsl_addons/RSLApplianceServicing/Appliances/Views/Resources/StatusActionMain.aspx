﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="StatusActionMain.aspx.vb" Inherits="Appliances.StatusActionMain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="status" TagName="Status" Src="~/Controls/Resources/Status.ascx" %>
<%@ Register TagPrefix="letters" TagName="Letters" Src="~/Controls/Resources/Letters.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">

        function openUploadWindow() {
            javascript: window.open('../../Views/Common/UploadDocument.aspx?type=signature', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
            return false;
        }

        function fireSignatureNameEvent(some) {
            if (document.getElementById('<%=ckBoxFileName.ClientID%>').checked == true) {

                document.getElementById('<%=ckBoxFileName.ClientID%>').checked = false

            } else {

                document.getElementById('<%=ckBoxFileName.ClientID%>').checked = true
            }

            setTimeout('__doPostBack(\'<%=ckBoxFileName.UniqueID%>\',\'\')', 0);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Resources" class="group">
        <div class="portlet">
            <div class="header">
                <span class="header-label">Resources</span>
            </div>
            <div class="portlet-body">
                <div style="padding:10px;">
                    <asp:UpdatePanel runat="server" ID="updPnlResources" UpdateMode="Conditional" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <div id="rightpanel">
                                <asp:Panel runat="server" ID="pnlStatusAndLetters">
                                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                    </asp:Panel>
                                    <div style="width:inherit;">
                                        <div style="width:50%; float:left;">
                                            <asp:Panel ID="Panel1" runat="server" >
                                                <status:Status ID="Status" runat="server" />
                                            </asp:Panel>
                                        </div>
                                        <div style="width:50%; float:left;">
                                            <asp:Panel ID="Panel2" runat="server" >
                                                <letters:Letters ID="Letters" runat="server" />
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="updateProgressStatus" runat="server" AssociatedUpdatePanelID="updPnlResources"
        DisplayAfter="5">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
