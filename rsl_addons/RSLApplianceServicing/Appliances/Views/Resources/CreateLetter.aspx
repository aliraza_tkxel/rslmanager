﻿<%@ Page Title="" ValidateRequest="false"  Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master" CodeBehind="CreateLetter.aspx.vb" Inherits="Appliances.CreateLetter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .select_div select 
        {
            width: 185px !important;
        }
        .no-form-control-input
        {
            width:175.5px !important;
        }
    </style>
    <script type="text/javascript">
        var w;
        function insertText(text) {
            document.getElementById('<%=richTxtComments.ClientID%>_ExtenderContentEditable').innerHTML += text;
        }

        function openPreview() {
            w = window.open("PreviewLetter.aspx?src=CreateLetter");
        }
    </script>
    <div id="Resources" class="portlet">
        <div class="header">
            <span class="header-label">Create New Letter Template</span>
        </div>
        <div class="portlet-body">
            <asp:UpdatePanel runat="server" ID="pnlAddLetterTemplate" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </asp:Panel>
                    <div style="overflow:auto;">
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Status:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlStatus" class="styleselect styleselect-control" runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Letter Title:
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtLetterTitle" CssClass="no-form-control-input" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Action:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlAction"  class="styleselect styleselect-control" runat="server" AutoPostBack="true">
                                        <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Letter Code:
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtLetterCode" CssClass="no-form-control-input" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Is Alternative:
                                </div>
                                <div class="field">
                                    <asp:CheckBox style="border: 0px" Checked="false" ID="chkIsAlternative" CssClass="no-form-control-input" runat="server"></asp:CheckBox>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style="overflow:auto; padding:10px;">
                        <asp:TextBox ID="txtLetterTemplate" TextMode="MultiLine" Columns="60" Rows="24" runat="server" Height="200" />
                        <asp:HtmlEditorExtender ID="richTxtComments" TargetControlID="txtLetterTemplate"
                        EnableSanitization="false" runat="server" OnImageUploadComplete="saveFile">
                            <Toolbar>
                                <ajaxToolkit:Bold />
                                <ajaxToolkit:Italic />
                                <ajaxToolkit:Underline />
                                <ajaxToolkit:StrikeThrough />
                                <ajaxToolkit:FontNameSelector />
                                <ajaxToolkit:FontSizeSelector />

                                <ajaxToolkit:JustifyLeft />
                                <ajaxToolkit:JustifyCenter />
                                <ajaxToolkit:JustifyRight />
                                <ajaxToolkit:JustifyFull />

                                <ajaxToolkit:InsertOrderedList />
                                <ajaxToolkit:InsertUnorderedList />
                                <ajaxToolkit:Undo />
                                <ajaxToolkit:Redo />                           
                            </Toolbar>
                        </asp:HtmlEditorExtender>
                    </div>
                    <style>
                        #ContentPlaceHolder1_lblMessage
                        {
                            float: left;
                            margin-bottom: 10px;
                            width: 100%;
                        }
                        .ajax__html_editor_extender_container
                        {
                            width: 100% !important;
                        }
                        .code_box 
                        {
                            border:2px solid #000;
                            border: 1px solid #A0A0A0;
                            margin:10px;
                            width:inherit;
                        }
                        .code_box p 
                        {
                            margin-left:10px;
                        }
                        .letter_buttons 
                        {
                            float:right;
                        }
                    </style>
                    <div class="code_box">
                        <p>
                            Enter the following codes for the following:</p>
                        <p>
                            Rent Charge = <a href="javascript:void(0)" onclick="insertText('[RC]');">[RC]</a></p>
                        <p>
                            Current Rent Balance = <a href="javascript:void(0)" onclick="insertText('[RB]');">[RB]</a></p>
                        <p>
                            Today's Date = <a href="javascript:void(0)" onclick="insertText('[TD]');">[TD]</a></p>
                    </div>
                    <div class="letter_buttons">
                        <asp:Button ID="btnAddStandardLetter" CssClass="btn btn-xs btn-blue right" style="padding:3px 10px !important; margin:-3px 10px 0 0;" runat="server" Text="Save Letter Template" BackColor="White" />
                        <asp:Button ID="btnPreviewStandardLetter" CssClass="btn btn-xs btn-blue right" style="padding:3px 10px !important; margin:-3px 5px 0 0;" runat="server" Text="Preview" BackColor="White"/>
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-xs btn-blue right" style="padding:3px 10px !important; margin:-3px 5px 0 0;" Text="Cancel" PostBackUrl="~/Views/Resources/ViewLetters.aspx" BackColor="White" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgressAddLetterTemplate" runat="server" AssociatedUpdatePanelID="pnlAddLetterTemplate"
                DisplayAfter="10">
                <ProgressTemplate>
                    <div class="loading-image">
                        <img alt="Please Wait" src="../../Images/progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
</asp:Content>
