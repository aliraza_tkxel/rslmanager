﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class Access1
    Inherits PageBase
    Dim objResourcesBL As ResourcesBL = New ResourcesBL()

#Region "Events"
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim lnkBtnAccess As LinkButton = CType(Master.FindControl("lnkBtnAccess"), LinkButton)
            'lnkBtnAccess.Style.Add(ApplicationConstants.ConWebBackgroundProperty, ApplicationConstants.ConWebBackgroundColor)

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim accessRightControl As UserControl = CType(updPnlAccess.FindControl("accessRightControl"), UserControl)
            Dim lblUserName As Label = CType(accessRightControl.FindControl("lblUserName"), Label)
            Dim pnlAccessRight As Panel = CType(accessRightControl.FindControl("pnlAccessRight"), Panel)
            Dim pnlMessageAccessControl As Panel = CType(accessRightControl.FindControl("pnlMessage"), Panel)

            If QueryStringParameter.Equals(PathConstants.LoadAccessTree, True) Then
                accessRightControl.Visible = True
                lblUserName.Text = Session.Item(ViewStateConstants.AccessTreeEmployeeName)
                PathConstants.LoadAccessTree = False
            Else
                pnlAccessRight.Visible = False
                lblUserName.Visible = True
                pnlMessageAccessControl.Visible = True
            End If
            If Not IsPostBack Then
                getUsersInfo()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Edit Click"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim objAccessControl As AccessRights = New AccessRights()
        Dim accessRightControl As UserControl = CType(updPnlAccess.FindControl("accessRightControl"), UserControl)
        Dim lblUserName As Label = CType(accessRightControl.FindControl("lblUserName"), Label)
        Dim AccessTree As TreeView = CType(accessRightControl.FindControl("trVwAccess"), TreeView)

        Dim BtnEdit As Button = CType(sender, Button)
        Dim UserDataSet As DataSet = New DataSet()
        Dim UserPageDataSet As DataSet = New DataSet()
        Try
            UserDataSet = ViewState(ViewStateConstants.AccessUserDataSet)
            accessRightControl.Visible = True
            Dim EmployeeId As Integer = BtnEdit.CommandArgument
            Session.Add(SessionConstants.AccessRightEmployeeId, EmployeeId)
            Dim resultDataTable As DataTable = UserDataSet.Tables(0)
            Dim customerQuery = (From dataRow In resultDataTable _
                Where _
                    dataRow.Field(Of Integer)("EMPLOYEEID") = EmployeeId _
                Select dataRow).ToList()

            Dim a As Int32 = 0

            'Query the data table
            Dim datatable As DataTable = customerQuery.CopyToDataTable()

            Dim firstName As String = CType(IIf(IsDBNull(datatable.Rows(0).Item(0)) = True, "", datatable.Rows(0).Item(0)), String)
            Dim lastName As String = CType(IIf(IsDBNull(datatable.Rows(0).Item(5)) = True, "", datatable.Rows(0).Item(5)), String)

            Dim employeeName As String = firstName + " " + lastName
            Session.Add(ViewStateConstants.AccessTreeEmployeeName, employeeName)
            PathConstants.LoadAccessTree = True
            Response.Redirect("~/Views/Resources/Access.aspx", True)



        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "get Users"
    Protected Sub getUsersInfo()
        Dim objUserBL As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        Try

            objUserBL.getUsers(resultDataSet)
            ViewState(ViewStateConstants.AccessUserDataSet) = resultDataSet
            grdAccessUsers.DataSource = resultDataSet
            grdAccessUsers.DataBind()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Pages"
    Public Sub PopulatePages(ByVal node As TreeNode)
        Dim count As Integer = 1
        Dim resultDataSet As DataSet = New DataSet()
        Try
            objResourcesBL.getPages(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("PageName")
                    newNode.Value = row("PageId")
                    newNode.ShowCheckBox = True
                    newNode.Checked = True
                    node.ChildNodes.Add(newNode)
                Next

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Sub pages"
    Public Sub PopulateSubPages(ByVal node As TreeNode, ByVal resultDataSet As DataSet)

        Try
            'Dim ParentId As Integer = node.Value
            'objResourcesBL.getPagesbyParentId(ParentId, resultDataSet)

            If resultDataSet.Tables(0).Rows.Count > 0 Then
                ViewState(ViewStateConstants.ActionDataTable) = resultDataSet
                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("PageName")
                    newNode.Value = row("PageId")
                    newNode.ShowCheckBox = True
                    newNode.Checked = True
                    node.ChildNodes.Add(newNode)
                Next
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    'Public Sub reloadTreeView()
    '    Try
    '        Dim ctrlAccess As UserControl = CType(updPnlAccess.FindControl("accessRightControl"), UserControl)
    '        Dim trVwAccess As TreeView = CType(ctrlAccess.FindControl("trVwAccess"), TreeView)
    '        trVwAccess.Nodes(0).ChildNodes.Clear()
    '        'tree.Height = 0
    '        PopulatePages(trVwAccess.Nodes(0))
    '        PopulateSubPages(trVwAccess.Nodes(0))

    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message
    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If
    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
    '        End If
    '    End Try
    'End Sub
#Region "Refresh View"
    Sub refreshView(ByRef node As TreeNode)
        CollapseAndExpandNode(node.Parent)
    End Sub
#End Region

#Region "Collapse And Expnad Node"
    Sub CollapseAndExpandNode(ByRef node As TreeNode)
        SelectNode()
    End Sub
#End Region

#Region "Select Node"
    Sub SelectNode()
        Dim node As TreeNode
        Dim accessControl As UserControl = CType(updPnlAccess.FindControl("accessRightControl"), UserControl)
        Dim trVwAccess As TreeView = CType(accessControl.FindControl("trVwAccess"), TreeView)
        Dim objAccessRight As AccessRights = New AccessRights()
        For Each node In trVwAccess.Nodes
            PopulatePages(node)
            If node.ChildNodes.Count > 0 Then
                Dim node1 As TreeNode
                For Each node1 In node.ChildNodes
                    node1.Select()
                    If node1.ChildNodes.Count > 0 Then
                        Dim node2 As TreeNode
                        For Each node2 In node1.ChildNodes
                            If node2.Text = Session("SelectedNodeText") Then
                                node2.Select()
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End Sub
#End Region
#End Region

End Class