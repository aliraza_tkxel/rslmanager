﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ResourceMaster.Master"
    CodeBehind="Access.aspx.vb" Inherits="Appliances.Access1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="accessRights" TagName="AccessRights" Src="~/Controls/Resources/AccessRights.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        h3
        {
            border-bottom: 1px solid gray;
            margin: 2px 0px 0 0px;
            padding:10px;
        }
        
        .grdAccessUsers td
        {
            padding: 10px 0 0 10px;
        }   
        
        .grdAccessUsers td:nth-child(1)
        {
            width:63px;
        }
        
        .pnlAccessDiv
        {
            float: left; 
            width: 42%; 
            border: 1px solid; 
            height: 553px; 
            overflow: auto;
        } 
        
        .pnlAccessRight
        {
            padding:10px;
        } 
        
        .editAccessRights
        {
            border: 1px solid gray; 
            padding: 5px 0 5px 0px;
            width:97%
        }          
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="updPnlAccess" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="pnlAccessDiv">
                <h3>
                    Access</h3>
                <asp:GridView ID="grdAccessUsers" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                    BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" CssClass="grdAccessUsers">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Width="45px" CommandArgument='<%#Eval("EMPLOYEEID") %>'
                                    Text="Edit" OnClick="btnEdit_Click" BackColor="White" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FIRSTNAME") %>' Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LASTNAME") %>' Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblJobDetail" runat="server" Text='<%# Bind("JOBTITLE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="resource-menu">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAccessRights">
                    <accessRights:AccessRights ID="accessRightControl" runat="server" Visible="False" />
                </asp:Panel>
                <asp:UpdateProgress ID="updateProgressAccessRight" runat="server" AssociatedUpdatePanelID="updPnlAccess"
                    DisplayAfter="5">
                    <ProgressTemplate>
                        <div class="loading-image">
                            <img alt="Please Wait" src="../../Images/progress.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
