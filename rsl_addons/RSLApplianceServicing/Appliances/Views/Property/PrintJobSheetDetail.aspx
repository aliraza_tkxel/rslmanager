﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" 
    CodeBehind="PrintJobSheetDetail.aspx.vb" Inherits="Appliances.PrintJobSheetDetail" %>

<form id="form1" runat="server">
<asp:panel id="pnlMessage" runat="server" visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:panel>
<asp:scriptmanager runat="server"></asp:scriptmanager>
<table id="jobsheet_detail_table" style="width: 100%; min-height: 100%; font-family: Arial;">
    <tr>
        <td colspan="3">
            <asp:label id="lblRecallDetailsFor" runat="server" text="Job sheet summary for : "
                font-bold="true"></asp:label>
            <asp:label id="lblClientNameHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;<asp:label id="lblClientStreetAddressHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;,
            <asp:label id="lblClientCityHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;,
            <asp:label id="lblClientPostalcodeHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;<asp:label id="lblClientTelHeader" runat="server" text=" Tel :" font-bold="true"> </asp:label><asp:label
                id="lblClientTelPhoneNumberHeader" runat="server" text="" font-bold="true"></asp:label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:label id="lblFaultId" runat="server" font-bold="true"></asp:label>
        </td>
        <td>
        </td>
        <td>
            <asp:label id="lblClientName" runat="server" font-bold="true"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Contractor&nbsp; :
        </td>
        <td>
            <asp:label id="lblContractor" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientStreetAddress" runat="server"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Operative&nbsp; :
        </td>
        <td>
            <asp:label id="lblOperativeName" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientCity" runat="server"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Priority&nbsp; :
        </td>
        <td>
            <asp:label id="lblFaultPriority" runat="server"></asp:label>
        </td>
        <td>
             <asp:label id="lblClientRegion" runat="server" text=""></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Completion due&nbsp; :
        </td>
        <td>
            <asp:label id="lblFaultCompletionDateTime" runat="server"></asp:label>
        </td>
        <td>
        <asp:label id="lblClientPostCode" runat="server" text=""></asp:label>
           
        </td>
    </tr>
    <tr>
        <td>
            Order date&nbsp; :
        </td>
        <td>
            <asp:label id="lblOrderDate" runat="server"></asp:label>
        </td>
        <td>
            Tel :
            <asp:label id="lblClientTelPhoneNumber" runat="server" text="" font-bold="true"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Location&nbsp; :
        </td>
        <td>
            <asp:label id="lblFaultLocation" runat="server"></asp:label>
        </td>
        <td>
            Mobile :
            <asp:label id="lblClientMobileNumber" runat="server" text=""></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Description&nbsp; :
        </td>
        <td>
            <asp:label id="lblFaultDescription" runat="server" text="" font-bold="true"></asp:label>
        </td>
        <td>
            Email :
            <asp:label id="lblClientEmailId" runat="server" text=""></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Trade&nbsp; :
        </td>
        <td>
            <asp:label id="lblTrade" runat="server" text=""></asp:label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Note :
        </td>
        <td>
            <asp:textbox enabled="false" id="lblFaultOperatorNote" cssclass="roundcornerby5"
                runat="server" textmode="MultiLine" height="65px" width="248px"></asp:textbox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <b>Appointment :</b>
        </td>
        <td>
            <asp:label id="lblTime" runat="server" text=""></asp:label>
        </td>
        <td>
            <b>Asbestos :</b>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:label id="lblFaultAppointmentDateTime" runat="server" text=""></asp:label>
        </td>
        <td rowspan="4">
            <asp:gridview id="grdAsbestos" runat="server" autogeneratecolumns="False" showheader="false"
                borderstyle="None" cellspacing="5" gridlines="None">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description" />
                            </Columns>
                        </asp:gridview>
        </td>
    </tr>
    <tr>
        <td rowspan="1" colspan="2">
            <asp:textbox id="txtAppointmentNote" runat="server" enabled="false" textmode="MultiLine"
                cssclass="roundcornerby5" height="53px" width="366px"></asp:textbox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
</table>
<div style="width: 100%; text-align: left; clear: both;">
</div>
</form>