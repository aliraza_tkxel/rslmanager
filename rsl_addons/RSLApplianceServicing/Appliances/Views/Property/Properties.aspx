﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/PropertyMasterPage.Master"
    CodeBehind="Properties.aspx.vb" Culture="en-GB" Inherits="Appliances.Properties"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="ucPropertiesListTab" TagName="PropertiesListTab" Src="~/Controls/PropertyData/PropertiesList.ascx" %>
<%@ Register TagPrefix="ucPropertiesTab" TagName="PropertiesTab" Src="~/Controls/PropertyData/PropetiesDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/PropertySummary.css" rel="stylesheet" type="text/css" />
    <style>
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            //RemoveSpecialChercterOnly(field);
            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }
        function openUploadDocumentWindow() {
            wl = window.open('../../Views/Common/UploadDocument.aspx', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }

        function openComplianceUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=compliance', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function openAsbestosUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=asbestos', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }

        function fireLetterUploadCheckboxEvent() {
            document.getElementById('<%=ckBoxRefreshDataSet.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxRefreshDataSet.UniqueID%>\',\'\')', 0);
        }

        function openUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=document', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }

        function fireDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDocumentUpload.UniqueID%>\',\'\')', 0);
        }


        function fireComplianceDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxComplianceDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxComplianceDocumentUpload.UniqueID%>\',\'\')', 0);
        }

        function fireAsbestosDocUploadCkBoxEvent() {

            document.getElementById('<%=ckBoxAsbestosDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxAsbestosDocumentUpload.UniqueID%>\',\'\')', 0);
        }
        function openPhotoUploadWindow(type) {
            var url = '../../Views/Common/UploadDocument.aspx?type=photo';

            if (type == 'defect') {
                url = url + '&photoType=defect';
            }
            if (type == 'summaryPhoto') {
                url = url + '&summaryPhoto=summaryPhoto';
            }

            wd = window.open(url, 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');

        }
        function firePhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxPhotoUpload.UniqueID%>\',\'\')', 0);
        }
        function fireSummaryPhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxSummaryPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxSummaryPhotoUpload.UniqueID%>\',\'\')', 0);
        }
        function fireDefectPhotoUploadCkBoxEvent() {

            document.getElementById('<%=ckBoxDefectPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDefectPhotoUpload.UniqueID%>\',\'\')', 0);
        }

        function searchTextChanged() {
            __doPostBack('<%= btnHidden.UniqueID %>', '');
        }
        function changeView() {
            __doPostBack('<%= btnHiddenNewProp.UniqueID %>', '');
        }
        function PrintConditionList(printContent) {
            var dvTarget = document.getElementById(printContent);
            var printWindow;
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if (isChrome) {
                printWindow = window.open('chrome://test/content/scanWindow.xul', 'Scan', '**chrome,width=850,height=150,centerscreen**');
            } else {
                printWindow = window.open('', '', 'left=0,top=0,toolbar=0,sta­tus=0,scrollbars=1');
            }

            printWindow.document.write(dvTarget.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();

        }

        function ShowWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'table-row';
        }
        function HideWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'none';
        }
        function calcrent() {

            var rent, services, counciltax, water, inelgserv, supp, totalrent
            rent = parseFloat(document.getElementById("<%=txtRent.ClientID%>").value);
            services = parseFloat(document.getElementById("<%=txtServices.ClientID%>").value);
            counciltax = parseFloat(document.getElementById("<%=txtCouncilTax.ClientID%>").value);
            water = parseFloat(document.getElementById("<%=txtWater.ClientID%>").value);
            inelgserv = parseFloat(document.getElementById("<%=txtIntelig.ClientID%>").value);
            supp = parseFloat(document.getElementById("<%=txtSupp.ClientID%>").value);
            garage = parseFloat(document.getElementById("<%=txtGarage.ClientID%>").value);
            totalrent = rent + services + counciltax + water + inelgserv + supp + garage
            document.getElementById("<%=txtTotalRent.ClientID%>").value = FormatCurrency(totalrent);
        }
        //this function will only be accurate to about 10 decimal places.
        function FormatCurrency(fPrice, decimals) {
            fPrice = Math.round(fPrice * Math.pow(10, 10)) / Math.pow(10, 10);
            if (decimals) {
                if (!isNaN(decimals)) indexCount = parseInt(decimals) + 1
                else indexCount = 3
            }
            else indexCount = 3 //default to two decimal places
            plusString = "0."
            for (i = 1; i < 11; i++) {
                if (indexCount == i) plusString += "5"
                else plusString += "0"
            }
            Negate = false
            if (parseFloat(fPrice) < 0) {
                fPrice = 0 - fPrice
                Negate = true
            }
            var sCurrency = "" + (parseFloat(fPrice) + parseFloat(plusString));
            var nPos = sCurrency.indexOf('.');
            if (nPos < 0 && indexCount > 0) {
                sCurrency += '.';
            }
            sCurrency = sCurrency.slice(0, nPos + indexCount);
            var nZero = indexCount - (sCurrency.length - nPos);
            for (var i = 0; i < nZero; i++)
                sCurrency += '0';
            if (Negate)
                return "-" + sCurrency;
            else
                return sCurrency;
        }
        function NumberOnly(field) {
            var re = /^[0-9.]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }

        function RemoveSpecialChercterOnly(field) {
            var re = /^[a-zA-Z0-9,-\/\s]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^a-zA-Z0-9\s,-]/g, "");
                return false;
            }
        }
        function LocationItemSelected(sender, e) {

            __doPostBack('<%= btnLocation.UniqueID %>', e.get_value());
        }
        function typeItemSelected(sender, e) {

            __doPostBack('<%= btnType.UniqueID %>', e.get_value());
        }
        function makeItemSelected(sender, e) {

            __doPostBack('<%= btnMake.UniqueID %>', e.get_value());
        }
        function modelItemSelected(sender, e) {

            __doPostBack('<%= btnModel.UniqueID %>', e.get_value());
        }
        function heatingFuelChanged(sender) {

            console.log(sender);
           var heatingType =sender.options[sender.selectedIndex].value;
           __doPostBack('<%= btnHeatingFuel.UniqueID %>', heatingType);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelProperties" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnHidden" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnHiddenNewProp" runat="server" Text="" UseSubmitBehavior="false"
                Style="display: none;" />
            <asp:Button ID="btnLocation" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnType" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnMake" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnModel" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" Font-Size="12px" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <!-- wrapper -->
            <div class="portlet">
                <div class="header">
                    <asp:Label Text="Properties" CssClass="header-label" Font-Bold="true" Font-Size="15px"
                        runat="server" ID="lblTitle" />
                    <div class="form-control right">
                        <div class="field">
                            <asp:TextBox ID="txtSearch" Style="padding: 4px 23px !important; margin: -3px 10px 0 0;"
                                class="styleselect styleselect-control" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                                onkeyup="TypingInterval();">
                            </asp:TextBox>
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                            </asp:TextBoxWatermarkExtender>
                        </div>
                    </div>
                    <div class="field right">
                        <asp:Button ID="btnAddProperty" class="btn btn-xs btn-blue right" UseSubmitBehavior="false"
                            OnClientClick="return changeView()" Style="padding: 3px 23px !important; margin: -3px 0 0 0;"
                            runat="server" Text="Add New property" />
                    </div>
                </div>
                <div class="portlet-body" style="padding-bottom: 0;">
                    <asp:Panel runat="server" ID="pnlPropertiesList">
                        <div style="float: Left; width: 100%;">
                            <asp:MultiView ActiveViewIndex="0" runat="server" ID="MainView">
                                <asp:View runat="server" ID="View1">
                                    <ucPropertiesListTab:PropertiesListTab ID="propertiesListTab" runat="server" />
                                </asp:View>
                                <asp:View runat="server" ID="View2">
                                    <ucPropertiesTab:PropertiesTab ID="propertiesDetails" runat="server" />
                                </asp:View>
                            </asp:MultiView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
