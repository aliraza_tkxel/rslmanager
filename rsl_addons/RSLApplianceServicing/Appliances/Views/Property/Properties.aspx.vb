﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Properties
    Inherits PageBase
#Region "Datamember/Properties (class level variables)"

    Public ddlLetter As DropDownList = New DropDownList()
    Public ckBoxRefreshDataSet As New CheckBox()
    Public ckBoxDocumentUpload As New CheckBox()
    Public ckBoxPhotoUpload As New CheckBox()
    Public ckBoxComplianceDocumentUpload As New CheckBox()
    Public ckBoxAsbestosDocumentUpload As New CheckBox()
    Public ckBoxDefectPhotoUpload As New CheckBox()
    Public ckBoxSummaryPhotoUpload As New CheckBox()
    Public txtRent As New TextBox()
    Public txtServices As New TextBox()
    Public txtCouncilTax As New TextBox()
    Public txtWater As New TextBox()
    Public txtIntelig As New TextBox()
    Public txtSupp As New TextBox()
    Public txtGarage As New TextBox()
    Public txtTotalRent As New TextBox()
    Public btnHeatingFuel As New Button()
#End Region
#Region "Events"
#Region "Page Load Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'Comment ViewPropertyImageLink.NavigateUrl when using on local machine in AddProperties.ascx.vb populateAllControls()
            ' Addition of new menu items and associated reports - Adnan 16/10/13
            If Not IsPostBack Then
                Dim searchText As String = String.Empty
                populatePropertiesList(searchText)
                SessionManager.removePropertyId()
                getQueryStringValues()
                If SessionManager.getPropertyId() <> String.Empty Then
                    MainView.ActiveViewIndex = 1
                    findControlsOfUserControl()
                    propertiesDetails.populatePropertyDetail()
                End If
                If Request.QueryString(PathConstants.Src) = PathConstants.AddProperty Then
                    MainView.ActiveViewIndex = 1

                End If
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "btnHidden click event"
    ''' <summary>
    ''' btnHidden click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Try
            'Dim txtSearch As TextBox = CType(propertiesListTab.FindControl("txtSearch"), TextBox)
            If txtSearch.Text.Trim <> String.Empty Then
                Dim regex = New Regex("[^a-zA-Z0-9 -]")
                If regex.IsMatch(txtSearch.Text) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidCheracter, True)
                Else
                    populatePropertiesList(txtSearch.Text.Trim())

                End If

            Else
                Dim searchText As String = String.Empty
                populatePropertiesList(searchText)
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region


#Region "btnHiddenNewProp click event"
    ''' <summary>
    ''' btnHiddenNewProp click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHiddenNewProp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHiddenNewProp.Click
        Try
            lblTitle.Text = "Add New Property"
            MainView.ActiveViewIndex = 1
            SessionManager.removePropertyId()
            btnAddProperty.Visible = False
            txtSearch.Visible = False
            propertiesDetails.populatePropertyDetail()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "btnLocation click event"
    ''' <summary>
    ''' btnLocation click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLocation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLocation.Click
        Dim locationId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(propertiesDetails.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedLocationId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedLocationId"), HiddenField)
        hdnSelectedLocationId.Value = locationId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnType click event"
    ''' <summary>
    ''' btnType click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnType_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnType.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(propertiesDetails.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedTypeId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedTypeId"), HiddenField)
        hdnSelectedTypeId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnMake click event"
    ''' <summary>
    ''' btnMake click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnMake_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMake.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(propertiesDetails.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedmakeId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedmakeId"), HiddenField)
        hdnSelectedmakeId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnModel click event"
    ''' <summary>
    ''' btnMake click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnModel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnModel.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(propertiesDetails.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedModelId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedModelId"), HiddenField)
        hdnSelectedModelId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region
#End Region
#Region "Functions"
#Region "Populate Properties List"
    ''' <summary>
    ''' Populate Properties List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertiesList(ByVal searchText As String)
        propertiesListTab.populatePropertiesList(searchText, True)

    End Sub

#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        Dim objPropertyBL As PropertyBL = New PropertyBL
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
            SessionManager.setPropertyId(propertyId)
            btnAddProperty.Visible = False
            txtSearch.Visible = False
            lblTitle.Text = String.Format("Property > {0}", objPropertyBL.getPropertyAddress(propertyId))
            'Else
            '    uiMessageHelper.IsError = True
            '    uiMessageHelper.message = UserMessageConstants.InvalidPropertyIdQueryString
        End If
    End Sub

#End Region

#Region "Find Controls"
    Protected Sub findControlsOfUserControl()


        Dim itemDetailControl As UserControl = New UserControl()
        Dim ucAttributes As UserControl = DirectCast(propertiesDetails.FindControl("Attributes"), UserControl)
        itemDetailControl = DirectCast(ucAttributes.FindControl("itemDetailControl"), UserControl)
        btnHeatingFuel = DirectCast(ucAttributes.FindControl("btnHeatingFuel"), Button)

        Dim ucDocument As UserControl = DirectCast(propertiesDetails.FindControl("Document"), UserControl)
        ckBoxComplianceDocumentUpload = DirectCast(ucDocument.FindControl("ckBoxComplianceDocumentUpload"), CheckBox)

        Dim ucHealthSafety As UserControl = DirectCast(propertiesDetails.FindControl("HealthSafety"), UserControl)
        Dim ucAsbestos As UserControl = DirectCast(ucHealthSafety.FindControl("Asbestos"), UserControl)
        ckBoxAsbestosDocumentUpload = DirectCast(ucAsbestos.FindControl("ckBoxAsbestosDocumentUpload"), CheckBox)

        Dim photographControl As UserControl = New UserControl()
        photographControl = itemDetailControl.FindControl("photosTab")

        ckBoxPhotoUpload = DirectCast(photographControl.FindControl("ckBoxPhotoUpload"), CheckBox)

        Dim defectTab As UserControl = New UserControl()
        defectTab = itemDetailControl.FindControl("defectTab")
        Dim ckBoxDefectPhotoUploadc As CheckBox = DirectCast(defectTab.FindControl("ckBoxPhotoUpload"), CheckBox)
        ckBoxDefectPhotoUpload = ckBoxDefectPhotoUploadc

        Dim RentTab As UserControl = New UserControl()
        Dim FinancialTab As UserControl = New UserControl()
        Dim currentRentTab As UserControl = New UserControl()
        FinancialTab = propertiesDetails.FindControl("Financial")
        RentTab = FinancialTab.FindControl("RentTab")
        currentRentTab = RentTab.FindControl("CurrentRentTab")
        txtRent = DirectCast(currentRentTab.FindControl("txtRent"), TextBox)
        txtServices = DirectCast(currentRentTab.FindControl("txtServices"), TextBox)
        txtCouncilTax = DirectCast(currentRentTab.FindControl("txtCouncilTax"), TextBox)
        txtWater = DirectCast(currentRentTab.FindControl("txtWater"), TextBox)
        txtIntelig = DirectCast(currentRentTab.FindControl("txtIntelig"), TextBox)
        txtSupp = DirectCast(currentRentTab.FindControl("txtSupp"), TextBox)
        txtGarage = DirectCast(currentRentTab.FindControl("txtGarage"), TextBox)
        txtTotalRent = DirectCast(currentRentTab.FindControl("txtTotalRent"), TextBox)
    End Sub
#End Region

#Region "Search Location"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceLocations(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getApplianceLocations(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Type"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceType(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getApplianceType(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Manufacturer"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceManufacturer(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getMake(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Model"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceModel(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getModelsList(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region
#End Region

End Class