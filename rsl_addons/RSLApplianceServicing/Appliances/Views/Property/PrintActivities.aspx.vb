﻿Public Class PrintActivities1
    Inherits PageBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim propertyId As String = Request.QueryString("Id")
        printAct.loadPrintActivities(propertyId)
        printAct.loadPropertyDetails(propertyId)
    End Sub

End Class