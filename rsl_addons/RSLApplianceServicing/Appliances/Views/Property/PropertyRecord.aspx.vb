﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class PropertyRecord
    Inherits PageBase

#Region "Datamember/Properties (class level variables)"

    Public ddlLetter As DropDownList = New DropDownList()
    Public ckBoxRefreshDataSet As New CheckBox()
    Public ckBoxDocumentUpload As New CheckBox()
    Public ckBoxComplianceDocumentUpload As New CheckBox()
    Public ckBoxPhotoUpload As New CheckBox()
    Public ckBoxDefectPhotoUpload As New CheckBox()
    Public ckBoxSummaryPhotoUpload As New CheckBox()
    Public ckBoxSummaryEPCPhotoUpload As New CheckBox()
    Public txtRent As New TextBox()
    Public txtServices As New TextBox()
    Public txtCouncilTax As New TextBox()
    Public txtWater As New TextBox()
    Public txtIntelig As New TextBox()
    Public txtSupp As New TextBox()
    Public txtGarage As New TextBox()
    Public txtTotalRent As New TextBox()

#End Region

#Region "Events"

#Region "Page Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.FindControlsOfUserControl()
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            AddHandler ucAssignToContractor.CloseButtonClicked, AddressOf btnClose_Click

            'If Not IsPostBack Then
            showPropertyAccessTabs()
            'End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                SessionManager.removePropertyId()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add More Faults"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs)
        mdlpopupRaisePO.Hide()
        updPanelPropertyRecord.Update()
    End Sub
#End Region

#End Region

#Region "Menu (Link Button Events)"

#Region "lnk Btn Summary Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' 3- Call the user control load function to load data from datasource.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnSummaryTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.Summary

        Summary.populateFullSummary()
    End Sub
#End Region

#Region "link Button Activities Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnActivitiesTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.Activities

        Activities.loadPropertyActivities()
    End Sub
#End Region

#Region "link Button Attributes Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAttributesTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        Attributes.populateAttributeControl()
        MainView.ActiveViewIndex = MenuIndex.Attributes
    End Sub
#End Region

#Region "link Button Accommodation Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' 3- Call the user control load function to load data from datasource.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAccommodation_Click(ByVal sender As Object, ByVal e As EventArgs)

        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.Accommodation
        Accommodation.populateRentTypeAndFinancialInformation()
        Accommodation.populateAccomudationDetails()
    End Sub
#End Region

#Region "link Button Doc Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDocTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnFinancial.Click, lnkBtnDocTab.Click
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.ComplianceDocuments
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        Document.bindToGrid(objPropertyDocumentBO)
    End Sub
#End Region

#Region "link Button Financial Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnFinancial_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnFinancial.Click
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.Financial
    End Sub
#End Region

#Region "link Button Health & Safety Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnHealthSafety_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnHealthSafety.Click
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.HealthAndSafety
    End Sub
#End Region

#Region "link Button Status History Tab Click"
    Protected Sub lnkBtnStatusHistory_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnStatusHistory.Click
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.StatusHistory
    End Sub
#End Region

#Region "link Button Restriction Tab Click"
    Protected Sub lnkBtnRestriction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnRestriction.Click
        setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
        MainView.ActiveViewIndex = MenuIndex.Restriction
        Restriction.loadDataReadOnlyView()
    End Sub
#End Region

#End Region

#Region "btnLocation click event"
    ''' <summary>
    ''' btnLocation click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnLocation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLocation.Click
        Dim locationId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(MainView.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedLocationId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedLocationId"), HiddenField)
        hdnSelectedLocationId.Value = locationId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnType click event"
    ''' <summary>
    ''' btnType click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnType_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnType.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(MainView.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedTypeId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedTypeId"), HiddenField)
        hdnSelectedTypeId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnMake click event"
    ''' <summary>
    ''' btnMake click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnMake_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMake.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(MainView.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedmakeId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedmakeId"), HiddenField)
        hdnSelectedmakeId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "btnModel click event"
    ''' <summary>
    ''' btnMake click event used on add appliance location field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnModel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnModel.Click
        Dim typeId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(MainView.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim applianceTabId As UserControl = CType(detailMainView.FindControl("applianceTabId"), UserControl)
        Dim hdnSelectedModelId As HiddenField = CType(applianceTabId.FindControl("hdnSelectedModelId"), HiddenField)
        hdnSelectedModelId.Value = typeId
        Dim mdlPopUpAddAppliance As AjaxControlToolkit.ModalPopupExtender = CType(applianceTabId.FindControl("mdlPopUpAddAppliance"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopUpAddAppliance.Show()
    End Sub
#End Region

#Region "Return tabs/usercontrols which user has access"

    Private Sub showPropertyAccessTabs()
        Dim objDashboardBL As DashboardBL = New DashboardBL
        Dim userId As Integer = SessionManager.getUserEmployeeId()
        Dim resultDataset As DataSet = New DataSet
        Dim firstPage As Integer = 0
        Dim menuName As String = ApplicationConstants.PropertyModuleMenu
        objDashboardBL.getPropertyPageList(resultDataset, userId, menuName)
        getQueryStringValues()
        For Each row As DataRow In resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt).Rows
            Dim tabName As String = row.Item(ApplicationConstants.GrantPagePageNameCol)
            Dim controlName = getPropertyAccessTabs(tabName)
            If (controlName = "") Then
                Continue For
            End If
            Dim lnkBtn As LinkButton = DirectCast(updPanelPropertyRecord.FindControl(controlName), LinkButton)
            lnkBtn.Visible = True
            lnkBtn.Enabled = True

            If (firstPage = 0) Then
                lnkBtn.CssClass = ApplicationConstants.TabClickedCssClass
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "lnkBtnsClick", String.Format("lnkBtnsClick('{0}');", lnkBtn.ClientID), True)
                firstPage = firstPage + 1
            End If

        Next
    End Sub

    Private Shared Function getPropertyAccessTabs(ByVal tabName As String) As String
        Dim controlTabsDict As Dictionary(Of String, String) = New Dictionary(Of String, String)
        controlTabsDict.Add(ApplicationConstants.Summary, ApplicationConstants.lnkBtnSummaryTab)
        controlTabsDict.Add(ApplicationConstants.Activities, ApplicationConstants.lnkBtnActivitiesTab)
        controlTabsDict.Add(ApplicationConstants.Attributes, ApplicationConstants.lnkBtnAttributesTab)
        controlTabsDict.Add(ApplicationConstants.Accommodation, ApplicationConstants.lnkBtnAccommodation)
        controlTabsDict.Add(ApplicationConstants.Documents, ApplicationConstants.lnkBtnDocTab)
        controlTabsDict.Add(ApplicationConstants.Financial, ApplicationConstants.lnkBtnFinancial)
        controlTabsDict.Add(ApplicationConstants.HealthSafety, ApplicationConstants.lnkBtnHealthSafety)
        controlTabsDict.Add(ApplicationConstants.StatusHistory, ApplicationConstants.lnkBtnStatusHistory)
        controlTabsDict.Add(ApplicationConstants.Restriction, ApplicationConstants.lnkBtnRestriction)

        Dim controlName As String = String.Empty
        If (controlTabsDict.ContainsKey(tabName)) Then
            controlName = controlTabsDict.Item(tabName)
        End If
        Return controlName
    End Function
#End Region

#Region "btnHidden click event"
    ''' <summary>
    ''' btnHidden click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Dim itemId As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT"))
        Dim attributesControl As UserControl = CType(MainView.FindControl("Attributes"), UserControl)
        Dim itemDetailControl As UserControl = CType(attributesControl.FindControl("itemDetailControl"), UserControl)
        Dim detailMainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim trVwAttributes As TreeView = CType(attributesControl.FindControl("trVwAttributes"), TreeView)
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim itemDetailDataSet As DataSet = New DataSet()
        objPropertyBL.getItemsByItemId(itemId, itemDetailDataSet)
        If itemDetailDataSet.Tables(0).Rows.Count > 0 Then
            SessionManager.setItemDetailDataSet(itemDetailDataSet)
            SessionManager.setTreeItemName(itemDetailDataSet.Tables(0).Rows(0).Item("ItemName"))
        End If
        ItemDetail.activeViewIndex = 3


        detailMainView.ActiveViewIndex = 1
        SessionManager.setTreeItemId(itemId)
        Dim parameterDataSet As DataSet = New DataSet()
        objPropertyBL.getItemDetail(itemId, SessionManager.getPropertyId(), parameterDataSet)
        SessionManager.removeAttributeDetailDataSet()
        SessionManager.setAttributeDetailDataSet(parameterDataSet)
        SessionManager.removepreviousValueId()
        SessionManager.removeValueId()
        Dim btnAmend As Button = CType(itemDetailControl.FindControl("btnAmend"), Button)
        Dim detailsControl As UserControl = CType(detailMainView.FindControl("detailsTabID"), UserControl)
        Dim pnlOtherDetailsTab As Panel = CType(detailsControl.FindControl("pnlOtherDetailsTab"), Panel)
        Dim tblOtherDetails As Table = New Table()
        tblOtherDetails.ID = "tblOtherDetails"


        If detailMainView.ActiveViewIndex = 1 Then
            btnAmend.Visible = True
        Else
            btnAmend.Visible = False
        End If

        If parameterDataSet.Tables(0).Rows.Count > 0 Then
            tblOtherDetails = ControlFactoryBL.createDynaicControls(parameterDataSet)
        End If
        pnlOtherDetailsTab.Controls.Clear()

        pnlOtherDetailsTab.Controls.Add(tblOtherDetails)
    End Sub
#End Region

#Region "Back Button"
    Protected Sub btnBack_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(PathConstants.PropertyModuleSearch)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                SessionManager.removePropertyId()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Find Controls"
    Protected Sub FindControlsOfUserControl()
        ddlLetter = DirectCast(Activities.FindControl("ddlLetter"), DropDownList)
        ckBoxRefreshDataSet = DirectCast(Activities.FindControl("ckBoxRefreshDataSet"), CheckBox)
        ckBoxDocumentUpload = DirectCast(Activities.FindControl("ckBoxDocumentUpload"), CheckBox)
        ckBoxSummaryPhotoUpload = DirectCast(Summary.FindControl("ckBoxPhotoUpload"), CheckBox)
        ckBoxSummaryEPCPhotoUpload = DirectCast(Summary.FindControl("ckBoxEPCPhotoUpload"), CheckBox)
        ckBoxComplianceDocumentUpload = DirectCast(Document.FindControl("ckBoxComplianceDocumentUpload"), CheckBox)
        Dim itemDetailControl As UserControl = New UserControl()
        itemDetailControl = DirectCast(Attributes.FindControl("itemDetailControl"), UserControl)

        Dim photographControl As UserControl = New UserControl()
        photographControl = itemDetailControl.FindControl("photosTab")

        ckBoxPhotoUpload = DirectCast(photographControl.FindControl("ckBoxPhotoUpload"), CheckBox)

        Dim defectTab As UserControl = New UserControl()
        defectTab = itemDetailControl.FindControl("defectTab")
        Dim ckBoxDefectPhotoUploadc As CheckBox = DirectCast(defectTab.FindControl("ckBoxPhotoUpload"), CheckBox)
        ckBoxDefectPhotoUpload = ckBoxDefectPhotoUploadc
        Dim RentTab As UserControl = New UserControl()
        Dim currentRentTab As UserControl = New UserControl()
        RentTab = Financial.FindControl("RentTab")
        currentRentTab = RentTab.FindControl("CurrentRentTab")
        txtRent = DirectCast(currentRentTab.FindControl("txtRent"), TextBox)
        txtServices = DirectCast(currentRentTab.FindControl("txtServices"), TextBox)
        txtCouncilTax = DirectCast(currentRentTab.FindControl("txtCouncilTax"), TextBox)
        txtWater = DirectCast(currentRentTab.FindControl("txtWater"), TextBox)
        txtIntelig = DirectCast(currentRentTab.FindControl("txtIntelig"), TextBox)
        txtSupp = DirectCast(currentRentTab.FindControl("txtSupp"), TextBox)
        txtGarage = DirectCast(currentRentTab.FindControl("txtGarage"), TextBox)
        txtTotalRent = DirectCast(currentRentTab.FindControl("txtTotalRent"), TextBox)

        Dim addDocTitle As Label = DirectCast(Document.FindControl("addDocTitle"), Label)
        Dim searchDocTitle As Label = DirectCast(Document.FindControl("searchDocTitle"), Label)
        Dim btnFileUpload As Button = DirectCast(Document.FindControl("btnFileUpload"), Button)
        Dim btnSavePropertyDocuments As Button = DirectCast(Document.FindControl("btnSavePropertyDocuments"), Button)
        Dim btnSearchPropertyDocuments As Button = DirectCast(Document.FindControl("btnSearchPropertyDocuments"), Button)
        Dim div_TitleSelector As System.Web.UI.HtmlControls.HtmlGenericControl = DirectCast(Document.FindControl("div_TitleSelector"), System.Web.UI.HtmlControls.HtmlGenericControl)
        Dim div_FileUpload As System.Web.UI.HtmlControls.HtmlGenericControl = DirectCast(Document.FindControl("div_FileUpload"), System.Web.UI.HtmlControls.HtmlGenericControl)

        addDocTitle.Visible = False
        searchDocTitle.Visible = True
        btnSavePropertyDocuments.Visible = False
        btnSearchPropertyDocuments.Visible = True
        div_FileUpload.Style.Add("display", "none")
        div_TitleSelector.Style.Add("display", "none")


        Dim ucAsbestos As UserControl = DirectCast(HealthSafety.FindControl("Asbestos"), UserControl)
        Dim btnSaveAsbestosDocuments As Button = DirectCast(Document.FindControl("btnSavePropertyDocuments"), Button)
        btnSaveAsbestosDocuments.Enabled = False
    End Sub
#End Region

#Region "set Property Id"

    Protected Sub setPropertyId(ByRef propertyId As String)
        ViewState(ViewStateConstants.PropertyId) = propertyId
    End Sub
#End Region

#Region "get Property Id"
    Public Function getPropertyId() As String
        If (IsNothing(ViewState(ViewStateConstants.PropertyId)) = False) Then
            Return ViewState(ViewStateConstants.PropertyId).ToString()
        Else
            Return Nothing
        End If

    End Function
#End Region

#Region "Redirect to search screen"

    Private Sub redirectToSearch()
        Try
            Response.Redirect(PathConstants.PropertyModuleSearch)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                SessionManager.removePropertyId()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        Dim objPropertyBL As PropertyBL = New PropertyBL
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
            Dim propertyAddress As String = objPropertyBL.getPropertyAddress(propertyId)

            If (String.IsNullOrEmpty(propertyAddress)) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.InvalidPropertyId
            Else
                lblPropertyAddres.Text = propertyAddress
                SessionManager.setPropertyId(propertyId)
            End If

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.InvalidPropertyIdQueryString
        End If
    End Sub

#End Region

#Region "Search Location"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceLocations(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getApplianceLocations(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Type"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceType(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getApplianceType(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Manufacturer"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceManufacturer(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getMake(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Search Appliance Model"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getApplianceModel(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        ' Dim objReportsBl As ReportsBL = New ReportsBL()
        objPropertyBl.getModelsList(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row(ApplicationConstants.DefaultDropDownDataTextField).ToString, row(ApplicationConstants.DefaultDropDownDataValueField).ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#End Region

#Region "Functions"

#Region "Set Selected Menu Item (Link Button) Style"

    ''' <summary>
    ''' Set the style of the selected link button(Horizontal Tab Menu):
    ''' Set all item to the initial styles and selected item differently.    ''' 
    ''' </summary>
    ''' <param name="lnkBtnSelected">Selected menu item.</param>
    ''' <remarks></remarks>
    Private Sub setSelectedMenuItemStyle(ByRef lnkBtnSelected As LinkButton)
        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnActivitiesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAccommodation.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnFinancial.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnHealthSafety.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnStatusHistory.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnRestriction.CssClass = ApplicationConstants.TabInitialCssClass

        lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass
    End Sub

#End Region

#End Region

#Region "Enum for Menu Index(es)"

    ''' <summary>
    ''' This Enum is choose specific menu index, based on the position of menu.
    ''' In case a menu is added, add it in specific position in aspx file and also in this Enum.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum MenuIndex
        Summary
        Activities
        Attributes
        Accommodation
        ComplianceDocuments
        Financial
        HealthAndSafety
        StatusHistory
        Restriction
    End Enum

#End Region


#Region "Property Raise PO"

    Protected Sub btnRaisePO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim propertyId As String = Nothing

            If (Request.QueryString(ApplicationConstants.Id) IsNot Nothing) Then
                propertyId = Request.QueryString(ApplicationConstants.Id)
            End If

            SessionManager.PropertyId = propertyId
            SessionManager.PropertyName = lblPropertyAddres.Text
            Dim objAssignToContractorBo As PropertyAssignToContractorBo = New PropertyAssignToContractorBo()
            objAssignToContractorBo.PropertyId = propertyId
            objAssignToContractorBo.MsatType = "Property PO"
            SessionManager.AssignToContractorBo = objAssignToContractorBo
            ucAssignToContractor.populateControl()
            mdlpopupRaisePO.Show()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try



    End Sub

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            mdlpopupRaisePO.Hide()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

End Class