﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/PropertyModuleMaster.Master"
    CodeBehind="PropertyRecord.aspx.vb" Inherits="Appliances.PropertyRecord" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="prop" TagName="Summary" Src="~/Controls/Property/SummaryControl.ascx" %>
<%@ Register TagPrefix="prop" TagName="Activities" Src="~/Controls/Property/Activities.ascx" %>
<%@ Register TagPrefix="prop" TagName="Attributes" Src="~/Controls/Property/Attributes.ascx" %>
<%@ Register TagPrefix="prop" TagName="Document" Src="~/Controls/Property/DocumentTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="Financial" Src="~/Controls/Property/FinancialTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="HealthSafety" Src="~/Controls/Property/HealthSafetyTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="Accommodation" Src="~/Controls/Property/Accommodation.ascx" %>
<%@ Register TagPrefix="prop" TagName="StatusHistory" Src="~/Controls/Property/StatusHistory.ascx" %>
<%@ Register TagPrefix="prop" TagName="Restriction" Src="~/Controls/Property/Restriction.ascx" %>
<%@ Register TagPrefix="ucAssignToContractor" TagName="AssignToContractor" Src="~/Controls/Property/PurchaseOrderForm.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/PropertyModule.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/PropertySummary.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .pnlPropertyAddress
        {
            float: left;
        }
        
        .pm-Financialbox
        {
            border: 1px solid #000;
            float: left;
            margin-top: 5px;
            margin-left: 2px;
            padding: 10px;
            width: 98%;
        }
        
           .modalBack
    {
        background-color: Black;
        filter: alpha(opacity=30);
        opacity: 0.3;
        border: 1px solid black;
    }
    </style>
    <script type="text/javascript">

        function openModal() {
            $('#poModal').modal('show');
        }

        var wl, wd;
        function uploadError(sender, args) {
            alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
        }

        function uploadComplete() {
            __doPostBack();
        }

        function lnkBtnsClick(lnkBtnId) {
            document.getElementById(lnkBtnId).click();
        }

        function openJobSheetSummary(JournalHistoryId) {
            window.open('../Common/JobSheetSummary.aspx?JournalHistoryId=' + JournalHistoryId, '_blank', 'directories=no,height=800,width=1400,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
            //w.onunload = fireCheckboxEvent;

        }
        function PrintJobSheet_AC() {
            var str_win
            str_win = '/PlannedMaintenance/Views/Reports/PrintJobSheetSummary.aspx?JobSheetNumber=' + document.getElementById("lblJSN").innerHTML;
            window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
            return false;
        }
        function openAsbestosUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=asbestos', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function openEditLetterWindow() {

            var letterId = document.getElementById("<%=ddlLetter.ClientID%>").value;
            if (letterId == -1) {
            }
            else {
                wl = window.open('../../Views/Resources/EditLetter.aspx?id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
                //w.onunload = fireCheckboxEvent;
            }
        }

        function openUploadDocumentWindow() {
            wl = window.open('../../Views/Common/UploadDocument.aspx', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }

        function fireLetterUploadCheckboxEvent() {
            document.getElementById('<%=ckBoxRefreshDataSet.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxRefreshDataSet.UniqueID%>\',\'\')', 0);
        }

        function openUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=document', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function openComplianceUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=compliance', 'new', 'directories=no,height=170,width=330,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function fireDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDocumentUpload.UniqueID%>\',\'\')', 0);
        }
        function fireComplianceDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxComplianceDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxComplianceDocumentUpload.UniqueID%>\',\'\')', 0);
        }
        function openPhotoUploadWindow(type) {
            var url = '../../Views/Common/UploadDocument.aspx?type=photo';

            if (type == 'defect') {
                url = url + '&photoType=defect';
            }
            if (type == 'summaryPhoto') {
                url = url + '&summaryPhoto=summaryPhoto';
            }
            if (type == 'summaryEPCPhoto') {
                url = url + '&summaryEPCPhoto=summaryEPCPhoto';
            }

            wd = window.open(url, 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');

        }
        function firePhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxPhotoUpload.UniqueID%>\',\'\')', 0);
        }
        function fireSummaryPhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxSummaryPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxSummaryPhotoUpload.UniqueID%>\',\'\')', 0);
        }
        function fireSummaryEPCPhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxSummaryEPCPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxSummaryEPCPhotoUpload.UniqueID%>\',\'\')', 0);
        }
        function fireDefectPhotoUploadCkBoxEvent() {

            document.getElementById('<%=ckBoxDefectPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDefectPhotoUpload.UniqueID%>\',\'\')', 0);
        }

        function PrintJobSheet() {

            javascript: window.open('PrintJobSheetDetail.aspx?JobSheetNumber=' + document.getElementById("lblFaultId").innerHTML);
            return false;
        }

        function PrintConditionList(printContent) {
            var dvTarget = document.getElementById(printContent);
            var printWindow;
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if (isChrome) {
                printWindow = window.open('chrome://test/content/scanWindow.xul', 'Scan', '**chrome,width=850,height=150,centerscreen**');
            } else {
                printWindow = window.open('', '', 'left=0,top=0,toolbar=0,sta­tus=0,scrollbars=1');
            }

            printWindow.document.write(dvTarget.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();

        }

        function ShowWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'table-row';
        }
        function HideWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'none';
        }
        function pageLoad() {
            jQuery("#loadingPropertyImages").show();
            jQuery("#loadingEPCImage").show();
            jQuery("#propertyImageContainer").hide();

            setTimeout(function () {
                jQuery("#loadingPropertyImages").hide();
                jQuery("#propertyImageContainer").show();
                jQuery("#loadingEPCImage").hide();
            }, 10000);
        }


        function setItemIdInSession(itemId) {
            //alert(itemId);
            __doPostBack('<%= btnHidden.UniqueID %>', itemId);

        }

        function NumberOnly(field) {
            var re = /^[0-9.]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }

        function LocationItemSelected(sender, e) {

            __doPostBack('<%= btnLocation.UniqueID %>', e.get_value());
        }
        function typeItemSelected(sender, e) {

            __doPostBack('<%= btnType.UniqueID %>', e.get_value());
        }
        function makeItemSelected(sender, e) {

            __doPostBack('<%= btnMake.UniqueID %>', e.get_value());
        }
        function modelItemSelected(sender, e) {

            __doPostBack('<%= btnModel.UniqueID %>', e.get_value());
        }
        function calcrent() {

            var rent, services, counciltax, water, inelgserv, supp, totalrent
            rent = parseFloat(document.getElementById("<%=txtRent.ClientID%>").value);
            services = parseFloat(document.getElementById("<%=txtServices.ClientID%>").value);
            counciltax = parseFloat(document.getElementById("<%=txtCouncilTax.ClientID%>").value);
            water = parseFloat(document.getElementById("<%=txtWater.ClientID%>").value);
            inelgserv = parseFloat(document.getElementById("<%=txtIntelig.ClientID%>").value);
            supp = parseFloat(document.getElementById("<%=txtSupp.ClientID%>").value);
            garage = parseFloat(document.getElementById("<%=txtGarage.ClientID%>").value);
            totalrent = rent + services + counciltax + water + inelgserv + supp + garage
            document.getElementById("<%=txtTotalRent.ClientID%>").value = FormatCurrency(totalrent);
        }
        //this function will only be accurate to about 10 decimal places.
        function FormatCurrency(fPrice, decimals) {
            fPrice = Math.round(fPrice * Math.pow(10, 10)) / Math.pow(10, 10);
            if (decimals) {
                if (!isNaN(decimals)) indexCount = parseInt(decimals) + 1
                else indexCount = 3
            }
            else indexCount = 3 //default to two decimal places
            plusString = "0."
            for (i = 1; i < 11; i++) {
                if (indexCount == i) plusString += "5"
                else plusString += "0"
            }
            Negate = false
            if (parseFloat(fPrice) < 0) {
                fPrice = 0 - fPrice
                Negate = true
            }
            var sCurrency = "" + (parseFloat(fPrice) + parseFloat(plusString));
            var nPos = sCurrency.indexOf('.');
            if (nPos < 0 && indexCount > 0) {
                sCurrency += '.';
            }
            sCurrency = sCurrency.slice(0, nPos + indexCount);
            var nZero = indexCount - (sCurrency.length - nPos);
            for (var i = 0; i < nZero; i++)
                sCurrency += '0';
            if (Negate)
                return "-" + sCurrency;
            else
                return sCurrency;
        }
    </script>
    <link rel="stylesheet" href="../../Styles/lightbox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../../Scripts/prototype.js"></script>
    <script type="text/javascript" src="../../Scripts/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" src="../../Scripts/lightbox.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelPropertyRecord" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnHidden" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnLocation" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnType" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnMake" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <asp:Button ID="btnModel" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
            <div style="margin-left: 2px;">
                <table style="border: thin solid #000000; width: 100%;">
                    <tr>
                        <td style="border-bottom-style: solid; border-width: thin; border-color: #000000">
                            <div>
                                <asp:Panel ID="pnlPropertyAddress" runat="server" CssClass="pnlPropertyAddress">
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblProperty" runat="server" Font-Bold="True" Text="Property &gt;"></asp:Label>
                                    <asp:Label ID="lblPropertyAddres" runat="server" Font-Bold="True"></asp:Label>
                                </asp:Panel>
                                 <div style="float: right; padding-top: 2px; padding-right: 5px; padding-bottom: 5px;">
                                    <asp:UpdatePanel ID="updReportFault" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel2" runat="server" CssClass="pnlPropertyAddress">
                                                <asp:Button style="padding: 5px;" ID="btnRaisePO" Text="Raise a PO" runat="server" OnClick="btnRaisePO_Click"
                                                     />
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="float: right; padding-top: 2px; padding-right: 5px; padding-bottom: 5px;">
                                    <asp:Button ID="btnBack" style="padding: 5px;" runat="server" OnClick="btnBack_click" Text="&nbsp;&lt; Back &nbsp;" />
                                </div>
                               
            </div>
            </td> </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <div style='margin-left: 14px; margin-top: 5px;'>
                        <asp:LinkButton ID="lnkBtnSummaryTab" OnClick="lnkBtnSummaryTab_Click" CommandArgument="lnkBtnSummaryTab_Click"
                            CssClass="TabInitial" runat="server" Visible="false" BorderStyle="Solid">Summary: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnActivitiesTab" OnClick="lnkBtnActivitiesTab_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Activities: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnAttributesTab" OnClick="lnkBtnAttributesTab_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Attributes: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnAccommodation" OnClick="lnkBtnAccommodation_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Accommodation: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnDocTab" OnClick="lnkBtnDocTab_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Documents: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnFinancial" OnClick="lnkBtnFinancial_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Financial: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnHealthSafety" OnClick="lnkBtnHealthSafety_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Health & Safety: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnStatusHistory" OnClick="lnkBtnStatusHistory_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Status History: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnRestriction" OnClick="lnkBtnRestriction_Click" CssClass="TabInitial"
                            runat="server" Visible="false" BorderStyle="Solid">Restriction: </asp:LinkButton>
                        <div style='clear: both;'>
                        </div>
                    </div>
                    <div style='margin-top: 8px; border-bottom: 1px solid black;'>
                    </div>
                    <asp:MultiView ID="MainView" runat="server">
                        <asp:View ID="View1" runat="server">
                            <prop:Summary ID="Summary" runat="server" />
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <prop:Activities ID="Activities" runat="server" />
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <prop:Attributes ID="Attributes" runat="server" />
                        </asp:View>
                        <asp:View ID="View7" runat="server">
                            <prop:Accommodation ID="Accommodation" runat="server" />
                        </asp:View>
                        <asp:View ID="View4" runat="server">
                            <prop:Document ID="Document" runat="server" />
                        </asp:View>
                        <asp:View ID="View5" runat="server">
                            <div class="pm-Financialbox">
                                <prop:Financial ID="Financial" runat="server" />
                            </div>
                        </asp:View>
                        <asp:View ID="View6" runat="server">
                            <prop:HealthSafety ID="HealthSafety" runat="server" />
                        </asp:View>
                        <asp:View ID="View8" runat="server">
                            <prop:StatusHistory ID="StatusHistory" runat="server" />
                        </asp:View>
                        <asp:View ID="View9" runat="server">
                            <prop:Restriction ID="Restriction" runat="server" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            </table> </div>
            <asp:UpdateProgress ID="updateProgressPropertyRecord" runat="server" AssociatedUpdatePanelID="updPanelPropertyRecord"
                DisplayAfter="10">
                <ProgressTemplate>
                    <div class="loading-image">
                        <img alt="Please Wait" src="../../Images/progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Button ID="btnHiddenRaisePO" UseSubmitBehavior="false" runat="server" Text=""
                Style="display: none;" />
            <asp:ModalPopupExtender ID="mdlpopupRaisePO" runat="server" BackgroundCssClass="modalBack" PopupControlID="pnlRaisePO"
                TargetControlID="btnHiddenRaisePO">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlRaisePO" runat="server">
                <ucAssignToContractor:AssignToContractor ID="ucAssignToContractor" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
