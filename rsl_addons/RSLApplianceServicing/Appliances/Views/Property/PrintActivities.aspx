﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master" CodeBehind="PrintActivities.aspx.vb" Inherits="Appliances.PrintActivities1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="PrintActivities" Src="~/Controls/Property/PrintActivities.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<link href="../../Styles/printStyle.css" rel="stylesheet" type="text/css" media="print"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<uc1:PrintActivities runat="server" ID="printAct" Visible="true" />

</asp:Content>
