﻿Module modMain
    Private WithEvents calculator As calculator = New calculator()

    Public Sub Main()
        Call calculator.DoSomethingEventful()
    End Sub

    Private Sub TotalUpdated(ByVal Amount As Decimal) Handles Calculator.TotalUpdatedEvent
        Trace.Write("Total was updated to " + Amount.ToString())
    End Sub
End Module

Public Class Calculator
    Public Event TotalUpdatedEvent(ByVal Amount As Decimal)

    Public Sub DoSomethingEventful()
        RaiseEvent TotalUpdatedEvent(0)
    End Sub
End Class
