﻿Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Public Class PrintPropertyCase
    Inherits PageBase

#Region "Data Members"

    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Dim lResultSet As DataSet = New DataSet()
    Dim mPropertyId As String

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        If Not IsNothing(Request.QueryString("id")) Then
            Me.mPropertyId = Request.QueryString("id")

            FetchAndShowData()
        Else
            'Handle Error
            uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.PropertyIdDoesNotExist, True)
        End If
    End Sub

#End Region

#Region "Private Functions"

    Private Sub FetchAndShowData()
        Try
            'Fetch data based on Property id and populated the dataset
            objPropertyBL.getApplianceCaseDetails(lResultSet, mPropertyId)

            If lResultSet.Tables.Count > 0 Then
                'Show individual sections
                ShowBasicInfo(lResultSet.Tables("BasicInfo"))
                ShowActivitiesData(lResultSet.Tables("Activities"))
                ShowAppliancesData(lResultSet.Tables("Appliances"))
                ShowDefectsData(lResultSet.Tables("Defects"))
            Else
                'Handle Error
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.SomeServerError, True)
            End If
        Catch ex As Exception

            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Private Sub ShowBasicInfo(ByVal pBasicInfoDt As DataTable)
        If pBasicInfoDt.Rows.Count > 0 Then
            lblCustomerName.Text = If(pBasicInfoDt.Rows(0)("CUSTOMERNAME").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("CUSTOMERNAME").ToString())
            lblAddress1.Text = If(pBasicInfoDt.Rows(0)("ADDRESS").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("ADDRESS").ToString())
            lblTown.Text = If(pBasicInfoDt.Rows(0)("TOWN").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("TOWN").ToString())
            lblPostCode.Text = If(pBasicInfoDt.Rows(0)("POST CODE").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("POST CODE").ToString())
            lblDate.Text = If(pBasicInfoDt.Rows(0)("START DATE").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("START DATE").ToString())
            lblPropertyFuel.Text = If(pBasicInfoDt.Rows(0)("FUEL TYPE").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("FUEL TYPE").ToString())
            lblTenancyRef.Text = If(pBasicInfoDt.Rows(0)("TENANCYID").ToString() = String.Empty, "N/A", pBasicInfoDt.Rows(0)("TENANCYID").ToString())
        Else
            'Handle Error
            lblCustomerName.Text = "N/A"
            lblAddress1.Text = "N/A"
            lblTown.Text = "N/A"
            lblPostCode.Text = "N/A"
            lblDate.Text = "N/A"
            lblPropertyFuel.Text = "N/A"
            lblTenancyRef.Text = "N/A"

            If lResultSet.Tables("Activities").Rows.Count = 0 And lResultSet.Tables("Appliances").Rows.Count = 0 And lResultSet.Tables("Defects").Rows.Count = 0 Then
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.NoDataFoundCase, True)
            Else
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.ErrorFetchingCustomerInfo, True)
            End If
        End If
    End Sub

    Private Sub ShowActivitiesData(ByVal pActivitiesDt As DataTable)
        rptActivities.DataSource = pActivitiesDt
        rptActivities.DataBind()
    End Sub

    Private Sub ShowAppliancesData(ByVal pAppliancesDt As DataTable)
        rptAppliance.DataSource = pAppliancesDt
        rptAppliance.DataBind()
    End Sub

    Private Sub ShowDefectsData(ByVal pDefectsDt As DataTable)
        rptDefects.DataSource = pDefectsDt
        rptDefects.DataBind()
    End Sub

#End Region

#Region "Control Events"

    Protected Sub rptActivities_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptActivities.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Then
                For Each c As Control In e.Item.Controls
                    If TypeOf (c) Is Label Then
                        Dim lbl As Label = CType(c, Label)

                        If lbl.Text.Trim() = String.Empty Then
                            lbl.Text = "-"
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptAppliance_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAppliance.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Then
                For Each c As Control In e.Item.Controls
                    If TypeOf (c) Is Label Then
                        Dim lbl As Label = CType(c, Label)

                        If lbl.Text.Trim() = String.Empty Then
                            lbl.Text = "-"
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptDefects_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDefects.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Then
                For Each c As Control In e.Item.Controls
                    If TypeOf (c) Is Label Then
                        Dim lbl As Label = CType(c, Label)

                        If lbl.Text.Trim() = String.Empty Then
                            lbl.Text = "-"
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

End Class