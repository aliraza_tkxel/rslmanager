﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectApproval.aspx.vb"  Async="true" Inherits="Appliances.DefectApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="DefectToBeApproved" Src="~/Controls/Reports/DefectToBeApproved.ascx" %>
<%@ Register TagPrefix="uc3" TagName="DefectApproved" Src="~/Controls/Reports/DefectApproved.ascx" %>
<%@ Register TagPrefix="uc4" TagName="DefectRejected" Src="~/Controls/Reports/DefectRejected.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <link href="../../../Styles/scheduling.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/jquery-1.10.1.min.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        .message
        {
            padding-left: 12px;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function sendSMS(mobile, body, smsurl) {

            $.ajax({
                type: "GET",
                url: smsurl,
                data: "mobile=" + mobile + "&message=" + body + "",
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                },
                error: function(xhr, error){
                }
            });
        }
  
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        function showSearchText(x) {
            console.log('asdas');
            
            var buttonID = '<%= txtSearch.ClientID %>';
            var button = document.getElementById(buttonID);
            if (button) { button.style.display = 'inherit'; }
        }
        function hideSearchText(x) {
            console.log('asdasdasdasdasdas');

            var buttonID = '<%= txtSearch.ClientID %>';
            var button = document.getElementById(buttonID);
            if (button) { button.style.display = 'none'; }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="updPanelDefect" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Defects Approval Report</span>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <br />
                    </asp:Panel>
                    <div style="overflow:auto;">
                       
                        <div class="form-control right">
                            <div class="select_div right" style="padding-top:12px;">
                                <div class="field right" style="margin-left:0;">
                                    <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right">
                                        <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3.4px 10px !important; width:150px; margin:-3px 20px 0 0;" AutoCompleteType="Search" 
                                            onkeyup="TypingInterval();" ToolTip="Search" class="searchbox searchText styleselect-control" runat="server">
                                        </asp:TextBox>
                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                            TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText styleselect-control">
                                        </ajaxToolkit:TextBoxWatermarkExtender>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <hr />
                    <div style="padding: 10px 10px 0 10px;">
                        <div style="overflow:auto; padding: 0px !important;">
                            <asp:LinkButton ID="lnkBtnDefectToBeApprovedTab" OnClick="lnkBtnDefectToBeApprovedTab_Click" CssClass="display TabInitial"
                                runat="server">To Be Approved: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnDefectApprovedTab" OnClick="lnkBtnDefectApprovedTab_Click" CssClass="TabInitial"
                                runat="server" style="display:block;">Approved: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnDefectRejectedTab" OnClick="lnkBtnDefectRejectedTab_Click" CssClass="TabInitial"
                                runat="server" style="display:block;">Rejected: </asp:LinkButton>
                            <span style="display:block; height: 27px; border-bottom:1px solid #c5c5c5">
                            </span>
                        </div>    
                    </div>
                    
                    <div style="padding:0 10px">
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <uc2:DefectToBeApproved ID="DefectToBeApproved" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <uc3:DefectApproved ID="DefectApproved" runat="server" />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <uc4:DefectRejected ID="DefectRejected" runat="server" />
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelDefect" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="padding-left: 11px;">
                <b>Defects Approval Report</b>
            </div>
            <br />
            
            <table style="border: thin solid #000000; width: 100%;">
                <%--<tr>
                    <td style="padding-left: 10px; padding-right: 0px;">
                        <div id="div_lineAppointmentsToBeArranged" runat="server" style="border-bottom: 1px solid black;
                            clear: both; width: 100%;">
                        </div>
                    </td>
                    <td style="padding-left: 0px;">
                        <div id="div_lineAppointmentsArranged" runat="server" style="border-bottom: 1px solid black; clear: both; 
                            width: 100%;">
                        </div>
                    </td>
                     <td style="padding-left: 1px;">
                        <div id="div_lineConditionRatingRejected" runat="server" style="border-bottom: 1px solid black; clear: both; 
                            width: 100%;">
                        </div>
                    </td>
                    <td colspan="1" style="padding-left: 1px; padding-right: 1px;">
                        <div style="border-bottom: 1px solid black; clear: both;
                            width: 100%;">
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>--%>