﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class DefectApproval
    Inherits PageBase

#Region "Properties "

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer = -1
            'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
            '    componentid = Request.QueryString(PathConstants.ComponentId)
            'End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'Me.highLightCurrentPageMenuItems()
                MainView.ActiveViewIndex = 0

                If lnkBtnDefectToBeApprovedTab.Enabled Then
                    'div_lineAppointmentsToBeArranged.Visible = False
                    'div_lineAppointmentsArranged.Visible = True
                Else
                    'div_lineAppointmentsToBeArranged.Visible = True
                    'div_lineAppointmentsArranged.Visible = False
                End If

                activateDefectToBeApproved()
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"
    ''' <summary>
    ''' Text Field Search Text Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try

            searchResults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try

    End Sub

#End Region


#Region "Lnk Btn Defect To Be Approved Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating To Be Approved Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDefectToBeApprovedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = False
        'div_lineAppointmentsArranged.Visible = True
        'div_lineConditionRatingRejected.Visible = True
        txtSearch.Text = ""
        activateDefectToBeApproved()
    End Sub
#End Region

#Region "Lnk Btn Defect Approved Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating Approved Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDefectApprovedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = True
        'div_lineAppointmentsArranged.Visible = False
        'div_lineConditionRatingRejected.Visible = True
        txtSearch.Text = ""
        activateAppointmentsArranged()

    End Sub
#End Region

#Region "Lnk Btn Defect Rejected Tab Click"
    ''' <summary>
    ''' Lnk Btn Condition Rating Rejected Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDefectRejectedTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        'div_lineAppointmentsToBeArranged.Visible = True
        'div_lineAppointmentsArranged.Visible = True
        'div_lineConditionRatingRejected.Visible = False
        txtSearch.Text = ""
        activateDefectRejected()

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Activate Appointment to be Arranged"
    ''' <summary>
    ''' Activate Appointment to be Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateDefectToBeApproved()
        lnkBtnDefectToBeApprovedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnDefectApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDefectRejectedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        populateDefectToBeApprovedList()
    End Sub
#End Region

#Region "Activate Appointments Arranged"
    ''' <summary>
    ''' Activate Appointments Arranged
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateAppointmentsArranged()
        lnkBtnDefectApprovedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnDefectToBeApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDefectRejectedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 1
        Me.populateDefectApprovedList()
    End Sub
#End Region

#Region "Activate Defect Rejected"
    ''' <summary>
    ''' Activate Condition Rating Rejected
    ''' </summary>
    ''' <remarks></remarks>
    Sub activateDefectRejected()
        lnkBtnDefectRejectedTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnDefectApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDefectToBeApprovedTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 2
        Me.populateDefectRejectedList()
    End Sub
#End Region


#Region "Populate Appointment To Be Arranged List"
    ''' <summary>
    ''' Populate Appointment To Be Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectToBeApprovedList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        DefectToBeApproved.populateDefectToBeApprovedReport(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region

#Region "populate Defect Approved List"
    ''' <summary>
    ''' Populate Appointments Arranged List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectApprovedList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        DefectApproved.populateDefectApprovedReport(resultDataSet, search, True, GetComponentId)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region



#Region "populate Defect Rejected List"
    ''' <summary>
    ''' populate Defect Rejected List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectRejectedList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()

        DefectRejected.populateDefectRejectedReport(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ' ViewState.Add(ViewStateConstants.populateConditionRatingToBeApprovedReport, resultDataSet)

    End Sub

#End Region

#Region "Search Results"
    ''' <summary>
    ''' Search Results
    ''' </summary>
    ''' <remarks></remarks>
    Sub searchResults()

        If (MainView.ActiveViewIndex = 0) Then
            populateDefectToBeApprovedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateDefectApprovedList()
        Else
            populateDefectRejectedList()
        End If

    End Sub

#End Region

#Region "Change Scheme"
    ''' <summary>
    ''' Change Scheme
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeScheme()

        'If (MainView.ActiveViewIndex = 0) Then
        '    populateConditionRatingToBeApprovedList()
        '    lblStartDate.Visible = False
        '    txtDate.Visible = False
        'Else
        '    populateAppointmentsArrangedList()
        '    lblStartDate.Visible = True
        '    txtDate.Visible = True
        'End If
    End Sub

#End Region

    '#Region "highLight Current Page Menu Items"
    '    ''' <summary>
    '    ''' This function 'll hight light the menu link button and it 'll also make the update panel visible
    '    ''' </summary>
    '    ''' <remarks></remarks>
    '    Public Sub highLightCurrentPageMenuItems()
    '        Dim controlList As List(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))
    '        'Add link buttons that should be highlighted        
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnAppScehduleing"))
    '        controlList.Add(Tuple.Create(ApplicationConstants.LinkButtonType, "lnkBtnScheduleWorks"))
    '        'Add update panel that should be visible e.g
    '        controlList.Add(Tuple.Create(ApplicationConstants.UpdatePanelType, "updPanelScheduling"))
    '        'call the base class function to highlight the items
    '        MyBase.highLightMenuItems(controlList)
    '    End Sub
    '#End Region


#End Region

End Class