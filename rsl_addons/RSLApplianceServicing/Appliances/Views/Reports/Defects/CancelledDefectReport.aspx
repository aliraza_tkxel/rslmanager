﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="CancelledDefectReport.aspx.vb" Inherits="Appliances.CancelledDefectReport"
    MaintainScrollPositionOnPostback="true" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .select_div select
        {
            width: 254px !important;
        }
        .searchbox
        {
            background-position:right center;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchBox.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelCancelledDefectReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Cancelled Defect Report</span>
                    <div class="field right">
                        <asp:TextBox ID="txtSearchBox" AutoPostBack="false" style="padding:3.4px 10px !important; margin:-3px 10px 0 0;" AutoCompleteType="Search" 
                            ToolTip="Search" class="searchbox styleselect-control searchbox right" onkeyup="TypingInterval();" runat="server">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                            TargetControlID="txtSearchBox" WatermarkText="Quick find ..." WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow:inherit; padding-bottom:0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server" Text="asdf">
                        </asp:Label>
                    </asp:Panel>
                    <div style="overflow:auto; padding-bottom:10px">
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Select Scheme
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlSchemes" runat="server" CssClass="styleselect styleselect-control" OnSelectedIndexChanged="ddlSchemes_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Select Type
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="styleselect styleselect-control" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    Select Name
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlAppliances" runat="server" CssClass="styleselect styleselect-control" OnSelectedIndexChanged="ddlAppliances_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <cc1:PagingGridView ID="grdCancelledDefectReport" runat="server" AutoGenerateColumns="False" AllowSorting="True" PageSize="10"
                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" OnRowCreated="grdCancelledDefectReport_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Scheme" SortExpression="SchemeName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSchemeName" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Post Code" SortExpression="PostCode">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("PostCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appliance" SortExpression="ApplianceType">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplianceType" runat="server" Text='<%# Bind("ApplianceType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Recorded" SortExpression="RecordedBy">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRecordedBy" runat="server" Text='<%# Bind("RecordedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appointment Date" SortExpression="AppointmentDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointmentDate" runat="server" Text='<%# Eval("AppointmentDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cancelled By" SortExpression="CancelledBy">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCancelledBy" runat="server" Text='<%# Bind("CancelledBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cancelled" SortExpression="CancelledDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCancelled" runat="server" Text='<%# Eval("CancelledDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewReason" runat="server" CommandArgument='<%# Eval("CancelNotes") %>' OnClick="btnViewReason_Click" 
                                            CssClass="btn btn-xs btn-blue right" style="padding:3px 10px !important; margin:-3px 0 0 0;" Text="View Reason" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <hr />
                                <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                                    Font-Bold="true" />
                            </EmptyDataTemplate>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                    class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <asp:Panel ID="pnlCancelReasonPopup" runat="server">
                <asp:ImageButton ID="imgBtnCancelCancelReasonPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    BorderWidth="0" />
                <div class="apt-cancel-reason-popup">
                    <div style="padding: 10px;">
                        <b>Cancel Reason</b></div>
                    <hr />
                    <div style="padding: 10px;">
                        <asp:Label ID="lblCancelReasonMessage" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Label ID="lblDisplayCancelReasonPopup" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlCancelReasonPopup" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDisplayCancelReasonPopup" PopupControlID="pnlCancelReasonPopup"
                DropShadow="true" CancelControlID="imgBtnCancelCancelReasonPopup" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressCancelledDefectReport" runat="server" AssociatedUpdatePanelID="updPanelCancelledDefectReport"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
