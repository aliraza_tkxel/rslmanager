﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" Async="true" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="ApplianceDefectReport.aspx.vb" Inherits="Appliances.ApplianceDefectReport"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table#ReportStats tr td a
        {
            color: Black;
        }
        .searchbox
        {
            background-position:right center;
        }
        .text_div
        {
            width:220px;
        }
        .text_div input
        {
            width:130px !important;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchBox.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelApplianceDefectReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Appliance Defect Report</span>
                    <div class="field right">
                        <asp:TextBox ID="txtSearchBox" AutoPostBack="false" onkeyup="TypingInterval();" style="padding:3.4px 10px !important; margin:-3px 10px 0 0;" AutoCompleteType="Search" 
                            ToolTip="Search" class="styleselect-control searchbox right" runat="server">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                            TargetControlID="txtSearchBox" WatermarkCssClass="searchbox" WatermarkText="Quick find ...">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow:inherit; padding-bottom:0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="true">
                        <asp:Label ID="lblMessage" runat="server" Text="asdf">
                        </asp:Label>
                    </asp:Panel>
                    <div style="overflow:auto; padding-bottom:10px">
                        <div class="form-control" >
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="lblAppointmentStatus" runat="server" Text="Appointment Status"></asp:Label>
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlAppointmentStatus" OnSelectedIndexChanged="ddlAppointmentStatus_SelectedIndexChanged" 
                                        runat="server" CssClass="styleselect styleselect-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <div style="height: 500px; overflow: auto;">
                            <cc1:PagingGridView ID="grdApplianceDefectReport" runat="server" AutoGenerateColumns="False" AllowSorting="True" PageSize="30"
                                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" OnRowCreated="grdApplianceDefectReport_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="JSD" ItemStyle-CssClass="dashboard" SortExpression="JSD">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJsd" runat="server" Text='<%# Bind("JSD") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="78px" />
                                        <ItemStyle BorderStyle="None" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="140px" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="140px" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Block" SortExpression="Block">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="140px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Post Code" SortExpression="PostCode">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPostCode" Text='<%# EVAL("PostCode") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="Left" Width="130px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Appliance" SortExpression="ApplianceType">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAppliance" runat="server" Text='<%# Bind("ApplianceType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="Left" Width="95px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Recorded" SortExpression="RecordedBy">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRecordedBy" runat="server" Text='<%# Bind("RecordedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Appointment(Status)" SortExpression="AppointmentStatus">
                                        <ItemTemplate>
                                            <asp:Label ID="AppointmentStatus" runat="server" Text='<%# Bind("AppointmentStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="Left" Width="168px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDefectDetail" ImageUrl="~/Images/aero.png" AlternateText=">"
                                                runat="server" BorderStyle="None" 
                                                CommandArgument='<%# Eval("JSD") %>' onclick="imgbtnDefectDetail_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                            </cc1:PagingGridView>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                    class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressApplianceDefectReport" runat="server" AssociatedUpdatePanelID="updPanelApplianceDefectReport"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
