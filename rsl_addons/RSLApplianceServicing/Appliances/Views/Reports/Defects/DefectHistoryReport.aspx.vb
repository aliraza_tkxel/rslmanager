﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessObject

Public Class DefectHistoryReport
    Inherits PageBase

#Region "Attributes"
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "SchemeName", 1, 10)
#End Region

#Region "Events"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If (Not IsPostBack) Then
                Me.populateDefectTypes()
                Me.setPageSortBo(objPageSortBo)
                Me.populateYears()
                Me.populateAllSchemes()
                Me.populateAllAppliances()
                Me.populateDefectHistoryReport()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "txt Search Box Text Changed"
    Protected Sub txtSearchBox_TextChanged(sender As Object, e As EventArgs) Handles txtSearchBox.TextChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Year Selected Index Changed"
    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Schemes Selected Index Changed"
    Protected Sub ddlSchemes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSchemes.SelectedIndexChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Appliances Selected Index Changed"
    Protected Sub ddlAppliances_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAppliances.SelectedIndexChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub grdDefectHistoryReport_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoPageNumber.Click
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = Me.getPageSortBo()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then

                objPageSortBo.PageNumber = pageNumber

                setPageSortBo(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Me.populateDefectHistoryReport()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "grd Defect History Report Sorting"

    Protected Sub grdDefectHistoryReport_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDefectHistoryReport.Sorting
        Try
             Dim objPageSortBo As PageSortBO = getPageSortBo()
            objPageSortBo.PageNumber = 1
            If (objPageSortBo.SortExpression = e.SortExpression) Then
                objPageSortBo.setSortDirection()
            End If
            objPageSortBo.SortExpression = e.SortExpression
            setPageSortBo(objPageSortBo)
            populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            populateDefectHistoryReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn View Click"
    Protected Sub lnkBtnView_Click(sender As Object, e As EventArgs)
        Try
            Dim viewButton As LinkButton = CType(sender, LinkButton)
            Dim parameters As String = viewButton.CommandArgument.ToString()
            Dim splitter() As Char = {":::"}
            Dim parameterArray As String() = parameters.Split(splitter, StringSplitOptions.RemoveEmptyEntries)
            Me.lblScheme.Text = parameterArray(1)
            Me.lblAddress.Text = parameterArray(2)
            If parameterArray(parameterArray.Length - 1) = "Block" Then
                lblForAddress.Text = "Block"
                lblForpnlHeading.Text = "Block Defects List"
            ElseIf parameterArray(parameterArray.Length - 1) = "Scheme" Then
                lblForAddress.Visible = False
                lblForpnlHeading.Text = "Scheme Defects List"
            End If
            If parameterArray(parameterArray.Length - 1) = "Property" Then
                Me.getPropertyDefectsList(parameterArray(0))
            Else
                Dim pId As Integer = parameterArray(0)
                Me.lblAddress.Text = parameterArray(3)
                Me.getSchemeBlockDefectsList(pId, parameterArray(parameterArray.Length - 1))
            End If
            Me.mdlViewDefectsPopup.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Appliances Selected Index Changed"
    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        Try
            Me.populateAllAppliances()
            Me.populateDefectHistoryReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate defect types"
    Private Sub populateDefectTypes()
        ddlType.Items.Clear()
        ddlType.Items.Add(New ListItem(ApplicationConstants.ApplianceValue, ApplicationConstants.ApplianceKey))
        ddlType.Items.Add(New ListItem(ApplicationConstants.BoilerValue, ApplicationConstants.BoilerKey))
        ddlType.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownDefaultValue))
    End Sub
#End Region

#Region "populate Defect History Report "

    Private Sub populateDefectHistoryReport()

        Dim objReportBl As ReportBL = New ReportBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0
        Dim objDefectReportBo As DefectReportBo = New DefectReportBo()
        objDefectReportBo.SchemeId = Convert.ToInt32(ddlSchemes.SelectedValue)
        objDefectReportBo.ApplianceType = ddlAppliances.SelectedItem.Text
        objDefectReportBo.SearchText = txtSearchBox.Text
        objDefectReportBo.Year = Convert.ToInt32(ddlYear.SelectedValue)
        objDefectReportBo.DefectType = ddlType.SelectedValue
        objPageSortBo = getPageSortBo()
        totalCount = objReportBl.getDefectHistoryReport(resultDataSet, objDefectReportBo, objPageSortBo)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
        grdDefectHistoryReport.VirtualItemCount = totalCount
        grdDefectHistoryReport.DataSource = resultDataSet
        grdDefectHistoryReport.DataBind()
        setPageSortBo(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
    End Sub
#End Region

#Region "Get Property Defects List"
    Private Sub getPropertyDefectsList(propertyId As String)
        Dim objReportBl As ReportBL = New ReportBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim year As Integer = Convert.ToInt32(ddlYear.SelectedValue)
        Dim defectType As String = ddlType.SelectedValue
        Dim applianceType As String = ddlAppliances.SelectedItem.Text
        objReportBl.getPropertyDefectList(resultDataSet, propertyId, year, defectType, applianceType)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
        grdViewPropertyDefectsList.DataSource = resultDataSet
        grdViewPropertyDefectsList.DataBind()
    End Sub
#End Region

#Region "Get Scheme/Block Defects List"
    Private Sub getSchemeBlockDefectsList(propertyId As Integer, reqType As String)
        Dim objReportBl As ReportBL = New ReportBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim year As Integer = Convert.ToInt32(ddlYear.SelectedValue)
        Dim defectType As String = ddlType.SelectedValue
        Dim applianceType As String = ddlAppliances.SelectedItem.Text
        objReportBl.getSchemeBlockDefectList(resultDataSet, propertyId, reqType, year, defectType, applianceType)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
        grdViewPropertyDefectsList.DataSource = resultDataSet
        grdViewPropertyDefectsList.DataBind()
    End Sub
#End Region


#Region "Populate Years"
    Private Sub populateYears()
        Dim calUtility As CalendarUtilities = New CalendarUtilities()
        ddlYear.DataSource = calUtility.GetLast50YearsList()
        ddlYear.DataBind()
        ddlYear.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownDefaultValue))
		
		If (Not IsPostBack) Then
              ddlYear.SelectedValue = Convert.toString(Date.Today.Year)
        End If
         
    End Sub
#End Region

#Region "Populate All Schemes"
    Private Sub populateAllSchemes()
        Dim objDefectsBl As DefectsBL = New DefectsBL()
        Dim resultDataSet As DataSet = New DataSet()
        objDefectsBl.getAllSchemes(resultDataSet)
        ddlSchemes.DataSource = resultDataSet
        ddlSchemes.DataValueField = ApplicationConstants.Id
        ddlSchemes.DataTextField = ApplicationConstants.Title
        ddlSchemes.DataBind()
        ddlSchemes.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddlSchemes.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Populate All Appliances"
    Private Sub populateAllAppliances()
        Dim objDefectsBl As DefectsBL = New DefectsBL()
        Dim resultDataTable As DataTable = New DataTable()
        Dim defectType As String = ddlType.SelectedValue.ToString()
        resultDataTable = objDefectsBl.getAllApplianceTypes(defectType)
        ddlAppliances.DataSource = resultDataTable
        ddlAppliances.DataValueField = ApplicationConstants.Id
        ddlAppliances.DataTextField = ApplicationConstants.Title
        ddlAppliances.DataBind()
        ddlAppliances.Items.Insert(0, New ListItem("All", ApplicationConstants.ApplianceDropDownDefaultValue))
        ddlAppliances.SelectedValue = ApplicationConstants.ApplianceDropDownDefaultValue
    End Sub
#End Region

#Region "Sort Grid"
    Private Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateDefectHistoryReport()
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdDefectHistoryReport.DataSource = dvSortedView
            grdDefectHistoryReport.DataBind()
        End If

    End Sub
#End Region

#Region "Set Page Sort Bo"
    Private Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Private Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#End Region




End Class