﻿Imports AS_Utilities
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports AS_BusinessObject

Public Class ApplianceDefectReport
    Inherits PageBase

#Region "Attributes"
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JSD", 1, 30)
#End Region

#Region "Events"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If (Not IsPostBack) Then
                Me.setPageSortBo(objPageSortBo)
                Me.populateAppointmentStatuses()
                Me.populateApplianceDefectReport()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "txt Search Box Text Changed"
    Protected Sub txtSearchBox_TextChanged(sender As Object, e As EventArgs) Handles txtSearchBox.TextChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateApplianceDefectReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddl Appointment Status Selected Index Changed"
    Protected Sub ddlAppointmentStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAppointmentStatus.SelectedIndexChanged
        Try
            Me.setPageSortBo(objPageSortBo)
            Me.populateApplianceDefectReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grd Appliance Defect Report Sorting"
    Protected Sub grdApplianceDefectReport_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdApplianceDefectReport.Sorting
        Try
             Dim objPageSortBo As PageSortBO = getPageSortBo()
            objPageSortBo.PageNumber = 1
            If (objPageSortBo.SortExpression = e.SortExpression) Then
                objPageSortBo.setSortDirection()
            End If
            objPageSortBo.SortExpression = e.SortExpression
            setPageSortBo(objPageSortBo)
            populateApplianceDefectReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoPageNumber.Click
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = Me.getPageSortBo()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then

                objPageSortBo.PageNumber = pageNumber

                setPageSortBo(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Me.populateApplianceDefectReport()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "grd Appliance Defect Report Row data bound"
     Protected Sub grdApplianceDefectReport_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdApplianceDefectReport.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim imgbtnDefectDetail As ImageButton = DirectCast(e.Row.FindControl("imgbtnDefectDetail"), ImageButton)
                Dim lblAppointmentStatus As Label = DirectCast(e.Row.FindControl("AppointmentStatus"), Label)
                If lblAppointmentStatus.Text = "Logged" Then
                    imgbtnDefectDetail.Visible = False



                End If

            End If
        Catch ex As Exception

        End Try


    End Sub

#End Region

    Protected Sub grdApplianceDefectReport_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub



#Region "img btn Defect Detail Click"

    Protected Sub imgbtnDefectDetail_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgbtnDefectDetail As ImageButton = CType(sender, ImageButton)
            Response.Redirect(String.Format("{0}?defectId={1}", PathConstants.DefectJobSheet, imgbtnDefectDetail.CommandArgument.ToString()))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region


#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            populateApplianceDefectReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region
#End Region

#Region "Functions"

#Region "populate Appliance Defect Report "

    Private Sub populateApplianceDefectReport()

        Dim objReportBl As ReportBL = New ReportBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0
        Dim objDefectReportBo As DefectReportBo = New DefectReportBo()
        objDefectReportBo.AppointmentStatusId = Convert.ToInt32(ddlAppointmentStatus.SelectedValue)
        objDefectReportBo.SearchText = txtSearchBox.Text
        objPageSortBo = getPageSortBo()
        totalCount = objReportBl.getApplianceDefectReport(resultDataSet, objDefectReportBo, objPageSortBo)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)

        grdApplianceDefectReport.VirtualItemCount = totalCount
        grdApplianceDefectReport.DataSource = resultDataSet
        grdApplianceDefectReport.DataBind()
        setPageSortBo(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
    End Sub
#End Region

#Region "Get Appointment Status"
    Private Sub populateAppointmentStatuses()
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getDefectAppointmentStatuses(resultDataSet)
        ddlAppointmentStatus.DataSource = resultDataSet
        ddlAppointmentStatus.DataValueField = ApplicationConstants.StatusId
        ddlAppointmentStatus.DataTextField = ApplicationConstants.Title
        ddlAppointmentStatus.DataBind()
        ddlAppointmentStatus.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddlAppointmentStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Sort Grid"
    Private Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateApplianceDefectReport()
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdApplianceDefectReport.DataSource = dvSortedView
            grdApplianceDefectReport.DataBind()
        End If

    End Sub
#End Region

#Region "Set Page Sort Bo"
    Private Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Private Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#End Region





   
End Class