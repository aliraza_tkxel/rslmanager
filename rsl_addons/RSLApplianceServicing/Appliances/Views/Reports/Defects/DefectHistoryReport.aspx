﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectHistoryReport.aspx.vb" Inherits="Appliances.DefectHistoryReport"
    MaintainScrollPositionOnPostback="true" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .searchbox
        {
            background-position: right center;
        }
        .select_div select
        {
            width: 164px !important;
        }
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchBox.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelDefectHistoryReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Defect History Report</span>
                    <div class="field right">
                        <asp:TextBox ID="txtSearchBox" AutoPostBack="false" AutoCompleteType="Search" ToolTip="Search"
                            runat="server" onkeyup="TypingInterval();" Style="padding: 3.4px 10px !important;
                            margin: -3px 10px 0 0;" class="searchbox styleselect-control searchbox right">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                            TargetControlID="txtSearchBox" WatermarkText="Quick find ..." WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow: inherit; padding-bottom: 0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="true">
                        <asp:Label ID="lblMessage" runat="server" Text="asdf">
                        </asp:Label>
                    </asp:Panel>
                    <div style="overflow: auto; padding-bottom: 10px">
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Select Year:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Select Scheme:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlSchemes" runat="server" OnSelectedIndexChanged="ddlSchemes_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Select Type:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Select Name:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlAppliances" runat="server" OnSelectedIndexChanged="ddlAppliances_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                        <cc1:PagingGridView ID="grdDefectHistoryReport" runat="server" AutoGenerateColumns="False"
                            AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
                            CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" OnRowCreated="grdDefectHistoryReport_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scheme" SortExpression="SchemeName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSchemeName" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Block" SortExpression="BlockName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Post Code" SortExpression="PostCode">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("PostCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Defects" SortExpression="NumberOfDefects">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNumberOfDefects" runat="server" Text='<%# Bind("NumberOfDefects") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last<br>Inspected" SortExpression="LastInspectedDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastInspectedDate" runat="server" Text='<%# Eval("LastInspectedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Certificate<br>Expiry" SortExpression="CertificateExpiryDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCertificateExpiry" runat="server" Text='<%# Eval("CertificateExpiryDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="110px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnView" runat="server" CommandArgument='<%#Eval("PropertyId")+":::"+ Eval("SchemeName")+":::"+ Eval("Address")+":::"+ Eval("BlockName")+":::"+ Eval("RequestType")%>'
                                            OnClick="lnkBtnView_Click" CssClass="btn btn-xs btn-blue" Style="padding: 2px 10px !important;">View</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="70px" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <hr />
                                <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                                    Font-Bold="true" />
                            </EmptyDataTemplate>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                            <div class="paging-left">
                                <span style="padding-right: 10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                        CssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                        CommandArgument="Prev" CssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span><span style="padding-right: 10px;"><b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. </span><span style="padding-right: 20px;">
                                        <b>Result:</b>
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        to
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        of
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </span><span style="padding-right: 10px;">
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                            CommandArgument="Next" CssClass="lnk-btn">
                                    
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                            CommandArgument="Last" CssClass="lnk-btn">
                                        
                                        </asp:LinkButton>
                                    </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                        PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                    UseSubmitBehavior="false" class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;"
                                    OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressDefectHistoryReport" runat="server" AssociatedUpdatePanelID="updPanelDefectHistoryReport"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelViewDefects" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlViewDefectsPopup" runat="server">
                <asp:ImageButton ID="imgBtnViewDefectsPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    BorderWidth="0" />
                <div class="defect-history-popup">
                    <div>
                        <b>Scheme:</b>
                        <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>, <b>
                            <asp:Label ID="lblForAddress" runat="server" Text="Address"></asp:Label></b>
                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></div>
                    <br />
                    <table style="width: 100%;">
                        <tr>
                            <td class="pnlHeading">
                                <b>
                                    <asp:Label ID="lblForpnlHeading" runat="server" Text="Property Defects List"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 5px 0 0;">
                                <div style="height: 400px; overflow: auto;">
                                    <cc1:PagingGridView ID="grdViewPropertyDefectsList" runat="server" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Appliance" SortExpression="ApplianceType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApplianceType" runat="server" Text='<%# Bind("ApplianceType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Recorded" SortExpression="RecordedBy">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRecordedBy" runat="server" Text='<%# Bind("RecordedBy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remedial Action Taken " SortExpression="IsActionTaken">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIsActionTaken" runat="server" Text='<%# Bind("IsActionTaken") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Appliance Disconnected" SortExpression="IsDiconnected">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIsDiconnected" runat="server" Text='<%# Eval("IsDiconnected") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Parts Ordered" SortExpression="IsPartsOrdered">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIsPartsOrdered" runat="server" Text='<%# Eval("IsPartsOrdered") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <hr />
                                            <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                                                Font-Bold="true" />
                                        </EmptyDataTemplate>
                                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                        <HeaderStyle CssClass="grdReportHeading" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="Gray" Font-Bold="True" />
                                    </cc1:PagingGridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Label ID="lblDisplayViewDefectsPopup" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlViewDefectsPopup" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDisplayViewDefectsPopup" PopupControlID="pnlViewDefectsPopup"
                DropShadow="true" CancelControlID="imgBtnViewDefectsPopup" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
