﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ApplianceDefectReport

    '''<summary>
    '''updPanelApplianceDefectReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelApplianceDefectReport As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''txtSearchBox control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchBox As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSearchBoxWatermarkExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchBoxWatermarkExtender As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAppointmentStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAppointmentStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlAppointmentStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAppointmentStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''grdApplianceDefectReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdApplianceDefectReport As Global.Fadrian.Web.Control.PagingGridView

    '''<summary>
    '''pnlPagination control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPagination As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lnkbtnPagerFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerFirst As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerPrev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerPrev As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblPagerCurrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerCurrentPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerTotalPages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerTotalPages As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordStart As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordEnd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkbtnPagerNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerNext As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerLast As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''rangevalidatorPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rangevalidatorPageNumber As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''txtPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnGoPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGoPageNumber As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''updateProgressApplianceDefectReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updateProgressApplianceDefectReport As Global.System.Web.UI.UpdateProgress
End Class
