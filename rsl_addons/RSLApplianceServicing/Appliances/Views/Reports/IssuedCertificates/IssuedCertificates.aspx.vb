﻿Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Imports System.Globalization
Imports AS_BusinessObject
Imports System.IO
Imports System.Threading

Public Class IssuedCertificates
    Inherits PageBase

#Region "Attributes"

    Dim objReportBL As New ReportBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Issued", 1, 20)
    Dim totalCount As Integer = 0

#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
            scriptManager.RegisterPostBackControl(Me.btnExport)
            If Not IsPostBack Then
                Me.setPageSortBo(objPageSortBo)
                Me.setDefaultValues()
                GetFuelType(ddlFuelType)
                Me.GetCertificatesNames(ddlCertificates)
                Me.populateIssuedCertificatesReport()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Get ceritificate names"

    Private Sub GetCertificatesNames(ByVal ddl As DropDownList, Optional ByVal fuelTypeSelectedValue As Integer = -1)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not fuelTypeSelectedValue = -1) Then
            objPropertyBl.GetCertificates(resultDataSet, fuelTypeSelectedValue)
        Else
            objPropertyBl.GetCertificates(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.heatingTypeId
        ddl.DataTextField = ApplicationConstants.CertificateName
        ddl.DataBind()
        If (fuelTypeSelectedValue = -1) Then
            ddl.Items.Insert(0, New ListItem("All", 0))
        Else
            ddl.Items.Add(New ListItem("All", 0))
        End If
    End Sub
#End Region

#Region "Load Fuel Type"

    Private Sub GetFuelType(ByVal ddl As DropDownList, Optional ByVal certificateNameSelectedValue As Integer = 0)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not certificateNameSelectedValue = 0) Then
            objPropertyBl.GetFuel(resultDataSet, certificateNameSelectedValue)
        Else
            objPropertyBl.GetFuel(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        Dim itemToRemove As ListItem
        itemToRemove = ddl.Items.FindByValue("901")
        If itemToRemove IsNot Nothing Then
            ddl.Items.Remove(itemToRemove)
        End If

        If (certificateNameSelectedValue = 0) Then
            ddl.Items.Insert(0, New ListItem("All", -1))
        Else
            ddl.Items.Add(New ListItem("All", -1))
        End If
    End Sub
#End Region

#Region "DDL Fuel Type Selected Index Change"

    Protected Sub ddlFuelType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            Dim fuelTypeSelectedValue = ddlFuelType.SelectedValue
            GetCertificatesNames(ddlCertificates, fuelTypeSelectedValue)
            If (fuelTypeSelectedValue = -1) Then
                GetFuelType(ddlFuelType, 0)
            End If
            populateIssuedCertificatesReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "DDL Certificate Name Selected Index Change"

    Protected Sub ddlCertificates_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCertificates.SelectedIndexChanged
        Try
            Dim certificateSelectedValue = ddlCertificates.SelectedValue
            GetFuelType(ddlFuelType, certificateSelectedValue)
            If (certificateSelectedValue = 0) Then
                GetCertificatesNames(ddlCertificates, -1)
            End If
            populateIssuedCertificatesReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region


#Region "Submit button click"

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        Try
            Me.searchReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Search button click"

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Try
            Me.searchReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Export button click"

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click

        Try
            Me.exportToExcel()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Follow On List Sorting Event"

    Protected Sub grdIssuedCertificate_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdIssuedCertificate.Sorting

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdIssuedCertificate.PageIndex = 0
            objPageSortBo.setSortDirection()
            Me.setPageSortBo(objPageSortBo)

            Me.populateIssuedCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdIssuedCertificate_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "lnk Btn IS Previous Click"

    Protected Sub lnkBtnISPrevious_Click(sender As Object, e As EventArgs) Handles lnkBtnISPrevious.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber - 1
            Me.setPageSortBo(objPageSortBo)
            Me.populateIssuedCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS Next Click"

    Protected Sub lnkBtnISNext_Click(sender As Object, e As EventArgs) Handles lnkBtnISNext.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber + 1
            Me.setPageSortBo(objPageSortBo)
            populateIssuedCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS First Click"

    Protected Sub lnkBtnISFirst_Click(sender As Object, e As EventArgs) Handles lnkBtnISFirst.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = 1
            Me.setPageSortBo(objPageSortBo)
            populateIssuedCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS Last Click"

    Protected Sub lnkBtnISLast_Click(sender As Object, e As EventArgs) Handles lnkBtnISLast.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = Me.getTotalPageNumbers()
            Me.setPageSortBo(objPageSortBo)
            populateIssuedCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Search Report"
    Public Sub searchReport()
        objPageSortBo = Me.getPageSortBo()

        objPageSortBo.PageNumber = 1
        grdIssuedCertificate.PageIndex = 0
        Me.setPageSortBo(objPageSortBo)

        Me.populateIssuedCertificatesReport()
    End Sub
#End Region

#Region "Set Default Values"

    Private Sub setDefaultValues()
        'Set from date to First day of current month
        txtBoxFromDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy")
        'Set to date to Last day of current month
        txtBoxToDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).ToString("dd/MM/yyyy")
        txtBoxQuickFind.Text = String.Empty
    End Sub

#End Region

#Region "Populate Issued Certificate Report"

    Private Sub populateIssuedCertificatesReport()

        Dim resultDS As New DataSet()
        Dim objIssuedCertificateBO As New IssuedCertificateBO()

        objPageSortBo = getPageSortBo()

        Dim cultureInfo As New CultureInfo("en-GB")
        DateTime.TryParseExact(txtBoxFromDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objIssuedCertificateBO.fromDate)
        DateTime.TryParseExact(txtBoxToDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objIssuedCertificateBO.toDate)
        objIssuedCertificateBO.searchText = Trim(txtBoxQuickFind.Text)
        objIssuedCertificateBO.heatingTypeId = ddlFuelType.SelectedValue

        If objIssuedCertificateBO.toDate < objIssuedCertificateBO.fromDate Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.IncorrectDateInterval, True)
        Else
            totalCount = objReportBL.getIssuedCertificateReport(objIssuedCertificateBO, objPageSortBo, resultDS)

            Dim totalPages As Integer = Math.Ceiling(totalCount / objPageSortBo.PageSize)

            Me.setTotalPageNumbers(totalPages)

            Dim dtTimeSheet = resultDS.Tables(0)

            grdIssuedCertificate.VirtualItemCount = totalCount
            grdIssuedCertificate.DataSource = dtTimeSheet
            grdIssuedCertificate.DataBind()

            If dtTimeSheet.Rows.Count = 0 Then
                pnlPagination.Visible = False
                pnlPaginationDummy.Visible = True
            Else
                pnlPagination.Visible = True
                pnlPaginationDummy.Visible = False
                setPagingLabels(totalCount)
                setNextPrevious(totalCount)
            End If

        End If

    End Sub

#End Region

#Region "Export to Excel"

    Private Sub exportToExcel()

        '------------------------------------------------------------------------------------------------------------
        'populate the grid for all records for given search criteria

        Dim resultDS As New DataSet()
        Dim objIssuedCertificateBO As New IssuedCertificateBO()

        objPageSortBo = getPageSortBo()
        objPageSortBo.PageNumber = 1
        objPageSortBo.PageSize = 65000

        Dim cultureInfo As New CultureInfo("en-GB")
        DateTime.TryParseExact(txtBoxFromDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objIssuedCertificateBO.fromDate)
        DateTime.TryParseExact(txtBoxToDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objIssuedCertificateBO.toDate)
        objIssuedCertificateBO.searchText = Trim(txtBoxQuickFind.Text)
        objIssuedCertificateBO.heatingTypeId = ddlFuelType.SelectedValue


        totalCount = objReportBL.getIssuedCertificateReport(objIssuedCertificateBO, objPageSortBo, resultDS)

        Dim dtPrintCertificate = resultDS.Tables(0)

        grdIssuedCertificateForXLS.PageSize = objPageSortBo.PageSize
        grdIssuedCertificateForXLS.VirtualItemCount = totalCount
        grdIssuedCertificateForXLS.DataSource = dtPrintCertificate
        grdIssuedCertificateForXLS.DataBind()

        '------------------------------------------------------------------------------------------------------------

        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "IssuedCertificateReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"

        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmWrite As New HtmlTextWriter(strWrite)
        Dim htmfrm As New HtmlForm()
        grdIssuedCertificateForXLS.Parent.Controls.Add(htmfrm)
        htmfrm.Attributes("runat") = "server"
        htmfrm.Controls.Add(grdIssuedCertificateForXLS)
        htmfrm.RenderControl(htmWrite)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()

    End Sub

#End Region

#Region "Set Page Sort Bo"

    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub

#End Region

#Region "Get Page Sort Bo"

    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Set Total Page Numbers"

    Protected Sub setTotalPageNumbers(ByRef totalCount As Integer)
        ViewState(ViewStateConstants.TotalPageNumbers) = totalCount
    End Sub

#End Region

#Region "Get Total Page Numbers"

    Protected Function getTotalPageNumbers() As Integer
        Return CType(ViewState(ViewStateConstants.TotalPageNumbers), Integer)
    End Function

#End Region

#Region "set Paging Labels"

    Private Sub setPagingLabels(ByVal totalCount As Integer)
        objPageSortBo = Me.getPageSortBo()
        If totalCount = 0 Then
            Me.lblISRecords.Text = "0 to 0 of 0"
            Me.lblISPages.Text = "0 of 0"

        ElseIf (totalCount <= objPageSortBo.PageSize) Then
            Me.lblISRecords.Text = "1 to " + Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
            Me.lblISPages.Text = "1 of 1"
        ElseIf (totalCount > objPageSortBo.PageSize) Then

            If (objPageSortBo.PageNumber * objPageSortBo.PageSize) <= totalCount Then

                Me.lblISRecords.Text = Convert.ToString((objPageSortBo.PageSize * (objPageSortBo.PageNumber - 1)) + 1) + " to " + Convert.ToString(objPageSortBo.PageSize * objPageSortBo.PageNumber) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(Math.Ceiling(totalCount / objPageSortBo.PageSize))

            ElseIf (objPageSortBo.PageNumber * objPageSortBo.PageSize) > totalCount Then

                Me.lblISRecords.Text = Convert.ToString((objPageSortBo.PageSize * (objPageSortBo.PageNumber - 1)) + 1) + " to " + Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(objPageSortBo.PageNumber)

            End If
        End If

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoPageNumber.Click
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = Me.getPageSortBo()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then

                objPageSortBo.PageNumber = pageNumber

                setPageSortBo(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Me.populateIssuedCertificatesReport()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Set Next Previous"

    Private Sub setNextPrevious(ByVal totalCount As Integer)
        objPageSortBo = Me.getPageSortBo()
        'if total records are less than page size
        If (totalCount <= objPageSortBo.PageSize) Then

            lnkBtnISPrevious.Enabled = False
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = False
            lnkBtnISLast.Enabled = False

        ElseIf (objPageSortBo.PageNumber = 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number = 1

            lnkBtnISPrevious.Enabled = False
            lnkBtnISNext.Enabled = True
            lnkBtnISFirst.Enabled = False
            lnkBtnISLast.Enabled = True

        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its last page 

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = False

        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its not last page 

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = True
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = True

        ElseIf (objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then

            'if its last page enable pervious link and disable next link

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = False

        End If
    End Sub

#End Region

#Region "Check Need to show CP12NUMBER hyperlink"

    Public Function IsShowCp12Number(ByRef isDocumentExist As Object)
        If (isDocumentExist = 0) Then
            Return 1
        Else
            Return 0
        End If
    End Function

#End Region

#Region "Cp12 Document Click"
    Protected Sub cp12Document_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mdlNoCp12Document.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close alert popup Click"

    Protected Sub imgBtnCancelAlertPopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancelAlertPopup.Click
        Try
            mdlNoCp12Document.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#End Region

End Class