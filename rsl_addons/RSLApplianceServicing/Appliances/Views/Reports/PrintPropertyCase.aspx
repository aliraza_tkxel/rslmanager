﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master"
    CodeBehind="PrintPropertyCase.aspx.vb" Inherits="Appliances.PrintPropertyCase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 24px;
        }
        
        /*
        .LeftAlignHeading
        {
            text-align: left;
            font-family: Tahoma, Helvetica, sans-serif;
            font-size: 13px;
        }
        
        tr.HeadingBorder td
        {
            border: 1px solid #000;
        }
        
        table
        {
            border-collapse: collapse;
        }
        
        .mainHeadings
        {
            background-color: #E0E0E0;
            height: 25px;
            padding-left: 10px;
            font-size: 15px;
        }
        
        .padDiv
        {
            margin-top: 15px;
            margin-bottom: 15px;
        }
        
        tr.padCells > td
        {
            padding-top: 5px;
            padding-bottom: 5px;
            font-family: Tahoma, Helvetica, sans-serif;
            font-size: 13px;
        }
        @media print {
        
        .hide { display:none;  }
        .mainHeadings
        {
            background-color: #E0E0E0;
            height: 25px;
            padding-left: 10px;
            font-size: 15px;
            -webkit-print-color-adjust: exact;
        }
        }
        
        */
    </style>
    <link href="../../Styles/propertymodule.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div style="border-width: 1px; border-color: gray; font-family: Tahoma, Helvetica, sans-serif;
        border-style: solid; border-width: thin; padding: 20px;">
        <div style="padding: 8px; padding-left: 15px; border-width: 1px; border-color: gray;
            border-style: solid;" class="hide">
            <%--<input id="btnBack" type="button" value="Back" />&nbsp;&nbsp;--%>
            <input id="btnPrint" type="button" value="Print" onclick="window.print();" />
        </div>
        <div style="padding: 15px; border-width: 1px; border-color: gray; border-style: solid;
            font-size: 13px;">
            <div style="float: left; font-size: 16px; width: 100%; border-width: 1px; border-style: solid;
                padding-top: 10px; padding-bottom: 10px; padding-left: 5px; margin-bottom: 10px;">
                <div>
                    <b>Appliance Case Details</b>
                </div>
                <%--<div>--%>
                <%--<input id="btnDisplayPhotographs" type="button" value="Display Photographs" style="float: right;" />--%>
                <%--</div>--%>
            </div>
            <div>
                <table style="width: 100%; margin-top: 0px;">
                    <tr>
                        <td>
                            <asp:Label ID="lblCustomerName" runat="server" Text="Customer Name"></asp:Label>
                        </td>
                        <td align="right">
                            Date:
                        </td>
                        <td>
                            &nbsp;
                            <asp:Label ID="lblDate" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblAddress1" runat="server" Text="Address 1"></asp:Label>
                        </td>
                        <td align="right" class="style1">
                            Tenancy Ref:
                        </td>
                        <td class="style1">
                            &nbsp;
                            <asp:Label ID="lblTenancyRef" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTown" runat="server" Text="Address 2"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;Property Fuel:
                        </td>
                        <td>
                            &nbsp;
                            <asp:Label ID="lblPropertyFuel" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPostCode" runat="server" Text="Address 3"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%--<div style="padding: 15px; border-width: 1px; border-color: gray; border-style: solid;">--%>
        <div class="padDiv">
            <div class="mainHeadings">
                <%--<div style="background-color: #E0E0E0">--%>
                <b>Activities:</b>
            </div>
            <div style="padding-top: 10px; padding-right: 10px; padding-left: 10px;">
                <%--<table style="width: 100%; margin-top: 0px;">
                        
                </table>--%>
                <asp:Repeater ID="rptActivities" runat="server">
                    <HeaderTemplate>
                        <table width="100%" style="text-align: left">
                            <tr class="HeadingBorder">
                                <th class="LeftAlignHeading" width="10%">
                                    Date:
                                </th>
                                <th class="LeftAlignHeading" width="10%">
                                    Fuel:
                                </th>
                                <th class="LeftAlignHeading" width="25%">
                                    Status/Action:
                                </th>
                                <th class="LeftAlignHeading">
                                    Notes:
                                </th>
                                <th class="LeftAlignHeading" width="20%">
                                    Recorded by:
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="padCells">
                            <td>
                                <%--<%# Container.DataItem("CREATE DATE")%>--%>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE DATE")%>'></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("INSPECTION TYPE")%>--%>
                                <asp:Label ID="lblInspectionType" Text='<%# Eval("INSPECTION TYPE")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("STATUS")%>/<%# Container.DataItem("ACTION")%>--%>
                                <asp:Label ID="lblStatus" Text='<%# Eval("STATUS")%>' runat="server"></asp:Label>
                                /
                                <asp:Label ID="lblAction" Text='<%# Eval("ACTION")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%#Container.DataItem("NOTES")%>--%>
                                <asp:Label ID="lblNotes" Text='<%# Eval("NOTES")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%#Container.DataItem("NAME")%>--%>
                                <asp:Label ID="lblName" Text='<%# Eval("NAME")%>' runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
        <%--<div style="padding: 15px; border-width: 1px; border-color: gray; border-style: solid;">--%>
        <div class="padDiv">
            <div class="mainHeadings">
                <b>Appliances:</b>
                <%--<div >
                    </div>
                <div>
                
                </div>--%>
            </div>
            <div style="padding-top: 10px; padding-right: 10px; padding-left: 10px;">
                <%--<table style="width: 100%; margin-top: 0px;">
                        
                </table>--%>
                <asp:Repeater ID="rptAppliance" runat="server">
                    <HeaderTemplate>
                        <table width="100%" style="text-align: left">
                            <tr class="HeadingBorder">
                                <th class="LeftAlignHeading">
                                    Fuel:
                                </th>
                                <th class="LeftAlignHeading">
                                    Type:
                                </th>
                                <th class="LeftAlignHeading">
                                    Manufacturer:
                                </th>
                                <th class="LeftAlignHeading">
                                    Installed:
                                </th>
                                <th class="LeftAlignHeading">
                                    Removed:
                                </th>
                                <th class="LeftAlignHeading">
                                    Inspected:
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="padCells">
                            <td>
                                <%--<%# Container.DataItem("FUEL")%>--%>
                                <asp:Label ID="lblFuel" Text='<%# Eval("FUEL")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("TYPE")%>--%>
                                <asp:Label ID="lblType" Text='<%# Eval("TYPE")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("MAKE")%>--%>
                                <asp:Label ID="lblMake" Text='<%# Eval("MAKE")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%#Container.DataItem("DATEINSTALLED")%>--%>
                                <asp:Label ID="lblDateInstalled" Text='<%# Eval("DATEINSTALLED")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("REMOVED")%>--%>
                                <asp:Label ID="lblRemoved" Text='<%# Eval("REMOVED")%>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <%--<%# Container.DataItem("INSPECTED")%>--%>
                                <%--<asp:Label ID="lblInspected" Text='<%# Eval("INSPECTED")%>' runat="server"></asp:Label>--%>
                                <asp:Label ID="lblInspected" Text="-" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
        <%--<div style="padding: 15px; border-width: 1px; border-color: gray; border-style: solid;">--%>
        <div class="padDiv">
            <div class="mainHeadings">
                <b>Service Defects:</b>
            </div>
            <%--<div >
                    </div>
                <div>--%>
            <%--<input id="btnDisplayPhotographs" type="button" value="Display Photographs" style="float: right;" />--%>
            <%--</div>--%>
        </div>
        <div style="padding-top: 0px; padding-right: 10px; padding-left: 10px;">
            <%--<table style="width: 100%; margin-top: 0px;">
                        
                </table>--%>
            <asp:Repeater ID="rptDefects" runat="server">
                <HeaderTemplate>
                    <table width="100%" style="text-align: left">
                        <tr class="HeadingBorder">
                            <th class="LeftAlignHeading">
                                Date:
                            </th>
                            <th class="LeftAlignHeading">
                                Category:
                            </th>
                            <th class="LeftAlignHeading">
                                Defect:
                            </th>
                            <th class="LeftAlignHeading">
                                Remedial action:
                            </th>
                            <th class="LeftAlignHeading">
                                Warning advice note:
                            </th>
                            <th class="LeftAlignHeading">
                                Warning tag:
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="padCells">
                        <td>
                            <%--<%# Container.DataItem("DATE")%>--%>
                            <asp:Label ID="lblDateDefects" Text='<%# Eval("DATE")%>' runat="server"></asp:Label>
                        </td>
                        <td>
                            <%--<%# Container.DataItem("CATEGORY")%>--%>
                            <asp:Label ID="lblCategory" Text='<%# Eval("CATEGORY")%>' runat="server"></asp:Label>
                        </td>
                        <td>
                            <%--<%# Container.DataItem("DEFECT")%>--%>
                            <asp:Label ID="lblDefect" Text='<%# Eval("DEFECT")%>' runat="server"></asp:Label>
                        </td>
                        <td>
                            <%--<%# Container.DataItem("REMEDIAL ACTION")%>--%>
                            <asp:Label ID="lblRemedialAction" Text='<%# Eval("REMEDIAL ACTION")%>' runat="server"></asp:Label>
                        </td>
                        <td>
                            <%--<%# Container.DataItem("")%>--%>
                        </td>
                        <td>
                            <%--<%# Container.DataItem("INSPECTED")%>--%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    </div>
</asp:Content>
