﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

'Imports System.Linq
'Imports System.Dynamic

Public Class CertificateExpiry
    Inherits PageBase
#Region "Attributes"

    Dim objReportBL As ReportBL = New ReportBL()
    Dim objReportBO As ReportBO = New ReportBO()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "ExpiryDate", 1, 30)

    'To Implement different reports (report filters like 1-2 weeks etc.)
    Enum certificateExpiryReportType
        NoFilter = -1
        Expired = 0
        noWeeks = 1
        Weeks2 = 2
        Weeks4 = 3
        Weeks8 = 4
        Weeks12 = 5
        Weeks16 = 6
        Weeks52 = 7
        Weeks52Plus = 8
    End Enum
    Dim certType As Integer = 0

#End Region

#Region "Events"

#Region "Pre Init"
    Protected Sub Pre_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            'When Search Button is clicked set the report to NoFilter to Show all records.
            If (IsPostBack) AndAlso (Request(btnSearch.UniqueID) = btnSearch.Text) Then
                Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.NoFilter)
            End If
            If (Not IsPostBack) Then
                objPageSortBo.PageSize = 50
                Me.setPageSortBo(objPageSortBo)
            End If
            Me.GetCertificateExpiryReportInfo()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                SessionManager.removePropertyId()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        'End Code
    End Sub

#End Region

    Protected Sub grdCertificateExpiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCertificateExpiry.PageIndexChanging
        Try
            objPageSortBo = Me.getPageSortBo()
            Dim pg As Integer = 0
            pg = e.NewPageIndex
            objPageSortBo.PageNumber = pg + 1
            Me.grdCertificateExpiry.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.GetCertificateExpiryReportInfo()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub grdDevelopmentList_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Link Button Events"

    Protected Sub btnSearchClick(ByVal sender As Object, ByVal e As EventArgs)
        objPageSortBo.PageNumber = 1
        GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnNoCertificate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnNoCertificate.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.noWeeks)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnExpired_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnExpired.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Expired)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue1_2_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue1_2_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks2)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue2_4_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue2_4_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks4)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue4_8_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue4_8_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks8)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue8_12_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue8_12_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks12)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue12_16_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue12_16_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks16)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue16_52_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue16_52_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks52)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    Protected Sub lbtnDue52_Week_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnDue52_Week.Click
        objPageSortBo.PageNumber = 1
        Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.Weeks52Plus)
        Me.GetCertificateExpiryReportInfo()
    End Sub

    'Protected Sub lbtnTotal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnTotal.Click
    '    Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.NoFilter)
    '    Me.GetCertificateExpiryReportInfo()
    'End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoPageNumber.Click
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = Me.getPageSortBo()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then

                objPageSortBo.PageNumber = pageNumber

                setPageSortBo(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                Me.GetCertificateExpiryReportInfo()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region
#End Region

#Region "Function"

#Region "Certificate Expiry Report Details"

    Private Sub GetCertificateExpiryReportInfo()
        If (Not IsPostBack) Then
            Me.getPatches(ddlPatch)
            Me.getSchemes(ddlScheme, 0)
            Me.GetFuelType(ddlFuelType)
            Me.getPropertyType(ddlPropertyType)
            Me.getStage(ddlStage)
            Me.getCertificatesNames(ddlCertificates)

            'At the first page load set report type to no filter to show all records.
            Me.setCertificateExpiryReportTypeViewState(certificateExpiryReportType.NoFilter)
        End If

        '------------------------

        Dim reportType As certificateExpiryReportType = getCertificateExpiryReportTypeViewState()

        If (reportType = certificateExpiryReportType.NoFilter) Then
            lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_AllRecords
            certType = 0
        Else
            Dim filterexpression = String.Empty
            Select Case reportType
                Case certificateExpiryReportType.Expired
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_Expired
                    certType = 1
                    Exit Select
                Case certificateExpiryReportType.noWeeks
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_NoCertificateData
                    certType = 9
                    Exit Select
                Case certificateExpiryReportType.Weeks2
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn1_2Weeks
                    certType = 2
                    Exit Select
                Case certificateExpiryReportType.Weeks4
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn2_4Week
                    certType = 3
                    Exit Select
                Case certificateExpiryReportType.Weeks8
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn4_8Weeks
                    certType = 4
                    Exit Select
                Case certificateExpiryReportType.Weeks12
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn8_12Weeks
                    certType = 5
                    Exit Select
                Case certificateExpiryReportType.Weeks16
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn12_16Weeks
                    certType = 6
                    Exit Select
                Case certificateExpiryReportType.Weeks52
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn16_52Weeks
                    certType = 7
                    Exit Select
                Case certificateExpiryReportType.Weeks52Plus
                    lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueInMoreThan52Weeks
                    certType = 8
                    Exit Select
            End Select
        End If


        '---------------------------

        'Applying Search Criteria
        objReportBO.FuelType = ddlFuelType.SelectedItem.Value
        objReportBO.Patch = ddlPatch.SelectedItem.Value
        objReportBO.Scheme = ddlScheme.SelectedItem.Value
        objReportBO.PropertyType = ddlPropertyType.SelectedItem.Value
        objReportBO.Stage = ddlStage.SelectedItem.Value
        objReportBO.UPRN = txtBoxUPRN.Text
        objReportBO.CertificateType = certType
        objReportBO.StartDate = txtStartDate.Text
        objReportBO.EndDate = txtEndDate.Text
        objReportBO.HeatingType = ddlCertificates.SelectedValue

        objPageSortBo = Me.getPageSortBo()
        'Loading Stats
        Dim statsResultDataSet As DataSet = New DataSet()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0
        Dim array(10) As String
        array = objReportBL.getCertificateExpiryReportStats(statsResultDataSet, objReportBO, objPageSortBo)

        'Get total count from getCertificateExpiryReportStats
        totalCount = array(0)

        lblExpired.Text = array(1)
        lblNoCertificate.Text = array(9)
        lblDue1_2_Week.Text = array(2)
        lblDue1_4_Week.Text = array(3)
        lblDue4_8_Week.Text = array(4)
        lblDue8_12_Week.Text = array(5)
        lblDue12_16_Week.Text = array(6)
        lblDue16_52_Week.Text = array(7)
        lblDue52_Week.Text = array(8)
        lblTotal.Text = array(0)

        Dim filteredReport As DataTable = statsResultDataSet.Tables(0).Clone()

        Select Case reportType
            Case certificateExpiryReportType.Expired
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_Expired
                totalCount = Convert.ToInt32(lblExpired.Text)
                Exit Select
            Case certificateExpiryReportType.noWeeks
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_NoCertificateData
                totalCount = Convert.ToInt32(lblNoCertificate.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks2
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn1_2Weeks
                totalCount = Convert.ToInt32(lblDue1_2_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks4
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn2_4Week
                totalCount = Convert.ToInt32(lblDue1_4_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks8
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn4_8Weeks
                totalCount = Convert.ToInt32(lblDue4_8_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks12
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn8_12Weeks
                totalCount = Convert.ToInt32(lblDue8_12_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks16
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn12_16Weeks
                totalCount = Convert.ToInt32(lblDue12_16_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks52
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueIn16_52Weeks
                totalCount = Convert.ToInt32(lblDue16_52_Week.Text)
                Exit Select
            Case certificateExpiryReportType.Weeks52Plus
                lblCetificateExpiryReportHeading.Text = ApplicationConstants.CertificateExpiryReport_DueInMoreThan52Weeks
                totalCount = Convert.ToInt32(lblDue52_Week.Text)
                Exit Select
        End Select

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        grdCertificateExpiry.VirtualItemCount = totalCount
        grdCertificateExpiry.DataSource = statsResultDataSet.Tables(0)
        grdCertificateExpiry.DataBind()
        setPageSortBo(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
    End Sub
#End Region

    Public Sub ddlPatch_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
        getSchemes(ddlScheme, ddlPatch.SelectedValue)
    End Sub

#Region "Load Patches Information"
    Private Sub getPatches(ByVal ddl As DropDownList)
        Dim objBL As UsersBL = New UsersBL()
        Dim userTypeList As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        objBL.getPatchLocations(0, userTypeList)
        ddl.DataSource = userTypeList
        ddl.DataValueField = "UserTypeID"
        ddl.DataTextField = "Description"
        ddl.DataBind()
        ddl.Items.Add(New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub

#End Region

#Region "Load Schemes Information"
    Protected Sub getSchemes(ByVal ddl As DropDownList, ByVal selectedValue As Int32)

        Dim userTypeList As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        Dim objBL As UsersBL = New UsersBL()
        objBL.getSchemeNames(selectedValue, userTypeList)
        ddl.DataSource = userTypeList
        ddl.DataValueField = "UserTypeID"
        ddl.DataTextField = "Description"
        ddl.DataBind()
        ddl.Items.RemoveAt(0)
        ddl.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Fuel Type"

    Private Sub GetFuelType(ByVal ddl As DropDownList, Optional ByVal certificateNameSelectedValue As Integer = 0)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not certificateNameSelectedValue = 0) Then
            objPropertyBl.GetFuel(resultDataSet, certificateNameSelectedValue)
        Else
            objPropertyBl.GetFuel(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        Dim itemToRemove As ListItem
        itemToRemove = ddl.Items.FindByValue("901")
        If itemToRemove IsNot Nothing Then
            ddl.Items.Remove(itemToRemove)
        End If

        If (certificateNameSelectedValue = 0) Then
            ddl.Items.Insert(0, New ListItem("All", -1))
        Else
            ddl.Items.Add(New ListItem("All", -1))
        End If
    End Sub
#End Region

#Region "Load Property Type"
    Protected Sub getPropertyType(ByVal ddl As DropDownList)
        Dim resultDataSet As DataSet = New DataSet()
        objReportBL.getPropertyType(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.TypeId
        ddl.DataTextField = ApplicationConstants.TypeTitle
        ddl.DataBind()
        ddl.Items.Add(New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Stage"
    Protected Sub getStage(ByVal ddl As DropDownList)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.StatusId
        ddl.DataTextField = ApplicationConstants.Title
        ddl.DataBind()
        ddl.Items.Add(New ListItem("All", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region
#Region "get ceritificate names"

    Private Sub GetCertificatesNames(ByVal ddl As DropDownList, Optional ByVal fuelTypeSelectedValue As Integer = -1)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not fuelTypeSelectedValue = -1) Then
            objPropertyBl.GetCertificates(resultDataSet, fuelTypeSelectedValue)
        Else
            objPropertyBl.GetCertificates(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.heatingTypeId
        ddl.DataTextField = ApplicationConstants.CertificateName
        ddl.DataBind()
        If (fuelTypeSelectedValue = -1) Then
            ddl.Items.Insert(0, New ListItem("All", 0))
        Else
            ddl.Items.Add(New ListItem("All", 0))
        End If

    End Sub
#End Region
#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "grd Appliance Defect Report Sorting"
    Protected Sub grdCertificateExpiry_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCertificateExpiry.Sorting
        Try
            Dim objPageSortBo As PageSortBO = Me.getPageSortBo()
            objPageSortBo.PageNumber = 1
            If (objPageSortBo.SortExpression = e.SortExpression) Then
                objPageSortBo.setSortDirection()
            End If
            objPageSortBo.SortExpression = e.SortExpression
            Me.setPageSortBo(objPageSortBo)
            Me.GetCertificateExpiryReportInfo()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            Me.GetCertificateExpiryReportInfo()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region


#Region "View state Get/Set/Remove Function(s)/Method(s)"

#Region "Report Type Get/Set/Remove View state"

    Protected Function getCertificateExpiryReportTypeViewState() As certificateExpiryReportType
        Dim ReportType = certificateExpiryReportType.NoFilter
        If Not IsNothing(ViewState(ViewStateConstants.certificateExpiryReportType)) Then
            ReportType = (CType(ViewState(ViewStateConstants.certificateExpiryReportType), certificateExpiryReportType))
        End If
        Return ReportType
    End Function

    Protected Sub removeCertificateExpiryReportTypeViewState()
        ViewState.Remove(ViewStateConstants.certificateExpiryReportType)
    End Sub

    Protected Sub setCertificateExpiryReportTypeViewState(ByVal value As certificateExpiryReportType)
        ViewState(ViewStateConstants.certificateExpiryReportType) = value
    End Sub

#End Region

#End Region


#Region "Link Button Scheme Click"
    ''' <summary>
    ''' Link Button Scheme Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnScheme_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")
            Dim propertyId As String = commandParams(0)
            Response.Redirect(PathConstants.SchemeRecordDashboard + propertyId + "&requestType=scheme&src=search")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")
            Dim propertyId As String = commandParams(0)
            Response.Redirect(String.Format("../../Property/PropertyRecord.aspx?id={0}", propertyId))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "DDL Fuel Type Selected Index Change"

    Protected Sub ddlFuelType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            Dim fuelTypeSelectedValue = ddlFuelType.SelectedValue
            GetCertificatesNames(ddlCertificates, fuelTypeSelectedValue)
            If (fuelTypeSelectedValue = -1) Then
                GetFuelType(ddlFuelType, 0)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "DDL Certificate Name Selected Index Change"

    Protected Sub ddlCertificates_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCertificates.SelectedIndexChanged
        Try
            Dim certificateSelectedValue = ddlCertificates.SelectedValue
            GetFuelType(ddlFuelType, certificateSelectedValue)
            If (certificateSelectedValue = 0) Then
                GetCertificatesNames(ddlCertificates, -1)
            End If
            GetCertificateExpiryReportInfo()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

End Class