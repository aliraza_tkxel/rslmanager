﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="CertificateExpiry.aspx.vb" Async="true" Inherits="Appliances.CertificateExpiry" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table#ReportStats tr td a
        {
            color: Black;
        }
        .select_div select
        {
            width: 177px !important;
        }
        .text_div input
        {
            width: 170px !important;
        }
        .text_div
        {
            width: 244px;
        }
        .no-form-control
        {
            width: 188px;
        }
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
        .calendar-width {
            width: 200px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelApplianceDefectReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Certificate Expiry Report</span>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow: inherit; padding-bottom: 0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <div style="overflow: auto; padding-bottom: 10px">
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Fuel Type:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlFuelType" AutoPostBack="True" class="styleselect styleselect-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Patch:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlPatch" class="styleselect styleselect-control" runat="server"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPatch_Changed">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Property Type:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlPropertyType" class="styleselect styleselect-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    Scheme:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlScheme" class="styleselect styleselect-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="text_div">
                                <div class="label">
                                    UPRN:
                                </div>
                                <div class="field" style="margin-left: 32px;">
                                    <asp:TextBox ID="txtBoxUPRN" CssClass="styleselect styleselect-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    Stage:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlStage" class="styleselect styleselect-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    From:
                                </div>
                                <div class="field">
                                    <div class="input-area" style="padding-left: 0px !important">
                                        <asp:TextBox runat="server" ID="txtStartDate" Style="width: 150px"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender  runat="server" ID="clndrDocumentDate" TargetControlID="txtStartDate"
                                            PopupButtonID="imgDocumentDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                            Format="dd/MM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumentDate"
                                            Style="vertical-align: bottom; border: none;" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtStartDate"
                                            ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtStartDate"
                                            ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                            CssClass="Required" ValidationGroup="Doc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    To:
                                </div>
                                <div class="field">
                                    <div class="input-area" style="padding-left: 0px !important">
                                        <asp:TextBox runat="server" ID="txtEndDate" Style="width: 150px"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender  runat="server" ID="CalendarExtender1" TargetControlID="txtEndDate"
                                            PopupButtonID="imgDocumentEndDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                            Format="dd/MM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumentEndDate"
                                            Style="vertical-align: bottom; border: none;" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEndDate"
                                            ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtEndDate"
                                            ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                            CssClass="Required" ValidationGroup="Doc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    Cert Name:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlCertificates" AutoPostBack="True" class="styleselect styleselect-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="field right">
                            <asp:Button ID="btnSearch" class="btn btn-xs btn-blue right" Style="padding: 3px 23px !important;
                                margin: 10px 10px 0 0;" runat="server" Text="Search" OnClick="btnSearchClick" />
                        </div>
                    </div>
                    <hr />
                    <div id="ReportStats" style="overflow: auto;">
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnNoCertificate" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblNoCertificate" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;No Cert. Data
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnExpired" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblExpired" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Expired
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue1_2_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue1_2_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 2 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue2_4_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue1_4_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 2-4 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue4_8_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue4_8_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 4-8 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue8_12_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue8_12_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 8-12 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue12_16_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue12_16_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 12-16 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue16_52_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue16_52_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due in 16-52 weeks
                        </div>
                        <div class="no-form-control">
                            <strong style="float: left; text-decoration: underline; vertical-align: middle;">
                                <asp:LinkButton ID="lbtnDue52_Week" runat="server" Style="color: #000;">
                                    <asp:Label ID="lblDue52_Week" runat="server"></asp:Label>
                                </asp:LinkButton>
                            </strong>&nbsp;Due&gt;52 weeks&nbsp;
                        </div>
                        <div class="no-form-control">
                            <strong>Total Units: </strong><strong>
                                <%--<asp:LinkButton ID="lbtnTotal" runat="server">--%>
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                <%--</asp:LinkButton>--%>
                            </strong>
                        </div>
                    </div>
                    <hr />
                    <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                        <div style="padding: 10px; font-weight: bold; display: none;">
                            <asp:Label ID="lblCetificateExpiryReportHeading" Text="Total Units" runat="server" />
                        </div>
                        <cc1:PagingGridView ID="grdCertificateExpiry" runat="server" AutoGenerateColumns="False"
                            AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
                            CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" OnRowCreated="grdDevelopmentList_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Address" ItemStyle-CssClass="dashboard" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="lnkAddress" CommandArgument='<%# Eval("PropertyId")%>'
                                            OnClick="lnkBtnAddress_Click" Target="_blank">
                                            <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scheme" ItemStyle-CssClass="dashboard" SortExpression="SchemeName">
                                    <ItemTemplate>
                                        <%-- <asp:LinkButton runat="server" ID="lnkAddress" NavigateUrl='<%# String.Format ("~/../PropertyDataRestructure/Bridge.aspx?pg=schemerecord&id={0}{1}", Eval("PropertyID").ToString(),"&requestType=scheme&src=search") %>'
                                        --%>
                                        <asp:LinkButton runat="server" ID="lnkScheme" CommandArgument='<%# Eval("PropertyId")%>'
                                            OnClick="lnkBtnScheme_Click" Target="_blank">
                                            <asp:Label ID="lblTENANCYIDForScheme" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Block" ItemStyle-CssClass="dashboard" SortExpression="BlockName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTENANCYIDForBlock" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date" SortExpression="ExpiryDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Property Type" SortExpression="PTYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPType" runat="server" Text='<%# Bind("PTYPE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Property<br>Status" SortExpression="PropertyStatus">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDays" runat="server" Text='<%# Bind("PropertyStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="85px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset Type" SortExpression="ATYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAsset" runat="server" Text='<%# Bind("ATYPE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fuel Type" SortExpression="FTYPE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FTYPE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="85px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Stage" SortExpression="Stage">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Stage") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="140px" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <hr />
                                <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                                    Font-Bold="true" />
                            </EmptyDataTemplate>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                            <div class="paging-left">
                                <span style="padding-right: 10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                        CssClass="lnk-btn">
                                    &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                        CommandArgument="Prev" CssClass="lnk-btn">
                                    &lt;Prev
                                    </asp:LinkButton>
                                </span><span style="padding-right: 10px;"><b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. </span><span style="padding-right: 20px;">
                                        <b>Result:</b>
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        to
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        of
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </span><span style="padding-right: 10px;">
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                            CommandArgument="Next" CssClass="lnk-btn">
                                    
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                            CommandArgument="Last" CssClass="lnk-btn">
                                        
                                        </asp:LinkButton>
                                    </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                        PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                    UseSubmitBehavior="false" class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;"
                                    OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressApplianceDefectReport" runat="server" AssociatedUpdatePanelID="updPanelApplianceDefectReport"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
