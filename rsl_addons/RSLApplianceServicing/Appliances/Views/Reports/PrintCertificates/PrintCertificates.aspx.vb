﻿Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Imports System.Globalization
Imports AS_BusinessObject
Imports System.IO
Imports System.Threading
Imports System.Net.Mail
Imports System.Net.Mime

Public Class PrintCertificates
    Inherits PageBase

#Region "Attributes"

    Dim objReportBL As New ReportBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Issued", 1, 20)
    Dim totalCount As Integer = 0
    Dim objPropertyBl As New PropertyBL()

#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
            scriptManager.RegisterPostBackControl(Me.btnExport)
            If Not IsPostBack Then
                setPageSortBo(objPageSortBo)
                setDefaultValues()
                GetFuelType(ddlFuelType)
                GetCertificatesNames(ddlCertificates)
                populatePrintCertificatesReport()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Get ceritificate names"

    Private Sub GetCertificatesNames(ByVal ddl As DropDownList, Optional ByVal fuelTypeSelectedValue As Integer = -1)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not fuelTypeSelectedValue = -1) Then
            objPropertyBl.GetCertificates(resultDataSet, fuelTypeSelectedValue)
        Else
            objPropertyBl.GetCertificates(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.heatingTypeId
        ddl.DataTextField = ApplicationConstants.CertificateName
        ddl.DataBind()
        If (fuelTypeSelectedValue = -1) Then
            ddl.Items.Insert(0, New ListItem("All", 0))
        Else
            ddl.Items.Add(New ListItem("All", 0))
        End If
    End Sub
#End Region

#Region "Load Fuel Type"

    Private Sub GetFuelType(ByVal ddl As DropDownList, Optional ByVal certificateNameSelectedValue As Integer = 0)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        If (Not certificateNameSelectedValue = 0) Then
            objPropertyBl.GetFuel(resultDataSet, certificateNameSelectedValue)
        Else
            objPropertyBl.GetFuel(resultDataSet)
        End If
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        Dim itemToRemove As ListItem
        itemToRemove = ddl.Items.FindByValue("901")
        If itemToRemove IsNot Nothing Then
            ddl.Items.Remove(itemToRemove)
        End If

        If (certificateNameSelectedValue = 0) Then
            ddl.Items.Insert(0, New ListItem("All", -1))
        Else
            ddl.Items.Add(New ListItem("All", -1))
        End If
    End Sub
#End Region

#Region "DDL Fuel Type Selected Index Change"

    Protected Sub ddlFuelType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            Dim fuelTypeSelectedValue = ddlFuelType.SelectedValue
            GetCertificatesNames(ddlCertificates, fuelTypeSelectedValue)
            If (fuelTypeSelectedValue = -1) Then
                GetFuelType(ddlFuelType, 0)
            End If
            populatePrintCertificatesReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "DDL Certificate Name Selected Index Change"

    Protected Sub ddlCertificates_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCertificates.SelectedIndexChanged
        Try
            Dim certificateSelectedValue = ddlCertificates.SelectedValue
            GetFuelType(ddlFuelType, certificateSelectedValue)
            If (certificateSelectedValue = 0) Then
                GetCertificatesNames(ddlCertificates, -1)
            End If
            populatePrintCertificatesReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Submit button click"

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        Try
            Me.searchReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Search button click"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click

        Try
            Me.searchReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Export button click"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click

        Try
            Me.exportToExcel()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Print Certificate Report Sorting Event"

    Protected Sub grdPrintCertificate_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPrintCertificate.Sorting

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdPrintCertificate.PageIndex = 0
            objPageSortBo.setSortDirection()
            Me.setPageSortBo(objPageSortBo)

            Me.populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdPrintCertificate_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = Me.getPageSortBo()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            Dim objPageSortBo As PageSortBO = Me.getPageSortBo()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = Me.getPageSortBo()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBo(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                populatePrintCertificatesReport()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn IS Previous Click"

    Protected Sub lnkBtnISPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISPrevious.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber - 1
            Me.setPageSortBo(objPageSortBo)
            Me.populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS Next Click"

    Protected Sub lnkBtnISNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISNext.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber + 1
            Me.setPageSortBo(objPageSortBo)
            populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS First Click"

    Protected Sub lnkBtnISFirst_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISFirst.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = 1
            Me.setPageSortBo(objPageSortBo)
            populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lnk Btn IS Last Click"

    Protected Sub lnkBtnISLast_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISLast.Click

        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = Me.getTotalPageNumbers()
            Me.setPageSortBo(objPageSortBo)
            populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Hidden Checked Box Checked"

    Protected Sub chkHidden_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHidden.CheckedChanged

        Try
            Dim dataString As String = hidden.Value
            Dim dataArray As String() = dataString.Split(New Char() {","c})
            Dim lgsrID As Integer = Int32.Parse(dataArray.GetValue(0))
            ' ************************************************************************
            ' Changes
            ' This code will be used to add record to property activities table
            ' The record will be added to property activities that the certificate for this property
            ' has been printed.
            ' ************************************************************************
            Dim propertyID As String = dataArray.GetValue(1)
            Dim reqType As String = dataArray.GetValue(2)
            Dim isDocumentExist As Integer = dataArray.GetValue(3)
            Dim objPropertyActivityBO As New PropertyActivityBO()
            objPropertyActivityBO.PropertyId = propertyID
            objPropertyActivityBO.ActionDate = DateTime.Now
            objPropertyActivityBO.CreatedBy = SessionManager.getAppServicingUserId()
            objPropertyActivityBO.ReqTye = reqType
            ' ************************************************************************

            If (isDocumentExist = 1) Then
                objPropertyBl.savePrintCertificateActivity(objPropertyActivityBO)
                objReportBL.setCP12DocumentPrinted(lgsrID)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
            End If
            Me.populatePrintCertificatesReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Hidden Checked Box Email Checked"

    Protected Sub chkHiddenEmail_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHiddenEmail.CheckedChanged

        Try
            Dim dataString As String = hidden.Value
            Dim dataArray As String() = dataString.Split(New Char() {","c})
            Dim lgsrID As Integer = Int32.Parse(dataArray.GetValue(0))
            ' ************************************************************************
            ' This code will be used to add record to property activities table
            ' The record will be added to property activities that the certificate for this property
            ' has been printed.
            ' ************************************************************************
            Dim propertyID As String = dataArray.GetValue(1)
            Dim reqType As String = dataArray.GetValue(2)
            Dim isDocumentExist As Integer = dataArray.GetValue(3)
            Dim objPropertyActivityBO As New PropertyActivityBO()
            objPropertyActivityBO.PropertyId = propertyID
            objPropertyActivityBO.ActionDate = DateTime.Now
            objPropertyActivityBO.CreatedBy = SessionManager.getAppServicingUserId()
            objPropertyActivityBO.ReqTye = reqType
            ' ************************************************************************
            emailCertificate(lgsrID, propertyID)

            If (isDocumentExist = 1) Then
                objPropertyBl.savePrintCertificateActivity(objPropertyActivityBO)
                objReportBL.setCP12DocumentPrinted(lgsrID)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
            End If

            Me.populatePrintCertificatesReport()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Search Report"
    Public Sub searchReport()
        objPageSortBo = Me.getPageSortBo()

        objPageSortBo.PageNumber = 1
        grdPrintCertificate.PageIndex = 0
        Me.setPageSortBo(objPageSortBo)

        Me.populatePrintCertificatesReport()
    End Sub
#End Region

#Region "Set Default Values"

    Private Sub setDefaultValues()
        'Set from date to First day of current month
        txtBoxFromDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy")
        'Set to date to Last day of current month
        txtBoxToDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).ToString("dd/MM/yyyy")
        txtBoxQuickFind.Text = String.Empty
    End Sub

#End Region

#Region "Populate Print Certificate Report"

    Private Sub populatePrintCertificatesReport()

        Dim resultDS As New DataSet()
        Dim objPrintCertificateBO As New IssuedCertificateBO()

        objPageSortBo = getPageSortBo()

        Dim cultureInfo As New CultureInfo("en-GB")
        DateTime.TryParseExact(txtBoxFromDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objPrintCertificateBO.fromDate)
        DateTime.TryParseExact(txtBoxToDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objPrintCertificateBO.toDate)
        objPrintCertificateBO.searchText = Trim(txtBoxQuickFind.Text)
        objPrintCertificateBO.heatingTypeId = ddlFuelType.SelectedValue

        If objPrintCertificateBO.toDate < objPrintCertificateBO.fromDate Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.IncorrectDateInterval, True)
        Else
            totalCount = objReportBL.getPrintCertificateReport(objPrintCertificateBO, objPageSortBo, resultDS)

            Dim totalPages As Integer = Math.Ceiling(totalCount / objPageSortBo.PageSize)

            Me.setTotalPageNumbers(totalPages)

            Dim dtTimeSheet = resultDS.Tables(0)

            grdPrintCertificate.VirtualItemCount = totalCount
            grdPrintCertificate.DataSource = dtTimeSheet
            grdPrintCertificate.DataBind()

            If dtTimeSheet.Rows.Count = 0 Then
                pnlPagination.Visible = False
                pnlPaginationDummy.Visible = True
            Else
                pnlPagination.Visible = True
                pnlPaginationDummy.Visible = False
                setPagingLabels(totalCount)
                setNextPrevious(totalCount)
            End If

        End If

    End Sub

#End Region

#Region "Export to Excel"

    Private Sub exportToExcel()

        '------------------------------------------------------------------------------------------------------------
        'populate the grid for all records for given search criteria

        Dim resultDS As New DataSet()
        Dim objPrintCertificateBO As New IssuedCertificateBO()

        objPageSortBo = getPageSortBo()
        objPageSortBo.PageNumber = 1
        objPageSortBo.PageSize = 65000

        Dim cultureInfo As New CultureInfo("en-GB")
        DateTime.TryParseExact(txtBoxFromDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objPrintCertificateBO.fromDate)
        DateTime.TryParseExact(txtBoxToDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objPrintCertificateBO.toDate)
        objPrintCertificateBO.searchText = Trim(txtBoxQuickFind.Text)
        objPrintCertificateBO.heatingTypeId = ddlFuelType.SelectedValue

        totalCount = objReportBL.getPrintCertificateReport(objPrintCertificateBO, objPageSortBo, resultDS)

        Dim dtPrintCertificate = resultDS.Tables(0)

        grdPrintCertificateForXLS.PageSize = objPageSortBo.PageSize
        grdPrintCertificateForXLS.VirtualItemCount = totalCount
        grdPrintCertificateForXLS.DataSource = dtPrintCertificate
        grdPrintCertificateForXLS.GridLines = GridLines.Both
        grdPrintCertificateForXLS.DataBind()

        '------------------------------------------------------------------------------------------------------------

        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "PrintCertificateReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"

        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmlWrite As New HtmlTextWriter(strWrite)
        Dim htmlfrm As New HtmlForm()

        grdPrintCertificateForXLS.Parent.Controls.Add(htmlfrm)
        htmlfrm.Attributes("runat") = "server"
        htmlfrm.Controls.Add(grdPrintCertificateForXLS)
        htmlfrm.RenderControl(htmlWrite)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()

    End Sub

#End Region

#Region "Set Page Sort Bo"

    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub

#End Region

#Region "Get Page Sort Bo"

    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Set Total Page Numbers"

    Protected Sub setTotalPageNumbers(ByRef totalCount As Integer)
        ViewState(ViewStateConstants.TotalPageNumbers) = totalCount
    End Sub

#End Region

#Region "Get Total Page Numbers"

    Protected Function getTotalPageNumbers() As Integer
        Return CType(ViewState(ViewStateConstants.TotalPageNumbers), Integer)
    End Function

#End Region

#Region "set Paging Labels"

    Private Sub setPagingLabels(ByVal totalCount As Integer)
        objPageSortBo = Me.getPageSortBo()
        If totalCount = 0 Then
            Me.lblISRecords.Text = "0 to 0 of 0"
            Me.lblISPages.Text = "0 of 0"

        ElseIf (totalCount <= objPageSortBo.PageSize) Then
            Me.lblISRecords.Text = "1 to " + Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
            Me.lblISPages.Text = "1 of 1"
        ElseIf (totalCount > objPageSortBo.PageSize) Then

            If (objPageSortBo.PageNumber * objPageSortBo.PageSize) <= totalCount Then

                Me.lblISRecords.Text = Convert.ToString((objPageSortBo.PageSize * (objPageSortBo.PageNumber - 1)) + 1) + " to " + Convert.ToString(objPageSortBo.PageSize * objPageSortBo.PageNumber) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(Math.Ceiling(totalCount / objPageSortBo.PageSize))

            ElseIf (objPageSortBo.PageNumber * objPageSortBo.PageSize) > totalCount Then

                Me.lblISRecords.Text = Convert.ToString((objPageSortBo.PageSize * (objPageSortBo.PageNumber - 1)) + 1) + " to " + Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(objPageSortBo.PageNumber)

            End If
        End If

    End Sub

#End Region

#Region "Set Next Previous"

    Private Sub setNextPrevious(ByVal totalCount As Integer)
        objPageSortBo = Me.getPageSortBo()
        'if total records are less than page size
        If (totalCount <= objPageSortBo.PageSize) Then

            lnkBtnISPrevious.Enabled = False
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = False
            lnkBtnISLast.Enabled = False

        ElseIf (objPageSortBo.PageNumber = 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number = 1

            lnkBtnISPrevious.Enabled = False
            lnkBtnISNext.Enabled = True
            lnkBtnISFirst.Enabled = False
            lnkBtnISLast.Enabled = True

        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its last page 

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = False

        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its not last page 

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = True
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = True

        ElseIf (objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then

            'if its last page enable pervious link and disable next link

            lnkBtnISPrevious.Enabled = True
            lnkBtnISNext.Enabled = False
            lnkBtnISFirst.Enabled = True
            lnkBtnISLast.Enabled = False

        End If
    End Sub

#End Region

#Region "Email Certificate"

    Public Sub emailCertificate(ByVal lgsrId As Integer, ByVal propertyId As String)
        Try
            Dim tenantsDS As New DataSet
            objPropertyBl.getTenantsByPropertyId(tenantsDS, propertyId)

            If (tenantsDS.Tables.Count > 0 AndAlso tenantsDS.Tables(0).Rows.Count > 0) Then
                Dim documentDS As New DataSet()
                objPropertyBl.getCP12DocumentByLGSRID(documentDS, lgsrId)

                If documentDS.Tables.Count > 0 AndAlso documentDS.Tables(0).Rows.Count > 0 Then
                    With documentDS.Tables(0).Rows(0)

                        If Not IsDBNull(.Item("CP12DOCUMENT")) Then

                            Dim mailMessage As New MailMessage()

                            'Set email subject it is a simple string from UserMessageConstants
                            mailMessage.Subject = UserMessageConstants.LGSREmailSubject '"Your Current LGSR Certificate"

                            'Set Email body from Html file stored on server
                            Dim reader As StreamReader = New StreamReader(Server.MapPath("~/App_Data/LGSREmail.html"))
                            Dim body As String = String.Empty
                            body = reader.ReadToEnd.ToString
                            mailMessage.Body = body
                            mailMessage.IsBodyHtml = True

                            'Add on or multiple address(es), as email recipents.
                            For Each row As DataRow In tenantsDS.Tables(0).Rows
                                If (Not IsNothing(row.Item("Email")) AndAlso Validation.isEmail(row.Item("Email").ToString())) Then
                                    mailMessage.To.Add(New MailAddress(row.Item("Email"), row.Item("CustomerName")))
                                Else
                                    uiMessageHelper.IsError = True
                                    uiMessageHelper.Message = UserMessageConstants.UnableToSendEmailToTenant
                                End If
                            Next

                            'Get CP12 Document from database and set it content type as PDF and attach it with email.
                            Dim CP12DOCUMENT As [Byte]() = DirectCast(.Item("CP12DOCUMENT"), [Byte]())

                            Dim contentType As New ContentType(MediaTypeNames.Application.Pdf)

                            Dim attachment As New Attachment(New MemoryStream(CP12DOCUMENT), contentType)

                            Dim disposition = attachment.ContentDisposition
                            disposition.FileName = "CP12_Document_" + propertyId + "_" + DateTime.Now.ToString("dd/MM/yyyy")

                            mailMessage.Attachments.Add(attachment)

                            'Only call send email function if there is a valid to address.
                            If (mailMessage.To.Count > 0) Then

                                EmailHelper.sendEmail(mailMessage)

                                'Once email is sent sucessfully add an activity in property activities. 

                                Dim objPropertyActivityBO As New PropertyActivityBO()
                                objPropertyActivityBO.PropertyId = propertyId
                                objPropertyActivityBO.ActionDate = DateTime.Now
                                objPropertyActivityBO.CreatedBy = SessionManager.getAppServicingUserId()

                                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EmailSentSucessfully, False)

                                objPropertyBl.saveEmailCertificateActivity(objPropertyActivityBO)
                            End If

                        Else
                            uiMessageHelper.IsError = True
                            uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
                        End If

                    End With
                End If

            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.NoTenantFoundToEmail
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try
    End Sub

#End Region

#Region "hide Show Email Button"
    ''' <summary>
    ''' hide Show Notes Button
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowEmailButton(ByVal reqType As String)
        If (reqType = "Scheme" Or reqType = "Block") Then
            Return False
        ElseIf (reqType = "Property") Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Check Need to show CP12NUMBER hyperlink"

    Public Function IsShowCp12Number(ByRef isDocumentExist As Object)
        If (isDocumentExist = 0) Then
            Return 1
        Else
            Return 0
        End If
    End Function
#End Region

#Region "Cp12 Document Click"
    Protected Sub cp12Document_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mdlNoCp12Document.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close alert popup Click"

    Protected Sub imgBtnCancelAlertPopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancelAlertPopup.Click
        Try
            mdlNoCp12Document.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#End Region

End Class