﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="PrintCertificates.aspx.vb" Inherits="Appliances.PrintCertificates"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table#ReportStats tr td a
        {
            color: Black;
        }
        .searchbox
        {
            background-position: right center;
        }
        .text_div
        {
            width: 220px;
        }
        .text_div input
        {
            width: 130px !important;
        }
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
        .popupHeader
        {
            font-weight: bold;
            padding-left: 20px;
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">
        function showCP12Document(lgsrID, propertyID, reqType, isDocumentExist) {
            debugger;
            if (isDocumentExist == 1) {
                window.open("../../Common/PrintCP12Document.aspx?CP12DocumentID=" + lgsrID + "&ReqType=" + reqType);
            }
            var hiddenField = document.getElementById('<%= hidden.ClientID %>');
            hiddenField.value = lgsrID + "," + propertyID + "," + reqType + "," + isDocumentExist;
            var checkbox = document.getElementById('<%= chkHidden.ClientID %>');
            checkbox.checked = !(checkbox.checked);
        }               
    </script>
    <script type="text/javascript">
        function emailCP12Document(lgsrID, propertyID, reqType, isDocumentExist) {
            debugger;
            var hiddenField = document.getElementById('<%= hidden.ClientID %>');
            hiddenField.value = lgsrID + "," + propertyID + "," + reqType + "," + isDocumentExist;
            var checkbox = document.getElementById('<%= chkHiddenEmail.ClientID %>');
            checkbox.checked = !(checkbox.checked);
        }               
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelMain" runat="server">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Print Certificates</span>
                    <div class="field right" style="margin-top: -3px;">
                        <asp:CheckBox runat="server" Text="Hidden" ID="chkHidden" CssClass="aspNetHidden" />
                        <asp:CheckBox runat="server" Text="Hidden" ID="chkHiddenEmail" CssClass="aspNetHidden" />
                        <asp:HiddenField runat="server" ID="hidden" />
                        <asp:TextBox ID="txtBoxQuickFind" AutoPostBack="false" Style="padding: 3.4px 10px !important;
                            margin: 0 10px 0 0;" AutoCompleteType="Search" ToolTip="Search" class="searchbox styleselect-control searchbox right"
                            runat="server">
                        </asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtBoxWatermarkExtenderQuickFind" runat="server"
                            TargetControlID="txtBoxQuickFind" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <asp:Button ID="btnSearch" Text="Search" runat="server" UseSubmitBehavior="False"
                            CssClass="btn btn-xs btn-blue right" Style="padding: 3px 23px !important; margin: 0 10px 0 0;" />
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow: inherit; padding-bottom: 0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <div style="overflow: auto; padding-bottom: 10px">
                        <div class="form-control">
                            <div class="text_div">
                                <div class="label">
                                    <asp:Label ID="lblFrom" Text="From:" runat="server" />
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtBoxFromDate" runat="server" CssClass="styleselect styleselect-control"
                                        Enabled="false" />
                                    <asp:Image ID="imgFromDate" Style="margin-bottom: -5px;" runat="server" src="../../../Images/calendar.png" />
                                    <asp:CalendarExtender ID="calFromDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        TargetControlID="txtBoxFromDate" PopupButtonID="imgFromDate" PopupPosition="BottomLeft"
                                        TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="text_div">
                                <div class="label">
                                    <asp:Label ID="lblTo" Text="To:" runat="server" />
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtBoxToDate" runat="server" CssClass="styleselect styleselect-control"
                                        Enabled="false" />
                                    <asp:Image ID="imgToCal" Style="margin-bottom: -5px;" runat="server" src="../../../Images/calendar.png" />
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        TargetControlID="txtBoxToDate" PopupButtonID="imgToCal" PopupPosition="BottomLeft"
                                        TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    Fuel Type:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlFuelType" AutoPostBack="True" class="styleselect styleselect-control"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="margin-left: -2px;">
                            <div class="select_div">
                                <div class="label">
                                    Cert Names:
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlCertificates" AutoPostBack="True" class="styleselect styleselect-control"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="field right">
                            <asp:Button ID="btnSubmit" class="btn btn-xs btn-blue right" UseSubmitBehavior="False"
                                Style="padding: 3px 23px !important; margin: 10px 10px 0 0;" runat="server" Text="Submit" />
                        </div>
                    </div>
                    <hr />
                    <asp:UpdatePanel runat="server" ID="updPnlPrintCertificate">
                        <ContentTemplate>
                            <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                                <cc1:PagingGridView ID="grdPrintCertificate" runat="server" AutoGenerateColumns="False"
                                    AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
                                    CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                                    GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" OnRowCreated="grdPrintCertificate_RowCreated">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Ref:" ItemStyle-CssClass="dashboard" SortExpression="Ref">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" Width="90px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="125px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Block:" SortExpression="BlockName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="125px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scheme:" SortExpression="SchemeName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="125px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Certificate:" SortExpression="CP12NUMBER">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkCertificate" Visible='<%# EVAL("isDocumentExist") %>' runat="server"
                                                    NavigateUrl='<%# String.Format ("../../Common/Download.aspx?CP12DocumentID={0}", EVAL("Certificate").ToString()) %>'
                                                    Target="_blank" Font-Underline="True" ForeColor="Black">
                                                    <asp:Label runat="server" ID="lblCertificate" Text='<%# EVAL("Certificate") %>'></asp:Label>
                                                </asp:HyperLink>
                                                <asp:LinkButton Style="color: black" Visible='<%# IsShowCp12Number(EVAL("isDocumentExist")) %>'
                                                    runat="server" ID="Label1" Text='<%# EVAL("Certificate") %>' OnClick="cp12Document_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Fuel Type:" SortExpression="FuelType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFuelType" runat="server" Text='<%# Bind("FuelType") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Issued:" SortExpression="Issued">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIssued" runat="server" Text='<%# Bind("Issued") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="Left" Width="85px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="By:" SortExpression="By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBy" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="Left" Width="105px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:Panel ID="pnlEmailButton" runat="server" HorizontalAlign="Right">
                                                    <asp:Button runat="server" ID="btnEmail" Text="Email" UseSubmitBehavior="False" OnClientClick='<%# String.Format ("emailCP12Document({0},{1}{2}{1},{1}{3}{1},{1}{4}{1});", EVAL("Certificate").ToString(),"&#39;",EVAL("Ref").ToString(),EVAL("ReqType").ToString(),EVAL("isDocumentExist")) %>'
                                                        CssClass="btn btn-xs btn-blue right" Style="padding: 3px 10px !important; margin: 0 10px 0 0;"
                                                        Visible='<%# hideShowEmailButton(Eval("ReqType").ToString()) %>' />
                                                </asp:Panel>
                                                <asp:Panel ID="pnlPrintButton" runat="server" HorizontalAlign="Right">
                                                    <asp:Button ID="btnPrint" Text="Print Certificate" runat="server" UseSubmitBehavior="False"
                                                        OnClientClick='<%# String.Format ("showCP12Document({0},{1}{2}{1},{1}{3}{1},{1}{4}{1});", EVAL("Certificate").ToString(),"&#39;",EVAL("Ref").ToString(),EVAL("ReqType").ToString(),EVAL("isDocumentExist")) %>'
                                                        CssClass="btn btn-xs btn-blue right" Style="padding: 3px 10px !important; margin: 0 10px 0 0;" />
                                                </asp:Panel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="195px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <hr />
                                        <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                                            Font-Bold="true" />
                                    </EmptyDataTemplate>
                                    <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                </cc1:PagingGridView>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                                        margin: 0 auto; width: 98%;">
                                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                                            <div class="paging-left">
                                                <span style="padding-right: 10px;">
                                                    <asp:LinkButton ID="lnkBtnISFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                                        CssClass="lnk-btn">
                                                        &lt;&lt;First
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnISPrevious" Text="Prev" runat="server" CommandName="Page"
                                                        CommandArgument="Prev" CssClass="lnk-btn">
                                                        &lt;Prev
                                                    </asp:LinkButton>
                                                </span><span style="padding-right: 10px;"><b>Page:</b>
                                                    <asp:Label ID="lblISPages" runat="server">0 of 0</asp:Label>. </span><span style="padding-right: 20px;">
                                                        <b>Result:</b>
                                                        <asp:Label ID="lblISRecords" runat="server">0 to 0 of 0</asp:Label>
                                                    </span><span style="padding-right: 10px;">
                                                        <asp:LinkButton ID="lnkBtnISNext" Text="Next>" runat="server" CommandName="Page"
                                                            CommandArgument="Next" CssClass="lnk-btn">
                                    
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lnkBtnISLast" Text="Last>>" runat="server" CommandName="Page"
                                                            CommandArgument="Last" CssClass="lnk-btn">
                                        
                                                        </asp:LinkButton>
                                                    </span>
                                            </div>
                                            <div style="float: right;">
                                                <span>
                                                    <asp:Button ID="btnExport" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                                                        class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;" />
                                                </span>
                                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                                <div class="field" style="margin-right: 10px;">
                                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                                        PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                                </div>
                                                <span>
                                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                                        UseSubmitBehavior="false" class="btn btn-xs btn-blue" Style="padding: 1px 5px !important;
                                                        margin-right: 10px;" OnClick="changePageNumber" />
                                                </span>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlPaginationDummy" Visible="false">
                                        <asp:Label ID="lblSpace6" runat="server">&nbsp;</asp:Label>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressMain" runat="server" AssociatedUpdatePanelID="updPanelMain"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="UpdatePanel2" Visible="False">
        <ContentTemplate>
            <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                <cc1:PagingGridView ID="grdPrintCertificateForXLS" GridLines="Both" runat="server"
                    AutoGenerateColumns="False" AllowSorting="True" Width="100%" BorderWidth="1px"
                    EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Ref" ItemStyle-CssClass="dashboard">
                            <ItemTemplate>
                                <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="140px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Block">
                            <ItemTemplate>
                                <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheme">
                            <ItemTemplate>
                                <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Certificate">
                            <ItemTemplate>
                                <asp:Label ID="lblCertificate" runat="server" Text='<%# EVAL("Certificate") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="140px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fuel Type">
                            <ItemTemplate>
                                <asp:Label ID="lblFuelType" runat="server" Text='<%# Bind("FuelType") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="110px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Issued">
                            <ItemTemplate>
                                <asp:Label ID="lblIssued" runat="server" Text='<%# Bind("Issued") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="140px" BorderStyle="Solid" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="By">
                            <ItemTemplate>
                                <asp:Label ID="lblBy" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" BorderStyle="Solid" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" BorderStyle="Solid" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <hr />
                        <asp:Label ID="lblEmptyItemTemplate" Text="No Records Found." runat="server" ForeColor="Red"
                            Font-Bold="true" />
                    </EmptyDataTemplate>
                    <HeaderStyle BorderColor="Black" HorizontalAlign="Center" />
                </cc1:PagingGridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlNoCp12Document" runat="server">
                <asp:ImageButton ID="imgBtnCancelAlertPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    OnClick="imgBtnCancelAlertPopup_Click" BorderWidth="0" />
                <div class="apt-success-popup" style="width: 250px !Important; height: 150px; border: 1px solid black;">
                    <div class="popupHeader">
                        <strong>Alert</strong>
                        <br />
                        <br />
                    </div>
                    <hr />
                    <p style="padding-left: 15px; padding-top: 8px;">
                        There is no document available.
                    </p>
                </div>
            </asp:Panel>
            <asp:Label ID="lblNoCp12DocumentPopUp" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlNoCp12Document" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblNoCp12DocumentPopUp" PopupControlID="pnlNoCp12Document"
                DropShadow="true" CancelControlID="lblNoCp12DocumentPopUp" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
