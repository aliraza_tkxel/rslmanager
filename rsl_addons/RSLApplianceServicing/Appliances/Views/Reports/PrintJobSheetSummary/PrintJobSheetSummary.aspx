﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master"
    CodeBehind="PrintJobSheetSummary.aspx.vb" Inherits="Appliances.PrintJobSheetSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function Clickheretoprint() {
            self.print();

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopupJobSheet" Visible="True">
        <div style="height: auto; clear: both;">
            <table id="tbl_header" style="width: 100%; line-height: 20px;" cellpadding="0px"
                cellspacing="0px">
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr style="height: 30px;">
                    <td colspan="2" style="border-bottom: 1px solid #7F7F7F;">
                        <b>Summary </b>
                    </td>
                    <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                        border-left: 1px solid #7F7F7F;">
                    </td>
                    <td style="width: 20%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                        <b>JS:</b>&nbsp;
                        <asp:Label ID="lblJournalId" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td style="width: 15%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                        border-right: 1px solid #7F7F7F;">
                        <div runat="server" id="divCP12Expiry">
                            <b>CP12Expiry:</b>&nbsp;
                            <asp:Label ID="lblCp12Expiry" runat="server" Text=""></asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
            <table id="Table1" style="width: 100%; margin-top: 0px; line-height: 18px;">
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b>PO Number:</b>&nbsp;
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b>Status:</b>&nbsp;
                    </td>
                    <td align="left" style="width: 40%;">
                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Contractor:</b>&nbsp;
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b>Budget:</b>&nbsp;
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Contact:</b>&nbsp;
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b>Estimate:</b>&nbsp;
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 20%;">
                        <b>Works Required:</b>
                    </td>
                    <td style="width: 20%;">
                        <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 230px;
                            height: 40px;" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="hdnFieldPurchaseOrderItemId" runat="server" />
                    </td>
                    <td style="width: 10%;">
                        <b>NET Cost:</b>&nbsp;
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Ordered By:</b>&nbsp;
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b>VAT:</b>&nbsp;
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Ordered:</b>&nbsp;
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 10%;">
                        <b>Total:</b>&nbsp;
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 30%;">
                    </td>
                    <td style="width: 10%;">
                        <b>Order Total:&nbsp;</b>
                    </td>
                    <td style="width: 40%;">
                        <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr style="vertical-align: middle;">
                    <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="2" style="width: 40%;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 32%;">
                                    <b>Scheme:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32%;">
                                    <b>Address:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32%;">
                                    <b>Postcode:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="2" style="width: 50%;">
                        <div id="divBoilderInfo" runat="server" style="max-height: 170px; width: 350px; overflow: auto;"
                            visible="False">
                            <asp:Repeater ID="boilerInfoRpt" runat="server">
                                <HeaderTemplate>
                                    <table style='font-size: 15px; width: 100%;'>
                                        <tr>
                                            <td>
                                                <b>Boiler<br/>Name</b>
                                            </td>
                                            <td>
                                                <b>Heating<br/>Fuel</b>
                                            </td>
                                            <td>
                                                <b>CP12<br/>Expiry</b>
                                            </td>
                                            <td>
                                                <b>CP12<br/>Issued</b>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="Label1" Text='<%# Eval("BoilerName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="Label10" Text='<%# Eval("HeatingFuel") %>' />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="Label11" Text='<%# Eval("CP12Expiry") %>' />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="Label12" Text='<%# Eval("CP12Issued") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <table style="width: 100%" runat="server" id="tblCustomerInfo">
                            <tr>
                                <td style="width: 32%;">
                                    <b>Customer:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32%;">
                                    <b>Telephone:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32%;">
                                    <b>Mobile:&nbsp;</b>
                                </td>
                                <td style="width: 68%;">
                                    <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">
                                    <b>Email:&nbsp;</b>
                                </td>
                                <td style="width: 63%;">
                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 35%;">
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr style="vertical-align: middle;">
                    <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="vertical-align: top;">
                        <b>Asbestos:</b>
                    </td>
                    <td colspan="3">
                        <div style="height: 130px; width: 400px; overflow: auto;">
                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                BorderStyle="None" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="AsbRiskID" />
                                    <asp:BoundField DataField="Description">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td colspan="4">
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
