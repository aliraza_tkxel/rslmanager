﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessObject



Public Class PrintJobSheetSummary
    Inherits PageBase


#Region "Data Members"
    Dim objJobSheetBL As New ReportBL
    Dim jSNClass As String = String.Empty
    Dim appointmentType As String = String.Empty
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If uiMessageHelper.IsError = False Then
                    Me.populateJobSheetSummary(jSNClass, appointmentType)
                End If
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "Clickheretoprint()", True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString("JobSheetNumber")) Then
            jSNClass = Request.QueryString("JobSheetNumber").ToString
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.InvalidJobSheetNumber
        End If
        If Not IsNothing(Request.QueryString("appointmentType")) Then
            appointmentType = Request.QueryString("appointmentType").ToString
        End If
    End Sub

    ''' <summary>
    ''' Get job sheet summary data on the basis of selected JSN no
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Private Sub populateJobSheetSummary(ByRef jsn As String, ByRef appointmentType As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objJobSheetBL As ReportBL = New ReportBL
        divBoilderInfo.Visible = False

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.JobSheetSummaryBoilerInfoTable)

        If (appointmentType = "Block" Or appointmentType = "Scheme") Then
            objJobSheetBL.getJobSheetSummaryForSchemeBlockByJournalId(dsJobSheetSummary, jsn)

        Else
            objJobSheetBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)
        End If

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)
                lblJournalId.Text = .Item("Ref").ToString().Trim()
                lblStatus.Text = .Item("Status")
                lblContractor.Text = .Item("Contractor")
                lblContact.Text = .Item("Contact")
                txtWorksRequired.Text = .Item("WorkRequired")
                If (Not IsDBNull(.Item("CP12Expiry"))) Then
                    lblCp12Expiry.Text = .Item("CP12Expiry")
                Else
                    lblCp12Expiry.Text = ""
                End If
                lblPONo.Text = .Item("PoNumber")
                lblOrderedBy.Text = .Item("AssignedBy")
                lblOrdered.Text = .Item("AssignedDate")

                lblBudget.Text = String.Format("{0:C}", .Item("BUDGET"))
                lblEstimate.Text = ("£" + .Item("Estimate"))
                lblNetCost.Text = String.Format("{0:C}", .Item("NetCost"))
                lblVat.Text = String.Format("{0:C}", .Item("Vat"))

                lblTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))
                lblOrderTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))

                lblScheme.Text = .Item("SCHEMENAME")
                lblPostCode.Text = .Item("PostCode")
                lblHouseNumber.Text = .Item("Address")

                hdnFieldPurchaseOrderItemId.Value = .Item("PurchaseOrderItemId")
                '+ ", " + .Item("HOUSEADDRESS1") + ", " + .Item("HOUSEADDRESS2") + ", " + .Item("HOUSEADDRESS3")
                'lblAddress1.Text = .Item("HOUSEADDRESS1")
                'lblAddress2.Text = .Item("HOUSEADDRESS2")
                'lblAddress3.Text = .Item("HOUSEADDRESS3")

                lblCustomer.Text = .Item("ClientName")
                lblTelephone.Text = .Item("ClientTel")
                lblMobile.Text = .Item("ClientMobile")
                lblEmail.Text = .Item("ClientEmail")

            End With
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.JobSheetSummaryBoilerInfoTable).Rows.Count > 0) Then
            tblCustomerInfo.Visible = False
            divCP12Expiry.Visible = False
            divBoilderInfo.Visible = True
            boilerInfoRpt.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.JobSheetSummaryBoilerInfoTable)
            boilerInfoRpt.DataBind()
        End If

    End Sub


End Class