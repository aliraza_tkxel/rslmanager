﻿Imports AS_BusinessObject
Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Net.Mail
Imports System.Net
Imports System.IO


Public Class AssignedToContractor
    Inherits PageBase





#Region "Properties / Class level Attributes"

    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
#End Region

#Region "Events"

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                populateAssignedToContractorList()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Grid View Events"

#Region "grdcontractorList Sorting"

    Protected Sub grdcontractorList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdcontractorList.Sorting
        Try
            'Get PageSortBO from View State
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            'setting pageindex of gridview equal to zero for new search
            grdcontractorList.PageIndex = 0

            'setting new sorting expression and order
            objPageSortBo.PageNumber = 1

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.setSortDirection()

            setPageSortBoViewState(objPageSortBo)

            'populating gridview with new results
            populateAssignedToContractorList(getSearchTextViewState)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

    Protected Sub cancelSelectedAppointment(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim objCancelAppointment = New AppointmentCancellationBO()
            Dim imgBtn As Button = CType(sender, Button)
            Dim journalId As String = imgBtn.CommandArgument.ToString()
            objCancelAppointment.FaultsList = journalId.Remove(0, 2)
            SessionManager.removeConfirmedGasAppointment()
            SessionManager.setConfirmedGasAppointment(objCancelAppointment)
            mdlPopUpCancelFaults.Show()
            If txtBoxDescription.Enabled = False Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#Region "Button Click View Button Grid View"

    Protected Sub grdContractorList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdcontractorList.SelectedIndexChanged
        Try
            Dim btnView As Button = CType(sender, Button)
            Dim commandParams() As String = CType(btnView.CommandArgument, String).Split(";")
            Dim jsn As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)
            hdnAppointmentType.Value = appointmentType
            Dim contractorID As Integer = Integer.Parse(commandParams(2))

            ViewStateConstants.CurrentIndexForAssignGasServicingToContractor = "1"
            Dim a As Integer = Integer.Parse(jsn.Remove(0, 2))

            Me.resetJobSheetControls()
            Me.populateJobSheetSummary(Integer.Parse(jsn.Remove(0, 2)), appointmentType)
            Me.SetJobSheetSummary()

            Me.pnlJobSheetSummary.Visible = True
            Me.mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Grid View Custom Pager Events"

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)

            populateAssignedToContractorList(search)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                populateAssignedToContractorList(search)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

    Protected Sub grdcontractorList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#End Region

#Region "Search Box Text Changed"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged
        Try
            Dim objPageSorterBo As PageSortBO = getPageSortBoViewState()
            objPageSorterBo.PageNumber = 1
            grdcontractorList.PageIndex = 0
            populateAssignedToContractorList(txtSearch.Text.Trim)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

    Private Sub populateAssignedToContractorList(Optional ByRef searchText As String = "")
        Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
        Dim objReportsBl As New ReportBL

        Dim totalCount As Integer

        Dim resultDataSet As New DataSet

        setSearchTextViewState(searchText)

        totalCount = objReportsBl.getAssignedToContractorReport(resultDataSet, objPageSortBo, searchText)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        grdcontractorList.VirtualItemCount = totalCount
        grdcontractorList.DataSource = resultDataSet
        grdcontractorList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdcontractorList.PageCount Then
            setResultDataSetViewState(resultDataSet)
        End If
    End Sub

#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO As PageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)

        If IsNothing(pageSortBO) Then
            pageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Search Text Set/Get/Remove"

    Protected Sub setSearchTextViewState(ByRef searchText As String)
        ViewState.Item(ViewStateConstants.Search) = searchText
    End Sub

    Protected Function getSearchTextViewState() As String
        Dim searchText As String = ViewState.Item(ViewStateConstants.Search)
        If IsNothing(searchText) Then
            searchText = String.Empty
        End If
        Return searchText
    End Function

    Protected Sub removeSearchTextViewState()
        ViewState.Remove(ViewStateConstants.Search)
    End Sub

#End Region

#End Region



#Region " View Job Sheet "



    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try

            If btnUpdate.Text = "Update" Then
                btnUpdate.Text = "Save"

                txtWorksRequired.ReadOnly = False


                mdlPopupJobSheetSummarySubContractorUpdate.Show()

            ElseIf btnUpdate.Text = "Save" Then
                pnlJobSheetSummary.Visible = True
                AppointmentJobSheetStatusUpdate()
                mdlPopupJobSheetSummarySubContractorUpdate.Hide()
                populateAssignedToContractorList()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Get job sheet summary data on the basis of selected JSN no
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Private Sub populateJobSheetSummary(ByRef jsn As Integer, ByRef appointmentType As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objReportBL As ReportBL = New ReportBL()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.JobSheetSummaryBoilerInfoTable)

        If (appointmentType = "Block" Or appointmentType = "Scheme") Then
            objReportBL.getJobSheetSummaryForSchemeBlockByJournalId(dsJobSheetSummary, jsn)

        Else
            objReportBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)
        End If

        SessionManager.setJobSheetSummaryDs(dsJobSheetSummary)

    End Sub
    Private Sub SetJobSheetSummary()
        Dim dsJobSheetSummary As New DataSet()
        Dim currentRow, totalRows As Integer

        dsJobSheetSummary = SessionManager.getJobSheetSummaryDs()

        currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndexForAssignGasServicingToContractor)


        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            totalRows = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count
            lblTotalSheets.Text = "Page " + currentRow.ToString() + " of " + totalRows.ToString()

            If (currentRow = 1 And currentRow = totalRows) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = False
            ElseIf (currentRow = 1) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = True
            ElseIf (currentRow = totalRows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False
            ElseIf (currentRow > 1 And currentRow < totalRows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
            'If (currentRow = 1 And currentRow = totalRows) Then
            '    btnPrevious.Enabled = False
            '    btnNext.Enabled = False
            'ElseIf (currentRow > 1 And currentRow < totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = True
            'ElseIf (currentRow > 1 And currentRow < totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = True
            'ElseIf (currentRow + 1 = totalRows) Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = False
            'End If

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentRow - 1)
                lblJournalId.Text = .Item("Ref").ToString().Trim()
                'lblPlannedWorkComponent.Text = .Item("COMPONENTNAME")
                ' lblTrade.Text = .Item("Trade")
                lblStatus.Text = .Item("Status")
                lblContractor.Text = .Item("Contractor")
                lblContact.Text = .Item("Contact")
                txtWorksRequired.Text = .Item("WorkRequired")
                If (Not IsDBNull(.Item("CP12Expiry"))) Then
                    lblCp12Expiry.Text = .Item("CP12Expiry")
                Else
                    lblCp12Expiry.Text = ""
                End If
                'lblCp12Expiry.Text = .Item("CP12Expiry")
                lblPONo.Text = .Item("PoNumber")
                lblOrderedBy.Text = .Item("AssignedBy")
                lblOrdered.Text = .Item("AssignedDate")

                lblBudget.Text = String.Format("{0:C}", .Item("BUDGET"))
                lblEstimate.Text = ("£" + .Item("Estimate"))
                lblNetCost.Text = String.Format("{0:C}", .Item("NetCost"))
                lblVat.Text = String.Format("{0:C}", .Item("Vat"))

                lblTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))
                lblOrderTotal.Text = String.Format("{0:C}", (.Item("NetCost") + .Item("Vat")))

                lblScheme.Text = .Item("SCHEMENAME").ToString()
                lblPostCode.Text = .Item("PostCode")
                lblHouseNumber.Text = .Item("Address")

                hdnFieldPurchaseOrderItemId.Value = .Item("PurchaseOrderItemId")
                '+ ", " + .Item("HOUSEADDRESS1") + ", " + .Item("HOUSEADDRESS2") + ", " + .Item("HOUSEADDRESS3")
                'lblAddress1.Text = .Item("HOUSEADDRESS1")
                'lblAddress2.Text = .Item("HOUSEADDRESS2")
                'lblAddress3.Text = .Item("HOUSEADDRESS3")

                lblCustomer.Text = .Item("ClientName")
                lblTelephone.Text = .Item("ClientTel")
                lblMobile.Text = .Item("ClientMobile")
                lblEmail.Text = .Item("ClientEmail")

                'MA' ViewState(ViewStateConstants.CustomerId) = .Item("CustomerId")
            End With
            'If lblStatus.Text.ToUpper() = "complete".ToUpper() Then
            '    btnUpdate.Enabled = False
            'Else
            '    btnUpdate.Enabled = True
            'End If
        End If

        divCP12Expiry.Visible = True
        divBoilderInfo.Visible = False
        divCustomerInfo.Visible = True
        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If

        'Bind Boiler Info with data from Database for ContractorID
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.JobSheetSummaryBoilerInfoTable).Rows.Count > 0) Then
            divCustomerInfo.Visible = False
            divCP12Expiry.Visible = False
            divBoilderInfo.Visible = True
            boilerInfoRpt.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.JobSheetSummaryBoilerInfoTable)
            boilerInfoRpt.DataBind()
        End If


    End Sub

    ''' <summary>
    '''  Reset job sheet controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub resetJobSheetControls()
        lblJournalId.Text = String.Empty
        lblCp12Expiry.Text = String.Empty
        lblStatus.Text = String.Empty
        lblStatus.Visible = True
        lblContractor.Text = String.Empty
        lblContact.Text = String.Empty
        txtWorksRequired.Text = String.Empty

        lblPONo.Text = String.Empty
        lblOrderedBy.Text = String.Empty
        lblOrdered.Text = String.Empty
        hdnFieldPurchaseOrderItemId.Value = 0
        lblBudget.Text = String.Empty
        lblEstimate.Text = String.Empty
        lblVat.Text = String.Empty
        lblNetCost.Text = String.Empty

        lblScheme.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblHouseNumber.Text = String.Empty
        'lblAddress1.Text = String.Empty
        'lblAddress2.Text = String.Empty
        'lblAddress3.Text = String.Empty

        lblCustomer.Text = String.Empty
        lblTelephone.Text = String.Empty
        lblMobile.Text = String.Empty
        lblEmail.Text = String.Empty

        btnUpdate.Enabled = True
        btnUpdate.Text = "Update"

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()
    End Sub

    Private Function AppointmentJobSheetStatusUpdate() As Integer
        Try
            Dim objJobSheetBO As New JobSheetBO
            Dim objReportBL As New ReportBL
            uiMessageHelper.IsError = False
            ' Dim journalId As Integer = Integer.Parse((lblJournalId.Text).Remove(0, 2))

            objJobSheetBO.AppointmentJSNId = Integer.Parse((lblJournalId.Text))
            objJobSheetBO.PurchaseOrderItemId = hdnFieldPurchaseOrderItemId.Value
            objJobSheetBO.WorkRequired = txtWorksRequired.Text
            objJobSheetBO.UserID = SessionManager.getUserEmployeeId()
            objJobSheetBO.OrderId = lblPONo.Text

            objReportBL.setAppointmentJobSheetStatusUpdate(objJobSheetBO)

            If objJobSheetBO.IsFlagStatus Then
                uiMessageHelper.IsError = False
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = objJobSheetBO.UserMsg
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message


            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            End If
        End Try

        Return 1

    End Function

    Private Sub updatePlannedAppointmentStatus()


        Try
            If btnUpdate.Text = "Update" Then
                btnUpdate.Text = "Save"
                ShowOrHideDetailsOnStatusBasis()

            ElseIf btnUpdate.Text = "Save" Then
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

    Private Sub ShowOrHideDetailsOnStatusBasis()
        Try
            btnUpdate.Enabled = True
            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub





    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndexForAssignGasServicingToContractor)
            currentRow = currentRow - 1
            ViewStateConstants.CurrentIndexForAssignGasServicingToContractor = currentRow.ToString()
            SetJobSheetSummary()
            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndexForAssignGasServicingToContractor)
            currentRow = currentRow + 1
            ViewStateConstants.CurrentIndexForAssignGasServicingToContractor = currentRow.ToString()
            SetJobSheetSummary()
            mdlPopupJobSheetSummarySubContractorUpdate.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            cancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Cancel Faults"

    Sub cancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As AppointmentCancellationBO = New AppointmentCancellationBO()
            resultDataSet = SessionManager.getConfirmedGasAppointment()

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True

                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim faultsList As StringBuilder = New StringBuilder()
                faultsList.Append(resultDataSet.FaultsList.ToString())
                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text
                objAppointmentCancellationBO.userId = SessionManager.getAppServicingUserId()
                Dim purchaseOrderId? As Integer = objSchedulingBL.saveGasServicingCancellationforContractors(objAppointmentCancellationBO)
                If purchaseOrderId = 0 Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False

                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    Me.sendCancellationEmailToContractor(CInt(faultsList.ToString()), purchaseOrderId)
                    Me.sendCancellationEmailToOriginator(CInt(faultsList.ToString()), purchaseOrderId)

                    SessionManager.removeConfirmedGasAppointment()
                    mdlPopUpCancelFaults.Hide()
                    populateAssignedToContractorList()
                    uiMessageHelper.Message = UserMessageConstants.FaultCancelled
                    'pushAppointmnetCancellation()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, False)
                End If

            End If

        End If

    End Sub

#End Region

#Region "Send Cancellation Email to Contractor"
    Sub sendCancellationEmailToContractor(ByVal journalId As Integer, ByVal purchaseOrderId As Integer)
        Dim detailsForEmailDS As New DataSet()
        objSchedulingBL.getEmailDetailForContractor(journalId, purchaseOrderId, detailsForEmailDS)
        Dim socket
        If Request.ServerVariables("HTTPS") = "on" Then
            socket = "https"
        Else
            socket = "http"
        End If
        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then

                If (DBNull.Value.Equals(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email"))) Then
                    Throw New Exception("email is not sent as email id of contractor is not available.")
                End If


                If String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) _
                    OrElse Not isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) Then

                    Throw New Exception("Unable to send email, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/CancelPurchaseOrder.html"))
                    body.Append(reader.ReadToEnd())

                    ' Set contractor detail(s) '
                    '==========================================='
                    'Populate Contractor Contact Name
                    body.Replace("{ContractorName}", detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName"))
                    '==========================================='
                    ' Populate work, estimate and cost details
                    '==========================================='

                    'Set Order Id 
                    body.Replace("{JSNumber}", journalId)

                    ' Set JSN (Reactive Repair work reference)
                    body.Replace("{PONumber}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("POID"))


                    body.Replace("{Address}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("FullAddress"))

                    body.Replace("{Block}", detailsForEmailDS.Tables(ApplicationConstants.BlockDetailsdt)(0)("BlockName"))

                    body.Replace("{TownCity}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("TOWNCITY"))

                    body.Replace("{Postcode}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("POSTCODE"))

                    body.Replace("{WorkRequired}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("WorkRequired"))

                    'Get sum/total of net cost from works required data table.
                    body.Replace("{NetCost}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("NetCost").ToString)

                    'Get sum/total of Vat from works required data table.
                    body.Replace("{VAT}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("VAT").ToString())

                    'Get sum/total of Gross/Total from works required data table.
                    body.Replace("{Total}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("Gross").ToString())

                    '==========================================='
                    ' There is an asbestos present at the below property. Please contact Broad land Housing for details.
                    ' Attach logo images with email
                    '==========================================='


                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)



                    'Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/PropertyModule/broadland.png"))
                    'logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    'body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    'Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")
                    'Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    'alternatevw.LinkedResources.Add(logoBroadLandRepairs)

                    '==========================================='

                    Dim mailMessage As New MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString, detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName").ToString))

                    ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub
#End Region

#Region "Send Cancellation Email to Originator"
    Sub sendCancellationEmailToOriginator(ByVal journalId As Integer, ByVal purchaseOrderId As Integer)
        Dim detailsForEmailDS As New DataSet()
        objSchedulingBL.getEmailDetailForOriginator(journalId, purchaseOrderId, detailsForEmailDS)
        Dim socket
        If Request.ServerVariables("HTTPS") = "on" Then
            socket = "https"
        Else
            socket = "http"
        End If
        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then

                If (DBNull.Value.Equals(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email"))) Then
                    Throw New Exception("email is not sent as email id of contractor is not available.")
                End If


                If String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) _
                    OrElse Not isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) Then

                    Throw New Exception("Unable to send email, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/CancelPurchaseOrder.html"))
                    body.Append(reader.ReadToEnd())

                    ' Set contractor detail(s) '
                    '==========================================='
                    'Populate Contractor Contact Name
                    body.Replace("{ContractorName}", detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName"))
                    '==========================================='
                    ' Populate work, estimate and cost details
                    '==========================================='

                    'Set Order Id 
                    body.Replace("{JSNumber}", journalId)

                    ' Set JSN (Reactive Repair work reference)
                    body.Replace("{PONumber}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("POID"))


                    body.Replace("{Address}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("FullAddress"))

                    body.Replace("{Block}", detailsForEmailDS.Tables(ApplicationConstants.BlockDetailsdt)(0)("BlockName"))

                    body.Replace("{TownCity}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("TOWNCITY"))

                    body.Replace("{Postcode}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("POSTCODE"))

                    body.Replace("{WorkRequired}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("WorkRequired"))

                    'Get sum/total of net cost from works required data table.
                    body.Replace("{NetCost}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("NetCost").ToString)

                    'Get sum/total of Vat from works required data table.
                    body.Replace("{VAT}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("VAT").ToString())

                    'Get sum/total of Gross/Total from works required data table.
                    body.Replace("{Total}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("Gross").ToString())

                    '==========================================='
                    ' There is an asbestos present at the below property. Please contact Broad land Housing for details.
                    ' Attach logo images with email
                    '==========================================='


                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)



                    'Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/PropertyModule/broadland.png"))
                    'logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    'body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    'Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")
                    'Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    'alternatevw.LinkedResources.Add(logoBroadLandRepairs)

                    '==========================================='

                    Dim mailMessage As New MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString, detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName").ToString))

                    ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub
#End Region

#Region "Is Email"
    Public Shared Function isEmail(ByVal value As String) As Boolean
        Dim rgx As New Regex(RegularExpConstants.emailExp)
        If rgx.IsMatch(value) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

End Class