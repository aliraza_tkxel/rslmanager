﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="AssignedToContractor.aspx.vb" Async="true" Inherits="Appliances.AssignedToContractor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example is 5000

        function TypingInterval() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
        }
        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }

        function PrintJobSheet() {
            var str_win
            str_win = '/RSLApplianceServicing/Views/Reports/PrintJobSheetSummary/PrintJobSheetSummary.aspx?JobSheetNumber=' + document.getElementById("lblJournalId").innerHTML  + "&appointmentType=" + document.getElementById('hdnAppointmentType').value;
            window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
            return false;
        }

    </script>
    <style type="text/css" media="all">
        .label-bold
        {
            font-weight: bold;
        }
        .searchbox
        {
            background-position: right center;
            background-repeat: no-repeat;
            float: right;
            width: 200px;
        }
        .StatusDropdown
        {
            float: left !important;
        }
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
        .modalPopupJobSheet
        {
            padding: 10px;
        }
        .padding-10
        {
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet">
        <div class="header">
            <span class="header-label">Contractor Report</span>
            <div class="field right">
                <asp:TextBox ID="txtSearch" AutoPostBack="false" Style="padding: 3px 10px !important;
                    margin: -4px 10px 0 0;" AutoCompleteType="Search" class="searchbox styleselect-control searchbox right"
                    onkeyup="TypingInterval();" PlaceHolder="Search User" runat="server">
                </asp:TextBox>
                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                </ajaxToolkit:TextBoxWatermarkExtender>
            </div>
        </div>
        <div class="portlet-body" style="font-size: 12px; overflow: inherit; padding-bottom: 0;">
            <asp:UpdatePanel ID="updPnlContractorList" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnAppointmentType" ClientIDMode="Static" runat="server" />
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <br />
                    </asp:Panel>
                    <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                        <cc2:PagingGridView ID="grdcontractorList" runat="server" AutoGenerateColumns="False"
                            AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
                            CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" PagerSettings-Visible="false"
                            OnRowCreated="grdcontractorList_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Ref" SortExpression="Ref">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PostCode" SortExpression="PostCode">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("PostCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contractor" SortExpression="Contractor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractor" runat="server" Text='<%# Bind("Contractor") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Assigned" SortExpression="AssignedDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssigned" runat="server" Text='<%# Bind("AssignedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By" SortExpression="AssignedBy">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssignedBy" runat="server" Text='<%# Bind("AssignedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CP12Expiry" SortExpression="CP12Expiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCP12Expiry" runat="server" Text='<%# Bind("CP12Expiry") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="false">
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewSubcontractorJobsheet" runat="server" CommandArgument='<%#Eval("Ref") + ";" + Eval("AppointmentType").ToString() + ";" + Eval("ASContractorID").ToString()%>'
                                            Text="View" OnClick="grdContractorList_SelectedIndexChanged" UseSubmitBehavior="False"
                                            CssClass="btn btn-xs btn-blue" Style="padding: 2px 10px !important; margin: -3px 5px 0 0;" />
                                        <asp:Button ID="btnCancelSubcontractorJobsheet" runat="server" CommandArgument='<%#Eval("Ref")%>'
                                            Text="Cancel" OnClick="cancelSelectedAppointment" UseSubmitBehavior="False" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="120px" />                                    
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc2:PagingGridView>
                    </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                            <div class="paging-left">
                                <span style="padding-right: 10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                        CssClass="lnk-btn">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                        CommandArgument="Prev" CssClass="lnk-btn">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span><span style="padding-right: 10px;"><b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. </span><span style="padding-right: 20px;">
                                        <b>Result:</b>
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        to
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        of
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </span><span style="padding-right: 10px;">
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                            CommandArgument="Next" CssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                            CommandArgument="Last" CssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                    </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                        PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                    UseSubmitBehavior="false" class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;"
                                    OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Pager Template End--%>
                    <%--<asp:UpdatePanel ID="updPnlJobSheetSummary" runat="server">
                        <ContentTemplate>--%>
                    <asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopupJobSheet" Visible="false">
                        <div class="portlet">
                            <div class="header" style="background-color: #e51837">
                                <span class="header-label">Appliance Servicing</span>
                                <div class="field right">
                                    <asp:Button ID="btnPrevious" runat="server" Text="&lt; Previous" Width="100px" CssClass="btn btn-xs btn-blue"
                                        Style="padding: 2px 10px !important; margin: -3px 5px 0 0;" OnClick="btnPrevious_Click" />
                                    <asp:Button ID="btnNext" runat="server" Text="Next >" Width="100px" CssClass="btn btn-xs btn-blue"
                                        Style="padding: 2px 10px !important; margin: -3px 10px 0 0;" OnClick="btnNext_Click" />
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div style="padding: 10px; border-bottom: 1px solid #A0A0A0">
                                    <span class="header-label label-bold">Summary</span> <span class="header-label right">
                                        <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                                            Text="Page"></asp:Label>
                                    </span>
                                </div>
                                <div id="tbl_header" style="padding: 10px;">
                                    <div style="padding: 10px; border: 1px solid #A0A0A0; display: inline-block; width: 98%">
                                        <div style="float: left">
                                            <span class="header-label label-bold">JS:</span>
                                            <asp:Label ID="lblJournalId" runat="server" ClientIDMode="Static" Style="vertical-align: top"></asp:Label>
                                        </div>
                                        <div runat="server" id="divCP12Expiry" style="float: right">
                                            <span class="header-label label-bold">CP12Expiry: </span>
                                            <asp:Label ID="lblCp12Expiry" runat="server" Text="" Style="vertical-align: top"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div style="border-bottom: 1px solid #A0A0A0; overflow: auto;">
                                    <div style="width: 50%; float: left;">
                                        <div class="padding-10">
                                            <span class="header-label label-bold">PO Number:</span>
                                            <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Contractor:</span>
                                            <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Contact:</span>
                                            <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Works Required:</span>
                                            <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 100px;
                                                height: 40px;" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Ordered By:</span>
                                            <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Ordered:</span>
                                            <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div style="width: 50%; float: left;">
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Status:</span>
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Budget:</span>
                                            <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Estimate:</span>
                                            <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">NET Cost:</span>
                                            <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">VAT:</span>
                                            <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Total:</span>
                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Order Total:</span>
                                            <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div id="Table1" style="border-bottom: 1px solid #A0A0A0; overflow: auto;">
                                    <div style="width: 50%; float: left;">
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Scheme:</span>
                                            <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Address:</span>
                                            <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Postcode:</span>
                                            <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div style="width: 50%; float: left;" runat="server" id="divCustomerInfo">
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Customer:</span>
                                            <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Telephone:</span>
                                            <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Mobile:</span>
                                            <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Email:</span>
                                            <asp:HiddenField ID="hdnFieldPurchaseOrderItemId" runat="server" />
                                            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div style="width: 50%; float: left; overflow: auto; max-height: 132px" runat="server"
                                        id="divBoilderInfo">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:Repeater ID="boilerInfoRpt" runat="server">
                                            <HeaderTemplate>
                                                <table style="width: 100%; margin-top: 5px" cellspacing="10" cellpadding="5">
                                                    <tr>
                                                        <td style="font-weight: bold;">
                                                            Boiler Name
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            Heating Fuel
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            Certificate Expiry
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            Certificate Issued
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label1" Text='<%# Eval("BoilerName") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label10" Text='<%# Eval("HeatingFuel") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label11" Text='<%# Eval("CP12Expiry") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label12" Text='<%# Eval("CP12Issued") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div style="border-bottom: 1px solid #A0A0A0; overflow: auto;">
                                    <div style="width: 50%; float: left;">
                                        <div class="padding-10">
                                            <span class="header-label label-bold">Asbestos:</span>
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div style="width: 50%; float: left;">
                                        <div class="padding-10" style="height: 100px; width: 400px; overflow: auto;">
                                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                BorderStyle="None" GridLines="None" Width="100%">
                                                <Columns>
                                                    <asp:BoundField DataField="AsbRiskID" />
                                                    <asp:BoundField DataField="Description">
                                                        <ItemStyle Wrap="False" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; text-align: right; clear: both; padding-top: 12px;">
                                    <div>
                                        <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" Width="100px" CssClass="btn btn-xs btn-blue"
                                            Style="padding: 2px 10px !important; margin: -3px 5px 0 0;" />
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="100px" Enabled="false"
                                            CssClass="btn btn-xs btn-blue" Style="padding: 2px 10px !important; margin: -3px 5px 0 0;" />
                                        <input id="btnPrintJobSheetSummary" type="button" value="Print Job Sheet" onclick="PrintJobSheet()"
                                            class="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 10px 0 0;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mdlPopupJobSheetSummarySubContractorUpdate" runat="server"
                        PopupControlID="pnlJobSheetSummary" TargetControlID="lblJournalId" BackgroundCssClass="modalBackground"
                        CancelControlID="btnBack">
                    </ajaxToolkit:ModalPopupExtender>
                    <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                    <!-- start of in progress image -->
                    <div style="float: left; padding-left: 164px;">
                        <asp:UpdateProgress ID="updateProgressFuelScheduling" runat="server" AssociatedUpdatePanelID="updPnlContractorList"
                            DisplayAfter="10">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img alt="Please Wait" src="/Images/progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <!-- End of in progress image -->
                    <asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
                        Width="390px" Height="270px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;">
                        <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                            Cancel</div>
                        <div style="clear: both; height: 1px;">
                        </div>
                        <hr />
                        <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
                            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                        <div style="width: 100%; height: 490px; overflow: hidden;">
                            <div style="height: 370px; padding-top: 10px !important; padding-left: 20px;">
                                <table>
                                    <tr>
                                        <td align="left" class="style1">
                                            <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                                                <asp:Label ID="lblErrorMessage" runat="server">
                                                </asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Panel ID="pnlDescription" runat="server">
                                                <asp:Label ID="lblDescription" runat="server" Text="Enter the reason for cancellation below:"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" class="style1">
                                            <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Panel ID="pnlCancelMessage" runat="server" Visible="False" HorizontalAlign="Center">
                                                <asp:Label ID="lblCancelFaultsText" runat="server" Text="The appointment has been cancelled."
                                                    Font-Bold="True"></asp:Label>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlSaveButton" runat="server" HorizontalAlign="Right">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
                        Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
                        DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
                        BehaviorID="mdlPopUpCancelFaults">
                    </cc1:ModalPopupExtender>
                    <asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
