﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="AdminScheduling.aspx.vb" Inherits="Appliances.AdminScheduling" EnableEventValidation="False"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="apparg" TagName="AppointmentArranged" Src="~/Controls/Scheduling/AdminAppointmentArranged.ascx" %>
<%@ Register TagPrefix="apptarg" TagName="AppointmentToBeArranged" Src="~/Controls/Scheduling/AdminAppointmentToBeArranged.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <%--Start - Changes - By Aamir Waheed on 15th July 2013 --%>
    <%--Thehse script has been added to the master and accessible on every page using ASMasterPage.Master --%>
    <script src="../../Scripts/jquery.fixedheadertable.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="../../Scripts/date.js"></script> --%>
    <%--End - Changes - By Aamir Waheed on 15th July 2013--%>
    <style type="text/css">
        #seta a
        {
            width: 191px !important;
        }
        #seta2 a
        {
            width: 149px !important;
            margin-left: 0px;
            margin-right: 0px;
        }
    </style>
    <script type="text/javascript">

        function createFixedHeader() {
            $(document).ready(function () {


                $('.fixedHeaderCalendar').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });


            });
        }

        function focusCurrentAppointment() {
            //document.getElementById("currentAppointment").scrollIntoView();
            $("#currentAppointment").focus();
        }
        function sendSMS(mobile, body, smsurl) {

            $.ajax({
                type: "GET",
                url: smsurl,
                data: "mobile=" + mobile + "&message=" + body + "",
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function LoadTab() {
            __doPostBack('ContentPlaceHolder1_tabAppointments', '');
        }
        function openAmendUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=document', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function fireDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxRefreshDataSetApt.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxRefreshDataSetApt.UniqueID%>\',\'\')', 0);
        }
        function openAddUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=document&popType=1', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
        function fireAddDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxRefreshDataSetAptba.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxRefreshDataSetAptba.UniqueID%>\',\'\')', 0);
        }

        var expiryAlertCounter = 0
        function checkMExpiryDate(expiryDateInSec, appointmentDate) {

            if (expiryAlertCounter == 0) {

                var appointmentDateInSec = new Date();
                appointmentDateInSec = Date.parse(appointmentDate.value).getTime() / 1000;

                if (appointmentDateInSec >= expiryDateInSec.value) {
                    alert('Alert: The certificate expiry date for this property is prior to the date of the appointment you have chosen.');
                    expiryAlertCounter = expiryAlertCounter + 1
                }
            }
        }

        function resetExpiryAlertCounter() {
            expiryAlertCounter = 0
        }       
        
    </script>
    <asp:UpdatePanel ID="updPanelFuelScheduling" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">Fuel Scheduling</font>
            </p>
            <table style="width: 100%;">
                <tr>
                    <td colspan="100%" style="border: 1px solid black; padding-right: 0px !important;
                        width: 100%;">
                        <div style="width: 100%; padding: 0; margin: 0;">
                            <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="TabInitial"
                                runat="server" BorderStyle="Solid" BorderColor="Black">Appointment to be arranged: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="TabInitial"
                                runat="server" BorderStyle="Solid" BorderColor="Black">Appointment arranged: </asp:LinkButton>
                            <div>
                                <div class="form-control">
                                    <div class="select_div">
                                        <div class="label">
                                            <asp:Label ID="lblStatusType" runat="server" Text="Status Type:" />
                                        </div>
                                        <div class="field right">
                                            <asp:DropDownList runat="server" ID="ddlStatusType" AutoPostBack="True" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-control">
                                    <div class="select_div">
                                        <div class="label">
                                            <asp:Label ID="lblFuelType" runat="server" Text="Fuel Type:" />
                                        </div>
                                        <div class="field right">
                                        <asp:DropDownList runat="server" ID="ddlFuelType" AutoPostBack="True" />                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; margin-top: 10px;">
                                <asp:CheckBox ID="ckBoxDueWithIn56Days" runat="server" AutoPostBack="True" Text="Due within 56 Days"
                                    Style="text-align: center" Checked="True" />
                            </div>
                            <div style="float: left; padding-left: 164px;">
                                <asp:UpdateProgress ID="updateProgressFuelScheduling" runat="server" AssociatedUpdatePanelID="updPanelFuelScheduling"
                                    DisplayAfter="10">
                                    <ProgressTemplate>
                                        <div class="loading-image">
                                            <img alt="Please Wait" src="../../Images/progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                            <div style="float: right; margin-top: 10px;">
                                <input id="txtSearch" runat="server" type="text" value="Search" onclick="this.value=''" />
                                <asp:ImageButton ID="imgSearchbtn" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/magnifir.png"
                                    OnClick="imgSearchbtn_DataBinding" BorderWidth="0px" />
                            </div>
                        </div>
                        <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                            width: 99.7%;">
                        </div>
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <apptarg:AppointmentToBeArranged ID="AdminAppointToBeArranged" runat="server" MaxValue="10"
                                    MinValue="1" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <apparg:AppointmentArranged ID="AppointArranged" runat="server" MaxValue="10" MinValue="1" />
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
