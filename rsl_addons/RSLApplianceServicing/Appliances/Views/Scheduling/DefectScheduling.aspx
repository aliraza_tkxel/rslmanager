﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectScheduling.aspx.vb" Inherits="Appliances.DefectScheduling"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="defect" TagName="apptarg" Src="~/Controls/Scheduling/DefectAppointmentsToBeArranged.ascx" %>
<%@ Register TagPrefix="defect" TagName="apparg" Src="~/Controls/Scheduling/DefectAppointmentsArranged.ascx" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
<script language="JavaScript" type="text/javascript" src="../../Scripts/jquery-1.6.1.min.js"></script>
    <style type="text/css" media="all">
        .TabFirst
        {
            margin-right: 0px;
            border-collapse: collapse;
        }
        .TabLast
        {
            margin-left: 0px;
            border-collapse: collapse;
        }
        .TabClicked
        {
            border-style: solid;
            border-color: Black;
            border-bottom-width: 0px;
        }
        .TabInitial
        {
            border-style: solid;
            border-color: Black;
        }
        .grdDefectAppointmentsHeader th 
        {
            text-align:left;  
            padding-left:8px;   
        }
    </style>
</asp:Content>
<asp:Content ID="ContentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="pageHeader ">
        Schedule Defects
    </p>
    <%--<asp:UpdatePanel ID="updPanelDefectsScheduling" runat="server">
        <ContentTemplate>--%>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <table style="width: 100%;">
        <tr>
            <td style="border: 1px solid black; padding-right: 0px !important; width: 100%;">
                <div style="width: 100%; padding: 0; margin: 0;">
                    <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="TabClicked TabFirst"
                        runat="server">Appointments to be arranged </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="TabInitial TabLast"
                        runat="server">Appointments arranged </asp:LinkButton>
                    <div style="float: right; margin-top: 10px; margin-right:10px; /*border-bottom: 1px solid black; */">
                        <asp:DropDownList runat="server" ID="ddlCategories" AutoPostBack="true">
                            <asp:ListItem Text="Select Category" Value="-1" />
                            <asp:ListItem Text="RIDDOR" Value="8" />
                            <asp:ListItem Text="Immediately Dangerous" Value="9" />
                            <asp:ListItem Text="At Risk" Value="10" />
                            <asp:ListItem Text="Not to Current Standards" Value="11" />
                            <asp:ListItem Text="Other" Value="12" />
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" />
                        <%--<asp:ImageButton ID="imgSearchbtn" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/magnifir.png"
                                    OnClick="imgSearchbtn_DataBinding" BorderWidth="0px" />--%>
                    </div>
                </div>
                <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                    width: 99.7%;">
                </div>
                <div style="clear: both; margin-bottom: 5px;">
                </div>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <defect:apptarg runat="server" ID="appointmentToBeArranged"></defect:apptarg>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <defect:apparg runat="server" ID="appointmentArranged"></defect:apparg>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
    <asp:UpdateProgress ID="updateProgressDefectsScheduling" runat="server" 
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
