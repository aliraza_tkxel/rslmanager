﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="OilServicing.aspx.vb" Inherits="Appliances.OilServicing" EnableEventValidation="False"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="apparg" TagName="OilAppointmentArranged" Src="~/Controls/Scheduling/OilAppointmentArranged.ascx" %>
<%@ Register TagPrefix="apptarg" TagName="OilAppointmentToBeArranged" Src="~/Controls/Scheduling/OilAppointmentToBeArranged.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../../Scripts/date.js"></script>
    <script src="../../Scripts/jquery-1.10.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&client=gme-broadlandhousing&v=3.17"></script>
    <style type="text/css">
        #seta a
        {
            width: 191px !important;
        }
        #seta2 a
        {
            width: 149px !important;
            margin-left: 0px;
            margin-right: 0px;
        }
        .searchbox
        {
            background-position: right center;
        }
        .select_div select
        {
            width: 209px !important;
        }
        .display
        {
            display: block;
            border-bottom: 1px solid #ddd;
        }
    </style>
    <script type="text/javascript">

        function createFixedHeader() {
            $(document).ready(function () {
                $('.fixedHeaderCalendar').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });
            });
        }

        function focusCurrentAppointment() {
            //document.getElementById("currentAppointment").scrollIntoView();
            $("#currentAppointment").focus();
        }

        function sendSMS(mobile, body, smsurl) {
            $.ajax({
                type: "GET",
                url: smsurl,
                data: "mobile=" + mobile + "&message=" + body + "",
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function LoadTab() {
            __doPostBack('ContentPlaceHolder1_tabAppointments', '');
        }
    </script>
    <asp:UpdatePanel ID="updPanelFuelScheduling" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="portlet">
                <div class="header">
                    <span class="header-label">Oil Servicing</span>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom: 0;">
                    <div style="overflow: auto;">
                        
                        <asp:Panel ID="pnlStatus" runat="server">
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="label">
                                        <asp:Label ID="lblStatusType" runat="server" Text="Status Type:" />
                                    </div>
                                    <div class="field right">
                                        <asp:DropDownList runat="server" ID="ddlStatusType" AutoPostBack="True" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="form-control right">
                            <div class="select_div right" style="padding-top: 12px;">
                                <div class="field right" style="margin-left: 0;">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="false" Style="padding: 3.4px 10px !important;
                                        width: 150px; margin: -3px 20px 0 0;" AutoCompleteType="Search" ToolTip="Search"
                                        class="searchbox styleselect-control" runat="server">
                                    </asp:TextBox>
                                    <asp:TextBoxWatermarkExtender ID="txtBoxWatermarkExtenderSearch" runat="server" TargetControlID="txtSearch"
                                        WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                                    </asp:TextBoxWatermarkExtender>
                                    <asp:Button ID="btnSearch" Text="Search" runat="server" UseSubmitBehavior="False"
                                        CssClass="btn btn-xs btn-blue right" Style="padding: 3px 23px !important; margin: -3px 0 0 0;" />
                                </div>
                            </div>
                        </div>
                        <div class="form-control right">
                            <div class="select_div">
                                <div class="field" style="margin-left: 0;">
                                    <asp:CheckBox ID="ckBoxDueWithIn56Days" runat="server" AutoPostBack="True" Text="Due within 56 Days"
                                        Style="text-align: center" Checked="True" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style="padding: 10px 10px 0 10px;">
                        <div style="overflow: auto; padding: 0px !important;">
                            <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="display TabInitial"
                                runat="server">Appointment to be arranged: </asp:LinkButton>
                            <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="display TabInitial"
                                runat="server" Style="display: block;">Appointment arranged: </asp:LinkButton>
                            <span style="display: block; height: 27px; border-bottom: 1px solid #c5c5c5"></span>
                        </div>
                    </div>
                    <div style="width: 100%; padding: 0; margin: 0;">
                        <div style="float: left; padding-left: 164px;">
                            <asp:UpdateProgress ID="updateProgressFuelScheduling" runat="server" AssociatedUpdatePanelID="updPanelFuelScheduling"
                                DisplayAfter="10">
                                <ProgressTemplate>
                                    <div class="loading-image">
                                        <img alt="Please Wait" src="../../Images/progress.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                    <div style="padding: 0 10px">
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <apptarg:OilAppointmentToBeArranged ID="oilAppointmentToBeArranged" runat="server"
                                    MaxValue="10" MinValue="1" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <apparg:OilAppointmentArranged ID="oilAppointmentArranged" runat="server" MaxValue="10"
                                    MinValue="1" />
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
