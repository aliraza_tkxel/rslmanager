﻿Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports System.Threading
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class OilServicing
    Inherits PageBase

    Public ckBoxDocumentUploadApta As New CheckBox()
    Public ckBoxDocumentUploadAptba As New CheckBox()
    Public ckBoxRefreshDataSetApta As New CheckBox()
    Public ddlLetterApta As New DropDownList

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.FindControlsOfUserControl()
            If (Not IsPostBack) Then
                ckBoxDueWithIn56Days.Checked = False
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
                Me.GetStatusType(ddlStatusType)
                PopulateFuelServicingTab()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub ckBoxDueWithIn56Days_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDueWithIn56Days.CheckedChanged
        Try
            SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
            If (MainView.ActiveViewIndex = 0) Then
                oilAppointmentToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlStatusType.SelectedItem.Text)
            ElseIf (MainView.ActiveViewIndex = 1) Then
                oilAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            If (MainView.ActiveViewIndex = 0) Then
                oilAppointmentToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlStatusType.SelectedItem.Text)
            ElseIf (MainView.ActiveViewIndex = 1) Then
                oilAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#Region "lnk Btn Aptba Tab Click"

    Protected Sub lnkBtnAptbaTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            PopulateAppointmentToBeArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Apt Tab Click"
    Protected Sub lnkBtnAptTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            PopulateAppointmentArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Populate Fuel Servicing Tab"

    Private Sub PopulateFuelServicingTab()
        If Request.QueryString(PathConstants.Tab) IsNot Nothing AndAlso Request.QueryString(PathConstants.Tab) <> String.Empty Then

            If Request.QueryString(PathConstants.Tab).Equals(PathConstants.AppointmentArranged) Then
                PopulateAppointmentArrangedList()
            Else
                PopulateAppointmentToBeArrangedList()
            End If

        Else
            PopulateAppointmentToBeArrangedList()
        End If
    End Sub

#End Region

#Region "Populate Appointment To Be Arranged"

    Private Sub PopulateAppointmentToBeArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        pnlStatus.Visible = True
        oilAppointmentToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlStatusType.SelectedItem.Text)
    End Sub

#End Region

#Region "Populate Appointment Arranged"

    Private Sub PopulateAppointmentArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 1
        pnlStatus.Visible = False
        oilAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text)
    End Sub
#End Region

#Region "Load Status Type"
    Protected Sub GetStatusType(ByVal ddl As DropDownList)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getStatus(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("All", -1))

    End Sub
#End Region

#Region "Find Contorls"

    Private Sub FindControlsOfUserControl()

        ckBoxDocumentUploadApta = DirectCast(oilAppointmentArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ckBoxDocumentUploadAptba = DirectCast(oilAppointmentToBeArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ddlLetterApta = DirectCast(oilAppointmentArranged.FindControl("ddlLetter"), DropDownList)
        ckBoxRefreshDataSetApta = DirectCast(oilAppointmentArranged.FindControl("ckBoxRefreshDataSet"), CheckBox)

    End Sub
#End Region

#Region "Web Method - Get Existing Appointments Data (Property Address)"

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function getExistingAppointmentsData() As String
        Dim returnJSON As New StringBuilder()
        Dim serializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim appointmentsForMap = SessionManager.getExistingAppointmentsForSchedulingMap()

        Dim rows = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        If Not IsNothing(appointmentsForMap) Then
            For Each dr As DataRow In appointmentsForMap.Rows
                row = New Dictionary(Of String, Object)()
                For Each col As DataColumn In appointmentsForMap.Columns
                    row.Add(col.ColumnName, dr(col))
                Next
                rows.Add(row)
            Next
        End If

        Return serializer.Serialize(rows)
    End Function

#End Region

    Protected Sub ddlStatusType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatusType.SelectedIndexChanged
        Try
            changeFuelType()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try
    End Sub

#Region "Change Fuel Type"
    ''' <summary>
    ''' Change Fuel Type
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeFuelType()
        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateAppointmentArrangedList()
        End If
    End Sub

#End Region

End Class