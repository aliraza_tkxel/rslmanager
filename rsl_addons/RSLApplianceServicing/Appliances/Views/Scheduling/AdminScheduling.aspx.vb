﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Delegate Sub aptbaSearchDelegate(ByVal searchText As String)

Public Class AdminScheduling
    Inherits PageBase

    Public ckBoxRefreshDataSetApt As New CheckBox()
    Public ckBoxRefreshDataSetAptba As New CheckBox()
    Dim objAppointmentToBeArranged As AppointmentToBeArranged = CType(Page.LoadControl("~/Controls/Scheduling/AppointmentToBeArranged.ascx"), AppointmentToBeArranged)
    Public Event aptbaSearchEvent As aptbaSearchDelegate

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.FindControlsOfUserControl()

            'Open Popup for Add New Appointment from <AppointmentToBeArranged>
            'If ((Not IsNothing(Request.QueryString("param")) And ApplicationConstants.addAppointmentClicked = False)) Then
            If ((Not IsNothing(Request.QueryString("param")) And SessionManager.getAptbaAppointmentClicked() = False)) Then
                'Setting <AppointmentToBeArranged> Tab section
                MainView.ActiveViewIndex = 0
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
                lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass

                If Not Page.IsPostBack Then
                    AdminAppointToBeArranged.AddAppointmentPopup()
                End If
                'Open Popup for Amend Existing Appointment from <AppointmentArranged>
            ElseIf ((Not IsNothing(Request.QueryString("op")) And SessionManager.getApaAmendAppointmentClicked() = False) Or (Request.QueryString("chk") = 1 And SessionManager.getAptbaIsSuccess() = True)) Then
                'Setting <AppointmentArranged> Tab section
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass
                lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass
                MainView.ActiveViewIndex = 1

                'For Successful Addition of New Appointment from <AppointmentToBeArranged>  
            ElseIf (Not IsNothing(Request.QueryString("ac")) And Request.QueryString("ac") = 0 And SessionManager.getAptbaIsSuccess() = True) Then
                'Setting <Tab> section
                MainView.ActiveViewIndex = 0
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
                lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass

                AdminAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Value, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)
                AdminAppointToBeArranged.setCalendarInfo()

                ' Control Calendar Next and Previous movement from <Appointment To Be Arranged> 
            ElseIf ((Not IsNothing(Request.QueryString("pg"))) And (SessionManager.getAptbaCalendarPageingClicked() = False) And (Not IsPostBack)) Then
                MainView.ActiveViewIndex = 0
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
                lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass
                AdminAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Value, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)

                'Setting Calendar - Week to show on calendar of appointment to be arranged.
                If (Request.QueryString("pg") = "nt" And SessionManager.getAptbaCalendarPageingClicked() = False) Then
                    'Set the new start date +7 days from current date to show next week calander.
                    SessionManager.setAptbaStartDate(DateAdd("d", 7, SessionManager.getAptbaStartDate()))
                ElseIf (Request.QueryString("pg") = "pr" And SessionManager.getAptbaCalendarPageingClicked() = False) Then
                    'Set the new end date -1 days from current start date to show  previous week calander.
                    SessionManager.setAptbaEndDate(DateAdd("d", -1, SessionManager.getAptbaStartDate()))
                    'Set the new start date -7 days from current start date to show previous week calander.
                    SessionManager.setAptbaStartDate(DateAdd("d", -7, SessionManager.getAptbaStartDate()))
                ElseIf (Request.QueryString("pg") = "td" And SessionManager.getAptbaCalendarPageingClicked() = False) Then
                    SessionManager.setAptbaStartDate(Date.Today)
                End If

                'To Remove Amend Calander Weekly Data Table from session so it can be created again
                SessionManager.removeAddCalanderWeeklyDataTable()
                AdminAppointToBeArranged.setCalendarInfo()

                ' Control Calendar Next and Previous movement from <Appointment Arranged> 
            ElseIf ((Not IsNothing(Request.QueryString("e"))) And (SessionManager.getApaAmendCalendarPageingClicked() = False) And (Not IsPostBack)) Then
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                populateAppointmentArrangedList()

                'Setting Calendar - Week to show on calendar of appointment arranged.
                If (Request.QueryString("e") = "n") Then 'Appointment arranged move next                
                    'Set the new start date +7 days from current date to show next week calander.
                    SessionManager.setApaStartDate(DateAdd("d", 7, SessionManager.getApaStartDate()))
                ElseIf (Request.QueryString("e") = "p") Then
                    'Set the new end date -1 days from current start date to show  previous week calander.
                    SessionManager.setApaEndDate(DateAdd("d", -1, SessionManager.getApaStartDate()))
                    'Set the new start date -7 days from current start date to show previous week calander.
                    SessionManager.setApaStartDate(DateAdd("d", -7, SessionManager.getApaStartDate()))
                ElseIf (Request.QueryString("e") = "t") Then
                    SessionManager.setApaStartDate(Date.Today)
                End If
                'To Remove Amend Calander Weekly Data Table from session so it can be created again
                SessionManager.removeAmendCalanderWeeklyDataTable()
                AppointArranged.setCalendarInfo()

            ElseIf (Not IsNothing(Request.QueryString("chk")) And Request.QueryString("chk") = 1 And SessionManager.getApaAmendIsSuccess() = True) Then
                ckBoxDueWithIn56Days.Checked = True
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
                'Tenancy Popup for calendar view in Appointment Arranged
            ElseIf (Not IsNothing(Request.QueryString("tId")) And SessionManager.getTenancyClicked() = False AndAlso (Not IsPostBack)) Then
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                populateAppointmentArrangedList()
                AppointArranged.setCalendarInfo()
            ElseIf (Not IsNothing(Request.QueryString("aptId")) And SessionManager.getAptbaTenancyClicked() = False AndAlso (Not IsPostBack)) Then
                ckBoxDueWithIn56Days.Checked = SessionManager.getDueWithIn56Days()
                populateAppointmentToBeArrangedList()
                AdminAppointToBeArranged.setCalendarInfo()
            End If

            'To Implement bug fix - The 'Due in 56 days' check box should remain checked after an appointment has been arranged in the Add Appointment pop up
            If (Not IsNothing(Request.QueryString("chk")) AndAlso Request.QueryString("chk") = 1) Then
                ckBoxDueWithIn56Days.Checked = True
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
            ElseIf (Not IsNothing(Request.QueryString("chk")) AndAlso Request.QueryString("chk") = 0) Then
                ckBoxDueWithIn56Days.Checked = False
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
            End If

            'Set default <Tab> for first time        
            If (Not IsPostBack AndAlso Request.QueryString.Count = 0) Then
                Me.getFuelType(ddlFuelType)
                Me.getStatusType(ddlStatusType)
                ckBoxDueWithIn56Days.Checked = True
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
                populateAppointmentToBeArrangedList()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Protected Sub ckBoxDueWithIn56Days_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDueWithIn56Days.CheckedChanged
        Try
            SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
            If (MainView.ActiveViewIndex = 0) Then
                populateAppointmentToBeArrangedList()
            ElseIf (MainView.ActiveViewIndex = 1) Then
                populateAppointmentArrangedList()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Protected Sub imgSearchbtn_DataBinding(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearchbtn.Click
        Try
            If (MainView.ActiveViewIndex = 0) Then
                populateAppointmentToBeArrangedList()
            ElseIf (MainView.ActiveViewIndex = 1) Then
                populateAppointmentArrangedList()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub


#Region "Setup Defaults"
    Sub setupDefaults()
        ddlFuelType.Items.FindByText(ApplicationConstants.DefautFuelType).Selected = True
    End Sub
#End Region

#Region "lnk Btn Aptba Tab Click"

    Protected Sub lnkBtnAptbaTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentToBeArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Apt Tab Click"
    Protected Sub lnkBtnAptTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "ddlStatusType Selected Index Changed"
    ''' <summary>
    ''' ddlStatusType Selected Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlStatusType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatusType.SelectedIndexChanged
        Try
            changeFuelType()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try
    End Sub
#End Region

#Region "ddlStatusType Selected Index Changed"
    ''' <summary>
    ''' ddlStatusType Selected Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlFuelType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            changeFuelType()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try
    End Sub
#End Region

#Region "Change Fuel Type"
    ''' <summary>
    ''' Change Fuel Type
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeFuelType()
        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateAppointmentArrangedList()
        End If
    End Sub

#End Region

#Region "Populate Appointment To Be Arranged"

    Sub populateAppointmentToBeArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        ddlStatusType.Visible = True
        lblStatusType.Visible = True
        AdminAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Value, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)
    End Sub

#End Region

#Region "Populate Appointment Arranged"
    Sub populateAppointmentArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 1
        ddlStatusType.Visible = False
        lblStatusType.Visible = False
        AppointArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Value, ddlFuelType.SelectedValue)
    End Sub
#End Region

#Region "Find Contorls"
    Protected Sub FindControlsOfUserControl()

        ckBoxRefreshDataSetApt = DirectCast(AppointArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ckBoxRefreshDataSetAptba = DirectCast(AdminAppointToBeArranged.FindControl("ckBoxDocumentUpload"), CheckBox)

    End Sub
#End Region

#Region "Load Fuel Type"
    Protected Sub getFuelType(ByVal ddl As DropDownList)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getFuel(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        Dim itemToRemove As ListItem
        itemToRemove = ddl.Items.FindByValue("901")
        If itemToRemove IsNot Nothing Then
            ddl.Items.Remove(itemToRemove)
        End If
        ddl.Items.Insert(0, New ListItem("All", -1))

    End Sub
#End Region

#Region "Load Status Type"
    Protected Sub getStatusType(ByVal ddl As DropDownList)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getStatus(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("All", -1))

    End Sub
#End Region


End Class