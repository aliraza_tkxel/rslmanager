﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master" CodeBehind="VoidScheduling.aspx.vb" Inherits="Appliances.VoidScheduling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="apparg" TagName="AppointmentArranged" Src="~/Controls/Scheduling/VoidAppointmentArranged.ascx" %>
<%@ Register TagPrefix="apptarg" TagName="AppointmentToBeArranged" Src="~/Controls/Scheduling/VoidAppointmentToBeArranged.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="../../Scripts/date.js"></script>
    <script src="../../Scripts/jquery-1.10.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&client=gme-broadlandhousing&v=3.17"></script>

<script type="text/javascript">

    function createFixedHeader() {
        $(document).ready(function () {


            $('.fixedHeaderCalendar').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });


        });
    }

    function focusCurrentAppointment() {
        //document.getElementById("currentAppointment").scrollIntoView();
        $("#currentAppointment").focus();
    }
    var map;
    var markers = [];
    var markersAddress = [];
    function showMap(mapContainerId, destinationAddress) {
        var map_canvas = document.getElementById(mapContainerId);

        var map_options = {
            center: new google.maps.LatLng(52.630886, -0.4573616),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(map_canvas, map_options);
        setAllMap(map);

        addOriginMarkers();

        var maxZindex = google.maps.Marker.MAX_ZINDEX;
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': destinationAddress }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    title: destinationAddress,
                    zIndex: maxZindex,
                    icon: "https://maps.google.com/mapfiles/kml/paddle/D.png"
                });
            }
        });
    }

    function addOriginMarkers() {
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "FuelScheduling.aspx/getExistingAppointmentsData",
            data: "{}",
            dataType: "json",
            success: function (response) {
                var appointments = JSON.parse(response.d);
                var counter = 0;
                var timeout = 1000;
                for (var i = 0; i < appointments.length; i++) {

                    if (counter > 0 && counter % 5 == 0) {
                        timeout += 1000;
                    }
                    setTimeout(setMarker(appointments[i]), timeout);
                    counter++;
                }
            }
        });
    }

    function setMarker(appointment) {
        var adrs = appointment.Address + ", " + appointment.TownCity + ", " + appointment.County;
        // Check if a property address is already processed cancel geocode call and return.
        if ($.inArray(adrs, markersAddress) >= 0) {
            console.log("skiped:" + adrs);
            return;
        }

        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': adrs }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(status + ":" + adrs + "Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    title: adrs,
                    icon: "https://maps.google.com/mapfiles/kml/paddle/ylw-circle.png"
                });
                markers.push(marker);
                markersAddress.push(adrs);
            }
            else {
                console.log(status + ":" + adrs);
            }
        });
    }

    // Sets the map on all markers in the array.
    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setAllMap(null);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
        markersAddress = [];
    }

    function sendSMS(mobile, body, smsurl) {

        $.ajax({
            type: "GET",
            url: smsurl,
            data: "mobile=" + mobile + "&message=" + body + "",
            dataType: 'json',
            success: function (data, textStatus, xhr) {
            }
        });
    }

    </script>


    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        
    </script>

    <link href="../../Styles/VoidServicing.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
    function LoadTab() {
        __doPostBack('ContentPlaceHolder1_tabAppointments', '');
    }
    function openAmendUploadWindow() {
        wd = window.open('../../Views/Common/UploadDocument.aspx?type=document', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
    }
    function fireDocUploadCkBoxEvent() {
        document.getElementById('<%=ckBoxDocumentUploadApta.ClientID%>').checked = true;
        setTimeout('__doPostBack(\'<%=ckBoxDocumentUploadApta.UniqueID%>\',\'\')', 0);
    }
    function openAddUploadWindow() {
        wd = window.open('../../Views/Common/UploadDocument.aspx?type=document&popType=1', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
    }
    function fireAddDocUploadCkBoxEvent() {
        document.getElementById('<%=ckBoxDocumentUploadAptba.ClientID%>').checked = true;
        setTimeout('__doPostBack(\'<%=ckBoxDocumentUploadAptba.UniqueID%>\',\'\')', 0);
    }

    function fireLetterUploadCheckboxEvent() {
        // alert('sdfsd');
        document.getElementById('<%= ckBoxRefreshDataSetApta.ClientID %>').checked = true;
        setTimeout('__doPostBack(\'<%= ckBoxRefreshDataSetApta.UniqueID %>\',\'\')', 0);
    }

    function openEditLetterWindow() {

        var letterId = document.getElementById('<%= ddlLetterApta.ClientID %>').value;
        if (letterId == -1) {

        }
        else {
            w = window.open('../../Views/Resources/EditLetter.aspx?id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
    }

        
    </script>
<asp:UpdatePanel ID="updPanelFuelScheduling" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">Void Servicing</font>
            </p>
            <table style="width: 100%;">
                <tr>
                    <td colspan="100%" style="border: 1px solid black; padding-right: 0px !important;
                        width: 100%;">
                        <div id="tabsPanel">

                            <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="TabInitial tabs"
                                runat="server" BorderStyle="Solid" BorderColor="Black">Appointment to be arranged: </asp:LinkButton>

                            <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="TabInitial tabs"
                                runat="server" BorderStyle="Solid" BorderColor="Black">Appointment arranged: </asp:LinkButton>

                            <div style="float: left; padding-left: 164px;">
                                <asp:UpdateProgress ID="updateProgressFuelScheduling" runat="server" AssociatedUpdatePanelID="updPanelFuelScheduling"
                                    DisplayAfter="10">
                                    <ProgressTemplate>
                                        <div class="loading-image">
                                            <img alt="Please Wait" src="../../Images/progress.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                            <div id="searchBoxPanel">
                                <%--<input id="txtSearch" runat="server" type="text" class="searchBox" value="Search" onclick="this.value=''" />--%>
                                <asp:TextBox ID="txtSearch" runat="server" class="searchBox" AutoPostBack="false" onkeyup="TypingInterval();"
                                        OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                                <%--<asp:ImageButton ID="imgSearchbtn" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/magnifir.png"
                                    OnClick="imgSearchbtn_DataBinding" BorderWidth="0px" />--%>
                            </div>
                        </div>
                        <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                            width: 99.7%;">
                        </div>
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <apptarg:AppointmentToBeArranged ID="AppointToBeArranged1" runat="server" MaxValue="10"
                                    MinValue="1" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <apparg:AppointmentArranged ID="AppointArranged" runat="server" MaxValue="10" MinValue="1" />
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
