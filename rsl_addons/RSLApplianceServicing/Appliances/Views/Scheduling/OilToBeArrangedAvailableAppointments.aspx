﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="OilToBeArrangedAvailableAppointments.aspx.vb" Inherits="Appliances.OilToBeArrangedAvailableAppointments"
    EnableEventValidation="False" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="appointmentNotesTag" TagName="AppointmentNotes" Src="~/Controls/Scheduling/AppointmentNotes.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function openEditLetterWindowForAdd() {

            var letterId = document.getElementById('<%= ddlAddLetter.ClientID %>').value;
            if (letterId == -1) {

            }
            else {
                w = window.open('../../Views/Resources/EditLetter.aspx?pg=0&id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
            }
        }

        function fireCheckboxEventForAddApp() {
            document.getElementById('<%= ckBoxAddRefreshDataSet.ClientID %>').checked = true;
            setTimeout('__doPostBack(\'<%= ckBoxAddRefreshDataSet.UniqueID %>\',\'\')', 0);
        }

        function fireAddDocUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDocumentUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDocumentUpload.UniqueID%>\',\'\')', 0);
        }

        function openAddUploadWindow() {
            wd = window.open('../../Views/Common/UploadDocument.aspx?type=document&popType=1', 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:CheckBox ID="ckBoxAddRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
        CssClass="hiddenField" />
    <asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
    <div class="headingTitle">
        <b>
            <%--Schedule planned works--%>
            <asp:Label ID="lblStreetAddress" runat="server"></asp:Label>, &nbsp;<asp:Label ID="lblPostCode"
                runat="server"></asp:Label>
        </b>
    </div>
    <div>
        <asp:UpdatePanel ID="updPanelMain" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="message scheduling-message">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlPageContainer" runat="server" CssClass="trade-appointment-blocks-container">
                </asp:Panel>
                <div style="width: 100%; padding: 0; margin: 0;">
                    <div style="float: left; padding-left: 164px;">
                        <asp:UpdateProgress ID="updateProgress1" runat="server" AssociatedUpdatePanelID="updPanelMain"
                            DisplayAfter="10">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img alt="Please Wait" src="../../Images/progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlSuccessPopup" runat="server">
                <asp:ImageButton ID="imgBtnCancelSuccessPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    OnClick="imgBtnCancelSuccessPopup_Click" BorderWidth="0" />
                <div class="apt-success-popup" style="width: 550px !Important; height: 150px; border: 1px solid black;">
                    <%--<propertyAddressTag:PropertyAddress ID="webUserCtrlPropAddressSuccessPopup" runat="server"
            MaxValue="10" MinValue="1" />--%>
                    <asp:Panel ID="pnlSuccessAppointmentMessage" runat="server" CssClass="pnl-apt-success-msg"
                        Style="border: none !important; padding-top: 50px !important">
                        <asp:Label ID="lblSuccessAppointmentMessage" runat="server" Text=""></asp:Label>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlSuccessPopup" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlSuccessPopup"
                DropShadow="true" CancelControlID="lblDisplaySuccessPopup" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updPanelAddApointment" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlAddAppointment" CssClass="left" runat="server" BackColor="White"
                Width="470px" Height="500px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
                position: absolute; display: none">
                <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                    Add Appointment</div>
                <div style="clear: both; height: 1px;">
                </div>
                <div style="width: 100%; padding: 0; margin: 0;">
                    <div style="float: left; padding-left: 164px;">
                        <asp:UpdateProgress ID="updateProgressFuelScheduling" runat="server" AssociatedUpdatePanelID="updPanelAddApointment"
                            DisplayAfter="10">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img alt="Please Wait" src="../../Images/progress.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
                <hr />
                <asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
                    top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <%--<div style="width: 100%; height: 490px; overflow: hidden;">--%>
                <div style="overflow-y: scroll; height: 470px; padding-top: 5px !important;">
                    <asp:UpdatePanel ID="updPanelAddAppointmentPopup" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td colspan="3" valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" colspan="3">
                                        <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                                            <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Appointment Type:<span class="Required">*</span>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblAppointmentType" runat="server" Text="Appliance Servicing"></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Location:
                                    </td>
                                    <td align="right" valign="top" class="style2">
                                        <span style="float: left; margin-top: -4px; vertical-align: top;">
                                            <asp:ImageButton ID="lnkBtnAppDetails" runat="server" ImageUrl="../../images/phone.jpg"
                                                BorderStyle="None" />
                                        </span>
                                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Date:<span class="Required">*</span>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblAppointmentDate" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Starts:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblAppointmentStartTime" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Ends:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblAppointmentEndTime" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Operative:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblOperative" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Status:<span class="Required">*</span>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:DropDownList ID="ddlAddStatus" runat="server" AutoPostBack="True" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Action:<span class="Required">*</span>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:DropDownList ID="ddlAddAction" runat="server" AutoPostBack="True" Width="200px">
                                            <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Letter:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:DropDownList ID="ddlAddLetter" runat="server" Width="200px">
                                            <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" class="style5">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAddUploadWindow()"
                                            BackColor="White" />
                                        &nbsp;
                                        <asp:Button ID="btnAddViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindowForAdd()"
                                            BackColor="White" />
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Docs:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                                            <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                                <ItemTemplate>
                                                    <table class="LetterDocInnerTable">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                                    Visible="<%# False %>"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                                    Visible="<%# False %>"></asp:Label>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                                    BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                                                    OnClick="imgBtnRemoveLetterDoc_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <SelectedItemStyle BackColor="Gray" />
                                            </asp:DataList>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="style5">
                                        Notes:
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine" Width="246px"></asp:TextBox>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="add-apt-canel-save">
                        <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
                        <asp:Button ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
            <asp:ModalPopupExtender ID="mdlPopUpAddAppointment" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlAddAppointment"
                DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnCancelAction"
                BehaviorID="mdlPopupAddAppointmentBehaviorId">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--Appointment Notes Popup Start--%>
            <asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
                Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
                top: 20px; position: absolute;">
                <div>
                    <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
                        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </div>
                <div style="width: 97%; padding-left: 10px; padding-right: 10px;">
                    <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" Style="padding-right: 10px;
                        font-weight: bold"></asp:Label>
                </div>
                <hr />
                <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
                    <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled."
                        Style="padding-right: 10px;"></asp:Label>
                </div>
            </asp:Panel>
            <asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
            <asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
                PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlAppointmentNotesPopup" runat="server">
                <appointmentnotestag:appointmentnotes id="ucAppointmentNotes" runat="server" maxvalue="10"
                    minvalue="1" />
            </asp:Panel>
            <asp:Label ID="lblDisplayAppointmentNotesPopup" runat="server" Text=""></asp:Label>
            <asp:ModalPopupExtender ID="mdlAppointmentNotesPopup" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDisplayAppointmentNotesPopup" PopupControlID="pnlAppointmentNotesPopup"
                DropShadow="true" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--Appointment Notes Popup End--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlAmendApointmentContactDetails" runat="server" BackColor="#A4DAF6"
                Style="padding: 10px; border: 1px solid gray;">
                <table id="tblTenantInfoAmendPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
                    <tr>
                        <td style="width: 0px; height: 0px;">
                            <asp:ImageButton ID="imgbtnCloseAmendContactDetail" runat="server" Style="position: absolute;
                                top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Label ID="lblAmendApointmentContactDetails" runat="server"></asp:Label>
            <asp:ModalPopupExtender ID="mdlPopUpAmenAptContactDetails" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblAmendApointmentContactDetails" PopupControlID="pnlAmendApointmentContactDetails"
                DropShadow="true" CancelControlID="imgbtnCloseAmendContactDetail" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPropertyAttributesNotes" CssClass="left" BackColor="White"
                Width="470px" Style="max-height: 350px; padding: 15px 5px; top: 32px; border: 1px solid black;
                position: absolute; z-index: 100000;">
                <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                    Notes</div>
                <div style="clear: both; height: 1px;">
                </div>
                <hr />
                <asp:ImageButton ID="imgBtnNotes" runat="server" Style="position: absolute; top: -12px;
                    right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <div style="height: 200px; overflow: auto; width: 100%;">
                    <asp:Repeater runat="server" ID="rptPropertyNotes">
                        <ItemTemplate>
                            <div style='width: 100%; padding: 10px;'>
                                <asp:Label Text='<%# Eval("AttributeName") %>' ID="lblAttribute" Font-Bold="true"
                                    runat="server" />
                                <br />
                                <asp:Label Text='<%# Eval("AttributeNotes") %>' ID="lblAttributeNotes" runat="server" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mdlPopupNotes" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="lblPropertyAttributesNotes" PopupControlID="pnlPropertyAttributesNotes"
                DropShadow="true" CancelControlID="imgBtnNotes" BackgroundCssClass="modalBackground2">
            </asp:ModalPopupExtender>
            <asp:Label ID="lblPropertyAttributesNotes" runat="server" Text=""></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
