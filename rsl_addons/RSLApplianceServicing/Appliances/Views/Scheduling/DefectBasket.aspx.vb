﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class DefectBasket
    Inherits PageBase

#Region "Events Handling"

#Region "Page Events Handling"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                populateDefects()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Schedule Selected Faults Click"

    Protected Sub btnScheduleSelectedFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleSelectedFaults.Click
        Try
            Dim checkedDefectIdsList As New List(Of Integer)
            If (Not checkAnySelected(checkedDefectIdsList)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.selectDefect, True)
            ElseIf (Not checkTradeExist()) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorNoTradeExist, True)
            Else
                setSelectedDefectsInSession(checkedDefectIdsList)

                Dim selectedInspectionRecord As DataTable = SessionManager.getSelectDefectInseptionRef()
                If (Not IsNothing(selectedInspectionRecord) AndAlso selectedInspectionRecord.Rows.Count > 0) Then
                    Dim commonAddressBO As New CommonAddressBO()
                    If selectedInspectionRecord(0)("AppointmentType") = "Property" Then
                        commonAddressBO.Address = selectedInspectionRecord(0)("Address")
                    ElseIf selectedInspectionRecord(0)("AppointmentType") = "Scheme" Then
                        commonAddressBO.Address = selectedInspectionRecord(0)("Scheme")
                    Else
                        commonAddressBO.Address = selectedInspectionRecord(0)("Block")
                    End If
                    commonAddressBO.Area = selectedInspectionRecord(0)("COUNTY")
                    commonAddressBO.City = selectedInspectionRecord(0)("TownCity")
                    commonAddressBO.PatchId = selectedInspectionRecord(0)("PatchId")
                    commonAddressBO.PostCode = selectedInspectionRecord(0)("POSTCODE")
                    commonAddressBO.Street = selectedInspectionRecord(0)("Address")
                    commonAddressBO.PropertyId = selectedInspectionRecord(0)("PROPERTYID")
                    commonAddressBO.Telephone = selectedInspectionRecord(0)("Telephone")
                    SessionManager.setCommonAddressBO(commonAddressBO)
                End If

                SessionManager.setGroupBy(rbtnGroupTradeYes.Checked)
                Response.Redirect(PathConstants.DefectAvailableAppointments, False)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "btn Assign To Contractor Click"
    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim objAssignToContaractorBL As AssignToContractorBL = New AssignToContractorBL()
            Dim dsDefectAssignToContractor As DataSet = New DataSet()
            Dim btnAssignToContractor As Button = DirectCast(sender, Button)
            Dim lblDefectTrade As Label = DirectCast(btnAssignToContractor.NamingContainer.FindControl("lblDefectTrade"), Label)
            If (lblDefectTrade.Text = "N/A") Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorNoTradeExist, True)
            Else


                Dim defectId As Int32 = Convert.ToInt32(btnAssignToContractor.CommandArgument)
                objAssignToContaractorBL.getApplianceInfo(dsDefectAssignToContractor, defectId)
                Dim dtDefectAssignToContractor = dsDefectAssignToContractor.Tables(ApplicationConstants.DefectInfoDt)
                Dim objAssignToContractorBo As AssignToContractorBO = New AssignToContractorBO()


                If (dtDefectAssignToContractor.Rows.Count > 0) Then

                    objAssignToContractorBo.ReportedApplianceAndCategory = dtDefectAssignToContractor.Rows(0)("ApplianceAndCategory").ToString()
                    If (Not (String.IsNullOrEmpty(dtDefectAssignToContractor.Rows(0)("ApplianceId").ToString()))) Then
                        objAssignToContractorBo.ApplianceId = Convert.ToInt32(dtDefectAssignToContractor.Rows(0)("ApplianceId").ToString())
                    End If
                    objAssignToContractorBo.DefectWorksRequired = dtDefectAssignToContractor.Rows(0)("WorksRequired").ToString()
                    objAssignToContractorBo.DefaultNetCost = dtDefectAssignToContractor.Rows(0)("NetCost").ToString()
                    objAssignToContractorBo.DefaultVat = dtDefectAssignToContractor.Rows(0)("Vat").ToString()
                    objAssignToContractorBo.DefaultGross = dtDefectAssignToContractor.Rows(0)("Gross").ToString()
                    objAssignToContractorBo.DefaultVatId = dtDefectAssignToContractor.Rows(0)("VatRateId").ToString()
                    objAssignToContractorBo.PropertyId = dtDefectAssignToContractor.Rows(0)("PropertyId").ToString()
                    objAssignToContractorBo.RequestType = dtDefectAssignToContractor.Rows(0)("ReqType").ToString()
                    objAssignToContractorBo.DefectId = defectId

                    SessionManager.removeAssignToContractorBO()
                    SessionManager.setAssignToContractorBO(objAssignToContractorBo)

                    pnlAssignToContractor.Visible = True
                    ucAssignToContractor.PopulateControl()
                    popupAssignToContractor.Show()
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidDefectId, True)
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucAssignToContractor.CloseButtonClicked
        Try

            popupAssignToContractor.Hide()
            populateDefects()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region
#End Region

#Region "Functions"

#Region "Check if there is any fault slected"

    Function checkAnySelected(ByRef checkedDefectIdsList As List(Of Integer)) As Boolean

        Dim chkBoxDefect As CheckBox
        Dim btnDefectId As Button
        Dim defectId As Integer
        Dim isAnyChecked As Boolean = False

        For Each basketGridRow As GridViewRow In grdDefects.Rows

            chkBoxDefect = basketGridRow.FindControl("chkBoxDefect")
            If (chkBoxDefect.Checked = True) Then
                isAnyChecked = True

                'Get Defect Id from Grid row and add to list.
                btnDefectId = DirectCast(basketGridRow.FindControl("btnDefectId"), Button)
                defectId = Convert.ToInt32(btnDefectId.CommandArgument)
                checkedDefectIdsList.Add(defectId)
            End If

        Next

        Return isAnyChecked

    End Function

#End Region

#Region "Check if there is any fault slected"

    Function checkTradeExist() As Boolean

        Dim chkBoxDefect As CheckBox
        Dim lblDefectTrade As Label
        Dim isAnyChecked As Boolean = False

        For Each basketGridRow As GridViewRow In grdDefects.Rows

            chkBoxDefect = basketGridRow.FindControl("chkBoxDefect")
            If (chkBoxDefect.Checked = True) Then
                isAnyChecked = True
                'Get Defect Trade from Grid row to check trade exist or not.
                lblDefectTrade = DirectCast(basketGridRow.FindControl("lblDefectTrade"), Label)
                If lblDefectTrade.Text = "N/A" Then
                    Return False
                End If
            End If
        Next

        Return isAnyChecked

    End Function

#End Region

#Region "Populate Defects Grid"

    Private Sub populateDefects()

        Dim selectedInspectionRecord As DataTable = SessionManager.getSelectDefectInseptionRef()
        If (Not IsNothing(selectedInspectionRecord)) Then
            grdInspectionDetail.DataSource = selectedInspectionRecord
            grdInspectionDetail.DataBind()

            Dim journalId As Integer = selectedInspectionRecord(0)("JournalId")

            ' -----------------------------------------------------------------------
            ' Get Defect from database and bind to defects grid(child grid)
            Dim applianceDefectsDataSet As New DataSet()
            Dim defectsBL As New DefectsBL()
            defectsBL.GetDefectsByJournalId(applianceDefectsDataSet, journalId, defectCategoryId:=-1)
            grdDefects.DataSource = applianceDefectsDataSet
            grdDefects.DataBind()

            ' -----------------------------------------------------------------------

            ' -----------------------------------------------------------------------
            ' Save datatable in view state to get list of selected defects
            setDefectsDataTableViewState(applianceDefectsDataSet.Tables(0))
            ' -----------------------------------------------------------------------

            If applianceDefectsDataSet.Tables.Count > 0 AndAlso applianceDefectsDataSet.Tables(0).Rows.Count > 0 Then
                lblRecordedBy.Text = If(IsDBNull(applianceDefectsDataSet.Tables(0)(0)("EmployeeName")), "N/A", applianceDefectsDataSet.Tables(0)(0)("EmployeeName"))
            End If

            Dim appointmentsCount As Integer = 0
            lblJSDs.Text = String.Empty
            Dim defectIds As New StringBuilder
            Dim defectId As Integer
            For Each item In applianceDefectsDataSet.Tables(0).Rows
                If item("isAppointmentCreated") Then
                    appointmentsCount += 1
                    defectId = item("defectId")
                    lblJSDs.Text += String.Format("<br />JSD{0}", item("defectId").ToString().PadLeft(5, "0"))
                    defectIds.AppendFormat("{0},", defectId)
                End If
            Next

            If (appointmentsCount = applianceDefectsDataSet.Tables(0).Rows.Count And appointmentsCount <> 0) Then
                defectIds.Remove(defectIds.Length - 1, 1)
                defectsBL.confirmDefectAppointmentsByDefectIds(defectIds.ToString())
                mdlPoPUppnldefectJSDs.Show()
            End If

        End If

    End Sub

#End Region

#Region "Set Selected Defects In Session"

    Private Function setSelectedDefectsInSession(ByVal checkedDefectIdsList As List(Of Integer)) As Integer
        Dim defectsDataTableViewState As DataTable = getDefectsDataTableViewState()
        Dim totalSelected As Integer = 0

        If Not IsNothing(defectsDataTableViewState) Then

            Dim selectedDefects As DataTable = (From d In defectsDataTableViewState.AsEnumerable()
                                               Where checkedDefectIdsList.Contains(d.Field(Of Integer)("DefectId"))
                                               Select d).CopyToDataTable

            SessionManager.setSelectedDefectsDataTable(selectedDefects)

            totalSelected = selectedDefects.Rows.Count

        End If

        Return totalSelected

    End Function

#End Region

#End Region

#Region "View State Functions"

#Region "Defects Datatable/GridView View State Get, Set and Remove Method/Function(s)"

    Protected Sub setDefectsDataTableViewState(ByRef defectsDataTable As DataTable)
        ViewState(ViewStateConstants.DefectsDataTable) = defectsDataTable
    End Sub

    Protected Function getDefectsDataTableViewState() As DataTable
        Dim defectsDataTable As DataTable = Nothing
        If Not IsNothing(ViewState(ViewStateConstants.DefectsDataTable)) Then
            defectsDataTable = CType(ViewState(ViewStateConstants.DefectsDataTable), DataTable)
        End If
        Return defectsDataTable
    End Function

    Protected Sub removeDefectsDataTableViewState()
        ViewState.Remove(ViewStateConstants.DefectsDataTable)
    End Sub

#End Region

#End Region

#Region "Enable Disable Assign To Contractor Button"
    Public Function EnableDisableAssignToContractorButton(ByVal isAppointmentCreated, ByVal isAssignedToContractor)
        Dim enableButton As Boolean = True
        If (isAppointmentCreated = 1 Or isAssignedToContractor = 1) Then
            enableButton = False
        Else
            enableButton = True
        End If
        Return enableButton
    End Function
#End Region



End Class