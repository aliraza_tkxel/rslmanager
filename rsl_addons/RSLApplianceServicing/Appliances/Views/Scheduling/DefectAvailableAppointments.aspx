﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectAvailableAppointments.aspx.vb" Inherits="Appliances.DefectAvailableAppointments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
<style type="text/css" >
div.text_a_r_padd__r_0px
{
    text-align: right;
   
}
.panelDefectAppointmentsContainer
{
 width:100%;
 min-height:160px; 
 max-height:250px;
 overflow:auto;     
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="pageHeader ">
        Schedule Defects
    </p>
    <p>
        <strong>Next Available appointments for:
            <asp:Label ID="lblAddress" runat="server" />
            ,<asp:Label ID="lblTownCity" runat="server" />
            <asp:Label ID="lblPostCode" runat="server" />
            Tel:
            <asp:Label ID="lblTel" runat="server" /></strong></p>
    <asp:UpdatePanel ID="updPanelAvailableAppointments" runat="server" UpdateMode="Always"  >
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="clear">
            </div>
            <asp:Panel ID="pnlAppointmentSlotsContainer" runat="server" CssClass="defects-appointment-blocks-container">
            </asp:Panel>
            <div class="clear">
            </div>
            <div class="appointmentsMap" id="mapContainer">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressAvailableAppointments" runat="server" AssociatedUpdatePanelID="updPanelAvailableAppointments"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
