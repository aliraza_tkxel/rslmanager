﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class DefectScheduling
    Inherits PageBase

#Region "Events Handling"

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                If Not (IsNothing(Request.QueryString(PathConstants.Tab))) AndAlso Request.QueryString(PathConstants.Tab) = PathConstants.AppointmentArranged Then
                    lnkBtnAptTab_Click(sender, e)
                End If

                'Display initial view of report, Pre Req: MainView.ActiveViewIndex should be selected in page ui (.aspx file)
                populateSelectedReport()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Aptba Tab Click"

    Protected Sub lnkBtnAptbaTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass + " TabFirst"
            lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass + " TabLast"
            MainView.ActiveViewIndex = 0
            populateSelectedReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Apt Tab Click"

    Protected Sub lnkBtnAptTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass + " TabFirst"
            lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass + " TabLast"
            MainView.ActiveViewIndex = 1
            populateSelectedReport()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Text Seach Text Changed"

    Private Sub txtSearch_TextChanged(sender As Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            populateSelectedReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "ddl Categories Selected Index Changed"

    Private Sub ddlCategories_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCategories.SelectedIndexChanged
        Try
            populateSelectedReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Apply Filter to selected report"

    Private Sub populateSelectedReport()

        Select Case MainView.ActiveViewIndex
            Case 0
                appointmentToBeArranged.populateAppointmentToBeArrangedList(txtSearch.Text.Trim, ddlCategories.SelectedValue)
                ddlCategories.Visible = True
            Case 1
                ddlCategories.Visible = False
                appointmentArranged.populateAppointmentArrangedList(txtSearch.Text.Trim)
        End Select

    End Sub

#End Region

#End Region

End Class