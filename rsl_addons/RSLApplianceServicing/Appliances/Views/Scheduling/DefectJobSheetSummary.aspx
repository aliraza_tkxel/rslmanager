﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectJobSheetSummary.aspx.vb" Inherits="Appliances.DefectJobSheetSummary" %>

<%@ Register TagPrefix="defect" TagName="defectmanegement" Src="~/Controls/DefectManagement/DefectManagement.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" type="text/css" rel="Stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <asp:UpdatePanel runat="server" UpdateMode="Always" ID="updPanelDefectJobSheet" >
        <ContentTemplate>
            <p class="pageHeader ">
                Schedule Defects
            </p>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <span style="font-size: 14px;">Appointment Summary:</span>
            <hr />
            <asp:HiddenField ID="hidAppointmentId" runat="server" Value="-1" />
            <asp:GridView ID="grdDefects" runat="server" AutoGenerateColumns="false" ShowHeader="true"
                BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="0px" ForeColor="Black"
                GridLines="None" Width="100%" ShowHeaderWhenEmpty="true" AllowSorting="false">
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Appliance" SortExpression="Appliance">
                        <ItemTemplate>
                            <asp:Label ID="lblAppliance" Text='<%# Eval("Appliance:") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Trade:" SortExpression="DefectTrade">
                        <ItemTemplate>
                            <asp:Label ID="lblDefectTrade" Text='<%# Eval("Trade:") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="2 Person" SortExpression="isTwoPersonsJob">
                        <ItemTemplate>
                            <asp:Label ID="lblisTwoPersonsJob" Text='<%# Eval("2 Person:") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Disconnected?" SortExpression="isApplianceDisconnected">
                        <ItemTemplate>
                            <asp:Label ID="lblisApplianceDisconnected" Text='<%# Eval("Disconnected:") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Duration:" SortExpression="Duration">
                        <ItemTemplate>
                            <asp:Label ID="lblDefectDuration" Text='<%# Eval("Duration:") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Parts Due:" SortExpression="DefectPartsDue">
                        <ItemTemplate>
                            <asp:Label ID="lblPartsDue" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("Parts Due:")) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnAmendDefect" Text="Amend" runat="server" Visible='<%# defectsAmendAllowed %>'
                                OnClick="btnAmendDefect_Click" CommandArgument='<%# Eval("DefectId:").toString()+","+Eval("ReqType:") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:GridView>
            <div class="clear">
            </div>
            <hr />
            <div class="clear">
            </div>
            <asp:Panel ID="pnlAppointmentDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label1" runat="server" Text="Inspection Ref:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblInspectionRef" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label2" runat="server" Text="Operative:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label3" runat="server" Text="Duration:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label4" runat="server" Text="Trade:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label5" runat="server" Text="JSD:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblJSD" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label6" runat="server" Text="Start:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStartDateTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label7" runat="server" Text="End:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEndDateTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="clear">
            </div>
            <hr />
            <asp:Panel ID="pnlPropertyDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                            </td>
                            <td>
                                <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                            </td>
                            <td>
                                <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                            </td>
                            <td>
                                <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label13" runat="server" Text="Customer:"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label14" runat="server" Text="Telephone:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label15" runat="server" Text="Mobile:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label16" runat="server" Text="Email:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                            </td>
                            <td>
                                <div style="text-align: right;">
                                    <asp:Button ID="btnUpdateCustomerDetails" runat="server" Text="Update Customer Details" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear">
                </div>
            </asp:Panel>
            <div class="clear">
            </div>
            <hr />
            <div class="clear">
            </div>
            <asp:Panel ID="pnlNotes" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                Customer Appointment Notes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                Job Sheet Notes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both">
                </div>
                <br />
                <div style="width: 100%;">
                    <div style="text-align: left; float: left; margin-left: 10px;">
                        <asp:Button ID="btnBack" runat="server" Text="< Back" PostBackUrl="~/Views/Scheduling/DefectAvailableAppointments.aspx" />
                    </div>
                    <asp:Panel ID="pnlScheduleAppointmentButtons" runat="server">
                        <div style="text-align: right; float: right; margin-right: 50px;">
                            <asp:Button ID="btnScheduleAppointment" runat="server" Text="Schedule Appointment" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlArrangeAppointmentButtons" runat="server" Visible="false">
                        <div style="text-align: right; float: right; margin-right: 50px;">
                            <asp:Button ID="btnReArrangeAppointment" runat="server" Text="Rearrange Appointment" />
                        </div>
                        <div style="text-align: right; float: right; margin-right: 50px;">
                            <asp:Button ID="btnCancelAppointment" runat="server" Text="Cancel Appointment" />
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="pnlConnfirmCancel" runat="server" BackColor="White" Width="400px"
                BorderColor="Black" BorderStyle="Outset" BorderWidth="1px" Style=" padding:10px;">
                <strong>The appointment will be cancelled and added to the Cancelled Defects report.</strong>
                <br />
                <br />
                <div style="text-align: center; width: 100%">
                    Cancellation Reason<br />
                    <asp:TextBox runat="server" ID="txtCancelReason" Width="95%" TextMode="MultiLine"
                        Rows="4" Style="resize: none;" />
                    <asp:RequiredFieldValidator ID="rfvCancelReason" ErrorMessage="Please provide cancel reason."
                        ControlToValidate="txtCancelReason" runat="server" ValidationGroup="cancelAppoitment"
                        InitialValue="" SetFocusOnError="true" CssClass="Required" Display="Dynamic" />
                    <asp:CustomValidator ID="cvCancelReason" runat="server" ControlToValidate="txtCancelReason"
                        ErrorMessage="Cancel reason must not exceed 1000 characters." Display="Dynamic"
                        ForeColor="Red" ValidationGroup="cancelAppoitment" EnableClientScript="False" />
                    <br />
                    <asp:Button ID="btnConfrimCancel" Text="Cancel Appointment" runat="server" ValidationGroup="cancelAppoitment"
                        CausesValidation="true" />
                    <asp:Button ID="btnCloseCancelpopup" Text="Close" runat="server" />
                </div>
            </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mdlConfirmCancel" runat="server" Enabled="True"
        TargetControlID="lblDisplayCancelPopup" PopupControlID="pnlConnfirmCancel" DropShadow="true"
        BackgroundCssClass="modalBackground" CancelControlID="btnCloseCancelpopup">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblDisplayCancelPopup" runat="server"></asp:Label>
    <%-- Defect Detail POPUP - Start --%>
    <asp:Panel ID="pnlAddDefect" runat="server" BackColor="White" Width="530px" Style="min-height: 550px; padding:10px;"
        BorderColor="Black" BorderStyle="Outset" BorderWidth="1px">
        <defect:defectmanegement runat="server" ID="ucDefectManagement" />
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mdlPoPUpAddDefect" runat="server" Enabled="True"
        TargetControlID="lblDispDefect" PopupControlID="pnlAddDefect" DropShadow="true"  BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblDispDefect" runat="server"></asp:Label>
    <%-- Defect Detail POPUP - End --%>

    <asp:UpdateProgress ID="updateProgressDefectJobSheet" runat="server" AssociatedUpdatePanelID="updPanelDefectJobSheet"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
