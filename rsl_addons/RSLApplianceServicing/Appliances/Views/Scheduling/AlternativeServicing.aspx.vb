﻿Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AlternativeServicing
    Inherits PageBase

    Public ckBoxDocumentUploadApta As New CheckBox()
    Public ckBoxDocumentUploadAptba As New CheckBox()
    Public ckBoxRefreshDataSetApta As New CheckBox()
    Public ddlLetterApta As New DropDownList

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.FindControlsOfUserControl()
            SessionManager.RemoveServicingTypeForOilAlternative()
            Dim servicingType As ServicingType = New ServicingType(False, True)
            SessionManager.SetServicingTypeForOilAlternativ(servicingType)
            If (Not IsPostBack) Then
                ckBoxDueWithIn56Days.Checked = False
                SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
                Me.GetStatusType(ddlStatusType)
                Me.GetAlternativeFuelType(ddlFuelType)
                Me.setupDefaults()
                PopulateFuelServicingTab()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub ckBoxDueWithIn56Days_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDueWithIn56Days.CheckedChanged
        Try
            SessionManager.setDueWithIn56Days(ckBoxDueWithIn56Days.Checked)
            If (MainView.ActiveViewIndex = 0) Then
                FuelAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)
            ElseIf (MainView.ActiveViewIndex = 1) Then
                AlternativeAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            If (MainView.ActiveViewIndex = 0) Then
                FuelAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)
            ElseIf (MainView.ActiveViewIndex = 1) Then
                AlternativeAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#Region "Dropdown List Fuel Type Selected Index Changed Event"
    ''' <summary>
    ''' Dropdown List Fuel Type Selected Index Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlFuelType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            changeFuelType()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try

    End Sub

#End Region

#Region "lnk Btn Aptba Tab Click"

    Protected Sub lnkBtnAptbaTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentToBeArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Apt Tab Click"
    Protected Sub lnkBtnAptTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Populate Fuel Servicing Tab"

    Private Sub PopulateFuelServicingTab()
        If Request.QueryString(PathConstants.Tab) IsNot Nothing AndAlso Request.QueryString(PathConstants.Tab) <> String.Empty Then

            If Request.QueryString(PathConstants.Tab).Equals(PathConstants.AppointmentArranged) Then
                populateAppointmentArrangedList()
            Else
                populateAppointmentToBeArrangedList()
            End If

        Else
            populateAppointmentToBeArrangedList()
        End If
    End Sub

#End Region

#Region "Change Fuel Type"
    ''' <summary>
    ''' Change Fuel Type
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeFuelType()
        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateAppointmentArrangedList()
        End If
    End Sub

#End Region

#Region "Change Status Type"
    ''' <summary>
    ''' Change Fuel Type
    ''' </summary>
    ''' <remarks></remarks>
    Sub changeStatusType()
        If (MainView.ActiveViewIndex = 0) Then
            populateAppointmentToBeArrangedList()
        ElseIf (MainView.ActiveViewIndex = 1) Then
            populateAppointmentArrangedList()
        End If
    End Sub

#End Region

#Region "Populate Appointment To Be Arranged"

    Private Sub PopulateAppointmentToBeArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        pnlStatus.Visible = True
        FuelAppointToBeArranged.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue, ddlStatusType.SelectedItem.Text)
    End Sub

#End Region

#Region "Setup Defaults"
    Sub setupDefaults()
        ddlFuelType.Items.FindByText(ApplicationConstants.DefautAlternativeFuelType).Selected = True
    End Sub
#End Region

#Region "Populate Appointment Arranged"

    Private Sub PopulateAppointmentArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 1
        ddlFuelType.Visible = True
        pnlStatus.Visible = False
        AlternativeAppointmentArranged.searchAppointmentArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Text, ddlFuelType.SelectedValue)
    End Sub
#End Region

#Region "Load Alternative Fuel Type"

    Private Sub GetAlternativeFuelType(ByVal ddl As DropDownList)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.GetAlternativeFuelType(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        Dim itemToRemove As ListItem
        itemToRemove = ddl.Items.FindByValue("901")
        If itemToRemove IsNot Nothing Then
            ddl.Items.Remove(itemToRemove)
        End If
        ddl.Items.Insert(0, New ListItem(ApplicationConstants.DefautAlternativeFuelType, -1))

    End Sub
#End Region

#Region "Load Status Type"
    Protected Sub GetStatusType(ByVal ddl As DropDownList)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getStatus(resultDataSet)
        ddl.DataSource = resultDataSet
        ddl.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddl.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("All", -1))

    End Sub
#End Region

#Region "Find Contorls"

    Private Sub FindControlsOfUserControl()

        ckBoxDocumentUploadApta = DirectCast(AlternativeAppointmentArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ckBoxDocumentUploadAptba = DirectCast(FuelAppointToBeArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ddlLetterApta = DirectCast(AlternativeAppointmentArranged.FindControl("ddlLetter"), DropDownList)
        ckBoxRefreshDataSetApta = DirectCast(AlternativeAppointmentArranged.FindControl("ckBoxRefreshDataSet"), CheckBox)

    End Sub
#End Region

#Region "Web Method - Get Existing Appointments Data (Property Address)"

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function getExistingAppointmentsData() As String
        Dim returnJSON As New StringBuilder()
        Dim serializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim appointmentsForMap = SessionManager.getExistingAppointmentsForSchedulingMap()

        Dim rows = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        If Not IsNothing(appointmentsForMap) Then
            For Each dr As DataRow In appointmentsForMap.Rows
                row = New Dictionary(Of String, Object)()
                For Each col As DataColumn In appointmentsForMap.Columns
                    row.Add(col.ColumnName, dr(col))
                Next
                rows.Add(row)
            Next
        End If

        Return serializer.Serialize(rows)
    End Function

#End Region

    Protected Sub ddlStatusType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatusType.SelectedIndexChanged
        Try
            changeFuelType()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try
    End Sub

End Class