﻿Imports System.Drawing
Imports System.Net.Mail
Imports System.Threading
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_BusinessLogic
Imports AjaxControlToolkit
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.DateTime


Public Class AlternativeArrangedAppointments
    Inherits PageBase

#Region "Properties"

    ''' <summary>
    ''' This object 'll retain the values of planned appointment bo
    ''' </summary>
    ''' <remarks></remarks>
    Private _selectedPropertySchedulingBo As SelectedPropertySchedulingBO
    Public Property SelectedPropertySchedulingBo As SelectedPropertySchedulingBO
        Get
            Return _selectedPropertySchedulingBo
        End Get
        Set(ByVal value As SelectedPropertySchedulingBO)
            _selectedPropertySchedulingBo = value
        End Set
    End Property

    Dim objAlternativeSchedulingBL As AlternativeSchedulingBL = New AlternativeSchedulingBL()
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

    Private _lstAlternativeSchedullingSlotHeader As List(Of OilAlternativeSchedullingSlotHeader)
    Public Property LstAlternativeSchedullingSlotHeader As List(Of OilAlternativeSchedullingSlotHeader)
        Get
            Return _lstAlternativeSchedullingSlotHeader
        End Get
        Set(ByVal value As List(Of OilAlternativeSchedullingSlotHeader))
            _lstAlternativeSchedullingSlotHeader = value
        End Set
    End Property


    ''' <summary>
    ''' This key 'll retain the operatives, their leaves and their appointments
    ''' </summary>
    ''' <remarks></remarks>
    Private _availableOperatives As DataSet
    Public Property AvailableOperativesDs() As DataSet
        Get
            Return _availableOperatives
        End Get
        Set(ByVal value As DataSet)
            _availableOperatives = value
        End Set
    End Property

    Dim _journalId As Int32
    Dim _appointmentId As Int32
#End Region

    '#Region "Events"

    '#Region "Page Load"
    '    ''' <summary>
    '    ''' Event handler for page load
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            'get values from session
            GetValuesFromSession()
            SetPropertyHeader()

            If Request.QueryString("jourId") IsNot Nothing Then
                _journalId = CType(Request.QueryString("jourId"), Int32)
            End If

            If Request.QueryString("apptId") IsNot Nothing Then
                _appointmentId = CType(Request.QueryString("apptId"), Int32)
            End If

            If (Not IsPostBack) Then

                If Not IsNothing(SelectedPropertySchedulingBo) Then
                    Dim operativeDs As DataSet = New DataSet()
                    Me.GetAvailableOperatives(operativeDs)

                    'if duration is change while scheduling it will be reflected in Data tables in this function
                    SetUpdatedComponentDuration()

                    PopulateAlternativeAppointments(SelectedPropertySchedulingBo, operativeDs, Date.Now, _journalId, _appointmentId)
                End If

            ElseIf (uiMessageHelper.IsError = False) Then
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary trade and appointment list (this is requirement of dynamic controls on post back)
                Me.ReDrawTempAlternativeAppointmentGrid()

                Dim appointmentInfo As String() = SessionManager.GetAppointmentTypeInfo()
                If Not IsNothing(appointmentInfo) Then
                    ShowAmendAppointment(appointmentInfo)
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Private Sub ReDrawTempAlternativeAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary trade and appointment list
        Dim alternativeSchedullingSlot = SessionManager.GetAlternativeSchedulingBo()
        BindTempAlternativeAppointment(alternativeSchedullingSlot)
    End Sub

    Private Sub SetPropertyHeader()
        If Not IsNothing(SelectedPropertySchedulingBo) Then
            lblStreetAddress.Text = SelectedPropertySchedulingBo.PropertyAddress
            lblPostCode.Text = SelectedPropertySchedulingBo.PostCode
        End If
    End Sub

#Region "Intelligent Scheduling Principal Popup - Functions"
#Region "get Available Operatives"
    ''' <summary>
    ''' This function retrieves the available operatives, their appointments and their leaves
    ''' </summary>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>
    Private Sub GetAvailableOperatives(ByRef operativeDs As DataSet)

        objAlternativeSchedulingBL.GetAvailableOperativesForAlternativeOilFuel(operativeDs, "Alternative")

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            SessionManager.setAvailableOperativesDs(operativeDs)
        End If
    End Sub
#End Region

#Region "Populate Appointment"
    ''' <summary>
    ''' This function calls the business logic layer and creates the appointment slots and 
    ''' then binds that datatable with grid
    ''' </summary>
    ''' <param name="propertySchedulingBo"></param>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>

    Private Function PopulateAlternativeAppointments(ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal operativeDs As DataSet, ByVal startDate As DateTime, ByVal journalId As Integer, ByVal appointmentId As Integer)

        Dim alternativeSchedullingSlot = New AlternativeSchedullingSlot

        alternativeSchedullingSlot = objAlternativeSchedulingBL.GetArrangedAppointmentsForAlternative(operativeDs.Tables(ApplicationConstants.OperativesDt), operativeDs.Tables(ApplicationConstants.LeavesDt), operativeDs.Tables(ApplicationConstants.AppointmentsDt), propertySchedulingBo, startDate, journalId, appointmentId)
        SessionManager.SetAlternativeSchedulingBo(alternativeSchedullingSlot)
        LstAlternativeSchedullingSlotHeader = SessionManager.GetAlternativeSchedulingHeaderBoList()
        'Bind the appointment slots with asp.net control
        BindTempAlternativeAppointment(alternativeSchedullingSlot)
    End Function
#End Region
#End Region

#Region "Get Values From Session"
    ''' <summary>
    ''' This function 'll be used to get the values from session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetValuesFromSession()
        SelectedPropertySchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()
        LstAlternativeSchedullingSlotHeader = SessionManager.GetAlternativeSchedulingHeaderBoList()
    End Sub
#End Region

#Region "Bind Temp Trade Appointments"
    ''' <summary>
    ''' This funciton 'll bind the data with temporary trade and appointments block
    ''' </summary>
    ''' <param name="alternativeSchedullingSlot"></param>
    ''' <remarks></remarks>
    Private Sub BindTempAlternativeAppointment(ByVal alternativeSchedullingSlot As AlternativeSchedullingSlot)

        If IsNothing(alternativeSchedullingSlot) Then
            'Me.unHideBackButton()
        Else
            'LstAlternativeSchedullingSlotHeader = SessionManager.GetAlternativeSchedulingHeaderBoList()
            'panel control 
            Dim panelContainer As Panel
            ' We are considering trade and its respective operative list as block so 
            'these varaibles 'll be used to recognize each block when some button/checkbox 'll be clicked e.g refresh list button
            Dim uniqueControlId As Integer = 0
            Dim uniqueControlIdString As String = uniqueControlId.ToString()

            'create container for every block of temporary Trades and appointment 
            panelContainer = Me.CreateContainer(uniqueControlIdString)

            'Create object of Appointment grid
            Dim tempAppointmentGrid As New GridView
            tempAppointmentGrid = Me.CreateTempAppointmentsHeaderGrid(tempAppointmentGrid, uniqueControlIdString, alternativeSchedullingSlot)
            'Add the GridView control object dynamically into the div control
            panelContainer.Controls.Add(tempAppointmentGrid)

            'Create object of appointment grid
            Dim tempAptGrid As New GridView()
            'Add the GridView control into the div control/panel
            tempAptGrid = Me.CreateTempAppointmentGrids(tempAptGrid, uniqueControlIdString, alternativeSchedullingSlot)
            panelContainer.Controls.Add(tempAptGrid)

            'Add this panel container to div page container
            pnlPageContainer.Controls.Add(panelContainer)

        End If
    End Sub
#End Region

#Region "create Temp Appointment Grids"
    ''' <summary>
    ''' This function 'll create the temporary appointment grid on the basis of unique block id and on data of TempTradeAppointmentBO
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateTempAppointmentGrids(ByVal tempAptGrid As GridView, ByVal uniqueBlockId As String, ByVal item As AlternativeSchedullingSlot)
        Dim isEmptyGrid As Boolean = False

        tempAptGrid.ID = ApplicationConstants.TempAppointmentGridControlId + uniqueBlockId.ToString()
        tempAptGrid.ShowFooter = True
        tempAptGrid.EnableViewState = True
        tempAptGrid.RowStyle.BackColor = ColorTranslator.FromHtml("#E6E6E6")
        tempAptGrid.AlternatingRowStyle.BackColor = ColorTranslator.FromHtml("#FFFFFF")
        tempAptGrid.CssClass = "grid_appointment available-apptmts-optable"
        tempAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
        'Check if , are there any appointments or not .If not then display message
        If item.alternativeAppointmentSlotsDtBO.dt.Rows.Count = 0 Then
            Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoOperativesExistWithInTime)
            isEmptyGrid = True
        Else
            tempAptGrid.DataSource = item.alternativeAppointmentSlotsDtBO.dt
            tempAptGrid.DataBind()
            'add row span on footer and add back button, refresh list button and view calendar button
            tempAptGrid.FooterRow.Cells(0).Controls.Add(CreateAppointmentFooterControls(uniqueBlockId, item))
            Dim headerRow As GridViewRow
            headerRow = Me.CreateFilterHeader(uniqueBlockId.ToString(), item.alternativeAppointmentSlotsDtBO.IsPatchSelected, item.alternativeAppointmentDtBO.IsDateSelected, item.StartSelectedDate)

            Dim t As Table = TryCast(tempAptGrid.Controls(0), Table)
            If t IsNot Nothing Then
                t.Rows.AddAt(0, headerRow)
            End If
        End If

        'after adding the buttons add col span to footer so that buttons should appear according to design
        If tempAptGrid.Rows.Count > 0 Then
            Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
            For i As Integer = m - 1 To 1 Step -1
                tempAptGrid.FooterRow.Cells.RemoveAt(i)
            Next i
            tempAptGrid.FooterRow.Cells(0).ColumnSpan = 6 '6 is the number of visible columns to span.
        End If

        'declare the variables that 'll store the information required to bind with select button in grid                
        Dim operativeId As String = String.Empty
        Dim operativeName As String = String.Empty
        Dim startDateString As String = String.Empty
        Dim endDateString As String = String.Empty
        Dim distance As String = String.Empty
        Dim patch As String = String.Empty
        Dim dtCounter As Integer = 0
        Dim completeAppointmentInfo As String = String.Empty
        tempAptGrid.ShowHeader = False
        'remove the header of unwanted columns
        tempAptGrid.HeaderRow.Cells(5).Visible = False
        tempAptGrid.HeaderRow.Cells(6).Visible = False
        tempAptGrid.HeaderRow.Cells(7).Visible = False
        tempAptGrid.HeaderRow.Cells(8).Visible = False
        tempAptGrid.HeaderRow.Cells(9).Visible = False
        tempAptGrid.HeaderRow.Cells(10).Visible = False
        'if grid is not emtpy then remove the cells and also create select button and set command argument with that
        If isEmptyGrid = False Then
            'run the loop on grid view rows to remove the cells from each row 
            'and also to add the select button in each row
            For Each gvr As GridViewRow In tempAptGrid.Rows
                'gvr.Cells(5).Visible = False
                gvr.Cells(6).Visible = False
                gvr.Cells(7).Visible = False
                gvr.Cells(8).Visible = False
                gvr.Cells(9).Visible = False
                gvr.Cells(10).Visible = False
                'get the information that should be set with select button                                             
                operativeId = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.OperativeIdColName)
                operativeName = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.OperativeColName)
                patch = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.PatchColName)
                startDateString = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.StartDateStringColName)
                endDateString = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.EndDateStringColName)
                distance = item.alternativeAppointmentSlotsDtBO.dt.Rows(dtCounter).Item(item.alternativeAppointmentSlotsDtBO.DistanceColName)
                dtCounter += 1

                'save this operative and time slot information in a string, after that it 'll be bind with select button
                completeAppointmentInfo = operativeId.ToString() _
                    + Me.appointmentInfoSeparater + operativeName _
                    + Me.appointmentInfoSeparater + startDateString _
                    + Me.appointmentInfoSeparater + endDateString _
                    + Me.appointmentInfoSeparater + item.alternativeAppointmentDtBO.AppointmentDuration

                'Add the select button 
                Dim btnSelectOperative As Button = New Button()
                btnSelectOperative.ID = "btnSelectOperative" + uniqueBlockId.ToString()
                btnSelectOperative.Text = "Select"
                btnSelectOperative.CssClass = "row-button"
                btnSelectOperative.CommandArgument = completeAppointmentInfo
                AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                gvr.Cells(5).Controls.Add(btnSelectOperative)

            Next
        End If
        Return tempAptGrid
    End Function

    Private Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            resetAmendAppointmentControls()
            Dim btnSelectOperative As Button = DirectCast(sender, Button)
            Dim appointmentInfo As String() = CType(btnSelectOperative.CommandArgument, String).Split(";")
            SessionManager.RemoveAppointmentTypeInfo()
            SessionManager.SetAppointmentTypeInfo(appointmentInfo)
            'this function will set the appropriate values 
            Me.ShowAmendAppointment(appointmentInfo)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region


    Private Sub ShowAmendAppointment(ByVal appointmentInfo As String())
        Try
            Me.openAmendAppointmentPopup(appointmentInfo)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub

    ''' <summary>
    ''' this spearator 'll be used to bind the information of appointment with select button
    ''' </summary>
    ''' <remarks></remarks>
    Private appointmentInfoSeparater As String = ";"

#Region "create Filter Header"
    Private Function CreateFilterHeader(ByVal uniqueBlockId As String, ByVal isPatchSelected As Boolean, ByVal isDateSelected As Boolean, ByVal startSelectedDate As Date)

        Dim headerRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        Dim headerCell As New TableCell()
        headerCell.ColumnSpan = 6
        headerCell.Style.Add("background-color", "lightgray")
        headerCell.Style.Add("font-weight", "bold")
        headerCell.CssClass = "available-apptmts-frow"

        'add filter div
        Dim divFilter As HtmlGenericControl = New HtmlGenericControl("div")
        divFilter.InnerText = "Filter Available Operatives By: "
        divFilter.Attributes.Add("class", "grid_Defects_filter")
        headerCell.Controls.Add(divFilter)

        'add patch label
        Dim divPatch As HtmlGenericControl = New HtmlGenericControl("div")
        divPatch.InnerText = "Patch: "
        divPatch.Attributes.Add("class", "grid_Defects_patch")

        'add patch checkbox
        Dim chkPatch As CheckBox = New CheckBox()
        chkPatch.ID = "chkPatch"
        chkPatch.AutoPostBack = True
        chkPatch.Checked = isPatchSelected
        chkPatch.Attributes("CommandArgument") = uniqueBlockId
        AddHandler chkPatch.CheckedChanged, AddressOf chkPatch_CheckedChanged

        'add check box to patch label
        divPatch.Controls.Add(chkPatch)
        headerCell.Controls.Add(divPatch)

        'add Expiry Date label
        Dim divExpiryDate As HtmlGenericControl = New HtmlGenericControl("div")
        divExpiryDate.InnerText = "Expiry Date:"
        divExpiryDate.Attributes.Add("class", "grid_Defects_patch")

        'add Expiry Date checkbox
        Dim chkExpiryDate As CheckBox = New CheckBox()
        chkExpiryDate.ID = "chkExpiryDate"
        chkExpiryDate.AutoPostBack = True
        chkExpiryDate.Checked = isDateSelected
        chkExpiryDate.Attributes("CommandArgument") = uniqueBlockId

        'There are some properties which does not have any certificate expiry date for that this check is implemented
        'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
        If IsNothing(SelectedPropertySchedulingBo.CertificateExpiryDate) Then
            chkExpiryDate.Enabled = False
        End If

        AddHandler chkExpiryDate.CheckedChanged, AddressOf chkExpiryDate_CheckedChanged

        'add check box to Expiry date label
        divExpiryDate.Controls.Add(chkExpiryDate)
        headerCell.Controls.Add(divExpiryDate)

        'add Certificate Expires label
        Dim divCertificateExpiry As HtmlGenericControl = New HtmlGenericControl("div")
        divCertificateExpiry.InnerText = "Certificate Expires In: " + "" + "days"
        divCertificateExpiry.Attributes.Add("style", "float: right;")
        divCertificateExpiry.Attributes.Add("class", "grid_Defects_patch")

        headerCell.Controls.Add(divCertificateExpiry)

        'add From Date label
        Dim divFromDate As HtmlGenericControl = New HtmlGenericControl("div")
        divFromDate.InnerText = "From Date: "
        divFromDate.Attributes.Add("class", "grid_Defects_patch")

        'date text box
        Dim txtDate As TextBox = New TextBox()
        txtDate.ID = ApplicationConstants.AppointmentStartDateControlId + uniqueBlockId
        txtDate.Text = startSelectedDate.Date
        txtDate.AutoPostBack = True
        txtDate.Attributes("CommandArgument") = uniqueBlockId
        AddHandler txtDate.TextChanged, AddressOf fromDateTextChange_TextChanged

        'image button
        Dim imgCalDate As HtmlImage = New HtmlImage()
        imgCalDate.ID = ApplicationConstants.CalDateControlId + uniqueBlockId
        imgCalDate.Src = "../../Images/calendar.png"
        'calendar extendar
        Dim calExtStartDate As CalendarExtender = New CalendarExtender()
        calExtStartDate.PopupButtonID = imgCalDate.ID
        calExtStartDate.PopupPosition = CalendarPosition.Right
        calExtStartDate.TargetControlID = txtDate.ID
        calExtStartDate.ID = ApplicationConstants.CalExtDateControlId + uniqueBlockId
        calExtStartDate.Format = "dd/MM/yyyy"
        calExtStartDate.StartDate = DateTime.Now

        divFromDate.Controls.Add(txtDate)
        divFromDate.Controls.Add(imgCalDate)
        divFromDate.Controls.Add(calExtStartDate)

        headerCell.Controls.Add(divFromDate)

        'add the addition in cell to header row
        headerRow.Cells.Add(headerCell)
        Return headerRow
    End Function

#End Region

#Region "From Date Text Changed "

    Private Sub fromDateTextChange_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim fromDate As TextBox = TryCast(sender, TextBox)
            Dim startAgain As Boolean = False
            Dim selectedAptBlockId As Integer = Integer.Parse(fromDate.Attributes("CommandArgument"))

            If (Not IsNothing(fromDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(fromDate.Text)
                If (Not IsNothing(fromDate)) Then
                    startDate = GeneralHelper.getUKCulturedDateTime(fromDate.Text)
                End If
                If fromDate.Text > Now.Date Then
                    startAgain = True
                End If
            End If

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            RefreshTheOpertivesList(selectedAptBlockId, startDate, startAgain)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub

#End Region

#Region "Check Expiry Date Checked Changed "

    Private Sub chkExpiryDate_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chkExpiry As CheckBox = TryCast(sender, CheckBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False
            Dim selectedAptBlockId As Integer = Integer.Parse(chkExpiry.Attributes("CommandArgument"))
            SelectedPropertySchedulingBo.IsDateSelected = chkExpiry.Checked
            SessionManager.RemoveSelectedPropertySchedulingBo()
            SessionManager.SetSelectedPropertySchedulingBo(SelectedPropertySchedulingBo)

            If (chkExpiry.Checked) Then
                'get the start date which is selected against this trade
                Dim txtStartDate As TextBox = CType(chkExpiry.Parent.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedAptBlockId.ToString()), TextBox)
                If (Not IsNothing(txtStartDate)) Then
                    startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                    If (Not IsNothing(txtStartDate)) Then
                        startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                    End If
                    'If txtStartDate.Text > Now.Date Then
                    '    startAgain = True
                    'End If
                End If
            End If
            chkExpiry.Checked = chkExpiry.Checked
            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            RefreshTheOpertivesList(selectedAptBlockId, startDate, startAgain)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub

#End Region

#Region "Check Patch Checked Changed "

    Private Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try

            Dim chkPatch As CheckBox = TryCast(sender, CheckBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False

            Dim selectedAptBlockId As Integer = Integer.Parse(chkPatch.Attributes("CommandArgument"))
            SelectedPropertySchedulingBo.IsPatchSelected = chkPatch.Checked
            SessionManager.RemoveSelectedPropertySchedulingBo()
            SessionManager.SetSelectedPropertySchedulingBo(SelectedPropertySchedulingBo)

            If (chkPatch.Checked) Then
                'get the start date which is selected against this trade
                Dim txtStartDate As TextBox = CType(chkPatch.Parent.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedAptBlockId.ToString()), TextBox)

                If (Not IsNothing(txtStartDate)) Then
                    startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                End If
                'If txtStartDate.Text > Now.Date Then
                '    startAgain = True
                'End If
            End If

            chkPatch.Checked = chkPatch.Checked

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            RefreshTheOpertivesList(selectedAptBlockId, startDate, startAgain)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try

    End Sub

#End Region

#Region "Clear Appointments Slots Containter"

    Private Sub ClearAppointmentSlotsContainer()
        pnlPageContainer.Controls.Clear()
    End Sub

#End Region

#Region "Create Temp Appointments Header Grid"
    ''' <summary>
    ''' This function 'll create the temporary appointment grid on the basis of unique id and on data of AlternativeSchedullingSlot
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <param name="item"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateTempAppointmentsHeaderGrid(ByVal tempTradeGrid As GridView, ByVal uniqueBlockId As String, ByVal item As AlternativeSchedullingSlot)

        tempTradeGrid.ID = ApplicationConstants.TempGridControlId + uniqueBlockId
        tempTradeGrid.EnableViewState = True
        tempTradeGrid.CssClass = "grid_temp_trade"
        tempTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow"

        tempTradeGrid.DataSource = item.alternativeAppointmentDtBO.dt
        tempTradeGrid.DataBind()

        'hide the last column from temporary trade grid(that are not required in grid view) because those columns 
        'were there in datatable and those columns have been automatically created, when we bound datatable with grid view
        'the columns of these data table 'll be used through sessoion
        tempTradeGrid.HeaderRow.Cells(4).Visible = False
        tempTradeGrid.HeaderRow.Cells(5).Visible = False

        Dim uniqueControlId As Integer = uniqueBlockId
        'run the loop on grid view rows to remove the cells (that are not required in grid view) from each row 
        'and also to add the select button in each row
        For Each gvr As GridViewRow In tempTradeGrid.Rows
            gvr.Cells(4).Visible = False
            gvr.Cells(5).Visible = False
            gvr.Cells(1).Controls.Add(createDurationControls(uniqueControlId, item, LstAlternativeSchedullingSlotHeader))
            uniqueControlId = uniqueControlId + 1
        Next
        Return tempTradeGrid
    End Function
#End Region

#Region "Create Duration Controls"
    ''' <summary>
    ''' This function 'll create the duration control on the top of trade grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createDurationControls(ByVal uniqueBlockId As String, ByVal item As AlternativeSchedullingSlot, ByVal alternativeSchedullingSlotHeaders As List(Of OilAlternativeSchedullingSlotHeader))

        'Edit ImageButton
        Dim imgBtnEdit As ImageButton = New ImageButton()
        imgBtnEdit.ID = ApplicationConstants.EditImageButtonControlId + uniqueBlockId.ToString()
        imgBtnEdit.ImageUrl = "../../Images/edit.png"
        imgBtnEdit.CssClass = "temp-trade-content-left"
        imgBtnEdit.Visible = True
        imgBtnEdit.CommandArgument = uniqueBlockId
        AddHandler imgBtnEdit.Click, AddressOf imgBtnEdit_Click

        'Duration Label
        Dim lblDuration As Label = New Label()
        lblDuration.ID = ApplicationConstants.DurationLabelControlId + uniqueBlockId.ToString()
        lblDuration.Text = IIf(Convert.ToDouble(alternativeSchedullingSlotHeaders(uniqueBlockId).AppointmentDuration) > 1, alternativeSchedullingSlotHeaders(uniqueBlockId).AppointmentDuration + " hours", alternativeSchedullingSlotHeaders(uniqueBlockId).AppointmentDuration + " hour")
        lblDuration.Visible = True

        'Duration Dropdown
        Dim ddlDuration As DropDownList = New DropDownList()
        ddlDuration.ID = ApplicationConstants.DurationDropdownControlId + uniqueBlockId.ToString()
        ddlDuration.Visible = False
        PopulateDurationDropdownList(ddlDuration)
        ddlDuration.SelectedValue = Convert.ToDouble(alternativeSchedullingSlotHeaders(uniqueBlockId).AppointmentDuration)

        'Done ImageButton
        Dim imgBtnDone As ImageButton = New ImageButton()
        imgBtnDone.ID = ApplicationConstants.DoneImageButtonControlId + uniqueBlockId.ToString()
        imgBtnDone.ImageUrl = "../../Images/done.png"
        imgBtnDone.Visible = False
        imgBtnDone.CommandArgument = uniqueBlockId + "," + LstAlternativeSchedullingSlotHeader(uniqueBlockId).FuelType
        AddHandler imgBtnDone.Click, AddressOf imgBtnDone_Click

        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-trade-content"
        pnl.ID = ApplicationConstants.DurationPanelControlId + uniqueBlockId.ToString()
        pnl.Controls.Add(imgBtnEdit)
        pnl.Controls.Add(lblDuration)
        pnl.Controls.Add(ddlDuration)
        pnl.Controls.Add(imgBtnDone)
        Return pnl

    End Function
#End Region

#Region "Image btn Edit Click"

    Private Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnEdit As ImageButton = DirectCast(sender, ImageButton)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(selectedImgBtnEdit.CommandArgument)

            Dim row As GridViewRow = DirectCast(selectedImgBtnEdit.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim imgBtnEdit As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.EditImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)
            Dim lblDuration As Label = CType(pnlDuration.FindControl(ApplicationConstants.DurationLabelControlId + selectedTradeAptBlockId.ToString()), Label)
            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)
            imgBtnDone.Attributes.Add("style", "margin-top: -20px;")
            imgBtnEdit.Visible = False
            lblDuration.Visible = False
            ddlDuration.Visible = True
            imgBtnDone.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image btn Done Click"

    Private Sub imgBtnDone_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnDone As ImageButton = DirectCast(sender, ImageButton)
            Dim blockInfo() As String = selectedImgBtnDone.CommandArgument.Split(",")

            Dim selectedTradeAptBlockId As Integer = Integer.Parse(blockInfo(0))
            Dim fuelType As String = blockInfo(1)

            Dim row As GridViewRow = DirectCast(selectedImgBtnDone.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)

            UpdateAppointmentDuration(fuelType, ddlDuration.SelectedValue)

            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.ClearAppointmentSlotsContainer()
            Me.ReloadCurrentWebPage()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateDurationDropdownList(ByRef ddl As DropDownList)
        Dim newListItem As ListItem
        ddl.Items.Clear()

        For i = 1 To 100

            If (i = 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddl.Items.Add(newListItem)
        Next
    End Sub
#End Region

#Region "Update Appointment Duration"
    ''' <summary>
    ''' Update Appointment duration
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateAppointmentDuration(ByVal fuelType As String, ByVal duration As Double)

        Dim objDurationDict As Dictionary(Of String, Double) = SessionManager.GetPropertyDurationDict()
        If (objDurationDict.ContainsKey(fuelType)) Then
            objDurationDict(fuelType) = duration
        Else
            objDurationDict.Add(fuelType, duration)
        End If

        SessionManager.SetPropertyDurationDict(objDurationDict)

    End Sub

#End Region

#Region "Create Container"
    ''' <summary>
    ''' This function creates the html container for every block of trade and appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateContainer(ByVal uniqueBlockId As String)
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + uniqueBlockId
        divContainer.CssClass = "trade-appointment-block"
        Return divContainer
    End Function
#End Region

#Region "Reload the Current Web Page"
    ''' <summary>
    ''' This function 'll reload the current page. This is being used when user rearrange the scheduled appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ReloadCurrentWebPage()
        Response.Redirect(Request.RawUrl)
    End Sub
#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef alternativeSchedullingSlot As AlternativeSchedullingSlot, ByVal msg As String)
        'create the new empty row in data table to display the message
        alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.Rows.Add(alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.NewRow())
        grid.DataSource = alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        alternativeSchedullingSlot.alternativeAppointmentSlotsDtBO.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Appointment Footer Controls"
    ''' <summary>
    ''' This function 'll create the footer buttons of appointment grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateAppointmentFooterControls(ByVal uniqueBlockId As String, ByVal alternativeSchedullingSlot As AlternativeSchedullingSlot)
        Dim buttonsContainer As Panel = New Panel()
        Dim btnRefreshList As Button = New Button()

        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = uniqueBlockId
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click

        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "trade-appointment-buttons"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnRefreshList)
        Return buttonsContainer
    End Function
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedAptBlockId As Integer = Integer.Parse(btnRefresh.CommandArgument)

            'get the start date which is selected against this trade
            Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedAptBlockId.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedAptBlockId.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                startAgain = True
                'Me.validateStartDate(startDate)
            End If

            'this function 'll get the already temporary trade and appointment list then 
            'it 'll add five more operatives in the list of selected (trade/operative) block            
            RefreshTheOpertivesList(selectedAptBlockId, startDate, startAgain)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub

    Private Sub RefreshTheOpertivesList(ByVal selectedTradeAptBlockId As Integer, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
        GetValuesFromSession()
        Dim tempAptBo As AlternativeSchedullingSlot = New AlternativeSchedullingSlot()

        tempAptBo = SessionManager.GetAlternativeSchedulingBo()
        'increase the count of operatives by 
        tempAptBo.DisplayCount = tempAptBo.DisplayCount + 5
        'if user didn't change the start date 
        If startAgain = True Then
            tempAptBo.StartSelectedDate = startDate
        End If
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim schedulingBl As SchedulingBL = New SchedulingBL()
        'get the available operatives, their leaves, appointments and selected start date
        AvailableOperativesDs = SessionManager.getAvailableOperativesDs()
        tempAptBo.alternativeAppointmentSlotsDtBO.IsPatchSelected = SelectedPropertySchedulingBo.IsPatchSelected
        tempAptBo.alternativeAppointmentDtBO.IsDateSelected = SelectedPropertySchedulingBo.IsDateSelected
        objAlternativeSchedulingBL.AddMoreTempAltrnativeAppointments(AvailableOperativesDs.Tables(ApplicationConstants.OperativesDt), AvailableOperativesDs.Tables(ApplicationConstants.LeavesDt), AvailableOperativesDs.Tables(ApplicationConstants.AppointmentsDt), tempAptBo, startDate, SelectedPropertySchedulingBo, startAgain)
        ''this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        objAlternativeSchedulingBL.OrderAlternativeAppointmentsByDistance(tempAptBo)

        ''get the instance of gridview control
        Dim grdTempAppointments As GridView = CType(pnlPageContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedTradeAptBlockId.ToString()), GridView)
        Me.CreateTempAppointmentGrids(grdTempAppointments, selectedTradeAptBlockId, tempAptBo)
    End Sub

#End Region

#Region "Set updated component duration"
    ''' <summary>
    ''' This function updates duration in data tables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetUpdatedComponentDuration()

        Dim objDurationDict As Dictionary(Of String, Double) = SessionManager.GetPropertyDurationDict()
        GetValuesFromSession()

        For Each item As KeyValuePair(Of String, Double) In objDurationDict
            Dim fuelType As String = item.Key
            Dim duration As Double = item.Value

            If Not IsNothing(LstAlternativeSchedullingSlotHeader) And (LstAlternativeSchedullingSlotHeader.Any()) Then
                LstAlternativeSchedullingSlotHeader.FirstOrDefault(Function(xy) xy.FuelType = fuelType).AppointmentDuration = duration
            End If

        Next
        SessionManager.RemoveAlternativeSchedulingHeaderBoList()
        SessionManager.SetAlternativeSchedulingHeaderBoList(LstAlternativeSchedullingSlotHeader)

    End Sub
#End Region

#Region "Amend Appointment Popup - Events"


#Region "ck Box Refresh Data Set Checked Changed"
    Protected Sub ckBoxRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxRefreshDataSet.CheckedChanged
        Me.ckBoxRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId())
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.mdlPopUpAmendAppointment.Show()

        End Try

    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"

    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub

#End Region

#Region "ddl Status Selected Index Changed"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            SessionManager.setApaAmendAppointmentClicked(True)
            Dim statusId As Integer = CType(ddlStatus.SelectedValue(), Integer)
            Me.LoadActions(statusId)
            Me.LoadLetters(-1)
            Me.ddlStatus.SelectedValue = statusId
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAction.SelectedValue(), Integer)
            Me.LoadLetters(actionId)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAmendAppointment.Show()
        End Try
    End Sub

#End Region

#Region "Btn Save Appointment Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click
        Dim isSaved As Boolean = False
        Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session
        Dim startDate As Date = SessionManager.getApaStartDate()
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        Dim SmsStatus As String = UserMessageConstants.SmsNotSent
        Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
        Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
        Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
        Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
        Dim pId As String = ""
        pId = SessionManager.GetSelectedPropertySchedulingBo().PropertyId
        Dim journalId As Integer = SessionManager.GetSelectedPropertySchedulingBo().JournalId
        If Not IsNothing(pId) AndAlso Not pId = "" Then
            If SessionManager.getUserEmployeeId = objSchedulingBL.GetSchedulingPropertyLockedBy(journalId, SessionManager.GetSelectedPropertySchedulingBo().AppointmentType) Then
                Try
                    If (Me.validateAmendAppointment() = True) Then
                        Dim journalHistoryId As Integer = 0
                        journalHistoryId = Me.saveAmendAppointment(PushNoticificationStatus, PushNoticificationDescription)
                        If journalHistoryId > 0 Then
                            'populate appointment to be arranged grid 
                            'Me.populateAppointmentArrangedGrid()
                            'show success message in new popup. Set the property address label in new pupup
                            'webUserCtrlPropAddressSuccessPopup.setPropertyLabels()
                            uiMessageHelper.setMessage(lblSuccessAppointmentMessage, pnlSuccessAppointmentMessage, UserMessageConstants.SaveAmendAppointmentSuccessfully, False)
                            isSaved = True
                            SessionManager.removeActivityLetterDetail()

                            If (SessionManager.GetSelectedPropertySchedulingBo().AppointmentType = "Property") Then
                                ' Start of code to send the SMS'
                                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                                selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()
                                Dim tenancyID As Integer = selPropSchedulingBo.TenancyId
                                Dim dstenantsInfo As New DataSet()
                                objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

                                If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                                    SmsStatus = UserMessageConstants.SmsSending
                                    SmsDescription = UserMessageConstants.SmsDescriptionSending
                                    Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                                    Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                                End If
                                ' End of Code to send the SMS
                                EmailStatus = UserMessageConstants.EmailSending
                                EmailDescription = UserMessageConstants.EmailDescriptionSending

                                Me.sendEmail(journalHistoryId, EmailStatus, EmailDescription)
                                Me.setEmailAndSmsStatusForAppointment(journalHistoryId, SmsStatus, SmsDescription, EmailStatus, EmailDescription)
                            End If

                            Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)

                        Else
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                        End If

                    End If
                Catch ex As ThreadAbortException
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                    End If
                    If isSaved = False Then
                        Me.mdlPopUpAmendAppointment.Show()
                    Else
                        objSchedulingBL.SetSchedulingPropertyStatus(journalId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), SessionManager.GetSelectedPropertySchedulingBo().AppointmentType)
                        Me.mdlPopUpAmendAppointment.Hide()
                        Me.mdlSuccessPopup.Show()
                    End If

                End Try
            Else
                PropertyScheduledPopUp.Show()
            End If
        End If
    End Sub
#End Region

#Region "set Push Noticification Status For Appointment"
    Private Sub setPushNoticificationStatusForAppointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
        Try
            objSchedulingBL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#Region "set Email And Sms Status For Appointment"
    Private Sub setEmailAndSmsStatusForAppointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
        Try
            objSchedulingBL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close success popup Click"

    Protected Sub imgBtnCancelSuccessPopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancelSuccessPopup.Click
        Try
            SessionManager.RemoveAppointmentTypeInfo()
            Response.Redirect(PathConstants.AlternativeServicingPath + "?" + PathConstants.Tab + "=" + PathConstants.AppointmentArranged)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal tenantmobile As String, ByRef smsStatus As String, ByRef smsDescription As String)
        Try

            tenantmobile = tenantmobile.Replace(" ", String.Empty)

            If tenantmobile.Length >= 10 Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(lblAppointmentStartTime.Text, lblAppointmentDate.Text)
                'Dim postData As String

                'POSTdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(tenantmobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)


                '  GeneralHelper.sendSMS(body, tenantmobile)
                'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                'Dim javascriptMethod As String = "sendSMS('" + tenantmobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(tenantmobile, body)

                smsStatus = UserMessageConstants.SmsSent
                smsDescription = UserMessageConstants.SmsDescriptionSent
            Else
                smsStatus = UserMessageConstants.SmsSendingFailed
                smsDescription = UserMessageConstants.AppointmentSavedSMSError + UserMessageConstants.InvalidMobile
                'uiMessageHelper.IsError = True
                'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            smsStatus = UserMessageConstants.SmsSendingFailed
            smsDescription = UserMessageConstants.AppointmentSavedSMSError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal journalHistoryId As Integer, ByRef emailStatus As String, ByRef emailDescription As String)

        Try
            Dim objScheduling As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            objScheduling.getAppointmentInfoForEmail(resultDataset, journalHistoryId)
            Dim infoDt As DataTable = resultDataset.Tables(0)
            If (infoDt.Rows.Count > 0) Then
                Dim recepientEmail As String = infoDt.Rows(0)(ApplicationConstants.EmailCol)
                Dim recepientName As String = infoDt.Rows(0)(ApplicationConstants.CustomerNameCol)
                Dim jobSheet As String = infoDt.Rows(0)(ApplicationConstants.JSGCol)
                Dim appointmentDate As String = infoDt.Rows(0)(ApplicationConstants.AppointmentDateCol)
                Dim appointmentTime As String = infoDt.Rows(0)(ApplicationConstants.AppointmentTimeCol)
                If Validation.isEmail(recepientEmail) Then
                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheet, appointmentTime, appointmentDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail
                    Dim mailMessage As New System.Net.Mail.MailMessage
                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)
                    emailStatus = UserMessageConstants.EmailSent
                    emailDescription = UserMessageConstants.EmailDescriptionSent
                Else
                    Throw New Exception(UserMessageConstants.InvalidEmail)
                End If
            Else
                Throw New Exception(UserMessageConstants.TanencyTerminated)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")

        Catch ex As Exception
            emailStatus = UserMessageConstants.EmailSendingFailed
            emailDescription = UserMessageConstants.AppointmentSavedEmailError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "btn View Letter Click"
    Protected Sub btnViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewLetter.Click
        Try
            If (ddlLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region

#Region "Image Button Close Amend Appointment Popup"
    Protected Sub imgBtnCloseAmendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAmendApp.Click
        SessionManager.RemoveAppointmentTypeInfo()
        Me.mdlPopUpAmendAppointment.Hide()
    End Sub
#End Region

#Region "lnkBtn App Details Click"

    Protected Sub lnkBtnAppDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkBtnAppDetails.Click
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)
            Dim dstenantsInfo As New DataSet()
            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                setTenantsInfo(dstenantsInfo, tblTenantInfoAmendPopup)
                Me.mdlPopUpAmendAppointment.Show()
                Me.mdlPopUpAmenAptContactDetails.Show()
            Else
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.noTenantInformationFound, True)
                Me.mdlPopUpAmendAppointment.Show()

            End If

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If

        End Try
    End Sub

#End Region
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
#End Region

#Region "Amend Appointment Popup - Functions"

#Region "Open Amend Appointment Popup"

    Public Sub openAmendAppointmentPopup(ByVal appointmentInfo As String())
        Try

            'below mention sequence is expected in appointmentInfo array
            '0 index - operative id
            '1 index - operative name
            '2 index - appointment start time
            '3 index - appointment end time
            '4 index - appointment date                    
            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            'retreive the information that has been already saved in session related to this selected property
            selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()

            'fill up the information of appointment against selected property
            selPropSchedulingBo.OperativeId = CType(appointmentInfo(0), Integer)
            selPropSchedulingBo.OperativeName = appointmentInfo(1)
            selPropSchedulingBo.AppointmentStartTime = CType(appointmentInfo(2), Date)
            selPropSchedulingBo.AppointmentEndTime = CType(appointmentInfo(3), Date)
            selPropSchedulingBo.Duration = CType(appointmentInfo(4).Split(" ").FirstOrDefault(), Integer)
            selPropSchedulingBo.AppointmentDate = CType(appointmentInfo(2), Date)

            'save this above information in session again
            SessionManager.SetSelectedPropertySchedulingBo(selPropSchedulingBo)

            'fill up the address
            lblLocation.Text = selPropSchedulingBo.PropertyAddress
            'set the appointment date in label
            'This(fucntion) will convert the date in following format e.g Mon 12th Nov 2012
            lblAppointmentDate.Text = GeneralHelper.convertDateToUsCulture(selPropSchedulingBo.AppointmentDate)
            'set the appointment start time into 24 hrs format string
            lblAppointmentStartTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentStartTime)
            'set the appointment end time into 24 hrs format string
            lblAppointmentEndTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentEndTime)
            'set the operative name 
            lblOperative.Text = selPropSchedulingBo.OperativeName

            'declare the data set. This will be used to fetch the already saved data of appointment
            Dim dsAppointments As DataSet = New DataSet()

            'get & set the already saved data of appointment
            objSchedulingBL.getSelectedAppointmentDetails(dsAppointments, selPropSchedulingBo.AppointmentId)

            lblLocation.Text = selPropSchedulingBo.PropertyAddress
            If (ddlStatus.Items.Count <= 0) Then
                Me.LoadSatus(dsAppointments.Tables(0).Rows(0).Item(1))
                Me.LoadActions(dsAppointments.Tables(0).Rows(0).Item(1))
            Else
                Me.LoadSatus(dsAppointments.Tables(0).Rows(0).Item(1))
            End If
            txtNotes.Text = dsAppointments.Tables(0).Rows(0).Item(9).ToString()

            'To implement joint tenants information.
            lnkBtnAppDetails.Visible = GeneralHelper.isPropertyStatusLet(selPropSchedulingBo.PropertyStatus)
            lnkBtnAppDetails.CommandArgument = selPropSchedulingBo.TenancyId
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()

        End Try
    End Sub

#End Region

#Region "Load Satus"
    Protected Sub LoadSatus(Optional ByVal defaultValue As Integer = -1)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = ApplicationConstants.StatusId
        ddlStatus.DataTextField = ApplicationConstants.Title
        ddlStatus.DataBind()

        If (defaultValue <> -1) Then
            ddlStatus.SelectedValue = defaultValue
        End If

        'To Implement CR, Remove the status 'appointment to be arranged' from the drop down list when arranging or rearranging an appointment
        If ddlStatus.Items.Count > 0 AndAlso ddlStatus.Items.Contains(ddlStatus.Items.FindByText("Appointment to be arranged")) Then
            ddlStatus.Items.RemoveAt(ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Appointment to be arranged")))
        End If

    End Sub
#End Region

#Region "Load Actions"
    Protected Sub LoadActions(ByRef statusId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAction.DataSource = resultDataSet
        ddlAction.DataValueField = ApplicationConstants.ActionId
        ddlAction.DataTextField = ApplicationConstants.Title
        ddlAction.DataBind()
        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)


        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub LoadLetters(ByRef actionId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim objResourceBL As ResourcesBL = New ResourcesBL()
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.GetAlternativeLettersByActionId(actionId, resultDataSet)
        ddlLetter.DataSource = resultDataSet
        ddlLetter.DataValueField = ApplicationConstants.LetterId
        ddlLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlLetter.DataBind()
        ddlLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Validate Amend Appointment"
    Protected Function validateAmendAppointment()
        Dim success As Boolean = True
        If Me.ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)
        End If

        Return success
    End Function
#End Region

#Region "Save Amend Appointment"
    Public Function saveAmendAppointment(ByRef pushNoticificationStatus As String, ByRef pushNoticificationDescription As String)
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        Dim selPropSechdulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        Dim journalHistoryId As String

        'get the property and operative information from sesssion 
        selPropSechdulingBo = SessionManager.GetSelectedPropertySchedulingBo()

        'Amend Appointment Information
        objScheduleAppointmentsBO.Duration = selPropSechdulingBo.Duration
        objScheduleAppointmentsBO.JournelId = selPropSechdulingBo.JournalId
        objScheduleAppointmentsBO.PropertyId = selPropSechdulingBo.PropertyId
        objScheduleAppointmentsBO.CustomerEmail = selPropSechdulingBo.CustomerEmail
        objScheduleAppointmentsBO.CustomerEmailStatus = selPropSechdulingBo.CustomerEmailStatus
        objScheduleAppointmentsBO.CreatedBy = SessionManager.getAppServicingUserId()
        objScheduleAppointmentsBO.InspectionTypeId = SessionManager.getApplianceServicingId()
        objScheduleAppointmentsBO.AppointmentDate = lblAppointmentDate.Text
        objScheduleAppointmentsBO.AppointmentEndDate = selPropSechdulingBo.AppointmentEndTime.Date
        objScheduleAppointmentsBO.StatusId = ddlStatus.SelectedValue
        objScheduleAppointmentsBO.ActionId = ddlAction.SelectedValue
        objScheduleAppointmentsBO.LetterId = ddlLetter.SelectedValue
        objScheduleAppointmentsBO.Notes = txtNotes.Text
        objScheduleAppointmentsBO.DocumentPath = GeneralHelper.getDocumentUploadPath()
        If (System.DateTime.Parse(Me.lblAppointmentEndTime.Text).TimeOfDay > System.DateTime.Parse("12:00").TimeOfDay) Then
            objScheduleAppointmentsBO.Shift = "PM"
        Else
            objScheduleAppointmentsBO.Shift = "AM"
        End If
        objScheduleAppointmentsBO.AppStartTime = Me.lblAppointmentStartTime.Text
        objScheduleAppointmentsBO.AppEndTime = Me.lblAppointmentEndTime.Text
        objScheduleAppointmentsBO.AssignedTo = selPropSechdulingBo.OperativeId
        objScheduleAppointmentsBO.AppointmentId = selPropSechdulingBo.AppointmentId

        'Save Standard Letter
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objScheduleAppointmentsBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objScheduleAppointmentsBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objScheduleAppointmentsBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objScheduleAppointmentsBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objScheduleAppointmentsBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objScheduleAppointmentsBO.IsDocumentAttached = False
            End If
        End If
        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        journalHistoryId = objSchedulingBL.AmendAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

        Dim appointmentHeatingFuelDuration = New DataTable()
        If (LstAlternativeSchedullingSlotHeader.Any()) Then
            appointmentHeatingFuelDuration.Columns.Add("HeatingTypeId")
            appointmentHeatingFuelDuration.Columns.Add("Duration")
            appointmentHeatingFuelDuration.Columns.Add("TradeId")

            For Each item As OilAlternativeSchedullingSlotHeader In LstAlternativeSchedullingSlotHeader
                Dim row = appointmentHeatingFuelDuration.NewRow()
                row("Duration") = item.AppointmentDuration
                row("TradeId") = item.TradeId
                row("HeatingTypeId") = item.HeatingTypeId
                appointmentHeatingFuelDuration.Rows.Add(row)
            Next
        End If
        objAlternativeSchedulingBL.UpdateHeatingAppointmentDuration(objScheduleAppointmentsBO.AppointmentId, appointmentHeatingFuelDuration)

        If (journalHistoryId > 0 And objScheduleAppointmentsBO.AppointmentDate = Today()) Then
            pushNoticificationStatus = UserMessageConstants.PushNoticificationSending
            pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSending
            Dim propertyAddress As String = String.Empty
            propertyAddress = lblLocation.Text.Trim()

            Dim message As String = String.Empty
            message = GeneralHelper.generatePushNotificationMessage("Existing", objScheduleAppointmentsBO.AppointmentDate, objScheduleAppointmentsBO.AppStartTime,
                                                    objScheduleAppointmentsBO.AppEndTime, propertyAddress)
            Dim pushnoticicesent As Boolean = GeneralHelper.pushNotificationAppliance(message, objScheduleAppointmentsBO.AssignedTo)
            If pushnoticicesent = True Then
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSent
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSent
            Else
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSendingFailed
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSendingFailed
            End If
        End If

        Return journalHistoryId

    End Function

#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Reset Amend Appointment Controls"
    Protected Sub resetAmendAppointmentControls()

        Me.lblActivityPopupMessage.Text = String.Empty
        Me.LoadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#End Region

End Class