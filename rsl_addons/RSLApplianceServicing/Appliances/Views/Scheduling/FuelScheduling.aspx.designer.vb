﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FuelScheduling

    '''<summary>
    '''updPanelFuelScheduling control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelFuelScheduling As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStatus As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblStatusType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStatusType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlStatusType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatusType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''pnlFuelType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFuelType As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblFuelType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFuelType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlFuelType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFuelType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtBoxWatermarkExtenderSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxWatermarkExtenderSearch As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ckBoxDueWithIn56Days control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ckBoxDueWithIn56Days As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''lnkBtnAptbaTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBtnAptbaTab As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkBtnAptTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBtnAptTab As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''updateProgressFuelScheduling control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updateProgressFuelScheduling As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''MainView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MainView As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''View1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents View1 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''FuelAppointToBeArranged control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FuelAppointToBeArranged As Global.Appliances.AppointmentToBeArranged

    '''<summary>
    '''View2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents View2 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''AppointArranged control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AppointArranged As Global.Appliances.AppointmentArranged
End Class
