﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="DefectBasket.aspx.vb" Inherits="Appliances.DefectBasket" %>

<%@ Import Namespace="AS_Utilities" %>
<%@ Register TagPrefix="contractor" TagName="AssignToContractor" Src="~/Controls/DefectManagement/AssignToContractor.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .DefectsChildRow
        {
            display: none;
        }
        .grdDefectAppointmentsHeader
        {
            border-bottom: 1px Solid Black;
        }
		        
        .grdDefectAppointmentsHeader th 
        {
            text-align:left;  
            padding-left:8px;   
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="pageHeader ">
        Schedule Defects
    </p>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div style="width: 100%; text-align: right">
        Recorded By:
        <asp:Label ID="lblRecordedBy" Text="" runat="server" />
    </div>
    <div class="clear">
    </div>
    <div style="margin-bottom: 20px;">
        <asp:GridView runat="server" ID="grdInspectionDetail" AutoGenerateColumns="False"
            BackColor="White" BorderStyle="None" ForeColor="Black" GridLines="None" Width="100%"
            AllowSorting="false" PageSize="1" ShowHeaderWhenEmpty="false" AllowPaging="false">
            <Columns>
                <asp:TemplateField HeaderText="Ref:" SortExpression="JournalId">
                    <ItemTemplate>
                        <asp:Label ID="JournalId" Text='<%# Eval("JournalId") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Address:" SortExpression="ADDRESS">
                    <ItemTemplate>
                        <asp:Label ID="lblAddress" Text='<%# Eval("Address") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                    <ItemTemplate>
                        <asp:Label ID="lblScheme" Text='<%# Eval("Scheme") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Block:" SortExpression="Block">
                    <ItemTemplate>
                        <asp:Label ID="lblBlock" Text='<%# Eval("Block") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Post Code:" SortExpression="PostCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPostCode" Text='<%# Eval("PostCode") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Capped?" SortExpression="isCapped">
                    <ItemTemplate>
                        <asp:Label ID="lblisCapped" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isCapped")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Heating:" SortExpression="isHeatingAvailable">
                    <ItemTemplate>
                        <asp:Label ID="lblisHeatingAvailable" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isHeatingAvailable")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Heaters:" SortExpression="isHeatersLeft">
                    <ItemTemplate>
                        <asp:Label ID="lblHeaters" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isHeatersLeft")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Recorded:" SortExpression="DefectDate">
                    <ItemTemplate>
                        <asp:Label ID="lblDefectDate" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("DefectDate")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Parts Due:" SortExpression="PartsDue">
                    <ItemTemplate>
                        <asp:Label ID="lblPartsDue" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("PartsDue")) %>'
                            runat="server" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(GeneralHelper.getColourForPartsDue(Eval("PartsDue"))) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:Button ID="btnSchedule" Text="Schedule" runat="server" Visible="false"></asp:Button>
                    </ItemTemplate>
                    <ItemStyle CssClass="hiddenField" />
                </asp:TemplateField>
            </Columns>
            <RowStyle Font-Bold="true" />
            
            <HeaderStyle CssClass="grdDefectAppointmentsHeader" />
        </asp:GridView>
    </div>
    <div style="border-bottom: 1px solid black;">
        <asp:GridView ID="grdDefects" runat="server" AutoGenerateColumns="false" ShowHeader="true"
            BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="0px" ForeColor="Black"
            GridLines="None" Width="100%" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkBoxDefect" runat="server" Visible='<%# GeneralHelper.getInverseOfBooleanValue(Eval("isAppointmentCreated"))%>'>
                        </asp:CheckBox>
                        <asp:Image ImageUrl="~/Images/FlatGreenTick.png" runat="server" Visible='<%#Eval("isAppointmentCreated")%>' />
                        <asp:Button runat="server" ID="btnDefectId" CommandArgument='<%#Eval("DefectId")%>'
                            Visible="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appliance" SortExpression="Appliance">
                    <ItemTemplate>
                        <asp:Label ID="lblAppliance" Text='<%# Eval("Appliance") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category" SortExpression="DefectCategory">
                    <ItemTemplate>
                        <asp:Label ID="lblDefectCategory" Text='<%# Eval("DefectCategory") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Priority:" SortExpression="DefectPriority">
                    <ItemTemplate>
                        <asp:Label ID="lblDefectPriority" Text='<%# Eval("DefectPriority") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Trade:" SortExpression="DefectTrade">
                    <ItemTemplate>
                        <asp:Label ID="lblDefectTrade" Text='<%# Eval("DefectTrade") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="2 Person" SortExpression="isTwoPersonsJob">
                    <ItemTemplate>
                        <asp:Label ID="lblisTwoPersonsJob" Text='<%# GeneralHelper.getYesNoStringFromBooleanValue(Eval("isTwoPersonsJob")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Disconnected?" SortExpression="isApplianceDisconnected">
                    <ItemTemplate>
                        <asp:Label ID="lblisApplianceDisconnected" Text='<%# GeneralHelper.getYesNoStringFromBooleanValue(Eval("isApplianceDisconnected")) %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appointment:" SortExpression="AppointmentDate">
                    <ItemTemplate>
                        <asp:Label ID="lblAppointmentDate" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("AppointmentDate"), "HH:mm dd/MM/yyyy", "-") %>'
                            runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Parts Due:" SortExpression="DefectPartsDue">
                    <ItemTemplate>
                        <asp:Label ID="lblPartsDue" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("DefectPartsDue")) %>'
                            runat="server" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(GeneralHelper.getColourForPartsDue(Eval("DefectPartsDue"))) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Contractor">
                    <ItemTemplate>
                        <asp:Button Text="Assign to Contractor" runat="server" Visible="true" ID="btnAssignToContractor"
                            OnClick="btnAssignToContractor_Click"  CommandArgument='<%#Eval("DefectId")%>' Enabled='<%# EnableDisableAssignToContractorButton(Eval("isAppointmentCreated"),Eval("isAssignedToContractor"))%>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="grdDefectAppointmentsHeader" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
        </asp:GridView>
    </div>
    <div>
        Would you like to group trades?
        <asp:RadioButton ID="rbtnGroupTradeYes" runat="server" GroupName="GroupTrades" CssClass="margin_right20"
            Text="Yes" />
        <asp:RadioButton ID="rbtnGroupTradeNo" runat="server" GroupName="GroupTrades" Text="No"
            Checked="True" />
    </div>
    <div class="clear">
    </div>
    <div style="text-align: right; float: right">
        <asp:Button Text="Back" runat="server" PostBackUrl="~/Views/Scheduling/DefectScheduling.aspx" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button runat="server" Text="Schedule Selected Defects" ID="btnScheduleSelectedFaults" />
    </div>
    <div class="clear">
    </div>
    <asp:Panel ID="pnldefectJSDs" runat="server" BackColor="White" Width="400px" BorderColor="Black"
        BorderStyle="Outset" BorderWidth="1px" style=" padding:10px; ">
        <asp:UpdatePanel runat="server" ID="updPaneldefectJSDs">
            <ContentTemplate>
           
        <strong>The Defect(s) have been recorded and scheduled and the following JSD number(s)
            assigned</strong>
        <br />
        <div style="text-align: center; width: 100%">
            <asp:Label Text="" runat="server" ID="lblJSDs" /><br />
            <br />
            <asp:Button Text="Close" runat="server" PostBackUrl="~/Views/Scheduling/DefectScheduling.aspx" />
        </div>
         </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mdlPoPUppnldefectJSDs" runat="server" Enabled="True"
        TargetControlID="lblDisppnldefectJSDs" PopupControlID="pnldefectJSDs" DropShadow="true"
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblDisppnldefectJSDs" runat="server"></asp:Label>
    <asp:Panel ID="pnlAssignToContractor" runat="server" CssClass="assignToContractorModalPopup" Style="width: 47%;
        display: none; padding: 0px !important;">
        <div style="width: 100%; text-align: left; clear: both; overflow: auto; max-height: 520px; padding:10px;">
            <contractor:AssignToContractor ID="ucAssignToContractor" runat="server">
            </contractor:AssignToContractor>
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <asp:Label ID="lblDefectIdHidden" runat="server" Text="" Style="display: none;" ClientIDMode="Static" />
    <ajaxToolkit:modalpopupextender id="popupAssignToContractor" runat="server" popupcontrolid="pnlAssignToContractor"
        targetcontrolid="lblDefectIdHidden" BackgroundCssClass="modalBackground">
    </ajaxToolkit:modalpopupextender>
</asp:Content>
