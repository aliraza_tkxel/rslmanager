﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class VoidScheduling
    Inherits PageBase
    Public ckBoxDocumentUploadApta As New CheckBox()
    Public ckBoxDocumentUploadAptba As New CheckBox()
    Public ckBoxRefreshDataSetApta As New CheckBox()
    Public ddlLetterApta As New DropDownList

    Dim objAppointmentToBeArranged As VoidAppointmentToBeArranged = CType(Page.LoadControl("~/Controls/Scheduling/VoidAppointmentToBeArranged.ascx"), VoidAppointmentToBeArranged)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.FindControlsOfUserControl()

            If (Not IsPostBack) Then
                If (SessionManager.getSearchText <> "") Then
                    txtSearch.Text = SessionManager.getSearchText
                End If
                populateVoidServicingTab()
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub


#Region "lnk Btn Aptba Tab Click"

    Protected Sub lnkBtnAptbaTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentToBeArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "lnk Btn Apt Tab Click"
    Protected Sub lnkBtnAptTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            populateAppointmentArrangedList()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region


#Region "Populate Void Servicing Tab"

    Sub populateVoidServicingTab()
        If Request.QueryString(PathConstants.Tab) IsNot Nothing AndAlso Request.QueryString(PathConstants.Tab) <> String.Empty Then

            If Request.QueryString(PathConstants.Tab).Equals(PathConstants.AppointmentArranged) Then
                populateAppointmentArrangedList()
            Else
                populateAppointmentToBeArrangedList()
            End If

        Else
            populateAppointmentToBeArrangedList()
        End If
    End Sub

#End Region


#Region "Populate Appointment To Be Arranged"

    Sub populateAppointmentToBeArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        AppointToBeArranged1.searchAppointmentToBeArranged(SessionManager.getSearchText())
    End Sub

#End Region



#Region "Populate Appointment Arranged"
    Sub populateAppointmentArrangedList()
        lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 1
        AppointArranged.searchAppointmentArranged(SessionManager.getSearchText())
    End Sub
#End Region


#Region "Find Contorls"
    Protected Sub FindControlsOfUserControl()

        ckBoxDocumentUploadApta = DirectCast(AppointArranged.FindControl("ckBoxDocumentUpload"), CheckBox)
        ckBoxDocumentUploadAptba = DirectCast(AppointToBeArranged1.FindControl("ckBoxDocumentUpload"), CheckBox)
        ddlLetterApta = DirectCast(AppointArranged.FindControl("ddlLetter"), DropDownList)
        ckBoxRefreshDataSetApta = DirectCast(AppointArranged.FindControl("ckBoxRefreshDataSet"), CheckBox)

    End Sub
#End Region


    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            SessionManager.setSearchText(txtSearch.Text)
            If (MainView.ActiveViewIndex = 0) Then
                AppointToBeArranged1.searchAppointmentToBeArranged(txtSearch.Text)
            ElseIf (MainView.ActiveViewIndex = 1) Then
                AppointArranged.searchAppointmentArranged(txtSearch.Text)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub


End Class