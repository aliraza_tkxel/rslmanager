﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Threading

Public Class DefectJobSheetSummary
    Inherits PageBase

#Region "Properties"

    Protected Property defectsAmendAllowed() As Boolean = False

#End Region

#Region "Events Handling"

#Region "Page Events Handling"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                If Not IsNothing(Request.QueryString("defectId")) Then
                    Dim defectId As Integer
                    If Integer.TryParse(Request.QueryString("defectId"), defectId) Then
                        populateArrangedJobSheet(defectId)
                    Else
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidDefectId, True)
                    End If
                Else
                    fillJobSheetDetails()
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"

    ''' <summary>
    ''' Button Update Customer Detail Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateCustomerDetails_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetails.Click
        Try
            Me.updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Schedule Appointment Click Event"

    Protected Sub btnScheduleAppointment_Click(sender As Object, e As EventArgs) Handles btnScheduleAppointment.Click
        Try
            Dim saveStatus As Boolean = scheduleAppointment()
            If saveStatus Then
                txtCustAppointmentNotes.Enabled = False
                txtJobSheetNotes.Enabled = False
                btnScheduleAppointment.Enabled = False
                Response.Redirect(PathConstants.DefectBasket)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Cancel Appointment Click"

    Private Sub btnCancelAppointment_Click(sender As Object, e As System.EventArgs) Handles btnCancelAppointment.Click
        Try
            mdlConfirmCancel.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Confrim Cancel Click"

    Private Sub btnConfrimCancel_Click(sender As Object, e As System.EventArgs) Handles btnConfrimCancel.Click
        Try
            Page.Validate("cancelAppoitment")
            cancelDefectAppointment()
            Response.Redirect(PathConstants.DefectSchedulingPage + "?" + PathConstants.Tab + "=" + PathConstants.AppointmentArranged)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Button Rearrange Appointment Click"

    Private Sub btnReArrangeAppointment_Click(sender As Object, e As System.EventArgs) Handles btnReArrangeAppointment.Click
        Try
            rearrangeDefectAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "btn Amend Defect Click"

    Protected Sub btnAmendDefect_Click(sender As Object, e As EventArgs)
        Try
            Dim amendButton = CType(sender, Button)
            ucDefectManagement.viewDefectDetails(amendButton.CommandArgument)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            mdlPoPUpAddDefect.Show()
        End Try
    End Sub

#End Region

#Region "uc Defect Management save button click"

    Private Sub ucDefectManagement_saveButton_Clicked(sender As Object, e As System.EventArgs) Handles ucDefectManagement.saveButton_Clicked
        Try
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectSavedSuccessfuly, False)
            If Not IsNothing(Request.QueryString("defectId")) Then
                Dim defectId As Integer
                If Integer.TryParse(Request.QueryString("defectId"), defectId) Then
                    populateArrangedJobSheet(defectId)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidDefectId, True)
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            mdlPoPUpAddDefect.Hide()
        End Try
    End Sub

#End Region

#Region "uc Defect Managment cancel button click"

    Private Sub ucDefectManagement_cancelButton_Clicked(sender As Object, e As System.EventArgs) Handles ucDefectManagement.cancelButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Show popup again event fired"

    Private Sub showPopupAgain(ByVal sender As Object, ByVal e As System.EventArgs) Handles ucDefectManagement.showPoupAgain
        Try
            mdlPoPUpAddDefect.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True


            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If


        End Try
    End Sub

#End Region
#End Region

#Region "Functions"

#Region "Fill Job Sheet Details"

    Private Sub fillJobSheetDetails()

        Dim selectedInspectionRecord As DataTable = SessionManager.getSelectDefectInseptionRef()
        If (Not IsNothing(selectedInspectionRecord)) Then

            ' -----------------------------------------------------------------------
            ' Populate property detail from session.

            Dim propertyId As String = selectedInspectionRecord(0)("propertyId")
            populatePropertyInfo(propertyId, selectedInspectionRecord(0)("AppointmentType"))

            ' -----------------------------------------------------------------------
            ' Populate Defect(s) grid from session

            Dim tempDefectAptBlock As TempDefectAppointmentBO = SessionManager.getSelectedAppointmentBlockforScheduling()

            Dim applianceDefects As New DataTable()

            applianceDefects = tempDefectAptBlock.tempDefectDtBo.dt

            grdDefects.DataSource = applianceDefects
            grdDefects.DataBind()

            ' -----------------------------------------------------------------------

            ' -----------------------------------------------------------------------
            ' Populate appointment detail from session.
            Dim objAppointmentBO As AppointmentBO = SessionManager.getDefectAppointmentDataForAppointmentSummary

            lblInspectionRef.Text = selectedInspectionRecord(0)("journalId")
            lblOperative.Text = objAppointmentBO.Operative
            lblDuration.Text = GeneralHelper.getSingularOrPluralPostFixForNumericValues(tempDefectAptBlock.TotalDuration, "hour", "hours")
            lblTrade.Text = applianceDefects(0)(TempDefectDtBO.tradeColName)
            lblJSD.Text = Right("00000" + applianceDefects(0)(TempDefectDtBO.DefectIdColName).ToString(), 5)
            lblStartDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", objAppointmentBO.AppointmentStartDate)
            lblEndDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", objAppointmentBO.AppointmentEndDate)

            objAppointmentBO.Duration = tempDefectAptBlock.TotalDuration

            SessionManager.setDefectAppointmentDataForAppointmentSummary(objAppointmentBO)

        End If

    End Sub

#End Region

#Region "Populate Property Info"

    ''' <summary>
    ''' Populate Property Info
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populatePropertyInfo(ByVal propertyId As String, ByVal RequestType As String)

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

        If (String.IsNullOrEmpty(propertyId)) Then
            btnScheduleAppointment.Enabled = False
            btnUpdateCustomerDetails.Enabled = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPropertyId, True)
        Else

            Dim resultDataset As DataSet = New DataSet()
            If RequestType = "Property" Then
                objSchedulingBL.getPropertyCustomerDetail(resultDataset, propertyId)
            Else
                objSchedulingBL.getSchemeBlockDetail(resultDataset, propertyId, RequestType)
            End If

            Dim propertyDt As DataTable = resultDataset.Tables(0)

            bindPropertyInfo(propertyDt, RequestType)
        End If
    End Sub

#End Region

#Region "Bind Property Info"

    Private Sub bindPropertyInfo(propertyDt As DataTable, RequestType As String)
        Dim tenancyId As Integer
        'Property Information
        lblScheme.Text = propertyDt.Rows(0)("SchemeName")
        If RequestType = "Property" Then
            lblAddress.Text = propertyDt.Rows(0)("HOUSENUMBER") + " " + propertyDt.Rows(0)("ADDRESS1") + " " + propertyDt.Rows(0)("ADDRESS2")
            lblTowncity.Text = propertyDt.Rows(0)("TOWNCITY")
            lblCounty.Text = propertyDt.Rows(0)("COUNTY")
            lblPostcode.Text = propertyDt.Rows(0)("POSTCODE")
            'Customer Information
            hdnCustomerId.Value = propertyDt.Rows(0)("CustomerId")
            lblCustomerName.Text = propertyDt.Rows(0)("TenantName")
            lblCustomerTelephone.Text = propertyDt.Rows(0)("Telephone")
            lblCustomerMobile.Text = propertyDt.Rows(0)("Mobile")
            lblCustomerEmail.Text = propertyDt.Rows(0)("Email")
            tenancyId = propertyDt.Rows(0)("TenancyId")

            If (tenancyId = -1) Then
                btnUpdateCustomerDetails.Enabled = False
            Else
                btnUpdateCustomerDetails.Enabled = True
            End If
        ElseIf RequestType = "Scheme" Then
            Label9.Visible = False
            Label13.Visible = False
            Label14.Visible = False
            Label15.Visible = False
            Label16.Visible = False
            lblAddress.Visible = False
            lblTowncity.Visible = False
            lblCounty.Visible = False
            lblPostcode.Visible = False
            'Customer Information
            hdnCustomerId.Value = propertyDt.Rows(0)("CustomerId")
            lblCustomerName.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            tenancyId = propertyDt.Rows(0)("TenancyId")
            btnUpdateCustomerDetails.Visible = False
        Else
            Label13.Visible = False
            Label14.Visible = False
            Label15.Visible = False
            Label16.Visible = False
            Label9.Text = "Block:"
            lblAddress.Text = propertyDt.Rows(0)("HOUSENUMBER") + " " + propertyDt.Rows(0)("ADDRESS1") + " " + propertyDt.Rows(0)("ADDRESS2")
            lblTowncity.Text = propertyDt.Rows(0)("TOWNCITY")
            lblCounty.Text = propertyDt.Rows(0)("COUNTY")
            lblPostcode.Text = propertyDt.Rows(0)("POSTCODE")
            'Customer Information
            hdnCustomerId.Value = propertyDt.Rows(0)("CustomerId")
            lblCustomerName.Visible = False
            lblCustomerTelephone.Visible = False
            lblCustomerMobile.Visible = False
            lblCustomerEmail.Visible = False
            tenancyId = propertyDt.Rows(0)("TenancyId")
            btnUpdateCustomerDetails.Visible = False
        End If
    End Sub

#End Region

#Region "Update Customer Address"

    ''' <summary>
    ''' Update Customer Address
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateAddress()

        'Update Enable
        If btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges

        Else
            'Save Changes
            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                'If Not isError Then
                Dim objCustomerBL As CustomerBLManager = New CustomerBLManager()
                Dim objCustomerBO As CustomerBO = New CustomerBO()

                objCustomerBO.CustomerId = hdnCustomerId.Value
                objCustomerBO.Telephone = txtCustomerTelephone.Text
                objCustomerBO.Mobile = txtCustomerMobile.Text
                objCustomerBO.Email = txtCustomerEmail.Text

                Dim results As ValidationResults = EnterpriseLibrary.Validation.Validation.Validate(objCustomerBO)
                If results.IsValid Then


                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

                    End If
                Else
                    Dim message = String.Empty
                    For Each result As ValidationResult In results
                        message += result.Message
                        Exit For
                    Next
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, True)
                End If
                'End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails

            End If

        End If

    End Sub

#End Region

#Region "Schedule Appointment"

    Private Function scheduleAppointment() As Boolean
        Dim objAppointment As AppointmentBO = SessionManager.getDefectAppointmentDataForAppointmentSummary

        objAppointment.JobSheetNotes = txtJobSheetNotes.Text.Trim
        objAppointment.AppointmentNotes = txtCustAppointmentNotes.Text.Trim
        objAppointment.SchedularId = SessionManager.getUserEmployeeId()

        Dim selectedInspectionRecord As DataTable = SessionManager.getSelectDefectInseptionRef()
        Dim journalId As Integer = selectedInspectionRecord(0)("journalId")

        Dim objSchedulingBL As New SchedulingBL
        Dim saveStatus As Boolean = objSchedulingBL.scheduleDefectAppointment(journalId, objAppointment, SessionManager.getSelectedAppointmentBlockforScheduling().tempDefectDtBo.dt)

        If saveStatus Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
        End If
        Return saveStatus
    End Function

#End Region

#Region "Populate Arranged JobSheet"

    Private Sub populateArrangedJobSheet(defectId As Integer)
        defectsAmendAllowed = True
        Dim objDefectBL As New DefectsBL
        Dim jobSheetDataSet As New DataSet
        txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
        txtJobSheetNotes.Attributes.Add("readonly", "readonly")
        pnlScheduleAppointmentButtons.Visible = False
        pnlArrangeAppointmentButtons.Visible = True
        btnBack.PostBackUrl = PathConstants.ScheduleDefect

        objDefectBL.getJobSheetDetails(jobSheetDataSet, defectId)

        If jobSheetDataSet.Tables.Count > 0 AndAlso jobSheetDataSet.Tables(0).Rows.Count > 0 Then
            grdDefects.DataSource = jobSheetDataSet.Tables(0)
            grdDefects.DataBind()

            lblJSD.Text = Right("00000" + jobSheetDataSet.Tables(0)(0)(TempDefectDtBO.DefectIdColName).ToString(), 5)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        If jobSheetDataSet.Tables.Count > 1 AndAlso jobSheetDataSet.Tables(1).Rows.Count > 0 Then
            Dim drAppointmentInfo = jobSheetDataSet.Tables(1)(0)
            lblInspectionRef.Text = drAppointmentInfo("InspectionRef")
            lblOperative.Text = drAppointmentInfo("Operative")
            lblDuration.Text = GeneralHelper.getSingularOrPluralPostFixForNumericValues(drAppointmentInfo("Duration"), "hour", "hours")
            lblTrade.Text = drAppointmentInfo("Trade")
            hidAppointmentId.Value = drAppointmentInfo("AppointmentId").ToString()

            lblStartDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", drAppointmentInfo("StartDateTime"))
            lblEndDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", drAppointmentInfo("EndDateTime"))
            txtCustAppointmentNotes.Text = drAppointmentInfo("AppointmentNotes")
            txtJobSheetNotes.Text = drAppointmentInfo("JobSheetNotes")
        End If

        If jobSheetDataSet.Tables.Count > 2 AndAlso jobSheetDataSet.Tables(2).Rows.Count > 0 Then
            bindPropertyInfo(jobSheetDataSet.Tables(2), jobSheetDataSet.Tables(0)(0)("ReqType:"))
        End If

    End Sub

#End Region

#Region "Cancel Defect Appointmnet"

    Private Sub cancelDefectAppointment()
        Dim objDefectsBL As New DefectsBL
        Dim stats As Boolean = objDefectsBL.cancelDefectAppointment(hidAppointmentId.Value, txtCancelReason.Text.Trim, SessionManager.getAppServicingUserId())
    End Sub

#End Region

#Region "Rearrange Defect Appointment"

    Private Sub rearrangeDefectAppointment()

        Dim objDefectsBL As New DefectsBL
        Dim status As Boolean = objDefectsBL.rearrangeDefectAppointment(hidAppointmentId.Value, SessionManager.getAppServicingUserId())
        If status Then
            Dim atbaDs As New DataSet
            Dim journalId As Integer = Integer.Parse(lblInspectionRef.Text)
            objDefectsBL.getAppointmentToBeArrangedList(atbaDs, -1, journalId, New PageSortBO("DESC", "DefectDate", 1, 30))

            If atbaDs.Tables.Count > 0 AndAlso atbaDs.Tables(0).Rows.Count > 0 Then

                Dim defectToBeArrangeListView As DataView = atbaDs.Tables(0).DefaultView

                defectToBeArrangeListView.RowFilter = "JournalId = " & journalId.ToString()

                SessionManager.setSelectDefectInseptionRef(defectToBeArrangeListView.ToTable())
                Response.Redirect(PathConstants.DefectBasket)
            Else
                btnReArrangeAppointment.Enabled = False
                btnCancelAppointment.Enabled = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentRearrangedButIssueNavigatingToDefectBasket, True)
            End If

        End If
    End Sub

#End Region

#End Region

#Region "Validation Functions"

#Region "Works Required Custom Validation"

    Protected Sub cvWorksRequired_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvCancelReason.ServerValidate

        Dim length As Integer = Len(args.Value)
        If length < 1 Or length > 1000 Then
            args.IsValid = False
            'args.ErrorMessage = "Works Required must not exceed 4000 characters. Your Characters: " + length.ToString()
        Else
            args.IsValid = True
        End If
    End Sub

#End Region

#End Region

End Class