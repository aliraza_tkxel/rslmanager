﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Globalization
Imports AjaxControlToolkit

Public Class DefectAvailableAppointments
    Inherits PageBase

#Region "Properties"

    Private Property propertyId As String = String.Empty
    Private Const appointmentInfoSeparater As String = ":::"
    Dim divContainerCounter As Integer = 0

#End Region

#Region "Events Handling"

#Region "Page Events Handling"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            clearAppointmentSlotsContainer()

            If Not IsPostBack Then
                setPropertyAddress()
                populateDefectsAndTempAppointments()
            Else
                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary faults and appointment list (this is requirement of dynamic controls on post back)
                reDrawDefectsAndTempAppointmentsGrid()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "btn Back Click"
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs)
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)

        Response.Redirect(PathConstants.DefectBasket, True)

    End Sub
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedDefectAptBlockID As Integer = Integer.Parse(btnRefresh.CommandArgument)
            Dim grdTempAppointments As GridView = CType(pnlAppointmentSlotsContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedDefectAptBlockID.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedDefectAptBlockID.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                Me.validateStartDate(startDate)
            End If
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            clearAppointmentSlotsContainer()

            'this function 'll get the already temporary fault and appointment list then 
            'it 'll add five more operatives in the list of selected (fault/operative) block
            refreshTheOpertivesList(selectedDefectAptBlockID, startDate)
            btnRefresh.Focus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Select Operative Click"
    Protected Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)

        'This a sample string that will be received on this event
        'Sample ---- operative Id:::Operative Name:::Time:::Selected Date:::Comma separated temprary Fault Ids:::Trade Id
        'Values ---- 468:::Jade Burrell:::09:00 AM - 12:00 PM:::25/02/2013:::37615,43057,:::1
        Dim parameters As String = String.Empty
        Dim btnSelectOperative As Button = DirectCast(sender, Button)
        parameters = btnSelectOperative.CommandArgument

        setAppointmentBoAndTempFaults(parameters)
        'Me.setSessionValues()
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        'Me.clearTempAndCofirmBlockFromSession()
        redirectToAppointmentSummary()

    End Sub
#End Region

#Region "btn Go Click"
    ''' <summary>
    ''' This event 'll refresh the appointments against the specific date. Appointment slots 'll be start from date entered.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(btnGo.CommandArgument)

            'get the start date which is selected against this trade
            Dim grdTempAppointments As GridView = CType(pnlAppointmentSlotsContainer.FindControl(ApplicationConstants.TempAppointmentGridControlId + selectedTradeAptBlockId.ToString()), GridView)
            Dim txtStartDate As TextBox = CType(grdTempAppointments.HeaderRow.FindControl(ApplicationConstants.AppointmentStartDateControlId + selectedTradeAptBlockId.ToString()), TextBox)
            Dim startDate As Date = DateTime.Now
            Dim startAgain As Boolean = False

            If (Not IsNothing(txtStartDate)) Then
                startDate = GeneralHelper.getUKCulturedDateTime(txtStartDate.Text)
                startAgain = True
                Me.validateStartDate(startDate)
            End If

            'it 'll add five more operatives in the list of selected (trade/operative) block            
            Me.refreshTheOpertivesList(selectedTradeAptBlockId, startDate, startAgain)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region


#Region "chk Patch Checked Changed "
    Protected Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.clearAppointmentSlotsContainer()
            Dim chkPatch As CheckBox = TryCast(sender, CheckBox)

            If chkPatch.Checked Then
                'If patch is checked remove existing appointments for map.
                'SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
                'remove pins from map.
                'removeMarkersJavaScript()
            End If

            Dim selectedDefectAptBlockId As Integer = Integer.Parse(chkPatch.Attributes("CommandArgument"))

            're bind the previously displayed datatables with grid, events with buttons 
            '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
            'Me.reDrawConfirmedFaultAppointmentGrid()

            Dim lstTempDefectAppointmentBO As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
            Dim tempFaultAptBo As TempDefectAppointmentBO = New TempDefectAppointmentBO()


            lstTempDefectAppointmentBO = SessionManager.getTempDefectAptBlockList()

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            tempFaultAptBo = lstTempDefectAppointmentBO(selectedDefectAptBlockId)
            tempFaultAptBo.IsPatchSelected = chkPatch.Checked

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date

            Dim objDefectSchedulingBL As DefectSchedulingBL = New DefectSchedulingBL()
            'Before calling the addMoreAppointment check for valid operatives dataset.
            Dim operativeDs As DataSet = SessionManager.getDefectAvailableOperativesDs()
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                objDefectSchedulingBL.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), SessionManager.getCommonAddressBO(), tempFaultAptBo, Date.Now)
            End If
            'save in temporary fault operative list
            lstTempDefectAppointmentBO(selectedDefectAptBlockId) = tempFaultAptBo

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment            
            objDefectSchedulingBL.orderAppointmentsByDistance(tempFaultAptBo)

            'save list in session 
            SessionManager.setTempDefectAptBlockList(lstTempDefectAppointmentBO)

            'pouplate the temporary fault appointment grid
            bindTempDefectAppointment(lstTempDefectAppointmentBO)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "chk operative Checked Changed"
    Protected Sub chkOperative_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            clearAppointmentSlotsContainer()
            Dim chkOperative As CheckBox = TryCast(sender, CheckBox)

            If chkOperative.Checked Then
                ''If expiry date is checked remove existing appointments for map.
                'SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
                ''remove pins from map.
                'removeMarkersJavaScript()
            End If

            Dim selectedDefectAptBlockId As Integer = Integer.Parse(chkOperative.Attributes("CommandArgument"))

            're bind the previously displayed data tables with grid, events with buttons 
            '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
            'Me.reDrawConfirmedFaultAppointmentGrid()

            Dim lstTempDefectAppointmentBO As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
            Dim tempDefectAptBo As New TempDefectAppointmentBO()

            lstTempDefectAppointmentBO = SessionManager.getTempDefectAptBlockList()

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            tempDefectAptBo = lstTempDefectAppointmentBO(selectedDefectAptBlockId)
            tempDefectAptBo.IsOperativeCheckSelected = chkOperative.Checked
            tempDefectAptBo.tempAppointmentDtBo.dt.Clear()
            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
            Dim objDefectSchedulingBL As DefectSchedulingBL = New DefectSchedulingBL()

            'Before calling the addMoreAppointment check for valid operatives dataset.
            Dim operativeDs As DataSet = SessionManager.getDefectAvailableOperativesDs()
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                objDefectSchedulingBL.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), SessionManager.getCommonAddressBO(), tempDefectAptBo, Date.Now)
            End If
            'save in temporary fault operative list
            lstTempDefectAppointmentBO(selectedDefectAptBlockId) = tempDefectAptBo

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment             
            objDefectSchedulingBL.orderAppointmentsByDistance(tempDefectAptBo)

            'save list in session 
            SessionManager.setTempDefectAptBlockList(lstTempDefectAppointmentBO)

            'pouplate the temporary fault appointment grid
            Me.bindTempDefectAppointment(lstTempDefectAppointmentBO)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image btn Edit Click"
    Protected Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnEdit As ImageButton = DirectCast(sender, ImageButton)
            Dim selectedTradeAptBlockId As Integer = Integer.Parse(selectedImgBtnEdit.CommandArgument)

            Dim row As GridViewRow = DirectCast(selectedImgBtnEdit.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim imgBtnEdit As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.EditImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)
            Dim lblDuration As Label = CType(pnlDuration.FindControl(ApplicationConstants.DurationLabelControlId + selectedTradeAptBlockId.ToString()), Label)
            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)

            imgBtnEdit.Visible = False
            lblDuration.Visible = False
            ddlDuration.Visible = True
            imgBtnDone.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image btn Done Click"
    Protected Sub imgBtnDone_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnDone As ImageButton = DirectCast(sender, ImageButton)
            Dim blockInfo() As String = selectedImgBtnDone.CommandArgument.Split(",")

            Dim selectedTradeAptBlockId As Integer = Integer.Parse(blockInfo(0))
            Dim compTradeId As Integer = Integer.Parse(blockInfo(1))

            Dim row As GridViewRow = DirectCast(selectedImgBtnDone.NamingContainer, GridViewRow)
            Dim pnlDuration As Panel = CType(row.FindControl(ApplicationConstants.DurationPanelControlId + selectedTradeAptBlockId.ToString()), Panel)

            Dim ddlDuration As DropDownList = CType(pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId + selectedTradeAptBlockId.ToString()), DropDownList)
            Dim imgBtnDone As ImageButton = CType(pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId + selectedTradeAptBlockId.ToString()), ImageButton)

            Dim selectedDefectsDataTable As DataTable = SessionManager.getSelectedDefectsDataTable()
            selectedDefectsDataTable.Rows(selectedTradeAptBlockId).Item("Duration") = ddlDuration.SelectedValue
            SessionManager.setSelectedDefectsDataTable(selectedDefectsDataTable)
            clearAppointmentSlotsContainer()
            populateDefectsAndTempAppointments()
            'updateTradeDuration(compTradeId, ddlDuration.SelectedValue)
            ' Me.reloadCurrentWebPage()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Function"

#Region "Clear Appointments Slots Containter"

    Private Sub clearAppointmentSlotsContainer()
        pnlAppointmentSlotsContainer.Controls.Clear()
    End Sub

#End Region

    Private Sub populateDefectsAndTempAppointments()

        Dim selectedDefectsDataTable As DataTable = SessionManager.getSelectedDefectsDataTable()

        Dim operativeDs As DataSet = getDefectAvailableOperatives(selectedDefectsDataTable)
        If operativeDs.Tables(0).Rows.Count > 0 Then


            Dim objDefectSchedulingBL As New DefectSchedulingBL()

            Dim lstTempDefectAppointmentBO = objDefectSchedulingBL.getTempDefectsAppointments(selectedDefectsDataTable, operativeDs.Tables(0), operativeDs.Tables(1), operativeDs.Tables(2), SessionManager.getGroupBy(), SessionManager.getCommonAddressBO(), Date.Now)

            'save list in session 
            SessionManager.setTempDefectAptBlockList(lstTempDefectAppointmentBO)

            bindTempDefectAppointment(lstTempDefectAppointmentBO)
        End If
    End Sub

    Private Sub reDrawDefectsAndTempAppointmentsGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary faults and appointment list
        Dim tempDefectAptList As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
        tempDefectAptList = SessionManager.getTempDefectAptBlockList()
        bindTempDefectAppointment(tempDefectAptList)
    End Sub

#Region "get Available Operatives"

    Private Function getDefectAvailableOperatives(ByRef selectedDefectsDataTable As DataTable) As DataSet

        Dim tradeIds As String = GeneralHelper.getDelimitedStringForDatatableColumn(selectedDefectsDataTable, "TradeId", ",")

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim operativeDs As New DataSet()
        objSchedulingBL.getDefectAvailableOperatives(operativeDs, tradeIds)

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.OperativeNotFound, True)
        End If

        SessionManager.setDefectAvailableOperativesDs(operativeDs)

        Return operativeDs

    End Function

#End Region

#Region "bind Temp Faults Appointment"
    Private Sub bindTempDefectAppointment(ByVal tempFaultAptBoList As List(Of TempDefectAppointmentBO))
        If IsNothing(tempFaultAptBoList) Then
            'Show single back button to go back to defect basket.
            'Me.createSingleBackButton()
        Else
            Dim defectAptBlockID = -1
            'Dim counter As Integer = -1
            'panel control 
            Dim panelContainer As Panel
            Dim pnlPageContainer As Panel = New Panel()
            'bind this above data with grid view             
            For Each item As TempDefectAppointmentBO In tempFaultAptBoList
                Dim selectedTempFaultIds As String = String.Empty
                'we are considering fault and their respective operative list as block so 
                'this varaible 'll be used to recognize each block whenever some button/checkbox event 'll be fired
                defectAptBlockID += 1
                'counter = counter + 1
                'create container for every block of temporary faults and appointment 
                panelContainer = Me.createContainer()

                'Create object of fault grid
                Dim selectedDefectsGrid As New GridView
                selectedDefectsGrid.ID = "grdDefects" + defectAptBlockID.ToString()
                AddHandler selectedDefectsGrid.RowDataBound, AddressOf tempDefectGrid_RowDataBound
                selectedDefectsGrid.EnableViewState = True
                selectedDefectsGrid.CssClass = "grid_defects"
                selectedDefectsGrid.GridLines = GridLines.None
                selectedDefectsGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                selectedDefectsGrid.DataSource = item.tempDefectDtBo.dt
                selectedDefectsGrid.DataBind()
                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(selectedDefectsGrid)

                Dim headerRow As GridViewRow
                headerRow = Me.createFilterHeader(item.IsPatchSelected, item.IsOperativeCheckSelected, defectAptBlockID, item.OperativeName)

                Dim t As Table = TryCast(selectedDefectsGrid.Controls(0), Table)
                If t IsNot Nothing Then
                    t.Rows.AddAt(0, headerRow)
                End If

                'hide the last column from temporary fault grid(that are not required in grid view) because those columns 
                'were there in datatable and those columns have been automatically created, when we bound datatable with grid view
                'the columns of these data table 'll be used through sessoion
                selectedDefectsGrid.HeaderRow.Cells(6).Visible = False 'TradeID
                selectedDefectsGrid.HeaderRow.Cells(7).Visible = False 'DefectId
                selectedDefectsGrid.HeaderRow.Cells(8).Visible = False 'DefectDuration

                'run the loop on grid view rows to remove the cells (that are not required in grid view) from each row 
                'and also to add the select button in each row
                Dim uniqueBlockId As Integer = 0
                For Each gvr As GridViewRow In selectedDefectsGrid.Rows
                    gvr.Cells(6).Visible = False 'TradeId
                    gvr.Cells(7).Visible = False 'DefectId
                    gvr.Cells(8).Visible = False 'DefectDuration
                    gvr.Cells(4).Controls.Add(createDurationControls(uniqueBlockId, item.tempDefectDtBo))
                    uniqueBlockId += 1

                Next

                'Create object of appointment grid
                Dim isEmptyGrid As Boolean = False
                Dim tempAptGrid As New GridView()
                tempAptGrid.ID = "grdTempAppointments" + defectAptBlockID.ToString()
                tempAptGrid.ShowFooter = True
                tempAptGrid.EnableViewState = True
                tempAptGrid.CssClass = "grid_appointment"
                tempAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                tempAptGrid.GridLines = GridLines.None
                'Check if , are there any appointments or not .If not then display message
                If item.tempAppointmentDtBo.dt.Rows.Count = 0 Then
                    Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoDefectOperativesAvailable)
                    isEmptyGrid = True
                Else
                    tempAptGrid.DataSource = item.tempAppointmentDtBo.dt
                    tempAptGrid.DataBind()
                    'add row span on footer and add back button, refresh list button and view calendar button
                    tempAptGrid.FooterRow.Cells(0).Controls.Add(createAppointmentButtons(defectAptBlockID))
                End If
                Dim panelTempAppointmentsContainer As Panel = New Panel()
                panelTempAppointmentsContainer.CssClass = "panelDefectAppointmentsContainer"
                panelTempAppointmentsContainer.Controls.Add(tempAptGrid)
                panelTempAppointmentsContainer.ID = "panelTempAppointmentsContainer" + defectAptBlockID.ToString()

                'Add the GridView control into the div control/panel
                panelContainer.Controls.Add(panelTempAppointmentsContainer)

                If tempAptGrid.Rows.Count > 0 Then
                    Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
                    For i As Integer = m - 1 To 1 Step -1
                        tempAptGrid.FooterRow.Cells.RemoveAt(i)
                    Next i
                    tempAptGrid.FooterRow.Cells(0).ColumnSpan = 7 '7 is the number of visible columns to span.
                End If

                'declare the variables that 'll store the information required to bind with select button in grid
                Dim operativeId As String = String.Empty
                Dim operativeName As String = String.Empty
                Dim aptTimeString As String = String.Empty
                Dim aptDateString As String = String.Empty
                Dim dtCounter As Integer = 0
                Dim completeAppointmentInfo As String = String.Empty
                'create header of appointment grid and add start date calenar in it
                Dim tradAptGridHeaderCell As TableCell = tempAptGrid.HeaderRow.Cells(6)
                tradAptGridHeaderCell.Controls.Add(Me.createAppointmentHeaderControls(defectAptBlockID.ToString(), item.StartSelectedDate))

                'show visible off for unwanted columns
                tempAptGrid.HeaderRow.Cells(1).Visible = False
                tempAptGrid.HeaderRow.Cells(5).Visible = False
                tempAptGrid.HeaderRow.Cells(7).Visible = False
                tempAptGrid.HeaderRow.Cells(8).Visible = False
                tempAptGrid.HeaderRow.Cells(9).Visible = False
                tempAptGrid.HeaderRow.Cells(10).Visible = False
                tempAptGrid.HeaderRow.Cells(11).Visible = False
                tempAptGrid.HeaderRow.Cells(6).Width = "250"
                'if grid is not emtpy then remove the cells and also create select button and set command argument with that
                If isEmptyGrid = False Then
                    'run the loop on grid view rows to remove the cells from each row 
                    'and also to add the select button in each row
                    For Each gvr As GridViewRow In tempAptGrid.Rows
                        gvr.Cells(1).Visible = False
                        gvr.Cells(5).Visible = False
                        gvr.Cells(7).Visible = False
                        gvr.Cells(8).Visible = False
                        gvr.Cells(9).Visible = False
                        gvr.Cells(10).Visible = False
                        gvr.Cells(11).Visible = False
                        'get the information that should be set with select button 
                        operativeId = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeIdColName)
                        operativeName = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeColName)
                        aptTimeString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.startDateColName)
                        aptDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.endDateColName)
                        dtCounter += 1

                        'save this operative and time slot information in a string, after that it 'll be bind with select button
                        completeAppointmentInfo = operativeId + appointmentInfoSeparater + operativeName + appointmentInfoSeparater + aptTimeString _
                                                    + appointmentInfoSeparater + aptDateString + appointmentInfoSeparater + selectedTempFaultIds _
                                                    + appointmentInfoSeparater + item.TradeId.ToString() _
                                                    + appointmentInfoSeparater + defectAptBlockID.ToString()

                        'Add the select button 
                        Dim btnSelectOperative As Button = New Button()
                        btnSelectOperative.ID = "btnSelectOperative" + defectAptBlockID.ToString()
                        btnSelectOperative.Text = "Select"
                        btnSelectOperative.CommandArgument = completeAppointmentInfo
                        AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                        gvr.Cells(6).Controls.Add(btnSelectOperative)
                        gvr.Cells(6).Style.Add("float", "right")

                    Next
                End If
                'pnlAppointmentSlotsContainer.Controls.Clear()
                'Add this panel container to div page container
                pnlAppointmentSlotsContainer.Controls.Add(panelContainer)

                ''Call the update markers for any refresh in appointment slots.
                'If Page.IsPostBack Then
                '    updateMarkersJavaScript()
                'End If
            Next

        End If


    End Sub
#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef tempFaultAptBo As TempDefectAppointmentBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        tempFaultAptBo.tempAppointmentDtBo.dt.Rows.Add(tempFaultAptBo.tempAppointmentDtBo.dt.NewRow())
        grid.DataSource = tempFaultAptBo.tempAppointmentDtBo.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        tempFaultAptBo.tempAppointmentDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Attributes.Add("style", "white-space: normal;")
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Container"
    ''' <summary>
    ''' This function creates the html container for every block of confirmed fault and confirmed appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContainer()
        divContainerCounter += 1
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + divContainerCounter.ToString()
        divContainer.CssClass = "fl_width_100_text_a_2"
        Return divContainer
    End Function
#End Region

#Region "temp Defect Grid Row Data Bound"

    ''' <summary>
    ''' his event makes the descirption text bold/strong
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tempDefectGrid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Text = Server.HtmlDecode(e.Row.Cells(0).Text)
        End If
    End Sub

#End Region

#Region "Create Appointment Header Controls"
    ''' <summary>
    ''' This function 'll create the date control on the top of appointment gird
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createAppointmentHeaderControls(ByVal uniqueBlockId As String, startDate As Date)
        'start label
        Dim lblStartDate As Label = New Label()
        lblStartDate.ID = ApplicationConstants.StartDateLabelControlId + uniqueBlockId
        lblStartDate.Text = "Start Date:"
        'date text box
        Dim txtDate As TextBox = New TextBox()
        txtDate.ID = ApplicationConstants.AppointmentStartDateControlId + uniqueBlockId
        txtDate.Text = startDate.Date
        txtDate.Width = 80
        'image button
        Dim imgCalDate As HtmlImage = New HtmlImage()
        imgCalDate.ID = ApplicationConstants.CalDateControlId + uniqueBlockId
        imgCalDate.Src = "../../Images/calendar.png"
        'calendar extendar
        Dim calExtStartDate As CalendarExtender = New CalendarExtender()
        calExtStartDate.PopupButtonID = imgCalDate.ID
        calExtStartDate.PopupPosition = CalendarPosition.Right
        calExtStartDate.TargetControlID = txtDate.ID
        calExtStartDate.ID = ApplicationConstants.CalExtDateControlId + uniqueBlockId
        calExtStartDate.Format = "dd/MM/yyyy"
        'create go button
        Dim btnGo As Button = New Button()
        btnGo.ID = ApplicationConstants.GoControlId + uniqueBlockId
        btnGo.Text = "Go"
        btnGo.CommandArgument = uniqueBlockId
        AddHandler btnGo.Click, AddressOf btnGo_Click
        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-appointment-header"
        pnl.Controls.Add(lblStartDate)
        pnl.Controls.Add(txtDate)
        pnl.Controls.Add(calExtStartDate)
        pnl.Controls.Add(imgCalDate)
        pnl.Style.Add("float", "right")
        pnl.Style.Add("Width", "250px")
        pnl.Controls.Add(btnGo)
        Return pnl
    End Function
#End Region

#Region "create Filter Header"
    Private Function createFilterHeader(ByVal isPatchSelected As Boolean, ByVal isOperativeFilterSelected As Boolean, ByVal defectAptBlockID As Integer, ByVal operativeName As String)

        Dim headerRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        Dim headerCell As New TableCell()
        headerCell.ColumnSpan = 6
        headerCell.Style.Add("background-color", "lightgray")
        headerCell.Style.Add("font-weight", "bold")
        headerCell.CssClass = "available-apptmts-frow"

        'add filter div
        Dim divFilter As HtmlGenericControl = New HtmlGenericControl("div")
        divFilter.InnerText = "Filter Available Operatives By: "
        divFilter.Attributes.Add("class", "grid_Defects_filter")
        headerCell.Controls.Add(divFilter)

        'add patch label
        Dim divPatch As HtmlGenericControl = New HtmlGenericControl("div")
        divPatch.InnerText = "Patch: "
        divPatch.Attributes.Add("class", "grid_Defects_patch")

        'add patch checkbox
        Dim chkPatch As CheckBox = New CheckBox()
        chkPatch.ID = "chkPatch"
        chkPatch.AutoPostBack = True
        chkPatch.Checked = isPatchSelected
        chkPatch.Attributes("CommandArgument") = defectAptBlockID
        AddHandler chkPatch.CheckedChanged, AddressOf chkPatch_CheckedChanged

        'add check box to patch label
        divPatch.Controls.Add(chkPatch)
        headerCell.Controls.Add(divPatch)

        'add date label
        Dim divOperativeCheck As HtmlGenericControl = New HtmlGenericControl("div")
        divOperativeCheck.InnerText = "Operative:"
        divOperativeCheck.Attributes.Add("class", "grid_Defects_operative")

        'add date checkbox
        Dim chkOperative As CheckBox = New CheckBox()
        chkOperative.ID = "chkOperative"
        chkOperative.AutoPostBack = True
        chkOperative.Checked = isOperativeFilterSelected
        chkOperative.Attributes("CommandArgument") = defectAptBlockID
        chkOperative.Text = String.Format("{0} ", operativeName)
        chkOperative.TextAlign = TextAlign.Right
        AddHandler chkOperative.CheckedChanged, AddressOf chkOperative_CheckedChanged


        'add check box to date label
        divOperativeCheck.Controls.Add(chkOperative)
        headerCell.Controls.Add(divOperativeCheck)



        ''add default value under default label
        'Dim lblDefault As Label = New Label()
        'lblDefault.Text = Me.customerBo.PatchName
        'divDefault.Controls.Add(lblDefault)
        'headerCell.Controls.Add(divDefault)

        'add the addition in cell to header row
        headerRow.Cells.Add(headerCell)
        Return headerRow
    End Function
#End Region

#Region "Create Duration Controls"
    ''' <summary>
    ''' This function 'll create the duration control on the top of trade grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createDurationControls(ByVal uniqueBlockId As String, item As TempDefectDtBO)

        'Edit ImageButton
        Dim imgBtnEdit As ImageButton = New ImageButton()
        imgBtnEdit.ID = ApplicationConstants.EditImageButtonControlId + uniqueBlockId.ToString()
        imgBtnEdit.ImageUrl = "../../Images/edit.png"
        imgBtnEdit.CssClass = "temp-trade-content-left"
        imgBtnEdit.Visible = True
        imgBtnEdit.CommandArgument = uniqueBlockId
        AddHandler imgBtnEdit.Click, AddressOf imgBtnEdit_Click

        'Duration Label
        Dim lblDuration As Label = New Label()
        lblDuration.ID = ApplicationConstants.DurationLabelControlId + uniqueBlockId.ToString()
        lblDuration.Text = If(item.dt.Rows(uniqueBlockId).Item("DefectDuration") > 1, Convert.ToString(item.dt.Rows(uniqueBlockId).Item("DefectDuration")) + " hours", Convert.ToString(item.dt.Rows(uniqueBlockId).Item("DefectDuration")) + " hour")
        lblDuration.Visible = True

        'Duration Dropdown
        Dim ddlDuration As DropDownList = New DropDownList()
        ddlDuration.ID = ApplicationConstants.DurationDropdownControlId + uniqueBlockId.ToString()
        ddlDuration.Visible = False
        populateDurationDropdownList(ddlDuration)
        ddlDuration.SelectedValue = item.dt.Rows(uniqueBlockId).Item("DefectDuration")

        'Done ImageButton
        Dim imgBtnDone As ImageButton = New ImageButton()
        imgBtnDone.ID = ApplicationConstants.DoneImageButtonControlId + uniqueBlockId.ToString()
        imgBtnDone.ImageUrl = "../../Images/done.png"
        imgBtnDone.Visible = False
        imgBtnDone.CommandArgument = uniqueBlockId + "," + item.tradeId.ToString
        AddHandler imgBtnDone.Click, AddressOf imgBtnDone_Click

        'create panel control and add all above controls in this panel
        Dim pnl As Panel = New Panel()
        pnl.CssClass = "temp-trade-content"
        pnl.ID = ApplicationConstants.DurationPanelControlId + uniqueBlockId.ToString()
        pnl.Controls.Add(imgBtnEdit)
        pnl.Controls.Add(lblDuration)
        pnl.Controls.Add(ddlDuration)
        pnl.Controls.Add(imgBtnDone)
        Return pnl

    End Function
#End Region

#Region "Populate duration dropdown"
    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDurationDropdownList(ByRef ddl As DropDownList)
        Dim newListItem As ListItem
        ddl.Items.Clear()

        For i = 1 To 100

            If (i = 1) Then
                newListItem = New ListItem(Convert.ToString(i) + " hour", i)
            Else
                newListItem = New ListItem(Convert.ToString(i) + " hours", i)
            End If

            ddl.Items.Add(newListItem)
        Next

    End Sub

#End Region


#Region "create Appointment Buttons"
    Private Function createAppointmentButtons(ByVal defectAptBlockID As Integer)
        Dim buttonsContainer As Panel = New Panel()
        Dim btnBack As Button = New Button()
        Dim btnRefreshList As Button = New Button()

        'set the back button 
        btnBack.Text = "Back"
        AddHandler btnBack.Click, AddressOf btnBack_Click
        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = defectAptBlockID
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click

        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "text_a_r_padd__r_0px"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnBack)
        buttonsContainer.Controls.Add(btnRefreshList)
        Return buttonsContainer
    End Function
#End Region

#Region "refresh The Opertives List"
    Private Sub refreshTheOpertivesList(ByVal selectedDefectAptBlockId As Integer, ByVal startDate As Date, Optional ByVal startAgain As Boolean = False)
        Dim lstTempDefectAppointmentBO As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
        Dim tempDefectAptBo As TempDefectAppointmentBO = New TempDefectAppointmentBO()

        lstTempDefectAppointmentBO = SessionManager.getTempDefectAptBlockList()

        'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
        'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
        Dim index As Integer = selectedDefectAptBlockId
        tempDefectAptBo = lstTempDefectAppointmentBO(index)

        'get the datatable from the object
        Dim tempAptDt As DataTable = tempDefectAptBo.tempAppointmentDtBo.dt

        'increase the count of operatives by 
        tempDefectAptBo.DisplayCount = tempDefectAptBo.DisplayCount + 5
        If startAgain = True Then
            tempDefectAptBo.StartSelectedDate = startDate
        End If
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim operativeDs As DataSet = SessionManager.getDefectAvailableOperativesDs()
        Dim objDefectSchedulingBL As DefectSchedulingBL = New DefectSchedulingBL()
        objDefectSchedulingBL.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), SessionManager.getCommonAddressBO(), tempDefectAptBo, startDate, startAgain)

        'save in temporary fault operative list
        lstTempDefectAppointmentBO(index) = tempDefectAptBo

        'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment         
        objDefectSchedulingBL.orderAppointmentsByDistance(tempDefectAptBo)

        'save list in session 
        SessionManager.setTempDefectAptBlockList(lstTempDefectAppointmentBO)
        clearAppointmentSlotsContainer()
        'pouplate the temporary fault appointment grid
        bindTempDefectAppointment(lstTempDefectAppointmentBO)

    End Sub

#End Region

#Region "set AppointmentBo And Temp Faults"

    Private Sub setAppointmentBoAndTempFaults(ByRef parameters As String)
        Dim parameterArray As String()

        Dim separater As String() = {appointmentInfoSeparater}
        parameterArray = parameters.Split(separater, StringSplitOptions.None)
        'Parameter array has the following indexes
        '0 index = operative id, 
        '1 index = operative name, 
        '2 index = appointment start date time, 
        '3 index = appointment end date time
        '4 index = selected temp fault ids, 
        '5 index = trade ids
        '6 index = appointmentBlockId

        Dim appointmentBo As AppointmentBO = New AppointmentBO()

        Dim appointmentDateTimeFormat As String = "HH:mm dd/MM/yyyy"

        Dim lAppointmentStartDateTime As Date = Date.ParseExact(parameterArray(appointmentBo.AppointmentParameter.AppointmentStartDateTime).Trim(), appointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))
        Dim lAppointmentEndDateTime As Date = Date.ParseExact(parameterArray(appointmentBo.AppointmentParameter.AppointmentEndDateTime).Trim(), appointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))

        appointmentBo.OperativeId = parameterArray(appointmentBo.AppointmentParameter.OperativeId)
        appointmentBo.Operative = parameterArray(appointmentBo.AppointmentParameter.Operative)

        appointmentBo.AppointmentStartDate = lAppointmentStartDateTime
        appointmentBo.AppointmentEndDate = lAppointmentEndDateTime
        appointmentBo.TradeId = parameterArray(appointmentBo.AppointmentParameter.tradeIds)

        Dim tempDefectAptList As List(Of TempDefectAppointmentBO) = New List(Of TempDefectAppointmentBO)
        tempDefectAptList = SessionManager.getTempDefectAptBlockList()
        Dim tempDefectAptBlock As TempDefectAppointmentBO

        tempDefectAptBlock = tempDefectAptList(parameterArray(appointmentBo.AppointmentParameter.appointmentBlockId))

        SessionManager.setSelectedAppointmentBlockforScheduling(tempDefectAptBlock)
        SessionManager.setDefectAppointmentDataForAppointmentSummary(appointmentBo)

    End Sub

#End Region

#Region "Set Property Address"

    Private Sub setPropertyAddress()
        Dim commonAddressBO As CommonAddressBO = SessionManager.getCommonAddressBO

        lblAddress.Text = commonAddressBO.Address
        lblPostCode.Text = commonAddressBO.PostCode
        lblTownCity.Text = commonAddressBO.Area
        lblTel.Text = commonAddressBO.Telephone
    End Sub

#End Region

#Region "Redirect to job sheet"

    Private Sub redirectToAppointmentSummary()
        Response.Redirect(PathConstants.DefectJobSheet)
    End Sub

#End Region

#Region "Validate Start Date"
    ''' <summary>
    ''' This funciton 'll perform the validation of start date
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <remarks></remarks>
    Private Sub validateStartDate(ByVal startDate As Date)
        Dim result As Integer = DateTime.Compare(startDate.Date, Date.Today)
        If result < 0 Then
            Throw New Exception(UserMessageConstants.SelectedDateIsInPast)
        End If
    End Sub
#End Region

#End Region

End Class