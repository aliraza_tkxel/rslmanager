﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports AS_BusinessObject
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary
Imports System.Threading




Public Class JobSheetSummaryAC
    'Inherits PageBase
    Inherits PageBase
#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then


                Dim journalId As Integer = CType(Request.QueryString(PathConstants.JournalId), Integer)
                Dim CurrentIndex As Integer = 1
                ViewState.Add(ViewStateConstants.CurrentIndex, CurrentIndex)
                populateJobSheet(journalId)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region
#Region "Assigned To Contractor"

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow - 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            setjobsheetsummary()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow + 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            SetJobSheetSummary()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Protected Sub populateJobSheet(ByVal journalHistoryId As Integer)
        Try
            ViewStateConstants.CurrentIndex = "1"
            Me.resetJobSheetControls()
            Me.populateJobSheetSummary(journalHistoryId.ToString())
            Me.setjobsheetsummary()
            Me.populatePlannedStatus()

            StatusDropDown.Visible = False
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub
    Private Sub populatePlannedStatus()
        Try
            Dim objJobSheetBL As New JobSheetBL()
            Dim dsPlannedStatus As New DataSet

            objJobSheetBL.getPlannedStatusLookUp(dsPlannedStatus)

            StatusDropDown.Items.Clear()
            StatusDropDown.DataSource = dsPlannedStatus
            StatusDropDown.DataValueField = "id"
            StatusDropDown.DataTextField = "val"
            StatusDropDown.DataBind()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub
    Private Sub setjobsheetsummary()
        Dim dsjobsheetsummary As New DataSet()
        Dim currentrow, totalrows As Integer

        dsjobsheetsummary = SessionManager.getJobSheetSummaryDs()

        currentrow = Convert.ToInt32(ViewStateConstants.CurrentIndex)

        If (dsjobsheetsummary.Tables.Count > 0 AndAlso dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            totalrows = dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count
            lblTotalSheets.Text = "page " + currentrow.ToString() + " of " + totalrows.ToString()

            If (currentrow = 1 And currentrow = totalrows) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = False
            ElseIf (currentrow = 1) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = True
            ElseIf (currentrow = totalrows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False
            ElseIf (currentrow > 1 And currentrow < totalrows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
            resetvisibility()
            controlsvisibilitybytype(dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentrow - 1).Item("type").ToString())
            With dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentrow - 1)
                lblJSN.Text = .Item("appointmentid").ToString().Trim()
                lblPlannedWorkComponent.Text = .Item("componentname")
                lblTrade.Text = .Item("trade")
                lblStatus.Text = .Item("appointmentstatus")
                lblContractor.Text = .Item("contractor")
                lblContact.Text = .Item("contact")
                txtWorksRequired.Text = .Item("workrequired")

                lblPONo.Text = .Item("purchaseorderid")
                lblOrderedBy.Text = .Item("orderby")
                lblOrdered.Text = .Item("podate")

                lblBudget.Text = String.Format("{0:c}", .Item("budget"))
                lblEstimate.Text = String.Format("{0:c}", .Item("estimate"))
                lblNetCost.Text = String.Format("{0:c}", .Item("netcost"))
                lblVat.Text = String.Format("{0:c}", .Item("vat"))

                lblTotal.Text = String.Format("{0:c}", (.Item("netcost") + .Item("vat")))
                lblOrderTotal.Text = String.Format("{0:c}", (.Item("netcost") + .Item("vat")))

                lblScheme.Text = .Item("schemename")
                lblBlock.Text = .Item("pjblock")
                lblPostCode.Text = .Item("clientpostcode")
                lblHouseNumber.Text = .Item("housenumber") + ", " + .Item("houseaddress1") + ", " + .Item("houseaddress2") + ", " + .Item("houseaddress3")
                lblCustomer.Text = .Item("clientname")
                lblTelephone.Text = .Item("clienttel")
                lblMobile.Text = .Item("clientmobile")
                lblEmail.Text = .Item("clientemail")
            End With
        End If

        'bind asbestos grid with data from database for jsn
        If (dsjobsheetsummary.Tables.Count > 1 _
            AndAlso dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

    Sub controlsvisibilitybytype(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblBlock.Visible = False
            lblForCustomer.Visible = False
            lblCustomer.Visible = False
            lblTelephone.Visible = False
            lblMobile.Visible = False
            lblForEmail.Visible = False
            lblForHouseNumber.Visible = False
            lblHouseNumber.Visible = False
            lblPostCode.Visible = False
            lblForPostCode.Visible = False
            lblForTelephone.Visible = False
            lblForMobile.Visible = False
            lblEmail.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
    Sub resetvisibility()
        lblForBlock.Visible = True
        lblForCustomer.Visible = True
        lblCustomer.Visible = True
        lblTelephone.Visible = True
        lblMobile.Visible = True
        lblEmail.Visible = True
        lblForHouseNumber.Visible = True
        lblHouseNumber.Visible = True
        lblPostCode.Visible = True
        lblForPostCode.Visible = True
        lblForTelephone.Visible = True
        lblForMobile.Visible = True
        lblEmail.Visible = True
        lblBlock.Visible = True
    End Sub
    Private Sub populateJobSheetSummary(ByRef jsn As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objJobSheetBL As JobSheetBL = New JobSheetBL()
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)
        objJobSheetBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)

        SessionManager.setJobSheetSummaryDs(dsJobSheetSummary)

    End Sub
    Private Sub resetJobSheetControls()
        lblJSN.Text = String.Empty
        lblPlannedWorkComponent.Text = String.Empty
        lblTrade.Text = String.Empty
        lblStatus.Text = String.Empty
        lblStatus.Visible = True
        StatusDropDown.Visible = Not (lblStatus.Visible)
        lblContractor.Text = String.Empty
        lblContact.Text = String.Empty
        txtWorksRequired.Text = String.Empty

        lblPONo.Text = String.Empty
        lblOrderedBy.Text = String.Empty
        lblOrdered.Text = String.Empty

        lblBudget.Text = String.Empty
        lblEstimate.Text = String.Empty
        lblVat.Text = String.Empty
        lblNetCost.Text = String.Empty

        lblScheme.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblHouseNumber.Text = String.Empty

        lblCustomer.Text = String.Empty
        lblTelephone.Text = String.Empty
        lblMobile.Text = String.Empty
        lblEmail.Text = String.Empty

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()
    End Sub

#End Region
End Class