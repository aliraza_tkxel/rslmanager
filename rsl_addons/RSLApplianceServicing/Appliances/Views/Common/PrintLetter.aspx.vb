﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports System.Threading

Public Class PrintLetter
    Inherits PageBase


#Region "Attributes"
    Dim stdLetterId As Integer
    Dim propertyId As String = String.Empty
    Dim rentBalance As Double = 0.0
    Dim rentCharge As Double = 0.0
    Dim letterBl As LettersBL = New LettersBL()
#End Region

#Region "Page_Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim resultDataSet As DataSet = New DataSet()
            Me.getCustomerAndPropertyRbRc(resultDataSet)
            Me.setLetterLabels(resultDataSet)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "Clickheretoprint()", True)
            'Response.Write("<script type='text/JavaScript'>Clickheretoprint2();</script>")
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "get Customer And Property Rb Rc"

    Protected Sub getCustomerAndPropertyRbRc(ByRef resultDataSet As DataSet)

        Me.propertyId = SessionManager.getPropertyIdForEditLetter()
        letterBl.getCustomerAndPropertyRbRc(resultDataSet, Me.propertyId, Me.rentBalance, Me.rentCharge)

    End Sub

#End Region

#Region "Set Letter Labels"
    Private Sub setLetterLabels(ByRef resultDataSet As DataSet)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.TanencyRecordNotFound, True)
            Exit Sub
        End If

        Me.lblTenancyRef.Text = resultDataSet.Tables(0).Rows(0).Item("Tenancy")

        Me.lblDate.Text = Replace(String.Format("{0} {1:Y}", Date.Today.Day, Date.Today), ",", "")
        Me.lblTenantName.Text = resultDataSet.Tables(0).Rows(0).Item("TenantName")
        Me.lblHouseNumber.Text = resultDataSet.Tables(0).Rows(0).Item("HouseNumber")
        Me.lblAddressLine1.Text = resultDataSet.Tables(0).Rows(0).Item("Address1")

        'Check values for empty strings to avoid blank lines.
        If Not resultDataSet.Tables(0).Rows(0).Item("Address2") = String.Empty Then
            Me.lblAddressLine2.Text = resultDataSet.Tables(0).Rows(0).Item("Address2") + "<br />"
        Else
            Me.lblAddressLine2.Visible = False
        End If
        If Not resultDataSet.Tables(0).Rows(0).Item("TownCity") = String.Empty Then
            Me.lblTownCity.Text = resultDataSet.Tables(0).Rows(0).Item("TownCity") + "<br />"
        Else
            Me.lblTownCity.Visible = False
        End If
        If Not resultDataSet.Tables(0).Rows(0).Item("County") = String.Empty Then
            Me.lblCounty.Text = resultDataSet.Tables(0).Rows(0).Item("County") + "<br />"
        Else
            Me.lblCounty.Visible = False
        End If

        If Not resultDataSet.Tables(0).Rows(0).Item("PostCode") = String.Empty Then
            Me.lblPostCode.Text = resultDataSet.Tables(0).Rows(0).Item("PostCode")
        Else
            Me.lblPostCode.Visible = False
        End If

        Me.lblTenantNameSalutation.Text = resultDataSet.Tables(0).Rows(0).Item("TenantNameSalutation")

        Dim todayDate As Date = Date.Today
        Dim letterBodyValue As String = String.Empty

        Dim printLetterBo As PrintLetterBO = New PrintLetterBO()
        printLetterBo = SessionManager.getPrintLetterBo()
        letterBodyValue = printLetterBo.LetterBody


        'To make first letter of signoff capital and other letters small
        Me.lblSignOffTitle.Text = printLetterBo.SignOff.Substring(0, 1).ToUpper & printLetterBo.SignOff.Substring(1).ToLower

        Me.lblFromResource.Text = printLetterBo.From

        'To Show Job title of from employee in place of team name
        'Me.lblResourceJobTitle.Text = printLetterBo.ResourceTeamName

        'Get From Direct Dial Contact, Email and Job Title From Database By Employee ID
        letterBl.getFromTelEmailPrintLetterByEmployeeID(printLetterBo)

        Me.lblResourceJobTitle.Text = printLetterBo.FromJobTitle

        Me.lblDirectDial.Text = printLetterBo.FromDirectDial
        Me.lblEmail.Text = printLetterBo.FromEmail

        letterBodyValue = GeneralHelper.repalceRcRBTd(letterBodyValue, rentBalance, rentCharge, todayDate)

        lblLetterBody.Text = letterBodyValue
    End Sub
#End Region

End Class