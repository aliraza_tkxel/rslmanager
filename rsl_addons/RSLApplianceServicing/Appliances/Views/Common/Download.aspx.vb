﻿Imports System.IO
Imports AS_Utilities
Imports AS_BusinessLogic
Imports Ionic.Zip
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Download
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

#Region "Attributes"
    Private docId As Integer = 0
    Private journalHistoryId As String = String.Empty
    Private fileName As String = String.Empty
    Private filePath As String = String.Empty
    Private fileExt As String = String.Empty
    Private type As String = String.Empty
    Private isCp12Document As Boolean = False
    Private cP12DocumentID As Integer = -1
    Private inspectionDocumentId As Integer = -1
    Private isRecordInspected As Boolean = False
    Private cp12DocByteArr As Byte()
    Private isHistoryCp12Document As Boolean = False
    Private historyCP12DocumentID As Integer = -1
    Private isPropertyDocument As Boolean = False
    Private isPlannedInspectionDocument As Boolean = False
    Private propertyDocumentId As Integer = -1
    Private inspectionJournalHistoryId As Integer = -1
    Private isEpcCertificate As Boolean = False

    Dim objPropertyBl As New PropertyBL()
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Try
            Me.setFileCredentials()
            If uiMessageHelper.IsError = False Then
                Dim objPropBl As New PropertyBL()

                If isCp12Document Then
                    downloadCP12Document()
                ElseIf isRecordInspected Then
                    downloadInspectionRecordDocument()
                ElseIf isHistoryCp12Document Then
                    downloadHistoryCp12Document()
                ElseIf isPropertyDocument Then
                    downloadPropertyDocument()
                ElseIf isPlannedInspectionDocument Then
                    downloadPlannedInspectionDocument()
                ElseIf isEpcCertificate Then
                    downloadEpcCertificateDocument()
                Else
                    objPropBl.getPropertyDocument(Me.docId, Me.journalHistoryId, Me.fileName, Me.filePath)
                    Me.downloadFile()
                End If

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "set File Credentials"
    Private Sub setFileCredentials()
        If Request.QueryString("did") IsNot Nothing And Request.QueryString("jhid") IsNot Nothing And Request.QueryString("doc") IsNot Nothing Then
            docId = Convert.ToInt32(Request.QueryString("did"))
            journalHistoryId = Convert.ToInt32(Request.QueryString("jhid"))

        ElseIf Request.QueryString("inspectionId") IsNot Nothing AndAlso Request.QueryString("inspectionId") > 0 Then
            isRecordInspected = True
            inspectionDocumentId = CType(Request.QueryString("inspectionId"), Integer)

        ElseIf Request.QueryString("CP12DocumentID") IsNot Nothing AndAlso Request.QueryString("CP12DocumentID") > 0 Then
            isCp12Document = True
            cP12DocumentID = CType(Request.QueryString("CP12DocumentID"), Integer)

        ElseIf Request.QueryString("HistoryCP12DocumentID") IsNot Nothing AndAlso Request.QueryString("HistoryCP12DocumentID") > 0 Then
            isHistoryCp12Document = True
            historyCP12DocumentID = CType(Request.QueryString("HistoryCP12DocumentID"), Integer)
        ElseIf Request.QueryString("PropertyDocumentID") IsNot Nothing AndAlso Request.QueryString("PropertyDocumentID") > 0 Then
            isPropertyDocument = True
            propertyDocumentId = CType(Request.QueryString("PropertyDocumentID"), Integer)
        ElseIf Request.QueryString("inspectionJournalHistoryId") IsNot Nothing AndAlso Request.QueryString("inspectionJournalHistoryId") > 0 Then
            isPlannedInspectionDocument = True
            inspectionJournalHistoryId = CType(Request.QueryString("inspectionJournalHistoryId"), Integer)
        ElseIf Request.QueryString("DocumentType") IsNot Nothing Then
            If Request.QueryString("DocumentType") = "EPC_CERTIFICATE" Then
                isEpcCertificate = True
            End If

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidUrl, True)
        End If
    End Sub
#End Region

#Region "download File"
    Public Sub downloadFile()
        Try
            Me.fileExt = Me.fileName.Substring(Me.fileName.LastIndexOf("."))

            If (Directory.Exists(Me.filePath) = False) Then
                Throw New DirectoryNotFoundException(String.Format(UserMessageConstants.DirectoryDoesNotExist, Me.filePath))
            Else
                If File.Exists(Me.filePath + Me.fileName) = False Then
                    Throw New FileNotFoundException(String.Format(UserMessageConstants.FileDoesNotExist, Me.filePath + Me.fileName))
                End If
            End If

            Response.Clear()
            Select Case Me.fileExt(1)
                Case "pdf"
                    Me.type = "application/pdf"
                    Exit Select
                Case "doc"
                    Me.type = "application/vnd.ms-word"
                    Exit Select
                Case "docx"
                    Me.type = "application/vnd.ms-word"
                    Exit Select
                Case "xls"
                    Me.type = "application/vnd.ms-excel"
                    Exit Select
                Case "xlsx"
                    Me.type = "application/vnd.ms-excel"
                    Exit Select
                Case "png"
                    Me.type = "application/image/png"
                    Exit Select
                Case "jpg"
                    Me.type = "application/image/jpg"
                    Exit Select

            End Select

            Response.AppendHeader("content-disposition", "attachment; filename=" + Me.fileName)
            Response.ContentType = Me.type
            Response.WriteFile(Me.filePath + Me.fileName)
            Response.Flush()
            Response.End()
        Catch ex As ConfigurationErrorsException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.DocumentUploadPathKeyDoesNotExist
        Catch ex As DirectoryNotFoundException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Catch ex As FileNotFoundException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Download CP12Document"

    Private Sub downloadCP12Document()
        Try

            Dim LGSRID As Integer = cP12DocumentID

            Dim dataSet As New DataSet()
            objPropertyBl.getCP12DocumentByLGSRID(dataSet, LGSRID)

            If dataSet.Tables.Count > 0 AndAlso dataSet.Tables(0).Rows.Count > 0 Then
                Dim propertyID As String = String.Empty
               
                With dataSet.Tables(0).Rows(0)
                    If Not (IsDBNull(.Item("PROPERTYID"))) Then
                        propertyID = DirectCast(.Item("PROPERTYID"), String)
                    ElseIf Not (IsDBNull(.Item("BLOCKID"))) Then '
                        propertyID = .Item("BLOCKID").ToString()
                    ElseIf Not (IsDBNull(.Item("SCHEMEID"))) Then
                        propertyID = .Item("SCHEMEID").ToString()
                   End If

                    If Not IsDBNull(.Item("CP12DOCUMENT")) Then
                        Dim CP12DOCUMENT As [Byte]() = DirectCast(.Item("CP12DOCUMENT"), [Byte]())

                        ' Send the file to the browser
                        Response.AddHeader("Content-type", "application/pdf")
                        Response.AddHeader("Content-Disposition", "attachment; filename=CP12Document_" & propertyID & ".pdf")

                        Response.BinaryWrite(CP12DOCUMENT)
                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
                    End If

                End With
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                Response.Flush()
                Response.End()
            End If

        End Try
    End Sub

#End Region

#Region "Download Inspection Record Document"
    Private Sub downloadInspectionRecordDocument()
        Try
            Dim dataSet As New DataSet()
            objPropertyBl.getInspectionRecordByInspectionId(dataSet, inspectionDocumentId)

            If dataSet.Tables.Count > 0 AndAlso dataSet.Tables(0).Rows.Count > 0 Then
                With dataSet.Tables(0).Rows(0)
                    Dim propertyID As String = DirectCast(.Item("PropertyId"), String)

                    If Not IsDBNull(.Item("InspectionDocument")) Then
                        Dim InspectionDocument As [Byte]() = DirectCast(.Item("InspectionDocument"), [Byte]())

                        ' Send the file to the browser
                        Response.AddHeader("Content-type", "application/pdf")
                        Response.AddHeader("Content-Disposition", "attachment; filename=InspectionRecordDocument_" & propertyID & ".pdf")

                        Response.BinaryWrite(InspectionDocument)
                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.InspectionDocumentNotAvailable
                    End If

                End With
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                Response.Flush()
                Response.End()
            End If

        End Try
    End Sub
#End Region

#Region "Download History Cp12 Document"
    Private Sub downloadHistoryCp12Document()
        Try

            Dim LGSRHISTORYID As Integer = historyCP12DocumentID

            Dim dataSet As New DataSet()
            objPropertyBl.getCP12DocumentByLGSRHISTORYID(dataSet, LGSRHISTORYID)

            If dataSet.Tables.Count > 0 AndAlso dataSet.Tables(0).Rows.Count > 0 Then
                With dataSet.Tables(0).Rows(0)
                    Dim propertyID As String = DirectCast(.Item("PROPERTYID"), String)

                    If Not IsDBNull(.Item("CP12DOCUMENT")) Then
                        Dim CP12DOCUMENT As [Byte]() = DirectCast(.Item("CP12DOCUMENT"), [Byte]())


                        ' Send the file to the browser
                        Response.AddHeader("Content-type", "application/pdf")
                        Response.AddHeader("Content-Disposition", "attachment; filename=CP12Document_" & propertyID & ".pdf")

                        Response.BinaryWrite(CP12DOCUMENT)
                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
                    End If

                End With
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                Response.Flush()
                Response.End()
            End If

        End Try
    End Sub
#End Region

#Region "Download Property Document"
    Private Sub downloadPropertyDocument()
        Try
            Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
            Dim objPropertyBL As PropertyBL = New PropertyBL()

            If propertyDocumentId > 0 Then
                objPropertyDocumentBO.DocumentId = propertyDocumentId
                objPropertyBL.getPropertyDocumentInformationById(objPropertyDocumentBO)
                OpenOrDownloadPropertyDocument(objPropertyDocumentBO, False)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                'Response.Flush()
                'Response.End()
            End If

        End Try
    End Sub
#End Region

#Region "Download Epc Certificate Document"
    Private Sub downloadEpcCertificateDocument()
        Try
            Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            objPropertyBL.getLatestEpcDocument(objPropertyDocumentBO)
            Me.OpenOrDownloadPropertyDocument(objPropertyDocumentBO, True)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                Response.Flush()
                Response.End()
            End If

        End Try
    End Sub
#End Region

#Region "Download Planned Inspection Document"
    Private Sub downloadPlannedInspectionDocument()
        Try
            Dim dataSet As New DataSet()
            objPropertyBl.getPlannedInspectionDocument(dataSet, inspectionJournalHistoryId)
            Me.filePath = GeneralHelper.getDocumentUploadPath()
            If (Directory.Exists(Me.filePath) = False) Then
                Throw New DirectoryNotFoundException(String.Format(UserMessageConstants.DirectoryDoesNotExist, Me.filePath))
            End If

            If dataSet.Tables(0).Rows().Count = 1 Then
                Me.fileName = dataSet.Tables(0).Rows(0).Item("DocumentName")
                Me.downloadFile()
            ElseIf dataSet.Tables(0).Rows().Count > 1 Then
                Using zip As New ZipFile()
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary
                    For Each row In dataSet.Tables(0).Rows()
                        Me.fileName = row.Item("DocumentName")
                        If File.Exists(Me.filePath + Me.fileName) = False Then
                            Throw New FileNotFoundException(String.Format(UserMessageConstants.FileDoesNotExist, Me.filePath + Me.fileName))
                        End If
                        zip.AddFile(filePath + fileName, "")
                    Next
                    Response.Clear()
                    Response.BufferOutput = False
                    Dim zipName As String = [String].Format("PlannedDocs_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"))
                    Response.ContentType = "application/zip"
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName)

                    zip.Save(Response.OutputStream)

                    Response.[End]()
                    Response.Flush()
                End Using
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.NoDocumentsFound
            End If

        Catch ex As ConfigurationErrorsException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.DocumentUploadPathKeyDoesNotExist
        Catch ex As DirectoryNotFoundException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Catch ex As FileNotFoundException
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try

    End Sub
#End Region

#Region "Open Or Download Property Document"
    Private Sub OpenOrDownloadPropertyDocument(ByVal objPropertyDocumentBO As PropertyDocumentBO, Optional ByVal openInBrowser As Boolean = False)
        Dim fileDirectoryPath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + SessionManager.getPropertyId() + "/Documents/")

        If (Directory.Exists(fileDirectoryPath) = True) Then
            If (File.Exists(fileDirectoryPath + objPropertyDocumentBO.DocumentPath)) Then
                Dim filePath = fileDirectoryPath + objPropertyDocumentBO.DocumentPath
                Dim fileExtension As String = Path.GetExtension(objPropertyDocumentBO.DocumentPath)

                If openInBrowser = True Then
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", "inline; filename=" + objPropertyDocumentBO.DocumentPath)
                Else
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + objPropertyDocumentBO.DocumentPath)
                End If

                Response.TransmitFile(filePath)
                Response.Flush()
                Response.End()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FileNotExist, True)
            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FileNotExist, True)
        End If
    End Sub
#End Region


End Class