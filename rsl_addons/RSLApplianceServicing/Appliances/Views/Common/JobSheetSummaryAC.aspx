﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JobSheetSummaryAC.aspx.vb" Inherits="Appliances.JobSheetSummaryAC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <link href="../../Styles/masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Navigation.css" rel="stylesheet" media="screen" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 50%;
        }
        .style2
        {
            width: 15%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="overflow:hidden">
    <div>
       <div class="headingTitle" style="width: 100%">
       <b>Planned works</b>
       </div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
         </asp:Panel>   
         <br />   
    <div style="height: auto; clear: both;">
        <table id="tbl_header" style="width: 98%; line-height: 20px;" cellpadding="0px" cellspacing="0px">
            <tr>
                <td style="width: 5%;">
                </td>
                <td colspan="3">
                    <div style="text-align: right;">
                        <asp:Button ID="btnPrevious" Enabled="false" runat="server" Text="&lt; Previous"
                            Width="100px" />
                        &nbsp;
                        <asp:Button ID="btnNext" Enabled="false" runat="server" Text="Next >" Width="100px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="height: 30px;">
                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                </td>
                <td colspan="2" style="border-bottom: 1px solid #7F7F7F;">
                    <b>Assigned To Contractor Summary</b>
                </td>
                <td style="width: 30%; border-bottom: 1px solid #7F7F7F; text-align: right;">
                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="Page"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr style='height: 10px;'>
                <td style="width: 5%;">
                </td>
                <td colspan="3">
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                    border-left: 1px solid #7F7F7F;">
                </td>
                <td style="width: 20%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>PMO:</b>&nbsp;
                    <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
                <td style="width: 40%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>Planned works component:</b>&nbsp;
                    <asp:Label ID="lblPlannedWorkComponent" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 30%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>Trade:</b>&nbsp;
                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                    border-right: 1px solid #7F7F7F;">
                </td>
            </tr>
        </table>
        <table id="Table1" style="width: 100%; margin-top: 0px; line-height: 18px;">
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>PO Number:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Status:</b>&nbsp;
                </td>
                <td align="left" style="width: 35%;">
                    <asp:DropDownList ID="StatusDropDown" runat="server" CssClass="StatusDropdown" Visible="False"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Contractor:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Budget:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Contact:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Estimate:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Works Required:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 230px;
                        height: 40px;" ReadOnly="true"></asp:TextBox>
                </td>
                <td style="width: 10%;">
                    <b>NET Cost:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Ordered By:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>VAT:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Ordered:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Total:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 10%;">
                    <b>Order Total:&nbsp;</b>
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="vertical-align: middle;">
                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForScheme" runat="server" Text="Scheme:"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForCustomer" runat="server" Text="Customer:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForBlock" runat="server" Text="Block"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForTelephone" runat="server" Text="Telephone:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForPostCode" runat="server" Text="Postcode:"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForMobile" runat="server" Text="Mobile:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForHouseNumber" runat="server" Text="Address:"></asp:Label>&nbsp;</b>
                </td>
                <td rowspan="2" style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForEmail" runat="server" Text="Email:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="vertical-align: middle;">
                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="vertical-align: top;">
                    <b>Asbestos:</b>
                </td>
                <td colspan="3">
                    <div style="height: 100px; width: 400px; overflow: auto;">
                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            BorderStyle="None" GridLines="None" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td colspan="4">
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
        </table>
    </div>
    </div>
    </form>
</body>
</html>
