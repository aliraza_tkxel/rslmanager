﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PrintCP12Document
    Inherits PageBase

#Region "Attributes"
    Private cP12DocumentID As Integer = -1
    Private reqType As String = String.Empty
#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Try
            Me.setFileCredentials()
            If uiMessageHelper.IsError = False Then
                showCP12Document()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "set File Credentials"

    Public Sub setFileCredentials()
        If (Request.QueryString("CP12DocumentID") IsNot Nothing AndAlso Request.QueryString("CP12DocumentID") > 0) AndAlso (Request.QueryString("ReqType") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString("ReqType"))) Then
            cP12DocumentID = CType(Request.QueryString("CP12DocumentID"), Integer)
            reqType = Request.QueryString("ReqType")

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidUrl, True)
        End If
    End Sub
#End Region

#Region "Show CP12 Document"

    Public Sub showCP12Document()
        Try
            Dim objPropertyBl As New PropertyBL()
            Dim LGSRID As Integer = cP12DocumentID
            Dim reqTypeId As String

            Dim dataSet As New DataSet()
            objPropertyBl.getCP12DocumentByLGSRID(dataSet, LGSRID)

            If dataSet.Tables.Count > 0 AndAlso dataSet.Tables(0).Rows.Count > 0 Then
                With dataSet.Tables(0).Rows(0)
                    If (reqType = "Property") Then
                        reqTypeId = DirectCast(.Item("PROPERTYID"), String)
                    ElseIf (reqType = "Scheme") Then
                        reqTypeId = Convert.ToString(.Item("SchemeId"))
                    Else
                        reqTypeId = Convert.ToString(.Item("BlockId"))
                    End If

                    If Not IsDBNull(.Item("CP12DOCUMENT")) Then
                        Dim CP12DOCUMENT As [Byte]() = DirectCast(.Item("CP12DOCUMENT"), [Byte]())

                        ' Send the file to the browser
                        Response.AddHeader("Content-type", "application/pdf")
                        Response.BinaryWrite(CP12DOCUMENT)
                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.Cp12DocumentNotAvailable
                    End If

                End With
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                'Flush the response to avoid blank window if there is no error.
                Response.Flush()
                Response.End()
            End If
            updPanelDownload.Update()
        End Try
    End Sub

#End Region

#End Region



End Class