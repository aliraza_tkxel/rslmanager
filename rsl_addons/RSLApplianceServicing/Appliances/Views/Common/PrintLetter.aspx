﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master"
    CodeBehind="PrintLetter.aspx.vb" Inherits="Appliances.PrintLetter" validateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script type="text/javascript">

        function Clickheretoprint() {
            self.print();

        }
    </script>
    <style type="text/css">
        .Arial12pt, .Arial12pt *
        {
            font-family: Arial !important;
            font-size: 11pt !important;
        }
        
        .gas_safe_logo
        {
            width: 19mm;
            position: absolute;
            margin-left: 13mm;
            margin-top: 0mm;
        }
        body
        {
            margin: 0mm;
        }
        .right-allign
        {
            text-align:right;
        }
        @page
        {
            margin: 0mm;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div id="gas_safe_logo" class="gas_safe_logo" style="display:none;">
        <asp:Image ID="Image1" ImageUrl="~/Images/gas-safe-logo.png" runat="server" />
    </div>--%>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div id="print_content" class="Arial12pt" style="margin-top: 24mm; margin-left: 20mm;
        position: absolute; margin-right: 12mm; font-size:11px; font-family:Tahoma;">
        Tenancy Ref:
        <asp:Label ID="lblTenancyRef" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="lblTenantName" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblHouseNumber" runat="server"></asp:Label>,
        <asp:Label ID="lblAddressLine1" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblAddressLine2" runat="server"></asp:Label>
        <asp:Label ID="lblTownCity" runat="server"></asp:Label>
        <asp:Label ID="lblCounty" runat="server"></asp:Label>
        <asp:Label ID="lblPostCode" runat="server"></asp:Label>
        <br />
        <br />
        <br />
        <div class="right-allign">
            Date:
            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <br />
        Dear
        <asp:Label ID="lblTenantNameSalutation" runat="server"></asp:Label>
        <br />
        <%--<br />
        <asp:Label ID="lblLetterTitle" runat="server"></asp:Label>
        <br />--%>
        <br />
        <asp:Label ID="lblLetterBody" runat="server"></asp:Label>
        
        <asp:Label ID="lblSignOffTitle" runat="server"></asp:Label><br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="lblFromResource" runat="server"></asp:Label>
        <br />
        <asp:Label ID="lblResourceJobTitle" runat="server"></asp:Label>
        <br />
        Direct Dial:
        <asp:Label ID="lblDirectDial" runat="server"></asp:Label>
        <br />
        Email:
        <asp:Label ID="lblEmail" runat="server"></asp:Label>
        <br />
    </div>
</asp:Content>
