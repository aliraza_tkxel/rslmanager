﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports AS_BusinessObject
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary
Imports System.Threading




Public Class JobSheetSummary
    'Inherits PageBase
    Inherits PageBase



#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then


                Dim journalHistoryId As Integer = CType(Request.QueryString(PathConstants.JournalHistoryId), Integer)
                Dim CurrentIndex As Integer = 1
                ViewState.Add(ViewStateConstants.CurrentIndex, CurrentIndex)
                populateJobSheet(journalHistoryId)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Populate Job Sheet"
    ''' <summary>
    ''' Populate Job Sheet Summary page
    ''' </summary>
    ''' <param name="journalHistoryId"></param>
    ''' <remarks></remarks>
    Public Sub populateJobSheet(ByVal journalHistoryId As Integer)

        Try
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            ViewState(ViewStateConstants.JournalHistoryId) = journalHistoryId
            objSchedulingBL.populateJobSheet(journalHistoryId, resultDataset)

            Dim resultDt As DataTable = resultDataset.Tables("PropertyInfo")
            Dim dtAppointmentsInfo As DataTable = resultDataset.Tables("AppointmentInfo")

            If dtAppointmentsInfo.Rows.Count > 0 Then

                Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))


                lblTotalSheets.Text = dtAppointmentsInfo.Rows.Count()
                lblSheetNumber.Text = currentIndex

                ViewState.Add(ViewStateConstants.TotalJobsheets, lblTotalSheets.Text)
                setPreviousNextButtonStates(currentIndex)

                Dim drAppointment() As DataRow
                drAppointment = dtAppointmentsInfo.Select("Row = " + Convert.ToString(currentIndex))

                'Populate Appointment Info
                lblPmo.Text = drAppointment(0).Item(ApplicationConstants.PmoColumn)
                lblComponent.Text = drAppointment(0).Item(ApplicationConstants.ComponentColumn)
                lblTrade.Text = drAppointment(0).Item(ApplicationConstants.TradesColumn)

                lblJsn.Text = drAppointment(0).Item(ApplicationConstants.JsnColumn)
                lblStatus.Text = drAppointment(0).Item(ApplicationConstants.InterimStatusColumn)
                lblOperative.Text = drAppointment(0).Item(ApplicationConstants.OperativeColumn)
                lblTime.Text = drAppointment(0).Item(ApplicationConstants.StartTimeColumn) + " - " + drAppointment(0).Item(ApplicationConstants.EndTimeColumn)

                If (IsDBNull(drAppointment(0).Item(ApplicationConstants.DurationsColumn))) Then
                    lblDuration.Text = ApplicationConstants.NotAvailable
                    'lblDurationTotal.Text = ApplicationConstants.NotAvailable
                ElseIf (drAppointment(0).Item(ApplicationConstants.IsMiscAppointmentColumn) = 1) Then
                    lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(0).Item(ApplicationConstants.DurationsColumn)))
                    'lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(0).Item(ApplicationConstants.TotalDurationColumn)))
                Else
                    lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(0).Item(ApplicationConstants.DurationsColumn)))
                    'lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(0).Item(ApplicationConstants.TotalDurationColumn)))
                End If


                Dim startDateTime As DateTime = Convert.ToDateTime(drAppointment(0).Item(ApplicationConstants.StartDateColumn))
                Dim startDay As String = startDateTime.DayOfWeek.ToString()
                Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(startDateTime.ToString("dd")))
                Dim startMonth As String = startDateTime.ToString("MMMMMMMMMMMMM")
                Dim startYear As String = startDateTime.ToString("yyyy")

                Dim endDateTime As DateTime = Convert.ToDateTime(drAppointment(0).Item(ApplicationConstants.EndDateColumn))
                Dim endDay As String = endDateTime.DayOfWeek.ToString()
                Dim endDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(endDateTime.ToString("dd")))
                Dim endMonth As String = endDateTime.ToString("MMMMMMMMMMMMM")
                Dim endYear As String = endDateTime.ToString("yyyy")

                lblStartDate.Text = startDay + " " + startDate + " " + startMonth + " " + startYear
                lblEndDate.Text = endDay + " " + endDate + " " + endMonth + " " + endYear


                If (IsDBNull(drAppointment(0).Item(ApplicationConstants.CustomerNotesColumn))) Then
                    txtCustAppointmentNotes.Text = String.Empty
                Else
                    txtCustAppointmentNotes.Text = drAppointment(0).Item(ApplicationConstants.CustomerNotesColumn)
                End If

                If (IsDBNull(drAppointment(0).Item(ApplicationConstants.JobsheetNotesColumn))) Then
                    txtJobSheetNotes.Text = String.Empty
                Else
                    txtJobSheetNotes.Text = drAppointment(0).Item(ApplicationConstants.JobsheetNotesColumn)
                End If
                If lblStatus.Text.ToUpper = "Complete".ToUpper Then
                    lblJobSheetNotes.Text = "Completion Notes:"
                Else
                    lblJobSheetNotes.Text = "Job Sheet Notes:"
                End If

                Dim propertyId As String = drAppointment(0).Item(ApplicationConstants.PropertyIdColumn)
                ViewState.Add(ViewStateConstants.PropertyId, propertyId)
            Else
                lblMessage.Text = "No Record Found"
            End If
            If resultDt.Rows.Count > 0 Then


                'Property Information
                lblScheme.Text = resultDt.Rows(0)("SchemeName")
                lblAddress.Text = resultDt.Rows(0)("HOUSENUMBER") + " " + resultDt.Rows(0)("ADDRESS1") + " " + resultDt.Rows(0)("ADDRESS2")
                lblTowncity.Text = resultDt.Rows(0)("TOWNCITY")
                lblCounty.Text = resultDt.Rows(0)("COUNTY")
                lblPostcode.Text = resultDt.Rows(0)("POSTCODE")

                'Customer Information
                hdnCustomerId.Value = resultDt.Rows(0)("CustomerId")
                lblCustomerName.Text = resultDt.Rows(0)("TenantName")
                lblCustomerTelephone.Text = resultDt.Rows(0)("Telephone")
                lblCustomerMobile.Text = resultDt.Rows(0)("Mobile")
                lblCustomerEmail.Text = resultDt.Rows(0)("Email")
            Else
                lblMessage.Text = "No Record Found"
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region



#End Region

#Region "Next Button Event"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex + 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Dim journalHistoryId As Integer = ViewState.Item(ViewStateConstants.JournalHistoryId)
            Me.setPreviousNextButtonStates(currentIndex)
            populateJobSheet(journalHistoryId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try




    End Sub
#End Region

#Region "Set previous next button states"
    Private Sub setPreviousNextButtonStates(ByVal updatedIndex As Integer)

        Dim currentIndex As Integer = updatedIndex
        Dim totalJobSheets As Integer = Convert.ToInt32(ViewState(ViewStateConstants.TotalJobsheets))

        lblSheetNumber.Text = currentIndex
        lblTotalSheets.Text = totalJobSheets

        'Previous Button state

        If (updatedIndex - 1 < 1) Then
            btnPrevious.Enabled = False
        Else
            btnPrevious.Enabled = True
        End If

        'Next Button state
        If (updatedIndex + 1 > totalJobSheets) Then
            btnNext.Enabled = False
        Else
            btnNext.Enabled = True
        End If

    End Sub
#End Region

#Region "Previous button clicked"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex - 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            Dim journalHistoryId As Integer = ViewState.Item(ViewStateConstants.JournalHistoryId)
            Me.setPreviousNextButtonStates(currentIndex)
            populateJobSheet(journalHistoryId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

End Class