﻿Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessObject
Imports System.Threading

Public Class WeeklyCalendar
    Inherits PageBase

#Region "Attributes"
    Public varWeeklyDT As DataTable = New DataTable()
    Dim objCalendarBL As CalendarBL = New CalendarBL()
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
    Dim dsEmployeeList As DataSet = New DataSet()
    Dim dsAppointments As DataSet = New DataSet()
    Dim dsEmployeeCoreWorkingHours As DataSet = New DataSet()
    Dim strAppointment As StringBuilder = New StringBuilder()
    Dim dsEmployeeLeaves As DataSet = New DataSet()
    Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
    Dim strEmployeeIds As StringBuilder = New StringBuilder()
    Dim rowCount As Integer
    Dim drLeaves() As DataRow
#End Region

#Region "Events"

#Region "Select Index Changed"
    ''' <summary>
    ''' Select Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rdBtnCalendarView_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdBtnCalendarView.SelectedIndexChanged
        Try
            If (rdBtnCalendarView.SelectedItem.Value = 1) Then
                Response.Redirect("MonthlyCalendar.aspx", False)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Page Load "

    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If (Not IsNothing(Request.QueryString("id"))) Then
                Me.displayScheduleAppointmentSearchResults()
            Else
                Me.displayCalendarScheduleAppointment()
            End If

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Function"

#Region "display Schedule Appointment For Weekly Calendar"
    Protected Sub displayCalendarScheduleAppointment()
        ApplicationConstants.btnSearchClicked = False
        'Calendar code start here
        If (Request.QueryString("e") = "n") Then
            'Add 7 days to start date to move to next week
            SessionManager.setWeeklyCalendarStartDate(DateAdd(DateInterval.Day, 7, SessionManager.getWeeklyCalendarStartDate()))
        ElseIf (Request.QueryString("e") = "p") Then
            'Subtract 7 days from start date to move to previous week
            SessionManager.setWeeklyCalendarStartDate(DateAdd(DateInterval.Day, -7, SessionManager.getWeeklyCalendarStartDate()))
        Else
            SessionManager.setWeeklyCalendarStartDate(Date.Today)
        End If

        'Seek for Monday as starting and Sunday ending day of the Week
        If (SessionManager.getWeeklyCalendarStartDate().ToString("dddd") <> "Monday") Then
            SessionManager.setWeeklyCalendarStartDate(objCalendarUtilities.calculateMondayFunction(SessionManager.getWeeklyCalendarStartDate()))
        End If
        'Add 6 days to start date(Monday) to get Last date(Sunday) of the week.
        SessionManager.setWeeklyCalendarEndDate(DateAdd("d", 6, SessionManager.getWeeklyCalendarStartDate()))

        lblMonth.Text = MonthName(SessionManager.getWeeklyCalendarStartDate().Month) + " " + Year(SessionManager.getWeeklyCalendarStartDate()).ToString()

        objCalendarBL.getEngineersForCalendarAppointment(dsEmployeeList)
        'Sending Start and End date
        objScheduleAppointmentsBO.StartDate = SessionManager.getWeeklyCalendarStartDate()
        objScheduleAppointmentsBO.ReturnDate = SessionManager.getWeeklyCalendarEndDate()
        objCalendarBL.getAppointmentsWeeklyCalendarDetail(objScheduleAppointmentsBO, dsAppointments)

        If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Gas Engineer exist
            'Creating Columns based on Engineers names
            varWeeklyDT.Columns.Add(" ")
            For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
                strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")

            Next

            'Getting Employees Leaves Information
            objScheduleAppointmentsBO.StartDate = SessionManager.getWeeklyCalendarStartDate()
            objScheduleAppointmentsBO.ReturnDate = SessionManager.getWeeklyCalendarEndDate()
            objScheduleAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
            objSchedulingBL.getEngineerLeavesDetail(objScheduleAppointmentsBO, dsEmployeeLeaves)
            objCalendarBL.getEmployeeCoreWorkingHours(dsEmployeeCoreWorkingHours, strEmployeeIds.ToString().TrimEnd(","))

            'End Getting Employees Leaves Information

            'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
            Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
            'Dim dr() As DataRow = dsAppointments.Tables(0).Select("(AppointmentDate>= '" + ApplicationConstants.startDate.ToShortDateString() + "' and AppointmentDate<='" + ApplicationConstants.endDate.ToShortDateString() + "')", "ASSIGNEDTO ASC")

            'Drawing Weekly Calendar
            'If (dr.Count > 0) Then 'Check if record exist
            For dayCount = 0 To 6
                scheduledAppt(0) = DateAdd("d", dayCount, SessionManager.getWeeklyCalendarStartDate()).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                rowCount = 1
                For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                    'Adding Leaves Information
                    drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, SessionManager.getWeeklyCalendarStartDate()).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, SessionManager.getWeeklyCalendarStartDate()).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                    If (drLeaves.Count > 0) Then 'System.DateTime.Parse(dr(rdCount).Item(1)).ToString("dddd")
                        For leaveCounter As Integer = 0 To drLeaves.Count - 1
                            strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></div><br/>")
                        Next
                    End If

                    For rdCount = 0 To dsAppointments.Tables(0).Rows.Count - 1

                        'Check the appointments of the engineers in Data row
                        'If ((System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString()).ToString("dddd") >= ApplicationConstants.daysArray(dayCount) And System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString()).ToString("dddd") <= ApplicationConstants.daysArray(dayCount)) AndAlso dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString()) Then
                        'If ((dayCount + 1 >= System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString()).DayOfWeek _
                        '     And dayCount + 1 <= System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString()).DayOfWeek) _
                        '    AndAlso dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString()) Then


                        'Check the appointments of the engineers in Datarow
                        Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString())
                        Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString())
                        Dim currentDate As DateTime = DateAdd("d", dayCount, SessionManager.getWeeklyCalendarStartDate())
                        Dim operativeId As Integer = dsEmployeeList.Tables(0).Rows(dsColCount).Item(0)
                        Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + operativeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")

                        If (currentDate >= aptStartDate And currentDate <= aptEndDate _
                            And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() _
                            And dr.Count > 0) Then

                            If dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "No Entry" Then
                                strAppointment.Append("<div class='detailBox' style='background-color:#EA2A2A;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'>" + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + "<b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span><br />")
                            ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString() = "AM" Then
                                strAppointment.Append("<div class='detailBox' style='background-color:#F7FE2E;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'>" + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + "<b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span><br />")
                            ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString() = "PM" Then
                                strAppointment.Append("<div class='detailBox' style='background-color:#58F872;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'>" + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + "<b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span><br />")
                            End If
                            'Commented to hide phone number.
                            'If (dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() <> "") Then
                            '    strAppointment.Append("<tr><td><img src='../../images/img_cell.png'></td><td>" + dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() + "</td></tr>")
                            'End If
                            If (dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() <> "") Then
                                If dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString().Contains(",") Then
                                    Dim tempstr = dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString().Split(",")
                                    strAppointment.Append("<div style='margin-left:13px;'>")
                                    For Each name In tempstr
                                        strAppointment.Append(name + "<br />")
                                    Next
                                Else
                                    strAppointment.Append("<div style='margin-left:13px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() + "<br/>")
                                End If
                            End If
                            If (dsAppointments.Tables(0).Rows(rdCount).Item(4).ToString() <> "") Then
                                strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item(4).ToString() + "<br/>")
                            End If

                            'If (rdCount + 1 < dsAppointments.Tables(0).Rows.Count) Then
                            '    'strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp;" + objCalendarUtilities.CalculateDistance(dsAppointments.Tables(0).Rows(rdCount).Item(6).ToString(), dsAppointments.Tables(0).Rows(rdCount + 1).Item(6).ToString()) + " m<br/><b>" + dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() + "</b></span></div></div><br/>")
                            '    strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp;" + Convert.ToString(0) + " m<br/><b>" + dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() + "</b></span></div></div><br/>")
                            'Else
                            '    strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp; 0m <br/><b>" + dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() + "</b></span></div></div><br/>")
                            'End If

                            strAppointment.Append("<span style='margin-right:2px;'><b>" + dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() + "</b></span>")

                            strAppointment.Append("</div></div><br/>")

                        End If

                    Next


                    'Changed the if else condition to if only to show appointments along with leaves.
                    'So commented out this End it and moved it to top.
                    'End If
                    If (strAppointment.ToString() <> "") Then
                        ' strAppointment.Append(DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"))
                        scheduledAppt(rowCount) = strAppointment.ToString()
                        strAppointment.Clear()
                    Else
                        scheduledAppt(rowCount) = "" 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy") '""
                    End If
                    rowCount = rowCount + 1
                Next

                varWeeklyDT.Rows.Add(scheduledAppt)

            Next
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.GasEngNotFound, True)
        End If
        'Else
        'lblMessage.Text = "No record found"
        'End If
    End Sub

#End Region

#Region "Get Appointment Start and End Time"

    Function getAppointmentStartEndTime(ByVal dsAppointments As DataSet, ByVal dsEmployeeCoreWorkingHours As DataSet, ByVal currentDate As DateTime, ByVal rdCount As Integer, ByVal employeeId As Integer)

        Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString())
        Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString())
        Dim aptStartTime As String = String.Empty
        Dim aptEndTime As String = String.Empty
        Dim empCoreStartTime As String = String.Empty
        Dim empCoreEndTime As String = String.Empty
        Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
        Dim ofcCoreEndTime As String = DateTime.Parse(GeneralHelper.getDayEndHour().ToString() + ":00").ToString("HH:mm")

        'Get employee core working hour
        Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + employeeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")
        If (dr.Count > 0) Then
            empCoreStartTime = DateTime.Parse(dr(0).Item(3).ToString()).ToString("HH:mm")
            empCoreEndTime = DateTime.Parse(dr(0).Item(4).ToString()).ToString("HH:mm")
        Else
            empCoreStartTime = ofcCoreStartTime
            empCoreEndTime = ofcCoreEndTime
        End If

        'Setting start and end time depending on cases
        'Case 1 : Single day appointment start time and end time exist in same day
        'Case 2 : Multiple day appointment where start time exist in current date
        'Case 3 : Multiple day appointment where end time exist in current date
        'Case 4 : Multiple day appointment where start and end time do not exist in current date

        If (aptStartDate = aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(8)).ToString("HH:mm")
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        ElseIf (currentDate = aptStartDate And currentDate <> aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(8)).ToString("HH:mm")
            aptEndTime = empCoreEndTime
        ElseIf (currentDate <> aptStartDate And currentDate = aptEndDate) Then
            aptStartTime = empCoreStartTime
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        Else
            aptStartTime = empCoreStartTime
            aptEndTime = empCoreEndTime
        End If

        Return aptStartTime + " - " + aptEndTime

    End Function

#End Region

#Region "display Schedule Appointment Search Results"
    Protected Sub displayScheduleAppointmentSearchResults()
        Dim appShift As String = "Morning"
        Dim appStyle As String = "detailBox"
        Dim boxColor As String = "#F7FE2E"

        lblMonth.Text = MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString()

        ' If (IsPostBack) Then
        objCalendarBL.getEngineersForCalendarAppointment(dsEmployeeList)
        objCalendarBL.getSearchedAppointmentsDetail(dsAppointments)
        'End If

        'Creating Columns based on Engineers names
        varWeeklyDT.Columns.Add(" ")
        For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
            objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
        Next

        'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
        Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
        Dim dr() As DataRow = dsAppointments.Tables(0).Select("APPOINTMENTID=" + Request.QueryString("id"), "AppointmentDate ASC")
        'Seek for Monday as starting and Sunday ending day of the Month
        If (dr.Count > 0) Then
            ApplicationConstants.startDate = DateTime.Parse(dr(0).Item(1).ToString())
        End If
        If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
            objCalendarUtilities.calculateMonday(ApplicationConstants.startDate)
        End If

        If (Request.QueryString("e") <> "p") Then
            ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
            'ApplicationConstants.endDate = DateAdd("d", 7, ApplicationConstants.startDate)
        End If
        'Drawing Weekly Calendar
        If (dr.Count > 0) Then 'Check if record exist
            For dayCount = 0 To 6
                'scheduledAppt(0) = DateAdd("d", dayCount, SessionManager.getWeeklyCalendarStartDate()).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                rowCount = 1
                For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                    For rdCount = 0 To dr.Count - 1
                        'Check the appointments of the engineers in Datarow
                        If (System.DateTime.Parse(dr(rdCount).Item(14)).ToString("dddd") = ApplicationConstants.daysArray(dayCount) And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0) = dr(rdCount).Item(4)) Then
                            If (Request.QueryString("id") = dr(rdCount).Item(13)) Then
                                appStyle = "detailSearchedBox"
                            End If
                            If dr(rdCount).Item(0).ToString() = "PM" Then
                                appShift = "Afternoon"
                                boxColor = "#58F872"
                            End If
                            If dr(rdCount).Item(6).ToString() = 3 Then
                                boxColor = "#EA2A2A"
                            End If
                            strAppointment.Append("<div class='" + appStyle + "' style='background-color:" + boxColor + ";padding:4px'><table><tr><td><img src='../../images/arrow_right.png'></td><td><span style='margin-right:10px;'>" + appShift + "<b>&nbsp;" + dr(rdCount).Item("Type") + "</b></span></td></tr>")
                            If (dr(rdCount).Item(3).ToString() <> "") Then
                                strAppointment.Append("<tr><td><img src='../../images/img_cell.png'></td><td>" + dr(rdCount).Item(3).ToString() + "</td></tr>")
                            End If
                            If (dr(rdCount).Item(2).ToString() <> "") Then
                                strAppointment.Append("<tr><td></td><td>" + dr(rdCount).Item(2).ToString() + "<br/>")
                            End If
                            If (dr(rdCount).Item(4).ToString() <> "") Then
                                strAppointment.Append(dr(rdCount).Item(4).ToString() + "<br/>")
                            End If

                            'Adding Distance to appointment

                            'If (rdCount + 1 < dr.Count) Then
                            '    'strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp;" + objCalendarUtilities.CalculateDistance(dr(rdCount).Item(6).ToString(), dr(rdCount + 1).Item(6).ToString()) + " m<br/><b>" + dr(rdCount).Item(11).ToString() + "</b></span></td></tr></table></div><br/>")
                            '    strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp;" + Convert.ToString(0) + " m<br/><b>" + dr(rdCount).Item(11).ToString() + "</b></span></td></tr></table></div><br/>")
                            'Else
                            '    strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp;0 m<br/><b> </b></span></td></tr></table></div><br/>") 'ApplicationConstants.appointmentStatus(dr(rdCount).Item(7) - 1) 
                            'End If

                            strAppointment.Append("<span style='margin-right:2px;'><b>" + dr(rdCount).Item(11).ToString() + "</b></span>")

                            strAppointment.Append("</td></tr></table></div><br/>")

                        End If

                    Next
                    If (strAppointment.ToString() <> "") Then
                        ' strAppointment.Append(filter)
                        scheduledAppt(rowCount) = strAppointment.ToString()
                        strAppointment.Clear()
                    Else
                        scheduledAppt(rowCount) = "" 'filter 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy") '""
                    End If
                    rowCount = rowCount + 1
                Next

                varWeeklyDT.Rows.Add(scheduledAppt)

            Next
        Else
            lblMessage.Text = "No record found"
        End If
    End Sub

#End Region

#End Region

End Class