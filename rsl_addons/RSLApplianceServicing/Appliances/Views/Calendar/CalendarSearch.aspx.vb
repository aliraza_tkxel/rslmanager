﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Text
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class CalendarSearch
    Inherits PageBase
#Region "Attributes"
    Public varWeeklyDT As DataTable = New DataTable()
    Dim objCalendarBL As CalendarBL = New CalendarBL()
    Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
    Dim dsAppointments As DataSet = New DataSet()
    Dim strAppointment As StringBuilder = New StringBuilder()
    'Dim filter As String
    Dim rowCount As Integer

#End Region

#Region "Events"

#Region "Page Load "
    ''' <summary>
    ''' Page Load 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'If (Not IsPostBack) Then
            Me.displayScheduleAppointmentSearchResults()
            'End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Function"

#Region "display Schedule Appointment Search Results"
    Protected Sub displayScheduleAppointmentSearchResults()
        ApplicationConstants.filter = ""
        'Creating Search Criteria
        If (Request.QueryString("ad") <> "Address" And Request.QueryString("ad") <> "" And Not IsNothing(Request.QueryString("ad"))) Then
            ApplicationConstants.filter = "CustAddress like'%" + Request.QueryString("ad") + "%')"
        End If
        If (Request.QueryString("pc") <> "Postcode" And Request.QueryString("pc") <> "" And Not IsNothing(Request.QueryString("pc"))) Then
            ApplicationConstants.filter = ApplicationConstants.filter.Replace("')", "' and") + "  POSTCODE='" + Request.QueryString("pc") + "')"

        End If
        If (Request.QueryString("pa") <> 0 And Not IsNothing(Request.QueryString("pa"))) Then
            ApplicationConstants.filter = ApplicationConstants.filter.Replace("')", "' and") + "PATCHID=" + Request.QueryString("pa") + ")"

        End If
        If (Request.QueryString("sc") <> "Please Select Scheme" And Not IsNothing(Request.QueryString("sc"))) Then
            ApplicationConstants.filter = ApplicationConstants.filter.Replace(")", "and") + " SCHEMENAME='" + Request.QueryString("sc") + "')"
        End If
        If (Request.QueryString("dd") <> "Due Date" And Request.QueryString("dd") <> "" And Not IsNothing(Request.QueryString("dd"))) Then
            ApplicationConstants.filter = ApplicationConstants.filter.Replace("')", "' and") + " AppointmentDateOnly='" + Request.QueryString("dd") + "')" ' System.DateTime.Parse(Request.QueryString("dd")).ToString("MM/dd/yyyy")
            ApplicationConstants.startDate = Request.QueryString("dd")
        End If
        If (Not IsNothing(Request.QueryString("ft"))) Then
            ApplicationConstants.filter = ApplicationConstants.filter.Replace(")", "and") + " INSPECTIONTYPEID='" + Request.QueryString("ft") + "')"
        End If

        objCalendarBL.getSearchedAppointmentsDetail(dsAppointments)
        If (dsAppointments.Tables(0).Rows.Count > 0 And ApplicationConstants.filter <> "") Then
            Dim dr() As DataRow = dsAppointments.Tables(0).Select("(" + ApplicationConstants.filter, "APPOINTMENTDATE ASC")
            If (dr.Count > 0) Then
                varWeeklyDT = dsAppointments.Tables(0).Clone()
                varWeeklyDT = dr.CopyToDataTable()
                RptSearchResults.DataSource = varWeeklyDT
                RptSearchResults.DataBind()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RecordNotFound, True)
            End If

        End If

    End Sub

#End Region

#End Region

End Class