﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="MonthlyCalendar.aspx.vb" Inherits="Appliances.MonthlyCalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="apparg" TagName="AppointmentArranged" Src="~/Controls/Scheduling/AppointmentArranged.ascx" %>
<%@ Register TagPrefix="apptarg" TagName="AppointmentToBeArranged" Src="~/Controls/Scheduling/AppointmentToBeArranged.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
       
     .dataTables_wrapper
        {
            width: 100%;
        }
        /* Applied his rule set to hide wrapper for fixed right column, as we are not using fixed right column
         * In case right column fix is required rule set must be removed/changed.
         */
        .DTFC_RightBodyLiner
        {
            display:none;
        }
    </style>
    <script src="../../Scripts/jquery.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            //applyFixHeader();

            var offset = $('#monthlyCalendar').offset();
            var myCalendartableheight = $('#monthlyCalendar').height();

            if (myCalendartableheight > $(window).height() - offset.top) {
                myCalendartableheight = $(window).height() - offset.top;
            }
            $("#divMonthlyCalendar").height($(window).height());
            myCalendartableheight -= 10;

            var calendarTable = $('#monthlyCalendar').DataTable({
                "searching": false,
                "paging": false,
                "ordering": false,
                "info": false,
                "scrollX": true,
                "scrollCollapse": true,
                "scrollY": myCalendartableheight
            });

            new $.fn.dataTable.FixedColumns(calendarTable, {
                heightMatch: 'none'
            });

            setTimeout(function () {
                var tblCalendarRows = $("#monthlyCalendar > tbody > tr");
                for (index = 0; index < tblCalendarRows.length; index++) {
                    $("div.DTFC_LeftBodyWrapper div.DTFC_LeftBodyLiner > table > tbody > tr").eq(index).height($("#monthlyCalendar > tbody > tr").eq(index).height());
                }
            }, 50);
        });    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="background-color: Black; color: White; border: solid 1px gray; height: 20px;
        padding: 8px;">
        <span style="font-size: 13px; font-weight: bold;">Schedule Calendar</span></div>
    <div class="group" style="margin: 10px 0; border: 1px solid gray;">
        <span style="float: left; padding-left: 12px; padding-top: 4px;">
            <asp:Label ID="lblMonth" runat="server" Font-Bold="True"></asp:Label>
        </span>
        <div id="calToolbar">
            <span style="float: left; padding-right: 19px;">
                <asp:RadioButtonList ID="rdBtnCalendarView" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Flow" EnableTheming="True" Font-Size="Small" AutoPostBack="True">
                    <asp:ListItem Value="0"><span style="vertical-align:top;">Week</span></asp:ListItem>
                    <asp:ListItem Selected="True" Value="1"><span style="vertical-align:top;">Month</span></asp:ListItem>
                </asp:RadioButtonList>
            </span>&nbsp;&nbsp;<span style="border: solid 1px gray; padding: 0px !important;
                float: left;">
                <asp:HyperLink ID="hlPrevious" runat="server" NavigateUrl="~/Views/Calendar/MonthlyCalendar.aspx?e=p"><img src="../../images/arrow_left.png" border="0" style="padding-top: 2px;
    vertical-align: text-top;"/></asp:HyperLink>
                <span style="border-left: solid 1px gray; border-right: solid 1px gray; vertical-align: top;">
                    <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="~/Views/Calendar/MonthlyCalendar.aspx">&nbsp;Today</asp:HyperLink>
                </span>
                <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="~/Views/Calendar/MonthlyCalendar.aspx?e=n"><img src="../../images/arrow_right.png" border="0" style="padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;"/></asp:HyperLink>
            </span>
        </div>
    </div>
    <div id="divMonthlyCalendar" style="width: auto; overflow: auto;">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server">
            </asp:Label>
        </asp:Panel>
        <table id="monthlyCalendar" class="fixedHeaderCalendar" width="100%" border="1" style="border: solid 1px gray;
            float: left;" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <%  Dim empArray() As String
                        For dtCount = 0 To varWeeklyDT.Columns.Count - 1%>
                    <th style="font-weight: bold; border-left: solid 1px gray; border-right: solid 1px gray;
                            border-bottom: solid 1px gray; text-align: center; min-width: 95px; white-space: nowrap; background-color:White;">
                        <span style="margin-bottom: 10px;">
                            <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                    empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                            %>
                            <%Response.Write(empArray(1).Trim())%><!--a href="#"> <img src="../../images/btn_add.png" border="0"></a-->
                            <%End If%>
                        </span>
                    </th>
                    <%Next%>
                </tr>
            </thead>
            <tbody>
                <%  Dim bgColor As String
                    For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                        If (rdCount Mod 2 = 0) Then
                            bgColor = "#E8E9EA"
                            
                        Else
                            bgColor = "#FFFFFF"
                        End If
                        
                %>
                <tr style='<%response.write("background-color:"+ bgColor+";")%>'>
                    <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                            If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then
                                If rdItemCount = 0 Then
                             
                    %>
                     <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 95px;
                            white-space: nowrap; font-weight:bold; ">
                            <%= varWeeklyDT.Rows(rdCount).Item(rdItemCount)%>
                    </td>
                    <%Else%>
                    <td style="border: solid 1px gray;">
                    <div style="max-height: 225px; overflow: auto; width:170px; ">
                        <%= varWeeklyDT.Rows(rdCount).Item(rdItemCount)%></div>
                    </td>
                    <%End If%><%Else%>
                    <td>
                        &nbsp;
                    </td>
                    <%End If%>
                    <%Next%>
                </tr>
                <%  Next%>
            </tbody>
        </table>
    </div>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            var offset = $("#divMonthlyCalendar").offset();
            var tableHeight = $("#monthlyCalendar").height();
            if (tableHeight > $(window).height() - offset.top) {
                $("#divMonthlyCalendar").height($(window).height() - offset.top);
                $('#monthlyCalendar').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });
            }
            else {
                $('#monthlyCalendar').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });
                $("#divMonthlyCalendar").height(tableHeight);
            }
        });
    </script>--%>
</asp:Content>
