﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master" CodeBehind="CalendarSearch.aspx.vb" Inherits="Appliances.CalendarSearch" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
    <div style=" background-color:Black; color:White;border: solid 1px gray;height: 20px;padding: 8px;" >
        <span style="font-size:13px; font-weight:bold;">Schedule Calendar</span></div>
    <div class="group" style="margin: 10px 0;  border: 1px solid gray;">
   
        <strong><span style="font: 12pt verdana; font-weight:bold; padding-left:8px;">Results</span> </strong></div>
            <div>
             <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server">
        </asp:Label>
    </asp:Panel>
     <ASP:Repeater id="RptSearchResults" runat="server">
      <HeaderTemplate>
         <table width="100%" style="font: 10pt verdana">
         <tr style="font-weight:bold; border:1px solid gray">
        <td width="235" rowspan="2" style="font-weight:bold; border:1px solid gray">Address</td>
        <td colspan="5" style="font-weight:bold; border:1px solid gray; text-align:center" >Appointment</td>
      </tr>
      <tr style="font-weight:bold; border:1px solid gray">
        <td width="135" style="font-weight:bold; border:1px solid gray">Status</td>
        <td width="131" style="font-weight:bold; border:1px solid gray">Type</td>
        <td width="126" style="font-weight:bold; border:1px solid gray">Date</td>
        <td width="160" style="font-weight:bold; border:1px solid gray">Time</td>
        <td width="160" style="font-weight:bold; border:1px solid gray">Operative</td>
      </tr>
      </HeaderTemplate>
      <ItemTemplate>
            <tr style="border: solid 1px gray;text-align: center;">
               <td align="center" style=" border:1px solid gray">
                  <%# DataBinder.Eval(Container.DataItem, "CustAddress")%>
               </td>
               <td style=" border:1px solid gray">
                  <%# DataBinder.Eval(Container.DataItem, "Title")%>
               </td style=" border:1px solid gray">
               <td style=" border:1px solid gray">
                  <%# DataBinder.Eval(Container.DataItem, "Type") %>
               </td>
               <td style=" border:1px solid gray">
                  <%# System.DateTime.Parse(DataBinder.Eval(Container.DataItem, "AppointmentDateOnly")).ToShortDateString%>
               </td>
               <td style=" border:1px solid gray">
                   <a style="color:Black; text-decoration:underline" href="<%= Request.QueryString("vi") %>?id=<%# DataBinder.Eval(Container.DataItem, "AppointmentId")%>"><%# System.DateTime.Parse(DataBinder.Eval(Container.DataItem, "APPOINTMENTSTARTTIME")).ToShortTimeString +" "+ Eval("AppointmentShift")%></a>
               </td>
               <td style=" border:1px solid gray">
                    <%# DataBinder.Eval(Container.DataItem, "EmpName") %>
               </td>
            </tr>
      </ItemTemplate>
      <FooterTemplate>
         </table>
      </FooterTemplate>
   </ASP:Repeater>
        </div>

</asp:Content>
