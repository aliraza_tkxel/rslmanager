﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/ASMasterPage.Master"
    CodeBehind="WeeklyCalendar.aspx.vb" Inherits="Appliances.WeeklyCalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--These script has been added to the master and accessible on every page using ASMasterPage.Master --%>
    <style type="text/css">
        #ContentPlaceHolder1_rdBtnCalendarView_0
        {
            border: none !important;
        }
        #ContentPlaceHolder1_rdBtnCalendarView_1
        {
            border: none !important;
        }
        .dataTables_wrapper
        {
            width: 100%;
        }
        /* Applied his rule set to hide wrapper for fixed right column, as we are not using fixed right column
         * In case right column fix is required rule set must be removed/changed.
         */
        .DTFC_RightBodyLiner
        {
            display: none;
        }
    </style>
    <script src="../../Scripts/jquery.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            //applyFixHeader();

            var offset = $('#weeklyCalander').offset();
            var myCalendartableheight = $('#weeklyCalander').height();

            if (myCalendartableheight > $(window).height() - offset.top) {
                myCalendartableheight = $(window).height() - offset.top;
            }
            $("#divWeeklyCalander").height($(window).height());
            myCalendartableheight -= 10;

            var calendarTable = $('#weeklyCalander').DataTable({
                "searching": false,
                "paging": false,
                "ordering": false,
                "info": false,
                "scrollX": true,
                "scrollCollapse": true,
                "scrollY": myCalendartableheight
            });
            debugger;
            new $.fn.dataTable.FixedColumns(calendarTable, {

                heightMatch: 'none'
            });

            setTimeout(function () {
                var tblCalendarRows = $("#weeklyCalander > tbody > tr");
                for (index = 0; index < tblCalendarRows.length; index++) {
                    $("div.DTFC_LeftBodyWrapper div.DTFC_LeftBodyLiner > table > tbody > tr").eq(index).height($("#weeklyCalander > tbody > tr").eq(index).height());
                }
            }, 50);
        });    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="background-color: Black; color: White; border: solid 1px gray; height: 20px;
        padding: 8px;">
        <span style="font-size: 13px; font-weight: bold;">Schedule Calendar</span></div>
    <div class="group" style="margin: 10px 0; border: 1px solid gray;">
        <span style="float: left; padding-left: 12px; padding-top: 4px;">
            <asp:Label ID="lblMonth" runat="server" Font-Bold="True"></asp:Label>
        </span>
        <div id="calToolbar_ks">
            <span style="float: left; padding-right: 19px;">
                <asp:RadioButtonList ID="rdBtnCalendarView" BorderStyle="None" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Flow" EnableTheming="True" Font-Size="Small" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="0"><span style="vertical-align:top;">Week</span></asp:ListItem>
                    <asp:ListItem Value="1"><span style="vertical-align:top;">Month</span></asp:ListItem>
                </asp:RadioButtonList>
            </span>&nbsp;&nbsp;<span style="border: solid 1px gray; padding: 0px !important;
                float: left;">
                <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="~/Views/Calendar/WeeklyCalendar.aspx?e=p"><img src="../../images/arrow_left.png" style="border-width:0px; padding-top: 2px;
    vertical-align: text-top;float:left" alt="arrow_left" /></asp:HyperLink>
                <span style="border-left: solid 1px gray; border-right: solid 1px gray; float: left;">
                    <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="~/Views/Calendar/WeeklyCalendar.aspx">Today</asp:HyperLink>
                </span>
                <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="~/Views/Calendar/WeeklyCalendar.aspx?e=n"><img src="../../images/arrow_right.png" style=" padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;float:left" alt="arrow_right"/></asp:HyperLink>
            </span>
        </div>
    </div>
    <div id="divWeeklyCalander" style="width: auto; overflow: auto;">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server">
            </asp:Label>
        </asp:Panel>
        <% 
            If varWeeklyDT.Rows.Count > 0 Then%>
        <table id="weeklyCalander" class="fixedHeaderCalendar" width="100%" border="1" style="border: 0px;
            border-left: solid 1px gray; border-top: solid 1px gray; border-collapse: collapse;
            width: 98%" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <%  Dim empArray() As String
                        For dtCount = 0 To varWeeklyDT.Columns.Count - 1%>
                    <th style="font-weight: bold; border-left: solid 1px gray; border-right: solid 1px gray;
                            border-bottom: solid 1px gray; text-align: center; min-width: 95px; white-space: nowrap; background-color:White;">
                        <span style="margin-bottom: 10px;">
                            <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                    empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                            %>
                            <%Response.Write(empArray(1).Trim())%>
                            <%End If%>
                        </span>
                    </th>
                    <%Next%>
                </tr>
            </thead>
            <tbody>
                <%  Dim bgColor As String
                    For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                        If (rdCount Mod 2 = 0) Then
                            bgColor = "#E8E9EA"
                            
                        Else
                            bgColor = "#FFFFFF"
                        End If
                %>
                <tr style='<%response.write("background-color:"+ bgColor+";")%>'>
                    <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                            If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then
                                If rdItemCount = 0 Then%>
                   <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 95px;
                            white-space: nowrap; font-weight:bold; ">
                            <%= varWeeklyDT.Rows(rdCount).Item(rdItemCount)%>
                    </td>
                    <%Else%>
                    <td style="border: solid 1px gray;">
                        <div style="max-height: 225px; overflow: auto; width: 170px;">
                            <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%></div>
                    </td>
                    <%End If%>
                    <%Else%>
                    <td>
                        &nbsp;
                    </td>
                    <% End If%>
                    <% Next%>
                </tr>
                <%  Next%>
            </tbody>
        </table>
        <%  End If%>
    </div>
    <%-- <script type="text/javascript">
         $(document).ready(function () {
             debugger;
             var divHeight = $("#divWeeklyCalander").height();
             var offset = $("#divWeeklyCalander").offset();
             var tableHeight = $("#weeklyCalander").height();
             if (tableHeight > $(window).height() - offset.top) {
                 console.log("in if");
                 $("#divWeeklyCalander").height($(window).height() - offset.top);
                 $('#weeklyCalander').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });
                 }
             else {
                 $("#divWeeklyCalander").height($(window).height());
                  $('#weeklyCalander').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: 1 });
             }
             console.log("out function");
         });
    </script>--%>
</asp:Content>
