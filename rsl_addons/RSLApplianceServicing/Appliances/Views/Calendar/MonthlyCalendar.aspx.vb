﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports AS_Utilities
Imports AS_BusinessLogic
Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Xml
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class MonthlyCalendar
    Inherits PageBase
#Region "Attributes"
    Public varWeeklyDT As DataTable = New DataTable()
    Dim objCalendarBL As CalendarBL = New CalendarBL()
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Dim dsEmployeeList As DataSet = New DataSet()
    Dim dsAppointments As DataSet = New DataSet()
    Dim strAppointment As StringBuilder = New StringBuilder()
    Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
    Dim dsEmployeeLeaves As DataSet = New DataSet()
    Dim dsEmployeeCoreWorkingHours As DataSet = New DataSet()
    Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
    Dim strEmployeeIds As StringBuilder = New StringBuilder()
    Dim rowCount As Integer
    Dim drLeaves() As DataRow

#End Region

#Region "Events"

#Region "Page Load "
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If (Not IsNothing(Request.QueryString("id"))) Then
                Me.displaySearchedMonthlyAppointments()
            Else
                Me.displayCalendarScheduleAppointment()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Select Index Changed"
    ''' <summary>
    ''' Select Index Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rdBtnCalendarView_SelectedIndexChanged1(ByVal sender As Object, ByVal e As EventArgs) Handles rdBtnCalendarView.SelectedIndexChanged
        Try
            If (rdBtnCalendarView.SelectedItem.Value = 0) Then
                Response.Redirect("WeeklyCalendar.aspx", False)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Function"

#Region "display Schedule Appointment For Monthly Calendar"
    Protected Sub displayCalendarScheduleAppointment()
        'Calendar code start here
        If (Request.QueryString("e") = "n") Then
            'Start - Changed By Aamir Waheed on July 17, 2013.
            'Changed the code to calculate start and end date of monthly calendar old code was causing wrong start and end dates

            ApplicationConstants.startDate = ApplicationConstants.startDate.AddDays(10).AddMonths(1)
            ApplicationConstants.startDate = DateSerial(ApplicationConstants.startDate.Year, ApplicationConstants.startDate.Month, 1)

            ApplicationConstants.monthDays = System.DateTime.DaysInMonth(Year(ApplicationConstants.startDate).ToString(), ApplicationConstants.startDate.Month)

            ApplicationConstants.endDate = DateSerial(ApplicationConstants.startDate.Year, ApplicationConstants.startDate.Month, ApplicationConstants.monthDays)
            'ApplicationConstants.startDate = ApplicationConstants.endDate
            'If (ApplicationConstants.startDate.Month + 1 > 12) Then
            '    ApplicationConstants.endDate = DateSerial(ApplicationConstants.startDate.Year() + 1, 1, 1)
            'Else
            '    ApplicationConstants.monthDays = System.DateTime.DaysInMonth(Year(ApplicationConstants.startDate).ToString(), ApplicationConstants.startDate.Month + 1)
            '    ApplicationConstants.endDate = DateAdd("d", ApplicationConstants.monthDays, DateSerial(ApplicationConstants.startDate.Year(), ApplicationConstants.startDate.Month(), 1))
            'End If



        ElseIf (Request.QueryString("e") = "p") Then

            ApplicationConstants.startDate = ApplicationConstants.startDate.AddDays(10).AddMonths(-1)

            ApplicationConstants.startDate = DateSerial(ApplicationConstants.startDate.Year, ApplicationConstants.startDate.Month, 1)

            ApplicationConstants.monthDays = System.DateTime.DaysInMonth(Year(ApplicationConstants.startDate).ToString(), ApplicationConstants.startDate.Month)

            ApplicationConstants.endDate = DateSerial(ApplicationConstants.startDate.Year, ApplicationConstants.startDate.Month, ApplicationConstants.monthDays)

            'ApplicationConstants.endDate = ApplicationConstants.startDate
            'If (ApplicationConstants.startDate.Month - 1 < 0) Then
            '    ApplicationConstants.endDate = DateSerial(ApplicationConstants.startDate.Year() - 1, 1, 1)
            'Else
            '    ApplicationConstants.monthDays = System.DateTime.DaysInMonth(Year(ApplicationConstants.endDate).ToString(), ApplicationConstants.startDate.Month)
            '    ApplicationConstants.startDate = DateSerial(ApplicationConstants.endDate.Year(), ApplicationConstants.endDate.Month(), 1)
            'End If


        Else

            ApplicationConstants.startDate = DateSerial(Now.Year(), Now.Month(), 1)
            ApplicationConstants.monthDays = System.DateTime.DaysInMonth(Year(ApplicationConstants.startDate).ToString(), ApplicationConstants.startDate.Month)

            ApplicationConstants.endDate = DateSerial(ApplicationConstants.startDate.Year, ApplicationConstants.startDate.Month, ApplicationConstants.monthDays)

            'ApplicationConstants.endDate = DateAdd("d", ApplicationConstants.monthDays, ApplicationConstants.startDate)

            'End - Changed By Aamir Waheed on July 17, 2013.
        End If

        'Seek for Monday as starting and Sunday ending day of the Month
        If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
            objCalendarUtilities.calculateMonday(ApplicationConstants.startDate)
        End If
        If (ApplicationConstants.endDate.ToString("dddd") <> "Sunday") Then
            objCalendarUtilities.calculateSunday()
        End If
        lblMonth.Text = MonthName(ApplicationConstants.startDate.Month) + " " + objCalendarUtilities.FormatDayNumberSuffix(ApplicationConstants.startDate.Day.ToString()) + "-" + MonthName(ApplicationConstants.endDate.Month) + " " + objCalendarUtilities.FormatDayNumberSuffix(ApplicationConstants.endDate.Day) + "," + Year(ApplicationConstants.endDate).ToString()


        If (Not IsPostBack) Then
            objCalendarBL.getEngineersForCalendarAppointment(dsEmployeeList)
            'Sending Start and End date
            objScheduleAppointmentsBO.StartDate = ApplicationConstants.startDate
            objScheduleAppointmentsBO.ReturnDate = ApplicationConstants.endDate
            objCalendarBL.getAppointmentsMonthlyCalendarDetail(objScheduleAppointmentsBO, dsAppointments)
        End If
        If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Gas Engineer exist
            'Creating columns of Datatable
            varWeeklyDT.Columns.Add(" ")
            'Creating Columns based on Engineers names
            For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
                strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")
            Next

            'Getting Employees Leaves Information
            objScheduleAppointmentsBO.StartDate = ApplicationConstants.startDate
            objScheduleAppointmentsBO.ReturnDate = ApplicationConstants.endDate
            objScheduleAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
            objSchedulingBL.getEngineerLeavesDetail(objScheduleAppointmentsBO, dsEmployeeLeaves)
            objCalendarBL.getEmployeeCoreWorkingHours(dsEmployeeCoreWorkingHours, strEmployeeIds.ToString().TrimEnd(","))
            'End Getting Employees Leaves Information

            'Adding Rows to Data table
            Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
            Dim weekDaysCount, dayCount As Integer
            For dayCount = 0 To ApplicationConstants.monthDays
                If (DateDiff("d", DateAdd("d", dayCount, ApplicationConstants.startDate.ToShortDateString()), ApplicationConstants.endDate.ToShortDateString()) >= 0) Then 'ApplicationConstants.endDate.ToString("MM/dd/yyyy")
                    If weekDaysCount > 6 Then
                        weekDaysCount = 0
                    End If
                    scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(weekDaysCount)
                    rowCount = 1
                    For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                        'Adding Leaves Information
                        drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                        If (drLeaves.Count > 0) Then 'System.DateTime.Parse(dr(rdCount).Item(1)).ToString("dddd")
                            For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></div><br/>")
                            Next
                        End If
                        'End - Changed By Aamir Waheed on July 05, 2013.
                        Dim resultCount = dsAppointments.Tables(0).Rows.Count - 1
                        For rdCount = 0 To resultCount

                            'Check the appointments of the engineers in Datarow
                            Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString())
                            Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString())
                            Dim currentDate As DateTime = DateAdd("d", dayCount, ApplicationConstants.startDate)
                            Dim operativeId As Integer = dsEmployeeList.Tables(0).Rows(dsColCount).Item(0)
                            Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + operativeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")

                            If (currentDate >= aptStartDate And currentDate <= aptEndDate _
                                And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() _
                                And dr.Count > 0) Then

                                If dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "No Entry" Then
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#EA2A2A;padding:4px;height:35px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'> " + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + " <b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span></div><br/>")
                                ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString() = "AM" Then
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#F7FE2E;padding:4px;height:35px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'> " + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + " <b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span></div><br/>")
                                ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString() = "PM" Then
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#58F872;padding:4px;height:35px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;'> " + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + " <b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Type") + "</b></span></div><br/>")
                                End If

                            End If
                        Next

                        'Start - Changed By Aamir Waheed on July 05, 2013.
                        'Changed the if else condition to if only to show appointments along with leaves.
                        'So commented out this End it and moved it to top.
                        'End If
                        'End - Changed By Aamir Waheed on July 05, 2013.

                        If (strAppointment.ToString() <> "") Then
                            'strAppointment.Append(DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"))
                            scheduledAppt(rowCount) = strAppointment.ToString()
                            strAppointment.Clear()
                        Else
                            scheduledAppt(rowCount) = "" 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy")
                        End If
                        rowCount = rowCount + 1

                    Next

                    varWeeklyDT.Rows.Add(scheduledAppt)
                    weekDaysCount = weekDaysCount + 1
                Else
                    dayCount = ApplicationConstants.monthDays
                End If
            Next
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.GasEngNotFound, True)
        End If
    End Sub

#End Region

#Region "Get Appointment Start and End Time"

    Function getAppointmentStartEndTime(ByVal dsAppointments As DataSet, ByVal dsEmployeeCoreWorkingHours As DataSet, ByVal currentDate As DateTime, ByVal rdCount As Integer, ByVal employeeId As Integer)

        Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1).ToString())
        Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(12).ToString())
        Dim aptStartTime As String = String.Empty
        Dim aptEndTime As String = String.Empty
        Dim empCoreStartTime As String = String.Empty
        Dim empCoreEndTime As String = String.Empty
        Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
        Dim ofcCoreEndTime As String = DateTime.Parse(GeneralHelper.getDayEndHour().ToString() + ":00").ToString("HH:mm")

        'Get employee core working hour
        Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + employeeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")
        If (dr.Count > 0) Then
            empCoreStartTime = DateTime.Parse(dr(0).Item(3).ToString()).ToString("HH:mm")
            empCoreEndTime = DateTime.Parse(dr(0).Item(4).ToString()).ToString("HH:mm")
        Else
            empCoreStartTime = ofcCoreStartTime
            empCoreEndTime = ofcCoreEndTime
        End If

        'Setting start and end time depending on cases
        'Case 1 : Single day appointment start time and end time exist in same day
        'Case 2 : Multiple day appointment where start time exist in current date
        'Case 3 : Multiple day appointment where end time exist in current date
        'Case 4 : Multiple day appointment where start and end time do not exist in current date

        If (aptStartDate = aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(8)).ToString("HH:mm")
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        ElseIf (currentDate = aptStartDate And currentDate <> aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(8)).ToString("HH:mm")
            aptEndTime = empCoreEndTime
        ElseIf (currentDate <> aptStartDate And currentDate = aptEndDate) Then
            aptStartTime = empCoreStartTime
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        Else
            aptStartTime = empCoreStartTime
            aptEndTime = empCoreEndTime
        End If

        Return aptStartTime + " - " + aptEndTime

    End Function

#End Region

#Region "display Searhed Appointments On Monthly Calendar"
    Protected Sub displaySearchedMonthlyAppointments()

        'Monthly Calendar code start here
        Dim appShift As String = "Morning"
        Dim boxColor As String = "#F7FE2E"
        Dim appStyle As String = "detailBoxSmall"

        lblMonth.Text = MonthName(ApplicationConstants.startDate.Month) + " " + objCalendarUtilities.FormatDayNumberSuffix(ApplicationConstants.startDate.Day.ToString()) + "-" + MonthName(ApplicationConstants.endDate.Month) + " " + objCalendarUtilities.FormatDayNumberSuffix(ApplicationConstants.endDate.Day) + "," + Year(ApplicationConstants.endDate).ToString()

        'Creating columns of Datatable
        varWeeklyDT.Columns.Add(" ")
        If (Not IsPostBack) Then
            objCalendarBL.getEngineersForCalendarAppointment(dsEmployeeList)
            objCalendarBL.getSearchedAppointmentsDetail(dsAppointments)
        End If

        'Creating Columns based on Engineers names
        For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
            objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
        Next

        'Adding Rows to Data table
        Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
        Dim dr() As DataRow = dsAppointments.Tables(0).Select("APPOINTMENTID=" + Request.QueryString("id"), "AppointmentDate ASC")
        Dim weekDaysCount, dayCount As Integer
        If (dr.Count > 0) Then
            ApplicationConstants.startDate = DateTime.Parse(dr(0).Item(1).ToString())
        End If
        'Seek for Monday as starting and Sunday ending day of the Month
        If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
            objCalendarUtilities.calculateMonday(ApplicationConstants.startDate)
        End If
        If (ApplicationConstants.endDate.ToString("dddd") <> "Sunday") Then
            objCalendarUtilities.calculateSunday()
        End If
        For dayCount = 0 To ApplicationConstants.monthDays
            'If (DateDiff("d", DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"), ApplicationConstants.endDate.ToString("MM/dd/yyyy")) >= 0) Then
            If (DateDiff("d", DateAdd("d", dayCount, ApplicationConstants.startDate.ToShortDateString()), ApplicationConstants.endDate.ToShortDateString()) >= 0) Then 'ApplicationConstants.endDate.ToString("MM/dd/yyyy")

                If weekDaysCount > 6 Then
                    weekDaysCount = 0
                End If
                scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(weekDaysCount)
                rowCount = 1
                For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1

                    For rdCount = 0 To dr.Count - 1

                        'Parsing Appointments Information
                        If (DateTime.Parse(dr(rdCount).Item(14)).ToShortDateString() = DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dr(rdCount).Item(4).ToString()) Then 'System.DateTime.Parse(dr(rdCount).Item(1)).ToString("MM/dd/yyyy")
                            If (Request.QueryString("id") = dr(rdCount).Item(13)) Then
                                appStyle = "detailSearchedBoxSmall"
                                'strAppointment.Append("<div class='detailBoxSmall' style='background-color:#EA2A2A;padding:4px'><table><tr><td><img src='../../images/arrow_right.png'></td><td><span style='margin-right:10px;'> Morning <b>&nbsp;GAS</b></span></td></tr></table></div><br/>")
                            End If

                            If dr(rdCount).Item(0).ToString() = "PM" Then
                                appShift = "Afternoon"
                                boxColor = "#58F872"
                            End If
                            If dr(rdCount).Item(6).ToString() = 3 Then
                                boxColor = "#EA2A2A"
                            End If
                            strAppointment.Append("<div class='" + appStyle + "' style='background-color:" + boxColor + ";padding:4px'><table><tr><td><img src='../../images/arrow_right.png'></td><td><span style='margin-right:10px;'>" + appShift + "<b>&nbsp;" + dr(rdCount).Item("Type") + "</b></span></td></tr></table></div><br/>")
                        End If
                    Next
                    If (strAppointment.ToString() <> "") Then
                        'strAppointment.Append(DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"))
                        scheduledAppt(rowCount) = strAppointment.ToString()
                        strAppointment.Clear()
                    Else
                        scheduledAppt(rowCount) = "" 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy")
                    End If
                    rowCount = rowCount + 1

                Next
                varWeeklyDT.Rows.Add(scheduledAppt)
                weekDaysCount = weekDaysCount + 1
            Else
                dayCount = ApplicationConstants.monthDays
            End If
        Next

    End Sub

#End Region
#End Region

End Class