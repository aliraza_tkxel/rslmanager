﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class Dashboard
    Inherits PageBase

    Dim objUserBL As UsersBL = New UsersBL()
    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objDashboardBL As DashboardBL = New DashboardBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Address", 1, 10)
    Dim objMEPageSortBo As PageSortBO = New PageSortBO("ASC", "Address", 1, 10)
    Dim defaultPageSize As Integer = 1

#Region " Constants "

    '
    ' Column Names
    '
    Private Const COLUMN_APPOINTMENT_DATE As String = "APPOINTMENTDATE"
    Private Const COLUMN_EXPIRY_DATE_SORT As String = "ExpiryDateSort"
    Private Const COLUMN_Fuel_Type As String = "FUEL"


    '
    ' Exceptions
    '
    Private Const EXCEPTION_INSPECTION_STATUS_VALUE As String = "Failed to determine inspection status value."

#End Region

#Region "Event Handlers"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not (IsPostBack) Then
                Me.loadFuelTypeDropDown()
                Me.loadMEDropdown()
                Me.loadPatchesDropDown()
                Me.loadSchemesDropDown()
                Me.loadInpectionTypesDropDown()

                Me.setPageSortBo(objPageSortBo)
                Me.setMEPageSortBo(objMEPageSortBo)
                Me.updateValues()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ddl Patch Selected Index Changed"
    Protected Sub ddlPatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPatch.SelectedIndexChanged
        Try
            Me.loadSchemesDropDown()
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ddl Scheme Selected Index Changed"
    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Try
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Refresh Click"

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefresh.Click
        Try
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ddl Inspection Type Selected IndexChanged "
    Protected Sub ddlInspectionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlInspectionType.SelectedIndexChanged
        Try
            Me.updateInspectionStatus()
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn No Entries Click"
    Sub lnkBtnNoEntries_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.ddlInspectionType.SelectedValue = 1
            'Me.updateInspectionStatus()
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn No Entry Detail Click"
    Sub lnkBtnNoEntryDetail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim noEntryQueryString As String = String.Format("{0}={1}", PathConstants.Status, PathConstants.NoEntry)
            Dim patch As String = ddlPatch.SelectedValue
            Dim scheme As String = ddlScheme.SelectedValue
            Response.Redirect(String.Format(PathConstants.FuelServicingPath + "?{0}&{1}={2}&{3}={4}&{5}={6}", noEntryQueryString, PathConstants.Tab, PathConstants.AppointmentArranged, PathConstants.PatchId, patch, PathConstants.SchemeId, scheme), False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region


#Region "lnk button address clicked in inspection status"
    Sub lnkBtnAddressClicked(ByVal sender As Object, ByVal e As EventArgs)
        Try


            If Not (ddlInspectionType.SelectedValue = ApplicationConstants.InspectionStatusDropDownAppointmentsToBeArranges Or ddlInspectionType.SelectedValue = ApplicationConstants.InspectionStatusDropDownAppointmentsAborted) Then
                Return
            End If


            Dim abortedQueryString As String = String.Format("{0}={1}", PathConstants.Status, PathConstants.Aborted)
            Dim patch As String = ddlPatch.SelectedValue
            Dim scheme As String = ddlScheme.SelectedValue

            If (ddlInspectionType.SelectedValue = ApplicationConstants.InspectionStatusDropDownAppointmentsAborted) Then
                Response.Redirect(String.Format(PathConstants.FuelServicingPath + "?{0}&{1}={2}&{3}={4}&{5}={6}", abortedQueryString, PathConstants.Tab, PathConstants.AppointmentArranged, PathConstants.PatchId, Nothing, PathConstants.SchemeId, Nothing), False)
            Else
                Response.Redirect(String.Format(PathConstants.FuelServicingPath))
            End If

            '+ "?{0}&{1}={2}&{3}={4}&{5}={6}", noEntryQueryString, PathConstants.Tab, PathConstants.PatchId, patch, PathConstants.SchemeId, scheme), False)
            'Response.Redirect(String.Format(PathConstants.FuelServicingPath))
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn Expired Click"

    Sub lnkBtnExpired_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.ddlInspectionType.SelectedValue = 3
            'Me.updateInspectionStatus()
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Legal Proceedings Click"
    Sub lnkBtnLegalProceedings_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.ddlInspectionType.SelectedValue = 2
            'Me.updateInspectionStatus()
            Me.updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Appointments To Be Arranged Click"
    Sub lnkBtnAppointmentsToBeArranged_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnCappedAppliances.Click
        Try
            Dim fuel As String = ddlFuelType.SelectedItem.Text
            If (fuel.Equals("Alternative Servicing")) Then
                Response.Redirect(PathConstants.AlternativeServicingPath, False)
            ElseIf (fuel.Equals("Oil")) Then
                Response.Redirect(PathConstants.OilServicingPath, False)
            Else
                Response.Redirect(PathConstants.FuelServicingPath, False)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn IS Next Click"

    Protected Sub lnkBtnISNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISNext.Click
        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber + 1
            Me.setPageSortBo(objPageSortBo)
            Me.updateInspectionStatus()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn IS Previous Click"
    Protected Sub lnkBtnISPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnISPrevious.Click
        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = objPageSortBo.PageNumber - 1
            Me.setPageSortBo(objPageSortBo)
            Me.updateInspectionStatus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "grd Inspection Status Sorting"

    Protected Sub grdInspectionStatus_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdInspectionStatus.Sorting
        Try
            objPageSortBo = getPageSortBo()

            If (Not e.SortExpression.Equals(objPageSortBo.SortExpression)) Then
                objPageSortBo.SortDirection = e.SortDirection
            End If

            objPageSortBo.SortExpression = e.SortExpression

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.updateInspectionStatus()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddlFuelType Selected index changed "
    Protected Sub ddlFuelType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFuelType.SelectedIndexChanged
        Try
            pnlPatch.Visible = True
            pnlCounts.Visible = True
            pnlRightside.Visible = True
            pnlSchemeBlock.Visible = False
            pnlSchemeBlockCounts.Visible = False
            pnlRightSchemeBlock.Visible = False
            objPageSortBo = New PageSortBO("ASC", "Address", 1, 10)
            updateValues()


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn Defects To Be Arranged Click"
    Protected Sub lnkBtnDefectsToBeArranged_Click(sender As Object, e As EventArgs) Handles lnkBtnDefectsToBeArranged.Click
        Try

            Response.Redirect(PathConstants.ScheduleDefect, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Defects Arranged Click"
    Protected Sub lnkBtnDefectsArranged_Click(sender As Object, e As EventArgs) Handles lnkBtnDefectsArranged.Click
        Try
            Response.Redirect(String.Format("{0}?{1}={2}", PathConstants.ScheduleDefect, PathConstants.Tab, PathConstants.AppointmentArranged), False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Capped Appliances Click"
    Protected Sub lnkBtnCappedAppliances_Click(sender As Object, e As EventArgs) Handles lnkBtnCappedAppliances.Click
        Try

            Response.Redirect(PathConstants.DisconnectedApplianceReportPath, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "lnk Btn Void Properties Click"
    Protected Sub lnkBtnVoidProperties_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Response.Redirect(PathConstants.AvailableProperties, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Void To Be Arranged Click"
    Protected Sub lnkBtnVoidAppointmentsToBeArranged_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Response.Redirect(PathConstants.VoidAppointmentsToBeArranged, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Void Appointments Arranged Click"
    Protected Sub lnkBtnVoidAppointmentsArranged_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Response.Redirect(String.Format("{0}?{1}={2}", PathConstants.VoidAppointmentsArranged, PathConstants.Tab, PathConstants.AppointmentArranged), False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lnk Btn Defects Requiring Approval Click"
    Protected Sub lnkBtnDefectsRequiringApproval_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Response.Redirect(PathConstants.DefectsRequiringApprovalPath, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Private Methods"

#Region "load Fuel Type Drop Down"
    Private Sub loadFuelTypeDropDown()

        Dim resultDataSet As DataSet = New DataSet()
        objDashboardBL.getServicingTypes(resultDataSet)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlFuelType.DataSource = resultDataSet
            'ddlFuelType.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
            'ddlFuelType.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
            'ddlFuelType.DataBind()
            'ddlFuelType.Items.FindByText("Please Select").Selected = True

            ddlFuelType.DataTextField = "Description"
            ddlFuelType.DataValueField = "ServicingTypeID"
            ddlFuelType.DataBind()
            ddlFuelType.Items.Insert(0, "Select Type")
        End If
        ddlFuelType.SelectedIndex = 1
    End Sub
#End Region

#Region "load Patches Drop Down"
    Private Sub loadPatchesDropDown()

        Dim codeId As String = "UserTypeID"
        Dim codeName As String = "Description"
        Dim list As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        objUserBL.getPatchLocations(0, list)

        ddlPatch.DataSource = list
        ddlPatch.DataValueField = codeId
        ddlPatch.DataTextField = codeName
        ddlPatch.DataBind()


        ddlPatch.Items.Add(New ListItem("All Patches", ApplicationConstants.DropDownDefaultValue))
        ddlPatch.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub
#End Region

#Region "load Schemes Drop Down"
    Private Sub loadSchemesDropDown()

        Dim patchSelected As Int32 = CType(ddlPatch.SelectedItem.Value, Int32)

        If (Not patchSelected = ApplicationConstants.DropDownDefaultValue) Then
            Dim codeId As String = "UserTypeID"
            Dim codeName As String = "Description"
            Dim list As List(Of UserTypeBO) = New List(Of UserTypeBO)()
            objUserBL.getSchemeNames(patchSelected, list)

            ddlScheme.DataSource = list
            ddlScheme.DataValueField = codeId
            ddlScheme.DataTextField = codeName
            ddlScheme.DataBind()

        End If

        ddlScheme.Items.Add(New ListItem("All Schemes", ApplicationConstants.DropDownDefaultValue))
        ddlScheme.SelectedValue = ApplicationConstants.DropDownDefaultValue


        'updpnlOptions.Update()
        updPanelMain.Update()
        'Me.updateValues()
    End Sub
#End Region

#Region " Load Scheme and Block Dropdowns"
    ''' <summary>
    ''' Load Scheme and Block Dropdowns
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadMEDropdown()
        Dim resultDataSet As DataSet = New DataSet()
        ddlNewSchemes.Items.Clear()
        objDashboardBL.populateMESchemeDropdown(resultDataSet)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlNewSchemes.DataSource = resultDataSet
            ddlNewSchemes.DataValueField = "SCHEMEID"
            ddlNewSchemes.DataTextField = "SCHEMENAME"
            ddlNewSchemes.DataBind()
            ddlNewSchemes.Items.Insert(0, New ListItem("Select Scheme", "0"))
        End If
        ddlBlock.Items.Insert(0, New ListItem("Select Block", "0"))
    End Sub


    Protected Sub ddlNewSchemes_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlNewSchemes.SelectedIndexChanged
        Dim resultDataSet As DataSet = New DataSet()
        Dim schemeId As Integer = Convert.ToInt32(ddlNewSchemes.SelectedValue)
        ddlBlock.Items.Clear()
        objDashboardBL.populateBlockDropdown(resultDataSet, schemeId)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlBlock.DataSource = resultDataSet
            ddlBlock.DataValueField = "BLOCKID"
            ddlBlock.DataTextField = "BLOCKNAME"
            ddlBlock.DataBind()
            ddlBlock.Items.Insert(0, New ListItem("Select Block", "0"))
        Else
            ddlBlock.Items.Insert(0, New ListItem("Select Block", "0"))
        End If

        updateValues()
    End Sub
#End Region

#Region "Update Scheme Block Dashboard on selected Block"
    ''' <summary>
    ''' Update Scheme Block Dashboard
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlBlock_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBlock.SelectedIndexChanged
        Try
            updateValues()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load Inpection Types Drop Down"
    Private Sub loadInpectionTypesDropDown()

        ddlInspectionType.Items.Add(New ListItem("No Entries", ApplicationConstants.InspectionStatusDropDownNoEntries))
        ddlInspectionType.Items.Add(New ListItem("Legal Proceedings", ApplicationConstants.InspectionStatusDropDownLegalProceedings))
        ddlInspectionType.Items.Add(New ListItem("Certificates Expired", ApplicationConstants.InspectionStatusDropDownExpired))
        ddlInspectionType.Items.Add(New ListItem("Appointments To Be Arranged", ApplicationConstants.InspectionStatusDropDownAppointmentsToBeArranges))
        ddlInspectionType.Items.Add(New ListItem("Aborted", ApplicationConstants.InspectionStatusDropDownAppointmentsAborted))

        ddlServicingStatus.Items.Add(New ListItem("No Entries", ApplicationConstants.ServicingStatusDropDownNoEntries))
        ddlServicingStatus.Items.Add(New ListItem("Appointments To Be Arranged", ApplicationConstants.ServicingStatusDropDownAppointmentToBeArranged))


        ddlInspectionType.SelectedValue = ApplicationConstants.InspectionStatusDropDownNoEntries
        ddlServicingStatus.SelectedValue = ApplicationConstants.ServicingStatusDropDownNoEntries

    End Sub
#End Region

#Region "update Values"
    ' This method should be used to update the values of the screen. 
    Private Sub updateValues()

        Dim countHashtable As Hashtable = New Hashtable()

        Dim fuelTypeSelected As String = CType(ddlFuelType.SelectedItem.Text, String)

        Dim patchSelected As Integer = CType(ddlPatch.SelectedItem.Value, Integer)

        Dim schemeSelected As Integer = CType(ddlScheme.SelectedItem.Value, Integer)

        Dim selectedMEScheme As Integer = -1
        Dim selectedMEBlock As Integer = -1
        Dim MECountHashtable As Hashtable = New Hashtable()

        If (Not IsNothing(ddlPatch.SelectedItem)) Then
            patchSelected = (CType(ddlPatch.SelectedItem.Value, Integer))
        End If

        If (Not IsNothing(ddlScheme.SelectedItem)) Then
            schemeSelected = (CType(ddlScheme.SelectedItem.Value, Integer))
        End If


        If (Not IsNothing(ddlNewSchemes.SelectedItem)) Then
            selectedMEScheme = (CType(ddlNewSchemes.SelectedItem.Value, Integer))
        End If

        If (Not IsNothing(ddlBlock.SelectedItem)) Then
            selectedMEBlock = (CType(ddlBlock.SelectedItem.Value, Integer))
        End If

        objDashboardBL.getCounts(fuelTypeSelected, patchSelected, schemeSelected, countHashtable)

        objDashboardBL.getMECounts(selectedMEScheme, selectedMEBlock, MECountHashtable)

        ' Update the No Entries Number
        lnkBtnNoEntryDetail.Text = countHashtable(ApplicationConstants.HashTableKeyNoEntry)

        ' Update the Certificates Expired Number
        lblExpired.Text = countHashtable(ApplicationConstants.HashTableKeyExpired)

        ' Update the Legal Proceedings Number
        lblLegalProceedings.Text = countHashtable(ApplicationConstants.HashTableKeyLegalProceedings)

        ' Update the Due to Expire in 56 days Number
        lblExpiresIn56Days.Text = countHashtable(ApplicationConstants.HashTableKeyExpiresIn56Days)

        ' Update the Appointments Arranged Number
        lblAppointmentsArranged.Text = countHashtable(ApplicationConstants.HashTableKeyAppointmentsArranged)

        ' Update the Appointments to be Arranged Number
        lblAppointmentsToBeArranged.Text = countHashtable(ApplicationConstants.HashTableKeyAppointmentsToBeArranged)

        ' Update the Defects Requiring Approval Number
        lblDefectsRequiringApprovalCount.Text = countHashtable(ApplicationConstants.HashTableKeyDefectsRequiringApproval)

        'Set ME Dashboard Count
        'Set NoEntry Counts
        lblMENoEntry.Text = MECountHashtable(ApplicationConstants.MEHashTableKeyNoEntry)

        'Set NoEntry Counts
        lblMEOverDue.Text = MECountHashtable(ApplicationConstants.MEHashTableKeyOverdue)

        'Set NoEntry Counts
        lblMEAppointmentArranged.Text = MECountHashtable(ApplicationConstants.MEHashTableKeyAppointmentArranged)

        'Set NoEntry Counts
        lblMEAPTBA.Text = MECountHashtable(ApplicationConstants.MEHashTableKeyAppointmentToBeArranged)

        'set defects appointment to be arranged count
        lblDefectsToBeArranged.Text = countHashtable(ApplicationConstants.HashTableKeyDefectAppointmentsToBeArranged)

        'set defects appointment arranged count
        lblDefectsArranged.Text = countHashtable(ApplicationConstants.HashTableKeyDefectAppointmentsArranged)

        'set defects capped appliances count
        lblCappedAppliances.Text = countHashtable(ApplicationConstants.HashTableKeyDefectCappedAppliances)

        'set void appointments to be arranged
        lblVoidAppointmentsToBeArranged.Text = countHashtable(ApplicationConstants.HashTableKeyVoidAppointmentsToBeArranged)

        'set void appointments arranged
        lblVoidAppointmentsArranged.Text = countHashtable(ApplicationConstants.HashTableKeyVoidAppointmentsArranged)

        'set Void Properties
        lblVoidProperties.Text = countHashtable(ApplicationConstants.HashTableKeyVoidProperties)

        ' Update the Management Info 
        Dim miData As DataSet = New DataSet()
        objDashboardBL.getCountMIData(patchSelected, schemeSelected, miData, ddlFuelType.SelectedItem.Text)
        If ((Not miData.Tables.Count = 0) And (Not miData.Tables(0).Rows.Count = 0)) Then
            Dim intMIProperties As Integer = CType(miData.Tables(0).Rows(0).Item(0), Integer)
            Dim intMIBoilerRooms As Integer = CType(miData.Tables(0).Rows(0).Item(1), Integer)
            Dim intMINoCertificates As Integer = CType(miData.Tables(0).Rows(0).Item(2), Integer)
            Dim intMIDueIn8To12Weeks As Integer = CType(miData.Tables(0).Rows(0).Item(3), Integer)
            Dim intMIReportedFaults As Double = 0.0
            If (Not intMIDueIn8To12Weeks = 0) Then
                intMIReportedFaults = Math.Round(((intMIDueIn8To12Weeks / (intMIProperties + intMIBoilerRooms)) * 100), 2)
            End If

            lblMIProperties.Text = intMIProperties.ToString
            lblMIBoilerRooms.Text = intMIBoilerRooms.ToString
            lblMINoCertificates.Text = intMINoCertificates.ToString
            lblMIDueIn8To12Weeks.Text = intMIDueIn8To12Weeks.ToString
            lblMIReportedFaults.Text = intMIReportedFaults.ToString + "%"
        Else
            lblMIProperties.Text = "0"
            lblMIBoilerRooms.Text = "0"
            lblMINoCertificates.Text = "0"
            lblMIDueIn8To12Weeks.Text = "0"
            lblMIReportedFaults.Text = "0" + "%"

        End If

        ' Update the Certificate Expiry Graph
        Me.updateCertificateExpirayChart()
        ' Update the Inpection Status Grid 

        updateInspectionStatus()
        updateServicingStatus()
        
        If (fuelTypeSelected.Equals("Oil") OrElse fuelTypeSelected.Equals("Alternative Servicing")) Then
            lnkBtnVoidProp.Visible = False
            lnkBtnVoidAppArrang.Visible = False
            lnkBtnVoidAppToBeArrang.Visible = False
            lnkBtnDefectsToBeArranged.Visible = False
            lnkBtnDefectsArranged.Visible = False
        Else
            lnkBtnVoidProp.Visible = True
            lnkBtnVoidAppArrang.Visible = True
            lnkBtnVoidAppToBeArrang.Visible = True
            lnkBtnDefectsToBeArranged.Visible = True
            lnkBtnDefectsArranged.Visible = True
        End If

    End Sub
#End Region

#Region "update Inspection Status"

    Private Sub updateInspectionStatus()
        Dim patchSelected As Integer = CType(ddlPatch.SelectedItem.Value, Integer)
        Dim schemeSelected As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        Dim inspectionTypeSelected As Integer = CType(ddlInspectionType.SelectedItem.Value, Integer)
        Dim totalCount As Integer = 0

        Dim resultSet As DataSet = New DataSet()
        totalCount = objDashboardBL.getInspectionStatus(inspectionTypeSelected, patchSelected, schemeSelected, objPageSortBo, resultSet, ddlFuelType.SelectedItem.Text)
        If (resultSet.Tables(0).Rows.Count = 0) Then
            Me.grdInspectionStatus.Visible = False
            Me.lblISNoResults.Visible = True
        Else
            Me.grdInspectionStatus.Visible = True
            Me.lblISNoResults.Visible = False
            grdInspectionStatus.VirtualItemCount = totalCount
            Me.UpdateInspectionStatusColumns(resultSet)
            Me.grdInspectionStatus.DataSource = resultSet
            Me.grdInspectionStatus.DataBind()
            Me.UpdateInspectionStatusGridColumns(inspectionTypeSelected)

        End If

        Me.setPagingLabels(totalCount)
        Me.setNextPrevious(totalCount)

        'updPnlInspectionStatus.Update()
        updPanelMain.Update()

    End Sub

    Private Sub UpdateInspectionStatusColumns(ByVal dataSet As DataSet)

        If dataSet.Tables(0).Columns.Contains(COLUMN_APPOINTMENT_DATE) = False Then
            dataSet.Tables(0).Columns.Add(New DataColumn(COLUMN_APPOINTMENT_DATE, GetType(DateTime)))
        End If

        If dataSet.Tables(0).Columns.Contains(COLUMN_EXPIRY_DATE_SORT) = False Then
            dataSet.Tables(0).Columns.Add(New DataColumn(COLUMN_EXPIRY_DATE_SORT, GetType(DateTime)))
        End If

        If dataSet.Tables(0).Columns.Contains(COLUMN_Fuel_Type) = False Then
            dataSet.Tables(0).Columns.Add(New DataColumn(COLUMN_Fuel_Type, GetType(String)))
        End If

        dataSet.AcceptChanges()
    End Sub

    Private Sub UpdateInspectionStatusGridColumns(ByVal inspectionTypeSelected As Integer)

        Me.grdInspectionStatus.Columns(2).Visible = True ' Appointment Date
        Me.grdInspectionStatus.Columns(3).Visible = True ' Cert Expiry
        Me.grdInspectionStatus.Columns(4).Visible = True ' Fuel Type

        Select Case inspectionTypeSelected

            Case ApplicationConstants.InspectionStatusDropDownExpired
                ' Do nothing

            Case ApplicationConstants.InspectionStatusDropDownLegalProceedings
                Me.grdInspectionStatus.Columns(2).Visible = False ' AppointmentDate
                Me.grdInspectionStatus.Columns(3).Visible = False ' Cert Expiry

            Case ApplicationConstants.InspectionStatusDropDownNoEntries
                Me.grdInspectionStatus.Columns(2).Visible = False ' Appointment Date

            Case ApplicationConstants.InspectionStatusDropDownAppointmentsToBeArranges
                Me.grdInspectionStatus.Columns(2).Visible = False ' AppointmentDate
                Me.grdInspectionStatus.Columns(3).Visible = False ' Cert Expiry

            Case ApplicationConstants.InspectionStatusDropDownAppointmentsAborted
                'Me.grdInspectionStatus.Columns(4).Visible = False ' Fuel Type
                ' Me.grdInspectionStatus.Columns(2).Visible = False ' AppointmentDate
                ' Me.grdInspectionStatus.Columns(3).Visible = False ' Cert Expiry

            Case Else

                Throw New Exception(EXCEPTION_INSPECTION_STATUS_VALUE)

        End Select

    End Sub

#End Region

#Region "update Certificate Expiray Chart "
    Private Sub updateCertificateExpirayChart()
        Dim patchSelected As Integer = CType(ddlPatch.SelectedItem.Value, Integer)
        Dim schemeSelected As Integer = CType(ddlScheme.SelectedItem.Value, Integer)
        Dim isAlternative As Boolean = False
        Dim FuelType As String = String.Empty

        Dim resultSet As DataSet = New DataSet()
        FuelType = ddlFuelType.SelectedItem.Text

        objDashboardBL.getCertificateExpiry(patchSelected, schemeSelected, resultSet, FuelType)

        If (Not resultSet.Tables(0).Rows.Count = 0) Then

            Me.chartCertificateExpiry.DataSource = resultSet
            Me.chartCertificateExpiry.Series("seriesCertificateExpiry").XValueMember = "MonName"
            Me.chartCertificateExpiry.Series("seriesCertificateExpiry").YValueMembers = "MonValue"

            Me.chartCertificateExpiry.DataBind()

            For Each point In Me.chartCertificateExpiry.Series("seriesCertificateExpiry").Points
                point.Url = "../../Views/Reports/CertificateExpiry/CertificateExpiry.aspx"
            Next


            Me.chartCertificateExpiry.Visible = True
        Else
            Me.chartCertificateExpiry.Visible = False

        End If

    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Set ME Page Sort Bo"
    Protected Sub setMEPageSortBo(ByRef objMEPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.MEPageSortBo) = objMEPageSortBo
    End Sub
#End Region

#Region "Get ME Page Sort Bo"
    Protected Function getMEPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.MEPageSortBo), PageSortBO)
    End Function

#End Region

#Region "set Paging Labels"
    Private Sub setPagingLabels(ByVal totalCount As Integer)

        If totalCount = 0 Then
            Me.lblISResults.Text = "0 of 0"
            Me.lblISPages.Text = "0 of 0"

        ElseIf (totalCount <= objPageSortBo.PageSize) Then
            Me.lblISResults.Text = Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
            Me.lblISPages.Text = "1 of 1"
        ElseIf (totalCount > objPageSortBo.PageSize) Then

            If (objPageSortBo.PageNumber * objPageSortBo.PageSize) <= totalCount Then

                Me.lblISResults.Text = Convert.ToString(objPageSortBo.PageSize * objPageSortBo.PageNumber) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(Math.Ceiling(totalCount / objPageSortBo.PageSize))

            ElseIf (objPageSortBo.PageNumber * objPageSortBo.PageSize) > totalCount Then

                Me.lblISResults.Text = Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
                Me.lblISPages.Text = Convert.ToString(objPageSortBo.PageNumber) + " of " + Convert.ToString(objPageSortBo.PageNumber)

            End If
        End If

    End Sub
#End Region

#Region "Set Next Previous"
    Private Sub setNextPrevious(ByVal totalCount As Integer)
        objPageSortBo = getPageSortBo()
        'if total records are less than page size
        If (totalCount <= objPageSortBo.PageSize) Then

            Me.lnkBtnISNext.CssClass = "disable-link"
            Me.lnkBtnISPrevious.CssClass = "disable-link"

        ElseIf (objPageSortBo.PageNumber = 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number = 1
            Me.lnkBtnISPrevious.CssClass = "disable-link"
            Me.lnkBtnISNext.CssClass = "enable-link"
        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its last page 
            Me.lnkBtnISPrevious.CssClass = "enable-link"
            Me.lnkBtnISNext.CssClass = "disable-link"

        ElseIf (objPageSortBo.PageNumber > 1 And objPageSortBo.PageNumber < Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then
            'if page number > 1 and its not last page 
            Me.lnkBtnISNext.CssClass = "enable-link"
            Me.lnkBtnISPrevious.CssClass = "enable-link"

        ElseIf (objPageSortBo.PageNumber = Math.Ceiling(totalCount / objPageSortBo.PageSize)) Then

            'if its last page enable pervious link and disable next link
            Me.lnkBtnISPrevious.CssClass = "enable-link"
            Me.lnkBtnISNext.CssClass = "disable-link"
        End If
    End Sub
#End Region

#Region "set ME Paging Labels"
    Private Sub setMEPagingLabels(ByVal totalCount As Integer)

        If totalCount = 0 Then
            Me.lblServicingResults.Text = "0 of 0"
            Me.lblServicingPages.Text = "0 of 0"

        ElseIf (totalCount <= objMEPageSortBo.PageSize) Then
            Me.lblServicingResults.Text = Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
            Me.lblServicingPages.Text = "1 of 1"
        ElseIf (totalCount > objMEPageSortBo.PageSize) Then

            If (objMEPageSortBo.PageNumber * objMEPageSortBo.PageSize) <= totalCount Then

                Me.lblServicingResults.Text = Convert.ToString(objMEPageSortBo.PageSize * objMEPageSortBo.PageNumber) + " of " + Convert.ToString(totalCount)
                Me.lblServicingPages.Text = Convert.ToString(objMEPageSortBo.PageNumber) + " of " + Convert.ToString(Math.Ceiling(totalCount / objMEPageSortBo.PageSize))

            ElseIf (objMEPageSortBo.PageNumber * objMEPageSortBo.PageSize) > totalCount Then

                Me.lblServicingResults.Text = Convert.ToString(totalCount) + " of " + Convert.ToString(totalCount)
                Me.lblServicingPages.Text = Convert.ToString(objMEPageSortBo.PageNumber) + " of " + Convert.ToString(objMEPageSortBo.PageNumber)

            End If
        End If

    End Sub
#End Region

#Region "Set ME Next Previous"
    Private Sub setMENextPrevious(ByVal totalCount As Integer)
        objMEPageSortBo = getMEPageSortBo()
        'if total records are less than page size
        If (totalCount <= objMEPageSortBo.PageSize) Then

            Me.lnkBtnServicingNext.CssClass = "disable-link"
            Me.lnkBtnServicingPrevious.CssClass = "disable-link"

        ElseIf (objMEPageSortBo.PageNumber = 1 And objMEPageSortBo.PageNumber < Math.Ceiling(totalCount / objMEPageSortBo.PageSize)) Then
            'if page number = 1
            Me.lnkBtnServicingPrevious.CssClass = "disable-link"
            Me.lnkBtnServicingNext.CssClass = "enable-link"
        ElseIf (objMEPageSortBo.PageNumber > 1 And objMEPageSortBo.PageNumber = Math.Ceiling(totalCount / objMEPageSortBo.PageSize)) Then
            'if page number > 1 and its last page 
            Me.lnkBtnServicingPrevious.CssClass = "enable-link"
            Me.lnkBtnServicingNext.CssClass = "disable-link"

        ElseIf (objMEPageSortBo.PageNumber > 1 And objMEPageSortBo.PageNumber < Math.Ceiling(totalCount / objMEPageSortBo.PageSize)) Then
            'if page number > 1 and its not last page 
            Me.lnkBtnServicingNext.CssClass = "enable-link"
            Me.lnkBtnServicingPrevious.CssClass = "enable-link"

        ElseIf (objMEPageSortBo.PageNumber = Math.Ceiling(totalCount / objMEPageSortBo.PageSize)) Then

            'if its last page enable pervious link and disable next link
            Me.lnkBtnServicingPrevious.CssClass = "enable-link"
            Me.lnkBtnServicingNext.CssClass = "disable-link"
        End If
    End Sub
#End Region

#End Region

#Region "Public Methods"

    Public Function FormatDate(ByVal input As Object) As Object

        If Not IsDBNull(input) Then
            Return String.Format("{0:d}", input)
        Else
            Return String.Empty
        End If

    End Function

#End Region

#Region "ME Servicing Events"


    Protected Sub lnkBtnSchemeATBA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnSchemeATBA.Click
        Try
            Dim schemeId As Integer = Convert.ToInt32(ddlNewSchemes.SelectedValue)
            If (schemeId = 0) Then
                schemeId = -1
            End If
            Dim blockId As Integer = Convert.ToInt32(ddlBlock.SelectedValue)
            If (blockId = 0) Then
                blockId = -1
            End If
            Response.Redirect(PathConstants.MEServicingAPTBA + "&sid=" + schemeId.ToString() + "&bid=" + blockId.ToString())
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub lnkBtnSchemeAppointmentsArranged_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnSchemeAppointmentsArranged.Click
        Try
            Dim schemeId As Integer = Convert.ToInt32(ddlNewSchemes.SelectedValue)
            If (schemeId = 0) Then
                schemeId = -1
            End If
            Dim blockId As Integer = Convert.ToInt32(ddlBlock.SelectedValue)
            If (blockId = 0) Then
                blockId = -1
            End If
            Response.Redirect(PathConstants.MEServicingAppointmentArranged + "&sid=" + schemeId.ToString() + "&bid=" + blockId.ToString())
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub



#Region " Update Servicing Grid "
    Private Sub updateServicingStatus()

        Dim selectedScheme As Integer = If(ddlNewSchemes.SelectedValue <> Nothing, CType(ddlNewSchemes.SelectedItem.Value, Integer), -1)
        Dim selectedBlock As Integer = If(ddlBlock.SelectedValue <> Nothing, CType(ddlBlock.SelectedItem.Value, Integer), -1)
        Dim servicingTypeSelected As Integer = If(ddlBlock.SelectedValue <> Nothing, CType(ddlServicingStatus.SelectedItem.Value, Integer), -1)
        Dim totalCount As Integer = 0

        Dim resultSet As DataSet = New DataSet()
        totalCount = objDashboardBL.getServicingStatus(servicingTypeSelected, selectedScheme, selectedBlock, objMEPageSortBo, resultSet)
        If (resultSet.Tables(0).Rows.Count = 0) Then
            Me.grdServicingStatus.Visible = False
            Me.lblServicingResults.Visible = True
        Else
            Me.grdServicingStatus.Visible = True
            Me.lblServicingResults.Visible = False
            grdServicingStatus.VirtualItemCount = totalCount
            'Me.UpdateInspectionStatusColumns(resultSet)
            Me.grdServicingStatus.DataSource = resultSet
            Me.grdServicingStatus.DataBind()
            'Me.UpdateInspectionStatusGridColumns(inspectionTypeSelected)

        End If

        Me.setMEPagingLabels(totalCount)
        Me.setMENextPrevious(totalCount)

        updPanelMain.Update()

    End Sub

    Protected Sub lnkBtnServicingPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnServicingPrevious.Click
        Try
            objMEPageSortBo = Me.getMEPageSortBo()
            objMEPageSortBo.PageNumber = objMEPageSortBo.PageNumber - 1
            Me.setMEPageSortBo(objMEPageSortBo)
            Me.updateServicingStatus()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub lnkBtnServicingNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnServicingNext.Click
        Try
            objMEPageSortBo = Me.getMEPageSortBo()
            objMEPageSortBo.PageNumber = objMEPageSortBo.PageNumber + 1
            Me.setMEPageSortBo(objMEPageSortBo)
            Me.updateServicingStatus()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub ddlServicingStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlServicingStatus.SelectedIndexChanged
        Try
            Me.updateServicingStatus()
            Me.updateValues()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

End Class