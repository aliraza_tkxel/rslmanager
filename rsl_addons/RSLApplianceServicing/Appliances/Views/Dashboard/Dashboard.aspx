﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/ASMasterPage.Master"
    CodeBehind="Dashboard.aspx.vb" Inherits="Appliances.Dashboard" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <asp:Panel ID="pnlDashboardOuter" runat="server" UpdateMode="Conditional">
        <div class="wrapper">
            <div class="main_div">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <asp:UpdatePanel ID="updPanelMain" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="padding-bottom: 10px;">
                            <div class="portlet">
                                <div class="header">
                                    <span class="header-label">Search:</span>
                                </div>
                                <div class="portlet-body" style="">
                                    <div class="form-control">
                                        <div class="select_div">
                                            <div class="label">
                                                Servicing type:
                                            </div>
                                            <div class="field">
                                                <asp:DropDownList ID="ddlFuelType" class="styleselect" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlPatch" runat="server">
                                            <div class="form-control">
                                                <div class="select_div">
                                                    <div class="label">
                                                        Properties within:
                                                    </div>
                                                    <div class="field">
                                                        <asp:DropDownList ID="ddlPatch" class="styleselect" runat="server" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlPatch_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding: 10px; min-height: 13px;">
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlScheme" class="styleselect" style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                        height: 25px !important;
                                                        font-size: 12px !important;
                                                        width: 205px !important;" 
                                                        runat="server" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:Button ID="btnRefresh" class="btn btn-xs btn-blue right" runat="server" Text="Refresh" />
                                                <asp:Label ID="lblWaiting" runat="server" Visible="false">
                                                    Please Wait ...
                                                </asp:Label>
                                            </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlSchemeBlock" Visible="false">
                                        <div class="form-control">
                                            <div class="select_div">
                                                <div class="label">
                                                    Scheme:
                                                </div>
                                                <div class="field">
                                                    <asp:DropDownList runat="server" ID="ddlNewSchemes" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <div class="select_div">
                                                <div class="label">
                                                    Block:
                                                </div>
                                                <div class="field">
                                                    <asp:DropDownList runat="server" ID="ddlBlock" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                            <asp:Panel runat="server" ID="pnlCounts">
                                <div class="left_part">

                                <div class="outer-boxes">
                                    <div class="cover-box">
                                        <div class="box">
                                            <div class="text">
                                                No
                                                <br />
                                                Entries:
                                            </div>
                                            <div class="number">
                                                <%--  <asp:Label ID="lblNoEntries" runat="server"> -1 </asp:Label>--%>
                                                <asp:LinkButton ID="lnkBtnNoEntryDetail" runat="server" OnClick="lnkBtnNoEntryDetail_Click">
                                                -1
                                                </asp:LinkButton>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnNoEntries" runat="server" OnClick="lnkBtnNoEntries_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Certificates
                                                <br />
                                                Expired:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblExpired" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnExpired" runat="server" OnClick="lnkBtnExpired_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Legal<br />
                                                Proceedings:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblLegalProceedings" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnLegalProceedings" runat="server" OnClick="lnkBtnLegalProceedings_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="outer-boxes">
                                    <div class="cover-box">
                                        <div class="box">
                                            <div class="text">
                                                Due to Expire
                                                <br />
                                                in 56 Days:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblExpiresIn56Days" runat="server"> -1 </asp:Label>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Appointments
                                                <br />
                                                Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblAppointmentsArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Appointments
                                                <br />
                                                To Be Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblAppointmentsToBeArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnAppointmentsToBeArranged" runat="server" OnClick="lnkBtnAppointmentsToBeArranged_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="outer-boxes">
                                    <div class="cover-box">
                                        <div class="box">
                                            <div class="text">
                                                Defects
                                                <br />
                                                (To Be Arranged):
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblDefectsToBeArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnDefectsToBeArranged" runat="server" OnClick="lnkBtnDefectsToBeArranged_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Defects
                                                <br />
                                                Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblDefectsArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnDefectsArranged" runat="server" OnClick="lnkBtnDefectsArranged_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Capped
                                                <br />
                                                Appliances:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblCappedAppliances" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnCappedAppliances" runat="server" OnClick="lnkBtnCappedAppliances_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--Void Properties   --  START --%>
                                <div class="outer-boxes">
                                    <div class="cover-box">
                                        <div class="box">
                                            <div class="text">
                                                Void
                                                <br />
                                                Properties:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblVoidProperties" runat="server"> -1 </asp:Label>
                                                <%--<asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkBtnNoEntryDetail_Click">
                                                -1
                                                </asp:LinkButton>--%>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnVoidProp" runat="server" OnClick="lnkBtnVoidProperties_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Void Appointments
                                                <br />
                                                Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblVoidAppointmentsArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnVoidAppArrang" runat="server" OnClick="lnkBtnVoidAppointmentsArranged_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Void Appointments<br />
                                                To Be Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblVoidAppointmentsToBeArranged" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnVoidAppToBeArrang" runat="server" OnClick="lnkBtnVoidAppointmentsToBeArranged_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--Void Properties   --  END--%>
                                
                                <div class="outer-boxes">
                                    <div class="cover-box">
                                        <div class="box">
                                            <div class="text">
                                                Defects
                                                <br />
                                                Requiring Approval:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblDefectsRequiringApprovalCount" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton runat="server" OnClick="lnkBtnDefectsRequiringApproval_Click">
                                                <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>                                                                                
                                    </div>
                                </div>

                                </div>
                            </asp:Panel>
                            <%-- M&E and PAT   --  START--%>
                            <asp:Panel runat="server" ID="pnlSchemeBlockCounts" Visible="false">
                                <div class="left_part" style="width:33%">
                                    <div class="outer-boxes">
                                        <div class="outer-boxes">
                                            <div class="cover-box">
                                                <div class="box">
                                                    <div class="text">
                                                        No
                                                        <br />
                                                        Entries:
                                                    </div>
                                                    <div class="number">
                                                        <asp:Label ID="lblMENoEntry" runat="server"> -1 </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <div class="text">
                                                        Overdue
                                                        <br />
                                                        Services:
                                                    </div>
                                                    <div class="number">
                                                        <asp:Label ID="lblMEOverDue" runat="server"> -1 </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-boxes">
                                        <div class="cover-box">
                                            <div class="box">
                                                <div class="text">
                                                    Appointments
                                                    <br />
                                                    Arranged:
                                                </div>
                                                <div class="number">
                                                    <asp:Label ID="lblMEAppointmentArranged" runat="server"> -1 </asp:Label>
                                                </div>
                                                <div class="arrow">
                                                    <asp:LinkButton ID="lnkBtnSchemeAppointmentsArranged" runat="server" OnClick="lnkBtnSchemeAppointmentsArranged_Click">
                                                    <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <div class="text">
                                                    Appointments
                                                    <br />
                                                    to be Arranged:
                                                </div>
                                                <div class="number">
                                                    <asp:Label ID="lblMEAPTBA" runat="server"> -1 </asp:Label>
                                                </div>
                                                <div class="arrow">
                                                    <asp:LinkButton ID="lnkBtnSchemeATBA" runat="server" OnClick="lnkBtnSchemeATBA_Click">
                                                    <img src="../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%-- M&E and PAT   --  End--%>
                        <asp:Panel runat="server" ID="pnlRightside">
                            <div class="right_part">
                                <div class="right-outer-boxes">
                                    <div class="box-left">
                                        <div class="boxborder-s">
                                            Management Info:
                                        </div>
                                        <div class="box-s">
                                            <div class="text_div">
                                                <div class="label-text">
                                                    <asp:Label ID="lblMIProperties" runat="server"> 0 </asp:Label>
                                                </div>
                                                <div class="label-collon">
                                                    :
                                                </div>
                                                <div class="label-text-r">
                                                    Properties
                                                </div>
                                            </div>
                                            <div class="text_div">
                                                <div class="label-text">
                                                    <asp:Label ID="lblMIBoilerRooms" runat="server"> 0 </asp:Label>
                                                </div>
                                                <div class="label-collon">
                                                    :
                                                </div>
                                                <div class="label-text-r">
                                                    Boiler rooms
                                                </div>
                                            </div>
                                            <div class="text_div">
                                                <div class="label-text">
                                                    <asp:Label ID="lblMINoCertificates" runat="server"> 0 </asp:Label>
                                                </div>
                                                <div class="label-collon">
                                                    :
                                                </div>
                                                <div class="label-text-r">
                                                    No certificate data
                                                </div>
                                            </div>
                                            <div class="text_div">
                                                <div class="label-text">
                                                    <asp:Label ID="lblMIDueIn8To12Weeks" runat="server"> 0 </asp:Label>
                                                </div>
                                                <div class="label-collon">
                                                    :
                                                </div>
                                                <div class="label-text-r">
                                                    Due in 8-12 wks
                                                </div>
                                            </div>
                                            <div class="text_div">
                                                <div class="label-text" style="overflow-x: hidden">
                                                    <asp:Label ID="lblMIReportedFaults" runat="server"> 0 </asp:Label>
                                                </div>
                                                <div class="label-collon">
                                                    :
                                                </div>
                                                <div class="label-text-r">
                                                    % Due in 8-12 weeks
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-left">
                                        <div class="boxborder-s">
                                            Certificate Expiry:
                                        </div>
                                        <div class="box-s">
                                            <asp:Chart ID="chartCertificateExpiry" runat="server" Visible="true" Height="150"
                                                Width="220" Palette="BrightPastel" Style="padding-left: 10px; margin-top: 8px;">
                                                <Series>
                                                    <asp:Series Name="seriesCertificateExpiry" ChartType="Column" ChartArea="chartAreaCertificateExpiry"
                                                        Color="Aqua" IsXValueIndexed="false">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chartAreaCertificateExpiry" BackGradientStyle="Center">
                                                        <Area3DStyle Enable3D="true" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-left-b">
                                    <div class="boxborder-s-right">
                                        <div class="select_div-b">
                                            <div class="label-b" style="color: white;">
                                                Inspection Status:
                                            </div>
                                            <div style="float: right;>
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlInspectionType" class="styleselect" style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                        height: 25px !important;
                                                        font-size: 12px !important;
                                                        width: 205px !important;" 
                                                        runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-s" style="text-align: center">
                                            <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                                                <asp:Label ID="lblISNoResults" runat="server" ForeColor="Red">
                                                No Records
                                                </asp:Label>
                                                <cc1:PagingGridView ID="grdInspectionStatus" runat="server" AutoGenerateColumns="False"
                                                    Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                                                    GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Address" ItemStyle-CssClass="dashboard" SortExpression="Address">
                                                            <ItemTemplate>
                                                                <span style="margin-left: 0; padding: 8px 0 8px 0 !important; text-align: left;">
                                                                    <%-- <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>                                                              --%>
                                                                    <asp:LinkButton ID="btnAddress" runat="server" OnClick="lnkBtnAddressClicked" Text='<%# Bind("Address") %>'></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                            <ItemStyle BorderStyle="None" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Appointment Date" SortExpression="APPOINTMENTDATE">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAppointmentDate" runat="server" Text='<%# FormatDate(Eval("APPOINTMENTDATE")) %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ControlStyle BorderStyle="None" />
                                                            <ItemStyle BorderStyle="None" />
                                                        </asp:TemplateField>                 
                                                        <asp:TemplateField HeaderText="Cert Expiry" SortExpression="ExpiryDateSort">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCertExpiry" runat="server" Text='<%# FormatDate(Eval("ExpiryDateSort")) %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ControlStyle BorderStyle="None" />
                                                            <ItemStyle BorderStyle="None" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Fuel Type" SortExpression="ValueDetail">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblValueDetail" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ControlStyle BorderStyle="None" />
                                                            <ItemStyle BorderStyle="None" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                                </cc1:PagingGridView>
                                            </div>
                                            <div style="width: 100%; padding: 10px 0 24px 0px;">
                                                <div style="float: left; width: 33%">
                                                    Result
                                                    <asp:Label ID="lblISResults" runat="server">
                                                    0 of 0
                                                    </asp:Label>
                                                </div>
                                                <div style="float: left; width: 33%">
                                                    Page
                                                    <asp:Label ID="lblISPages" runat="server">
                                                    1 of 1
                                                    </asp:Label>
                                                </div>
                                                <div style="float: right; width: 18%; padding-right: 40px;">
                                                    <asp:LinkButton ID="lnkBtnISPrevious" runat="server" Text="Previous">
                                        
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnISNext" runat="server" Text="Next">
                                        
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- M&E and PAT   --  START--%>
                        <asp:Panel runat="server" ID="pnlRightSchemeBlock" Visible="false">
                            <div class="right_part" style="margin-top:9px; width:50%">
                                <div class="box-left-b">
                                    <div class="boxborder-s-right">
                                        <div class="select_div-b">
                                            <div class="label-b" style="color: white;">
                                                Servicing Status:
                                            </div>
                                            <div style="float: right;">
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlServicingStatus" class="styleselect" style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                        height: 25px !important;
                                                        font-size: 12px !important;
                                                        width: 205px !important;" 
                                                        runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-s" style="text-align: center">
                                        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                                                
                                            <cc1:PagingGridView ID="grdServicingStatus" runat="server" AutoGenerateColumns="False"
                                                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                                                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Address" ItemStyle-CssClass="dashboard" SortExpression="Address">
                                                        <ItemTemplate>
                                                            <span style="margin-left: 0; padding: 8px 0 8px 0 !important; text-align: left;">
                                                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>                                                              
                                                            </span>
                                                        </ItemTemplate>
                                                        <ItemStyle BorderStyle="None" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Next Date" SortExpression="NextDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNextDate" runat="server" Text='<%# FormatDate(Eval("NextDate")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ControlStyle BorderStyle="None" />
                                                        <ItemStyle BorderStyle="None" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                            </cc1:PagingGridView>
                                        </div>
                                        <div style="width: 100%; padding: 10px 0 24px 0px;">
                                            <div style="float: left; width: 33%">
                                                Result
                                                <asp:Label ID="lblServicingResults" runat="server">
                                                0 of 0
                                                </asp:Label>
                                            </div>
                                            <div style="float: left; width: 33%">
                                                Page
                                                <asp:Label ID="lblServicingPages" runat="server">
                                                1 of 1
                                                </asp:Label>
                                            </div>
                                            <div style="float: right; width: 18%; padding-right: 40px;">
                                                <asp:LinkButton ID="lnkBtnServicingPrevious" runat="server" Text="Previous">
                                        
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkBtnServicingNext" runat="server" Text="Next">
                                        
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- M&E and PAT   --  START--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>
    <asp:UpdateProgress ID="updateProgressMain" runat="server" AssociatedUpdatePanelID="updPanelMain"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
