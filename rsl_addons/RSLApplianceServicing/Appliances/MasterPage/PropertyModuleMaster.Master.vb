﻿Imports System
Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PropertyModuleMaster
    Inherits System.Web.UI.MasterPage

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                populateMenus()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Menus"
    ''' <summary>
    ''' Populate Menus
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateMenus()
        Dim objDashboard As DashboardBL = New DashboardBL
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        Dim servicingMenu As String = ApplicationConstants.ServicingMenu
        Dim resultDataset As DataSet = New DataSet
        objDashboard.getPropertyPageList(resultDataset, userId, servicingMenu)
        SessionManager.setPageAccessDataSet(resultDataset)

        Dim accessGrantedModulesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedModulesDt)
        Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
        Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
        Dim randomPageDt As DataTable = resultDataset.Tables(ApplicationConstants.RandomPageDt)

        populateRSLModulesList(accessGrantedModulesDt)
        populateHeaderMenus(accessGrantedMenusDt)

    End Sub

#End Region

#Region "Populate Modules"
    ''' <summary>
    ''' Populate Modules
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateRSLModulesList(ByVal accessGrantedModulesDt As DataTable)

        rptRSLMenu.DataSource = accessGrantedModulesDt
        rptRSLMenu.DataBind()

    End Sub

#End Region

#Region "Populate Menu"
    ''' <summary>
    ''' Populate header menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateHeaderMenus(ByVal accessGrantedMenusDt As DataTable)
        menuHeader.Items.Clear()
        For Each parentItem As DataRow In accessGrantedMenusDt.Rows
            Dim item As MenuItem = New MenuItem()
            item.Text = parentItem(ApplicationConstants.MenuCol)
            item.Value = parentItem(ApplicationConstants.MenuIdCol).ToString()
            item.SeparatorImageUrl = PathConstants.SeparatorImagePath
            item.NavigateUrl = parentItem(ApplicationConstants.UrlCol)

            If (parentItem(ApplicationConstants.MenuCol).Equals(ApplicationConstants.PropertyModuleMenu)) Then
                item.Selected = True
            End If

            If (String.IsNullOrEmpty(item.NavigateUrl) Or String.IsNullOrWhiteSpace(item.NavigateUrl)) Then
                item.Enabled = False
            End If

            menuHeader.Items.Add(item)
        Next
        menuHeader.DataBind()

    End Sub

#End Region

#End Region

End Class