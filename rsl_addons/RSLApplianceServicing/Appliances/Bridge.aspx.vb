﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.Threading


Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

#Region "Attributes"

    Dim isPropertyModule As Boolean = False
    Dim propertyid As String = String.Empty
    Dim isResources As Boolean = False
    Dim src As String = String.Empty
    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            UIMessageHelper.IsError = True
            UIMessageHelper.Message = ex.Message

            If UIMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If UIMessageHelper.IsError = True Then
                UIMessageHelper.setMessage(lblMessage, pnlMessage, UIMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Function"

#Region "Load User"
    ''' <summary>
    ''' Load User
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadUser()


        If ASPSession("USERID") IsNot Nothing Then

            'ASPSession("USERID") = 760
            'SessionManager.setUserEmployeeId(760)
            'SessionManager.setAppServicingUserId(760)


            getQueryStringValues()

            Dim classicUserId As Integer = Integer.Parse(ASPSession("USERID").ToString())
            'Dim classicUserId As Integer = 760
            Dim objUserBo As UserBO = New UserBO()
            Dim objUserBl As UsersBL = New UsersBL()
            Dim resultDataSet As DataSet = New DataSet()

            resultDataSet = objUserBl.getEmployeeById(classicUserId)

            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
            Else

                Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

                If isActive = False Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
                Else
                    setUserSession(resultDataSet, classicUserId)

                    If isResources Then
                        redirectToAccessGrantedPage(ApplicationConstants.ResourcesMenu)
                    ElseIf isPropertyModule Then
                        'To go directly to property module if the property id is set.
                        Response.Redirect(PathConstants.PropertyModule + "?id=" + propertyid)
                    ElseIf Request.QueryString(PathConstants.Page) IsNot Nothing Then
                        Dim url As String = getPageUrl(Request.QueryString(PathConstants.Page))
                        Response.Redirect(url)
                    Else
                        redirectToAccessGrantedPage(ApplicationConstants.ServicingMenu)
                    End If

                End If

            End If
        Else
            Response.Redirect(PathConstants.LoginPath, True)
        End If
    End Sub

#End Region

#Region "Set User Session"
    ''' <summary>
    ''' Set User Session
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="classicUserId"></param>
    ''' <remarks></remarks>
    Private Sub setUserSession(ByRef resultDataSet, ByRef classicUserId)

        SessionManager.setAppServicingUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())
        'Dim userPageAccess As New Dictionary(Of String, Integer)
        'Dim count As Integer = 0
        'Dim dr As DataRow
        'For Each dr In resultDataSet.Tables(0).Rows
        '    userPageAccess.Add(dr.Item("PageName").ToString(), count)
        '    count = count + 1
        'Next

        'SessionManager.setUserPageAccess(userPageAccess)
        SessionManager.setUserType(resultDataSet.Tables(0).Rows(0).Item("UserType").ToString())
    End Sub

#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()

        If Request.QueryString(PathConstants.Resources) IsNot Nothing AndAlso Request.QueryString(PathConstants.Resources) = PathConstants.Yes Then
            isResources = True
        ElseIf Request.QueryString(PathConstants.PropertyID) IsNot Nothing AndAlso Request.QueryString(PathConstants.PropertyID) <> String.Empty Then
            isPropertyModule = True
            propertyid = CType(Request.QueryString(PathConstants.PropertyID), String)
        End If

    End Sub
#End Region

#Region "Redirect to access granted page"
    ''' <summary>
    ''' Redirect to access granted page
    ''' </summary>
    ''' <remarks></remarks>
    Sub redirectToAccessGrantedPage(ByVal menuName As String)
        ' SessionManager.setUserEmployeeId(760)
        Dim objDashboardBL As DashboardBL = New DashboardBL
        Dim resultDatatable As DataTable
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        Dim resultDataset As DataSet = New DataSet
        objDashboardBL.getPropertyPageList(resultDataset, userId, menuName)

        Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
        resultDatatable = accessGrantedPagesDt.Clone()
        Dim navigateUrl As String = String.Empty

        Dim query = (From dataRow In accessGrantedPagesDt _
                  Where dataRow.Field(Of String)(ApplicationConstants.GrantPageCoreUrlCol).Trim() <> String.Empty _
                  Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol), _
                            dataRow.Field(Of Integer)(ApplicationConstants.GrantPageIdCol) Ascending _
                  Select dataRow)

        If query.Count > 0 Then
            resultDatatable = query.CopyToDataTable()
            navigateUrl = resultDatatable.Rows(0).Item(ApplicationConstants.GrantPageCoreUrlCol)
        Else
            navigateUrl = GeneralHelper.getAccessDeniedPage(menuName)
        End If

        Response.Redirect(navigateUrl, True)

    End Sub

#End Region

#Region "Get page url"
    ''' <summary>
    ''' Get page url 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getPageUrl(ByVal page As String)

        Dim pageDict As Dictionary(Of String, String) = New Dictionary(Of String, String)
        pageDict.Add(PathConstants.DashdoardPageKey, PathConstants.DashboardPath)
        pageDict.Add(PathConstants.StatusActionPageKey, PathConstants.StatusActionPath)
        pageDict.Add(PathConstants.ViewLettersPageKey, PathConstants.ViewLettersPath)
        pageDict.Add(PathConstants.AdminSchedulingPageKey, PathConstants.AdminSchedulingPath)
        pageDict.Add(PathConstants.WeeklyCalendarPageKey, PathConstants.WeeklyCalendarPagePath)
        pageDict.Add(PathConstants.CertificateExpiryPageKey, PathConstants.CertificateExpiryPath)
        pageDict.Add(PathConstants.IssuedCertificatesPageKey, PathConstants.IssuedCertificatesPath)
        pageDict.Add(PathConstants.PrintCertificatesPageKey, PathConstants.PrintCertificatesPath)
        pageDict.Add(PathConstants.FuelServicingPageKey, PathConstants.FuelServicingPath)
        pageDict.Add(PathConstants.VoidServicingPageKey, PathConstants.VoidServicingPath)
        pageDict.Add(PathConstants.PropertiesPageKey, PathConstants.PropertiesPath)
        pageDict.Add(PathConstants.DefectSchedulingPageKey, PathConstants.ScheduleDefect)
        pageDict.Add(PathConstants.AssignToContractorPageKey, PathConstants.AssignToContractorReportPath)
        pageDict.Add(PathConstants.DefectApprovalPageKey, PathConstants.DefectApprovalReportPath)

        pageDict.Add(PathConstants.ViewPlannedJobSheetKey, PathConstants.PlannedJobSheet)
        pageDict.Add(PathConstants.AlternativeServicingPageKey, PathConstants.AlternativeServicingPath)
        pageDict.Add(PathConstants.OilServicingPageKey, PathConstants.OilServicingPath)

        Dim url As String = String.Empty
        If (pageDict.ContainsKey(page)) Then
            url = pageDict.Item(page)
            If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing AndAlso Request.QueryString(PathConstants.PropertyIds) <> String.Empty Then
                url = url + "?id=" + Request.QueryString(PathConstants.PropertyIds).ToString()
            End If
            If Request.QueryString(PathConstants.Src) IsNot Nothing AndAlso Request.QueryString(PathConstants.Src) <> String.Empty Then
                url = url + "?src=" + Request.QueryString(PathConstants.Src).ToString()
            End If
        End If

        Return url

    End Function

#End Region

#End Region

End Class
