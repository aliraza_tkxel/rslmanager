﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports AS_BusinessLogic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<System.Web.Script.Services.ScriptService()>
Public Class SearchNames
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function GetCompletionList(ByVal prefixText As String, ByVal count As Integer) As String()

        Dim resultDataSet As DataSet = New DataSet()
        Dim addUsers As AddUser = New AddUser()
        addUsers.getQuickFindUsers(prefixText, resultDataSet)

        Dim names(resultDataSet.Tables(0).Rows.Count() - 1) As String
        Dim counter As Integer = 0
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In resultDataSet.Tables(0).Rows
                Dim employeeName As String = row.Item("FIRSTNAME") + " " + row.Item("LASTNAME")
                names(counter) = employeeName
                counter = counter + 1
            Next
        End If
        Return names
    End Function
    'Public Function setEmpployeeId(ByVal prefixText As String) As Integer

    '    Dim resultDataSet As DataSet = New DataSet()
    '    Dim addUsers As AddUser = New AddUser()
    '    addUsers.getQuickFindUsers(prefixText, resultDataSet)

    '    Dim names(resultDataSet.Tables(0).Rows.Count() - 1) As String
    '    Dim counter As Integer = 0

    '    For Each row As DataRow In resultDataSet.Tables(0).Rows
    '        Dim employeeId As String = row.Item("EMPLOYEEID")
    '        names(counter) = employeeName
    '        counter = counter + 1
    '    Next

    'End Function

End Class