﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization
Imports System.Net.Mail



Public Class AsbestosTab
    Inherits UserControlBase
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objUserBl As UsersBL = New UsersBL()
#Region "Properties"


    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "DocumentId", 1, 10)
    Dim totalCount As Integer = 0

#End Region
#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PropPageName As String = sender.Page.ToString()
        If PropPageName.Contains("properties") Then
            _readOnly = False
        ElseIf PropPageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnAddAsbestos.Visible = False
            btnAmend.Visible = False
            btnReset.Visible = False
            btnCancel.Visible = False
            btnSavePropertyDocuments.Visible = False
            lblFileUpload.Visible = False
            btnFileUpload.Visible = False
            pnlAddPhotoPopup.Enabled = False
        End If
        Try
            uiMessageHelper.resetMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage)
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            bindToGrid(SessionManager.getPropertyId())
            If Not IsPostBack Then
                Me.populateAsbestosAndRisk()
                btnAmend.Enabled = False
                rdBtnUrgentActionRequired.SelectedValue = "False"
                BindDocumentSubTypes(3)
            End If
            Dim pageName As String = System.IO.Path.GetFileName(Request.Url.ToString())
            If (pageName.Contains("PropertyRecord.aspx")) Then
                btnSavePropertyDocuments.Enabled = False
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region



#Region "btn Add asbestos "
    Protected Sub btnAddAsbestos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAsbestos.Click
        Try
            Me.saveAsbestosAndRisk()
            Me.bindToGrid(SessionManager.getPropertyId())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Amend click event"
    Protected Sub btnAmendAsbestos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAmend.Click
        Try
            Me.amendAsbestosInformation()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Reset click event"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        Try
            txtAdded.Text = ""
            txtNotes.Text = ""
            txtRemoved.Text = ""
            ddlRisk.SelectedIndex = 0
            ddlAsbestosElement.SelectedIndex = 0
            ddlAsbestosLevel.SelectedIndex = 0
            btnAddAsbestos.Enabled = True
            btnAmend.Enabled = False
            rdBtnUrgentActionRequired.Items.FindByValue("False").Selected = True
            ddlRiskLevel.SelectedIndex = 0
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn view click event"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddlDataSet As DataSet = New DataSet()
            Dim asbestosId As Integer = 0
            Dim btnView As Button = DirectCast(sender, Button)
            asbestosId = btnView.CommandArgument
            Dim resultDataSet As DataSet = New DataSet()
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            objPropertyBL.getPropertyAsbestosRiskAndOtherInfo(resultDataSet, asbestosId)
            objPropertyBL.getPropertyAsbestosAndRisk(ddlDataSet)


            If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("DateRemoved")) Then
                Dim dateRemoved As Date = Convert.ToDateTime(resultDataSet.Tables(0).Rows(0).Item("DateRemoved"))
                If (dateRemoved < DateTime.Now.Date) Then
                    uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.AsbestosDisable, True)
                    Return
                End If

            End If


            If resultDataSet.Tables(0).Rows.Count > 0 Then
                hdnHiddenId.Value = resultDataSet.Tables(0).Rows(0).Item("PROPASBLEVELID")
                Dim asbriskLevelCheck As String
                Dim ASBRISKLEVELID As Integer = resultDataSet.Tables(0).Rows(0).Item("ASBRISKLEVELID")
                asbriskLevelCheck = "ASBRISKLEVELID =" + ASBRISKLEVELID.ToString()
                Dim dr() As DataRow = ddlDataSet.Tables("AsbestosLevel").Select(asbriskLevelCheck)
                If dr.Count > 0 Then
                    ddlAsbestosLevel.SelectedValue = resultDataSet.Tables(0).Rows(0).Item("ASBRISKLEVELID")


                Else
                    uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidAsbestosLevel, True)
                End If

                Dim asbestosElementCheck As String
                Dim AsbestosIdCheck As Integer = resultDataSet.Tables(0).Rows(0).Item("ASBESTOSID")
                asbestosElementCheck = "ASBESTOSID =" + AsbestosIdCheck.ToString()
                Dim drc() As DataRow = ddlDataSet.Tables("AsbestosElements").Select(asbestosElementCheck)
                If drc.Count > 0 Then
                    ddlAsbestosElement.SelectedValue = resultDataSet.Tables(0).Rows(0).Item("ASBESTOSID")
                Else
                    uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidAsbestosElement, True)
                End If

                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("ASBRISKID")) Then
                    ddlRisk.SelectedValue = resultDataSet.Tables(0).Rows(0).Item("ASBRISKID")
                End If

                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("DateAdded")) Then
                    txtAdded.Text = resultDataSet.Tables(0).Rows(0).Item("DateAdded")
                Else
                    txtAdded.Text = String.Empty
                End If
                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("RiskLevelId")) Then
                    If resultDataSet.Tables(0).Rows(0).Item("RiskLevelId").ToString() <> "-" AndAlso resultDataSet.Tables(0).Rows(0).Item("RiskLevelId") > 0 Then
                        ddlRiskLevel.SelectedValue = resultDataSet.Tables(0).Rows(0).Item("RiskLevelId")
                    End If
                End If
                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("IsUrgentActionRequired")) Then

                    rdBtnUrgentActionRequired.SelectedValue = resultDataSet.Tables(0).Rows(0).Item("IsUrgentActionRequired").ToString()
                Else
                    rdBtnUrgentActionRequired.SelectedValue = "False"
                End If

                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("DateRemoved")) Then
                    txtRemoved.Text = resultDataSet.Tables(0).Rows(0).Item("DateRemoved")
                Else
                    txtRemoved.Text = String.Empty
                End If

                If Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("Notes")) Then
                    txtNotes.Text = resultDataSet.Tables(0).Rows(0).Item("Notes")
                Else
                    txtNotes.Text = String.Empty
                End If
                btnAddAsbestos.Enabled = False
                btnAmend.Enabled = True
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlType.SelectedIndexChanged

        Dim documentTypeId As Integer = Convert.ToInt32(ddlType.SelectedValue)
        If documentTypeId > -1 Then
            ddlSubtype.Enabled = True
        End If

        BindDocumentSubTypes(documentTypeId)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSavePropertyDocuments.Click
        Try
            If validateForm() Then
                propertyDocumentUpload()
                resetDocumentControls()

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                pnlMessage.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If




        End Try


    End Sub


#End Region


#Region "Bind To Grid"

    Public Sub bindToGrid(ByVal propertyId As String)
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPageSortBo = getPageSortBoViewState()
        'resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        totalCount = objPropertyBL.getHealthAndSafetyTabInformation(resultDataSet, propertyId, objPageSortBo)
        'If (totalCount >= 0) Then
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        grdDocumentInfo.VirtualItemCount = totalCount
        grdDocumentInfo.DataSource = resultDataSet
        grdDocumentInfo.DataBind()
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        divAsbestosGrid.Visible = True
        ' Else
        'divAsbestosGrid.Visible = False
        'uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.NoRecordFound, True)
        'End If
    End Sub


#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub
#End Region

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub



#End Region
#Region "Send Email notification on Urgent Action Required"
    Protected Sub sendEmailNotificationOnUrgentAction()
        Try
            ' property address
            ' specific job role persons email address
            Dim employeeEmailForAsbestosDS As New DataSet()

            objPropertyBl.GetEmployeeEmailForAsbestosEmailNotifications(SessionManager.getPropertyId(), employeeEmailForAsbestosDS)
            If Not IsNothing(employeeEmailForAsbestosDS) Then
                For Each row As DataRow In employeeEmailForAsbestosDS.Tables(0).Rows
                    If Not IsDBNull(row.Item("WORKEMAIL")) Then
                        ' send email
                        Dim mailMessage As New Mail.MailMessage
                        mailMessage.Subject = ApplicationConstants.UrgentAsbestosEmailSubject

                        ' load email body

                        Dim body As New StringBuilder
                        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AsbestosUrgentAction.htm"))
                        body.Append(reader.ReadToEnd())

                        '==========================================='
                        ' Employee Name
                        body.Replace("{FULLNAME}", row.Item("FULLNAME"))
                        ' Property Address
                        body.Replace("{PROPERTYADDRESS}", row.Item("PROPERTYADDRESS"))
                        '==========================================='
                        ' There is an asbestos present at the below property. Please contact Broad land Housing for details.
                        ' Attach logo images with email
                        '==========================================='

                        Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/PropertyModule/broadland.png"))
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                        body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                        Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")
                        Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs)

                        '==========================================='

                        ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.To.Add(New MailAddress(row.Item("WORKEMAIL"), row.Item("FULLNAME")))
                        mailMessage.Body = body.ToString
                        mailMessage.AlternateViews.Add(alternatevw)
                        mailMessage.IsBodyHtml = True
                        EmailHelper.sendEmail(mailMessage)
                    End If
                Next
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region
#Region "Save Asbestos & Risk Information"
    Protected Sub saveAsbestosAndRisk()
        Try
            Dim objPropertyAsbestos As New PropertyAsbestosBO()
            Dim dat2 As System.Nullable(Of DateTime)
            dat2 = Nothing
            objPropertyAsbestos.AsbestosLevelId = ddlAsbestosLevel.SelectedItem.Value
            objPropertyAsbestos.AsbestosElementId = ddlAsbestosElement.SelectedItem.Value
            objPropertyAsbestos.RiskId = ddlRisk.SelectedValue
            objPropertyAsbestos.AddedDate = Date.Parse(txtAdded.Text)
            Dim dat1 As DateTime = DateTime.Parse(txtAdded.Text)
            If (txtRemoved.Text <> String.Empty) Then
                dat2 = DateTime.Parse(txtRemoved.Text)
            End If
            objPropertyAsbestos.Notes = txtNotes.Text
            objPropertyAsbestos.PropertyId = SessionManager.getPropertyId()
            'objPropertyAsbestos.UserId = 605 ' For Testing
            objPropertyAsbestos.UserId = SessionManager.getAppServicingUserId() ' When go live
            If (ddlRiskLevel.SelectedValue > 0) Then
                objPropertyAsbestos.RiskLevel = ddlRiskLevel.SelectedValue
            End If
            objPropertyAsbestos.UrgentActionRequired = Convert.ToBoolean(rdBtnUrgentActionRequired.SelectedValue)
            ' if UrgentActionRequired = true then send email to the holders of Job Role:
            ' Executive Property Director
            ' Head of property Operations
            ' Interim Head of Asset Management

            If (objPropertyAsbestos.UrgentActionRequired = True) Then
                sendEmailNotificationOnUrgentAction()
            End If

            If (dat2 > dat1 Or IsNothing(dat2)) Then

                If Not (IsNothing(dat2)) Then
                    If (dat2 < DateTime.Now.Date) Then
                        uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidRemovedDate, True)
                        Return
                    End If
                End If

                If txtRemoved.Text <> String.Empty Then
                    objPropertyAsbestos.RemovedDate = txtRemoved.Text
                End If
                objPropertyBl.saveAsbestos(objPropertyAsbestos)
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.SuccessMessage, False)
                txtAdded.Text = ""
                txtNotes.Text = ""
                txtRemoved.Text = ""
                ddlRisk.SelectedIndex = 0
                ddlAsbestosElement.SelectedIndex = 0
                ddlAsbestosLevel.SelectedIndex = 0
                btnAddAsbestos.Enabled = True
                btnAmend.Enabled = False
                rdBtnUrgentActionRequired.SelectedValue = "False"
                ddlRiskLevel.SelectedIndex = 0
            Else
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidDate, True)
            End If

            '  Dim healthSafety As UserControl = CType(Parent.FindControl("HealthSafety"), UserControl)
            ' Dim healthSafety As UserControl = CType(Me.Parent.FindControl("HealthSafety"), UserControl)
            ' Me.Page.GetType().InvokeMember("bindToGrid(Request.QueryString('id')", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, Nothing, New Object())
            ' DirectCast(Parent.Page, UserControl).bindToGrid(SessionManager.getPropertyId())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try



    End Sub

#End Region

#Region "Amend Asbestos Information"
    Protected Sub amendAsbestosInformation()
        Try
            Dim objPropertyAsbestos As New PropertyAsbestosBO()
            Dim dat2 As System.Nullable(Of DateTime)
            dat2 = Nothing
            objPropertyAsbestos.AsbestosLevelId = ddlAsbestosLevel.SelectedItem.Value
            objPropertyAsbestos.AsbestosElementId = ddlAsbestosElement.SelectedItem.Value
            objPropertyAsbestos.RiskId = ddlRisk.SelectedValue
            objPropertyAsbestos.AddedDate = Date.Parse(txtAdded.Text)
            Dim dat1 As DateTime = DateTime.Parse(txtAdded.Text)
            If (txtRemoved.Text <> String.Empty) Then
                dat2 = DateTime.Parse(txtRemoved.Text)
            End If
            objPropertyAsbestos.Notes = txtNotes.Text
            objPropertyAsbestos.PropasbLevelID = hdnHiddenId.Value
            objPropertyAsbestos.UserId = SessionManager.getApplianceServicingId()
            If (ddlRiskLevel.SelectedValue > 0) Then
                objPropertyAsbestos.RiskLevel = ddlRiskLevel.SelectedValue
            End If
            objPropertyAsbestos.UrgentActionRequired = Convert.ToBoolean(rdBtnUrgentActionRequired.SelectedValue)

            ' if UrgentActionRequired = true then send email to the holders of Job Role:
            ' Executive Property Director
            ' Head of property Operations
            ' Interim Head of Asset Management

            If (objPropertyAsbestos.UrgentActionRequired = True) Then
                sendEmailNotificationOnUrgentAction()
            End If

            If (dat2 > dat1 Or IsNothing(dat2)) Then

                If Not (IsNothing(dat2)) Then
                    If (dat2 < DateTime.Now.Date) Then
                        uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidRemovedDate, True)
                        Return
                    End If
                End If


                If txtRemoved.Text <> String.Empty Then
                    objPropertyAsbestos.RemovedDate = txtRemoved.Text
                End If
                objPropertyBl.amendAsbestos(objPropertyAsbestos)
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.SuccessMessage, False)
                btnAmend.Enabled = False
                bindToGrid(SessionManager.getPropertyId())
                txtAdded.Text = ""
                txtNotes.Text = ""
                txtRemoved.Text = ""
                ddlRisk.SelectedIndex = 0
                ddlAsbestosElement.SelectedIndex = 0
                ddlAsbestosLevel.SelectedIndex = 0
                btnAddAsbestos.Enabled = True
                rdBtnUrgentActionRequired.Items.FindByValue("False").Selected = True
                ddlRiskLevel.SelectedIndex = 0

            Else
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, UserMessageConstants.InvalidDate, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Get Property Asbestos (Level and Elements) & Risk Information"
    Protected Sub populateAsbestosAndRisk()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyAsbestosAndRisk(resultDataSet)
        'Populating Asbestos Level Drop down List
        ddlAsbestosLevel.DataSource = resultDataSet.Tables("AsbestosLevel")
        ddlAsbestosLevel.DataTextField = "ASBRISKLEVELDESCRIPTION"
        ddlAsbestosLevel.DataValueField = "ASBRISKLEVELID"
        ddlAsbestosLevel.DataBind()
        'Populating Asbestos Element Drop down List   
        ddlAsbestosElement.DataSource = resultDataSet.Tables("AsbestosElements")
        ddlAsbestosElement.DataTextField = "RISKDESCRIPTION"
        ddlAsbestosElement.DataValueField = "ASBESTOSID"
        ddlAsbestosElement.DataBind()
        'Populating Risk Drop down List
        ddlRisk.DataSource = resultDataSet.Tables("Risk")
        ddlRisk.DataTextField = "ASBRISKID"
        ddlRisk.DataValueField = "ASBRISKID"
        ddlRisk.DataBind()
        'Populating Risk Level drop down List
        ddlRiskLevel.DataSource = resultDataSet.Tables("Level")
        ddlRiskLevel.DataTextField = "Description"
        ddlRiskLevel.DataValueField = "AsbestosLevelId"
        ddlRiskLevel.DataBind()

        ddlAsbestosLevel.Items.Insert(0, New ListItem("Please Select", "-1"))
        ddlAsbestosElement.Items.Insert(0, New ListItem("Please Select", "-1"))
        ddlRisk.Items.Insert(0, New ListItem("Please Select", "-1"))
        ddlRiskLevel.Items.Insert(0, New ListItem("Please Select", "-1"))




    End Sub

#End Region


#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            bindToGrid(SessionManager.getPropertyId())

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)

                bindToGrid(SessionManager.getPropertyId())
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddAsbestosMessage, pnlAddAsbestosMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region



    Sub BindDocumentSubTypes(ByVal DocumentTypeId As Int16)

        Dim objDocumentTypeBL As DocumentsBL = New DocumentsBL()
        Dim resultDataSet As DataSet = New DataSet()

        objDocumentTypeBL.getDocumentSubtypesList(resultDataSet, DocumentTypeId, "Property")
        ddlSubtype.DataSource = resultDataSet.Tables(0)
        ddlSubtype.DataValueField = "DocumentSubtypeId"
        ddlSubtype.DataTextField = "Title"
        ddlSubtype.DataBind()

    End Sub

#Region " Validate Form "

    Private Function validateForm() As Boolean

        If Convert.ToInt32(Me.ddlType.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTypeValidationError, True)
            Return False
        End If

        If Convert.ToInt32(Me.ddlSubtype.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTitleValidationError, True)
            Return False
        End If



        If txtDocumentDate.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDocumentDateValidationError, True)
            Return False
        End If


        If txtExpires.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectExpiryDateValidationError, True)
            Return False
        End If


        If lblFileName.Text = String.Empty Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAFile, True)
            Return False
        End If

        'If (System.IO.Path.GetExtension(flUploadDoc.FileName) <> ".pdf") Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAPDF, True)
        '    Return False
        'End If

        Return True

    End Function

#End Region

#Region "Save Property Document Information"
    Sub propertyDocumentUpload()
        'Dim cp12InByteForm As Byte()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        End If
        Try
            objPropertyDocumentBO.DocumentCategory = ddlDocumentCategory.SelectedValue
            objPropertyDocumentBO.DocumentTypeId = ddlType.SelectedItem.Value
            objPropertyDocumentBO.DocumentSubTypeId = ddlSubtype.SelectedItem.Value
            objPropertyDocumentBO.Keyword = txtKeyword.Text
            objPropertyDocumentBO.UploadedBy = SessionManager.getUserEmployeeId()
            objPropertyDocumentBO.ExpiryDate = txtExpires.Text
            objPropertyDocumentBO.DocumentDate = txtDocumentDate.Text

            Dim fileDirectoryPath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + objPropertyDocumentBO.PropertyId + "/Documents/")
            If (Directory.Exists(fileDirectoryPath) = False) Then
                Directory.CreateDirectory(fileDirectoryPath)
            End If
            objPropertyDocumentBO.DocumentName = lblFileName.Text
            objPropertyDocumentBO.DocumentPath = fileDirectoryPath
            If Not objPropertyDocumentBO.CP12Number = "" Or Not IsNothing(objPropertyDocumentBO.CP12Number) Then
                objPropertyDocumentBO.CP12Dcoument = SessionManager.getCP12Dcoument()
            End If
            objPropertyBL.savePropertyDocumentUpload(objPropertyDocumentBO)
            uiMessageHelper.setMessage(lblMessage, pnlMessage, "Document uploaded successfully!", False)
        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)
        End Try

        lblFileName.Text = String.Empty
    End Sub
#End Region

#Region "Reset Controls"

    Protected Sub resetDocumentControls()
        ddlSubtype.SelectedIndex = 0
        ddlType.SelectedIndex = 0

        txtDocumentDate.Text = String.Empty
        txtExpires.Text = String.Empty
        txtKeyword.Text = String.Empty
        lblFileName.Text = String.Empty

    End Sub

#End Region

#Region "ckBoxPhotoUpload Checked Changed"
    ''' <summary>
    ''' ckBoxPhotoUpload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxAsbestosDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxAsbestosDocumentUpload.CheckedChanged
        Me.ckBoxAsbestosDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                lblFileName.Text = SessionManager.getDocumentUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

            SessionManager.removePhotoUploadName()
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        resetDocumentControls()
    End Sub
End Class