﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Accommodation.ascx.vb"
    Inherits="Appliances.Accommodation" %>
<%@ Register TagPrefix="uc" TagName="Summary" Src="~/Controls/Property/AttributeSummary.ascx" %>
<%@ Register TagPrefix="uc" TagName="RoomsDimensions" Src="~/Controls/Property/RoomsDimensions.ascx" %>
<style type="text/css">
    .roomsDimensionsSection
    {
        float: left;
        width: 300px;
        margin-left: 2px;
        margin-top: 5px;
        border: 1px solid black;
    }
    .summarySection
    {
        margin-left: 320px;
        min-width: 880px;
        border: 1px solid gray;
        padding: 5px;
        padding-top: 10px;
        min-height: 600px;
    }
    .roomsDimensionsSectionMain
    {
        float: left;
        width: 305px;
        margin-left: 10px;
        margin-top: 5px;
        
    }
</style>
<div class="roomsDimensionsSectionMain">
<div class="roomsDimensionsSection">
    <uc:RoomsDimensions runat="server" ID="roomsDimensionsSection" />
    
</div>
<div class="roomsDimensionsSection">
<div style="float: left; width: 100%; margin-left: 5px;">
        <asp:Panel ID="pnlCurrentRentTab" runat="server" BackColor="White" Width="280px">
            <table id="tblCurrentRentTab">
                <tr>
                    <td style="width: 30%">
                    </td>
                    <td style="width: 20%">
                    </td>
                    <td style="width: 25%">
                    </td>
                    <td style="width: 25%">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <div class="mainheading-panel" style=" margin-top: 5px; 
                            width: 100%;">
                            Financials
                        </div>
                    </td>
                </tr>
                

                <tr>
                    <td align="left" valign="top" style="padding-left: 2px;" colspan="2">
                        Date Rent Set:
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <asp:TextBox ID="txtRentDate" runat="server" Width="150px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-left: 2px;" colspan="2">
                        Rent effective to:<span class="Required"></span>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <asp:TextBox ID="txtEffectiveDate" runat="server" Width="150px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-left: 2px;" colspan="2">
                        Rent Type:<span class="Required"></span>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <asp:Label Text="" ID="lblRentType" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-left: 2px;">
                        Rent
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblRent" runat="server" />
                    </td>
                    <td valign="top">
                        Services
                    </td>
                    <td align="left" class="style3" valign="top">
                        <asp:Label Text="0.00" ID="lblServices" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="padding-left: 2px;">
                        Council Tax
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblCouncilTax" runat="server" />
                    </td>
                    <td valign="top">
                        Water
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblWater" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-left: 2px;">
                        Inelig.
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblInelig" runat="server" />
                    </td>
                    <td align="left" class="" valign="top">
                        Supp
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblSupp" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="padding-left: 2px;" valign="top">
                        Garage
                    </td>
                    <td align="left" valign="top">
                        <asp:Label Text="0.00" ID="lblGarage" runat="server" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr bgcolor="#133E71">
                    <td align="left" class="style7" valign="top">
                        <strong style="color: #fff">Total Rent</strong>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        
                         <asp:TextBox ID="txtTotalRent" runat="server" Width="50px" BackColor="White"  ReadOnly="true"  >0.00</asp:TextBox>
                    </td>
                    <td align="center" style="text-align: center;" valign="top">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div> 
</div> 
<div class="summarySection">
    <uc:Summary runat="server" ID="summarySection" />
</div>
