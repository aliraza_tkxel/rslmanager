﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PrintActivities
    Inherits UserControlBase
#Region "Attributes"

    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Dim sortDirection As String
    Dim sortExpression As String
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
        loadPrintActivities(propertyId)
        loadPropertyDetails(propertyId)

    End Sub

    Public Sub loadPrintActivities(ByVal propertyId As String)
        Try
            Dim resultDataSet As DataSet = New DataSet()
            sortExpression = "CREATIONDATE"
            sortDirection = "DESC"
            objPropertyBL.propertyActivitiesList(resultDataSet, sortDirection, sortExpression, -1, propertyId)
            grdPrintActivities.DataSource = resultDataSet
            grdPrintActivities.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try
    End Sub

    Public Sub loadPropertyDetails(ByVal propertyId As String)
        Dim objProertyDetails As PropertyDetailsBO = New PropertyDetailsBO()
        objPropertyBL.loadPropertyDetails(propertyId, objProertyDetails)
        lblAddress.Text = objProertyDetails.Address
        lblDev.Text = objProertyDetails.Dev
        lblPostCode.Text = objProertyDetails.PostCode
        lblScheme.Text = objProertyDetails.Scheme
        lblStatus.Text = objProertyDetails.Status
        lblType.Text = objProertyDetails.Type

        'Start - Changes By Aamir Waheed on April 19 2013
        'To show reference no on print activities page.
        lblRefNumber.Text = propertyId 'objProertyDetails.RefNumber
        'End - Changes By Aamir Waheed on April 19 2013

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
        Response.Redirect("~/Views/Property/PropertyRecord.aspx?" + PathConstants.PropertyIds + "=" + propertyId, False)

    End Sub
End Class