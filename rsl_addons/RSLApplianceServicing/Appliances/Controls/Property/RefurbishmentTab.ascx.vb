﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class RefurbishmentTab
    Inherits UserControlBase
    Dim _readOnly As Boolean = False

    Public uploadedThumbUri As String = GeneralHelper.getThumbnailUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/"
#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PropPageName As String = sender.Page.ToString()
        If PropPageName.Contains("properties") Then
            _readOnly = False
        ElseIf PropPageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnSubmit.Visible = False
            pnlRefurbishmentForm.Enabled = False
        End If
        uiMessageHelper.resetMessage(lblRefurbishmentMessage, pnlRefurbishmentMessage)
        Me.bindToGrid(SessionManager.getPropertyId())
    End Sub
#End Region

#Region "Submit Form"
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Me.saveRefurbishmentInformation()
        Me.bindToGrid(SessionManager.getPropertyId())
    End Sub
#End Region

#Region "Save Refurbishment Information"
    Protected Sub saveRefurbishmentInformation()
        Try
            Dim errorMessage As String
            Dim objPropertyBl As PropertyBL = New PropertyBL()
            Dim objRefurbishmentBO As New RefurbishmentBO()
            objRefurbishmentBO.RefurbishmentDate = txtRefurbishmentDate.Text
            objRefurbishmentBO.Notes = txtNotes.Text
            objRefurbishmentBO.PropertyId = SessionManager.getPropertyId()
            'objRefurbishmentBO.UserId = 605 ' For Testing
            objRefurbishmentBO.UserId = SessionManager.getAppServicingUserId() ' When go live
            errorMessage = objPropertyBl.saveRefurbishment(objRefurbishmentBO)
            If (errorMessage = String.Empty) Then
                uiMessageHelper.setMessage(lblRefurbishmentMessage, pnlRefurbishmentMessage, UserMessageConstants.SuccessMessage, False)
            Else
                uiMessageHelper.setMessage(lblRefurbishmentMessage, pnlRefurbishmentMessage, UserMessageConstants.RefurbishmentError, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblRefurbishmentMessage, pnlRefurbishmentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Bind To Grid"

    Public Sub bindToGrid(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPropertyBL.getRefurbishmentInformation(resultDataSet, propertyId)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            grdRefurbishment.DataSource = resultDataSet
            grdRefurbishment.DataBind()
            divRefurbishmentGrid.Visible = True
        Else
            divRefurbishmentGrid.Visible = False
            uiMessageHelper.setMessage(lblRefurbishmentMessage, pnlRefurbishmentMessage, UserMessageConstants.NoRecordFound, True)
        End If


    End Sub


#End Region

#End Region

   
    Protected Sub grdRefurbishment_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRefurbishment.PageIndexChanging
        grdRefurbishment.PageIndex = e.NewPageIndex
        Me.bindToGrid(SessionManager.getPropertyId())
    End Sub
End Class