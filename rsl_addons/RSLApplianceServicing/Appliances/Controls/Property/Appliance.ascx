﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Appliance.ascx.vb"
    Inherits="Appliances.Appliance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="defects" TagName="ServiceDefects" Src="~/Controls/Property/ServiceDefects.ascx" %>
<%@ Register TagPrefix="dphotos" TagName="DefectPhotographs" Src="~/Controls/Property/DefectPhotographs.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .style3
    {
        width: 283px;
    }
    .style4
    {
        width: 138px;
    }
    .style5
    {
        width: 221px;
    }
    
    .panelPopup
    {
        min-height: 130px;
        overflow: auto;
    }
</style>
<div style="margin-top: 10px">
    <%-- <asp:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </asp:ToolkitScriptManager>--%>
    <asp:UpdatePanel runat="server" ID="updPnlAppliances">
        <ContentTemplate>
            <div style="float: right; margin-top: -15px; margin-bottom: 8px;">
                <asp:Button ID="btnAddAppliance" runat="server" Text="Add Appliance" BackColor="White" />
            </div>
            <div style="display: none;">
                <asp:HiddenField ID="hdnSelectedLocationId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedmakeId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedTypeId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedModelId" runat="server" Value="-1" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div style="clear: both;">
            </div>
            <asp:Panel ID="pnlAppliancesGrid" runat="server" Height="450px" ScrollBars="Auto">
                <cc1:PagingGridView runat="server" ID="grdAppliances" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                    GridLines="Horizontal" Width="100%" AllowSorting="True" ShowHeaderWhenEmpty="True" OnRowDataBound="grdAppliances_RowDataBound" 
                    PageSize="30" AllowPaging="True" OrderBy="" EmptyDataText="No Record Found" Font-Strikeout="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Item" SortExpression="Item">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%# Bind("Item") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dimensions" SortExpression="Dimensions">
                            <ItemTemplate>
                                <asp:Label ID="lblDimensions" runat="server" Text='<%# Bind("Dimensions") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" SortExpression="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Make" SortExpression="Make">
                            <ItemTemplate>
                                <asp:Label ID="lblMake" runat="server" Text='<%# Bind("Make") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Serial No" SortExpression="SerialNumber">
                            <ItemTemplate>
                                <asp:Label ID="lblSerialNumber" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purchase Cost" SortExpression="PurchaseCost">
                            <ItemTemplate>
                                <asp:Label ID="lblPurchaseCost" runat="server" Text='<%# Bind("PurchaseCost") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Purchased" SortExpression="DatePurchased">
                            <ItemTemplate>
                                <asp:Label ID="lblDatePurchased" runat="server" Text='<%# Bind("DatePurchased") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Removed " SortExpression="DateRemoved">
                            <ItemTemplate>
                                <asp:Label ID="lblRemovedDate" runat="server" Text='<%# Bind("DateRemoved") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Eval("ApplianceId") %>'
                                    Text="Delete" OnClick="btnDelete_Click" BackColor="White" UseSubmitBehavior="false"
                                    Visible="false" />
                                <asp:Button ID="btnAmend" runat="server" CommandArgument='<%# Eval("ApplianceId") %>'
                                    Text="Amend" OnClick="btnAmend_Click" BackColor="White" />
                                   
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                    <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" BorderColor="Black"
                        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle HorizontalAlign="Left" />
                    <SelectedRowStyle BackColor="#FFFFCC" Font-Bold="True" />
                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                    <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                    <SortedDescendingHeaderStyle BackColor="#242121" />
                    <SortedAscendingCellStyle BackColor="#F7F7F7"></SortedAscendingCellStyle>
                    <SortedAscendingHeaderStyle BackColor="#4B4B4B"></SortedAscendingHeaderStyle>
                    <SortedDescendingCellStyle BackColor="#E5E5E5"></SortedDescendingCellStyle>
                    <SortedDescendingHeaderStyle BackColor="#242121"></SortedDescendingHeaderStyle>
                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="5" />
                    <PagerStyle BackColor="White" />
                </cc1:PagingGridView>
                <asp:Label ID="Message" ForeColor="Red" runat="server" Visible="False" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:Panel ID="pnlAddAppliancePopup" runat="server" BackColor="White" Width="530px"
    Height="552px">
    <table id="pnlAddActivityTable" style="margin: 2px; border-color: Black !important">
        <tr>
            <td colspan="2" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px; padding-top: 6px;">
                    <asp:Label Text="Add New Appliance" runat="server" ID="lblNewAppliance" />
                </div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                <asp:Panel ID="pnlAddApplianceMessage" runat="server" Visible="false">
                    <asp:Label ID="lblAddApplianceMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Item:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtItem" Width="200" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Quantity:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtQuantity" Width="200" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Dimensions:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtDimensions" Width="200" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Location:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="autoComplete1" runat="server" EnableCaching="true"
                    BehaviorID="AutoCompleteEx" TargetControlID="txtLocation" ServicePath="~/Views/Property/PropertyRecord.aspx"
                    ServiceMethod="getApplianceLocations" CompletionInterval="1000" CompletionSetCount="1" MinimumPrefixLength="0"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="locationList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="LocationItemSelected">
                </asp:AutoCompleteExtender>
                <div id="locationList">
                </div>
                <%-- <asp:DropDownList ID="ddlLocation" runat="server">
                </asp:DropDownList>--%>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Make:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtMake" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true"
                    TargetControlID="txtMake" ServicePath="~/Views/Property/PropertyRecord.aspx"
                    ServiceMethod="getApplianceManufacturer" CompletionInterval="1000" CompletionSetCount="1" MinimumPrefixLength="0"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="makeList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="makeItemSelected">
                </asp:AutoCompleteExtender>
                <div id="makeList">
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Type:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtType" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" EnableCaching="true"
                    TargetControlID="txtType" ServicePath="~/Views/Property/PropertyRecord.aspx"
                    ServiceMethod="getApplianceType" CompletionInterval="1000" CompletionSetCount="1" MinimumPrefixLength="0"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="typeList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="typeItemSelected">
                </asp:AutoCompleteExtender>
                <div id="typeList">
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Model:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtModel" runat="server" MaxLength="50" Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" EnableCaching="true"
                    TargetControlID="txtModel" ServicePath="~/Views/Property/PropertyRecord.aspx"
                    ServiceMethod="getApplianceModel" CompletionInterval="1000" CompletionSetCount="1" MinimumPrefixLength="0"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="modelList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="modelItemSelected">
                </asp:AutoCompleteExtender>
                <div id="modelList">
                </div>
            </td>
        </tr>
        <%--Start - Changes by Aamir Wahed on 07 May 2013--%>
        <%--To Implement Additional Field in Add Appliance, 
            Seial Number and Gas Concil Number Field(s) --%>
        <tr>
            <td align="left" valign="top" width="120px" style='padding-left: 10px;'>
                Serial No:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="50" Width="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="140px" style="padding-left: 10px;">
                Life Span:
            </td>
            <td align="left" valign="top">
                <asp:DropDownList runat="server" ID="ddlLifeSpan" Width="200" Style='float: left;'>
                </asp:DropDownList>
                <p style='float: left; margin: 0px; padding-left: 3px;'>
                    Year(s)</p>
            </td>
        </tr>
        <%--End - Changes by Aamir Wahed on 07 May 2013--%>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Purchase Cost(£):
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtPurchaseCost" Width="200" onkeyup="NumberOnly(this)" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Date Purchased:
            </td>
            <td align="left" valign="top">
                <ajaxToolkit:CalendarExtender ID="calInstalledDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtDatePurchased" PopupButtonID="imgCalInstalledDate" PopupPosition="Right"
                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:TextBox ID="txtDatePurchased" runat="server" Width="200"></asp:TextBox>&nbsp;<img
                    alt="Open Calendar" src="../../Images/calendar.png" id="imgCalInstalledDate" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Removed Date:
            </td>
            <td align="left" valign="top">
                <ajaxToolkit:CalendarExtender ID="CalRemovedDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtRemovedDate" PopupButtonID="imgCalRemovedDate" PopupPosition="Right"
                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:TextBox ID="txtRemovedDate" runat="server" Width="200"></asp:TextBox>&nbsp;<img
                    alt="Open Calendar" src="../../Images/calendar.png" id="imgCalRemovedDate" />
            </td>
        </tr>
        <%--Start - Changes by Aamir Wahed on 07 May 2013--%>
        <%--To Implement Additional Field in Add Appliance, Replacement Due Field --%>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Notes:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Width="200" />
            </td>
        </tr>
        <%--End - Changes by Aamir Wahed on 07 May 2013--%>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="White" />
                &nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" />
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpAddAppliance" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAppliance" PopupControlID="pnlAddAppliancePopup"
    DropShadow="true" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Label ID="lblDispAppliance" runat="server"></asp:Label>
<asp:Panel ID="pnlDeleteAppliancePopup" runat="server" BackColor="White" Width="300px"
    CssClass="panelPopup">
    <div style="float: left; padding-left: 10px; padding-top: 6px; padding-right: 10px;
        margin-bottom: 10px;">
        <asp:Label Text="Alert!" runat="server" Font-Bold="true" ID="Label1" />
        <div style="clear: both">
        </div>
        <hr />
        <asp:Panel ID="pnlDeleteApplianceMessage" runat="server" Visible="false">
            <br />
            <asp:Label ID="lblDeleteApplianceMessage" runat="server" Text=""></asp:Label>
            <br />
            <br />
        </asp:Panel>
        <asp:Label ID="Label2" runat="server" Text="You have selected to delete the appliance. All historical information will be deleted. Are you happy to proceed?"></asp:Label>
        <br />
        <br />
        <div style="width: 100%;">
            <asp:Button ID="btnYes" runat="server" Text="Yes" Width="100" Style="float: left"
                UseSubmitBehavior="false" />
            <asp:Button ID="btnNo" runat="server" Text="No" Width="100" Style="float: right"
                UseSubmitBehavior="false" />
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpDeleteAppliance" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDeleteAppliance" PopupControlID="pnlDeleteAppliancePopup"
    DropShadow="true" CancelControlID="btnNo" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Label ID="lblDeleteAppliance" runat="server"></asp:Label>
