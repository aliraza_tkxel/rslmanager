﻿Imports AS_Utilities
Imports AS_BusinessObject
Imports AS_BusinessLogic
Imports System.Reflection
Imports System.Globalization

Public Class Accommodation
    Inherits UserControlBase
#Region "Properties"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
#End Region
#Region "Events"

#End Region

#Region "Functions"

#Region "Populate Rooms Dimensions"
    ''' <summary>
    ''' This function is used to load property Rooms Dimensions on demands only from parent control.
    ''' </summary>
    ''' <param name="propertyId">PropertyId is optional, if not provided the function will check for propertyId in query string with key "Id"</param>
    ''' <remarks></remarks>
    Public Sub populateAccomudationDetails(Optional ByVal propertyId As String = Nothing)
        If propertyId Is Nothing Then
            propertyId = getPropertyIdfromQueryString()
        End If

        roomsDimensionsSection.populateRoomsDimensions(propertyId)
        summarySection.populateAttributeSummary()
    End Sub
#End Region

#Region "Get Property Id from Query String"
    ''' <summary>
    ''' Get propertyId from query string, using the key stored in pathconstants (PathConstants.PropertyIds = "id")
    ''' </summary>
    ''' <param name="propertyId">Optional propertyId by ref to store propertyid.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getPropertyIdfromQueryString(Optional ByRef propertyId As String = Nothing) As String
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            propertyId = CType(Request.QueryString(PathConstants.PropertyIds), String)
        End If
        Return propertyId
    End Function
#End Region


#Region "Get Property Rent Type and Financial Information"
    Sub populateRentTypeAndFinancialInformation()
        Dim propertyId As String = String.Empty
        getPropertyIdfromQueryString(propertyId)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getRentTypeAndFinancialInfomration(resultDataSet, propertyId)
        
        'Set Rent Dates
        txtRentDate.Text = ConfigurationManager.AppSettings("RentStartDate").ToString() + Now.Year().ToString()
        txtEffectiveDate.Text = ConfigurationManager.AppSettings("RentEndDate").ToString() + Now.Year().ToString()
        'Populating Current Rent Infomraiton
        If Not IsNothing(propertyId) And resultDataSet.Tables("RentFinancial").Rows.Count > 0 Then
            txtRentDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("DATERENTSET")
            txtEffectiveDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("RENTEFFECTIVE")
            lblRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("RENT"))
            lblServices.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SERVICES"))
            lblCouncilTax.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("COUNCILTAX"))
            lblWater.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("WATERRATES"))
            lblInelig.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("INELIGSERV"))
            lblSupp.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SUPPORTEDSERVICES"))
            lblGarage.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("GARAGE"))
            txtTotalRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("TOTALRENT"))
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("RENTTYPE"))) Then
                Dim iResult = (From ps In resultDataSet.Tables("RentType") Where ps.Item("TENANCYTYPEID") = resultDataSet.Tables("RentFinancial").Rows(0)("RENTTYPE") Select ps)
                If iResult.Count > 0 Then
                    lblRentType.Text = iResult.First.Item("DESCRIPTION")

                End If
            End If

           End If
    End Sub

#End Region
#End Region

End Class