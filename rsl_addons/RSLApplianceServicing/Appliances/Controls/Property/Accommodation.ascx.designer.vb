﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Accommodation

    '''<summary>
    '''roomsDimensionsSection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents roomsDimensionsSection As Global.Appliances.RoomsDimensions

    '''<summary>
    '''pnlCurrentRentTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCurrentRentTab As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtRentDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRentDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEffectiveDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEffectiveDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblRentType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRentType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblServices control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblServices As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCouncilTax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouncilTax As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWater control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWater As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInelig control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInelig As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSupp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblGarage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGarage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtTotalRent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalRent As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''summarySection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents summarySection As Global.Appliances.AttributeSummary
End Class
