﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Attributes.ascx.vb"
    Inherits="Appliances.Attributes" %>
<%@ Register TagPrefix="ItemDetail" TagName="ItemDetailControl" Src="~/Controls/Property/ItemDetail.ascx" %>
<%@ Register TagPrefix="prop" TagName="ConditionRatingList" Src="~/Controls/Property/ConditionRatingList.ascx" %>
<script type="text/javascript" charset="utf-8">

</script>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updPnlAttributes" runat="server">
    <ContentTemplate>
        <div style="float: left; width: 25%; border: 1px solid; height: 603px; overflow: scroll;">
            <div class="mainheading-panel" style="margin-top: 5px; margin-left: 5px; width: 94%;">
                Attributes
            </div>
            <asp:Panel ID="pnlTreeView" runat="server">
                <asp:TreeView runat="server" ID="trVwAttributes" ShowLines="true" ExpandDepth="1"
                    ForeColor="Black" PopulateNodesFromClient = "false" SelectedNodeStyle-BackColor="#6dcff7" PathSeparator="&gt;"
                    LineImagesFolder="~/Images/TreeLineImages">
                    <Nodes>
                        <asp:TreeNode PopulateOnDemand="true" Text="Menu"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
            </asp:Panel>
        </div>
        <div style="float: left; width: 74%; border: 1px solid; min-height: 603px;">
            <div style="margin-top: 10px;">
                <asp:Label runat="server" Text="" ID="lblBreadCrump" Style="margin-left: 15px;"></asp:Label></div>
            <asp:Panel runat="server" ID="pnlConditionRatingList" Style="margin: 2px;">
                <prop:ConditionRatingList runat="server" ID="ucConditionRatingList" />
            </asp:Panel>
            <ItemDetail:ItemDetailControl runat="server" ID="itemDetailControl" Visible="false" />
            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
            <asp:Button Text="" runat="server" ID="btnHeatingFuel" Style="display: none;"  />
              
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
