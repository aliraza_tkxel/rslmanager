﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ValuationTabs.ascx.vb"
    Inherits="Appliances.ValuationTabs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<div>
<asp:Panel ID="ValuationPanel" runat="server">
    <asp:UpdatePanel ID="updPnlRent" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlRentMessage" runat="server" Visible="false">
                <asp:Label ID="lblRentMessage" runat="server"></asp:Label>
            </asp:Panel>
            <strong></strong>
            <div style="border: 1px solid; padding: 7px; width: 23%; margin-left: 10px; min-width: 300px;
                float: left">
                <table width="100%" style="height: 150px;">
                    <tr>
                        <td>
                            <b>Funding</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nominating Body:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNominatingBody" runat="server" Width="190px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Funding Authority:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFundingAuthority" runat="server" class="selectval" Width="195px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Charge:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCharge" runat="server" class="selectval" Width="195px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Charge Value:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtChargeValue" runat="server" Text="0.00" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgCalendar" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtChargeValueDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                                <asp:TextBox ID="txtChargeValueDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                            </div>
                            <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtChargeValue"
                                Operator="DataTypeCheck" Type="Currency" ErrorMessage="Only digits allowed" Style="float: right;"
                                CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                            </asp:CompareValidator>
                            <asp:CompareValidator runat="server" ID="CompareValidator17" ControlToValidate="txtChargeValueDate"
                                Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                            </asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="border: 1px solid; padding: 7px; width: 23%; margin-left: 10px; min-width: 300px;
                float: left">
                <table width="100%" style="height: 150px;">
                    <tr>
                        <td colspan="2">
                            <b>Capital Valuation</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            MV-T:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtOMVST" OnTextChanged="txtOMVST_TextChanged" AutoPostBack="True" runat="server" Text="0" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgOMVST" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtOMVSTDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgOMVST" />
                                <asp:TextBox ID="txtOMVSTDate"  runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regexpNumber" runat="server"     
                                    ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate"
                                    ControlToValidate="txtOMVST"     
                                    ValidationExpression="^[0-9]+(,[0-9]+)*$" />
                                <%--<asp:CompareValidator runat="server" ID="CompareValidator12" ControlToValidate="txtOMVST"
                                    Type="Currency"  ErrorMessage="Only digits allowed" Style="float: right;" CssClass="Required"
                                    Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>--%>
                                <asp:CompareValidator runat="server" ID="CompareValidator16" ControlToValidate="txtOMVSTDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            EUV-SH:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtEUV" runat="server" OnTextChanged="txtEUV_TextChanged" AutoPostBack="True" Text="0" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgEUV" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEUVDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgEUV" />
                                <asp:TextBox ID="txtEUVDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"     
                                    ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate"
                                    ControlToValidate="txtEUV"     
                                    ValidationExpression="^[0-9]+(,[0-9]+)*$" />
                                <%--<asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtEUV"
                                    Type="Double" ErrorMessage="Only digits allowed" Style="float: right;" CssClass="Required"
                                    Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>--%>
                                <asp:CompareValidator runat="server" ID="CompareValidator15" ControlToValidate="txtEUVDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Insurance Value:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtInsurancValue" runat="server" OnTextChanged="txtInsurancValue_TextChanged" AutoPostBack="True" Text="0" Style="float: left;
                                    width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgInsurancValue" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtInsurancValueDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgInsurancValue" />
                                <asp:TextBox ID="txtInsurancValueDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"     
                                    ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate"
                                    ControlToValidate="txtInsurancValue"     
                                    ValidationExpression="^[0-9]+(,[0-9]+)*$" />
                                <%--<asp:CompareValidator runat="server" ID="CompareValidator3" ControlToValidate="txtInsurancValue"
                                    Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>--%>
                                <asp:CompareValidator runat="server" ID="CompareValidator14" ControlToValidate="txtInsurancValueDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            MV-VP:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtOMV" runat="server" OnTextChanged="txtOMV_TextChanged" AutoPostBack="True" Text="0" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgOMV" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtOMVDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgOMV" />
                                <asp:TextBox ID="txtOMVDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"     
                                    ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate"
                                    ControlToValidate="txtOMV"     
                                    ValidationExpression="^[0-9]+(,[0-9]+)*$" />
                               <%-- <asp:CompareValidator runat="server" ID="CompareValidator4" ControlToValidate="txtOMV"
                                    Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>--%>
                                <asp:CompareValidator runat="server" ID="CompareValidator13" ControlToValidate="txtOMVDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            EUV-SH Yield (%):
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtYield" runat="server" Text="0.00" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgYield" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtYieldDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgYield" />
                                <asp:TextBox ID="txtYieldDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="CompareValidator18" ControlToValidate="txtYield"
                                    Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                                <asp:CompareValidator runat="server" ID="CompareValidator11" ControlToValidate="txtYieldDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="rentalValues" style="border: 1px solid; padding: 7px; width: 23%; margin-left: 10px;
                min-width: 300px; float: left">
                <table width="100%" style="height: 130px;">
                    <tr>
                        <td>
                            <b>Rental Values</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Market Rent:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtMarketRent" runat="server" Text="0.00" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgMarketRent" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txtMarketRentDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgMarketRent" />
                                <asp:TextBox ID="txtMarketRentDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="CompareValidator5" ControlToValidate="txtMarketRent"
                                    Operator="DataTypeCheck" Type="Currency" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                                <asp:CompareValidator runat="server" ID="CompareValidator10" ControlToValidate="txtMarketRentDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Affordable Rent:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtAffordableRent" runat="server" Text="0.00" Style="float: left;
                                    width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgAffordableRent" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtAffordableRentDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgAffordableRent" />
                                <asp:TextBox ID="txtAffordableRentDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="CompareValidator6" ControlToValidate="txtAffordableRent"
                                    Operator="DataTypeCheck" Type="Currency" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                                <asp:CompareValidator runat="server" ID="CompareValidator9" ControlToValidate="txtAffordableRentDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Social Rent:
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:TextBox ID="txtSocialRent" runat="server" Text="0.00" Style="float: left; width: 80px;"></asp:TextBox>
                                <asp:Image ID="imgSocialRent" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right;" Width="16px" />
                                <asp:CalendarExtender ID="CalendarExtender8" runat="server" TargetControlID="txtSocialRentDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgSocialRent" />
                                <asp:TextBox ID="txtSocialRentDate" runat="server" OnClick="this.value=''" Style="margin-left: 3px;
                                    float: left; width: 80px; margin-right: 3px;"></asp:TextBox>
                                <asp:CompareValidator runat="server" ID="CompareValidator7" ControlToValidate="txtSocialRent"
                                    Operator="DataTypeCheck" Type="Currency" ErrorMessage="Only digits allowed" Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                                <asp:CompareValidator runat="server" ID="CompareValidator8" ControlToValidate="txtSocialRentDate"
                                    Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                                    CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                                </asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- End here  -->
                <div class="frow">
                    &nbsp;<asp:Button ID="btnSavePropertyRent" runat="server" Text="Save" ValidationGroup="validate" />
                    &nbsp;<div class="clear">
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>
</div>
<div style="float: left; width: 976px; margin-left: 10px; margin-top: 10px; border: 1px solid Black;">
    <asp:UpdatePanel ID="upPnlRenthistory" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCurrentRentMessage" runat="server" Visible="false" Height="19px"
                Width="289px">
                <asp:Label ID="lblCurrentRentTab" runat="server" Text=""></asp:Label>
            </asp:Panel>
            <br />
            <b>&nbsp;Capital Value History:</b>
            <asp:Label style="margin-left:470px;" runat="server" ><b>Rental Values History:</b></asp:Label>
            <div style="height: 282px; margin-top: 10px;">
                <cc1:PagingGridView ID="grdValuationHistory" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="false" BorderStyle="None" CellPadding="2" GridLines="None"
                    OrderBy="" PageSize="7" Width="100%" PagerSettings-Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="No Record Found.">
                    <Columns>
                        <asp:BoundField HeaderText="Date:" SortExpression="Updated" DataField="Updated" />
                        <asp:TemplateField HeaderText="MV-T:">
                            <ItemTemplate>
                                <asp:Label ID="lblOMVST" runat="server" Text='<%# Eval("OMVST", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblOMVSTDate" runat="server" Text='<%# Bind("OMVSTDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EUV-SH:">
                            <ItemTemplate>
                                <asp:Label ID="lblEUV" runat="server" Text='<%# Eval("EUV", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblEUVDate" runat="server" Text='<%# Bind("EUVDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Insurance:">
                            <ItemTemplate>
                                <asp:Label ID="lblINSURANCEVALUE" runat="server" Text='<%# Eval("INSURANCEVALUE", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblInsuranceValuedate" runat="server" Text='<%# Bind("InsuranceValuedate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MV-VP:">
                            <ItemTemplate>
                                <asp:Label ID="lblOMV" runat="server" Text='<%# Eval("OMV", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblOMVdate" runat="server" Text='<%# Bind("OMVdate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EUV-SH Yield (%):">
                            <ItemTemplate>
                                <asp:Label ID="lblYIELD" runat="server" Text='<%# Eval("YIELD", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblYIELDdate" runat="server" Text='<%# Bind("YieldDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Market Rent:">
                            <ItemTemplate>
                                <asp:Label ID="lblMarketRent" runat="server" Text='<%# Eval("MarketRent", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblMarketRentDate" runat="server" Text='<%# Bind("MarketRentDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Affordable Rent:">
                            <ItemTemplate>
                                <asp:Label ID="lblAffordableRent" runat="server" Text='<%# Eval("AffordableRent", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblAffordableRentDate" runat="server" Text='<%# Bind("AffordableRentDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Social Rent:">
                            <ItemTemplate>
                                <asp:Label ID="lblSocialRent" runat="server" Text='<%# Eval("SocialRent", "{0:n2}") %>'></asp:Label>
                                <asp:Label ID="lblSocialRentDate" runat="server" Text='<%# Bind("SocialRentDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgCurrentRentHistoryArrow" runat="server" OnClick="imgCurrentRentHistoryArrow_Click"
                                    BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("SID").ToString() %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                    <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                    <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                </cc1:PagingGridView>
            </div>
            <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                background-color: #FAFAD2; border-color: #ADADAD; border-width: 2px; border-style: none;
                vertical-align: middle; border-top: 0px; border-top-color: transparent;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td align="left" style="width: 70%;">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="<<" runat="server" CommandName="Page"
                                                    CommandArgument="First" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="<" runat="server" CommandName="Page" CommandArgument="Prev" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                Page:
                                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                of
                                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                to
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                of
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text=">" runat="server" CommandName="Page" CommandArgument="Next" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text=">>" runat="server" CommandName="Page"
                                                    CommandArgument="Last" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td align="center" style="width: 30%; text-align: center;">
                                <asp:RangeValidator Display="Dynamic" ID="rangevalidatorPageNumber" runat="server"
                                    ErrorMessage="Enter a page between 1 and " ValidationGroup="pageNumber" MinimumValue="1"
                                    MaximumValue="1" ControlToValidate="txtPageNumber" Type="Integer" SetFocusOnError="True"
                                    CssClass="Required" />
                                <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                    ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updPanelCurrentRentHistory" runat="server">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlCurrentRentHistory" Style="height: 300px; width: 900px;
            background-color: White; border: 1px solid gray; padding: 10px; top: 0px;">
            <div style="background-color: #E8E9EA; color: Black; border: solid 1px gray; height: 30px;
                padding: 8px; text-align: left;">
                <table id="tbl" style="width: 100%">
                    <tr>
                        <td style="font-size: 13px; font-weight: bold; width: 50%;">
                            Details of Changes
                        </td>
                        <td align="right" style="width: 10%;">
                            Amended:&nbsp;
                        </td>
                        <td style="width: 40%;">
                            <asp:Label ID="lblAmended" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            By:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblBy" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:ImageButton ID="imgbtnAddAppointmentCancelCalendar" runat="server" ImageAlign="Right"
                ImageUrl="~/Images/cross2.png" Style="position: absolute; top: -10px; right: -10px"
                BorderStyle="None" />
            <div style="height: 100%; width: 100%;">
                <table id="RentHistoryPopup" style="width: 100%;">
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td style="width: 25%">
                        </td>
                        <td style="width: 15%">
                        </td>
                        <td style="width: 25%">
                        </td>
                        <td style="width: 15%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>Previously:</b>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>As At:</b>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>Changed To:</b>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>As At:</b>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px; width: 10%;">
                            Nominating Body
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblPreviousNominatingBody" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedNominatingBody" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Funding Authority
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblPreviousFundingAuthority" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedFundingAuthority" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Charge
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousCharge" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedCharge" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Charge Value
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousChargeValue" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousChargeValueDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedChargeValue" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedChargeValueDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td valign="top">
                            MV-T
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousOmvSt" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousOmvStDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedOmvSt" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedOmvStDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding-left: 2px;">
                            EUV-SH
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousEUV" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousEUVDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedEUV" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedEUVDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td valign="top">
                            Insurance Value
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousInsuranceValue" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousInsuranceValueDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedInsuranceValue" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedInsuranceValueDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            MV-VP
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousOmv" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousOmvDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedOmv" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedOmvDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" class="" valign="top">
                            Yield (%)
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousYield" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousYieldDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedYield" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedYieldDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Market Rent
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousMarketRent" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousMarketRentDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedMarketRent" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedMarketRentDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Affordable Rent
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousAffordableRent" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousAffordableRentDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedAffordableRent" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedAffordableRentDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Social Rent
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousSocialRent" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblPreviousSocialRentDate" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedSocialRent" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="text-align: center;" align="center">
                            <asp:Label ID="lblChangedSocialRentDate" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Label ID="lblCurrentRentHistory" runat="server"></asp:Label>
        <asp:ModalPopupExtender ID="mdlCurrentRentHistoryPopUp" runat="server" TargetControlID="lblCurrentRentHistory"
            PopupControlID="pnlCurrentRentHistory" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
            CancelControlID="imgbtnAddAppointmentCancelCalendar">
            <Animations>    
        <OnShown>
            <ScriptAction Script="createFixedHeader();" />
        </OnShown>
            </Animations>
        </asp:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
