﻿
Option Explicit On

Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.IO
Imports System.Linq
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class DocumentTab
    Inherits UserControlBase

#Region "Properties"

    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "DocumentId", 1, 20)
    Dim totalCount As Integer = 0


#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.Form.Attributes.Add("enctype", "multipart/form-data")
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        End If
        If Not IsPostBack Then
            Me.BindEpcCategoryTypes()
            Me.BindCategories()
            Me.BindDocumentTypes()
            Me.BindDocumentSubTypes(Me.ddlType.SelectedValue)
            bindToGrid(objPropertyDocumentBO)
        ElseIf ddlType.SelectedItem.ToString = "EPC" And ddlSubtype.SelectedItem.ToString = "Certificate" Then
            CategoryPnl.Visible = True
            CP12NumberPnl.Visible = False
        ElseIf ddlType.SelectedItem.ToString = "CP12" And ddlSubtype.SelectedItem.ToString = "Certificate" Then
            CP12NumberPnl.Visible = True
            CategoryPnl.Visible = False
        Else
            CategoryPnl.Visible = False
            CP12NumberPnl.Visible = False
        End If
    End Sub

#End Region


#Region "Button Delete Document Click"

    Protected Sub btnDeleteDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnDeleteDocument As Button = DirectCast(sender, Button)
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        Dim objPropertyBL As PropertyBL = New PropertyBL()

        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        End If

        Integer.TryParse(btnDeleteDocument.CommandArgument, objPropertyDocumentBO.DocumentId)

        If objPropertyDocumentBO.DocumentId > 0 Then
            objPropertyBL.deleteDocumentByIdAndGetPath(objPropertyDocumentBO)

            Dim fileDirectoryPath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + objPropertyDocumentBO.PropertyId + "/Documents/")

            If (Directory.Exists(fileDirectoryPath) = True) Then
                If (File.Exists(fileDirectoryPath + objPropertyDocumentBO.DocumentPath)) Then
                    File.Delete(fileDirectoryPath + objPropertyDocumentBO.DocumentPath)
                End If
            End If
        End If

        bindToGrid(objPropertyDocumentBO)

    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSavePropertyDocuments.Click
        Try
            If validateForm() Then
                propertyDocumentUpload()
                resetControls()

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                pnlMessage.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If




        End Try


    End Sub



    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearchPropertyDocuments.Click
        Try
            'If validateSearchForm() Then
            propertyDocumentSearch()
            'resetControls()

            'End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                pnlMessage.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If




        End Try


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        resetControls()
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
            If Not IsNothing(SessionManager.getPropertyId()) Then
                objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
            End If
            bindToGrid(objPropertyDocumentBO)

        Catch ex As Exception

        Finally

        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
                If Not IsNothing(SessionManager.getPropertyId()) Then
                    objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
                End If
                bindToGrid(objPropertyDocumentBO)
            End If
        Catch ex As Exception

        Finally

        End Try
    End Sub

#End Region

#End Region

#Region "Grid Activities Row Data bound"

    Protected Sub grdDocumentInfo_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdDocumentInfo.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim btnDeleteDocument As Button = DirectCast(e.Row.FindControl("btnDeleteDocument"), Button)
                Dim pageName = System.IO.Path.GetFileName(Request.Url.ToString())
                If pageName.Contains(PathConstants.PropertyRecordPage) Then
                    btnDeleteDocument.Visible = False
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region






#Region "Bind To Grid"

    Sub bindToGrid(ByVal objPropertyDocumentBO As PropertyDocumentBO, Optional ByVal isSerachClick As Boolean = False)
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPageSortBo = getPageSortBoViewState()
        'resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)

        totalCount = objPropertyBL.getPropertyDocuments(resultDataSet, objPropertyDocumentBO, objPageSortBo, isSerachClick)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        grdDocumentInfo.VirtualItemCount = totalCount
        grdDocumentInfo.DataSource = resultDataSet
        grdDocumentInfo.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)


        'If grdDocumentInfo.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSubcontractorList.PageCount Then
        'setVirtualItemCountViewState(totalCount)
        'setResultDataSetViewState(resultDataSet)
        'End If

    End Sub
    Sub BindCategories()
        Dim objDocumentTypeBL As DocumentsBL = New DocumentsBL()
        Dim resultDataSet As DataSet = New DataSet()

        objDocumentTypeBL.getCategories(resultDataSet, "Property")

        ddlDocumentCategory.DataSource = resultDataSet.Tables(0)
        ddlDocumentCategory.DataValueField = "categorytypeid"
        ddlDocumentCategory.DataTextField = "title"
        ddlDocumentCategory.DataBind()
    End Sub
    Sub BindDocumentTypes()

        Dim objDocumentTypeBL As DocumentsBL = New DocumentsBL()
        Dim resultDataSet As DataSet = New DataSet()

        objDocumentTypeBL.getDocumentTypesList(resultDataSet, True, "Property", Convert.ToInt32(ddlDocumentCategory.SelectedItem.Value))

        ddlType.DataSource = resultDataSet.Tables(0)
        ddlType.DataValueField = "DocumentTypeId"
        ddlType.DataTextField = "Title"
        ddlType.DataBind()

    End Sub

    Sub BindDocumentSubTypes(ByVal DocumentTypeId As Int16)

        Dim objDocumentTypeBL As DocumentsBL = New DocumentsBL()
        Dim resultDataSet As DataSet = New DataSet()

        objDocumentTypeBL.getDocumentSubtypesList(resultDataSet, DocumentTypeId, "Property")

        ddlSubtype.DataSource = resultDataSet.Tables(0)
        ddlSubtype.DataValueField = "DocumentSubtypeId"
        ddlSubtype.DataTextField = "Title"
        ddlSubtype.DataBind()

    End Sub

    Sub BindEpcCategoryTypes()

        Dim objDocumentTypeBL As DocumentsBL = New DocumentsBL()
        Dim resultDataSet As DataSet = New DataSet()

        objDocumentTypeBL.EpcCategoryTypeList(resultDataSet)

        ddlCategory.DataSource = resultDataSet.Tables(0)
        ddlCategory.DataValueField = "CategoryTypeId"
        ddlCategory.DataTextField = "CategoryType"
        ddlCategory.DataBind()

    End Sub

#End Region

#Region "Get Document Information"

    Sub getUploadedDocumentInfo()

    End Sub

#End Region

#Region "Convert  CP12 Document To binary stream"
    Public Function ConvertToStream(cp12DocumentPath As String)
        Dim cp12InByteForm As Byte()
        Using stream = New FileStream(cp12DocumentPath, FileMode.Open, FileAccess.Read)
            Using reader = New BinaryReader(stream)
                cp12InByteForm = reader.ReadBytes(CInt(stream.Length))
            End Using
        End Using
        ConvertToStream = cp12InByteForm
    End Function

#End Region
#Region "Save Property Document Information"
    Sub propertyDocumentUpload()
        'Dim cp12InByteForm As Byte()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        End If
        Try
            objPropertyDocumentBO.DocumentCategory = ddlDocumentCategory.SelectedItem.Text
            objPropertyDocumentBO.DocumentTypeId = ddlType.SelectedItem.Value
            objPropertyDocumentBO.DocumentSubTypeId = ddlSubtype.SelectedItem.Value
            objPropertyDocumentBO.Keyword = txtKeyword.Text
            objPropertyDocumentBO.UploadedBy = SessionManager.getUserEmployeeId()
            objPropertyDocumentBO.ExpiryDate = txtExpires.Text
            objPropertyDocumentBO.DocumentDate = txtDocumentDate.Text
            objPropertyDocumentBO.EpcRating = ddlCategory.SelectedItem.Value
            objPropertyDocumentBO.CP12Number = txtCp12Number.Text
	    Dim fileDirectoryPath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + objPropertyDocumentBO.PropertyId + "/Documents/")
            If (Directory.Exists(fileDirectoryPath) = False) Then
                Directory.CreateDirectory(fileDirectoryPath)
            End If
            objPropertyDocumentBO.DocumentName = lblFileName.Text
            objPropertyDocumentBO.DocumentPath = fileDirectoryPath
            If Not objPropertyDocumentBO.CP12Number = "" Or Not IsNothing(objPropertyDocumentBO.CP12Number) Then
                objPropertyDocumentBO.CP12Dcoument = SessionManager.getCP12Dcoument()
            End If
            objPropertyBL.savePropertyDocumentUpload(objPropertyDocumentBO)
        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)
        End Try
        bindToGrid(objPropertyDocumentBO)
        lblFileName.Text = String.Empty
    End Sub
#End Region


#Region "Search Property Document Information"
    Sub propertyDocumentSearch()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
        End If
        Try
            objPropertyDocumentBO.DocumentCategory = ddlDocumentCategory.SelectedValue
            objPropertyDocumentBO.DocumentTypeId = ddlType.SelectedItem.Value
            objPropertyDocumentBO.Keyword = txtKeyword.Text

            objPropertyDocumentBO.ExpiryDate = txtExpires.Text
            objPropertyDocumentBO.DocumentDate = txtDocumentDate.Text
            bindToGrid(objPropertyDocumentBO, True)
            lblFileName.Text = String.Empty
        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region "Reset Controls"

    Protected Sub resetControls()
        ddlSubtype.SelectedIndex = 0
        ddlType.SelectedIndex = 0
        ddlCategory.SelectedIndex = 0
        txtDocumentDate.Text = String.Empty
        txtExpires.Text = String.Empty
        txtKeyword.Text = String.Empty
        ddlSubtype.Enabled = False
        txtCp12Number.Text = String.Empty
    End Sub

#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region


    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlType.SelectedIndexChanged

        Dim documentTypeId As Integer = Convert.ToInt32(ddlType.SelectedValue)
        If documentTypeId > -1 Then
            ddlSubtype.Enabled = True
        End If

        BindDocumentSubTypes(documentTypeId)

    End Sub

#Region " Validate Form "

    Private Function validateForm() As Boolean

        If Convert.ToInt32(Me.ddlType.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTypeValidationError, True)
            Return False
        End If

        If Convert.ToInt32(Me.ddlSubtype.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTitleValidationError, True)
            Return False
        End If

        If Convert.ToInt32(Me.ddlCategory.SelectedValue) = -1 And CategoryPnl.Visible = True Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectCategoryValidationError, True)
            Return False
        End If

        If txtCp12Number.Text = "" And CP12NumberPnl.Visible = True Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.Cp12ValidationError, True)
            Return False
        End If

        If txtDocumentDate.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDocumentDateValidationError, True)
            Return False
        End If


        If txtExpires.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectExpiryDateValidationError, True)
            Return False
        End If


        If lblFileName.Text = String.Empty Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAFile, True)
            Return False
        End If

        'If (System.IO.Path.GetExtension(flUploadDoc.FileName) <> ".pdf") Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAPDF, True)
        '    Return False
        'End If

        Return True

    End Function

#End Region

#Region " Validate Form "

    Private Function validateSearchForm() As Boolean

        If Convert.ToInt32(Me.ddlType.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTypeValidationError, True)
            Return False
        End If

        If Convert.ToInt32(Me.ddlCategory.SelectedValue) = -1 And CategoryPnl.Visible = True Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectCategoryValidationError, True)
            Return False
        End If

        If txtCp12Number.Text = "" And CP12NumberPnl.Visible = True Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.Cp12ValidationError, True)
            Return False
        End If

        If txtDocumentDate.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectDocumentDateValidationError, True)
            Return False
        End If


        If txtExpires.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectExpiryDateValidationError, True)
            Return False
        End If

        Return True

    End Function

#End Region

#Region "ckBoxPhotoUpload Checked Changed"
    ''' <summary>
    ''' ckBoxPhotoUpload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxComplianceDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxComplianceDocumentUpload.CheckedChanged
        Me.ckBoxComplianceDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                lblFileName.Text = SessionManager.getDocumentUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

            SessionManager.removePhotoUploadName()
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

    Protected Sub ddlDocumentCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDocumentCategory.SelectedIndexChanged
        BindDocumentTypes()
    End Sub
End Class