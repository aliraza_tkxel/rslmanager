﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AsbestosTab.ascx.vb"
    Inherits="Appliances.AsbestosTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .style2
    {
        width: 135px;
        text-align: left;
        padding-left: 5px;
    }
    .style3
    {
        width: 160px;
    }
</style>
<asp:Panel ID="pnlAddPhotoPopup" runat="server" BackColor="White" Width="100%">
    <div style="float: left; width: 39%; margin-left: 2px;">
        <div style="float: left; width: 96%; margin-left: 2px; margin-bottom: 5px; border: 1px solid #000;
            padding: 5px;">
            <asp:UpdatePanel ID="upPnlAsbestos" runat="server">
                <ContentTemplate>
                    <table id="pnlAddPhotoTable">
                        <tr>
                            <td align="left" valign="top" colspan="2" width="30%">
                                <div style='margin-left: 15px;'>
                                    <asp:Panel ID="pnlAddAsbestosMessage" runat="server" Visible="false">
                                        <asp:Label ID="lblAddAsbestosMessage" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <asp:HiddenField ID="hdnHiddenId" runat="server"></asp:HiddenField>
                            <td align="right" valign="top" class="style2">
                                Asbestos Type:<span class="Required"> *</span>
                            </td>
                            <td align="left" valign="top" class="style3">
                                <asp:DropDownList ID="ddlAsbestosLevel" runat="server" Width="168px">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="rfvddlAsbestosLevel" ControlToValidate="ddlAsbestosLevel"
                                    InitialValue="-1" ErrorMessage="Please select a Asbestos Type" Display="Dynamic"
                                    ValidationGroup="check" CssClass="Required" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Asbestos Elements:<span class="Required">*</span>
                            </td>
                            <td align="left" valign="top" class="style3">
                                <asp:DropDownList ID="ddlAsbestosElement" runat="server" AutoPostBack="false" Width="168px">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="efvddlAsbestosElement" ControlToValidate="ddlAsbestosElement"
                                    InitialValue="-1" ErrorMessage="Please select a Asbestos Elements" Display="Dynamic"
                                    ValidationGroup="check" CssClass="Required" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Type:<span class="Required"> *</span>
                            </td>
                            <td align="left" class="style3" valign="top">
                                <asp:DropDownList ID="ddlRisk" runat="server" AutoPostBack="false" Width="168px">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="rfvddlRisk" ControlToValidate="ddlRisk"
                                    InitialValue="-1" ErrorMessage="Please select a Type." Display="Dynamic" ValidationGroup="check"
                                    CssClass="Required" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Risk Level:<span class="Required"> *</span>
                            </td>
                            <td align="left" class="style3" valign="top">
                                <asp:DropDownList ID="ddlRiskLevel" runat="server" AutoPostBack="false" Width="168px">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="rfvddlRiskLevel" ControlToValidate="ddlRiskLevel"
                                    InitialValue="-1" ErrorMessage="Please select a Risk Level." Display="Dynamic"
                                    ValidationGroup="check" CssClass="Required" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Added:<span class="Required"> *</span>
                            </td>
                            <td align="left" class="style3" valign="top">
                                <asp:TextBox ID="txtAdded" runat="server" Enabled="false" Style="width: 168px; float: left;"></asp:TextBox>
                                <asp:CalendarExtender ID="txtAdded_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgAddedCalendar" TargetControlID="txtAdded" />
                                <asp:Image ID="imgAddedCalendar" runat="server" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: right; margin-left: 5px; float: left" />
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAdded"
                                    ErrorMessage="Required" ValidationGroup="check" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Removed:<span class="Required"></span>
                            </td>
                            <td align="left" valign="top" class="style3">
                                <asp:TextBox ID="txtRemoved" runat="server" Enabled="false" Style="width: 168px;
                                    float: left;"></asp:TextBox>
                                <asp:CalendarExtender ID="txtRemoved_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgCalendarRemoved" TargetControlID="txtRemoved" />
                                <asp:Image ID="imgCalendarRemoved" runat="server" ImageUrl="~/Images/Calendar-icon.png"
                                    Style="float: left; margin-left: 5px;" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Notes:<span class="Required"></span>
                            </td>
                            <td align="left" valign="top" class="style3">
                                <asp:TextBox ID="txtNotes" runat="server" Style="width: 168px;" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style2">
                                Urgent Action Required?:
                            </td>
                            <td align="left" valign="top" class="style3">
                                <asp:RadioButtonList runat="server" ID="rdBtnUrgentActionRequired" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Yes" Value="True" />
                                    <asp:ListItem Text="No" Value="False" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style2" valign="top">
                                &nbsp;
                            </td>
                            <td align="right" class="style3" style="text-align: right;" valign="top">
                                <asp:Button ID="btnAddAsbestos" runat="server" BackColor="White" Text="Add" ValidationGroup="check" />
                                &nbsp;
                                <asp:Button ID="btnAmend" runat="server" BackColor="White" Text="Amend" ValidationGroup="check" />&nbsp;
                                <asp:Button ID="btnReset" runat="server" BackColor="White" Text="Reset" ValidationGroup="Reset" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="float: left; width: 96%; margin-left: 2px; border: 1px solid #000; padding: 5px;">
            <asp:UpdatePanel ID="updPnlAttributes" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <%--<div class="title-bar">--%>
                    <div class="mainheading-panel" style="margin-top: 2px; margin-left: 5px; width: 94%;">
                        <asp:Label ID="addDocTitle" runat="server"> <strong>Add a Document</strong> </asp:Label>
                    </div>
                    <div class="add-form">
                        <div class="frow">
                            <div class="title">
                                Category:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlDocumentCategory" runat="server" class="selectval" CssClass="selectval"
                                    Enabled="false">
                                    <asp:ListItem Value="Compliance" Text="Compliance" Enabled="true" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="General" Text="General" Enabled="true"></asp:ListItem>
                                    <asp:ListItem Value="Legal" Text="Legal" Enabled="true"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Type:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlType" runat="server" class="selectval" CssClass="selectval">
                                    <asp:ListItem Value="3" Text="Asbestos" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow" id="div_TitleSelector" runat="server">
                            <div class="title">
                                <asp:Label ID="lblTitle" runat="server"> Title: </asp:Label>
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlSubtype" runat="server" class="selectval" CssClass="selectval">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Document:
                            </div>
                            <div class="input-area" style="padding-left: 0px !important">
                                <asp:TextBox runat="server" ID="txtDocumentDate" Style="width: 150px"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrDocumentDate" TargetControlID="txtDocumentDate"
                                    PopupButtonID="imgDocumentDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumentDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Expiry:
                            </div>
                            <div class="input-area" style="padding-left: 0px !important">
                                <asp:TextBox runat="server" ID="txtExpires" Style="width: 150px;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrExpires" TargetControlID="txtExpires"
                                    PopupButtonID="imgExpiresDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgExpiresDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow" id="div_FileUpload" runat="server">
                            <div class="title">
                                <asp:Label ID="lblFileUpload" runat="server"> Upload: </asp:Label>
                            </div>
                            <div class="input-area" style="padding-left: 2px !important">
                                <asp:Button ID="btnFileUpload" runat="server" Text="Upload" Width="60px" OnClientClick="openAsbestosUploadWindow('phototab')" />
                                <div style="display: block; width: 200px; word-wrap: break-word">
                                    <asp:Label runat="server" ID="lblFileName"></asp:Label>
                                    <asp:CheckBox ID="ckBoxAsbestosDocumentUpload" runat="server" AutoPostBack="True"
                                        Visible="true" CssClass="hiddenField" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Keywords:
                            </div>
                            <div class="input-area" style="padding-left: 0px !important">
                                <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="txtarea"
                                    MaxLength="255"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Reset" />
                            <asp:Button ID="btnSavePropertyDocuments" runat="server" Text="Save" />
                            &nbsp;<div class="clear">
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Panel>
    <div id="divAsbestosGrid" runat="server" style="float: left; width: 59%; margin-left: 5px;
        border: 1px solid #000; padding: 5px;">
        <asp:UpdatePanel ID="upPnlHealthAndSafetyDetail" runat="server">
            <ContentTemplate>
                <div style="float: left; width: 99%; margin-left: 5px;">
                    <cc1:PagingGridView ID="grdDocumentInfo" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                        CellSpacing="5" GridLines="None" OrderBy="" PageSize="10" Width="100%" PagerSettings-Visible="true"
                        ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Asbestos Type:" SortExpression="RiskLevel">
                                <ItemTemplate>
                                    <asp:Label ID="lblRiskLevel" runat="server" Text='<%# Bind("RiskLevel") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Element:" SortExpression="ElementDescription">
                                <ItemTemplate>
                                    <asp:Label ID="lblElementDescription" runat="server" Text='<%# Bind("ElementDescription") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type:" SortExpression="Risk">
                                <ItemTemplate>
                                    <asp:Label ID="lblRisk" runat="server" Text='<%# Bind("Risk") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Risk Level:" SortExpression="RiskLevel">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("Level") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Added:" SortExpression="RiskLevel">
                                <ItemTemplate>
                                    <asp:Label ID="lblDateAdded" runat="server" Text='<%# Bind("DateAdded") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Removed:" SortExpression="RiskLevel">
                                <ItemTemplate>
                                    <asp:Label ID="lblDateRemoved" runat="server" Text='<%# Bind("DateRemoved") %>'></asp:Label></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="By:" SortExpression="UploadedBy">
                                <ItemTemplate>
                                    <asp:Label ID="lblUploadedBy" runat="server" ToolTip='<%# Bind("UploadedByEmp") %>'
                                        Text='<%# Bind("UploadedBy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDocument" runat="server" Text="View" UseSubmitBehavior="False"
                                        OnClick="btnView_Click" CommandArgument='<%# Bind("ASBESTOSID") %>' /></ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="#EFF3FB" />
                        <EditRowStyle BackColor="#2461BF" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left" />
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" />
                        <AlternatingRowStyle BackColor="White" Wrap="True" />
                        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                    </cc1:PagingGridView>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        background-color: #FAFAD2; border-color: #ADADAD; border-width: 2px; border-style: none;
                        vertical-align: middle; border-top: 0px; border-top-color: transparent;">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                            CommandArgument="First" OnClick="lnkbtnPager_Click" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                                            CommandArgument="Prev" OnClick="lnkbtnPager_Click" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        Page:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                                        of
                                                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                        to
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                        of
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                            CommandArgument="Next" OnClick="lnkbtnPager_Click" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                            CommandArgument="Last" OnClick="lnkbtnPager_Click" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right">
                                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
