﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectImages.ascx.vb"
    Inherits="Appliances.DefectImages" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 350px;
    }
</style>
<script type="text/javascript">
    function imageUrl(path) {
        debugger;
        alert(path);
    }
</script>
<asp:Panel ID="Panel1" runat="server" Style='margin-left: 15px; padding: 10px;'>
    <b><u>Defect Detail Images</u> </b>
</asp:Panel>
<asp:Panel ID="pnlMessage" runat="server" Style='margin-left: 15px;'>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel runat="server" ID="updPnlPhotos">
    <ContentTemplate>
        <div style="margin: 0 auto; max-width: 77%; overflow-y: auto; max-height: 450px;
            overflow-x: hidden; padding: 10px;">
            <asp:ListView runat="server" ID="lstViewPhotos" GroupItemCount="2">
                <LayoutTemplate>
                    <table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
                        style="border-collapse: collapse; width: 100%;">
                        <tr id="groupPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server">
                        </td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <div class="itemPhoto">
                        <img src='<%= uploadedThumbUri %><%# Eval("ImageTitle") %>' alt='<%# Eval("ImageTitle") %>'
                            width="156px" height="120px" style="margin: 5px;" />
                        <div style="float: right; font-size: 10px;">
                            <asp:ImageButton ID="imgBtnShow" runat="server" CommandName="ShowImage" CommandArgument='<%# Eval("ImageTitle")%>'
                                ImageUrl="../../Images/zoom_in.png" BorderStyle="None" Style='margin-right: -1px;
                                margin-top: -2px;' />
                        </div>
                    </div>
                </ItemTemplate>
                <EmptyItemTemplate>
                </EmptyItemTemplate>
            </asp:ListView>
        </div>
        <div style="width: 100%; padding: 20px;">
            <asp:Button runat="server" ID="btnRefresh" Text="Refresh" Style="position: absolute;
                border-radius: 5px; bottom: 10px; right: 0; margin-right: 80px;" BackColor="White"
                UseSubmitBehavior="false" />
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" Style="position: absolute;
                border-radius: 5px; bottom: 10px; right: 0; margin-right: 15px;" BackColor="White"
                UseSubmitBehavior="false" />
        </div>
        <asp:Label runat="server" ID="lblDispSlideShow">   </asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label>
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpViewImage" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblPopUpShowImage" PopupControlID="pnlLargeImage"
    BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage">
    <asp:UpdatePanel runat="server" ID="upPnlLargePhoto">
        <ContentTemplate>
            <div style='width: 100%;'>
                <asp:ImageButton ID="btnClose" OnClick="HidePotoPanel" runat="server" Style="position: absolute; top: -17px;
                    right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
