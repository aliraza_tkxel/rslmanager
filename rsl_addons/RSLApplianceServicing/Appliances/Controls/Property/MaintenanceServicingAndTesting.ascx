﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MaintenanceServicingAndTesting.ascx.vb"
    Inherits="Appliances.MaintenanceServicingAndTesting" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="MSTPanel" runat="server">
    <table width="100%">
        <tr>
            <td align="right" style="width: 150px;">
                PAT Testing?:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnPATTesting" RepeatDirection="Horizontal" AutoPostBack="true" 
                    CellPadding="2" Width ="200">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="pnlPATTesting" Visible ="false" >
                    <table cellpadding="5">
                        <tr>
                            <td align="right" style="width: 150px;">
                                Last Test Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtTestDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtRentDate_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgRentCalendar" TargetControlID="txtTestDate" />
                                <asp:Image ID="imgRentCalendar" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                <asp:CompareValidator ID="CompareValidator6" runat="server" 
                                    ControlToValidate="txtTestDate" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Testing Cycle:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                Every&nbsp;
                                <asp:TextBox ID="txtTestingCycle" runat="server" Width="50px" Style="padding-right: 20px;" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:DropDownList runat="server" ID="ddlTestingCycle" Width="130px">
                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Next Test Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtNextTestDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgNextTestDate"
                                    TargetControlID="txtNextTestDate" />
                                <asp:Image ID="imgNextTestDate" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                 <asp:CompareValidator ID="CompareValidator5" runat="server" 
                                    ControlToValidate="txtNextTestDate" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <b>Maintenance Item:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #000000">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Is Regular Maintenance
                <br />
                Required?:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnRegularMaintenance" RepeatDirection="Horizontal"
                    CellPadding="2">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel1" runat="server">
                    <table cellpadding="5">
                        <tr>
                            <td align="right" style="width: 150px;">
                                Previous Maintenance Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtPreviousMaintenance" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgPreviousMaintenance"
                                    TargetControlID="txtPreviousMaintenance" />
                                <asp:Image ID="imgPreviousMaintenance" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                 <asp:CompareValidator ID="CompareValidator3" runat="server" 
                                    ControlToValidate="txtPreviousMaintenance" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Maintenance Cycle:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                Every&nbsp;
                                <asp:TextBox ID="txtMaintenanceCycle" runat="server" Width="50px" Style="padding-right: 20px;" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:DropDownList runat="server" ID="ddlMaintenanceCycle" Width="130px">
                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Next Maintenance Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtNextMaintenance" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgNextMaintenance"
                                    TargetControlID="txtNextMaintenance" />
                                <asp:Image ID="imgNextMaintenance" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                    ControlToValidate="txtNextMaintenance" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <b>Service Charge:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #000000">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Service Charge Item:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnServiceCharge" RepeatDirection="Horizontal"
                    CellPadding="2">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Annual Apportionment (£):
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox runat="server" ID="txtApportionment" />
                <asp:RegularExpressionValidator ID="rxvEstimate" runat="server" ErrorMessage="<br/>Please enter estimate, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                    Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtApportionment"
                    ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                <asp:RangeValidator ID="rvEstimate" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                    ControlToValidate="txtApportionment" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                    Display="Dynamic" ForeColor="Red" ValidationGroup="save" Type="Double" />

            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <b>M&E Servicing:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #000000">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Servicing Required?:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnServicingRequired" RepeatDirection="Horizontal"
                    CellPadding="5">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel2" runat="server">
                    <table cellpadding="5">
                        <tr>
                            <td align="right" style="width: 150px;">
                                Last Service Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtLastServiceDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgLastServiceDate"
                                    TargetControlID="txtLastServiceDate" />
                                <asp:Image ID="imgLastServiceDate" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                 <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="txtLastServiceDate" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Service Cycle:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                Every&nbsp;
                                <asp:TextBox ID="txtServiceCycle" runat="server" Width="50px" Style="padding-right: 20px;" onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:DropDownList runat="server" ID="ddlServiceCycle" Width="130px">
                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Next Service Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtNextServiceDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgNextServiceDate"
                                    TargetControlID="txtNextServiceDate" />
                                <asp:Image ID="imgNextServiceDate" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                                <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                    ControlToValidate="txtNextServiceDate" ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck"
                                    Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
