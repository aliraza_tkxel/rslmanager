﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Attributes
    Inherits UserControlBase
    Dim objPropertyBL As PropertyBL = New PropertyBL()

#Region "Events"

#Region "PageLoad"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ucConditionRatingList.BackToSummarybutton.Visible = False
            If IsPostBack Then
                ' If trVwAttributes.ExpandDepth > 1 Then
                If (ItemDetail.activeViewIndex > 2) Or ((SessionManager.getTreeItemName() IsNot Nothing) AndAlso SessionManager.getTreeItemName().Equals("Electrics")) Then

                    Dim itemId As Integer = SessionManager.getTreeItemId()
                    If itemId > 0 AndAlso SessionManager.getTreeItemName() <> "Heating Controls" Then

                        loadDynamicControls(itemId)
                    End If
                End If
            Else
                trVwAttributes.Nodes(0).Expanded = True
                SessionManager.removepreviousValueId()
                SessionManager.removeValueId()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Tree Nodes Populate"
    Protected Sub trVwAttributes_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwAttributes.TreeNodePopulate
        Try
            If e.Node.ChildNodes.Count = 0 Then
                Select Case (e.Node.Depth)
                    Case 0
                        populateLocations(e.Node)
                    Case 1
                        If (Convert.ToInt32(e.Node.Value) > 3) Then
                            If ((e.Node.Text = "Meters" Or e.Node.Text = "Electrics") And e.Node.Parent.Text = "Menu") Then
                                populateSubItems(e.Node)
                            Else
                                populateItems(e.Node)
                            End If
                        Else
                            populateAreas(e.Node)
                        End If
                    Case 2
                        If (e.Node.Parent.Text = "Services") Then
                            populateSubItems(e.Node)
                        Else
                            populateItems(e.Node)
                        End If
                    Case 3
                        populateSubItems(e.Node)
                    Case 4
                        populateSubItems(e.Node)
                End Select
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Selected Node Changed"
    ''' <summary>
    ''' Selected Node Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub trVwAttributes_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles trVwAttributes.SelectedNodeChanged
        Try
            Dim heatingFuelIdFromSession = SessionManager.getHeatingFuelId()
            resetSessionOnItemChanged()

            ' Dim tabGas As AjaxControlToolkit.CalendarExtender .TabContainer = CType(gasControl.FindControl("tabGas"), AjaxControlToolkit.TabContainer)
            Dim mainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
            Dim detailsControl As UserControl = CType(mainView.FindControl("detailsTabID"), UserControl)
            Dim notesControl As UserControl = CType(mainView.FindControl("notesTab"), UserControl)
            Dim pnlOtherDetailsTab As Panel = CType(detailsControl.FindControl("pnlOtherDetailsTab"), Panel)
            Dim summaryTab As UserControl = CType(mainView.FindControl("summaryTab"), UserControl)
            pnlOtherDetailsTab.Controls.Clear()
            Dim photosControl As UserControl = CType(mainView.FindControl("photosTab"), UserControl)
            Dim applianceTabId As UserControl = CType(mainView.FindControl("applianceTabId"), UserControl)
            Dim btnAddAppliance As Button = CType(applianceTabId.FindControl("btnAddAppliance"), Button)
            Dim lblNewAppliance As Label = CType(applianceTabId.FindControl("lblNewAppliance"), Label)
            Dim defectTab As UserControl = CType(mainView.FindControl("defectTab"), UserControl)
            Dim updPnlPhotos As UpdatePanel = CType(photosControl.FindControl("updPnlPhotos"), UpdatePanel)
            Dim pnlPhotosMsg As Panel = CType(photosControl.FindControl("pnlMessage"), Panel)
            Dim lblPhotoMsg As Label = CType(pnlPhotosMsg.FindControl("lblMessage"), Label)
            Dim lnkBtnDetailTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnDetailTab"), LinkButton)
            Dim lnkBtnSummaryTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnSummaryTab"), LinkButton)
            Dim lnkBtnAppliancesTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnAppliancesTab"), LinkButton)
            Dim lnkBtnDefectsTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnDefectsTab"), LinkButton)
            Dim lnkBtnNotesTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnNotesTab"), LinkButton)
            Dim lnkBtnPhotographsTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnPhotographsTab"), LinkButton)
            Dim lnkBtnDetectorTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnDetectorTab"), LinkButton)
            Dim btnAmend As Button = CType(itemDetailControl.FindControl("btnAmend"), Button)
            Dim btnDelete As Button = CType(itemDetailControl.FindControl("btnDelete"), Button)
            Dim detectorTab As UserControl = CType(mainView.FindControl("detectorTab"), UserControl)
            ItemDetail.activeViewIndex = trVwAttributes.SelectedNode.Depth
            lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass
            lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass
            SessionManager.setPlannedComponentId(0)
            'hide notescontrol,photoscontrol and btnamend on parent node click
            btnAmend.Visible = False
            btnDelete.Visible = False
            notesControl.Visible = False
            photosControl.Visible = False
            applianceTabId.Visible = False
            defectTab.Visible = False
            summaryTab.Visible = True
            trVwAttributes.ExpandDepth = 0
            lnkBtnSummaryTab.Visible = True
            lnkBtnDetailTab.Visible = False
            lnkBtnAppliancesTab.Visible = False
            lnkBtnDefectsTab.Visible = False
            lnkBtnDetectorTab.Visible = False
            mainView.ActiveViewIndex = 0
            lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass
            lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass
            'check selected node depth to create breadcrump
            lblBreadCrump.Text = String.Empty
            If trVwAttributes.SelectedNode.Depth = 1 Then
                ItemDetail.activeViewIndex = trVwAttributes.SelectedNode.Depth
                lblBreadCrump.Text = "<b>" + trVwAttributes.SelectedNode.Text + " </b> "

            ElseIf trVwAttributes.SelectedNode.Depth > 1 Or ((trVwAttributes.SelectedNode.Text = "Meters" Or
                    trVwAttributes.SelectedNode.Text = "Electrics") And
                    trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                ItemDetail.activeViewIndex = trVwAttributes.SelectedNode.Depth
                lblBreadCrump.Text = trVwAttributes.SelectedNode.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Text + "</b>"

            End If
            itemDetailControl.displayPhotoIcon(False)
            detailsControl.Visible = True
            If trVwAttributes.SelectedNode.Depth > 1 Or ((trVwAttributes.SelectedNode.Text = "Meters" Or trVwAttributes.SelectedNode.Text = "Electrics") And
                    trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                showItemdetail()
                lnkBtnSummaryTab.Visible = False
                lnkBtnDetailTab.Visible = True
                'get item detail by itemid to check showlistview 
                Dim showListView As Boolean = False
                Dim itemId As Integer = trVwAttributes.SelectedNode.Value
                Dim itemDetailDataSet As DataSet = New DataSet()
                objPropertyBL.getItemsByItemId(itemId, itemDetailDataSet)
                If itemDetailDataSet.Tables(0).Rows.Count > 0 Then
                    SessionManager.setItemDetailDataSet(itemDetailDataSet)
                End If
                Dim itemDetailResult = (From ps In itemDetailDataSet.Tables(0) Where ps.Item("ItemID").ToString() = itemId.ToString() Select ps)
                If itemDetailResult.Count() > 0 Then
                    showListView = Convert.ToBoolean(itemDetailResult.First().Item("ShowListView"))
                End If
                SessionManager.setTreeItemName(trVwAttributes.SelectedNode.Text.ToString())
                Dim proerptyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
                ' visible appliances tab for gas item only 
                'If trVwAttributes.SelectedNode.Target = "Appliances" Then
                If showListView = True AndAlso (trVwAttributes.SelectedNode.Target <> "CO" And trVwAttributes.SelectedNode.Target <> "Smoke") Then
                    lnkBtnAppliancesTab.Visible = True
                    applianceTabId.Visible = True
                    If ItemDetail.activeViewIndex > 1 Or ((trVwAttributes.SelectedNode.Text = "Meters" Or trVwAttributes.SelectedNode.Text = "Electrics") And
                    trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                        lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabClickedCssClass
                        mainView.ActiveViewIndex = 4
                        lnkBtnDetailTab.Visible = False
                        btnAddAppliance.Text = "Add " + trVwAttributes.SelectedNode.Text
                        lblNewAppliance.Text = "Add New " + trVwAttributes.SelectedNode.Text
                    End If
                ElseIf trVwAttributes.SelectedNode.Target = "Heating" Then
                    lnkBtnNotesTab.Enabled = False
                    lnkBtnPhotographsTab.Enabled = False
                    lnkBtnDefectsTab.Enabled = False
                    lnkBtnDefectsTab.Visible = True
                    defectTab.Visible = True
                    If ItemDetail.activeViewIndex > 1 Or ((trVwAttributes.SelectedNode.Text = "Meters" Or trVwAttributes.SelectedNode.Text = "Electrics") And
                    trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                        lnkBtnDetailTab.CssClass = ApplicationConstants.TabClickedCssClass
                        mainView.ActiveViewIndex = 1
                    End If
                ElseIf trVwAttributes.SelectedNode.Target = "CO" Or trVwAttributes.SelectedNode.Target = "Smoke" Then
                    lnkBtnDetectorTab.Visible = True
                    lnkBtnDetailTab.Visible = False
                    detectorTab.Visible = True
                    mainView.ActiveViewIndex = 6
                    Dim detectors As Detectors = detectorTab
                    lnkBtnDetectorTab.CssClass = ApplicationConstants.TabClickedCssClass
                    detectors.loadData()
                Else
                    lnkBtnAppliancesTab.Visible = False
                    If ItemDetail.activeViewIndex > 1 Or ((trVwAttributes.SelectedNode.Text = "Meters" Or trVwAttributes.SelectedNode.Text = "Electrics") And
                    trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                        lnkBtnDetailTab.CssClass = ApplicationConstants.TabClickedCssClass
                        mainView.ActiveViewIndex = 1
                    End If
                End If

                'visible notescontrol and photoscontrol on leaf node click
                notesControl.Visible = True
                photosControl.Visible = True
                ItemDetail.activeViewIndex = trVwAttributes.SelectedNode.Depth
                trVwAttributes.ExpandDepth = 2
                'breadcrump for leaf node

                'Dim areaId As Integer = trVwAttributes.SelectedNode.Parent.Value
                If (trVwAttributes.SelectedNode.Parent.Text = "Menu") Then
                    lblBreadCrump.Text = trVwAttributes.SelectedNode.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Text + "</b>"
                Else
                    lblBreadCrump.Text = trVwAttributes.SelectedNode.Parent.Parent.Text + " > " + trVwAttributes.SelectedNode.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Text + "</b>"
                End If
                'Store clicked item id in session
                SessionManager.setTreeItemId(itemId)
                updPnlPhotos.Visible = True
                pnlOtherDetailsTab.Visible = True
                If showListView = False AndAlso (trVwAttributes.SelectedNode.Target <> "CO" Or trVwAttributes.SelectedNode.Target <> "Smoke") Then
                    If trVwAttributes.SelectedNode.Text <> "Heating Controls" Then
                        Dim resultDataSet As DataSet = New DataSet()
                        objPropertyBL.getItemDetail(itemId, proerptyId, resultDataSet)
                        SessionManager.removeAttributeDetailDataSet()
                        SessionManager.setAttributeDetailDataSet(resultDataSet)
                        SessionManager.removepreviousValueId()
                        SessionManager.removeValueId()
                        Dim ucMSAT As UserControl = CType(detailsControl.FindControl("ucMSAT"), UserControl)
                        Dim objMST As MaintenanceServicingAndTesting = ucMSAT
                        objMST.resetMSTControl()
                        objMST.loadCycleDdl()
                        objMST.populateMST()

                        loadDynamicControls(itemId)
                    Else
                        lnkBtnDetailTab.Visible = False
                        notesControl.Visible = True
                        photosControl.Visible = True
                        detailsControl.Visible = False
                        mainView.ActiveViewIndex = 2
                        lnkBtnNotesTab.CssClass = ApplicationConstants.TabClickedCssClass
                        ' Bug834 Changes
                        loadDynamicControls(itemId)

                    End If

                ElseIf (trVwAttributes.SelectedNode.Target <> "CO" And trVwAttributes.SelectedNode.Target <> "Smoke") Then
                    'load appliance
                    Dim grdAppliances As GridView = CType(applianceTabId.FindControl("grdAppliances"), GridView)
                    Dim applianceDataSet As DataSet = New DataSet()
                    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "ApplianceId", 1, 30)
                    objPropertyBL.propertyAppliancesList(applianceDataSet, objPageSortBo, proerptyId, itemId)
                    SessionManager.setApplianceDetailDataSet(applianceDataSet)
                    grdAppliances.DataSource = applianceDataSet
                    SessionManager.removeAttributeDetailDataSet()
                    SessionManager.removepreviousValueId()
                    SessionManager.removeValueId()
                    grdAppliances.DataBind()
                    SessionManager.setHeatingFuelId(heatingFuelIdFromSession)
                End If
                itemDetailControl.BtnNotesTabClick()
                'itemDetailControl.displayPhotoIcon(True)
            Else
                showConditionRatingList()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "reset Session On Item Changed()"
    ''' <summary>
    ''' reset Session On Item Changed()
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub resetSessionOnItemChanged()
        SessionManager.removepreviousValueId()
        SessionManager.removeAttributrTypeId()
        SessionManager.removeValueLifeCycle()
        SessionManager.removeIsDdlChangeEvent()
        SessionManager.removePlannedComponentId()
        SessionManager.removeIsMapped()
        SessionManager.removeIsLifeCycleMapped()
        SessionManager.removeIsAccountingMapped()
        SessionManager.removeIsWorkSatisfactory()
        SessionManager.removeHeatingFuelId()
        SessionManager.removeheatingMapping()
        SessionManager.removeHeatingMappingId()
    End Sub
    Private Sub resetSessionOnItemHeatingChanged()
        SessionManager.removepreviousValueId()
        SessionManager.removeAttributrTypeId()
        SessionManager.removeValueLifeCycle()
        SessionManager.removeIsDdlChangeEvent()
        SessionManager.removePlannedComponentId()
        SessionManager.removeIsMapped()
        SessionManager.removeIsLifeCycleMapped()
        SessionManager.removeIsAccountingMapped()
        SessionManager.removeIsWorkSatisfactory()

    End Sub
#End Region

#Region "btnHeatingFuel click Event"
    Protected Sub btnHeatingFuel_Click(sender As Object, e As EventArgs) Handles btnHeatingFuel.Click
        resetSessionOnItemHeatingChanged()
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Dim heatingType As String = Request.Form("__EVENTARGUMENT").ToString()
        SessionManager.setheatingMapping("-1|" + heatingType)
        SessionManager.setHeatingFuelId(heatingType)
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        Dim dtHeatingMapping As DataTable = parameterDataSet.Tables(ApplicationConstants.dtHeatingMapping)
        Dim itemDetailControl As UserControl = CType(updPnlAttributes.FindControl("itemDetailControl"), UserControl)
        Dim MainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim btnAmend As Button = CType(itemDetailControl.FindControl("btnAmend"), Button)
        Dim detailsControl As UserControl = CType(MainView.FindControl("detailsTabID"), UserControl)
        Dim pnlOtherDetailsTab As Panel = CType(detailsControl.FindControl("pnlOtherDetailsTab"), Panel)
        Dim ddl65 As DropDownList = CType(pnlOtherDetailsTab.FindControl(ApplicationConstants.ddlParameter + "65"), DropDownList)
        'If dtHeatingMapping.Rows.Count > 0 AndAlso ddl65.SelectedItem.Text = "Mains Gas" Then
        If dtHeatingMapping.Rows.Count > 0 Then
            Dim mainsGasResult = (From ps In parameterDataSet.Tables(ApplicationConstants.dtHeatingMapping) Where ps.Item("HeatingType").ToString() = ddl65.SelectedValue.ToString() Select ps)
            If mainsGasResult.Count > 0 Then
                SessionManager.setheatingMapping("-1|-1")
                SessionManager.setHeatingFuelId("-1")
                loadDynamicControls(SessionManager.getTreeItemId())
                uiMessageHelper.setMessage(lblMessage, pnlMessage, "Heating type already added for this property ", True)
                Exit Sub
            Else
                loadDynamicControls(SessionManager.getTreeItemId())
                Dim dropDownBlObj As DropDownControlBL = New DropDownControlBL()

                dropDownBlObj.parameter_SelectedIndexChanged(ddl65, Nothing)
            End If
        Else
            loadDynamicControls(SessionManager.getTreeItemId())
            Dim dropDownBlObj As DropDownControlBL = New DropDownControlBL()

            dropDownBlObj.parameter_SelectedIndexChanged(ddl65, Nothing)
        End If

        ' dropDownBlObj.reloadManufacturer(ddl65)
    End Sub

#End Region

#Region "Heating Type clicked"
    Private Sub HeatingType_Clicked(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            resetSessionOnItemHeatingChanged()
            Dim btn As LinkButton = CType(sender, LinkButton)
            Dim heatingMapping As String = btn.CommandArgument
            SessionManager.setheatingMapping(heatingMapping)
            loadDynamicControls(SessionManager.getTreeItemId())
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub itemDetailControl_Amend_Clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemDetailControl.Amend_Clicked
        Try
            Dim itemId As Integer = Convert.ToInt32(SessionManager.getTreeItemId())
            loadDynamicControls(itemId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Functions"

#Region "Populate Tree - Functions"

#Region "Populate Location"
    ''' <summary>
    ''' Populate Location
    ''' </summary>
    ''' <param name="treeNode"></param>
    ''' <remarks></remarks>
    Private Sub populateLocations(ByVal treeNode As TreeNode)
        Dim count As Integer = 1
        Dim resultDataSet As DataSet = New DataSet()
        Try
            objPropertyBL.getLocations(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then

                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("LocationName")
                    newNode.Value = row("LocationID")
                    newNode.Target = row("Location")
                    If (Not newNode.Target.Equals("Item")) Then
                        newNode.SelectAction = TreeNodeSelectAction.None
                    End If
                    treeNode.ChildNodes.Add(newNode)
                Next

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Populate Area"
    ''' <summary>
    ''' Populate Area
    ''' </summary>
    ''' <param name="treeNode"></param>
    ''' <remarks></remarks>
    Private Sub populateAreas(ByVal treeNode As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Try

            Dim locationId As Integer = treeNode.Value
            objPropertyBL.getAreasByLocationId(locationId, resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then

                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("AreaName")
                    newNode.Value = row("AreaID")
                    newNode.Target = "area"
                    newNode.SelectAction = TreeNodeSelectAction.None
                    treeNode.ChildNodes.Add(newNode)
                Next
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Populate Items"
    ''' <summary>
    ''' Populate Items
    ''' </summary>
    ''' <param name="treeNode"></param>
    ''' <remarks></remarks>
    Private Sub populateItems(ByVal treeNode As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Try

            Dim areaId As Integer = treeNode.Value
            objPropertyBL.getItemsByAreaId(areaId, resultDataSet)

            If resultDataSet.Tables(0).Rows.Count > 0 Then
                SessionManager.setItemDetailDataSet(resultDataSet)
                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("ItemName")
                    newNode.Value = row("ItemID")
                    newNode.Target = row("ItemName")
                    If Convert.ToInt32(row("SubItem")) > 0 AndAlso (row("ItemName") <> "Bathroom" AndAlso row("ItemName") <> "Kitchen") Then
                        newNode.SelectAction = TreeNodeSelectAction.None
                    Else
                        newNode.Expand()
                    End If
                    treeNode.ChildNodes.Add(newNode)
                Next
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Populate Sub Items"
    ''' <summary>
    ''' Populate Sub Items
    ''' </summary>
    ''' <param name="treeNode"></param>
    ''' <remarks></remarks>
    Private Sub populateSubItems(ByVal treeNode As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Try

            Dim itemId As Integer = treeNode.Value
            objPropertyBL.getSubItemsByItemId(itemId, resultDataSet)

            If resultDataSet.Tables(0).Rows.Count > 0 Then
                SessionManager.setItemDetailDataSet(resultDataSet)
                For Each row As DataRow In resultDataSet.Tables(0).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.PopulateOnDemand = True
                    newNode.Text = row("ItemName")
                    newNode.Value = row("ItemID")
                    newNode.Target = row("ItemName")

                    If Convert.ToInt32(row("SubItem")) > 0 AndAlso (row("ItemName") <> "Bathroom" AndAlso row("ItemName") <> "Kitchen") Then
                        newNode.SelectAction = TreeNodeSelectAction.None
                    Else
                        newNode.Expand()

                    End If
                    treeNode.ChildNodes.Add(newNode)
                Next
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#End Region

#Region "load Dynamic Controls"
    ''' <summary>
    ''' load Dynamic Controls
    ''' </summary>
    ''' <param name="itemId"></param>
    ''' <remarks></remarks>
    Public Sub loadDynamicControls(ByVal itemId As Integer)
        ' Dim itemDetailControl As UserControl = CType(updPnlAttributes.FindControl("itemDetailControl"), UserControl)
        Dim MainView As MultiView = CType(itemDetailControl.FindControl("MainView"), MultiView)
        Dim btnAmend As Button = CType(itemDetailControl.FindControl("btnAmend"), Button)
        Dim btnDelete As Button = CType(itemDetailControl.FindControl("btnDelete"), Button)
        Dim detailsControl As UserControl = CType(MainView.FindControl("detailsTabID"), UserControl)
        Dim pnlOtherDetailsTab As Panel = CType(detailsControl.FindControl("pnlOtherDetailsTab"), Panel)
        Dim pnlMultipleHeating As Panel = CType(detailsControl.FindControl("pnlMultipleHeating"), Panel)
        Dim tblOtherDetails As Table = New Table()
        tblOtherDetails.ID = "tblOtherDetails"
        pnlOtherDetailsTab.Controls.Clear()
        pnlMultipleHeating.Controls.Clear()
        btnDelete.Visible = False
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        itemDetailControl.displayPhotoIcon(False)
        Dim lnkBtnDefectsTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnDefectsTab"), LinkButton)
        Dim lnkBtnNotesTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnNotesTab"), LinkButton)
        Dim lnkBtnPhotographsTab As LinkButton = CType(itemDetailControl.FindControl("lnkBtnPhotographsTab"), LinkButton)
        lnkBtnNotesTab.Enabled = True
        lnkBtnPhotographsTab.Enabled = True
        If SessionManager.getTreeItemName() = "Heating" Then
            Dim tblHeatingTypes As Table = New Table()
            tblHeatingTypes.ID = "tblHeatingTypes"
            Dim tblrow As TableRow = New TableRow()
            Dim dtHeatingMapping As DataTable = parameterDataSet.Tables(ApplicationConstants.dtHeatingMapping)
            If dtHeatingMapping.Rows.Count > 0 Then
                lnkBtnNotesTab.Enabled = True
                lnkBtnPhotographsTab.Enabled = True
                lnkBtnDefectsTab.Enabled = True
                Dim heatingType As Integer = 0
                For Each dr As DataRow In dtHeatingMapping.Rows
                    Dim td As TableCell = New TableCell()
                    Dim lnkBtnHeatingType As LinkButton = New LinkButton()
                    lnkBtnHeatingType.Text = "Heating Type " + Convert.ToString((heatingType + 1))
                    lnkBtnHeatingType.CssClass = ApplicationConstants.TabInitialCssClass
                    lnkBtnHeatingType.ID = dr.Item("HeatingMappingId")
                    lnkBtnHeatingType.CommandArgument = dr.Item("HeatingMappingId").ToString() + "|" + dr.Item("HeatingType").ToString()
                    If SessionManager.getheatingMapping() = lnkBtnHeatingType.CommandArgument Then
                        lnkBtnHeatingType.CssClass = ApplicationConstants.TabClickedCssClass
                    End If
                    If SessionManager.getHeatingMappingId() = Nothing AndAlso heatingType = 0 Then
                        lnkBtnHeatingType.CssClass = ApplicationConstants.TabClickedCssClass
                    End If
                    AddHandler lnkBtnHeatingType.Click, AddressOf HeatingType_Clicked
                    td.Controls.Add(lnkBtnHeatingType)
                    tblrow.Cells.Add(td)
                    heatingType = heatingType + 1
                Next
                If SessionManager.getHeatingMappingId() = Nothing Then
                    Dim HeatingMapping As String = dtHeatingMapping.Rows(0).Item("HeatingMappingId").ToString() + "|" + dtHeatingMapping.Rows(0).Item("HeatingType").ToString()
                    SessionManager.setheatingMapping(HeatingMapping)
                    SessionManager.setHeatingFuelId(dtHeatingMapping.Rows(0).Item("HeatingType").ToString())
                End If
            Else
                If SessionManager.getHeatingMappingId() = Nothing Then
                    SessionManager.setheatingMapping("-1|-1")
                End If
            End If
            Dim tdAdd As TableCell = New TableCell()
            Dim lnkBtnAddHeatingType As LinkButton = New LinkButton()
            lnkBtnAddHeatingType.Text = "Add Heating Type "
            lnkBtnAddHeatingType.CssClass = ApplicationConstants.TabInitialCssClass
            AddHandler lnkBtnAddHeatingType.Click, AddressOf HeatingType_Clicked
            lnkBtnAddHeatingType.ID = "AddHeatingType"
            lnkBtnAddHeatingType.CommandArgument = "-1|-1"
            If SessionManager.getheatingMapping() = lnkBtnAddHeatingType.CommandArgument Or SessionManager.getheatingMapping().ToString().Contains("-1") Then
                lnkBtnAddHeatingType.CssClass = ApplicationConstants.TabClickedCssClass
                lnkBtnNotesTab.Enabled = False
                lnkBtnPhotographsTab.Enabled = False
                lnkBtnDefectsTab.Enabled = False
            End If
            tdAdd.Controls.Add(lnkBtnAddHeatingType)
            tblrow.Cells.Add(tdAdd)
            tblHeatingTypes.Rows.Add(tblrow)
            pnlMultipleHeating.Controls.Add(tblHeatingTypes)
        End If

        If MainView.ActiveViewIndex = 1 Then
            Dim pageName = System.IO.Path.GetFileName(Request.Url.ToString())
            If pageName.Contains(PathConstants.PropertiesPage) Then
                btnAmend.Visible = True
                If SessionManager.getTreeItemName() = "Heating" Then

                    btnDelete.Visible = True
                End If
            End If
        Else
            btnAmend.Visible = False
            btnDelete.Visible = False
        End If

        If parameterDataSet.Tables.Count > 0 Then
            If parameterDataSet.Tables(0).Rows.Count > 0 Then
                If SessionManager.getTreeItemName() = "Heating" Then
                    tblOtherDetails = ControlFactoryBL.createHeatingDynaicControls(parameterDataSet, SessionManager.getheatingMapping())
                    Dim HeatingMappingId = SessionManager.getHeatingMappingId()

                    Dim photosDataSet As DataSet = New DataSet()
                    objPropertyBL.getPropertyImages(SessionManager.getPropertyId, SessionManager.getTreeItemId, photosDataSet, HeatingMappingId)

                    If photosDataSet.Tables(0).Rows.Count > 0 Then
                        itemDetailControl.displayPhotoIcon(True)
                    Else
                        itemDetailControl.displayPhotoIcon(False)
                    End If
                Else
                    If SessionManager.getTreeItemName() <> "Heating Controls" Then
                        tblOtherDetails = ControlFactoryBL.createDynaicControls(parameterDataSet)
                    End If

                End If
            End If
        End If
        pnlOtherDetailsTab.Controls.Add(tblOtherDetails)


    End Sub
#End Region

#Region "Populate Attributes control"

    Public Sub populateAttributeControl(Optional ByVal propertyId As String = Nothing)
        showConditionRatingList(propertyId)
    End Sub

#End Region

#Region "Get Property Id from Query String"
    ''' <summary>
    ''' Get propertyId from query string, using the key stored in pathconstants (PathConstants.PropertyIds = "id")
    ''' </summary>
    ''' <param name="propertyId">Optional propertyId by ref to store propertyid.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getPropertyIdfromQueryString(Optional ByRef propertyId As String = Nothing) As String
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            propertyId = CType(Request.QueryString(PathConstants.PropertyIds), String)
        End If
        Return propertyId
    End Function
#End Region

#Region "Show Condition Rating List"

    Private Sub showConditionRatingList(Optional ByVal propertyId As String = Nothing)
        ucConditionRatingList.populateConditionRatingList(propertyId)
        pnlConditionRatingList.Visible = True
        itemDetailControl.Visible = False
    End Sub

#End Region

#Region "Show Item Detail"

    Private Sub showItemdetail()
        pnlConditionRatingList.Visible = False
        itemDetailControl.Visible = True
    End Sub

#End Region

    Public Sub DisplayMessage(ByVal message As String)
        Response.Write(message)
    End Sub

#End Region
 
End Class