﻿Imports AS_Utilities
Imports AS_BusinessObject
Imports AS_BusinessLogic
Imports System.Reflection
Imports System.Globalization

Public Class ItemDetail
    Inherits UserControlBase
    Dim _readOnly As Boolean = False
    Public Shared activeViewIndex As Integer
    Public Event Amend_Clicked(ByVal sender As Object, ByVal e As EventArgs)

#Region "Events"
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
            btnAmend.Visible = False
        End If
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        If Not IsPostBack Then
            lnkBtnSummaryTab.CssClass = ApplicationConstants.TabClickedCssClass
            MainView.ActiveViewIndex = 0
            summaryTab.populateAttributeSummary()
            lnkBtnDetailTab.Visible = False
        End If
    End Sub
#End Region

#Region "lnk Btn Summary Tab Click"
    ''' <summary>
    ''' lnk Btn Summary Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnummaryTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(lnkBtnSummaryTab)
        summaryTab.populateAttributeSummary()
        btnAmend.Visible = False
        MainView.ActiveViewIndex = 0
        btnDelete.Visible = False
    End Sub

#End Region

#Region "lnk Btn Detail Tab Click"
    ''' <summary>
    ''' lnk Btn Detail Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDetailTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnDetailTab.Click
        setSelectedMenuItemStyle(lnkBtnDetailTab)
        MainView.ActiveViewIndex = 1
        btnDelete.Visible = False
        If activeViewIndex > 2 Then
            If _readOnly = True Then
                btnAmend.Visible = False
            Else
                btnAmend.Visible = True
            End If
        End If

    End Sub

#End Region

#Region "lnk Btn Notes Tab Click"
    ''' <summary>
    ''' lnk Btn Notes Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnNotesTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(lnkBtnNotesTab)
        btnAmend.Visible = False
        btnDelete.Visible = False
        MainView.ActiveViewIndex = 2
        notesTab.loadData()
        If activeViewIndex <= 2 Then
            notesTab.Visible = False
        End If
    End Sub
#End Region

    Public Sub BtnNotesTabClick()
        notesTab.loadData()
    End Sub

#Region "lnk Btn Photographs Tab Click"
    ''' <summary>
    ''' lnk Btn Photographs Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnPhotographsTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(lnkBtnPhotographsTab)
        btnAmend.Visible = False
        btnDelete.Visible = False
        MainView.ActiveViewIndex = 3
        photosTab.loadData()
        If activeViewIndex <= 2 Then
            photosTab.Visible = False
        End If
    End Sub
#End Region

#Region "lnk Btn Appliances Tab Click"
    ''' <summary>
    ''' lnk Btn Appliances Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppliancesTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        setSelectedMenuItemStyle(lnkBtnAppliancesTab)
        btnAmend.Visible = False
        MainView.ActiveViewIndex = 4
        applianceTabId.loadPropertyAppliances()
    End Sub
#End Region

#Region "lnkBtnDefectsTab Click"


    Protected Sub lnkBtnDefectsTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnDefectsTab.Click
        setSelectedMenuItemStyle(lnkBtnDefectsTab)
        btnAmend.Visible = False
        btnDelete.Visible = False
        MainView.ActiveViewIndex = 5
        defectTab.loadDefects()
    End Sub

#End Region

#Region "lnkBtnDetectorTab Click"


    Protected Sub lnkBtnDetectorTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnDetectorTab.Click
        setSelectedMenuItemStyle(lnkBtnDetectorTab)
        btnAmend.Visible = False
        btnDelete.Visible = False
        MainView.ActiveViewIndex = 6
        detectorTab.loadData()
    End Sub

#End Region

#Region "btn amend click"

    ''' <summary>
    ''' btn amend click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAmend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAmend.Click
        Dim pnlDetailMsg As Panel = CType(detailsTabID.FindControl("pnlMessage"), Panel)
        Dim lblDetailMsg As Label = CType(detailsTabID.FindControl("lblMessage"), Label)
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim itemDetailList As List(Of ItemDetailBO) = New List(Of ItemDetailBO)()
            Dim itemDateList As List(Of ItemDatesControlBO) = New List(Of ItemDatesControlBO)()
            Dim conditionRatingList As List(Of ConditionRatingBO) = New List(Of ConditionRatingBO)()
            Dim objItemAttributeBo As ItemAttributeBO = New ItemAttributeBO()
            Dim proerptyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
            Dim msatDetailList As List(Of MSATDetailBO) = New List(Of MSATDetailBO)()
            Dim detailsControl As UserControl = CType(MainView.FindControl("detailsTabID"), UserControl)
            Dim ucMSAT As UserControl = CType(detailsControl.FindControl("ucMSAT"), UserControl)
            Dim objMST As MaintenanceServicingAndTesting = ucMSAT
            Dim itemName As String = SessionManager.getTreeItemName()
            ''if item name is heating then validation will be applien on base of landlord appliance check box
            If itemName.ToUpper() = "Heating".ToUpper() Then
                Me.amendHeatingAttribute(itemDetailList, itemDateList, conditionRatingList)
                objItemAttributeBo.HeatingMappingId = Convert.ToInt32(SessionManager.getHeatingMappingId())
            Else
                Me.amendAttribute(itemDetailList, itemDateList, conditionRatingList)
            End If
            objMST.fillMSATObject(msatDetailList)
            If uiMessageHelper.IsError = False Then
                objItemAttributeBo.PropertyId = proerptyId
                objItemAttributeBo.ItemId = SessionManager.getTreeItemId()
                objItemAttributeBo.UpdatedBy = SessionManager.getAppServicingUserId()

                Dim objPropertyBl As PropertyBL = New PropertyBL()
                objPropertyBl.amendItemDetail(objItemAttributeBo, ConvertToDataTable(itemDetailList), ConvertToDataTable(itemDateList), ConvertToDataTable(conditionRatingList), ConvertToDataTable(msatDetailList))
                uiMessageHelper.setMessage(lblDetailMsg, pnlDetailMsg, UserMessageConstants.ItemAddedSuccessfuly, False)

                Dim itemId As Integer = Convert.ToInt32(SessionManager.getTreeItemId())
                Dim resultDataSet As DataSet = New DataSet()
                objPropertyBl.getItemDetail(itemId, proerptyId, resultDataSet)

                SessionManager.setAttributeDetailDataSet(resultDataSet)
                SessionManager.removeHeatingFuelId()
                SessionManager.removeheatingMapping()
                SessionManager.removeHeatingMappingId()
                RaiseEvent Amend_Clicked(sender, e)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblDetailMsg, pnlDetailMsg, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "Amend Item Attribute Detail"
    ''' <summary>
    ''' Amend Item Attribute Detail
    ''' </summary>
    ''' <param name="itemDetailList"></param>
    ''' <param name="itemDateList"></param>
    ''' <remarks></remarks>
    Private Sub amendAttribute(ByRef itemDetailList As List(Of ItemDetailBO), ByRef itemDateList As List(Of ItemDatesControlBO), ByRef conditionRatingList As List(Of ConditionRatingBO))
        Dim pnlDetailMsg As Panel = CType(detailsTabID.FindControl("pnlMessage"), Panel)
        Dim lblDetailMsg As Label = CType(detailsTabID.FindControl("lblMessage"), Label)
        Dim itemName As String = SessionManager.getTreeItemName()
        ''if item name is heating then validation will be applien on base of landlord appliance check box
        If itemName.ToUpper() = "Heating".ToUpper() Then
            Dim result As Boolean = applyValidation()
            Dim resultHeating As Boolean = applyHeatingValidation(itemDetailList)
            If result = False AndAlso resultHeating = False Then
                Exit Sub
            End If
        End If
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        If parameterDataSet.Tables("Parameters").Rows.Count > 0 Then
            Dim conditionValueId As Integer = 0

            For Each dr As DataRow In parameterDataSet.Tables(ApplicationConstants.parameters).Rows
                Dim objItemDetailBo As ItemDetailBO = New ItemDetailBO()
                Dim objItemDatesControlBo As ItemDatesControlBO = New ItemDatesControlBO()
                Dim ControlType As String = dr.Item("ControlType")

                Dim parameterId As Integer = dr.Item("ParameterID")
                objItemDetailBo.ItemParamId = dr.Item("ItemParamID")
                Dim parameterName As String = dr.Item("ParameterName")
                If SessionManager.getAttributrTypeId() = 1 Then
                    Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                    If attributeResult.Count > 0 Then
                        Dim plannedComponentId As Integer = attributeResult.First.Item("COMPONENTID")
                        SessionManager.setPlannedComponentId(plannedComponentId)
                    End If
                ElseIf SessionManager.getAttributrTypeId() = 2 Then
                    Dim parameterLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("PARAMETERID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                    If parameterLifeCycleResult.Count > 0 Then

                        Dim plannedComponentId As Integer = parameterLifeCycleResult.First.Item("COMPONENTID")
                        SessionManager.setPlannedComponentId(plannedComponentId)
                    Else

                        SessionManager.setPlannedComponentId(0)
                    End If
                End If

                If ControlType.ToUpper() = "Dropdown".ToUpper() Then
                    Dim ddlParameter As DropDownList = CType(detailsTabID.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString()), DropDownList)
                    objItemDetailBo.ParameterValue = ddlParameter.SelectedItem.Text
                    objItemDetailBo.ValueId = ddlParameter.SelectedValue
                    If ddlParameter.SelectedValue > 0 Then
                        itemDetailList.Add(objItemDetailBo)
                    End If
                    If SessionManager.getAttributrTypeId() = 3 Then
                        'Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID").ToString() = objItemDetailBo.ValueId Select ps)
                        Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = objItemDetailBo.ValueId And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable

                        If attributeResult.Count > 0 Then
                            SessionManager.setpreviousValueId(objItemDetailBo.ValueId)

                        Else
                            Dim previousValueId As String = SessionManager.getpreviousValueId()
                            Dim valueId As String = objItemDetailBo.ValueId
                            If previousValueId > 0 And previousValueId <> valueId Then

                                Dim loadComponentDv As DataView = New DataView()
                                Dim dtLoadComponent As DataTable = New DataTable()
                                dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                                loadComponentDv = dtLoadComponent.AsDataView()
                                loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                                dtLoadComponent = loadComponentDv.ToTable()
                                If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                                    Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                                    If ipreviousResult.Count > 0 Then

                                        Dim plannedComponentId As Integer = ipreviousResult.First.Item("COMPONENTID")
                                        SessionManager.setPlannedComponentId(plannedComponentId)
                                    End If
                                End If
                            Else
                                Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                                If iResult.Count > 0 Then

                                    Dim plannedComponentId As Integer = iResult.First.Item("COMPONENTID")
                                    SessionManager.setPlannedComponentId(plannedComponentId)
                                End If
                            End If
                        End If

                    End If

                ElseIf ControlType.ToUpper() = "Date".ToUpper() Then
                    ' Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId Select ps)
                    Dim txtDate As TextBox
                    If parameterName.Contains("Rewire Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Upgrade Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Cur Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Replacement Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Last Rewired") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString()), TextBox)
                    ElseIf parameterName.Contains("CUR Last Replaced") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curLastReplaced.ToString()), TextBox)
                    ElseIf parameterName.Contains("Upgrade Last Done") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString()), TextBox)
                    ElseIf parameterName.Contains("Last Replaced") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                    ElseIf parameterName.Contains("Original Install Date") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString()), TextBox)
                    ElseIf parameterName.Contains("Gasket Set Replacement") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()), TextBox)
                    Else
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + parameterId.ToString()), TextBox)

                    End If



                    objItemDatesControlBo.ParameterId = parameterId
                    objItemDatesControlBo.ParameterName = parameterName
                    Dim dateTime As Date
                    Dim valid As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)

                    If valid = True OrElse Not (txtDate.Enabled) Then
                        If parameterName.Contains("Due") Then
                            If parameterName.Contains("Rewire Due") Or parameterName.Contains("Upgrade Due") Or parameterName.Contains("Cur Due") _
                                Or parameterName.ToUpper() = "Replacement Due".ToUpper() Then
                                itemDateList(itemDateList.LongCount - 1).DueDate = txtDate.Text
                                itemDateList(itemDateList.LongCount - 1).ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList(itemDateList.LongCount - 1).ParameterName = parameterName
                                If parameterName.ToUpper() = "Replacement Due".ToUpper() Then
                                    itemDateList(itemDateList.LongCount - 1).ParameterId = 0
                                End If
                            Else
                                objItemDatesControlBo.DueDate = txtDate.Text
                                objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList.Add(objItemDatesControlBo)
                            End If
                        ElseIf parameterName.Contains("Last Rewired") Or parameterName.Contains("Upgrade Last Done") Or parameterName.Contains("CUR Last Replaced") _
                            Or parameterName.ToUpper() = "Last Replaced".ToUpper() Then
                            objItemDatesControlBo.LastDone = txtDate.Text
                            objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                            itemDateList.Add(objItemDatesControlBo)
                        Else
                            objItemDatesControlBo.LastDone = String.Empty
                            objItemDatesControlBo.DueDate = txtDate.Text
                            itemDateList.Add(objItemDatesControlBo)
                        End If

                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.ValidDateFormat + parameterName
                        Exit Sub
                    End If

                    ' Dim selectedvalue As String = ddlParameter.SelectedValue
                ElseIf ControlType.ToUpper() = "Textbox".ToUpper() Then
                    Dim txtBox As TextBox = CType(detailsTabID.FindControl(ApplicationConstants.txt + parameterId.ToString()), TextBox)
                    If Not txtBox.Text Is Nothing Then
                        objItemDetailBo.ParameterValue = txtBox.Text
                        itemDetailList.Add(objItemDetailBo)
                    End If

                ElseIf ControlType.ToUpper() = "TextArea".ToUpper() Then
                    Dim txtBox As TextBox = CType(detailsTabID.FindControl(ApplicationConstants.txtArea + parameterId.ToString()), TextBox)
                    If conditionValueId > 0 Then
                        If Not txtBox.Text Is Nothing Then
                            objItemDetailBo.ParameterValue = txtBox.Text
                            itemDetailList.Add(objItemDetailBo)
                        End If
                        Dim attrId As Integer = SessionManager.getAttributrTypeId()
                        If attrId > 0 AndAlso itemDateList.Count > 0 Then

                            If itemName.ToUpper() = "Heating".ToUpper() Then
                                Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                                Dim itm As ItemDatesControlBO = itemDateList.Find(Function(c) c.ComponentId > 0)
                                If (Not IsNothing(itm)) Then
                                    objConditionWorkBO.ComponentId = itm.ComponentId
                                End If
                                objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                                objConditionWorkBO.ValueId = conditionValueId
                                conditionRatingList.Add(objConditionWorkBO)
                            Else
                                For Each itemDate As ItemDatesControlBO In itemDateList

                                    Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                                    If itemDate.ComponentId > 0 Then
                                        objConditionWorkBO.ComponentId = itemDate.ComponentId
                                    End If
                                    objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                                    objConditionWorkBO.ValueId = conditionValueId
                                    conditionRatingList.Add(objConditionWorkBO)
                                Next
                            End If

                        Else
                            Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                            ' objConditionWorkBO.ComponentId = DBNull.Value
                            objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                            objConditionWorkBO.ValueId = conditionValueId
                            conditionRatingList.Add(objConditionWorkBO)
                        End If


                    End If
                ElseIf ControlType.ToUpper() = "Checkboxes".ToUpper() Then
                    Dim chkList As CheckBoxList = CType(detailsTabID.FindControl(ApplicationConstants.chkList + parameterId.ToString()), CheckBoxList)
                    For Each item As ListItem In chkList.Items
                        Dim objSubItemDetailBO As ItemDetailBO = New ItemDetailBO()
                        objSubItemDetailBO.ItemParamId = dr.Item("ItemParamID")
                        objSubItemDetailBO.ParameterValue = item.Text
                        objSubItemDetailBO.ValueId = item.Value
                        If item.Selected Then
                            objSubItemDetailBO.IsCheckBoxSelected = True
                        Else
                            objSubItemDetailBO.IsCheckBoxSelected = False
                        End If
                        itemDetailList.Add(objSubItemDetailBO)
                    Next

                ElseIf ControlType.ToUpper() = "Radiobutton".ToUpper() Then
                    Dim rdBtn As RadioButtonList = CType(detailsTabID.FindControl(ApplicationConstants.rdBtn + parameterId.ToString()), RadioButtonList)
                    For Each item As ListItem In rdBtn.Items
                        If item.Selected Then
                            objItemDetailBo.ParameterValue = item.Text
                            objItemDetailBo.ValueId = item.Value
                            itemDetailList.Add(objItemDetailBo)
                            If item.Text.ToUpper = ApplicationConstants.conditionUnsatisfactory.ToUpper Or item.Text.ToUpper = ApplicationConstants.conditionPotentiallyUnsatisfactory.ToUpper Then
                                conditionValueId = item.Value
                            Else
                                conditionValueId = 0

                            End If
                        End If
                    Next
                End If
            Next
        End If

    End Sub

#End Region

#Region "Convert list to datatable"
    ''' <summary>
    ''' Convert Generic list to data table
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="list"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertToDataTable(Of T)(ByVal list As List(Of T)) As DataTable
        Dim dt As New DataTable()
        For Each info As PropertyInfo In GetType(T).GetProperties()
            dt.Columns.Add(New DataColumn(info.Name, If(Nullable.GetUnderlyingType(info.PropertyType), info.PropertyType)))
        Next
        If list IsNot Nothing Then
            For Each tt As T In list
                Dim row As DataRow = dt.NewRow()
                For Each info As PropertyInfo In GetType(T).GetProperties()
                    row(info.Name) = If(info.GetValue(tt, Nothing), DBNull.Value)
                Next
                dt.Rows.Add(row)
            Next
        End If
        Return dt
    End Function

#End Region

    Function applyValidation() As Boolean
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        'get landloard appliance parameter to check checkbox is selected or not.
        Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterName").ToString() = "Landlord Appliance" Select ps)
        If attributeResult.Count() > 0 Then
            Dim parameterId As Integer = attributeResult.First().Item("ParameterID")
            Dim chkList As CheckBoxList = CType(detailsTabID.FindControl(ApplicationConstants.chkList + parameterId.ToString()), CheckBoxList)
            Dim chkboxSelected As Boolean = False
            For Each item As ListItem In chkList.Items
                If item.Selected Then
                    chkboxSelected = True
                End If
            Next
            If chkboxSelected = True Then
                Dim heatingType As Integer = 0
                heatingType = SessionManager.getHeatingFuelId()

                Dim filteredDV As DataView = parameterDataSet.Tables(ApplicationConstants.parameters).AsDataView()
                filteredDV.RowFilter = "ParameterValueId =  " + heatingType.ToString()
                Dim filteredDT As DataTable = filteredDV.ToTable()

                For Each dr As DataRow In filteredDT.Rows
                    Dim objItemDetailBo As ItemDetailBO = New ItemDetailBO()
                    Dim objItemDatesControlBo As ItemDatesControlBO = New ItemDatesControlBO()
                    Dim ControlType As String = dr.Item("ControlType")
                    parameterId = dr.Item("ParameterID")
                    Dim parameterName = dr.Item("ParameterName")
                    If ControlType.ToUpper() = "Dropdown".ToUpper() Then
                        Dim ddlParameter As DropDownList = CType(detailsTabID.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString()), DropDownList)
                        If ddlParameter.SelectedValue <= 0 Or ddlParameter.SelectedItem.Text = "Please Select" Then
                            ddlParameter.Focus()
                            uiMessageHelper.IsError = True
                            uiMessageHelper.Message = UserMessageConstants.mandatoryField
                            Return False
                            Exit Function
                        End If
                    ElseIf ControlType.ToUpper() = "Date".ToUpper() Then

                        Dim txtDate As TextBox
                        If parameterName.Contains("Rewire Due") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString()), TextBox)
                        ElseIf parameterName.Contains("Upgrade Due") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString()), TextBox)
                        ElseIf parameterName.Contains("Cur Due") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curDue.ToString()), TextBox)
                        ElseIf parameterName.Contains("Replacement Due") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                        ElseIf parameterName.Contains("Last Rewired") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString()), TextBox)
                        ElseIf parameterName.Contains("CUR Last Replaced") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curLastReplaced.ToString()), TextBox)
                        ElseIf parameterName.Contains("Upgrade Last Done") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString()), TextBox)
                        ElseIf parameterName.Contains("Last Replaced") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                        ElseIf parameterName.Contains("Original Install Date") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString()), TextBox)
                        ElseIf parameterName.Contains("Gasket Set Replacement") Then
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()), TextBox)
                        Else
                            txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + parameterId.ToString()), TextBox)

                        End If
                        Dim dateTime As Date
                        Dim valid As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)

                        If valid = False Then
                            txtDate.Focus()
                            uiMessageHelper.IsError = True
                            uiMessageHelper.Message = String.Format(UserMessageConstants.ValidDateFormat, parameterName)
                            Return False
                            Exit Function
                        End If
                    ElseIf ControlType.ToUpper() = "Textbox".ToUpper() Then
                        Dim txtBox As TextBox = CType(detailsTabID.FindControl(ApplicationConstants.txt + parameterId.ToString()), TextBox)
                        If txtBox.Text = Nothing Or txtBox.Text = "" Then
                            txtBox.Focus()
                            uiMessageHelper.IsError = True
                            uiMessageHelper.Message = UserMessageConstants.mandatoryField
                            Return False
                            Exit Function
                        End If
                    ElseIf ControlType.ToUpper() = "Radiobutton".ToUpper() Then
                        Dim rdBtn As RadioButtonList = CType(detailsTabID.FindControl(ApplicationConstants.rdBtn + parameterId.ToString()), RadioButtonList)
                        Dim isSelected As Integer = 0
                        For Each item As ListItem In rdBtn.Items
                            If item.Selected Then
                                isSelected = isSelected + 1
                            End If
                        Next
                        If isSelected = 0 Then
                            rdBtn.Focus()
                            uiMessageHelper.IsError = True
                            uiMessageHelper.Message = UserMessageConstants.mandatoryField
                            Return False
                            Exit Function
                        End If
                    End If



                Next
            End If
        End If
        Return True
    End Function

    Function applyHeatingValidation(ByRef itemDetailList As List(Of ItemDetailBO)) As Boolean
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        'get landloard appliance parameter to check checkbox is selected or not.
        Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterName").ToString() = "Heating Fuel" Select ps)
        If attributeResult.Count() > 0 Then
            Dim parameterId As Integer = attributeResult.First().Item("ParameterID")
            Dim ControlType As String = attributeResult.First().Item("ControlType")
            Dim parameterName = attributeResult.First().Item("ParameterName")
            If ControlType.ToUpper() = "Dropdown".ToUpper() Then
                Dim ddlParameter As DropDownList = CType(detailsTabID.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString()), DropDownList)
                If ddlParameter.SelectedValue <= 0 Or ddlParameter.SelectedItem.Text = "Please Select" Then
                    ddlParameter.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.pleaseSelect + " " + parameterName
                    Return False
                    Exit Function
                Else
                    Dim objItemDetailBo As ItemDetailBO = New ItemDetailBO()
                    objItemDetailBo.ItemParamId = attributeResult.First().Item("ItemParamID")
                    objItemDetailBo.ParameterValue = ddlParameter.SelectedItem.Text
                    objItemDetailBo.ValueId = ddlParameter.SelectedValue
                    If ddlParameter.SelectedValue > 0 Then
                        itemDetailList.Add(objItemDetailBo)
                    End If
                    If SessionManager.getAttributrTypeId() = 3 Then
                        'Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID").ToString() = objItemDetailBo.ValueId Select ps)
                        Dim componentResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = ddlParameter.SelectedValue And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable

                        If componentResult.Count > 0 Then
                            SessionManager.setpreviousValueId(ddlParameter.SelectedValue)

                        Else
                            Dim previousValueId As String = SessionManager.getpreviousValueId()
                            Dim valueId As String = ddlParameter.SelectedValue
                            If previousValueId > 0 And previousValueId <> valueId Then

                                Dim loadComponentDv As DataView = New DataView()
                                Dim dtLoadComponent As DataTable = New DataTable()
                                dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                                loadComponentDv = dtLoadComponent.AsDataView()
                                loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                                dtLoadComponent = loadComponentDv.ToTable()
                                If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                                    Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                                    If ipreviousResult.Count > 0 Then

                                        Dim plannedComponentId As Integer = ipreviousResult.First.Item("COMPONENTID")
                                        SessionManager.setPlannedComponentId(plannedComponentId)
                                    End If
                                End If
                            Else
                                Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                                If iResult.Count > 0 Then

                                    Dim plannedComponentId As Integer = iResult.First.Item("COMPONENTID")
                                    SessionManager.setPlannedComponentId(plannedComponentId)
                                End If
                            End If
                        End If

                    End If
                End If
            End If
        End If
        Return True
    End Function

#Region "Set Selected Menu Item (Link Button) Style"

    ''' <summary>
    ''' Set the style of the selected link button(Horizontal Tab Menu):
    ''' Set all item to the initial styles and selected item differently.    ''' 
    ''' </summary>
    ''' <param name="lnkBtnSelected">Selected menu item.</param>
    ''' <remarks></remarks>
    Public Sub setSelectedMenuItemStyle(ByRef lnkBtnSelected As LinkButton)
        lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass
    End Sub

#End Region


#Region "Amend Item Attribute Detail"
    ''' <summary>
    ''' Amend Item Attribute Detail
    ''' </summary>
    ''' <param name="itemDetailList"></param>
    ''' <param name="itemDateList"></param>
    ''' <remarks></remarks>
    Private Sub amendHeatingAttribute(ByRef itemDetailList As List(Of ItemDetailBO), ByRef itemDateList As List(Of ItemDatesControlBO), ByRef conditionRatingList As List(Of ConditionRatingBO))
        Dim pnlDetailMsg As Panel = CType(detailsTabID.FindControl("pnlMessage"), Panel)
        Dim lblDetailMsg As Label = CType(detailsTabID.FindControl("lblMessage"), Label)
        Dim itemName As String = SessionManager.getTreeItemName()
        ''if item name is heating then validation will be applien on base of landlord appliance check box
        'If itemName.ToUpper() = "Heating".ToUpper() Then
        Dim result As Boolean = applyValidation()
        Dim resultHeating As Boolean = applyHeatingValidation(itemDetailList)
        If result = False AndAlso resultHeating = False Then
            Exit Sub
        End If
        ' End If
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        Dim heatingType As Integer = 0
        heatingType = SessionManager.getHeatingFuelId()

        Dim filteredDV As DataView = parameterDataSet.Tables(ApplicationConstants.parameters).AsDataView()
        filteredDV.RowFilter = "ParameterValueId =  " + heatingType.ToString()
        Dim filteredDT As DataTable = filteredDV.ToTable()


        If filteredDT.Rows.Count > 0 Then
            Dim conditionValueId As Integer = 0

            For Each dr As DataRow In filteredDT.Rows
                Dim objItemDetailBo As ItemDetailBO = New ItemDetailBO()
                Dim objItemDatesControlBo As ItemDatesControlBO = New ItemDatesControlBO()
                Dim ControlType As String = dr.Item("ControlType")

                Dim parameterId As Integer = dr.Item("ParameterID")
                objItemDetailBo.ItemParamId = dr.Item("ItemParamID")
                Dim parameterName As String = dr.Item("ParameterName")
                If SessionManager.getAttributrTypeId() = 1 Then
                    Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("ITEMID").ToString() = SessionManager.getTreeItemId().ToString() And ps.Item("PARAMETERID") Is DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                    If attributeResult.Count > 0 Then
                        Dim plannedComponentId As Integer = attributeResult.First.Item("COMPONENTID")
                        SessionManager.setPlannedComponentId(plannedComponentId)
                    End If
                ElseIf SessionManager.getAttributrTypeId() = 2 Then
                    Dim parameterLifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId.ToString() And ps.Item("PARAMETERID") IsNot DBNull.Value And ps.Item("VALUEID") Is DBNull.Value Select ps)
                    If parameterLifeCycleResult.Count > 0 Then

                        Dim plannedComponentId As Integer = parameterLifeCycleResult.First.Item("COMPONENTID")
                        SessionManager.setPlannedComponentId(plannedComponentId)
                    Else

                        SessionManager.setPlannedComponentId(0)
                    End If
                End If

                If ControlType.ToUpper() = "Dropdown".ToUpper() Then
                    Dim ddlParameter As DropDownList = CType(detailsTabID.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString()), DropDownList)
                    objItemDetailBo.ParameterValue = ddlParameter.SelectedItem.Text
                    objItemDetailBo.ValueId = ddlParameter.SelectedValue
                    If ddlParameter.SelectedValue > 0 Then
                        itemDetailList.Add(objItemDetailBo)
                    End If
                    If SessionManager.getAttributrTypeId() = 3 Then
                        'Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID").ToString() = objItemDetailBo.ValueId Select ps)
                        Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = objItemDetailBo.ValueId And ps.Item("SubParameter") IsNot DBNull.Value And ps.Item("SubValue") IsNot DBNull.Value Select ps).AsEnumerable

                        If attributeResult.Count > 0 Then
                            SessionManager.setpreviousValueId(objItemDetailBo.ValueId)

                        Else
                            Dim previousValueId As String = SessionManager.getpreviousValueId()
                            Dim valueId As String = objItemDetailBo.ValueId
                            If previousValueId > 0 And previousValueId <> valueId Then

                                Dim loadComponentDv As DataView = New DataView()
                                Dim dtLoadComponent As DataTable = New DataTable()
                                dtLoadComponent = parameterDataSet.Tables(ApplicationConstants.plannedComponent)
                                loadComponentDv = dtLoadComponent.AsDataView()
                                loadComponentDv.RowFilter = "VALUEID =  " + previousValueId.ToString() + " And SubValue= " + valueId.ToString()
                                dtLoadComponent = loadComponentDv.ToTable()
                                If Not dtLoadComponent Is Nothing And dtLoadComponent.Rows.Count > 0 Then
                                    Dim ipreviousResult = (From ps In dtLoadComponent Where ps.Item("VALUEID") = previousValueId And ps.Item("SubValue") = valueId Select ps)
                                    If ipreviousResult.Count > 0 Then

                                        Dim plannedComponentId As Integer = ipreviousResult.First.Item("COMPONENTID")
                                        SessionManager.setPlannedComponentId(plannedComponentId)
                                    End If
                                End If
                            Else
                                Dim iResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("VALUEID") = valueId And ps.Item("SubParameter") Is DBNull.Value And ps.Item("SubValue") Is DBNull.Value Select ps)
                                If iResult.Count > 0 Then

                                    Dim plannedComponentId As Integer = iResult.First.Item("COMPONENTID")
                                    SessionManager.setPlannedComponentId(plannedComponentId)
                                End If
                            End If
                        End If

                    End If

                ElseIf ControlType.ToUpper() = "Date".ToUpper() Then
                    ' Dim attributeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.parameters) Where ps.Item("ParameterID").ToString() = parameterId Select ps)
                    Dim txtDate As TextBox
                    If parameterName.Contains("Rewire Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Upgrade Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Cur Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Replacement Due") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.replacementDue.ToString()), TextBox)
                    ElseIf parameterName.Contains("Last Rewired") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString()), TextBox)
                    ElseIf parameterName.Contains("CUR Last Replaced") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.curLastReplaced.ToString()), TextBox)
                    ElseIf parameterName.Contains("Upgrade Last Done") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString()), TextBox)
                    ElseIf parameterName.Contains("Last Replaced") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastReplaced.ToString()), TextBox)
                    ElseIf parameterName.Contains("Original Install Date") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.installedDate.ToString()), TextBox)
                    ElseIf parameterName.Contains("Gasket Set Replacement") Then
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + ApplicationConstants.gasketSetReplacement.ToString()), TextBox)
                    Else
                        txtDate = CType(detailsTabID.FindControl(ApplicationConstants.txtDate + parameterId.ToString()), TextBox)

                    End If



                    objItemDatesControlBo.ParameterId = parameterId
                    objItemDatesControlBo.ParameterName = parameterName
                    Dim dateTime As Date
                    Dim valid As Boolean = Date.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)

                    If valid = True OrElse Not (txtDate.Enabled) Then
                        If parameterName.Contains("Due") Then
                            If parameterName.Contains("Rewire Due") Or parameterName.Contains("Upgrade Due") Or parameterName.Contains("Cur Due") _
                                Or parameterName.ToUpper() = "Replacement Due".ToUpper() Then
                                itemDateList(itemDateList.LongCount - 1).DueDate = txtDate.Text
                                itemDateList(itemDateList.LongCount - 1).ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList(itemDateList.LongCount - 1).ParameterName = parameterName
                                If parameterName.ToUpper() = "Replacement Due".ToUpper() Then
                                    itemDateList(itemDateList.LongCount - 1).ParameterId = 0
                                End If
                            Else
                                objItemDatesControlBo.DueDate = txtDate.Text
                                objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList.Add(objItemDatesControlBo)
                            End If
                        ElseIf parameterName.Contains("Last Rewired") Or parameterName.Contains("Upgrade Last Done") Or parameterName.Contains("CUR Last Replaced") _
                            Or parameterName.ToUpper() = "Last Replaced".ToUpper() Then
                            objItemDatesControlBo.LastDone = txtDate.Text
                            objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                            itemDateList.Add(objItemDatesControlBo)
                        Else
                            objItemDatesControlBo.LastDone = String.Empty
                            objItemDatesControlBo.DueDate = txtDate.Text
                            itemDateList.Add(objItemDatesControlBo)
                        End If

                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.Message = UserMessageConstants.ValidDateFormat + parameterName
                        Exit Sub
                    End If

                    ' Dim selectedvalue As String = ddlParameter.SelectedValue
                ElseIf ControlType.ToUpper() = "Textbox".ToUpper() Then
                    Dim txtBox As TextBox = CType(detailsTabID.FindControl(ApplicationConstants.txt + parameterId.ToString()), TextBox)
                    If Not txtBox.Text Is Nothing Then
                        objItemDetailBo.ParameterValue = txtBox.Text
                        itemDetailList.Add(objItemDetailBo)
                    End If

                ElseIf ControlType.ToUpper() = "TextArea".ToUpper() Then
                    Dim txtBox As TextBox = CType(detailsTabID.FindControl(ApplicationConstants.txtArea + parameterId.ToString()), TextBox)
                    If conditionValueId > 0 Then
                        If Not txtBox.Text Is Nothing Then
                            objItemDetailBo.ParameterValue = txtBox.Text
                            itemDetailList.Add(objItemDetailBo)
                        End If
                        Dim attrId As Integer = SessionManager.getAttributrTypeId()
                        If attrId > 0 AndAlso itemDateList.Count > 0 Then

                            If itemName.ToUpper() = "Heating".ToUpper() Then
                                Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                                Dim itm As ItemDatesControlBO = itemDateList.Find(Function(c) c.ComponentId > 0)
                                If (Not IsNothing(itm)) Then
                                    objConditionWorkBO.ComponentId = itm.ComponentId
                                End If
                                objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                                objConditionWorkBO.ValueId = conditionValueId
                                conditionRatingList.Add(objConditionWorkBO)
                            Else
                                For Each itemDate As ItemDatesControlBO In itemDateList

                                    Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                                    If itemDate.ComponentId > 0 Then
                                        objConditionWorkBO.ComponentId = itemDate.ComponentId
                                    End If
                                    objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                                    objConditionWorkBO.ValueId = conditionValueId
                                    conditionRatingList.Add(objConditionWorkBO)
                                Next
                            End If

                        Else
                            Dim objConditionWorkBO As ConditionRatingBO = New ConditionRatingBO()
                            ' objConditionWorkBO.ComponentId = DBNull.Value
                            objConditionWorkBO.WorksRequired = txtBox.Text.Trim
                            objConditionWorkBO.ValueId = conditionValueId
                            conditionRatingList.Add(objConditionWorkBO)
                        End If


                    End If
                ElseIf ControlType.ToUpper() = "Checkboxes".ToUpper() Then
                    Dim chkList As CheckBoxList = CType(detailsTabID.FindControl(ApplicationConstants.chkList + parameterId.ToString()), CheckBoxList)
                    For Each item As ListItem In chkList.Items
                        Dim objSubItemDetailBO As ItemDetailBO = New ItemDetailBO()
                        objSubItemDetailBO.ItemParamId = dr.Item("ItemParamID")
                        objSubItemDetailBO.ParameterValue = item.Text
                        objSubItemDetailBO.ValueId = item.Value
                        If item.Selected Then
                            objSubItemDetailBO.IsCheckBoxSelected = True
                        Else
                            objSubItemDetailBO.IsCheckBoxSelected = False
                        End If
                        itemDetailList.Add(objSubItemDetailBO)
                    Next

                ElseIf ControlType.ToUpper() = "Radiobutton".ToUpper() Then
                    Dim rdBtn As RadioButtonList = CType(detailsTabID.FindControl(ApplicationConstants.rdBtn + parameterId.ToString()), RadioButtonList)
                    For Each item As ListItem In rdBtn.Items
                        If item.Selected Then
                            objItemDetailBo.ParameterValue = item.Text
                            objItemDetailBo.ValueId = item.Value
                            itemDetailList.Add(objItemDetailBo)
                            If item.Text.ToUpper = ApplicationConstants.conditionUnsatisfactory.ToUpper Or item.Text.ToUpper = ApplicationConstants.conditionPotentiallyUnsatisfactory.ToUpper Then
                                conditionValueId = item.Value
                            Else
                                conditionValueId = 0

                            End If
                        End If
                    Next
                End If
            Next
        End If

    End Sub

#End Region

    Public Sub displayPhotoIcon(ByVal visible As Boolean)
        If visible = False Then
            lnkBtnPhotographsTab.Text = "Photographs:"
        Else
            lnkBtnPhotographsTab.Text = "Photographs:<img src='../../Images/CameraIcon.png'  width='16px' height='12px' /> "
        End If
        ' imgCamera.Visible = visible
    End Sub
#End Region

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim pnlDetailMsg As Panel = CType(detailsTabID.FindControl("pnlMessage"), Panel)
        Dim lblDetailMsg As Label = CType(detailsTabID.FindControl("lblMessage"), Label)
        Try
            mdlPopUpDelete.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblDetailMsg, pnlDetailMsg, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim pnlDetailMsg As Panel = CType(detailsTabID.FindControl("pnlMessage"), Panel)
        Dim lblDetailMsg As Label = CType(detailsTabID.FindControl("lblMessage"), Label)
        Try
            If SessionManager.getHeatingMappingId() > 0 Then
                Dim objPropertyBL As PropertyBL = New PropertyBL()
                objPropertyBL.deleteHeating(Convert.ToInt32(SessionManager.getHeatingMappingId()))
                Dim proerptyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
                Dim itemId As Integer = Convert.ToInt32(SessionManager.getTreeItemId())
                Dim resultDataSet As DataSet = New DataSet()
                objPropertyBL.getItemDetail(itemId, proerptyId, resultDataSet)

                SessionManager.setAttributeDetailDataSet(resultDataSet)
                SessionManager.removeHeatingFuelId()
                SessionManager.removeheatingMapping()
                SessionManager.removeHeatingMappingId()
                RaiseEvent Amend_Clicked(sender, e)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = "No heating selected for delete."
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblDetailMsg, pnlDetailMsg, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
End Class