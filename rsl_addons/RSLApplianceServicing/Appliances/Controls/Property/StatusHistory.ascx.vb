﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports AjaxControlToolkit
Imports System.Globalization


Public Class StatusHistory
    Inherits UserControlBase

#Region "Properties"
    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Dim propertyId As String
#End Region

#Region "Events"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.propertyId = Request.QueryString(PathConstants.PropertyIds)

        Dim resultDataSet As DataSet = objPropertyBL.getStatusHistory(propertyId)
        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            grdVwStatusHistory.DataSource = resultDataSet
            grdVwStatusHistory.DataBind()
        End If
    End Sub
#End Region

End Class