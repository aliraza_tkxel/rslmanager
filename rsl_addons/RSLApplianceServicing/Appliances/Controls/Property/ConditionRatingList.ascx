﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConditionRatingList.ascx.vb"
    Inherits="Appliances.ConditionRatingList" %>
<asp:Panel ID="pnlConditionRating" runat="server">
    <div class="conditionRatingPanel">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"> </asp:Label>
        </asp:Panel>
        <div class="mainheading-panel" style="margin-top: 2px; width: 99%;">
            Condition Ratings:
            <asp:Button ID="btnBackToSummary" runat="server" Text="< Back To Summary" CssClass="rightControl" />
            <asp:Button ID="btnPrint" Text="Print" runat="server" CssClass="conditionratinglist-rightControl" OnClientClick="PrintConditionList('dvConditionRating')" />
        </div>
        <div style="clear: both;">
        </div>
        <hr />
        <div id="dvConditionRating" style="height: 510px; overflow: auto;">
            <asp:GridView ID="grdConditionRatings" runat="server" AutoGenerateColumns="False"
                HeaderStyle-CssClass="gridfaults-header" ShowHeader="true" Width="100%" GridLines="None"
                CellPadding="3" CellSpacing="5" AllowSorting="true">
                <Columns>
                    <asp:BoundField DataField="Location" HeaderStyle-HorizontalAlign="Left" HeaderText="Location"
                        ItemStyle-Width="35%" SortExpression="Location" />
                    <asp:BoundField DataField="Component" HeaderStyle-HorizontalAlign="Left" HeaderText="Component"
                        ItemStyle-Width="30%" SortExpression="Component" />
                    <asp:BoundField DataField="Condition" HeaderStyle-HorizontalAlign="Left" HeaderText="Condition"
                        ItemStyle-Width="10%" SortExpression="Condition" />
                    <asp:BoundField DataField="LastReplaced" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                        HeaderText="Last Replaced" ItemStyle-Width="10%" SortExpression="LastReplaced" />
                    <asp:BoundField DataField="DueDate" HeaderStyle-HorizontalAlign="Center" HeaderText="Replacement Due"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" SortExpression="DueDate" />
                    <asp:BoundField DataField="By" HeaderStyle-HorizontalAlign="Left" HeaderText="Last updated by"
                        ItemStyle-Width="10%" SortExpression="By" />
                </Columns>
                <EmptyDataTemplate>
                    No Records Found</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
</asp:Panel>
