﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Globalization
Imports System.Data.OleDb



Public Class AnnualRentTab
    Inherits UserControlBase
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            pnlAnnualRentTab.Enabled = False
        End If
        uiMessageHelper.resetMessage(lblAnnualRentTab, pnlAnnualRentMsg)


    End Sub
    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.uploadAndDisplayAnnualRent()

    'End Sub

#End Region
#Region "Populate Annual Rent Tab"
    Sub populateAnnualRentTab()
        txtAnnualRentDate.Text = ConfigurationManager.AppSettings("RentStartDate").ToString() + Now.Year().ToString()
        txtAnnualEffectiveDate.Text = ConfigurationManager.AppSettings("RentEndDate").ToString() + Now.Year().ToString()

        Dim propertyId = getPropertyIdfromQueryString()

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getRentTypeAndFinancialInfomration(resultDataSet, propertyId)

        If Not IsNothing(propertyId) And resultDataSet.Tables("RentAnnual").Rows.Count > 0 Then

            If (IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("DATERENTSET"))) Then
                txtAnnualRentDate.Text = ""
            Else
                txtAnnualRentDate.Text = resultDataSet.Tables("RentAnnual").Rows(0)("DATERENTSET")
            End If

            If (IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("RENTEFFECTIVE"))) Then
                txtAnnualEffectiveDate.Text = ""
            Else
                txtAnnualEffectiveDate.Text = resultDataSet.Tables("RentAnnual").Rows(0)("RENTEFFECTIVE")
            End If
            If (IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("UPLOADDATE"))) Then
                txtFileUploaded.Text = ""
            Else
                txtFileUploaded.Text = resultDataSet.Tables("RentAnnual").Rows(0)("UPLOADDATE")
            End If

            'txtAnnualRent.Text = resultDataSet.Tables("RentAnnual").Rows(0)("RENT").ToString()
            'txtAnnualServices.Text = resultDataSet.Tables("RentAnnual").Rows(0)("SERVICES").ToString()
            'txtAnnualCouncilTax.Text = resultDataSet.Tables("RentAnnual").Rows(0)("COUNCILTAX").ToString()
            'txtAnnualIntelig.Text = resultDataSet.Tables("RentAnnual").Rows(0)("INELIGSERV").ToString()
            'txtAnnualSupp.Text = resultDataSet.Tables("RentAnnual").Rows(0)("SUPPORTEDSERVICES").ToString()
            'txtAnnualWater.Text = resultDataSet.Tables("RentAnnual").Rows(0)("WATERRATES").ToString()
            'txtAnnualTotalRent.Text = resultDataSet.Tables("RentAnnual").Rows(0)("TOTALRENT").ToString()
            If Not IsNothing(propertyId) And resultDataSet.Tables("RentAnnual").Rows.Count > 0 Then
                txtAnnualRent.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("RENT")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("RENT")), "0.00")
                txtAnnualServices.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("SERVICES")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("SERVICES")), "0.00")
                txtAnnualCouncilTax.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("COUNCILTAX")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("COUNCILTAX")), "0.00")
                txtAnnualWater.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("WATERRATES")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("WATERRATES")), "0.00")
                txtAnnualIntelig.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("INELIGSERV")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("INELIGSERV")), "0.00")
                txtAnnualSupp.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("SUPPORTEDSERVICES")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("SUPPORTEDSERVICES")), "0.00")
                'txtTargetGarage.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("GARAGE")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("GARAGE")), "0.00")
                txtAnnualTotalRent.Text = If(Not IsDBNull(resultDataSet.Tables("RentAnnual").Rows(0)("TOTALRENT")), String.Format("{0:F2}", resultDataSet.Tables("RentAnnual").Rows(0)("TOTALRENT")), "0.00")

            End If


            txtAnnualRentDate.Text = Convert.ToDateTime(resultDataSet.Tables("RentAnnual").Rows(0)("REASSESMENTDATE")).ToString("MM/dd/yyyy")
            txtAnnualEffectiveDate.Text = Convert.ToDateTime(resultDataSet.Tables("RentAnnual").Rows(0)("RENTEFFECTIVE")).ToString("MM/dd/yyyy")
        End If

    End Sub
#End Region

#Region "Get Property Id from Query String"
    ''' <summary>
    ''' Get propertyId from query string, using the key stored in pathconstants (PathConstants.PropertyIds = "id")
    ''' </summary>
    ''' <param name="propertyId">Optional propertyId by ref to store propertyid.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getPropertyIdfromQueryString(Optional ByRef propertyId As String = Nothing) As String
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            propertyId = CType(Request.QueryString(PathConstants.PropertyIds), String)
        End If
        Return propertyId
    End Function
#End Region

#Region "Get Annual Rent Information From Excel Sheet"
    'Public Sub uploadAndDisplayAnnualRent()
    '    Try
    '        'File storage path
    '        Dim filePath As String = Server.MapPath("~/AnnualReports/" + txtUploadFile.FileName)
    '        If (txtUploadFile.HasFile) Then
    '            'Saving file
    '            txtUploadFile.SaveAs(filePath)
    '            'Open the connection with excel file based on excel version
    '            Dim dbConnection As OleDbConnection = Nothing
    '            If IO.Path.GetExtension(txtUploadFile.FileName) = ".xls" Then
    '                dbConnection = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filePath & ";Extended Properties=Excel 8.0;")
    '            ElseIf IO.Path.GetExtension(txtUploadFile.FileName) = ".xlsx" Then
    '                dbConnection = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filePath & ";Extended Properties=Excel 12.0;")
    '            End If
    '            dbConnection.Open()
    '            'Get first sheet name
    '            Dim getExcelSheetName As String = dbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing).Rows(0)("Table_Name").ToString()
    '            'Select rows from first sheet in excel sheet and fill into dataset
    '            Dim ExcelCommand As New OleDbCommand("SELECT * FROM [" & getExcelSheetName & "]", dbConnection)
    '            Dim ExcelAdapter As New OleDbDataAdapter(ExcelCommand)
    '            'Getting the sheet data
    '            Dim dtExcelData As New DataTable("AnnualRentInfo")
    '            ExcelAdapter.Fill(dtExcelData)
    '            dbConnection.Close()
    '            'Populating the Form
    '            Dim drAnnualRent() As DataRow = dtExcelData.Select("PropertyId='" + SessionManager.getPropertyId() + "'")
    '            If (drAnnualRent.Length > 0) Then
    '                txtAnnualRent.Text = drAnnualRent(0).Item(2).ToString()
    '                txtAnnualServices.Text = drAnnualRent(0).Item(3).ToString()
    '                txtAnnualCouncilTax.Text = drAnnualRent(0).Item(4).ToString()
    '                txtAnnualIntelig.Text = drAnnualRent(0).Item(6).ToString()
    '                txtAnnualSupp.Text = drAnnualRent(0).Item(7).ToString()
    '                txtAnnualWater.Text = drAnnualRent(0).Item(5).ToString()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If
    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblAnnualRentTab, pnlAnnualRentMsg, uiMessageHelper.Message, True)
    '        End If
    '    End Try

    'End Sub


#End Region

#End Region
End Class