﻿Imports AS_Utilities
Imports AS_BusinessObject
Imports AS_BusinessLogic
Imports System.Reflection
Imports System.Globalization

Public Class RoomsDimensions
    Inherits UserControlBase

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#Region "Functions"

#Region "Populate Rooms Dimensions"
    ''' <summary>
    ''' This function is used to load property Rooms Dimensions on demands only from parent control.
    ''' </summary>
    ''' <param name="propertyId">PropertyId is optional, if not provided the function will check for propertyId in query string with key "Id"</param>
    ''' <remarks></remarks>
    Public Sub populateRoomsDimensions(Optional ByVal propertyId As String = Nothing)
        If propertyId Is Nothing Then
            propertyId = getPropertyIdfromQueryString()
            If propertyId Is Nothing Then
                Throw New Exception(UserMessageConstants.InvalidPropertyId)
            End If
        End If

        Dim objPropertyBL As New PropertyBL()

        Dim dataset As DataSet = objPropertyBL.getRoomsDimensions(propertyId)
        grdRoomsDimensions.DataSource = dataset
        grdRoomsDimensions.DataBind()
    End Sub
#End Region

#Region "Get Property Id from Query String"
    ''' <summary>
    ''' Get propertyId from query string, using the key stored in pathconstants (PathConstants.PropertyIds = "id")
    ''' </summary>
    ''' <param name="propertyId">Optional propertyId by ref to store propertyid.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getPropertyIdfromQueryString(Optional ByRef propertyId As String = Nothing) As String
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            propertyId = CType(Request.QueryString(PathConstants.PropertyIds), String)
        End If
        Return propertyId
    End Function
#End Region

#End Region

End Class