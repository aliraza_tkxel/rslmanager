﻿Imports System.IO
Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class DefectImages
    Inherits UserControlBase
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Public uploadedThumbUri As String = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/"
    'Public uploadedThumbUri As String = "../../Photographs/Images/"

#Region "Control Events"

    Public Event cancelButton_Clicked(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
    End Sub
#End Region

#Region "Load Defect Detail Images"
    Public Sub LoadImageData(ByVal propertyId As String)
        Dim photosDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyDefectImages(propertyId, photosDataSet)
        lstViewPhotos.DataSource = photosDataSet
        lstViewPhotos.DataBind()
    End Sub
#End Region

#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotos.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                Dim imagePath = uploadedThumbUri + fileName

                imgDynamicImage.ImageUrl = imagePath

                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnRefresh click event"
    ''' <summary>
    ''' btnRefresh click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefresh.Click
        Try
            Dim resultdataSet As DataSet = New DataSet()
            Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
            objPropertyBl.getPropertyDefectImages(propertyId, resultdataSet)
            lstViewPhotos.DataSource = resultdataSet
            lstViewPhotos.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnCancel click event"
    ''' <summary>
    ''' btnCancel click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            RaiseEvent cancelButton_Clicked(sender, e)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub HidePotoPanel(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mdlPopUpViewImage.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
End Class