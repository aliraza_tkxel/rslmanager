﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CurrentRentTab.ascx.vb"
    Inherits="Appliances.CurrentRentTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Import Namespace="AS_Utilities" %>
<style type="text/css">
    .style7
    {
        width: 404px;
        text-align: left;
        padding-left: 5px;
    }
    
    .popupTabletr
    {
    }
    
    .popupTabletd
    {
    }
</style>
<style type="text/css">
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 190px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
</style>
<div style="float: left; width: 100%; margin-left: 5px;">
    <asp:Panel ID="pnlCurrentRentTab" runat="server" BackColor="White" Width="400px">
        <table id="tblCurrentRentTab">
            <tr>
                <td style="width: 30%">
                </td>
                <td style="width: 20%">
                </td>
                <td style="width: 25%">
                </td>
                <td style="width: 25%">
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" colspan="4">
                    <div style='margin-left: 15px;'>
                        <asp:Panel ID="pnlCurrentRentMessage" runat="server" Visible="false" Height="19px"
                            Width="289px">
                            <asp:Label ID="lblCurrentRentTab" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Rent Frequency:
                </td>
                <td align="left" valign="top" colspan="3">
                    <asp:DropDownList ID="ddlRentFrequency" runat="server" class="selectval" CssClass="dd-list">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Date Rent Set:
                </td>
                <td align="left" valign="top" colspan="3">
                    <asp:TextBox ID="txtRentDate" runat="server" Width="150px"></asp:TextBox>
                    <asp:CalendarExtender ID="txtRentDate_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="imgRentCalendar" TargetControlID="txtRentDate" />
                    <asp:Image ID="imgRentCalendar" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                    <asp:CompareValidator runat="server" ID="CompareValidator11" ControlToValidate="txtRentDate"
                        Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Rent effective to:<span class="Required"></span>
                </td>
                <td align="left" valign="top" colspan="3">
                    <asp:TextBox ID="txtEffectiveDate" runat="server" Width="150px"></asp:TextBox>
                    <asp:CalendarExtender ID="txtEffectiveDate_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="imgCalendarRentEffective" TargetControlID="txtEffectiveDate" />
                    <asp:Image ID="imgCalendarRentEffective" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                    <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtEffectiveDate"
                        Operator="DataTypeCheck" Type="Date" ErrorMessage="Enter valid date." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Rent Type:<span class="Required"></span>
                </td>
                <td align="left" valign="top" colspan="3">
                    <asp:DropDownList ID="ddlRentType" runat="server" Style="width: 167px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Rent
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtRent" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtRent"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtRent"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
                <td valign="top">
                    Services
                </td>
                <td align="left" class="style3" valign="top">
                    <asp:TextBox ID="txtServices" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator3" ControlToValidate="txtServices"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtServices"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding-left: 2px;">
                    Council Tax
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtCouncilTax" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator4" ControlToValidate="txtCouncilTax"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtCouncilTax"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
                <td valign="top">
                    Water
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtWater" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator5" ControlToValidate="txtWater"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtWater"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="padding-left: 2px;">
                    Inelig.
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtIntelig" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator6" ControlToValidate="txtIntelig"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtIntelig"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
                <td align="left" class="" valign="top">
                    Supp
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtSupp" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator7" ControlToValidate="txtSupp"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtSupp"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left: 2px;" valign="top">
                    Garage.
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtGarage" runat="server" Width="50px" onblur="calcrent()">0.00</asp:TextBox>
                    <asp:CompareValidator runat="server" ID="CompareValidator8" ControlToValidate="txtGarage"
                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed." Style="float: right;"
                        CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                    </asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtGarage"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                </td>
                <td align="right" class="style3" style="text-align: right;" valign="top">
                    &nbsp;
                </td>
                <td align="right" class="style3" style="text-align: right;" valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr bgcolor="#133E71">
                <td align="left" class="style7" valign="top">
                    <strong style="color: #fff">Total Rent</strong>
                </td>
                <td align="left" valign="top" colspan="2">
                    <asp:TextBox ID="txtTotalRent" runat="server" Width="50px">0.00</asp:TextBox>
                </td>
                <td align="center" style="text-align: center;" valign="top">
                    <asp:Button ID="btnSaveCurrentRent" runat="server" BackColor="White" Text="Save Current Rent"
                        ValidationGroup="validate" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
