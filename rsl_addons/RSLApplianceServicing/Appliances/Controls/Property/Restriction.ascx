﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Restriction.ascx.vb" Inherits="Appliances.Restriction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/Controls/Common/UIMessage.ascx" %>

<asp:UpdatePanel runat="server" ID="updPnlMainDetail">
    <ContentTemplate>
        <br />
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <br />
        <div class="table-container block-main">
            <div style="padding: 10px; width: 60%">
                <table id="tblMainDetails">
                    <tr>
                        <td valign="top">
                            Permitted planning use:
                        </td>
                        <td>
                             <asp:TextBox ID="txtPermittedPlanning" runat="server" Height="70px" Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtPermittedPlanning"
                                ID="RegularExpressionValidator1" ValidationExpression="^[\s\S]{0,500}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 500 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Relevant planning restrictions:
                        </td>
                        <td>
                             <asp:TextBox ID="txtRelevantPlanning" runat="server" Height="70px" Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtRelevantPlanning"
                                ID="RegularExpressionValidator2" ValidationExpression="^[\s\S]{0,500}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 500 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Relevant title restrictions and issues:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRelevantTitle" runat="server" Height="70px" Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtRelevantTitle"
                                ID="RegularExpressionValidator3" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Detail comments on restrictions:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDetailComments" runat="server" Height="70px" 
                                Style="width: 305px;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtDetailComments"
                                ID="RegularExpressionValidator4" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Access issues:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAccessIssues" runat="server" Height="70px"  Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtAccessIssues"
                                ID="RegularExpressionValidator5" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Drainage or other service media issues:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMediaIssues" runat="server" Height="70px" Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtMediaIssues"
                                ID="RegularExpressionValidator6" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Any third party management agreement:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlThirdParty" Height="23px" Width="310px">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            SP or other non-HCA funding arrangements:
                        </td>
                        <td>
                            <asp:TextBox ID="txtSpFunding" runat="server" Height="70px"  Style="width: 305px;"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtSpFunding"
                                ID="RegularExpressionValidator7" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Registered with land registry:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlIsRegistered" Height="23px" Width="310px">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Management company details:
                        </td>
                        <td>
                            <asp:TextBox ID="txtManagementDetail" runat="server" Height="70px"
                                Style="width: 305px;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtManagementDetail"
                                ID="RegularExpressionValidator8" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Non BHA building insurance details:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNonBhaDetails" runat="server" Height="70px" 
                                Style="width: 305px;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNonBhaDetails"
                                ID="RegularExpressionValidator9" ValidationExpression="^[\s\S]{0,1000}$" runat="server"
                                ValidationGroup="saveRestriction" CssClass="Required" ErrorMessage="Maximum 1000 characters allowed."></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button runat="server" ID="btnSaveRestriction" Text="Save Restriction" Style="float: right;"
                                OnClick="btnSaveRestriction_Click" ValidationGroup="saveRestriction" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>