﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractorJobSheetSummary.ascx.vb"
    Inherits="Appliances.ContractorJobSheetSummary" %>

<asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopup">
    <div id="header-Popup">
        <p id="header_label">
            BHG Fault Locator
        </p>
        <div id="header_box">
        </div>
    </div>
    <br style="border-style: none; border-color: #FFFFFF">
    <div style="height: auto; overflow: auto; clear: both;">
        <table style="width: 100%; margin-top: 10px;">
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                        Font-Bold="true"></asp:Label>
                    <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;,
                    <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel :" Font-Bold="true"> </asp:Label><asp:Label
                        ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="lblFaultLogID" runat="server" Visible="false" />
                    <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static" Font-Bold="True"></asp:Label>
                </td>
                <td width="40%">
                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Font-Bold="True">Status : </asp:Label>
                    <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td rowspan="10" width="44%" valign="top">
                    <div style='width: 90%;'>
                        <asp:Panel ID="pnlPropertyData" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tel : &nbsp;<asp:Label ID="lblClientTelPhoneNumber" runat="server" Text=" " Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mobile: &nbsp;
                                        <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email :
                                        <asp:Label ID="lblClientEmail" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Contractor :
                </td>
                <td>
                    <asp:Label ID="lblContractor" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Operative :
                </td>
                <td>
                    <asp:Label ID="lblOperativeName" runat="server">-</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Priority :
                </td>
                <td>
                    <asp:Label ID="lblPriority" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Completion due :
                </td>
                <td>
                    <asp:Label ID="lblCompletionDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Order date :
                </td>
                <td>
                    <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Location :
                </td>
                <td>
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Description:
                </td>
                <td>
                    <asp:Label ID="lblDescription" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Trade :
                </td>
                <td>
                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Note :
                </td>
                <td>
                    <asp:TextBox ID="lblNotes" CssClass="roundcornerby5" runat="server" Enabled="false"
                        TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <b>Appointment:</b>
                </td>
                <td valign="top">
                    <asp:Label ID="lblTime" runat="server" Text="N/A"></asp:Label><br />
                    <br />
                </td>
                <td>
                    <b>Asbestos:</b>
                    <div style="height: 50px; overflow: auto;">
                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            BorderStyle="None" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="float:left; ">
                        <div class="text_a_r_padd__r_20px">
                            <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" CssClass="margin_right20"
                                Width="100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
