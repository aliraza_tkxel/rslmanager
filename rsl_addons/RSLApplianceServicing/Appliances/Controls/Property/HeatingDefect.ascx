﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="HeatingDefect.ascx.vb"
    Inherits="Appliances.HeatingDefect" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="defects" TagName="ServiceDefects" Src="~/Controls/Property/ServiceDefects.ascx" %>
<%@ Register TagPrefix="dphotos" TagName="DefectPhotographs" Src="~/Controls/Property/DefectPhotographs.ascx" %>
<%@ Register TagPrefix="defect" TagName="DefectManegement" Src="~/Controls/DefectManagement/DefectManagement.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .style3
    {
        width: 283px;
    }
    .style5
    {
        width: 221px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
</style>
<div style="margin-top: 10px">
    <%-- <asp:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </asp:ToolkitScriptManager>--%>
    <asp:UpdatePanel runat="server" ID="updPnlDefectsTab">
        <ContentTemplate>
            <div style="float: right; margin-top: -15px; margin-bottom: 8px;">
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div style="clear: both;">
            </div>
            <asp:Button ID="btnAddDefect" runat="server" Text="Add Appliance Defect" Style="float: right;
                margin-top: 5px;" BackColor="White" />
            <div>
                <asp:LinkButton ID="lnkBtnServDefectTab" OnClick="lnkBtnServDefectTab_Click" CssClass="TabInitial"
                    runat="server" BorderStyle="Solid" BorderColor="Black">Service Defects: </asp:LinkButton>
                <asp:LinkButton ID="lnkBtnPhotographTab" OnClick="lnkBtnPhotographTab_Click" CssClass="TabInitial"
                    runat="server" BorderStyle="Solid" BorderColor="Black">Photographs: </asp:LinkButton>
                <%--<div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                    width: 205px;">
                </div>--%>
                <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                    width: 100%;">
                </div>
                <div style="clear: both; margin-bottom: 5px;">
                </div>
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <defects:ServiceDefects runat="server" ID="serviceDefects" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <dphotos:DefectPhotographs runat="server" ID="dphotosTab" />
                    </asp:View>
                </asp:MultiView>
            </div>
            <asp:Label ID="lblDispAppliance" runat="server"></asp:Label>
            <asp:Panel ID="pnlAddDefect" runat="server" BackColor="White" Width="600px" Style="max-height: 600px;
                padding: 10px;" BorderColor="Black" BorderStyle="Outset" BorderWidth="1px">
                <defect:DefectManegement runat="server" ID="ucDefectManagement" />
            </asp:Panel>
            <asp:CheckBox ID="ckBoxPhotoUpload" runat="server" AutoPostBack="True" Visible="true"
                CssClass="hiddenField" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPoPUpAddDefect" runat="server" Enabled="True"
                TargetControlID="lblDispDefect" PopupControlID="pnlAddDefect" DropShadow="true"
                BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Label ID="lblDispDefect" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
