﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AjaxControlToolkit

Public Class ContractorJobSheetSummary
    Inherits UserControlBase

#Region "Properties/Attrivutes"
    Dim objPropertyBL As PropertyBL = New PropertyBL()
#End Region

#Region "Events"

#Region "Page Load event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#Region "Back button click event"
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

        Dim mdlPopupContractorJobsheet As ModalPopupExtender = DirectCast(Me.Parent.FindControl("mdlPopupContractorJobsheet"), ModalPopupExtender)
        mdlPopupContractorJobsheet.Hide()
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Show Job Sheet Summary Sub Contractor Update"

    Public Sub showPopupJobSheetSummary(ByRef faultHistoryId As Int32)
        Try
            Me.resetControls()
            Me.populateJobSheetSummary(faultHistoryId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Reset All Controls to default values"

    Private Sub resetControls()
        lblStatus.Visible = True

        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        lblFaultLogID.Text = String.Empty
        lblJSN.Text = String.Empty
        lblStatus.Text = String.Empty
        lblContractor.Text = String.Empty
        lblPriority.Text = String.Empty
        lblCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblLocation.Text = String.Empty
        lblDescription.Text = String.Empty
        lblTrade.Text = String.Empty
        lblNotes.Text = String.Empty
        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmail.Text = String.Empty

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()

    End Sub

#End Region

#Region "Get Job Sheet Summary Detail"

    Private Sub populateJobSheetSummary(ByRef faultHistoryId As Int32)

        Dim dsJobSheetSummary As New DataSet()
        objPropertyBL.getContractorJobsheetDetail(faultHistoryId, dsJobSheetSummary)

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)

                lblFaultLogID.Text = .Item("FaultLogID")
                lblJSN.Text = .Item("JSN")
                lblStatus.Text = .Item("FaultStatus")
                lblContractor.Text = .Item("Contractor")
                lblPriority.Text = .Item("priority")
                lblCompletionDateTime.Text = .Item("completionDue")
                lblOrderDate.Text = .Item("orderDate")
                lblLocation.Text = .Item("Location")
                lblDescription.Text = .Item("Description")
                lblTrade.Text = .Item("Trade")
                lblNotes.Text = .Item("Notes")

                lblClientName.Text = .Item("ClientName")
                lblClientNameHeader.Text = lblClientName.Text

                lblClientStreetAddress.Text = .Item("ClientStreetAddress")
                lblClientStreetAddressHeader.Text = lblClientStreetAddress.Text

                lblClientCity.Text = .Item("ClientCity")
                lblClientCityHeader.Text = lblClientCity.Text

                lblClientPostCode.Text = .Item("ClientPostCode")
                lblClientRegion.Text = .Item("ClientRegion")

                lblClientTelPhoneNumber.Text = .Item("ClientTel")
                lblClientTelPhoneNumberHeader.Text = lblClientTelPhoneNumber.Text

                lblClientMobileNumber.Text = .Item("ClientMobile")
                lblClientEmail.Text = .Item("ClientEmail")

            End With

        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

#End Region

#End Region


End Class