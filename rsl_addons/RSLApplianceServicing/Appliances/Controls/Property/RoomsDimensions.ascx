﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RoomsDimensions.ascx.vb"
    Inherits="Appliances.RoomsDimensions" %>
<style type="text/css">
    .dimensionsHeader
    {
        background-color: Gray;
        width: 100%;
        height: 20px;
    }
    .dimensionsHeaderText
    {
        font-weight: bold;
        vertical-align: middle;
        padding-left: 10px;
        line-height: 20px;
    }
    .roomsDimensions
    {
        width: 100%;
    }
    .superScript
    {
    }
</style>
<div>
    <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
        width: 94%;">
        Rooms Dimensions:
    </div>
    <div class="dimentionsDetail" style="margin-left: 5px; ">
        <asp:GridView runat="server" ID="grdRoomsDimensions" ShowHeaderWhenEmpty="false"
            CssClass="roomsDimensions" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
            EmptyDataText="No Room dimensions are available." GridLines="None">
            <Columns>
                <asp:BoundField DataField="RoomName" HeaderText=" " NullDisplayText="n/a" ShowHeader="False"
                    ItemStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="RoomWidth" HeaderText="Width" NullDisplayText="n/a" />
                <asp:BoundField DataField="RoomLength" HeaderText="Length" NullDisplayText="n/a" />
                <asp:TemplateField>
                    <HeaderTemplate>
                        Area (m<sup>2</sup></sapn>)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblRoomArea" Text='<%# String.Format("{0:0.00}",Eval("Area")) %>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle Height="20px" />
        </asp:GridView>
    </div>
</div>
