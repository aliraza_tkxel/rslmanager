﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Imports AS_BusinessLogic



Public Class Restriction
    Inherits UserControlBase
    Implements IAddEditPage



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not IsPostBack Then
            populateDropDowns()
            loadData()
        End If
        uiMessage.hideMessage()
    End Sub

    Protected Sub btnSaveRestriction_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Page.Validate("saveRestriction")

            If Page.IsValid Then
                saveData()
            End If

        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Public Sub saveData() Implements IAddEditPage.saveData
        Dim objRestrictionBO As RestrictionBO = New RestrictionBO()
        objRestrictionBO.RestrictionId = If(String.IsNullOrEmpty(btnSaveRestriction.CommandArgument), 0, Convert.ToInt32(btnSaveRestriction.CommandArgument))
        objRestrictionBO.PermittedPlanning = txtPermittedPlanning.Text
        objRestrictionBO.RelevantPlanning = txtRelevantPlanning.Text
        objRestrictionBO.RelevantTitle = txtRelevantTitle.Text
        objRestrictionBO.RestrictionComments = txtDetailComments.Text
        objRestrictionBO.AccessIssues = txtAccessIssues.Text
        objRestrictionBO.MediaIssues = txtMediaIssues.Text
        objRestrictionBO.ThirdPartyAgreement = If(ddlThirdParty.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValues), Nothing, ddlThirdParty.SelectedValue)
        objRestrictionBO.SpFundingArrangements = txtSpFunding.Text
        objRestrictionBO.IsRegistered = If(ddlIsRegistered.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValues), Nothing, Convert.ToInt32(ddlIsRegistered.SelectedValue))
        objRestrictionBO.ManagementDetail = txtManagementDetail.Text
        objRestrictionBO.NonBhaInsuranceDetail = txtNonBhaDetails.Text
        objRestrictionBO.UpdatedBy = SessionManager.getUserEmployeeId()

        If validateData(objRestrictionBO) Then
            objRestrictionBO.PropertyId = Request.QueryString(ApplicationConstants.Id)
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim result As Boolean = objPropertyBL.SaveRestriction(objRestrictionBO)

            If result Then
                btnSaveRestriction.CommandArgument = objRestrictionBO.RestrictionId.ToString()
                uiMessage.messageText = UserMessageConstants.RestrictionSavedSuccessfully
                uiMessage.showInformationMessage(uiMessage.messageText)
            Else
                uiMessage.messageText = UserMessageConstants.RestrictionNotSaved
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End If
    End Sub

    Public Function validateData(ByVal objRest As RestrictionBO) As Boolean
        Dim isValid As Boolean = True
        Dim message As String = String.Empty        

        If String.IsNullOrEmpty(Request.QueryString(ApplicationConstants.Id)) Then
            isValid = False
            message = UserMessageConstants.InvalidPropertyId
        ElseIf String.IsNullOrEmpty(objRest.PermittedPlanning) AndAlso String.IsNullOrEmpty(objRest.RelevantPlanning) _
            AndAlso String.IsNullOrEmpty(objRest.RelevantTitle) AndAlso String.IsNullOrEmpty(objRest.RestrictionComments) _
            AndAlso String.IsNullOrEmpty(objRest.AccessIssues) AndAlso String.IsNullOrEmpty(objRest.MediaIssues) _
            AndAlso String.IsNullOrEmpty(objRest.ThirdPartyAgreement) AndAlso String.IsNullOrEmpty(objRest.SpFundingArrangements) _
            AndAlso objRest.IsRegistered = 0 AndAlso String.IsNullOrEmpty(objRest.ManagementDetail) _
            AndAlso String.IsNullOrEmpty(objRest.NonBhaInsuranceDetail) Then
            isValid = False
            message = UserMessageConstants.FieldRestrictionCheck
        End If

        If isValid = False Then
            uiMessage.showErrorMessage(message)
        End If

        Return isValid
    End Function


    Public Sub loadData() Implements IAddEditPage.loadData
        resetControls()

        If String.IsNullOrEmpty(Request.QueryString(ApplicationConstants.Id)) Then
            uiMessage.showErrorMessage(UserMessageConstants.InvalidPropertyId)
        Else
            Dim propertyId As String = Request.QueryString(ApplicationConstants.Id)
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim dataset As DataSet = New DataSet()
            objPropertyBL.getRestrictions(dataset, propertyId)
            Dim dtRestriction = dataset.Tables(0)

            If dtRestriction.Rows.Count > 0 Then

                If Not String.IsNullOrEmpty(dtRestriction.Rows(0)("ThirdPartyAgreement").ToString()) Then
                    ddlThirdParty.SelectedValue = dtRestriction.Rows(0)("ThirdPartyAgreement").ToString()
                End If

                If Not IsDBNull(dtRestriction.Rows(0)("IsRegistered")) Then
                    ddlIsRegistered.SelectedValue = dtRestriction.Rows(0)("IsRegistered").ToString()
                End If

                txtPermittedPlanning.Text = dtRestriction.Rows(0)("PermittedPlanning").ToString()
                txtRelevantPlanning.Text = dtRestriction.Rows(0)("RelevantPlanning").ToString()
                txtRelevantTitle.Text = dtRestriction.Rows(0)("RelevantTitle").ToString()
                txtDetailComments.Text = dtRestriction.Rows(0)("RestrictionComments").ToString()
                txtAccessIssues.Text = dtRestriction.Rows(0)("AccessIssues").ToString()
                txtMediaIssues.Text = dtRestriction.Rows(0)("MediaIssues").ToString()
                txtSpFunding.Text = dtRestriction.Rows(0)("SpFundingArrangements").ToString()
                txtManagementDetail.Text = dtRestriction.Rows(0)("ManagementDetail").ToString()
                txtNonBhaDetails.Text = dtRestriction.Rows(0)("NonBhaInsuranceDetail").ToString()
                btnSaveRestriction.CommandArgument = dtRestriction.Rows(0)("RestrictionId").ToString()
            End If
        End If

    End Sub

    Public Sub loadDataReadOnlyView()

        loadData()

        ddlThirdParty.Enabled = False
        ddlIsRegistered.Enabled = False
        txtPermittedPlanning.ReadOnly = True
        txtRelevantPlanning.ReadOnly = True
        txtRelevantTitle.ReadOnly = True
        txtDetailComments.ReadOnly = True
        txtAccessIssues.ReadOnly = True
        txtMediaIssues.ReadOnly = True
        txtSpFunding.ReadOnly = True
        txtManagementDetail.ReadOnly = True
        txtNonBhaDetails.ReadOnly = True
        btnSaveRestriction.Visible = False

    End Sub



    Public Sub populateDropDowns() Implements IAddEditPage.populateDropDowns
        ddlThirdParty.Items.Clear()
        ddlIsRegistered.Items.Clear()
        Dim lstItems As List(Of ListItem) = New List(Of ListItem)()
        lstItems.Add(New ListItem("yes", "yes"))
        lstItems.Add(New ListItem("no", "no"))
        ddlThirdParty.DataSource = lstItems
        ddlThirdParty.DataBind()

        Dim defaultItem As ListItem = New ListItem("Please select", ApplicationConstants.DropDownDefaultValues)
        ddlThirdParty.Items.Insert(0, defaultItem)

        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim dataset As DataSet = New DataSet()
        objPropertyBL.getLandRegistrationOptions(dataset)
        ddlIsRegistered.DataSource = dataset
        ddlIsRegistered.DataValueField = "RegistryStatusId"
        ddlIsRegistered.DataTextField = "Description"
        ddlIsRegistered.DataBind()
        ddlIsRegistered.Items.Insert(0, defaultItem)
    End Sub

    Public Sub populateDropDown(ByVal ddl As DropDownList) Implements IAddEditPage.populateDropDown
        Throw New NotImplementedException()
    End Sub

    Public Function validateData() As Boolean Implements IAddEditPage.validateData
        Throw New NotImplementedException()
    End Function

    Public Sub setTextboxLength()
        txtPermittedPlanning.Attributes.Add("maxlength", txtPermittedPlanning.MaxLength.ToString())
        txtRelevantPlanning.Attributes.Add("maxlength", txtRelevantPlanning.MaxLength.ToString())
        txtRelevantTitle.Attributes.Add("maxlength", txtRelevantTitle.MaxLength.ToString())
        txtDetailComments.Attributes.Add("maxlength", txtDetailComments.MaxLength.ToString())
        txtAccessIssues.Attributes.Add("maxlength", txtAccessIssues.MaxLength.ToString())
        txtMediaIssues.Attributes.Add("maxlength", txtMediaIssues.MaxLength.ToString())
        txtSpFunding.Attributes.Add("maxlength", txtSpFunding.MaxLength.ToString())
        txtManagementDetail.Attributes.Add("maxlength", txtManagementDetail.MaxLength.ToString())
        txtNonBhaDetails.Attributes.Add("maxlength", txtNonBhaDetails.MaxLength.ToString())
    End Sub

    Public Sub resetControls() Implements IAddEditPage.resetControls
        populateDropDowns()
        txtPermittedPlanning.Text = String.Empty
        txtRelevantPlanning.Text = String.Empty
        txtRelevantTitle.Text = String.Empty
        txtDetailComments.Text = String.Empty
        txtAccessIssues.Text = String.Empty
        txtMediaIssues.Text = String.Empty
        txtSpFunding.Text = String.Empty
        txtManagementDetail.Text = String.Empty
        txtNonBhaDetails.Text = String.Empty
        btnSaveRestriction.CommandArgument = String.Empty
        uiMessage.hideMessage()
    End Sub


End Class





