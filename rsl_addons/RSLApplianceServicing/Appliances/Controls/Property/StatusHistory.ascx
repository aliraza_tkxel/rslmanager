﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="StatusHistory.ascx.vb"
    Inherits="Appliances.StatusHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel runat="server" ID="updPnlStatusHistory">
    <ContentTemplate>
        <asp:GridView runat="server" ID="grdVwStatusHistory" ShowHeaderWhenEmpty="True" HeaderStyle-Font-Underline="false"
            AutoGenerateColumns="False" GridLines="None" CellSpacing="5" 
            CellPadding="4" Width="100%">
            <Columns>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblStatus" Text='<%# Bind("STATUS") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFromDate" Text='<%# Bind("FROMDATE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To" >
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblToDate" Text='<%# Bind("TODATE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="No. Of Days Let" >
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblDaysLet" Text='<%# Bind("NUMBOFDAYSLET") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>


                 <asp:TemplateField HeaderText="No. Of Days Void" >
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblDaysVoid" Text='<%# Bind("NUMBOFDAYSVOID") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Rent" >
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblRent" Text='<%# Bind("Rent") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>


            </Columns>
            <EmptyDataTemplate>No Records Found</EmptyDataTemplate>
            <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Center" Font-Underline="false" />
            <RowStyle BackColor="#E8E9EA" Wrap="True" />
            <AlternatingRowStyle BackColor="#ffffff" Wrap="True" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
