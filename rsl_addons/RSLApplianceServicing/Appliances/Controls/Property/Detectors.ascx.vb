﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Imports AS_BusinessLogic
Imports AS_BusinessObject

Public Class Detectors
    Inherits UserControlBase
    Dim _readOnly As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
            btnAddDetector.Visible = False
        End If
    End Sub

    Protected Sub btnAddDetector_Click(sender As Object, e As EventArgs) Handles btnAddDetector.Click
        Try
            uiMessageHelper.resetMessage(lblAddDetectorMessage, pnlAddDetectorMessage)
            populatePowerSource()
            populateSmokeDetectorType()
            resetControl()
            mdlPopUpAddDetector.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddDetectorMessage, pnlAddDetectorMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub

    Public Sub loadData()
        Try
            SessionManager.removePowerSourceDataSet()
            Dim objDefectsBl As DefectsBL = New DefectsBL()
            Dim detectorDataSet As DataSet = New DataSet()
            detectorDataSet = objDefectsBl.getDetectorByPropertyId(SessionManager.getPropertyId(), SessionManager.getTreeItemName)
            SessionManager.setPowerSourceDataSet(detectorDataSet)
            grdDetectors.DataSource = detectorDataSet.Tables(0)
            grdDetectors.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub resetControl()
        txtBatteryReplaced.Text = String.Empty
        txtInstalled.Text = String.Empty
        txtLastTested.Text = String.Empty
        txtLocation.Text = String.Empty
        txtManufacturer.Text = String.Empty
        ddlPowerSource.SelectedIndex = 0
        txtSerialNumber.Text = String.Empty
        txtNotes.Text = String.Empty
    End Sub

#Region "Populate Years"
    Private Sub populatePowerSource()
        Dim detectorDataSet As DataSet = New DataSet()
        detectorDataSet = SessionManager.getPowerSourceDataSet()

        ddlPowerSource.DataSource = detectorDataSet.Tables("PowerSource")
        ddlPowerSource.DataValueField = "PowerTypeId"
        ddlPowerSource.DataTextField = "PowerType"

        ddlPowerSource.DataBind()
        ddlPowerSource.Items.Insert(0, New ListItem("Please Select", ApplicationConstants.DropDownDefaultValue))
        ddlPowerSource.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Populate Years"
    Private Sub populateSmokeDetectorType()

        ddlSmokeDetectorType.Items.Clear()
        ddlNoOfSmokeDetectors.Items.Clear()

        ddlSmokeDetectorType.Items.Insert(0, New ListItem("Please Select", ApplicationConstants.DropDownDefaultValue))
        ddlSmokeDetectorType.Items.Insert(1, New ListItem("Ionisation"))
        ddlSmokeDetectorType.Items.Insert(2, New ListItem("Optical"))
        ddlSmokeDetectorType.Items.Insert(3, New ListItem("Combined"))
        ddlSmokeDetectorType.Items.Insert(4, New ListItem("Heat"))
        ddlSmokeDetectorType.SelectedValue = ApplicationConstants.DropDownDefaultValue

        ddlNoOfSmokeDetectors.Items.Insert(0, New ListItem("Please Select", ApplicationConstants.DropDownDefaultValue))
        ddlNoOfSmokeDetectors.Items.Insert(1, New ListItem("0"))
        ddlNoOfSmokeDetectors.Items.Insert(2, New ListItem("1"))
        ddlNoOfSmokeDetectors.Items.Insert(3, New ListItem("2"))
        ddlNoOfSmokeDetectors.Items.Insert(4, New ListItem("3"))
        ddlNoOfSmokeDetectors.Items.Insert(5, New ListItem("4"))
        ddlNoOfSmokeDetectors.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub
#End Region

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objDetectorsBo As DetectorsBO = New DetectorsBO()
            Dim objDefectsBl As DefectsBL = New DefectsBL()
            objDetectorsBo.PropertyId = SessionManager.getPropertyId()
            objDetectorsBo.DetectorType = SessionManager.getTreeItemName
            If IsDate(txtBatteryReplaced.Text.Trim) Then
                objDetectorsBo.BatteryReplaced = Convert.ToDateTime(txtBatteryReplaced.Text)
            End If
            If IsDate(txtInstalled.Text.Trim) Then
                objDetectorsBo.InstalledDate = Convert.ToDateTime(txtInstalled.Text)
            End If

            objDetectorsBo.IsLandlordsDetector = rdBtnLandlordsDetector.SelectedValue
            objDetectorsBo.IsPassed = rdBtnPassed.SelectedValue
            If IsDate(txtLastTested.Text.Trim) Then
                objDetectorsBo.LastTestedDate = Convert.ToDateTime(txtLastTested.Text)
            End If
            ' SessionManager.setAppServicingUserId(760)
            objDetectorsBo.Location = txtLocation.Text
            objDetectorsBo.Manufacturer = txtManufacturer.Text
            objDetectorsBo.PowerSource = ddlPowerSource.SelectedValue
            objDetectorsBo.SerialNumber = txtSerialNumber.Text
            objDetectorsBo.Notes = txtNotes.Text
            
            Dim saveStatus As Boolean = objDefectsBl.saveDetectors(objDetectorsBo, ddlSmokeDetectorType.SelectedItem.Text,
                                                                                ddlNoOfSmokeDetectors.SelectedItem.Text)
            If saveStatus Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveDetectorSuccessfully, False)
            End If
            loadData()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddDetectorMessage, pnlAddDetectorMessage, uiMessageHelper.Message, True)
                mdlPopUpAddDetector.Show()
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
End Class