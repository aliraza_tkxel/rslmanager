﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TargetRentTab.ascx.vb"
    Inherits="Appliances.TargetRentTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style3
    {
        width: 259px;
    }
    .style6
    {
        width: 182px;
    }
    .style7
    {
        width: 238px;
    }
</style>
<asp:UpdatePanel ID="upPnlTargetRentTab" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlTargentRentTab" runat="server" BackColor="White" Width="400px">
            <table id="pnlTargentRentTabTable" width="400px">
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <div style='margin-left: 15px;'>
                            <asp:Panel ID="pnlTargetRent" runat="server" Visible="false">
                                <asp:Label ID="lblTargetRentTab" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" style='margin-left: 25px;' class="style7">
                        Rent Type:<span class="Required"></span>
                    </td>
                    <td align="left" valign="top" class="style3" colspan="3">
                        <asp:DropDownList ID="ddlTargetRentType" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style7" valign="top">
                        &nbsp;Rent:
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetRent" runat="server" Width="50px" Style="margin-right: 78px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator9" ControlToValidate="txtTargetRent"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetRent"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" style="text-align: right;" valign="top">
                        &nbsp;Services
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetServices" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtTargetServices"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetServices"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style7" valign="top">
                        Council Tax
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetCouncilTax" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtTargetCouncilTax"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetCouncilTax"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        Water
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetWater" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator3" ControlToValidate="txtTargetWater"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetWater"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style7" valign="top">
                        Inelig
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetIntelig" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator4" ControlToValidate="txtTargetIntelig"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetIntelig"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        Supp
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetSupp" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator5" ControlToValidate="txtTargetSupp"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetSupp"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style7" valign="top">
                        Garage
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtTargetGarage" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator6" ControlToValidate="txtTargetGarage"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetGarage"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr bgcolor="#133E71">
                    <td align="left" class="style7" valign="top">
                        <strong style="color: #fff">Total Rent</strong>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <asp:TextBox ID="txtTargetTotalRent" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator7" ControlToValidate="txtTargetTotalRent"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="Only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                        Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtTargetTotalRent"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="left" valign="top">
                        <asp:Button ID="btnSaveTargetRent" runat="server" BackColor="White" Text="Save Target Rent"
                            ValidationGroup="validate" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style7" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" class="style3" colspan="2" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSaveTargetRent" />
    </Triggers>
</asp:UpdatePanel>
