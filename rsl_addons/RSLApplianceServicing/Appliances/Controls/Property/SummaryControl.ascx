﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SummaryControl.ascx.vb"
    Inherits="Appliances.SummaryControl" %>
<%@ Register TagPrefix="prop" TagName="ConditionRatingList" Src="~/Controls/Property/ConditionRatingList.ascx" %>
<script type="text/javascript">

    //    $(document).ready(function () {
    //        alert("ready");
    //        jQuery("#propertyImageContainer").hide();
    //        jQuery("#loadingPropertyImages").show();
    //    });

    //    $(window).load(function () {
    //        alert("load");
    //
    //    });

    function loadImage() {

        jQuery("#loadingPropertyImages").show();
        jQuery("#loadingEPCImage").show();
        var nImages = jQuery("#propertyImageContainer").length;
        var nEPCImages = jQuery("#epcImageContainer").length;
        var loadCounter = 0;

        jQuery("#propertyImageContainer img").one("load", function () {
            loadCounter++;
            if (nImages == loadCounter) {
                jQuery(this).parent().show();
                jQuery("#loadingPropertyImages").hide();
                jQuery("#loadingEPCImage").hide();
            }
        }).each(function () {

            // attempt to defeat cases where load event does not fire
            // on cached images
            if (this.complete) jQuery(this).trigger("load");
        });

        //        loadCounter = 0;
        //        jQuery("#epcImageContainer img").one("load", function () {
        //            loadCounter++;
        //            if (nEPCImages == loadCounter) {
        //                jQuery(this).parent().show();
        //                jQuery("#loadingEPCImage").hide();
        //            }
        //        }).each(function () {
        //            // attempt to defeat cases where load event does not fire
        //            // on cached images
        //            if (this.complete) jQuery(this).trigger("load");
        //        });


    }
</script>
<div>
    <asp:Panel ID="pnlControlMessage" runat="server" Visible="false">
        <asp:Label ID="lblControlMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <div id="leftContainer">
        <div class="propertyInfo" style="height: 160px;">
            <div class="mainheading-panel">
                Property:
            </div>
            <asp:Panel ID="pnlTenancyProperty" runat="server" Visible="false">
                <asp:Label ID="lblTenancyProperty" runat="server">
                </asp:Label>
            </asp:Panel>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Ref Number:"></asp:Label>
                <asp:Label ID="lblRefNumber" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Address:"></asp:Label>
                <asp:Label ID="lblAddress" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Postcode:"></asp:Label>
                <asp:Label ID="lblPostCode" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Dev:"></asp:Label>
                <asp:Label ID="lblDev" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Scheme"></asp:Label>
                <asp:Label ID="lblScheme" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Status:"></asp:Label>
                <asp:Label ID="lblStatus" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Type:"></asp:Label>
                <asp:Label ID="lblType" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label5" runat="server" CssClass="leftControl" Text="Built:"></asp:Label>
                <asp:Label ID="lblBuiltDate" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label8" runat="server" CssClass="leftControl" Text="EPC Rating:"></asp:Label>
                <asp:Label ID="lblEpcRating" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label2" runat="server" CssClass="leftControl" Text="SAP Rating:"></asp:Label>
                <asp:Label ID="lblSAPRating" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label4" runat="server" CssClass="leftControl" Text="Gas Certificate Expiry:"></asp:Label>
                <asp:Label ID="lblGasCertificateExpiry" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label9" runat="server" CssClass="leftControl" Text="Asset Type:"></asp:Label>
                <asp:Label ID="lblAssetType" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label6" runat="server" CssClass="leftControl" Text="Tenure:"></asp:Label>
                <asp:Label ID="lblNROSH1" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
        </div>
        <div class="propertyInfo" style="margin-top: 10px;">
            <div class="mainheading-panel">
                Occupancy:
            </div>
            <asp:Panel ID="pnlAccomodation" runat="server" Visible="false">
                <asp:Label ID="lblAccommodation" runat="server">
                </asp:Label>
            </asp:Panel>
            <div class="labelSpace">
                <asp:Label ID="Label1" runat="server" CssClass="leftControl" Text="Floor Area:"></asp:Label>
                <asp:Label ID="lblFloorArea" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label3" runat="server" CssClass="leftControl" Text="Maximum People:"></asp:Label>
                <asp:Label ID="lblMaxPeople" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label7" runat="server" CssClass="leftControl" Text="Bedrooms:"></asp:Label>
                <asp:Label ID="lblBedrooms" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
        </div>
        <div class="propertyInfo" style="height: 139px; margin-top: 11px;">
            <div class="mainheading-panel">
                Appointments:
            </div>
            <br />
            <div style="height: 100px; overflow: auto;">
                <asp:GridView ID="grdAppointments" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                    Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                    <Columns>
                        <asp:BoundField ItemStyle-Width="100px" DataField="AppointmentType" />
                        <asp:BoundField HeaderText="Status" DataField="AppointmentTime" ItemStyle-Width="150px" />
                        <asp:BoundField HeaderText="Status" DataField="AppointmentDate" ItemStyle-Width="150px" />
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found</EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
        <div class="propertyInfo" style="height: 220px;">
            <div class="mainheading-panel">
                Asbestos:
            </div>
            <div style="height: 190px; overflow: auto; margin-top: 3px;">
                <asp:GridView ID="grdPropertyAsbestos" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
                    runat="server" AutoGenerateColumns="False" ShowHeader="false" HorizontalAlign="left"
                    Width="100%" GridLines="None" CellPadding="3" CellSpacing="5">
                    <Columns>
                        <asp:BoundField HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-Width="150px"
                            DataField="ASBRISKLEVELDESCRIPTION" />
                        <asp:BoundField DataField="RISKDESCRIPTION" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="left" />
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found</EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div id="rightContainer">
        <asp:UpdatePanel ID="updSummaryControl" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPlanned" runat="server" Visible="true">
                    <div id="topLevel1">
                        <div class="topLevel2Left">
                            <div class="topLeftLevel3Top">
                                <div class="mainheading-panel">
                                    Planned Programme:
                                    <asp:Button ID="btnCondition" runat="server" Text="Condition" CssClass="rightControl" />
                                </div>
                                <div style="height: 358px; width: 100%; overflow: auto; margin-top: 5px;">
                                    <asp:GridView ID="grdPlannedMaintenance" runat="server" AutoGenerateColumns="False"
                                        ShowHeader="true" Width="100%" GridLines="None" CellPadding="1" CellSpacing="1"
                                        AllowSorting="true">
                                        <Columns>
                                            <asp:BoundField DataField="Component" HeaderStyle-HorizontalAlign="Left" HeaderText="Component"
                                                ItemStyle-Width="40%" SortExpression="Component" />
                                            <asp:BoundField DataField="LastReplaced" HeaderStyle-HorizontalAlign="Left" HeaderText="Last Replaced"
                                                ItemStyle-Width="30%" SortExpression="LastReplaced" />
                                            <asp:BoundField DataField="Due" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%"
                                                HeaderText="Due" SortExpression="Due" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Records Found</EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="topLevel2MiddleBoth">
                            <div class="topLevel2Middle">
                                <div class="topLevel3Middle">
                                    <asp:UpdatePanel runat="server" ID="updPanelEpcImage">
                                        <ContentTemplate>
                                            <div style="border-width: 0px; width: 100%; height: 22px; z-index: 89; top: 3px;
                                                right: 0px; position: relative; display: block;">
                                                <asp:ImageButton ID="btnEPCCamera" runat="server" Width="35px" Height="30px" ImageUrl="../../Images/camera.png"
                                                    Visible="true" Style="float: right;" />
                                            </div>
                                            <div id="loadingEPCImage" style="text-align: center; margin-top: 45px;">
                                                <img alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                            </div>
                                            <asp:ImageButton ID="btnImgEmptyEPC" runat="server" ImageUrl="../../Images/epc_static.png"
                                                Visible="false" Width="100%" Height="100%" Style="margin-top: -10px;" UseSubmitBehavior="False"
                                                OnClientClick="window.open('../../Views/Common/Download.aspx?DocumentType=EPC_CERTIFICATE'); return false;" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="topMiddleLevel3Bottom">
                                    <asp:Label ID="Label15" runat="server" CssClass="leftControl" Text="Site/Floor plans:"></asp:Label>
                                    (<asp:Label ID="lblSiteFloorPlansCount" runat="server" CssClass="rightControl" Text=""></asp:Label>)
                                    <br />
                                </div>
                            </div>
                            <div class="topLevel2Right">
                                <asp:UpdatePanel runat="server" ID="updPanelPropertyImage">
                                    <ContentTemplate>
                                        <div class="topMiddleLevel3Top">
                                            <div style="border-width: 0px; width: 100%; height: 22px; z-index: 99; top: 3px;
                                                right: 0px; position: relative; display: block;">
                                                <asp:ImageButton ID="btnCamera" runat="server" Width="35px" Height="30px" ImageUrl="../../Images/camera.png"
                                                    Visible="true" Style="float: right;" />
                                            </div>
                                            <div id="loadingPropertyImages" style="text-align: center; margin-top: 100px;">
                                                <img alt="Please Wait" src="../../Images/ajax-loader.gif" />
                                            </div>
                                            <asp:Image ID="imgEmpty" runat="server" ImageUrl="../../Images/awaiting_image.png"
                                                Visible="false" Width="100%" Height="100%" Style="margin-top: 7px;" />
                                        </div>
                                        <div style="border: 1px solid black; height: 49px; width: 195px; margin: 10px; display: none;">
                                            <span style="text-align: center; vertical-align: middle;">Photographs:</span><asp:Label
                                                runat="server" ID="lblPhotographCount"><a id="imageLink" href='#' rel="lightbox[Property]"
                                                    runat="server"></a></asp:Label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="serviceChargesInfo">
                                <div class="mainheading-panel">
                                    Tenancy Details:
                                </div>
                                <div style="height: 120px; overflow: auto;">
                                    <asp:GridView ID="grdTenancyHistory" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
                                        runat="server" AutoGenerateColumns="False" ShowHeader="true" HorizontalAlign="left"
                                        Width="100%" GridLines="None" CellPadding="3" CellSpacing="5">
                                        <Columns>
                                            <asp:BoundField HeaderText="Name" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-Width="150px" DataField="FullCustomerName" />
                                            <asp:BoundField HeaderText="Start" DataField="StartDate" ItemStyle-Width="100px"
                                                HeaderStyle-HorizontalAlign="left" />
                                            <asp:BoundField HeaderText="End" DataField="EndDate" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="left" />
                                            <asp:BoundField HeaderText="Tenancy Type" DataField="TENANCYTYPE" ItemStyle-Width="100px"
                                                HeaderStyle-HorizontalAlign="left" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Records Found</EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="updPanelReportedFaults" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="middleLevel1">
                                <%--                                <b>
                                    <asp:Label ID="Label19" runat="server" CssClass="leftControl" Text=":"></asp:Label></b>--%>
                                <div class="mainheading-panel">
                                    Maintenance job sheets:
                                    <asp:DropDownList Width="150px" ID="ddlFaultType" runat="server" CssClass="rightControl"
                                        AutoPostBack="True">
                                        <asp:ListItem Text="Select Type" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Gas" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Reactive" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <%--<div style="height: 133px; overflow: auto;">--%>
                                <asp:GridView ID="grdReportedFaults" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
                                    runat="server" AutoGenerateColumns="False" ShowHeader="true" Width="100%" GridLines="None"
                                    CellSpacing="5" CellPadding="4" PageSize="5" AllowPaging="True" PagerSettings-Visible="false"
                                    DataKeyNames="JournalId">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgShow" runat="server" OnClick="Show_Hide_ChildGrid" ImageUrl="~/Images/plus.gif"
                                                    CommandArgument="Show" Style="border: 0px none; width: 11px; height: 11px;" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="15px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reported:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReported" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="15%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ref:">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnRef" Enabled='<%# CheckJobSheetExist(Eval("REF"))%>' OnClick="ViewFault"
                                                    runat="server" Text='<%#Eval("REF") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="15%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fault/Defect:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="30%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInspectionType" runat="server" Text='<%#Eval("InspectionType") %>'></asp:Label>
                                                <asp:Label ID="lblInspectionTypeDescription" Style="display: none;" runat="server"
                                                    Text='<%#Eval("InspectionTypeDescription") %>'></asp:Label>
                                                <asp:Label ID="lblJournalI" Style="display: none;" runat="server" Text='<%#Eval("JournalId") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="15%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="25%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionID(Eval("IsDocumentAttached"), Eval("CP12DocumentID"))%>'
                                                    Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                                                    NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="15px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <tr>
                                                    <td colspan="100%" style="background: #ffffff">
                                                        <div id="div<%# Eval("JournalId") %>" style="overflow: auto; position: relative;
                                                            left: 15px;">
                                                            <asp:Panel ID="pnlChild" runat="server" Visible="false">
                                                                <asp:GridView ID="grdReportedFaultsChild" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
                                                                    runat="server" AutoGenerateColumns="False" ShowHeader="false" Width="100%" GridLines="None"
                                                                    CellSpacing="5" CellPadding="4" AllowPaging="True" PagerSettings-Visible="false">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblReported" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                                            <ItemStyle Width="15%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label Text='<%#Eval("REF") %>' ID="lblRef" runat="server" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                                            <ItemStyle Width="15%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Action") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                                            <ItemStyle Width="30%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFaultType" runat="server" Text='<%#Eval("InspectionType") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                                            <ItemStyle Width="15%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                                            <ItemStyle Width="20%" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ShowHeader="False">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionID(Eval("IsDocumentAttached"), Eval("CP12DocumentID"))%>'
                                                                                    Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                                                                                    NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                                                                            </ItemTemplate>
                                                                            <ItemStyle Height="10%" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        No Records Found</EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records Found</EmptyDataTemplate>
                                    <RowStyle BackColor="#E8E9EA" Wrap="True" />
                                    <AlternatingRowStyle BackColor="#ffffff" Wrap="True" />
                                </asp:GridView>
                                <%--</div>--%>
                                <asp:LinkButton ID="lnkBtnViewAll" runat="server" Text="View All ..." CssClass="rightControl"
                                    ForeColor="#000000" Font-Underline="false" Visible="false"></asp:LinkButton>
                            </div>
                            <!-- ModalPopupExtender -->
                            <asp:CheckBox ID="ckBoxPhotoUpload" runat="server" AutoPostBack="True" Visible="true"
                                CssClass="hiddenField" />
                            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                            <ajaxToolkit:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
                                TargetControlID="btnHidden" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="height: 600px;
                                width: 900px; overflow: hidden;">
                                <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheet" style="height: 575px;
                                    overflow: auto; width: 100%; border: 0px none transparent;" />
                                <div style="width: 100%; text-align: left; clear: both;">
                                    <div style="float: right;">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                                        <input id="btnPrintJobSheet" type="button" value="Print Job Sheet" class="margin_right20"
                                            onclick="PrintJobSheet()" style="display: none;" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <!-- ModalPopupExtender -->
                            <asp:Button runat="server" ID="btnHiddenPhotoGraph" Style="display: none;" />
                            <ajaxToolkit:ModalPopupExtender runat="server" ID="mdlPopUpViewImage" PopupControlID="pnlViewImage"
                                TargetControlID="btnHiddenPhotoGraph" CancelControlID="btnClosePhotographPopup"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel runat="server" ID="pnlViewImage" CssClass="modalPopup" Style="height: 65%;
                                width: 45%; overflow: auto;">
                                <table style="width: 100%;">
                                    <tr>
                                        <asp:Image runat="server" ID="imgDefaultImage" Style="height: 85%; width: 100%;" />
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td colspan="2" style="width: 100px;">
                                            Title:<asp:Label ID="lblTitle" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <span style="float: right;">Date:<asp:Label ID="lblImageDate" runat="server"></asp:Label></span>
                                        </td>
                                        <td style="padding-left: 15px;">
                                            By:<asp:Label ID="lblUploadedBy" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkBoxDefaultImage" runat="server" Text="Default Image" Checked="true" />
                                        </td>
                                        <td colspan="2">
                                            <asp:Button ID="btnPhotograph" runat="server" Text="Add New Photograph" BackColor="White" />
                                        </td>
                                        <td style="float: right;">
                                            <asp:Button ID="btnClosePhotographPopup" runat="server" Text="Close Window" BackColor="White" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlAddPhotoPopup" runat="server" BackColor="White" Width="400px">
                                <table id="pnlAddPhotoTable">
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <div style="float: left; font-weight: bold; padding-left: 10px;">
                                                Add New Photograph</div>
                                            <div style="clear: both">
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <asp:Panel ID="pnlAddPhotographMessage" runat="server" Visible="false">
                                                <asp:Label ID="lblAddPhotographMessage" runat="server" Text=""></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Date:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Image ID="imgCalendar" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                                Style="height: 16px; float: right; margin-right: 37px; float: right" />
                                            <ajaxToolkit:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtBoxDueDate"
                                                PopupButtonID="imgCalendar" Format="dd/MM/yyyy" />
                                            <asp:TextBox ID="txtBoxDueDate" runat="server" OnClick="this.value=''" Style="width: 100px;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Title:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:TextBox ID="txtBoxTitle" runat="server"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Upload:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Button ID="btnUploadPhotos" runat="server" Text="Upload" Width="60px" OnClientClick="openPhotoUploadWindow('summaryPhoto')" />
                                            <asp:Label runat="server" ID="lblFileName"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                        <td align="right" valign="top" style="text-align: right;" class="style1">
                                            <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                                            &nbsp;
                                            <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpAddPhoto" runat="server" DynamicServicePath=""
                                Enabled="True" TargetControlID="lblDispPhotoGraph" PopupControlID="pnlAddPhotoPopup"
                                DropShadow="true" CancelControlID="btnCancel">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>
                            <!-- BEGIN - EPC images upload -->
                            <asp:CheckBox ID="ckBoxEPCPhotoUpload" runat="server" AutoPostBack="True" CssClass="hiddenField" />
                            <asp:Button runat="server" ID="btnHiddenEPCPhotoGraph" Style="display: none;" />
                            <ajaxToolkit:ModalPopupExtender runat="server" ID="mdlPopUpViewEPCImage" PopupControlID="pnlViewEPCImage"
                                TargetControlID="btnHiddenEPCPhotoGraph" CancelControlID="btnCloseEPCPhotographPopup"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel runat="server" ID="pnlViewEPCImage" CssClass="modalPopup" Style="height: 65%;
                                width: 45%; overflow: auto;">
                                <table style="width: 100%;">
                                    <tr>
                                        <asp:Image runat="server" ID="imgEPCDefaultImage" Style="height: 85%; width: 100%;" />
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td colspan="2" style="width: 100px;">
                                            Title:<asp:Label ID="lblMainEPCTitle" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <span style="float: right;">Date:
                                                <asp:Label ID="lblMainEPCDueDate" runat="server"></asp:Label>
                                            </span>
                                        </td>
                                        <td style="padding-left: 15px;">
                                            By:<asp:Label ID="lblMainEPCUploadedBy" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Default Image" Checked="true" />
                                        </td>
                                        <td colspan="2">
                                            <asp:Button ID="btnEPCPhotograph" runat="server" Text="Add New Photograph" BackColor="White" />
                                        </td>
                                        <td style="float: right;">
                                            <asp:Button ID="btnCloseEPCPhotographPopup" runat="server" Text="Close Window" BackColor="White" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Label ID="lblDispEPCPhotoGraph" runat="server"></asp:Label>
                            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpAddEPCPhoto" runat="server" DynamicServicePath=""
                                Enabled="True" TargetControlID="lblDispEPCPhotoGraph" PopupControlID="pnlAddEPCPhotoPopup"
                                DropShadow="true" CancelControlID="btnEPCCancel">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlAddEPCPhotoPopup" runat="server" BackColor="White" Width="400px">
                                <table id="pnlAddEPCPhotoTable">
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <div style="float: left; font-weight: bold; padding-left: 10px;">
                                                Add New EPC Photograph</div>
                                            <div style="clear: both">
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <asp:Panel ID="pnlAddEPCPhotographMessage" runat="server" Visible="false">
                                                <asp:Label ID="lblAddEPCPhotographMessage" runat="server" Text=""></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Date:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Image ID="imgEPCCalender" runat="server" Height="16px" ImageUrl="~/Images/Calendar-icon.png"
                                                Style="height: 16px; float: right; margin-right: 37px; float: right" />
                                            <ajaxToolkit:CalendarExtender ID="ctrlEPCCalendarExtender" runat="server" TargetControlID="txtBoxEPCDueDate"
                                                PopupButtonID="imgEPCCalender" Format="dd/MM/yyyy" />
                                            <asp:TextBox ID="txtBoxEPCDueDate" runat="server" OnClick="this.value=''" Style="width: 100px;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Title:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:TextBox ID="txtBoxEPCTitle" runat="server"> </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="120px">
                                            Upload:<span class="Required">*</span>
                                        </td>
                                        <td align="left" valign="top" class="style1">
                                            <asp:Button ID="btnUploadEPCPhotos" runat="server" Text="Upload" Width="60px" OnClientClick="openPhotoUploadWindow('summaryEPCPhoto')" />
                                            <asp:Label runat="server" ID="lblEPCFileName"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                        <td align="right" valign="top" style="text-align: right;" class="style1">
                                            <input id="btnEPCCancel" type="button" value="Cancel" style="background-color: White;" />
                                            &nbsp;
                                            <asp:Button ID="btnSaveEPCImage" runat="server" Text="Save" BackColor="White" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- END - EPC images upload -->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="bottomLevel1">
                        <div id="bottomLeftLevel2" style="display: none;">
                            <b>Warranties:</b><br />
                            <br />
                            <asp:GridView ID="GridView2" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="False"
                                ShowHeader="true" Width="100%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                                <Columns>
                                    <asp:BoundField HeaderText="Type:" DataField="Type" ItemStyle-Width="150px" />
                                    <asp:BoundField HeaderText="For:" DataField="For" ItemStyle-Width="150px" />
                                    <asp:BoundField HeaderText="Expiry:" DataField="Expiry" ItemStyle-Width="150px" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="bottomRightLevel2" style="display: none;">
                            <b>Rent Details:</b><br />
                            <br />
                            <asp:GridView ID="GridView3" runat="server" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False"
                                ShowHeader="true" Width="100%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                                <Columns>
                                    <asp:BoundField HeaderText="Rent:" DataField="Rent" ItemStyle-Width="150px" />
                                    <asp:BoundField HeaderText="Set:" DataField="Set" ItemStyle-Width="150px" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlConditionRatingList" runat="server" Visible="false">
                    <prop:ConditionRatingList ID="ucConditionRatingList" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
