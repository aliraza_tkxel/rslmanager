﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class FinancialTab
    Inherits UserControlBase

#Region "Properties"




#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If Not IsPostBack Then
            lnkBtnRentTab.CssClass = ApplicationConstants.TabClickedCssClass
            MainSubView.ActiveViewIndex = 0
        End If
    End Sub

#End Region


#End Region





#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region



    Protected Sub lnkBtnRentTab_Click(sender As Object, e As EventArgs) Handles lnkBtnRentTab.Click
       
        lnkBtnValuationTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnRentTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainSubView.ActiveViewIndex = 0
    End Sub

    Protected Sub lnkBtnValuationTab_Click(sender As Object, e As EventArgs) Handles lnkBtnValuationTab.Click
        lnkBtnRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnValuationTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainSubView.ActiveViewIndex = 1
        Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
        ValuationTab.bindToGrid(propertyId)
    End Sub
End Class