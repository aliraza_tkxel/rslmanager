﻿<%@ Control Language="VB" AutoEventWireup="false" CodeBehind="~/Controls/Property/HealthSafetyTab.ascx.vb"
    Inherits="Appliances.HealthSafetyTab" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="ucAbs" TagName="Asbestos" Src="AsbestosTab.ascx" %>
<%@ Register TagPrefix="ucRefurishment" TagName="Refurishment" Src="RefurbishmentTab.ascx" %>
<%@ Register TagPrefix="ucAidAda" TagName="AidAda" Src="AidsAdaptationTab.ascx" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<!--Upload form starts here -->
<div style="min-width: 1020px;">
    <div style="border-style: solid; border-color: black; border-width: 0px; margin-left: 2px;
        min-width: 300px; float: left; width: 100%;">
        <asp:LinkButton ID="lnkBtnAsbestosTab" OnClick="lnkBtnAsbestosTab_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Asbestos: </asp:LinkButton>
        <asp:LinkButton ID="lnkRefurbishmentTab" OnClick="lnkRefurbishmentTab_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Refurbishment: </asp:LinkButton>
        <%--<asp:LinkButton ID="lnkBtnAidAdaptationTab" OnClick="lnkBtnAidAdaptationTab_Click" CssClass="TabInitial" runat="server" BorderStyle="Solid">Aid & Adaptation: </asp:LinkButton>--%>
        <br />
        <div class="pm-lbox" style="width: 98%;">
            <asp:MultiView ID="MainSubView" runat="server">
                <asp:View ID="SubView1" runat="server">
                    <ucAbs:Asbestos ID="Asbestos" runat="server" />
                </asp:View>
                <asp:View ID="SubView2" runat="server">
                    <ucRefurishment:Refurishment ID="Refurbishment" runat="server" />
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</div>
<div class="clear">
</div>
