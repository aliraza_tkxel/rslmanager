﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DocumentTab.ascx.vb"
    Inherits="Appliances.DocumentTab" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:UpdatePanel ID="updPanelMessage" runat ="server">
<ContentTemplate>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

<!--Upload form starts here -->
<div style="min-width: 1020px; display:flex;">
    <div style="border-style: solid; border-color: black; border-width: 0px; width: 30%;
        margin-left: 2px; min-width: 400px;">
        <div class="pm-lbox" style=" width:350px !important;" >
            <asp:UpdatePanel ID="updPnlAttributes" runat="server">
                <ContentTemplate>
                    <%--<div class="title-bar">--%>
                    <div class="mainheading-panel" style="margin-top: 2px; margin-left: 5px; width: 94%;">
                        <asp:Label ID="addDocTitle" runat="server"> <strong>Add a Document</strong> </asp:Label>
                        <asp:Label ID="searchDocTitle" runat="server" Visible="false"> <strong>Search a Document</strong> </asp:Label>
                    </div>
                    <div class="add-form">
                        <div class="frow">
                            <div class="title">
                                Category:
                            </div>
                            <div class="input-area">

                                <%-- <asp:RadioButtonList ID="rbDocumentCategory" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="General" Value="General" />
                                            <asp:ListItem Text="Compliance" Value="Compliance" Selected="True" />
                                </asp:RadioButtonList>--%>
                                <asp:DropDownList ID="ddlDocumentCategory" OnSelectedIndexChanged="ddlDocumentCategory_SelectedIndexChanged" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="true">
                                   <%-- <asp:ListItem Value = "Compliance" Text="Compliance" Enabled ="true" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value = "General" Text="General" Enabled="true"></asp:ListItem>
                                    <asp:ListItem Value="Legal" Text="Legal" Enabled = "true"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>

                        <div class="frow">
                            <div class="title">
                                Type:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlType" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow" id="div_TitleSelector" runat="server">
                            <div class="title">
                                 <asp:Label id="lblTitle" runat="server"> Title: </asp:Label>
                            </div>
                            <div class="input-area">
                                &nbsp;<asp:DropDownList ID="ddlSubtype" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <asp:Panel ID="CP12NumberPnl" runat="server">
                            <div class="frow">
                                <div class="title" Style = "width:55px !important">
                                    CP12 No:
                                </div>
                               
                                    <div class="input-area" style="padding-left:-4px !important">
                                <asp:TextBox runat="server" ID="txtCp12Number" Style="width: 200px;"></asp:TextBox>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="CategoryPnl" runat="server">
                            <div class="frow">
                                <div class="title">
                                    Category:
                                </div>
                                <div class="input-area">
                                    &nbsp;<asp:DropDownList ID="ddlCategory" runat="server" class="selectval" CssClass="selectval">
                                    </asp:DropDownList>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="frow">
                            <div class="title">
                                Document:
                            </div>
                            <div class="input-area" style="padding-left:0px !important">
                                <asp:TextBox runat="server" ID="txtDocumentDate" Style="width: 150px"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrDocumentDate" TargetControlID="txtDocumentDate"
                                    PopupButtonID="imgDocumentDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumentDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Expiry:
                            </div>
                            <div class="input-area" style="padding-left:0px !important">
                                <asp:TextBox runat="server" ID="txtExpires" Style="width: 150px;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrExpires" TargetControlID="txtExpires"
                                    PopupButtonID="imgExpiresDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgExpiresDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow" id="div_FileUpload" runat="server">
                            <div class="title">
                                <asp:Label id="lblFileUpload" runat="server"> Upload: </asp:Label>
                            </div>
                            <div class="input-area" style="padding-left:2px !important">
                                <asp:Button ID="btnFileUpload" runat="server" Text="Upload" Width="60px" OnClientClick="openComplianceUploadWindow('phototab')" />
                               
                                <div style="display: block; width: 200px;word-wrap: break-word">

                                    <asp:Label runat="server" ID="lblFileName" ></asp:Label>
                                    <asp:CheckBox ID="ckBoxComplianceDocumentUpload" runat="server" AutoPostBack="True"
                                        Visible="true" CssClass="hiddenField" />
                                 </div>  
                                 
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Keywords:
                            </div>
                            <div class="input-area" style="padding-left:0px !important">
                               <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="txtarea"
                                    MaxLength="255"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Reset" />
                            <asp:Button ID="btnSavePropertyDocuments" runat="server" Text="Save" />
                            <asp:Button ID="btnSearchPropertyDocuments" runat="server" Text="Search" Visible="false" />
                            &nbsp;<div class="clear">
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
               
            </asp:UpdatePanel>
        </div>
    </div>
    <div style="width: 65%; margin-left: 10px; margin-top: 5px;">
        <asp:UpdatePanel runat="server" ID="updpanelDocumentList">
            <ContentTemplate>
           
        <cc1:PagingGridView ID="grdDocumentInfo" runat="server" AllowPaging="false" AllowSorting="false"
            AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" OnRowDataBound="grdDocumentInfo_RowDataBound"
            GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false">
            <Columns>
                <asp:TemplateField HeaderText="Date Added:" SortExpression="CreatedDate">
                    <ItemTemplate>
                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="By:" SortExpression="UploadedBy">
                    <ItemTemplate>
                        <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Bind("UploadedBy") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type:" SortExpression="DocumentName">
                    <ItemTemplate>
                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("TypeTitle") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title:" SortExpression="DocumentType">
                    <ItemTemplate>
                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("SubtypeTitle") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category:" SortExpression="Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Document:" SortExpression="DocumentDate">
                    <ItemTemplate>
                        <asp:Label ID="lblDocumentDate" runat="server" Text='<%# Bind("DocumentDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expiry:" SortExpression="ExpiryDate">
                    <ItemTemplate>
                        <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnDeleteDocument" runat="server" CommandArgument='<%#Eval("DocumentId")%>'
                            Text="Delete" UseSubmitBehavior="False" OnClientClick="if(! confirm('Are you sure you want to permanently remove the selected file?')) return false;"
                            OnClick="btnDeleteDocument_Click" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnViewDocument" runat="server" Text="View" UseSubmitBehavior="False"
                            OnClientClick='<%# String.Format("window.open(""../../Views/Common/Download.aspx?PropertyDocumentID={0}""); return false;", Eval("DocumentId").ToString())%>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
            <%-- Grid View Styling Start --%>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
            <%-- Grid View Styling End --%>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        </cc1:PagingGridView>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
            background-color: #FAFAD2; border-color: #ADADAD; border-width: 2px; border-style: none;
            vertical-align: middle; border-top: 0px; border-top-color: transparent;">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                CommandArgument="First" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                                CommandArgument="Prev" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            Page:
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                            of
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                            to
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                            of
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                CommandArgument="Next" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                CommandArgument="Last" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td align="right">
                            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                Type="Integer" SetFocusOnError="True" CssClass="Required" />
                            <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                            <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
         </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<div class="clear">
</div>
