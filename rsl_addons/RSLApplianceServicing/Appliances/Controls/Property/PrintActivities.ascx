﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PrintActivities.ascx.vb" Inherits="Appliances.PrintActivities" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript">
    function PrintPage() {
        window.print();
        
    }
</script>
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
     <asp:Button ID="btnPrint" runat="server" class="printBtns" Text="Print" OnClientClick="javascript:PrintPage();" BackColor="White" />
     <asp:Button ID="btnBack" runat="server" Text="Back" class="printBtns" BackColor="White"/>
<div style="float:right;margin-right: 40px;">

<img src="<%= Page.ResolveUrl("~")%>Images/PropertyModule/broadland.png" alt="logo" />
</div>
<div style="border:1px solid black; width:30%;padding: 10px;margin-left: 50px;margin-top: 50px;" >
<asp:label ID="Label5" runat="server" class="labelPrintActivities">Ref Number: </asp:label><asp:Label runat="server" ID ="lblRefNumber" class="labelPrintActivities"> </asp:Label> <br />
<asp:label ID="Label6" runat="server" class="labelPrintActivities">Address:  </asp:label><asp:Label runat="server" ID ="lblAddress" class="labelPrintActivities"></asp:Label><br />
<asp:label ID="Label7" runat="server" class="labelPrintActivities">PostCode: </asp:label><asp:Label runat="server" ID ="lblPostCode" class="labelPrintActivities"> </asp:Label><br />
<asp:label ID="Label8" runat="server" class="labelPrintActivities">Dev:  </asp:label><asp:Label runat="server" ID ="lblDev" class="labelPrintActivities"></asp:Label><br />
<asp:label ID="Label9" runat="server" class="labelPrintActivities">Scheme:  </asp:label><asp:Label runat="server" ID ="lblScheme" class="labelPrintActivities"></asp:Label><br />
<asp:label ID="Label10" runat="server" class="labelPrintActivities">Status:  </asp:label><asp:Label runat="server" ID ="lblStatus" class="labelPrintActivities"></asp:Label><br />
<asp:label ID="Label11" runat="server" class="labelPrintActivities">Type:  </asp:label><asp:Label runat="server" ID ="lblType" class="labelPrintActivities"></asp:Label><br />
</div>

<h1 style="float:right;font-size: 40px;">Activities</h1>
<br />
<hr style="margin-top: 87px; border: 1px solid black;" />


        <asp:GridView runat="server" ID="grdPrintActivities" 
            AutoGenerateColumns="False" GridLines="None" Width="100%" >
            <Columns>
                <asp:TemplateField HeaderText="Date">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type:">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("InspectionType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action:">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="By:">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File :">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Document") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <HeaderStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="2px" Font-Bold="True" Font-Overline="False" Font-Strikeout="False" 
                Font-Underline="False" HorizontalAlign="Left" />
        </asp:GridView>
