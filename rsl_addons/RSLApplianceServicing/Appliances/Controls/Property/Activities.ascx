﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Activities.ascx.vb"
    Inherits="Appliances.Activities" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="prop" TagName="ContractorJobSheetSummary" Src="~/Controls/Property/ContractorJobSheetSummary.ascx" %>
<%@ Register TagPrefix="defect" TagName="defectmanegement" Src="~/Controls/DefectManagement/DefectManagement.ascx" %>
<%@ Register TagPrefix="dpImages" TagName="DefectImages" Src="~/Controls/Property/DefectImages.ascx" %>
<%@ Register TagPrefix="appointmentNotesTag" TagName="AppointmentNotes" Src="~/Controls/Scheduling/AppointmentNotes.ascx" %>
<%@ Import Namespace="AS_Utilities" %>

<style type="text/css">
    tr
    {
        margin: 0px !important;
        padding: 0px !important;
    }
    .left_td
    {
        width: 105px;
        padding-left: 15px;
    }
    .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.5;
    }
</style>
<div style="width: 100%; float: left;">
    <div style="width: 50%; float: left; padding-top: 10px;">
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
    </div>
    <div class="mainheading-panel" style="margin-top: 5px; margin-left: 10px; width: 98%;">
        <div style="float: right; text-align: center; margin-top: -4px;">
            <asp:DropDownList ID="ddlActionType" runat="server" AutoPostBack="true" CssClass="acitivity-dropdown">
            </asp:DropDownList>
            <asp:Button ID="btnPrintActivities" runat="server" Text="Print Activities" BackColor="White" />&nbsp;&nbsp;
        </div>
    </div>
</div>
<div style="clear: both">
</div>
<div style="background-color: #ffffff; margin-top: 5px; margin-left: 10px; margin-right: 10px;
    border: 1px solid black;">
    <asp:GridView ID="grdActivities" runat="server" AutoGenerateColumns="false" BackColor="White"
        BorderColor="Gray" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="5"
        ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True" ShowHeaderWhenEmpty="True"
        AllowPaging="True" PageSize="25" DataKeyNames="JournalId">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgShow" runat="server" OnClick="Show_Hide_ChildGrid" ImageUrl="~/Images/plus.gif"
                        CommandArgument="Show" Style="border: 0px none; width: 11px; height: 11px;" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="15px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" SortExpression="CreateDate">
                <ItemTemplate>
                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type" SortExpression="InspectionType">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="linkbtnDefectDetail" Visible="False" Text='<%# Bind("InspectionType")%>'
                        CommandArgument='<%# Eval("PropertyDefectId") %>' OnClick="linkbtnDefectDetail_Click" />
                    <asp:Label ID="lblInspectionType" runat="server" Visible="True" Text='<%# Bind("InspectionType") %>'></asp:Label>
                    <asp:Label ID="lblInspectionTypeDescription" runat="server" Visible="false" Text='<%# Bind("InspectionTypeDescription") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="135px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="Status">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="180px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ref" SortExpression="Ref">
                <ItemTemplate>
                    <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment" SortExpression="AppointmentDate">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("AppointmentDate") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="110px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Operative" SortExpression="OPERATIVENAME">
                <ItemTemplate>
                    <asp:Label ID="lblOperativeName" runat="server" Text='<%# Bind("OPERATIVENAME") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="170px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trade" SortExpression="OPERATIVETRADE">
                <ItemTemplate>
                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("OPERATIVETRADE") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action" SortExpression="Action">
                <ItemTemplate>
                    <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recorded By" SortExpression="Name">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="150px" />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgBtnLetterDoc" runat="server" BorderStyle="None" ImageUrl="~/Images/paperclip_img.jpg"
                        Visible='<%# Eval("IsLetterAttached").ToString() %>' OnClick="imgBtnLetterDoc_Click"
                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:HyperLink ID="lnkID" runat="server" Visible='<%# CheckCp12Status(Eval("Status"), Eval("CP12DocumentID")) %>'
                        Target="_blank" ImageUrl="~/Images/document.png" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("LGSRHISTORYID", "~/Views/Common/Download.aspx?HistoryCP12DocumentID={0}") %>' />
                    <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionID(Eval("IsDocumentAttached"), Eval("CP12DocumentID"))%>'
                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                    <asp:ImageButton ID="imgBtnPlannedJobsheet" runat="server" BorderStyle="None" ImageUrl="~/Images/jobsheetclip.png"
                        Visible="false" ToolTip="Planned Job Sheet" OnClientClick='<%# String.Format("JavaScript:openJobSheetSummary({0})",Eval("JournalHistoryId").ToString())%>'
                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:HyperLink ID="lnkInspectionDocument" runat="server" Visible='<%# CheckPlannedInspectionID(Eval("IsDocumentAttached"), Eval("Status"))%>'
                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("JournalHistoryId", "~/Views/Common/Download.aspx?inspectionJournalHistoryId={0}") %>' />
                    <asp:ImageButton ID="imgBtnFaultContractorJobsheet" runat="server" BorderStyle="None"
                        ImageUrl="~/Images/jobsheetclip.png" Visible="false" ToolTip="Fault Contractor Job Sheet"
                        OnClick="imgBtnFaultContractorJobsheet_Click" CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:ImageButton ID="imgBtnAssignedToContractor" runat="server" BorderStyle="None"
                        ImageUrl="~/Images/jobsheetclip.png" Visible="false" ToolTip="Planned Contractor Job Sheet"
                        OnClick="imgAssignedToContractor_Click" CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:ImageButton ID="imgbtnAptNotes" runat="server" CommandArgument='<%# Eval("APPOINTMENTID") %>'
                        ImageUrl="~/Images/editCustomerNotes.png" OnClick="imgbtnAptNotes_Click" BorderStyle="None"
                        Visible='<%# hideShowAppointmentNotesButton(Eval("AppointmentNotes").ToString(),Eval("InspectionTypeDescription").ToString()) %>'
                        Width="18px" Height="18px" BorderWidth="0px" />
                    <asp:ImageButton ID="imgbtnCancelNotes" runat="server" BorderStyle="None" ImageUrl="~/Images/cancel_notes.png"
                        Visible='<%# CheckApplianceStatus(Eval("Status"), Eval("InspectionTypeDescription"))%>'
                        ToolTip="Cancel Notes" OnClick="imgBtnCancelNotes_Click" CommandArgument='<%# Eval("JournalId") %>'
                        Height="18px" BorderWidth="0px" />
                    <asp:ImageButton ID="imgShowDefectImages" Width="30px" Height="30px" ImageUrl="~/Images/camera.png"
                        AlternateText=">" runat="server" OnClick="showDefectImages_Click" BorderStyle="None"
                        Visible='<%# Eval("IsDefectImageExist") %>' />
                </ItemTemplate>
                <ItemStyle Height="15px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <tr>
                        <td colspan="100%" style="background: #ffffff">
                            <!--Eval("JournalId")-->
                            <div id="div<%# Eval("JournalId") %>" style="overflow: auto; position: relative;
                                left: 15px; overflow: auto">
                                <asp:Panel ID="pnlChild" runat="server" Visible="false">
                                    <asp:GridView ID="grdActivitiesChild" runat="server" AutoGenerateColumns="False"
                                        ShowHeader="false" BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="1px"
                                        CellPadding="3" CellSpacing="5" ForeColor="Black" GridLines="None" Width="99%"
                                        AllowSorting="True" AllowPaging="false" OnRowDataBound="grdActivities_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="left" CssClass="left_td" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInspectionType" runat="server" Text='<%# Bind("InspectionType") %>'></asp:Label>
                                                    <asp:Label ID="lblInspectionTypeDescription" runat="server" Visible="false" Text='<%# Bind("InspectionTypeDescription") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="135px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="180px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("AppointmentDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="110px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOperativeName" runat="server" Text='<%# Bind("OPERATIVENAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="170px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("OPERATIVETRADE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="115px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="115px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgBtnLetterDoc" runat="server" BorderStyle="None" ImageUrl="~/Images/paperclip_img.jpg"
                                                        Visible='<%# Eval("IsLetterAttached").ToString() %>' OnClick="imgBtnLetterDoc_Click"
                                                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                                                    <asp:HyperLink ID="lnkID" runat="server" Visible='<%# CheckCp12Status(Eval("Status"), Eval("CP12DocumentID")) %>'
                                                        Target="_blank" ImageUrl="~/Images/document.png" BorderStyle="None" BorderWidth="0px"
                                                        NavigateUrl='<%# Eval("HistoryCP12DocumentID", "~/Views/Common/Download.aspx?HistoryCP12DocumentID={0}") %>' />
                                                    <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionID(Eval("IsDocumentAttached"), Eval("CP12DocumentID"))%>'
                                                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                                                        NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                                                    <asp:ImageButton ID="imgBtnPlannedJobsheet" runat="server" BorderStyle="None" ImageUrl="~/Images/jobsheetclip.png"
                                                        Visible="false" ToolTip="Planned Job Sheet" OnClientClick='<%# String.Format("JavaScript:openJobSheetSummary({0})",Eval("JournalHistoryId").ToString())%>'
                                                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                                                    <asp:HyperLink ID="lnkInspectionDocument" runat="server" Visible='<%# CheckPlannedInspectionID(Eval("IsDocumentAttached"), Eval("Status"))%>'
                                                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                                                        NavigateUrl='<%# Eval("JournalHistoryId", "~/Views/Common/Download.aspx?inspectionJournalHistoryId={0}") %>' />
                                                    <asp:ImageButton ID="imgBtnFaultContractorJobsheet" runat="server" BorderStyle="None"
                                                        ImageUrl="~/Images/jobsheetclip.png" Visible="false" ToolTip="Fault Contractor Job Sheet"
                                                        OnClick="imgBtnFaultContractorJobsheet_Click" CommandArgument='<%# Eval("JournalHistoryId") %>' />
                                                    <asp:ImageButton ID="imgBtnAssignedToContractor" runat="server" BorderStyle="None"
                                                        ImageUrl="~/Images/jobsheetclip.png" Visible="false" ToolTip="Planned Contractor Job Sheet"
                                                        OnClick="imgAssignedToContractor_Click" CommandArgument='<%# Eval("JournalHistoryId") %>' />
                                                    <asp:ImageButton ID="imgbtnDefectDetail" ImageUrl="~/Images/aero.png" AlternateText=">"
                                                        runat="server" OnClick="showDefectDetail" BorderStyle="None" Visible='<%# CheckDefectStatus(Eval("Status")) %>'
                                                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Height="15px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#E8E9EA" Wrap="True" />
                                        <AlternatingRowStyle BackColor="#ffffff" Wrap="True" />
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
            HorizontalAlign="Left" Font-Underline="false" />
        <RowStyle BackColor="#E8E9EA" Wrap="True" />
        <AlternatingRowStyle BackColor="#ffffff" Wrap="True" />
    </asp:GridView>
</div>
<!-- Assigned to Contractor job sheet!-->
<asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopup" width="1000px">
    <div id="header-Popup">
        <p id="header_label">
            <b>Planned works</b>
        </p>
        <div id="header_box">
        </div>
    </div>
    <div style="height: auto; clear: both;">
        <table id="tbl_header" style="width: 98%; line-height: 20px;" cellpadding="0px" cellspacing="0px">
            <tr>
                <td style="width: 5%;">
                </td>
                <td colspan="3">
                    <div style="text-align: right;">
                        <asp:Button ID="btnPrevious" Enabled="false" runat="server" Text="&lt; Previous"
                            Width="100px" />
                        &nbsp;
                        <asp:Button ID="btnNext" Enabled="false" runat="server" Text="Next >" Width="100px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="height: 30px;">
                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                </td>
                <td colspan="2" style="border-bottom: 1px solid #7F7F7F;">
                    <b>Summary </b>
                </td>
                <td style="width: 30%; border-bottom: 1px solid #7F7F7F; text-align: right;">
                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="Page"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td style="width: 5%; border-bottom: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr style='height: 10px;'>
                <td style="width: 5%;">
                </td>
                <td colspan="3">
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                    border-left: 1px solid #7F7F7F;">
                </td>
                <td style="width: 20%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>PMO:</b>&nbsp;
                    <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
                <td style="width: 40%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>Planned works component:</b>&nbsp;
                    <asp:Label ID="lblPlannedWorkComponent" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 30%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;">
                    <b>Trade:</b>&nbsp;
                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%; border-top: 1px solid #7F7F7F; border-bottom: 1px solid #7F7F7F;
                    border-right: 1px solid #7F7F7F;">
                </td>
            </tr>
        </table>
        <table id="Table1" style="width: 100%; margin-top: 0px; line-height: 18px;">
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>PO Number:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblPONo" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Status:</b>&nbsp;
                </td>
                <td align="left" style="width: 35%;">
                    <asp:DropDownList ID="StatusDropDown" runat="server" CssClass="StatusDropdown" Visible="False"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Contractor:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblContractor" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Budget:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblBudget" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Contact:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Estimate:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblEstimate" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Works Required:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Style="width: 230px;
                        height: 40px;" ReadOnly="true"></asp:TextBox>
                </td>
                <td style="width: 10%;">
                    <b>NET Cost:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblNetCost" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Ordered By:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrderedBy" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>VAT:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>Ordered:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrdered" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>Total:</b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 10%;">
                    <b>Order Total:&nbsp;</b>
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblOrderTotal" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="vertical-align: middle;">
                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForScheme" runat="server" Text="Scheme:"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForCustomer" runat="server" Text="Customer:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForBlock" runat="server" Text="Block"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForTelephone" runat="server" Text="Telephone:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForPostCode" runat="server" Text="Postcode:"></asp:Label>&nbsp;</b>
                </td>
                <td style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblPostCode" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForMobile" runat="server" Text="Mobile:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForHouseNumber" runat="server" Text="Address:"></asp:Label>&nbsp;</b>
                </td>
                <td rowspan="2" style="width: 35%; vertical-align: top;">
                    <asp:Label ID="lblHouseNumber" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 10%;">
                    <b>
                        <asp:Label ID="lblForEmail" runat="server" Text="Email:"></asp:Label></b>&nbsp;
                </td>
                <td style="width: 35%;">
                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 10%;">
                </td>
                <td style="width: 35%;">
                    <asp:Button ID="btn_CustDetailUpdate" runat="server" Text="Update customer details"
                        Visible="false" />
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr style="vertical-align: middle;">
                <td colspan="6" style="border-top: 1px solid #7F7F7F;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="vertical-align: top;">
                    <b>Asbestos:</b>
                </td>
                <td colspan="3">
                    <div style="height: 100px; width: 400px; overflow: auto;">
                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            BorderStyle="None" GridLines="None" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td colspan="4">
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td colspan="4">
                    <div style="width: 100%; text-align: right; clear: both;">
                        <div class="text_a_r_padd__r_20px">
                            <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" CssClass="margin_right20"
                                Width="100px" />
                            <asp:Button ID="btnUpdate" runat="server" CssClass="margin_right20" Text="Update"
                                Width="100px" Enabled="false" />
                            <input id="btnPrintJobSheetSummary" type="button" value="Print Job Sheet" CssClass="margin_right20"
                                onclick="PrintJobSheet_AC()" />
                        </div>
                    </div>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>

<ajaxToolkit:ModalPopupExtender ID="mdlPopupAssignedToContractorJobsheet" runat="server"
    TargetControlID="lblJSN" PopupControlID="pnlJobSheetSummary" CancelControlID="btnBack"
    BackgroundCssClass="modalBackground" Enabled="true" DropShadow="true">
</ajaxToolkit:ModalPopupExtender>
<!-- Assigned to Contractor job sheet!-->
<%-- Contractor Jobsheet --%>
<asp:Panel ID="pnlContractorJobsheet" runat="server" BorderColor="Black" BorderStyle="Solid"
    BorderWidth="1px" BackColor="White">
    <prop:ContractorJobSheetSummary ID="ucContractorJobSheetSummary" runat="server" />
</asp:Panel>
<asp:Label ID="lblContractorJobsheet" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopupContractorJobsheet" runat="server" TargetControlID="lblContractorJobsheet"
    PopupControlID="pnlContractorJobsheet" BackgroundCssClass="modalBackground" Enabled="true"
    DropShadow="true">
</asp:ModalPopupExtender>
<%-----------------%>
<%--  LETTER DOC --%>
<asp:Panel ID="pnlLetterDoc" runat="server" BorderColor="Black" BorderStyle="Solid"
    BorderWidth="1px" BackColor="White">
    <div style="border-bottom: 1px solid black; height: 25px">
        <div style="padding: 5px; float: left">
            Documents</div>
        <div style="float: right;">
            <img alt="Close Letter Popup" src="../../Images/cross2.png" style="border: 0px none white;
                cursor: pointer" id="imgBtnLetterPopupClose" />
        </div>
    </div>
    <div>
        <table style="width: 100%;">
            <tr>
                <td width="140px">
                    <img alt="" src="../../Images/paperclip_img.jpg" style="border-style: none; vertical-align: middle;" />
                    Standard Letter
                </td>
                <td>
                    <asp:Panel ID="pnlLetters" BorderWidth="1" runat="server" Height="110px" ScrollBars="Vertical"
                        Width="200px">
                        <asp:DataList ID="dataListLetters" runat="server" Width="180px">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkBtnLetter" runat="server" Text='<%# Eval("LetterTitle") %>'
                                                CommandArgument='<%# Eval("LetterHistoryId") %>'></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgbtnEmailStatus" OnClick="imgbtnEmailStatus_Click" runat="server"
                                                BorderWidth="0px" Width="20px" Height="20px" BorderStyle="None" CssClass="Noticification"
                                                ImageUrl='<%# Eval("EmailImagePath") %>' CommandArgument='<%# Eval("LetterHistoryId").ToString() + ";" + Eval("AppointmentId").ToString() %>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <SelectedItemStyle BackColor="#CCCCCC" BorderColor="Black" />
                        </asp:DataList>
                        <asp:Label ID="lblNoLetters" runat="server" Font-Bold="True" Width="169px"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" src="../../Images/Document%20Icon.jpg" style="border-style: none; vertical-align: middle;" />
                    Documents
                </td>
                <td>
                    <asp:Panel ID="pnlDocs" runat="server" BorderWidth="1" Height="110px" ScrollBars="Vertical"
                        Width="200px">
                        <asp:DataList ID="dataListDocs" runat="server" Width="180px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnDoc" runat="server" Text='<%#Eval("DocumentName") %>' CommandArgument='<%# Eval("DocumentId").toString() + ";;" + Eval("JournalHistoryId").toString() %>'></asp:LinkButton>
                            </ItemTemplate>
                            <SelectedItemStyle BackColor="#CCCCCC" BorderColor="Black" />
                        </asp:DataList>
                        <asp:Label ID="lblNoDoc" runat="server" Font-Bold="True" Width="169px"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Label ID="lbldispActivity" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpLetterDoc" runat="server" TargetControlID="lbldispActivity"
    PopupControlID="pnlLetterDoc" Enabled="true" DropShadow="true" CancelControlID="imgBtnLetterPopupClose">
</asp:ModalPopupExtender>
<%--Appointment Notes Popup Start--%>
<asp:Panel ID="pnlAppointmentNotesPopup" runat="server">
    <appointmentNotesTag:AppointmentNotes ID="ucAppointmentNotes" runat="server" MaxValue="10"
        MinValue="1" />
</asp:Panel>
<asp:Label ID="lblDisplayAppointmentNotesPopup" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="mdlAppointmentNotesPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplayAppointmentNotesPopup" PopupControlID="pnlAppointmentNotesPopup"
    DropShadow="true" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Appointment Notes Popup End--%>
<%-----------------%>
<%--  ADD ACTIVITY --%>
<asp:Panel ID="pnlAddActivity" runat="server" BackColor="White" Width="530px" Height="652px">
    <table id="pnlAddActivityTable" class="" style="height: 652px;">
        <tr>
            <td colspan="3" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px; padding-top: 5px;">
                    Add Activity</div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="3">
                <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                    <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Action Added By:
            </td>
            <td align="left" valign="top">
                <asp:Label ID="lblAddedBy" runat="server"></asp:Label>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Type:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlInspectionType" runat="server">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Date:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtDate" PopupButtonID="imgCalDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>&nbsp;<img alt="Open Calendar"
                    src="../../Images/calendar.png" id="imgCalDate" />
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Status:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Action:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Letter:
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlLetter" runat="server">
                    <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
                <div style="float: left; width: 179px;">
                    <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openUploadWindow()"
                        BackColor="White" /></div>
                <div style="float: left;">
                    <asp:Button ID="btnViewLetter" Width="71" runat="server" Text="View Letter" OnClientClick='openEditLetterWindow()'
                        BackColor="White" /></div>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Docs:
            </td>
            <td align="left" valign="top">
                <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                    <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                        <ItemTemplate>
                            <table class="LetterDocInnerTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                            Visible="<%# False %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                            Visible="<%# False %>"></asp:Label>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                            BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                            OnClick="imgBtnRemoveLetterDoc_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <SelectedItemStyle BackColor="Gray" />
                    </asp:DataList>
                </asp:Panel>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="left" valign="top">
                <%--<asp:AsyncFileUpload ID="asyncDocUpload" runat="server" ErrorBackColor="Red" ThrobberID="imgUploadProgress"
                    Width="179px" UploaderStyle="Traditional" OnClientUploadComplete="uploadComplete"
                    OnUploadedComplete="asyncDocUpload_UploadedComplete" OnClientUploadError="uploadError" />
                <asp:Label ID="imgUploadProgress" runat="server" Style="display: none">
                    <img alt="" src="../../Images/uploading.gif" />
                </asp:Label>--%>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Notes:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;">
                <input id="btnCancelAction" type="button" value="Cancel" style="background-color: White;" />
            </td>
            <td align="left" valign="top" style="text-align: left;">
                <div>
                    <asp:Button ID="btnSaveAction" runat="server" Text="Save Action" BackColor="White" />
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddActivity" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAction" PopupControlID="pnlAddActivity"
    DropShadow="true" CancelControlID="btnCancelAction">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispAction" runat="server"></asp:Label>
<%-------------------%>
&nbsp;<asp:CheckBox ID="ckBoxRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<%--  <%Else%>--%>
<%-- Defect Detail POPUP - Start --%>
<asp:Panel ID="pnlAddDefect" runat="server" BackColor="White" Width="530px" BorderColor="Black"
    BorderStyle="Outset" BorderWidth="1px">
    <defect:defectmanegement runat="server" ID="ucDefectManagement" />
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdlPoPUpAddDefect" runat="server" Enabled="True"
    BackgroundCssClass="modalBackground" TargetControlID="lblDispDefect" PopupControlID="pnlAddDefect"
    DropShadow="true">
</ajaxToolkit:ModalPopupExtender>
<asp:Label ID="lblDispDefect" runat="server"></asp:Label>
<%-- Defect Detail POPUP - End --%>
<%-- Defect Detail Images POPUP - Start --%>
<asp:Panel ID="pnlShowDefectImages" runat="server" BackColor="White" Width="530px"
    BorderColor="Black" BorderStyle="Outset" BorderWidth="1px">
    <dpImages:DefectImages runat="server" ID="ucDefectImages" />
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpShowDefectImages" runat="server" Enabled="True"
    BackgroundCssClass="modalBackground" TargetControlID="lblDispDefectImages" PopupControlID="pnlShowDefectImages"
    DropShadow="true">
</ajaxToolkit:ModalPopupExtender>
<asp:Label ID="lblDispDefectImages" runat="server"></asp:Label>
<%-- Defect Detail Images POPUP - End --%>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpSendEmail" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlSendEmail"
    DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnCancelAction"
    BehaviorID="mdlPopupAddAppointmentBehaviorId">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSendEmail" CssClass="left" runat="server" BackColor="White" Width="470px"
    Height="200px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
    position: absolute; display: none /*-bracket-: hack(; left: 430px !important;
    top: 100px; ); */">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Send Email</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseSendEmail" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="overflow-y: scroll; height: 150px; padding-top: 5px !important;">
        <asp:UpdatePanel ID="updPanelSendEmailPopup" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="pnlSendEmailMessage" runat="server">
                    <asp:Label ID="lblSendEmailMessage" runat="server" Text="" CssClass="lblMessage"></asp:Label>
                </asp:Panel>
                <table>
                    <tr>
                        <td colspan="3" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="3">
                            <asp:Panel ID="Panel1" runat="server" Visible="false">
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Send To:
                        </td>
                        <td align="left" valign="top">
                            <asp:HiddenField ID="hdnLetterHistoryId" runat="server" />
                            <asp:HiddenField ID="hdnAppointmentId" runat="server" />
                            <asp:TextBox ID="txtSendTo" runat="server" Width="246px"></asp:TextBox>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="add-apt-canel-save">
            <asp:Button ID="Button1" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
            <asp:Button ID="btnSendAction" runat="server" Text="Send Email" BackColor="White" />
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
    Width="390px" Height="190px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Cancel Notes</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="width: 100%; height: 490px; overflow: hidden;">
        <div style="height: 370px; padding-top: 20px !important; padding-left: 20px;">
            <table>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
    DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
    BehaviorID="mdlPopUpCancelFaults">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>