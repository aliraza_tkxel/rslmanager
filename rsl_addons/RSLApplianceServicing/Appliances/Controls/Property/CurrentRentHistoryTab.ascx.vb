﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class CurrentRentHistory
    Inherits UserControlBase

#Region "Properties"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "SID", 1, 7)
    Dim totalCount As Integer = 0
    Protected uniqueKey As String
#End Region
#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblCurrentRentTab, pnlCurrentRentMessage)


        If Not IsPostBack Then

        End If
    End Sub
#End Region


#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            bindToGrid(SessionManager.getPropertyId())

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region


    Protected Sub imgCurrentRentHistoryArrow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim resultDataSet As DataSet = New DataSet()

            Dim rowIndex As Integer
            Dim commandArgument As String = sender.CommandArgument

            resetControls()
            If commandArgument > 0 Then
                Dim rowId As Integer = 0
                Me.mdlCurrentRentHistoryPopUp.Show()
                rowIndex = commandArgument
                resultDataSet = SessionManager.getRentHistoryDataSet()

                Dim applianceResult = (From ps In resultDataSet.Tables(0) Where ps.Item("SID") = rowIndex Select ps)
                If applianceResult.Count > 0 Then
                    lblChangedRentType.Text = If(Not IsDBNull(applianceResult.First.Item("Type")), applianceResult.First.Item("Type"), String.Empty)
                    lblChangedRentFrequency.Text = If(Not IsDBNull(applianceResult.First.Item("RentFrequencyDesc")), applianceResult.First.Item("RentFrequencyDesc"), String.Empty)
                    lblChangedRent.Text = If(Not IsDBNull(applianceResult.First.Item("Rent")), Convert.ToDouble(applianceResult.First.Item("Rent")).ToString("N2"), String.Empty)
                    lblChangedServices.Text = If(Not IsDBNull(applianceResult.First.Item("SERVICES")), Convert.ToDouble(applianceResult.First.Item("SERVICES")).ToString("N2"), String.Empty)
                    lblChangedTotalRent.Text = If(Not IsDBNull(applianceResult.First.Item("TotalRent")), Convert.ToDouble(applianceResult.First.Item("TotalRent")).ToString("N2"), String.Empty)
                    lblChangedCouncilTax.Text = If(Not IsDBNull(applianceResult.First.Item("COUNCILTAX")), Convert.ToDouble(applianceResult.First.Item("COUNCILTAX")).ToString("N2"), String.Empty)

                    lblChangedWater.Text = If(Not IsDBNull(applianceResult.First.Item("WATERRATES")), Convert.ToDouble(applianceResult.First.Item("WATERRATES")).ToString("N2"), String.Empty)
                    lblChangedInelig.Text = If(Not IsDBNull(applianceResult.First.Item("INELIGSERV")), Convert.ToDouble(applianceResult.First.Item("INELIGSERV")).ToString("N2"), String.Empty)
                    lblChangedSupp.Text = If(Not IsDBNull(applianceResult.First.Item("SUPPORTEDSERVICES")), Convert.ToDouble(applianceResult.First.Item("SUPPORTEDSERVICES")).ToString("N2"), String.Empty)
                    lblChangedGarage.Text = If(Not IsDBNull(applianceResult.First.Item("GARAGE")), Convert.ToDouble(applianceResult.First.Item("GARAGE")).ToString("N2"), String.Empty)
                    lblAmended.Text = If(Not IsDBNull(applianceResult.First.Item("Updated")), Convert.ToDateTime(applianceResult.First.Item("Updated")).ToString("dd/MM/yyyy"), String.Empty)
                    lblBy.Text = If(Not IsDBNull(applianceResult.First.Item("ByFullName")), applianceResult.First.Item("ByFullName").ToString(), String.Empty)

                    lblChangedDateRentSet.Text = If(Not IsDBNull(applianceResult.First.Item("RentSet")), Convert.ToDateTime(applianceResult.First.Item("RentSet")).ToString("dd/MM/yyyy"), String.Empty)
                    lblChangedRentEffective.Text = If(Not IsDBNull(applianceResult.First.Item("RENTEFFECTIVE")), Convert.ToDateTime(applianceResult.First.Item("RENTEFFECTIVE")).ToString("dd/MM/yyyy"), String.Empty)
                    rowId = applianceResult.First.Item("row")
                End If
                If rowId > 0 Then
                    rowId = rowId + 1
                    Dim previousRentResult = (From ps In resultDataSet.Tables(0) Where ps.Item("row") = rowId Select ps)
                    If previousRentResult.Count > 0 Then
                        lblRentType.Text = If(Not IsDBNull(previousRentResult.First.Item("Type")), previousRentResult.First.Item("Type"), String.Empty)
                        lblPreviousRentFrequency.Text = If(Not IsDBNull(previousRentResult.First.Item("RentFrequencyDesc")), previousRentResult.First.Item("RentFrequencyDesc"), String.Empty)
                        lblCouncilTax.Text = If(Not IsDBNull(previousRentResult.First.Item("COUNCILTAX")), Convert.ToDouble(previousRentResult.First.Item("COUNCILTAX")).ToString("N2"), String.Empty)
                        lblWater.Text = If(Not IsDBNull(previousRentResult.First.Item("WATERRATES")), Convert.ToDouble(previousRentResult.First.Item("WATERRATES")).ToString("N2"), String.Empty)
                        lblInelig.Text = If(Not IsDBNull(previousRentResult.First.Item("INELIGSERV")), Convert.ToDouble(previousRentResult.First.Item("INELIGSERV")).ToString("N2"), String.Empty)
                        lblSupp.Text = If(Not IsDBNull(previousRentResult.First.Item("SUPPORTEDSERVICES")), Convert.ToDouble(previousRentResult.First.Item("SUPPORTEDSERVICES")).ToString("N2"), String.Empty)
                        lblGarage.Text = If(Not IsDBNull(previousRentResult.First.Item("GARAGE")), Convert.ToDouble(previousRentResult.First.Item("GARAGE")).ToString("N2"), String.Empty)
                        lblPreviousDateRentSet.Text = If(Not IsDBNull(previousRentResult.First.Item("RentSet")), Convert.ToDateTime(previousRentResult.First.Item("RentSet")).ToString("dd/MM/yyyy"), String.Empty)
                        lblPreviousRentEffective.Text = If(Not IsDBNull(previousRentResult.First.Item("RENTEFFECTIVE")), Convert.ToDateTime(previousRentResult.First.Item("RENTEFFECTIVE")).ToString("dd/MM/yyyy"), String.Empty)
                        lblPreviousRent.Text = If(Not IsDBNull(previousRentResult.First.Item("Rent")), Convert.ToDouble(previousRentResult.First.Item("Rent")).ToString("N2"), String.Empty)
                        lblPreviousServices.Text = If(Not IsDBNull(previousRentResult.First.Item("SERVICES")), Convert.ToDouble(previousRentResult.First.Item("SERVICES")).ToString("N2"), String.Empty)
                        lblPreviousTotalRent.Text = If(Not IsDBNull(previousRentResult.First.Item("TotalRent")), Convert.ToDouble(previousRentResult.First.Item("TotalRent")).ToString("N2"), String.Empty)

                    End If
                End If

                If lblCouncilTax.Text = lblChangedCouncilTax.Text Then
                    lblChangedCouncilTax.Visible = False
                End If
                If lblWater.Text = lblChangedWater.Text Then
                    lblChangedWater.Visible = False
                End If
                If lblInelig.Text = lblChangedInelig.Text Then
                    lblChangedInelig.Visible = False
                End If
                If lblSupp.Text = lblChangedSupp.Text Then
                    lblChangedSupp.Visible = False
                End If
                If lblGarage.Text = lblChangedGarage.Text Then
                    lblChangedGarage.Visible = False
                End If
                If lblChangedRent.Text = lblPreviousRent.Text Then
                    lblChangedRent.Visible = False
                End If
                If lblPreviousServices.Text = lblChangedServices.Text Then
                    lblChangedServices.Visible = False
                End If
                If lblPreviousTotalRent.Text = lblChangedTotalRent.Text Then
                    lblChangedTotalRent.Visible = False
                End If
                If lblPreviousRentFrequency.Text = lblChangedRentFrequency.Text Then
                    lblChangedRentFrequency.Visible = False
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region
#Region "Functions"

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)

                bindToGrid(SessionManager.getPropertyId())
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Bind To Grid"

    Public Sub bindToGrid(ByVal propertyId As String)
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPageSortBo = getPageSortBoViewState()

        'If (totalCount > 0) Then
        '    objPageSortBo.PageSize = 7
        '    If Math.Ceiling(totalCount / 7) = Math.Ceiling(totalCount / 8) Then
        '        objPageSortBo.PageSize = 8
        '    End If
        'End If

        totalCount = objPropertyBL.getRentHistory(resultDataSet, propertyId, objPageSortBo)
        If (totalCount > 0) Then
            SessionManager.setRentHistoryDataSet(resultDataSet)
            Dim resultDt As DataTable = New DataTable()
            resultDt = resultDataSet.Tables(0).Copy
            If resultDataSet.Tables(0).Rows.Count = 8 Then

                resultDt.Rows(7).Delete()
            End If
            objPageSortBo.PageSize = 7
            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

            grdRentHistory.VirtualItemCount = totalCount
            grdRentHistory.DataSource = resultDt
            grdRentHistory.DataBind()
            ViewState.Item(ViewStateConstants.TotalCount) = totalCount
            'setPageSortBoViewState(objPageSortBo)
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
            divRentHistory.Visible = True
        Else
            divRentHistory.Visible = False
            uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, UserMessageConstants.NoRecordFound, True)
        End If
    End Sub


#End Region
#Region "Reset Controls"
    Sub resetControls()
        lblPreviousDateRentSet.Text = ""
        lblPreviousRentEffective.Text = ""
        lblRentType.Text = ""
        lblPreviousRent.Text = ""
        lblPreviousServices.Text = ""
        lblCouncilTax.Text = ""
        lblWater.Text = ""
        lblInelig.Text = ""
        lblSupp.Text = ""
        lblGarage.Text = ""
        lblPreviousTotalRent.Text = ""
        lblPreviousRentFrequency.Text = ""
        lblChangedCouncilTax.Visible = True
        lblChangedWater.Visible = True
        lblChangedInelig.Visible = True
        lblChangedSupp.Visible = True
        lblChangedGarage.Visible = True
        lblChangedRent.Visible = True
        lblChangedServices.Visible = True
        lblChangedTotalRent.Visible = True
        lblChangedRentFrequency.Visible = True

    End Sub
#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub
#End Region

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub



#End Region
#End Region


End Class