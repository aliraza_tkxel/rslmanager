﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ActivitiesOld.ascx.vb"
    Inherits="Appliances.ActivitiesOld" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="prop" TagName="ContractorJobSheetSummary" Src="~/Controls/Property/ContractorJobSheetSummary.ascx" %>

<%@ Import Namespace="AS_Utilities" %>
<div style="width: 100%; float: left;">
    <div style="width: 50%; float: left; padding-top: 10px;">
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
    </div>
    <div class="mainheading-panel" style="margin-top: 5px; margin-left: 10px; width: 98%;">
        <div style="float: right; text-align: center; margin-top: -4px;">
            <asp:DropDownList ID="ddlActionType" runat="server" AutoPostBack="true" CssClass="acitivity-dropdown">
            </asp:DropDownList>
            <asp:Button ID="btnPrintActivities" runat="server" Text="Print Activities" BackColor="White" />&nbsp;&nbsp;
        </div>
    </div>
</div>
<div style="clear: both">
</div>
<div style="margin-top: 5px; margin-left: 10px; margin-right: 10px; border: 1px solid black;">
    <asp:GridView ID="grdActivities" runat="server" AutoGenerateColumns="False" BackColor="White"
        BorderColor="Gray" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="5"
        ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True" ShowHeaderWhenEmpty="True"
        PageSize="25" AllowPaging="true">
        <Columns>
            <asp:TemplateField HeaderText="Date" SortExpression="CreateDate">
                <ItemTemplate>
                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type" SortExpression="InspectionType">
                <ItemTemplate>
                    <asp:Label ID="lblInspectionType" runat="server" Text='<%# Bind("InspectionType") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="Status">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ref" SortExpression="Ref">
                <ItemTemplate>
                    <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment" SortExpression="AppointmentDate">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("AppointmentDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Operative" SortExpression="OPERATIVENAME">
                <ItemTemplate>
                    <asp:Label ID="lblOperativeName" runat="server" Text='<%# Bind("OPERATIVENAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trade" SortExpression="OPERATIVETRADE">
                <ItemTemplate>
                    <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("OPERATIVETRADE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action" SortExpression="Action">
                <ItemTemplate>
                    <asp:Label ID="lblAction" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recorded By" SortExpression="Name">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgBtnLetterDoc" runat="server" BorderStyle="None" ImageUrl="~/Images/paperclip_img.jpg"
                        Visible='<%# Eval("IsLetterAttached").ToString() %>' OnClick="imgBtnLetterDoc_Click"
                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:HyperLink ID="lnkID" runat="server" Visible='<%# CheckCp12Status(Eval("Status"), Eval("CP12DocumentID")) %>'
                        Target="_blank" ImageUrl="~/Images/document.png" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?HistoryCP12DocumentID={0}") %>' />
                    <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionID(Eval("IsDocumentAttached"), Eval("CP12DocumentID"))%>'
                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("CP12DocumentID", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                    <asp:ImageButton ID="imgBtnPlannedJobsheet" runat="server" BorderStyle="None" ImageUrl="~/Images/jobsheetclip.png"
                        Visible="false" ToolTip="Planned Job Sheet" OnClientClick='<%# String.Format("JavaScript:openJobSheetSummary({0})",Eval("JournalHistoryId").ToString())%>'
                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                    <asp:HyperLink ID="lnkInspectionDocument" runat="server" Visible='<%# CheckPlannedInspectionID(Eval("IsDocumentAttached"), Eval("Status"))%>'
                        Target="_blank" ImageUrl="~/Images/paperclip_img.jpg" BorderStyle="None" BorderWidth="0px"
                        NavigateUrl='<%# Eval("JournalHistoryId", "~/Views/Common/Download.aspx?inspectionJournalHistoryId={0}") %>' />

                        <asp:ImageButton ID="imgBtnFaultContractorJobsheet" runat="server" BorderStyle="None" ImageUrl="~/Images/jobsheetclip.png"
                        Visible="false" ToolTip="Fault Contractor Job Sheet" OnClick = "imgBtnFaultContractorJobsheet_Click"
                        CommandArgument='<%# Eval("JournalHistoryId") %>' />
                </ItemTemplate>
                <ItemStyle Height="15px" />
            </asp:TemplateField>
            <%-- <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                   
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" BorderColor="White"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Left" />
        <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
        <SelectedRowStyle BackColor="#FFFFCC" Font-Bold="True" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
        <SortedAscendingCellStyle BackColor="#F7F7F7"></SortedAscendingCellStyle>
        <SortedAscendingHeaderStyle BackColor="#4B4B4B"></SortedAscendingHeaderStyle>
        <SortedDescendingCellStyle BackColor="#E5E5E5"></SortedDescendingCellStyle>
        <SortedDescendingHeaderStyle BackColor="#242121"></SortedDescendingHeaderStyle>
    </asp:GridView>
</div>

<%-- Contractor Jobsheet --%>
<asp:Panel ID="pnlContractorJobsheet" runat="server" BorderColor="Black" BorderStyle="Solid"
    BorderWidth="1px" BackColor="White">
    <prop:ContractorJobSheetSummary ID="ucContractorJobSheetSummary" runat="server" />
</asp:Panel>
<asp:Label ID="lblContractorJobsheet" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopupContractorJobsheet" runat="server" TargetControlID="lblContractorJobsheet"
    PopupControlID="pnlContractorJobsheet" BackgroundCssClass="modalBackground" Enabled="true" DropShadow="true">
</asp:ModalPopupExtender>
<%-----------------%>

<%--  LETTER DOC --%>
<asp:Panel ID="pnlLetterDoc" runat="server" BorderColor="Black" BorderStyle="Solid"
    BorderWidth="1px" BackColor="White">
    <div style="border-bottom: 1px solid black; height: 25px">
        <div style="padding: 5px; float: left">
            Documents</div>
        <div style="float: right;">
            <img alt="Close Letter Popup" src="../../Images/cross2.png" style="border: 0px none white;
                cursor: pointer" id="imgBtnLetterPopupClose" />
        </div>
    </div>
    <div>
        <table style="width: 100%;">
            <tr>
                <td width="140px">
                    <img alt="" src="../../Images/paperclip_img.jpg" style="border-style: none; vertical-align: middle;" />
                    Standard Letter
                </td>
                <td>
                    <asp:Panel ID="pnlLetters" BorderWidth="1" runat="server" Height="110px" ScrollBars="Vertical"
                        Width="200px">
                        <asp:DataList ID="dataListLetters" runat="server" Width="180px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLetter" runat="server" Text='<%# Eval("LetterTitle") %>'
                                    CommandArgument='<%# Eval("LetterHistoryId") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <SelectedItemStyle BackColor="#CCCCCC" BorderColor="Black" />
                        </asp:DataList>
                        <asp:Label ID="lblNoLetters" runat="server" Font-Bold="True" Width="169px"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" src="../../Images/Document%20Icon.jpg" style="border-style: none; vertical-align: middle;" />
                    Documents
                </td>
                <td>
                    <asp:Panel ID="pnlDocs" runat="server" BorderWidth="1" Height="110px" ScrollBars="Vertical"
                        Width="200px">
                        <asp:DataList ID="dataListDocs" runat="server" Width="180px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnDoc" runat="server" Text='<%#Eval("DocumentName") %>' CommandArgument='<%# Eval("DocumentId").toString() + ";;" + Eval("JournalHistoryId").toString() %>'></asp:LinkButton>
                            </ItemTemplate>
                            <SelectedItemStyle BackColor="#CCCCCC" BorderColor="Black" />
                        </asp:DataList>
                        <asp:Label ID="lblNoDoc" runat="server" Font-Bold="True" Width="169px"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Label ID="lbldispActivity" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpLetterDoc" runat="server" TargetControlID="lbldispActivity"
    PopupControlID="pnlLetterDoc" Enabled="true" DropShadow="true" CancelControlID="imgBtnLetterPopupClose">
</asp:ModalPopupExtender>
<%-----------------%>
<%--  ADD ACTIVITY --%>
<asp:Panel ID="pnlAddActivity" runat="server" BackColor="White" Width="530px" Height="652px">
    <table id="pnlAddActivityTable" class="" style="height: 652px;">
        <tr>
            <td colspan="3" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px; padding-top: 5px;">
                    Add Activity</div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="3">
                <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                    <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Action Added By:
            </td>
            <td align="left" valign="top">
                <asp:Label ID="lblAddedBy" runat="server"></asp:Label>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Type:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlInspectionType" runat="server">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Date:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtDate" PopupButtonID="imgCalDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>&nbsp;<img alt="Open Calendar"
                    src="../../Images/calendar.png" id="imgCalDate" />
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Status:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Action:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Letter:
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlLetter" runat="server">
                    <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
                <div style="float: left; width: 179px;">
                    <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openUploadWindow()"
                        BackColor="White" /></div>
                <div style="float: left;">
                    <asp:Button ID="btnViewLetter" Width="71" runat="server" Text="View Letter" OnClientClick='openEditLetterWindow()'
                        BackColor="White" /></div>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Docs:
            </td>
            <td align="left" valign="top">
                <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                    <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                        <ItemTemplate>
                            <table class="LetterDocInnerTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                            Visible="<%# False %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                            Visible="<%# False %>"></asp:Label>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                            BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                            OnClick="imgBtnRemoveLetterDoc_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <SelectedItemStyle BackColor="Gray" />
                    </asp:DataList>
                </asp:Panel>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="left" valign="top">
                <%--<asp:AsyncFileUpload ID="asyncDocUpload" runat="server" ErrorBackColor="Red" ThrobberID="imgUploadProgress"
                    Width="179px" UploaderStyle="Traditional" OnClientUploadComplete="uploadComplete"
                    OnUploadedComplete="asyncDocUpload_UploadedComplete" OnClientUploadError="uploadError" />
                <asp:Label ID="imgUploadProgress" runat="server" Style="display: none">
                    <img alt="" src="../../Images/uploading.gif" />
                </asp:Label>--%>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-left: 10px;">
                Notes:<span class="Required">*</span>
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;">
                <input id="btnCancelAction" type="button" value="Cancel" style="background-color: White;" />
            </td>
            <td align="left" valign="top" style="text-align: left;">
                <div>
                    <asp:Button ID="btnSaveAction" runat="server" Text="Save Action" BackColor="White" />
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddActivity" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAction" PopupControlID="pnlAddActivity"
    DropShadow="true" CancelControlID="btnCancelAction">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispAction" runat="server"></asp:Label>
<%-------------------%>
&nbsp;<asp:CheckBox ID="ckBoxRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<%--  <%Else%>--%>
