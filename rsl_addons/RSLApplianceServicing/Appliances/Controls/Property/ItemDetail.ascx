﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ItemDetail.ascx.vb"
    Inherits="Appliances.ItemDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="appliance" TagName="ApplianceTab" Src="~/Controls/Property/Appliance.ascx" %>
<%@ Register TagPrefix="details" TagName="DetailsTab" Src="~/Controls/Property/DetailsTab.ascx" %>
<%@ Register TagPrefix="photos" TagName="Photographs" Src="~/Controls/Property/Photographs.ascx" %>
<%@ Register TagPrefix="notes" TagName="Notes" Src="~/Controls/Property/ItemNotes.ascx" %>
<%@ Register TagPrefix="summary" TagName="Summary" Src="~/Controls/Property/AttributeSummary.ascx" %>
<%@ Register TagPrefix="defect" TagName="Defect" Src="~/Controls/Property/HeatingDefect.ascx" %>
<%@ Register TagPrefix="detector" TagName="detector" Src="~/Controls/Property/Detectors.ascx" %>
<style type="text/css">
    .style1
    {
        width: 131px;
    }
</style>
<asp:Panel ID="pnlMessage" runat="server" Visible="false" Style='margin-left: 15px;'>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<div style="padding: 10px 10px 0px 10px;">
    <asp:LinkButton ID="lnkBtnSummaryTab" OnClick="lnkBtnummaryTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Summary: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnAppliancesTab" OnClick="lnkBtnAppliancesTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black" Visible="false">Appliances: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnDetailTab" OnClick="lnkBtnDetailTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Details: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnDetectorTab" OnClick="lnkBtnDetectorTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black" Visible="false">Detector: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnNotesTab" OnClick="lnkBtnNotesTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Notes: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnPhotographsTab" OnClick="lnkBtnPhotographsTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Photographs: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnDefectsTab" runat="server" CssClass="TabInitial" BorderStyle="Solid"
        BorderColor="Black" Visible="false">Defects:</asp:LinkButton>
    <asp:Button runat="server" ID="btnAmend" Text="Amend" Style="float: right; margin: 10px 11px 0 0px;"
        BackColor="White" Visible="false" ValidationGroup="save" />
    <asp:Button runat="server" ID="btnDelete" Text="Delete" Style="float: right; margin: 10px 11px 0 0px;"
        BackColor="White" Visible="false" />
    <div style="border: 1px solid black; height: 500px; overflow: scroll; clear: both;
        margin-left: 2px; width: 98%; padding-left: 10px; float: left; padding-right: 10px;
        padding-bottom: 20px; margin-top: 7px">
        <div style="clear: both; margin-bottom: 5px;">
        </div>
        <br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View0" runat="server">
                        <summary:Summary runat="server" ID="summaryTab" />
                    </asp:View>
                    <asp:View ID="View1" runat="server">
                        <details:DetailsTab runat="server" ID="detailsTabID" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <notes:Notes runat="server" ID="notesTab" />
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <photos:Photographs runat="server" ID="photosTab" />
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <appliance:ApplianceTab runat="server" ID="applianceTabId" />
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <defect:Defect ID="defectTab" runat="server" />
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <detector:detector ID="detectorTab" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updPanelNotes" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlDeletePopup" runat="server" BackColor="White" Width="340px">
                    <table width="300px" style='margin: 20px;'>
                        <tr>
                            <td colspan="2" valign="top">
                                <div style="float: left; font-weight: bold; padding-top: 10px;">
                                    Alert</div>
                                <div style="clear: both">
                                </div>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Are you Sure! you want to delete Heating Type?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp; &nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                            <td align="right" valign="top" style="text-align: right;" class="style1">
                                <div style='margin-top: 5px;'>
                                    <input id="btnCancel" type="button" value="No" style="background-color: White; width: 50px;" />
                                    &nbsp;
                                    <asp:Button ID="btnSave" runat="server" Text="Yes" BackColor="White" Width="50px" />
                                    &nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ajaxToolkit:ModalPopupExtender ID="mdlPopUpDelete" runat="server" DynamicServicePath=""
            Enabled="True" PopupControlID="pnlDeletePopup" DropShadow="true" CancelControlID="btnCancel"
            TargetControlID="lblDisp" BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Label ID="lblDisp" runat="server"></asp:Label>
    </div>
</div>
