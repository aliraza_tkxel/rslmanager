﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports AjaxControlToolkit

Public Class ActivitiesOld
    Inherits UserControlBase

#Region "Attributes"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objUsersBL As UsersBL = New UsersBL()
    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objResourceBL As ResourcesBL = New ResourcesBL()
    Dim sortDirection As String
    Dim sortExpression As String
    Dim propertyId As String


#End Region

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblActivityPopupMessage, pnlActivityPopupMessage)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ClientScriptName", "uploadComplete", True)

            'Dim objContainerPage As PropertyRecord = New PropertyRecord()

            If Not IsNothing(Request.QueryString(PathConstants.PropertyIds)) Then
                Me.propertyId = Request.QueryString(PathConstants.PropertyIds)
            Else
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Grid Activities Row Data bound"

    Protected Sub grdActivities_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdActivities.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim imgBtnPlannedJobsheet As ImageButton = DirectCast(e.Row.FindControl("imgBtnPlannedJobsheet"), ImageButton)
                Dim imgBtnFaultContractorJobsheet As ImageButton = DirectCast(e.Row.FindControl("imgBtnFaultContractorJobsheet"), ImageButton)
                Dim lblInspectionType As Label = DirectCast(e.Row.FindControl("lblInspectionType"), Label)
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)

                Dim inspectionType As String = lblInspectionType.Text.Split(" ")(0)
                If (inspectionType.Equals(ApplicationConstants.PlannedActiontype) _
                    AndAlso Not lblStatus.Text.ToString.StartsWith(ApplicationConstants.InspectionArrangedStatus) _
                    AndAlso Not ApplicationConstants.ToBeArrangedStatus.Equals(lblStatus.Text.ToString()) _
                    AndAlso Not ApplicationConstants.ConditionToBeArrangedStatus.Equals(lblStatus.Text.ToString())) Then
                    imgBtnPlannedJobsheet.Visible = True
                End If

                If (inspectionType.Equals(ApplicationConstants.FaultActiontype) _
                  AndAlso lblStatus.Text.ToString.StartsWith(ApplicationConstants.AssignedToContractorStatus)) Then
                    imgBtnFaultContractorJobsheet.Visible = True
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Grid Activities Sorting"

    Protected Sub grdActivities_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdActivities.Sorting
        Try
            Me.sortExpression = e.SortExpression
            ViewState(ViewStateConstants.SortExpression) = e.SortExpression
            Me.sortActivitiesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl ActionType Selected Index Changed"

    Protected Sub ddlActionType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActionType.SelectedIndexChanged
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Me.populateActivitiesGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Letter Doc Click"
    Protected Sub imgBtnLetterDoc_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            Dim journalHistoryId As Int64 = CType(btn.CommandArgument(), Integer)
            Me.resetLetterDocLabels()
            Me.getPropertyLetters(journalHistoryId)
            Me.getPropertyDocuments(journalHistoryId)
            Me.bindUrlsWithDocLinkButton(sender)
            Me.bindUrlsWithLetterLinkButton()
            mdlPopUpLetterDoc.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Job sheet Click"
    Protected Sub imgBtnPlannedJobsheet_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Fault Contractor Job sheet Click"
    Protected Sub imgBtnFaultContractorJobsheet_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtnContractorJobsheet As ImageButton = DirectCast(sender, ImageButton)
            Dim faultHistoryId As Int32 = Convert.ToInt32(imgBtnContractorJobsheet.CommandArgument)
            ucContractorJobSheetSummary.showPopupJobSheetSummary(faultHistoryId)
            mdlPopupContractorJobsheet.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add Activity Popup Events"


#Region "ddl Status Selected Index Changed"

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            Dim statusId As Integer = CType(ddlStatus.SelectedValue(), Integer)
            Me.loadActions(statusId)
            Me.loadLetters(-1)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAction.SelectedValue(), Integer)
            Me.loadLetters(actionId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try
    End Sub

#End Region

#Region "async Doc Upload Uploaded Complete"
    'Protected Sub asyncDocUpload_UploadedComplete(sender As Object, e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles asyncDocUpload.UploadedComplete

    '    If IsNothing(Session(SessionConstants.DocumentName)) Then

    '        If (asyncDocUpload.HasFile) Then

    '            Dim filePath As String = String.Empty
    '            Dim fileName As String = String.Empty
    '            filePath = GeneralHelper.getDocumentUploadPath()

    '            If IsNothing(filePath) Then
    '                Me.asyncDocUpload.FailedValidation = True
    '            End If

    '            If (Directory.Exists(filePath) = False) Then
    '                Directory.CreateDirectory(filePath)
    '            Else
    '                fileName = Path.GetFileName(e.FileName)
    '                Dim filePathName As String = filePath + fileName
    '                asyncDocUpload.SaveAs(filePathName)
    '                Session(SessionConstants.DocumentName) = fileName

    '            End If
    '        End If
    '    Else
    '        Try
    '            Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.Message = ex.Message

    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If

    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
    '            End If
    '            mdlPopUpAddActivity.Show()
    '        End Try
    '    End If


    'End Sub
#End Region

#Region "Btn Save Action Click"
    Protected Sub btnSaveAction_Click(sender As Object, e As EventArgs) Handles btnSaveAction.Click

        Dim isSaved As Boolean = False
        Try
            If (Me.validateAddActivity() = True) Then
                Dim journalHistoryId As Integer = 0

                journalHistoryId = saveAddActivityPopup()

                If journalHistoryId > 0 Then
                    Me.populateActivitiesGrid()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveActvitySuccessfully, False)
                    isSaved = True
                    Me.resetAddActivityControls()

                    'To remove the record of existing letters from the session,
                    'So these these letters can be re attached on re arrange.
                    SessionManager.removeActivityLetterDetail()

                Else
                    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            If isSaved = False Then
                Me.mdlPopUpAddActivity.Show()
            Else
                Me.mdlPopUpAddActivity.Hide()
            End If
        End Try
    End Sub
#End Region

#Region "btn View Letter Click"
    Protected Sub btnViewLetter_Click(sender As Object, e As EventArgs) Handles btnViewLetter.Click
        Try
            If (ddlLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
            SessionManager.setPropertyIdForEditLetter(Me.propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try

    End Sub
#End Region

#Region "btn Upload Letter Click"

    Protected Sub btnUploadLetter_Click(sender As Object, e As EventArgs) Handles btnUploadLetter.Click
        Try
            mdlPopUpAddActivity.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ckBox Refresh DataSet Checked Changed"

    Protected Sub ckBoxRefreshDataSet_CheckedChanged(sender As Object, e As EventArgs) Handles ckBoxRefreshDataSet.CheckedChanged
        Me.ckBoxRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId())
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try

    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"
    Protected Sub ckBoxDocumentUpload_CheckedChanged(sender As Object, e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "Btn Print Activities"
    Protected Sub btnPrintActivities_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrintActivities.Click
        Response.Redirect("~/Views/Property/PrintActivities.aspx?Id=" + Me.propertyId, False)
    End Sub
#End Region

#End Region

#End Region

#Region "Functions"
#Region "show Property Activities "

    Public Sub loadPropertyActivities()
        Try
            btnPrintActivities.Visible = True
            ddlActionType.Visible = True
            'printActivities.Visible = False
            Me.loadInspectionTypes(Me.ddlActionType)
            Me.setDefaultSort()
            Me.populateActivitiesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Load Inspection Types in DropDown"
    Public Sub loadInspectionTypes(ByRef dropDownRef As DropDownList)

        Dim resultDataSet As DataSet = New DataSet()
        objUsersBL.getInspectionTypes(resultDataSet)
        dropDownRef.DataSource = resultDataSet
        dropDownRef.DataValueField = ApplicationConstants.InspectionId
        dropDownRef.DataTextField = ApplicationConstants.InspectionName
        dropDownRef.DataBind()
        dropDownRef.Items.Add(New ListItem("Select Type", ApplicationConstants.DropDownDefaultValue))
        dropDownRef.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "populate Activities Grid "

    Public Sub populateActivitiesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyID As String = Request.QueryString(PathConstants.PropertyIds)

        Me.sortDirection = ViewState(ViewStateConstants.SortDirection)
        Me.sortExpression = ViewState(ViewStateConstants.SortExpression)

        objPropertyBl.propertyActivitiesList(resultDataSet, Me.sortDirection, Me.sortExpression, ddlActionType.SelectedValue, propertyID)
        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Me.grdActivities.Visible = False
        Else
            Me.grdActivities.Visible = True
            ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
            Me.grdActivities.DataSource = resultDataSet
            Me.grdActivities.DataBind()
        End If
    End Sub
#End Region

#Region "sort Activities Grid "
    Protected Sub sortActivitiesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)

        Dim activitiesDataTable As DataTable = New DataTable()
        activitiesDataTable = resultDataSet.Tables(0)

        Me.setSortDirection()

        If IsNothing(activitiesDataTable) = False Then
            Dim dvSortedView As DataView = New DataView(activitiesDataTable)
            dvSortedView.Sort = Me.sortExpression + " " + Me.sortDirection
            grdActivities.DataSource = dvSortedView
            grdActivities.DataBind()

        End If
    End Sub
#End Region

#Region "get Property Letters"
    Protected Sub getPropertyLetters(ByRef journalHistoryId As Int64)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.propertyLetterList(resultDataSet, journalHistoryId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            dataListLetters.DataSource = resultDataSet
            dataListLetters.DataBind()
            uiMessageHelper.setMessage(lblNoLetters, pnlLetters, UserMessageConstants.NoLettersFound, True)
        Else
            dataListLetters.DataSource = resultDataSet
            dataListLetters.DataBind()
        End If

    End Sub

#End Region

#Region "get Property Documents"
    Protected Sub getPropertyDocuments(ByRef journalHistoryId As Int64)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.propertyDocumentList(resultDataSet, journalHistoryId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            dataListDocs.DataSource = resultDataSet
            dataListDocs.DataBind()
            uiMessageHelper.setMessage(lblNoDoc, pnlDocs, UserMessageConstants.NoDocumentsFound, True)
        Else
            dataListDocs.DataSource = resultDataSet
            dataListDocs.DataBind()
        End If

    End Sub

#End Region

#Region "reset Letter Doc Labels"
    Protected Sub resetLetterDocLabels()
        Me.lblNoLetters.Text = ""
        Me.lblNoDoc.Text = ""
    End Sub
#End Region

#Region "set Sort Direction"
    Protected Sub setSortDirection()

        'This if condition will 
        If ViewState(ViewStateConstants.SortDirection).ToString() = ViewStateConstants.Ascending Then

            ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Descending
            Me.sortDirection = "DESC"
        Else
            ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Ascending
            Me.sortDirection = "ASC"

        End If
    End Sub
#End Region

#Region "get  Sort Direction"
    Protected Function getSortDirection()
        Return ViewState(ViewStateConstants.SortDirection).ToString()
    End Function
#End Region

#Region "set Default Sort"

    Protected Sub setDefaultSort()
        ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Descending
        ViewState(ViewStateConstants.SortExpression) = ViewStateConstants.CreationDate
        Me.sortDirection = "DESC"
        Me.sortExpression = "CREATIONDATE"
    End Sub
#End Region

#Region "convert String Bit To Boolean"

    Public Function convertStringBitToBoolean(value As String) As [Boolean]
        If value <> String.Empty Then
            If value = "False" Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function
#End Region

#Region "Bind Urls with Letter Link Button"
    Protected Sub bindUrlsWithLetterLinkButton()
        Dim lnkBtn As New LinkButton()
        Dim url As String = String.Empty

        For i As Integer = 0 To dataListLetters.Items.Count - 1
            lnkBtn = DirectCast(dataListLetters.Items(i).FindControl("lnkBtnLetter"), LinkButton)
            url = PathConstants.LetterWindowPath + "?id=" + lnkBtn.CommandArgument
            lnkBtn.Attributes.Add("onClick", String.Format(ApplicationConstants.DowloadWindowScript, url))
        Next
    End Sub
#End Region

#Region "Bind Urls with Doc Link Button"
    Protected Sub bindUrlsWithDocLinkButton(ByRef sender As Object)
        Dim lnkBtn As LinkButton = TryCast(sender, LinkButton)
        Dim url As String = String.Empty
        Dim idsArray() As String

        For i As Integer = 0 To dataListDocs.Items.Count - 1
            lnkBtn = DirectCast(dataListDocs.Items(i).FindControl("lnkBtnDoc"), LinkButton)
            'First index of this array will contain the document id and third index will contain the journalhistoryid
            idsArray = CType(lnkBtn.CommandArgument, String).Split(";;")

            url = PathConstants.DownloadWindowPath + "?did=" + idsArray(0) + "&jhid=" + idsArray(2) + "&doc=true"
            lnkBtn.Attributes.Add("onClick", String.Format(ApplicationConstants.DowloadWindowScript, url))
        Next
    End Sub
#End Region

#Region "Load Satus"
    Protected Sub loadSatus()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = ApplicationConstants.StatusId
        ddlStatus.DataTextField = ApplicationConstants.Title
        ddlStatus.DataBind()
        ddlStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Actions"
    Protected Sub loadActions(ByRef statusId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAction.DataSource = resultDataSet
        ddlAction.DataValueField = ApplicationConstants.ActionId
        ddlAction.DataTextField = ApplicationConstants.Title
        ddlAction.DataBind()
        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub loadLetters(ByRef actionId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlLetter.DataSource = resultDataSet
        ddlLetter.DataValueField = ApplicationConstants.LetterId
        ddlLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlLetter.DataBind()
        ddlLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
                SessionManager.removeDocumentUploadName()
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If


    End Sub
#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Validate Add Activity"
    Protected Function validateAddActivity()
        Dim success As Boolean = True
        If Me.ddlInspectionType.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionType, True)
        ElseIf IsNothing(Me.txtDate.Text) Or Me.txtDate.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionDate, True)
        ElseIf Me.ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionStatus, True)
        ElseIf Me.ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)
        ElseIf IsNothing(Me.txtNotes.Text) Or Me.txtNotes.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionNotes, True)
        End If
        Return success
    End Function
#End Region

#Region "Reset Add Activity Controls"
    Protected Sub resetAddActivityControls()
        Me.lblAddedBy.Text = SessionManager.getUserFullName()
        Me.ddlInspectionType.SelectedValue = "-1"
        Me.txtDate.Text = Date.Now.ToString().Substring(0, 10)
        Me.ddlStatus.SelectedValue = "-1"
        Me.loadActions(-1)
        Me.loadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#Region "Save Add Activity Popup"
    Protected Function saveAddActivityPopup()
        Dim objPropActivityBo As PropertyActivityBO = New PropertyActivityBO()

        objPropActivityBo.PropertyId = Me.propertyId
        objPropActivityBo.CreatedBy = SessionManager.getAppServicingUserId()
        objPropActivityBo.InspectionTypeId = ddlInspectionType.SelectedValue
        objPropActivityBo.ActionDate = CType(txtDate.Text, Date)
        objPropActivityBo.StatusId = ddlStatus.SelectedValue
        objPropActivityBo.ActionId = ddlAction.SelectedValue
        objPropActivityBo.LetterId = ddlLetter.SelectedValue
        objPropActivityBo.Notes = txtNotes.Text
        objPropActivityBo.DocumentPath = GeneralHelper.getDocumentUploadPath()


        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objPropActivityBo.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objPropActivityBo.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objPropActivityBo.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objPropActivityBo.IsLetterAttached = False
            End If

            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objPropActivityBo.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objPropActivityBo.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objPropActivityBo.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(4), String))
                Next
            Else
                objPropActivityBo.IsDocumentAttached = False
            End If
        End If

        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        Return objPropertyBl.savePropertyActivity(objPropActivityBo, savedLetterDt)

    End Function

#End Region

#Region "Check CP12 Status and Return True/False to Enable/Disable Clip Image"

    Public Function CheckCp12Status(ByRef Cp12Status As Object, ByRef CP12DocumentID As Object)
        Dim status As Boolean = False

        If (Not IsDBNull(Cp12Status)) Then
            If ((LCase(ApplicationConstants.Cp12IssuedStatus) = LCase(CType(Cp12Status, String))) _
                AndAlso (CType(CP12DocumentID, Integer)) > 0) Then
                status = True
                'End If
            End If
        End If

        Return status
    End Function

#End Region

#Region "Check InspectionId and Return True/False to Enable/Disable Clip Image"
    Public Function CheckInspectionID(ByRef IsDocumentAttached As Object, ByRef CP12DocumentID As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(IsDocumentAttached)) Then
            If ((CType(CP12DocumentID, Integer)) > 0 And (CType(IsDocumentAttached, Integer) = 1)) Then
                status = True
            End If
        End If
        Return status
    End Function
#End Region

#Region "Check Planned InspectionId and Return True/False to Enable/Disable Clip Image"
    Public Function CheckPlannedInspectionID(ByRef IsDocumentAttached As Object, ByRef plannedStatus As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(IsDocumentAttached)) Then
            If (plannedStatus.ToString().StartsWith(ApplicationConstants.InspectionArrangedStatus) And (CType(IsDocumentAttached, Integer) = 1)) Then
                status = True
            End If
        End If
        Return status
    End Function
#End Region

#End Region

#Region "Test Funcations"
    'Public Sub updateLabel(ByVal sender As System.Object, ByVal e As MessageEventArgs)
    '    'update the label with the message in the event arguments
    '    'Me.labMessage.Text = e.message
    '    'Dim mdlPopupEditLetter As ModalPopupExtender = CType(Me.Parent.FindControl("mdlPopupEditLetter"), ModalPopupExtender)
    '    'mdlPopupEditLetter.Hide()

    '    'Dim lblMessage As Label = CType(Parent.FindControl("lblMessage"), Label)

    '    'lblMessage.Text = "Hello"

    '    'Dim updPanelPropertyRecord As UpdatePanel = CType(Me.Parent.FindControl("updPanelPropertyRecord"), UpdatePanel)
    '    'updPanelPropertyRecord.Update()

    'End Sub  'updateLabel

    Public Sub updateLabel2()
        'update the label with the message in the event arguments
        'Dim lbl As Label = CType(Me.FindControl("labMessage"), Label)
        'lbl.Text = "hello"
        'Me.labMessage.Text = "hello"
        'Me.mdlPopupEditLetter.Hide()


    End Sub  'updateLabel

    'Changes By Aamir - April 17, 2013 - Start

    '#Region "Image Button File (CP12Document) Click"

    '    Protected Sub imgBtnFile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '        Try
    '            Dim btn As ImageButton = CType(sender, ImageButton)
    '            Dim LGSRID As Integer = CType(btn.CommandArgument, Integer)

    '            'Response.Redirect(ResolveUrl("~\Views\Common\DownloadCP12Document.aspx") & "?CP12DocumentID=" & LGSRID, False)

    '            Dim scriptforCpDocument = "window.open('" & ResolveUrl("~\Views\Common\DownloadCP12Document.aspx") & "?CP12DocumentID=" & LGSRID & "')"

    '            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Download CP Document", scriptforCpDocument, True)

    '            ScriptManager.RegisterStartupScript(grdActivities, Me.GetType(), "RedirectToDownloadPage", scriptforCpDocument, True)

    '            'getCP12DocumentByLGSRID(LGSRID)
    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.Message = ex.Message

    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If
    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
    '            End If
    '        End Try
    '    End Sub

    '#End Region

    '#Region "Get CP12Document By LGSRID"

    '    Private Sub getCP12DocumentByLGSRID(ByRef LGSRID As Integer)
    '        Dim dataSet As New DataSet()
    '        objPropertyBl.getCP12DocumentByLGSRID(dataSet, LGSRID)

    '        If dataSet.Tables.Count > 0 AndAlso dataSet.Tables(0).Rows.Count > 0 Then
    '            With dataSet.Tables(0).Rows(0)
    '                Dim propertyID As String = DirectCast(.Item("PROPERTYID"), String)
    '                Dim cP12Number As String = DirectCast(.Item("CP12NUMBER"), String)
    '                Dim documentType As String = DirectCast(.Item("DOCUMENTTYPE"), String)

    '                Dim CP12DOCUMENT As [Byte]() = DirectCast(.Item("CP12DOCUMENT"), [Byte]())

    '                ' Send the file to the browser
    '                Response.AddHeader("Content-type", "application/pdf")
    '                Response.AddHeader("Content-Disposition", "attachment; filename=CP12Document_" & propertyID & "_" & cP12Number & ".pdf")
    '                Response.BinaryWrite(CP12DOCUMENT)
    '                Response.Flush()
    '                Response.End()
    '            End With

    '        End If

    '    End Sub

    '#End Region

    'Changes By Aamir - April 17, 2013 - End

#End Region

    Protected Sub grdActivities_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdActivities.PageIndexChanging
        Dim resultDataSet As DataSet = New DataSet()
        ''Dim newPageNumber As Integer = e.NewPageIndex + 1

        grdActivities.PageIndex = e.NewPageIndex
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)
        grdActivities.DataSource = resultDataSet
        grdActivities.DataBind()
    End Sub
End Class