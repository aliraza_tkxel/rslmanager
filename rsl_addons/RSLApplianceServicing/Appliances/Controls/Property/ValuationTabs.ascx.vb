﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Public Class ValuationTabs
    Inherits UserControlBase
#Region "Properties"
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "SID", 1, 7)
    Dim totalCount As Integer = 0
    Protected uniqueKey As String
#End Region
#Region "Events"

#Region "Page load event"
    ''' <summary>
    ''' Page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnSavePropertyRent.Visible = False
            ValuationPanel.Enabled = False
        End If
        If Not IsPostBack Then
            Me.populateFinancialTabDropdownInformation()
        End If

    End Sub
#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
            bindToGrid(propertyId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "imgCurrentRentHistoryArrow Click event"
    ''' <summary>
    ''' imgCurrentRentHistoryArrow Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub imgCurrentRentHistoryArrow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim resultDataSet As DataSet = New DataSet()

            Dim rowIndex As Integer
            Dim commandArgument As String = sender.CommandArgument

            resetPopUpControls()
            If commandArgument > 0 Then
                Dim rowId As Integer = 0
                Me.mdlCurrentRentHistoryPopUp.Show()
                rowIndex = commandArgument
                resultDataSet = SessionManager.getValuationHistoryDataSet()

                Dim applianceResult = (From ps In resultDataSet.Tables(0) Where ps.Item("SID") = rowIndex Select ps)
                If applianceResult.Count > 0 Then
                    lblChangedNominatingBody.Text = If(Not IsDBNull(applianceResult.First.Item("NOMINATINGBODY")), applianceResult.First.Item("NOMINATINGBODY"), String.Empty)
                    lblChangedFundingAuthority.Text = If(Not IsDBNull(applianceResult.First.Item("FundingAuthorityDesc")), applianceResult.First.Item("FundingAuthorityDesc").ToString(), String.Empty)
                    lblChangedCharge.Text = If(Not IsDBNull(applianceResult.First.Item("ChargeDesc")), applianceResult.First.Item("ChargeDesc").ToString(), String.Empty)
                    lblChangedChargeValue.Text = If(Not IsDBNull(applianceResult.First.Item("CHARGEVALUE")), "£  " + Convert.ToDouble(applianceResult.First.Item("CHARGEVALUE")).ToString("N2"), String.Empty)
                    lblChangedOmvSt.Text = If(Not IsDBNull(applianceResult.First.Item("OMVST")), "£  " + Convert.ToDouble(applianceResult.First.Item("OMVST")).ToString("N2"), String.Empty)
                    lblChangedOmvStDate.Text = If(Not IsDBNull(applianceResult.First.Item("OMVSTDate")), applianceResult.First.Item("OMVSTDate"), String.Empty)
                    lblChangedChargeValueDate.Text = If(Not IsDBNull(applianceResult.First.Item("ChargeValueDate")), applianceResult.First.Item("ChargeValueDate"), String.Empty)
                    lblChangedEUV.Text = If(Not IsDBNull(applianceResult.First.Item("EUV")), "£  " + Convert.ToDouble(applianceResult.First.Item("EUV")).ToString("N2"), String.Empty)
                    lblChangedEUVDate.Text = If(Not IsDBNull(applianceResult.First.Item("EUVDate")), applianceResult.First.Item("EUVDate"), String.Empty)
                    lblChangedInsuranceValue.Text = If(Not IsDBNull(applianceResult.First.Item("INSURANCEVALUE")), "£  " + Convert.ToDouble(applianceResult.First.Item("INSURANCEVALUE")).ToString("N2"), String.Empty)
                    lblChangedInsuranceValueDate.Text = If(Not IsDBNull(applianceResult.First.Item("InsuranceValuedate")), applianceResult.First.Item("InsuranceValuedate"), String.Empty)
                    lblChangedOmv.Text = If(Not IsDBNull(applianceResult.First.Item("OMV")), "£  " + Convert.ToDouble(applianceResult.First.Item("OMV")).ToString("N2"), String.Empty)
                    lblChangedOmvDate.Text = If(Not IsDBNull(applianceResult.First.Item("OMVDate")), applianceResult.First.Item("OMVDate"), String.Empty)
                    lblChangedYield.Text = If(Not IsDBNull(applianceResult.First.Item("YIELD")), Convert.ToDouble(applianceResult.First.Item("YIELD")).ToString("N2") + " %", String.Empty)
                    lblChangedYieldDate.Text = If(Not IsDBNull(applianceResult.First.Item("YieldDate")), applianceResult.First.Item("YieldDate"), String.Empty)
                    lblAmended.Text = If(Not IsDBNull(applianceResult.First.Item("Updated")), Convert.ToDateTime(applianceResult.First.Item("Updated")).ToString("dd/MM/yyyy"), String.Empty)
                    lblBy.Text = If(Not IsDBNull(applianceResult.First.Item("ByFullName")), applianceResult.First.Item("ByFullName").ToString(), String.Empty)
                    lblChangedMarketRent.Text = If(Not IsDBNull(applianceResult.First.Item("MarketRent")), "£  " + Convert.ToDouble(applianceResult.First.Item("MarketRent")).ToString("N2"), String.Empty)
                    lblChangedMarketRentDate.Text = If(Not IsDBNull(applianceResult.First.Item("MarketRentDate")), applianceResult.First.Item("MarketRentDate"), String.Empty)
                    lblChangedAffordableRent.Text = If(Not IsDBNull(applianceResult.First.Item("AffordableRent")), "£  " + Convert.ToDouble(applianceResult.First.Item("AffordableRent")).ToString("N2"), String.Empty)
                    lblChangedAffordableRentDate.Text = If(Not IsDBNull(applianceResult.First.Item("AffordableRentDate")), applianceResult.First.Item("AffordableRentDate"), String.Empty)
                    lblChangedSocialRent.Text = If(Not IsDBNull(applianceResult.First.Item("SocialRent")), "£  " + Convert.ToDouble(applianceResult.First.Item("SocialRent")).ToString("N2"), String.Empty)
                    lblChangedSocialRentDate.Text = If(Not IsDBNull(applianceResult.First.Item("SocialRentDate")), applianceResult.First.Item("SocialRentDate"), String.Empty)
                    rowId = applianceResult.First.Item("row")
                End If

                If rowId > 0 Then
                    rowId = rowId + 1
                    Dim previousRentResult = (From ps In resultDataSet.Tables(0) Where ps.Item("row") = rowId Select ps)
                    If previousRentResult.Count > 0 Then
                        lblPreviousNominatingBody.Text = If(Not IsDBNull(previousRentResult.First.Item("NOMINATINGBODY")), previousRentResult.First.Item("NOMINATINGBODY"), String.Empty)
                        lblPreviousFundingAuthority.Text = If(Not IsDBNull(previousRentResult.First.Item("FundingAuthorityDesc")), previousRentResult.First.Item("FundingAuthorityDesc").ToString(), String.Empty)
                        lblPreviousCharge.Text = If(Not IsDBNull(previousRentResult.First.Item("ChargeDesc")), previousRentResult.First.Item("ChargeDesc").ToString(), String.Empty)
                        lblPreviousChargeValue.Text = If(Not IsDBNull(previousRentResult.First.Item("CHARGEVALUE")), "£  " + Convert.ToDouble(previousRentResult.First.Item("CHARGEVALUE")).ToString("N2"), String.Empty)
                        lblPreviousChargeValueDate.Text = If(Not IsDBNull(previousRentResult.First.Item("ChargeValueDate")), previousRentResult.First.Item("ChargeValueDate"), String.Empty)
                        lblPreviousOmvSt.Text = If(Not IsDBNull(previousRentResult.First.Item("OMVST")), "£  " + Convert.ToDouble(previousRentResult.First.Item("OMVST")).ToString("N2"), String.Empty)
                        lblPreviousOmvStDate.Text = If(Not IsDBNull(previousRentResult.First.Item("OMVSTDate")), previousRentResult.First.Item("OMVSTDate"), String.Empty)
                        lblPreviousEUV.Text = If(Not IsDBNull(previousRentResult.First.Item("EUV")), "£  " + Convert.ToDouble(previousRentResult.First.Item("EUV")).ToString("N2"), String.Empty)
                        lblPreviousEUVDate.Text = If(Not IsDBNull(previousRentResult.First.Item("EUVDate")), previousRentResult.First.Item("EUVDate"), String.Empty)
                        lblPreviousInsuranceValue.Text = If(Not IsDBNull(previousRentResult.First.Item("INSURANCEVALUE")), "£  " + Convert.ToDouble(previousRentResult.First.Item("INSURANCEVALUE")).ToString("N2"), String.Empty)
                        lblPreviousInsuranceValueDate.Text = If(Not IsDBNull(previousRentResult.First.Item("InsuranceValuedate")), previousRentResult.First.Item("InsuranceValuedate"), String.Empty)
                        lblPreviousOmv.Text = If(Not IsDBNull(previousRentResult.First.Item("OMV")), "£  " + Convert.ToDouble(previousRentResult.First.Item("OMV")).ToString("N2"), String.Empty)
                        lblPreviousOmvDate.Text = If(Not IsDBNull(previousRentResult.First.Item("OMVDate")), previousRentResult.First.Item("OMVDate"), String.Empty)
                        lblPreviousYield.Text = If(Not IsDBNull(previousRentResult.First.Item("YIELD")), Convert.ToDouble(previousRentResult.First.Item("YIELD")).ToString("N2") + " %", String.Empty)
                        lblPreviousYieldDate.Text = If(Not IsDBNull(previousRentResult.First.Item("YieldDate")), previousRentResult.First.Item("YieldDate"), String.Empty)
                        lblPreviousMarketRent.Text = If(Not IsDBNull(previousRentResult.First.Item("MarketRent")), "£  " + Convert.ToDouble(previousRentResult.First.Item("MarketRent")).ToString("N2"), String.Empty)
                        lblPreviousMarketRentDate.Text = If(Not IsDBNull(previousRentResult.First.Item("MarketRentDate")), previousRentResult.First.Item("MarketRentDate"), String.Empty)
                        lblPreviousAffordableRent.Text = If(Not IsDBNull(previousRentResult.First.Item("AffordableRent")), "£  " + Convert.ToDouble(previousRentResult.First.Item("AffordableRent")).ToString("N2"), String.Empty)
                        lblPreviousAffordableRentDate.Text = If(Not IsDBNull(previousRentResult.First.Item("AffordableRentDate")), previousRentResult.First.Item("AffordableRentDate"), String.Empty)
                        lblPreviousSocialRent.Text = If(Not IsDBNull(previousRentResult.First.Item("SocialRent")), "£  " + Convert.ToDouble(previousRentResult.First.Item("SocialRent")).ToString("N2"), String.Empty)
                        lblPreviousSocialRentDate.Text = If(Not IsDBNull(previousRentResult.First.Item("SocialRentDate")), previousRentResult.First.Item("SocialRentDate"), String.Empty)
                    End If
                End If

                Me.showHidePopUpControls()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region



#Region "btnSavePropertyRent Click event"
    ''' <summary>
    ''' btnSavePropertyRent Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSavePropertyRent_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSavePropertyRent.Click
        Try


            Me.savePropertyFinancialInformation()
            Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
            Me.bindToGrid(propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblRentMessage, pnlRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#End Region



#Region "Get Property Financial Tab Drop down Lists (Frequency,Funding Authority and Charge) Information"
    Public Sub populateFinancialTabDropdownInformation()

        Dim propertyId As String = SessionManager.getPropertyId()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyFinancialTabDropdownInformation(resultDataSet, propertyId)

        'Populating Funding Authority Drop down List   
        ddlFundingAuthority.DataSource = resultDataSet.Tables("FundingAuthority")
        ddlFundingAuthority.DataTextField = "DESCRIPTION"
        ddlFundingAuthority.DataValueField = "FUNDINGAUTHORITYID"
        ddlFundingAuthority.DataBind()
        'Populating Charge Drop down List
        ddlCharge.DataSource = resultDataSet.Tables("Charge")
        ddlCharge.DataTextField = "DESCRIPTION"
        ddlCharge.DataValueField = "CHARGEID"
        ddlCharge.DataBind()
        Me.prepopulateFinancialInformation(resultDataSet)

    End Sub

#End Region

#Region "Save Property Financial Information"
    Sub savePropertyFinancialInformation()
        Try
            Dim objPropertyBl As PropertyBL = New PropertyBL()
            Dim objPropertyFinancialBO As New PropertyFinancialBO()
            ' objPropertyFinancialBO.RentFequency = ddlRentFrequency.SelectedItem.Value
            objPropertyFinancialBO.NominatingBody = txtNominatingBody.Text
            objPropertyFinancialBO.FundingAuthority = ddlFundingAuthority.SelectedItem.Value
            objPropertyFinancialBO.Charge = ddlCharge.SelectedItem.Value
            objPropertyFinancialBO.ChargeValue = txtChargeValue.Text

            Dim num As Integer = Integer.Parse(txtOMVST.Text.Replace(",", ""))
            objPropertyFinancialBO.OMVSt = num.ToString()
            num = Integer.Parse(txtEUV.Text.Replace(",", ""))
            objPropertyFinancialBO.EUV = txtEUV.Text
            num = Integer.Parse(txtInsurancValue.Text.Replace(",", ""))
            objPropertyFinancialBO.InsuranceValue = txtInsurancValue.Text
            num = Integer.Parse(txtOMV.Text.Replace(",", ""))
            objPropertyFinancialBO.OMV = txtOMV.Text
            objPropertyFinancialBO.Yield = txtYield.Text
            objPropertyFinancialBO.PropertyId = SessionManager.getPropertyId()
            objPropertyFinancialBO.AffordableRent = txtAffordableRent.Text
            objPropertyFinancialBO.MarketRent = txtMarketRent.Text
            objPropertyFinancialBO.SocialRent = txtSocialRent.Text

            objPropertyFinancialBO.UserId = SessionManager.getAppServicingUserId() ' When go live
            'objPropertyFinancialBO.UserId = 605 'For testing
            If txtChargeValueDate.Text <> String.Empty Then
                objPropertyFinancialBO.ChargeValueDate = Convert.ToDateTime(txtChargeValueDate.Text)
            End If
            If txtOMVSTDate.Text <> String.Empty Then
                objPropertyFinancialBO.OMVSTDate = Convert.ToDateTime(txtOMVSTDate.Text)
            End If
            If txtEUVDate.Text <> String.Empty Then
                objPropertyFinancialBO.EUVDate = Convert.ToDateTime(txtEUVDate.Text)
            End If
            If txtInsurancValueDate.Text <> String.Empty Then
                objPropertyFinancialBO.InsuranceValueDate = Convert.ToDateTime(txtInsurancValueDate.Text)
            End If
            If txtOMVDate.Text <> String.Empty Then
                objPropertyFinancialBO.OMVDate = Convert.ToDateTime(txtOMVDate.Text)
            End If
            If txtYieldDate.Text <> String.Empty Then
                objPropertyFinancialBO.YieldDate = Convert.ToDateTime(txtYieldDate.Text)
            End If
            If txtAffordableRentDate.Text <> String.Empty Then
                objPropertyFinancialBO.AffordableRentDate = Convert.ToDateTime(txtAffordableRentDate.Text)
            End If
            If txtMarketRentDate.Text <> String.Empty Then
                objPropertyFinancialBO.MarketRentDate = Convert.ToDateTime(txtMarketRentDate.Text)
            End If
            If txtSocialRentDate.Text <> String.Empty Then
                objPropertyFinancialBO.SocialRentDate = Convert.ToDateTime(txtSocialRentDate.Text)
            End If

            objPropertyBl.savePropertyFinancialInformation(objPropertyFinancialBO)
            uiMessageHelper.setMessage(lblRentMessage, pnlRentMessage, UserMessageConstants.SuccessMessage, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblRentMessage, pnlRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Prepopulate Property Financial Information"
    Private Sub prepopulateFinancialInformation(ByVal resultDataSet As DataSet)
        If (resultDataSet.Tables("RentFinancial").Rows.Count > 0) Then

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(1))) Then
                ddlFundingAuthority.SelectedIndex = resultDataSet.Tables("RentFinancial").Rows(0)(1)
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(2))) Then
                txtNominatingBody.Text = resultDataSet.Tables("RentFinancial").Rows(0)(2).ToString()
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(3))) Then
                Dim intInsurancValue As Integer = resultDataSet.Tables("RentFinancial").Rows(0)(3)
                txtInsurancValue.Text = intInsurancValue.ToString("N0")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(4))) Then
                txtChargeValue.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)(4))
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(5))) Then
                Dim intOMVST As Integer = resultDataSet.Tables("RentFinancial").Rows(0)(5)
                txtOMVST.Text = intOMVST.ToString("N0")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(6))) Then
                Dim intEUV As Integer = resultDataSet.Tables("RentFinancial").Rows(0)(6)
                txtEUV.Text = intEUV.ToString("N0")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(7))) Then
                Dim intOMV As Integer = resultDataSet.Tables("RentFinancial").Rows(0)(7)
                txtOMV.Text = intOMV.ToString("N0")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(8))) Then
                ddlCharge.SelectedValue = resultDataSet.Tables("RentFinancial").Rows(0)(8)
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(7))) Then
                txtYield.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)(9))
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(16))) Then
                txtMarketRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)(16))
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(17))) Then
                txtAffordableRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)(17))
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)(18))) Then
                txtSocialRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)(18))
            End If
            ''dates
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("ChargeValueDate"))) Then
                txtChargeValueDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("ChargeValueDate")
            End If

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("OMVSTDate"))) Then
                txtOMVSTDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("OMVSTDate")
            End If

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("EUVDate"))) Then
                txtEUVDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("EUVDate")
            End If

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("OMVDate"))) Then
                txtOMVDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("OMVDate")
            End If

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("InsuranceValuedate"))) Then
                txtInsurancValueDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("InsuranceValuedate")
            End If

            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("YieldDate"))) Then
                txtYieldDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("YieldDate")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("MarketRentDate"))) Then
                txtMarketRentDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("MarketRentDate")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("AffordableRentDate"))) Then
                txtAffordableRentDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("AffordableRentDate")
            End If
            If (Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("SocialRentDate"))) Then
                txtSocialRentDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("SocialRentDate")
            End If

        End If
    End Sub
#End Region

#Region "Reset Controls"

    Protected Sub resetControls()
        txtOMVST.Text = String.Empty
        txtOMV.Text = String.Empty
        txtChargeValue.Text = String.Empty
        txtInsurancValue.Text = String.Empty
        txtNominatingBody.Text = String.Empty
        txtEUV.Text = String.Empty
        ' ddlRentFrequency.SelectedIndex = 1
        ddlFundingAuthority.SelectedIndex = 1
        ddlCharge.SelectedIndex = 0
        txtChargeValueDate.Text = String.Empty
        txtEUVDate.Text = String.Empty
        txtInsurancValueDate.Text = String.Empty
        txtOMVDate.Text = String.Empty
        txtOMVSTDate.Text = String.Empty
        txtYieldDate.Text = String.Empty
    End Sub

#End Region

    Protected Sub txtOMVST_TextChanged(sender As Object, e As EventArgs)

        ' Get TextBox reference from sender object.
        Dim num As Integer = Integer.Parse(txtOMVST.Text.Replace(",", ""))
        txtOMVST.Text = num.ToString("N0")
    End Sub

    Protected Sub txtEUV_TextChanged(sender As Object, e As EventArgs)

        ' Get TextBox reference from sender object.
        Dim num As Integer = Integer.Parse(txtEUV.Text.Replace(",", ""))
        txtEUV.Text = num.ToString("N0")
    End Sub

    Protected Sub txtInsurancValue_TextChanged(sender As Object, e As EventArgs)

        ' Get TextBox reference from sender object.
        Dim num As Integer = Integer.Parse(txtInsurancValue.Text.Replace(",", ""))
        txtInsurancValue.Text = num.ToString("N0")
    End Sub

    Protected Sub txtOMV_TextChanged(sender As Object, e As EventArgs)

        ' Get TextBox reference from sender object.
        Dim num As Integer = Integer.Parse(txtOMV.Text.Replace(",", ""))
        txtOMV.Text = num.ToString("N0")
    End Sub


#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)

                Dim propertyId As String = CType(Request.QueryString(PathConstants.PropertyIds), String)
                Me.bindToGrid(propertyId)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Bind To Grid"

    Public Sub bindToGrid(ByVal propertyId As String)
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPageSortBo = getPageSortBoViewState()


        totalCount = objPropertyBL.getValuationHistory(resultDataSet, propertyId, objPageSortBo)
        'If (totalCount > 0) Then
        SessionManager.setValuationHistoryDataSet(resultDataSet)
        Dim resultDt As DataTable = New DataTable()
        resultDt = resultDataSet.Tables(0).Copy
        If resultDataSet.Tables(0).Rows.Count = 8 Then

            resultDt.Rows(7).Delete()
        End If
        objPageSortBo.PageSize = 7
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        grdValuationHistory.VirtualItemCount = totalCount
        grdValuationHistory.DataSource = resultDt
        grdValuationHistory.DataBind()
        ViewState.Item(ViewStateConstants.TotalCount) = totalCount
        'setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        
    End Sub


#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub
#End Region

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub



#End Region

#Region "show Hide PopUp Controls"
    Private Sub showHidePopUpControls()
        If lblPreviousNominatingBody.Text = lblChangedNominatingBody.Text Then
            lblChangedNominatingBody.Visible = False
        End If
        If lblPreviousFundingAuthority.Text = lblChangedFundingAuthority.Text Then
            lblChangedFundingAuthority.Visible = False
        End If
        If lblPreviousCharge.Text = lblChangedCharge.Text Then
            lblChangedCharge.Visible = False
        End If
        If lblPreviousChargeValue.Text = lblChangedChargeValue.Text Then
            lblChangedChargeValue.Visible = False
        End If
        If lblPreviousOmvSt.Text = lblChangedOmvSt.Text Then
            lblChangedOmvSt.Visible = False
        End If
        If lblPreviousEUV.Text = lblChangedEUV.Text Then
            lblChangedEUV.Visible = False
        End If
        If lblPreviousInsuranceValue.Text = lblChangedInsuranceValue.Text Then
            lblChangedInsuranceValue.Visible = False
        End If
        If lblPreviousOmv.Text = lblChangedOmv.Text Then
            lblChangedOmv.Visible = False
        End If
        If lblPreviousYield.Text = lblChangedYield.Text Then
            lblChangedYield.Visible = False
        End If

        If lblPreviousChargeValueDate.Text = lblChangedChargeValueDate.Text Then
            lblChangedChargeValueDate.Visible = False
        End If
        If lblPreviousOmvStDate.Text = lblChangedOmvStDate.Text Then
            lblChangedOmvStDate.Visible = False
        End If
        If lblPreviousEUVDate.Text = lblChangedEUVDate.Text Then
            lblChangedEUVDate.Visible = False
        End If
        If lblPreviousInsuranceValueDate.Text = lblChangedInsuranceValueDate.Text Then
            lblChangedInsuranceValueDate.Visible = False
        End If
        If lblPreviousOmvDate.Text = lblChangedOmvDate.Text Then
            lblChangedOmvDate.Visible = False
        End If
        If lblPreviousYieldDate.Text = lblChangedYieldDate.Text Then
            lblChangedYieldDate.Visible = False
        End If
        If lblPreviousMarketRentDate.Text = lblChangedMarketRentDate.Text Then
            lblChangedMarketRentDate.Visible = False
        End If
        If lblPreviousAffordableRentDate.Text = lblChangedAffordableRentDate.Text Then
            lblChangedAffordableRentDate.Visible = False
        End If
        If lblPreviousSocialRentDate.Text = lblChangedSocialRentDate.Text Then
            lblChangedSocialRentDate.Visible = False
        End If

        If Not String.Compare(lblPreviousAffordableRent.Text, lblChangedAffordableRent.Text) Then
            lblChangedAffordableRent.Visible = False
        End If
        If Not String.Compare(lblPreviousMarketRent.Text, lblChangedMarketRent.Text) Then
            lblChangedMarketRent.Visible = False
        End If
        If Not String.Compare(lblPreviousSocialRent.Text, lblChangedSocialRent.Text) Then
            lblChangedSocialRent.Visible = False
        End If








    End Sub
#End Region

#Region "Reset Controls"
    Sub resetPopUpControls()
        lblPreviousNominatingBody.Text = ""
        lblPreviousFundingAuthority.Text = ""
        lblPreviousCharge.Text = ""
        lblPreviousChargeValue.Text = ""
        lblPreviousOmvSt.Text = ""
        lblPreviousEUV.Text = ""
        lblPreviousInsuranceValue.Text = ""
        lblPreviousOmv.Text = ""
        lblPreviousYield.Text = ""

        lblPreviousChargeValueDate.Text = ""
        lblPreviousOmvStDate.Text = ""
        lblPreviousEUVDate.Text = ""
        lblPreviousInsuranceValueDate.Text = ""
        lblPreviousOmvDate.Text = ""
        lblPreviousYieldDate.Text = ""
        lblPreviousAffordableRent.Text = ""
        lblPreviousMarketRent.Text = ""
        lblPreviousSocialRent.Text = ""
        lblPreviousMarketRentDate.Text = ""
        lblPreviousAffordableRentDate.Text = ""
        lblPreviousSocialRentDate.Text = ""

        lblChangedNominatingBody.Visible = True
        lblChangedFundingAuthority.Visible = True
        lblChangedCharge.Visible = True
        lblChangedChargeValue.Visible = True
        lblChangedOmvSt.Visible = True
        lblChangedEUV.Visible = True
        lblChangedInsuranceValue.Visible = True
        lblChangedOmv.Visible = True
        lblChangedYield.Visible = True

        lblChangedChargeValueDate.Visible = True
        lblChangedOmvStDate.Visible = True
        lblChangedEUVDate.Visible = True
        lblChangedInsuranceValueDate.Visible = True
        lblChangedOmvDate.Visible = True
        lblChangedYieldDate.Visible = True

        lblChangedAffordableRent.Visible = True
        lblChangedAffordableRentDate.Visible = True
        lblChangedMarketRent.Visible = True
        lblChangedMarketRentDate.Visible = True
        lblChangedSocialRent.Visible = True
        lblChangedSocialRentDate.Visible = True
    End Sub
#End Region

End Class