﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class CurrentRentTab
    Inherits UserControlBase

#Region "Properties"
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "SID", 1, 7)
    Dim totalCount As Integer = 0
    Protected uniqueKey As String
#End Region
#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnSaveCurrentRent.Visible = False
            pnlCurrentRentTab.Enabled = False
        End If
        uiMessageHelper.resetMessage(lblCurrentRentTab, pnlCurrentRentMessage)


        If Not IsPostBack Then

        End If
    End Sub
#End Region
#Region "save Current Rent"
    Protected Sub btnSaveCurrentRent_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveCurrentRent.Click
        Me.saveCurrentRentInformation()
    End Sub
#End Region


#End Region
#Region "Functions"

#Region "Save Current Rent Information"
    Sub saveCurrentRentInformation()
        Try
            Dim objPropertyBl As PropertyBL = New PropertyBL()
            Dim objPropertyCurrentRentBO As New PropertyRentBO()
            objPropertyCurrentRentBO.RentDateSet = txtRentDate.Text
            objPropertyCurrentRentBO.RentEffectiveSet = txtEffectiveDate.Text
            objPropertyCurrentRentBO.RentType = ddlRentType.SelectedItem.Value
            objPropertyCurrentRentBO.RentFrequency = ddlRentFrequency.SelectedItem.Value
            objPropertyCurrentRentBO.Rent = txtRent.Text
            objPropertyCurrentRentBO.Services = txtServices.Text
            objPropertyCurrentRentBO.CouncilTax = txtCouncilTax.Text
            objPropertyCurrentRentBO.Inelig = txtIntelig.Text
            objPropertyCurrentRentBO.Water = txtWater.Text
            objPropertyCurrentRentBO.Supp = txtSupp.Text
            objPropertyCurrentRentBO.Garage = txtGarage.Text
            objPropertyCurrentRentBO.TotalRent = txtTotalRent.Text
            objPropertyCurrentRentBO.PropertyId = SessionManager.getPropertyId()
            objPropertyCurrentRentBO.UserId = SessionManager.getAppServicingUserId() ' When go live
            'objPropertyCurrentRentBO.UserId = 605 'For testing
            objPropertyBl.savePropertyCurrentRentInformation(objPropertyCurrentRentBO)
            uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, UserMessageConstants.SuccessMessage, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblCurrentRentTab, pnlCurrentRentMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Get Property Rent Type and Financial Information"
    Sub populateRentTypeAndFinancialInformation(ByVal propertyId As String)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getRentTypeAndFinancialInfomration(resultDataSet, propertyId)
        'Populating Rent Type Drop down List
        ddlRentType.DataSource = resultDataSet.Tables("RentType")
        ddlRentType.DataTextField = "DESCRIPTION"
        ddlRentType.DataValueField = "TENANCYTYPEID"
        ddlRentType.DataBind()
        ddlRentFrequency.DataSource = resultDataSet.Tables("RentFrequency")
        ddlRentFrequency.DataTextField = "DESCRIPTION"
        ddlRentFrequency.DataValueField = "FID"
        ddlRentFrequency.DataBind()
        'Set Rent Dates
        txtRentDate.Text = ConfigurationManager.AppSettings("RentStartDate").ToString() + Now.Year().ToString()
        txtEffectiveDate.Text = ConfigurationManager.AppSettings("RentEndDate").ToString() + Now.Year().ToString()
        'Populating Current Rent Infomraiton
        If Not IsNothing(propertyId) And resultDataSet.Tables("RentFinancial").Rows.Count > 0 Then
            If (IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("DATERENTSET"))) Then
                txtRentDate.Text = ""
            Else
                txtRentDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("DATERENTSET")
            End If
            If (IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("RENTEFFECTIVE"))) Then
                txtEffectiveDate.Text = ""
            Else
                txtEffectiveDate.Text = resultDataSet.Tables("RentFinancial").Rows(0)("RENTEFFECTIVE")
            End If

            txtRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("RENT"))
            txtServices.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SERVICES"))
            txtCouncilTax.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("COUNCILTAX"))
            txtWater.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("WATERRATES"))
            txtIntelig.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("INELIGSERV"))
            txtSupp.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SUPPORTEDSERVICES"))
            txtGarage.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("GARAGE"))
            txtTotalRent.Text = String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("TOTALRENT"))
            ddlRentType.SelectedValue = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("RENTTYPE")), resultDataSet.Tables("RentFinancial").Rows(0)("RENTTYPE"), Nothing)
            ddlRentFrequency.SelectedValue = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("RENTFREQUENCY")), resultDataSet.Tables("RentFinancial").Rows(0)("RENTFREQUENCY"), Nothing)
        End If
    End Sub

#End Region






#End Region


End Class