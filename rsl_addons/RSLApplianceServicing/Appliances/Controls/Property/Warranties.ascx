﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Warranties.ascx.vb"
    Inherits="Appliances.Warranties" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        padding: 15px;
        width: 80%;
    }
</style>
<asp:UpdatePanel runat="server" ID="updPnlWarrantyListing">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" Font-Size="12px" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <cc1:PagingGridView runat="server" ID="grdWarranties" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
            CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True"
            AllowPaging="false" PageSize="30" OrderBy="" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
            <Columns>
                <asp:TemplateField HeaderText="Warranty Type:" SortExpression="WARRANTYTYPE">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdnWarrantyId" Value='<%# Eval("WARRANTYID") %>' />
                        <asp:Label ID="lblWARRANTYTYPE" runat="server" Text='<%# Bind("WARRANTYTYPE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category:" SortExpression="LOCATIONNAME">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("LOCATIONNAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Area:" SortExpression="AREANAME">
                    <ItemTemplate>
                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AREANAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item:" SortExpression="ITEMNAME">
                    <ItemTemplate>
                        <asp:Label ID="lblItem" runat="server" Text='<%# Bind("ITEMNAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Org/Contractor" SortExpression="CONTRACTOR">
                    <ItemTemplate>
                        <asp:Label ID="lblCONTRACTOR" runat="server" Text='<%# Bind("CONTRACTOR") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expiry:" SortExpression="Expiry">
                    <ItemTemplate>
                        <asp:Label ID="lblExpiry" runat="server" Text='<%# EVAL("EXPIRYDATE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="Amend"></asp:LinkButton>
                    </EditItemTemplate>
                    <ItemStyle Width="3%" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="btnAmend" runat="server" Text="Amend" BorderStyle="Solid" BorderWidth="1px"
                            CommandArgument='<%# Eval("WARRANTYID")%>' OnClick="btnAmend_Click" Style="vertical-align: middle;
                            text-align: left"></asp:Button>
                        <asp:ImageButton ID="imgBtnDelete" runat="server" BorderStyle="None" BorderWidth="0px"
                            ImageUrl="~/Images/cross2.png" CommandArgument='<%# Eval("WARRANTYID")%>' OnClick="imgBtnDelete_Click"
                            Style="vertical-align: middle; text-align: left" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        </cc1:PagingGridView>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<asp:Button runat="server" ID="btnAddNewWarranty" Text="Add New Warranty" Style="float: right;
    margin-right: 30px;" OnClick="btnAddNewWarranty_Click" />
<asp:Panel ID="pnlAddWarrantyPopup" runat="server" CssClass="modalPopup" Style="width: 350px;
    border-color: #CCCCCC; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelAddWarrantyPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <asp:UpdatePanel runat="server" ID="updPnlPopUp" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessagePopup" runat="server" Visible="false">
                <asp:Label ID="lblMessagePopup" Font-Size="12px" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <table style="margin-top:10px;" >
                <tr>
                    <td>
                        Warranty Type:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlWarrantyType" Style="width: 160px;">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlWarrantyType"
                            InitialValue="-1" ErrorMessage="<br/>Please select warranty type." Display="Dynamic"
                            ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Category:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCategory" runat="server" Style="width: 160px;" OnSelectedIndexChanged="categoryIndex_changed"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlCategory"
                            InitialValue="-1" ErrorMessage="<br/>Please select Category." Display="Dynamic"
                            ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Area:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlArea" runat="server" Style="width: 160px;" OnSelectedIndexChanged="areaIndex_changed"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="ddlArea"
                        InitialValue="-1" ErrorMessage="<br/>Please select Area." Display="Dynamic"
                        ValidationGroup="save" ForeColor="Red" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Item:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlItem" Style="width: 160px;">
                        </asp:DropDownList>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="ddlItem"
                        InitialValue="-1" ErrorMessage="<br/>Please select Item." Display="Dynamic"
                        ValidationGroup="save" ForeColor="Red" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Org/Contractor:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlContractor" Style="width: 160px;">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlContractor"
                            InitialValue="-1" ErrorMessage="<br/>Please select contractor." Display="Dynamic"
                            ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Expiry:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtExpiry" Style="width: 116px;"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="clndrExpiry" runat="server" TargetControlID="txtExpiry"
                            PopupButtonID="imgCalDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgCalDate"
                            Style="vertical-align: bottom; border: none;" />
                        <asp:CompareValidator ID="CompareValidator13" runat="server" ControlToValidate="txtExpiry"
                            ErrorMessage="<b/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                            ForeColor="Red" ValidationGroup="save" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtExpiry"
                            ErrorMessage="Required" ValidationGroup="save" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Notes:
                    </td>
                    <td>
                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txtNotes"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="btnRest" Text="RESET" OnClick="btnReset_Click" Style="float: right;" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnAdd" Text="ADD" OnClick="btnAdd_Click" ValidationGroup="save" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
<ajaxToolkit:ModalPopupExtender ID="mdlAddWarrantyPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlAddWarrantyPopup"
    DropShadow="true" CancelControlID="imgBtnCancelAddWarrantyPopup" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
