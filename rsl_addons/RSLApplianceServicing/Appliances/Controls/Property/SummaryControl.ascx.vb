﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports AjaxControlToolkit
Imports System.Globalization



Public Class SummaryControl
    Inherits UserControlBase

#Region "Properties"

    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Dim propertyId As String
    Protected imageUrl As String = GeneralHelper.getImageUploadPath()
    Protected WithEvents btnBackToSummary As Button
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Component", 1, 30)

#End Region

#Region "Events Handling"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnBackToSummary = ucConditionRatingList.BackToSummarybutton
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ClientScriptName", "uploadComplete", True)
        Dim objContainerPage As PropertyRecord = New PropertyRecord()
        If Not IsPostBack Then
            Me.loadInspectionTypes(Me.ddlFaultType)
            If Not IsNothing(Request.QueryString(PathConstants.PropertyIds)) Then
                Me.propertyId = Request.QueryString(PathConstants.PropertyIds)
                'Me.propertyId = "BHA0000118"
            Else
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If
        End If
    End Sub

#End Region

#Region "View Fault Link Button"

    Protected Sub ViewFault(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lnkBtnJobSheet As LinkButton = DirectCast(sender, LinkButton)

            Dim row As GridViewRow = DirectCast(lnkBtnJobSheet.NamingContainer, GridViewRow)
            Dim lblInspectionType As Label = DirectCast(row.FindControl("lblInspectionTypeDescription"), Label)
            Dim lblJournalI As Label = DirectCast(row.FindControl("lblJournalI"), Label)
            Me.displayJobSheetInfo(CType(lblJournalI.Text, String), CType(lnkBtnJobSheet.Text, String), lblInspectionType.Text)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Fault type dropdown index changed"
    ''' <summary>
    ''' Event fires when dropdown value is changed from fault type.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlFaultType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFaultType.SelectedIndexChanged

        Try
            Me.propertyId = Request.QueryString(PathConstants.PropertyIds)

            populateReportedFaultsDefects(Me.propertyId)
            updPanelReportedFaults.Update()


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If

        End Try

    End Sub
#End Region

#Region "Condition Button"
    ''' <summary>
    ''' Condition Button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCondition_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles btnCondition.Click

        Try
            If Not IsNothing(Request.QueryString(PathConstants.PropertyIds)) Then
                pnlPlanned.Visible = False
                pnlConditionRatingList.Visible = True
                ucConditionRatingList.populateConditionRatingList()
                updSummaryControl.Update()
            Else
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Back To Summary Button"
    ''' <summary>
    ''' Back To Summary Button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBackToSummary_Clicked(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToSummary.Click
        Try
            pnlPlanned.Visible = True
            pnlConditionRatingList.Visible = False
            updSummaryControl.Update()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Link Button View All Click"

    Private Sub lnkBtnViewAll_Click(sender As Object, e As System.EventArgs) Handles lnkBtnViewAll.Click
        Dim faultsDataSet As List(Of ActivitiesBO) = SessionManager.getActivitiesBOList()

        If faultsDataSet.Count > 0 AndAlso faultsDataSet.Count > 5 Then
            grdReportedFaults.PageSize = faultsDataSet.Count
            lnkBtnViewAll.Visible = False
            grdReportedFaults.DataSource = faultsDataSet
            grdReportedFaults.DataBind()
        End If
    End Sub

#End Region

#Region "ckBoxPhotoUpload Checked Changed"
    ''' <summary>
    ''' ckBoxPhotoUpload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxPhotoUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxPhotoUpload.CheckedChanged
        Me.ckBoxPhotoUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getPhotoUploadName())) Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                lblFileName.Text = SessionManager.getPhotoUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpViewImage.Show()
            mdlPopUpAddPhoto.Show()
            SessionManager.removePhotoUploadName()
        End Try
    End Sub
#End Region

#Region "ckBoxEPCPhotoUpload Checked Changed"
    ''' <summary>
    ''' ckBoxEPCPhotoUpload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxEPCPhotoUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxEPCPhotoUpload.CheckedChanged
        Me.ckBoxEPCPhotoUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getEPCPhotoUploadName())) Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                lblEPCFileName.Text = SessionManager.getEPCPhotoUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpViewEPCImage.Show()
            mdlPopUpAddEPCPhoto.Show()
            SessionManager.removeEPCPhotoUploadName()
        End Try
    End Sub
#End Region

    Protected Sub btnCamera_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnCamera.Click
        Try
            Dim porpertyImagesCount As Integer
            Dim resultDataSet As DataSet = New DataSet()
            Dim propertyId As String = SessionManager.getPropertyId()
            If (Not String.IsNullOrEmpty(propertyId)) Then
                porpertyImagesCount = objPropertyBL.getPropertyImages(resultDataSet, propertyId)
                If resultDataSet.Tables(0).Rows.Count > 0 Then
                    Dim rowNum As Integer = resultDataSet.Tables(0).Rows.Count - 1
                    imgDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                    lblImageDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                    lblTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                    lblUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()
                Else
                    imgDefaultImage.ImageUrl = "../../Images/awaiting_image.png"
                    lblImageDate.Text = String.Empty
                    lblTitle.Text = String.Empty
                    lblUploadedBy.Text = String.Empty
                End If

                Me.mdlPopUpViewImage.Show()
            Else
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If
            'populatePropertyImages(propertyId)
            'imgDefaultImage.ImageUrl = "../../Photographs/Images/1_17_2013_5_31_42_.jpg"
            'lblImageDate.Text = "05/03/2014"
            'lblTitle.Text = "Front View"
            'lblUploadedBy.Text = "Peter Thompson"
            'pnlViewImage.Visible = True


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnEPCCamera_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnEPCCamera.Click
        Try
            Dim epcImagesCount As Integer
            Dim resultDataSet As DataSet = New DataSet()
            Dim propertyId As String = SessionManager.getPropertyId()

            If (Not String.IsNullOrEmpty(propertyId)) Then
                epcImagesCount = objPropertyBL.getPropertyEPCImages(resultDataSet, propertyId)

                If resultDataSet.Tables(0).Rows.Count > 0 Then
                    Dim rowNum As Integer = resultDataSet.Tables(0).Rows.Count - 1
                    imgEPCDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                    lblMainEPCDueDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                    lblMainEPCTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                    lblMainEPCUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()
                Else
                    imgEPCDefaultImage.ImageUrl = "../../Images/awaiting_image.png"
                    lblMainEPCDueDate.Text = String.Empty
                    lblMainEPCTitle.Text = String.Empty
                    lblMainEPCUploadedBy.Text = String.Empty
                End If

                Me.mdlPopUpViewEPCImage.Show()
            Else
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnPhotograph_Click(sender As Object, e As EventArgs) Handles btnPhotograph.Click
        Try
            lblAddPhotographMessage.Text = String.Empty
            txtBoxDueDate.Text = String.Empty
            txtBoxTitle.Text = String.Empty
            lblFileName.Text = String.Empty
            Me.mdlPopUpViewImage.Show()
            Me.mdlPopUpAddPhoto.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub btnEPCPhotograph_Click(sender As Object, e As EventArgs) Handles btnEPCPhotograph.Click
        Try
            lblAddEPCPhotographMessage.Text = String.Empty
            txtBoxEPCDueDate.Text = String.Empty
            txtBoxEPCTitle.Text = String.Empty
            lblEPCFileName.Text = String.Empty
            Me.mdlPopUpViewEPCImage.Show()
            Me.mdlPopUpAddEPCPhoto.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnUploadPhotos_Click(sender As Object, e As EventArgs) Handles btnUploadPhotos.Click
        Try
            Me.mdlPopUpViewImage.Show()
            Me.mdlPopUpAddPhoto.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub btnUploadEPCPhotos_Click(sender As Object, e As EventArgs) Handles btnUploadEPCPhotos.Click
        Try
            Me.mdlPopUpViewEPCImage.Show()
            Me.mdlPopUpAddEPCPhoto.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            'If txtBoxDueDate.Text = "" Then
            Dim objPhotograph As PhotographBO = New PhotographBO()
            Dim porpertyImagesCount As Integer
            Dim resultDataSet As DataSet = New DataSet()
            Me.mdlPopUpViewImage.Show()
            If lblFileName.Text <> String.Empty And txtBoxTitle.Text <> String.Empty AndAlso txtBoxDueDate.Text <> String.Empty Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtBoxDueDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid Then
                    objPhotograph.Title = txtBoxTitle.Text
                    objPhotograph.UploadDate = CType(txtBoxDueDate.Text, DateTime)
                    objPhotograph.ImageName = lblFileName.Text
                    objPhotograph.PropertyId = SessionManager.getPropertyId()
                    objPhotograph.FilePath = Server.MapPath(GeneralHelper.getImageUploadPath())
                    objPhotograph.ItemId = 0
                    objPhotograph.CreatedBy = SessionManager.getAppServicingUserId()
                    If chkBoxDefaultImage.Checked = True Then
                        objPhotograph.IsDefaultImage = True
                    End If
                    objPropertyBL.savePhotograph(objPhotograph)
                    porpertyImagesCount = objPropertyBL.getPropertyImages(resultDataSet, propertyId)
                    Dim rowNum As Integer = resultDataSet.Tables(0).Rows.Count - 1
                    imgDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                    imgEmpty.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()

                    lblImageDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                    lblTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                    lblUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()

                    'updPnlImage.Update()
                    Me.mdlPopUpViewImage.Show()
                    Me.mdlPopUpAddPhoto.Hide()
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.notValidDateFormat
                    mdlPopUpAddPhoto.Show()
                End If
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.FillMandatory
                mdlPopUpAddPhoto.Show()
            End If
            '    lblAddPhotographMessage.Text = UserMessageConstants.SelectDefectDate
            '    uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, UserMessageConstants.SelectDefectDate, True)
            '    pnlAddPhotographMessage.Visible = True
            '    Me.mdlPopUpViewImage.Show()
            '    Me.mdlPopUpAddPhoto.Show()
            '    Exit Sub
            'End If
            'If txtBoxTitle.Text = "" Then
            '    lblAddPhotographMessage.Text = UserMessageConstants.PhotographTitle
            '    uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, UserMessageConstants.PhotographTitle, True)
            '    pnlAddPhotographMessage.Visible = True
            '    Me.mdlPopUpViewImage.Show()
            '    Me.mdlPopUpAddPhoto.Show()
            '    Exit Sub
            'End If
            'Save Data for photo

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnSaveEPCImage_Click(sender As Object, e As EventArgs) Handles btnSaveEPCImage.Click
        Try
            'If txtBoxDueDate.Text = "" Then
            Dim objPhotograph As PhotographBO = New PhotographBO()
            Dim porpertyImagesCount As Integer
            Dim resultDataSet As DataSet = New DataSet()

            Me.mdlPopUpViewEPCImage.Show()

            If lblEPCFileName.Text <> String.Empty And txtBoxEPCTitle.Text <> String.Empty AndAlso txtBoxEPCDueDate.Text <> String.Empty Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtBoxEPCDueDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)

                If valid Then
                    objPhotograph.Title = txtBoxEPCTitle.Text
                    objPhotograph.UploadDate = CType(txtBoxEPCDueDate.Text, DateTime)
                    objPhotograph.ImageName = lblEPCFileName.Text
                    objPhotograph.PropertyId = SessionManager.getPropertyId()
                    objPhotograph.FilePath = Server.MapPath(GeneralHelper.getImageUploadPath())
                    objPhotograph.ItemId = 0
                    objPhotograph.CreatedBy = SessionManager.getAppServicingUserId()
                    If chkBoxDefaultImage.Checked = True Then
                        objPhotograph.IsDefaultImage = True
                    End If
                    objPropertyBL.saveEPCPhotograph(objPhotograph)
                    porpertyImagesCount = objPropertyBL.getPropertyEPCImages(resultDataSet, propertyId)
                    Dim rowNum As Integer = resultDataSet.Tables(0).Rows.Count - 1
                    imgEPCDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                    btnImgEmptyEPC.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                    lblMainEPCDueDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                    lblMainEPCTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                    lblMainEPCUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()

                    'updPnlImage.Update()
                    Me.mdlPopUpViewEPCImage.Show()
                    Me.mdlPopUpAddEPCPhoto.Hide()
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.notValidDateFormat
                    mdlPopUpAddEPCPhoto.Show()
                End If
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.FillMandatory
                mdlPopUpAddEPCPhoto.Show()
            End If
            'Save Data for photo

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotographMessage, pnlAddPhotographMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub grdPlannedMaintenance_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPlannedMaintenance.Sorting
        Dim ds As New DataSet

        objPageSortBo = getPageSortBoViewState()
        objPageSortBo.SortExpression = e.SortExpression
        objPageSortBo.setSortDirection()
        setPageSortBoViewState(objPageSortBo)

        ds = SessionManager.getplannedMaintenanceDataSet()
        ds.Tables(0).DefaultView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
        grdPlannedMaintenance.DataSource = ds.Tables(0).DefaultView
        grdPlannedMaintenance.DataBind()
    End Sub

#End Region

#Region "Functions"

#Region "Populate full summary"

    Public Sub populateFullSummary()
        Try

            Dim propertyId As String = SessionManager.getPropertyId()
            If (Not String.IsNullOrEmpty(propertyId)) Then
                imageUrl += propertyId + "/Images/"
                populatePropertyImages(propertyId)
                populateEPCImages(propertyId)
                populateTenancyPropertyDetail(propertyId)
                populateAccommodationSummary(propertyId)
                populateAsbestosDetail(propertyId)
                ' populatePlannedAppointments(propertyId)
                populatePlannedComponentsDetail(propertyId)
                populateReportedFaultsDefects(propertyId)
                populateTenancyHistory(propertyId)
                populateStockGasFaultRepairsAppointments(propertyId)

            Else
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Display Job Sheet Details"
    Private Sub displayJobSheetInfo(ByVal JI As String, ByVal jobSheetNumber As String, ByVal appointmentType As String)
        Dim jsnLink As String = String.Empty
        If appointmentType = ApplicationConstants.SbFaultAppointment Then
            jsnLink = GeneralHelper.getSbJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.FaultAppointment Then
            jsnLink = GeneralHelper.getJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType.Contains("Void") Then
            jsnLink = GeneralHelper.getVoidJobSheetSummaryAddress() + jobSheetNumber + "&appointmentType=" + appointmentType
        ElseIf appointmentType = ApplicationConstants.PlannedAppointment _
        Or appointmentType = ApplicationConstants.MiscAppointment _
        Or appointmentType = ApplicationConstants.AdaptationAppointment _
        Or appointmentType = ApplicationConstants.ConditionAppointment Then
            jsnLink = GeneralHelper.getPlannedJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.GasAppointment Then
            jsnLink = GeneralHelper.getGasJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.DefectAppointment Then
            jsnLink = GeneralHelper.getDefectsJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.CyclicMaintenanceAppointment _
         Or appointmentType = ApplicationConstants.MEServicingAppointment Then
            jsnLink = GeneralHelper.getMEJobSheetSummary() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.AssignedToContractorStatus Then
            jsnLink = GeneralHelper.getAssignedToContractorJobSheetAddress(JI)
        End If
        ifrmJobSheet.Attributes("src") = jsnLink

        Me.popupJobSheet.Show()

    End Sub

#End Region

#Region "Populate Tenancy/Property detail"
    ''' <summary>
    ''' Populate Tenancy/Property detail
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateTenancyPropertyDetail(ByVal propertyId As String)
        Try
            Dim resultDataSet As DataSet = New DataSet()
            Me.resetTenancyPropertyControl()
            objPropertyBL.getTenancyPropertyDetail(resultDataSet, propertyId)

            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblTenancyProperty, pnlTenancyProperty, UserMessageConstants.NoRecordFound, True)
            Else
                With resultDataSet.Tables("TenancyPropertyDetail").Rows(0)
                    lblRefNumber.Text = .Item("PROPERTYID").ToString()
                    lblAddress.Text = .Item("ADDRESS").ToString()
                    lblPostCode.Text = .Item("POSTCODE").ToString()
                    lblDev.Text = .Item("DEVELOPMENTNAME").ToString()
                    lblScheme.Text = .Item("SCHEMENAME").ToString()
                    lblStatus.Text = .Item("STATUS").ToString() + " (" + Me.getCurrentCustomer(propertyId) + ") "
                    lblType.Text = .Item("PROPERTYTYPE").ToString()
                    lblSAPRating.Text = .Item("SAPRating").ToString()
                    lblGasCertificateExpiry.Text = .Item("GasCertificateExpiry").ToString()
                    lblBuiltDate.Text = .Item("BuildDate").ToString()
                    lblEpcRating.Text = .Item("EpcRating").ToString()
                    lblAssetType.Text = .Item("AssetType").ToString()
                End With
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Reset Tenancy/Property controls"
    ''' <summary>
    ''' Reset Tenancy/Property controls
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub resetTenancyPropertyControl()

        lblRefNumber.Text = String.Empty
        lblAddress.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblDev.Text = String.Empty
        lblScheme.Text = String.Empty
        lblStatus.Text = String.Empty
        lblType.Text = String.Empty
        lblTenancyProperty.Text = String.Empty
        lblSAPRating.Text = String.Empty


    End Sub

#End Region

#Region "Populate Accommodation summary"
    ''' <summary>
    ''' Populate Accommodation summary
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateAccommodationSummary(ByVal propertyId As String)

        Me.resetAccommodationSummary()

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getAccommodationSummary(resultDataSet, propertyId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblAccommodation, pnlAccomodation, UserMessageConstants.NoRecordFound, True)
        Else
            lblNROSH1.Text = resultDataSet.Tables("Accommodation").Rows(0)("NROSHASSETTYPEMAIN")
            lblFloorArea.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("FLOORAREA")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("FLOORAREA").ToString() + " sq m")
            lblMaxPeople.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("MAXPEOPLE")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("MAXPEOPLE").ToString())
            lblBedrooms.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("BEDROOMS")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("BEDROOMS").ToString())

        End If

    End Sub

#End Region

#Region "Reset Accommodation summary"
    ''' <summary>
    ''' Reset Accommodation summary
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub resetAccommodationSummary()

        lblFloorArea.Text = String.Empty
        lblMaxPeople.Text = String.Empty
        lblBedrooms.Text = String.Empty
        lblAccommodation.Text = String.Empty

    End Sub

#End Region

#Region "Populate Tenancy History"
    ''' <summary>
    ''' Populate Tenancy History
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateTenancyHistory(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getTenancyHistory(resultDataSet, propertyId)
        grdTenancyHistory.DataSource = resultDataSet.Tables(0).DefaultView
        grdTenancyHistory.DataBind()

    End Sub

#End Region

#Region "Populate Asbestos"
    ''' <summary>
    ''' Populate Asbestos detail
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateAsbestosDetail(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getAsbestosDetail(resultDataSet, propertyId)
        grdPropertyAsbestos.DataSource = resultDataSet.Tables(0).DefaultView
        grdPropertyAsbestos.DataBind()

    End Sub

#End Region

#Region "Populate Planned Appointments"
    ''' <summary>
    ''' Populate Planned Appointments
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populatePlannedAppointments(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getPlannedAppointmentsDetail(resultDataSet, propertyId)
        grdPlannedMaintenance.DataSource = resultDataSet.Tables(0).DefaultView
        grdPlannedMaintenance.DataBind()

    End Sub

#End Region

#Region "Populate Planned Components Detail"
    ''' <summary>
    ''' Populate Planned Components Detail
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populatePlannedComponentsDetail(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getPlannedComponentsDetail(resultDataSet, propertyId)
        SessionManager.setplannedMaintenanceDataSet(resultDataSet)
        grdPlannedMaintenance.DataSource = resultDataSet.Tables(0).DefaultView
        grdPlannedMaintenance.DataBind()

    End Sub

#End Region

#Region "Populate Stock,GAS & Fault Repairs Appointments"
    ''' <summary>
    ''' Populate Stock GAS Fault Repairs Appointments
    ''' </summary>
    ''' <param name="propertyId"></param>
    ''' <remarks></remarks>
    Private Sub populateStockGasFaultRepairsAppointments(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getStockGasFaultAppointmentsDetail(resultDataSet, propertyId)
        grdAppointments.DataSource = resultDataSet.Tables(0).DefaultView
        grdAppointments.DataBind()

    End Sub

#End Region

#Region "Set View All button state"
    ''' <summary>
    ''' Set View All button state
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub setViewAllButtonState(ByVal dtFaults As List(Of ActivitiesBO))
        Dim dtRowCount As Integer = dtFaults.Count
        If (dtRowCount >= 5 AndAlso Not (grdReportedFaults.PageSize >= dtRowCount)) Then
            grdReportedFaults.PageSize = 5
            lnkBtnViewAll.Visible = True
        Else
            lnkBtnViewAll.Visible = False
            If dtRowCount > 0 Then
                grdReportedFaults.PageSize = dtRowCount
            End If
        End If
    End Sub

#End Region

#Region "Populate reported Faults/Defects"
    ''' <summary>
    ''' Populate reported Faults/Defects
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateReportedFaultsDefects(ByVal propertyID As String)

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getReportedFaultsDefects(resultDataSet, propertyID, ddlFaultType.SelectedValue)



        If (resultDataSet.Tables(0).Rows.Count > 0) Then

            'Dim drlist As New List(Of DataRow)()

            'For Each row As DataRow In resultDataSet.Tables(0).Rows
            '    drlist.Add(CType(row, DataRow))
            'Next row
            Dim query = From row As DataRow In resultDataSet.Tables(0).Rows
            Group row By JournalId = row.Field(Of Integer)("JournalId"), InspectionType = row.Field(Of String)("InspectionType") Into Group
            Select Group

            Dim activitiesBoList As List(Of ActivitiesBO) = New List(Of ActivitiesBO)()

            For Each grp In query

                Dim activity = grp.FirstOrDefault()
                Dim activitiesBo As ActivitiesBO = New ActivitiesBO()

                If (Not IsDBNull(activity.Item("AppointmentDate"))) Then
                    activitiesBo.AppointmentDate = activity.Item("AppointmentDate")
                End If
                If (Not IsDBNull(activity.Item("Action"))) Then
                    activitiesBo.Action = activity.Item("Action")
                Else
                    activitiesBo.Action = "N/A"
                End If

                If (Not IsDBNull(activity.Item("CreateDate"))) Then
                    activitiesBo.CreateDate = activity.Item("CreateDate")
                End If
                If (Not IsDBNull(activity.Item("CREATIONDATE"))) Then
                    activitiesBo.CREATIONDate = activity.Item("CREATIONDATE")
                End If


                activitiesBo.InspectionType = activity.Item("InspectionType")

                activitiesBo.JournalHistoryId = activity.Item("JournalHistoryId")
                activitiesBo.JournalId = activity.Item("JournalId")
                activitiesBo.Name = activity.Item("Name")
                activitiesBo.OPERATIVENAME = activity.Item("OPERATIVENAME")

                activitiesBo.AppointmentId = activity.Item("APPOINTMENTID")
                activitiesBo.AppointmentNotes = activity.Item("AppointmentNotes")
                activitiesBo.InspectionTypeDescription = activity.Item("InspectionTypeDescription")

                If (Not IsDBNull(activity.Item("OPERATIVETRADE"))) Then
                    activitiesBo.OPERATIVETRADE = activity.Item("OPERATIVETRADE")
                End If
                If (Not IsDBNull(activity.Item("REF"))) Then
                    activitiesBo.REF = activity.Item("REF")
                End If

                activitiesBo.Status = activity.Item("Status")
                activitiesBo.IsDocumentAttached = activity.Item("IsDocumentAttached")
                activitiesBo.CP12DocumentID = activity.Item("CP12DocumentID")

                activitiesBoList.Add(activitiesBo)
            Next


            Me.grdReportedFaults.Visible = True
            setViewAllButtonState(activitiesBoList)
            setFaultsResultDataSetViewState(resultDataSet)
            '  ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
            SessionManager.setActivitiesBOList(activitiesBoList)
            Me.grdReportedFaults.DataSource = activitiesBoList
            Me.grdReportedFaults.DataBind()

        End If


    End Sub

#End Region

#Region "Check InspectionId and Return True/False to Enable/Disable Clip Image"
    Public Function CheckInspectionID(ByRef IsDocumentAttached As Object, ByRef CP12DocumentID As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(IsDocumentAttached)) Then
            If ((CType(CP12DocumentID, Integer)) > 0 And (CType(IsDocumentAttached, Integer) = 1)) Then
                status = True
            End If
        End If
        Return status
    End Function
#End Region

#Region "Check jobsheet exist"
    Public Function CheckJobSheetExist(ByRef jobsheetNumber As Object)

        If (IsDBNull(jobsheetNumber)) Then
            Return False
        End If

        If (jobsheetNumber.Equals("N/A") = True) Then
            Return False
        End If

        Return True
    End Function
#End Region

#Region "Populate property Images"
    ''' <summary>
    ''' Populate property Images
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populatePropertyImages(ByVal propertyID As String)

        btnCamera.Visible = True
        Dim resultDataSet As DataSet = New DataSet()
        Dim imageCount As Integer
        imageCount = objPropertyBL.getPropertyImages(resultDataSet, propertyID)
        Dim rowNum As Integer = imageCount - 1
        If (imageCount > 0) Then
            Dim dtFiltered As DataTable = resultDataSet.Tables(0)

            If (dtFiltered.Rows.Count > 0) Then
                imgEmpty.Visible = True
                imgEmpty.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + dtFiltered.Rows(dtFiltered.Rows.Count - 1)("ImageName").ToString()
                imgDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                ' imgEmpty.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()

                lblImageDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                lblTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                lblUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()
            Else
                imgEmpty.Visible = True
            End If
        Else
            imgEmpty.Visible = True
        End If

    End Sub

#End Region

#Region "Populate EPC Images"
    ''' <summary>
    ''' Populate EPC Images
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Private Sub populateEPCImages(ByVal propertyID As String)
        Dim resultDataSet As DataSet = New DataSet()
        Dim imageCount As Integer

        btnEPCCamera.Visible = True
        imageCount = objPropertyBL.getPropertyEPCImages(resultDataSet, propertyID)
        Dim rowNum As Integer = imageCount - 1
        If (imageCount > 0) Then
            Dim dtFiltered As DataTable = resultDataSet.Tables(0)

            If (dtFiltered.Rows.Count > 0) Then
                btnImgEmptyEPC.Visible = True
                btnImgEmptyEPC.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + dtFiltered.Rows(dtFiltered.Rows.Count - 1)("ImageName").ToString()
                imgEPCDefaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                'btnImgEmptyEPC.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + resultDataSet.Tables(0).Rows(rowNum)("ImageName").ToString()
                lblMainEPCDueDate.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedOn").ToString()
                lblMainEPCTitle.Text = resultDataSet.Tables(0).Rows(rowNum)("Title").ToString()
                lblMainEPCUploadedBy.Text = resultDataSet.Tables(0).Rows(rowNum)("CreatedBy").ToString()

            Else
                btnImgEmptyEPC.Visible = True
            End If
        Else
            btnImgEmptyEPC.Visible = True
        End If

    End Sub

#End Region

#Region "Result Faults DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result Faults DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setFaultsResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.FaultsResultDataSet) = resultDataSet
    End Sub

    Protected Function getFaultsResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.FaultsResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.FaultsResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeFaultsResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.FaultsResultDataSet)
    End Sub

#End Region

#Region "Get Empty Table"
    ''' <summary>
    ''' Get Empty Table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetEmptyTable() As DataTable
        ' Create new DataTable instance.
        Dim table As New DataTable
        ' Create four typed columns in the DataTable.
        table.Columns.Add("ImageName", GetType(String))

        ' Add five rows with those columns filled in the DataTable.
        table.Rows.Add("noimage.gif")
        Return table
    End Function

#End Region

#Region "Check whether image exists."

    Function checkImage(ByVal dtImages As DataTable) As DataTable
        Dim no As Integer = 0
        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr(no).ToString()
            Dim fullPath As String = Server.MapPath(GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/") + imageName

            If Not System.IO.File.Exists(fullPath) Then
                dr.Row.Delete()
            End If
            no = no + 1
        Next
        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region

#Region "Get Current Customer"

    Function getCurrentCustomer(ByVal propertyId As String) As String
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getTenancyHistory(resultDataSet, propertyId)
        Dim dtTenant As DataTable = resultDataSet.Tables(0)
        Dim customerName As String = ""

        If dtTenant.Rows.Count > 0 Then
            For value As Integer = 0 To dtTenant.Rows.Count - 1
                Dim enddate As String
                enddate = dtTenant.Rows(value)("EndDate").ToString()
                If enddate.Equals(String.Empty) Then
                    customerName = dtTenant.Rows(value)("FullCustomerName").ToString()
                End If
            Next

            Return customerName
        Else
            Return customerName
        End If

    End Function

#End Region


#Region "Load Inspection Types in DropDown"
    Public Sub loadInspectionTypes(ByRef dropDownRef As DropDownList)
        Dim objUsersBL As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        objUsersBL.getInspectionTypes(resultDataSet)
        dropDownRef.DataSource = resultDataSet
        dropDownRef.DataValueField = ApplicationConstants.InspectionId
        dropDownRef.DataTextField = ApplicationConstants.InspectionName
        dropDownRef.DataBind()
        dropDownRef.Items.Add(New ListItem("Select Type", ApplicationConstants.DropDownDefaultValue))
        dropDownRef.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Show Hide Child Grid"
    Protected Sub Show_Hide_ChildGrid(sender As Object, e As EventArgs)
        Dim imgShowHide As ImageButton = TryCast(sender, ImageButton)
        Dim row As GridViewRow = TryCast(imgShowHide.NamingContainer, GridViewRow)
        If imgShowHide.CommandArgument = "Show" Then
            row.FindControl("pnlChild").Visible = True
            imgShowHide.CommandArgument = "Hide"
            imgShowHide.ImageUrl = "~/Images/minus.gif"
            Dim JournalId As String = grdReportedFaults.DataKeys(row.RowIndex).Value.ToString()

            Dim grdReportedFaultsChild As GridView = TryCast(row.FindControl("grdReportedFaultsChild"), GridView)
            Dim lblInspectionType As Label = DirectCast(row.FindControl("lblInspectionType"), Label)
            BindActivitiesChild(JournalId, lblInspectionType.Text, grdReportedFaultsChild)
        Else
            row.FindControl("pnlChild").Visible = False
            imgShowHide.CommandArgument = "Show"
            imgShowHide.ImageUrl = "~/Images/plus.gif"
        End If
    End Sub

    Private Sub BindActivitiesChild(ByVal JournalId As String, ByVal inspectionType As String, ByVal grdActivitiesChild As GridView)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = getFaultsResultDataSetViewState()
        Dim filteredActivitiesDt As DataTable = New DataTable()
        Dim activitiesDv As DataView = New DataView()
        activitiesDv = resultDataSet.Tables(0).AsDataView()
        activitiesDv.RowFilter = "JournalId = '" & JournalId & "' AND InspectionType = '" & inspectionType & "'"
        filteredActivitiesDt = activitiesDv.ToTable
        grdActivitiesChild.DataSource = filteredActivitiesDt.DefaultView.ToTable(True)
        grdActivitiesChild.DataBind()
    End Sub
#End Region
#End Region

End Class

