﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CurrentRentHistoryTab.ascx.vb"
    Inherits="Appliances.CurrentRentHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Import Namespace="AS_Utilities" %>
<style type="text/css">
    .style7
    {
        width: 404px;
        text-align: left;
        padding-left: 5px;
    }
    
    .popupTabletr
    {
    }
    
    .popupTabletd
    {
    }
</style>
<style type="text/css">
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 190px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
</style>
<div id="divRentHistory" runat="server" style="float: left; width: 100%; margin-left: 5px;">
    <asp:UpdatePanel ID="upPnlRenthistory" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCurrentRentMessage" runat="server" Visible="false" Height="19px"
                Width="289px">
                <asp:Label ID="lblCurrentRentTab" runat="server" Text=""></asp:Label>
            </asp:Panel>
            <div style="min-height: 282px;">
                <cc1:PagingGridView ID="grdRentHistory" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" BorderStyle="None" CellPadding="2" GridLines="None"
                    OrderBy="" PageSize="7" Width="100%" PagerSettings-Visible="true">
                    <Columns>
                        <asp:BoundField HeaderText="Rent Set:" SortExpression="RentSet" DataField="RentSet"
                            DataFormatString="{0:d}" />
                        <asp:BoundField HeaderText="Total Rent:" SortExpression="TotalRent" DataField="TotalRent"
                            DataFormatString="{0:F}" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Type:" SortExpression="Type" DataField="Type" />
                        <asp:BoundField HeaderText="Updated:" SortExpression="Updated" DataField="Updated"
                            DataFormatString="{0:d}" />
                        <asp:BoundField HeaderText="By:" SortExpression="By" DataField="By" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgCurrentRentHistoryArrow" runat="server" OnClick="imgCurrentRentHistoryArrow_Click"
                                    BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("SID").ToString() %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                    <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                    <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                </cc1:PagingGridView>
            </div>
            <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                background-color: #FAFAD2; border-color: #ADADAD; border-width: 2px; border-style: none;
                vertical-align: middle; border-top: 0px; border-top-color: transparent;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td align="left" style="width: 70%;">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="<<" runat="server" CommandName="Page"
                                                    CommandArgument="First" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="<" runat="server" CommandName="Page" CommandArgument="Prev" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                Page:
                                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                of
                                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                to
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                of
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text=">" runat="server" CommandName="Page" CommandArgument="Next" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text=">>" runat="server" CommandName="Page"
                                                    CommandArgument="Last" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td align="center" style="width: 30%; text-align: center;">
                                <asp:RangeValidator Display="Dynamic" ID="rangevalidatorPageNumber" runat="server"
                                    ErrorMessage="Enter a page between 1 and " ValidationGroup="pageNumber" MinimumValue="1"
                                    MaximumValue="1" ControlToValidate="txtPageNumber" Type="Integer" SetFocusOnError="True"
                                    CssClass="Required" />
                                <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                    ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="updPanelCurrentRentHistory" runat="server">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlCurrentRentHistory" Style="height: 275px; width: 370px;
            background-color: White; border: 1px solid gray; padding: 10px; top: 0px;">
            <div style="background-color: #E8E9EA; color: Black; border: solid 1px gray; height: 30px;
                padding: 8px; text-align: left;">
                <table id="tbl" style="width: 100%">
                    <tr>
                        <td style="font-size: 13px; font-weight: bold; width: 50%;">
                            Details of Changes
                        </td>
                        <td align="right" style="width: 10%;">
                            Amended:&nbsp;
                        </td>
                        <td style="width: 40%;">
                            <asp:Label ID="lblAmended" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            By:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblBy" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:ImageButton ID="imgbtnAddAppointmentCancelCalendar" runat="server" ImageAlign="Right"
                ImageUrl="~/Images/cross2.png" Style="position: absolute; top: -10px; right: -10px"
                BorderStyle="None" />
            <div style="height: 100%; width: 100%;">
                <table id="RentHistoryPopup" style="width: 100%;">
                    <tr>
                        <td style="width: 35%">
                        </td>
                        <td style="width: 25%">
                        </td>
                        <td style="width: 25%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>Previously:</b>
                        </td>
                        <td style="text-align: center;" align="center">
                            <b>Changed To:</b>
                        </td>
                    </tr>
                     <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Rent Frequency:
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblPreviousRentFrequency" runat="server"></asp:Label>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedRentFrequency" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Date Rent Set:
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblPreviousDateRentSet" runat="server"></asp:Label>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedDateRentSet" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;" >
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Rent effective to:<span class="Required"></span>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblPreviousRentEffective" runat="server"></asp:Label>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedRentEffective" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Rent Type:<span class="Required"></span>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblRentType" runat="server"></asp:Label>
                        </td>
                        <td align="center" valign="top">
                            <asp:Label ID="lblChangedRentType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Rent
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblPreviousRent" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedRent" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td valign="top">
                            Services
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblPreviousServices" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedServices" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td valign="top" style="padding-left: 2px;">
                            Council Tax
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblCouncilTax" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedCouncilTax" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td valign="top">
                            Water
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblWater" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedWater" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" valign="top" style="padding-left: 2px;">
                            Inelig.:
                        </td>
                        <td align="right">
                            <asp:Label ID="lblInelig" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedInelig" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td align="left" class="" valign="top">
                            Supp:
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblSupp" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedSupp" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="background-color: #E8E9EA;">
                        <td align="left" style="padding-left: 2px;" valign="top">
                            Garage:
                        </td>
                        <td align="right">
                            <asp:Label ID="lblGarage" runat="server"></asp:Label>
                        </td>
                        <td align="right" valign="top">
                            <asp:Label ID="lblChangedGarage" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <b>Total Rent</b>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblPreviousTotalRent" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblChangedTotalRent" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Label ID="lblCurrentRentHistory" runat="server"></asp:Label>
        <asp:ModalPopupExtender ID="mdlCurrentRentHistoryPopUp" runat="server" TargetControlID="lblCurrentRentHistory"
            PopupControlID="pnlCurrentRentHistory" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
            CancelControlID="imgbtnAddAppointmentCancelCalendar">
            <Animations>    
        <OnShown>
            <ScriptAction Script="createFixedHeader();" />
        </OnShown>
            </Animations>
        </asp:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
