﻿Imports AS_Utilities

Public Class RentTabs
    Inherits UserControlBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.CurrentRent()
        End If
    End Sub


#Region "Current Rent Tab"
    Private Sub CurrentRent()
        lnkBtnCurrentRentTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnTargetRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAnnualRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnHistory.CssClass = ApplicationConstants.TabInitialCssClass
        MainSubView.ActiveViewIndex = 0
        If SessionManager.getPropertyId() IsNot Nothing Then
            CurrentRentTab.populateRentTypeAndFinancialInformation(SessionManager.getPropertyId())

        End If
    End Sub

#End Region

#Region "Target Rent"
    Private Sub TargetRent()
        lnkBtnCurrentRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnTargetRentTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAnnualRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnHistory.CssClass = ApplicationConstants.TabInitialCssClass
        MainSubView.ActiveViewIndex = 1
        TargetRentTab.populateRentTypeAndFinancialInformation(SessionManager.getPropertyId())
    End Sub

#End Region

#Region "Annual Rent"
    Private Sub AnnualRent()
        lnkBtnCurrentRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnTargetRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAnnualRentTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnHistory.CssClass = ApplicationConstants.TabInitialCssClass
        MainSubView.ActiveViewIndex = 2
        AnnualRentTab.populateAnnualRentTab()
    End Sub

#End Region




#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

    Protected Sub lnkBtnCurrentRentTab_Click(sender As Object, e As EventArgs) Handles lnkBtnCurrentRentTab.Click
        Me.CurrentRent()
    End Sub

    Protected Sub lnkBtnTargetRentTab_Click(sender As Object, e As EventArgs) Handles lnkBtnTargetRentTab.Click
        Me.TargetRent()
    End Sub

    Protected Sub lnkBtnAnnualRentTab_Click(sender As Object, e As EventArgs) Handles lnkBtnAnnualRentTab.Click
        Me.AnnualRent()
    End Sub


    Protected Sub lnkBtnHistory_Click(sender As Object, e As EventArgs) Handles lnkBtnHistory.Click
        RentHistory.bindToGrid(SessionManager.getPropertyId())
        lnkBtnCurrentRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnTargetRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAnnualRentTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnHistory.CssClass = ApplicationConstants.TabClickedCssClass

        MainSubView.ActiveViewIndex = 3
    End Sub

End Class