﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class AidsAdaptationTab
    Inherits UserControlBase
    Dim objPropertyBl As PropertyBL = New PropertyBL()

    Public uploadedThumbUri As String = GeneralHelper.getThumbnailUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/"
#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblAddPhotoMessage.Text = "View is not available"
    End Sub
#End Region

    

#End Region
End Class