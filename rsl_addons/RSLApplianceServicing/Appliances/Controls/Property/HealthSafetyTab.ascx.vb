﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.IO

Public Class HealthSafetyTab
    Inherits UserControlBase

#Region "Properties"

    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "DocumentId", 1, 20)
    Dim totalCount As Integer = 0

#End Region

#Region "Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
        ElseIf PageName.Contains("propertyrecord") Then
        End If
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        '  bindToGrid(SessionManager.getPropertyId())
        If Not IsPostBack Then

            Me.setAsbestosTab()
            'bindToGrid("BHA0000119")
        End If
    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub
#End Region
#Region "Asbestos Tab"
    Protected Sub setAsbestosTab()
        lnkBtnAsbestosTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkRefurbishmentTab.CssClass = ApplicationConstants.TabInitialCssClass
        'lnkBtnAidAdaptationTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainSubView.ActiveViewIndex = 0
    End Sub
#End Region

#Region "Refurbishment Tab"
    Sub setRefurbishmentTab()
        lnkBtnAsbestosTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkRefurbishmentTab.CssClass = ApplicationConstants.TabClickedCssClass
        'lnkBtnAidAdaptationTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainSubView.ActiveViewIndex = 1
    End Sub
#End Region

#Region "AidAdaptation Tab "
    Sub AidAdaptationTab()
        lnkBtnAsbestosTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkRefurbishmentTab.CssClass = ApplicationConstants.TabInitialCssClass
        'lnkBtnAidAdaptationTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainSubView.ActiveViewIndex = 2

    End Sub
#End Region

#Region "get Page SortBo ViewState"
    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function
#End Region

#Region "removePageSortBoViewState"
    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub
#End Region



#End Region

    Protected Sub lnkBtnAsbestosTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnAsbestosTab.Click
        Me.setAsbestosTab()
    End Sub

    Protected Sub lnkRefurbishmentTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkRefurbishmentTab.Click
        Me.setRefurbishmentTab()
    End Sub

    'Protected Sub lnkBtnAidAdaptationTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnAidAdaptationTab.Click
    '    Me.AidAdaptationTab()
    'End Sub
End Class