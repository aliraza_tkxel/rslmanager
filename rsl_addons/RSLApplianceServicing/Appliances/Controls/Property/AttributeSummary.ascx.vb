﻿Imports AS_Utilities
Imports AS_BusinessLogic

Public Class AttributeSummary
    Inherits UserControlBase

#Region "Properties/Attrivutes"

    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Protected imageUrl As String = GeneralHelper.getImageUploadPath()

#End Region

#Region "Events"
#Region "Page Load event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region
#End Region

#Region "Functions"

#Region "Populate Attribute summary"

    Public Sub populateAttributeSummary()
        Dim propertyId As String = SessionManager.getPropertyId()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getAttributeSummary(resultDataSet, propertyId)
        populateAttributeSummaryForAllItem(resultDataSet, "Structure", lblStructure, grvStructure, lnkBtnStructure)
        populateAttributeSummaryForAllItem(resultDataSet, "Heating", lblHeating, grvHeating, lnkBtnHeating)
        populateAttributeSummaryForAllItem(resultDataSet, "Water Supply", lblWaterSupply, grvWaterSupply, lnkBtnWaterSupply)
        populateAttributeSummaryForAllItem(resultDataSet, "Living Room/Dining Room", lblLivingRoomDiningRoom, grvLivingRoomDiningRoom, lnkBtnLivingRoomDiningRoom)
        populateAttributeSummaryForAllItem(resultDataSet, "Hallway", lblHallway, grvHallway, lnkBtnHallway)
        populateAttributeSummaryForAllItem(resultDataSet, "Kitchen", lblKitchen, grvKitchen, lnkBtnKitchen)
        populateAttributeSummaryForAllItem(resultDataSet, "Bathroom", lblBathroom, grvBathroom, lnkBtnBathroom)
        populateAttributeSummaryForAllItem(resultDataSet, "Bedrooms", lblBedrooms, grvBedrooms, lnkBtnBedrooms)
        populateAttributeSummaryForAllItem(resultDataSet, "Drive/Paths", lblDrivePaths, grvDrivePaths, lnkBtnDrivePaths)
        populateAttributeSummaryForAllItem(resultDataSet, "Garage", lblGarage, grvGarage, lnkBtnGarage)
        populateAttributeSummaryForAllItem(resultDataSet, "Garden", lblGarden, grvGarden, lnkBtnGarden)
        If resultDataSet.Tables(ApplicationConstants.propertyImage) IsNot Nothing AndAlso resultDataSet.Tables(ApplicationConstants.propertyImage).Rows.Count > 0 Then
            Dim propertyImageResult = (From ps In resultDataSet.Tables(ApplicationConstants.propertyImage) Where ps.Item("ImageName").ToString() IsNot DBNull.Value Select ps)
            If propertyImageResult.Count() > 0 AndAlso Not DBNull.Value.Equals(propertyImageResult.First().Item("ImageName")) Then
                imgDefaultProperty.ImageUrl = propertyImageResult.First().Item("ImageName")
            End If
        End If
    End Sub

    Private Sub populateAttributeSummaryForAllItem(ByVal resultDataSet As DataSet, ByVal itemName As String, ByRef label As Label, ByRef gridView As GridView, ByRef linkButton As LinkButton)

        Dim propertyAttributesResult = (From ps In resultDataSet.Tables(ApplicationConstants.propertyAttributes) Where ps.Item("ItemName").ToString() = itemName Select ps)
        If propertyAttributesResult.Count() > 0 Then
            label.Text = propertyAttributesResult.First().Item("LocationName") + " > " + propertyAttributesResult.First().Item("AreaName") + " > "
            linkButton.Text = propertyAttributesResult.First().Item("ItemName")
            Dim itemId As Integer = propertyAttributesResult.First().Item("ItemId")

            linkButton.Attributes.Add("onclick", " setItemIdInSession('" + itemId.ToString + "');return false;")
            Dim propertyAttributeValuesResult = (From ps In resultDataSet.Tables(ApplicationConstants.propertyAttributeValues) Where ps.Item("ItemId").ToString() = itemId Select ps)
            If propertyAttributeValuesResult.Count() > 0 Then
                Dim loadAttributeValues As DataTable = New DataTable()
                Dim loadAttributeDv As DataView = New DataView()
                loadAttributeDv = resultDataSet.Tables(ApplicationConstants.propertyAttributeValues).AsDataView()
                'Filter the Parameter values which have the same ParameterID.
                loadAttributeDv.RowFilter = "ItemId =  " + itemId.ToString()
                loadAttributeValues = loadAttributeDv.ToTable()
                gridView.DataSource = loadAttributeValues
                gridView.DataBind()
            End If

        End If

    End Sub

#End Region

#End Region

End Class