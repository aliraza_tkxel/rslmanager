﻿
<%@ Control Language="VB" AutoEventWireup="false" CodeBehind="~/Controls/Property/RefurbishmentTab.ascx.designer.vb" Inherits="Appliances.RefurbishmentTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="pnlRefurbishmentMessage" runat="server" Style='margin-left: 15px;'>
    <asp:Label ID="lblRefurbishmentMessage" runat="server"></asp:Label>
</asp:Panel>
<div style="float: left; width: 30%; margin-left: 5px; border: 1px solid #000; padding: 5px;">
    <asp:Panel ID="pnlRefurbishmentForm" runat="server" BackColor="White" Width="300px">
        <table id="pnlRefurbishmentTable">
            <tr>
                <td align="left" valign="top" class="style3">
                    Refurbishment Date:<span class="Required">*</span>
                </td>
                <td align="left" valign="top" class="style4">
                    <asp:TextBox ID="txtRefurbishmentDate" runat="server" Enabled="false"  Style="width: 140px;"></asp:TextBox>
                    <asp:CalendarExtender ID="txtAdded_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="imgRefurbishmentCalendar" TargetControlID="txtRefurbishmentDate"
                        BehaviorID="txtRefurbishmentDate_CalendarExtender" />
                    <asp:Image ID="imgRefurbishmentCalendar" runat="server" ImageUrl="~/Images/Calendar-icon.png" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRefurbishmentDate"
                        ErrorMessage="Required" ValidationGroup="check" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" class="style2">
                    Notes:<span class="Required"></span>
                </td>
                <td align="left" valign="top" class="style1">
                <asp:TextBox ID="txtNotes" runat="server" Style="width: 168px;"  TextMode="MultiLine" size = "2000" maxlength="2000"  ></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" class="style2">
                    &nbsp;
                </td>
                <td align="right" valign="top" style="text-align: right;" class="style1">
                    &nbsp;&nbsp;
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" BackColor="White" ValidationGroup="check" />
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<div id ="divRefurbishmentGrid" runat="server"   style="float: left; width: 60%; height:400px; overflow:auto; margin-left: 5px; border: 1px solid #000; padding: 5px;">
    <asp:GridView ID="grdRefurbishment" runat="server" AutoGenerateColumns="False" Width="100%"
        AllowPaging="True" PageSize="20" BorderStyle="Solid" BorderWidth="1px" CellPadding="4"
        GridLines="Vertical" RowStyle-Width="10">
        <Columns>
            <asp:BoundField HeaderText="Date of Refurbishment" 
                DataField="REFURBISHMENT_DATE" >
            <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Entered By" DataField="USERNAME" >
            <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Notes" DataField="NOTES"  >
            <ItemStyle Width="500px" Wrap="true"/>
            </asp:BoundField>
        </Columns>
       
        <HeaderStyle HorizontalAlign="Left" CssClass="table-head" />
       
        
        <RowStyle BackColor="#EFF3FB" />
        
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        
         <AlternatingRowStyle BackColor="White" Wrap="True" />
       
         
        <PagerSettings FirstPageText="First" LastPageText="Last"
            NextPageText="   Next" PreviousPageText="   Previous" />
        <PagerStyle Width="100%" HorizontalAlign="Left" VerticalAlign="Middle" 
            Wrap="True" />
          
    </asp:GridView>
      
</div>
