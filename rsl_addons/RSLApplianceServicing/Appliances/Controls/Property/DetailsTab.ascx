﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DetailsTab.ascx.vb" Inherits="Appliances.DetailsTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="attr" TagName="MSAT" Src="~/Controls/Property/MaintenanceServicingAndTesting.ascx"    %>
<asp:Panel runat="server" ID="pnlDetailsTab" >
<asp:Panel ID="pnlMessage" runat="server" >
      <asp:Label ID="lblMessage" runat="server"></asp:Label> 
</asp:Panel>  
</asp:Panel>

<asp:Panel runat="server" ID="pnlMultipleHeating" ></asp:Panel> 
<asp:Panel runat="server" ID="pnlOtherDetailsTab" >
<asp:Panel ID="pnlOtherDetailsMsg" runat="server" >
      <asp:Label ID="lblOtherDetailsMsg" runat="server"></asp:Label> 
</asp:Panel>
</asp:Panel>
<attr:MSAT runat="server" ID="ucMSAT" />