﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports System.Net.Mime
Imports System.Net.Mail
Imports System.IO
Imports AjaxControlToolkit



Public Class PurchaseOrderForm
    Inherits UserControlBase
    Implements IAddEditPage

    Public Property IsSaved As Boolean
        Get
            Dim isSavedret As Boolean = False

            If ViewState(ViewStateConstants.IsSaved) IsNot Nothing Then
                isSavedret = Convert.ToBoolean(ViewState(ViewStateConstants.IsSaved))
            End If

            Return isSavedret
        End Get
        Set(ByVal value As Boolean)
            ViewState(ViewStateConstants.IsSaved) = value
        End Set
    End Property

    Public WriteOnly Property SetCloseButtonText As String
        Set(ByVal value As String)
            btnClose.Text = value
        End Set
    End Property

    Public Event CloseButtonClicked As EventHandler

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        Try
            uiMessage.hideMessage()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub ddlCostCentre_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.getBudgetHeadDropDownValuesByCostCentreId()
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If uiMessage.isExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub btnOk_click(ByVal sender As Object, ByVal e As EventArgs)
        mdlPopupRis.Hide()
    End Sub

    Protected Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (Convert.ToInt32(ddlContact.SelectedValue) <> ApplicationConstants.DropDownDefaultValue) Then
            Dim objAssignToContractorBl As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
            Dim detailsForEmailDS As DataSet = New DataSet()
            Dim selectedIndex As Integer = Convert.ToInt32(ddlContact.SelectedValue)
            objAssignToContractorBl.getContactEmailDetail(selectedIndex, detailsForEmailDS)

            If (detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt) Is Nothing) Or detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt).Rows.Count = 0 Then
                mdlPopupRis.Show()
            End If
        End If
    End Sub

    Protected Sub ddlBudgetHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.getExpenditureDropDownValuesByBudgetHeadId()
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessage.isError = True Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.getContactDropDownValuesbyContractorId()
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessage.isError = True Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.Page.Validate("addServiceRequired")
            Me.calculateVatAndTotal()
            Me.ddlVat.Focus()
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub ddlAttributeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.resetArea()
            Me.resetProvisionAttributes()
            'If Me.ddlAttributeType.SelectedItem.Text.ToLower().Equals("provision") Then
            '    Me.getProvisionsDropdownValues()
            'End If

            If Me.ddlAttributeType.SelectedItem.Text.ToLower().Equals("attribute") Then
                Me.getAttributesDropdownValues()
            End If

            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub txtNetCost_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.calculateVatAndTotal()
            Me.txtNetCost.Focus()
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If uiMessage.isExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessage.isError = True Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs)

        RaiseEvent CloseButtonClicked(Me, New EventArgs())
        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.[GetType](), "Pop", "$('#poModal').modal('hide');", True)

    End Sub

    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            assignWorkToContractor()

            If ddlContact.Text = "-1" Then
                mdlPopupRis.Show()
            End If

            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.[GetType](), "Pop", "$('#poModal').modal('show');", True)
            Me.showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessage.isError = True Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Page.Validate("addServiceRequired")

            If Page.IsValid Then
                addPurchaseIteminServiceRequiredDt()
            End If

            showModalPopup()
        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If uiMessage.isExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessage.isError = True Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub grdWorksDeatil_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try

            If e.Row.RowType = DataControlRowType.Footer Then
                Dim serviceRequiredDt As DataTable = getServiceRequiredDTViewState()

                If serviceRequiredDt.Rows.Count > 0 Then
                    e.Row.Cells(1).Text = serviceRequiredDt.Compute("Sum(" & ApplicationConstants.NetCostCol & ")", "").ToString()
                    e.Row.Cells(2).Text = serviceRequiredDt.Compute("Sum(" & ApplicationConstants.VatCol & ")", "").ToString()
                    e.Row.Cells(3).Text = serviceRequiredDt.Compute("Sum(" & ApplicationConstants.GrossCol & ")", "").ToString()
                End If
            End If

        Catch ex As Exception
            uiMessage.isError = True
            uiMessage.messageText = ex.Message

            If (uiMessage.isExceptionLogged = False) Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If (uiMessage.isError = True) Then
                uiMessage.showErrorMessage(uiMessage.messageText)
            End If
        End Try
    End Sub

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Me.resetProvisionAttributes()
        Dim objAttrBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim areaId As Integer = Convert.ToInt32(ddlArea.SelectedValue)

        If areaId <> -1 Then
            Me.ddlProvisionAttributes.Items.Clear()
            Dim dropDownList As List(Of DropDownBO) = New List(Of DropDownBO)()
            objAttrBL.GetAttributesSubItemsDropdownValuesByAreaId(dropDownList, areaId)

            If dropDownList.Count = 0 Then
                ddlProvisionAttributes.Items.Insert(0, New ListItem(ApplicationConstants.NoAttributeFound, ApplicationConstants.DropDownDefaultValue.ToString()))
            Else
                bindDropDownList(ddlProvisionAttributes, dropDownList)
            End If
        End If

        Me.showModalPopup()
    End Sub

    Private Function getServiceRequiredDTViewState() As DataTable
        Dim serviceRequiredDt As DataTable = Nothing
        serviceRequiredDt = TryCast(ViewState(ViewStateConstants.ServiceRequiredDT), DataTable)

        If serviceRequiredDt Is Nothing Then
            serviceRequiredDt = New DataTable()

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.WorkDetailIdCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.WorkDetailIdCol, GetType(Integer))
                insertedColumn.DefaultValue = ApplicationConstants.NoneValue
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.ServiceRequiredCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ServiceRequiredCol, GetType(String))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = String.Empty
                insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.VatCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.GrossCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.GrossCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.BudgetHeadIdCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.BudgetHeadIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            If Not serviceRequiredDt.Columns.Contains(ApplicationConstants.CostCenterIdCol) Then
                Dim insertedColumn As DataColumn = Nothing
                insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.CostCenterIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If
        End If

        Return serviceRequiredDt
    End Function

    Private Sub removeServiceRequiredDTViewState()
        ViewState.Remove(ViewStateConstants.ServiceRequiredDT)
    End Sub

    Private Sub setServiceRequiredDTViewState(ByVal dt As DataTable)
        ViewState(ViewStateConstants.ServiceRequiredDT) = dt
    End Sub

    Public Sub populateControl()
        lblPropertyName.Text = SessionManager.PropertyName
        Me.getCostCentreDropDownValues()
        Me.getVatDropDownValues()
        Me.getAllContractors()
        Me.getContactDropDownValuesbyContractorId()
        Me.getAttributeTypeDropdownValues()        
        Me.resetControls()
    End Sub

    Public Sub populateDropDown(ByVal ddl As DropDownList) Implements IAddEditPage.populateDropDown
        Throw New NotImplementedException()
    End Sub

    Public Function validateData() As Boolean Implements IAddEditPage.validateData
        Dim serviceRequiredDT As DataTable = getServiceRequiredDTViewState()
        Dim isValid As Boolean = True

        If Not Page.IsValid Then
            isValid = False
        End If

        Return isValid
    End Function

    Public Sub saveData() Implements IAddEditPage.saveData
        Throw New NotImplementedException()
    End Sub

    Public Sub loadData() Implements IAddEditPage.loadData
        Throw New NotImplementedException()
    End Sub

    Public Sub populateDropDowns() Implements IAddEditPage.populateDropDowns
        Throw New NotImplementedException()
    End Sub

    Public Sub getAttributeTypeDropdownValues()

        ddlAttributeType.Items.Clear()

        Dim objMaintenanceBL As MaintenanceBL = New MaintenanceBL()
        Dim resultDataSet As DataSet = New DataSet()
        objMaintenanceBL.getMaintenanceTypes(resultDataSet)

        If resultDataSet.Tables(0).Rows.Count > 0 Then

            Dim result = From dt In resultDataSet.Tables(0).AsEnumerable() _
                         Where Not dt.Field(Of String)("AttributeType").Equals("Provision") _
                         Select New With { _
                                .AttributeTypeId = dt.Field(Of Integer)("AttributeTypeId"),
                                .AttributeType = dt.Field(Of String)("AttributeType")
                             }
            ddlAttributeType.DataSource = result

        Else
            ddlAttributeType.DataSource = resultDataSet
        End If

        ddlAttributeType.DataValueField = "AttributeTypeId"
        ddlAttributeType.DataTextField = "AttributeType"
        ddlAttributeType.DataBind()
        ddlAttributeType.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))

    End Sub

    Public Sub getAttributesDropdownValues()
        ddlArea.Enabled = True
        ddlArea.Items.Clear()

        Dim dropDownBoList As List(Of DropDownBO) = New List(Of DropDownBO)()
        Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractorBL.GetAreaDropDownValues(dropDownBoList)

        If dropDownBoList.Count = 0 Then
            ddlArea.Items.Insert(0, New ListItem(ApplicationConstants.NoAttributeFound, ApplicationConstants.DropDownDefaultValue.ToString()))
        Else
            bindDropDownList(ddlArea, dropDownBoList)
        End If
    End Sub

    Public Sub getProvisionsDropdownValues()

        ddlProvisionAttributes.Items.Clear()
        ddlArea.Items.Clear()
        ddlArea.Items.Insert(0, New ListItem("N/A", "-1"))
        ddlArea.Enabled = False

        Dim objProvisionBL As ProvisionsBL = New ProvisionsBL()
        Dim selectedSchemeId As Integer = 0 'objSession.SchemeId
        Dim resultDataSet As DataSet = objProvisionBL.GetProvisionsData(selectedSchemeId, Nothing)

        If resultDataSet IsNot Nothing AndAlso resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlProvisionAttributes.DataSource = resultDataSet
            ddlProvisionAttributes.DataTextField = "PROVISIONNAME"
            ddlProvisionAttributes.DataValueField = "PROVISIONID"
            ddlProvisionAttributes.DataBind()
            SessionManager.ProvisionDataSet = resultDataSet
            Dim provisionDR As DataRow = resultDataSet.Tables(0).AsEnumerable().Where(Function(r) (CInt(r("PROVISIONID"))) = Convert.ToInt32(ddlProvisionAttributes.SelectedValue)).Single()

            If provisionDR IsNot Nothing Then

                If resultDataSet.Tables(ApplicationConstants.mst).Rows.Count > 0 Then
                    provisionDR = resultDataSet.Tables(ApplicationConstants.mst).AsEnumerable().Where(Function(r) (CInt(r("PROVISIONID"))) = Convert.ToInt32(ddlProvisionAttributes.SelectedValue) AndAlso (CInt(r("MSATTypeId"))) = 4).FirstOrDefault()

                    If provisionDR IsNot Nothing Then
                    End If
                End If
            End If
        Else
            ddlProvisionAttributes.Items.Insert(0, New ListItem(ApplicationConstants.NoAttributeFound, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub

    Private Sub getCostCentreDropDownValues()
        Dim dropDownList As List(Of DropDownBO) = New List(Of DropDownBO)()
        Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractorBL.GetCostCentreDropDownVales(dropDownList)
        bindDropDownList(ddlCostCentre, dropDownList)
    End Sub

    Private Sub getAllContractors()
        Dim dropDownList As List(Of DropDownBO) = New List(Of DropDownBO)()
        Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractorBL.getAllContractors(dropDownList)
        bindDropDownList(ddlContractor, dropDownList)
    End Sub

    Public Sub getVatDropDownValues()
        Dim vatBoList As List(Of VatBo) = New List(Of VatBo)()
        Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractorBL.getVatDropDownValues(vatBoList)
        ddlVat.Items.Clear()
        ddlVat.DataSource = vatBoList
        ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlVat.DataBind()
        ddlVat.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        SessionManager.VatBOList = vatBoList
    End Sub

    Private Sub getBudgetHeadDropDownValuesByCostCentreId()
        Dim costCentreId As Integer = Convert.ToInt32(ddlCostCentre.SelectedValue)

        If costCentreId = ApplicationConstants.DropDownDefaultValue Then
            resetBudgetHead()
            resetExpenditureddl()
        Else
            Dim dropDownList As List(Of DropDownBO) = New List(Of DropDownBO)()
            Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
            objAssignToContractorBL.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, costCentreId)
            bindDropDownList(ddlBudgetHead, dropDownList)
        End If
    End Sub

    Private Sub getExpenditureDropDownValuesByBudgetHeadId()
        Dim budgetHeadId As Integer = Convert.ToInt32(ddlBudgetHead.SelectedValue)
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        If budgetHeadId = ApplicationConstants.DropDownDefaultValue Then
            resetExpenditureddl()
        Else
            Dim expenditureBOList As List(Of ExpenditureBO) = New List(Of ExpenditureBO)()
            Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, budgetHeadId, employeeId)
            SessionManager.PropertyExpenditureBOList = expenditureBOList
            ddlExpenditure.Items.Clear()
            ddlExpenditure.DataSource = expenditureBOList
            ddlExpenditure.DataTextField = ApplicationConstants.ddlDefaultDataTextField
            ddlExpenditure.DataValueField = ApplicationConstants.ddlDefaultDataValueField
            ddlExpenditure.DataBind()

            If expenditureBOList.Count = 0 Then
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.NoExpenditureSetup, ApplicationConstants.DropDownDefaultValue.ToString()))
            Else
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
            End If
        End If
    End Sub

    Public Sub getContactDropDownValuesbyContractorId()
        ddlContact.Items.Clear()
        Dim dropDownBoList As List(Of DropDownBO) = New List(Of DropDownBO)()
        Dim objAssignToContractorBL As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        Dim objAssignToContractorBo As PropertyAssignToContractorBo = New PropertyAssignToContractorBo()
        objAssignToContractorBo.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue)        
        objAssignToContractorBL.GetContactDropDownValuesAndDetailsbyContractorId(dropDownBoList, objAssignToContractorBo)

        If dropDownBoList.Count = 0 Then
            ddlContact.Items.Insert(0, New ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue.ToString()))
        Else
            bindDropDownList(ddlContact, dropDownBoList)
        End If
    End Sub

    Private Sub bindDropDownList(ByRef ddlToBind As DropDownList, ByRef dropDownItemsList As List(Of DropDownBO), Optional ByVal insertDefault As Boolean = True)
        ddlToBind.Items.Clear()
        ddlToBind.DataSource = dropDownItemsList
        ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlToBind.DataBind()

        If insertDefault Then
            ddlToBind.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub

    Private Sub resetServiceRequired()
        If ddlVat.Items.Count > 0 Then
            ddlVat.SelectedIndex = ApplicationConstants.DropDownDefaultValue
        End If

        ddlCostCentre.SelectedIndex = 0
        getBudgetHeadDropDownValuesByCostCentreId()
        getExpenditureDropDownValuesByBudgetHeadId()
        txtNetCost.Text = String.Empty
        txtVat.Text = String.Empty
        txtTotal.Text = String.Empty
    End Sub

    Private Sub resetBudgetHead()
        ddlBudgetHead.Items.Clear()

        If ddlBudgetHead.Items.Count = 0 Then
            ddlBudgetHead.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub


    Private Sub resetProvisionAttributes()
        ddlProvisionAttributes.Items.Clear()

        If ddlProvisionAttributes.Items.Count = 0 Then
            ddlProvisionAttributes.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub

    Private Sub resetArea()
        ddlArea.Items.Clear()

        If ddlArea.Items.Count = 0 Then
            ddlArea.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub

    Private Sub resetExpenditureddl()
        ddlExpenditure.Items.Clear()

        If ddlExpenditure.Items.Count = 0 Then
            ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()))
        End If
    End Sub

    Private Sub calculateVatAndTotal()
        Dim vatRateId As Integer = Convert.ToInt32(ddlVat.SelectedValue)
        Dim VatRate As Decimal = 0D

        If vatRateId >= 0 Then
            VatRate = getVatRateByVatId(vatRateId)
        End If

        Dim netCost As Decimal = Nothing
        Dim defaultNetCost As Decimal = 0D

        If Decimal.TryParse(txtNetCost.Text.ToString().Trim(), defaultNetCost) Then
            netCost = (If(String.IsNullOrEmpty(txtNetCost.Text.Trim()), defaultNetCost, Convert.ToDecimal(txtNetCost.Text.Trim())))
            Dim vat As Decimal = netCost * VatRate / 100
            Dim total As Decimal = netCost + vat
            txtNetCost.Text = String.Format("{0:0.00}", netCost)
            txtVat.Text = String.Format("{0:0.00}", vat)
            txtTotal.Text = String.Format("{0:0.00}", total)
        Else
            txtVat.Text = String.Empty
            txtTotal.Text = String.Empty
        End If
    End Sub

    Private Function getVatRateByVatId(ByVal vatRateId As Integer) As Decimal
        Dim vatRate As Decimal = 0D
        Dim vatBoList As List(Of VatBo) = SessionManager.VatBOList
        Dim vatBo As VatBo = vatBoList.Find(Function(i) i.Id = vatRateId)

        If (vatBo IsNot Nothing) Then
            vatRate = vatBo.VatRate
        End If

        Return vatRate
    End Function

    Public Sub resetControls() Implements IAddEditPage.resetControls
        Me.uiMessage.hideMessage()
        Me.IsSaved = False
        Me.btnAssignToContractor.Enabled = True
        Me.ddlArea.Enabled = True
        Me.btnAdd.Enabled = True
        Me.txtPropertyApportionment.Text = String.Empty
        Me.ServiceCharge.Checked = False
        Me.txtEstimateRef.Text = String.Empty
        Me.POName.Text = String.Empty
        Me.txtWorkRequired.Text = String.Empty
        Me.resetBudgetHead()
        Me.resetExpenditureddl()
        Me.resetServiceRequired()
        Me.resetProvisionAttributes()
        Me.resetArea()
        Me.removeServiceRequiredDTViewState()
        Me.bindServiceRequiredGrid()
    End Sub

    Private Sub bindServiceRequiredGrid()
        Dim dtServiceRequired As DataTable = getServiceRequiredDTViewState()
        Dim addExtraSpaces As Boolean = False

        If dtServiceRequired.Rows.Count = 0 Then

            For Each col As DataColumn In dtServiceRequired.Columns
                col.AllowDBNull = True
            Next

            Dim newRow As DataRow = dtServiceRequired.NewRow()
            dtServiceRequired.Rows.Add(newRow)
            addExtraSpaces = True
        End If

        grdWorksDeatil.DataSource = dtServiceRequired
        grdWorksDeatil.DataBind()

        If addExtraSpaces Then
            grdWorksDeatil.Rows(0).Cells(0).Text = "<br /><br /><br />"
        End If
    End Sub

    Private Sub addPurchaseIteminServiceRequiredDt()
        Dim objServiceRequiredBo As ServiceRequiredBo = New ServiceRequiredBo()
        objServiceRequiredBo.WorkDetailId = ApplicationConstants.NoneValue
        objServiceRequiredBo.ServiceRequired = txtWorkRequired.Text.Trim()
        objServiceRequiredBo.NetCost = (If(String.IsNullOrEmpty(txtNetCost.Text.Trim()), 0D, Convert.ToDecimal(txtNetCost.Text)))
        objServiceRequiredBo.VatIdDDLValue = Convert.ToInt32(ddlVat.SelectedValue)
        objServiceRequiredBo.Vat = (If(String.IsNullOrEmpty(txtVat.Text.Trim()), 0D, Convert.ToDecimal(txtVat.Text.Trim())))
        objServiceRequiredBo.Total = (If(String.IsNullOrEmpty(txtTotal.Text.Trim()), 0D, Convert.ToDecimal(txtTotal.Text.Trim())))
        objServiceRequiredBo.ExpenditureId = Convert.ToInt32(ddlExpenditure.SelectedValue)
        objServiceRequiredBo.BudgetHeadId = Convert.ToInt32(ddlBudgetHead.SelectedValue)
        objServiceRequiredBo.CostCenterId = Convert.ToInt32(ddlCostCentre.SelectedValue)
        Dim serviceRequiredDt As DataTable = getServiceRequiredDTViewState()
        Dim serviceRequiredRow As DataRow = serviceRequiredDt.NewRow()
        serviceRequiredRow(ApplicationConstants.WorkDetailIdCol) = objServiceRequiredBo.WorkDetailId
        serviceRequiredRow(ApplicationConstants.ExpenditureIdCol) = objServiceRequiredBo.ExpenditureId
        serviceRequiredRow(ApplicationConstants.BudgetHeadIdCol) = objServiceRequiredBo.BudgetHeadId
        serviceRequiredRow(ApplicationConstants.CostCenterIdCol) = objServiceRequiredBo.CostCenterId
        serviceRequiredRow(ApplicationConstants.ServiceRequiredCol) = objServiceRequiredBo.ServiceRequired
        serviceRequiredRow(ApplicationConstants.NetCostCol) = objServiceRequiredBo.NetCost
        serviceRequiredRow(ApplicationConstants.VatTypeCol) = objServiceRequiredBo.VatIdDDLValue
        serviceRequiredRow(ApplicationConstants.VatCol) = objServiceRequiredBo.Vat
        serviceRequiredRow(ApplicationConstants.GrossCol) = objServiceRequiredBo.Total
        serviceRequiredRow(ApplicationConstants.PIStatusCol) = getPIStatus(objServiceRequiredBo.ExpenditureId, objServiceRequiredBo.Total)
        serviceRequiredDt.Rows.Add(serviceRequiredRow)
        resetServiceRequired()
        setServiceRequiredDTViewState(serviceRequiredDt)
        bindServiceRequiredGrid()
    End Sub

    Private Function getPIStatus(ByVal expenditureId As Integer, ByVal gross As Decimal) As Object
        Dim pIStatus As Integer = 3
        Dim expenditureBoList As List(Of ExpenditureBO) = SessionManager.PropertyExpenditureBOList

        If gross > 0 Then
            Dim ExpenditureBo As ExpenditureBO = expenditureBoList.Find(Function(i) i.ID = expenditureId)

            If (ExpenditureBo IsNot Nothing) Then

                If (gross > ExpenditureBo.Limit OrElse gross > ExpenditureBo.Remaining) Then
                    pIStatus = 0
                End If
            End If
        End If

        Return pIStatus
    End Function

    Private Function getPOStatusFromWorkItemsDt(ByVal serviceRequiredDT As DataTable) As Integer
        Dim pOStatus As Integer = 3

        If serviceRequiredDT.[Select](ApplicationConstants.PIStatusCol & " = 0").Count() > 0 Then
            pOStatus = 0
        End If

        Return pOStatus
    End Function

    Public Sub showModalPopup()
        CType(Me.Parent.Parent.FindControl("mdlpopupRaisePO"), ModalPopupExtender).Show()
    End Sub

    Public Sub hideModalPopup()
        CType(Me.Parent.Parent.FindControl("mdlpopupRaisePO"), ModalPopupExtender).Hide()
    End Sub

    Private Sub assignWorkToContractor()
        Dim serviceRequiredDT As DataTable = getServiceRequiredDTViewState()

        If validateData() Then
            Dim assignToContractorBo As PropertyAssignToContractorBo = SessionManager.AssignToContractorBo
            assignToContractorBo.ServiceRequiredDt = serviceRequiredDT

            If Convert.ToInt32(ddlAttributeType.SelectedValue) > 0 Then
                assignToContractorBo.AttributeTypeId = Convert.ToInt32(ddlAttributeType.SelectedValue)
                assignToContractorBo.ItemId = Convert.ToInt32(ddlProvisionAttributes.SelectedValue)
            Else
                assignToContractorBo.AttributeTypeId = Nothing
            End If

            assignToContractorBo.IncInSChge = ServiceCharge.Checked
            If ((Not String.IsNullOrWhiteSpace(txtPropertyApportionment.Text)) And (Not String.IsNullOrEmpty(txtPropertyApportionment.Text))) Then
                assignToContractorBo.PropertyApportionment = Convert.ToDecimal(txtPropertyApportionment.Text)
            End If
            assignToContractorBo.PropertyId = SessionManager.PropertyId
            assignToContractorBo.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue)
            assignToContractorBo.ContactId = Convert.ToInt32(ddlContact.SelectedValue)
            assignToContractorBo.POName = POName.Text
            assignToContractorBo.EmpolyeeId = Convert.ToInt32(ddlContact.SelectedValue)
            assignToContractorBo.EstimateRef = txtEstimateRef.Text
            assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(serviceRequiredDT)
            assignToContractorBo.UserId = SessionManager.getUserEmployeeId

            Dim objAssignToContractorBl As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
            Dim isSavedStatus As Boolean = objAssignToContractorBl.assignToContractorForPropertyPO(assignToContractorBo)

            If isSavedStatus Then
                Me.IsSaved = True
                btnAssignToContractor.Enabled = False
                btnAdd.Enabled = False
                Dim message As String = UserMessageConstants.AssignedToContractor

                Try

                    If assignToContractorBo.POStatus = 3 Then
                        Dim detailsForEmailDS As DataSet = New DataSet()
                        Dim objAssignToContractor As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
                        objAssignToContractor.getPropertyDetailsForEmail(assignToContractorBo, detailsForEmailDS)

                        If detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                            sendEmailtoContractor(assignToContractorBo)
                        Else
                            message = message & UserMessageConstants.EmailToContractor
                        End If
                    Else
                        sendEmailtoBudgetHolder(assignToContractorBo)
                    End If

                    uiMessage.showInformationMessage(message)
                Catch ex As Exception
                    message += "<br />but " & ex.Message
                    uiMessage.showErrorMessage(message)
                End Try
            Else
                uiMessage.showErrorMessage(UserMessageConstants.AssignedToContractorFailed)
            End If
        End If
    End Sub

    Private Sub sendEmailtoContractor(ByRef assignToContractorBo As PropertyAssignToContractorBo)
        Dim detailsForEmailDS As DataSet = New DataSet()
        Dim objAssignToContractor As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractor.getPropertyDetailsForEmail(assignToContractorBo, detailsForEmailDS)

        If detailsForEmailDS IsNot Nothing Then

            If (detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                Dim email As String = Convert.ToString(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows(0)("Email"))

                If String.IsNullOrEmpty(email) OrElse Not GeneralHelper.isEmail(email) Then
                    Throw New Exception("Unable to send email, invalid email address.")
                Else
                    Dim body As StringBuilder = New StringBuilder()
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/PropertyAssignWorkToContractor.html"))
                    body.Append(reader.ReadToEnd())
                    body.Replace("{ContractorContactName}", Convert.ToString(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows(0)("ContractorContactName")))
                    body.Replace("{Details}", " Please find below details of works required:")
                    body.Replace("{NetCost}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.NetCostCol & ")", "").ToString())
                    body.Replace("{VAT}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.VatCol & ")", "").ToString())
                    body.Replace("{TOTAL}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.GrossCol & ")", "").ToString())
                    body.Replace("{WorkRequired}", getServiceRequired(assignToContractorBo.ServiceRequiredDt))

                    If (detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then
                        Dim dr As DataRow = detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows(0)
                        body.Replace("{TownCity}", Convert.ToString(dr("TOWNCITY")))
                        body.Replace("{PostCode}", Convert.ToString(dr("POSTCODE")))
                        body.Replace("{Block}", Convert.ToString(dr("Block")))
                        body.Replace("{Scheme}", Convert.ToString(dr("Scheme")))
                        body.Replace("{Property}", Convert.ToString(dr("Property")))
                    Else
                        body.Replace("{TownCity}", "")
                        body.Replace("{PostCode}", "")
                        body.Replace("{Block}", "")
                        body.Replace("{Scheme}", "N/A")
                    End If

                    If (detailsForEmailDS.Tables(ApplicationConstants.PurchaseOrderDetailsDt) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.PurchaseOrderDetailsDt).Rows.Count > 0 Then
                        Dim dr As DataRow = detailsForEmailDS.Tables(ApplicationConstants.PurchaseOrderDetailsDt).Rows(0)
                        body.Replace("{PO}", Convert.ToString(dr("ORDERID")))
                    End If

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/broadland.png"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"
                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))
                    Dim mimeType As ContentType = New ContentType("text/html")
                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                    Dim mailMessage As MailMessage = New MailMessage()
                    mailMessage.Subject = ApplicationConstants.EmailSubject_SB
                    mailMessage.[To].Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows(0)("Email").ToString(), detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows(0)("ContractorContactName").ToString()))
                    mailMessage.Body = body.ToString()
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)
                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub

    Private Sub sendEmailtoBudgetHolder(ByRef assignToContractorBo As PropertyAssignToContractorBo)
        Dim detailsForEmailDS As DataSet = New DataSet()
        Dim budgetHolderDs As DataSet = New DataSet()

        Dim objAssignToContractor As PropertyAssignToContractorBL = New PropertyAssignToContractorBL()
        objAssignToContractor.getPropertyDetailsForEmail(assignToContractorBo, detailsForEmailDS)
        objAssignToContractor.getBudgetHolderByOrderId(budgetHolderDs, assignToContractorBo.OrderId, assignToContractorBo.ExpenditureId)

        If detailsForEmailDS IsNot Nothing Then

            If (detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                Dim email As String = Convert.ToString(budgetHolderDs.Tables(0).Rows(0)("Email"))

                If String.IsNullOrEmpty(email) OrElse Not GeneralHelper.isEmail(email) Then
                    Throw New Exception("Unable to send email, Because budget holder's email not exists in the system.")
                Else
                    Dim body As StringBuilder = New StringBuilder()
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/PropertyAssignWorkToContractor.html"))
                    body.Append(reader.ReadToEnd())
                    body.Replace("{ContractorContactName}", Convert.ToString(budgetHolderDs.Tables(0).Rows(0)("OperativeName")))
                    body.Replace("{Details}", "Please find below details of works requiring budget approval:")
                    body.Replace("{NetCost}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.NetCostCol & ")", "").ToString())
                    body.Replace("{VAT}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.VatCol & ")", "").ToString())
                    body.Replace("{TOTAL}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" & ApplicationConstants.GrossCol & ")", "").ToString())
                    body.Replace("{WorkRequired}", getServiceRequired(assignToContractorBo.ServiceRequiredDt))
                    body.Replace("{PO}", Convert.ToString(assignToContractorBo.OrderId))

                    If (detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then
                        Dim dr As DataRow = detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows(0)
                        body.Replace("{TownCity}", Convert.ToString(dr("TOWNCITY")))
                        body.Replace("{PostCode}", Convert.ToString(dr("POSTCODE")))
                        body.Replace("{Block}", Convert.ToString(dr("Block")))
                        body.Replace("{Scheme}", Convert.ToString(dr("Scheme")))
                        body.Replace("{Property}", Convert.ToString(dr("Property")))
                    Else
                        body.Replace("{TownCity}", "")
                        body.Replace("{PostCode}", "")
                        body.Replace("{Block}", "")
                        body.Replace("{Scheme}", "N/A")
                    End If

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/broadland.png"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"
                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))
                    Dim mimeType As ContentType = New ContentType("text/html")
                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                    Dim mailMessage As MailMessage = New MailMessage()
                    mailMessage.Subject = ApplicationConstants.EmailSubject_SB
                    mailMessage.[To].Add(New MailAddress(budgetHolderDs.Tables(0).Rows(0)("Email").ToString(), budgetHolderDs.Tables(0).Rows(0)("OperativeName").ToString()))
                    mailMessage.Body = body.ToString()
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)
                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub

    Private Function getServiceRequired(ByVal dataTable As DataTable) As String
        Dim serviceRequired As StringBuilder = New StringBuilder()

        If dataTable.Rows.Count = 1 Then
            serviceRequired.Append(dataTable.Rows(0)(ApplicationConstants.ServiceRequiredCol).ToString())
        Else
            serviceRequired.Append("<ol>")

            For Each row As DataRow In dataTable.Rows
                serviceRequired.Append("<li>" & row(ApplicationConstants.ServiceRequiredCol).ToString() & "</li>")
            Next

            serviceRequired.Append("</ol>")
        End If

        Return serviceRequired.ToString()
    End Function

    Private Function getRiskDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim RiskDetails As StringBuilder = New StringBuilder("N/A")

        If (detailsForEmailDS IsNot Nothing) AndAlso (detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows.Count > 0 Then
            RiskDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows
                RiskDetails.Append(row("CATDESC") + (If(String.IsNullOrEmpty(Convert.ToString(row("SUBCATDESC"))), "", ": " & row("SUBCATDESC"))) & "<br />")
            Next
        End If

        Return RiskDetails.ToString()
    End Function

    Private Function getVulnerabilityDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim vulnerabilityDetails As StringBuilder = New StringBuilder("N/A")

        If (detailsForEmailDS IsNot Nothing) AndAlso (detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt) IsNot Nothing) AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows.Count > 0 Then
            vulnerabilityDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows
                vulnerabilityDetails.Append(row("CATDESC") + (If(String.IsNullOrEmpty(Convert.ToString(row("SUBCATDESC"))), "", ": " & row("SUBCATDESC"))) & "<br />")
            Next
        End If

        Return vulnerabilityDetails.ToString()
    End Function




End Class