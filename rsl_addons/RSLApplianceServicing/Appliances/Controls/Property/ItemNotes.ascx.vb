﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessObject
Imports AS_BusinessLogic

Public Class ItemNotes
    Inherits UserControlBase
    Dim _readOnly As Boolean = False

#Region "Events"
    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Public Shared ItemNotesId As Integer
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnAddNotes.Visible = False
        End If
    End Sub
#End Region

#Region "btn add notes click event"


    Protected Sub btnAddNotes_Click(sender As Object, e As EventArgs) Handles btnAddNotes.Click
        Try
            chkOnApp.Checked = False
            chkScheduling.Checked = False
            txtNote.Text = String.Empty
            uiMessageHelper.resetMessage(lblAddNoteMessage, pnlAddNoteMessage)
            mdlPopUpAddNote.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddNoteMessage, pnlAddNoteMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "btnsave click"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim uiHelper As UIMessageHelper = New UIMessageHelper()
        uiHelper.resetMessage(lblAddNoteMessage, pnlAddNoteMessage)
        Dim objItemNotesBo As ItemNotesBO = New ItemNotesBO()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
        Dim itemId As Integer = SessionManager.getTreeItemId()
        Try
            If txtNote.Text <> String.Empty Then
                objItemNotesBo.ItemId = itemId
                objItemNotesBo.PropertyId = propertyId
                objItemNotesBo.Notes = txtNote.Text
                objItemNotesBo.ShowinScheduling = chkScheduling.Checked
                objItemNotesBo.ShowOnApp = chkOnApp.Checked
                objItemNotesBo.CreatedBy = SessionManager.getAppServicingUserId()
                Dim HeatingMappingId = Nothing
                If SessionManager.getTreeItemName() = "Heating" Then
                    objItemNotesBo.HeatingMappingId = Convert.ToInt32(SessionManager.getHeatingMappingId())
                    HeatingMappingId = objItemNotesBo.HeatingMappingId
                End If
                objPropertyBl.SaveItemNotes(objItemNotesBo)
                If SessionManager.getTreeItemName() = "Heating" Then
                    objPropertyBl.GetItemNotes(propertyId, itemId, resultDataSet, Convert.ToInt32(SessionManager.getHeatingMappingId()))
                Else
                    objPropertyBl.GetItemNotes(propertyId, itemId, resultDataSet)
                End If

                grdItemNotes.DataSource = resultDataSet
                grdItemNotes.DataBind()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.NotValidNote
                mdlPopUpAddNote.Show()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddNoteMessage, pnlAddNoteMessage, uiMessageHelper.Message, True)
                mdlPopUpAddNote.Show()
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

    Public Sub loadData()
        Dim notesDataSet As DataSet = New DataSet()
        If SessionManager.getTreeItemName() = "Heating" Then
            objPropertyBL.GetItemNotes(SessionManager.getPropertyId, SessionManager.getTreeItemId, notesDataSet, Convert.ToInt32(SessionManager.getHeatingMappingId()))
        Else
            objPropertyBL.GetItemNotes(SessionManager.getPropertyId, SessionManager.getTreeItemId, notesDataSet)
        End If
        grdItemNotes.DataSource = notesDataSet
        grdItemNotes.DataBind()
    End Sub
#End Region

    Protected Sub btnShowNotesTrail_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnImage As Button = CType(sender, Button)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        Dim itemNotesId As Integer = commandParams(0)
        Dim notesDataSet As DataSet = New DataSet()
        objPropertyBL.GetItemNotesTrail(itemNotesId, notesDataSet)
        grdItemNotesTrail.DataSource = notesDataSet
        grdItemNotesTrail.DataBind()
        mdlPopupNotesTrail.Show()
    End Sub

    Protected Sub btnEditNotes_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnImage As Button = CType(sender, Button)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        txtEditNote.Text = commandParams(0)
        ItemNotesId = commandParams(1)
        chkEditScheduling.Checked = commandParams(2)
        chkEditOnApp.Checked = commandParams(3)
        mdlPopUpEditNotes.Show()
    End Sub

    Protected Sub btnDeleteNotes_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnImage As Button = CType(sender, Button)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        Dim itemNotesId As Integer = commandParams(0)
        objPropertyBL.DeleteItemNotes(itemNotesId)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.GetItemNotes(Request.QueryString(PathConstants.PropertyIds), SessionManager.getTreeItemId(), resultDataSet, Convert.ToInt32(SessionManager.getHeatingMappingId()))
        grdItemNotes.DataSource = resultDataSet
        grdItemNotes.DataBind()
    End Sub

#Region "btnsave click"

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Dim uiHelper As UIMessageHelper = New UIMessageHelper()
        uiHelper.resetMessage(lblEditNoteMessage, pnlEditNoteMessage)
        Dim objItemNotesBo As ItemNotesBO = New ItemNotesBO()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
        Dim itemId As Integer = SessionManager.getTreeItemId()
        Try
            If txtEditNote.Text <> String.Empty Then
                objItemNotesBo.Notes = txtEditNote.Text
                objItemNotesBo.NotesId = ItemNotesId
                objItemNotesBo.ShowOnApp = chkEditOnApp.Checked
                objItemNotesBo.ShowinScheduling = chkEditScheduling.Checked
                objPropertyBl.UpdateItemNotes(objItemNotesBo)

                objPropertyBl.GetItemNotes(propertyId, itemId, resultDataSet, Convert.ToInt32(SessionManager.getHeatingMappingId()))
                grdItemNotes.DataSource = resultDataSet
                grdItemNotes.DataBind()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.NotValidNote
                mdlPopUpEditNotes.Show()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblEditNoteMessage, pnlEditNoteMessage, uiMessageHelper.Message, True)
                mdlPopUpEditNotes.Show()
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Show Button For Heating"
    Public Function IsShowButton(ByVal IsActive As Boolean)
        'If SessionManager.getTreeItemName() = "Heating" Then
        '    Return True
        'End If
        'Return False
        Return IsActive
    End Function
#End Region
End Class