﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AnnualRentTab

    '''<summary>
    '''upPnlAnnualRentTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upPnlAnnualRentTab As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlAnnualRentTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAnnualRentTab As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlAnnualRentMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAnnualRentMsg As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblAnnualRentTab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAnnualRentTab As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtFileUploaded control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFileUploaded As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAnnualRentDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualRentDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAnnualEffectiveDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualEffectiveDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAnnualRent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualRent As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator3 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator7 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualServices control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualServices As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator4 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualCouncilTax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualCouncilTax As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator5 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator2 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualWater control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualWater As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator6 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator3 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualIntelig control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualIntelig As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator7 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator4 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualSupp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualSupp As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CompareValidator8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator8 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''RegularExpressionValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator5 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAnnualTotalRent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAnnualTotalRent As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator6 As Global.System.Web.UI.WebControls.RegularExpressionValidator
End Class
