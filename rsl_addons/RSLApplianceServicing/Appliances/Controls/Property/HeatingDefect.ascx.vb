﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic

Public Class HeatingDefect
    Inherits UserControlBase

#Region "Attributes"
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()

    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Fuel", 1, 30)
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnAddDefect.Visible = False
        End If
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        'uiMessageHelper.resetMessage(lblAddApplianceMessage, pnlAddApplianceMessage)

        If Not IsPostBack Then
            Me.setPropertyId(Request.QueryString(PathConstants.PropertyIds))
            lnkBtnServDefectTab.CssClass = ApplicationConstants.TabClickedCssClass
            MainView.ActiveViewIndex = 0
        End If
    End Sub
#End Region

#Region "lnk Btn Serv Defect Tab Click"

    Protected Sub lnkBtnServDefectTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        lnkBtnServDefectTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnPhotographTab.CssClass = ApplicationConstants.TabInitialCssClass

        MainView.ActiveViewIndex = 0
    End Sub

#End Region

#Region "lnk Btn Photograph Tab Click"
    Protected Sub lnkBtnPhotographTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        lnkBtnServDefectTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnPhotographTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 1
    End Sub
#End Region

#Region "btn Add Defect Click"

    Protected Sub btnAddDefect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDefect.Click
        Try
            'uiMessageHelper.resetMessage(lblFaultMessage, pnlFaultMessage)
            'uiMessageHelper.resetMessage(lblApplianceDDL, pnlApplianceDDL)
            'uiMessageHelper.resetMessage(lblCATErrorMsg, pnlCatErrorMsg)
            'uiMessageHelper.resetMessage(lblSNumber, pnlSNumber)

            Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
            'loadCategoryDDL()
            'loadPropertyAppliancesDDL(propertyId)
            ucDefectManagement.prepareAddDefect(propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                ' uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
            End If
            mdlPoPUpAddDefect.Show()
        End Try
    End Sub

#End Region

#Region "btn Save Defect Click"

    Protected Sub btnSaveDefect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucDefectManagement.saveButton_Clicked
        'Dim objDefectBO As ApplianceDefectBO = New ApplianceDefectBO()
        Dim flag As Boolean = True
        Try
            '    objDefectBO.PropertyId = Me.getPropertyId()
            '    If ddlAppliance.Items.Count() > 0 AndAlso Not ddlAppliance.SelectedItem.Value = 0 Then

            '        objDefectBO.ApplianceId = ddlAppliance.SelectedItem.Value
            '    Else
            '        flag = False
            '        mdlPoPUpAddDefect.Show()
            '        uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, UserMessageConstants.SelectAppliance, True)
            '        Exit Try
            '    End If
            '    If Not ddlCategory.SelectedItem.Value = 0 Then
            '        objDefectBO.CategoryId = ddlCategory.SelectedItem.Value
            '    Else
            '        flag = False
            '        mdlPoPUpAddDefect.Show()
            '        uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, UserMessageConstants.SelectCategory, True)
            '        Exit Try
            '    End If
            '    If Not txtBoxDueDate.Text = "" Then
            '        System.DateTime.Parse(txtBoxDueDate.Text)
            '        objDefectBO.DefectDate = txtBoxDueDate.Text
            '    Else
            '        flag = False
            '        mdlPoPUpAddDefect.Show()
            '        uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, UserMessageConstants.SelectDefectDate, True)
            '    End If
            '    If rdBtnDefectList.SelectedItem.Value = 1 Then
            '        objDefectBO.DefectIdentified = True
            '    End If
            '    objDefectBO.DefectIdentifiedNotes = txtBoxDefectNotes.Text
            '    If rdBtnActionList.SelectedItem.Value = 1 Then
            '        objDefectBO.IsRemedialActionTaken = True
            '    End If
            '    If rdBtnWarningList.SelectedItem.Value = 1 Then
            '        objDefectBO.IsWarningNoteIssued = True
            '        If Not txtBoxSerialNumber.Text = "" Then
            '            objDefectBO.SerialNumber = txtBoxSerialNumber.Text
            '        Else
            '            flag = False
            '            mdlPoPUpAddDefect.Show()
            '            uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, UserMessageConstants.SerialNumber, True)
            '        End If
            '    End If
            '    If chkBoxTag.Checked Then
            '        objDefectBO.IsWarningTagFixed = True
            '    End If
            '    objDefectBO.PhotoName = lblFileName.Text
            '    ' objDefectBO.FilePath = Server.MapPath(GeneralHelper.getImageUploadPath())
            '    objDefectBO.RemedialActionNotes = txtBoxActionNotes.Text
            '    objDefectBO.PhotosNotes = txtBoxPohoto.Text
            If flag = True Then
                'objPropertyBl.saveDefect(objDefectBO)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectSavedSuccessfuly, False)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                mdlPoPUpAddDefect.Show()
            Else
                'updPnlDefectsTab.Update()
                loadDefects()

                'Dim updPnlServiceDefects As UpdatePanel = CType(serviceDefects.FindControl("updPnlServiceDefects"), UpdatePanel)
                'updPnlServiceDefects.Update()
                'resetDefectControls()
            End If

        End Try
    End Sub

#End Region

#Region "imgBtn Add Category Click"
    'Protected Sub imgBtnAddCategory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnAddCategory.Click
    '    Try

    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            ' uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
    '        End If
    '        mdlPoPUpAddDefect.Show()
    '        mdlPopUpAddCategory.Show()
    '    End Try
    'End Sub
#End Region

#Region " btn Save Category Click"

    'Protected Sub btnSaveCategory_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveCategory.Click
    '    Try
    '        Dim categoryTitle As String = txtBoxCategoryTitle.Text

    '        objPropertyBl.addCategory(categoryTitle)

    '        uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, UserMessageConstants.CategoryAddedSuccessfuly, False)

    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblFaultMessage, pnlFaultMessage, uiMessageHelper.Message, True)
    '        End If
    '        loadCategoryDDL()
    '        mdlPoPUpAddDefect.Show()
    '    End Try
    'End Sub

#End Region

#Region "Button Upload Fault Photo Click"

    'Protected Sub btnUploadFaultPhotos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadFaultPhotos.Click
    '    mdlPoPUpAddDefect.Show()
    'End Sub

#End Region

#Region "Check Box Photo Upload Check Changed"

    Protected Sub ckBoxPhotoUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxPhotoUpload.CheckedChanged
        Me.ckBoxPhotoUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getPhotoUploadName())) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Dim lblFileName As Label = TryCast(ucDefectManagement.FindControl("lblFileName"), Label)
                lblFileName.Text = SessionManager.getPhotoUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            mdlPoPUpAddDefect.Show()
            SessionManager.removePhotoUploadName()
        End Try
    End Sub

#End Region

#Region "Defect Cancel Button Click"

    Private Sub ucDefectManagement_cancelButton_Clicked(sender As Object, e As System.EventArgs) Handles ucDefectManagement.cancelButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "serviceDefects viewDefect Clicked(sender As Object, e As System.EventArgs) Handles serviceDefects.viewDefect_Clicked"

    Private Sub serviceDefects_viewDefect_Clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles serviceDefects.viewDefect_Clicked
        Try
            ucDefectManagement.viewDefectDetails(serviceDefects.PropertyDefectId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            mdlPoPUpAddDefect.Show()
        End Try
    End Sub

#End Region



#Region "Show popup again event fired"

    Private Sub showPopupAgain(ByVal sender As Object, ByVal e As System.EventArgs) Handles ucDefectManagement.showPoupAgain
        Try
            mdlPoPUpAddDefect.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Functions"

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "set Property Id "

    Private Sub setPropertyId(ByVal propertyId As String)
        ViewState(ViewStateConstants.PropertyId) = propertyId
    End Sub
#End Region

#Region "get Property Id"
    Private Function getPropertyId() As String
        Return CType(ViewState(ViewStateConstants.PropertyId), String)
    End Function
#End Region

#Region "load Category DDL"

    'Public Sub loadCategoryDDL()
    '    Dim resultDataSet As DataSet = New DataSet()
    '    Try
    '        objPropertyBl.loadCategoryDDL(resultDataSet)
    '        ddlCategory.DataSource = resultDataSet
    '        ddlCategory.DataTextField = "Description"
    '        ddlCategory.DataValueField = "CategoryId"
    '        ddlCategory.DataBind()
    '        ddlCategory.Items.Add(New ListItem("Please Select...", "0"))
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            '  uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
    '        End If
    '    End Try

    'End Sub
#End Region

#Region "Is Appliance Has Defect "
    Public Function isApplianceHasDefect(ByVal applianceId As String) As Boolean
        If applianceId <> "" Then
            If (CType(applianceId, Integer) = 0) Then
                Return False
            Else
                Return True
            End If
        End If
    End Function
#End Region

#Region "load Property Appliances DDL"
    'Public Sub loadPropertyAppliancesDDL(ByVal propertyId As String)
    '    Dim resultDataSet As DataSet = New DataSet()
    '    Try
    '        objPropertyBl.loadPropertyAppliancesDDL(propertyId, resultDataSet)
    '        ddlAppliance.DataSource = resultDataSet
    '        ddlAppliance.DataTextField = "APPLIANCETYPE"
    '        ddlAppliance.DataValueField = "PROPERTYAPPLIANCEID"
    '        ddlAppliance.DataBind()
    '        'ddlAppliance.Items.Add(New ListItem("Please Select...", "0"))

    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If
    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            ' uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
    '        End If
    '    End Try
    'End Sub
#End Region

#Region "reset Defect Controls"
    'Public Sub resetDefectControls()
    '    If ddlAppliance.Items.Count() > 0 Then
    '        ddlAppliance.SelectedItem.Value = 1
    '    End If

    '    ddlCategory.SelectedItem.Value = 1
    '    rdBtnDefectList.SelectedItem.Value = 1
    '    rdBtnActionList.SelectedItem.Value = 1
    '    rdBtnWarningList.SelectedItem.Value = 1
    '    txtBoxDueDate.Text = String.Empty
    '    txtBoxPohoto.Text = String.Empty
    '    txtBoxDefectNotes.Text = String.Empty
    '    txtBoxActionNotes.Text = String.Empty
    '    chkBoxTag.Checked = False
    '    txtBoxSerialNumber.Text = String.Empty
    '    lblFileName.Text = String.Empty
    'End Sub
#End Region

#Region "load Defects"
    Public Sub loadDefects()
        ' Dim applianceId As Integer = ViewState(ViewStateConstants.ApplianceId)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyAppliances(Me.getPropertyId(), resultDataSet)
        Dim grdService As GridView = CType(serviceDefects.FindControl("grdServiceDefects"), GridView)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            grdService.DataSource = resultDataSet
            grdService.DataBind()
        End If
    End Sub
#End Region

#End Region

End Class