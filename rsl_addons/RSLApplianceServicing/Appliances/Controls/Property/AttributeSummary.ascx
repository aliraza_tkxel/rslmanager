﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AttributeSummary.ascx.vb"
    Inherits="Appliances.AttributeSummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel runat="server" ID="pnlDetailsTab">
    <asp:Panel ID="pnlMessage" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
</asp:Panel>
<asp:UpdatePanel runat="server" ID="updPnlAttributeSummary">
    <ContentTemplate>
        <div class="summary-Detail-Left">
            <div class="summary-Detail">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 94%; font-size: 11px;">
                    <asp:Label ID="lblStructure" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnStructure" runat="server" Style='float: left;'>
                    </asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 325px">
                    <asp:GridView ID="grvStructure" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 94%; font-size: 11px;">
                    <asp:Label ID="lblHeating" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnHeating" runat="server" Style='float: left;'></asp:LinkButton></div>
                <div style="margin-left: 5px; width: 325px">
                    <asp:GridView ID="grvHeating" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 94%; font-size: 11px;">
                    <asp:Label ID="lblWaterSupply" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnWaterSupply" runat="server" Style='float: left;'></asp:LinkButton></div>
                <div style="margin-left: 5px; width: 325px">
                    <asp:GridView ID="grvWaterSupply" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
                 
            <div class="summary-Detail">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 94%; font-size: 11px;">
                    <asp:Label ID="lblLivingRoomDiningRoom" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnLivingRoomDiningRoom" runat="server" Style='float: left;'></asp:LinkButton></div>
                <div style="margin-left: 5px; width: 325px">
                    <asp:GridView ID="grvLivingRoomDiningRoom" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 94%; font-size: 11px;">
                    <asp:Label ID="lblHallway" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnHallway" runat="server" Style='float: left;'></asp:LinkButton></div>
                <div style="margin-left: 5px; width: 325px">
                    <asp:GridView ID="grvHallway" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="summary-Detail-Middle">
            <div class="summary-Detail-Middle-Inner">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%; font-size: 11px;">
                    <asp:Label ID="lblKitchen" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnKitchen" runat="server" Style='float: left;'></asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvKitchen" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail-Middle-Inner">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%; font-size: 11px;">
                    <asp:Label ID="lblBathroom" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnBathroom" runat="server" Style='float: left;'></asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvBathroom" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail-Middle-Inner">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%; font-size: 11px;">
                    <asp:Label ID="lblBedrooms" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnBedrooms" runat="server" Style='float: left;'></asp:LinkButton></div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvBedrooms" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="summary-Detail-Right">
            <div class="summary-Detail-Right-Inner">
                <asp:Image ImageUrl="~/Images/noimage.gif" runat="server" ID="imgDefaultProperty"
                    Width="270px" Height="235px" />
            </div>
            <div class="summary-Detail-Right-Inner">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%; font-size: 11px;">
                    <asp:Label ID="lblDrivePaths" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnDrivePaths" runat="server" Style='float: left;'></asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvDrivePaths" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail-Right-Inner">
                <%-- <div style="width: 260px; background-color: #b7b8bb; height: 25px; font-weight: bold;
                    padding: 5px; font-size: 11px;">--%>
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%;">
                    <asp:Label ID="lblGarage" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnGarage" runat="server" Style='float: left;'></asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvGarage" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
            <div class="summary-Detail-Right-Inner">
                <div class="mainheading-panel" style="margin-left: 5px; margin-top: 5px; margin-right: 2px;
                    width: 93%; font-size: 11px;">
                    <asp:Label ID="lblGarden" runat="server" Style='float: left;' />
                    <asp:LinkButton ID="lnkBtnGarden" runat="server" Style='float: left;'></asp:LinkButton>
                </div>
                <div style="margin-left: 5px; width: 260px">
                    <asp:GridView ID="grvGarden" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="false"
                        runat="server" AutoGenerateColumns="False" HorizontalAlign="Left" Width="100%"
                        GridLines="None" ShowHeader="false" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField DataField="ParameterName" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ParameterValue" ItemStyle-Width="160px" />
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
