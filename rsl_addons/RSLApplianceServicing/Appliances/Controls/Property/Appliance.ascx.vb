﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic

Public Class Appliance
    Inherits UserControlBase

#Region "Attributes"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim _readOnly As Boolean = False
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "ApplianceId", 1, 30)
#End Region

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
            btnAddAppliance.Visible = False
        End If
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        uiMessageHelper.resetMessage(lblAddApplianceMessage, pnlAddApplianceMessage)
        Me.setPageSortBo(objPageSortBo)
    End Sub
#End Region

#Region "grd Appliances Page Index Changing "

    Protected Sub grdAppliances_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppliances.PageIndexChanging
        Try

            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppliances.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppliancesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Grid Appliances Sorting"

    Protected Sub grdAppliances_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppliances.Sorting
        Try

            'get the sort expression
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index. If we try to set it, grid has no effect.
            'So now whenever user 'll click on any column to sort, it 'll set first page
            ' objPageSortBo.PageNumber = 1
            ' grdAppliances.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction so we are saving the previous sort direction in view state 
            'and 
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortAppliancesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "btn Add Appliance Click"
    Protected Sub btnAddAppliance_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAppliance.Click
        Try
            Me.resetAddApplainceControls()
            getLifeSpanLookUpValues(ddlLifeSpan)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddAppliance.Show()
        End Try

    End Sub
#End Region

#Region "Grid Works Detail Row data bound"

    Protected Sub grdAppliances_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAppliances.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As Button = DirectCast(e.Row.FindControl("btnDelete"), Button)

                If SessionManager.getTreeItemName() = ApplicationConstants.appliancesItemName Then
                    btnDelete.Visible = True
                Else
                    btnDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "btn Save Click"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim isSaved As Boolean = False
        Try
            Dim proeprtyApplianceId As Integer = 0
            If validateForm() Then


                proeprtyApplianceId = saveAppliance()

                If proeprtyApplianceId > 0 Then
                    Me.loadPropertyAppliances()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ApplianceSavedSuccessfully, False)
                    isSaved = True
                Else
                    uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
                End If

            End If
            'Dim proeprtyApplianceId As Integer = 0

            'proeprtyApplianceId = saveAppliance()

            'If proeprtyApplianceId > 0 Then
            '    Me.loadPropertyAppliances()
            '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ApplianceSavedSuccessfully, False)
            '    isSaved = True
            'Else
            '    uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
            'End If



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
            End If

            If isSaved = False Then
                Me.mdlPopUpAddAppliance.Show()
            Else
                Me.mdlPopUpAddAppliance.Hide()
            End If
        End Try
    End Sub
#End Region

#Region " Validate Form "

    Private Function validateForm() As Boolean

        If txtItem.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectItem, True)

            Return False
        End If
        If txtQuantity.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectQuantity, True)

            Return False
        End If
        If txtDimensions.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectDimensions, True)

            Return False
        End If
        If txtLocation.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectLocation, True)

            Return False
        End If
        If txtMake.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectMake, True)

            Return False
        End If
        If txtType.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectType, True)

            Return False
        End If
        If txtModel.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.ModelIsEmpty, True)

            Return False
        End If
        If txtSerialNumber.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectSerialNo, True)

            Return False
        End If
        If Convert.ToInt32(ddlLifeSpan.SelectedValue) = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectLifespan, True)
            Return False
        End If

        'If txtPurchaseCost.Text = "" Then
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectPurchaseCost, True)

        '    Return False
        'End If
        'If txtDatePurchased.Text = "" Then
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectDatePurchased, True)

        '    Return False
        'End If
        'If txtRemovedDate.Text = "" Then
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectRemoveDate, True)

        '    Return False
        'End If
        If txtNotes.Text = "" Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, UserMessageConstants.SelectNotes, True)

            Return False
        End If

        
        'If (System.IO.Path.GetExtension(flUploadDoc.FileName) <> ".pdf") Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAPDF, True)
        '    Return False
        'End If

        Return True

    End Function

#End Region

#Region "btn Yes Click"
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnYes.Click
        Dim isDeleted As Boolean = False
        Try
            isDeleted = deleteAppliance()

            If isDeleted = True Then
                Me.loadPropertyAppliances()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ApplianceDeletedSuccessfully, False)
            Else
                uiMessageHelper.setMessage(lblDeleteApplianceMessage, pnlDeleteApplianceMessage, UserMessageConstants.ApplianceDeleteFailed, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblDeleteApplianceMessage, pnlDeleteApplianceMessage, uiMessageHelper.Message, True)
            End If

            If isDeleted = False Then
                Me.mdlPopUpDeleteAppliance.Show()
            Else
                Me.mdlPopUpDeleteAppliance.Hide()
            End If
        End Try
    End Sub
#End Region

#Region "btnAmend Click event"
    ''' <summary>
    ''' btnAmend Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAmend_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Me.resetAddApplainceControls()
            Dim btnAmend As Button = CType(sender, Button)
            Dim applianceId As Int32 = CType(btnAmend.CommandArgument, Int32)
            ViewState(ViewStateConstants.ApplianceId) = applianceId
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.getApplianceDetailDataSet()
            '  ViewState(ViewStateConstants.GridDataSet)
            getLifeSpanLookUpValues(ddlLifeSpan)
            Dim applianceDataTable As DataTable = New DataTable()
            applianceDataTable = resultDataSet.Tables(0)
            Dim applianceResult = (From ps In applianceDataTable Where ps.Item("ApplianceId").ToString() = applianceId.ToString() Select ps)
            If applianceResult.Count() > 0 Then
                If Not DBNull.Value.Equals(applianceResult.First().Item("LocationID")) Then
                    hdnSelectedLocationId.Value = applianceResult.First().Item("LocationID")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("MakeId")) Then
                    hdnSelectedmakeId.Value = applianceResult.First().Item("MakeId")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("ModelId")) Then
                    hdnSelectedModelId.Value = applianceResult.First().Item("ModelId")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("TypeId")) Then
                    hdnSelectedTypeId.Value = applianceResult.First().Item("TypeId")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("DatePurchased")) Then
                    txtDatePurchased.Text = applianceResult.First().Item("DatePurchased")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Dimensions")) Then
                    txtDimensions.Text = applianceResult.First().Item("Dimensions")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Item")) Then
                    txtItem.Text = applianceResult.First().Item("Item")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Location")) Then
                    txtLocation.Text = applianceResult.First().Item("Location")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Make")) Then
                    txtMake.Text = applianceResult.First().Item("Make")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Model")) Then
                    txtModel.Text = applianceResult.First().Item("Model")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Type")) Then
                    txtType.Text = applianceResult.First().Item("Type")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("PurchaseCost")) AndAlso applianceResult.First().Item("PurchaseCost") <> 0 Then
                    txtPurchaseCost.Text = applianceResult.First().Item("PurchaseCost")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Quantity")) Then
                    txtQuantity.Text = applianceResult.First().Item("Quantity")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("SerialNumber")) Then
                    txtSerialNumber.Text = applianceResult.First().Item("SerialNumber")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("Notes")) Then
                    txtNotes.Text = applianceResult.First().Item("Notes")
                End If
                If Not DBNull.Value.Equals(applianceResult.First().Item("LifeSpan")) AndAlso applianceResult.First().Item("LifeSpan") <> 0 Then
                    ddlLifeSpan.SelectedValue = applianceResult.First().Item("LifeSpan")
                End If
            End If
            mdlPopUpAddAppliance.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnDelete Click event"
    ''' <summary>
    ''' btnDelete Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnDelete As Button = CType(sender, Button)
            Dim applianceId As Int32 = CType(btnDelete.CommandArgument, Int32)
            ViewState(ViewStateConstants.ApplianceId) = applianceId            
            mdlPopUpDeleteAppliance.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Load Property Appliances"

    Public Sub loadPropertyAppliances()
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim objContainerPage As PropertyRecord = New PropertyRecord()

            If Not IsNothing(Request.QueryString(PathConstants.PropertyIds)) Then
                Me.setPropertyId(Request.QueryString(PathConstants.PropertyIds))
                Me.setPageSortBo(objPageSortBo)
                Me.populateAppliancesGrid()
            Else
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "populate Appliances Grid "

    Protected Sub populateAppliancesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalRecords As Integer = 0
        Dim itemId As Integer = SessionManager.getTreeItemId()
        Dim propertyId As String = Me.getPropertyId()
        totalRecords = objPropertyBl.propertyAppliancesList(resultDataSet, Me.getPageSortBo(), propertyId, itemId)
        SessionManager.setApplianceDetailDataSet(resultDataSet)
        Me.grdAppliances.VirtualItemCount = totalRecords
        Me.grdAppliances.DataSource = resultDataSet
        Me.grdAppliances.DataBind()
    End Sub
#End Region

#Region "sort Appliances Grid "
    Protected Sub sortAppliancesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppliancesGrid()
        resultDataSet = SessionManager.getApplianceDetailDataSet()
        Dim applianceDataTable As DataTable = New DataTable()
        applianceDataTable = resultDataSet.Tables(0)
        If IsNothing(applianceDataTable) = False Then
            Dim dvSortedView As DataView = New DataView(applianceDataTable)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppliances.DataSource = dvSortedView
            grdAppliances.DataBind()

        End If
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState.Item(ViewStateConstants.PageSortBo), PageSortBO)
    End Function
#End Region

#Region "set Property Id "
    Private Sub setPropertyId(ByVal propertyId As String)
        SessionManager.setPropertyIdForAppliance(propertyId)
    End Sub
#End Region

#Region "get Property Id"
    Private Function getPropertyId() As String
        Return SessionManager.getPropertyIdForAppliance()
    End Function
#End Region

#Region "Save Appliance"
    Protected Function saveAppliance() As Integer
        Dim objApplianceBo As ApplianceBO = New ApplianceBO()
        If ViewState(ViewStateConstants.ApplianceId) > 0 Then
            objApplianceBo.ExistingApplianceId = ViewState(ViewStateConstants.ApplianceId)
        End If

        If hdnSelectedLocationId.Value <> "" AndAlso hdnSelectedLocationId.Value > 0 Then
            objApplianceBo.LocationId = CType(hdnSelectedLocationId.Value, Integer)
        Else
            objApplianceBo.LocationId = -1
        End If
        objApplianceBo.Location = txtLocation.Text
        If hdnSelectedTypeId.Value <> "" AndAlso hdnSelectedTypeId.Value > 0 Then
            objApplianceBo.TypeId = CType(hdnSelectedTypeId.Value, Integer)
        Else
            objApplianceBo.TypeId = -1
        End If
        objApplianceBo.Type = txtType.Text.Trim()

        If hdnSelectedmakeId.Value <> "" AndAlso hdnSelectedmakeId.Value > 0 Then
            objApplianceBo.MakeId = CType(hdnSelectedmakeId.Value, Integer)
        Else
            objApplianceBo.MakeId = -1
        End If
        objApplianceBo.Make = txtMake.Text.Trim
        If hdnSelectedModelId.Value <> "" AndAlso hdnSelectedModelId.Value > 0 Then
            objApplianceBo.ModelId = CType(hdnSelectedModelId.Value, Integer)
        Else
            objApplianceBo.ModelId = -1
        End If
        objApplianceBo.Model = txtModel.Text.Trim
        objApplianceBo.Item = txtItem.Text.Trim()
        objApplianceBo.ItemId = SessionManager.getTreeItemId()
        objApplianceBo.Quantity = txtQuantity.Text.Trim()
        objApplianceBo.Dimensions = txtDimensions.Text.Trim()
        If txtPurchaseCost.Text <> "" Then
            objApplianceBo.PurchaseCost = Convert.ToDecimal(txtPurchaseCost.Text.Trim())
        End If
        If ddlLifeSpan.SelectedValue > 0 Then
            objApplianceBo.LifeSpan = ddlLifeSpan.SelectedValue
        End If

        'objApplianceBo.Datepurchased = txtDatePurchased.Text.Trim()
        objApplianceBo.PropertyId = Request.QueryString(PathConstants.PropertyIds)
        objApplianceBo.SerialNumber = txtSerialNumber.Text
        If String.IsNullOrEmpty(txtDatePurchased.Text.ToString) Then
            objApplianceBo.Datepurchased = Date.MinValue
        Else
            objApplianceBo.Datepurchased = CType(txtDatePurchased.Text, Date)
        End If


        If String.IsNullOrEmpty(txtRemovedDate.Text.ToString()) Then
            objApplianceBo.DateRemoved = Date.MinValue
        Else
            objApplianceBo.DateRemoved = CType(txtRemovedDate.Text, Date)
        End If


        objApplianceBo.Notes = txtNotes.Text.Trim()

        Return objPropertyBl.savePropertyAppliance(objApplianceBo)
    End Function
#End Region

#Region "Delete Appliance"
    Protected Function deleteAppliance() As Boolean
        Dim applianceId As Integer = ViewState(ViewStateConstants.ApplianceId)
        Return objPropertyBl.deletePropertyAppliance(applianceId)
    End Function
#End Region

#Region "Reset Add Applaince Controls"
    Protected Sub resetAddApplainceControls()
        hdnSelectedLocationId.Value = -1
        hdnSelectedmakeId.Value = -1
        hdnSelectedModelId.Value = -1
        hdnSelectedTypeId.Value = -1
        txtDatePurchased.Text = String.Empty
        txtDimensions.Text = String.Empty
        txtItem.Text = String.Empty
        txtLocation.Text = String.Empty
        txtMake.Text = String.Empty
        txtModel.Text = String.Empty
        txtNotes.Text = String.Empty
        txtPurchaseCost.Text = String.Empty
        txtQuantity.Text = String.Empty
        txtSerialNumber.Text = String.Empty
        txtType.Text = String.Empty
        ddlLifeSpan.SelectedIndex = 0
        ViewState(ViewStateConstants.ApplianceId) = 0
    End Sub
#End Region


#Region "LifeSpan Lookup Values"

    Private Sub getLifeSpanLookUpValues(ByRef ddlname As DropDownList)

        Dim lstLookUp As New List(Of LookUpBO)
        For i As Integer = 1 To 100
            Dim objLookUpBO As LookUpBO = New LookUpBO(i, i.ToString())
            lstLookUp.Add(objLookUpBO)
        Next i
        PopulateLookup(ddlname, lstLookUp)
    End Sub
#End Region

#Region "Populate Lookup"

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values from given lookuplist.
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="lstLookup">LookUpList: LookUpList to Bind with dropdown list</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with lookup values in lookup list.</remarks>
    ''' 
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
            ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))
            ddlLookup.SelectedIndex = 0
        End If
    End Sub

    'End Region "Populate Lookup"
#End Region
#End Region
End Class