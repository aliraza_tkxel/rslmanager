﻿Imports System
Imports AS_BusinessLogic
Imports AS_BusinessObject
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class ServiceDefects
    Inherits UserControlBase

    Dim objPropertyBL As PropertyBL = New PropertyBL()

#Region "User Contraol Events"

    Public Event viewDefect_Clicked(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Properties"

    Public PropertyDefectId As Integer = 0

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

    Protected Sub View_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.Message = ""
            Dim objApplianceDefectBO As ApplianceDefectBO = New ApplianceDefectBO()



            Dim objAppliance As Appliance = New Appliance()
            Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
            Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim btnView As Button = CType((grdServiceDefects.Rows(index).FindControl("btnView")), Button)
            propertyDefectId = CType(btnView.CommandArgument, Integer)
            objPropertyBL.getPropertyDefectDetails(propertyDefectId, objApplianceDefectBO)

            'Dim btnView As Button = CType(sender, Button)
            'PropertyDefectId = CType(btnView.CommandArgument, Integer)
            'RaiseEvent viewDefect_Clicked(sender, e)

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.Message = ""

            ' objPropertyBL.getPropertyDefectDetails(propertyDefectId, objApplianceDefectBO)

            'uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'uiMessageHelper.Message = ""
            'Dim objApplianceDefectBO As ApplianceDefectBO = New ApplianceDefectBO()
            ' Dim objAppliance As Appliance = New Appliance()
            ' Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
            ' Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
            ' Dim index As Integer = gvr.RowIndex
            'Dim btnView As Button = CType((grdServiceDefects.Rows(index).FindControl("btnView")), Button)
            'Dim propertyDefectId As Integer = CType(btnView.CommandArgument, Integer)
            'objPropertyBL.getPropertyDefectDetails(PropertyDefectId, objApplianceDefectBO)
            Dim ucDefectManagement As DefectManagement = TryCast(Parent.FindControl("ucDefectManagement"), DefectManagement)
            ucDefectManagement.viewDefectDetails(PropertyDefectId)
            Dim mdlPoPUpAddDefect As AjaxControlToolkit.ModalPopupExtender = TryCast(Parent.FindControl("mdlPoPUpAddDefect"), AjaxControlToolkit.ModalPopupExtender)
            mdlPoPUpAddDefect.Show()
            lblDate.Text = objApplianceDefectBO.DefectDate
            lblCategory.Text = getCategoryName(objApplianceDefectBO.CategoryId)
            lblAppliances.Text = getApplianceName(propertyId, objApplianceDefectBO.ApplianceId)
            If objApplianceDefectBO.IsRemedialActionTaken = True Then
                lblActionTaken.Text = "Yes"
            Else
                lblActionTaken.Text = "No"
            End If

            lblDate.Text = objApplianceDefectBO.DefectDate
            lblCategory.Text = getCategoryName(objApplianceDefectBO.CategoryId)
            'lblAppliances.Text = getApplianceName(propertyId, objApplianceDefectBO.ApplianceId)
            If objApplianceDefectBO.IsRemedialActionTaken = True Then
                lblActionTaken.Text = "Yes"
            Else
                lblActionTaken.Text = "No"
            End If

            ' If objApplianceDefectBO.DefectIdentified = True Then
            'lDefectIdentified.Text = "Yes"
            ' Else
            ' lblDefectIdentified.Text = "No"
            ' End If
            If objApplianceDefectBO.IsWarningNoteIssued = True Then
                lblWarningIssued.Text = "Yes"
            Else
                lblWarningIssued.Text = "No"
            End If



            'If objApplianceDefectBO.DefectIdentified = True Then
            '    lblDefectIdentified.Text = "Yes"
            'Else
            '    lblDefectIdentified.Text = "No"
            'End If
            If objApplianceDefectBO.IsWarningNoteIssued = True Then
                lblWarningIssued.Text = "Yes"
            Else
                lblWarningIssued.Text = "No"
            End If
            If objApplianceDefectBO.IsWarningTagFixed = True Then
                lblWarningTag.Text = "Yes"
            Else
                lblWarningTag.Text = "No"
            End If
            lblSerialNumber.Text = objApplianceDefectBO.SerialNumber


            If objApplianceDefectBO.IsWarningTagFixed = True Then
                lblWarningTag.Text = "Yes"
            Else
                lblWarningTag.Text = "No"
            End If
            lblSerialNumber.Text = objApplianceDefectBO.SerialNumber


            lblActionNotes.Text = Server.UrlDecode(objApplianceDefectBO.RemedialActionNotes)
            lblDefectNotes.Text = Server.UrlDecode(objApplianceDefectBO.DefectIdentifiedNotes)
            lblPhotoNotes.Text = Server.UrlDecode(objApplianceDefectBO.PhotosNotes)
            mdlPoPUpDefectDetails.Show()

           
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#Region "load Category DDL"

    Public Function getCategoryName(ByVal categoryId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resdatatable As DataTable = New DataTable()
        Dim categoryName As String = String.Empty
        Try
            objPropertyBL.loadCategoryDDL(resultDataSet)

            resdatatable = resultDataSet.Tables(0)

            'query the data set using this will return the data row
            Dim customerQuery = (From dataRow In resdatatable _
                Where _
                    dataRow.Field(Of Integer)("CategoryId") = categoryId _
                Select dataRow).ToList()

            Dim a As Int32 = 0

            'Query the data table
            Dim datatable As DataTable = customerQuery.CopyToDataTable()

            categoryName = CType(IIf(IsDBNull(datatable.Rows(0).Item(1)) = True, "", datatable.Rows(0).Item(1)), String)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        Return categoryName
    End Function
#End Region

#Region "get Property Name"
    Public Function getApplianceName(ByVal propertyId As String, ByVal applianceId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resdatatable As DataTable = New DataTable()
        Dim applianceName As String = String.Empty
        Try
            objPropertyBL.loadPropertyAppliancesDDL(propertyId, resultDataSet)

            resdatatable = resultDataSet.Tables(0)
            'query the data set using this will return the data row
            Dim customerQuery = (From dataRow In resdatatable Where dataRow.Item("PROPERTYAPPLIANCEID") = applianceId Select dataRow).ToList()

            If customerQuery.Count > 0 Then
                applianceName = CType(IIf(IsDBNull(customerQuery.First().Item(1)) = True, "", customerQuery.First().Item(1)), String)
            End If



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        Return applianceName
    End Function
#End Region

#Region "get Defect Notes"
    Public Function checkNotesExists(ByVal DefectNotes As Boolean)

        If DefectNotes = True Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function

#End Region

#Region "Grid Service Defects Row data bound event"
    Protected Sub grdServiceDefects_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdServiceDefects.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btnView = CType(e.Row.FindControl("btnView"), Button)
            Dim hdnDefectType = CType(e.Row.FindControl("hdnDefectType"), HiddenField)
            'If hdnDefectType.Value.Equals(ApplicationConstants.BoilerDefectType) Then
            '    btnView.Visible = False
            'End If
        End If
    End Sub
#End Region



End Class