﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class TargetRentTab
    Inherits UserControlBase
    Dim _readOnly As Boolean = False
    Dim objPropertyBl As PropertyBL = New PropertyBL()

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnSaveTargetRent.Visible = False
            pnlTargentRentTab.Enabled = False
        End If
        If Not IsPostBack Then

        End If
    End Sub
    Protected Sub btnSaveTargetRent_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveTargetRent.Click
        Me.saveTargetRentInformation()
    End Sub

#End Region

  #Region "Get Property Rent Type and Financial Information"
    Sub populateRentTypeAndFinancialInformation(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getTargetRentDetail(resultDataSet, propertyId)
        'Populating Rent Type Drop down List
        ddlTargetRentType.DataSource = resultDataSet.Tables("RentType")
        ddlTargetRentType.DataTextField = "DESCRIPTION"
        ddlTargetRentType.DataValueField = "TENANCYTYPEID"
        ddlTargetRentType.DataBind()

        'Populating Financial Infomraiton
        If Not IsNothing(propertyId) And resultDataSet.Tables("RentFinancial").Rows.Count > 0 Then
            txtTargetRent.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("RENT")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("RENT")), "0.00")
            txtTargetServices.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("SERVICES")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SERVICES")), "0.00")
            txtTargetCouncilTax.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("COUNCILTAX")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("COUNCILTAX")), "0.00")
            txtTargetWater.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("WATERRATES")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("WATERRATES")), "0.00")
            txtTargetIntelig.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("INELIGSERV")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("INELIGSERV")), "0.00")
            txtTargetSupp.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("SUPPORTEDSERVICES")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("SUPPORTEDSERVICES")), "0.00")
            txtTargetGarage.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("GARAGE")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("GARAGE")), "0.00")
            txtTargetTotalRent.Text = If(Not IsDBNull(resultDataSet.Tables("RentFinancial").Rows(0)("TOTALRENT")), String.Format("{0:F2}", resultDataSet.Tables("RentFinancial").Rows(0)("TOTALRENT")), "0.00")

        End If



    End Sub
#Region "Save Target Rent Information"
    Sub saveTargetRentInformation()
        Try
            Dim objPropertyBl As PropertyBL = New PropertyBL()
            Dim objPropertyCurrentRentBO As New PropertyRentBO()
            objPropertyCurrentRentBO.RentType = ddlTargetRentType.SelectedItem.Value
            objPropertyCurrentRentBO.Rent = txtTargetRent.Text
            objPropertyCurrentRentBO.Services = txtTargetServices.Text
            objPropertyCurrentRentBO.CouncilTax = txtTargetCouncilTax.Text
            objPropertyCurrentRentBO.Inelig = txtTargetIntelig.Text
            objPropertyCurrentRentBO.Water = txtTargetWater.Text
            objPropertyCurrentRentBO.Supp = txtTargetSupp.Text
            objPropertyCurrentRentBO.Garage = txtTargetGarage.Text
            objPropertyCurrentRentBO.TotalRent = txtTargetTotalRent.Text
            objPropertyCurrentRentBO.PropertyId = SessionManager.getPropertyId()
            objPropertyCurrentRentBO.UserId = SessionManager.getAppServicingUserId() ' When go live
            'objPropertyCurrentRentBO.UserId = 605 'For testing
            objPropertyBl.savePropertyTargetRentInformation(objPropertyCurrentRentBO)
            uiMessageHelper.setMessage(lblTargetRentTab, pnlTargentRentTab, UserMessageConstants.SuccessMessage, False)
            'pnlCurrentRentMessage.Visible = True
            'lblCurrentRentTab.Text = "Information has been added successfully"
            'lblCurrentRentTab.ForeColor = Drawing.Color.DarkGreen
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblTargetRentTab, pnlTargentRentTab, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#End Region

#End Region

    
End Class