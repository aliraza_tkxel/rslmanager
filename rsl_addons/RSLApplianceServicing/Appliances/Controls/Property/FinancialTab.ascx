﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FinancialTab.ascx.vb"
    Inherits="Appliances.FinancialTab" %>
<%@ Register TagPrefix="ucRentTab" TagName="RentTab" Src="RentTabs.ascx" %>
<%@ Register TagPrefix="ucValuationTab" TagName="ValuationTab" Src="ValuationTabs.ascx" %>
<style type="text/css">
    .pm-middlebox
    {
        border: 1px solid #000;
        float: left;
        margin-top: 5px;
        margin-left: 2px;
        padding: 10px;
        width: 98%;
    }
</style>
<asp:Panel ID="pnlRentMessage" runat="server" Visible="false">
    <asp:Label ID="lblRentMessage" runat="server"></asp:Label>
</asp:Panel>
<!--Upload form starts here -->
<div style="min-width: 100%;">
    <div style="float: left; width: 100%; margin-left: 2px;">
        <asp:LinkButton ID="lnkBtnRentTab" CssClass="TabInitial" runat="server" BorderStyle="Solid">Rent: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnValuationTab" CssClass="TabInitial" runat="server" BorderStyle="Solid">Value: </asp:LinkButton>
        <br />
    </div>
    <asp:MultiView ID="MainSubView" runat="server">
        <asp:View ID="SubView1" runat="server">
            <div class="pm-middlebox">
                <ucRentTab:RentTab ID="RentTab" runat="server" />
            </div>
        </asp:View>
        <asp:View ID="SubView2" runat="server">
            <div class="pm-middlebox">
                <ucValuationTab:ValuationTab ID="ValuationTab" runat="server" />
            </div>
        </asp:View>
    </asp:MultiView>
</div>
