﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AnnualRentTab.ascx.vb"
    Inherits="Appliances.AnnualRentTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style3
    {
        width: 400px;
    }
    .style7
    {
        width: 282px;
    }
    .style8
    {
        width: 373px;
    }
</style>
<asp:UpdatePanel ID="upPnlAnnualRentTab" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlAnnualRentTab" runat="server" BackColor="White" Width="400px">
            <table width="400px">
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <div style='margin-left: 15px;'>
                            <asp:Panel ID="pnlAnnualRentMsg" runat="server" Visible="false" Height="19px" Width="529px">
                                <asp:Label ID="lblAnnualRentTab" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style8">
                        File Uploaded:
                    </td>
                    <td align="left" class="style3" valign="top" colspan="3">
                        <asp:TextBox ID="txtFileUploaded" runat="server" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style8" valign="top">
                        Date Rent Set:<span class="Required"></span>
                    </td>
                    <td align="left" class="style3" colspan="3" valign="top">
                        <asp:TextBox ID="txtAnnualRentDate" runat="server" Width="150px"></asp:TextBox>
                      
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style8">
                        Rent effective to:<span class="Required"></span>
                    </td>
                    <td align="left" valign="top" class="style3" colspan="3">
                        <asp:TextBox ID="txtAnnualEffectiveDate" runat="server" Width="150px"></asp:TextBox>
                      
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style8" valign="top">
                        &nbsp;Rent:
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtAnnualRent" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator3" ControlToValidate="txtAnnualRent"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualRent"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" valign="top">
                        &nbsp;Services
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtAnnualServices" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator4" ControlToValidate="txtAnnualServices"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualServices"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style8" valign="top">
                        Council Tax
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtAnnualCouncilTax" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator5" ControlToValidate="txtAnnualCouncilTax"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualCouncilTax"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        Water
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtAnnualWater" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator6" ControlToValidate="txtAnnualWater"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualWater"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style8" valign="top">
                        Inelig.
                    </td>
                    <td align="right" valign="top">
                        <asp:TextBox ID="txtAnnualIntelig" runat="server" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator7" ControlToValidate="txtAnnualIntelig"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualIntelig"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        Supp.
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtAnnualSupp" runat="server" Height="22px" Width="50px">0.00</asp:TextBox>
                        <asp:CompareValidator runat="server" ID="CompareValidator8" ControlToValidate="txtAnnualSupp"
                            Operator="DataTypeCheck" Type="Double" ErrorMessage="only digits allowed" Style="float: right;"
                            CssClass="Required" Display="Dynamic" ValidationGroup="validate">
                        </asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                            Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualSupp"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                    </td>
                </tr>
                <tr bgcolor="#133E71">
                    <td align="left" class="style8" valign="top">
                        <strong style="color: #fff">Total Rent</strong>
                        <td align="left" valign="top" colspan="2">
                            <asp:TextBox ID="txtAnnualTotalRent" runat="server" Width="50px">0.00</asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="<br/>Please enter valid decimal."
                                Display="Dynamic" ValidationGroup="validate" ForeColor="Red" ControlToValidate="txtAnnualTotalRent"
                                ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,2})?$" />
                        </td>
                        <td align="right" class="style3" style="text-align: right;" valign="top">
                            &nbsp;
                        </td>
                </tr>
                <tr>
                    <td align="left" class="style8" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" class="style3" colspan="2" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" class="style3" style="text-align: right;" valign="top">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
