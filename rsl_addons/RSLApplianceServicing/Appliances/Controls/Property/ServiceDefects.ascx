﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ServiceDefects.ascx.vb"
    Inherits="Appliances.ServiceDefects" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 2%;
    }
    .style2
    {
        width: 120px;
    }
</style>
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel runat="server" ID="updPnlServiceDefects">
    <ContentTemplate>
        <div style="overflow: auto; height: 200px;">
            <asp:GridView ID="grdServiceDefects" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                OnRowDataBound="grdServiceDefects_RowDataBound" Width="100%" BackColor="White"
                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="4" ForeColor="Black" BorderStyle="Solid">
                <Columns>
                    <asp:TemplateField HeaderText="Date:">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category:">
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle Width="20%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Defect Identified:">
                        <ItemTemplate>
                            <asp:Label ID="lblIdentified" runat="server" Text='<%# checkNotesExists(Eval("IsDefectIdentified")).ToString() %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remedial Action:">
                        <ItemTemplate>
                            <asp:Label ID="lblAction" runat="server" Text='<%# checkNotesExists(Eval("IsActionTaken")).ToString() %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Defect Notes:">
                        <ItemTemplate>
                            <asp:Label ID="lblWarning" runat="server" Text='<%# Server.UrlDecode(Eval("DefectNotes")) %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Warning Tag/Sticker:">
                        <ItemTemplate>
                            <asp:Label ID="lblTag" runat="server" Text='<%# checkNotesExists(Eval("IsWarningFixed")).ToString() %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnView" runat="server" CommandArgument='<%# Eval("PropertyDefectId") %>'
                                Text="View" OnClick="View_Click" BackColor="White" />
                            <asp:HiddenField ID="hdnDefectType" Value='<%# Eval("DefectType") %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<hr />
<asp:Panel runat="server" ID="pnlDefectDetails" Style="height: 300px; width: 400px;
    background-color: White; border: 1px solid gray; padding: 10px; top: 0px;">
    <div style="background-color: #E8E9EA; color: Black; border: solid 1px gray; height: 30px;
        padding: center; text-align: left;">
        <table id="tbl" style="width: 100%">
            <tr>
                <th style="font-size: 13px; font-weight: bold; width: 80%;">
                    Fault Details
                </th>
            </tr>
        </table>
    </div>
    <asp:ImageButton ID="imgbtnCancelDetailsDefect" runat="server" ImageAlign="Right"
        ImageUrl="~/Images/cross2.png" Style="position: absolute; top: -10px; right: -10px"
        BorderWidth="0" />
    <br />
    <div>
        <table id="Table1" style="margin-left: 10px; height: 11px; width: 80%;">
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCategory"></asp:Label>
                </td>
            </tr>
            <tr style="background-color: #E8E9EA">
                <td>
                    Date:
                </td>
                <td>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle;">
                    Defect Identified:&nbsp;
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDefectIdentified"></asp:Label>
                </td>
            </tr>
            <tr style="background-color: #E8E9EA;">
                <td style="vertical-align: middle;">
                    Defect Notes:&nbsp;
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDefectNotes"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Remedial action taken:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblActionTaken"></asp:Label>
                </td>
            </tr>
            <tr style="background-color: #E8E9EA;">
                <td style="vertical-align: middle;">
                    Action Notes:&nbsp;
                </td>
                <td>
                    <asp:Label runat="server" ID="lblActionNotes"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Warning/advice note issued:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblWarningIssued"></asp:Label>
                </td>
            </tr>
            <tr style="background-color: #E8E9EA;">
                <td>
                    Appliance:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAppliances"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Serial Number:
                </td>
                <td>
                    <asp:Label ID="lblSerialNumber" runat="server" Style="margin-right: 83px; width: 139px;"></asp:Label>
                </td>
            </tr>
            <tr style="background-color: #E8E9EA;">
                <td>
                    Warning Tag/sticker fixed:&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                    <asp:Label runat="server" ID="lblWarningTag"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle;">
                    Photo Notes:&nbsp;
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPhotoNotes"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPoPUpDefectDetails" runat="server" TargetControlID="lblDispDefect"
    PopupControlID="pnlDefectDetails" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
    CancelControlID="imgbtnCancelDetailsDefect">
</asp:ModalPopupExtender>
<%--<ajaxToolkit:ModalPopupExtender ID="mdlPoPUpDefectDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispDefect" PopupControlID="pnlDefectDetails"
    DropShadow="true" CancelControlID="imgbtnCancelDetailsDefect" >
</ajaxToolkit:ModalPopupExtender>

--%>
<asp:Label ID="lblDispDefect" runat="server"></asp:Label>
