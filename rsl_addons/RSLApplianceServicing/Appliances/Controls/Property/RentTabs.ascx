﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RentTabs.ascx.vb" Inherits="Appliances.RentTabs" %>
<%@ Register TagPrefix="ucCurrentRentTab" TagName="CurrentRentTab" Src="CurrentRentTab.ascx" %>
<%@ Register TagPrefix="ucTargetRentTab" TagName="TargetRentTab" Src="TargetRentTab.ascx" %>
<%@ Register TagPrefix="ucAnnualRentTab" TagName="AnnualRentTab" Src="AnnualRentTab.ascx" %>
<%@ Register TagPrefix="ucRentHistory" TagName="RentHistory" Src="~/Controls/Property/CurrentRentHistoryTab.ascx" %>
<asp:Panel ID="pnlRentMessage" runat="server" Visible="false">
    <asp:Label ID="lblRentMessage" runat="server"></asp:Label>
</asp:Panel>
<div style="min-width: 100%;">
    
    <div style="float: left; width: 100%; margin-left: 2px;">
        <asp:LinkButton ID="lnkBtnCurrentRentTab" OnClick="lnkBtnCurrentRentTab_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Current: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnTargetRentTab" OnClick="lnkBtnTargetRentTab_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Target: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnAnnualRentTab" OnClick="lnkBtnAnnualRentTab_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Annual Rent: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnHistory" OnClick="lnkBtnHistory_Click" CssClass="TabInitial"
            runat="server" BorderStyle="Solid">Rent History: </asp:LinkButton>
        <br />
    </div>
    <asp:MultiView ID="MainSubView" runat="server">
        <asp:View ID="SubView1" runat="server">
            <div class="pm-lbox">
                <ucCurrentRentTab:CurrentRentTab ID="CurrentRentTab" runat="server" />
            </div>
        </asp:View>
        <asp:View ID="SubView2" runat="server">
            <div class="pm-lbox">
                <ucTargetRentTab:TargetRentTab ID="TargetRentTab" runat="server" />
            </div>
        </asp:View>
        <asp:View ID="SubView3" runat="server">
            <div class="pm-lbox">
                <ucAnnualRentTab:AnnualRentTab ID="AnnualRentTab" runat="server" />
            </div>
        </asp:View>
        <asp:View runat="server" ID="view4">
            <div class="pm-lbox" style=" width:650px; ">
                <ucRentHistory:RentHistory runat="server" ID="RentHistory" />
            </div>
        </asp:View>
    </asp:MultiView>
</div>