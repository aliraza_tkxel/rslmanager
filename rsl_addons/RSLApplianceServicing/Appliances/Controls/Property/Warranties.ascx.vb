﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.IO
Imports System.Linq
Imports System.Threading

Public Class Warranties
    Inherits UserControlBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim propertyId As String = Request.QueryString("id")
                bindToGrid(propertyId)
                populateDropdowns()
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Public Sub bindToGrid(ByVal propertyId As String)
        Try

            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim resultDataSet As DataSet = New DataSet()
            Dim objPageSortBO As PageSortBO = getPageSortBo()
            If (objPageSortBO Is Nothing) Then
                objPageSortBO = New PageSortBO("DESC", "WARRANTYTYPE", 1, 30)
                setPageSortBo(objPageSortBO)
            End If
            objPropertyBL.getWarrantiesList(resultDataSet, propertyId, objPageSortBO)
            grdWarranties.DataSource = resultDataSet
            grdWarranties.DataBind()


        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
    Public Sub populateDropdowns()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        Dim warrantyTypeDataSet As DataSet = New DataSet()
        objPropertyBL.getWarrantyTypes(warrantyTypeDataSet)
        If (warrantyTypeDataSet.Tables(0).Rows.Count > 0) Then
            ddlWarrantyType.DataSource = warrantyTypeDataSet
            ddlWarrantyType.DataValueField = "WARRANTYTYPEID"
            ddlWarrantyType.DataTextField = "DESCRIPTION"
            ddlWarrantyType.DataBind()
            ddlWarrantyType.Items.Insert(0, New ListItem("Select Warranty", -1))
        End If
        Dim categoryDataSet As DataSet = New DataSet
        objPropertyBL.getcategoryDropdownValue(categoryDataSet)
        If (categoryDataSet.Tables(0).Rows.Count > 0) Then
            ddlCategory.DataSource = categoryDataSet
            ddlCategory.DataValueField = "LocationID"
            ddlCategory.DataTextField = "LocationName"
            ddlCategory.DataBind()
        End If
        ddlCategory.Items.Insert(0, New ListItem("Select Category", -1))
        
        ddlArea.Items.Clear()
        ddlArea.Items.Insert(0, New ListItem("Select Area", "-1"))
        ddlItem.Items.Clear()
        ddlItem.Items.Insert(0, New ListItem("Select Item", "-1"))
        'End If
        Dim contractorDataSet As DataSet = New DataSet
        objPropertyBL.getContractors(contractorDataSet)
        If (contractorDataSet.Tables(0).Rows.Count > 0) Then
            ddlContractor.DataSource = contractorDataSet
            ddlContractor.DataValueField = "ORGID"
            ddlContractor.DataTextField = "contractor"
            ddlContractor.DataBind()
            ddlContractor.Items.Insert(0, New ListItem("Select Contractor", -1))
        End If
    End Sub

    Protected Sub btnAmend_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            uiMessageHelper.resetMessage(lblMessagePopup, pnlMessagePopup)
            populateDropdowns()
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim warrantyDataSet As DataSet = New DataSet()
            Dim amend As Button = CType(sender, Button)
            Dim warranty As Integer = Convert.ToInt32(amend.CommandArgument)
            objPropertyBL.getWarrantyData(warrantyDataSet, warranty)
            ddlContractor.SelectedValue = warrantyDataSet.Tables(0).Rows(0)("CONTRACTOR").ToString
            ddlWarrantyType.SelectedValue = warrantyDataSet.Tables(0).Rows(0)("WARRANTYTYPE").ToString
            ddlCategory.SelectedValue = warrantyDataSet.Tables(0).Rows(0)("Category").ToString
            categoryIndex_changed(ddlCategory, Nothing)
            ddlArea.SelectedValue = warrantyDataSet.Tables(0).Rows(0)("Area").ToString
            areaIndex_changed(ddlArea, Nothing)
            ddlItem.SelectedValue = warrantyDataSet.Tables(0).Rows(0)("Item").ToString
            txtExpiry.Text = warrantyDataSet.Tables(0).Rows(0)("EXPIRYDATE").ToString
            txtNotes.Text = warrantyDataSet.Tables(0).Rows(0)("NOTES").ToString
            btnAdd.CommandArgument = amend.CommandArgument
            'updPnlPopUp.Update();
            mdlAddWarrantyPopup.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub imgBtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim imgBtnDelete As ImageButton = CType(sender, ImageButton)
            Dim warranty As Integer = Convert.ToInt32(imgBtnDelete.CommandArgument)
            objPropertyBL.deleteWarranty(warranty)
            bindToGrid(Request.QueryString("id"))
            ' updPnlWarrantyListing.Update()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRest.Click
        Try
            resetControls()
            mdlAddWarrantyPopup.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            uiMessageHelper.resetMessage(lblMessagePopup, pnlMessagePopup)
            Page.Validate("save")
            If (Page.IsValid And validateData()) Then
                saveData()
                mdlAddWarrantyPopup.Hide()
                bindToGrid(Request.QueryString("id"))
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnAddNewWarranty_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddNewWarranty.Click
        Try

            resetControls()
            mdlAddWarrantyPopup.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Sub resetControls()
        btnAdd.CommandArgument = String.Empty
        ddlContractor.SelectedIndex = 0
        ddlCategory.SelectedIndex = 0
        ddlArea.SelectedIndex = 0
        ddlItem.SelectedIndex = 0
        ddlWarrantyType.SelectedIndex = 0
        txtExpiry.Text = String.Empty
        txtNotes.Text = String.Empty
        uiMessageHelper.resetMessage(lblMessagePopup, pnlMessagePopup)
    End Sub

    Sub saveData()
        Dim objWarrantyBO As WarrantyBO = New WarrantyBO()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        'Dim warrantyId As Integer = Convert.ToInt32(btnAdd.CommandArgument)
        If (Not String.IsNullOrEmpty(btnAdd.CommandArgument)) Then
            objWarrantyBO.WarrantyId = Convert.ToInt32(btnAdd.CommandArgument)
        Else
            objWarrantyBO.WarrantyId = 0
        End If
        objWarrantyBO.WarrantyType = Convert.ToInt32(ddlWarrantyType.SelectedItem.Value)
        objWarrantyBO.Category = Convert.ToInt32(ddlCategory.SelectedItem.Value)
        If (ddlArea.SelectedItem.Value = -1) Then
            objWarrantyBO.Area = Nothing
        Else
            objWarrantyBO.Area = Convert.ToInt32(ddlArea.SelectedItem.Value)
        End If
        If (ddlItem.SelectedItem.Value = -1) Then
            objWarrantyBO.Item = Nothing
        Else
            objWarrantyBO.Item = Convert.ToInt32(ddlItem.SelectedItem.Value)
        End If
        objWarrantyBO.Contractor = Convert.ToInt32(ddlContractor.SelectedItem.Value)
        objWarrantyBO.Expiry = Convert.ToDateTime(txtExpiry.Text)
        objWarrantyBO.Notes = txtNotes.Text
        'get these value from query string and set it here
        objWarrantyBO.Id = Request.QueryString("id")
        objWarrantyBO.RequestType = "Property"
        'add warranty for Block and Scheme.
        objPropertyBL.saveWarranty(objWarrantyBO)
        btnAdd.CommandArgument = String.Empty
    End Sub

    Protected Sub grdWarranties_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdWarranties.Sorting

        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.SortExpression = e.SortExpression
        objPageSortBo.PageNumber = 1
        grdWarranties.PageIndex = 0
        objPageSortBo.setSortDirection()
        setPageSortBo(objPageSortBo)
        bindToGrid(Request.QueryString("id"))
    End Sub


#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

    
    Protected Sub categoryIndex_changed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ddlArea.Items.Clear()
            ddlItem.Items.Clear()
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim areaDataSet As DataSet = New DataSet
            objPropertyBL.getAreaDropdownValueByCategoryId(CInt(ddlCategory.SelectedItem.Value), areaDataSet)
            If (areaDataSet.Tables(0).Rows.Count > 0) Then
                ddlArea.DataSource = areaDataSet
                ddlArea.DataValueField = "AreaID"
                ddlArea.DataTextField = "AreaName"
                ddlArea.DataBind()

                ddlArea.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownAllValue))

            End If
            ddlArea.Items.Insert(0, New ListItem("Select Area", "-1"))
            ddlItem.Items.Insert(0, New ListItem("Select Item", "-1"))
            mdlAddWarrantyPopup.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub areaIndex_changed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ddlItem.Items.Clear()
            Dim objPropertyBL As PropertyBL = New PropertyBL()
            Dim itemDataSet As DataSet = New DataSet
            objPropertyBL.getItemDropdownValueByAreaId(CInt(ddlArea.SelectedItem.Value), itemDataSet)
            If (itemDataSet.Tables(0).Rows.Count > 0) Then
                ddlItem.DataSource = itemDataSet
                ddlItem.DataValueField = "ItemID"
                ddlItem.DataTextField = "ItemName"
                ddlItem.DataBind()

                ddlItem.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownAllValue))
            Else
                If (ddlArea.SelectedItem.Value.Equals(ApplicationConstants.DropDownAllValue)) Then
                    ddlItem.Items.Insert(0, New ListItem("All", ApplicationConstants.DropDownAllValue))
                    ddlItem.SelectedValue = ApplicationConstants.DropDownAllValue
                End If
            End If

            ddlItem.Items.Insert(0, New ListItem("Select Item", "-1"))
            mdlAddWarrantyPopup.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Function validateData() As Boolean
        Dim isValid As Boolean = True

        If ddlWarrantyType.SelectedItem.Text.Equals(ApplicationConstants.NHBCWarrantyText) Then

            Dim objBl As PropertyBL = New PropertyBL()
            Dim warrantyDataSet As DataSet = New DataSet()
            objBl.getNHBCWarranties(warrantyDataSet, Request.QueryString("id"))
            Dim warranties As DataTable = warrantyDataSet.Tables(0)

            If warranties.Rows.Count > 1 Then
                isValid = False
            ElseIf warranties.Rows.Count = 1 Then
                Dim dbWarrantyId As Integer = warranties.AsEnumerable().[Select](Function(w) w.Field(Of Integer)("WARRANTYID")).First()

                If String.IsNullOrEmpty(btnAdd.CommandArgument) Then
                    isValid = False
                Else
                    Dim warrantyId As Integer = Convert.ToInt32(btnAdd.CommandArgument)

                    If dbWarrantyId <> warrantyId Then
                        isValid = False
                    End If
                End If
            End If
        End If

        If Not isValid Then
            uiMessageHelper.setMessage(lblMessagePopup, pnlMessagePopup, UserMessageConstants.DuplicateNHBCError, True)
            mdlAddWarrantyPopup.Show()
        End If

        Return isValid
    End Function


End Class