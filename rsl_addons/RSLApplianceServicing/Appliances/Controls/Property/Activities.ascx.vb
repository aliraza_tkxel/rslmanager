﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports System.Net
Imports System.Net.Mail
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports AS_BusinessObject


Public Class Activities
    Inherits UserControlBase

#Region "Attributes"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objUsersBL As UsersBL = New UsersBL()
    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objResourceBL As ResourcesBL = New ResourcesBL()
    Dim objLetterBL As LettersBL = New LettersBL()
    Dim sortDirection As String
    Dim sortExpression As String
    Dim propertyId As String


#End Region

#Region "Events"


#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblActivityPopupMessage, pnlActivityPopupMessage)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ClientScriptName", "uploadComplete", True)
            AddHandler ucAppointmentNotes.CloseButtonClicked, AddressOf btnClose_Click
            'Dim objContainerPage As PropertyRecord = New PropertyRecord()

            If Not IsNothing(Request.QueryString(PathConstants.PropertyIds)) Then
                Me.propertyId = Request.QueryString(PathConstants.PropertyIds)
            Else
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.InvalidPropertyId, True)
            End If
            txtBoxDescription.Enabled = False
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Grid Activities Row Data bound"

    Protected Sub grdActivities_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdActivities.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim imgBtnPlannedJobsheet As ImageButton = DirectCast(e.Row.FindControl("imgBtnPlannedJobsheet"), ImageButton)
                Dim imgBtnFaultContractorJobsheet As ImageButton = DirectCast(e.Row.FindControl("imgBtnFaultContractorJobsheet"), ImageButton)
                Dim imgBtnAssignedToContractor As ImageButton = DirectCast(e.Row.FindControl("imgBtnAssignedToContractor"), ImageButton)
                'Dim imgBtnOpenCamera As ImageButton = DirectCast(e.Row.FindControl("imgBtnPlannedJobsheet"), ImageButton)
                Dim lblInspectionType As Label = DirectCast(e.Row.FindControl("lblInspectionType"), Label)
                Dim lblInspectionTypeDescription As Label = DirectCast(e.Row.FindControl("lblInspectionTypeDescription"), Label)
                Dim linkbtnDefectDetail As LinkButton = DirectCast(e.Row.FindControl("linkbtnDefectDetail"), LinkButton)
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)

                Dim inspectionType As String = lblInspectionType.Text.Split(" ")(0)

                If (inspectionType.Equals(ApplicationConstants.PlannedActiontype) _
                    AndAlso Not lblInspectionTypeDescription.Text.ToString.Equals(ApplicationConstants.AssignedToContractorStatus) _
                    AndAlso Not lblStatus.Text.ToString.StartsWith(ApplicationConstants.InspectionArrangedStatus) _
                    AndAlso Not ApplicationConstants.ToBeArrangedStatus.Equals(lblStatus.Text.ToString()) _
                    AndAlso Not ApplicationConstants.ConditionToBeArrangedStatus.Equals(lblStatus.Text.ToString())) Then
                    imgBtnPlannedJobsheet.Visible = True
                End If

                If (inspectionType.Equals(ApplicationConstants.FaultActiontype) _
                  AndAlso lblStatus.Text.ToString.StartsWith(ApplicationConstants.AssignedToContractorStatus)) Then
                    imgBtnFaultContractorJobsheet.Visible = True
                End If

                If (inspectionType.Equals(ApplicationConstants.PlannedActiontype)) Then

                    If (lblInspectionTypeDescription.Text.ToString.Equals(ApplicationConstants.AssignedToContractorStatus)) Then
                        If lblStatus.Text.ToString.StartsWith(ApplicationConstants.AssignedToContractorStatus) Then
                            imgBtnAssignedToContractor.Visible = True
                        End If

                        If lblStatus.Text.ToString.StartsWith(ApplicationConstants.ToBeArrangedStatus) Then
                            imgBtnAssignedToContractor.Visible = False
                        End If

                        If lblStatus.Text.ToString.StartsWith(ApplicationConstants.ApplianceCancelledStatus) Then
                            imgBtnAssignedToContractor.Visible = True
                        End If
                        If lblStatus.Text.ToString.StartsWith(ApplicationConstants.ApplianceCompletedStatus) Then
                            imgBtnAssignedToContractor.Visible = True
                        End If
                    End If
                End If

                If (lblInspectionType.Text.Equals(ApplicationConstants.ApplianceDefect)) Then
                    If (Not IsNothing(linkbtnDefectDetail)) Then
                        linkbtnDefectDetail.Visible = True
                        lblInspectionType.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Assigned To Contractor"

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow - 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            setjobsheetsummary()
            mdlPopupAssignedToContractorJobsheet.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            Dim currentRow As Integer

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            currentRow = Convert.ToInt32(ViewStateConstants.CurrentIndex)
            currentRow = currentRow + 1
            ViewStateConstants.CurrentIndex = currentRow.ToString()
            SetJobSheetSummary()
            mdlPopupAssignedToContractorJobsheet.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
    
    Protected Sub imgAssignedToContractor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtnassignedtoContractorJobsheet As ImageButton = DirectCast(sender, ImageButton)
            Dim jsn As String = String.Empty
            jsn = imgBtnassignedtoContractorJobsheet.CommandArgument.ToString()
            ViewStateConstants.CurrentIndex = "1"

            Me.resetJobSheetControls()
            Me.populateJobSheetSummary(jsn)
            Me.setjobsheetsummary()
            Me.populatePlannedStatus()

            StatusDropDown.Visible = False
            Me.mdlPopupAssignedToContractorJobsheet.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub
    Private Sub populatePlannedStatus()
        Try
            Dim objJobSheetBL As New JobSheetBL()
            Dim dsPlannedStatus As New DataSet

            objJobSheetBL.getPlannedStatusLookUp(dsPlannedStatus)

            StatusDropDown.Items.Clear()
            StatusDropDown.DataSource = dsPlannedStatus
            StatusDropDown.DataValueField = "id"
            StatusDropDown.DataTextField = "val"
            StatusDropDown.DataBind()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub
    Private Sub setjobsheetsummary()
        Dim dsjobsheetsummary As New DataSet()
        Dim currentrow, totalrows As Integer

        dsjobsheetsummary = SessionManager.getJobSheetSummaryDs()

        currentrow = Convert.ToInt32(ViewStateConstants.CurrentIndex)

        If (dsjobsheetsummary.Tables.Count > 0 AndAlso dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            totalrows = dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count
            lblTotalSheets.Text = "page " + currentrow.ToString() + " of " + totalrows.ToString()

            If (currentrow = 1 And currentrow = totalrows) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = False
            ElseIf (currentrow = 1) Then
                btnPrevious.Enabled = False
                btnNext.Enabled = True
            ElseIf (currentrow = totalrows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False
            ElseIf (currentrow > 1 And currentrow < totalrows) Then
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
            resetvisibility()
            controlsvisibilitybytype(dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentrow - 1).Item("type").ToString())
            With dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(currentrow - 1)
                lblJSN.Text = .Item("appointmentid").ToString().Trim()
                lblPlannedWorkComponent.Text = .Item("componentname")
                lblTrade.Text = .Item("trade")
                lblStatus.Text = .Item("appointmentstatus")
                lblContractor.Text = .Item("contractor")
                lblContact.Text = .Item("contact")
                txtWorksRequired.Text = .Item("workrequired")

                lblPONo.Text = .Item("purchaseorderid")
                lblOrderedBy.Text = .Item("orderby")
                lblOrdered.Text = .Item("podate")

                lblBudget.Text = String.Format("{0:c}", .Item("budget"))
                lblEstimate.Text = String.Format("{0:c}", .Item("estimate"))
                lblNetCost.Text = String.Format("{0:c}", .Item("netcost"))
                lblVat.Text = String.Format("{0:c}", .Item("vat"))

                lblTotal.Text = String.Format("{0:c}", (.Item("netcost") + .Item("vat")))
                lblOrderTotal.Text = String.Format("{0:c}", (.Item("netcost") + .Item("vat")))

                lblScheme.Text = .Item("schemename")
                lblBlock.Text = .Item("pjblock")
                lblPostCode.Text = .Item("clientpostcode")
                lblHouseNumber.Text = .Item("housenumber") + ", " + .Item("houseaddress1") + ", " + .Item("houseaddress2") + ", " + .Item("houseaddress3")
                lblCustomer.Text = .Item("clientname")
                lblTelephone.Text = .Item("clienttel")
                lblMobile.Text = .Item("clientmobile")
                lblEmail.Text = .Item("clientemail")
            End With
        End If

        'bind asbestos grid with data from database for jsn
        If (dsjobsheetsummary.Tables.Count > 1 _
            AndAlso dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsjobsheetsummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

    Sub controlsvisibilitybytype(ByRef type As String)
        lblBlock.Visible = False
        lblForBlock.Visible = False
        If (type = ApplicationConstants.schemeType Or type = ApplicationConstants.blockType) Then
            lblForBlock.Visible = False
            lblBlock.Visible = False
            lblForCustomer.Visible = False
            lblCustomer.Visible = False
            lblTelephone.Visible = False
            lblMobile.Visible = False
            lblForEmail.Visible = False
            lblForHouseNumber.Visible = False
            lblHouseNumber.Visible = False
            lblPostCode.Visible = False
            lblForPostCode.Visible = False
            lblForTelephone.Visible = False
            lblForMobile.Visible = False
            lblEmail.Visible = False
            btn_CustDetailUpdate.Visible = False
        End If
        If (type = ApplicationConstants.blockType) Then
            lblBlock.Visible = True
            lblForBlock.Visible = True
        End If
    End Sub
    Sub resetvisibility()
        lblForBlock.Visible = True
        lblForCustomer.Visible = True
        lblCustomer.Visible = True
        lblTelephone.Visible = True
        lblMobile.Visible = True
        lblEmail.Visible = True
        lblForHouseNumber.Visible = True
        lblHouseNumber.Visible = True
        lblPostCode.Visible = True
        lblForPostCode.Visible = True
        lblForTelephone.Visible = True
        lblForMobile.Visible = True
        lblEmail.Visible = True
        btn_CustDetailUpdate.Visible = True
        lblBlock.Visible = True
    End Sub
    Private Sub populateJobSheetSummary(ByRef jsn As String)
        Dim dsJobSheetSummary As New DataSet()
        Dim objJobSheetBL As JobSheetBL = New JobSheetBL()
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)
        objJobSheetBL.getJobSheetSummaryByJsnAC(dsJobSheetSummary, jsn)

        SessionManager.setJobSheetSummaryDs(dsJobSheetSummary)

    End Sub
    Private Sub resetJobSheetControls()
        lblJSN.Text = String.Empty
        lblPlannedWorkComponent.Text = String.Empty
        lblTrade.Text = String.Empty
        lblStatus.Text = String.Empty
        lblStatus.Visible = True
        StatusDropDown.Visible = Not (lblStatus.Visible)
        lblContractor.Text = String.Empty
        lblContact.Text = String.Empty
        txtWorksRequired.Text = String.Empty

        lblPONo.Text = String.Empty
        lblOrderedBy.Text = String.Empty
        lblOrdered.Text = String.Empty

        lblBudget.Text = String.Empty
        lblEstimate.Text = String.Empty
        lblVat.Text = String.Empty
        lblNetCost.Text = String.Empty

        lblScheme.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblHouseNumber.Text = String.Empty

        lblCustomer.Text = String.Empty
        lblTelephone.Text = String.Empty
        lblMobile.Text = String.Empty
        lblEmail.Text = String.Empty
        btnUpdate.Text = "Update"

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()
    End Sub

#End Region
#Region "Grid Activities Sorting"

    Protected Sub grdActivities_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdActivities.Sorting
        Try
            Me.sortExpression = e.SortExpression
            ViewState(ViewStateConstants.SortExpression) = e.SortExpression
            Me.sortActivitiesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl ActionType Selected Index Changed"

    Protected Sub ddlActionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlActionType.SelectedIndexChanged
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Me.populateActivitiesGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Letter Doc Click"
    Protected Sub imgBtnLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            Dim journalHistoryId As Int64 = CType(btn.CommandArgument(), Integer)
            Me.resetLetterDocLabels()
            Me.getPropertyLetters(journalHistoryId)
            Me.getPropertyDocuments(journalHistoryId)
            Me.bindUrlsWithDocLinkButton(sender)
            Me.bindUrlsWithLetterLinkButton()
            mdlPopUpLetterDoc.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Job sheet Click"
    Protected Sub imgBtnPlannedJobsheet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Email Status Click"


    Protected Sub imgbtnEmailStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim commandParams() As String = CType(imgBtn.CommandArgument, String).Split(";")
            Dim itemId As String = commandParams(0).ToString()
            Dim CustomerEmail As String = String.Empty
            Dim resultDataSet As DataSet = New DataSet()

            hdnLetterHistoryId.Value = itemId
            hdnAppointmentId.Value = commandParams(1).ToString()
            objPropertyBl.getPropertyCustomerEmail(resultDataSet, itemId)
            For i As Integer = 0 To resultDataSet.Tables(0).Rows.Count - 1
                CustomerEmail = resultDataSet.Tables(0).Rows(i)(0).ToString()
            Next

            txtSendTo.Text = CustomerEmail
            Me.mdlPopUpSendEmail.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Send Action Click"
    Protected Sub btnSendAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSendAction.Click
        Dim strStdLetterId As String = hdnLetterHistoryId.Value
        Dim appointmentId As String = hdnAppointmentId.Value
        Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO()
        Me.getSavedLetter(strStdLetterId, savedLetterPDFBO)
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        If Validation.isEmail(txtSendTo.Text) Then
            Me.SendPDFEmail(savedLetterPDFBO, EmailStatus)
        Else
            uiMessageHelper.setMessage(lblSendEmailMessage, pnlSendEmailMessage, UserMessageConstants.InvalidEmail, True)
            Me.mdlPopUpSendEmail.Show()
        End If

        objPropertyBl.UpdatePropertyCustomerEmail(txtSendTo.Text(), appointmentId, EmailStatus)
    End Sub
#End Region

#Region "img Btn Fault Contractor Job sheet Click"
    Protected Sub imgBtnFaultContractorJobsheet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtnContractorJobsheet As ImageButton = DirectCast(sender, ImageButton)
            Dim faultHistoryId As Int32 = Convert.ToInt32(imgBtnContractorJobsheet.CommandArgument)
            ucContractorJobSheetSummary.showPopupJobSheetSummary(faultHistoryId)
            mdlPopupContractorJobsheet.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn Cancel Notes Click"
    Protected Sub imgBtnCancelNotes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtnContractorJobsheet As ImageButton = DirectCast(sender, ImageButton)
            Dim journalId As Int32 = Convert.ToInt32(imgBtnContractorJobsheet.CommandArgument)
            Dim notes As String = objPropertyBl.getApplianceCancelNotes(journalId)
            txtBoxDescription.Text = notes
            mdlPopUpCancelFaults.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img btn Appointment notes Click"

    Protected Sub imgbtnAptNotes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btnNotes As ImageButton = CType(sender, ImageButton)
            Dim appointmentId As Integer = CType(btnNotes.CommandArgument, Integer)
            ucAppointmentNotes.loadAppointmentData(appointmentId)
            mdlAppointmentNotesPopup.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region


#Region "Add Activity Popup Events"

#Region "ddl Status Selected Index Changed"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            Dim statusId As Integer = CType(ddlStatus.SelectedValue(), Integer)
            Me.loadActions(statusId)
            Me.loadLetters(-1)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAction.SelectedValue(), Integer)
            Me.loadLetters(actionId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try
    End Sub

#End Region

#Region "async Doc Upload Uploaded Complete"
    'Protected Sub asyncDocUpload_UploadedComplete(sender As Object, e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles asyncDocUpload.UploadedComplete

    '    If IsNothing(Session(SessionConstants.DocumentName)) Then

    '        If (asyncDocUpload.HasFile) Then

    '            Dim filePath As String = String.Empty
    '            Dim fileName As String = String.Empty
    '            filePath = GeneralHelper.getDocumentUploadPath()

    '            If IsNothing(filePath) Then
    '                Me.asyncDocUpload.FailedValidation = True
    '            End If

    '            If (Directory.Exists(filePath) = False) Then
    '                Directory.CreateDirectory(filePath)
    '            Else
    '                fileName = Path.GetFileName(e.FileName)
    '                Dim filePathName As String = filePath + fileName
    '                asyncDocUpload.SaveAs(filePathName)
    '                Session(SessionConstants.DocumentName) = fileName

    '            End If
    '        End If
    '    Else
    '        Try
    '            Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.Message = ex.Message

    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If

    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
    '            End If
    '            mdlPopUpAddActivity.Show()
    '        End Try
    '    End If


    'End Sub
#End Region

#Region "Btn Save Action Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click

        Dim isSaved As Boolean = False
        Try
            If (Me.validateAddActivity() = True) Then
                Dim journalHistoryId As Integer = 0

                journalHistoryId = saveAddActivityPopup()

                If journalHistoryId > 0 Then
                    Me.populateActivitiesGrid()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveActvitySuccessfully, False)
                    isSaved = True
                    Me.resetAddActivityControls()

                    'To remove the record of existing letters from the session,
                    'So these these letters can be re attached on re arrange.
                    SessionManager.removeActivityLetterDetail()

                Else
                    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            If isSaved = False Then
                Me.mdlPopUpAddActivity.Show()
            Else
                Me.mdlPopUpAddActivity.Hide()
            End If
        End Try
    End Sub
#End Region

#Region "Show Defect Images Click"
    Protected Sub showDefectImages_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim propertyID As String = Request.QueryString(PathConstants.PropertyIds)
            ucDefectImages.LoadImageData(propertyID)
            mdlPopUpShowDefectImages.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Defect Detail Action Click"
    Protected Sub linkbtnDefectDetail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkbtnDefectDetail As LinkButton = CType(sender, LinkButton)
            ucDefectManagement.viewDefectDetails(linkbtnDefectDetail.CommandArgument)
            mdlPoPUpAddDefect.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "btn View Letter Click"
    Protected Sub btnViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewLetter.Click
        Try
            If (ddlLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
            SessionManager.setPropertyIdForEditLetter(Me.propertyId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try

    End Sub
#End Region

#Region "btn Upload Letter Click"

    Protected Sub btnUploadLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadLetter.Click
        Try
            mdlPopUpAddActivity.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ckBox Refresh DataSet Checked Changed"

    Protected Sub ckBoxRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxRefreshDataSet.CheckedChanged
        Me.ckBoxRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId())
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try

    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"
    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddActivity.Show()
        End Try
    End Sub
#End Region

#Region "Btn Print Activities"
    Protected Sub btnPrintActivities_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrintActivities.Click
        Response.Redirect("~/Views/Property/PrintActivities.aspx?Id=" + Me.propertyId, False)
    End Sub
#End Region

#End Region

#Region "grdActivities PageIndexChanging"
    Protected Sub grdActivities_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdActivities.PageIndexChanging
        Dim resultDataSet As DataSet = New DataSet()
        ''Dim newPageNumber As Integer = e.NewPageIndex + 1

        grdActivities.PageIndex = e.NewPageIndex
        Dim activitiesBoList As List(Of ActivitiesBO) = SessionManager.getActivitiesBOList()
        grdActivities.DataSource = activitiesBoList
        grdActivities.DataBind()
    End Sub
#End Region

#Region "Show Hide Child Grid"
    Protected Sub Show_Hide_ChildGrid(ByVal sender As Object, ByVal e As EventArgs)
        Dim imgShowHide As ImageButton = TryCast(sender, ImageButton)
        Dim row As GridViewRow = TryCast(imgShowHide.NamingContainer, GridViewRow)
        If imgShowHide.CommandArgument = "Show" Then
            row.FindControl("pnlChild").Visible = True
            imgShowHide.CommandArgument = "Hide"
            imgShowHide.ImageUrl = "~/Images/minus.gif"
            Dim JournalId As String = grdActivities.DataKeys(row.RowIndex).Value.ToString()

            Dim grdActivitiesChild As GridView = TryCast(row.FindControl("grdActivitiesChild"), GridView)
            Dim lblInspectionType As Label = DirectCast(row.FindControl("lblInspectionType"), Label)
            BindActivitiesChild(JournalId, lblInspectionType.Text, grdActivitiesChild)
        Else
            row.FindControl("pnlChild").Visible = False
            imgShowHide.CommandArgument = "Show"
            imgShowHide.ImageUrl = "~/Images/plus.gif"
        End If
    End Sub
#End Region
#End Region

#Region "Functions"

#Region "btn Close Click"
    Protected Sub btnClose_Click()
        Try
            mdlAppointmentNotesPopup.Hide()
            populateActivitiesGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "hide Show Notes Button"
    ''' <summary>
    ''' hide Show Notes Button
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowAppointmentNotesButton(ByVal notes As String, ByVal inspecType As String)

        '' Appointment Notes link will only be visible against Appliance Appointments
        If Not inspecType.Equals("Appliance Servicing") Then
            Return False
        End If

        If notes.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "show Property Activities "

    Public Sub loadPropertyActivities()
        Try
            btnPrintActivities.Visible = True
            ddlActionType.Visible = True
            'printActivities.Visible = False
            Me.loadInspectionTypes(Me.ddlActionType)
            Me.setDefaultSort()
            Me.populateActivitiesGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Load Inspection Types in DropDown"
    Public Sub loadInspectionTypes(ByRef dropDownRef As DropDownList)

        Dim resultDataSet As DataSet = New DataSet()
        objUsersBL.getInspectionTypes(resultDataSet)
        dropDownRef.DataSource = resultDataSet
        dropDownRef.DataValueField = ApplicationConstants.InspectionId
        dropDownRef.DataTextField = ApplicationConstants.InspectionName
        dropDownRef.DataBind()
        dropDownRef.Items.Add(New ListItem("Select Type", ApplicationConstants.DropDownDefaultValue))
        dropDownRef.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "populate Activities Grid "

    Public Sub populateActivitiesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyID As String = Request.QueryString(PathConstants.PropertyIds)

        Me.sortDirection = ViewState(ViewStateConstants.SortDirection)
        Me.sortExpression = ViewState(ViewStateConstants.SortExpression)

        objPropertyBl.propertyActivitiesList(resultDataSet, Me.sortDirection, Me.sortExpression, ddlActionType.SelectedValue, propertyID)


        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Me.grdActivities.Visible = False
        Else
            'Dim drlist As New List(Of DataRow)()

            'For Each row As DataRow In resultDataSet.Tables(0).Rows
            '    drlist.Add(CType(row, DataRow))
            'Next row
            Dim query = From row As DataRow In resultDataSet.Tables(0).Rows
            Group row By JournalId = row.Field(Of Integer)("JournalId"), InspectionType = row.Field(Of String)("InspectionType") Into Group
            Select Group

            Dim activitiesBoList As List(Of ActivitiesBO) = New List(Of ActivitiesBO)()

            For Each grp In query

                Dim activity = grp.FirstOrDefault()
                Dim activitiesBo As ActivitiesBO = New ActivitiesBO()
                activitiesBo.Action = activity.Item("Action")
                If (Not IsDBNull(activity.Item("AppointmentDate"))) Then
                    activitiesBo.AppointmentDate = activity.Item("AppointmentDate")
                End If

                activitiesBo.CP12DocumentID = activity.Item("CP12DocumentID")
                If (Not IsDBNull(activity.Item("CreateDate"))) Then
                    activitiesBo.CreateDate = activity.Item("CreateDate")
                End If
                If (Not IsDBNull(activity.Item("CREATIONDATE"))) Then
                    activitiesBo.CREATIONDate = activity.Item("CREATIONDATE")
                End If

                activitiesBo.IsDefectImageExist = activity.Item("IsDefectImageExist")
                activitiesBo.PropertyDefectId = activity.Item("PropertyDefectId")
                activitiesBo.Document = activity.Item("Document")
                activitiesBo.InspectionType = activity.Item("InspectionType")
                activitiesBo.IsDocumentAttached = activity.Item("IsDocumentAttached")
                activitiesBo.IsLetterAttached = activity.Item("IsLetterAttached")
                activitiesBo.JournalHistoryId = activity.Item("JournalHistoryId")
                activitiesBo.JournalId = activity.Item("JournalId")
                activitiesBo.Name = activity.Item("Name")
                activitiesBo.OPERATIVENAME = activity.Item("OPERATIVENAME")

                activitiesBo.AppointmentId = activity.Item("APPOINTMENTID")
                activitiesBo.AppointmentNotes = activity.Item("AppointmentNotes")
                activitiesBo.InspectionTypeDescription = activity.Item("InspectionTypeDescription")


                'my change


                If (Not IsDBNull(activity.Item("HistoryCP12DocumentID"))) Then
                    activitiesBo.LGSRHISTORYID = activity.Item("HistoryCP12DocumentID")
                End If


                'end my change



                If (Not IsDBNull(activity.Item("OPERATIVETRADE"))) Then
                    activitiesBo.OPERATIVETRADE = activity.Item("OPERATIVETRADE")
                End If
                If (Not IsDBNull(activity.Item("REF"))) Then
                    activitiesBo.REF = activity.Item("REF")
                End If

                activitiesBo.Status = activity.Item("Status")
                activitiesBoList.Add(activitiesBo)
            Next


            Me.grdActivities.Visible = True
            ViewState.Add(ViewStateConstants.GridDataSet, resultDataSet)
            SessionManager.setActivitiesBOList(activitiesBoList)
            Me.grdActivities.DataSource = activitiesBoList
            Me.grdActivities.DataBind()
        End If
    End Sub
#End Region

#Region "sort Activities Grid "
    Protected Sub sortActivitiesGrid()
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)

        Dim activitiesBoList As List(Of ActivitiesBO) = SessionManager.getActivitiesBOList()
        grdActivities.DataSource = activitiesBoList
        grdActivities.DataBind()


        Dim activitiesDataTable As DataTable = New DataTable()
        activitiesDataTable = ConvertToDataTable(activitiesBoList)

        Me.setSortDirection()

        If IsNothing(activitiesDataTable) = False Then
            Dim dvSortedView As DataView = New DataView(activitiesDataTable)
            dvSortedView.Sort = Me.sortExpression + " " + Me.sortDirection
            grdActivities.DataSource = dvSortedView
            grdActivities.DataBind()

        End If
    End Sub
#End Region

#Region "get Property Letters"
    Protected Sub getPropertyLetters(ByRef journalHistoryId As Int64)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.propertyLetterList(resultDataSet, journalHistoryId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            dataListLetters.DataSource = resultDataSet
            dataListLetters.DataBind()
            uiMessageHelper.setMessage(lblNoLetters, pnlLetters, UserMessageConstants.NoLettersFound, True)
        Else
            dataListLetters.DataSource = resultDataSet
            dataListLetters.DataBind()
        End If

    End Sub

#End Region

#Region "get Property Documents"
    Protected Sub getPropertyDocuments(ByRef journalHistoryId As Int64)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.propertyDocumentList(resultDataSet, journalHistoryId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            dataListDocs.DataSource = resultDataSet
            dataListDocs.DataBind()
            uiMessageHelper.setMessage(lblNoDoc, pnlDocs, UserMessageConstants.NoDocumentsFound, True)
        Else
            dataListDocs.DataSource = resultDataSet
            dataListDocs.DataBind()
        End If

    End Sub

#End Region

#Region "reset Letter Doc Labels"
    Protected Sub resetLetterDocLabels()
        Me.lblNoLetters.Text = ""
        Me.lblNoDoc.Text = ""
    End Sub
#End Region

#Region "set Sort Direction"
    Protected Sub setSortDirection()

        'This if condition will 
        If ViewState(ViewStateConstants.SortDirection).ToString() = ViewStateConstants.Ascending Then

            ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Descending
            Me.sortDirection = "DESC"
        Else
            ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Ascending
            Me.sortDirection = "ASC"

        End If
    End Sub
#End Region

#Region "get  Sort Direction"
    Protected Function getSortDirection()
        Return ViewState(ViewStateConstants.SortDirection).ToString()
    End Function
#End Region

#Region "set Default Sort"

    Protected Sub setDefaultSort()
        ViewState(ViewStateConstants.SortDirection) = ViewStateConstants.Descending
        ViewState(ViewStateConstants.SortExpression) = ViewStateConstants.CreationDate
        Me.sortDirection = "DESC"
        Me.sortExpression = "CREATIONDATE"
    End Sub
#End Region

#Region "convert String Bit To Boolean"

    Public Function convertStringBitToBoolean(ByVal value As String) As [Boolean]
        If value <> String.Empty Then
            If value = "False" Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function
#End Region

#Region "Bind Urls with Letter Link Button"
    Protected Sub bindUrlsWithLetterLinkButton()
        Dim lnkBtn As New LinkButton()
        Dim imgBtn As New ImageButton()
        Dim url As String = String.Empty

        For i As Integer = 0 To dataListLetters.Items.Count - 1
            lnkBtn = DirectCast(dataListLetters.Items(i).FindControl("lnkBtnLetter"), LinkButton)
            If Not IsNothing(lnkBtn) Then
                url = PathConstants.LetterWindowPath + "?id=" + lnkBtn.CommandArgument
                lnkBtn.Attributes.Add("onClick", String.Format(ApplicationConstants.DowloadWindowScript, url))
            End If
            imgBtn = DirectCast(dataListLetters.Items(i).FindControl("imgbtnEmailStatus"), ImageButton)
            imgBtn.Attributes.Add("onClick", "imgbtnEmailStatus_Click")


        Next
    End Sub
#End Region

#Region "Bind Urls with Doc Link Button"
    Protected Sub bindUrlsWithDocLinkButton(ByRef sender As Object)
        Dim lnkBtn As LinkButton = TryCast(sender, LinkButton)
        Dim url As String = String.Empty
        Dim idsArray() As String

        For i As Integer = 0 To dataListDocs.Items.Count - 1
            lnkBtn = DirectCast(dataListDocs.Items(i).FindControl("lnkBtnDoc"), LinkButton)
            'First index of this array will contain the document id and third index will contain the journalhistoryid
            idsArray = CType(lnkBtn.CommandArgument, String).Split(";;")

            url = PathConstants.DownloadWindowPath + "?did=" + idsArray(0) + "&jhid=" + idsArray(2) + "&doc=true"
            lnkBtn.Attributes.Add("onClick", String.Format(ApplicationConstants.DowloadWindowScript, url))
        Next
    End Sub
#End Region

#Region "Load Satus"
    Protected Sub loadSatus()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = ApplicationConstants.StatusId
        ddlStatus.DataTextField = ApplicationConstants.Title
        ddlStatus.DataBind()
        ddlStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Actions"
    Protected Sub loadActions(ByRef statusId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAction.DataSource = resultDataSet
        ddlAction.DataValueField = ApplicationConstants.ActionId
        ddlAction.DataTextField = ApplicationConstants.Title
        ddlAction.DataBind()
        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub loadLetters(ByRef actionId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlLetter.DataSource = resultDataSet
        ddlLetter.DataValueField = ApplicationConstants.LetterId
        ddlLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlLetter.DataBind()
        ddlLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
                SessionManager.removeDocumentUploadName()
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If


    End Sub
#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Validate Add Activity"
    Protected Function validateAddActivity()
        Dim success As Boolean = True
        If Me.ddlInspectionType.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionType, True)
        ElseIf IsNothing(Me.txtDate.Text) Or Me.txtDate.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionDate, True)
        ElseIf Me.ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionStatus, True)
        ElseIf Me.ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)
        ElseIf IsNothing(Me.txtNotes.Text) Or Me.txtNotes.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionNotes, True)
        End If
        Return success
    End Function
#End Region

#Region "Reset Add Activity Controls"
    Protected Sub resetAddActivityControls()
        Me.lblAddedBy.Text = SessionManager.getUserFullName()
        Me.ddlInspectionType.SelectedValue = "-1"
        Me.txtDate.Text = Date.Now.ToString().Substring(0, 10)
        Me.ddlStatus.SelectedValue = "-1"
        Me.loadActions(-1)
        Me.loadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#Region "Save Add Activity Popup"
    Protected Function saveAddActivityPopup()
        Dim objPropActivityBo As PropertyActivityBO = New PropertyActivityBO()

        objPropActivityBo.PropertyId = Me.propertyId
        objPropActivityBo.CreatedBy = SessionManager.getAppServicingUserId()
        objPropActivityBo.InspectionTypeId = ddlInspectionType.SelectedValue
        objPropActivityBo.ActionDate = CType(txtDate.Text, Date)
        objPropActivityBo.StatusId = ddlStatus.SelectedValue
        objPropActivityBo.ActionId = ddlAction.SelectedValue
        objPropActivityBo.LetterId = ddlLetter.SelectedValue
        objPropActivityBo.Notes = txtNotes.Text
        objPropActivityBo.DocumentPath = GeneralHelper.getDocumentUploadPath()


        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objPropActivityBo.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objPropActivityBo.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objPropActivityBo.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objPropActivityBo.IsLetterAttached = False
            End If

            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objPropActivityBo.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objPropActivityBo.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objPropActivityBo.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(4), String))
                Next
            Else
                objPropActivityBo.IsDocumentAttached = False
            End If
        End If

        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        Return objPropertyBl.savePropertyActivity(objPropActivityBo, savedLetterDt)

    End Function

#End Region

#Region "Check CP12 Status and Return True/False to Enable/Disable Clip Image"

    Public Function CheckCp12Status(ByRef Cp12Status As Object, ByRef CP12DocumentID As Object)
        Dim status As Boolean = False

        If (Not IsDBNull(Cp12Status)) Then
            If ((LCase(ApplicationConstants.Cp12IssuedStatus) = LCase(CType(Cp12Status, String))) _
                AndAlso (CType(CP12DocumentID, Integer)) > 0) Then
                status = True
                'End If
            End If
        End If

        Return status
    End Function

#End Region

#Region "Check Defect Status and Return True/False to Enable/Disable button"

    Public Function CheckDefectStatus(ByRef DefectStatus As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(DefectStatus)) Then
            If LCase(DefectStatus.ToString()).Contains(LCase(ApplicationConstants.DefectTypeStatus)) Then
                status = True
            End If
        End If

        Return status
    End Function
#End Region

#Region "Check InspectionId and Return True/False to Enable/Disable Clip Image"
    Public Function CheckInspectionID(ByRef IsDocumentAttached As Object, ByRef CP12DocumentID As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(IsDocumentAttached)) Then
            If ((CType(CP12DocumentID, Integer)) > 0 And (CType(IsDocumentAttached, Integer) = 1)) Then
                status = True
            End If
        End If
        Return status
    End Function
#End Region

#Region "Check Planned InspectionId and Return True/False to Enable/Disable Clip Image"
    Public Function CheckPlannedInspectionID(ByRef IsDocumentAttached As Object, ByRef plannedStatus As Object)
        Dim status As Boolean = False
        If (Not IsDBNull(IsDocumentAttached)) Then
            If (plannedStatus.ToString().StartsWith(ApplicationConstants.InspectionArrangedStatus) And (CType(IsDocumentAttached, Integer) = 1)) Then
                status = True
            End If
        End If
        Return status
    End Function
#End Region

#Region "Show/Hide for Cancel Notes icon"
    Public Function CheckApplianceStatus(ByRef applianceStatus As Object, ByRef InspectionTypeDescription As Object)
        Dim status As Boolean = False

        If (String.Compare(applianceStatus.ToString(), ApplicationConstants.ApplianceCancelledStatus) = 0 AndAlso String.Compare(InspectionTypeDescription.ToString(), ApplicationConstants.ApplianceInspectionTypeDescription) = 0) Then
            status = True
        End If
        Return status
    End Function
#End Region

    Private Sub BindActivitiesChild(ByVal JournalId As String, ByVal inspectionType As String, ByVal grdActivitiesChild As GridView)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState(ViewStateConstants.GridDataSet)
        Dim filteredActivitiesDt As DataTable = New DataTable()
        Dim activitiesDv As DataView = New DataView()
        activitiesDv = resultDataSet.Tables(0).AsDataView()
        activitiesDv.RowFilter = "JournalId = '" & JournalId & "' AND InspectionType = '" & inspectionType & "'"
        filteredActivitiesDt = activitiesDv.ToTable
        grdActivitiesChild.DataSource = filteredActivitiesDt
        grdActivitiesChild.DataBind()
        For Each DGR As GridViewRow In grdActivitiesChild.Rows
            Dim imgbtnassignedtocontractor As ImageButton = DirectCast(DGR.FindControl("imgbtnassignedtocontractor"), ImageButton)
            Dim imgBtnPlannedJobsheet As ImageButton = DirectCast(DGR.FindControl("imgBtnPlannedJobsheet"), ImageButton)
            Dim lblstatus As Label = DirectCast(DGR.FindControl("lblstatus"), Label)
            Dim lblInspectionTypeDescription As Label = DirectCast(DGR.FindControl("lblInspectionTypeDescription"), Label)
            If (inspectionType.StartsWith(ApplicationConstants.PlannedActiontype)) Then
                If (lblInspectionTypeDescription.Text.ToString.Equals(ApplicationConstants.AssignedToContractorStatus)) Then

                    If lblstatus.Text.ToString.StartsWith(ApplicationConstants.AssignedToContractorStatus) Then
                        imgbtnassignedtocontractor.Visible = True
                    End If

                    If lblstatus.Text.ToString.StartsWith(ApplicationConstants.ToBeArrangedStatus) Then
                        imgbtnassignedtocontractor.Visible = False
                        imgBtnPlannedJobsheet.Visible = False
                    End If
                    If lblstatus.Text.ToString.StartsWith(ApplicationConstants.ApplianceCancelledStatus) Then
                        imgbtnassignedtocontractor.Visible = True
                        imgBtnPlannedJobsheet.Visible = False
                    End If
                    If lblstatus.Text.ToString.StartsWith(ApplicationConstants.ApplianceCompletedStatus) Then
                        imgbtnassignedtocontractor.Visible = True
                        imgBtnPlannedJobsheet.Visible = False
                    End If

                End If
            End If
        Next
    End Sub

#Region "Convert list to datatable"
    ''' <summary>
    ''' Convert Generic list to data table
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="list"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertToDataTable(Of T)(ByVal list As List(Of T)) As DataTable
        Dim dt As New DataTable()
        For Each info As PropertyInfo In GetType(T).GetProperties()
            dt.Columns.Add(New DataColumn(info.Name, If(Nullable.GetUnderlyingType(info.PropertyType), info.PropertyType)))
        Next
        If list IsNot Nothing Then
            For Each tt As T In list
                Dim row As DataRow = dt.NewRow()
                For Each info As PropertyInfo In GetType(T).GetProperties()
                    row(info.Name) = If(info.GetValue(tt, Nothing), DBNull.Value)
                Next
                dt.Rows.Add(row)
            Next
        End If
        Return dt
    End Function

#End Region

    Private Sub SendPDFEmail(ByRef savedLetterPDFBO As SavedLetterPDFBO, ByRef emailStatus As String)

        Dim propertySchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

        Using sw As New StringWriter()
            Using hw As New HtmlTextWriter(sw)

                '=========================================================================================

                Dim letterHTML As StringBuilder = New StringBuilder(String.Empty)
                letterHTML.Append("<html><body>")
                letterHTML.Append("<div id='print_content' style='position: absolute; font-family: Arial;font-size: 11pt;'>")
                letterHTML.Append("Tenancy Ref:").Append(savedLetterPDFBO.TenancyRef).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.TenantName).Append("<br>")

                letterHTML.Append(savedLetterPDFBO.HouseNumber).Append(", ").Append(savedLetterPDFBO.AddressLine1).Append("<br>")

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.AddressLine2) Then
                    letterHTML.Append(savedLetterPDFBO.AddressLine2).Append("<br>")
                End If
                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.TownCity) Then
                    letterHTML.Append(savedLetterPDFBO.TownCity).Append("<br>")
                End If

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.CountyValue) Then
                    letterHTML.Append(savedLetterPDFBO.CountyValue).Append("<br>")
                End If

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.PostCode) Then
                    letterHTML.Append(savedLetterPDFBO.PostCode).Append("<br>")
                End If

                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.LetterDate.ToString("dd MMMM yyyy")).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append("Dear ").Append(savedLetterPDFBO.TenantName).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.LetterBody).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.SignOff).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.FromResource).Append("<br>")

                letterHTML.Append(savedLetterPDFBO.Team).Append("<br>")
                letterHTML.Append("Direct Dial: ").Append(savedLetterPDFBO.DirectDial).Append("<br>")
                letterHTML.Append("Email: ").Append(savedLetterPDFBO.DirectEmail).Append("<br>")

                letterHTML.Append("</div>")
                letterHTML.Append("</body></html>")

                '=========================================================================================
                Dim sr As New StringReader(letterHTML.ToString())

                Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 56.692913386, 34.015748031, 85.039370079, 0) 'Margins are in points and 72 points equal 1 inch
                Dim htmlparser As New HTMLWorker(pdfDoc)
                Using memoryStream As New MemoryStream()
                    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, memoryStream)
                    pdfDoc.Open()
                    htmlparser.Parse(sr)
                    pdfDoc.Close()
                    Dim bytes As Byte() = memoryStream.ToArray()
                    memoryStream.Close()

                    If Validation.isEmail(txtSendTo.Text) Then
                        emailStatus = UserMessageConstants.EmailSending
                        Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                        Dim mailMessage As New Mail.MailMessage

                        mailMessage.Subject = subject
                        mailMessage.To.Add(New MailAddress(txtSendTo.Text, propertySchedulingBo.CustomerName))
                        mailMessage.Attachments.Add(New Attachment(New MemoryStream(bytes), "Letter1.pdf"))
                        mailMessage.IsBodyHtml = True

                        EmailHelper.sendEmail(mailMessage)
                        emailStatus = UserMessageConstants.EmailSent
                    Else
                        Throw New Exception(UserMessageConstants.InvalidEmail)
                    End If
                End Using
            End Using
        End Using
    End Sub

    Protected Function getSavedLetter(ByRef strStdLetterId As String, ByRef SavedLetterPDFBO As SavedLetterPDFBO) As SavedLetterPDFBO

        Dim stdLetterId As Integer = 0
        If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
            Dim resultSet As DataSet = New DataSet()

            SavedLetterPDFBO = objLetterBL.getSavedLetterById(stdLetterId, resultSet)
            Return SavedLetterPDFBO
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region "Show Defects Detail Popup"

    Protected Sub showDefectDetail(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgbtnDefectDetail As ImageButton = CType(sender, ImageButton)
            ucDefectManagement.viewDefectDetails(imgbtnDefectDetail.CommandArgument)
            mdlPoPUpAddDefect.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region
#Region "User Controls Events Handling"

    Private Sub ucDefectManagement_cancelButton_Clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles ucDefectManagement.cancelButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub ucDefectManagement_saveButton_Clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles ucDefectManagement.saveButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectSavedSuccessfuly, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub ucDefectImages_cancelButton_Clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles ucDefectImages.cancelButton_Clicked
        Try
            mdlPopUpShowDefectImages.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

    Protected Sub grdActivities_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdActivities.SelectedIndexChanged

    End Sub

End Class