﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectPhotographs.ascx.vb"
    Inherits="Appliances.DefectPhotographs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 270px;
    }
</style>
<asp:Panel ID="pnlMessage" runat="server" style=' margin-left:15px; ' >
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel runat="server" ID="updPnlPhotos">
    <ContentTemplate>
    <div style="float: right; width :30%;">
   <%--     <asp:Button runat="server" ID="btnUploadPhoto" Text="Add New Photograph" Style="float: right"
            BackColor="White" UseSubmitBehavior="false" />--%>
              <asp:Button runat="server" ID="btnRefresh" Text="Refresh" Style="float: right; margin-right:20px; "
            BackColor="White" UseSubmitBehavior="false" />
</div>
        <asp:ListView runat="server" ID="lstViewPhotos" GroupItemCount="3" >
            <LayoutTemplate>
                <table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
                    style="border-collapse: collapse; width: 100%;">
                    <tr id="groupPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <GroupTemplate>
                <tr id="itemPlaceholderContainer" runat="server">
                    <td id="itemPlaceholder" runat="server">
                    </td>
                </tr>
            </GroupTemplate>
            <ItemTemplate>
                <div class="itemPhoto">
                   
                  
                    <img src='<%= uploadedThumbUri %><%# Eval("ImageTitle") %>' alt='<%# Eval("ImageTitle") %>'
                        width="156px" height="120px" style="margin: 5px;" />
                   
                </div>
            </ItemTemplate>
            <EmptyItemTemplate>
            </EmptyItemTemplate>
        </asp:ListView>
        <asp:Label runat="server" ID="lblDispSlideShow">   </asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpAddPhoto" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispPhotoGraph" PopupControlID="pnlAddPhotoPopup"
    DropShadow="true" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlAddPhotoPopup" runat="server" BackColor="White" Width="400px" >
    <table id="pnlAddPhotoTable">
        <tr>
            <td colspan="2" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px;">
                    Add New Photograph</div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                 <div style=' margin-left:15px; '> <asp:Panel ID="pnlAddPhotoMessage" runat="server" Visible="false" >
                    <asp:Label ID="lblAddPhotoMessage" runat="server" Text=""></asp:Label>
                </asp:Panel></div> 
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" width="120px">
                Date:<span class="Required">*</span>
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Image ID="imgCalendar" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                    Style="float: right; margin-right: 65px; float: right" Width="25px" />
                <asp:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtBoxDueDate"
                    Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                <asp:TextBox ID="txtBoxDueDate" runat="server" OnClick="this.value=''" Style="width: 168px;" ></asp:TextBox>
            
                
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" width="120px">
                Title:<span class="Required">*</span>
            </td>
            <td align="left" valign="top" class="style1">
                <asp:TextBox ID="txtBoxTitle" runat="server">
                </asp:TextBox>
               
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" width="120px">
                Upload:<span class="Required">*</span>
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Button ID="btnUploadPhotos" runat="server" Text="Upload" Width="60px" OnClientClick="openPhotoUploadWindow('phototab')" />
                <asp:Label runat="server" ID="lblFileName"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;" class="style1">
                <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                &nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" ValidationGroup="check" />
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>
<asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label>
<asp:CheckBox ID="ckBoxPhotoUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<ajaxToolkit:ModalPopupExtender ID="mdlPopUpViewImage" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblPopUpShowImage" PopupControlID="pnlLargeImage"
    CancelControlID="btnClose" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage"  >
    <div style='width:100%;'>
       <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute;
        top: -17px; right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />

        <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage"  />
    </div>
</asp:Panel>


