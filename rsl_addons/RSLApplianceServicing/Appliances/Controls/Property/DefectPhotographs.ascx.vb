﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing.Imaging.ImageFormat
Imports System.Globalization



Public Class DefectPhotographs
    Inherits UserControlBase
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Public uploadedThumbUri As String = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/"
    'Public uploadedThumbUri As String = "../../Photographs/Images/"

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim photosDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyDefectImages(SessionManager.getPropertyId(), photosDataSet)
        lstViewPhotos.DataSource = photosDataSet
        lstViewPhotos.DataBind()
    End Sub
#End Region

    '#Region "Btn Upload Photo Click"
    '    ''' <summary>
    '    ''' Btn Upload Photo Click
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Protected Sub btnUploadPhoto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadPhoto.Click
    '        Try
    '            txtBoxDueDate.Text = String.Empty
    '            txtBoxTitle.Text = String.Empty
    '            lblFileName.Text = String.Empty
    '            uiMessageHelper.resetMessage(lblAddPhotoMessage, pnlAddPhotoMessage)
    '            mdlPopUpAddPhoto.Show()
    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.Message = ex.Message
    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If
    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
    '            End If
    '        End Try
    '    End Sub
    '#End Region

#Region "Btn Save Click event"
    ''' <summary>
    ''' Btn Save Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim objPhotographBo As PhotographBO = New PhotographBO()
        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyId As String = SessionManager.getPropertyId()
        Dim itemId As Integer = SessionManager.getTreeItemId() '   Session.Item(SessionConstants.treeItemId)
        Try
            If lblFileName.Text <> String.Empty And txtBoxTitle.Text <> String.Empty AndAlso txtBoxDueDate.Text <> String.Empty Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtBoxDueDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid Then
                    objPhotographBo.ImageName = lblFileName.Text  'Session.Item(SessionConstants.DocumentName) 
                    objPhotographBo.FilePath = Server.MapPath(GeneralHelper.getImageUploadPath())
                    objPhotographBo.UploadDate = CType(txtBoxDueDate.Text, DateTime)
                    objPhotographBo.Title = txtBoxTitle.Text
                    objPhotographBo.PropertyId = propertyId
                    objPhotographBo.ItemId = itemId
                    objPhotographBo.CreatedBy = SessionManager.getAppServicingUserId()
                    objPhotographBo.IsDefaultImage = False
                    Dim HeatingMappingId = Nothing
                    'If SessionManager.getTreeItemName() = "Heating" Then
                    '    objPhotographBo.HeatingMappingId = SessionManager.getHeatingMappingId()
                    '    HeatingMappingId = objPhotographBo.HeatingMappingId
                    'End If
                    objPropertyBl.savePhotograph(objPhotographBo)
                    objPropertyBl.getPropertyImages(propertyId, itemId, resultDataSet, HeatingMappingId)
                    lstViewPhotos.DataSource = resultDataSet
                    lstViewPhotos.DataBind()
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.notValidDateFormat
                    mdlPopUpAddPhoto.Show()
                End If
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.FillMandatory
                mdlPopUpAddPhoto.Show()
            End If

            'updPnlPhotos.Update()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Generate Thumbnail"
    ''' <summary>
    ''' Generate Thumbnail
    ''' </summary>
    ''' <param name="strFileName"></param>
    ''' <remarks></remarks>
    Protected Sub generateThumbnail(ByVal strFileName As String)
        'Create a new Bitmap Image loading from location of origional file
        Dim FilePath As String = String.Empty
        FilePath = GeneralHelper.getImageUploadPath()
        Dim bm As Bitmap = System.Drawing.Image.FromFile(FilePath & strFileName)
        'Declare Thumbnails Height and Width
        Dim newWidth As Integer = 100
        Dim newHeight As Integer = 100
        'Create the new image as a blank bitmap
        Dim resized As Bitmap = New Bitmap(newWidth, newHeight)
        'Create a new graphics object with the contents of the origional image
        Dim g As Graphics = Graphics.FromImage(resized)
        'Resize graphics object to fit onto the resized image
        g.DrawImage(bm, New Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel)
        'Get rid of the evidence
        g.Dispose()
        FilePath = GeneralHelper.getThumbnailUploadPath()
        'Create new path and filename for the resized image
        Dim newStrFileName As String = FilePath & strFileName
        'Save the new image to the same folder as the origional
        resized.Save(newStrFileName)

        resized.Dispose()
        bm.Dispose()
    End Sub
#End Region

#Region "ckBoxPhotoUpload Checked Changed"
    ''' <summary>
    ''' ckBoxPhotoUpload Checked Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxPhotoUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxPhotoUpload.CheckedChanged
        Me.ckBoxPhotoUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getPhotoUploadName())) Then
                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                lblFileName.Text = SessionManager.getPhotoUploadName()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
            End If
            mdlPopUpAddPhoto.Show()
            SessionManager.removePhotoUploadName()
        End Try
    End Sub
#End Region

#Region "btn Upload Photos Click"
    ''' <summary>
    ''' btn Upload Photos Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUploadPhotos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadPhotos.Click
        Try
            mdlPopUpAddPhoto.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotos.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                imgDynamicImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/" + fileName
                'mdlPopUpViewImage.X = 100
                'mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnRefresh click event"
    ''' <summary>
    ''' btnRefresh click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try
            Dim resultdataSet As DataSet = New DataSet()
            Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
            objPropertyBl.getPropertyDefectImages(propertyId, resultdataSet)
            lstViewPhotos.DataSource = resultdataSet
            lstViewPhotos.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#End Region
End Class