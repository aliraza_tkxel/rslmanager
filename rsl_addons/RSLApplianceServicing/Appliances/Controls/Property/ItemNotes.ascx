﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ItemNotes.ascx.vb"
    Inherits="Appliances.ItemNotes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:panel id="pnlMessage" runat="server" style='margin-left: 15px;'>
    <asp:label id="lblMessage" runat="server"></asp:label>
</asp:panel>
<asp:updatepanel id="updPanelNotes" runat="server">
    <contenttemplate>
        <div style="float: right; margin: -8px 0px 0 0px;">
            <asp:Button runat="server" ID="btnAddNotes" Text="Add Notes" BackColor="White" />
        </div>
        <asp:GridView ID="grdItemNotes" runat="server" AutoGenerateColumns="false" GridLines="None"
            ShowHeaderWhenEmpty="false" Width="100%">
            <Columns>
                <asp:BoundField DataField="CreatedOn" HeaderText="Date:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Notes:">
                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("Notes")%>' runat="server" Width="90%" Style='margin-bottom: 10px;' />
                    </ItemTemplate>
                    <ItemStyle Width="35%" VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedBy" HeaderText="By:">
                    <ItemStyle Width="25%" VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnEditNotes" OnClick="btnEditNotes_OnClick" Text="Edit"
                            Visible='<%# IsShowButton(Convert.ToBoolean(Eval("ISACTIVE"))) %>' BackColor="White" CommandArgument='<%# Eval("Notes").ToString()+";"+Eval("SID").ToString()+";"+Eval("ShowInScheduling").ToString()+";"+Eval("ShowOnApp").ToString() %>' />
                        <asp:Button runat="server" ID="btnDeleteNotes" OnClick="btnDeleteNotes_OnClick" Text="Delete"
                            Visible='<%# IsShowButton(Convert.ToBoolean(Eval("ISACTIVE"))) %>' BackColor="White" CommandArgument='<%# Eval("SID").ToString() %>' />
                        <asp:Button runat="server" ID="btnShowNotesTrail" OnClick="btnShowNotesTrail_OnClick"
                            Visible='<%# IsShowButton(Convert.ToBoolean(True)) %>' Text="History" BackColor="White" CommandArgument='<%# Eval("SID").ToString() %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
        </asp:GridView>
        <asp:Panel ID="pnlAddNotePopup" runat="server" BackColor="White" Width="340px">
            <table id="pnlAddNoteTable" width="300px" style='margin: 20px;'>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Add New Note</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <asp:Panel ID="pnlAddNoteMessage" runat="server" Visible="false">
                            <asp:Label ID="lblAddNoteMessage" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox Text="Show in Scheduling" runat="server" ID="chkScheduling" />
                        <br />
                        <asp:CheckBox Text="Show on App" runat="server" ID="chkOnApp" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 5px;">Note:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtNote" runat="server" Width="200" TextMode="MultiLine" Rows="4"
                            MaxLength="300">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID="revTexbox3" runat="server" ErrorMessage="<br/>You must enter up to a maximum of 300 caracteres"
                            ValidationExpression="^([\S\s]{0,300})$" ControlToValidate="txtNote" Display="Dynamic"
                            ForeColor="Red" ValidationGroup="AddNotes"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" ValidationGroup="AddNotes" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </contenttemplate>
</asp:updatepanel>
<asp:label id="lblDispPhotoGraph" runat="server"></asp:label>
<ajaxtoolkit:modalpopupextender id="mdlPopUpAddNote" runat="server" dynamicservicepath=""
    enabled="True" popupcontrolid="pnlAddNotePopup" dropshadow="true" cancelcontrolid="btnCancel"
    targetcontrolid="lblDispPhotoGraph" backgroundcssclass="modalBackground">
</ajaxtoolkit:modalpopupextender>
<asp:label id="Label3" runat="server"></asp:label>
<%------- Notes Trail Section ---------%>
<asp:updatepanel id="UpdatePanel1" runat="server">
    <contenttemplate>
        <asp:Panel ID="pnlPopupNotesTrail" runat="server" class="modalPopupSchedular" Style="min-width: 600px;
            max-width: 750px; border-width: 1px; border: 1px solid black">
            <div style="overflow: auto; max-height: 300px;">
                <asp:ImageButton ID="imgBtnCancelAlertPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    BorderWidth="0" />
                <asp:GridView ID="grdItemNotesTrail" runat="server" AutoGenerateColumns="false" GridLines="None"
                    ShowHeaderWhenEmpty="false" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="CreatedOn" HeaderText="Date:">
                            <ItemStyle  VerticalAlign="Top" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Notes" HeaderText="Notes">
                            <ItemStyle  VerticalAlign="Top" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CreatedBy" HeaderText="By:">
                            <ItemStyle  VerticalAlign="Top"/>
                        </asp:BoundField>           
                        <asp:BoundField DataField="ShowInScheduling" HeaderText="ShowInScheduling:">
                            <ItemStyle VerticalAlign="Top"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="ShowOnApp" HeaderText="ShowOnApp:">
                            <ItemStyle  VerticalAlign="Top"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="NotesStatus" HeaderText="Status:">
                            <ItemStyle VerticalAlign="Top"/>
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                </asp:GridView>
            </div>
        </asp:Panel>
    </contenttemplate>
</asp:updatepanel>
<asp:modalpopupextender id="mdlPopupNotesTrail" runat="server" dynamicservicepath=""
    targetcontrolid="lblPopupNotesTrailClose" popupcontrolid="pnlPopupNotesTrail"
    cancelcontrolid="imgBtnCancelAlertPopup" dropshadow="true" backgroundcssclass="modalBackground">
</asp:modalpopupextender>
<asp:label id="lblPopupNotesTrailClose" runat="server" text=""></asp:label>
<%------- Edit Notes Section ---------%>
<asp:updatepanel id="UpdatePanel2" runat="server">
    <contenttemplate>
        <asp:Panel ID="pnlPopUpEditNotes" runat="server" BackColor="White" Width="340px">
            <table id="Table1" width="300px" style='margin: 20px;'>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Edit Note</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <asp:Panel ID="pnlEditNoteMessage" runat="server" Visible="false">
                            <asp:Label ID="lblEditNoteMessage" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox Text="Show in Scheduling" runat="server" ID="chkEditScheduling" />
                        <br />
                        <asp:CheckBox Text="Show on App" runat="server" ID="chkEditOnApp" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 5px;">Note:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtEditNote" runat="server" Width="200" TextMode="MultiLine" Rows="4"
                            MaxLength="300">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>You must enter up to a maximum of 300 caracteres"
                            ValidationExpression="^([\S\s]{0,300})$" ControlToValidate="txtEditNote" Display="Dynamic"
                            ForeColor="Red" ValidationGroup="EditNotes"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnEditCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnEdit" runat="server" Text="Update" BackColor="White" ValidationGroup="EditNotes" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </contenttemplate>
</asp:updatepanel>
<asp:modalpopupextender id="mdlPopUpEditNotes" runat="server" dynamicservicepath=""
    targetcontrolid="lblPopUpEditNotes" popupcontrolid="pnlPopUpEditNotes" cancelcontrolid="btnEditCancel"
    dropshadow="true" backgroundcssclass="modalBackground">
</asp:modalpopupextender>
<asp:label id="lblPopUpEditNotes" runat="server" text=""></asp:label>