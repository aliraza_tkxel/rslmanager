﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class DetailsTab
    Inherits UserControlBase
    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Dim _readOnly As Boolean = False
#Region "Events"
#Region "Page Load"
    ''' <summary>
    ''' Page Load click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
            pnlDetailsTab.Enabled = False
            pnlOtherDetailsTab.Enabled = False
        End If
        Dim uiHelper As UIMessageHelper = New UIMessageHelper()
        uiHelper.resetMessage(lblMessage, pnlMessage)

    End Sub
#End Region

#End Region


    
End Class