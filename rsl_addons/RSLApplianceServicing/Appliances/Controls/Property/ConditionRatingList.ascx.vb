﻿Imports AS_BusinessLogic
Imports AS_Utilities

Public Class ConditionRatingList
    Inherits UserControlBase

#Region "Properties/Attributes"

    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Location", 1, 30)
    Public ReadOnly Property BackToSummarybutton As Button
        Get
            Return btnBackToSummary
        End Get
    End Property

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

    Protected Sub grdConditionRatings_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdConditionRatings.Sorting
       
        Dim ds As New DataSet

        objPageSortBo = getPageSortBoViewState()
        objPageSortBo.SortExpression = e.SortExpression
        objPageSortBo.setSortDirection()
        setPageSortBoViewState(objPageSortBo)

        ds = SessionManager.getconditionRatingDataSet()
        ds.Tables(0).DefaultView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
        grdConditionRatings.DataSource = ds.Tables(0).DefaultView
        grdConditionRatings.DataBind()

    End Sub

#End Region

#Region "Functions"

#Region "Populate condition rating list"
    ''' <summary>
    ''' Populate condition rating list
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Public Sub populateConditionRatingList(Optional ByVal propertyId As String = Nothing)
        If propertyId Is Nothing Then
            propertyId = getPropertyIdfromQueryString()
        End If

        Dim resultDataSet As DataSet = New DataSet()
        Dim objPropertyBL As PropertyBL = New PropertyBL()
        objPropertyBL.getPropertyConditionRatingDetail(resultDataSet, propertyId)
        SessionManager.setconditionRatingDataSet(resultDataSet)
        grdConditionRatings.DataSource = resultDataSet.Tables(0).DefaultView
        grdConditionRatings.DataBind()

        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            btnPrint.Visible = True
        Else
            btnPrint.Visible = False
        End If

    End Sub

#End Region

#Region "Show Condition Rating List"
    ''' <summary>
    ''' Show Condition Rating List
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Public Sub showConditionRatingList(ByVal propertyId As String)
        pnlConditionRating.Visible = True
    End Sub

#End Region

#Region "Hide Condition Rating List"
    ''' <summary>
    ''' Hide Condition Rating List
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Public Sub hideConditionRatingList(ByVal propertyId As String)
        pnlConditionRating.Visible = False
    End Sub

#End Region

#Region "Get Property Id from Query String"
    ''' <summary>
    ''' Get propertyId from query string, using the key stored in pathconstants (PathConstants.PropertyIds = "id")
    ''' </summary>
    ''' <param name="propertyId">Optional propertyId by ref to store propertyid.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getPropertyIdfromQueryString(Optional ByRef propertyId As String = Nothing) As String
        If Request.QueryString(PathConstants.PropertyIds) IsNot Nothing Then
            propertyId = CType(Request.QueryString(PathConstants.PropertyIds), String)
        End If
        Return propertyId
    End Function
#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region


#End Region

End Class