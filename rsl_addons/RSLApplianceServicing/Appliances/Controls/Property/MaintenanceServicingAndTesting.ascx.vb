﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_BusinessLogic
Imports AS_BusinessObject

Public Class MaintenanceServicingAndTesting
    Inherits UserControlBase
    Dim _readOnly As Boolean = False
    Dim objPropertyBL As PropertyBL = New PropertyBL()
    Enum MSTType
        PATTesting = 1
        MaintenanceItem = 2
        ServiceCharge = 3
        MEServicing = 4
    End Enum
#Region "Events"
#Region "Page load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
            MSTPanel.Enabled = False
        End If
        If Not IsPostBack Then
            loadCycleDdl()
        End If
    End Sub
#End Region
#Region "rdBtnPATTesting SelectedIndexChanged"
    Protected Sub rdBtnPATTesting_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdBtnPATTesting.SelectedIndexChanged
        Try
            If rdBtnPATTesting.SelectedValue = 1 Then
                pnlPATTesting.Visible = True
            Else
                pnlPATTesting.Visible = False
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        End Try
    End Sub
#End Region
#End Region
#Region "Functions"
#Region "Reset all control"

    Public Sub resetMSTControl()
        If rdBtnPATTesting.SelectedIndex >= 0 Then
            rdBtnPATTesting.ClearSelection()
        End If
        If rdBtnRegularMaintenance.SelectedIndex >= 0 Then
            rdBtnRegularMaintenance.ClearSelection()
        End If
        If rdBtnServiceCharge.SelectedIndex >= 0 Then
            rdBtnServiceCharge.ClearSelection()
        End If
        If rdBtnServicingRequired.SelectedIndex >= 0 Then
            rdBtnServicingRequired.ClearSelection()
        End If

        pnlPATTesting.Visible = False

        txtTestDate.Text = String.Empty
        txtTestingCycle.Text = String.Empty
        ddlTestingCycle.SelectedIndex = 0
        txtNextTestDate.Text = String.Empty

        txtPreviousMaintenance.Text = String.Empty
        txtMaintenanceCycle.Text = String.Empty
        ddlMaintenanceCycle.SelectedIndex = 0
        txtNextMaintenance.Text = String.Empty

        txtApportionment.Text = String.Empty

        txtLastServiceDate.Text = String.Empty
        txtServiceCycle.Text = String.Empty
        ddlServiceCycle.SelectedIndex = 0
        txtNextServiceDate.Text = String.Empty

    End Sub
#End Region

#Region "populate control"
    Public Sub populateMST()
        Dim parameterDataSet As DataSet = SessionManager.getAttributeDetailDataSet()
        If parameterDataSet.Tables(ApplicationConstants.mst).Rows.Count > 0 Then

            Dim serviceChargeResult = (From ps In parameterDataSet.Tables(ApplicationConstants.mst) Where ps.Item("MSATTypeId") = MSTType.ServiceCharge Select ps)
            If serviceChargeResult.Count > 0 Then
                If Not IsDBNull(serviceChargeResult.First.Item("IsRequired")) Then
                    rdBtnServiceCharge.SelectedValue = If(serviceChargeResult.First.Item("IsRequired") = True, "1", "0")
                End If
                txtApportionment.Text = If(Not IsDBNull(serviceChargeResult.First.Item("AnnualApportionment")), serviceChargeResult.First.Item("AnnualApportionment"), String.Empty)
                ' txtParameter.Text = mCycleResult.First.Item("DueDate")

            End If
            Dim PATTestingResult = (From ps In parameterDataSet.Tables(ApplicationConstants.mst) Where ps.Item("MSATTypeId") = MSTType.PATTesting Select ps)
            If PATTestingResult.Count > 0 Then
                If Not IsDBNull(PATTestingResult.First.Item("IsRequired")) Then
                    rdBtnPATTesting.SelectedValue = If(PATTestingResult.First.Item("IsRequired") = True, "1", "0")
                End If
                txtTestDate.Text = If(Not IsDBNull(PATTestingResult.First.Item("LastDate")), PATTestingResult.First.Item("LastDate"), String.Empty)
                txtTestingCycle.Text = If(Not IsDBNull(PATTestingResult.First.Item("Cycle")), PATTestingResult.First.Item("Cycle"), String.Empty)
                txtNextTestDate.Text = If(Not IsDBNull(PATTestingResult.First.Item("NextDate")), PATTestingResult.First.Item("NextDate"), String.Empty)
                ddlTestingCycle.SelectedValue = If(Not IsDBNull(PATTestingResult.First.Item("CycleTypeId")), PATTestingResult.First.Item("CycleTypeId").ToString(), "1")
                If rdBtnPATTesting.SelectedValue = "1" Then
                    pnlPATTesting.Visible = True
                End If

            End If

            Dim maintenanceItemResult = (From ps In parameterDataSet.Tables(ApplicationConstants.mst) Where ps.Item("MSATTypeId") = MSTType.MaintenanceItem Select ps)
            If maintenanceItemResult.Count > 0 Then
                If Not IsDBNull(maintenanceItemResult.First.Item("IsRequired")) Then
                    rdBtnRegularMaintenance.SelectedValue = If(maintenanceItemResult.First.Item("IsRequired") = True, "1", "0")
                End If
                txtPreviousMaintenance.Text = If(Not IsDBNull(maintenanceItemResult.First.Item("LastDate")), maintenanceItemResult.First.Item("LastDate"), String.Empty)
                txtMaintenanceCycle.Text = If(Not IsDBNull(maintenanceItemResult.First.Item("Cycle")), maintenanceItemResult.First.Item("Cycle"), String.Empty)
                txtNextMaintenance.Text = If(Not IsDBNull(maintenanceItemResult.First.Item("NextDate")), maintenanceItemResult.First.Item("NextDate"), String.Empty)
                ddlMaintenanceCycle.SelectedValue = If(Not IsDBNull(maintenanceItemResult.First.Item("CycleTypeId")), maintenanceItemResult.First.Item("CycleTypeId").ToString(), "1")


            End If
            Dim MEServicingResult = (From ps In parameterDataSet.Tables(ApplicationConstants.mst) Where ps.Item("MSATTypeId") = MSTType.MEServicing Select ps)
            If MEServicingResult.Count > 0 Then
                If Not IsDBNull(MEServicingResult.First.Item("IsRequired")) Then
                    rdBtnServicingRequired.SelectedValue = If(MEServicingResult.First.Item("IsRequired") = True, "1", "0")
                End If
                txtLastServiceDate.Text = If(Not IsDBNull(MEServicingResult.First.Item("LastDate")), MEServicingResult.First.Item("LastDate"), String.Empty)
                txtServiceCycle.Text = If(Not IsDBNull(MEServicingResult.First.Item("Cycle")), MEServicingResult.First.Item("Cycle"), String.Empty)
                txtNextServiceDate.Text = If(Not IsDBNull(MEServicingResult.First.Item("NextDate")), MEServicingResult.First.Item("NextDate"), String.Empty)
                ddlServiceCycle.SelectedValue = If(Not IsDBNull(MEServicingResult.First.Item("CycleTypeId")), MEServicingResult.First.Item("CycleTypeId").ToString(), "1")


            End If
        End If
    End Sub
#End Region

#Region "load Cycle Ddl"
    Public Sub loadCycleDdl()
        Dim resultDataSet As DataSet = New DataSet()
        Try

            objPropertyBL.loadCycleDdl(resultDataSet)
            populateDropdown(ddlTestingCycle, resultDataSet)
            populateDropdown(ddlMaintenanceCycle, resultDataSet)
            populateDropdown(ddlServiceCycle, resultDataSet)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                '  uiMessageHelper.setMessage(lblAddApplianceMessage, pnlAddApplianceMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "fill MSAT Object"

    Public Sub fillMSATObject(ByRef msatDetailList As List(Of MSATDetailBO))

        '#Region "add PAT Testing object in msatDetailList"
        If (rdBtnPATTesting.SelectedItem IsNot Nothing) Then
            If Convert.ToInt32(rdBtnPATTesting.SelectedValue) > 0 Then
                Dim valid As Boolean = False

                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnPATTesting.SelectedValue))
                valid = Validation.validateDateFormate(txtTestDate.Text)
                If valid = True Then
                    objmsatDetailBo.LastDate = Convert.ToDateTime(txtTestDate.Text)
                Else
                    pnlPATTesting.Style.Add("display", "table-row")
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidPATDateFormat
                    Exit Sub
                End If
                If Not String.IsNullOrEmpty(txtTestingCycle.Text) Then
                    objmsatDetailBo.Cycle = Convert.ToInt32(txtTestingCycle.Text)
                Else

                    pnlPATTesting.Style.Add("display", "table-row")
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.EnterTestingCycle
                    Exit Sub
                End If

                objmsatDetailBo.CycleTypeId = Convert.ToInt32(ddlTestingCycle.SelectedValue)
                valid = Validation.validateDateFormate(txtNextTestDate.Text)
                If valid = True Then
                    objmsatDetailBo.NextDate = Convert.ToDateTime(txtNextTestDate.Text)
                Else


                    pnlPATTesting.Style.Add("display", "table-row")
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidPATDateFormat
                    Exit Sub
                End If
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.PATTesting)
                msatDetailList.Add(objmsatDetailBo)
            Else
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnPATTesting.SelectedValue))
                objmsatDetailBo.LastDate = Nothing
                objmsatDetailBo.Cycle = Nothing
                objmsatDetailBo.CycleTypeId = Nothing
                objmsatDetailBo.NextDate = Nothing
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.PATTesting)
                msatDetailList.Add(objmsatDetailBo)
            End If
        End If
        '#End Region
        '#Region "add Maintenance item object in msatDetailList"
        If (rdBtnRegularMaintenance.SelectedItem IsNot Nothing) Then
            If Convert.ToInt32(rdBtnRegularMaintenance.SelectedValue) > 0 Then
                Dim valid As Boolean = False
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnRegularMaintenance.SelectedValue))
                valid = Validation.validateDateFormate(txtPreviousMaintenance.Text)
                If valid = True Then
                    objmsatDetailBo.LastDate = Convert.ToDateTime(txtPreviousMaintenance.Text)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidMaintenanceDateFormat
                    Exit Sub
                End If
                If Not String.IsNullOrEmpty(txtMaintenanceCycle.Text) Then
                    objmsatDetailBo.Cycle = Convert.ToInt32(txtMaintenanceCycle.Text)
                Else

                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.EnterMaintenanceCycle
                    Exit Sub
                End If
                objmsatDetailBo.CycleTypeId = Convert.ToInt32(ddlMaintenanceCycle.SelectedValue)
                valid = Validation.validateDateFormate(txtNextMaintenance.Text)
                If valid = True Then
                    objmsatDetailBo.NextDate = Convert.ToDateTime(txtNextMaintenance.Text)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidMaintenanceDateFormat
                    Exit Sub
                End If
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MaintenanceItem)
                msatDetailList.Add(objmsatDetailBo)
            Else
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnRegularMaintenance.SelectedValue))
                objmsatDetailBo.LastDate = Nothing
                objmsatDetailBo.Cycle = Nothing
                objmsatDetailBo.CycleTypeId = Nothing
                objmsatDetailBo.NextDate = Nothing
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MaintenanceItem)
                msatDetailList.Add(objmsatDetailBo)
            End If
        End If
        '#End Region

        '#Region "add Service charge item object in msatDetailList"

        If (rdBtnServiceCharge.SelectedItem IsNot Nothing) Then
            If Convert.ToInt32(rdBtnServiceCharge.SelectedValue) > 0 Then
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServiceCharge.SelectedValue))
                If Not String.IsNullOrEmpty(txtApportionment.Text) Then
                    objmsatDetailBo.AnnualApportionment = Convert.ToDecimal(txtApportionment.Text)
                End If
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.ServiceCharge)
                msatDetailList.Add(objmsatDetailBo)
            Else
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.AnnualApportionment = Nothing
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServiceCharge.SelectedValue))
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.ServiceCharge)
                msatDetailList.Add(objmsatDetailBo)
            End If
        End If
        '#End Region

        '#Region "add M&E Servicing item object in msatDetailList"
        If (rdBtnServicingRequired.SelectedItem IsNot Nothing) Then
            If Convert.ToInt32(rdBtnServicingRequired.SelectedValue) > 0 Then
                Dim valid As Boolean = False
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServicingRequired.SelectedValue))
                valid = Validation.validateDateFormate(txtLastServiceDate.Text)
                If valid = True Then
                    objmsatDetailBo.LastDate = Convert.ToDateTime(txtLastServiceDate.Text)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidMEDateFormat
                    Exit Sub
                End If
                If Not String.IsNullOrEmpty(txtServiceCycle.Text) Then
                    objmsatDetailBo.Cycle = Convert.ToInt32(txtServiceCycle.Text)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.EnterServiceCycle
                    Exit Sub
                End If

                objmsatDetailBo.CycleTypeId = Convert.ToInt32(ddlServiceCycle.SelectedValue)
                valid = Validation.validateDateFormate(txtNextServiceDate.Text)
                If valid = True Then
                    objmsatDetailBo.NextDate = Convert.ToDateTime(txtNextServiceDate.Text)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.NotValidMEDateFormat
                    Exit Sub
                End If
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MEServicing)
                msatDetailList.Add(objmsatDetailBo)
            Else
                Dim objmsatDetailBo As New MSATDetailBO()
                objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServicingRequired.SelectedValue))
                objmsatDetailBo.LastDate = Nothing
                objmsatDetailBo.Cycle = Nothing
                objmsatDetailBo.CycleTypeId = Nothing
                objmsatDetailBo.NextDate = Nothing
                objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MEServicing)
                msatDetailList.Add(objmsatDetailBo)
            End If
        End If
        '#End Region

    End Sub
#End Region
#Region "Populate dropdown"
    Private Sub populateDropdown(ByRef ddlLookup As DropDownList, ByRef resultDataSet As DataSet)
        ddlLookup.Items.Clear()
        ddlLookup.DataSource = resultDataSet
        ddlLookup.DataValueField = "CycleTypeId"
        ddlLookup.DataTextField = "CycleType"
        ddlLookup.DataBind()
        'ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))
        'ddlLookup.SelectedIndex = 0

    End Sub

#End Region






#End Region

End Class