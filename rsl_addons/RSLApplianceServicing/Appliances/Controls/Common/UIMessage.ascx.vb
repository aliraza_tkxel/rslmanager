﻿Imports System.ComponentModel
Imports System.Drawing

Partial Public Class UIMessage
    Inherits UserControlBase

    Private Const DefaultWidth As String = "400px"
    Private Const StatusViewState As String = "_STY"

    Public Property bubbleType As UIMessageControlType
        Get
            Dim item As Object = Me.ViewState("_STY")

            If item Is Nothing Then
                Return UIMessageControlType.Information
            End If

            Return CType(item, UIMessageControlType)
        End Get
        Set(ByVal value As UIMessageControlType)
            Me.ViewState("_STY") = value
            Dim statusControlType As UIMessageControlType = value

            Select Case statusControlType
                Case UIMessageControlType.Information
                    Me.StatusBox.Attributes("class") = "StatusInfo"
                    Return
                Case UIMessageControlType.Warning
                    Me.StatusBox.Attributes("class") = "StatusWarning"
                    Return
                Case UIMessageControlType.[Error]
                    Me.StatusBox.Attributes("class") = "StatusError"
                    Return
                Case Else
                    Return
            End Select
        End Set
    End Property

    <Bindable(True)>
    <Category("Appearance")>
    <Description("Text color.")>
    Public Property messageTextColor As Color
        Get
            Return Me.StatusLabel.ForeColor
        End Get
        Set(ByVal value As Color)
            Me.StatusLabel.ForeColor = value
        End Set
    End Property

    <Bindable(True)>
    <Category("Appearance")>
    <Description("Text that appears inside the text entry field.")>
    Public Property messageText As String
        Get
            Return Me.StatusLabel.Text
        End Get
        Set(ByVal value As String)
            Me.StatusLabel.Text = value
        End Set
    End Property

    <Bindable(True)>
    <Category("Appearance")>
    <DefaultValue("400px")>
    <Description("Width of the control in CSS units.")>
    Public Property width As String
        Get
            Dim item As Object = Me.ViewState("width")

            If item Is Nothing Then
                Return "400px"
            End If

            Return CStr(item)
        End Get
        Set(ByVal value As String)
            Me.ViewState("width") = value
        End Set
    End Property

    Public isExceptionLogged As Boolean = False
    Public isError As Boolean = False

    Public Sub addLinkButton(ByVal text As String, ByVal clientEvent As String)
        Dim literal As Literal = New Literal()
        literal.Text = String.Format("<span style=""color:#999;"">&nbsp;[&nbsp;<a href=""javascript:{1}"">{0}</a>&nbsp;]</span>", text, clientEvent)
        Me.StatusBox.Controls.AddAt(2, literal)
    End Sub

    Public Sub hideMessage()
        Me.Visible = False
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)
        Me.StatusBox.Style("width") = Me.width
    End Sub

    Public Sub showErrorMessage(ByVal text As String)
        Me.Visible = True
        Me.bubbleType = UIMessageControlType.[Error]
        Me.messageText = text
        Me.messageTextColor = Color.Red
    End Sub

    Public Sub showInformationMessage(ByVal text As String)
        Me.Visible = True
        Me.bubbleType = UIMessageControlType.Information
        Me.messageText = text
        Me.messageTextColor = Color.Green
    End Sub

    Public Sub showWarningMessage(ByVal text As String)
        Me.Visible = True
        Me.bubbleType = UIMessageControlType.Warning
        Me.messageText = text
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
End Class

Public Enum UIMessageControlType
    Information
    Warning
    [Error]
End Enum
