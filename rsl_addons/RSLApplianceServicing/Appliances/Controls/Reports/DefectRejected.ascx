﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectRejected.ascx.vb"
    Inherits="Appliances.DefectRejected" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="updPanelRejected" runat="server">
    <ContentTemplate>
        <style type="text/css">
            .dashboard th
            {
                background: #fff;
                border-bottom: 4px solid #8b8687;
            }
            .dashboard th a
            {
                color: #000 !important;
            }
            .dashboard th img
            {
                float: right;
            }
            .dashboard, .dashboard td
            {
                padding: 5px 5px 5px 5px !important;
            }
        </style>
        <div style="height: auto; overflow: auto;">
            <asp:Panel ID="pnlReport" runat="server" Visible="true">
                <asp:Panel ID="pnlMessage" runat="server" Visible="True">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div style="border-bottom: 1px solid #A0A0A0; min-height: 600px; width: 100%; padding: 0;
                    overflow: auto;">
                    <cc1:PagingGridView ID="grdDefect" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard dashboard-border webgrid table table-responsive"
                        EmptyDataText="No Records Found" GridLines="None" ShowHeaderWhenEmpty="True"
                        AllowPaging="True" OnRowCreated="grdDefect_RowCreated" OnRowDataBound="DefectRejectGridView_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBtnAddress" OnClick="lnkBtnAddress_Click" Font-Bold="true"
                                        Font-Underline="true" CommandArgument='<%# Bind("PropertyId") %>' ForeColor="Blue"
                                        runat="server" Text='<%# Bind("Address") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="140px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBtnScheme" OnClick="lnkBtnAddress_Click" Font-Bold="true"
                                        Font-Underline="true" CommandArgument='<%# Eval("PropertyId").ToString() + ";" + Eval("AppointmentType").ToString() %>'
                                        ForeColor="Blue" runat="server" Text='<%# Bind("Scheme") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="140px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Block" SortExpression="Block">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBtnBlock" Font-Bold="true" ForeColor="Blue" runat="server"
                                        Text='<%# Bind("Block") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="140px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="110px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Appliance" SortExpression="Appliance">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppliance" runat="server" Text='<%# Bind("Appliance") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="135px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" SortExpression="DefectCategory">
                                <ItemTemplate>
                                    <asp:Label ID="lblDefectCategory" runat="server" Text='<%# Bind("DefectCategory") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="140px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reported By" SortExpression="RecordedBy">
                                <ItemTemplate>
                                    <asp:Label ID="lblBy" runat="server" Text='<%# Bind("RecordedBy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rejected By" SortExpression="RejectedByAt">
                                <ItemTemplate>
                                    <asp:Label ID="lblRejectedByAt" runat="server" Text='<%# Bind("RejectedByAt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="145px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason" SortExpression="RejectionReason">
                                <ItemTemplate>
                                    <asp:Label ID="lblRejectionReason" runat="server" Text='<%# Bind("RejectionReason") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="140px" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkBtnView" OnClick="lnkBtnView_Click" Font-Bold="true"
                                        Font-Underline="true" CommandArgument='<%# Bind("RejectionNotes") %>' ForeColor="Blue"
                                        Text="View"></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkBtnUnhold" OnClick="lnkBtnUnhold_Click" Font-Bold="true"
                                        Font-Underline="true" CommandArgument='<%# Bind("DefectId") %>' ForeColor="Blue"
                                        Text="UnHold"></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                            </asp:TemplateField>
                        </Columns>
                        <%-- Grid View Styling Start --%>
                    </cc1:PagingGridView>
                </div>
                <%--Pager Template Start--%>
                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                    margin: 0 auto; width: 98%;" CssClass="pnlPaging">
                    <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                        <div class="paging-left">
                            <span style="padding-right: 10px;">
                                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                    CssClass="lnk-btn">
                                        &lt;&lt;First
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                    CommandArgument="Prev" CssClass="lnk-btn">
                                        &lt;Prev
                                </asp:LinkButton>
                            </span><span style="padding-right: 10px;"><b>Page:</b>
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                of
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. </span><span style="padding-right: 20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span><span style="padding-right: 10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" CssClass="lnk-btn">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" CssClass="lnk-btn">
                                        
                                    </asp:LinkButton>
                                </span>
                        </div>
                        <div style="float: right;">
                            <span>
                                <asp:Panel ID="pnlExportToExcel" runat="server" HorizontalAlign="Right" Style="display: inline;">
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                                        class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;" />
                                </asp:Panel>
                            </span>
                            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                            <div class="field" style="margin-right: 10px;">
                                <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                    PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                            </div>
                            <span>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                    UseSubmitBehavior="false" class="btn btn-xs btn-blue" Style="padding: 1px 5px !important;
                                    margin-right: 10px; min-width: 0px;" OnClick="changePageNumber" />
                            </span>
                            <%--<asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />--%>
                        </div>
                    </div>
                </asp:Panel>
                <%--<asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="height: 30px;
                color: Black; background-color: #FFFFFF; border-left: 1px solid #ADADAD; border-right: 1px solid #ADADAD;
                width: 99%; vertical-align: middle;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td align="right">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                    CommandArgument="First" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Previous" runat="server" CommandName="Page"
                                                    CommandArgument="Prev" />
                                                &nbsp; &nbsp &nbsp
                                            </td>
                                            <td>
                                                Records:
                                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                to
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                of
                                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                &nbsp &nbsp Page:
                                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                of
                                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                &nbsp &nbsp
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                    CommandArgument="Next" />
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                    CommandArgument="Last" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td align="right">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                <asp:TextBox Visible="false" ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                <asp:Button Visible="false" Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                    ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>--%>
                <%--Pager Template End--%>
                <%--<asp:Panel ID="pnlExportToExcel" runat="server" HorizontalAlign="Right">
                <div style="width: 99%; float: left; border: 1px solid #ADADAD;">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Panel ID="pnlInner" runat="server" HorizontalAlign="Right">
                                    <asp:Button ID="btnExportToExcel" UseSubmitBehavior="false"  runat="server" Text="Export to XLS" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>--%>
                <asp:HiddenField ID="hdnSelectedConditionId" runat="server" />
                <!----------------------------------------------------- View  Popup-------------------------------------------------->
                <asp:UpdatePanel ID="updPanelViewPopup" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlViewCondition" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style="padding: 15px 5px; top: 0; left: 0; bottom: 0; right: 0;
                            border: 1px solid black; -bracket-: hack(; left: 430px !important; top: 100px;
                            );">
                            <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                                <b>Rejection Details </b>
                            </div>
                            <div style="clear: both; height: 1px;">
                            </div>
                            <hr />
                            <asp:ImageButton ID="imgBtnCloseViewCondition" runat="server" Style="position: absolute;
                                top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                            <asp:Panel ID="pnlViewErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblViewErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="margin-left: 10px; margin-right: 10px;">
                                <p>
                                    <strong>Reason: </strong>
                                    <br />
                                    <asp:Label ID="lblRejectionReason" runat="server">
                                    </asp:Label>
                                </p>
                                <p>
                                    <strong>Notes: </strong>
                                    <br />
                                    <asp:Label ID="lblRejectionNotes" runat="server">
                                    </asp:Label>
                                </p>
                                <br />
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopUpViewCondition" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblViewPopup" PopupControlID="pnlViewCondition"
                            DropShadow="true" CancelControlID="imgBtnCloseViewCondition" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblViewPopup" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
