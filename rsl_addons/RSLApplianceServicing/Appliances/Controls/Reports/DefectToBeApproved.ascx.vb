﻿Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports AS_BusinessLogic
Imports AS_BusinessObject

Public Class DefectToBeApproved
    Inherits UserControlBase

#Region "Attributes"
    Dim objPageSortBo As PageSortBO
#End Region

#Region "Properties"

#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
            scriptManager.RegisterPostBackControl(Me.btnExportToExcel)
            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

                'populateConditionRatingReport()
            End If

            pnlReport.Visible = True
            pnlMoreDetail.Visible = False
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Cancel Click"
    ''' <summary>
    ''' Button Cancel Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            mdlPopUpApproveCondition.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Confirm Click"
    ''' <summary>
    ''' Button Confirm Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirm.Click
        Try
            Dim objReportsBL As ReportBL = New ReportBL()
            Dim cds As ChangeDefectStatus = New ChangeDefectStatus()
            cds.UserId = SessionManager.getApplianceServicingId()
            cds.DefectId = Convert.ToInt32(hdnSelectedDefectId.Value)
            cds.Status = ApplicationConstants.approved
            Dim isApproved As Boolean = objReportsBL.changeDefectStatus(cds)
            'Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
            ''uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "2", False)

            If (isApproved) Then
                mdlPopUpApproveCondition.Hide()

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectToBeApprovedReport(resultDataSet, search, False)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkApproved, False)
                'Dim body As String = populateBodyApprovedConditionWorksEmail(objConditionEmailBo.RequestedBy, objConditionEmailBo.Component, objConditionEmailBo.Address, objConditionEmailBo.Towncity, objConditionEmailBo.Username, objConditionEmailBo.PostCode)
                ''uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "4", False)
                'If (sendEmail(body)) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApproved, False)
                'Else
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApprovedFailedEmail, False)
                'End If
                'updPanelTobeApproved.Update()
                ' updPanelConditionRating.Update()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkFailedToApprove, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Approved Email body"
    Private Function populateBodyApprovedConditionWorksEmail(ByVal requestedBy As String, ByVal Component As String, ByVal address1 As String, ByVal townCity As String, ByVal username As String, ByVal PostCode As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/ConditionRatingApproved.html"))
        body.Append(reader.ReadToEnd.ToString)
        body = body.Replace("{Address}", address1 + ", " + townCity + ", " + PostCode)
        body = body.Replace("{Component}", Component)
        body = body.Replace("{Requestedby}", requestedBy)
        body = body.Replace("{Approvedby}", username)
        Return body.ToString()
    End Function
#End Region


#Region "Populate Rejected Email body"
    Private Function populateBodyRejectedConditionWorksEmail(ByVal requestedBy As String, ByVal Component As String, ByVal address1 As String, ByVal townCity As String, ByVal username As String, ByVal PostCode As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/ConditionRatingRejected.html"))
        body.Append(reader.ReadToEnd.ToString)
        body = body.Replace("{Address}", address1 + ", " + townCity + ", " + PostCode)
        body = body.Replace("{Component}", Component)
        body = body.Replace("{Requestedby}", requestedBy)
        body = body.Replace("{Rejectedby}", username)
        Return body.ToString()
    End Function
#End Region
#Region "Button Reject Click"
    ''' <summary>
    ''' Button Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRejectbtnRejectDefectWorks_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRejectDefect.Click
        Try
            Dim cds As ChangeDefectStatus = New ChangeDefectStatus()
            cds.UserId = SessionManager.getApplianceServicingId()
            cds.DefectId = Convert.ToInt32(hdnSelectedDefectId.Value)
            cds.Status = ApplicationConstants.rejected
            cds.RejectionReasonNotes = txtRejectNotes.Text
            cds.RejectionReasonId = Convert.ToInt32(ddlReason.SelectedValue)
            validateAndRejectDefect(cds)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                mdlPopupRejectCondition.Show()
                uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")
            Dim propertyId As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)
            If (appointmentType = "Property") Then
                Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
            Else
                Response.Redirect(PathConstants.SchemeRecordDashboard + propertyId + "&requestType=scheme&src=search")
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button More Detail Click"
    ''' <summary>
    ''' Link Button More Detail Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnMoreDetail_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")

            Dim defectId As Integer = Convert.ToInt32(commandParams(0))
            Dim appointmentType As String = commandParams(1)
            ucDefectMoreDetail.populateMoreDetail(defectId, appointmentType)
            pnlReport.Visible = False
            pnlMoreDetail.Visible = True
            Dim txtSearch As TextBox = TryCast(Me.Parent.FindControl("txtSearch"), TextBox)
            'txtSearch.Visible = False
            'Dim updPanelDefect As UpdatePanel = TryCast(Me.Parent.FindControl("updPanelDefect"), UpdatePanel)
            'updPanelDefect.Update()
            Dim javascriptMethod As String = "hideSearchText('s')"
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "script", javascriptMethod, True)
            Dim updPanelDefect As UpdatePanel = TryCast(Me.Parent.FindControl("updPanelDefect"), UpdatePanel)
            updPanelDefect.Update()
            ' Page.ClientScript.RegisterStartupScript(Me.GetType(), "script", "hideSearchText()", True)
            'Response.Redirect(PathConstants.ConditionMoreDetailPath + "?" + PathConstants.Pmo + "=" + journalId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Approve Click"
    ''' <summary>
    ''' Link Button Approve Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnApprove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")

            Dim defectId As Integer = Convert.ToInt32(commandParams(0))
            Dim appointmentType As String = commandParams(1)

            Dim lnkBtnApprove As LinkButton = DirectCast(sender, LinkButton)
            hdnSelectedDefectId.Value = defectId
            Dim row As GridViewRow = DirectCast(lnkBtnApprove.NamingContainer, GridViewRow)
            Dim lnkBtnAddress As LinkButton
            If (appointmentType = "Block") Then
                lnkBtnAddress = DirectCast(row.FindControl("lnkBtnBlock"), LinkButton)
            ElseIf (appointmentType = "Scheme") Then
                lnkBtnAddress = DirectCast(row.FindControl("lnkBtnScheme"), LinkButton)
            Else
                lnkBtnAddress = DirectCast(row.FindControl("lnkBtnAddress"), LinkButton)
            End If

            'saveSmsBoInSession(row)
            saveEmailBoInSession(row)
            Dim propertyAddress As String = lnkBtnAddress.Text
            showApprovePopup(propertyAddress)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Link Button Reject Click"
    ''' <summary>
    ''' Link Button Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnReject_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim lnkBtnReject As LinkButton = DirectCast(sender, LinkButton)
            hdnSelectedDefectId.Value = lnkBtnReject.CommandArgument
            Dim row As GridViewRow = DirectCast(lnkBtnReject.NamingContainer, GridViewRow)
            saveSmsBoInSession(row)
            saveEmailBoInSession(row)
            showRejectPopup()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdDefect_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDefect.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdDefect.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectToBeApprovedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectToBeApprovedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectToBeApprovedReport(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Reset Approve Popup"
    ''' <summary>
    ''' Reset Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetApprovePopup()

        uiMessageHelper.resetMessage(lblApproveErrorMessage, pnlApproveErrorMessage)
        lblApprovePropertyAddress.Text = String.Empty

    End Sub

#End Region

#Region "Reset Reject Popup"
    ''' <summary>
    ''' Reset Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetRejectPopup()

        uiMessageHelper.resetMessage(lblRejectErrorMessage, pnlRejectErrorMessage)
        populateReasonDropdown()
        populateReplacementYearDropdown()
        txtRejectNotes.Text = String.Empty

    End Sub

#End Region

#Region "Save SMS BO"
    ''' <summary>
    ''' Save SMS BO
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveSmsBoInSession(ByVal row As GridViewRow)

        Dim hdnAddress1 As HiddenField = DirectCast(row.FindControl("hdnAddress1"), HiddenField)
        Dim hdnTowncity As HiddenField = DirectCast(row.FindControl("hdnTowncity"), HiddenField)
        Dim hdnMobile As HiddenField = DirectCast(row.FindControl("hdnMobile"), HiddenField)

        'Dim objSmsBo As ConditionSmsBO = New ConditionSmsBO()
        'objSmsBo.Address = hdnAddress1.Value()
        'objSmsBo.Towncity = hdnTowncity.Value()
        'objSmsBo.Mobile = hdnMobile.Value()
        'objSmsBo.Username = SessionManager.getUserFullName()
        'SessionManager.setConditionSmsBo(objSmsBo)

    End Sub

#End Region

#Region "Save Email BO"
    ''' <summary>
    ''' Save SMS BO
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveEmailBoInSession(ByVal row As GridViewRow)

        'Dim hdnAddress1 As HiddenField = DirectCast(row.FindControl("hdnAddress1"), HiddenField)
        'Dim hdnTowncity As HiddenField = DirectCast(row.FindControl("hdnTowncity"), HiddenField)
        'Dim hdnEmail As HiddenField = DirectCast(row.FindControl("hdnEmail"), HiddenField)
        'Dim hdnRequestedBy As HiddenField = DirectCast(row.FindControl("hdnRequestedBy"), HiddenField)
        'Dim Component As Label = DirectCast(row.FindControl("lblComponent"), Label)
        'Dim PostCode As Label = DirectCast(row.FindControl("lblPostcode"), Label)
        'Dim objEmailBo As ConditionEmailBO = New ConditionEmailBO()
        'objEmailBo.Address = hdnAddress1.Value()
        'objEmailBo.Towncity = hdnTowncity.Value()
        'objEmailBo.Username = SessionManager.getUserFullName()
        'objEmailBo.Email = hdnEmail.Value()
        'objEmailBo.RequestedBy = hdnRequestedBy.Value()
        'objEmailBo.Component = Component.Text
        'objEmailBo.PostCode = PostCode.Text
        'SessionManager.setConditionEmailBO(objEmailBo)

    End Sub

#End Region

#Region "Validate reject information"
    ''' <summary>
    ''' Validate reject information
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateAndRejectDefect(ByVal objRejectBO As ChangeDefectStatus)

        Dim results As ValidationResults = Microsoft.Practices.EnterpriseLibrary.Validation.Validation.Validate(objRejectBO)

        If results.IsValid Then
            Dim objReportsBL As ReportBL = New ReportBL()
            Dim isRejected As Boolean = objReportsBL.changeDefectStatus(objRejectBO)
            'Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
            ''uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "2", False)

            If (isRejected) Then
                mdlPopupRejectCondition.Hide()

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectToBeApprovedReport(resultDataSet, search, False)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkRejected, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkFailedToApprove, True)
            End If
        Else

            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = message

        End If

    End Sub

#End Region

#Region "Reject condition works"
    ''' <summary>
    ''' Reject condition works
    ''' </summary>
    ''' <remarks></remarks>
    'Sub rejectDefect(ByVal objRejectBO As RejectConditionBO)

    '    'Dim objReportsBL As ReportBL = New ReportBL()
    '    'Dim userId As Integer = SessionManager.getPlannedMaintenanceUserId()
    '    'Dim isRejected As Boolean = objReportsBL.rejectConditionWorks(objRejectBO)
    '    'Dim objConditionSmsBo As ConditionSmsBO = SessionManager.getConditionSmsBo()
    '    'Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
    '    'If (isRejected) Then
    '    '    mdlPopupRejectCondition.Hide()

    '    '    Dim search As String = ViewState.Item(ViewStateConstants.Search)
    '    '    Dim resultDataSet As DataSet = New DataSet()
    '    '    populateDefectToBeApprovedReport(resultDataSet, search, False)

    '    '    Dim body As String = GeneralHelper.populateBodyRejectConditionWorksSMS(objConditionSmsBo.Address, objConditionSmsBo.Towncity, objConditionSmsBo.Username)
    '    '    Dim emaillBody As String = populateBodyRejectedConditionWorksEmail(objConditionEmailBo.RequestedBy, objConditionEmailBo.Component, objConditionEmailBo.Address, objConditionEmailBo.Towncity, objConditionEmailBo.Username, objConditionEmailBo.PostCode)
    '    '    Dim isSMSSent As Boolean = sendSMS(body)
    '    '    Dim isEmailSent As Boolean = sendRejectedEmail(emaillBody)
    '    '    If (isSMSSent = True And isEmailSent = True) Then
    '    '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejected, False)
    '    '    ElseIf (isSMSSent = True And isEmailSent = False) Then
    '    '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedEmail, False)
    '    '    ElseIf (isSMSSent = False And isEmailSent = True) Then
    '    '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedSms, False)
    '    '    ElseIf (isSMSSent = False And isEmailSent = False) Then
    '    '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkRejectedFailedSmsEmail, False)
    '    '    End If

    '    '    '  updPanelConditionRating.Update()

    '    'Else
    '    '    uiMessageHelper.setMessage(lblRejectErrorMessage, pnlRejectErrorMessage, UserMessageConstants.ConditionWorkFailedToRejected, True)
    '    'End If

    'End Sub

#End Region

#Region "Send SMS"
    Private Function sendSMS(ByVal body As String)
        Dim isMessageSent As Boolean = False

        'Try
        '    Dim objConditionSmsBo As ConditionSmsBO = SessionManager.getConditionSmsBo()
        '    Dim results As ValidationResults = Validation.Validate(objConditionSmsBo)

        '    'If results.IsValid Then
        '    If objConditionSmsBo.Mobile.Length >= 10 Then

        '        Dim smsUrl As String = ResolveClientUrl(ApplicationConstants.SMSUrl)


        '        Dim javascriptMethod As String = "sendSMS('" + objConditionSmsBo.Mobile + "','" + body + "','" + smsUrl + "')"
        '        ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
        '        isMessageSent = True
        '    Else
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
        '        isMessageSent = False
        '    End If
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.Message = ex.Message
        '    isMessageSent = False

        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If

        'Finally
        '    If uiMessageHelper.IsError = True Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
        '        isMessageSent = False
        '    End If
        'End Try

        Return isMessageSent

    End Function
#End Region

#Region "Send Email"
    Private Function sendEmail(ByVal body As String)
        Dim isEmailSent As Boolean = False

        'Try
        '    Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
        '    Dim results As ValidationResults = Validation.Validate(objConditionEmailBo)
        '    If results.IsValid Then
        '        EmailHelper.sendHtmlFormattedEmailForConditionWorksApproved(objConditionEmailBo.RequestedBy, ApplicationConstants.replyTo, objConditionEmailBo.Email, ApplicationConstants.Subject, body)
        '        isEmailSent = True
        '    Else
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
        '        isEmailSent = False
        '    End If
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.Message = ex.Message
        '    isEmailSent = False

        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If

        'Finally
        '    If uiMessageHelper.IsError = True Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
        '        isEmailSent = False
        '    End If
        'End Try
        Return isEmailSent
    End Function
#End Region
#Region "Send Email"
    Private Function sendRejectedEmail(ByVal body As String)
        Dim isEmailSent As Boolean = False

        'Try
        '    Dim objConditionEmailBo As ConditionEmailBO = SessionManager.getConditionEmailBo()
        '    Dim results As ValidationResults = Validation.Validate(objConditionEmailBo)
        '    If results.IsValid Then
        '        EmailHelper.sendHtmlFormattedEmailForConditionWorksApproved(objConditionEmailBo.RequestedBy, ApplicationConstants.replyTo, objConditionEmailBo.Email, ApplicationConstants.rejectedSubject, body)
        '        isEmailSent = True
        '    Else
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
        '        isEmailSent = False
        '    End If
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.Message = ex.Message
        '    isEmailSent = False

        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If

        'Finally
        'End Try

        Return isEmailSent

    End Function
#End Region

#Region "Show Approve Popup"
    ''' <summary>
    ''' Show Approve Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showApprovePopup(ByVal propertyAddress As String)

        resetApprovePopup()
        lblApprovePropertyAddress.Text = propertyAddress
        mdlPopUpApproveCondition.Show()

    End Sub

#End Region

#Region "Show Reject Popup"
    ''' <summary>
    ''' Show Reject Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showRejectPopup()

        resetRejectPopup()
        mdlPopupRejectCondition.Show()

    End Sub

#End Region

#Region "Populate replacement year"
    ''' <summary>
    ''' Populate replacement year from 2000 to 2050
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReplacementYearDropdown()

        'ddlReplacementDue.Items.Clear()

        'Dim year As Integer = 2000

        'While (year <= ApplicationConstants.YearEndRange)
        '    Dim yearItem As ListItem = New ListItem(Convert.ToString(year), Convert.ToString(year))
        '    ddlReplacementDue.Items.Add(yearItem)
        '    year = year + 1
        'End While

        'Dim item As ListItem = New ListItem("YYYY", "-1")
        'ddlReplacementDue.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate reason dropdown"
    ''' <summary>
    ''' Populate reason dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReasonDropdown()

        ddlReason.Items.Clear()

        Dim objReportsBl As ReportBL = New ReportBL()
        Dim resultDataset As DataSet = New DataSet()
        objReportsBl.getDefecteasons(resultDataset)

        ddlReason.DataSource = resultDataset.Tables(0).DefaultView
        ddlReason.DataValueField = "ReasonId"
        ddlReason.DataTextField = "Reason"
        ddlReason.DataBind()


        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlReason.Items.Insert(0, item)

    End Sub

#End Region



#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectToBeApprovedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportBL = New ReportBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If

        Dim totalCount As Integer = Convert.ToInt32(objReportsBl.getDefectToBeApprovedReport(resultDataSet, search, objPageSortBo))
        grdDefect.DataSource = resultDataSet.Tables(0)
        grdDefect.DataBind()

        'Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(1).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim applianceField As BoundField = New BoundField()
        applianceField.HeaderText = "Appliance"
        applianceField.DataField = "Appliance"
        grdFullConditionList.Columns.Add(applianceField)

        Dim categoryField As BoundField = New BoundField()
        categoryField.HeaderText = "Category"
        categoryField.DataField = "DefectCategory"
        grdFullConditionList.Columns.Add(categoryField)

        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "By"
        byField.DataField = "RecordedBy"
        grdFullConditionList.Columns.Add(byField)


        Dim objReportsBl As ReportBL = New ReportBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ViewState.Item(ViewStateConstants.Search)
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getDefectToBeApprovedReport(resultDataset, searchtext, objPageSortBo)

        grdFullConditionList.DataSource = resultDataset.Tables(0)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "DefectToBeApproved_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

    Protected Sub grdDefect_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#End Region

End Class