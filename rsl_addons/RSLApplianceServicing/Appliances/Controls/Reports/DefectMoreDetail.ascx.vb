﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AS_Utilities
Imports AS_BusinessLogic

Public Class DefectMoreDetail
    Inherits UserControlBase

#Region "Control Events"

    Public Event BackButtonClicked As EventHandler

#End Region

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not (IsPostBack) Then
                setControlProperty()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Link Button Property Address Event"
    ''' <summary>
    ''' Link Button Property Address Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnAddress.Click
        Try

            Dim propertyId As String = hdnPropertyId.Value()
            Dim url As String = PathConstants.PropertyRecordPath + propertyId
            Dim winStr As String = "window.open('" & url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "script", winStr, True)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Back Button Event"
    ''' <summary>
    ''' Back Button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBackToDefectApprovalReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToDefectApprovalReport.Click
        'Dim txtSearch As TextBox = TryCast(Page.FindControl("txtSearch"), TextBox)
        'txtSearch.Visible = True
        'Dim updPanelDefect As UpdatePanel = TryCast(Page.FindControl("updPanelDefect"), UpdatePanel)
        'updPanelDefect.Update()
        Dim javascriptMethod As String = "showSearchText('s')"
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "script", javascriptMethod, True)

        RaiseEvent BackButtonClicked(Me, New EventArgs)
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Set control property"
    ''' <summary>
    ''' Set control property
    ''' </summary>
    ''' <remarks></remarks>
    Sub setControlProperty()

        txtDefectRequired.Attributes.Add("readonly", "readonly")

    End Sub

#End Region

#Region "Reset Controls"
    ''' <summary>
    ''' Reset Controls
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetControls()

        lblScheme.Text = String.Empty
        lnkBtnAddress.Text = String.Empty
        lblTowncity.Text = String.Empty
        lblCounty.Text = String.Empty
        lblPostcode.Text = String.Empty

        lblCustomer.Text = ApplicationConstants.NotAvailable
        lblTelephone.Text = ApplicationConstants.NotAvailable
        lblMobile.Text = ApplicationConstants.NotAvailable
        lblEmail.Text = ApplicationConstants.NotAvailable

        lblAppliance.Text = String.Empty
        lblStatus.Text = String.Empty
        lblCategoryUpdated.Text = String.Empty
        lblCategoryUpdatedby.Text = String.Empty

        lblApproval.Text = String.Empty
        lblAppointment.Text = String.Empty

        txtDefectRequired.Text = String.Empty


    End Sub

#End Region

#Region "Populate More Detail"
    ''' <summary>
    ''' Populate More Detail
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateMoreDetail(ByVal defectId As Integer, ByVal appointmentType As String)

        Me.resetControls()

        Dim objReportsBl As ReportBL = New ReportBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim strLastSurvey As String = String.Empty
        Dim strSurveyBy As String = String.Empty
        objReportsBl.getDefectMoreDetail(resultDataset, defectId, appointmentType)

        Dim propertyCustomerDt As DataTable = resultDataset.Tables(ApplicationConstants.PropertyCustomerDetailDt)
        Dim defectDetailDt As DataTable = resultDataset.Tables(ApplicationConstants.DefectDetailDt)

        propertyAddress.Visible = True
        blockAddress.Visible = False
        divCustomerAddress.Visible = True

        If (propertyCustomerDt.Rows.Count > 0) Then

            lblScheme.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.SchemeCol)
            lnkBtnAddress.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.PropertyAddressCol)
            lblTowncity.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.TownCityCol)
            hdnPropertyId.Value = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.PropertyIdCol)
            lblCounty.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.CountyCol)
            lblPostcode.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.PostcodeCol)

            If (appointmentType = "Block") Then
                divCustomerInfo.Visible = False
                propertyAddress.Visible = False
                blockAddress.Visible = True
            End If

            If (appointmentType = "Scheme") Then
                divCustomerInfo.Visible = False
                propertyAddress.Visible = False
                divCustomerAddress.Visible = False
            End If

            If (appointmentType = "Property") Then
                divCustomerInfo.Visible = True
                lblCustomer.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.CustomerCol)
                lblTelephone.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.TelephoneCol)
                lblMobile.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.MobileCol)
                lblEmail.Text = propertyCustomerDt.Rows(propertyCustomerDt.Rows.Count - 1)(ApplicationConstants.EmailCol)
            End If

            
        End If

        If (defectDetailDt.Rows.Count > 0) Then
            If (IsDBNull(defectDetailDt.Rows(0)(ApplicationConstants.ApplianceCol))) Then
                lblAppliance.Text = ApplicationConstants.NotAvailable
            Else
                lblAppliance.Text = defectDetailDt.Rows(0)(ApplicationConstants.ApplianceCol)
            End If
            'lblAppliance.Text = IsDBNull(defectDetailDt.Rows(0)(ApplicationConstants.ApplianceCol))
            lblStatus.Text = defectDetailDt.Rows(0)(ApplicationConstants.StatusCol)
            lblCategoryUpdatedby.Text = defectDetailDt.Rows(0)(ApplicationConstants.CategoryUpdatedByCol)

            If (IsDBNull(defectDetailDt.Rows(0)(ApplicationConstants.CategoryUpdatedCol))) Then
                lblCategoryUpdated.Text = ApplicationConstants.NotAvailable
            Else
                lblCategoryUpdated.Text = GeneralHelper.getDateWithWeekdayFormat(defectDetailDt.Rows(0)(ApplicationConstants.CategoryUpdatedCol))
            End If

            lblApproval.Text = defectDetailDt.Rows(0)(ApplicationConstants.ApprovalCol)
            lblAppointment.Text = defectDetailDt.Rows(0)(ApplicationConstants.AppointmentCol)


            txtDefectRequired.Text = defectDetailDt.Rows(0)(ApplicationConstants.NotesCol)

        Else
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If


    End Sub

#End Region

#End Region

End Class