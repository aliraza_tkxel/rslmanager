﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports System.Drawing
Imports AS_Utilities
Imports AS_BusinessLogic
Imports AS_BusinessObject

Public Class DefectRejected
    Inherits UserControlBase

#Region "Properties"


#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
            scriptManager.RegisterPostBackControl(Me.btnExportToExcel)
            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

            End If
            pnlReport.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#Region "Grid View Row Bound"
    Sub DefectRejectGridView_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkBtnUnhold As LinkButton = DirectCast(e.Row.FindControl("lnkBtnUnhold"), LinkButton)
            Dim lblRejectionReason As Label = DirectCast(e.Row.FindControl("lblRejectionReason"), Label)
            If (lblRejectionReason.Text.Equals(ApplicationConstants.workOnHold)) Then
                ' Display the company name in italics.
                lnkBtnUnhold.Visible = True
            Else
                lnkBtnUnhold.Visible = False
            End If
        End If
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")

            Dim propertyId As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)

            If (appointmentType = "Property") Then
                Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
            Else
                Response.Redirect(PathConstants.SchemeRecordDashboard + propertyId + "&requestType=scheme&src=search")
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Link Button View Click"
    ''' <summary>
    ''' Link Button View Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim lnkBtnView As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnView.NamingContainer, GridViewRow)
            Dim reasonLabel As Label = DirectCast(row.FindControl("lblRejectionReason"), Label)
            showViewPopup(reasonLabel.Text, lnkBtnView.CommandArgument)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Unhold Click"
    ''' <summary>
    ''' Link Button View Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnUnhold_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim lnkBtnUnhold As LinkButton = DirectCast(sender, LinkButton)
            Dim objReportsBL As ReportBL = New ReportBL()
            Dim cds As ChangeDefectStatus = New ChangeDefectStatus()
            cds.UserId = SessionManager.getApplianceServicingId()
            cds.DefectId = Convert.ToInt32(lnkBtnUnhold.CommandArgument)
            cds.Status = ApplicationConstants.toBeApproved
            Dim isApproved As Boolean = objReportsBL.changeDefectStatus(cds)
            If (isApproved) Then

                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectRejectedReport(resultDataSet, search, False)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkToBeApproved, False)
                'Dim body As String = populateBodyApprovedConditionWorksEmail(objConditionEmailBo.RequestedBy, objConditionEmailBo.Component, objConditionEmailBo.Address, objConditionEmailBo.Towncity, objConditionEmailBo.Username, objConditionEmailBo.PostCode)
                ''uiMessageHelper.setMessage(lblApproveErrorMessage, pnlApproveErrorMessage, "4", False)
                'If (sendEmail(body)) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApproved, False)
                'Else
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ConditionWorkApprovedFailedEmail, False)
                'End If
                'updPanelTobeApproved.Update()
                ' updPanelConditionRating.Update()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectWorkFailedToBeApproved, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "Show View Popup"
    ''' <summary>
    ''' Show View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showViewPopup(ByVal reason As String, ByVal notes As String)

        resetViewPopup()
        lblRejectionReason.Text = reason
        lblRejectionNotes.Text = notes
        mdlPopUpViewCondition.Show()

    End Sub

#End Region

#Region "Reset View Popup"
    ''' <summary>
    ''' Reset View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetViewPopup()

        uiMessageHelper.resetMessage(lblViewErrorMessage, pnlViewErrorMessage)
        lblRejectionReason.Text = String.Empty
        lblRejectionNotes.Text = String.Empty

    End Sub

#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdConditionRating_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDefect.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdDefect.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectRejectedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectRejectedReport(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectRejectedReport(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectRejectedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportBL = New ReportBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If
        Dim totalCount As Integer = Convert.ToInt32(objReportsBl.getDefectRejectedReport(resultDataSet, search, objPageSortBo))
        'objReportsBl.getDefectRejectedReport(resultDataSet, search, objPageSortBo)
        grdDefect.DataSource = resultDataSet.Tables(0)
        grdDefect.DataBind()

        'Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(0).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim applianceField As BoundField = New BoundField()
        applianceField.HeaderText = "Appliance"
        applianceField.DataField = "Appliance"
        grdFullConditionList.Columns.Add(applianceField)

        Dim categoryField As BoundField = New BoundField()
        categoryField.HeaderText = "Category"
        categoryField.DataField = "DefectCategory"
        grdFullConditionList.Columns.Add(categoryField)

        Dim reportedByField As BoundField = New BoundField()
        reportedByField.HeaderText = "Reported By"
        reportedByField.DataField = "RecordedBy"
        grdFullConditionList.Columns.Add(reportedByField)

        Dim rejectedByField As BoundField = New BoundField()
        rejectedByField.HeaderText = "Rejected By"
        rejectedByField.DataField = "RejectedByAt"
        grdFullConditionList.Columns.Add(rejectedByField)

        Dim reasonField As BoundField = New BoundField()
        reasonField.HeaderText = "Reason"
        reasonField.DataField = "RejectionReason"
        grdFullConditionList.Columns.Add(reasonField)

        Dim objReportsBl As ReportBL = New ReportBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ViewState.Item(ViewStateConstants.Search)
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getDefectRejectedReport(resultDataset, searchtext, objPageSortBo)

        grdFullConditionList.DataSource = resultDataset.Tables(0)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "DefectRejected_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

    Protected Sub grdDefect_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#End Region

End Class