﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectMoreDetail.ascx.vb"
    Inherits="Appliances.DefectMoreDetail" %>
<link href="../../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
<div class="headingTitle" style="width: 97%">
    <asp:Image ID="Image1" ImageUrl="~/Images/question.png" runat="server" />
    <b>More Details</b>
</div>
<div style="border: 1px solid black; padding: 36px;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <br />
    </asp:Panel>
    <div id="PropertyDetail">
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label2" runat="server" Text="Scheme:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd" style="vertical-align: top">
                        <asp:Label ID="propertyAddress" runat="server" Text="Address:"></asp:Label>
                        <asp:Label ID="blockAddress" runat="server" Text="Block:"></asp:Label>
                    </td>
                    <td>
                        <asp:HiddenField ID="hdnPropertyId" runat="server" />
                        <div runat="server" id="divCustomerAddress">
                            <asp:LinkButton ID="lnkBtnAddress" Font-Bold="true" ForeColor="Blue" Font-Underline="true"
                                OnClick="lnkBtnAddress_Click" runat="server"></asp:LinkButton>
                            <br />
                            <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                        </div>
                    </td>
                </tr>
                <%-- <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                      
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                      
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        &nbsp
                    </td>
                    <td>
                       
                    </td>
                </tr>--%>
            </table>
        </div>
        <div class="leftDiv" runat="server" id="divCustomerInfo" visible="False">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label1" runat="server" Text="Customer:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label5" runat="server" Text="Telephone:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblTelephone" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label4" runat="server" Text="Mobile:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label6" runat="server" Text="Email:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" Font-Bold="true" Font-Underline="true" ForeColor="Blue"
                            runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div style="clear: both;">
        <br />
        <br />
    </div>
    <hr />
    <br />
    <br />
    <div id="DefectDetail">
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label9" runat="server" Text="Appliance:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblAppliance" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label8" runat="server" Text="Status:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label10" runat="server" Text="Category Updated:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCategoryUpdated" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label11" runat="server" Text="Category Updated by:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblCategoryUpdatedby" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label24" runat="server" Text="Approval:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblApproval" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label26" runat="server" Text="Appointment:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblAppointment" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="leftDiv">
            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                <tr>
                    <td class="labelBoldStyleTd">
                        <asp:Label ID="Label13" runat="server" Text="Defect Identified:"></asp:Label>
                    </td>
                    <td>
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td class="labelBoldStyleTd" colspan="2">
                        <asp:TextBox ID="txtDefectRequired" Font-Italic="true" TextMode="MultiLine" Width="82%"
                            Height="110px" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div style="clear: both;">
    </div>
    <br />
    <br />
    <hr />
    <br />
    <br />
    <br />
    <asp:Button ID="btnBackToDefectApprovalReport" class="btn btn-xs btn-blue right"
        runat="server" Text="< Back To Defect Approval Report" />
</div>
