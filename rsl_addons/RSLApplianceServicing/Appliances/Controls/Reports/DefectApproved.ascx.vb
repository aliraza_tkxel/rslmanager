﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports System.Drawing
Imports AS_Utilities
Imports AS_BusinessLogic

Public Class DefectApproved
    Inherits UserControlBase

#Region "Properties"

    Public ReadOnly Property GetComponentId As String
        Get
            Dim componentid As Integer = -1
            'If (Not IsNothing(Request.QueryString(PathConstants.ComponentId))) Then
            '    componentid = Request.QueryString(PathConstants.ComponentId)
            'End If
            Return componentid
        End Get
    End Property

#End Region

#Region "Events"

    '#Region "Page Pre Render Event"
    '    ''' <summary>
    '    ''' Page Pre Render Event
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>
    '    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '        Me.reBindGridForLastPage()
    '    End Sub
    '#End Region

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
            scriptManager.RegisterPostBackControl(Me.btnExportToExcel)
            If Not IsPostBack Then
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            End If
            pnlReport.Visible = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Button Export Click"
    ''' <summary>
    ''' Button Export Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(sender As Object, e As EventArgs) Handles btnExportToExcel.Click
        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button Address Click"
    ''' <summary>
    ''' Link Button Address Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAddress_Click(sender As Object, e As EventArgs)
        Try
            Dim commandParams() As String = CType(DirectCast(sender, LinkButton).CommandArgument, String).Split(";")

            Dim propertyId As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)

            If (appointmentType = "Property") Then
                Response.Redirect(PathConstants.PropertyRecordPath + propertyId)
            Else
                Response.Redirect(PathConstants.SchemeRecordDashboard + propertyId + "&requestType=scheme&src=search")
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Link Button More Detail Click"
    ''' <summary>
    ''' Link Button More Detail Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnViewAppointment_Click(sender As Object, e As EventArgs)
        Try
            Dim parameters() As String = DirectCast(sender, LinkButton).CommandArgument.Split(";")
            Dim ref As String = parameters(0)
            Dim statusTitle As String = parameters(1)

            Response.Redirect(PathConstants.ScheduleDefect, False)
            'SessionManager.setConditionRatingAptSearch("PMO" + ref)

            'Select Case statusTitle
            '    Case "Condition to be Arranged"
            '        Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentToBeArrangedTab)
            '    Case "Condition Arranged"
            '        Response.Redirect(PathConstants.ScheduleConditionWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
            '    Case "To be Arranged"
            '        Response.Redirect(PathConstants.ScheduleWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentToBeArrangedTab)
            '    Case "Arranged"
            '        Response.Redirect(PathConstants.ScheduleWorks + "?" + PathConstants.ScheduleWorksTab + "=" + ApplicationConstants.AppointmentArrangedTab)
            '    Case "Assigned To Contractor"
            '        Response.Redirect(PathConstants.AssignedToContractor)
            '    Case "Cancelled"
            '        Response.Redirect(PathConstants.CancelledApointmentReport)
            '    Case "Completed"
            '        statusTitle = "This work has been completed successfully."
            '        showViewPopup(statusTitle)
            '    Case Else
            '        showViewPopup("Information is not available right now.")
            'End Select

            'updPanelConditionRating.Update()
            'Response.Redirect(PathConstants.ConditionMoreDetailPath + "?" + PathConstants.Pmo + "=" + journalId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Show View Popup"
    ''' <summary>
    ''' Show View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showViewPopup(ByVal status As String)

        resetViewPopup()
        lblStatus.Text = status
        mdlPopUpViewCondition.Show()

    End Sub

#End Region

#Region "Reset View Popup"
    ''' <summary>
    ''' Reset View Popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetViewPopup()

        uiMessageHelper.resetMessage(lblViewErrorMessage, pnlViewErrorMessage)
        lblStatus.Text = String.Empty

    End Sub

#End Region

#Region "Condition Rating Sorting"
    ''' <summary>
    ''' Condition Rating Sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdDefect_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDefect.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdDefect.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo


            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateDefectApprovedReport(resultDataSet, search, False, GetComponentId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)


                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateDefectApprovedReport(resultDataSet, search, False, GetComponentId)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Condition Rating Report"
    ''' <summary>
    ''' Populate Condition Rating Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDefectApprovedReport(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean, Optional ByRef componentId As String = "-1")

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        Dim objReportsBl As ReportBL = New ReportBL()
        Dim isFullList As Boolean = False

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.Search, search)
        Else
            objPageSortBo = ViewState(ViewStateConstants.PageSortBo)
        End If
        Dim totalCount As Integer = Convert.ToInt32(objReportsBl.getDefectApprovedReport(resultDataSet, search, objPageSortBo))
        'objReportsBl.getDefectApprovedReport(resultDataSet, search, objPageSortBo)
        grdDefect.DataSource = resultDataSet.Tables(0)
        grdDefect.DataBind()

        'Dim totalCount As Integer = Convert.ToInt32(resultDataSet.Tables(0).Rows(0)(0))
        If (totalCount > 0) Then
            pnlExportToExcel.Visible = True
        Else
            pnlExportToExcel.Visible = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

    End Sub

#End Region

#Region "Export full List"
    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()

        Dim grdFullConditionList As New GridView()
        grdFullConditionList.AllowPaging = False
        grdFullConditionList.AutoGenerateColumns = False

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address"
        addressField.DataField = "Address"
        grdFullConditionList.Columns.Add(addressField)

        Dim postcodeField As BoundField = New BoundField()
        postcodeField.HeaderText = "Postcode"
        postcodeField.DataField = "Postcode"
        grdFullConditionList.Columns.Add(postcodeField)

        Dim applianceField As BoundField = New BoundField()
        applianceField.HeaderText = "Appliance"
        applianceField.DataField = "Appliance"
        grdFullConditionList.Columns.Add(applianceField)

        Dim categoryField As BoundField = New BoundField()
        categoryField.HeaderText = "Category"
        categoryField.DataField = "DefectCategory"
        grdFullConditionList.Columns.Add(categoryField)

        Dim reportedByField As BoundField = New BoundField()
        reportedByField.HeaderText = "Reported By"
        reportedByField.DataField = "RecordedBy"
        grdFullConditionList.Columns.Add(reportedByField)

        Dim approvedByField As BoundField = New BoundField()
        approvedByField.HeaderText = "Approved By"
        approvedByField.DataField = "ApprovedByAt"
        grdFullConditionList.Columns.Add(approvedByField)

        Dim objReportsBl As ReportBL = New ReportBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim searchtext As String = ViewState.Item(ViewStateConstants.Search)
        Dim isFullList As Boolean = True
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        objReportsBl.getDefectApprovedReport(resultDataset, searchtext, objPageSortBo)

        grdFullConditionList.DataSource = resultDataset.Tables(0)
        grdFullConditionList.DataBind()

        ExportGridToExcel(grdFullConditionList)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "DefectApproved_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", ApplicationConstants.AddressCol, 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

    Protected Sub grdDefect_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#End Region
End Class