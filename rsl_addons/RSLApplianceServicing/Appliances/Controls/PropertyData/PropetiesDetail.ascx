﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PropetiesDetail.ascx.vb"
    Inherits="Appliances.PropetiesDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="ucPropertiesTab" TagName="PropertiesTab" Src="~/Controls/PropertyData/AddProperties.ascx" %>
<%@ Register TagPrefix="prop" TagName="Attributes" Src="~/Controls/Property/Attributes.ascx" %>
<%@ Register TagPrefix="prop" TagName="Financial" Src="~/Controls/Property/FinancialTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="HealthSafety" Src="~/Controls/Property/HealthSafetyTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="Warranty" Src="~/Controls/Property/Warranties.ascx" %>
<%@ Register TagPrefix="prop" TagName="Document" Src="~/Controls/Property/DocumentTab.ascx" %>
<%@ Register TagPrefix="prop" TagName="Restriction" Src="~/Controls/Property/Restriction.ascx" %>

    <style type="text/css">
       
        
        .pm-Financialbox
        {
            border: 1px solid #000;
            float: left;
            margin-top: 5px;
            margin-left: 2px;
            padding: 10px;
            width: 98%;
        }
    </style>
<div style="width: 100%; float: left; padding:10px;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false" Height="19px" Width="289px">
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <div style='margin-left: 3px;'>
        <asp:LinkButton ID="lnkBtnSummaryTab" CommandArgument="lnkBtnSummaryTab_Click" CssClass="TabClicked"
            runat="server" Visible="true" BorderStyle="Solid" OnClick="lnkBtnSummaryTab_Click">Main Details: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnAttributesTab" CssClass="TabInitial" runat="server" Visible="true"
            BorderStyle="Solid" OnClick="lnkBtnAttributesTab_Click">Attributes: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnDocTab" OnClick="lnkBtnDocTab_Click" CssClass="TabInitial"
            runat="server" Visible="true" BorderStyle="Solid">Documents: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnFinancial" CssClass="TabInitial" runat="server" Visible="true"
            BorderStyle="Solid" OnClick="lnkBtnFinancial_Click">Financial: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnWarranty" CssClass="TabInitial" runat="server" Visible="true"
            BorderStyle="Solid" OnClick="lnkBtnWarranty_Click">Warranties: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnHealthSafety" CssClass="TabInitial" runat="server" Visible="true"
            BorderStyle="Solid" OnClick="lnkBtnHealthSafety_Click">Health & Safety: </asp:LinkButton>
        <asp:LinkButton ID="lnkBtnRestriction" CssClass="TabInitial" runat="server" Visible="true"
            BorderStyle="Solid" OnClick="lnkBtnRestriction_Click">Restriction: </asp:LinkButton>
        <div style="clear: both; margin-bottom: 5px;">
        </div>
    </div>
    <asp:MultiView ID="mainView" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <ucPropertiesTab:PropertiesTab ID="propertiesTab" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <prop:Attributes ID="Attributes" runat="server" />
        </asp:View>
        <asp:View ID="View6" runat="server">
            <prop:Document ID="Document" runat="server" />
        </asp:View>
        <asp:View ID="View3" runat="server">
             <div class="pm-Financialbox">
            <prop:Financial ID="Financial" runat="server" /></div> 
        </asp:View>
        <asp:View ID="View4" runat="server">
         <prop:Warranty ID="Warranty" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <prop:HealthSafety ID="HealthSafety" runat="server" />
        </asp:View>
           <asp:View ID="View7" runat="server">
            <prop:Restriction ID="Restriction" runat="server" />
        </asp:View>
       
    </asp:MultiView>
</div>
