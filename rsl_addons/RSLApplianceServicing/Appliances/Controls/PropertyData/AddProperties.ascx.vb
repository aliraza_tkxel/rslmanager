﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports System.Threading
Imports System.IO
Public Class AddProperties
    Inherits UserControlBase

#Region "Properties"
    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim first As Boolean = True
    Dim removePrevious As DataTable = New DataTable()
#End Region


#Region "Events"
#Region "Page Load Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            btnSaveNext.Visible = True
            If SessionManager.getPropertyId() <> String.Empty And PathConstants.PropertyIds IsNot Nothing Then
                btnSaveNext.Visible = False
                txtPropReference.Text = SessionManager.getPropertyId()
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


#Region "ddlTemplate Selected Index Changed"
    Protected Sub ddlTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTemplate.SelectedIndexChanged
        Try
            Dim propertyId As String = ddlTemplate.SelectedItem.Value
            If propertyId <> String.Empty Then
                If Request.QueryString("id") IsNot Nothing Then
                    mdlPopupMessage.Show()
                Else
                    populateControls(propertyId)

                End If
            End If

        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.Message = ""
            End If
        End Try
    End Sub
#End Region

#Region "ddlDevelopment Selected Index Changed"
    Protected Sub ddlDevelopment_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDevelopment.SelectedIndexChanged
        Try
            Dim devId As Integer = Convert.ToInt32(ddlDevelopment.SelectedItem.Value)
            populateBlockDropdownList(devId)
            populatePhaseDropdownList(devId)
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.Message = ""
            End If
        End Try
    End Sub
#End Region

#Region "ddlBlockName Selected Index Changed"
    Protected Sub ddlBlockName_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBlockName.SelectedIndexChanged
        Try
            Dim blockId As Integer = Convert.ToInt32(ddlBlockName.SelectedValue)
            Dim resultDataSet As DataSet = New DataSet()
            objPropertyBl.getBlocksByBlockId(resultDataSet, blockId)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                txtAddress1.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("ADDRESS1")), resultDataSet.Tables(0).Rows(0).Item("ADDRESS1"), Nothing)
                txtAddress2.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("ADDRESS2")), resultDataSet.Tables(0).Rows(0).Item("ADDRESS2"), Nothing)
                txtTown.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("TOWNCITY")), resultDataSet.Tables(0).Rows(0).Item("TOWNCITY"), Nothing)
                txtCounty.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("COUNTY")), resultDataSet.Tables(0).Rows(0).Item("COUNTY"), Nothing)
                txtPostCode.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("POSTCODE")), resultDataSet.Tables(0).Rows(0).Item("POSTCODE"), Nothing)
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.Message = ""
            End If
        End Try
    End Sub
#End Region

#Region "ddlStatus Selected Index Changed"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            Dim statusId As Integer = Convert.ToInt32(ddlStatus.SelectedItem.Value)
            If statusId > 0 Then
                populatePropertySubStatusDropdownList(statusId)
            End If

        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = ""
        End Try

    End Sub
#End Region

#Region "ddlPhase Selected Index Changed"
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPhase.SelectedIndexChanged
        Try
            Dim phaseId As Integer = Convert.ToInt32(ddlPhase.SelectedItem.Value)
            If (phaseId = -1) Then
                txtHandoverDate.Text = ""
                txtCompletionDate.Text = ""
            End If
            Me.populateCompleteionAndHandoverDates(phaseId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.Message = ""
            End If
        End Try
    End Sub
#End Region

#Region "btnSave Click Event"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objPropertyDetailsBO As PropertiesDetailsBO = New PropertiesDetailsBO()
            objPropertyDetailsBO.IsTemplate = False
            Page.Validate("save")
            If Page.IsValid() And lblCompletionError.Visible = False And lblCompletionError.Visible = False Then

                mdlPopupPrimaryHeatingFuel.Show()
                fillPropertyObject(objPropertyDetailsBO)
                ' saveProperty(objPropertyDetailsBO)
            End If

        Catch ex As ThreadAbortException

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try

    End Sub
#End Region

#Region "btnsaveandNext click event"
    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveNext.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objPropertyDetailsBO As PropertiesDetailsBO = New PropertiesDetailsBO()
            objPropertyDetailsBO.IsTemplate = True

            Page.Validate("save")
            If Page.IsValid() Then
                fillPropertyObject(objPropertyDetailsBO)
                If uiMessageHelper.IsError = False Then
                    SessionManager.removePropertiesDetailsBO()
                    SessionManager.setPropertiesDetailsBO(objPropertyDetailsBO)
                    mdlPopUpAddTemplate.Show()

                End If
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = ""
        End Try
    End Sub
#End Region

#Region "btnTemplateSave click event"
    Protected Sub btnTemplateSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTemplateSave.Click
        Try
            Dim objPropertyDetailsBO As PropertiesDetailsBO = SessionManager.getPropertiesDetailsBO()
            objPropertyDetailsBO.TemplateName = txtTemplateName.Text.Trim()
            'saveProperty(objPropertyDetailsBO)
            mdlPopUpAddTemplate.Hide()
            mdlPopupPrimaryHeatingFuel.Show()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnCancel Click event"
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            resetAllControls()
            Response.Redirect(PathConstants.PropertyListing.ToString)
        Catch ex As ThreadAbortException
            'uiMessageHelper.IsError = True
            'uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddlNROSHAssetTypeMain_SelectedIndexChanged"
    'Protected Sub ddlNROSHAssetTypeMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlNROSHAssetTypeMain.SelectedIndexChanged
    '    Try

    '        populatePropertyNROSHAssetTypeSubDropdownList(Convert.ToInt32(ddlNROSHAssetTypeMain.SelectedValue))
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.Message = ex.Message
    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
    '        End If
    '    End Try
    'End Sub
#End Region

#Region "btn Ok click Event"
    ''' <summary>
    ''' btn Ok click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOk.Click
        Try

            Dim propertyId As String = ddlTemplate.SelectedItem.Value
            If propertyId <> String.Empty Then
                populateControls(propertyId)
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.Message = ""
            End If
        End Try

    End Sub
#End Region

#Region "ddlAssetType Selected Index Changed"
    Protected Sub ddlAssetType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAssetType.SelectedIndexChanged
        If (ddlAssetType.SelectedItem.Text.Contains("Shared Ownership")) Then
            txtMinPurchase.Enabled = True
            txtPurchaseLevel.Enabled = True
            lblMinPurchase.Visible = True
            lblPurchaseLevel.Visible = True
        Else
            txtMinPurchase.Enabled = False
            txtPurchaseLevel.Enabled = False
            txtPurchaseLevel.Text = String.Empty
            txtMinPurchase.Text = String.Empty
            lblMinPurchase.Visible = False
            lblPurchaseLevel.Visible = False
        End If
    End Sub
#End Region

#Region "btnSaveProperty Click event"
    ''' <summary>
    ''' btnSaveProperty Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveProperty_Click(sender As Object, e As EventArgs) Handles btnSaveProperty.Click
        Try
            Dim objPropertyDetailsBO As PropertiesDetailsBO = New PropertiesDetailsBO()
            objPropertyDetailsBO = SessionManager.getPropertiesDetailsBO()
            objPropertyDetailsBO.ValueId = Convert.ToInt32(ddlHeatingFuel.SelectedValue)
            saveProperty(objPropertyDetailsBO)
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            Else
                mdlPopupPrimaryHeatingFuel.Show()
            End If
        End Try
    End Sub
#End Region
#End Region

#Region "Functions"
#Region "Populate All Dropdownlist"


#Region "Populate All Dropdownlist"
    ''' <summary>
    ''' Populate All Dropdownlist
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateDropDownList()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyTemplateForAllDropDown(resultDataSet)
        populateDevelopmentDropdownList(resultDataSet.Tables(ApplicationConstants.development))
        SessionManager.PropertyOwnerShip = resultDataSet
        If (first = True) Then
            removePrevious = resultDataSet.Tables(ApplicationConstants.propertyTemplate)
            first = False
        End If
        Dim templateTable As DataTable = resultDataSet.Tables(ApplicationConstants.propertyTemplate)
        For Each row As DataRow In removePrevious.Rows
            Dim tName = row.Item("TemplateName")
            Dim rCount As Integer = 0
            For Each tRow As DataRow In templateTable.Rows
                If tRow.RowState <> DataRowState.Deleted Then
                    If tRow.Item("TemplateName") = tName Then
                        templateTable.Rows(rCount).Delete()
                        Exit For
                    End If
                End If

                rCount = rCount + 1
            Next
        Next
        populatePropertyTemplateDropdownList(templateTable)
        populatePropertyStockTypeDropdownList(resultDataSet.Tables(ApplicationConstants.stocktype))

        populateTenureTypeDropdownList(resultDataSet.Tables(ApplicationConstants.TenureSelection))
        'populatePropertyOwnerShipDropdownList(resultDataSet.Tables(ApplicationConstants.ownership))

        populatePropertyStatusDropdownList(resultDataSet.Tables(ApplicationConstants.status))
        populatePropertyTypeDropdownList(resultDataSet.Tables(ApplicationConstants.propertyType))
        populateDwellingTypeDropdownList(resultDataSet.Tables(ApplicationConstants.dwellingType))
        populatePropertyLevelDropdownList(resultDataSet.Tables(ApplicationConstants.level))
        populatePropertyAssetTypeDropdownList(resultDataSet.Tables(ApplicationConstants.assetType))
        populatePropertyNROSHAssetTypeMainDropdownList(resultDataSet.Tables(ApplicationConstants.assetTypeMain))
        'populatePropertyNROSHAssetTypeSubDropdownList(-1)
        populateFuelType()
    End Sub
#End Region


#Region "Populate all Development List"
    ''' <summary>
    ''' Populate all Development List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDevelopmentDropdownList(ByRef dt As DataTable)


        ddlDevelopment.DataSource = dt.DefaultView
        ddlDevelopment.DataValueField = "DevelopmentId"
        ddlDevelopment.DataTextField = "DevelopmentName"
        ddlDevelopment.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlDevelopment.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate all Phase List"
    ''' <summary>
    ''' Populate all Phase List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePhaseDropdownList(ByVal devId As Integer)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPhaseList(resultDataSet, devId)

        ddlPhase.DataSource = resultDataSet.Tables(0).DefaultView
        ddlPhase.DataValueField = "PHASEID"
        ddlPhase.DataTextField = "PhaseName"
        ddlPhase.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlPhase.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate Template List"
    ''' <summary>
    ''' Populate all Template List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyTemplateDropdownList(ByRef dt As DataTable)
        ddlTemplate.DataSource = dt.DefaultView
        ddlTemplate.DataValueField = "PROPERTYID"
        ddlTemplate.DataTextField = "TemplateName"
        ddlTemplate.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlTemplate.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate STOCKTYPE List"
    ''' <summary>
    ''' Populate all STOCKTYPE List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyStockTypeDropdownList(ByRef dt As DataTable)
        ddlStockType.DataSource = dt.DefaultView
        ddlStockType.DataValueField = "STOCKTYPE"
        ddlStockType.DataTextField = "DESCRIPTION"
        ddlStockType.DataBind()
        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlStockType.Items.Insert(0, newListItem)
    End Sub

#End Region


#Region "Populate TenureType List"
    Sub populateTenureTypeDropdownList(ByRef dt As DataTable)
        ddlTenureType.DataSource = dt.DefaultView
        ddlTenureType.DataValueField = "TENURETYPEID"
        ddlTenureType.DataTextField = "TENURETYPE"
        ddlTenureType.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlTenureType.Items.Insert(0, newListItem)

    End Sub
#End Region





#Region "Populate OwnerShip List"
    ''' <summary>
    ''' Populate all OwnerShip List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyOwnerShipDropdownList(ByRef dt As DataTable, ByVal ownerShip As String)
        If ownerShip = "Freehold" Then
            Dim filterResult = From r In dt.AsEnumerable() Where r("DESCRIPTION") = "Outright Owner" Or r("DESCRIPTION") = "Others" Order By r("DESCRIPTION") Ascending Select r
            If filterResult.Count() > 0 Then
                dt = filterResult.CopyToDataTable()
            End If
        Else
            Dim filterResult = From r In dt.AsEnumerable() Where r("DESCRIPTION") = "Lease of 21 years or more" Or r("DESCRIPTION") = "Others" Or r("DESCRIPTION") = "Lease of less than 21 years" Order By r("DESCRIPTION") Ascending Select r
            If filterResult.Count() > 0 Then
                dt = filterResult.CopyToDataTable()
            End If
        End If

        ddlOwnerShip.DataSource = dt.DefaultView
        ddlOwnerShip.DataValueField = "OWNERSHIPID"
        ddlOwnerShip.DataTextField = "DESCRIPTION"
        ddlOwnerShip.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlOwnerShip.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "Populate Status List"
    ''' <summary>
    ''' Populate all Status List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyStatusDropdownList(ByRef dt As DataTable)


        ddlStatus.DataSource = dt.DefaultView
        ddlStatus.DataValueField = "STATUSID"
        ddlStatus.DataTextField = "DESCRIPTION"
        ddlStatus.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlStatus.Items.Insert(0, newListItem)
        populatePropertySubStatusDropdownList(Convert.ToInt32(ddlStatus.SelectedValue))
    End Sub

#End Region

#Region "Populate sub Status List"
    ''' <summary>
    ''' Populate all Status List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertySubStatusDropdownList(ByVal statusId As Integer)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertySubStatus(resultDataSet, statusId)

        ddlSubStatus.DataSource = resultDataSet.Tables(0).DefaultView
        ddlSubStatus.DataValueField = "SUBSTATUSID"
        ddlSubStatus.DataTextField = "DESCRIPTION"
        ddlSubStatus.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlSubStatus.Items.Insert(0, newListItem)
    End Sub

#End Region

#Region "Populate Property Type List"
    ''' <summary>
    ''' Populate all Property Type List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyTypeDropdownList(ByRef dt As DataTable)


        ddlPropType.DataSource = dt.DefaultView
        ddlPropType.DataValueField = "PROPERTYTYPEID"
        ddlPropType.DataTextField = "DESCRIPTION"
        ddlPropType.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlPropType.Items.Insert(0, newListItem)
    End Sub

#End Region

#Region "Populate Property Dwelling Type List"
    ''' <summary>
    ''' Populate all Property Dwelling Type List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateDwellingTypeDropdownList(ByRef dt As DataTable)


        ddlDwellingType.DataSource = dt.DefaultView
        ddlDwellingType.DataValueField = "DTYPEID"
        ddlDwellingType.DataTextField = "DESCRIPTION"
        ddlDwellingType.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlDwellingType.Items.Insert(0, newListItem)
    End Sub

#End Region

#Region "Populate Property Level List"
    ''' <summary>
    ''' Populate all Property Level List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyLevelDropdownList(ByRef dt As DataTable)


        ddlLevel.DataSource = dt.DefaultView
        ddlLevel.DataValueField = "LEVELID"
        ddlLevel.DataTextField = "DESCRIPTION"
        ddlLevel.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlLevel.Items.Insert(0, newListItem)
    End Sub

#End Region

#Region "Populate Property AssetType List"
    ''' <summary>
    ''' Populate all Property AssetType List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyAssetTypeDropdownList(ByRef dt As DataTable)


        ddlAssetType.DataSource = dt.DefaultView
        ddlAssetType.DataValueField = "ASSETTYPEID"
        ddlAssetType.DataTextField = "DESCRIPTION"
        ddlAssetType.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlAssetType.Items.Insert(0, newListItem)
        txtMinPurchase.Enabled = False
        txtMinPurchase.Text = String.Empty
        txtPurchaseLevel.Enabled = False
        txtPurchaseLevel.Text = String.Empty












    End Sub

#End Region

#Region "Populate Property NROSH AssetType Main List"
    ''' <summary>
    ''' Populate Property NROSH AssetType Main List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePropertyNROSHAssetTypeMainDropdownList(ByRef dt As DataTable)

        ddlNROSHAssetTypeMain.DataSource = dt.DefaultView
        ddlNROSHAssetTypeMain.DataValueField = "Sid"
        ddlNROSHAssetTypeMain.DataTextField = "DESCRIPTION"
        ddlNROSHAssetTypeMain.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlNROSHAssetTypeMain.Items.Insert(0, newListItem)
    End Sub

#End Region

#Region "Populate Property NROSH AssetType Sub List"
    ' ''' <summary>
    ' ''' Populate Property NROSH AssetType Sub List
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Sub populatePropertyNROSHAssetTypeSubDropdownList(ByVal catId As Integer)

    '    Dim resultDataSet As DataSet = New DataSet()
    '    objPropertyBl.getPropertyNROSHAssetTypeSub(resultDataSet, catId)

    '    ddlNROSHAssetTypeSub.DataSource = resultDataSet.Tables(0).DefaultView
    '    ddlNROSHAssetTypeSub.DataValueField = "Sid"
    '    ddlNROSHAssetTypeSub.DataTextField = "DESCRIPTION"
    '    ddlNROSHAssetTypeSub.DataBind()

    '    Dim newListItem As ListItem
    '    newListItem = New ListItem("Please select", "-1")
    '    ddlNROSHAssetTypeSub.Items.Insert(0, newListItem)
    'End Sub

#End Region
#Region "Populate  Block List"
    ''' <summary>
    ''' Populate  Block List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateBlockDropdownList(ByVal devId As Integer)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getBlocksByDevelopmentId(resultDataSet, devId)

        ddlBlockName.DataSource = resultDataSet.Tables(0).DefaultView
        ddlBlockName.DataValueField = "BLOCKID"
        ddlBlockName.DataTextField = "BLOCKNAME"
        ddlBlockName.DataBind()

        Dim newListItem As ListItem
        newListItem = New ListItem("Please select", "-1")
        ddlBlockName.Items.Insert(0, newListItem)

    End Sub

#End Region

#Region "populate Heating Fuel"
    Protected Sub populateFuelType()
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.GetFuel(resultDataSet)
        ddlHeatingFuel.DataSource = resultDataSet
        ddlHeatingFuel.DataValueField = ApplicationConstants.DefaultDropDownDataValueField
        ddlHeatingFuel.DataTextField = ApplicationConstants.DefaultDropDownDataTextField
        ddlHeatingFuel.DataBind()

        Dim filterResult = From r In resultDataSet.Tables(0).AsEnumerable() Where r("title") = "Please Select" Select r
        If filterResult.Count() > 0 Then
            rfvHeatingFuel.InitialValue = filterResult.First().Item("id")
        End If
        'rfvHeatingFuel.InitialValue = resultDataSet.Tables(0).Select(x => XAttribute.  
    End Sub
#End Region

#End Region

#Region "fill Property Object"
    Private Sub fillPropertyObject(ByRef objPropertyDetailsBO As PropertiesDetailsBO)
        Dim filePath
        Dim fileFormat
        If Not IsNothing(SessionManager.getPropertyId()) Then
            objPropertyDetailsBO.PropertyId = SessionManager.getPropertyId()
        End If
        If ddlDevelopment.SelectedValue > 0 Then
            objPropertyDetailsBO.DevelopmentId = ddlDevelopment.SelectedValue
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.SelectDevelopment
            Exit Sub
        End If

        objPropertyDetailsBO.BlockId = ddlBlockName.SelectedValue
        objPropertyDetailsBO.PhaseId = ddlPhase.SelectedValue
        If txtHouse.Text = String.Empty Or txtAddress1.Text = String.Empty Then

            uiMessageHelper.IsError = True
            uiMessageHelper.Message = UserMessageConstants.WriteAddress
            Exit Sub
        Else
            objPropertyDetailsBO.House = txtHouse.Text.Trim
            objPropertyDetailsBO.Address1 = txtAddress1.Text.Trim
        End If

        objPropertyDetailsBO.Address2 = txtAddress2.Text.Trim
        objPropertyDetailsBO.Address3 = txtAddress3.Text.Trim
        objPropertyDetailsBO.Town = txtTown.Text.Trim
        objPropertyDetailsBO.County = txtCounty.Text.Trim
        objPropertyDetailsBO.PostCode = txtPostCode.Text.Trim
        objPropertyDetailsBO.TitleNumber = txtTitle.Text.Trim
        objPropertyDetailsBO.DeedLocation = txtDeed.Value  '.Text.Trim


        If IsDate(txtDefectsPeriod.Text.Trim) Then
            objPropertyDetailsBO.DefectPeriod = Convert.ToDateTime(txtDefectsPeriod.Text.Trim)
        End If
        If IsDate(txtLeaseStart.Text.Trim) Then
            objPropertyDetailsBO.LeaseStart = Convert.ToDateTime(txtLeaseStart.Text.Trim)
        End If
        If IsDate(txtLeaseEnd.Text.Trim) Then
            objPropertyDetailsBO.LeaseEnd = Convert.ToDateTime(txtLeaseEnd.Text.Trim)
        End If
        If IsDate(txtDateBuild.Text.Trim) Then
            objPropertyDetailsBO.DateBuild = Convert.ToDateTime(txtDateBuild.Text.Trim)
        End If
        If IsDate(txtDateAcquired.Text.Trim) Then
            objPropertyDetailsBO.DateAcquired = Convert.ToDateTime(txtDateAcquired.Text.Trim)
        End If
        If IsDate(txtViewingComponent.Text.Trim) Then
            objPropertyDetailsBO.ViewingCommencement = Convert.ToDateTime(txtViewingComponent.Text.Trim)
        End If
        'If IsDate(txtValuationDate.Text.Trim) Then
        ' objPropertyDetailsBO.ValuationDate = Convert.ToDateTime(txtValuationDate.Text.Trim)
        'End If
        If IsDate(txtRentReview.Text.Trim) Then
            objPropertyDetailsBO.RentReview = Convert.ToDateTime(txtRentReview.Text.Trim)
        End If

        objPropertyDetailsBO.StockType = If(ddlStockType.SelectedValue > 0, Convert.ToInt32(ddlStockType.SelectedValue), objPropertyDetailsBO.StockType)
        objPropertyDetailsBO.TenureType = If(ddlTenureType.SelectedValue > 0, Convert.ToInt32(ddlTenureType.SelectedValue), objPropertyDetailsBO.TenureType)

        '       objPropertyDetailsBO()

        objPropertyDetailsBO.Ownership = If(ddlOwnerShip.SelectedValue > 0, Convert.ToInt32(ddlOwnerShip.SelectedValue), objPropertyDetailsBO.Ownership)
        objPropertyDetailsBO.RightToAcquire = Convert.ToInt32(rdbtnRightToAcquire.SelectedValue)
        objPropertyDetailsBO.FloodingRisk = Convert.ToBoolean(Convert.ToInt32(rdbtnFloodingRisk.SelectedValue))
        objPropertyDetailsBO.Status = If(ddlStatus.SelectedValue > 0, Convert.ToInt32(ddlStatus.SelectedValue), objPropertyDetailsBO.Status)

        If Not ddlStatus.SelectedItem.Text.Contains("Let") AndAlso ddlSubStatus.SelectedValue <= 0 Then

            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SubStatusError, False)
            Exit Sub

        End If


        objPropertyDetailsBO.SubStatus = If(ddlSubStatus.SelectedValue > 0, Convert.ToInt32(ddlSubStatus.SelectedValue), objPropertyDetailsBO.SubStatus)

        objPropertyDetailsBO.PropertyType = If(ddlPropType.SelectedValue > 0, Convert.ToInt32(ddlPropType.SelectedValue), objPropertyDetailsBO.PropertyType)
        objPropertyDetailsBO.DwellingType = If(ddlDwellingType.SelectedValue > 0, Convert.ToInt32(ddlDwellingType.SelectedValue), objPropertyDetailsBO.DwellingType)
        objPropertyDetailsBO.Level = If(ddlLevel.SelectedValue > 0, Convert.ToInt32(ddlLevel.SelectedValue), objPropertyDetailsBO.Level)
        objPropertyDetailsBO.AssetType = If(ddlAssetType.SelectedValue > 0, Convert.ToInt32(ddlAssetType.SelectedValue), objPropertyDetailsBO.AssetType)
        objPropertyDetailsBO.AssetTypeMain = If(ddlNROSHAssetTypeMain.SelectedValue > 0, Convert.ToInt32(ddlNROSHAssetTypeMain.SelectedValue), objPropertyDetailsBO.AssetTypeMain)
        'objPropertyDetailsBO.AssetTypeSub = If(ddlNROSHAssetTypeSub.SelectedValue > 0, Convert.ToInt32(ddlNROSHAssetTypeSub.SelectedValue), objPropertyDetailsBO.AssetTypeSub)
        objPropertyDetailsBO.MinPurchase = If(txtMinPurchase.Text.Trim <> String.Empty, Convert.ToDecimal(txtMinPurchase.Text.Trim), Nothing)
        objPropertyDetailsBO.PurchaseLevel = If(txtPurchaseLevel.Text.Trim <> String.Empty, Convert.ToDecimal(txtPurchaseLevel.Text.Trim), Nothing)
        objPropertyDetailsBO.GroundRent = txtGroundRent.Text.Trim
        objPropertyDetailsBO.LandLord = txtLandLord.Text.Trim
        objPropertyDetailsBO.UpdatedBy = SessionManager.getUserEmployeeId()
        If (ddlAssetType.SelectedItem.Text.Contains("Shared Ownership")) Then
        Else
            txtMinPurchase.Text = String.Empty
            txtPurchaseLevel.Text = String.Empty


        End If
        'Check if Assest Type is 'Shared Ownership' then 'Initial Min Purchase' and 'Current Purchase Level' are mandetory
        If (ddlAssetType.SelectedItem.Text.Contains("Shared Ownership")) Then
            txtMinPurchase.Enabled = True
            txtPurchaseLevel.Enabled = True
            If (objPropertyDetailsBO.MinPurchase = 0) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.InitialMinPurchase
                Exit Sub
            End If
            If (objPropertyDetailsBO.PurchaseLevel = 0) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.PurchaseLevel
                Exit Sub
            End If
        Else
            txtMinPurchase.Enabled = False
            txtPurchaseLevel.Enabled = False
            txtMinPurchase.Text = String.Empty
            txtPurchaseLevel.Text = String.Empty

        End If

        If (propertyImageUpload.HasFile) Then
            filePath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + SessionManager.getPropertyId() + "/Images/")
            fileFormat = System.IO.Path.GetExtension(propertyImageUpload.FileName)



            'If fileFormat = ".jpeg" Or fileFormat = ".jpg" Or fileFormat = ".png" Then
            If String.Equals(fileFormat, ".jpeg", StringComparison.InvariantCultureIgnoreCase) Or String.Equals(fileFormat, ".png", StringComparison.InvariantCultureIgnoreCase) Then
                If (Directory.Exists(filePath) = False) Then
                    Directory.CreateDirectory(filePath)
                End If
                objPropertyDetailsBO.ImageName = propertyImageUpload.FileName
                objPropertyDetailsBO.ImagePath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + SessionManager.getPropertyId() + "/Images/")
                objPropertyDetailsBO.IsDefault = True
                objPropertyDetailsBO.CreatedBy = SessionManager.getUserEmployeeId()
                propertyImageUpload.SaveAs(filePath + propertyImageUpload.FileName)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.FileFormatError

            End If

        End If
        SessionManager.setPropertiesDetailsBO(objPropertyDetailsBO)
    End Sub
#End Region

#Region "Get property image"

    Public Function GetPropertyImageName(ByVal propertyID As String) As String

        Dim resultDataSet As DataSet = New DataSet()
        Dim dtFiltered As DataTable
        Dim imageCount
        Dim imageUrl
        imageCount = objPropertyBl.getPropertyImages(resultDataSet, propertyID)
        If imageCount > 0 Then
            dtFiltered = Me.checkImage(resultDataSet.Tables(0))
            imageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/" + dtFiltered.Rows(0)("ImageName").ToString()
        Else
            imageUrl = ""

        End If
        Return imageUrl


    End Function

#End Region

#Region "Check whether image exists."

    Function checkImage(ByVal dtImages As DataTable) As DataTable
        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr(1).ToString()
            Dim fullPath As String = Server.MapPath(GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/") + imageName

            ' If Not System.IO.File.Exists(fullPath) Then
            'dr.Row.Delete()
            ' End If
        Next

        dtImages.AcceptChanges()

        Return dtImages



    End Function

#End Region



#Region "populate All Controls"
    ''' <summary>
    ''' populate All Controls
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Sub populateAllControls(ByRef dt As DataTable)
        Dim devid As Integer = If(Not IsDBNull(dt.Rows(0).Item("DEVELOPMENTID")) AndAlso dt.Rows(0).Item("DEVELOPMENTID") > 0, dt.Rows(0).Item("DEVELOPMENTID"), -1)
        If devid > 0 AndAlso Not IsNothing(ddlDevelopment.Items.FindByValue(devid.ToString())) Then
            ddlDevelopment.SelectedValue = devid
            populateBlockDropdownList(devid)
            ddlBlockName.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("BLOCKID")) AndAlso dt.Rows(0).Item("BLOCKID") > 0, dt.Rows(0).Item("BLOCKID"), Nothing)
            populatePhaseDropdownList(devid)
        End If
        'Getting completion and handover dates here
        Dim phaseId As Integer = If(Not IsDBNull(dt.Rows(0).Item("PhaseId")) AndAlso dt.Rows(0).Item("PhaseId") > 0, dt.Rows(0).Item("PhaseId"), Nothing)
        If phaseId > 0 AndAlso Not IsNothing(ddlPhase.Items.FindByValue(phaseId.ToString())) Then
            ddlPhase.SelectedValue = phaseId
            Me.populateCompleteionAndHandoverDates(phaseId)
        End If


        txtHouse.Text = If(Not IsDBNull(dt.Rows(0).Item("HOUSENUMBER")), dt.Rows(0).Item("HOUSENUMBER"), Nothing)
        txtAddress1.Text = If(Not IsDBNull(dt.Rows(0).Item("ADDRESS1")), dt.Rows(0).Item("ADDRESS1"), Nothing)
        txtAddress2.Text = If(Not IsDBNull(dt.Rows(0).Item("ADDRESS2")), dt.Rows(0).Item("ADDRESS2"), Nothing)
        txtAddress3.Text = If(Not IsDBNull(dt.Rows(0).Item("ADDRESS3")), dt.Rows(0).Item("ADDRESS3"), Nothing)
        txtTown.Text = If(Not IsDBNull(dt.Rows(0).Item("TOWNCITY")), dt.Rows(0).Item("TOWNCITY"), Nothing)
        txtCounty.Text = If(Not IsDBNull(dt.Rows(0).Item("COUNTY")), dt.Rows(0).Item("COUNTY"), Nothing)
        txtPostCode.Text = If(Not IsDBNull(dt.Rows(0).Item("POSTCODE")), dt.Rows(0).Item("POSTCODE"), Nothing)
        txtDefectsPeriod.Text = If(Not IsDBNull(dt.Rows(0).Item("DEFECTSPERIODEND")), dt.Rows(0).Item("DEFECTSPERIODEND"), Nothing)
        ddlStockType.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("STOCKTYPE")) AndAlso dt.Rows(0).Item("STOCKTYPE") > 0, dt.Rows(0).Item("STOCKTYPE"), Nothing)

        ddlTenureType.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("TENURETYPE")) AndAlso dt.Rows(0).Item("TENURETYPE") > 0, dt.Rows(0).Item("TENURETYPE"), Nothing)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = SessionManager.PropertyOwnerShip

        populatePropertyOwnerShipDropdownList(resultDataSet.Tables(ApplicationConstants.ownership), ddlTenureType.SelectedItem.Text)
        ddlOwnerShip.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("OWNERSHIP")) AndAlso dt.Rows(0).Item("OWNERSHIP") > 0, dt.Rows(0).Item("OWNERSHIP"), Nothing)
        txtLeaseStart.Text = If(Not IsDBNull(dt.Rows(0).Item("LeaseStart")), dt.Rows(0).Item("LeaseStart"), Nothing)
        txtLeaseEnd.Text = If(Not IsDBNull(dt.Rows(0).Item("LeaseEnd")), dt.Rows(0).Item("LeaseEnd"), Nothing)
        rdbtnRightToAcquire.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("RIGHTTOBUY")), dt.Rows(0).Item("RIGHTTOBUY"), Nothing)
        rdbtnFloodingRisk.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("FloodingRisk")), dt.Rows(0).Item("FloodingRisk"), Nothing)
        ddlStatus.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("STATUS")) AndAlso dt.Rows(0).Item("STATUS") > 0, dt.Rows(0).Item("STATUS").ToString(), Nothing)
        populatePropertySubStatusDropdownList(ddlStatus.SelectedValue)
        ddlSubStatus.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("SUBSTATUS")) AndAlso dt.Rows(0).Item("SUBSTATUS") > 0, dt.Rows(0).Item("SUBSTATUS"), Nothing)
        ddlPropType.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("PROPERTYTYPE")) AndAlso dt.Rows(0).Item("PROPERTYTYPE") > 0, dt.Rows(0).Item("PROPERTYTYPE"), Nothing)
        ddlAssetType.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("ASSETTYPE")) AndAlso dt.Rows(0).Item("ASSETTYPE") > 0, dt.Rows(0).Item("ASSETTYPE"), Nothing)
        ddlLevel.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("PROPERTYLEVEL")) AndAlso dt.Rows(0).Item("PROPERTYLEVEL") > 0, dt.Rows(0).Item("PROPERTYLEVEL"), Nothing)
        ddlDwellingType.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("DWELLINGTYPE")) AndAlso dt.Rows(0).Item("DWELLINGTYPE") > 0, dt.Rows(0).Item("DWELLINGTYPE"), Nothing)
        ddlNROSHAssetTypeMain.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("NROSHASSETTYPEMAIN")) AndAlso dt.Rows(0).Item("NROSHASSETTYPEMAIN") > 0, dt.Rows(0).Item("NROSHASSETTYPEMAIN"), Nothing)
        ' ddlNROSHAssetTypeSub.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("NROSHASSETTYPESUB")) AndAlso dt.Rows(0).Item("NROSHASSETTYPESUB") > 0, dt.Rows(0).Item("NROSHASSETTYPESUB"), Nothing)
        txtMinPurchase.Text = If(Not IsDBNull(dt.Rows(0).Item("MINPURCHASE")), dt.Rows(0).Item("MINPURCHASE"), Nothing)
        txtPurchaseLevel.Text = If(Not IsDBNull(dt.Rows(0).Item("PURCHASELEVEL")), dt.Rows(0).Item("PURCHASELEVEL"), Nothing)
        txtDateAcquired.Text = If(Not IsDBNull(dt.Rows(0).Item("DateAcquired")), dt.Rows(0).Item("DateAcquired"), Nothing)
        txtDateBuild.Text = If(Not IsDBNull(dt.Rows(0).Item("DATEBUILT")), dt.Rows(0).Item("DATEBUILT"), Nothing)
        txtViewingComponent.Text = If(Not IsDBNull(dt.Rows(0).Item("viewingcommencement")), dt.Rows(0).Item("viewingcommencement"), Nothing)
        txtGroundRent.Text = If(Not IsDBNull(dt.Rows(0).Item("GroundRent")), dt.Rows(0).Item("GroundRent"), Nothing)
        txtLandLord.Text = If(Not IsDBNull(dt.Rows(0).Item("LandLord")), dt.Rows(0).Item("LandLord"), Nothing)
        txtRentReview.Text = If(Not IsDBNull(dt.Rows(0).Item("RentReview")), dt.Rows(0).Item("RentReview"), Nothing)
        txtTitle.Text = If(Not IsDBNull(dt.Rows(0).Item("TitleNumber")), dt.Rows(0).Item("TitleNumber"), Nothing)
        txtDeed.Value = If(Not IsDBNull(dt.Rows(0).Item("DeedLocation")), dt.Rows(0).Item("DeedLocation"), Nothing)
        ViewPropertyImageLink.NavigateUrl = Me.GetPropertyImageName(SessionManager.getPropertyId())
        ddlHeatingFuel.SelectedValue = If(Not IsDBNull(dt.Rows(0).Item("ValueID")) AndAlso dt.Rows(0).Item("ValueID") > 0, dt.Rows(0).Item("ValueID"), Nothing)
        If Not ViewPropertyImageLink.NavigateUrl = "" Then
            ViewPropertyImageLink.Visible = True
        Else
            ViewPropertyImageLink.Visible = False
        End If

        If (ddlAssetType.SelectedItem.Text.Contains("Shared Ownership")) Then
            lblMinPurchase.Visible = True
            lblPurchaseLevel.Visible = True
        Else
            lblMinPurchase.Visible = False
            lblPurchaseLevel.Visible = False
        End If
        If (ddlAssetType.SelectedValue <> 10) Then
            txtMinPurchase.Text = String.Empty
            txtPurchaseLevel.Text = String.Empty


        End If
    End Sub
#End Region

#Region "reset All Controls"
    ''' <summary>
    ''' reset All Controls
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetAllControls()
        ddlDevelopment.SelectedValue = -1
        populateBlockDropdownList(ddlDevelopment.SelectedValue)
        ddlBlockName.SelectedValue = -1
        txtHouse.Text = String.Empty
        txtAddress1.Text = String.Empty
        txtAddress2.Text = String.Empty
        txtTown.Text = String.Empty
        txtCounty.Text = String.Empty
        txtPostCode.Text = String.Empty
        txtDefectsPeriod.Text = String.Empty
        ddlStockType.SelectedValue = -1
        ddlOwnerShip.SelectedValue = -1
        txtLeaseStart.Text = String.Empty
        txtLeaseEnd.Text = String.Empty
        rdbtnRightToAcquire.SelectedValue = 0
        rdbtnFloodingRisk.SelectedValue = 0
        ddlStatus.SelectedValue = -1
        ddlSubStatus.SelectedValue = -1
        ddlPropType.SelectedValue = -1
        ddlAssetType.SelectedValue = -1
        ddlLevel.SelectedValue = -1
        ddlDwellingType.SelectedValue = -1
        ddlNROSHAssetTypeMain.SelectedValue = -1
        'ddlNROSHAssetTypeSub.SelectedValue = -1
        txtMinPurchase.Text = String.Empty
        txtPurchaseLevel.Text = String.Empty
        txtDateAcquired.Text = String.Empty
        txtDateBuild.Text = String.Empty
        txtViewingComponent.Text = String.Empty
        txtGroundRent.Text = String.Empty
        txtLandLord.Text = String.Empty
        txtRentReview.Text = String.Empty
        ViewPropertyImageLink.Visible = False              ' new field
    End Sub
#End Region

#Region "populate Property Detail"
    ''' <summary>
    ''' Populate Property Detail
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populatePropertyDetail()
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyDetailByPropertyId(resultDataSet, SessionManager.getPropertyId())
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            Dim dt As DataTable = resultDataSet.Tables(0)
            populateAllControls(dt)
        End If
    End Sub
#End Region

#Region "Enable Disable Tabs"
    ''' <summary>
    ''' Enable Disable Tabs
    ''' </summary>
    ''' <param name="enable"></param>
    ''' <param name="viewIndex"></param>
    ''' <param name="lnkBtnSelected"></param>
    ''' <remarks></remarks>
    Public Sub enableDisableTabs(ByVal enable As Boolean, ByVal viewIndex As Integer, ByVal lnkBtnSelected As LinkButton)
        Dim lnkBtnSummaryTab As LinkButton = DirectCast(Parent.FindControl("lnkBtnSummaryTab"), LinkButton)
        lnkBtnSummaryTab.Enabled = enable
        Dim lnkBtnAttributesTab As LinkButton = DirectCast(Parent.FindControl("lnkBtnAttributesTab"), LinkButton)
        lnkBtnAttributesTab.Enabled = enable
        Dim lnkBtnFinancial As LinkButton = DirectCast(Parent.FindControl("lnkBtnFinancial"), LinkButton)
        lnkBtnFinancial.Enabled = enable
        Dim lnkBtnDocTab As LinkButton = DirectCast(Parent.FindControl("lnkBtnWarranty"), LinkButton)
        lnkBtnDocTab.Enabled = enable
        Dim lnkBtnHealthSafety As LinkButton = DirectCast(Parent.FindControl("lnkBtnHealthSafety"), LinkButton)
        lnkBtnHealthSafety.Enabled = enable
        lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass
        Dim mainView As MultiView = DirectCast(Parent.FindControl("MainView"), MultiView)
        mainView.ActiveViewIndex = viewIndex
    End Sub
#End Region

#Region "populate Completion And Handover Dates"

    Private Sub populateCompleteionAndHandoverDates(ByVal phaseId As Integer)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPhaseDetail(resultDataSet, phaseId)
        If Not IsNothing(resultDataSet.Tables(0)) AndAlso resultDataSet.Tables(0).Rows.Count > 0 Then
            If phaseId <> -1 Then
                txtCompletionDate.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0)("ActualCompletion")), Convert.ToDateTime(resultDataSet.Tables(0).Rows(0)("ActualCompletion")).ToShortDateString(), Nothing)
                txtHandoverDate.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0)("HandoverintoManagement")), Convert.ToDateTime(resultDataSet.Tables(0).Rows(0)("HandoverintoManagement")).ToShortDateString(), Nothing)

            End If
        End If

        If (phaseId <> -1) Then
            lblHandoverError.Visible = False
            lblCompletionError.Visible = False
        End If
    End Sub
#End Region

#Region "save Property"
    Private Sub saveProperty(ByVal objPropertyDetailsBO As PropertiesDetailsBO)
        If uiMessageHelper.IsError = False And lblCompletionError.Visible = False And lblCompletionError.Visible = False Then
            Dim complDate As Date
            If txtCompletionDate.Text <> "" Then
                complDate = Convert.ToDateTime(txtCompletionDate.Text)
            Else
                complDate = Date.MinValue
            End If

            Dim handDate As Date
            If txtHandoverDate.Text <> "" Then
                handDate = Convert.ToDateTime(txtHandoverDate.Text)
            Else
                handDate = Date.MinValue
            End If
            Dim result As String = objPropertyBl.SavePropertyDetail(objPropertyDetailsBO, complDate, handDate)
            If objPropertyDetailsBO.IsDefault = True Then
                objPropertyBl.SavePropertyImage(objPropertyDetailsBO)
            End If

            If result = "Exist" Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.PropertyAlreadyExist, True)
            ElseIf result = "Failed" Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavePropertyServerError, True)
            Else
                txtPropReference.Text = result
                SessionManager.setPropertyId(result)


                Dim source As String = Request.QueryString(PathConstants.Src)
                Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
                If (source IsNot Nothing AndAlso source = PathConstants.NewProperty) And (propertyId IsNot Nothing) Then

                    Dim lnkBtnSelected As LinkButton = DirectCast(Parent.FindControl("lnkBtnAttributesTab"), LinkButton)
                    Dim lnkBtnSummaryTab As LinkButton = DirectCast(Parent.FindControl("lnkBtnSummaryTab"), LinkButton)
                    lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
                    enableDisableTabs(True, 1, lnkBtnSelected)
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ItemAddedSuccessfuly, False)
                ElseIf (propertyId IsNot Nothing) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ItemAddedSuccessfuly, False)
                Else
                    Dim lnkBtnSelected As LinkButton = DirectCast(Parent.FindControl("lnkBtnAttributesTab"), LinkButton)
                    Dim lnkBtnSummaryTab As LinkButton = DirectCast(Parent.FindControl("lnkBtnSummaryTab"), LinkButton)
                    lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
                    enableDisableTabs(True, 1, lnkBtnSelected)

                    Response.Redirect(PathConstants.PropertyListing + "?" + PathConstants.PropertyIds + "=" + result + "&" + PathConstants.Src + "=" + PathConstants.NewProperty)
                End If
            End If
        End If
    End Sub
#End Region

#End Region

    Private Sub populateControls(ByVal propertyId As String)
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBl.getPropertyDetailByPropertyId(resultDataSet, propertyId)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            populateDropDownList()
            Dim dt As DataTable = resultDataSet.Tables(0)
            populateAllControls(dt)


        End If
    End Sub


    Protected Sub checkPhaseCompletion(sender As Object, _e As EventArgs) Handles txtCompletionDate.TextChanged
        If IsDate(txtCompletionDate.Text) = False And txtCompletionDate.Text <> "" Then
            lblCompletionError.Text = " Enter a valid date"
            lblCompletionError.Visible = True
        Else
            If ddlPhase.SelectedItem.Text = "Please select" And txtCompletionDate.Text <> "" And IsDate(txtCompletionDate.Text) Then
                lblCompletionError.Text = "Select phase first"
                lblCompletionError.Visible = True
            Else
                lblCompletionError.Visible = False
            End If
        End If
        If IsDate(txtHandoverDate.Text) = False And txtHandoverDate.Text <> "" Then
            lblHandoverError.Text = " Enter a valid date"
            lblHandoverError.Visible = True
        Else
            If ddlPhase.SelectedItem.Text = "Please select" And txtHandoverDate.Text <> "" And IsDate(txtHandoverDate.Text) Then
                lblHandoverError.Text = "Select phase first"
                lblHandoverError.Visible = True
            Else
                lblHandoverError.Visible = False
            End If
        End If
    End Sub

    Protected Sub checkPhaseHandover(sender As Object, _e As EventArgs) Handles txtHandoverDate.TextChanged
        If IsDate(txtCompletionDate.Text) = False And txtCompletionDate.Text <> "" Then
            lblCompletionError.Text = " Enter a valid date"
            lblCompletionError.Visible = True
        Else
            If ddlPhase.SelectedItem.Text = "Please select" And txtCompletionDate.Text <> "" And IsDate(txtCompletionDate.Text) Then
                lblCompletionError.Text = "Select phase first"
                lblCompletionError.Visible = True
            Else
                lblCompletionError.Visible = False
            End If
        End If
        If IsDate(txtHandoverDate.Text) = False And txtHandoverDate.Text <> "" Then
            lblHandoverError.Text = " Enter a valid date"
            lblHandoverError.Visible = True
        Else
            If ddlPhase.SelectedItem.Text = "Please select" And txtHandoverDate.Text <> "" And IsDate(txtHandoverDate.Text) Then
                lblHandoverError.Text = "Select phase first"
                lblHandoverError.Visible = True
            Else
                lblHandoverError.Visible = False
            End If
        End If

    End Sub


    Protected Sub ddlTenureType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTenureType.SelectedIndexChanged
        Try
            Dim resultDataSet As DataSet = New DataSet()
            resultDataSet = SessionManager.PropertyOwnerShip
            populatePropertyOwnerShipDropdownList(resultDataSet.Tables(ApplicationConstants.ownership), ddlTenureType.SelectedItem.Text)

        Catch ex As Exception

        End Try
    End Sub
End Class