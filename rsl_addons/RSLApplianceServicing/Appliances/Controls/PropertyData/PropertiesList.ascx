﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PropertiesList.ascx.vb"
    Inherits="Appliances.PropertiesList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<script type="text/javascript" charset="utf-8">

    function ShowJobsheetSummary(jsn) {
        __doPostBack('<%= btnHidden.UniqueID %>', jsn);
    }
     
</script>
<style type="text/css">
    .table-head
    {
        border-bottom: 1px solid #000;
        text-decoration: none;
    }
    .table-body
    {
        margin-left: 5px;
        margin-right: 5px;
    }
    .table-head-border-style
    {
        border: 1px solid #000;
        text-decoration: none;
    }
    .clear
    {
        clear: both;
    }
    .alternative-row-style
    {
        background-color: #E8E9EA !important;
    }
    .dashboard th{
        background: #fff;
        border-bottom: 4px solid #8b8687;
    }
    .dashboard th a{
        color: #000 !important;
    }
    .dashboard th img{
        float:right;
    }
</style>
<asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
<div style="width: 100%; float: left;">
    <asp:UpdatePanel ID="updPanelPropertiesList" runat="server">
        <ContentTemplate>
            <div style='margin-left: 15px;'>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false" Height="19px" Width="289px">
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
            <%--<div style="width: 100%; float: left;">
                <div style="float: left;">
                    <asp:Button ID="btnAddProperty" Text="Add New property" runat="server" CssClass="pdr-Button"
                        UseSubmitBehavior="false" OnClientClick="return changeView()" />
                </div>
                <div style="float: Right;">
                    <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;">
                        <asp:TextBox ID="txtSearch" runat="server" Width="200px" class="searchbox" Style="float: none"
                            onkeyup="TypingInterval();" AutoPostBack="false" AutoCompleteType="Search" UseSubmitBehavior="False"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                            TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                    </asp:Panel>
                </div>
            </div>
            <div class="clear">
            </div>--%>
            <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                <cc1:PagingGridView ID="grdPropertiesList" runat="server" AutoGenerateColumns="False" OnSorting="grdPropertiesList_Sorting"
                    OnRowDataBound="grdPropertiesList_RowDataBound" AllowSorting="True" PageSize="30" OnRowCreated="grdPropertiesList_RowCreated"
                    Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                    GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" PagerSettings-Visible="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Ref:" ItemStyle-CssClass="dashboard" SortExpression="PropertyId">
                            <ItemTemplate>
                                <asp:Label ID="lblPropertyId" runat="server" Text='<%# Bind("PropertyId") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                            <ItemTemplate>
                                <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Block:" SortExpression="Block">
                            <ItemTemplate>
                                <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tenure:" SortExpression="Tenure">
                            <ItemTemplate>
                                <asp:Label ID="lblTenure" runat="server" Text='<%# Bind("Tenure") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MV-T:" SortExpression="MVT">
                            <ItemTemplate>
                                <asp:Label ID="lblMVT" runat="server" Text='<%# Bind("MVT") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EUV-SH:" SortExpression="EUVSH">
                            <ItemTemplate>
                                <asp:Label ID="lblEUVSH" runat="server" Text='<%# Bind("EUVSH") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MV-VP:" SortExpression="MVVP">
                            <ItemTemplate>
                                <asp:Label ID="lblMVVP" runat="server" Text='<%# Bind("MVVP") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                </cc1:PagingGridView>
            </div>
            <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                    <div class="paging-left">
                        <span style="padding-right:10px;">
                            <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                                OnClick="lnkbtnPager_Click">
                                &lt;&lt;First
                            </asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                                OnClick="lnkbtnPager_Click">
                                &lt;Prev
                            </asp:LinkButton>
                        </span>
                        <span style="padding-right:10px;">
                            <b>Page:</b>
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            of
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                        </span>
                        <span style="padding-right:20px;">
                            <b>Result:</b>
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            to
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            of
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </span>
                        <span style="padding-right:10px;">
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                                OnClick="lnkbtnPager_Click">
                                    
                            </asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                                OnClick="lnkbtnPager_Click">
                                        
                            </asp:LinkButton>
                        </span>
                    </div>
                    <div style="float: right;">
                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                            Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                        <div class="field" style="margin-right: 10px;">
                            <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                        </div>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="changePageNumber" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
