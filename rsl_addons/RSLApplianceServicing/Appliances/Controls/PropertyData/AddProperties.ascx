﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AddProperties.ascx.vb"
    Inherits="Appliances.AddProperties" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div style="width: 100%; float: left;">
    <asp:UpdatePanel runat="server" ID="updPanelAddProperties">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" Font-Size="12px" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <table>
                <tr>
                    <td>
                        Select a template:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" Width="250px" Style="margin-right: 50px;" ID="ddlTemplate"
                            AutoPostBack="True" TabIndex="1">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="margin-left: 20px;">
                        Status: <span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" Width="250px" ID="ddlStatus" AutoPostBack="True">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlStatus"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Property Reference:
                    </td>
                    <td>
                        <asp:TextBox runat="server" Width="236px" ID="txtPropReference" ReadOnly="true" Text="AUTO GENERATED"
                            CssClass="searchText" TabIndex="2" />
                    </td>
                    <td>
                        Sub-Status: <span id="SpanSubStatus" class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSubStatus" runat="server" Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Development Name:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDevelopment" runat="server" Width="250px" AutoPostBack="true"
                            TabIndex="3">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlDevelopment"
                            InitialValue="-1" ErrorMessage="<br/>Please select a Development." Display="Dynamic"
                            ValidationGroup="save" ForeColor="Red" />
                    </td>
                    <td>
                        Property Type:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPropType" runat="server" Width="250px">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="ddlPropType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Block Name:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBlockName" runat="server" Width="250px" AutoPostBack="true"
                            TabIndex="4">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Dwelling Type:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDwellingType" runat="server" Width="250px">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="ddlDwellingType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Phase:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPhase" runat="server" Width="250px" AutoPostBack="true"
                            TabIndex="5">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlPhase"
                            InitialValue="-1" ErrorMessage="<br/>Please select a Phase." Display="Dynamic"
                            ValidationGroup="save" ForeColor="Red" />--%>
                    </td>
                    <td>
                        Level:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLevel" runat="server" Width="250px">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Title Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" Width="245px" TabIndex="6" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtHouse"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="<br/>Please enter valid title."
                            Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtTitle"
                            ValidationExpression="^[a-zA-Z0-9]*$" />
                    </td>
                    <td>
                        Deed Location:
                    </td>
                    <td>
                        <textarea id="txtDeed" rows="3" runat="server" tabindex="6" style="resize: none;
                            width: 244px;"></textarea>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="ddlAssetType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        House/Room Number:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHouse" runat="server" Width="245px" TabIndex="6" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHouse"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Asset Type:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAssetType" runat="server" Width="250px" AutoPostBack="True">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="ddlAssetType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Address1:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress1" runat="server" Width="245px" TabIndex="7" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAddress1"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Tenure:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlNROSHAssetTypeMain" runat="server" Width="250px" >
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address2:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress2" runat="server" Width="245px" TabIndex="8" />
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        Address3:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress3" runat="server" Width="245px" TabIndex="9" />
                    </td>
                    <td>
                        Initial Min Purchase(%):<label class="Required" runat="server" visible="false" id="lblMinPurchase">*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMinPurchase" runat="server" Width="245px" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>Please enter min purchase, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                            Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtMinPurchase"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                        <asp:RangeValidator ID="RangeValidator1" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                            ControlToValidate="txtMinPurchase" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                            Display="Dynamic" ForeColor="Red" ValidationGroup="save" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Town/City:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTown" runat="server" Width="245px" TabIndex="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTown"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Current Purchase Level(%):<label class="Required" runat="server" visible="false"
                            id="lblPurchaseLevel">*</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPurchaseLevel" runat="server" Width="245px" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="<br/>Please enter current purchase level, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                            Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtPurchaseLevel"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                        <asp:RangeValidator ID="RangeValidator2" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                            ControlToValidate="txtPurchaseLevel" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                            Display="Dynamic" ForeColor="Red" ValidationGroup="save" Type="Double" />
                    </td>
                </tr>
                <tr>
                    <td>
                        County:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCounty" runat="server" Width="245px" TabIndex="11" />
                    </td>
                    <td>
                        Date Acquired:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDateAcquired" runat="server" Width="150px" Style="float: left;" />
                        <asp:Image ID="imgCalendarDateAcquired" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateAcquired"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendarDateAcquired" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator4" runat="server" Style="float: left;"
                            ControlToValidate="txtDateAcquired" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Post Code:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPostCode" runat="server" Width="245px" onkeyup="RemoveSpecialChercterOnly(this)"
                            TabIndex="12" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPostCode"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Date Built:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDateBuild" runat="server" Width="150px" Style="float: left;" />
                        <asp:Image ID="imgCalendarDateBuild" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtDateBuild"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendarDateBuild" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator5" runat="server" Style="float: left;"
                            ControlToValidate="txtDateBuild" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Defects Period:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDefectsPeriod" runat="server" Width="150px" Style="float: left;"
                            TabIndex="13" />
                        <asp:Image ID="imgDefectsPeriod" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtDefectsPeriod"
                            Format="dd/MM/yyyy" PopupButtonID="imgDefectsPeriod" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator1" runat="server" Style="float: left;"
                            ControlToValidate="txtDefectsPeriod" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                    <td>
                        Viewing Commencement:
                    </td>
                    <td>
                        <asp:TextBox ID="txtViewingComponent" runat="server" Width="150px" Style="float: left;" />
                        <asp:Image ID="imgCalendarViewingComponent" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtViewingComponent"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendarViewingComponent" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator6" runat="server" Style="float: left;"
                            ControlToValidate="txtViewingComponent" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        BHA Ownership:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTenureType" runat="server" Width="250px" TabIndex="14" AutoPostBack="true">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="ddlTenureType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                    <td>
                        Stock Type/Agent:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStockType" runat="server" Width="250px" TabIndex="14">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="ddlStockType"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Confirmation of Ownership:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOwnerShip" runat="server" Width="250px" TabIndex="15">
                            <asp:ListItem Selected="True" Value="-1">Please select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="ddlOwnerShip"
                            InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" ForeColor="Red" />
                    </td>
                    <td>
                        Lease Start:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLeaseStart" runat="server" Width="150px" Style="float: left;"
                            TabIndex="16" />
                        <asp:Image ID="imgCalendarLeaseStart" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left" Width="25px" />
                        <asp:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtLeaseStart"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendarLeaseStart" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator2" runat="server" Style="float: left;"
                            ControlToValidate="txtLeaseStart" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Ground Rent:
                    </td>
                    <td>
                        <asp:TextBox ID="txtGroundRent" runat="server" Width="245px" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="<br/>Please enter ground rent, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                            Display="Dynamic" ValidationGroup="save" ForeColor="Red" ControlToValidate="txtGroundRent"
                            ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                        <asp:RangeValidator ID="RangeValidator3" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                            ControlToValidate="txtGroundRent" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                            Display="Dynamic" ForeColor="Red" ValidationGroup="save" Type="Double" />
                    </td>
                    <td>
                        Lease End:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLeaseEnd" runat="server" Width="150px" Style="float: left;" TabIndex="17" />
                        <asp:Image ID="imgCalendarLeaseEnd" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtLeaseEnd"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendarLeaseEnd" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator3" runat="server" Style="float: left;"
                            ControlToValidate="txtLeaseEnd" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Landlord:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLandLord" runat="server" Width="240px" />
                    </td>
                    <td>
                        Right to Acquire:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rdbtnRightToAcquire" runat="server" RepeatDirection="Horizontal"
                            TabIndex="18">
                            <asp:ListItem Text="Yes" Value="1" />
                            <asp:ListItem Selected="True" Text="No" Value="0" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Rent Review:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRentReview" runat="server" Style="float: left;" Width="150px" />
                        <asp:Image ID="imgCalendarRentReview" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalendarRentReview"
                            TargetControlID="txtRentReview" />
                        <br />
                        <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="txtRentReview"
                            Display="Dynamic" ErrorMessage=" Enter a valid date" ForeColor="Red" Operator="DataTypeCheck"
                            Style="float: left;" Type="Date" ValidationGroup="save" />
                    </td>
                    <td>
                        At Risk of Flooding?:
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rdbtnFloodingRisk" runat="server" RepeatDirection="Horizontal"
                            TabIndex="19">
                            <asp:ListItem Text="Yes" Value="1" />
                            <asp:ListItem Selected="True" Text="No" Value="0" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Completion Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCompletionDate" runat="server"
                            Style="float: left;" Width="150px"  AutoPostBack="True" />
                        <asp:Image ID="imgCalendarCompletionDate" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalendarCompletionDate"
                            TargetControlID="txtCompletionDate" />
                            <br /><br />
                        <asp:Label ID="lblCompletionError" runat="server" Text="Select phase first" Visible="false" ForeColor="Red" />
                    </td>
                    <td>
                        Property Image:
                    </td>
                    <td>
                        <asp:FileUpload ID="propertyImageUpload" runat="server" EnableViewState="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Handover Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtHandoverDate" runat="server"
                            Style="float: left;" Width="150px" AutoPostBack="True"  />
                        <asp:Image ID="imgCalendarHandoverDate" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="margin-right: 65px; float: left;" Width="25px" />
                        <asp:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalendarHandoverDate"
                            TargetControlID="txtHandoverDate" /><br /><br />
                        <asp:Label ID="lblHandoverError" runat="server" Text="Select phase first" Visible="false" ForeColor="Red" />
                   
                    </td>
                    <td>
                        <asp:HyperLink ID="ViewPropertyImageLink" runat="server" NavigateUrl="#" Target="_blank"
                            Visible="false">  View Image</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="4">
                        <asp:Button ID="btnCancel" runat="server" CssClass="pdr-Button" Text="Cancel" />
                        <asp:Button ID="btnSaveNext" runat="server" CssClass="pdr-Button" Text="Save Property &amp; Template"
                            ValidationGroup="Save" />
                        <asp:Button ID="btnSave" runat="server" CssClass="pdr-Button" Text="Save Property"
                            ValidationGroup="save" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Panel ID="pnlAddTemplatePopup" runat="server" BackColor="White" Width="440px"
        CssClass="modalPopup" Style="width: 440px; border-color: #CCCCCC; border-width: 1px;">
        <div class="pdr-wrap" style="height: 150px;">
            <div class="title-bar">
                Save as a Template
            </div>
            <table id="pnlAddNoteTable" width="400px" style='margin: 20px;'>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Template Name:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtTemplateName" runat="server" Width="200">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTemplateName"
                            ErrorMessage="*" ForeColor="Red" ValidationGroup="saveNext"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnPopUpCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnTemplateSave" runat="server" Text="Save" BackColor="White" ValidationGroup="saveNext" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopUpAddTemplate" runat="server" DynamicServicePath=""
        Enabled="True" PopupControlID="pnlAddTemplatePopup" DropShadow="true" CancelControlID="btnPopUpCancel"
        TargetControlID="lblDismiss" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblDismiss" runat="server"></asp:Label>
</div>
<%-- MESSAGE POPUP --%>
<asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
<ajaxToolkit:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
    PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
    CancelControlID="btnClose">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
    Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
    <br />
    <div>
        <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
            <tr style="background-color: Gray; height: 40px;">
                <td align="center">
                    <asp:Label ID="lblMessageStatus" Font-Bold="true" ForeColor="White" runat="server"
                        Text="Properties Template"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblUserMessage" runat="server" Text="Template selection would override the already saved values against this Property. Press 'OK' to continue."></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button Text="Ok" ID="btnOk" runat="server" Width="70px" CssClass="btn" Style="margin: 2px;"
                        OnClick="btnOk_Click" />
                    <asp:Button ID="btnClose" CssClass="btn" Width="70px" runat="server" Text="Cancel"
                        Style="margin: 2px;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<%-- MESSAGE POPUP --%>
<asp:Button ID="btnHidden4" runat="server" Text="" Style="display: none;" />
<asp:ModalPopupExtender ID="mdlPopupPrimaryHeatingFuel" runat="server" TargetControlID="btnHidden4"
    PopupControlID="pnlPrimaryHeatingFuel" Enabled="true" BackgroundCssClass="modalBackground"
    CancelControlID="btnClosePopUp">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPrimaryHeatingFuel" CssClass="left" runat="server" BackColor="White"
    Width="350px" Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
    <br />
    <div>
        <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
            <tr style="background-color: Gray; height: 40px;">
                <td align="center" colspan="2">
                    <asp:Label ID="txtLableHeading" Text="Confirm Primary Fuel Type" Font-Bold="true"
                        ForeColor="White" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-left: 10px;">
                   <b> Please confirm the Heating Fuel:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; width: 115px;">
                    Heating Fuel:
                </td>
                <td align="left">
                    <asp:DropDownList runat="server" ID="ddlHeatingFuel" Width="200">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvHeatingFuel" ControlToValidate="ddlHeatingFuel"
                        InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="saveProperty"
                        ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2" style=" padding-right:22px;">
                    <asp:Button ID="btnClosePopUp" CssClass="pdr-Button" Width="50px" runat="server" Text="Cancel" />
                    <asp:Button ID="btnSaveProperty" CssClass="pdr-Button" Width="100px" runat="server" Text="Save Property"
                        ValidationGroup="saveProperty" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
