﻿Imports AS_Utilities
Imports System.Threading

Public Class PropetiesDetail
    Inherits UserControlBase

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then


                If SessionManager.getPropertyId() = String.Empty Then
                    lnkBtnAttributesTab.Enabled = False
                    lnkBtnHealthSafety.Enabled = False
                    lnkBtnDocTab.Enabled = False
                    lnkBtnFinancial.Enabled = False
                    lnkBtnWarranty.Enabled = False
                    lnkBtnRestriction.Enabled = False
                Else
                    lnkBtnAttributesTab.Enabled = True
                    lnkBtnHealthSafety.Enabled = True
                    lnkBtnDocTab.Enabled = True
                    lnkBtnFinancial.Enabled = True
                    lnkBtnWarranty.Enabled = True
                    lnkBtnRestriction.Enabled = True
                End If

                Dim source As String = Request.QueryString(PathConstants.Src)
                If source IsNot Nothing AndAlso source = PathConstants.NewProperty Then
                    setSelectedMenuItemStyle(lnkBtnAttributesTab)
                    Attributes.populateAttributeControl()
                    mainView.ActiveViewIndex = 1
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ItemAddedSuccessfuly, False)
                Else
                    setSelectedMenuItemStyle(lnkBtnSummaryTab)
                    mainView.ActiveViewIndex = 0
                    'populatePropertyDetail()
                End If
            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty

        End Try
    End Sub
#End Region

#Region "populate Property Detail"
    Public Sub populatePropertyDetail()
        propertiesTab.populateDropDownList()
        propertiesTab.populatePropertyDetail()
    End Sub
#End Region


#Region "Menu (Link Button Events)"

#Region "lnk Btn Summary Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' 3- Call the user control load function to load data from datasource.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnSummaryTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            mainView.ActiveViewIndex = 0
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "link Button Doc Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDocTab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnDocTab.Click
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            mainView.ActiveViewIndex = 2
            Dim objPropertyDocumentBO As PropertyDocumentBO = New PropertyDocumentBO()
            objPropertyDocumentBO.PropertyId = SessionManager.getPropertyId()
            Document.bindToGrid(objPropertyDocumentBO)
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "link Button Attributes Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAttributesTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            Attributes.populateAttributeControl()
            mainView.ActiveViewIndex = 1
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "link Button Financial Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnFinancial_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnFinancial.Click
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            mainView.ActiveViewIndex = 3
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "link Button Health & Safety Tab Click"
    ''' <summary>
    ''' This Event/function is to perform different actions
    ''' 1- Set the styles of clicked item (link button), and also remaining.
    ''' 2- Select the view index of this user control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnHealthSafety_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnHealthSafety.Click
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            mainView.ActiveViewIndex = 5
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "link Button Restriction Tab Click"
    ''' <summary>
    ''' link Button Restriction Tab Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnRestriction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnRestriction.Click
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            mainView.ActiveViewIndex = 6
            Restriction.loadData()
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End Try
    End Sub
#End Region

#Region "Loads Warranties Tab for property."
    ''' <summary>
    ''' Loads Warranties Tab for property.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnWarranty_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnWarranty.Click
        Try
            setSelectedMenuItemStyle(DirectCast(sender, LinkButton))
            mainView.ActiveViewIndex = 4
            Dim propertyId As String = Request.QueryString("id")
            Warranty.populateDropdowns()
            Warranty.bindToGrid(propertyId)
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        uiMessageHelper.IsError = False
        uiMessageHelper.Message = String.Empty
    End Sub
#End Region

#Region "Set Selected Menu Item (Link Button) Style"

    ''' <summary>
    ''' Set the style of the selected link button(Horizontal Tab Menu):
    ''' Set all item to the initial styles and selected item differently.    ''' 
    ''' </summary>
    ''' <param name="lnkBtnSelected">Selected menu item.</param>
    ''' <remarks></remarks>
    Private Sub setSelectedMenuItemStyle(ByRef lnkBtnSelected As LinkButton)
        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnWarranty.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnFinancial.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnHealthSafety.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnRestriction.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass
    End Sub

#End Region


#End Region


End Class