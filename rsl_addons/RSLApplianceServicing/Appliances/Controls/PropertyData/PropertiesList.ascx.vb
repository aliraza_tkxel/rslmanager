﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class PropertiesList
    Inherits UserControlBase
#Region "Properties"

    Dim objPropertyBl As PropertyBL = New PropertyBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "PropertyId", 1, 30)
    Dim totalCount As Integer = 0

#End Region

#Region "Events"
#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                'populateDropDownList()

            End If
        Catch ex As ThreadAbortException
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            bindToGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region


#Region "Grid PropertiesList Sorting Event"
    ''' <summary>
    ''' Grid Properties List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdPropertiesList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPropertiesList.Sorting

        Try
            objPageSortBo = getPageSortBoViewState()

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdPropertiesList.PageIndex = 0
            objPageSortBo.setSortDirection()

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            populatePropertiesList(search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

    Protected Sub grdPropertiesList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        objPageSortBo = getPageSortBoViewState()
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If getPageSortBoViewState() IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub
#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)

                populatePropertiesList(search, True)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Gridview row databound"
    ''' <summary>
    ''' Gridview row databound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdPropertiesList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPropertiesList.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim row As GridViewRow = e.Row
                Dim lblPropertyId As Label = DirectCast(row.FindControl("lblPropertyId"), Label)
                e.Row.Attributes.Add("onclick", String.Format("javascript:ShowJobsheetSummary('{0}')", lblPropertyId.Text))
                'e.Row.Attributes.Add("onmouseover", "mouseIn(this);")
                'e.Row.Attributes.Add("onmouseout", "mouseOut(this);")
                e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'")

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Gridview row selecting"
    ''' <summary>
    ''' Gridview row selecting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Dim argument = Request.Form("__EVENTARGUMENT")

        Response.Redirect(PathConstants.PropertyListing + "?id=" + argument.ToString())
    End Sub
#End Region

#End Region

#Region "Functions"
#Region "Bind To Grid"
    ''' <summary>
    ''' Bind To Grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub bindToGrid()
        objPageSortBo = getPageSortBoViewState()
        Me.populatePropertiesList(ViewState.Item(ViewStateConstants.Search), True)
    End Sub

#End Region

#Region "Populate Properties List"
    ''' <summary>
    ''' Populate Properties List
    ''' </summary>
    ''' <param name="search"></param>
    ''' <param name="setSession"></param>
    ''' <remarks></remarks>
    Sub populatePropertiesList(ByVal search As String, ByVal setSession As Boolean)
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        objPageSortBo = getPageSortBoViewState()
        If Not IsNothing(search) AndAlso search <> "" Then
            objPageSortBo = New PageSortBO("DESC", "PropertyId", 1, 30)
        End If

        Dim resultDataSet As DataSet = New DataSet()
        ViewState.Add(ViewStateConstants.Search, search)
        totalCount = objPropertyBl.getPropertiesList(resultDataSet, search, objPageSortBo)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            setPageSortBoViewState(objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        End If

        grdPropertiesList.VirtualItemCount = totalCount
        grdPropertiesList.DataSource = resultDataSet
        grdPropertiesList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdPropertiesList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdPropertiesList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If
        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        objPageSortBo = getPageSortBoViewState()
        If grdPropertiesList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdPropertiesList.PageCount Then
            grdPropertiesList.VirtualItemCount = getVirtualItemCountViewState()
            grdPropertiesList.PageIndex = objPageSortBo.PageNumber - 1
            grdPropertiesList.DataSource = getResultDataSetViewState()
            grdPropertiesList.DataBind()

            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        End If
    End Sub

#End Region
#End Region


#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region


End Class