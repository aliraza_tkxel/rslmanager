﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Globalization
Imports System.IO

Public Class DefectManagement
    Inherits UserControlBase

#Region "Properties"
    Private Const SaveText As String = "Save"
    Private Const AmendText As String = "Amend"
    Dim _readOnly As Boolean = False
#End Region

#Region "Control Events"

    Public Event saveButton_Clicked(ByVal sender As Object, ByVal e As EventArgs)
    Public Event cancelButton_Clicked(ByVal sender As Object, ByVal e As EventArgs)
    Public Event showPoupAgain(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Events Handling"

#Region "User Control Events Handling"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PageName As String = sender.Page.ToString()
        If PageName.Contains("properties") Then
            _readOnly = False
        ElseIf PageName.Contains("propertyrecord") Then
            _readOnly = True
        End If
        If _readOnly = True Then
            btnSave.Visible = False
        End If
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        'Page.Form.Attributes.Add("enctype", "multipart/form-data")
    End Sub

#End Region

#Region "Save Button Click"

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try

            If btnSave.Text = SaveText Then
                Dim saveStatus = saveApplianceDefect()
                If saveStatus Then
                    btnSave.Enabled = False
                    RaiseEvent saveButton_Clicked(sender, e)
                Else
                    RaiseEvent showPoupAgain(sender, e)
                End If
            ElseIf btnSave.Text = AmendText Then
                prepareAmendDefect()
                RaiseEvent showPoupAgain(sender, e)
            End If



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
                RaiseEvent showPoupAgain(sender, e)
            End If
        End Try
    End Sub

#End Region

#Region "Cancel Button Click"

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        RaiseEvent cancelButton_Clicked(sender, e)
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Prepare Add Defect"

    Public Sub prepareAddDefect(ByVal propertyId As String)
        lblDefectHeading.Text = "Add New Defect"
        lblType.Text = ApplicationConstants.Appliance
        btnSave.Text = SaveText
        ' Enable save button, it is disables after successful defect save.
        btnSave.Enabled = True
        setPropertyIdViewState(propertyId)
        ' Show all editable fields
        changeAllEditableFieldsVisibility(visible:=True)
        changeVisibilityOfAllReadOnlyFields(visible:=False)
        ' Reset All editable fields to default value i.e empty textboxes etc

        populateLookUpFields(propertyId)
        resetEditableFields()
        setDefaultTrade()

    End Sub

#End Region

#Region "Populate Lookups"

    Private Sub populateLookUpFields(ByVal propertyId As String)

        Dim defectBL As New DefectsBL()

        Dim defectsLookUpsDS As New DataSet()
        defectBL.getDefectManagementLookupValues(defectsLookUpsDS, propertyId)

        ddlDefectCategories.DataSource = defectsLookUpsDS.Tables(0)
        ddlDefectCategories.DataBind()

        ddlAppliances.DataSource = defectsLookUpsDS.Tables(1)
        ddlAppliances.DataBind()

        ddlPartsOrderedBy.DataSource = defectsLookUpsDS.Tables(2)
        ddlPartsOrderedBy.DataBind()

        ddlPriority.DataSource = defectsLookUpsDS.Tables(3)
        ddlPriority.DataBind()

        Dim dtTrade As DataTable = defectsLookUpsDS.Tables(4)
        ddlTrade.DataSource = dtTrade
        ddlTrade.DataBind()

        GeneralHelper.PopulateDropDownWithDurationHours(ddlEstimatedDuration)

    End Sub

#End Region

#Region "Save Appliance Defect"

    Public Function saveApplianceDefect() As Boolean
        Dim saveStatus = False

        Dim propertyDefectId As Integer = -1
        Integer.TryParse(hidDefectId.Value, propertyDefectId)

        Dim objDefectBO As ApplianceDefectBO = New ApplianceDefectBO()
        Dim isValidated As Boolean = True

        objDefectBO.PropertyId = getPropertyIdViewState()

        objDefectBO.PropertyDefectId = propertyDefectId

        ' ============================== Validate Defect ==============================

        If propertyDefectId = -1 Then
            'Validate Defect Category
            If isValidated AndAlso ddlDefectCategories.Items.Count() > 0 AndAlso Not ddlDefectCategories.SelectedItem.Value = 0 Then
                objDefectBO.CategoryId = ddlDefectCategories.SelectedValue
            Else
                isValidated = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectCategory, True)
                Exit Function
            End If

            If Not Date.TryParseExact(txtDefectDate.Text, "dd/MM/yyyy", New CultureInfo("en-GB"), DateTimeStyles.None, objDefectBO.DefectDate) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.notValidDateFormat, True)
                Exit Function
            End If

            objDefectBO.IsDefectIdentified = Boolean.Parse(rbtnGrpIsDefectIdentified.SelectedValue)
            objDefectBO.DefectIdentifiedNotes = txtDefectNotes.Text

            objDefectBO.IsRemedialActionTaken = Boolean.Parse(rbtnGrpIsRemedialActionTaken.SelectedValue)
            objDefectBO.RemedialActionNotes = txtRemedialActionNotes.Text

            objDefectBO.IsWarningNoteIssued = Boolean.Parse(rbtnGrpIsWarningNoteIssued.SelectedValue)

            'Validate Appliance
            If isValidated AndAlso ddlAppliances.Items.Count() > 0 AndAlso Not ddlAppliances.SelectedValue = 0 Then
                objDefectBO.ApplianceId = ddlAppliances.SelectedValue
            Else
                isValidated = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAppliance, True)
                Exit Function
            End If

            If isValidated AndAlso txtSerialNumber.Text.Trim = String.Empty Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SerialNumber, True)
                objDefectBO.SerialNumber = txtSerialNumber.Text.Trim()
                Exit Function
            End If
            objDefectBO.SerialNumber = txtSerialNumber.Text.Trim()
            objDefectBO.GcNumber = String.Format("{0}-{1}-{2}", txtGCNumber0.Text.Trim(), txtGCNumber1.Text.Trim(), txtGCNumber2.Text.Trim())
            objDefectBO.IsWarningTagFixed = chkBoxWarningTagFixed.Checked

            GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(objDefectBO.IsAppliancedDisconnected, rbtnGrpIsDisconnected.SelectedValue)
        End If

        GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(objDefectBO.IsPartsRequired, rbtnGrpIsPartsRequired.SelectedValue)
        GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(objDefectBO.IsPartsOrdered, rbtnGrpIsPartsOrdered.SelectedValue)

        objDefectBO.PartsOrderedBy = Nothing
        'If objDefectBO.IsPartsOrdered Then
        objDefectBO.PartsOrderedBy = ddlPartsOrderedBy.SelectedValue
        'End If

        Dim partsDueDate As DateTime
        If txtPartsDueDate.Text <> String.Empty AndAlso Not Date.TryParseExact(txtPartsDueDate.Text, "dd/MM/yyyy", New CultureInfo("en-GB"), DateTimeStyles.None, partsDueDate) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.notValidDateFormat, True)
            Exit Function
        ElseIf txtPartsDueDate.Text <> String.Empty Then
            objDefectBO.PartsDueDate = partsDueDate
        End If

        objDefectBO.PartsDescription = txtPartsDescription.Text.Trim
        objDefectBO.PartsLocation = txtPartsLocation.Text.Trim

        objDefectBO.IsTwoPersonsJob = Boolean.Parse(rbtnGrpIsTwoPersonJob.SelectedValue)
        objDefectBO.ReasonForSecondPerson = txtReasonFor2ndPerson.Text.Trim()

        objDefectBO.EstimatedDuration = ddlEstimatedDuration.SelectedValue
        objDefectBO.PriorityId = ddlPriority.SelectedValue
        objDefectBO.TradeId = ddlTrade.SelectedValue
        objDefectBO.UserId = SessionManager.getUserEmployeeId

        ' ============================== End - Validate Defect ==============================
        Dim filePath As String = String.Empty
        'Dim fileName As String = String.Empty
        filePath = Server.MapPath(ResolveClientUrl(GeneralHelper.getImageUploadPath()) + SessionManager.getPropertyId() + "/Images/")
        'If (Directory.Exists(filePath) = False) Then
        '    Directory.CreateDirectory(filePath)
        'End If

        'fileName = Path.GetFileName(flUploadPhoto.FileName)
        'fileName = getUniqueFileName()
        'Dim filePathName As String = filePath + fileName
        'flUploadPhoto.SaveAs(filePathName)

        objDefectBO.FilePath = lblFileName.Text
        objDefectBO.PhotoName = lblFileName.Text
        objDefectBO.PhotosNotes = txtPhotoNotes.Text.Trim

        Dim objPropertyBl As PropertyBL = New PropertyBL()
        'If getAppliaceDefectBOViewState().RequestType = "Property" Then
        objPropertyBl.saveDefect(objDefectBO)
        'Else
        'objPropertyBl.saveDefectForSchemeBlock(objDefectBO)
        'End If

        saveStatus = True
        Return saveStatus

    End Function

#End Region

#Region "View Defect Details"

    Public Sub viewDefectDetails(ByVal propertyDefectId As String)
        lblDefectHeading.Text = "Defect Details"
        btnSave.Text = AmendText
        btnSave.Enabled = True
        changeAllEditableFieldsVisibility(visible:=False)
        Dim objPropertyBl As PropertyBL = New PropertyBL()
        Dim objApplianceDefectBO As New ApplianceDefectBO()
        objPropertyBL.getPropertyDefectDetails(propertyDefectId, objApplianceDefectBO)
        changeVisibilityOfAllReadOnlyFields(visible:=True)
        changeAllEditableFieldsVisibility(visible:=False)
        'lblPhotoGraphNotes.Visible = True

        populateDefectDetails(objApplianceDefectBO)
        ' Save appliance defect bo to fill values when modifing defect
        setAppliaceDefectBOViewState(objApplianceDefectBO)
    End Sub

#End Region

#Region "Set Default Trade"

    Private Sub setDefaultTrade()

        For Each item As ListItem In ddlTrade.Items
            If item.Text = ApplicationConstants.DefaultDefectTrade Then
                ddlTrade.SelectedValue = item.Value
                Exit For
            End If
        Next

    End Sub

#End Region

#Region "Prepare Amend Defect"

    Private Sub prepareAmendDefect()
        btnSave.Text = SaveText
        changeAmendModeEditableFieldsVisibility(Visible:=True)
        changeAmendModeReadonlyFieldsVisibility(Visible:=False)
        populateLookUpFields(getPropertyIdViewState)
        populateAmendEditableFields()
    End Sub

#End Region

#Region "Change All editable Fields visibility"

    Private Sub changeAllEditableFieldsVisibility(ByVal visible As Boolean)
        GeneralHelper.changeServerControlsVisibility(visible, ddlDefectCategories _
                                         , txtDefectDate _
                                         , imgCalDefectDate _
                                         , rbtnGrpIsDefectIdentified _
                                         , txtDefectNotes _
                                         , rbtnGrpIsRemedialActionTaken _
                                         , txtRemedialActionNotes _
                                         , rbtnGrpIsWarningNoteIssued _
                                         , ddlAppliances _
                                         , txtSerialNumber _
                                         , txtGCNumber0, txtGCNumber1, txtGCNumber2 _
                                         , chkBoxWarningTagFixed _
                                         , rbtnGrpIsDisconnected _
                                         , rbtnGrpIsPartsRequired _
                                         , rbtnGrpIsPartsOrdered _
                                         , ddlPartsOrderedBy _
                                         , txtPartsDueDate _
                                         , imgPartsDueDate _
                                         , txtPartsDescription _
                                         , txtPartsLocation _
                                         , rbtnGrpIsTwoPersonJob _
                                         , txtReasonFor2ndPerson _
                                         , ddlEstimatedDuration _
                                         , ddlPriority _
                                         , ddlTrade _
                                         , txtPhotoNotes _
                                         , btnUploadPhotos _
                                         , lblFileName _
                                         , lblPhotoGraphNotes _
                                         , lblPhotograph _
                                         , lblPhotoNotes)
        'lblPhotograph.Visible = visible
        'lblPhotoGraphNotes.Visible = visible
    End Sub

#End Region

#Region "Change Visibility of all ready only fields"

    Private Sub changeVisibilityOfAllReadOnlyFields(ByVal visible As Boolean)
        GeneralHelper.changeServerControlsVisibility(visible, lbldefectCategory _
                                                     , lbldefectCategory _
                                                     , lblDefectDate _
                                                     , lblIsDefectIdentified _
                                                     , lblDefectNotes _
                                                     , pnlDefectNotes _
                                                     , lblIsRemedialActionTaken _
                                                     , pnlRemedialActionNotes _
                                                     , lblRemedialActionNotes _
                                                     , lblIsWarningNoteIssued _
                                                     , lblAppliances _
                                                     , lblBoiler _
                                                     , lblSerialNumber _
                                                     , lblGcNumber _
                                                     , lblIsWarningTagFixed _
                                                     , lblIsDisconnected _
                                                     , lblIsPartsRequired _
                                                     , lblIsPartsOrdered _
                                                     , lblPartsOrderedBy _
                                                     , lblPartsDueDate _
                                                     , lblPartsDescription _
                                                     , lblPartsLocation _
                                                     , lblIsTwoPersonJob _
                                                     , lblReasonFor2ndPerson _
                                                     , lblEstimatedDuration _
                                                     , lblPriority _
                                                     , lblTrade _
                                                     , lblPhotoNotes _
                                                     , lblPhotoGraphNotes)
    End Sub

#End Region

#Region "Change Visibility of amend mode editable fields"

    Private Sub changeAmendModeEditableFieldsVisibility(Visible As Boolean)
        GeneralHelper.changeServerControlsVisibility(Visible, rbtnGrpIsPartsRequired _
                                                     , rbtnGrpIsPartsOrdered _
                                                     , txtPartsDueDate _
                                                     , imgPartsDueDate _
                                                     , txtPartsDescription _
                                                     , txtPartsLocation _
                                                     , rbtnGrpIsTwoPersonJob _
                                                     , txtReasonFor2ndPerson _
                                                     , ddlEstimatedDuration _
                                                     , ddlPriority _
                                                     , ddlTrade _
                                                     )
    End Sub

#End Region

#Region "Change Visibility of amend mode ready only fields"

    Private Sub changeAmendModeReadonlyFieldsVisibility(Visible As Boolean)
        GeneralHelper.changeServerControlsVisibility(Visible, lblIsPartsRequired _
                                                     , lblIsPartsOrdered _
                                                     , lblPartsDueDate _
                                                     , lblPartsDescription _
                                                     , lblPartsLocation _
                                                     , lblIsTwoPersonJob _
                                                     , lblReasonFor2ndPerson _
                                                     , lblEstimatedDuration _
                                                     , lblPriority _
                                                     , lblTrade _
                                                     )
    End Sub

#End Region

#Region "Populate Amend Editable Fields"

    Private Sub populateAmendEditableFields()
        Dim objApplianceDefectBO As ApplianceDefectBO = getAppliaceDefectBOViewState()
        If Not IsNothing(objApplianceDefectBO) Then

            rbtnGrpIsPartsRequired.SelectedValue = String.Empty
            If Not IsNothing(objApplianceDefectBO.IsPartsRequired) Then
                rbtnGrpIsPartsRequired.SelectedValue = objApplianceDefectBO.IsPartsRequired.ToString()
            End If

            rbtnGrpIsPartsOrdered.SelectedValue = String.Empty
            If Not IsNothing(objApplianceDefectBO.IsPartsOrdered) Then
                rbtnGrpIsPartsOrdered.SelectedValue = objApplianceDefectBO.IsPartsOrdered.ToString()
            End If

            txtPartsDueDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.PartsDueDate, defaultOutput:=String.Empty)
            txtPartsDescription.Text = objApplianceDefectBO.PartsDescription.Trim
            txtPartsLocation.Text = objApplianceDefectBO.PartsLocation

            If Not IsNothing(objApplianceDefectBO.IsTwoPersonsJob) Then
                rbtnGrpIsTwoPersonJob.SelectedValue = objApplianceDefectBO.IsTwoPersonsJob.ToString()
            End If

            txtReasonFor2ndPerson.Text = objApplianceDefectBO.ReasonForSecondPerson

            If Not IsNothing(objApplianceDefectBO.EstimatedDuration) Then
                ddlEstimatedDuration.SelectedValue = Convert.ToInt32(objApplianceDefectBO.EstimatedDuration)
            End If

            If Not IsNothing(objApplianceDefectBO.PriorityId) Then
                ddlPriority.SelectedValue = objApplianceDefectBO.PriorityId
            End If

            If Not IsNothing(objApplianceDefectBO.TradeId) Then
                ddlTrade.SelectedValue = objApplianceDefectBO.TradeId
            End If

        End If
    End Sub

#End Region

#Region "Populate Defect Details"

    Private Sub populateDefectDetails(objApplianceDefectBO As ApplianceDefectBO)
        hidDefectId.Value = objApplianceDefectBO.PropertyDefectId
        lbldefectCategory.Text = objApplianceDefectBO.CategoryDescription
        lblDefectDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.DefectDate)
        lblIsDefectIdentified.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsDefectIdentified)
        lblDefectNotes.Text = objApplianceDefectBO.DefectIdentifiedNotes
        lblIsRemedialActionTaken.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsRemedialActionTaken)
        lblRemedialActionNotes.Text = objApplianceDefectBO.RemedialActionNotes
        lblIsWarningNoteIssued.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsWarningNoteIssued)

        If objApplianceDefectBO.ApplianceId > 0 Then
            lblType.Text = ApplicationConstants.Appliance
            lblAppliances.Text = objApplianceDefectBO.Appliance
            lblBoiler.Text = ""
        Else
            lblType.Text = ApplicationConstants.Boiler
            lblBoiler.Text = objApplianceDefectBO.Boiler
            lblAppliances.Text = ""
        End If

        lblSerialNumber.Text = objApplianceDefectBO.SerialNumber
        lblGcNumber.Text = objApplianceDefectBO.GcNumber
        lblIsWarningTagFixed.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsWarningTagFixed)
        lblIsDisconnected.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsAppliancedDisconnected)
        lblIsPartsRequired.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsPartsRequired)
        lblIsPartsOrdered.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsPartsOrdered)
        lblPartsOrderedBy.Text = objApplianceDefectBO.PartsOrderedByName
        lblPartsDueDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.PartsDueDate)
        lblPartsDescription.Text = objApplianceDefectBO.PartsDescription
        lblPartsLocation.Text = objApplianceDefectBO.PartsLocation
        lblIsTwoPersonJob.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsTwoPersonsJob)
        lblReasonFor2ndPerson.Text = objApplianceDefectBO.ReasonForSecondPerson
        If Not IsNothing(objApplianceDefectBO.EstimatedDuration) Then
            lblEstimatedDuration.Text = GeneralHelper.getSingularOrPluralPostFixForNumericValues(objApplianceDefectBO.EstimatedDuration, "hour")
        Else
            lblEstimatedDuration.Text = "N/A"
        End If

        lblPriority.Text = objApplianceDefectBO.PriorityName
        lblTrade.Text = objApplianceDefectBO.Trade
        lblPhotoNotes.Text = objApplianceDefectBO.PhotosNotes
    End Sub

#End Region

#Region "Reset Editable Fields"

    Private Sub resetEditableFields()
        For Each serverControl In pnldefectFilds.Controls
            If TypeOf serverControl Is TextBox Then
                serverControl.Text = String.Empty
            End If
            If TypeOf serverControl Is DropDownList Then
                CType(serverControl, DropDownList).SelectedIndex = 0
            End If
        Next
        rbtnGrpIsDefectIdentified.SelectedIndex = 0
        rbtnGrpIsRemedialActionTaken.SelectedIndex = 1
        rbtnGrpIsWarningNoteIssued.SelectedIndex = 1
        rbtnGrpIsDisconnected.SelectedIndex = 2
        rbtnGrpIsPartsRequired.SelectedIndex = 2
        rbtnGrpIsPartsOrdered.SelectedIndex = 2
        rbtnGrpIsTwoPersonJob.SelectedIndex = 0
        hidDefectId.Value = "-1"
        lblFileName.Text = String.Empty
    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "PropertyId"

#Region "Get PropertyId View State"
    Public Function getPropertyIdViewState() As String
        Dim propertyId As String = String.Empty
        If (IsNothing(ViewState(ViewStateConstants.PropertyId)) = False) Then
            propertyId = ViewState(ViewStateConstants.PropertyId).ToString()
        End If
        Return propertyId
    End Function
#End Region

#Region "set PropertyId View State"

    Protected Sub setPropertyIdViewState(ByRef propertyId As String)
        ViewState(ViewStateConstants.PropertyId) = propertyId
    End Sub

#End Region

#End Region

#Region "Applianc Defect BO"

#Region "Get Appliance Defect BO"

    Private Function getAppliaceDefectBOViewState() As ApplianceDefectBO
        Dim objApplianceDefectBO As ApplianceDefectBO = Nothing
        If Not IsNothing(ViewState(ViewStateConstants.ApplianceDefectBO)) Then
            objApplianceDefectBO = CType(ViewState(ViewStateConstants.ApplianceDefectBO), ApplianceDefectBO)
        End If
        Return objApplianceDefectBO
    End Function

#End Region

#Region "Set Appliance Defect BO"

    Private Sub setAppliaceDefectBOViewState(ByVal objApplianceDefectBO As ApplianceDefectBO)
        ViewState(ViewStateConstants.ApplianceDefectBO) = objApplianceDefectBO
    End Sub

#End Region

#End Region

#End Region


End Class