﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DefectManagement

    '''<summary>
    '''pnlDefect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDefect As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''updPanelAddEditDefect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelAddEditDefect As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDefectHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDefectHeading As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnldefectFilds control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnldefectFilds As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlDefectCategories control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDefectCategories As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rfvddlDefectCategories control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvddlDefectCategories As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''lbldefectCategory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldefectCategory As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hidDefectId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidDefectId As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''txtDefectDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDefectDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''imgCalDefectDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCalDefectDate As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''calDefectDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calDefectDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''lblDefectDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDefectDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsDefectIdentified control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsDefectIdentified As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsDefectIdentified control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsDefectIdentified As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtDefectNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDefectNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''valDefectNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents valDefectNotes As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''pnlDefectNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDefectNotes As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblDefectNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDefectNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsRemedialActionTaken control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsRemedialActionTaken As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsRemedialActionTaken control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsRemedialActionTaken As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRemedialActionNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRemedialActionNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''valActionNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents valActionNotes As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''pnlRemedialActionNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRemedialActionNotes As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblRemedialActionNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRemedialActionNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsWarningNoteIssued control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsWarningNoteIssued As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsWarningNoteIssued control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsWarningNoteIssued As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlAppliances control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAppliances As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rfvddlAppliances control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvddlAppliances As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''lblAppliances control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAppliances As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBoiler control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBoiler As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSerialNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSerialNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblSerialNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSerialNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtGCNumber0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGCNumber0 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtGCNumber1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGCNumber1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtGCNumber2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGCNumber2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblGcNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGcNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkBoxWarningTagFixed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkBoxWarningTagFixed As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''lblIsWarningTagFixed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsWarningTagFixed As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsDisconnected control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsDisconnected As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsDisconnected control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsDisconnected As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsPartsRequired control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsPartsRequired As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsPartsRequired control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsPartsRequired As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsPartsOrdered control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsPartsOrdered As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsPartsOrdered control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsPartsOrdered As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlPartsOrderedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPartsOrderedBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPartsOrderedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPartsOrderedBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPartsDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPartsDueDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''imgPartsDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgPartsDueDate As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''calPartsDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calPartsDueDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''lblPartsDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPartsDueDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPartsDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPartsDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblPartsDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPartsDescription As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPartsLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPartsLocation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblPartsLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPartsLocation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbtnGrpIsTwoPersonJob control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbtnGrpIsTwoPersonJob As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblIsTwoPersonJob control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIsTwoPersonJob As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtReasonFor2ndPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReasonFor2ndPerson As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblReasonFor2ndPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReasonFor2ndPerson As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlEstimatedDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEstimatedDuration As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblEstimatedDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEstimatedDuration As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPriority As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPriority As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlTrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlTrade As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblTrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhotograph control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhotograph As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnUploadPhotos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUploadPhotos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblFileName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFileName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhotoGraphNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhotoGraphNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPhotoNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPhotoNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblPhotoNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhotoNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlDefectButtons control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDefectButtons As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
End Class
