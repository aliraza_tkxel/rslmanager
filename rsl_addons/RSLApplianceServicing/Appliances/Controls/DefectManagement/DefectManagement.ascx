﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectManagement.ascx.vb"
    Inherits="Appliances.DefectManagement" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style>
    .tableTopAlignedCells td
    {
        vertical-align: top;
        height: 25px;
    }
    .required
    {
        color: Red;
    }
    .defectsFieldsPanel
    {
        height: 500px;
        overflow: auto;
    }
    input[type="text"], textarea, input[type="file"]
    {
        resize: none;
        width: 220px;
    }
    
    select
    {
        width: 225px;
    }
</style>
<asp:Panel runat="server" ID="pnlDefect" Style="padding: 10px;">
    <asp:UpdatePanel ID="updPanelAddEditDefect" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <asp:Label Text="Add Appliance Defect" runat="server" ID="lblDefectHeading" Font-Names="ArialBlack"
                Font-Size="Larger" Font-Bold="true" />
            <hr />
            <asp:Panel runat="server" ID="pnldefectFilds" CssClass="defectsFieldsPanel">
                <table class="tableTopAlignedCells">
                    <tbody>
                        <tr>
                            <td>
                                Category:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlDefectCategories" DataValueField="CategoryId"
                                    DataTextField="CategoryName">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="rfvddlDefectCategories" InitialValue="-1"
                                    ErrorMessage="</br>Please select a Defect Categories" Display="Dynamic" ValidationGroup="save"
                                    ControlToValidate="ddlDefectCategories" ForeColor="Red" />
                                <asp:Label runat="server" ID="lbldefectCategory" Visible="false" />
                                <asp:HiddenField runat="server" ID="hidDefectId" Value="-1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDefectDate" />
                                <asp:Image ImageUrl="~/Images/calendar.png" runat="server" ID="imgCalDefectDate"
                                    AlternateText="Open Calendar" />
                                <ajaxToolkit:CalendarExtender ID="calDefectDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                    TargetControlID="txtDefectDate" PopupButtonID="imgCalDefectDate" PopupPosition="BottomRight"
                                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
                                <asp:Label Text="" runat="server" ID="lblDefectDate" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Defect Identified:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsDefectIdentified" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" Selected="True" />
                                    <asp:ListItem Value="False" Text="No" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsDefectIdentified" Visible="false" />
                                <br />
                                <asp:TextBox runat="server" ID="txtDefectNotes" TextMode="MultiLine" Rows="3" />
                                <asp:RegularExpressionValidator runat="server" ID="valDefectNotes" ControlToValidate="txtDefectNotes" ValidationGroup="save"
                                    ValidationExpression="^[\s\S]{0,200}$" ErrorMessage="Please enter a maximum of 200 characters" ForeColor="Red"
                                    Display="Dynamic">*</asp:RegularExpressionValidator>
                                <asp:Panel runat="server" Style='width: 250px; min-height: 50px; padding: 5px; background-color: #DDDDDD;'
                                    ID="pnlDefectNotes">
                                    <asp:Label runat="server" ID="lblDefectNotes" Visible="false" Style="padding: 3px;" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Remedial Action Taken?:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsRemedialActionTaken" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" />
                                    <asp:ListItem Value="False" Text="No" Selected="True" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsRemedialActionTaken" Visible="false" />
                                <br />
                                <asp:TextBox runat="server" ID="txtRemedialActionNotes" TextMode="MultiLine" Rows="3" />
                                 <asp:RegularExpressionValidator runat="server" ID="valActionNotes" ControlToValidate="txtRemedialActionNotes" ValidationGroup="save"
                                    ValidationExpression="^[\s\S]{0,200}$" ErrorMessage="Please enter a maximum of 200 characters" ForeColor="Red"
                                    Display="Dynamic">*</asp:RegularExpressionValidator>
                                <asp:Panel runat="server" Style='width: 250px; min-height: 50px; padding: 5px; background-color: #DDDDDD;'
                                    ID="pnlRemedialActionNotes">
                                    <asp:Label runat="server" ID="lblRemedialActionNotes" Visible="false" Style="padding: 3px;" /></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Warning/Advice note issued?:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsWarningNoteIssued" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" />
                                    <asp:ListItem Value="False" Text="No" Selected="True" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsWarningNoteIssued" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--Appliance:<span class="required">*</span>--%>
                               <asp:Label ID="lblType" runat="server" Text="Appliance:"></asp:Label> <span class="required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlAppliances" DataValueField="PROPERTYAPPLIANCEID"
                                    DataTextField="APPLIANCE" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvddlAppliances" InitialValue="-1"
                                    ErrorMessage="</br>Please select an Appliance" Display="Dynamic" ValidationGroup="save"
                                    ControlToValidate="ddlAppliances" ForeColor="Red" />
                                <asp:Label runat="server" ID="lblAppliances" Visible="false" />
                                <asp:Label runat="server" ID="lblBoiler" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Serial Number:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSerialNumber" MaxLength="10" />
                                <asp:Label runat="server" ID="lblSerialNumber" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                GC Number:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtGCNumber0" MaxLength="2" Width="20px" />
                                <asp:TextBox runat="server" ID="txtGCNumber1" MaxLength="3" Width="30px" />
                                <asp:TextBox runat="server" ID="txtGCNumber2" MaxLength="2" Width="20px" />
                                <asp:Label runat="server" ID="lblGcNumber" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Warning Tag/sticker fixed?:
                            </td>
                            <td>
                                <asp:CheckBox runat="server" Checked="false" ID="chkBoxWarningTagFixed" />
                                <asp:Label runat="server" ID="lblIsWarningTagFixed" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Disconnected/Capped:<span class="required">*</span>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsDisconnected" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" />
                                    <asp:ListItem Value="False" Text="No" />
                                    <asp:ListItem Value="" Text="N/A" Selected="True" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsDisconnected" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Required:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsPartsRequired" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" />
                                    <asp:ListItem Value="False" Text="No" />
                                    <asp:ListItem Value="" Text="N/A" Selected="True" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsPartsRequired" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Ordered?:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsPartsOrdered" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" />
                                    <asp:ListItem Value="False" Text="No" />
                                    <asp:ListItem Value="" Text="N/A" Selected="True" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsPartsOrdered" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Ordered By:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPartsOrderedBy" runat="server" DataValueField="EMPLOYEEID"
                                    DataTextField="EmployeeName" />
                                <asp:Label runat="server" ID="lblPartsOrderedBy" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Due?:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPartsDueDate" />
                                <asp:Image ImageUrl="~/Images/calendar.png" runat="server" ID="imgPartsDueDate" AlternateText="Open Calendar" />
                                <ajaxToolkit:CalendarExtender ID="calPartsDueDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                    TargetControlID="txtPartsDueDate" PopupButtonID="imgPartsDueDate" PopupPosition="BottomRight"
                                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
                                <asp:Label runat="server" ID="lblPartsDueDate" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Description:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPartsDescription" Height="80px"  TextMode="MultiLine" Rows="3" />
                                <asp:Label runat="server" ID="lblPartsDescription" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Parts Location:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPartsLocation" />
                                <asp:Label runat="server" ID="lblPartsLocation" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2 person job?
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtnGrpIsTwoPersonJob" runat="server" RepeatLayout="Flow"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Text="Yes" Selected="True" />
                                    <asp:ListItem Value="False" Text="No" />
                                </asp:RadioButtonList>
                                <asp:Label runat="server" ID="lblIsTwoPersonJob" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Reason for 2nd person:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtReasonFor2ndPerson" Height="80px" TextMode="MultiLine" Rows="3" />
                                <asp:Label runat="server" ID="lblReasonFor2ndPerson" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estimated Duration:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlEstimatedDuration" />
                                <asp:Label runat="server" ID="lblEstimatedDuration" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Priority:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlPriority" DataValueField="PriorityID" DataTextField="PriorityName" />
                                <asp:Label runat="server" ID="lblPriority" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trade:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlTrade" DataValueField="TradeId" DataTextField="Trade" />
                                <asp:Label runat="server" ID="lblTrade" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" Text="Photograph:" ID="lblPhotograph" Visible="false" />
                            </td>
                            <td>
                                <%--<ajaxToolkit:AjaxFileUpload AllowedFileTypes="PNG, JPG, GIF" runat="server" ID="uploadPhotos" />--%>
                                <asp:Button ID="btnUploadPhotos" runat="server" Text="Upload" Width="60px" OnClientClick="openPhotoUploadWindow('defect'); return false;" />
                                <asp:Label runat="server" ID="lblFileName"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblPhotoGraphNotes" Text="Photograph Notes:" Visible="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhotoNotes" runat="server" TextMode="MultiLine" Height="82px"
                                    Width="220px"></asp:TextBox>
                                <asp:Label runat="server" ID="lblPhotoNotes" Visible="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDefectButtons">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <div>
                                <div style="display: inline;">
                                    <asp:Button Text="Cancel" runat="server" ID="btnCancel" />
                                </div>
                                <div style="display: inline; margin-left: 20px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave"></asp:Button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Panel>
