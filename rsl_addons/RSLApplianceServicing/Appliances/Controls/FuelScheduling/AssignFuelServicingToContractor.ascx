﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AssignFuelServicingToContractor.ascx.vb"
    Inherits="Appliances.AssignFuelServicingToContractor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--Add appropriate style sheet in web form or master page.--%>
<style type="text/css" media="all">
    .assignToContractor
    {
        background-color: White;
    }
    .modalBack
    {
        background-color: Black;
        filter: alpha(opacity=30);
        opacity: 0.3;
        border: 1px solid black;
    }
    .assignToContractor .popupHeader
    {
        font-weight: bold;
        padding-left: 20px;
        padding-top: 10px;
    }
    .assignToContractor .worksRequiredGrid
    {
        padding-left: 20px;
        padding-right: 20px;
        max-height: 200px;
        max-width: 800px;
        overflow: auto;
    }
    .assignToContractor .input_row
    {
        margin-bottom: 10px;
        padding-left: 20px;
    }
    .assignToContractor .input_label
    {
        width: 150px;
        float: left;
    }
    .assignToContractor .input
    {
        width: auto;
    }
    .assignToContractor .input *
    {
        width: 250px;
    }
    .assignToContractor .input_small
    {
        width: auto;
    }
    .assignToContractor .input_small *
    {
        width: 140px;
    }
    .assignToContractor .input_small .addButton
    {
        width: 60px;
        margin-left: 40px;
    }
    
    .assignToContractor .bottomButtonsContainer
    {
        width: 100%;
        height: 100%;
    }
    
    .assignToContractor .bottomButtonsContainer .buttonFloatLeft
    {
        float: left;
        text-align: left;
        padding-left: 20px;
    }
    
    .assignToContractor .bottomButtonsContainer .buttonFloatRight
    {
        float: right;
        text-align: right;
        padding-right: 20px;
    }
    
    .assignToContractor .worksRequired
    {
        word-wrap: break-word;
    }
    .assignToContractor .WorksRequiredCount
    {
        display: none;
    }
    
    .clear
    {
        clear: both;
    }
</style>
<div class="assignToContractor" style="overflow: auto; max-height: 625px">
    <div class="popupHeader">
        <strong>Assign to Contractor</strong>
        <br />
        <br />
        <asp:Panel ID="pnlMessage" runat="server" Width="100%" Visible="false">
            <asp:Label ID="lblMessage" runat="server">
            </asp:Label>
        </asp:Panel>
    </div>
    <hr />
    <br />
    <div class="input_row">
        <div class="input_label">
            Cost Centre:</div>
        <div class="input">
            <asp:Label ID="lblCostCentre" runat="server" Text="Property Asset Management"></asp:Label>
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Budget Head:</div>
        <div class="input">
            <asp:Label ID="lblBudgetHead" runat="server" Text="Servicing"></asp:Label>
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Expenditure:</div>
        <div class="input">
            <asp:Label ID="lblExpenditure" runat="server" Text="Gas Servicing"></asp:Label>
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Contractor:</div>
        <div class="input">
            <asp:DropDownList runat="server" ID="ddlContractor" AutoPostBack="True" Style="padding: 3px;">
                <asp:ListItem Selected="True" Text="Please select" Value="-1" />
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="rfvContractor" InitialValue="-1" ErrorMessage="Please select a Contractor"
                Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContractor"
                ForeColor="Red" />
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Contact:</div>
        <div class="input">
            <asp:DropDownList runat="server" ID="ddlContact" AutoPostBack="True" Style="padding: 3px;">
                <asp:ListItem Selected="True" Text="-- Please select contact --" Value="-1" />
            </asp:DropDownList>
            <%--<asp:RequiredFieldValidator runat="server" ID="rfvContact" InitialValue="-1" ErrorMessage="Please select a Contact"
                Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContact"
                ForeColor="Red" ToolTip="If no contact is found, you may add a new contact (name/email) for this contractor in supplier module " />--%>
        </div>
    </div>
    <hr />
    <br />
    <div style="clear: both">
    </div>
    <div id="divBoilerInfo" runat="server">
        <div class="input_row">
            <span>
                <asp:Label runat="server" ID="lblAppointmentTypeInfo" Style="font-weight: bold;"></asp:Label>
            </span>
        </div>
        <div style="overflow: auto; max-height: 100px; margin-bottom: 20px" visible="false">
            <asp:Repeater ID="boilersInfoRpt" runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label Style="padding-left: 20px" runat="server" ID="Label1" Text='<%# Eval("BoilerName") + ":" %>' />
                        </td>
                        <td>
                            <asp:Label Style="padding-left: 15px;" runat="server" ID="Label2" Text='<%# Eval("HeatingFuel") + " : " %>' />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="Label3" Text='<%# Eval("BoilerType") + " : " %>' />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="Label4" Text='<%# Eval("Manufacturer") %>' />
                        </td>
                        <td>
                            <asp:Label Style="padding-left: 15px;" runat="server" ID="Label5" Text='<%# "    " + Eval("EXPIRYDATE") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Works Required:<asp:Label Text="" runat="server" ID="lblWorksRequiredCount" CssClass="WorksRequiredCount" /></div>
        <div class="input">
            <asp:TextBox runat="server" ID="txtWorksRequired" Height="85" MaxLength="4000" TextMode="MultiLine"
                Rows="5" />
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                TargetControlID="txtWorksRequired" WatermarkText="Enter details here" WatermarkCssClass="searchTextDefault">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator runat="server" ID="rfvWorksRequired" ErrorMessage="Please enter works required."
                Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="txtWorksRequired"
                ForeColor="Red" Style="vertical-align: top;" />
            <asp:CustomValidator ID="cvWorksRequired" runat="server" ControlToValidate="txtWorksRequired"
                ErrorMessage="Works Required must not exceed 4000 characters." Display="Dynamic"
                ForeColor="Red" ValidationGroup="addWorkRequired" EnableClientScript="False" />
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Estimate Ref:</div>
        <div class="input_small">
            <asp:TextBox runat="server" ID="txtEstimateRef" MaxLength="200" />
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                TargetControlID="txtEstimateRef" WatermarkText="Optional" WatermarkCssClass="searchTextDefault">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <asp:RegularExpressionValidator runat="server" ID="rxvEstimateRef" ErrorMessage="Works Required must not exceed 200 characters"
                Display="Dynamic" ForeColor="Red" ControlToValidate="txtEstimateRef" ValidationGroup="assignToContractor"
                ValidationExpression="^[\s\S]{0,200}$" Style="vertical-align: top;" />
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Net Cost (£):</div>
        <div class="input_small">
            <asp:TextBox runat="server" ID="txtNetCost" AutoPostBack="True" CausesValidation="true"
                ValidationGroup="addWorkRequired" MaxLength="11"></asp:TextBox>
            <asp:RequiredFieldValidator ErrorMessage="Please enter net cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                ControlToValidate="txtNetCost" ID="rfvNetCost" ValidationGroup="addWorkRequired"
                runat="server" ForeColor="Red" Display="Dynamic" />
            <asp:RegularExpressionValidator ID="rxvNetCost" runat="server" ErrorMessage="Please enter net Cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                Display="Dynamic" ValidationGroup="addWorkRequired" ForeColor="Red" ControlToValidate="txtNetCost"
                ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Vat:</div>
        <div class="input_small">
            <asp:DropDownList runat="server" ID="ddlVat" AutoPostBack="True" Style="padding: 3px;"
                CausesValidation="True" />
            <asp:RequiredFieldValidator runat="server" ID="rfvddlVat" InitialValue="-1" ErrorMessage="Please select a Vat(Vat Rate)"
                Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="ddlVat"
                ForeColor="Red" />
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            Vat (£):</div>
        <div class="input_small">
            <asp:TextBox runat="server" ID="txtVat" ReadOnly="true"></asp:TextBox>
        </div>
    </div>
    <div class="input_row">
        <div class="input_label">
            <strong>Total (£):</strong></div>
        <div class="input_small">
            <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true"></asp:TextBox>
            <asp:Button Text="Add" runat="server" ID="btnAdd" CssClass="addButton" ValidationGroup="addWorkRequired" />
        </div>
    </div>
    <br />
    <hr />
    <br />
    <div class="worksRequiredGrid">
        <asp:GridView runat="server" ID="grdWorksDeatil" ShowFooter="True" GridLines="Horizontal"
            AutoGenerateColumns="False" PageSize="100" ShowHeaderWhenEmpty="True" Width="100%"
            Visible="true" BorderStyle="None">
            <HeaderStyle Font-Bold="false" />
            <FooterStyle BorderColor="Black" Font-Bold="true" HorizontalAlign="Center" Wrap="False" />
            <RowStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None" />
            <Columns>
                <asp:BoundField HeaderText="Works Required:" FooterText="Total:" DataField="WorksRequired"
                    HeaderStyle-HorizontalAlign="Left" FooterStyle-HorizontalAlign="Right" ItemStyle-CssClass="worksRequired" />
                <asp:BoundField HeaderText="Net:" FooterText="0.00" DataFormatString="{0:0.####}"
                    DataField="NetCost" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="100px" />
                <asp:BoundField HeaderText="Vat:" FooterText="0.00" DataFormatString="{0:0.####}"
                    DataField="Vat" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="100px" />
                <asp:BoundField HeaderText="Gross:" FooterText="0.00" DataFormatString="{0:0.####}"
                    DataField="Gross" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Width="100px" />
            </Columns>
        </asp:GridView>
    </div>
    <br />
    <hr />
    <br />
    <br />
    <div class="bottomButtonsContainer">
        <div class="buttonFloatLeft">
            <asp:Button ID="btnCloseAssignFuelServicingToContractor" runat="server" Text="Close"
                CausesValidation="false" />
        </div>
        <div class="buttonFloatRight">
            <asp:Button ID="btnAssignToContractor" runat="server" Style="text-align: right" Text="Assign to selected contractor"
                ValidationGroup="assignToContractor" />
        </div>
        <div class="clear" />
    </div>
    <div class="clear" />
    <br />
    <br />
</div>
<asp:UpdatePanel ID="updpnlAssignToContractor" runat="server">
    <ContentTemplate>
        <div style="border: 2 solid black">
            <asp:Panel ID="pnlRiskAndVulnerabilit" runat="server" BackColor="White" Width="350px"
                Style="font-weight: normal; font-size: 13px; padding: 10px;">
                <b>Warning </b>
                <br />
                <br />
                <hr />
                <p>
                    No contact details exist for this Supplier, and therefore they have not received
                    an email notification that the Purchase Order has been approved. Please contact
                    the supplier directly with the Purchase Order details. To update the Supplier records
                    with contact details please contact a member of the Finance Team.
                </p>
                <asp:Button ID="btnOk" runat="server" Text="OK" />
            </asp:Panel>
            <asp:Button ID="btnHidde" runat="server" UseSubmitBehavior="false" Text="" Style="display: none;" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopupRis" runat="server" TargetControlID="btnHidde"
                PopupControlID="pnlRiskAndVulnerabilit" CancelControlID="btnOk" Enabled="true"
                DropShadow="true" BackgroundCssClass="modalBack">
            </ajaxToolkit:ModalPopupExtender>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
