﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PopUpAlternateFuelConfirmation.ascx.vb"
    Inherits="Appliances.PopUpAlternateFuelConfirmation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--Add appropriate style sheet in web form or master page.--%>

<div style="text-align: center; background-color: white; padding: 10px">
    <span style="font-weight: bold;">Alternative Fuel!</span>
    <br />
    <span>The selected property also has alternative fuels installed.</span>
    <br />
    <span>Would you like to assign the appointment to a Gas</span>
    <br />
    <span>Engineer who is qualified to carry out a service on</span>
    <br />
    <span>alternative fuels?</span>
    <br />
    <asp:Button style="margin-right: 5px; margin-top: 6px" ID="btnYesAlternativeFuel" runat="server" Text="YES" CausesValidation="false" />
    <asp:Button style="margin-left: 5px" ID="btnNoAlternativeFuel" runat="server" Text="NO" CausesValidation="false" />
</div>
