﻿Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports AS_Utilities
Imports AS_BusinessLogic
Imports AjaxControlToolkit

'Public Enum AssignToContractorType As Integer
'    Planned = 1
'    Miscellaneous = 2
'    Conditional = 3
'    Adaptation = 4
'End Enum
Public Class AssignFuelServicingToContractor : Inherits UserControlBase




#Region "Properties"

#Region "Is Saved Property to use in consumer page to determine either work is assign to contractor or not"

    Public Property IsSaved() As Boolean
        Get
            Dim isSavedret As Boolean = False
            If Not IsNothing(ViewState(ViewStateConstants.IsSaved)) Then
                isSavedret = CType(ViewState(ViewStateConstants.IsSaved), Boolean)
            End If
            Return isSavedret
        End Get
        Set(ByVal value As Boolean)
            ViewState(ViewStateConstants.IsSaved) = value
        End Set
    End Property

#End Region

    Dim maxCost As Integer = 0 '200000
#End Region


#Region "Show Assign to contractor popup"
    Public Sub ShowAssignFuelServicingPopup()
        Dim mdlPopupAssignToContractor As AjaxControlToolkit.ModalPopupExtender = CType(Parent.FindControl("mdlpopupAssignToContractor"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopupAssignToContractor.Show()
    End Sub
#End Region

#Region "Events Handling"

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        populateLocalControls()
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

#End Region

#End Region


#Region "Button Events"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            Page.Validate("addWorkRequired")
            If Page.IsValid Then

                If Decimal.Parse(txtTotal.Text) <= maxCost Then
                    SetExpenditureValues(SessionManager.getHeadId, SessionManager.getAppServicingUserId())
                    AddPurchaseIteminWorkRequiredDT()
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = "Total Cost should be less then " + maxCost.ToString()

                End If


            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            ShowAssignFuelServicingPopup()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnCloseAssignFuelServicingToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseAssignFuelServicingToContractor.Click
        Dim mdlPopupAssignToContractor As AjaxControlToolkit.ModalPopupExtender = CType(Parent.FindControl("mdlpopupAssignToContractor"), AjaxControlToolkit.ModalPopupExtender)
        SessionManager.RemoveServicingTypeForOilAlternative()
        mdlPopupAssignToContractor.Hide()
    End Sub

    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssignToContractor.Click
        Try
            assignWorkToContractor()
            If (ddlContact.Text = "-1") Then
                mdlPopupRis.Show()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            ShowAssignFuelServicingPopup()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Assign Work To Contractor"

    Private Sub assignWorkToContractor()

        Dim workRequiredDT As DataTable = getWorkRequiredDTViewState()
        Dim jsn As String = String.Empty

        If workRequiredDT.Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.workRequiredCount, True)
        Else
            If Page.IsValid Then
                Dim assignToContractorBo As AssignToContractorBO = SessionManager.getAssignToContractorBO
                assignToContractorBo.WorksRequired = workRequiredDT
                assignToContractorBo.ContractorId = ddlContractor.SelectedValue
                assignToContractorBo.ContactId = ddlContact.SelectedValue
                assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(workRequiredDT)
                assignToContractorBo.UserId = SessionManager.getAppServicingUserId()
                assignToContractorBo.PropertyId = SessionManager.getPropertyIdForFuelScheduling()
                assignToContractorBo.EstimateRef = txtEstimateRef.Text
                assignToContractorBo.AppointmentType = SessionManager.getBoilerTypeInfoForSchemeBlock().AppointmentType

                ' assignToContractorBo.FuelServicingWorksRequired = txtWorksRequired.Text
                'Dim journalId As Integer = ConfirmSelectedRepair(jsn)
                assignToContractorBo.JournalId = SessionManager.getJournalIdForFuelScheduling()
                'assignToContractorBo.jsn = jsn

                Dim objAssignFuelServicingToContractorBL As New AssignFuelServicingToContractorBL
                Dim isSavedStatus As Boolean
                If (assignToContractorBo.AppointmentType = "Scheme") Then
                    assignToContractorBo.SchemeId = SessionManager.getPropertyIdForFuelScheduling()
                    assignToContractorBo.BlockId = Nothing
                    assignToContractorBo.PropertyId = String.Empty
                ElseIf (assignToContractorBo.AppointmentType = "Block") Then
                    assignToContractorBo.BlockId = SessionManager.getPropertyIdForFuelScheduling()
                    assignToContractorBo.SchemeId = Nothing
                    assignToContractorBo.PropertyId = String.Empty
                ElseIf (assignToContractorBo.AppointmentType = "Property") Then
                    assignToContractorBo.PropertyId = SessionManager.getPropertyIdForFuelScheduling()
                    assignToContractorBo.SchemeId = Nothing
                    assignToContractorBo.BlockId = Nothing
                End If
                isSavedStatus = objAssignFuelServicingToContractorBL.assignToContractor(assignToContractorBo)

                If isSavedStatus Then
                    'Set Property Is Saved to True.
                    Me.IsSaved = True
                    'Disable btnAssignToContractor to avoid assigning the same work more than once. 
                    btnAssignToContractor.Enabled = False
                    btnAdd.Enabled = False

                    Dim message As String = UserMessageConstants.AssignedToContractor
                    Try
                        If (assignToContractorBo.POStatus = ApplicationConstants.WorkOrderedPoStatusId And assignToContractorBo.PoCurrentStatus <> ApplicationConstants.QueuedPoStatusId) Then

                            Dim detailsForEmailDS As New DataSet()
                            Dim objAssignToContractor As New AssignToContractorBL
                            objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
                            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                                     AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                                sendEmailtoContractor(assignToContractorBo)
                            Else
                                message = message + UserMessageConstants.EmailToContractor
                            End If

                        ElseIf (assignToContractorBo.PoCurrentStatus = ApplicationConstants.QueuedPoStatusId) Then 'AndAlso assignToContractorBo.OrderId > 0) Then
                            sendEmailtoBudgetHolder(assignToContractorBo)


                        End If
                    Catch ex As Exception
                        message += "<br />but " + ex.Message
                    End Try
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, False)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AssignedToContractorFailed, True)
                End If

            End If
        End If

    End Sub

#End Region


#Region "Drop down Events"

#Region "ddl Vat Selected Index Change"

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlVat.SelectedIndexChanged
        Try
            Page.Validate("addWorkRequired")
            CalculateVatAndTotal()
            ddlVat.Focus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            ShowAssignFuelServicingPopup()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Contractor Selected Index Change"

    Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContractor.SelectedIndexChanged
        Try
            GetContactDropDownValuesbyContractorId(ddlContractor.SelectedValue)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            ShowAssignFuelServicingPopup()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

#End Region
#Region "ddl Contact Selected Index Change"

    Private Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.SelectedIndexChanged

        showModalPopup()

        If (Not ddlContact.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue)) Then
            Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
            Dim detailsForEmailDS As New DataSet()
            objAssignToContractorBL.getContactEmailDetail(ddlContact.SelectedValue, detailsForEmailDS)

            If IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt)) _
                   Or detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt).Rows.Count = 0 Then
                mdlPopupRis.Show()
            End If
        End If


    End Sub

#End Region
#Region "ModalPopup"
    Public Sub showModalPopup()
        DirectCast(Me.Parent.Parent.FindControl("mdlpopupAssignToContractor"), ModalPopupExtender).Show()
    End Sub
    Public Sub hideModalPopup()
        DirectCast(Me.Parent.Parent.FindControl("mdlpopupAssignToContractor"), ModalPopupExtender).Hide()
    End Sub
#End Region

#End Region

#Region "TextBox Events"

    Protected Sub txtNetCost_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtNetCost.TextChanged
        Try
            'Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL
            'maxCost = objAssignFuelServicingToContractorBL.GetMaxCost(SessionManager.getExpenditureId)
            If Decimal.Parse(txtNetCost.Text) <= maxCost Then
                CalculateVatAndTotal()
                txtNetCost.Focus()
                btnAdd.Enabled = True
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = "Total Cost should be less then " + maxCost.ToString()
                btnAdd.Enabled = False
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            ShowAssignFuelServicingPopup()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        ShowAssignFuelServicingPopup()
    End Sub

#End Region
#End Region

#Region "Bind Drop Down List"

    Private Sub BindDropDownList(ByRef ddlToBind As DropDownList, ByRef dropDownItemsList As List(Of DropDownBO), ByVal insertDefault As Boolean)

        ddlToBind.Items.Clear()
        ddlToBind.DataSource = dropDownItemsList
        ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlToBind.DataBind()

        If insertDefault Then
            ddlToBind.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Calculate Vat and Total"

    Private Sub CalculateVatAndTotal()
        Dim vatRateId As Integer = ddlVat.SelectedValue
        Dim VatRate As Decimal = 0.0

        If vatRateId >= 0 Then
            VatRate = getVatRateByVatId(vatRateId)
        End If

        Dim netCost As Decimal

        If Decimal.TryParse(txtNetCost.Text.Trim, 0.0) Then
            netCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
            Dim vat As Decimal = netCost * VatRate / 100
            Dim total As Decimal = netCost + vat

            txtNetCost.Text = String.Format("{0:0.00}", netCost)
            txtVat.Text = String.Format("{0:0.00}", vat)
            txtTotal.Text = String.Format("{0:0.00}", total)
        Else
            txtVat.Text = String.Empty
            txtTotal.Text = String.Empty
        End If
        If Decimal.Parse(txtTotal.Text) > maxCost Then
            btnAdd.Enabled = False
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = "Total Cost should be less then " + maxCost.ToString()
        Else
            btnAdd.Enabled = True
            uiMessageHelper.IsError = False
            uiMessageHelper.Message = String.Empty
        End If
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Id as Value Field and Vat Name as text field."

    Sub GetVatDropDownValues()
        Dim vatBoList As New List(Of ContractorVatBO)

        Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL

        objAssignFuelServicingToContractorBL.getVatDropDownValues(vatBoList)

        ddlVat.Items.Clear()
        ddlVat.DataSource = vatBoList
        ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlVat.DataBind()

        ddlVat.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))

        ddlVat.SelectedValue = ApplicationConstants.DropDownDefaultValue
        ' Save Vat Bo List in session, to get vat rate while adding a work required.
        SessionManager.setVatBOList(ddlVat.DataSource)

    End Sub

#End Region

#Region "Get Vat Rate by VatId"

    Private Function getVatRateByVatId(ByVal vatRateId As Integer) As Decimal
        Dim vatRate As Decimal = 0.0

        Dim vatBoList As List(Of ContractorVatBO) = SessionManager.getVatBOList

        Dim vatBo As ContractorVatBO = vatBoList.Find(Function(i) i.ID = vatRateId)

        If Not IsNothing(vatBo) Then
            vatRate = vatBo.VatRate
        End If

        Return vatRate
    End Function

#End Region

#Region "Get Contractor Having Defect Contract"

    Private Sub GetFuelServicingContractors()
        Dim dropDownList As New List(Of DropDownBO)

        Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL
        objAssignFuelServicingToContractorBL.GetFeulServicingContractors(dropDownList)
        BindDropDownList(ddlContractor, dropDownList, True)

    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByVal ContractorId As Integer)
        ddlContact.Items.Clear()
        Dim dropDownBoList As New List(Of DropDownBO)

        Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL
        objAssignFuelServicingToContractorBL.GetContactDropDownValuesbyContractorId(dropDownBoList, ContractorId)

        If dropDownBoList.Count = 0 Then
            ddlContact.Items.Insert(0, New ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue))
        Else
            BindDropDownList(ddlContact, dropDownBoList, True)
        End If
    End Sub

#End Region

#Region "Populate User Control By Passing Values from Page"

    Public Sub PopulateControl()

        ResetControls()
        Dim objAssignToContractorBO As AssignToContractorBO = SessionManager.getAssignToContractorBO()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim boilerInfoDataSet As DataSet = New DataSet()
        Dim appointmentTypeInfo As BoilerTypeInfo = SessionManager.getBoilerTypeInfoForSchemeBlock()
        Dim journalId As Integer = SessionManager.getJournalIdForFuelScheduling()

        GetFuelServicingContractors()
        'Vat dropdown
        GetVatDropDownValues()

        txtWorksRequired.Text = HttpUtility.UrlDecode(objAssignToContractorBO.DefectWorksRequired)
        txtNetCost.Text = objAssignToContractorBO.DefaultNetCost
        ddlVat.SelectedValue = objAssignToContractorBO.DefaultVatId
        txtVat.Text = objAssignToContractorBO.DefaultVat
        txtTotal.Text = objAssignToContractorBO.DefaultGross

        Dim servicingType As ServicingType = SessionManager.GetServicingTypeForOilAlternativ()

        If Not IsNothing(servicingType) Then
            If (servicingType.IsAlternative) Then
                objSchedulingBL.GetHeatingInfoForAlternative(boilerInfoDataSet, journalId)

            ElseIf (servicingType.IsOil) Then
                objSchedulingBL.GetHeatingInfoForOil(boilerInfoDataSet, journalId)
            End If

            Dim count As Integer = boilerInfoDataSet.Tables(0).Rows.Count
            If (Not IsNothing(boilerInfoDataSet) OrElse count > 0) Then

                lblAppointmentTypeInfo.Text = "The Property has " + count.ToString()
                lblAppointmentTypeInfo.Text = lblAppointmentTypeInfo.Text + If(count > 1, " Boilers:", " Boiler:")
                divBoilerInfo.Visible = True
                boilersInfoRpt.Visible = True
                boilersInfoRpt.DataSource = boilerInfoDataSet
                boilersInfoRpt.DataBind()
            End If

        End If

        If (appointmentTypeInfo.AppointmentType.Contains("Block") Or appointmentTypeInfo.AppointmentType.Contains("Scheme")) Then
            objSchedulingBL.getSchemeBlockBoilersInfo(boilerInfoDataSet, journalId, appointmentTypeInfo.AppointmentType)
            Dim count As Integer = boilerInfoDataSet.Tables(0).Rows.Count
            If (Not IsNothing(boilerInfoDataSet) OrElse count > 0) Then
                lblAppointmentTypeInfo.Text = "The " + appointmentTypeInfo.AppointmentType + " has " + appointmentTypeInfo.BoilerCount
                lblAppointmentTypeInfo.Text = lblAppointmentTypeInfo.Text + If(Convert.ToInt32(appointmentTypeInfo.BoilerCount) > 1, " Boilers:", " Boiler:")
                divBoilerInfo.Visible = True
                boilersInfoRpt.Visible = True
                boilersInfoRpt.DataSource = boilerInfoDataSet
                boilersInfoRpt.DataBind()
            End If

        End If


        bindWorkRequiredGrid()


    End Sub

#End Region

#Region "Reset Works Required"

    Private Sub resetWorksRequired()

        If ddlVat.Items.Count > 0 Then
            ddlVat.SelectedIndex = 0
        End If

        txtWorksRequired.Text = String.Empty
        txtNetCost.Text = String.Empty
        txtVat.Text = String.Empty
        txtTotal.Text = String.Empty
    End Sub

#End Region

#Region "Reset Controls (on first populate)"

    Private Sub ResetControls()
        'Set is Saved property to false on populate.
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        ddlContact.Items.Clear()
        ddlContact.Items.Insert(0, New ListItem(ApplicationConstants.DropdownContactDefaultText, ApplicationConstants.DropDownDefaultValue))
        IsSaved = False
        btnAssignToContractor.Enabled = True
        btnAdd.Enabled = True
        resetWorksRequired()
        removeWorkRequiredDTViewState()
        bindWorkRequiredGrid()

        divBoilerInfo.Visible = False
        boilersInfoRpt.DataSource = Nothing
        boilersInfoRpt.DataBind()

    End Sub

#End Region

#Region "Bind Work Required Grid"

    Private Sub bindWorkRequiredGrid()
        Dim dtWorksRequired As DataTable = getWorkRequiredDTViewState()
        Dim addExtraSpaces As Boolean = False

        If dtWorksRequired.Rows.Count = 0 Then
            For Each col As DataColumn In dtWorksRequired.Columns
                col.AllowDBNull = True
            Next
            Dim newRow As DataRow = dtWorksRequired.NewRow
            dtWorksRequired.Rows.Add(newRow)
            addExtraSpaces = True
        End If

        grdWorksDeatil.DataSource = dtWorksRequired
        grdWorksDeatil.DataBind()
        If addExtraSpaces Then
            grdWorksDeatil.Rows(0).Cells(0).Text = "<br /><br /><br />"
        End If
    End Sub

#End Region

#Region "View State Functions"

#Region "Work Required Data Table"

    Private Function getWorkRequiredDTViewState() As DataTable
        Dim workRequiredDt As DataTable
        workRequiredDt = TryCast(ViewState(ViewStateConstants.WorkRequiredDT), DataTable)

        If IsNothing(workRequiredDt) Then
            workRequiredDt = New DataTable

            '1- Add Work Required Column with data type string
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.WorksRequiredCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.WorksRequiredCol, GetType(String))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = String.Empty
                'Set Maximum length for Work Required Column.
                insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired
            End If

            '2- Add Net Cost Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '3- Add Vat type Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            '4- Add Vat Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '5- Add Gross/Total Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.GrossCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.GrossCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '6- Add PI Status Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = False
            End If

            '7- Add Expenditure ID Col with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            'If Not workRequiredDt.Columns.Contains(ApplicationConstants.CostCenterIdCol) Then
            '    Dim insertedColumn As DataColumn
            '    insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.CostCenterIdCol, GetType(Integer))
            '    insertedColumn.AllowDBNull = False
            'End If

            'If Not workRequiredDt.Columns.Contains(ApplicationConstants.BudgetHeadIdCol) Then
            '    Dim insertedColumn As DataColumn
            '    insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.BudgetHeadIdCol, GetType(Integer))
            '    insertedColumn.AllowDBNull = False
            'End If

        End If

        Return workRequiredDt
    End Function

#End Region


    Private Sub removeWorkRequiredDTViewState()
        ViewState.Remove(ViewStateConstants.WorkRequiredDT)
    End Sub

    Private Sub setWorkRequiredDTViewState(ByVal dt As DataTable)
        ViewState(ViewStateConstants.WorkRequiredDT) = dt
    End Sub

#End Region

#Region "Add Purchase Items in Work Required Data Table"

    Private Sub AddPurchaseIteminWorkRequiredDT()

        Dim objWorkRequiredBo As New WorkRequiredBo
        objWorkRequiredBo.WorkRequired = txtWorksRequired.Text.Trim
        objWorkRequiredBo.NetCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
        objWorkRequiredBo.VatIdDDLValue = ddlVat.SelectedValue
        objWorkRequiredBo.Vat = If(IIf(String.IsNullOrEmpty(txtVat.Text.Trim), Nothing, txtVat.Text.Trim), 0.0)
        objWorkRequiredBo.Total = If(IIf(String.IsNullOrEmpty(txtTotal.Text.Trim), Nothing, txtTotal.Text.Trim), 0.0)
        objWorkRequiredBo.ExpenditureId = SessionManager.getExpenditureId


        Dim workRequiredDt = getWorkRequiredDTViewState()
        Dim workRequiredRow = workRequiredDt.NewRow
        workRequiredRow(ApplicationConstants.ExpenditureIdCol) = objWorkRequiredBo.ExpenditureId
        ' workRequiredRow(ApplicationConstants.CostCenterIdCol) = objWorkRequiredBo.CostCenterId
        '  workRequiredRow(ApplicationConstants.BudgetHeadIdCol) = objWorkRequiredBo.BudgetHeadId
        workRequiredRow(ApplicationConstants.WorksRequiredCol) = objWorkRequiredBo.WorkRequired
        workRequiredRow(ApplicationConstants.NetCostCol) = objWorkRequiredBo.NetCost
        workRequiredRow(ApplicationConstants.VatTypeCol) = objWorkRequiredBo.VatIdDDLValue
        workRequiredRow(ApplicationConstants.VatCol) = objWorkRequiredBo.Vat
        workRequiredRow(ApplicationConstants.GrossCol) = objWorkRequiredBo.Total
        workRequiredRow(ApplicationConstants.PIStatusCol) = getPIStatus(objWorkRequiredBo.ExpenditureId, objWorkRequiredBo.Total)
        workRequiredDt.Rows.Add(workRequiredRow)
        resetWorksRequired()
        setWorkRequiredDTViewState(workRequiredDt)
        bindWorkRequiredGrid()

    End Sub

#End Region

#Region "Get Pi Status (Pending Status)"

    Private Function getPIStatus(ByVal expenditureId As Integer, ByVal gross As Decimal) As Object
        Dim pIStatus As Integer = 3 ' 3 = "Work Ordered" in table F_POSTATUS
        Dim expenditureBoList As List(Of ExpenditureBO) = SessionManager.getExpenditureBOList

        'Check if an item is costing 0 (zero) or less then it should not go to queued list.
        If gross > 0 Then
            Dim ExpenditureBo As ExpenditureBO = expenditureBoList.Find(Function(i) i.ID = expenditureId)

            If Not IsNothing(ExpenditureBo) Then
                If (gross > ExpenditureBo.Limit OrElse gross > ExpenditureBo.Remaining) Then
                    pIStatus = 0 ' 0 = "Queued" in table F_POSTATUS
                End If
            End If
        End If

        Return pIStatus
    End Function

#End Region


#Region "Grid View Events"

#Region "Grid Works Detail Row data bound"

    Protected Sub grdWorksDeatil_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdWorksDeatil.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim workRequiredDt As DataTable = getWorkRequiredDTViewState()

                If workRequiredDt.Rows.Count > 0 Then
                    e.Row.Cells(1).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.NetCostCol & ")", "").ToString
                    e.Row.Cells(2).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.VatCol & ")", "").ToString
                    e.Row.Cells(3).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.GrossCol & ")", "").ToString
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Set PO Status"

    Private Function getPOStatusFromWorkItemsDt(ByVal workRequiredDT As DataTable) As Integer
        Dim pOStatus As Integer = ApplicationConstants.WorkOrderedPoStatusId   ' 3 = "Work Ordered" in table F_POSTATUS

        If (workRequiredDT.Select(ApplicationConstants.PIStatusCol & " = 0").Count > 0) Then
            pOStatus = ApplicationConstants.QueuedPoStatusId  ' 0 = "Queued" in table F_POSTATUS
        End If

        Return pOStatus
    End Function

#End Region


#End Region


#Region "Set Expenditure Values by Budget Head Id"
    Private Sub SetExpenditureValues(ByVal BudgetHeadId As Integer, ByRef EmployeeId As Integer)

        Dim expenditureBOList As New List(Of ExpenditureBO)
        Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL
        objAssignFuelServicingToContractorBL.SetExpenditureValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)
        SessionManager.setExpenditureBOList(expenditureBOList)
    End Sub

#End Region


#Region "Populate Body and Send Email to Contractor"

    Private Sub sendEmailtoContractor(ByRef assignToContractorBo As AssignToContractorBO)
        Dim detailsForEmailDS As New DataSet()
        Dim objAssignFuelServicingToContractorBL As New AssignFuelServicingToContractorBL
        objAssignFuelServicingToContractorBL.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)

        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then

                If String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString()) _
                    OrElse Not AS_Utilities.Validation.isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString()) Then

                    Throw New Exception("Unable to send email to contractor, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AssignWorkToFuelServicingContractor.html"))
                    body.Append(reader.ReadToEnd())

                    'filling email format

                    body.Replace("{ContractorContactName}", detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName"))
                    body.Replace("{JobRef}", assignToContractorBo.JournalId.ToString)
                    body.Replace("{OrderId}", assignToContractorBo.OrderId)
                    body.Replace("{OrderedBy}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("OrderedBy"))
                    body.Replace("{DDial}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("DDial"))
                    body.Replace("{Email}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("Email"))
                    If String.IsNullOrEmpty(assignToContractorBo.EstimateRef) Then
                        body.Replace("{EstimateRef}", "N/A")
                    Else
                        body.Replace("{EstimateRef}", assignToContractorBo.EstimateRef)
                    End If
                    body.Replace("{NetCost}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString)
                    body.Replace("{VAT}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString)
                    body.Replace("{TOTAL}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString)
                    body.Replace("{WorksRequired}", getWorksRequired(assignToContractorBo.WorksRequired))

                    If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)) _
                        AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then

                        If (detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt) IsNot Nothing AndAlso
                            detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt).Rows.Count > 0) Then

                            body.Replace("{Asbestos}", "{Asbestos}")

                            body.Replace("{Asbestos}", GetPropertyAsbestos(detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt)))
                        Else
                            body.Replace("{Asbestos}", "None")
                        End If
                        With detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)
                            body.Replace("{Address}", .Item("FullStreetAddress"))
                            body.Replace("{TownCity}", .Item("TOWNCITY"))
                            body.Replace("{County}", .Item("COUNTY"))
                            body.Replace("{PostCode}", .Item("POSTCODE"))
                        End With
                    Else
                        body.Replace("{Address}", "N/A")
                        body.Replace("{TownCity}", "")
                        body.Replace("{County}", "")
                        body.Replace("{PostCode}", "")
                    End If

                    If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)) _
                        AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt).Rows.Count > 0 Then
                        With detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)(0)
                            If String.IsNullOrEmpty(.Item("TEL")) Then
                                body.Replace("{Telephone}", "N/A")
                            Else
                                body.Replace("{Telephone}", .Item("TEL"))
                            End If
                            If String.IsNullOrEmpty(.Item("FullName")) Then
                                body.Replace("{TenantName}", "N/A")
                            Else
                                body.Replace("{TenantName}", .Item("FullName"))
                            End If
                        End With
                    Else
                        body.Replace("{TenantName}", "N/A")
                        body.Replace("{Telephone}", "N/A")
                    End If

                    body.Replace("{RiskDetail}", getRiskDetails(detailsForEmailDS))
                    body.Replace("{VulnerabilityDetail}", getVulnerabilityDetails(detailsForEmailDS))

                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                    '==========================================='

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString, detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName").ToString))
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub

#End Region

#Region "Populate Body and Send Email to Budget holder"

    Private Sub sendEmailtoBudgetHolder(ByRef assignToContractorBo As AssignToContractorBO)
        Dim detailsForEmailDS As New DataSet()
        Dim budgetHolderDs As New DataSet()
        Dim objAssignFuelServicingToContractorBL As New AssignFuelServicingToContractorBL

        objAssignFuelServicingToContractorBL.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
        objAssignFuelServicingToContractorBL.getBudgetHolderByOrderId(budgetHolderDs, assignToContractorBo.OrderId)
        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                If (Not IsNothing(budgetHolderDs.Tables(0))) AndAlso budgetHolderDs.Tables(0).Rows.Count() > 0 Then
                    If String.IsNullOrEmpty(budgetHolderDs.Tables(0)(0)("Email").ToString()) _
                           OrElse Not AS_Utilities.Validation.isEmail(budgetHolderDs.Tables(0)(0)("Email").ToString()) Then

                        Throw New Exception("Unable to send email to budget holder, invalid email address.")
                    Else

                        Dim body As New StringBuilder
                        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AssignWorkToFuelServicingContractor.html"))
                        body.Append(reader.ReadToEnd())

                        'filling email format 

                        body.Replace("{ContractorContactName}", budgetHolderDs.Tables(0)(0)("OperativeName"))
                        body.Replace("{OrderId}", assignToContractorBo.OrderId)
                        body.Replace("{JobRef}", assignToContractorBo.JournalId.ToString())
                        body.Replace("{OrderedBy}", assignToContractorBo.OrderId)
                        body.Replace("{DDial}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("DDial"))
                        body.Replace("{Email}", detailsForEmailDS.Tables(ApplicationConstants.OrderedByDetailsDt)(0)("Email"))
                        If String.IsNullOrEmpty(assignToContractorBo.EstimateRef) Then
                            body.Replace("{EstimateRef}", "N/A")
                        Else
                            body.Replace("{EstimateRef}", assignToContractorBo.EstimateRef)
                        End If

                        body.Replace("{NetCost}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString)
                        body.Replace("{VAT}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString)
                        body.Replace("{TOTAL}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString)
                        body.Replace("{WorksRequired}", getWorksRequired(assignToContractorBo.WorksRequired))
                        If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)) _
                            AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then

                            If (detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt) IsNot Nothing AndAlso
                                detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt).Rows.Count > 0) Then
                                body.Replace("{Asbestos}", "{Asbestos}")
                                body.Replace("{Asbestos}", GetPropertyAsbestos(detailsForEmailDS.Tables(ApplicationConstants.AsbestosDt)))

                            Else
                                body.Replace("{Asbestos}", "None")
                            End If
                            With detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)
                                body.Replace("{Address}", .Item("FullStreetAddress"))
                                body.Replace("{TownCity}", .Item("TOWNCITY"))
                                body.Replace("{County}", .Item("COUNTY"))
                                body.Replace("{PostCode}", .Item("POSTCODE"))
                            End With
                        Else

                            body.Replace("{Address}", "N/A")
                            body.Replace("{TownCity}", "")
                            body.Replace("{County}", "")
                            body.Replace("{PostCode}", "")
                        End If

                        If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)) _
                            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt).Rows.Count > 0 Then
                            With detailsForEmailDS.Tables(ApplicationConstants.TenantDetailsDt)(0)

                                If String.IsNullOrEmpty(.Item("TEL")) Then
                                    body.Replace("{Telephone}", "N/A")
                                Else
                                    body.Replace("{Telephone}", .Item("TEL"))
                                End If
                                If String.IsNullOrEmpty(.Item("FullName")) Then
                                    body.Replace("{TenantName}", "N/A")
                                Else
                                    body.Replace("{TenantName}", .Item("FullName"))
                                End If


                            End With
                        Else
                            body.Replace("{TenantName}", "N/A")
                            body.Replace("{Telephone}", "N/A")
                        End If

                        body.Replace("{RiskDetail}", getRiskDetails(detailsForEmailDS))
                        body.Replace("{VulnerabilityDetail}", getVulnerabilityDetails(detailsForEmailDS))
                        Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/PropertyModule/broadland.png"))
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                        body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                        Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")
                        Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                        Dim mailMessage As New Mail.MailMessage
                        mailMessage.Subject = ApplicationConstants.EmailSubject
                        mailMessage.To.Add(New MailAddress(budgetHolderDs.Tables(0)(0)("Email").ToString, budgetHolderDs.Tables(0)(0)("OperativeName").ToString))
                        mailMessage.Body = body.ToString
                        mailMessage.AlternateViews.Add(alternatevw)
                        mailMessage.IsBodyHtml = True

                        EmailHelper.sendEmail(mailMessage)

                    End If
                Else
                    Throw New Exception("Unable to send email, Budget holder not found.")
                End If


            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub
#End Region


#Region "Get Works Required - Concatenated in form of ordered list to add in email"

    Private Function getWorksRequired(ByVal dataTable As DataTable) As String
        Dim worksRequired As New StringBuilder
        If dataTable.Rows.Count = 1 Then
            worksRequired.Append(dataTable(0)(ApplicationConstants.WorksRequiredCol).ToString)
        Else
            worksRequired.Append("<ol>")
            For Each row As DataRow In dataTable.Rows
                worksRequired.Append("<li>" + row(ApplicationConstants.WorksRequiredCol).ToString + "</li>")
            Next
            worksRequired.Append("</ol>")
        End If
        Return worksRequired.ToString
    End Function

#End Region

#Region "Get Property Asbestos Grid/Table to use in Email"

    Private Function GetPropertyAsbestos(ByRef asbestosDt As DataTable) As String
        Dim renderedGrid As String = String.Empty
        'Create a grid view for asbestos
        Dim asbestosGrid As New GridView

        'Set Asbestos Grid Properties
        asbestosGrid.ID = "grdPropertyAsbestos"
        asbestosGrid.ShowHeader = False
        asbestosGrid.HorizontalAlign = HorizontalAlign.Left
        asbestosGrid.GridLines = GridLines.None
        asbestosGrid.AutoGenerateColumns = False
        asbestosGrid.ShowHeaderWhenEmpty = False
        asbestosGrid.EmptyDataText = String.Empty
        asbestosGrid.CellSpacing = 5
        asbestosGrid.CellPadding = 3

        'Set Asbestos Grid Columns
        Dim asbriskLevelDescription As New BoundField
        asbriskLevelDescription.DataField = "ASBRISKLEVELDESCRIPTION"
        asbestosGrid.Columns.Add(asbriskLevelDescription)

        Dim riskDescription As New BoundField
        riskDescription.DataField = "RISKDESCRIPTION"
        asbestosGrid.Columns.Add(riskDescription)

        'Bind Data table to asbestosGrid
        asbestosGrid.DataSource = asbestosDt
        asbestosGrid.DataBind()

        'Render Asbestos Grid and get string.
        Using sw As New StringWriter()
            Using hw As New HtmlTextWriter(sw)
                asbestosGrid.RenderControl(hw)
                Dim sr As New StringReader(sw.ToString())
                renderedGrid = sw.ToString()
            End Using
        End Using

        Return renderedGrid

    End Function

#End Region

#Region "Get Risk Details - As concatenated string split on separate row."

    Private Function getRiskDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim RiskDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows.Count > 0 Then

            RiskDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows
                RiskDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next
        End If

        Return RiskDetails.ToString
    End Function

#End Region

#Region "Get Vulnerability Details - As concatenated string split on separate row."

    Private Function getVulnerabilityDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim vulnerabilityDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows.Count > 0 Then

            vulnerabilityDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows
                vulnerabilityDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next

        End If

        Return vulnerabilityDetails.ToString
    End Function

#End Region

#Region "Populate Session with updated values from Database"

    Private Sub populateSession()
        Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
        Dim resultDataSet As New DataSet
        Dim HeadId, ExpenditureId As Integer
        Dim ExpenditureType As String = GeneralHelper.GetExpenditureType_GasServicing()

        objAssignToContractorBL.GetHeadAndExpenditureId(resultDataSet, ExpenditureType)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            HeadId = resultDataSet.Tables(0)(0)("HEADID")
            ExpenditureId = resultDataSet.Tables(0)(0)("EXPENDITUREID")
            SessionManager.setExpenditureId(ExpenditureId)
            SessionManager.setHeadId(HeadId)

        End If
    End Sub

#End Region

#Region "Populate Local variables with required values"

    Private Sub populateLocalControls()
        populateSession()
        Dim objAssignFuelServicingToContractorBL As AssignFuelServicingToContractorBL = New AssignFuelServicingToContractorBL
        maxCost = objAssignFuelServicingToContractorBL.GetMaxCost(SessionManager.getExpenditureId)

    End Sub

#End Region


End Class