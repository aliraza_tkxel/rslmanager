﻿
Imports AS_Utilities
Imports AS_BusinessLogic

Public Class PopUpAlternateFuelConfirmation : Inherits UserControlBase


#Region "Properties"

#Region "Is Saved Property to use in consumer page to determine either work is assign to contractor or not"

    Public Property IsSaved() As Boolean
        Get
            Dim isSavedret As Boolean = False
            If Not IsNothing(ViewState(ViewStateConstants.IsSaved)) Then
                isSavedret = CType(ViewState(ViewStateConstants.IsSaved), Boolean)
            End If
            Return isSavedret
        End Get
        Set(ByVal value As Boolean)
            ViewState(ViewStateConstants.IsSaved) = value
        End Set
    End Property

#End Region

    Dim maxCost As Integer = 0 '200000
#End Region


#Region "Show Assign to contractor popup"
    Public Sub ShowAssignFuelServicingPopup()
        Dim mdlPopupAssignToContractor As AjaxControlToolkit.ModalPopupExtender = CType(Parent.FindControl("mdlpopupAssignToContractor"), AjaxControlToolkit.ModalPopupExtender)
        mdlPopupAssignToContractor.Show()
    End Sub
#End Region

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

#End Region

#End Region

#Region "Control Events"

    Public Event BtnYesAlternativeFuelClicked As EventHandler
    Public Event BtnNoAlternativeFuelClicked As EventHandler
    Protected Sub btnYesAlternativeFuel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnYesAlternativeFuel.Click
        RaiseEvent BtnYesAlternativeFuelClicked(Me, New EventArgs)
    End Sub

    Protected Sub btnNoAlternativeFuel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNoAlternativeFuel.Click
        'Dim mdlpopupAlternativeFuel As AjaxControlToolkit.ModalPopupExtender = CType(Parent.FindControl("mdlpopupAlternativeFuel"), AjaxControlToolkit.ModalPopupExtender)
        'mdlpopupAlternativeFuel.Hide()
        RaiseEvent BtnNoAlternativeFuelClicked(Me, New EventArgs)
    End Sub


#End Region
End Class