﻿Imports AS_Utilities
Imports AS_BusinessLogic

Public Class PropertyAddress
    Inherits UserControlBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#Region "set Property Labels"
    ''' <summary>
    ''' 'This function 'll set the property labels
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub setPropertyLabels()
        Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()

        selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()
        If (Not IsNothing(selPropSchedulingBo)) Then
            lblCustomerName.Text = selPropSchedulingBo.CustomerName
            lblStreetAddress.Text = "," + selPropSchedulingBo.PropertyAddress
            lblPostCode.Text = selPropSchedulingBo.PostCode
            lblTelNo.Text = "," + "Tel: " + selPropSchedulingBo.Telephone
            divBoilerInfo.Visible = False
            boilersInfoRpt.DataSource = Nothing
            boilersInfoRpt.DataBind()
            If Not selPropSchedulingBo.AppointmentType Is Nothing Then
                If (selPropSchedulingBo.AppointmentType.ToString().Contains("Block") Or selPropSchedulingBo.AppointmentType.ToString().Contains("Scheme")) Then
                    objSchedulingBL.getSchemeBlockBoilersInfo(resultDataSet, selPropSchedulingBo.JournalId, selPropSchedulingBo.AppointmentType)
                    Dim count As Integer = resultDataSet.Tables(0).Rows.Count
                    If (Not IsNothing(resultDataSet) OrElse count > 0) Then
                        lblAppointmentTypeInfo.Text = "The " + selPropSchedulingBo.AppointmentType + " has " + resultDataSet.Tables(0).Rows.Count.ToString()
                        lblAppointmentTypeInfo.Text = lblAppointmentTypeInfo.Text + If(resultDataSet.Tables(0).Rows.Count > 1, " Boilers:", " Boiler:")
                        divBoilerInfo.Visible = True
                        boilersInfoRpt.Visible = True
                        boilersInfoRpt.DataSource = resultDataSet
                        boilersInfoRpt.DataBind()
                    End If
                End If
            End If
            updPanelPropertyAddress.Update()
        End If
    End Sub
#End Region

End Class