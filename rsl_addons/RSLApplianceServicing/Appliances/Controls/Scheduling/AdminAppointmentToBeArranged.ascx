﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AdminAppointmentToBeArranged.ascx.vb" Inherits="Appliances.AdminAppointmentToBeArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Import Namespace="AS_Utilities" %>
<script type="text/javascript">
    var w;
    function uploadError(sender, args) {
        alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
    }

    function uploadComplete() {
        __doPostBack();
    }

    function openEditLetterWindowForAdd() {

        var letterId = document.getElementById('<%= ddlAddLetter.ClientID %>').value;
        if (letterId == -1) {

        }
        else {
            w = window.open('../../Views/Resources/EditLetter.aspx?pg=0&id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
    }

    function fireCheckboxEventForAddApp() {
        document.getElementById('<%= ckBoxAddRefreshDataSet.ClientID %>').checked = true;
        setTimeout('__doPostBack(\'<%= ckBoxAddRefreshDataSet.UniqueID %>\',\'\')', 0);
    }   
        
</script>
<style type="text/css">
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 190px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
</style>
<div style="height: 400px; overflow: auto;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <cc1:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AutoGenerateColumns="False"
        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
        CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True"
        AllowPaging="True" EmptyDataText="No Record Found" PageSize="30">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("TENANCYID") %>'
                        ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnAptbaTenantInfo_Click" BorderStyle="None"
                        Visible='<%# GeneralHelper.isPropertyStatusLet(Eval("PropertyStatus").ToString()) %>'
                        BorderWidth="0px" />
                    <%--<asp:PopupControlExtender ID="popUpAppointmentToBeArrangedPhone" runat="server" 
                                     Enabled="True" ExtenderControlID="" 
                                     PopupControlID="pnlAppointmentToBeArrangedTenantInfo" Position="Bottom" 
                                     TargetControlID="imgbtnAptbaTenantInfo" >
                                         TargetControlID="imgbtnAptbaTenantInfo" CacheDynamicResults="true" EnableViewState="true"  ViewStateMode="Enabled" DropShadow="true">
                                 </asp:PopupControlExtender>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address" >
                <ItemTemplate>
                    <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                <ItemTemplate>
                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">--%>
            <asp:TemplateField HeaderText="Expiry Date">
                <ItemTemplate>
                    <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                    <%--<asp:Label ID="lblExpirydate" runat="server" Text='<%# Convert.ToDateTime(Eval("EXPIRYDATE")).ToShortDateString() %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Days" SortExpression="Days">
                <ItemTemplate>
                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("DAYS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fuel Type" SortExpression="FUEL">
                <ItemTemplate>
                    <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'>></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgAppointmentToBeArrangedArrow" runat="server" OnClick="imgbtnCalendarDetail_Click"
                        BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("PROPERTYID").ToString()+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString() %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" BorderColor="Black"
            BorderStyle="Solid" BorderWidth="1px" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
        <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="10" />
        <PagerStyle BackColor="White" />
    </cc1:PagingGridView>
    <asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnAptbaClose" runat="server" Style="position: absolute;
                        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
            <%--<tr>
                <td class="amendColCss">
                    Tenant:
                </td>
                <td class="amendColCss">
                    <asp:Label ID="lblAptbaFisrtname" runat="server" Text='<%# Bind("FIRSTNAME") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="amendColCss">
                    Mobile:
                </td>
                <td class="amendColCss">
                    <asp:Label ID="lblAptbaMobile" runat="server" Text='<%# Bind("MOBILE") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="amendColCss">
                    Telephone:
                </td>
                <td class="amendColCss">
                    <asp:Label ID="lblAptbaTel" runat="server" Text='<%# Bind("TEL") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="amendColCss">
                    Email:
                </td>
                <td class="amendColCss">
                    <asp:Label ID="lblAptbaEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                </td>
            </tr>--%>
        </table>
    </asp:Panel>
</div>
<%--<asp:PopupControlExtender ID="popUpAppointmentToBeArrangedPhone" runat="server" 
                                     Enabled="True" ExtenderControlID="" 
                                     PopupControlID="pnlAppointmentToBeArrangedTenantInfo" Position="Bottom" 
                                     TargetControlID="imgbtnAptbaTenantInfo" >
                                         TargetControlID="imgbtnAptbaTenantInfo" CacheDynamicResults="true" EnableViewState="true"  ViewStateMode="Enabled" DropShadow="true">
                                 </asp:PopupControlExtender>--%>
<asp:Panel runat="server" ID="pnlAddCalendar" Style="height: 500px; width: 1000px;
    background-color: White; border: 1px solid gray; padding: 10px; top: -10px; top: 0px;
    body: nth-of-type(1){left:-200px; } body: nth-of-type(1){top:-200px; } position: fixed !important;">
    <div style="background-color: Black; color: White; border: solid 1px gray; height: 20px;
        padding: 8px; text-align: left;">
        <span style="font-size: 13px; font-weight: bold;">Schedule Appointment</span>
        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("FIRSTNAME") %>' Style="padding-left: 240px;"></asp:Label>
        &nbsp;<span style="padding-left: 150px;">Certificate Expires in
            <asp:Label ID="lblExpiryDays" runat="server" Text='<%# Bind("FIRSTNAME") %>' Style="vertical-align: baseline;"></asp:Label>
            &nbsp;days</span>
    </div>
    <asp:ImageButton ID="imgbtnAddAppointmentCancelCalendar" runat="server" ImageAlign="Right"
        ImageUrl="~/Images/cross2.png" Style="position: absolute; top: -10px; right: -10px"
        BorderStyle="None" />
    <div class="group" style="margin: 10px 0; border: 1px solid gray; text-align: left;
        width: 977px;">
        <span style="float: left; padding-left: 12px; padding-top: 4px;">
            <asp:Label ID="lblMonth" runat="server" Font-Bold="True" Style="padding: 8px;"></asp:Label>
        </span>
        <asp:Panel ID="msgAddPanel" runat="server" Visible="false">
            <span style="float: left; padding-left: 100px; padding-top: 4px;">
                <asp:Label ID="lblAddMsg" runat="server">
                </asp:Label>
            </span>
        </asp:Panel>
        <span id="calToolbar"><span style="padding-right: 10px; float: left;">Proximity of Engineer
            within: <span>
                <asp:DropDownList ID="ddlAddDistance" runat="server" Width="30%" AutoPostBack="True">
                    <asp:ListItem Value="1">1 mile</asp:ListItem>
                    <asp:ListItem Value="2">2 miles</asp:ListItem>
                    <asp:ListItem Value="3">3 miles</asp:ListItem>
                    <asp:ListItem Value="4">4 miles</asp:ListItem>
                    <asp:ListItem Value="5">5 miles</asp:ListItem>
                    <asp:ListItem Value="6">6 miles</asp:ListItem>
                    <asp:ListItem Value="7">7 miles</asp:ListItem>
                    <asp:ListItem Value="8">8 miles</asp:ListItem>
                    <asp:ListItem Value="9">9 miles</asp:ListItem>
                    <asp:ListItem Value="10">10 miles</asp:ListItem>
                </asp:DropDownList>
            </span></span><span style="margin-top: 1px; float: left; margin-right: 10px;">
                <div style="border: solid 1px gray; padding: 0px !important; float: left;">
                    <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?pg=pr"><img src="../../images/arrow_left.png" alt="arrow_left" style="border-width:0px; padding-top: 2px; 
    vertical-align: text-top;float:left" /></asp:HyperLink>
                    <span style="border-left: solid 1px gray; border-right: solid 1px gray; vertical-align: top;
                        float: left">
                        <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?pg=td"
                            ForeColor="Black">Today</asp:HyperLink>
                    </span>
                    <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?pg=nt"><img src="../../images/arrow_right.png" alt="arrow_right" style="border-width:0px; padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;float:left"/></asp:HyperLink>
                </div>
            </span>
            <asp:Label ID="lbl_startDate" runat="server" ForeColor="White"></asp:Label>
            <asp:Label ID="lbl_endDate" runat="server" ForeColor="White"></asp:Label>
        </span>
    </div>
    <div style="height: 400px; width: 980px; overflow: auto;">
        <table class="fixedHeaderCalendar" id="appointmentCalendar" cellpadding="0" cellspacing="0"
            border="1" style="border: solid 1px gray;">
            <%--<caption>--%>
            <thead>
                <% If addAppointmentWeeklyDT.Columns.Count <= 1 Then%>
                <tr>
                    <td valign="top">
                        <span class="error">Gas engineer not found.</span>
                    </td>
                </tr>
                <%Else%>
                <tr>
                    <%  Dim empArray() As String
                        
                        For dtCount As Integer = 0 To addAppointmentWeeklyDT.Columns.Count - 1%>
                    <th style="font-weight: bold; border: solid 1px gray; text-align: center;">
                        
                            <%  If addAppointmentWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                    empArray = addAppointmentWeeklyDT.Columns(dtCount).ToString().Split(":")
                                    'bgColor = "<asp:ImageButton ID='imgAddAppointment' runat='server' OnClientClick='populateTextBox('" + addAppointmentWeeklyDT.Columns(dtCount).ToString() + "')'  OnClick='imgAddAppointment_Click' BorderStyle='None' ImageUrl='../../images/btn_add.png'  />"
                            %>
                            <span style="margin-bottom: 10px;">
                            <%= empArray(1).Trim() %></span>&nbsp;
                        <%--<a href="#" onclick="hello()">
      
                        <img border="0" src="../../images/btn_add.png" />
                        </a>--%>
                        <a href="../../Views/Scheduling/AdminScheduling.aspx?param=<%=addAppointmentWeeklyDT.Columns(dtCount).ToString() %>">
                            <img src="../../images/btn_add.png" style="border-width:0" alt="Add" />
                        </a>
                        <%-- <asp:ImageButton ID="imgAddAppointment" runat="server" OnClientClick="populateTextBox('<%=addAppointmentWeeklyDT.Columns(dtCount).ToString() %> ')"  OnClick="imgAddAppointment_Click" BorderStyle="None" ImageUrl='../../images/btn_add.png'  /> ss--%>
                    
                    
                    <%  End If %>
                    </th>
                    <% Next %>
                    </tr>
                    <% End If %>
                
            </thead>
            <tbody>
                <%  For rdCount = 0 To addAppointmentWeeklyDT.Rows.Count - 1
                        Dim bgColor As String
                        If (rdCount Mod 2 = 0) Then
                            bgColor = "#E8E9EA"
                        Else
                            bgColor = "#FFFFFF"
                        End If
                %>
                <tr style='<%="background-color:"+ bgColor+";"%>'>
                    <%  For rdItemCount = 0 To addAppointmentWeeklyDT.Columns.Count - 1
                            If Not String.IsNullOrEmpty(addAppointmentWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then
                                If rdItemCount = 0 Then
                             
                    %>
                    <td style="font-weight: bold; border: solid 1px gray;">
                        <div class="subHeading" style="text-align: center;">
                            <%= addAppointmentWeeklyDT.Rows(rdCount).Item(rdItemCount)%>                            
                        </div>
                    </td>
                    <%Else%>
                    <td style="border: solid 1px gray;">
                        <div style="height: 225px; overflow: auto; margin: 5px;">
                            <%= addAppointmentWeeklyDT.Rows(rdCount).Item(rdItemCount)%>
                        </div>
                    </td>
                    <%End If%>
                    <%Else%>
                    <td>
                        &nbsp;
                    </td>
                    <%End If
                      Next%>
                </tr>
                <%  Next%>
                <%--</caption>--%>
            </tbody>
        </table>
    </div>
</asp:Panel>
<asp:Label ID="lblAddisplayCalendar" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlAddPopUpCalendar" runat="server" TargetControlID="lblAddisplayCalendar"
    PopupControlID="pnlAddCalendar" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
    <Animations>    
        <OnShown>
            <ScriptAction Script="createFixedHeader();" />
        </OnShown>
    </Animations>
</asp:ModalPopupExtender>
<asp:Label ID="lbldisplayAppointment" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
    TargetControlID="lbldisplayAppointment" PopupControlID="pnlAppointmentToBeArrangedTenantInfo"
    Enabled="true" DropShadow="true" CancelControlID="imgBtnAptbaClose">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlAddAppointment" CssClass="left" runat="server" BackColor="White"
    Width="470px" Height="500px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
    position: fixed !important; -bracket-: hack(; left: 430px !important; top: 100px;
    );">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Add Appointment</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <%--<div style="width: 100%; height: 490px; overflow: hidden;">--%>
        <div style="overflow-y: scroll; height: 470px; padding-top: 5px !important;">
            <asp:HiddenField ID="hdnExpiryDate" runat="server" ClientIDMode="Static" />
            <table>
                <tr>
                    <td colspan="3" valign="top">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="3">
                        <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                            <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Appointment Type:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAddInspectionType" runat="server" Width="200px">
                            <asp:ListItem>Gas Inspection</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Location:
                    </td>
                    <td align="right" valign="top" class="style2">
                        <span style="float: left; margin-top: -4px; vertical-align: top;">
                            <asp:ImageButton ID="lnkBtnAppDetails" runat="server" ImageUrl="../../images/phone.jpg"
                                BorderStyle="None" />
                        </span>
                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Date:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                            PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:TextBox ID="txtDate" runat="server" Text="" ClientIDMode="Static" onfocus="resetExpiryAlertCounter()"></asp:TextBox>
                        &nbsp;<img alt="Open Calendar" src="../../Images/calendar.png" id="imgCalDate" onclick="resetExpiryAlertCounter()" />
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Starts:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAppStartTime" runat="server" Width="100px" AutoPostBack="True"
                            onClick="checkMExpiryDate(document.getElementById('hdnExpiryDate'),document.getElementById('txtDate'))">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Ends:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAppEndTime" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Operative:
                    </td>
                    <td align="left" valign="top">
                        <asp:Label ID="lblOperative" runat="server"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Status:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAddStatus" runat="server" AutoPostBack="True" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Action:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAddAction" runat="server" AutoPostBack="True" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Letter:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAddLetter" runat="server" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" class="style5">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAddUploadWindow()"
                            BackColor="White" />
                        &nbsp;
                        <asp:Button ID="btnAddViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindowForAdd()"
                            BackColor="White" />
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Docs:
                    </td>
                    <td align="left" valign="top">
                        <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                            <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                <ItemTemplate>
                                    <table class="LetterDocInnerTable">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                    Visible="<%# False %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                    Visible="<%# False %>"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                    BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                                    OnClick="imgBtnRemoveLetterDoc_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <SelectedItemStyle BackColor="Gray" />
                            </asp:DataList>
                        </asp:Panel>
                    </td>
                </tr>
                <%--<tr>
                <td align="left" valign="top" class="style5">
                    Upload:
                </td>
                <td align="left" valign="top">
                    <asp:Label ID="imgUploadProgress" runat="server" Style="display: none"> <img alt="" src="../../Images/uploading.gif" /></asp:Label>
                    <asp:AsyncFileUpload ID="asyncDocUpload" runat="server" ErrorBackColor="Red" Height="26px"
                        OnClientUploadComplete="uploadComplete" OnClientUploadError="uploadError" OnUploadedComplete="asyncDocUpload_UploadedComplete"
                        ThrobberID="imgUploadProgress" UploaderStyle="Traditional" Width="179px" />
                </td>
                <td align="left" valign="top">
                    &nbsp;
                </td>
            </tr>--%>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Notes:
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine" Width="246px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    </td>
                    <td align="right" valign="top" style="text-align: right;">
                        <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
                        <asp:Button ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
                    </td>
                    <td align="left" valign="top" style="text-align: left;">
                        <div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddAppointment" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlAddAppointment"
    DropShadow="true" CancelControlID="btnCancelAction" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<asp:Panel ID="pnlAddApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblAptbaTenantInfoAddPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />                    
            </td>
        </tr>
        <%--<tr>
            <td>
                Tenant:
            </td>
            <td class="amendColCss">
                <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FIRSTNAME") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Mobile:
            </td>
            <td class="amendColCss">
                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Bind("MOBILE") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Telephone:
            </td>
            <td class="amendColCss">
                <asp:Label ID="lblTelNo" runat="server" Text='<%# Bind("TEL") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Email:
            </td>
            <td class="amendColCss">
                <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
            </td>
        </tr>--%>
    </table>
</asp:Panel>

<asp:ModalPopupExtender ID="mdlPopUpAddAppointmentContactDetails" runat="server"
    DynamicServicePath="" Enabled="True" TargetControlID="lblAddAppointmentContactDetails"
    PopupControlID="pnlAddApointmentContactDetails" DropShadow="true" CancelControlID=""
    BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>

<%--Start - Tenant Info Box Add Appoint Calander--%>
<asp:Panel ID="pnlAddCalanderTennatInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblAddCalanderTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgbtnCloseTenantInfoAddCalander" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />                    
            </td>
        </tr>        
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopupAddCalanderTenantInfo" runat="server"
    DynamicServicePath="" Enabled="True" TargetControlID="lblAddCalendarTenantInfo"
    PopupControlID="pnlAddCalanderTennatInfo" DropShadow="true" CancelControlID=""
    BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--End - Tenant Info Box Add Appoint Calander--%>
<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right:10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" style="padding-right:10px; font-weight:bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right:10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled." style="padding-right:10px;"></asp:Label>
    </div>
   
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    >
</asp:ModalPopupExtender>
<asp:Label ID="lblAddAppointmentContactDetails" runat="server"></asp:Label>
<asp:Label ID="lblAddCalendarTenantInfo" runat="server" />
<asp:CheckBox ID="ckBoxAddRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
