﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentNotes.ascx.vb"
    Inherits="Appliances.AppointmentNotes" %>
<style type='text/css'>
    .apt-notes-popup
    {
        width: 446px;
        background-color: white;
        margin: 0 0 0 0px;
        border: 1px solid black;
        padding-bottom:10px;
    }
    
    .popup-sub-container
    {
        padding: 5px 10px 5px 10px;
    }
    
    .right-align
    {
        float: right;
    }
    
    .left-align
    {
        float: left;
    }
    
    td
    {
        padding: 4px;
    }
    
    input[type="submit"]
    {
        background-color: White;
        border-radius: 5px;
        padding: 4px;
    }
</style>
<asp:UpdatePanel ID="updPanelAppointmentNotes" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="apt-notes-popup">
            <div class="popup-sub-container">
                <asp:Image ID="Image1" runat="server" Width="20px" Height="20px" ImageUrl="~/Images/editCustomerNotes.png" />
                &nbsp;
                <asp:Label ID="Label1" runat="server" Text="Appointment Notes" Font-Bold="true"></asp:Label>
            </div>
            <hr />
            <div class="popup-sub-container">
                <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false">
                    <asp:Label ID="lblPopupMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 20%;">
                            </th>
                            <th style="width: 90%;">
                            </th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            Location:
                        </td>
                        <td>
                            <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date:*
                        </td>
                        <td>
                            <asp:Label ID="lblAppointmentDate" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Starts:
                        </td>
                        <td>
                            <asp:Label ID="lblStartTime" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ends:
                        </td>
                        <td>
                            <asp:Label ID="lblEndTime" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Operative:
                        </td>
                        <td>
                            <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <hr />
            <div class="popup-sub-container">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAppointmentNotes" TextMode="MultiLine" runat="server" 
                                Height="92px" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <hr />
            <div class="popup-sub-container" style="margin-top: 5px;">
                <asp:Button ID="btnClose" runat="server" Text="  Close  " CssClass="left-align" />
                <asp:Button ID="btnSaveChanges" runat="server" Text="Save Changes" CssClass="right-align" />
                <br />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
