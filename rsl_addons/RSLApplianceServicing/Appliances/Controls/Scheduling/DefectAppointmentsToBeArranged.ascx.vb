﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class DefectAppointmentsToBeArranged
    Inherits UserControlBase

#Region "Events Handling"
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "DefectDate", 1, 30)
#Region "User Control Events"
    'Please put - Events Like Page Load in this region

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

#End Region

#Region "Server Controls Events Handling"

#Region "Grid View Events Handling "

#Region "Grid Appointment To Be Arranged RowDataBound"

    Private Sub grdAppointmentToBeArranged_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAppointmentToBeArranged.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim row = e.Row
            Dim journalId As String = grdAppointmentToBeArranged.DataKeys(row.RowIndex).Value.ToString()

            Dim grdDefectsChild As GridView = TryCast(row.FindControl("grdDefectsChild"), GridView)
            ' -----------------------------------------------------------------------
            ' Get Defect from database and bind to defects grid(child grid)
            Dim applianceDefectsDataSet As New DataSet()
            Dim defectsBL As New DefectsBL()
            defectsBL.GetDefectsByJournalId(applianceDefectsDataSet, journalId, getDefectCategoryId)
            grdDefectsChild.DataSource = applianceDefectsDataSet
            grdDefectsChild.DataBind()

            ' -----------------------------------------------------------------------
        End If
    End Sub

#End Region

#Region "Grid Appointment To Be Arranged Paging Index Changing"

    Private Sub grdAppointmentToBeArranged_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = e.NewPageIndex + 1
        setPageSortBo(objPageSortBo)
        getAndBindAppointmentsToBeArrangedList()
    End Sub

#End Region

#Region "Grid Appointment To Be Arranged Sorting"

    Private Sub grdAppointmentToBeArranged_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = 1
        If (objPageSortBo.SortExpression = e.SortExpression) Then
            objPageSortBo.setSortDirection()
        End If
        objPageSortBo.SortExpression = e.SortExpression
        setPageSortBo(objPageSortBo)
        getAndBindAppointmentsToBeArrangedList()
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            getAndBindAppointmentsToBeArrangedList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "btn Schedule Click"

    ''' <summary>
    ''' event handler for schedule button.
    ''' get the journal id from the command argument
    ''' use linq to filter the result data set of appointment to be arranged
    ''' save the filtered row in new data table and save that data table in session
    ''' redirect to available appointments page or show error
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSchedule_Click(sender As Object, e As EventArgs)
        Try
            'get the journal id from the command argument
            Dim scheduleBtn As Button = CType(sender, Button)
            Dim journalId As Integer = CType(scheduleBtn.CommandArgument(), Integer)

            'save the filtered row in new data table and save that data table in session
            If Me.saveDataForDefectBasket(journalId) = True Then
                'Check whether risk or vulnerability exist against the customer
                If (checkRiskAndVulnerability(journalId)) Then
                    mdlPopupRisk.Show()
                Else
                    redirectToDefectBasket()
                End If

            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "btn Ok Click"

    ''' <summary>
    ''' btn Ok Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnOk_click(sender As Object, e As EventArgs)
        Try
            redirectToDefectBasket()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "User Controls Events Handling"

    Private Sub ucDefectManagement_cancelButton_Clicked(sender As Object, e As System.EventArgs) Handles ucDefectManagement.cancelButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

    Private Sub ucDefectManagement_saveButton_Clicked(sender As Object, e As System.EventArgs) Handles ucDefectManagement.saveButton_Clicked
        Try
            mdlPoPUpAddDefect.Hide()
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DefectSavedSuccessfuly, False)
            
            getAndBindAppointmentsToBeArrangedList()
            updPnlDefectsGrid.Update()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Defects Appointments To be Arranged list"

    Public Sub populateAppointmentToBeArrangedList(ByVal searchText As String, ByVal defectCategoryId As Integer)
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = 1
        setPageSortBo(objPageSortBo)
        setSearchText(searchText)
        setDefectCategoryId(defectCategoryId)

        getAndBindAppointmentsToBeArrangedList()
    End Sub

#End Region

#Region "Get and Bind Appointments To be Arranged List"

    Private Sub getAndBindAppointmentsToBeArrangedList()
        Dim resultDataSet As New DataSet()
        Dim totalCount As Integer = 0
        Dim defectsBL As New DefectsBL()
        objPageSortBo = getPageSortBo()
        totalCount = defectsBL.getAppointmentToBeArrangedList(resultDataSet, getDefectCategoryId(), getSearchText(), objPageSortBo)
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        grdAppointmentToBeArranged.VirtualItemCount = totalCount
        grdAppointmentToBeArranged.DataSource = resultDataSet
        grdAppointmentToBeArranged.DataBind()
        SessionManager.setDefectToBeArrangedList(resultDataSet.Tables(0))
        setPageSortBo(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        If totalCount = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

    End Sub

#End Region

#Region "Show Defects Detail Popup"

    Protected Sub showDefectDetail(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgbtnDefectDetail As ImageButton = CType(sender, ImageButton)
            ucDefectManagement.viewDefectDetails(imgbtnDefectDetail.CommandArgument)
            mdlPoPUpAddDefect.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            uiMessageHelper.setMessage(lblMessage, pnlMessage, ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub

#End Region

#Region "Check risk and vulnerability"

    ''' <summary>
    ''' Check risk and vulnerability
    ''' </summary>
    ''' <remarks></remarks>
    Public Function checkRiskAndVulnerability(ByVal journalId As Integer)

        Dim riskExist As Boolean = False
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataset As DataSet = New DataSet()
        objSchedulingBL.getRiskAndVulnerabilityInfo(resultDataset, journalId)

        If (resultDataset.Tables(ApplicationConstants.RiskInfo).Rows.Count > 0) Then
            grdRisks.DataSource = resultDataset.Tables(ApplicationConstants.RiskInfo).DefaultView
            grdRisks.DataBind()
            pnlRisks.Visible = True

            riskExist = True
        Else
            pnlRisks.Visible = False
        End If

        If (resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).Rows.Count > 0) Then
            grdVulnerability.DataSource = resultDataset.Tables(ApplicationConstants.VulnerabilityInfo).DefaultView
            grdVulnerability.DataBind()
            pnlVulnerability.Visible = True

            riskExist = True

        Else
            pnlVulnerability.Visible = False
        End If

        Return riskExist

    End Function

#End Region

#Region "Redirect to Defects Basket"

    Private Sub redirectToDefectBasket()
        Response.Redirect(PathConstants.DefectBasket, False)
    End Sub

#End Region

#Region "save Data For Scheduling"

    Private Function saveDataForDefectBasket(journalId As Integer) As Boolean
        Dim defectToBeArrangeList As DataTable = SessionManager.getDefectToBeArrangedList()

        Dim recordSavedForBasket As Boolean = False
        If (Not IsNothing(defectToBeArrangeList) AndAlso defectToBeArrangeList.Columns.Count > 0) Then

            Dim defectToBeArrangeListView As DataView = defectToBeArrangeList.DefaultView

            defectToBeArrangeListView.RowFilter = "JournalId = " & journalId.ToString()

            SessionManager.setSelectDefectInseptionRef(defectToBeArrangeListView.ToTable())

            recordSavedForBasket = True
        End If

        Return recordSavedForBasket

    End Function

#End Region

#End Region

#Region "View State Function"

#Region "Page Sort BO View State Function"

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "DefectDate", 1, 30)
        If Not IsNothing(ViewState(ViewStateConstants.PageSortBo)) Then
            objPageSortBo = CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
        End If
        Return objPageSortBo
    End Function
#End Region

#Region "Remove Page Sort Bo"
    Protected Sub removePageSortBo()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#End Region

#Region "Search Text View State Function"

#Region "get Search Text"
    Protected Function getSearchText() As String
        Dim searchText As String = String.Empty
        If Not IsNothing(ViewState(ViewStateConstants.AptbaSearchString)) Then
            searchText = CType(ViewState(ViewStateConstants.AptbaSearchString), String)
        End If
        Return searchText
    End Function
#End Region

#Region "remove Search Text"
    Protected Sub removeSearchText()
        ViewState.Remove(ViewStateConstants.AptaSearchString)
    End Sub
#End Region

#Region "set Search Text"
    Protected Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptbaSearchString) = searchText
        End If
    End Sub
#End Region

#End Region

#Region "Category View State Function"

#Region "get CategoryId"
    Protected Function getDefectCategoryId() As Integer
        Dim defectCategoryId As Integer = -1
        If (Not IsNothing(ViewState(ViewStateConstants.DefectCategoryId))) Then
            defectCategoryId = CType(ViewState(ViewStateConstants.DefectCategoryId), Integer)
        End If
        Return defectCategoryId
    End Function
#End Region

#Region "remove CategoryId"
    Protected Sub removeDefectCategoryId()
        ViewState.Remove(ViewStateConstants.DefectCategoryId)
    End Sub
#End Region

#Region "set CategoryId"
    Protected Sub setDefectCategoryId(ByVal defectCategoryId As Integer)
        ViewState(ViewStateConstants.DefectCategoryId) = defectCategoryId
    End Sub
#End Region

#End Region

#End Region

End Class

#Region "Test Functions"

'#Region "Show Hide Child Grid"
'Protected Sub Show_Hide_ChildGrid(sender As Object, e As EventArgs)
'    Dim imgShowHide As ImageButton = TryCast(sender, ImageButton)
'    Dim row As GridViewRow = TryCast(imgShowHide.NamingContainer, GridViewRow)
'    If imgShowHide.CommandArgument = "Show" Then
'        Dim pnlChild As Panel = row.FindControl("pnlChild")
'        pnlChild.Visible = True
'        imgShowHide.CommandArgument = "Hide"
'        imgShowHide.ImageUrl = "~/Images/minus.gif"
'        Dim journalId As String = grdAppointmentToBeArranged.DataKeys(row.RowIndex).Value.ToString()

'        Dim grdDefectsChild As GridView = TryCast(row.FindControl("grdDefectsChild"), GridView)
'        ' -----------------------------------------------------------------------
'        ' Get Defect from database and bind to defects grid(child grid)
'        Dim applianceDefectsDataSet As New DataSet()
'        Dim defectsBL As New DefectsBL()
'        defectsBL.GetDefectsByJournalId(applianceDefectsDataSet, journalId, getDefectCategoryId)
'        grdDefectsChild.DataSource = applianceDefectsDataSet
'        grdDefectsChild.DataBind()

'        ' -----------------------------------------------------------------------

'        'Dim stringWriter As New IO.StringWriter()
'        'Dim htmlWriter As New HtmlTextWriter(stringWriter)
'        'grdDefectsChild.RenderControl(htmlWriter)

'        'Dim generatedHTML As String = stringWriter.ToString()
'        'Dim justForLineBreak As Integer
'        'justForLineBreak = -1

'    Else
'        'row.FindControl("pnlChild").Visible = False
'        imgShowHide.CommandArgument = "Show"
'        imgShowHide.ImageUrl = "~/Images/plus.gif"
'    End If
'End Sub
'#End Region

#End Region