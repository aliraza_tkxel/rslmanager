﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing
Imports Appliances.FuelScheduling
Imports System.IO
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Net.Mail
Imports System.Net
Imports System.Threading



Public Class AdminAppointmentToBeArranged
    Inherits UserControlBase


#Region "Attributes"
    'Calendar Variables
    Public addAppointmentWeeklyDT As DataTable = New DataTable()
    'Appointment to be Arranged Variables
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

    'public event MissedPaymentAlertCountDelegate MissedPaymentAlertCountEvent;    
    'Public Event onAptbaIn56DaysEvent As AptbaIn56DaysDelegate

    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Address", 1, 30)
#End Region

#Region "Page Load "

    'Private Property Path As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim boolCheckBox As Boolean = False
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblAddMsg, msgAddPanel)
            uiMessageHelper.resetMessage(lblActivityPopupMessage, pnlActivityPopupMessage)
            'Display success message on appointment creation
            If (Not IsNothing(Request.QueryString("ac")) And Request.QueryString("ac") = 0 And SessionManager.getAptbaIsSuccess() = True) Then 'issue
                uiMessageHelper.setMessage(lblAddMsg, msgAddPanel, UserMessageConstants.SaveAppointmentSuccessfully, False)
                SessionManager.setAptbaIsSuccess(False)
                'ApplicationConstants.addIsSuccess = False 'issue
            End If
            'Open Popup for Add New Appointment from <AppointmentToBeArranged>
            If ((Not IsNothing(Request.QueryString("param")) And SessionManager.getAptbaAppointmentClicked() = False)) Then
                Me.mdlPopUpAddAppointment.Show()
            End If

            If (Not IsNothing(Request.QueryString("aptId")) And SessionManager.getAptbaTenancyClicked() = False AndAlso (Not IsPostBack)) Then
                Try
                    'Me.searchAppointmentToBeArranged(ckBoxDueWithIn56Days.Checked, txtSearch.Value())
                    'Me.setCalendarInfo()
                    Dim dstenantsInfo As New DataSet()
                    objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, CInt(Request.QueryString("aptId")))
                    If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                        setTenantsInfo(dstenantsInfo, tblAddCalanderTenantInfo)
                        Me.mdlPopupAddCalanderTenantInfo.Show()
                    End If
                    SessionManager.setAptbaTenancyClicked(True)
                Catch ex As ThreadAbortException
                    uiMessageHelper.Message = ex.Message
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally

                    If uiMessageHelper.IsError Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
                    End If

                End Try
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "search Appointment To Be Arranged"
    Public Sub searchAppointmentToBeArranged(ByRef check56Days As Boolean, ByRef searchText As String, ByVal fuelType As Integer, ByRef statusType As String)
        Try
            Me.setSearchText(searchText)
            Me.setCheck56Days(check56Days)
            Me.setPageSortBo(objPageSortBo)
            Me.setFuelType(fuelType)
            Me.setStatusType(statusType)
            Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "populate Appointment To Be ArrangedGrid "

    Public Sub populateAppointmentToBeArrangedGrid()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0
        totalCount = objSchedulingBL.getAppointmentToBeArrangedList(resultDataSet, Me.getCheck56Days(), getSearchText(), Me.getPageSortBo(), Me.getFuelType(), Me.getStatusType(), -1, -1)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAppointmentToBeArranged.Visible = False
        Else
            grdAppointmentToBeArranged.Visible = True
            ViewState.Add(ViewStateConstants.AptbaDataSet, resultDataSet)
            grdAppointmentToBeArranged.VirtualItemCount = totalCount
            grdAppointmentToBeArranged.DataSource = resultDataSet
            grdAppointmentToBeArranged.DataBind()

        End If

    End Sub
#End Region

#Region "img btn Aptba TenantInfo Click"

    Protected Sub imgbtnAptbaTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)
            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)
            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                setTenantsInfo(dstenantsInfo, tblTenantInfo)
                Me.mdlPopUpAppointmentToBeArrangedPhone.Show()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)
                For counter As Integer = 0 To .Rows.Count - 1
                    'Add Tenant's name to table
                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table
                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table
                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table
                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If
                Next
            End With

        End If
    End Sub

#End Region

#Region "set Tenant Info"

    Protected Sub setTenantInfo(ByVal customerId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resultDataTable As New DataTable

        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblAptbaEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblAptbaFisrtname.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblAptbaMobile.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblAptbaTel.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        Me.mdlPopUpAppointmentToBeArrangedPhone.Show()

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdAppointmentToBeArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting
        Try
            'get the sort expression
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdAppointmentToBeArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortGrid()

            '''''''''''''''''''''''''''''''''''''''''' old code '''''''''''''''''''''' 
            'ViewState(ViewStateConstants.SortDirection) = e.SortDirection
            'Me.sortExpression = e.SortExpression
            'Me.setSortDirection()
            'Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Sort Grid"
    Protected Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppointmentToBeArrangedGrid()
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppointmentToBeArranged.DataSource = dvSortedView
            grdAppointmentToBeArranged.DataBind()
        End If

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging
        Try

            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppointmentToBeArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "set Search Text"
    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptbaSearchString) = searchText
        End If


    End Sub
#End Region

#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptbaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptbaSearchString), String)
        End If
    End Function
#End Region

#Region "set Check 56 Days"
    Public Sub setCheck56Days(ByVal checkBoxVal As Boolean)
        ViewState(ViewStateConstants.AptbaCheck56Days) = checkBoxVal
    End Sub
#End Region

#Region "set Check 56 Days"
    Public Function getCheck56Days() As Boolean
        If (IsNothing(ViewState(ViewStateConstants.AptbaCheck56Days))) Then
            Return True
        Else
            Return CType(ViewState(ViewStateConstants.AptbaCheck56Days), Boolean)
        End If
    End Function
#End Region

#Region "reset Message"
    Public Sub resetMessage()
        lblMessage.Text = String.Empty
        pnlMessage.Visible = False
    End Sub
#End Region

#Region "display Schedule Appointment"
    Protected Sub displayScheduleAppointment()
        Dim apptScheduleParam() As String = SessionManager.getAptbaParam()
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            'Get calander weekly data table from session. (If not exists it will return new DataTable)
            addAppointmentWeeklyDT = SessionManager.getAddCalanderWeeklyDataTable()

            If ((addAppointmentWeeklyDT.Columns.Count <= 0 OrElse SessionManager.getAptbaCalendarPageingClicked() = False) AndAlso (Not SessionManager.isAddCalanderWeeklyDTSessionExists)) Then 'issue
                ' Variable Declaration 
                Dim dsEmployeeList As DataSet = New DataSet()
                Dim dsAppointments As DataSet = New DataSet()
                Dim dsEmployeeLeaves As DataSet = New DataSet()
                Dim strAppointment As StringBuilder = New StringBuilder()
                Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
                Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
                Dim strEmployeeIds As StringBuilder = New StringBuilder()
                Dim rowCount As Integer
                Dim drLeaves() As DataRow
                'Calendar code start here
                If IsNothing(Request.QueryString("pg")) Then
                    'ApplicationConstants.startDate = Date.Today 'issue
                    SessionManager.setAptbaStartDate(Date.Today)
                End If
                'Seek for Monday as starting and Sunday ending day of the Month
                If (SessionManager.getAptbaStartDate().ToString("dddd") <> "Monday") Then 'issue
                    objCalendarUtilities.calculateAptbaMonday(SessionManager.getAptbaStartDate())
                End If
                If (Request.QueryString("e") <> "p") Then 'Appointment to be arranged move previous
                    SessionManager.setAptbaEndDate(DateAdd("d", 6, SessionManager.getAptbaStartDate()))
                    'SessionManager.setAptbaEndDate(DateAdd("d", 7, SessionManager.getAptbaStartDate()))
                    'ApplicationConstants.endDate = DateAdd("d", 7, ApplicationConstants.startDate) 'issue
                End If
                objSchedulingBL.getEngineersListForAppointment(dsEmployeeList)
                'Getting Appointments Information between specific dates
                objScheduleAppointmentsBO.StartDate = SessionManager.getAptbaStartDate()
                objScheduleAppointmentsBO.ReturnDate = SessionManager.getAptbaEndDate()
                'objScheduleAppointmentsBO.StartDate = ApplicationConstants.startDate
                'objScheduleAppointmentsBO.ReturnDate = ApplicationConstants.endDate
                objSchedulingBL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, dsAppointments)
                If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Gas Engineer exist
                    'Creating columns of Datatable
                    addAppointmentWeeklyDT.Columns.Add(" ")
                    'Creating Columns based on Engineers names
                    For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                        objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), addAppointmentWeeklyDT)
                        strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")
                    Next

                    'Getting Employees Leaves Information
                    objScheduleAppointmentsBO.StartDate = SessionManager.getAptbaStartDate()
                    objScheduleAppointmentsBO.ReturnDate = SessionManager.getAptbaEndDate()

                    'objScheduleAppointmentsBO.StartDate = ApplicationConstants.startDate
                    'objScheduleAppointmentsBO.ReturnDate = ApplicationConstants.endDate
                    objScheduleAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
                    objSchedulingBL.getEngineerLeavesDetail(objScheduleAppointmentsBO, dsEmployeeLeaves)
                    'End Getting Employees Leaves Information

                    'Getting Appointments Information in Datarow for Data table {addAppointmentWeeklyDT}
                    dsAppointments.Tables(0).Columns.Add("Distance")
                    Dim scheduledAppt(addAppointmentWeeklyDT.Columns.Count - 1) As String
                    'Dim dr() As DataRow = dsAppointments.Tables(0).Select("(AppointmentDate>= '" + ApplicationConstants.startDate.ToString("MM/dd/yyyy") + "' and AppointmentDate<='" + ApplicationConstants.endDate.ToString("MM/dd/yyyy") + "')", "ASSIGNEDTO ASC")

                    'Calculating Distance via Google API
                    For drdsAppointments = 0 To dsAppointments.Tables(0).Rows.Count - 1
                        dsAppointments.Tables(0).Rows(drdsAppointments).Item("Distance") = 0 'objCalendarUtilities.CalculateDistance(apptScheduleParam(1), dsAppointments.Tables(0).Rows(drdsAppointments).Item(6).ToString())
                    Next

                    'Drawing Weekly Calendar
                    Dim startDate As Date = SessionManager.getAptbaStartDate()
                    For dayCount = 0 To 6
                        'scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                        'SessionManager.setAptbaStartDate(DateAdd("d", 1, SessionManager.getAptbaStartDate()))
                        scheduledAppt(0) = DateAdd("d", dayCount, startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                        rowCount = 1
                        For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1

                            'Adding Leaves Information
                            'drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                            drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                            If (drLeaves.Count > 0) Then 'System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1)).ToString("dddd")
                                For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></div><br/>")
                                Next
                            End If

                            'Adding Appointment Information
                            For rdCount = 0 To dsAppointments.Tables(0).Rows.Count - 1

                                'Check the appointments of the engineers in Datarow
                                If (System.DateTime.Parse(GeneralHelper.getUKCulturedDateTime(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate").ToString())).ToString("dddd") = ApplicationConstants.daysArray(dayCount) And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item("ASSIGNEDTO").ToString()) Then 'System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1)).ToString("dddd")
                                    'if appointment type is gas then it 'll have color according to status other wise appointment box 'll be yelloow

                                    If dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") = "Gas" Then
                                        'Checking Distance selected from selection box
                                        If (dsAppointments.Tables(0).Rows(rdCount).Item("Distance") <= CInt(ddlAddDistance.SelectedItem.Value)) Then
                                            strAppointment.Append("<div class='detailFilterBox' style='padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + " <b>&nbsp;GAS</b></span><br />")
                                        ElseIf (apptScheduleParam(5) = dsAppointments.Tables(0).Rows(rdCount).Item(10).ToString()) Then
                                            strAppointment.Append("<div class='detailAmendBox' style='padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;GAS</b></span><br />")
                                            'Check other appointments
                                        Else
                                            strAppointment.Append("<div class='detailBox' style='background-color:#FFF;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;GAS</b></span><br />")
                                        End If
                                    Else
                                        'if appointment other than gas then appointment box 'll be yellow
                                        strAppointment.Append("<div class='detailBox' style='background-color:#ffef99;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + CType(CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate"), Date).Date + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate"), Date).Date + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") + "</b></span> <br />")
                                    End If
                                    ''Rendering Mobile #
                                    'If (dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() <> "" And dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") = "Gas") Then
                                    '    strAppointment.Append("<asp:ImageButton ID='imgbtnCalanderPhone" + rdCount + "' runat='server' ImageUrl='~/Images/img_cell.png' BorderWidth='0' />" + dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() + "</span><br />")
                                    '    'strAppointment.Append("<img src='../../images/img_cell.png'><span style='margin-left:3px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() + "</span><br />")
                                    'End If
                                    ''Rending Customer Name
                                    'If (dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() <> "") Then
                                    '    strAppointment.Append("<div style='margin-left:25px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() + "<br/>")
                                    'End If
                                    'Rending Address

                                    If (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "" AndAlso GeneralHelper.isPropertyStatusLet(dsAppointments.Tables(0).Rows(rdCount).Item("PropertyStatus").ToString())) Then
                                        strAppointment.Append("<a href='AdminScheduling.aspx?aptId=" + dsAppointments.Tables(0).Rows(rdCount).Item("TenancyId").ToString() + "'><img src='../../Images/img_cell.png' style='border-width:0px' /></a>" + dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + ", ")
                                        strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("PostCode").ToString() + "<br/>")
                                    ElseIf (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "") Then
                                        strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + ", ")
                                        strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("PostCode").ToString() + "<br/>")
                                    End If

                                    'Adding Distance to Appointment
                                    'strAppointment.Append("<span style='margin-right:2px;'>Distance:&nbsp;&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Distance").ToString() + " m<br/>")
                                    'strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp; 0 m <br/></span>")

                                    strAppointment.Append("<span style='margin-right:2px;'><b>" + dsAppointments.Tables(0).Rows(rdCount).Item("Title") + "</b></span>")

                                    'Closing the current Appointment div tag
                                    strAppointment.Append("</div><br />")


                                End If
                            Next

                            'Changed the if else condition to if only to show appointments along with leaves.
                            'So commented out this End it and moved it to top.
                            'End If

                            'If Appointment OR Leave found
                            If (strAppointment.ToString() <> "") Then
                                scheduledAppt(rowCount) = strAppointment.ToString()
                                strAppointment.Clear()
                            Else
                                scheduledAppt(rowCount) = ""
                            End If
                            rowCount = rowCount + 1
                        Next
                        addAppointmentWeeklyDT.Rows.Add(scheduledAppt)
                    Next
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.GasEngNotFound, True)
                End If

                'Save calander weekly data table to session.
                SessionManager.setAddCalanderWeeklyDataTable(addAppointmentWeeklyDT)
            End If
            'Displaying Current Month, Address and Expiration Days
            ' lblMonth.Text = MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString()
            lblMonth.Text = MonthName(SessionManager.getAptbaStartDate().Month) + " " + Year(SessionManager.getAptbaStartDate()).ToString()
            If (apptScheduleParam.Count > 0) Then
                lblAddress.Text = apptScheduleParam(0)
                lblExpiryDays.Text = apptScheduleParam(6)
            End If
            SessionManager.setAptbaAppointmentClicked(False)
            'ApplicationConstants.addAppointmentClicked = False
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Load Inspection Types in DropDown"
    Public Sub loadInspectionTypes(ByRef dropDownRef As DropDownList)
        Dim objUsersBL As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        objUsersBL.getInspectionTypes(resultDataSet)
        dropDownRef.DataSource = resultDataSet
        dropDownRef.DataValueField = ApplicationConstants.InspectionId
        dropDownRef.DataTextField = ApplicationConstants.InspectionName
        dropDownRef.DataBind()
        dropDownRef.Items.Add(New ListItem("Select Type", ApplicationConstants.DropDownDefaultValue))
        If (Me.ddlAddInspectionType.SelectedItem.Value <> ApplicationConstants.DropDownDefaultValue) Then
            dropDownRef.SelectedValue = Me.ddlAddInspectionType.SelectedItem.Value
        Else
            dropDownRef.SelectedValue = ApplicationConstants.DropDownDefaultValue
        End If
        'dropDownRef.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "img btn Calendar Details"

    Protected Sub imgbtnCalendarDetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim btn As ImageButton = CType(sender, ImageButton)
        Dim apptScheduleParam() As String = CType(btn.CommandArgument, String).Split(";")
        Dim pId As String = apptScheduleParam(4)
        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False
        Dim locked As Integer = objSchedulingBL.getPropertyschedulestatus(pId, isTimeExceeded, isScheduled, "Property")
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        End If
        If criticalsection = 1 Then
            Try

                'Dim apptScheduleParam() As String = CType(btn.CommandArgument, String).Split(";")

                SessionManager.setAptbaParam(apptScheduleParam) 'Storing Selected Property Information
                SessionManager.setPropertyIdForEditLetter(apptScheduleParam(4))

                'Removed Calander Weekly Data Table from session so it can be created again.
                SessionManager.removeAmendCalanderWeeklyDataTable()



                Me.setCalendarInfo()

                SessionManager.setAptbaCalendarPageingClicked(False)
                ''Alert: The certificate expiry date for this property is prior to the date of the appointment you have chosen.

                'Check Expiry date for null or empty string if not valid set expiry date to 1970,1,1
                'Commented out previous line of code containing CType to date.

                Dim expiryDate As New Date(1970, 1, 1)

                If Not String.IsNullOrEmpty(apptScheduleParam(3)) Then
                    expiryDate = CType(apptScheduleParam(3), Date)
                End If

                Dim startDate As New DateTime(1970, 1, 1)
                SessionManager.setCertificateExpiryDate((expiryDate - startDate).TotalSeconds)
                SessionManager.setAptbaTenancyClicked(False)
            Catch ex As ThreadAbortException
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try
        Else
            PropertyScheduledPopUp.Show()
        End If

    End Sub
#End Region

#Region "set Schedule Apppointment Popup"

    Public Sub setCalendarInfo()
        Me.displayScheduleAppointment()
        Me.mdlAddPopUpCalendar.Show()

    End Sub
#End Region

#Region "Load Satus"
    Protected Sub LoadSatus()
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlAddStatus.DataSource = resultDataSet
        ddlAddStatus.DataValueField = ApplicationConstants.StatusId
        ddlAddStatus.DataTextField = ApplicationConstants.Title
        ddlAddStatus.DataBind()
        ddlAddStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlAddStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue

        'To Implement CR, Remove the status 'appointment to be arranged' from the drop down list when arranging or rearranging an appointment
        'Action : Removed the status 'appointment to be arranged' from drop down list.
        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Appointment to be arranged")) Then
            ddlAddStatus.Items.RemoveAt(ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Appointment to be arranged")))
        End If
        'To Implement CR, The default status when in the 'Add AppoIntment' pop-up should be 'Arranged'
        'Action : Selected the Status 'Arranged' and Load Actions accordingly.
        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Arranged")) Then
            ddlAddStatus.SelectedIndex = ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Arranged"))
            LoadActions(CType(ddlAddStatus.SelectedValue, Integer))
        End If
    End Sub
#End Region

#Region "Load Actions"
    Protected Sub LoadActions(ByRef statusId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAddAction.DataSource = resultDataSet
        ddlAddAction.DataValueField = ApplicationConstants.ActionId
        ddlAddAction.DataTextField = ApplicationConstants.Title
        ddlAddAction.DataBind()
        ddlAddAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAddAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub LoadLetters(ByRef actionId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim objResourceBL As ResourcesBL = New ResourcesBL()
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlAddLetter.DataSource = resultDataSet
        ddlAddLetter.DataValueField = ApplicationConstants.LetterId
        ddlAddLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlAddLetter.DataBind()
        ddlAddLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlAddLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Validate Add Appointment"
    Protected Function validateAddAppointment()
        Dim success As Boolean = True
        If Me.ddlAddInspectionType.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionType, True)
        ElseIf IsNothing(Me.txtDate.Text) Or Me.txtDate.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionDate, True)
        ElseIf System.DateTime.Parse(Me.ddlAppEndTime.SelectedItem.Value).TimeOfDay <= System.DateTime.Parse(Me.ddlAppStartTime.SelectedItem.Value).TimeOfDay Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentTime, True)

        ElseIf Me.ddlAddStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionStatus, True)
        ElseIf Me.ddlAddAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)

            'To Implement CR, Notes should not be mandatory when adding a new appointment
            'Action : Commented out the ElseIf condition for extNotes

            'ElseIf IsNothing(Me.txtNotes.Text) Or Me.txtNotes.Text = String.Empty Then
            '    success = False
            '    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionNotes, True)

            'To check, if an appointment already exists in given date/time existing 
        ElseIf Me.validateStartEndTime() > 0 Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.duplicateAppointmentTime, True)
        End If

        Return success
    End Function
#End Region

#Region "Reset Add Appointment Controls"
    Protected Sub resetAddAppointmentControls()
        Me.txtDate.Text = Date.Today
        Me.ddlAddInspectionType.SelectedValue = "-1"
        Me.txtDate.Text = Date.Now.ToString().Substring(0, 10)
        'Me.ddlAddStatus.SelectedValue = "-1"

        'To Implement CR, The default status when in the 'Add AppoIntment' pop-up should be 'Arranged'
        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Arranged")) Then
            Me.ddlAddStatus.SelectedValue = ddlAddStatus.Items(ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Arranged"))).Value
        Else
            Me.ddlAddStatus.SelectedValue = "-1"
        End If

        'Me.LoadActions(-1)
        Me.LoadActions(CType(ddlAddStatus.SelectedValue, Integer))

        Me.LoadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#Region "Populate Appointment Start/End Dropdowns"

    Private Sub PopulateAppointmentDropdown()
        Dim dsAppointments As DataSet = New DataSet()
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        objScheduleAppointmentsBO.StartDate = SessionManager.getAptbaStartDate() 'issue
        objScheduleAppointmentsBO.ReturnDate = SessionManager.getAptbaEndDate() 'issue
        'objScheduleAppointmentsBO.StartDate = ApplicationConstants.startDate 'issue
        'objScheduleAppointmentsBO.ReturnDate = ApplicationConstants.endDate 'issue

        objSchedulingBL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, dsAppointments)
        Dim addAppointmentParam() As String = SessionManager.getAddAptbaParam() 'Getting Operative Name, Id from session
        Dim newListItem As ListItem
        'Dim drCounter As Integer
        'Dim dr() As DataRow = dsAppointments.Tables(0).Select("(AppointmentDate= '" + System.DateTime.Parse(txtDate.Text).ToString("MM/dd/yyyy") + "' and ASSIGNEDTO=" + addAppointmentParam(0) + ")", "ASSIGNEDTO ASC")
        For i = 0 To ApplicationConstants.appointmentTime.Count - 1

            newListItem = New ListItem(ApplicationConstants.appointmentTime(i), ApplicationConstants.appointmentTime(i))
            ddlAppStartTime.Items.Add(newListItem)
            newListItem = New ListItem(ApplicationConstants.appointmentTime(i), ApplicationConstants.appointmentTime(i))
            ddlAppEndTime.Items.Add(newListItem)
        Next

        newListItem = New ListItem("20:00", "20:00")

        ddlAppEndTime.Items.Add(newListItem)
        setEndTimeSelectedindex(ddlAppStartTime, ddlAppEndTime)


    End Sub

#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'if user didn't selected the letter from letter drop down
        If ddlAddLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlAddLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If

    End Sub
#End Region

#Region "Open Appointment Popup"
    'Protected Sub imgAddAppointment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAddAppointment.Click
    Public Sub AddAppointmentPopup()
        Try
            SessionManager.setAddAptbaParam(Request.QueryString("param").Split(":")) 'Storing Operative Name, Id in session
            Dim addAppointmentParam() As String = SessionManager.getAddAptbaParam() 'Getting Operative Name, Id from session
            Dim apptScheduleParam() As String = SessionManager.getAptbaParam()
            'Me.hdnExpiryDate.Value = SessionManager.getCertificateExpiryDate()
            lblLocation.Text = apptScheduleParam(0)

            lblOperative.Text = addAppointmentParam(1) 'issue
            If (txtDate.Text = "") Then
                txtDate.Text = Date.Today

            End If
            'lblMonth.Text = MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString() 'issue
            lblMonth.Text = MonthName(SessionManager.getAptbaStartDate().Month) + " " + Year(SessionManager.getAptbaStartDate()).ToString() 'issue

            If (apptScheduleParam.Count > 0) Then
                lblAddress.Text = apptScheduleParam(0)
                lblExpiryDays.Text = apptScheduleParam(6)
            End If
            Me.loadInspectionTypes(Me.ddlAddInspectionType)
            If (Me.ddlAddStatus.Items.Count <= 0) Then
                Me.LoadSatus()
            End If

            lnkBtnAppDetails.Visible = GeneralHelper.isPropertyStatusLet(apptScheduleParam(8).ToString())
            lnkBtnAppDetails.CommandArgument = apptScheduleParam(2)
            ' Me.resetAddAppointmentControls()
            Me.PopulateAppointmentDropdown()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()

        End Try
    End Sub
#End Region

#Region "Save Appointment Information"
    Protected Function saveAddAppointment(ByRef pushNoticificationStatus As String, ByRef pushNoticificationDescription As String)
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        Dim apptScheduleParam() As String = SessionManager.getAptbaParam() 'Getting Property Information from session
        Dim addAppointmentParam() As String = SessionManager.getAddAptbaParam() 'Getting Operative Name, Id from session
        'Save Appointment Information
        objScheduleAppointmentsBO.Duration = ApplicationConstants.DefaultSchedulingHour
        objScheduleAppointmentsBO.JournelId = apptScheduleParam(5)
        objScheduleAppointmentsBO.PropertyId = apptScheduleParam(4)
        objScheduleAppointmentsBO.CreatedBy = SessionManager.getAppServicingUserId()
        objScheduleAppointmentsBO.InspectionTypeId = ddlAddInspectionType.SelectedValue
        objScheduleAppointmentsBO.AppointmentDate = System.DateTime.Parse(txtDate.Text)
        objScheduleAppointmentsBO.AppointmentEndDate = objScheduleAppointmentsBO.AppointmentDate
        objScheduleAppointmentsBO.StatusId = ddlAddStatus.SelectedValue
        objScheduleAppointmentsBO.ActionId = ddlAddAction.SelectedValue
        objScheduleAppointmentsBO.LetterId = ddlAddLetter.SelectedValue
        objScheduleAppointmentsBO.Notes = txtNotes.Text
        objScheduleAppointmentsBO.DocumentPath = GeneralHelper.getDocumentUploadPath()
        If (System.DateTime.Parse(Me.ddlAppEndTime.SelectedItem.Value).TimeOfDay > System.DateTime.Parse("12:00").TimeOfDay) Then
            objScheduleAppointmentsBO.Shift = "PM"
        Else
            objScheduleAppointmentsBO.Shift = "AM"
        End If
        objScheduleAppointmentsBO.AppStartTime = ddlAppStartTime.SelectedItem.Value
        objScheduleAppointmentsBO.AppEndTime = ddlAppEndTime.SelectedItem.Value
        objScheduleAppointmentsBO.AssignedTo = addAppointmentParam(0)

        'To save null value in tenancyId in case of void property.
        objScheduleAppointmentsBO.TenancyId = Nothing
        If Not (String.IsNullOrEmpty(apptScheduleParam(2))) Then
            objScheduleAppointmentsBO.TenancyId = apptScheduleParam(2)
        End If

        'Save Standard Letter
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objScheduleAppointmentsBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objScheduleAppointmentsBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objScheduleAppointmentsBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()
            If docQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objScheduleAppointmentsBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objScheduleAppointmentsBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objScheduleAppointmentsBO.IsDocumentAttached = False
            End If
        End If
        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        Return objSchedulingBL.saveAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

    End Function

#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList() 'issue
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList() 'issue

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAddStatus.SelectedIndexChanged
        Try
            SessionManager.setAptbaAppointmentClicked(True)
            'ApplicationConstants.addAppointmentClicked = True 'issue
            Dim statusId As Integer = CType(ddlAddStatus.SelectedValue(), Integer)
            Me.LoadActions(statusId)
            Me.LoadLetters(-1)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()
        End Try
    End Sub

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAddAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAddAction.SelectedValue(), Integer)
            Me.LoadLetters(actionId)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()
        End Try
    End Sub
#End Region



#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()
        End Try
    End Sub

#End Region



#Region "Btn Save Appointment Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click
        Dim isSaved As Boolean = False
        Dim checked As Boolean = True
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        Dim SmsStatus As String = UserMessageConstants.SmsNotSent
        Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
        Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
        Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
        Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
        Dim apptScheduleParam() As String = SessionManager.getAptbaParam()
        Dim pId As String = ""
        pId = apptScheduleParam(4)
        If Not IsNothing(pId) AndAlso Not pId = "" Then
            If SessionManager.getUserEmployeeId = objSchedulingBL.getPropertyLockedBy(pId, "Property") Then
                Try

                    If (Me.validateAddAppointment() = True) Then
                        Dim journalHistoryId As Integer = 0
                        journalHistoryId = Me.saveAddAppointment(PushNoticificationStatus, PushNoticificationDescription)
                        If journalHistoryId > 0 Then
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)

                            isSaved = True



                            ' Start of code to send the SMS'
                            Dim tenancyID As Integer = Convert.ToInt32(apptScheduleParam(2))
                            Dim dstenantsInfo As New DataSet()
                            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

                            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                                SmsStatus = UserMessageConstants.SmsSending
                                SmsDescription = UserMessageConstants.SmsDescriptionSending
                                Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                                Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                            End If

                            EmailStatus = UserMessageConstants.EmailSending
                            EmailDescription = UserMessageConstants.EmailDescriptionSending
                            Me.sendEmail(journalHistoryId, EmailStatus, EmailDescription)
                            Me.setEmailAndSmsStatusForAppointment(journalHistoryId, SmsStatus, SmsDescription, EmailStatus, EmailDescription)
                            Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)

                            SessionManager.removeActivityLetterDetail()
                        Else
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                        End If

                    End If
                Catch ex As ThreadAbortException
                    uiMessageHelper.Message = ex.Message
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                    End If

                    If isSaved = False Then

                        Me.setCalendarInfo()
                        Me.mdlPopUpAddAppointment.Show()

                    Else

                        SessionManager.setAptbaAppointmentClicked(True)
                        SessionManager.setAptbaIsSuccess(True)
                        objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
                        'If the appointment is saved sucessfully Disable save button. and cancel button.
                        'Further more set calendar info from session using Me.setCalendarInfo() and also show popup.
                        btnSaveAction.Enabled = False
                        btnCancelAction.Enabled = False
                        'Me.setCalendarInfo()
                        'Me.mdlPopUpAddAppointment.Show()
                        Me.mdlAddPopUpCalendar.Hide()
                        Me.mdlPopUpAddAppointment.Hide()
                        searchAppointmentToBeArranged(SessionManager.getDueWithIn56Days(), String.Empty, Me.getFuelType(), Me.getStatusType())
                        'Commented out, it is handelded in a different way now.
                        'Response.Redirect("AdminScheduling.aspx?ac=0")
                    End If
                End Try
            Else
                mdlPopUpAddAppointment.Hide()
                SessionManager.setAptbaAppointmentClicked(True)
                'populateAppointmentToBeArrangedGrid()
                PropertyScheduledPopUp.Show()
            End If

        End If

    End Sub
#End Region

#Region "set Push Noticification Status For Appointment"
    Private Sub setPushNoticificationStatusForAppointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
        Try
            objSchedulingBL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
    Protected Sub btnCancelAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelAction.Click
        SessionManager.setAptbaIsSuccess(True)
        SessionManager.setAptbaCalendarPageingClicked(True)
        mdlPopUpAddAppointment.Hide()
    End Sub

    Protected Sub btnAddViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddViewLetter.Click
        Try
            If (ddlAddLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()
        End Try
    End Sub
#Region "ckBox Refresh DataSet Checked Changed"


    Protected Sub ckBoxAddRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxAddRefreshDataSet.CheckedChanged
        Me.ckBoxAddRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlAddLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId()) 'issue
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            Me.mdlPopUpAddAppointment.Show()
        End Try

    End Sub
#End Region
#End Region

    Protected Sub imgBtnAptbaClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub imgbtnAddAppointmentCancelCalendar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnAddAppointmentCancelCalendar.Click

        Dim apptScheduleParam() As String = SessionManager.getAptbaParam()
        Dim pId As String = ""
        pId = apptScheduleParam(4)
        If SessionManager.getUserEmployeeId() = objSchedulingBL.getPropertyLockedBy(pId, "Property") Then
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        End If

        SessionManager.setAptbaAppointmentClicked(True)

        SessionManager.setAptbaCalendarPageingClicked(True)

        'Commented out addAppointmentWeeklyDT and removed it from session so it can be created again
        SessionManager.removeAddCalanderWeeklyDataTable()
        'addAppointmentWeeklyDT = New DataTable()

        mdlPopUpAddAppointment.Hide()
        mdlAddPopUpCalendar.Hide()

        'Re Populate Appointment to be Arranged Grid only on calender close.
        searchAppointmentToBeArranged(SessionManager.getDueWithIn56Days(), String.Empty, Me.getFuelType(), Me.getStatusType())
    End Sub

#Region "img Btn close Property Scheduled Click"

    Protected Sub imgPropertyScheduleClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPropertyScheduleClose.Click
        Try
            mdlPopUpAddAppointment.Hide()
            mdlAddPopUpCalendar.Hide()
            populateAppointmentToBeArrangedGrid()


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region



#Region "Set Fuel Type"
    Protected Sub setFuelType(ByRef fuelType As Integer)
        ViewState(ViewStateConstants.FuelType) = fuelType
    End Sub
#End Region

#Region "Set Status Type"
    Protected Sub setStatusType(ByRef statusType As String)
        ViewState(ViewStateConstants.StatusType) = statusType
    End Sub
#End Region

#Region "Get Fuel Type"
    Protected Function getFuelType() As Integer
        If (IsNothing(ViewState(ViewStateConstants.FuelType))) Then
            Return -1
        Else
            Return CType(ViewState(ViewStateConstants.FuelType), Integer)
        End If
    End Function

#End Region

#Region "Get Status Type"
    Protected Function getStatusType() As String
        If (IsNothing(ViewState(ViewStateConstants.StatusType)) Or ViewState(ViewStateConstants.StatusType) = "All") Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.StatusType), String)
        End If
    End Function

#End Region
    Protected Sub ddlAddDistance_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAddDistance.SelectedIndexChanged

        'Commented out addAppointmentWeeklyDT and removed it from session so it can be created again
        SessionManager.removeAddCalanderWeeklyDataTable()
        'addAppointmentWeeklyDT = New DataTable()

        Me.setCalendarInfo()
    End Sub

#Region "ck Box Document Upload Checked Changed"
    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty) 'issue
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAddPopUpCalendar.Show()

            Me.mdlPopUpAddAppointment.Show()
        End Try
    End Sub
#End Region

#Region "lnk Btn App Details Click"
    Protected Sub lnkBtnAppDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkBtnAppDetails.Click
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)

            Dim isAddAppointmentPopup As Boolean = True

            setisAddAppointmentPopupViewState(isAddAppointmentPopup)

            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                setTenantsInfo(dstenantsInfo, tblAptbaTenantInfoAddPopup)

                Me.setCalendarInfo()
                Me.mdlPopUpAddAppointment.Show()
                Me.mdlPopUpAddAppointmentContactDetails.Show()

            Else
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.noTenantInformationFound, True)

                Me.setCalendarInfo()
                Me.mdlPopUpAddAppointment.Show()
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub


    Protected Sub setContactInfo(ByVal customerId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resultDataTable As New DataTable

        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblFirstName.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblMobileNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblTelNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)

        Me.mdlPopUpAddAppointment.Show()

        Me.mdlPopUpAddAppointmentContactDetails.Show()

    End Sub

#End Region

#Region "Image Button Close Tenant Info Click"

    Protected Sub imgBtnCloseTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseTenantInfo.Click
        If getisAddAppointmentPopupViewState() Then
            Me.setCalendarInfo()
            mdlPopUpAddAppointment.Show()
        End If
        removeisAddAppointmentPopupViewState()
    End Sub

#End Region

#Region "Set End Time Drop Down Selected index - On Start Time Index change"

    Private Sub setEndTimeSelectedindex(ByRef ddlStartTime As DropDownList, ByRef ddlEndTime As DropDownList)
        'Set end time 1 hour from start time
        'If 1 hour from start time is not available set to the last item of ddlEndTime
        If ddlStartTime.SelectedIndex + 4 < ddlEndTime.Items.Count Then
            ddlEndTime.SelectedIndex = ddlStartTime.SelectedIndex + 4
        Else
            ddlEndTime.SelectedIndex = ddlEndTime.Items.Count - 1
        End If
    End Sub


#End Region

#Region "ddlAppStartTime Selected Index Changed"

    Protected Sub ddlAppStartTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAppStartTime.SelectedIndexChanged
        Try
            setEndTimeSelectedindex(ddlAppStartTime, ddlAppEndTime)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAddPopUpCalendar.Show()
            mdlPopUpAddAppointment.Show()
        End Try
    End Sub


#End Region

#Region "View State Function"

#Region "isAddAppointment View State Set/Get/Remove"

    Public Sub setisAddAppointmentPopupViewState(ByRef isAddAppointmentPopup As Boolean)
        ViewState(ViewStateConstants.isAddAppointmentPopup) = isAddAppointmentPopup
    End Sub

    Public Function getisAddAppointmentPopupViewState() As Boolean
        Dim isAddAppointmentPopup As Boolean = False
        If ViewState(ViewStateConstants.isAddAppointmentPopup) IsNot Nothing Then
            isAddAppointmentPopup = CType(ViewState(ViewStateConstants.isAddAppointmentPopup), Boolean)
        End If
        Return isAddAppointmentPopup
    End Function

    Public Sub removeisAddAppointmentPopupViewState()
        ViewState.Remove(ViewStateConstants.isAddAppointmentPopup)
    End Sub

#End Region

#End Region

#Region "Close Add Calander Tenant Info Popup"

    Protected Sub imgbtnCloseTenantInfoAddCalander_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnCloseTenantInfoAddCalander.Click
        Me.setCalendarInfo()
        SessionManager.setAptbaTenancyClicked(False)
    End Sub

#End Region

#Region "Image Button Close Add Appointment Click"

    Protected Sub imgBtnCloseAddAppointment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAddAppointment.Click

        'If the appointment is saved, remove the existing calendar from session.
        If Not btnSaveAction.Enabled Then
            SessionManager.removeAddCalanderWeeklyDataTable()
        End If

        'Hide Add Appointment Popup and Update Calendar (conditional)
        Me.setCalendarInfo()
        Me.mdlPopUpAddAppointment.Hide()

    End Sub

#End Region

#Region "Validate Appointment Start/End Time"
    'To check, if an appointment already exists in given date/time existing 

    Protected Function validateStartEndTime()
        'Getting Appointments Information between specific dates
        Dim appStartDate As Date = SessionManager.getAptbaStartDate()
        Dim appEndDate As Date = SessionManager.getAptbaEndDate()
        Dim addAppointmentParam() As String = SessionManager.getAddAptbaParam() 'Getting Operative Name, Id from session
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()

        objScheduleAppointmentsBO.StartDate = appStartDate
        objScheduleAppointmentsBO.ReturnDate = appEndDate

        Dim dsAppointments As DataSet = New DataSet()
        objSchedulingBL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, dsAppointments)

        Dim startDate As New DateTime(1970, 1, 1)
        Dim startTimeInSec, endTimeInSec As Integer
        startTimeInSec = (System.DateTime.Parse(txtDate.Text + " " + ddlAppStartTime.SelectedItem.Value) - startDate).TotalSeconds
        endTimeInSec = (System.DateTime.Parse(txtDate.Text + " " + ddlAppEndTime.SelectedItem.Value) - startDate).TotalSeconds
        Dim strStartFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec<=" + startTimeInSec.ToString() + " and endTimeInSec>=" + startTimeInSec.ToString() + ")"
        Dim strendFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec<=" + endTimeInSec.ToString() + " and endTimeInSec>=" + endTimeInSec.ToString() + ")"
        Dim strBetweenFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec>=" + startTimeInSec.ToString() + " and endTimeInSec<=" + endTimeInSec.ToString() + ")"
        Return dsAppointments.Tables(0).Select(strStartFilter, "startTimeInSec ASC").Count _
            + dsAppointments.Tables(0).Select(strendFilter, "startTimeInSec ASC").Count _
            + dsAppointments.Tables(0).Select(strBetweenFilter, "startTimeInSec ASC").Count
    End Function

#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal tenantmobile As String, ByRef smsStatus As String, ByRef smsDescription As String)
        Try

            tenantmobile = tenantmobile.Replace(" ", String.Empty)

            If tenantmobile.Length >= 10 Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(ddlAppStartTime.SelectedItem.Text, txtDate.Text)
                ' GeneralHelper.sendSMS(body, tenantmobile)
                'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                'Dim javascriptMethod As String = "sendSMS('" + tenantmobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(tenantmobile, body)
                smsStatus = UserMessageConstants.SmsSent
                smsDescription = UserMessageConstants.SmsDescriptionSent
            Else
                smsStatus = UserMessageConstants.SmsSendingFailed
                smsDescription = UserMessageConstants.AppointmentSavedSMSError + UserMessageConstants.InvalidMobile
                'uiMessageHelper.IsError = True
                'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            smsStatus = UserMessageConstants.SmsSendingFailed
            smsDescription = UserMessageConstants.AppointmentSavedSMSError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal journalHistoryId As Integer, ByRef emailStatus As String, ByRef emailDescription As String)

        Try
            Dim objScheduling As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            objScheduling.getAppointmentInfoForEmail(resultDataset, journalHistoryId)
            Dim infoDt As DataTable = resultDataset.Tables(0)
            If (infoDt.Rows.Count > 0) Then
                Dim recepientEmail As String = infoDt.Rows(0)(ApplicationConstants.EmailCol)
                Dim recepientName As String = infoDt.Rows(0)(ApplicationConstants.CustomerNameCol)
                Dim jobSheet As String = infoDt.Rows(0)(ApplicationConstants.JSGCol)
                Dim appointmentDate As String = infoDt.Rows(0)(ApplicationConstants.AppointmentDateCol)
                Dim appointmentTime As String = infoDt.Rows(0)(ApplicationConstants.AppointmentTimeCol)

                If Validation.isEmail(recepientEmail) Then
                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheet, appointmentTime, appointmentDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)
                    emailStatus = UserMessageConstants.EmailSent
                    emailDescription = UserMessageConstants.EmailDescriptionSent
                Else
                    emailStatus = UserMessageConstants.EmailSendingFailed
                    emailDescription = UserMessageConstants.AppointmentSavedEmailError + UserMessageConstants.InvalidEmail
                    'uiMessageHelper.IsError = True
                    'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidEmail, True)
                End If
            Else
                Throw New Exception(UserMessageConstants.AppointmentInformationNotFound)
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            emailStatus = UserMessageConstants.EmailSendingFailed
            emailDescription = UserMessageConstants.AppointmentSavedEmailError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "set Email And Sms Status For Appointment"
    Private Sub setEmailAndSmsStatusForAppointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
        Try
            objSchedulingBL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region


End Class