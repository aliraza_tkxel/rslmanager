﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AppointmentNotes
    Inherits UserControlBase

#Region "Control Events"

    Public Event CloseButtonClicked As EventHandler

#End Region

#Region "Events"

#Region "Page Load "
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Btn Save Changes Click"
    Protected Sub btnSaveChanges_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveChanges.Click
        Try
            saveAppointmentNotes()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Btn Close Click"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click
        Try
            RaiseEvent CloseButtonClicked(Me, New EventArgs)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Validate notes"
    ''' <summary>
    ''' Validate notes
    ''' </summary>
    ''' <remarks></remarks>
    Public Function isNotesValid()

        Dim isValid = True
        If txtAppointmentNotes.Text.Length > 1000 Then
            isValid = False
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.InValidNotesLength, True)
        End If
        Return isValid

    End Function
#End Region

#Region "Load appointment data"
    ''' <summary>
    ''' load appointment data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub loadAppointmentData(ByVal appointmentId As Integer)
        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim dsAppointmentDetail As DataSet = New DataSet()

        ViewState.Add(ViewStateConstants.AppointmentId, appointmentId)

        objSchedulingBL.getAppointmentDetail(dsAppointmentDetail, appointmentId)
        Dim dtAppointmentDetail As DataTable = dsAppointmentDetail.Tables(0)
        If dtAppointmentDetail.Rows.Count > 0 Then
            lblLocation.Text = dtAppointmentDetail.Rows(0)("location")
            lblAppointmentDate.Text = dtAppointmentDetail.Rows(0)("appointmentDate")
            lblStartTime.Text = dtAppointmentDetail.Rows(0)("startTime")
            lblEndTime.Text = dtAppointmentDetail.Rows(0)("endTime")
            lblOperative.Text = dtAppointmentDetail.Rows(0)("operative")
            txtAppointmentNotes.Text = dtAppointmentDetail.Rows(0)("notes")
            Dim operativeId As Integer = Convert.ToInt32(dtAppointmentDetail.Rows(0)("operativeId"))
            ViewState.Add(ViewStateConstants.OperativeId, operativeId)
        Else
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

    End Sub
#End Region

#Region "Save appointment notes"
    ''' <summary>
    ''' Save appointment notes
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub saveAppointmentNotes()

        If isNotesValid() Then
            Dim appointmentId As Integer = Convert.ToInt32(ViewState(ViewStateConstants.AppointmentId))
            Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
            Dim notes As String = txtAppointmentNotes.Text

            objSchedulingBL.saveAppointmentNotes(appointmentId, notes)

            Dim message As String = UserMessageConstants.AppointmentNotesPushNotificationMessage
            Dim operativeId As Integer = Convert.ToInt32(ViewState(ViewStateConstants.OperativeId))
            Dim isSent As Boolean = GeneralHelper.pushNotificationAppliance(message, operativeId)

            If isSent = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.AppointmentNotesSaved, False)
            Else
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.AppointmentNotesSavedNotificationFailed, False)
            End If

        End If

    End Sub
#End Region

#End Region

End Class