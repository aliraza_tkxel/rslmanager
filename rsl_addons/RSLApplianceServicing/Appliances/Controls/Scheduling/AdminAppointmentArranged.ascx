﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AdminAppointmentArranged.ascx.vb" Inherits="Appliances.AdminAppointmentArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Import Namespace="AS_Utilities" %>
<script type="text/javascript">
    var w;
    function uploadError(sender, args) {
        alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
    }

    function uploadComplete() {
        __doPostBack();
    }

    function openEditLetterWindow() {

        var letterId = document.getElementById('<%= ddlLetter.ClientID %>').value;
        if (letterId == -1) {

        }
        else {
            w = window.open('../../Views/Resources/EditLetter.aspx?id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
    }

    function fireLetterUploadCheckboxEvent() {
        // alert('sdfsd');
        document.getElementById('<%= ckBoxRefreshDataSet.ClientID %>').checked = true;
        setTimeout('__doPostBack(\'<%= ckBoxRefreshDataSet.UniqueID %>\',\'\')', 0);
    }    
   
</script>
<style type="text/css">
    .style1
    {
        width: 266px;
    }
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 229px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
    .Noticification
    {
        height:20px;
        width:20px;
    }
    .greyFont
    {
        color : Gray;
    }
    .yellowFont
    {
        color : Yellow;
    }
    .redFont
    {
        color : Red;
    }
    .greenFont
    {
        color : Green;
    }
</style>
<div style="height: 400px; overflow: auto;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <cc1:PagingGridView ID="grdAppointmentArranged" runat="server" AutoGenerateColumns="False"
        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
        CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True"
        AllowPaging="True" PageSize="30">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnApaTenantInfo" runat="server" CommandArgument='<%# Eval("TENANCYID") %>'
                        ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnApaTenantInfo_Click" BorderStyle="None"
                        Visible='<%# GeneralHelper.isPropertyStatusLet(Eval("PropertyStatus").ToString()) %>'
                        BorderWidth="0px" />                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" JSG No" SortExpression="JSGNUMBER">
                <ItemTemplate>
                    <asp:Label ID="lbljsgno" runat="server" Text='<%# "JSG"+ Eval("JSGNUMBER").ToString() %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Logged " SortExpression="LOGGEDDATE">
                <ItemTemplate>
                    <asp:Label ID="lblLogged" runat="server" Text='<%# Bind("LOGGEDDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                <ItemTemplate>
                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">--%>
            <asp:TemplateField HeaderText="Expiry Date">
                <ItemTemplate>
                    <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                    <%--<asp:Label ID="lblExpirydate" runat="server" Text='<%# Convert.ToDateTime(Eval("EXPIRYDATE")).ToShortDateString() %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Days" SortExpression="Days">
                <ItemTemplate>
                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("DAYS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fuel Type" SortExpression="Fuel">
                <ItemTemplate>
                    <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'>></asp:Label>                    
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Engineer" SortExpression="ENGINEER">
                <ItemTemplate>
                    <asp:Label ID="lblEngineer" runat="server" Text='<%# Bind("ENGINEER") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment" SortExpression="APPOINTMENT">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("APPOINTMENT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnEmailStatus" runat="server" CommandArgument='<%# Eval("EmailDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EmailStatusDescription").ToString() %>'
                        ImageUrl='<%# Eval("EmailImagePath") %>' OnClick="imgbtnEmail_Click" BorderStyle="None" CssClass="Noticification"
                        BorderWidth="0px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnSmsStatus" runat="server" CommandArgument='<%# Eval("SmsDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("SmsStatusDescription").ToString() %>'
                        ImageUrl='<%# Eval("SmsImagePath") %>' OnClick="imgbtnSms_Click" BorderStyle="None" CssClass="Noticification"
                        BorderWidth="0px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnPushNoticificationStatus" runat="server" CommandArgument='<%# Eval("PushNoticificationDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("ASSIGNEDTO").ToString()+";"+Eval("ADDRESS").ToString()+";"+Eval("ApDate").ToString()+";"+Eval("APPOINTMENT").ToString()+";"+Eval("PushNoticificationStatusDescription").ToString() %>'
                        ImageUrl='<%# Eval("PushNoticificationImagePath") %>' OnClick="imgbtnPushNoticification_Click" BorderStyle="None" CssClass="Noticification"
                        BorderWidth="0px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgAppointmentArrangedArrow" runat="server" OnClick="imgbtnCalendarDetail_Click"
                        BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("PROPERTYID").ToString()+";"+Eval("APPOINTMENTID").ToString()+";"+Eval("APPOINTMENT")+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString() %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="btnCancelReportedFaults" runat="server" OnClick="btnCancelReportedFaults_Click"
                        BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/cross2.png" %>'
                        CommandArgument='<%# Eval("JournalId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        

        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" BorderColor="Black"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFFFCC" Font-Bold="True" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
        <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="10" />
        <PagerStyle BackColor="White" />
    </cc1:PagingGridView>
    <asp:Panel ID="pnlAppointmentArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfoApa" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnApaClose" runat="server" Style="position: absolute; top: -10px;
                        right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    Tenant:
                </td>
                <td>
                    <asp:ImageButton ID="imgBtnApaClose" runat="server" Style="position: absolute; top: -10px;
                        right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    <asp:Label ID="lblApaFisrtname" runat="server" Text='<%# Bind("FIRSTNAME") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile:
                </td>
                <td>
                    <asp:Label ID="lblApaMobile" runat="server" Text='<%# Bind("MOBILE") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone:
                </td>
                <td>
                    <asp:Label ID="lblApaTel" runat="server" Text='<%# Bind("TEL") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <asp:Label ID="lblApaEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                </td>
            </tr>--%>
        </table>
    </asp:Panel>
</div>
<asp:ModalPopupExtender ID="mdlPopUpAppointmentArrangedPhone" runat="server" TargetControlID="lbldispActivity"
    PopupControlID="pnlAppointmentArrangedTenantInfo" Enabled="true" DropShadow="true"
    CancelControlID="imgBtnApaClose">
</asp:ModalPopupExtender>
<%--<asp:PopupControlExtender ID="popUpAppointmentToBeArrangedPhone" runat="server" 
                                     Enabled="True" ExtenderControlID="" 
                                     PopupControlID="pnlAppointmentToBeArrangedTenantInfo" Position="Bottom" 
                                     TargetControlID="imgbtnAptbaTenantInfo" >
                                         TargetControlID="imgbtnAptbaTenantInfo" CacheDynamicResults="true" EnableViewState="true"  ViewStateMode="Enabled" DropShadow="true">
                                 </asp:PopupControlExtender>--%>
<asp:Panel runat="server" ID="pnlCalendar" Style="height: 500px; width: 1000px; background-color: White;
    border: 1px solid gray; padding: 10px; top: -10px; top: 0px;">
    <asp:ImageButton ID="imgbtnCancelCalendarpnl" runat="server" ImageAlign="Right" ImageUrl="~/Images/cross2.png"
        Style="position: absolute; top: -10px; right: -10px" BorderWidth="0" />
    <div style="background-color: Black; color: White; border: solid 1px gray; height: 20px;
        padding: 8px; text-align: left;">
        <span style="font-size: 13px; font-weight: bold;">Rearrange Appointment</span>
        <asp:Label ID="lblAddress" runat="server" Text='' Style="padding-left: 240px;"></asp:Label>
        <span style="padding-left: 150px;">Certificate Expires in
            <asp:Label ID="lblExpiryDays" runat="server" Text='' Style="vertical-align: baseline;"></asp:Label>
            &nbsp;days</span>
    </div>
    <div class="group" style="margin: 10px 0; border: 1px solid gray; text-align: left;
        width: 977px;">
        <span style="float: left; padding-left: 12px; padding-top: 4px;">
            <asp:Label ID="lblMonth" runat="server" Font-Bold="True" Style="padding: 8px;"></asp:Label>
        </span>
        <asp:Panel ID="msgAmendPanel" runat="server" Visible="false">
            <span style="float: left; padding-left: 100px; padding-top: 4px;">
                <asp:Label ID="lblAmendMsg" runat="server">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </asp:Label>
            </span>
        </asp:Panel>
        <span id="calToolbar"><span style="float: left;">Proximity of Engineer within: <span>
            <asp:DropDownList ID="ddlAmendDistance" runat="server" Width="30%" AutoPostBack="True">
                <asp:ListItem Value="1">1 mile</asp:ListItem>
                <asp:ListItem Value="2">2 miles</asp:ListItem>
                <asp:ListItem Value="3">3 miles</asp:ListItem>
                <asp:ListItem Value="4">4 miles</asp:ListItem>
                <asp:ListItem Value="5">5 miles</asp:ListItem>
                <asp:ListItem Value="6">6 miles</asp:ListItem>
                <asp:ListItem Value="7">7 miles</asp:ListItem>
                <asp:ListItem Value="8">8 miles</asp:ListItem>
                <asp:ListItem Value="9">9 miles</asp:ListItem>
                <asp:ListItem Value="10">10 miles</asp:ListItem>
            </asp:DropDownList>
        </span></span><span style="margin-top: 1px; float: left; margin-right: 20px;">
            <div style="border: solid 1px gray; padding: 0px !important; float: left;">
                <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?e=p"><img src="../../images/arrow_left.png" border="0" style="padding-top: 2px;
    vertical-align: text-top;float:left" /></asp:HyperLink>
                <span style="border-left: solid 1px gray; border-right: solid 1px gray; vertical-align: top;
                    float: left">
                    <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?e=t"
                        ForeColor="Black">Today</asp:HyperLink>
                </span>
                <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="../../Views/Scheduling/AdminScheduling.aspx?e=n"><img src="../../images/arrow_right.png" border="0" style="padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;float:left"/></asp:HyperLink>
            </div>
        </span>
            <!--            <asp:Label ID="lbl_startDate" runat="server" ForeColor="White"></asp:Label>
            <asp:Label ID="lbl_endDate" runat="server" ForeColor="White"></asp:Label>-->
        </span>
    </div>
    <div style="width: 980px; height: 400px; overflow: auto; z-index:2;">
        <table class="fixedHeaderCalendar" id="appointmentCalendar" cellpadding="0" cellspacing="0"
            border="1" style="border: solid 1px gray;">
            <thead>
                <% 
                    If varWeeklyDT.Columns.Count <= 1 Then%>
                <tr>
                    <td valign="top">
                        <span class="error">Gas engineer not found.</span>
                    </td>
                </tr>
                <%Else%>
                <tr>
                    <%  Dim empArray() As String
                        
                        For dtCount As Integer = 0 To varWeeklyDT.Columns.Count - 1%>
                    <th style="font-weight: bold; border: solid 1px gray; text-align: center;">
                        
                            <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " And varWeeklyDT.Columns(dtCount).ToString() <> "Distance" Then
                                    empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                                
                            %>
                            <span style="margin-bottom: 10px;">
                            <%= empArray(1).Trim() %></span>&nbsp; <a href="../../Views/Scheduling/AdminScheduling.aspx?op=<%=varWeeklyDT.Columns(dtCount).ToString() %>">
                                <img style="border-width:0px;" alt="btn_add" src="../../images/btn_add.png" />
                            </a>
                        <%--<asp:ImageButton ID="imgAddAppointment" runat="server" OnClientClick="populateTextBox('<%=varWeeklyDT.Columns(dtCount).ToString() %> ')"  OnClick="imgAddAppointment_Click" BorderStyle="None" ImageUrl='../../images/btn_add.png'  /> --%>
                    <%  End If %>
                    </th>
                    <% Next %>
                    </tr>
                    <% End If %>                
            </thead>
            <tbody>
                <%  For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                        Dim bgColor As String
                        If (rdCount Mod 2 = 0) Then
                            bgColor = "#E8E9EA"
                            
                        Else
                            bgColor = "#FFFFFF"
                        End If
                %>
                <tr style='<%="background-color:"+ bgColor+";"%>'>
                    <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                            If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then
                                If rdItemCount = 0 Then
                             
                    %>
                    <td style="font-weight: bold; border: solid 1px gray;">
                        <div class="subHeading" style="text-align: center;">
                            <%= varWeeklyDT.Rows(rdCount).Item(rdItemCount)%></div>
                    </td>
                    <%Else%>
                    <td style="border: solid 1px gray;">
                        <div style="height: 225px; overflow: auto; margin: 5px;">
                            <%= varWeeklyDT.Rows(rdCount).Item(rdItemCount)%>
                        </div>
                    </td>
                    <%End If%>
                    <%Else%>
                    <td style="font-weight: bold; border: solid 1px gray;">
                        &nbsp;
                    </td>
                    <%End If%>
                    <%
                    Next%>
                </tr>
                <%  Next %>
            </tbody>
        </table>
    </div>
</asp:Panel>
<asp:Label ID="lbldispCalendar" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlAmendPopUpCalendar" runat="server" TargetControlID="lbldispCalendar"
    PopupControlID="pnlCalendar" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
    <Animations>
        <OnShown> 
            <ScriptAction Script="createFixedHeader(); focusCurrentAppointment();" />
        </OnShown>
    </Animations>
</asp:ModalPopupExtender>
<asp:Label ID="lbldispActivity" runat="server"></asp:Label>

<asp:Panel ID="pnlAmendAppointment" runat="server" BackColor="White" Width="470px"
    Height="500px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Amend Appointment</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseAmendApp" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <div style="width: 100%; height: 490px; overflow: hidden;">
        <div style="overflow-y: scroll; height: 470px; padding-top: 5px !important;">
            <asp:HiddenField ID="hdnAmendExpiryDate" runat="server" ClientIDMode="Static" />
            <table>
                <tr>
                    <td colspan="3" valign="top">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="3">
                        <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                            <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Appointment Type:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlInspectionType" runat="server" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Location:
                    </td>
                    <td align="right" valign="top" class="style2">
                        <span style="float: left; margin-top: -4px; vertical-align: top;">
                            <asp:ImageButton ID="lnkBtnAppDetails" runat="server" ImageUrl="../../images/phone.jpg"
                                BorderStyle="None" />
                        </span>
                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Date:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:CalendarExtender ID="calAmendDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                            PopupButtonID="imgAmendCalDate" PopupPosition="Right" TargetControlID="txtAmendDate"
                            TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:TextBox ID="txtAmendDate" runat="server" ClientIDMode="Static" onfocus="resetExpiryAlertCounter()"></asp:TextBox>
                        &nbsp;<img alt="Open Calendar" src="../../Images/calendar.png" id="imgAmendCalDate"
                            onclick="resetExpiryAlertCounter()" />
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Starts:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAppStartTime" runat="server" Width="100px" onClick="checkMExpiryDate(document.getElementById('hdnAmendExpiryDate'),document.getElementById('txtAmendDate'))"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Ends:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAppEndTime" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Operative:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlOperative" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Status:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Action:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Letter:
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="ddlLetter" runat="server" Width="200px">
                            <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" class="style5">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAmendUploadWindow()"
                            BackColor="White" />&nbsp;
                        <asp:Button ID="btnViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindow()"
                            BackColor="White" />
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Docs:
                    </td>
                    <td align="left" valign="top">
                        <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                            <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                <ItemTemplate>
                                    <table class="LetterDocInnerTable">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                    Visible="<%# False %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                    Visible="<%# False %>"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                    BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                                    OnClick="imgBtnRemoveLetterDoc_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <SelectedItemStyle BackColor="Gray" />
                            </asp:DataList>
                        </asp:Panel>
                    </td>
                </tr>
                <%-- <tr>
            <td align="left" valign="top" class="style5">
                Upload:
            </td>
            <td align="left" valign="top">
                <asp:Label ID="imgUploadProgress" runat="server" Style="display: none"> <img alt="" src="../../Images/uploading.gif" /></asp:Label>
                            <asp:AsyncFileUpload ID="asyncAmendDocUpload" runat="server" 
                    ErrorBackColor="Red" Height="26px" OnClientUploadComplete="uploadComplete" 
                    OnClientUploadError="uploadError" 
                    ThrobberID="imgUploadProgress" UploaderStyle="Traditional" Width="179px" />
                            </td>
            <td align="left" valign="top">
                &nbsp;
            </td>
        </tr>--%>
                <tr>
                    <td align="left" valign="top" class="style5">
                        Notes:
                        <%--Start - Changed By Aamir Waheed on April 22, 2013.
                            To Implement CR, Notes should not be mandatory when adding a new appointment
                            Action : Commented out Span
                            <span class="Required">*</span>
                            End - Changed By Aamir Waheed on April 22, 2013
                        --%>
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine" Width="246px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                    </td>
                    <td align="right" valign="top" style="text-align: right;">
                        <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;<asp:Button
                            ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
                    </td>
                    <td align="left" valign="top" style="text-align: left;">
                        <div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style5">
                    </td>
                    <td align="right" valign="top" style="text-align: right;">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="text-align: left;">
                        <div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAmendAppointment" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAmendAppointmentPopup" PopupControlID="pnlAmendAppointment"
    DropShadow="true" CancelControlID="btnCancelAction" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispAmendAppointmentPopup" runat="server"></asp:Label>
<asp:Panel ID="pnlAmendApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblTenantInfoAmendPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgbtn11" runat="server" Style="position: absolute; top: -10px;
                    right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            </td>
        </tr>
        <%--<tr>
            <td>
                Tenant:
            </td>
            <td>
                <asp:ImageButton ID="ImageButton1" runat="server" Style="position: absolute; top: -10px;
                    right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <asp:Label ID="lblFName" runat="server" Text='<%# Bind("FIRSTNAME") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Mobile:
            </td>
            <td>
                <asp:Label ID="lblMobileNo" runat="server" Text='<%# Bind("MOBILE") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Telephone:
            </td>
            <td>
                <asp:Label ID="lblTelNo" runat="server" Text='<%# Bind("TEL") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Email:
            </td>
            <td>
                <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
            </td>
        </tr>--%>
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAmenAptContactDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblAmendApointmentContactDetails" PopupControlID="pnlAmendApointmentContactDetails"
    DropShadow="true" CancelControlID="" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>

<%--Start - Tenant Info Box Add Appoint Calander--%>
<asp:Panel ID="pnlAmendCalanderTennatInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblAmendCalanderTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgbtnCloseTenantInfoAmendCalander" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />                    
            </td>
        </tr>        
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopupAmendCalanderTenantInfo" runat="server"
    DynamicServicePath="" Enabled="True" TargetControlID="lblAmendCalanderTennatInfo"
    PopupControlID="pnlAmendCalanderTennatInfo" DropShadow="true" CancelControlID=""
    BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--End - Tenant Info Box Add Appoint Calander--%>

<asp:Panel ID="EmailSmsPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgEmailSmsClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
    <asp:Label ID="lblStatusHeadingMessage" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
        <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right:10px;">
        <asp:Label ID="lblStatusDescriptionMessage" runat="server" Text=""></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
    <asp:Button ID="btnEmailSmsCancel" runat="server" Text="Cancel" Visible="true" BackColor="White" />
                <asp:Button ID="btnEmailSmsResend" runat="server" Text="Resend" Visible="false" BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="EmailSmsLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="EmailSmsPopUp" runat="server" TargetControlID="EmailSmsLabel"
    PopupControlID="EmailSmsPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgEmailSmsClose">
</asp:ModalPopupExtender>
<asp:Panel ID="PushNoticificationPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPushNoticificationClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
    <asp:Label ID="lblPushNoticificationStatusHeadingMessage" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblPushNoticificationStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
        <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right:10px;">
        <asp:Label ID="lblPushNoticificationStatusDescriptionMessage" runat="server" Text=""></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
    <asp:Button ID="btnPushNoticificationCancel" runat="server" Text="Cancel" Visible="true" BackColor="White" />
                <asp:Button ID="btnPushNoticificationResend" runat="server" Text="Resend" Visible="false" BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="PushNoticificationLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PushNoticificationPopUp" runat="server" TargetControlID="PushNoticificationLabel"
    PopupControlID="PushNoticificationPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgPushNoticificationClose">
</asp:ModalPopupExtender>

<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right:10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" style="padding-right:10px; font-weight:bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right:10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled." style="padding-right:10px;"></asp:Label>
    </div>
   
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    >
</asp:ModalPopupExtender>

<asp:Label ID="lblAmendApointmentContactDetails" runat="server"></asp:Label>
<asp:Label ID="lblAmendCalanderTennatInfo" runat="server" />
<asp:CheckBox ID="ckBoxRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />

    <asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
        Width="390px" Height="270px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
        ">
        <div style="width: 100%; font-weight: bold; padding-left: 10px;">
            Cancel</div>
        <div style="clear: both; height: 1px;">
        </div>
        <hr />
        <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        <div style="width: 100%; height: 490px; overflow: hidden;">
            <div style="height: 370px; padding-top: 10px !important; padding-left: 20px;">
                <table>
                    <tr>
                        <td align="left" class="style1">
                            <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlDescription" runat="server">
                                <asp:Label ID="lblDescription" runat="server" Text="Enter the reason for cancellation below:"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlCancelMessage" runat="server" Visible="False" HorizontalAlign="Center">
                                <asp:Label ID="lblCancelFaultsText" runat="server" Text="The appointment has been cancelled."
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="pnlSaveButton" runat="server" HorizontalAlign="Right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlCustomerMessage" runat="server" HorizontalAlign="Center" Visible="False">
                                <asp:LinkButton ID="lnkBtnClickHere" runat="server" Visible="True" OnClientClick="closeWindow()">Click here</asp:LinkButton>
                                &nbsp;<asp:Label ID="lblCustomerText" runat="server" Text="to return to the customers record"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
        Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
        DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
        BehaviorID="mdlPopUpCancelFaults">
    </asp:ModalPopupExtender>
    <asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>
