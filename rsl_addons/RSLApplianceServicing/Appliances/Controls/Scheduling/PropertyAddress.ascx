﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PropertyAddress.ascx.vb"
    Inherits="Appliances.PropertyAddress" %>
<style type="text/css">
    td
    {
        padding: 4px;
    }
    
    span-class
    {
        font-weight: normal;
    }
</style>
<asp:UpdatePanel ID="updPanelPropertyAddress" runat="server" UpdateMode="Conditional"
    style="margin-left: 5px">
    <ContentTemplate>
        <div style="margin: 0 0 0 0px">
            <div>
                <div style="margin-left: 3px; text-align: left;">
                    <asp:Label Style="font-weight: bold" ID="lblNextAvaialbleHeading" runat="server"
                        Text="Next available appointments for:"></asp:Label>
                    <asp:Label Style="font-weight: bold" ID="lblCustomerName" runat="server" Text=""></asp:Label>
                    <asp:Label Style="font-weight: bold" ID="lblStreetAddress" runat="server" Text=""></asp:Label>
                    <asp:Label Style="font-weight: bold" ID="lblComma" runat="server" Text=","></asp:Label>
                    <asp:Label Style="font-weight: bold" ID="lblPostCode" runat="server" Text=""></asp:Label>
                    <asp:Label Style="font-weight: bold" ID="lblTelNo" runat="server" Text=""></asp:Label>
                </div>
                <div visible="false" id="divBoilerInfo" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td valign="top">
                                <span style="font-weight: bold; position: relative; top: -1px">
                                    <asp:Label runat="server" ID="lblAppointmentTypeInfo"></asp:Label>
                                </span>
                            </td>
                            <td valign="top">
                                <div style="overflow: auto; max-height: 100px;">
                                    <table style="margin-top: -3px">
                                        <asp:Repeater ID="boilersInfoRpt" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label1" Text='<%# Eval("BoilerName") + ":" %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label Style="padding-left: 15px;" runat="server" ID="Label2" Text='<%# Eval("HeatingFuel") + " : " %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label3" Text='<%# Eval("BoilerType") + " : " %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="Label4" Text='<%# Eval("Manufacturer") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label Style="padding-left: 15px;" runat="server" ID="Label5" Text='<%# "    " + Eval("EXPIRYDATE") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
