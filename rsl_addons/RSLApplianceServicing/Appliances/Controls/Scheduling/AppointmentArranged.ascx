﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentArranged.ascx.vb"
    Inherits="Appliances.AppointmentArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="propertyAddressTag" TagName="PropertyAddress" Src="~/Controls/Scheduling/PropertyAddress.ascx" %>
<%@ Register TagPrefix="appointmentNotesTag" TagName="AppointmentNotes" Src="~/Controls/Scheduling/AppointmentNotes.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<script type="text/javascript">
    document.onload = function () {
        alert('here');
        var w;
        function uploadError(sender, args) {
            alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
        }

        function uploadComplete() {
            __doPostBack();
        }

        function callAlert(msg) {
            alert(msg);
        }
    }
        
</script>
<style type="text/css">
    .style1
    {
        width: 266px;
    }
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 229px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
    .Noticification
    {
        height: 20px;
        width: 20px;
    }
    .greyFont
    {
        color: Gray;
    }
    .yellowFont
    {
        color: Yellow;
    }
    .redFont
    {
        color: Red;
    }
    .greenFont
    {
        color: Green;
    }
    .dashboard th
    {
        background: #fff;
        border-bottom: 4px solid #8b8687;
    }
    .dashboard th a
    {
        color: #000 !important;
    }
    .dashboard th img
    {
        float: right;
    }
    .dashboard, .dashboard td
    {
        padding: 5px 5px 5px 5px !important;
    }
</style>
<div style="overflow: auto;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <cc1:PagingGridView ID="grdAppointmentArranged" runat="server" AutoGenerateColumns="False"
        AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
        CssClass="dashboard dashboard-border webgrid table table-responsive" EmptyDataText="No Records Found"
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="True" OnRowCreated="grdAppointmentArranged_RowCreated">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnApaTenantInfo" runat="server" CommandArgument='<%# Eval("TENANCYID") %>'
                        ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnApaTenantInfo_Click" BorderStyle="None"
                        Visible='<%# GeneralHelper.isPropertyStatusLet(Eval("PropertyStatus").ToString()) %>'
                        BorderWidth="0px" />
                    <asp:ImageButton ID="imgbtnAptNotes" runat="server" CommandArgument='<%# Eval("APPOINTMENTID") %>'
                        ImageUrl='<%# "~/Images/editCustomerNotes.png" %>' OnClick="imgbtnAptNotes_Click"
                        BorderStyle="None" Visible='<%# hideShowAppointmentNotesButton(Eval("Notes").ToString()) %>'
                        Width="18px" Height="18px" BorderWidth="0px" />
                </ItemTemplate>
                <ItemStyle Width="52px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" JSG No" SortExpression="JSGNUMBER">
                <ItemTemplate>
                    <asp:Label ID="lbljsgno" runat="server" Text='<%# "JSG"+ Eval("JSGNUMBER").ToString() %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Logged " SortExpression="LOGGEDDATE">
                <ItemTemplate>
                    <asp:Label ID="lblLogged" runat="server" Text='<%# Bind("LOGGEDDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                <ItemTemplate>
                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">--%>
            <asp:TemplateField HeaderText="Expiry Date" SortExpression="LGSRDate">
                <ItemTemplate>
                    <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Days" SortExpression="Days">
                <ItemTemplate>
                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("DAYS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fuel Type" SortExpression="Fuel">
                <ItemTemplate>
                    <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Engineer" SortExpression="ENGINEER">
                <ItemTemplate>
                    <asp:Label ID="lblEngineer" runat="server" Text='<%# Bind("ENGINEER") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment" SortExpression="AppointmentSortDate">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("APPOINTMENT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <div style="text-align: right">
                        <asp:ImageButton ID="imgbtnEmailStatus" runat="server" CommandArgument='<%# Eval("EmailDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EmailStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("EmailImagePath") %>' OnClick="imgbtnEmail_Click" BorderStyle="None"
                            CssClass="Noticification" Visible='<%# Eval("AppointmentType").ToString() = "Property"  %>'
                            BorderWidth="0px" />
                        <asp:ImageButton ID="imgbtnSmsStatus" runat="server" CommandArgument='<%# Eval("SmsDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("SmsStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("SmsImagePath") %>' OnClick="imgbtnSms_Click" BorderStyle="None"
                            CssClass="Noticification" Visible='<%# Eval("AppointmentType").ToString() = "Property"  %>'
                            BorderWidth="0px" />
                        <asp:ImageButton ID="imgbtnPushNoticificationStatus" runat="server" CommandArgument='<%# Eval("PushNoticificationDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("ASSIGNEDTO").ToString()+";"+Eval("ADDRESS").ToString()+";"+Eval("ApDate").ToString()+";"+Eval("APPOINTMENT").ToString()+";"+Eval("PushNoticificationStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("PushNoticificationImagePath") %>' OnClick="imgbtnPushNoticification_Click"
                            BorderStyle="None" CssClass="Noticification" BorderWidth="0px" />
                        <asp:ImageButton ID="imgBtnAppointmentArrangedArrow" runat="server" OnClick="imgBtnAppointmentArrangedArrow_Click"
                            BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("PROPERTYID").ToString()+";"+Eval("APPOINTMENTID").ToString()+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString()+";"+Eval("PatchId").toString()+";"+Eval("Telephone").ToString()+";"+Eval("NAME").ToString()+";"+Eval("CustomerId").ToString() + ";" + Eval("EMAIL").ToString() + ";" + Eval("HouseNumber").ToString() + ";" + Eval("ADDRESS1").ToString() + ";" + Eval("ADDRESS2").ToString() + ";" + Eval("ADDRESS3").ToString() + ";" + Eval("TOWNCITY").ToString() + ";" + Eval("COUNTY").ToString()+ ";" + Eval("AppointmentType").ToString() %>' />
                        <asp:ImageButton ID="btnCancelReportedFaults" runat="server" OnClick="btnCancelReportedFaults_Click"
                            BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/cross2.png" %>'
                            CommandArgument='<%# Eval("JournalId").ToString()+ ";" + Eval("AppointmentType").ToString() + ";" + Eval("APPOINTMENTID").ToString() + ";" + Eval("StatusTitle")%>'
                            ToolTip="Cancel Appointment" />
                    </div>
                </ItemTemplate>
                <ItemStyle Width="140px" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
    <%--Appointment Arrnaged Customer Contact Details-- START--%>
    <asp:Panel ID="pnlAppointmentArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfoApa" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnApaClose" runat="server" Style="position: absolute; top: -10px;
                        right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--Appointment Arrnaged Customer Contact Details-- END--%>
</div>
<asp:Label ID="lbldispActivity" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAppointmentArrangedPhone" runat="server" TargetControlID="lbldispActivity"
    PopupControlID="pnlAppointmentArrangedTenantInfo" Enabled="true" DropShadow="true"
    CancelControlID="imgBtnApaClose">
</asp:ModalPopupExtender>
<%--Available Appointments-- START--%>
<asp:Panel ID="pnlAvailalbleAppointments" runat="server" CssClass="pnlAvail-Appt">
    <div style="overflow: auto; max-width: 100%; max-height: 600px; overflow-x: hidden">
        <div>
            <asp:ImageButton ID="imgBtnCloseAvailableAppointments" runat="server" Style="position: absolute;
                top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        </div>
        <asp:UpdatePanel ID="updPanelAvailableOperatives" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="float: left; width: 100%; margin: 5px; text-align: left;">
                    <asp:Panel ID="pnlMessageOnPopUp" runat="server" Visible="false">
                        <asp:Label ID="lblMessageOnPopUp" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <div style="margin-left: 10px; float: left;">
                    <propertyAddressTag:PropertyAddress ID="webUserCtrlPropAddressAvailableAppt" runat="server"
                        MaxValue="10" MinValue="1" />
                </div>
                <div class="grid_avail_appt_filter" style="width: 99%; margin-top: 10px; font-weight: bold; margin-left: 12px">
                    <table style="background-color: #c1c1c1; width: 100%;">
                        <tr>
                            <td>
                                <asp:Label ID="lblInfo" runat="server"><b>Filter Available Operatives By:</b></asp:Label>
                            </td>
                            <td>
                                <div class="grid_avail_appt_patch">
                                    <b style='float: left; margin-top: 5px;'>Patch: </b>
                                    <asp:CheckBox ID="chkPatch" runat="server" AutoPostBack="true" OnCheckedChanged="chkPatch_CheckedChanged"
                                        Style='float: left; margin-top: 3px;' /></div>
                            </td>
                            <td>
                                <div class="grid_avail_appt_date">
                                    <b style='float: left; margin-top: 5px;'>Expiry Date:</b>
                                    <asp:CheckBox ID="chkDate" runat="server" AutoPostBack="true" OnCheckedChanged="chkExpiryDate_CheckedChanged"
                                        Style='float: left; margin-top: 3px;' /></div>
                            </td>
                            <td class="grid_avail_appt_date">
                                <b style='float: left; margin-top: 5px;'>From Date:</b>
                                <asp:TextBox ID="txtfromDate" runat="server" AutoPostBack="true" OnTextChanged="txtfromDate_TextChanged"
                                    Style='float: left; margin-top: 3px;'></asp:TextBox>
                                <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                    PopupPosition="Right" TargetControlID="txtfromDate" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td align="right">
                                <b style='float: left; margin-top: 5px;'>Certificate Expires in:</b>
                                <asp:Label Text="" ID="lblDefault" runat="server" Style='float: left; margin-top: 5px;' />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 260px; overflow: auto; width: 100%; margin-left: 5px">
                    &nbsp;<asp:GridView ID="grdAvailableAppointments" runat="server" AutoGenerateColumns="False"
                        CssClass="grid_avail_appt" ShowFooter="true" GridLines="None">
                        <Columns>
                            <asp:TemplateField HeaderText="Operative:">
                                <ItemTemplate>
                                    <asp:Label ID="lblOperative" runat="server" Text='<%# Eval("Operative:") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Patch:">
                                <ItemTemplate>
                                    <asp:Label ID="lblPatch" runat="server" Text='<%# Eval("Patch:") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date:">
                                <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%# Eval("AppointmentDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date:">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("AppointmentEndDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Distance(miles):">
                                <ItemTemplate>
                                    <asp:Label ID="lblDistance" runat="server" Text='<%# Eval("Distance:") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <FooterTemplate>
                                    <asp:Button ID="btnRefreshList" runat="server" Text="Refresh List" Visible='<%# hideShowRefreshListButton() %>'
                                        OnClick="btnRefreshList_Click" ClientIDMode="Static" />
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" runat="server" Text="Select" CommandArgument='<%# Eval("OperativeId").toString()+";"+Eval("Operative:")+";"+Eval("AppointmentStartTime")+";"+Eval("AppointmentEndTime")+";"+Eval("AppointmentDate").toString()+";"+Eval("AppointmentEndDate").toString() %>'
                                        OnClick="btnSelect_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BorderWidth="1px" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel runat="server" ClientIDMode="Static" CssClass="appointmentsMap" ID="aptAMap">
        </asp:Panel>
    </div>
</asp:Panel>
<asp:Label ID="lblDisplayAvailableAppointments" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopupAvailableAppointments" runat="server" TargetControlID="lblDisplayAvailableAppointments"
    PopupControlID="pnlAvailalbleAppointments" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt">
</asp:ModalPopupExtender>
<asp:Panel ID="EmailSmsPanel" runat="server" BackColor="White" Width="470px" Height="100px"
    Style="padding: 15px 5px; top: 20px; border: 1px solid black; top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgEmailSmsClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        <asp:Label ID="lblStatusHeadingMessage" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="lblStatusDescriptionMessage" runat="server" Text="" Style="padding-right: 10px;"></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
        <asp:Button ID="btnEmailSmsCancel" runat="server" Text="Cancel" Visible="true" BackColor="White" />
        <asp:Button ID="btnEmailSmsResend" runat="server" Text="Resend" Visible="false" BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="EmailSmsLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="EmailSmsPopUp" runat="server" TargetControlID="EmailSmsLabel"
    PopupControlID="EmailSmsPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgEmailSmsClose">
</asp:ModalPopupExtender>
<asp:Panel ID="PushNoticificationPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPushNoticificationClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        <asp:Label ID="lblPushNoticificationStatusHeadingMessage" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblPushNoticificationStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="lblPushNoticificationStatusDescriptionMessage" runat="server" Text=""></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
        <asp:Button ID="btnPushNoticificationCancel" runat="server" Text="Cancel" Visible="true"
            BackColor="White" />
        <asp:Button ID="btnPushNoticificationResend" runat="server" Text="Resend" Visible="false"
            BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="PushNoticificationLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PushNoticificationPopUp" runat="server" TargetControlID="PushNoticificationLabel"
    PopupControlID="PushNoticificationPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgPushNoticificationClose">
</asp:ModalPopupExtender>
<%--Available Appointments-- END--%>
<asp:Panel ID="pnlAmendAppointment" runat="server" BackColor="White" Width="470px"
    Height="500px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Amend Appointment</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseAmendApp" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <div style="width: 100%; height: 490px; overflow: hidden;">
        <div style="overflow-y: scroll; height: 470px; padding-top: 5px !important;">
            <asp:UpdatePanel ID="updPanelAddAppointmentPopup" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td colspan="3" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" colspan="3">
                                <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                                    <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Appointment Type:<span class="Required">*</span>
                            </td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblInspectionType" runat="server" Text="Appliance Servicing"></asp:Label>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Location:
                            </td>
                            <td align="right" valign="top" class="style2">
                                <span style="float: left; margin-top: -4px; vertical-align: top;">
                                    <asp:ImageButton ID="lnkBtnAppDetails" runat="server" ImageUrl="../../images/phone.jpg"
                                        BorderStyle="None" />
                                </span>
                                <asp:Label ID="lblLocation" runat="server"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Date:<span class="Required">*</span>
                            </td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblAppointmentDate" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Starts:
                            </td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblAppointmentStartTime" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Ends:
                            </td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblAppointmentEndTime" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Operative:
                            </td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Status:<span class="Required">*</span>
                            </td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Action:<span class="Required">*</span>
                            </td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True" Width="200px">
                                    <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Letter:
                            </td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlLetter" runat="server" Width="200px">
                                    <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" class="style5">
                                &nbsp;
                            </td>
                            <td align="left" valign="top">
                                <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAmendUploadWindow()"
                                    BackColor="White" />&nbsp;
                                <asp:Button ID="btnViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindow()"
                                    BackColor="White" />
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Docs:
                            </td>
                            <td align="left" valign="top">
                                <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                                    <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                        <ItemTemplate>
                                            <table class="LetterDocInnerTable">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                            Visible="<%# False %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                            Visible="<%# False %>"></asp:Label>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                            BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                                            OnClick="imgBtnRemoveLetterDoc_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <SelectedItemStyle BackColor="Gray" />
                                    </asp:DataList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style5">
                                Notes:
                                <%--To Implement CR, Notes should not be mandatory when adding a new appointment
                                --%>
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine" Width="246px"></asp:TextBox>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="add-apt-canel-save">
                <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
                <asp:Button ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Label ID="lblDispAmendAppointmentPopup" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAmendAppointment" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAmendAppointmentPopup" PopupControlID="pnlAmendAppointment"
    DropShadow="true" CancelControlID="btnCancelAction" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSuccessPopup" runat="server">
    <asp:ImageButton ID="imgBtnCancelSuccessPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <div class="apt-success-popup" style="width: 550px !Important; height: 150px; border: 1px solid black;">
        <%--        <propertyAddressTag:PropertyAddress ID="webUserCtrlPropAddressSuccessPopup" runat="server"
            MaxValue="10" MinValue="1" />--%>
        <asp:Panel ID="pnlSuccessAppointmentMessage" runat="server" CssClass="pnl-apt-success-msg"
            Style="border: none !important; padding-top: 50px !important">
            <asp:Label ID="lblSuccessAppointmentMessage" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </div>
</asp:Panel>
<asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="mdlSuccessPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlSuccessPopup"
    DropShadow="true" CancelControlID="lblDisplaySuccessPopup" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Appointment Notes Popup Start--%>
<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right: 10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" Style="padding-right: 10px;
            font-weight: bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled."
            Style="padding-right: 10px;"></asp:Label>
    </div>
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlAppointmentNotesPopup" runat="server">
    <appointmentNotesTag:AppointmentNotes ID="ucAppointmentNotes" runat="server" MaxValue="10"
        MinValue="1" />
</asp:Panel>
<asp:Label ID="lblDisplayAppointmentNotesPopup" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="mdlAppointmentNotesPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplayAppointmentNotesPopup" PopupControlID="pnlAppointmentNotesPopup"
    DropShadow="true" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Appointment Notes Popup End--%>
<asp:Panel ID="pnlAmendApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblTenantInfoAmendPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgbtnCloseAmendContactDetail" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Label ID="lblAmendApointmentContactDetails" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAmenAptContactDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblAmendApointmentContactDetails" PopupControlID="pnlAmendApointmentContactDetails"
    DropShadow="true" CancelControlID="imgbtnCloseAmendContactDetail" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Amend Appointment Contact Details Popup-- END--%>
<asp:CheckBox ID="ckBoxRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
    Width="390px" Height="270px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Cancel</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="width: 100%; height: 490px; overflow: hidden;">
        <div style="height: 370px; padding-top: 10px !important; padding-left: 20px;">
            <table>
                <tr>
                    <td align="left" class="style1">
                        <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                            <asp:Label ID="lblErrorMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlDescription" runat="server">
                            <asp:Label ID="lblDescription" runat="server" Text="Enter the reason for cancellation below:"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlCancelMessage" runat="server" Visible="False" HorizontalAlign="Center">
                            <asp:Label ID="lblCancelFaultsText" runat="server" Text="The appointment has been cancelled."
                                Font-Bold="True"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlSaveButton" runat="server" HorizontalAlign="Right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlCustomerMessage" runat="server" HorizontalAlign="Center" Visible="False">
                            <asp:LinkButton ID="lnkBtnClickHere" runat="server" Visible="True" OnClientClick="closeWindow()">Click here</asp:LinkButton>
                            &nbsp;<asp:Label ID="lblCustomerText" runat="server" Text="to return to the customers record"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
    DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
    BehaviorID="mdlPopUpCancelFaults">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>