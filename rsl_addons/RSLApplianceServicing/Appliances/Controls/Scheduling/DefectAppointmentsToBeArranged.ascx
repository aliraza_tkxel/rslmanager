﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectAppointmentsToBeArranged.ascx.vb"
    Inherits="Appliances.DefectAppointmentsToBeArranged" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="defect" TagName="defectmanegement" Src="~/Controls/DefectManagement/DefectManagement.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        $(function () {
            $(".showHideImage").click(function () {
                var currentElement = $(this);

                $(this).parentsUntil("tbody", "tr").next().toggle();

                var src = currentElement.attr("src");
                if (src == "../../Images/roundPlus.png")
                    src = src.replace("Plus", "Minus");
                else
                    src = src.replace("Minus", "Plus");
                currentElement.attr("src", src);
            });
        });

        function EndRequestHandler(sender, args) {

            $(function () {
                $(".showHideImage").click(function () {
                    var currentElement = $(this);

                    $(this).parentsUntil("tbody", "tr").next().toggle();

                    var src = currentElement.attr("src");
                    if (src.contains("roundPlus.png"))
                        src = src.replace("Plus", "Minus");
                    else
                        src = src.replace("Minus", "Plus");
                    currentElement.attr("src", src);
                });
            });
        }

    </script>
    <style type="text/css">
        .yourclass {
         text-align = left
        }
        .DefectsChildRow
        {
            display: none;
        }
        .grdDefectAppointmentsHeader
        {
            border-bottom: 1px Solid #B7B8BB;
        }
        .gridtd
        {
            padding : 2px !important;
            text-align:left !important;
        }
        .gridtdCenter
        {
            padding: 5px !important;
            text-align: center !important;
        }
    </style>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updPnlDefectsGrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc1:PagingGridView runat="server" ID="grdAppointmentToBeArranged" AutoGenerateColumns="False"
                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True" PageSize="30"
                ShowHeaderWhenEmpty="false" DataKeyNames="JournalId" AllowPaging="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="imgShow" runat="server" OnClientClick="return javascript:void(0);"
                                ImageUrl="~/Images/roundPlus.png" Style="border: 0px none; width: 18px; height: 18px;"
                                AlternateText="+" CssClass="showHideImage" />
                        </ItemTemplate>
                        <ItemStyle BackColor="White" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ref:" SortExpression="JournalId">
                        <ItemTemplate>
                            <asp:Label ID="JournalId" Font-Bold="false" Text='<%# Eval("JournalId") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="ADDRESS">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" Font-Bold="false" Text='<%# Eval("Address") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Scheme:" SortExpression="ADDRESS">
                        <ItemTemplate>
                            <asp:Label ID="lblScheme" Font-Bold="false" Text='<%# Eval("Scheme") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Block:" SortExpression="ADDRESS">
                        <ItemTemplate>
                            <asp:Label ID="lblBlock" Font-Bold="false" Text='<%# Eval("Block") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Post Code:" SortExpression="PostCode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostCode" Font-Bold="false" Text='<%# Eval("PostCode") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Capped?" SortExpression="isCapped">
                        <ItemTemplate>
                            <asp:Label ID="lblisCapped" Font-Bold="false" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isCapped")) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Heating:" SortExpression="isHeatingAvailable">
                        <ItemTemplate>
                            <asp:Label ID="lblisHeatingAvailable" Font-Bold="false" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isHeatingAvailable")) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Heaters:" SortExpression="isHeatersLeft">
                        <ItemTemplate>
                            <asp:Label ID="lblHeaters" Font-Bold="false" Text='<%# GeneralHelper.getYesNoStringFromValue(Eval("isHeatersLeft")) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recorded:" SortExpression="DefectDate">
                        <ItemTemplate>
                            <asp:Label ID="lblDefectDate" Font-Bold="false" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("DefectDate")) %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parts Due:" SortExpression="PartsDue">
                        <ItemTemplate>
                            <asp:Label ID="lblPartsDue" Font-Bold="false" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("PartsDue")) %>'
                                runat="server" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(GeneralHelper.getColourForPartsDue(Eval("PartsDue"))) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:Button ID="btnSchedule" Text="Schedule" runat="server" CommandArgument='<%#Eval("JournalId")%>'
                                OnClick="btnSchedule_Click"></asp:Button>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <tr class="DefectsChildRow" runat="server" enableviewstate="true">
                                <td colspan="11" style="background: #ffffff; padding-left: 60px;" runat="server">
                                    <asp:GridView ID="grdDefectsChild" runat="server" AutoGenerateColumns="false" ShowHeader="true"
                                        BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="1px" ForeColor="Black"
                                        GridLines="None" Width="100%" AllowSorting="false" AllowPaging="false" ShowHeaderWhenEmpty="true"
                                        Style="border: 1px Solid #B7B8BB;">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Appliance" SortExpression="Appliance" HeaderStyle-CssClass="gridtd" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppliance" Text='<%# Eval("Appliance") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Left" CssClass="gridtd" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Category" SortExpression="DefectCategory" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDefectCategory" Text='<%# Eval("DefectCategory") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="15%" HorizontalAlign="Left" CssClass="gridtd" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority:" SortExpression="DefectPriority" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDefectPriority" Text='<%# Eval("DefectPriority") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Left" CssClass="gridtd" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trade:" SortExpression="DefectTrade" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDefectTrade" Text='<%# Eval("DefectTrade") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" HorizontalAlign="Left" CssClass="gridtd" />
                                            </asp:TemplateField>
                                            <asp:TemplateField  HeaderText="2 Person"  SortExpression="isTwoPersonsJob" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblisTwoPersonsJob" Text='<%# GeneralHelper.getYesNoStringFromBooleanValue(Eval("isTwoPersonsJob")) %>'
                                                        runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="7%"  HorizontalAlign="Center" CssClass="gridtdCenter" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Disconnected?" SortExpression="isApplianceDisconnected" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblisApplianceDisconnected" Text='<%# GeneralHelper.getYesNoStringFromBooleanValue(Eval("isApplianceDisconnected")) %>'
                                                        runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" HorizontalAlign="Center" CssClass="gridtdCenter" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Appointment:" SortExpression="AppointmentDate" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppointmentDate" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("AppointmentDate"), "dd/MM/yyyy HH:mm", "-") %>'
                                                        runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Parts Due:" SortExpression="DefectPartsDue" HeaderStyle-CssClass="gridtd">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPartsDue" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("DefectPartsDue")) %>'
                                                        runat="server" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(GeneralHelper.getColourForPartsDue(Eval("DefectPartsDue"))) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnDefectDetail" ImageUrl="~/Images/aero.png" AlternateText=">"
                                                        runat="server" OnClick="showDefectDetail" BorderStyle="None" CommandArgument='<%# "" + Eval("DefectId").toString() + "," + Eval("AppointmentType") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#DDDDDD" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle Font-Bold="true" />
                <AlternatingRowStyle BackColor="#DCDCDC" />
                <HeaderStyle CssClass="grdDefectAppointmentsHeader" />
                <%--<PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="10" />
                <PagerStyle BackColor="White" />--%>
            </cc1:PagingGridView>
            <div align="center">
                <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                    vertical-align: middle; float: left; width: 100%;">
                    <table style="width: 55%;">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                        Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                        Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    Page:&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                    of&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- Defect Detail POPUP - Start --%>
    <asp:Panel ID="pnlAddDefect" runat="server" BackColor="White" Width="530px" Style="min-height: 632px"
        BorderColor="Black" BorderStyle="Outset" BorderWidth="1px">
        <defect:defectmanegement runat="server" ID="ucDefectManagement" />
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mdlPoPUpAddDefect" runat="server" Enabled="True"
        TargetControlID="lblDispDefect" PopupControlID="pnlAddDefect" DropShadow="true">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblDispDefect" runat="server"></asp:Label>
    <%-- Defect Detail POPUP - End --%>
    <%-- RISK POPUP - Start --%>
    <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="mdlPopupRisk" runat="server" TargetControlID="btnHidden3"
        PopupControlID="pnlRiskAndVulnerability" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlRiskAndVulnerability" runat="server" BackColor="White" Width="350px"
        Style="font-weight: normal; font-size: 13px; padding: 10px;">
        <div style="width: 100%; font-weight: bold;">
            <b>Risk and Vulnerability Info </b>
        </div>
        <div style="clear: both; height: 1px;">
        </div>
        <hr />
        <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
            <asp:Label ID="lblErrorMessage" runat="server">
            </asp:Label>
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlRisks" runat="server" Visible="false">
            The customer has the following Risk
            <br />
            recorded against their current details
            <br />
            <br />
            <asp:GridView ID="grdRisks" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATDESC") %>'></asp:Label>
                            <asp:Label ID="separator" runat="server" Text=' - '></asp:Label>
                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SUBCATDESC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlVulnerability" runat="server" Visible="false">
            The customer has the following Vulnerability
            <br />
            recorded against their current details
            <br />
            <br />
            <asp:GridView ID="grdVulnerability" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATDESC") %>'></asp:Label>
                            <asp:Label ID="separator" runat="server" Text=' - '></asp:Label>
                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SUBCATDESC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="pnlConfirmButton" runat="server" HorizontalAlign="Right">
            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_click" />
        </asp:Panel>
    </asp:Panel>
    <%-- RISK POPUP - End --%>
</div>
