﻿Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class DefectAppointmentsArranged
    Inherits UserControlBase
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "defectId", 1, 30)
#Region "Events Handling"

#Region "User Control Events Handling"
    'Please put - Events Like Page Load in this region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub

#End Region

#Region "Server Controls Events Handling"

#Region "Grid View Events Handling "

#Region "Grid Appointment To Be Arranged Paging Index Changing"

    Private Sub grdAppointmentArranged_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentArranged.PageIndexChanging
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = e.NewPageIndex + 1
        setPageSortBo(objPageSortBo)
        getAndBindAppointmentsArrangedList()
    End Sub

#End Region

#Region "Grid Appointment Arranged Sorting"

    Private Sub grdAppointmentArranged_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentArranged.Sorting
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = 1
        If (objPageSortBo.SortExpression = e.SortExpression) Then
            objPageSortBo.setSortDirection()
        End If
        objPageSortBo.SortExpression = e.SortExpression
        setPageSortBo(objPageSortBo)
        getAndBindAppointmentsArrangedList()
    End Sub

#End Region

#End Region

#Region "Image Button Show Defect Job Sheet Click"

    Protected Sub showDefectJobSheet(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim imgButton = CType(sender, ImageButton)
        Response.Redirect(String.Format("{0}?defectId={1}", PathConstants.DefectJobSheet, imgButton.CommandArgument))
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            getAndBindAppointmentsArrangedList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region
#End Region

#End Region

#Region "Functions"

#Region "Populate Defects Appointments Arranged list"

    Public Sub populateAppointmentArrangedList(ByVal searchText As String)
        Dim objPageSortBo As PageSortBO = getPageSortBo()
        objPageSortBo.PageNumber = 1
        setPageSortBo(objPageSortBo)
        setSearchText(searchText)

        getAndBindAppointmentsArrangedList()
    End Sub

#End Region

#Region "Get and Bind Appointments To be Arranged List"

    Private Sub getAndBindAppointmentsArrangedList()
        Dim resultDataSet As New DataSet()
        Dim totalCount As Integer = 0
        Dim defectsBL As New DefectsBL()
        objPageSortBo = getPageSortBo()
        totalCount = defectsBL.getAppointmentArrangedList(resultDataSet, getSearchText(), objPageSortBo)
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        grdAppointmentArranged.VirtualItemCount = totalCount
        grdAppointmentArranged.DataSource = resultDataSet
        grdAppointmentArranged.DataBind()
        setPageSortBo(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        If totalCount = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

    End Sub

#End Region

#End Region

#Region "View State Function"

#Region "Page Sort BO View State Function"

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "defectId", 1, 30)
        If Not IsNothing(ViewState(ViewStateConstants.PageSortBo)) Then
            objPageSortBo = CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
        End If
        Return objPageSortBo
    End Function
#End Region

#Region "Remove Page Sort Bo"
    Protected Sub removePageSortBo()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#End Region

#Region "Search Text View State Function"

#Region "get Search Text"
    Protected Function getSearchText() As String
        Dim searchText As String = String.Empty
        If Not IsNothing(ViewState(ViewStateConstants.AptbaSearchString)) Then
            searchText = CType(ViewState(ViewStateConstants.AptbaSearchString), String)
        End If
        Return searchText
    End Function
#End Region

#Region "remove Search Text"
    Protected Sub removeSearchText()
        ViewState.Remove(ViewStateConstants.AptaSearchString)
    End Sub
#End Region

#Region "set Search Text"
    Protected Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptbaSearchString) = searchText
        End If
    End Sub
#End Region

#End Region

#End Region

End Class