﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class OilAppointmentArranged
    Inherits UserControlBase

#Region "Delegates"
    Public Delegate Sub AptbaIn56DaysDelegate(ByVal isDueWithIn56Days As Boolean)
#End Region

#Region "Attributes"

    'Calendar Variables
    Public varWeeklyDT As DataTable = New DataTable()
    'Appointment Arranged Variables
    Dim resultDataSet As DataSet = New DataSet()
    Dim resultDataTable As New DataTable
    Dim check56Days As Boolean = New Boolean
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Dim objOilSchedulingBL As OilSchedulingBL = New OilSchedulingBL()
    Public Event onAptbaIn56DaysEvent As AptbaIn56DaysDelegate
    Dim dsAppointments As DataSet = New DataSet()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "ApDate", 1, 30)

    Private _isError As Boolean
    Public Property isError() As Boolean
        Get
            Return _isError
        End Get
        Set(ByVal value As Boolean)
            _isError = value
        End Set
    End Property

#End Region

#Region "Events"
#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region
#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            AddHandler ucAppointmentNotes.CloseButtonClicked, AddressOf btnClose_Click
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Appointment Arranged Grid - Events"

#Region "img btn Aptba TenantInfo Click"

    Protected Sub imgbtnApaTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                Me.setTenantsInfo(dstenantsInfo, tblTenantInfoApa)

                Me.mdlPopUpAppointmentArrangedPhone.Show()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "img btn Email Click"

    Protected Sub imgbtnEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim emailDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = emailDescription(0)
            lblStatusHeadingDescription.Text = emailDescription(3)
            lblStatusHeadingMessage.Text = UserMessageConstants.EmailHeading
            If emailDescription(3) = UserMessageConstants.EmailNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not emailDescription(1) = "" And Not IsNothing(emailDescription(1)) Then
                btnEmailSmsResend.CommandArgument = emailDescription(1) + ";" + emailDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not emailDescription(0) = "" Then
                If emailDescription(0) = UserMessageConstants.EmailDescriptionSending Or emailDescription(0) = UserMessageConstants.EmailDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "img btn Sms Click"

    Protected Sub imgbtnSms_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim smsDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = smsDescription(0)
            lblStatusHeadingMessage.Text = UserMessageConstants.SmsHeading
            lblStatusHeadingDescription.Text = smsDescription(3)
            If smsDescription(3) = UserMessageConstants.SmsNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not smsDescription(1) = "" And Not IsNothing(smsDescription(1)) Then
                btnEmailSmsResend.CommandArgument = smsDescription(1) + ";" + smsDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not smsDescription(0) = "" Then
                If smsDescription(0) = UserMessageConstants.SmsDescriptionSending Or smsDescription(0) = UserMessageConstants.SmsDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region
#Region "img btn Push Noticifications Click"

    Protected Sub imgbtnPushNoticification_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim pushnoticificationDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblPushNoticificationStatusDescriptionMessage.Text = pushnoticificationDescription(0)
            lblPushNoticificationStatusHeadingMessage.Text = UserMessageConstants.PushNoticificationHeading
            lblPushNoticificationStatusHeadingDescription.Text = pushnoticificationDescription(6)
            If pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationNotSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greyFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSending Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSendingFailed Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "redFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not pushnoticificationDescription(1) = "" And Not IsNothing(pushnoticificationDescription(1)) Then
                btnPushNoticificationResend.CommandArgument = pushnoticificationDescription(1) + ";" + pushnoticificationDescription(2) + ";" + pushnoticificationDescription(3) + ";" + pushnoticificationDescription(4) + ";" + pushnoticificationDescription(5)
            Else
                btnPushNoticificationResend.CommandArgument = ""
            End If
            If Not pushnoticificationDescription(0) = "" Then
                If pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSending Or pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSent Then
                    btnPushNoticificationResend.Visible = False
                Else
                    btnPushNoticificationResend.Visible = True
                End If
            Else
                btnPushNoticificationResend.Visible = False
            End If
            Me.PushNoticificationPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentArranged.PageIndexChanging
        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppointmentArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentArrangedGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub grdAppointmentArranged_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBo()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "img btn Appointment notes Click"

    Protected Sub imgbtnAptNotes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btnNotes As ImageButton = CType(sender, ImageButton)
            Dim appointmentId As Integer = CType(btnNotes.CommandArgument, Integer)
            ucAppointmentNotes.loadAppointmentData(appointmentId)
            mdlAppointmentNotesPopup.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "btn Close Click"
    Protected Sub btnClose_Click()
        Try
            mdlAppointmentNotesPopup.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdAppointmentArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentArranged.Sorting
        Try
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdAppointmentArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Appointment Arranged Arrow Click"
    Protected Sub imgBtnAppointmentArrangedArrow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim btnImage As ImageButton = CType(sender, ImageButton)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        Dim pId As String = commandParams(4)
        Dim appointmentType As String = commandParams(21)

        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False
        Dim locked As Integer = objSchedulingBL.GetArrangedSchedulingPropertyStatus(commandParams(6), isTimeExceeded, isScheduled, appointmentType)
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandParams(6), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandParams(6), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        End If
        If criticalsection = 1 Then
            Try
                If Not commandParams(13) = "" And Not IsNothing(commandParams(13)) Then
                    Dim customerId As Integer = Convert.ToInt32(commandParams(13))
                    Dim riskVulnerability As String = getCustomerRiskVulnerability(customerId) 'index -13 : customerId
                    If Not riskVulnerability.Equals("") Then
                        Dim script As String = "callAlert('" + riskVulnerability + "')"
                        ScriptManager.RegisterStartupScript(Page, Page.GetType, "radalert", script, True)
                        PropertyScheduledPopUp.Hide()
                    End If
                End If
            Catch ex As Exception
                Throw New Exception("Customer Id is Invalid or Null")
            End Try
            Try
                uiMessageHelper.IsError = False

                Dim btn As ImageButton = CType(sender, ImageButton)
                Dim commandArgumentParams() As String = CType(btn.CommandArgument, String).Split(";")

                If IsNothing(SessionManager.getApplianceServicingId()) Or SessionManager.getApplianceServicingId() = 0 Then
                    Dim userBl As UsersBL = New UsersBL()
                    Dim inspectionTypeDs As DataSet = New DataSet()
                    userBl.getInspectionTypesByDesc(inspectionTypeDs, "Appliance Servicing")
                    Dim applianceServicingId As Integer = inspectionTypeDs.Tables(0).Rows(0).Item("InspectionTypeId")
                    SessionManager.setApplianceServicingId(applianceServicingId)
                End If

                'set the property id in session so that it 'll be used to open the print letter window
                SessionManager.setPropertyIdForEditLetter(commandArgumentParams(4))

                'fill up the property scheduling bo
                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                selPropSchedulingBo.PropertyAddress = commandArgumentParams(0)
                selPropSchedulingBo.PostCode = commandArgumentParams(1)
                'tenancy id can be null for void properties
                If (String.IsNullOrEmpty(commandArgumentParams(2)) = True) Then
                    selPropSchedulingBo.TenancyId = Nothing
                Else
                    selPropSchedulingBo.TenancyId = CType(commandArgumentParams(2), Integer)
                End If
                'There are some properties which does not have any certificate expiry date for that this check is implemented
                'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
                If (String.IsNullOrEmpty(commandArgumentParams(3)) = True) Then
                    selPropSchedulingBo.CertificateExpiryDate = Nothing
                Else
                    selPropSchedulingBo.CertificateExpiryDate = CType(commandArgumentParams(3), Date)
                End If
                selPropSchedulingBo.PropertyId = commandArgumentParams(4)
                selPropSchedulingBo.AppointmentId = CType(commandArgumentParams(5), Integer)
                selPropSchedulingBo.JournalId = CType(commandArgumentParams(6), Integer)
                selPropSchedulingBo.ExpiryInDays = commandArgumentParams(7)
                selPropSchedulingBo.PropertyStatus = commandArgumentParams(9)
                selPropSchedulingBo.PatchId = CType(commandArgumentParams(10), Integer)
                selPropSchedulingBo.Telephone = commandArgumentParams(11)
                selPropSchedulingBo.CustomerName = commandArgumentParams(12)

                selPropSchedulingBo.CustomerEmail = commandArgumentParams(14)
                selPropSchedulingBo.HouseNumber = commandArgumentParams(15)
                selPropSchedulingBo.Address1 = commandArgumentParams(16)
                selPropSchedulingBo.Address2 = commandArgumentParams(17)
                selPropSchedulingBo.Address3 = commandArgumentParams(18)
                selPropSchedulingBo.TownCity = commandArgumentParams(19)
                selPropSchedulingBo.County = commandArgumentParams(20)
                selPropSchedulingBo.AppointmentType = commandArgumentParams(21)

                selPropSchedulingBo.Duration = commandArgumentParams(22)

                'remove the proeprty scheduling bo from session if already exist 
                'this is just to avoid the overlapping of sesssion values between different properties
                SessionManager.RemoveSelectedPropertySchedulingBo()
                SessionManager.RemovePropertyDurationDict()
                SessionManager.RemoveAlternativeSchedulingHeaderBoList()

                'now set the session with selected property values
                SessionManager.SetSelectedPropertySchedulingBo(selPropSchedulingBo)

                Response.Redirect(PathConstants.OilArrangedAppointments + "?jourId=" + commandArgumentParams(6) + "&apptId=" + commandArgumentParams(5))

            Catch ex As ThreadAbortException
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try
        Else
            PropertyScheduledPopUp.Show()
        End If
    End Sub
#End Region

#End Region
#Region "Risk & Vulnerability of Customer"
    Private Function getCustomerRiskVulnerability(ByVal customerId As Integer)

        Dim schedulingBL As SchedulingBL = New SchedulingBL()
        Dim riskVulnerabilityResult As DataSet = New DataSet()
        Return schedulingBL.getCustomerRiskVulnerability(riskVulnerabilityResult, customerId)

    End Function
#End Region

#End Region

#Region "Function"

#Region "Appointment Arranged Grid - Functions"
#Region "search Appointment Arranged"
    Public Sub searchAppointmentArranged(ByRef check56Days As Boolean, ByRef searchText As String)
        Try

            Me.setSearchText(searchText)
            Me.setCheck56Days(check56Days)
            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Cancel Reported Faults Click Event"

    Protected Sub btnCancelReportedFaults_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim objCancelAppointment = New AppointmentCancellationBO()
            Dim objCancelAppointmentForSchemeBlock = New AppointmentCancellationForSchemeBlockBO()
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim commandParams() As String = CType(imgBtn.CommandArgument, String).Split(";")

            Dim journalId As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)
            Dim appointmentId As String = commandParams(2)
            Dim propertyStatus As String = commandParams(3)

            objCancelAppointment.FaultsList = journalId
            objCancelAppointmentForSchemeBlock.AppointmentType = appointmentType
            objCancelAppointmentForSchemeBlock.AppointmentId = appointmentId
            SessionManager.setConfirmedGasAppointment(objCancelAppointment)
            SessionManager.setAppointmentCancellationForSchemeBlock(objCancelAppointmentForSchemeBlock)

            pnlErrorMessage.Visible = False
            pnlCancelMessage.Visible = False
            pnlCustomerMessage.Visible = False
            pnlSaveButton.Visible = True
            txtBoxDescription.Text = ""
            txtBoxDescription.Enabled = True

            mdlPopUpCancelFaults.Show()
            If (propertyStatus.Contains("Cancelled")) Then
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            CancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Cancel Faults"

    Private Sub CancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As AppointmentCancellationBO = New AppointmentCancellationBO()
            resultDataSet = SessionManager.getConfirmedGasAppointment()

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim faultsList As StringBuilder = New StringBuilder()
                faultsList.Append(resultDataSet.FaultsList.ToString())
                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                Dim appointmentCancellationForSchemeBlockBo As New AppointmentCancellationForSchemeBlockBO()
                appointmentCancellationForSchemeBlockBo.Notes = txtBoxDescription.Text
                appointmentCancellationForSchemeBlockBo.userId = SessionManager.getAppServicingUserId()
                appointmentCancellationForSchemeBlockBo.AppointmentType = SessionManager.getAppointmentCancellationForSchemeBlock().AppointmentType
                appointmentCancellationForSchemeBlockBo.AppointmentId = SessionManager.getAppointmentCancellationForSchemeBlock().AppointmentId

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text
                objAppointmentCancellationBO.userId = SessionManager.getAppServicingUserId()

                Dim isCanceled As Integer
                'If (Not appointmentCancellationForSchemeBlockBo.AppointmentType.Contains("Property")) Then
                '    isCanceled = objSchedulingBL.saveGasServicingCancellationForSchemeBlock(appointmentCancellationForSchemeBlockBo)
                'Else
                isCanceled = objSchedulingBL.saveGasServicingCancellation(objAppointmentCancellationBO)
                'End If

                If IsDBNull(isCanceled) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False
                    pnlCustomerMessage.Visible = False
                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    SessionManager.removeConfirmedGasAppointment()
                    SessionManager.removeAppointmentCancellationForSchemeBlock()
                    mdlPopUpCancelFaults.Hide()
                    populateAppointmentArrangedGrid()
                    uiMessageHelper.Message = UserMessageConstants.FaultCancelled
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, False)
                End If

            End If

        End If

    End Sub

#End Region

#Region "populate Appointment ArrangedGrid "

    Public Sub populateAppointmentArrangedGrid()
        Dim totalCount As Integer = 0
        Dim status As String = String.Empty
        Dim schemeId As Integer = -1
        Dim patchId As Integer = -1

        If Request.QueryString(PathConstants.Status) IsNot Nothing AndAlso Request.QueryString(PathConstants.Status) <> String.Empty Then
            If Request.QueryString(PathConstants.Status).Equals(PathConstants.NoEntry) Then
                status = ApplicationConstants.noEntry
            ElseIf Request.QueryString(PathConstants.Status).Equals(PathConstants.Aborted) Then
                status = ApplicationConstants.aborted
            End If
            If Request.QueryString(PathConstants.SchemeId) IsNot Nothing AndAlso Request.QueryString(PathConstants.SchemeId) <> String.Empty Then
                schemeId = Convert.ToInt32(Request.QueryString(PathConstants.SchemeId))
            End If
            If Request.QueryString(PathConstants.PatchId) IsNot Nothing AndAlso Request.QueryString(PathConstants.PatchId) <> String.Empty Then
                patchId = Convert.ToInt32(Request.QueryString(PathConstants.PatchId))
            End If
        End If
        totalCount = objOilSchedulingBL.GetOilAppointmentsArrangedList(resultDataSet, Me.getCheck56Days(), getSearchText(), objPageSortBo, patchId, schemeId, -1)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAppointmentArranged.Visible = False
        Else
            grdAppointmentArranged.Visible = True
            ViewState.Add(ViewStateConstants.AptaDataSet, resultDataSet)
            grdAppointmentArranged.VirtualItemCount = totalCount
            grdAppointmentArranged.DataSource = resultDataSet
            grdAppointmentArranged.DataBind()
        End If
        setVirtualItemCountViewState(totalCount)
        Me.setResultDataSetViewState(resultDataSet)
    End Sub
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
#End Region

#Region "Sort Grid"
    Protected Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppointmentArrangedGrid()
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppointmentArranged.DataSource = dvSortedView
            grdAppointmentArranged.DataBind()
        End If

        ''''''''''''old Code''''''''''''
        'Dim resultDataSet As DataSet = New DataSet()
        'Dim resultDataTable As New DataTable

        ''get the dataset that was stored in view state
        'resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        ''find the table in dataset
        'resultDataTable = resultDataSet.Tables(0)

        'resultDataTable.DefaultView.Sort = Me.sortExpression + " " + Me.sortDirection
        'grdAppointmentArranged.DataSource = resultDataTable
        'grdAppointmentArranged.DataBind()

    End Sub
#End Region

#Region "set Search Text"
    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptaSearchString) = searchText
        End If
    End Sub
#End Region

#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptaSearchString), String)
        End If
    End Function
#End Region

#Region "set Check 56 Days"
    Public Sub setCheck56Days(ByVal checkBoxVal As Boolean)
        ViewState(ViewStateConstants.AptaCheck56Days) = checkBoxVal
    End Sub
#End Region

#Region "get Check 56 Days"
    Public Function getCheck56Days() As Boolean
        If (IsNothing(ViewState(ViewStateConstants.AptaCheck56Days))) Then
            Return True
        Else
            Return CType(ViewState(ViewStateConstants.AptaCheck56Days), Boolean)
        End If
    End Function
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#End Region

#Region "Available Appointments - Intelligent Scheduling Principal - Functions"

#Region "hide Show Notes Button"
    ''' <summary>
    ''' hide Show Notes Button
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowAppointmentNotesButton(ByVal notes As String)
        If notes.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = getPageSortBo()

        If grdAppointmentArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentArranged.PageCount Then
            grdAppointmentArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentArranged.DataSource = getResultDataSetViewState()
            grdAppointmentArranged.DataBind()
        End If

    End Sub

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region
#End Region

End Class