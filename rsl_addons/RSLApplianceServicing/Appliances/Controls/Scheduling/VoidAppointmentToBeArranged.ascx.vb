﻿Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing
Imports Appliances.FuelScheduling
Imports System.IO
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Globalization
Imports System.Net.Mail
Imports System.Net
Imports System.Threading
Imports AjaxControlToolkit
Imports System.DateTime

Public Class VoidAppointmentToBeArranged
    Inherits UserControlBase

#Region "Attributes"
    'Calendar Variables
    Public addAppointmentWeeklyDT As DataTable = New DataTable()
    'Appointment to be Arranged Variables
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()

    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Termination", 1, 30)

#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub




#Region "search Appointment To Be Arranged"
    Public Sub searchAppointmentToBeArranged(ByRef searchText As String)
        Try
            Me.setSearchText(searchText)
            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region



#Region "populate Appointment To Be ArrangedGrid "

    Public Sub populateAppointmentToBeArrangedGrid()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0

        totalCount = objSchedulingBL.getVoidAppointmentToBeArrangedList(resultDataSet, getSearchText(), Me.getPageSortBo())

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdVoidAppointmentToBeArranged.Visible = False
            pnlPagination.Visible = False
        Else
            grdVoidAppointmentToBeArranged.Visible = True
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            pnlPagination.Visible = True
            ViewState.Add(ViewStateConstants.AptbaDataSet, resultDataSet)
            grdVoidAppointmentToBeArranged.VirtualItemCount = totalCount

            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
            grdVoidAppointmentToBeArranged.DataSource = resultDataSet
            grdVoidAppointmentToBeArranged.DataBind()
            SessionManager.setVoidToBeArrangedList(resultDataSet.Tables(0))
            setPageSortBo(objPageSortBo)

            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
            If totalCount = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If


            'dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill)

        End If

    End Sub
#End Region


#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdVoidAppointmentToBeArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdVoidAppointmentToBeArranged.Sorting
        Try
            'get the sort expression
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdVoidAppointmentToBeArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.populateAppointmentToBeArrangedGrid()

            '''''''''''''''''''''''''''''''''''''''''' old code '''''''''''''''''''''' 
            'ViewState(ViewStateConstants.SortDirection) = e.SortDirection
            'Me.sortExpression = e.SortExpression
            'Me.setSortDirection()
            'Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region


#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVoidAppointmentToBeArranged.PageIndexChanging
        Try

            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdVoidAppointmentToBeArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentToBeArrangedGrid()

            ''''''old code '''''''''''
            'Dim resultDataSet As DataSet = New DataSet()
            'resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
            'Me.grdAppointmentToBeArranged.PageIndex = e.NewPageIndex
            'Me.grdAppointmentToBeArranged.SelectedIndex = -1
            'Me.grdAppointmentToBeArranged.DataSource = resultDataSet
            'Me.grdAppointmentToBeArranged.DataBind()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region


#Region "img Btn Available Appointment Slots Click"
    ''' <summary>
    ''' This is event handler for arrow sign in appointment to be arranged grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub imgBtnAvailableAppointmentSlots_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnImage As LinkButton = CType(sender, LinkButton)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        Dim pId As String = commandParams(4)
        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False
        Dim locked As Integer = objSchedulingBL.getPropertyschedulestatus(pId, isTimeExceeded, isScheduled, "Property")
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        End If
        If criticalsection = 1 Then
            Try
                pnlAvailalbleAppointments.Visible = False
                If Not commandParams(12) = "" And Not IsNothing(commandParams(12)) Then
                    Dim customerId As Integer = Convert.ToInt32(commandParams(12))
                    Dim riskVulnerability As String = getCustomerRiskVulnerability(customerId) 'index -12 : customerId
                    If Not riskVulnerability.Equals("") Then
                        Dim script As String = "callAlert('" + riskVulnerability + "')"
                        ScriptManager.RegisterStartupScript(Page, Page.GetType, "radalert", script, True)
                    End If
                    pnlAvailalbleAppointments.Visible = True
                Else
                    pnlAvailalbleAppointments.Visible = True
                End If
            Catch ex As Exception

            End Try

            Try
                uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                uiMessageHelper.IsError = False
                'Remove the existing appointments from the session, to show new pins of map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                removeMarkersJavaScript()
                txtfromDate.Text = String.Empty
                Dim operativeDs As DataSet = New DataSet()
                Dim btn As LinkButton = CType(sender, LinkButton)
                Dim commandArgumentParams() As String = CType(btn.CommandArgument, String).Split(";")
                'below mention sequence is expected in command argument parameters
                '0 index - address
                '1 index - post code
                '2 index - tenancy id
                '3 index - expiry date
                '4 index - property id            
                '5 index - journal id
                '6 index - days
                '7 index - 0
                '8 index - property status
                '9 index - patch id
                '10 index - telephone
                '11 index - name
                '12 index - CustomerId
                '13 index - Termination
                'get the avialable operatives, their appointments and leaves
                Me.getAvailableOperatives(operativeDs)

                If IsNothing(SessionManager.getApplianceServicingId()) Or SessionManager.getApplianceServicingId() = 0 Then
                    Dim userBl As UsersBL = New UsersBL()
                    Dim inspectionTypeDs As DataSet = New DataSet()
                    userBl.getInspectionTypesByDesc(inspectionTypeDs, "Appliance Servicing")
                    Dim applianceServicingId As Integer = inspectionTypeDs.Tables(0).Rows(0).Item("InspectionTypeId")
                    SessionManager.setApplianceServicingId(applianceServicingId)
                End If

                'set the property id in session so that it 'll be used to open the print letter window
                SessionManager.setPropertyIdForEditLetter(commandArgumentParams(4))
                SessionManager.setTerminationDate(commandArgumentParams(13))

                'fill up the property scheduling bo
                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                selPropSchedulingBo.PropertyAddress = commandArgumentParams(0)
                selPropSchedulingBo.PostCode = commandArgumentParams(1)
                'tenancy id can be null for void properties
                If (String.IsNullOrEmpty(commandArgumentParams(2)) = True) Then
                    selPropSchedulingBo.TenancyId = Nothing
                Else
                    selPropSchedulingBo.TenancyId = CType(commandArgumentParams(2), Integer)
                End If

                'There are some properties which does not have any certificate expiry date for that this check is implemented
                'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
                If (String.IsNullOrEmpty(commandArgumentParams(3)) = True) Then
                    selPropSchedulingBo.CertificateExpiryDate = Nothing
                Else
                    selPropSchedulingBo.CertificateExpiryDate = CType(commandArgumentParams(3), Date)
                End If
                selPropSchedulingBo.PropertyId = commandArgumentParams(4)
                selPropSchedulingBo.JournalId = CType(commandArgumentParams(5), Integer)
                selPropSchedulingBo.ExpiryInDays = commandArgumentParams(6)
                selPropSchedulingBo.PropertyStatus = commandArgumentParams(8)
                selPropSchedulingBo.PatchId = CType(commandArgumentParams(9), Integer)
                selPropSchedulingBo.Telephone = commandArgumentParams(10)
                selPropSchedulingBo.CustomerName = commandArgumentParams(11)

                selPropSchedulingBo.Duration = 1
                'remove the proeprty scheduling bo from session if already exist  this is just to avoid the overlapping of sesssion values between different properties
                SessionManager.removeSelectedPropertySchedulingBo()

                'now set the session with selected property values
                SessionManager.setSelectedPropertySchedulingBo(selPropSchedulingBo)
                'set the property address label in available appointment pupup
                webUserCtrlPropAddressAvailableAppt.setPropertyLabels()
                'now create the appointment slots and bind data with available appointment grid
                Dim startDate As DateTime = Date.Now
                txtfromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy")

                Me.populateAppointments(selPropSchedulingBo, operativeDs, startDate)
                'Attach show map java script
                showMapJavaScript()
                mdlPopupAvailableAppointments.Show()
                Dim propertyId As String = commandArgumentParams(4)
                Dim attributeNotesDs As DataSet = New DataSet()
                objSchedulingBL.getAttributeNotesByPropertyId(attributeNotesDs, propertyId)
                If attributeNotesDs.Tables(0).Rows.Count > 0 Then
                    rptPropertyNotes.DataSource = attributeNotesDs
                    rptPropertyNotes.DataBind()
                    mdlPopupNotes.Show()
                End If

            Catch ex As ThreadAbortException
                uiMessageHelper.Message = ex.Message
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try
        Else
            PropertyScheduledPopUp.Show()
        End If

    End Sub
#End Region

#Region "Risk & Vulnerability of Customer"
    Private Function getCustomerRiskVulnerability(ByVal customerId As Integer)

        Dim schedulingBL As SchedulingBL = New SchedulingBL()
        Dim riskVulnerabilityResult As DataSet = New DataSet()
        Return schedulingBL.getCustomerRiskVulnerability(riskVulnerabilityResult, customerId)

    End Function
#End Region


#Region "Intelligent Scheduling Principal Popup - Functions"
#Region "get Available Operatives"
    ''' <summary>
    ''' This function retrieves the available operatives, their appointments and their leaves
    ''' </summary>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>
    Private Sub getAvailableOperatives(ByRef operativeDs As DataSet)

        Dim objFaultAppointmentBl As SchedulingBL = New SchedulingBL()
        objFaultAppointmentBl.getAvailableOperatives(operativeDs)

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            SessionManager.setAvailableOperativesDs(operativeDs)
        End If
    End Sub
#End Region

#Region "Populate Appointment"
    ''' <summary>
    ''' This function calls the business logic layer and creates the appointment slots and 
    ''' then binds that datatable with grid
    ''' </summary>
    ''' <param name="propertySchedulingBo"></param>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>

    Private Sub populateAppointments(ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal operativeDs As DataSet, Optional ByVal startDate As DateTime = Nothing)

        Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
        If Not startDate = Nothing Then
            appointmentSlotsDtBo.FromDate = startDate
        End If

        appointmentSlotsDtBo = objSchedulingBL.getAppointments(operativeDs.Tables(ApplicationConstants.OperativesDt), operativeDs.Tables(ApplicationConstants.LeavesDt), operativeDs.Tables(ApplicationConstants.AppointmentsDt), propertySchedulingBo, startDate)
        'Bind the appointment slots with asp.net control
        bindAppointmentSlotsWithControl(appointmentSlotsDtBo)

        'Save existing appointments for visible operatives to session to display pins on map.
        saveExistingAppointmentForVisibleOperatives(appointmentSlotsDtBo.dt, operativeDs.Tables(ApplicationConstants.AppointmentsDt))

    End Sub
#End Region

#Region "bind Appointment Slots With Control"
    ''' <summary>
    ''' Bind the appointment slots with asp.net control
    ''' </summary>
    ''' <param name="appointmentSlotsDtBo"></param>
    ''' <remarks></remarks>
    Private Sub bindAppointmentSlotsWithControl(ByVal appointmentSlotsDtBo As AppointmentSlotsDtBO)

        If (appointmentSlotsDtBo.dt.Rows.Count > 0) Then
            Me.setRefreshListButtonDisplay(True)
            Me.grdAvailableAppointments.DataSource = appointmentSlotsDtBo.dt
            Me.grdAvailableAppointments.DataBind()
            Dim btnRefreshList As Button = CType(grdAvailableAppointments.FooterRow.FindControl("btnRefreshList"), Button)
            btnRefreshList.Focus()
        Else
            Me.setRefreshListButtonDisplay(False)
            Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
        End If

    End Sub
#End Region

#Region "refresh The Appointment Slots"
    ''' <summary>
    ''' This function refresh the appointment list and adds more operatives in the list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub refreshTheAppointmentSlots(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO)

        Dim propertySchedulingBo As SelectedPropertySchedulingBO

        'get the property scheduling bo
        propertySchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

        Dim operativeDs As DataSet = New DataSet()
        operativeDs = SessionManager.getAvailableOperativesDs()

        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and expiry date 
        objSchedulingBL.addMoreAppointments(operativeDs.Tables(ApplicationConstants.OperativesDt), operativeDs.Tables(ApplicationConstants.LeavesDt), operativeDs.Tables(ApplicationConstants.AppointmentsDt), propertySchedulingBo, appointmentSlotsDtBo)

        'save the appointment slots in session 
        'First save a copy of object in session before applying the order function.
        SessionManager.setAppointmentSlots(appointmentSlotsDtBo)

        'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment         
        objSchedulingBL.orderAppointmentsByDistance(appointmentSlotsDtBo)

        'Bind the appointment slots with asp.net control
        bindAppointmentSlotsWithControl(appointmentSlotsDtBo)

        'Save existing appointments for visible operatives to session to display pins on map.
        saveExistingAppointmentForVisibleOperatives(appointmentSlotsDtBo.dt, operativeDs.Tables(ApplicationConstants.AppointmentsDt))

        'Register Client side script to update markers
        updateMarkersJavaScript()
    End Sub
#End Region

#Region "Display Msg In Empty Grid"
    ''' <summary>
    ''' This function will be called if there are no appointments in the grid. This 'll create the empty row with message in it.
    ''' </summary>
    ''' <param name="appointmentSlotsDtBo"></param>
    ''' <param name="msg"></param>
    ''' <remarks></remarks>
    Private Sub displayMsgInEmptyGrid(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        appointmentSlotsDtBo.dt.Rows.Add(appointmentSlotsDtBo.dt.NewRow())
        grdAvailableAppointments.DataSource = appointmentSlotsDtBo.dt
        grdAvailableAppointments.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        appointmentSlotsDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "avail-no-record-msg")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grdAvailableAppointments.Rows(0).Cells.Count
        grdAvailableAppointments.Rows(0).Cells.Clear()
        grdAvailableAppointments.Rows(0).Cells.Add(New TableCell())
        grdAvailableAppointments.Rows(0).Cells(0).ColumnSpan = columncount
        grdAvailableAppointments.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Grid Header"
    ''' <summary>
    ''' This function create the 2nd header of available appointments grid
    ''' </summary>
    ''' <param name="isPatchSelected"></param>
    ''' <param name="isDateSelected"></param>
    ''' <param name="expireInDays"></param>
    ''' <remarks></remarks>
    Private Sub createGridHeader(ByVal isPatchSelected As Boolean, ByVal isDateSelected As Boolean, ByVal expireInDays As String, Optional ByVal disableDateCheckBox As Boolean = False)

        'add patch checkbox

        chkPatch.Checked = isPatchSelected

        'add date checkbox
        chkDate.Checked = isDateSelected

        'add from date textbox

        'There are some properties which does not have any certificate expiry date for that this check is implemented
        'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
        If disableDateCheckBox = True Then
            chkDate.Enabled = False
        End If
        'AddHandler chkDate.CheckedChanged, AddressOf chkExpiryDate_CheckedChanged

        'add default value under default label
        lblDefault.Text = SessionManager.getTerminationDate

    End Sub
#End Region

#Region "hide Show Refresh List Button"
    ''' <summary>
    ''' This function will be while assigning the datasource to grid. This 'll be called from aspx page.
    ''' this function checks the state of refresh list button and then deicides its visibility
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowRefreshListButton()
        If getRefreshListButtonDisplay() = False Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Set / Get Refresh List Button Display"

#Region "get Refresh List Button Display"
    Private Function getRefreshListButtonDisplay()
        Dim display As Boolean = False
        If ViewState(ViewStateConstants.RefreshListButtonDisplay) IsNot Nothing Then
            display = CType(ViewState(ViewStateConstants.RefreshListButtonDisplay), Boolean)
        End If
        Return display
    End Function
#End Region

#Region "set Refresh List Button Display"
    Private Sub setRefreshListButtonDisplay(ByVal display As Boolean)
        ViewState(ViewStateConstants.RefreshListButtonDisplay) = display
    End Sub
#End Region
#End Region
#End Region



#Region "Intelligent Scheduling Principal - Popup"
#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the perviously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.DisplayCount = appointmentSlotsDtBo.DisplayCount + 5
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If
            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "chk Patch Checked Changed"
    Protected Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chkPatch As CheckBox = TryCast(sender, CheckBox)

            If chkPatch.Checked = True Then
                'If patch is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the perviously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.IsPatchSelected = chkPatch.Checked
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If
            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
            Me.updPanelAvailableOperatives.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "chk Expiry Date Checked Changed"
    Protected Sub chkExpiryDate_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim chkExpiryDate As CheckBox = TryCast(sender, CheckBox)

            If chkExpiryDate.Checked = True Then
                'If expiry date is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the previously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.IsDateSelected = chkExpiryDate.Checked
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If

            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region



#Region "TextChanged event for  txtfromDate"

    Protected Sub txtfromDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try

            Dim startDate As DateTime = Date.Now

            If txtfromDate.Text <> "" Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                'If expiry date is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
                startDate = Convert.ToDateTime(txtfromDate.Text)
            End If

            If startDate < Now.Date Then
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.DateNotValid
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                Me.setRefreshListButtonDisplay(False)
                Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                Exit Sub
            Else
                uiMessageHelper.IsError = False
                uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)


            End If



            Dim operativeDs As DataSet = New DataSet()
            Me.getAvailableOperatives(operativeDs)

            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo

            'now create the appointment slots and bind data with available appointment grid
            Me.populateAppointments(selPropSchedulingBo, operativeDs, startDate)
            'Attach show map java script
            showMapJavaScript()
            'this 'll add five more operatives in the list appointments
            'Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)

            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageOnPopUp, pnlMessageOnPopUp, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region



#Region "grd Available Appointments Row Created"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAvailableAppointments_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAvailableAppointments.RowCreated
        'get the appointment slots from database
        Dim appointmentSlotsDtbo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
        appointmentSlotsDtbo = SessionManager.getAppointmentSlots()

        'get the property scheduling bo . this bo contains the property related data
        Dim propertySchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        propertySchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

        'create the grid header
        If e.Row.RowType = DataControlRowType.Header Then
            'There are some properties which does not have any certificate expiry date for that this check is implemented
            'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
            Dim disableExpiryDate As Boolean = False
            If IsNothing(propertySchedulingBo.CertificateExpiryDate) Then
                disableExpiryDate = True
            End If
            createGridHeader(appointmentSlotsDtbo.IsPatchSelected, appointmentSlotsDtbo.IsDateSelected, propertySchedulingBo.ExpiryInDays, disableExpiryDate)
        End If
    End Sub
#End Region

#Region "btn Select Click"
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'clear the add appointment popup controls
            Me.resetAddAppointmentControls()
            'below mention sequence is expected in command argument parameters
            '0 index - operative id
            '1 index - operative name
            '2 index - appointment start time
            '3 index - appointment end time
            '4 index - appointment date                        
            Dim btnSelect As Button = TryCast(sender, Button)
            Dim appointmentInfo As String() = CType(btnSelect.CommandArgument, String).Split(";")
            'this function will set the appropriate values 
            Me.openAddAppointmentPopup(appointmentInfo)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try

    End Sub
#End Region
#End Region

#Region "img Btn close Property Scheduled Click"

    Protected Sub imgPropertyScheduleClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPropertyScheduleClose.Click
        Try
            populateAppointmentToBeArrangedGrid()
            mdlPopupAvailableAppointments.Hide()
            mdlPopUpAddAppointment.Hide()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close Available Appointments Click"

    Protected Sub imgBtnCloseAvailableAppointments_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAvailableAppointments.Click
        Try
            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo()
            If SessionManager.getUserEmployeeId() = objSchedulingBL.getPropertyLockedBy(selPropSchedulingBo.PropertyId, "Property") Then
                objSchedulingBL.setPropertyschedulestatus(selPropSchedulingBo.PropertyId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
            End If
            'objSchedulingBL.setPropertyschedulestatus(selPropSchedulingBo.PropertyId, 0, 0, SessionManager.getUserEmployeeId())
            mdlPopupAvailableAppointments.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add Appointment Popup - Functions"
#Region "Open Appointment Popup"

    Public Sub openAddAppointmentPopup(ByVal appointmentInfo As String())
        'below mention sequence is expected in appointmentInfo array
        '0 index - operative id
        '1 index - operative name
        '2 index - appointment start time
        '3 index - appointment end time
        '4 index - appointment date                    
        Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        'retreive the information that has been already saved in session related to this selected property
        selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

        'fill up the information of appointment against selected property
        selPropSchedulingBo.OperativeId = CType(appointmentInfo(0), Integer)
        selPropSchedulingBo.OperativeName = appointmentInfo(1)
        selPropSchedulingBo.AppointmentStartTime = CType(appointmentInfo(2), Date)
        selPropSchedulingBo.AppointmentEndTime = CType(appointmentInfo(3), Date)
        selPropSchedulingBo.AppointmentDate = CType(appointmentInfo(4), Date)

        'save this above information in session again
        SessionManager.setSelectedPropertySchedulingBo(selPropSchedulingBo)

        'fill up the address
        lblLocation.Text = selPropSchedulingBo.PropertyAddress
        'set the appointment date in label
        'This(fucntion) will convert the date in following format e.g Mon 12th Nov 2012
        lblAppointmentDate.Text = GeneralHelper.convertDateToUsCulture(selPropSchedulingBo.AppointmentDate)
        'set the appointment start time into 24 hrs format string
        lblAppointmentStartTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentStartTime)
        'set the appointment end time into 24 hrs format string
        lblAppointmentEndTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentEndTime)
        'set the operative name 
        lblOperative.Text = selPropSchedulingBo.OperativeName

        If (Me.ddlAddStatus.Items.Count <= 0) Then
            Me.LoadSatus()
        End If

        ''Start - Changed By Aamir Waheed on May 30, 2013.
        ''To implement joint tenants information.
        'lnkBtnAppDetails.Visible = GeneralHelper.isPropertyStatusLet(apptScheduleParam(8).ToString())
        'lnkBtnAppDetails.CommandArgument = apptScheduleParam(2)


    End Sub
#End Region

#Region "Load Satus"
    Protected Sub LoadSatus()
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlAddStatus.DataSource = resultDataSet
        ddlAddStatus.DataValueField = ApplicationConstants.StatusId
        ddlAddStatus.DataTextField = ApplicationConstants.Title
        ddlAddStatus.DataBind()

        'Remove the status 'appointment to be arranged' from the drop down list when arranging or rearranging an appointment
        'Action : Removed the status 'appointment to be arranged' from drop down list.
        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Appointment to be arranged")) Then
            ddlAddStatus.Items.RemoveAt(ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Appointment to be arranged")))
        Else
            ddlAddStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
            ddlAddStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue
        End If

        'The default status when in the 'Add AppoIntment' pop-up should be 'Arranged'
        'Action : Selected the Status 'Arranged' and Load Actions accordingly.

        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Arranged")) Then
            ddlAddStatus.SelectedIndex = ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Arranged"))
            LoadActions(CType(ddlAddStatus.SelectedValue, Integer))
        End If
    End Sub
#End Region

#Region "Load Actions"
    Protected Sub LoadActions(ByRef statusId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAddAction.DataSource = resultDataSet
        ddlAddAction.DataValueField = ApplicationConstants.ActionId
        ddlAddAction.DataTextField = ApplicationConstants.Title
        ddlAddAction.DataBind()
        ddlAddAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAddAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub LoadLetters(ByRef actionId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim objResourceBL As ResourcesBL = New ResourcesBL()
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlAddLetter.DataSource = resultDataSet
        ddlAddLetter.DataValueField = ApplicationConstants.LetterId
        ddlAddLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlAddLetter.DataBind()
        ddlAddLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlAddLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Validate Add Appointment"
    Protected Function validateAddAppointment()
        Dim success As Boolean = True
        If Me.ddlAddStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionStatus, True)
        ElseIf Me.ddlAddAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)
        End If
        Return success
    End Function
#End Region

#Region "Reset Add Appointment Controls"
    Protected Sub resetAddAppointmentControls()

        'The default status when in the 'Add AppoIntment' pop-up should be 'Arranged'
        If ddlAddStatus.Items.Count > 0 AndAlso ddlAddStatus.Items.Contains(ddlAddStatus.Items.FindByText("Arranged")) Then
            Me.ddlAddStatus.SelectedValue = ddlAddStatus.Items(ddlAddStatus.Items.IndexOf(ddlAddStatus.Items.FindByText("Arranged"))).Value
            Me.LoadActions(Me.ddlAddStatus.SelectedValue)
        End If

        Me.lblActivityPopupMessage.Text = String.Empty
        Me.LoadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'if user didn't selected the letter from letter drop down
        If ddlAddLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlAddLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If

    End Sub
#End Region

#Region "Save Appointment Information"
    Protected Function saveAddAppointment(ByRef pushNoticificationStatus As String, ByRef pushNoticificationDescription As String)
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        Dim selPropSechdulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        Dim journalHistoryId As String

        'get the property and operative information from sesssion 
        selPropSechdulingBo = SessionManager.getSelectedPropertySchedulingBo()

        'Save Appointment Information
        objScheduleAppointmentsBO.Duration = ApplicationConstants.DefaultSchedulingHour
        objScheduleAppointmentsBO.JournelId = selPropSechdulingBo.JournalId
        objScheduleAppointmentsBO.IsVoid = 1 ' 1 if Void Appointment, default is 0 for Fuel Appointment
        objScheduleAppointmentsBO.PropertyId = selPropSechdulingBo.PropertyId
        objScheduleAppointmentsBO.CreatedBy = SessionManager.getAppServicingUserId()
        objScheduleAppointmentsBO.InspectionTypeId = SessionManager.getApplianceServicingId()
        objScheduleAppointmentsBO.AppointmentDate = lblAppointmentDate.Text
        objScheduleAppointmentsBO.AppointmentEndDate = lblAppointmentDate.Text
        objScheduleAppointmentsBO.StatusId = ddlAddStatus.SelectedValue
        objScheduleAppointmentsBO.ActionId = ddlAddAction.SelectedValue
        objScheduleAppointmentsBO.LetterId = ddlAddLetter.SelectedValue
        objScheduleAppointmentsBO.Notes = txtNotes.Text.Trim
        objScheduleAppointmentsBO.DocumentPath = GeneralHelper.getDocumentUploadPath()
        If (System.DateTime.Parse(Me.lblAppointmentEndTime.Text).TimeOfDay > System.DateTime.Parse("12:00").TimeOfDay) Then
            objScheduleAppointmentsBO.Shift = "PM"
        Else
            objScheduleAppointmentsBO.Shift = "AM"
        End If
        objScheduleAppointmentsBO.AppStartTime = lblAppointmentStartTime.Text
        objScheduleAppointmentsBO.AppEndTime = lblAppointmentEndTime.Text
        objScheduleAppointmentsBO.AssignedTo = selPropSechdulingBo.OperativeId


        'To save null value in tenancyId in case of void property.
        objScheduleAppointmentsBO.TenancyId = Nothing
        If Not (String.IsNullOrEmpty(selPropSechdulingBo.TenancyId)) Then
            objScheduleAppointmentsBO.TenancyId = selPropSechdulingBo.TenancyId
        End If

        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objScheduleAppointmentsBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objScheduleAppointmentsBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objScheduleAppointmentsBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()
            If docQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objScheduleAppointmentsBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objScheduleAppointmentsBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objScheduleAppointmentsBO.IsDocumentAttached = False
            End If
        End If
        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        journalHistoryId = objSchedulingBL.saveAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

        If (journalHistoryId > 0 And objScheduleAppointmentsBO.AppointmentDate = Today()) Then
            pushNoticificationStatus = UserMessageConstants.PushNoticificationSending
            pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSending
            Dim propertyAddress As String = String.Empty
            propertyAddress = lblLocation.Text.Trim()

            Dim message As String = String.Empty
            message = GeneralHelper.generatePushNotificationMessage("New", objScheduleAppointmentsBO.AppointmentDate, objScheduleAppointmentsBO.AppStartTime,
                                                    objScheduleAppointmentsBO.AppEndTime, propertyAddress)
            Dim pushnoticicesent As Boolean = GeneralHelper.pushNotificationAppliance(message, objScheduleAppointmentsBO.AssignedTo)
            If pushnoticicesent = True Then
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSent
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSent
            Else
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSendingFailed
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSendingFailed
            End If
        End If

        Return journalHistoryId

    End Function

#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal tenantmobile As String, ByRef smsStatus As String, ByRef smsDescription As String)
        Try
            'tenantmobile = "07788131379"
            tenantmobile = tenantmobile.Replace(" ", String.Empty)

            If tenantmobile.Length >= 10 Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(lblAppointmentStartTime.Text, lblAppointmentDate.Text)
                'Dim postData As String

                'postData = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(tenantmobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)

                'GeneralHelper.sendSMS(body, tenantmobile)
                'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                'Dim javascriptMethod As String = "sendSMS('" + tenantmobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(tenantmobile, body)

            Else
                smsStatus = UserMessageConstants.SmsSendingFailed
                smsDescription = UserMessageConstants.AppointmentSavedSMSError + UserMessageConstants.InvalidMobile
                'uiMessageHelper.IsError = True
                'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As Exception
            smsStatus = UserMessageConstants.SmsSendingFailed
            smsDescription = UserMessageConstants.AppointmentSavedSMSError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal journalHistoryId As Integer, ByRef emailStatus As String, ByRef emailDescription As String)

        Try
            Dim objScheduling As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            objScheduling.getAppointmentInfoForEmail(resultDataset, journalHistoryId)
            Dim infoDt As DataTable = resultDataset.Tables(0)
            If (infoDt.Rows.Count > 0) Then
                Dim recepientEmail As String = infoDt.Rows(0)(ApplicationConstants.EmailCol)
                Dim recepientName As String = infoDt.Rows(0)(ApplicationConstants.CustomerNameCol)
                Dim jobSheet As String = infoDt.Rows(0)(ApplicationConstants.JSGCol)
                Dim appointmentDate As String = infoDt.Rows(0)(ApplicationConstants.AppointmentDateCol)
                Dim appointmentTime As String = infoDt.Rows(0)(ApplicationConstants.AppointmentTimeCol)

                If Validation.isEmail(recepientEmail) Then
                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheet, appointmentTime, appointmentDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)
                    emailStatus = UserMessageConstants.EmailSent
                    emailDescription = UserMessageConstants.EmailDescriptionSent
                Else
                    Throw New Exception(UserMessageConstants.InvalidEmail)
                End If
            Else
                Throw New Exception(UserMessageConstants.TanencyTerminated)
            End If

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            emailStatus = UserMessageConstants.EmailSendingFailed
            emailDescription = UserMessageConstants.AppointmentSavedEmailError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region
#Region "set Email And Sms Status For Appointment"
    Private Sub setEmailAndSmsStatusForAppointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
        Try
            objSchedulingBL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#Region "set Push Noticification Status For Appointment"
    Private Sub setPushNoticificationStatusForAppointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
        Try
            objSchedulingBL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList() 'issue
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList() 'issue

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region
#End Region




#Region "Add Appointment Popup"

#Region "lnk Btn App Details Click"
    ''' <summary>
    ''' This event handler will handle the phone icon on add appointment popup
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnAppDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkBtnAppDetails.Click

        'To implement joint tenants information.
        'Action: commented out previous function and applied new function.        
        Try
            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

            Dim tenancyID As Integer = selPropSchedulingBo.TenancyId
            Dim dstenantsInfo As New DataSet()
            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                setTenantsInfo(dstenantsInfo, tblAptbaTenantInfoAddPopup)
                Me.mdlPopUpAddAppointmentContactDetails.Show()
                Me.mdlPopupAvailableAppointments.Show()
                Me.mdlPopUpAddAppointment.Show()
            Else
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.noTenantInformationFound, True)
                Me.mdlPopupAvailableAppointments.Show()
                Me.mdlPopUpAddAppointment.Show()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub


    Protected Sub setContactInfo(ByVal customerId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resultDataTable As New DataTable

        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblFirstName.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblMobileNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblTelNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)

        Me.mdlPopUpAddAppointment.Show()
    End Sub

#End Region

#Region "ddl Status Selected Index Changed"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAddStatus.SelectedIndexChanged
        Try

            Dim statusId As Integer = CType(ddlAddStatus.SelectedValue(), Integer)
            Me.LoadActions(statusId)
            Me.LoadLetters(-1)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try
    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAddAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAddAction.SelectedValue(), Integer)
            Me.LoadLetters(actionId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
            'Attach show map java script            
            showMapJavaScript()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try
    End Sub

#End Region

#Region "Btn Save Appointment Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click
        Dim isSaved As Boolean = False
        Dim checked As Boolean = True
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        Dim SmsStatus As String = UserMessageConstants.SmsNotSent
        Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
        Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
        Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
        Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
        Dim pId As String = ""
        pId = SessionManager.getSelectedPropertySchedulingBo().PropertyId
        If Not IsNothing(pId) AndAlso Not pId = "" Then
            If SessionManager.getUserEmployeeId = objSchedulingBL.getPropertyLockedBy(pId, "Property") Then
                Try

                    If (Me.validateAddAppointment() = True) Then
                        Dim journalHistoryId As Integer = 0
                        journalHistoryId = Me.saveAddAppointment(PushNoticificationStatus, PushNoticificationDescription)
                        If journalHistoryId > 0 Then
                            'populate appointment to be arranged grid 
                            'Me.populateAppointmentToBeArrangedGrid()
                            'show success message in new popup. Set the property address label in new pupup
                            'webUserCtrlPropAddressSuccessPopup.setPropertyLabels()
                            uiMessageHelper.setMessage(lblSuccessAppointmentMessage, pnlSuccessAppointmentMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
                            isSaved = True

                            ' Start of code to send the SMS'
                            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                            selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

                            Dim tenancyID As Integer = selPropSchedulingBo.TenancyId
                            Dim dstenantsInfo As New DataSet()
                            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

                            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                                SmsStatus = UserMessageConstants.SmsSending
                                SmsDescription = UserMessageConstants.SmsDescriptionSending
                                Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                                Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                            End If
                            ' End of code to send the SMS'
                            EmailStatus = UserMessageConstants.EmailSending
                            EmailDescription = UserMessageConstants.EmailDescriptionSending
                            Me.sendEmail(journalHistoryId, EmailStatus, EmailDescription)
                            Me.setEmailAndSmsStatusForAppointment(journalHistoryId, SmsStatus, SmsDescription, EmailStatus, EmailDescription)
                            Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)
                            'To remove the record of existing letters from the session,
                            ' so these these letters can be re attached on re arrange.
                            SessionManager.removeActivityLetterDetail()
                        Else
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                        End If
                    End If
                Catch ex As ThreadAbortException
                    uiMessageHelper.Message = ex.Message
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                    End If

                    If isSaved = False Then
                        Me.mdlPopupAvailableAppointments.Show()
                        Me.mdlPopUpAddAppointment.Show()
                    Else
                        If isSaved = True Then
                            Me.mdlPopupAvailableAppointments.Hide()
                            Me.mdlPopUpAddAppointment.Hide()
                            Me.mdlSuccessPopup.Show()
                            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
                        Else
                            'If the appointment is saved sucessfully Disable save button. and cancel button.                               
                            Me.mdlPopupAvailableAppointments.Show()
                            Me.mdlPopUpAddAppointment.Show()
                        End If

                    End If

                End Try
            Else
                PropertyScheduledPopUp.Show()
            End If

        End If

    End Sub
#End Region

#Region "img Btn close success popup Click"

    Protected Sub imgBtnCancelSuccessPopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancelSuccessPopup.Click
        Try
            Response.Redirect(PathConstants.VoidServicingPath)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Add View Letter Click"
    Protected Sub btnAddViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddViewLetter.Click
        Try
            If (ddlAddLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try
    End Sub
#End Region

#Region "ckBox Refresh DataSet Checked Changed"
    Protected Sub ckBoxAddRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxAddRefreshDataSet.CheckedChanged
        Me.ckBoxAddRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlAddLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId()) 'issue
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
        Me.mdlPopUpAddAppointment.Show()
        Me.mdlPopupAvailableAppointments.Show()
    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"
    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty) 'issue
            End If
            'Attach show map java script            
            showMapJavaScript()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAddAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Close Add Appointment Click"
    Protected Sub imgBtnCloseAddAppointment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAddAppointment.Click
        Try
            Me.mdlPopUpAddAppointment.Hide()
            'Attach show map java script            
            showMapJavaScript()
            Me.mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception

        End Try
    End Sub
#End Region

#End Region


#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"
    ''' <summary>
    ''' 'To implement joint tenants information.
    ''' </summary>
    ''' <param name="dsTenantsInfo"></param>
    ''' <param name="tblTenantInfo"></param>
    ''' <remarks></remarks>
    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
#End Region

#Region "set Tenant Info"

    Protected Sub setTenantInfo(ByVal customerId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resultDataTable As New DataTable

        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblAptbaEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblAptbaFisrtname.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblAptbaMobile.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblAptbaTel.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        Me.mdlPopUpAppointmentToBeArrangedPhone.Show()

    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region


#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region


#Region "set Search Text"

    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptbaSearchString) = searchText
        End If
    End Sub

#End Region


#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptbaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptbaSearchString), String)
        End If
    End Function
#End Region

#Region "Save existing appointments in session to show on map"

    Private Sub saveExistingAppointmentForVisibleOperatives(ByVal appointmentSlots As DataTable, ByVal existingAppointments As DataTable)

        'Remove the appointments/properties from session

        'Get appointment of only visible operatives.
        Dim appointmentsForMap = (From extapt In existingAppointments.AsEnumerable()
                                    Join aptSlot In appointmentSlots.AsEnumerable() On extapt("OperativeId").ToString() Equals aptSlot("OperativeId").ToString()
                                    Select extapt)

        Dim appointmentsForMapDt As DataTable = Nothing

        If appointmentsForMap.Count() > 0 Then
            'Join may cause repetition of rows so distinct is applied.
            appointmentsForMapDt = appointmentsForMap.Distinct().CopyToDataTable()
        End If

        SessionManager.setExistingAppointmentsForSchedulingMap(appointmentsForMapDt)

    End Sub

#End Region

#Region "Register client side Startup Script to show map"

    Public Sub showMapJavaScript()
        Dim selPropSchedulingBo As SelectedPropertySchedulingBO = SessionManager.getSelectedPropertySchedulingBo()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "showMapATBA", "showMap('" + aptTBAMap.ClientID + "','" + selPropSchedulingBo.PropertyAddress.Replace("'", "\'") + "');", True)
    End Sub

#End Region

#Region "Register client side script to update markers on Google Map. on appointments refresh."

    Private Sub updateMarkersJavaScript()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "updateMarkerATBA", "addOriginMarkers();", True)
    End Sub

#End Region

#Region "Register client side script to remove existing markers from Map"

    Private Sub removeMarkersJavaScript()
        'Register client side script to remove markers on Google Map.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "deleteMarkersATBA", "deleteMarkers();", True)
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBo()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBo(objPageSortBo)

            populateAppointmentToBeArrangedGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            'If uiMessageHelper.IsError = True Then
            '    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            'End If
        End Try

    End Sub

#End Region




End Class