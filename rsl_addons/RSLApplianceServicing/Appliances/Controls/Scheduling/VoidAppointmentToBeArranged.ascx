﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VoidAppointmentToBeArranged.ascx.vb" Inherits="Appliances.VoidAppointmentToBeArranged" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="propertyAddressTag" TagName="PropertyAddress" Src="~/Controls/Scheduling/PropertyAddress.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<%@ Register TagName="AssignFuelServicingToContractor" TagPrefix="ucAssignFuelServicingToContractor"
    Src="~/Controls/FuelScheduling/AssignFuelServicingToContractor.ascx" %>

<%--JavaScript Code--%>
<script type="text/javascript">
    var w;
    function uploadError(sender, args) {
        alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
    }

    function uploadComplete() {
        __doPostBack();
    }

    function openEditLetterWindowForAdd() {

        var letterId = document.getElementById('<%= ddlAddLetter.ClientID %>').value;
        if (letterId == -1) {

        }
        else {
            w = window.open('../../Views/Resources/EditLetter.aspx?pg=0&id=' + letterId, '_blank', 'directories=no,height=600,width=800,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
    }

    function fireCheckboxEventForAddApp() {
        document.getElementById('<%= ckBoxAddRefreshDataSet.ClientID %>').checked = true;
        setTimeout('__doPostBack(\'<%= ckBoxAddRefreshDataSet.UniqueID %>\',\'\')', 0);
    }

    function callAlert(msg) {
        alert(msg);
    }
        
</script>



<%--CSS Styling--%>
<style type="text/css">
    .modalBackground
    {
        background-color: #C1C1C1;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    .modalPopUpNotesBackground
    {
        background-color: Gray;
        filter: alpha(opacity=80);
        opacity: 0.8;
    }
    
    
    .modalPopup
    {
        background-color: White; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
    .availableAppointmentPopOut
    {
        height: 100%;
        width: 100%;
        background: #000;
        display: block;
        z-index: 9999;
        position: fixed;
        top: 0;
        left: 0;
    }
    .modalBackground, .modalBackground2
    {
        background-color: #455560;
        filter: alpha(opacity=40);
        opacity: 0.4;
    }
    .modalBackground
    {
        z-index: 1000;
    }
    ..modalBackground2
    {
        z-index: 10001;
    }
    .arrangeButton
    {
        padding : 3px;
        border-style : solid;
        border-width:1px;
        border-color:Black;
        background-color:#808080;
        color:White;
    }
    
</style>


<%--Page Contents--%>
<div style="max-height: 500px; overflow: auto;">

        <%--Error Messages--%>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>

        <cc1:PagingGridView ID="grdVoidAppointmentToBeArranged" runat="server" AllowPaging="false" AllowSorting="true"
                AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found." >

        <Columns>
            <asp:TemplateField HeaderText="Ref:" SortExpression="Ref">
                <ItemTemplate>
                    <asp:Label ID="lblRef" runat="server" Text='<%# Eval("Ref") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Eval("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Block:" SortExpression="Block">
                <ItemTemplate>
                    <asp:Label ID="lblBlock" runat="server" Text='<%# Eval("Block") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="15%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status:" SortExpression="Status">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Sub-Status:" SortExpression="SubStatus">
                <ItemTemplate>
                    <asp:Label ID="lblSubStatus" runat="server" Text='<%# Eval("SubStatus") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                <ItemTemplate>
                    <asp:Label ID="lblTermination" runat="server" Text='<%# Eval("Termination") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Relet:" SortExpression="Relet">
                <ItemTemplate>
                    <asp:Label ID="lblRelet" runat="server" Text='<%# Eval("Relet") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="5%" />
            </asp:TemplateField>

            <asp:TemplateField ShowHeader="false">
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnArrow" Text="Arrange" runat="server" OnClick="imgBtnAvailableAppointmentSlots_Click" CssClass="arrangeButton"
                     CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("PROPERTYID").ToString()+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString()+";"+Eval("PatchId").toString()+";"+Eval("Telephone").ToString()+";" + Eval("NAME").ToString() + ";" + Eval("CustomerId").ToString()+ ";" + Eval("Termination").ToString()%>' 
                      />
                </ItemTemplate>
                <ItemStyle Width="5%" />
            </asp:TemplateField>

          </Columns>

        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="White" Font-Bold="True" ForeColor="Black" BorderColor="Black"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
        <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="10" />
        <PagerStyle BackColor="White" />
        <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
        </cc1:PagingGridView>

        <asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
            <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                        <asp:ImageButton ID="imgBtnAptbaClose" runat="server" Style="position: absolute;
                            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

</div>
        <asp:Label ID="lbldisplayAppointment" runat="server"></asp:Label>
        <asp:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
            TargetControlID="lbldisplayAppointment" PopupControlID="pnlAppointmentToBeArrangedTenantInfo"
            Enabled="true" DropShadow="true" CancelControlID="imgBtnAptbaClose">
        </asp:ModalPopupExtender>

        
        <div align="center">
                <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                    vertical-align: middle; float: left; width: 100%;">
                    <table style="width: 55%;">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                        Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                        Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    Page:&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                    of&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
        </div>

    

    <div id="divAvailableAppointment" runat="server">
        <asp:Panel ID="pnlAvailalbleAppointments" runat="server" CssClass="pnlAvail-Appt">
        <div>
            <asp:ImageButton ID="imgBtnCloseAvailableAppointments" runat="server" Style="position: absolute;
                top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        </div>
        <asp:UpdatePanel ID="updPanelAvailableOperatives" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div style="float: left; width: 100%; margin: 5px; text-align: left;">
                        <asp:Panel ID="pnlMessageOnPopUp" runat="server" Visible="false">
                            <asp:Label ID="lblMessageOnPopUp" runat="server"></asp:Label>
                        </asp:Panel>
                    </div>
                    <div style="float: left; margin: 5px;">
                        <propertyAddressTag:PropertyAddress ID="webUserCtrlPropAddressAvailableAppt" runat="server"
                            MaxValue="10" MinValue="1" />
                    </div>
                    <br />
                    <div class="grid_avail_appt_filter" style="width: 100%; margin-top: 10px; font-weight: bold;">
                        <table style="background-color: #c1c1c1; width: 100%;">
                            <tr>
                                <td>
                                    <asp:Label ID="lblInfo" runat="server"><b>Filter Available Operatives By:</b></asp:Label>
                                </td>
                                <td>
                                    <div class="grid_avail_appt_patch">
                                        <b style='float: left; margin-top: 5px;'>Patch: </b>
                                        <asp:CheckBox ID="chkPatch" runat="server" AutoPostBack="true" OnCheckedChanged="chkPatch_CheckedChanged"
                                            Style='float: left; margin-top: 3px;' /></div>
                                </td>
                                <td>
                                    <div class="grid_avail_appt_date">
                                        <b style='float: left; margin-top: 5px;'>Expiry Date:</b>
                                        <asp:CheckBox ID="chkDate" runat="server" AutoPostBack="true" OnCheckedChanged="chkExpiryDate_CheckedChanged"
                                            Style='float: left; margin-top: 3px;' /></div>
                                </td>
                                <td class="grid_avail_appt_date">
                                    <b style='float: left; margin-top: 5px;'>From Date:</b>
                                    <asp:TextBox ID="txtfromDate" runat="server" AutoPostBack="true" OnTextChanged="txtfromDate_TextChanged"
                                        Style='float: left; margin-top: 3px;'></asp:TextBox>
                                    <asp:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        PopupPosition="Right" TargetControlID="txtfromDate" TodaysDateFormat="dd/MM/yyyy"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </td>
                                <td align="right">
                                    <b style='float: left; margin-top: 5px;'>Termination Date:</b>
                                    <asp:Label Text="" ID="lblDefault" runat="server" Style='float: left; margin-top: 5px;' />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 260px; overflow: auto; width: 100%;">
                        &nbsp;<asp:GridView ID="grdAvailableAppointments" runat="server" AutoGenerateColumns="False"
                            CssClass="grid_avail_appt" ShowFooter="true" GridLines="None">
                            <Columns>
                                <asp:TemplateField HeaderText="Operative:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOperative" runat="server" Text='<%# Eval("Operative:") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Patch:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPatch" runat="server" Text='<%# Eval("Patch:") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" Text='<%# Eval("Time:") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date:") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Distance(miles):">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDistance" runat="server" Text='<%# Eval("Distance:") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" ShowHeader="False">
                                    <FooterTemplate>
                                        <asp:Button ID="btnRefreshList" runat="server" Text="Refresh List" Visible='<%# hideShowRefreshListButton() %>'
                                            OnClick="btnRefreshList_Click" ClientIDMode="Static" />
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Button ID="btnSelect" runat="server" Text="Select" CommandArgument='<%# Eval("OperativeId").toString()+";"+Eval("Operative:")+";"+Eval("AppointmentStartTime")+";"+Eval("AppointmentEndTime")+";"+Eval("AppointmentDate").toString() %>'
                                            OnClick="btnSelect_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderWidth="1px" />
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel runat="server" ClientIDMode="Static" CssClass="appointmentsMap" ID="aptTBAMap">
        </asp:Panel>
    </asp:Panel>
    </div>
<asp:Label ID="lblDisplayAvailableAppointments" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopupAvailableAppointments" runat="server" TargetControlID="lblDisplayAvailableAppointments"
    PopupControlID="pnlAvailalbleAppointments" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
    >
</asp:ModalPopupExtender>






<asp:Panel ID="pnlAddAppointment" CssClass="left" runat="server" BackColor="White"
    Width="470px" Height="500px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
    position: absolute; display: none /*-bracket-: hack(; left: 430px !important;
    top: 100px; ); */">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Add Appointment</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <%--<div style="width: 100%; height: 490px; overflow: hidden;">--%>
    <div style="overflow-y: scroll; height: 470px; padding-top: 5px !important;">
        <asp:UpdatePanel ID="updPanelAddAppointmentPopup" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="3" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="3">
                            <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                                <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Appointment Type:<span class="Required">*</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblAppointmentType" runat="server" Text="Appliance Servicing"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Location:
                        </td>
                        <td align="right" valign="top" class="style2">
                            <span style="float: left; margin-top: -4px; vertical-align: top;">
                                <asp:ImageButton ID="lnkBtnAppDetails" runat="server" ImageUrl="../../images/phone.jpg"
                                    BorderStyle="None" CommandArgument="" />
                            </span>
                            <asp:Label ID="lblLocation" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Date:<span class="Required">*</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblAppointmentDate" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Starts:
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblAppointmentStartTime" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Ends:
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblAppointmentEndTime" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Operative:
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="lblOperative" runat="server"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Status:<span class="Required">*</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlAddStatus" runat="server" AutoPostBack="True" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Action:<span class="Required">*</span>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlAddAction" runat="server" AutoPostBack="True" Width="200px">
                                <asp:ListItem Selected="True" Value="-1">Select Action</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Letter:
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlAddLetter" runat="server" Width="200px">
                                <asp:ListItem Selected="True" Value="-1">Select Letter</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top" class="style5">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <asp:Button ID="btnUploadLetter" runat="server" Text="Upload" Width="60px" OnClientClick="openAddUploadWindow()"
                                BackColor="White" />
                            &nbsp;
                            <asp:Button ID="btnAddViewLetter" runat="server" Text="View Letter" OnClientClick="openEditLetterWindowForAdd()"
                                BackColor="White" />
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Docs:
                        </td>
                        <td align="left" valign="top">
                            <asp:Panel ID="pnlLetterDocList" runat="server" CssClass="LetterDocDiv">
                                <asp:DataList ID="dataListLetterDoc" runat="server" CssClass="LetterDocOuterTable">
                                    <ItemTemplate>
                                        <table class="LetterDocInnerTable">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblLetterDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblLetterDocId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterDocValue") %>'
                                                        Visible="<%# False %>"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblLetterId" Text='<%# DataBinder.Eval(Container.DataItem, "LetterId") %>'
                                                        Visible="<%# False %>"></asp:Label>
                                                </td>
                                                <td style="text-align: right">
                                                    <asp:ImageButton ID="imgBtnRemoveLetterDoc" runat="server" ImageUrl="~/Images/delete-btn.png"
                                                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="0px" CommandArgument='<%#Eval("LetterDocValue")+"%%"+Eval("LetterId")%>'
                                                        OnClick="imgBtnRemoveLetterDoc_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <SelectedItemStyle BackColor="Gray" />
                                </asp:DataList>
                            </asp:Panel>
                        </td>
                    </tr>
                    <%--<tr>
                <td align="left" valign="top" class="style5">
                    Upload:
                </td>
                <td align="left" valign="top">
                    <asp:Label ID="imgUploadProgress" runat="server" Style="display: none"> <img alt="" src="../../Images/uploading.gif" /></asp:Label>
                    <asp:AsyncFileUpload ID="asyncDocUpload" runat="server" ErrorBackColor="Red" Height="26px"
                        OnClientUploadComplete="uploadComplete" OnClientUploadError="uploadError" OnUploadedComplete="asyncDocUpload_UploadedComplete"
                        ThrobberID="imgUploadProgress" UploaderStyle="Traditional" Width="179px" />
                </td>
                <td align="left" valign="top">
                    &nbsp;
                </td>
            </tr>--%>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Notes:
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtNotes" runat="server" Rows="3" TextMode="MultiLine" Width="246px"></asp:TextBox>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="left" valign="top">
                        </td>
                        <td align="right" valign="top" style="text-align: right;">
                            <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
                            <asp:Button ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
                        </td>
                        <td align="left" valign="top" style="text-align: left;">
                            <div>
                            </div>
                        </td>
                    </tr>--%>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="add-apt-canel-save">
            <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
            <asp:Button ID="btnSaveAction" runat="server" Text="Save Appointment" BackColor="White" />
        </div>
    </div>
</asp:Panel>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAddAppointment" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlAddAppointment"
    DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnCancelAction"
    BehaviorID="mdlPopupAddAppointmentBehaviorId">
</asp:ModalPopupExtender>





<asp:Panel ID="pnlAddApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblAptbaTenantInfoAddPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddAppointmentContactDetails" runat="server"
    DynamicServicePath="" Enabled="True" TargetControlID="lblAddAppointmentContactDetails"
    PopupControlID="pnlAddApointmentContactDetails" DropShadow="true" CancelControlID="imgBtnCloseTenantInfo"
    BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblAddAppointmentContactDetails" runat="server"></asp:Label>



<asp:Panel ID="pnlSuccessPopup" runat="server">
    <asp:ImageButton ID="imgBtnCancelSuccessPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        OnClick="imgBtnCancelSuccessPopup_Click" BorderWidth="0" />
    <div class="apt-success-popup" style="width:550px !Important;height:150px;border:1px solid black;">
        <%--<propertyAddressTag:PropertyAddress ID="webUserCtrlPropAddressSuccessPopup" runat="server"
            MaxValue="10" MinValue="1" />--%>
        <asp:Panel ID="pnlSuccessAppointmentMessage" runat="server" CssClass="pnl-apt-success-msg" style="border:none !important;padding-top:50px !important">
            <asp:Label ID="lblSuccessAppointmentMessage" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </div>
</asp:Panel>
<asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="mdlSuccessPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlSuccessPopup"
    DropShadow="true" CancelControlID="lblDisplaySuccessPopup" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>

<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right:10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" style="padding-right:10px; font-weight:bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right:10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled." style="padding-right:10px;"></asp:Label>
    </div>
   
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    >
</asp:ModalPopupExtender>



<asp:CheckBox ID="ckBoxAddRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />




<asp:Panel runat="server" ID="pnlPropertyAttributesNotes" CssClass="left" BackColor="White"
    Width="470px" Style="max-height: 350px; padding: 15px 5px; top: 32px; border: 1px solid black;
    position: absolute; z-index: 100000;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Notes</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnNotes" runat="server" Style="position: absolute; top: -12px;
        right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="height:200px; overflow:auto; width:100%;">
        <asp:Repeater runat="server" ID="rptPropertyNotes">
            <ItemTemplate>
                <div style='width: 100%; padding: 10px;'>
                    <asp:Label Text='<%# Eval("AttributeName") %>' ID="lblAttribute" Font-Bold="true"
                        runat="server" />
                    <br />
                    <asp:Label Text='<%# Eval("AttributeNotes") %>' ID="lblAttributeNotes" runat="server" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopupNotes" runat="server" DynamicServicePath="" Enabled="True"
    TargetControlID="lblPropertyAttributesNotes" PopupControlID="pnlPropertyAttributesNotes"
    DropShadow="true" CancelControlID="imgBtnNotes" BackgroundCssClass="modalBackground2">
</asp:ModalPopupExtender>
<asp:Label ID="lblPropertyAttributesNotes" runat="server" Text=""></asp:Label>



<!-- start Risk details popup-->
<asp:Panel runat="server" ID="pnlRiskDetails" CssClass="left" BackColor="White" Width="400px"
    Height="200px">
    <div style="font-weight: bold; padding: 10px">
        The customer has following risks recorded against their current details.
    </div>
    <hr />
    <div style="height: 80px; overflow: auto; padding-top: 10px; padding-left: 20px">
        <asp:GridView ID="grdRisk" runat="server" AutoGenerateColumns="False" ShowHeader="false"
            GridLines="None">
            <Columns>
                <asp:BoundField DataField="CATDESC" />
                <asp:BoundField DataField="SUBCATDESC" />
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: center; padding-top: 30px">
        <asp:Button ID="btnOk" runat="server" Text="OK" Width="100px" />
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupRiskDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblRiskDetailsClose" PopupControlID="pnlRiskDetails"
    DropShadow="true" CancelControlID="btnOk" BackgroundCssClass="modalBackground2">
</asp:ModalPopupExtender>
<asp:Label ID="lblRiskDetailsClose" runat="server" Text=""></asp:Label>
<!-- end Risk details popup-->





</div>