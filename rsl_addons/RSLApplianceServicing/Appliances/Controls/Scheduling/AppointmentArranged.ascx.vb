﻿Imports System.Text
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Net
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Net.Mail
Imports System.Threading
Imports System.Globalization
Public Class AppointmentArranged
    Inherits UserControlBase

#Region "Delegates"
    Public Delegate Sub AptbaIn56DaysDelegate(ByVal isDueWithIn56Days As Boolean)
#End Region

#Region "Attributes"

    'Calendar Variables
    Public varWeeklyDT As DataTable = New DataTable()
    'Appointment Arranged Variables
    Dim resultDataSet As DataSet = New DataSet()
    Dim resultDataTable As New DataTable
    Dim check56Days As Boolean = New Boolean
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Public Event onAptbaIn56DaysEvent As AptbaIn56DaysDelegate
    Dim dsAppointments As DataSet = New DataSet()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "ApDate", 1, 30)

    Private _isError As Boolean
    Public Property isError() As Boolean
        Get
            Return _isError
        End Get
        Set(ByVal value As Boolean)
            _isError = value
        End Set
    End Property

#End Region

#Region "Events"
#Region "Page Pre Render Event"
    ''' <summary>
    ''' Page Pre Render Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblActivityPopupMessage, pnlActivityPopupMessage)
            AddHandler ucAppointmentNotes.CloseButtonClicked, AddressOf btnClose_Click
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Appointment Arranged Grid - Events"

#Region "img btn Aptba TenantInfo Click"

    Protected Sub imgbtnApaTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                Me.setTenantsInfo(dstenantsInfo, tblTenantInfoApa)

                Me.mdlPopUpAppointmentArrangedPhone.Show()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "img btn Email Click"

    Protected Sub imgbtnEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim emailDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = emailDescription(0)
            lblStatusHeadingDescription.Text = emailDescription(3)
            lblStatusHeadingMessage.Text = UserMessageConstants.EmailHeading
            If emailDescription(3) = UserMessageConstants.EmailNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not emailDescription(1) = "" And Not IsNothing(emailDescription(1)) Then
                btnEmailSmsResend.CommandArgument = emailDescription(1) + ";" + emailDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not emailDescription(0) = "" Then
                If emailDescription(0) = UserMessageConstants.EmailDescriptionSending Or emailDescription(0) = UserMessageConstants.EmailDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "img btn Sms Click"

    Protected Sub imgbtnSms_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim smsDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = smsDescription(0)
            lblStatusHeadingMessage.Text = UserMessageConstants.SmsHeading
            lblStatusHeadingDescription.Text = smsDescription(3)
            If smsDescription(3) = UserMessageConstants.SmsNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not smsDescription(1) = "" And Not IsNothing(smsDescription(1)) Then
                btnEmailSmsResend.CommandArgument = smsDescription(1) + ";" + smsDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not smsDescription(0) = "" Then
                If smsDescription(0) = UserMessageConstants.SmsDescriptionSending Or smsDescription(0) = UserMessageConstants.SmsDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region
#Region "img btn Push Noticifications Click"

    Protected Sub imgbtnPushNoticification_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim pushnoticificationDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblPushNoticificationStatusDescriptionMessage.Text = pushnoticificationDescription(0)
            lblPushNoticificationStatusHeadingMessage.Text = UserMessageConstants.PushNoticificationHeading
            lblPushNoticificationStatusHeadingDescription.Text = pushnoticificationDescription(6)
            If pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationNotSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greyFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSending Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSendingFailed Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "redFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not pushnoticificationDescription(1) = "" And Not IsNothing(pushnoticificationDescription(1)) Then
                btnPushNoticificationResend.CommandArgument = pushnoticificationDescription(1) + ";" + pushnoticificationDescription(2) + ";" + pushnoticificationDescription(3) + ";" + pushnoticificationDescription(4) + ";" + pushnoticificationDescription(5)
            Else
                btnPushNoticificationResend.CommandArgument = ""
            End If
            If Not pushnoticificationDescription(0) = "" Then
                If pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSending Or pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSent Then
                    btnPushNoticificationResend.Visible = False
                Else
                    btnPushNoticificationResend.Visible = True
                End If
            Else
                btnPushNoticificationResend.Visible = False
            End If
            Me.PushNoticificationPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentArranged.PageIndexChanging
        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppointmentArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentArrangedGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

    Protected Sub grdAppointmentArranged_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBo()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "img btn Appointment notes Click"

    Protected Sub imgbtnAptNotes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btnNotes As ImageButton = CType(sender, ImageButton)
            Dim appointmentId As Integer = CType(btnNotes.CommandArgument, Integer)
            ucAppointmentNotes.loadAppointmentData(appointmentId)
            mdlAppointmentNotesPopup.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdAppointmentArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentArranged.Sorting
        Try
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdAppointmentArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortGrid()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Appointment Arranged Arrow Click"
    Protected Sub imgBtnAppointmentArrangedArrow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim btnImage As ImageButton = CType(sender, ImageButton)
        Dim commandParams() As String = CType(btnImage.CommandArgument, String).Split(";")
        Dim pId As String = commandParams(4)
        Dim appointmentType As String = commandParams(21)

        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False
        Dim locked As Integer = objSchedulingBL.GetArrangedSchedulingPropertyStatus(commandParams(6), isTimeExceeded, isScheduled, appointmentType)
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandParams(6), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandParams(6), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        End If
        If criticalsection = 1 Then
            Try
                If Not commandParams(13) = "" And Not IsNothing(commandParams(13)) Then
                    Dim customerId As Integer = Convert.ToInt32(commandParams(13))
                    Dim riskVulnerability As String = getCustomerRiskVulnerability(customerId) 'index -13 : customerId
                    If Not riskVulnerability.Equals("") Then
                        Dim script As String = "callAlert('" + riskVulnerability + "')"
                        ScriptManager.RegisterStartupScript(Page, Page.GetType, "radalert", script, True)
                        PropertyScheduledPopUp.Hide()
                        mdlPopUpAmendAppointment.Hide()
                    End If
                    pnlAvailalbleAppointments.Visible = True
                Else
                    pnlAvailalbleAppointments.Visible = True
                End If
            Catch ex As Exception
                Throw New Exception("Customer Id is Invalid or Null")
            End Try
            Try
                uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                uiMessageHelper.IsError = False
                'Remove the existing appointments from the session, to show new pins of map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                removeMarkersJavaScript()

                Dim operativeDs As DataSet = New DataSet()
                Dim btn As ImageButton = CType(sender, ImageButton)
                Dim commandArgumentParams() As String = CType(btn.CommandArgument, String).Split(";")
                'below mention sequence is expected in command argument parameters
                '0 index - address
                '1 index - post code
                '2 index - tenancy id
                '3 index - expiry date
                '4 index - property id            
                '5 index - appointment id            
                '6 index - journal id
                '7 index - days
                '8 index - 0
                '9 index - property status
                '10 index - patch id
                '11 index - telephone
                '13 index - customerId
                'get the avialable operatives, their appointments and leaves
                Me.getAvailableOperatives(operativeDs)

                If IsNothing(SessionManager.getApplianceServicingId()) Or SessionManager.getApplianceServicingId() = 0 Then
                    Dim userBl As UsersBL = New UsersBL()
                    Dim inspectionTypeDs As DataSet = New DataSet()
                    userBl.getInspectionTypesByDesc(inspectionTypeDs, "Appliance Servicing")
                    Dim applianceServicingId As Integer = inspectionTypeDs.Tables(0).Rows(0).Item("InspectionTypeId")
                    SessionManager.setApplianceServicingId(applianceServicingId)
                End If

                'set the property id in session so that it 'll be used to open the print letter window
                SessionManager.setPropertyIdForEditLetter(commandArgumentParams(4))

                'fill up the property scheduling bo
                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                selPropSchedulingBo.PropertyAddress = commandArgumentParams(0)
                selPropSchedulingBo.PostCode = commandArgumentParams(1)
                'tenancy id can be null for void properties
                If (String.IsNullOrEmpty(commandArgumentParams(2)) = True) Then
                    selPropSchedulingBo.TenancyId = Nothing
                Else
                    selPropSchedulingBo.TenancyId = CType(commandArgumentParams(2), Integer)
                End If
                'There are some properties which does not have any certificate expiry date for that this check is implemented
                'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
                If (String.IsNullOrEmpty(commandArgumentParams(3)) = True) Then
                    selPropSchedulingBo.CertificateExpiryDate = Nothing
                Else
                    selPropSchedulingBo.CertificateExpiryDate = CType(commandArgumentParams(3), Date)
                End If
                selPropSchedulingBo.PropertyId = commandArgumentParams(4)
                selPropSchedulingBo.AppointmentId = CType(commandArgumentParams(5), Integer)
                selPropSchedulingBo.JournalId = CType(commandArgumentParams(6), Integer)
                selPropSchedulingBo.ExpiryInDays = commandArgumentParams(7)
                selPropSchedulingBo.PropertyStatus = commandArgumentParams(9)
                selPropSchedulingBo.PatchId = CType(commandArgumentParams(10), Integer)
                selPropSchedulingBo.Telephone = commandArgumentParams(11)
                selPropSchedulingBo.CustomerName = commandArgumentParams(12)

                selPropSchedulingBo.CustomerEmail = commandArgumentParams(14)
                selPropSchedulingBo.HouseNumber = commandArgumentParams(15)
                selPropSchedulingBo.Address1 = commandArgumentParams(16)
                selPropSchedulingBo.Address2 = commandArgumentParams(17)
                selPropSchedulingBo.Address3 = commandArgumentParams(18)
                selPropSchedulingBo.TownCity = commandArgumentParams(19)
                selPropSchedulingBo.County = commandArgumentParams(20)
                selPropSchedulingBo.AppointmentType = commandArgumentParams(21)

                If (selPropSchedulingBo.AppointmentType = "Property") Then
                    selPropSchedulingBo.Duration = 1
                Else
                    Dim boilerInfoResultDataSet As DataSet = New DataSet()
                    objSchedulingBL.getSchemeBlockBoilersInfo(boilerInfoResultDataSet, selPropSchedulingBo.JournalId, selPropSchedulingBo.AppointmentType)
                    selPropSchedulingBo.Duration = boilerInfoResultDataSet.Tables(0).Rows.Count
                End If

                'remove the proeprty scheduling bo from session if already exist 
                'this is just to avoid the overlapping of sesssion values between different properties
                SessionManager.RemoveSelectedPropertySchedulingBo()

                'now set the session with selected property values
                SessionManager.SetSelectedPropertySchedulingBo(selPropSchedulingBo)
                'set the property address label in available appointment pupup
                webUserCtrlPropAddressAvailableAppt.setPropertyLabels()
                Dim startDate As DateTime = Date.Now
                txtfromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy")

                'now create the appointment slots and bind data with available appointment grid
                Me.populateAppointments(selPropSchedulingBo, operativeDs, startDate)

                'Register client side Startup Script to show map
                showMapJavaScript()

                mdlPopupAvailableAppointments.Show()
            Catch ex As ThreadAbortException
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try
        Else
            PropertyScheduledPopUp.Show()
        End If
    End Sub
#End Region

#End Region
#Region "Risk & Vulnerability of Customer"
    Private Function getCustomerRiskVulnerability(ByVal customerId As Integer)

        Dim schedulingBL As SchedulingBL = New SchedulingBL()
        Dim riskVulnerabilityResult As DataSet = New DataSet()
        Return schedulingBL.getCustomerRiskVulnerability(riskVulnerabilityResult, customerId)

    End Function
#End Region
#Region "Amend Appointment Popup - Events"

#Region "btn Close Click"
    Protected Sub btnClose_Click()
        Try
            mdlAppointmentNotesPopup.Hide()
            'Me.populateAppointmentArrangedGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "ck Box Refresh Data Set Checked Changed"
    Protected Sub ckBoxRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxRefreshDataSet.CheckedChanged
        Me.ckBoxRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId())
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.mdlPopUpAmendAppointment.Show()

        End Try

    End Sub
#End Region

#Region "ck Box Document Upload Checked Changed"

    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
            End If

            'Register client side Startup Script to show map            
            showMapJavaScript()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopupAvailableAppointments.Show()
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub

#End Region

#Region "ddl Status Selected Index Changed"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            SessionManager.setApaAmendAppointmentClicked(True)
            Dim statusId As Integer = CType(ddlStatus.SelectedValue(), Integer)
            Me.LoadActions(statusId)
            Me.LoadLetters(-1)
            Me.ddlStatus.SelectedValue = statusId
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub
#End Region

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAction.SelectedValue(), Integer)
            Me.LoadLetters(actionId)
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try
    End Sub
#End Region

#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            mdlPopupAvailableAppointments.Show()
            mdlPopUpAmendAppointment.Show()
        End Try
    End Sub

#End Region

#Region "Btn Save Appointment Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click
        Dim isSaved As Boolean = False
        Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session
        Dim startDate As Date = SessionManager.getApaStartDate()
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        Dim SmsStatus As String = UserMessageConstants.SmsNotSent
        Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
        Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
        Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
        Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
        Dim pId As String = ""
        pId = SessionManager.GetSelectedPropertySchedulingBo().PropertyId
        Dim journalId As Integer = SessionManager.GetSelectedPropertySchedulingBo().JournalId
        If Not IsNothing(pId) AndAlso Not pId = "" Then
            If SessionManager.getUserEmployeeId = objSchedulingBL.GetSchedulingPropertyLockedBy(journalId, SessionManager.GetSelectedPropertySchedulingBo().AppointmentType) Then
                Try
                    If (Me.validateAmendAppointment() = True) Then
                        Dim journalHistoryId As Integer = 0
                        journalHistoryId = Me.saveAmendAppointment(PushNoticificationStatus, PushNoticificationDescription)
                        If journalHistoryId > 0 Then
                            'populate appointment to be arranged grid 
                            'Me.populateAppointmentArrangedGrid()
                            'show success message in new popup. Set the property address label in new pupup
                            'webUserCtrlPropAddressSuccessPopup.setPropertyLabels()
                            uiMessageHelper.setMessage(lblSuccessAppointmentMessage, pnlSuccessAppointmentMessage, UserMessageConstants.SaveAmendAppointmentSuccessfully, False)
                            isSaved = True
                            SessionManager.removeActivityLetterDetail()

                            If (SessionManager.GetSelectedPropertySchedulingBo().AppointmentType = "Property") Then
                                ' Start of code to send the SMS'
                                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                                selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()
                                Dim tenancyID As Integer = selPropSchedulingBo.TenancyId
                                Dim dstenantsInfo As New DataSet()
                                objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

                                If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                                    SmsStatus = UserMessageConstants.SmsSending
                                    SmsDescription = UserMessageConstants.SmsDescriptionSending
                                    Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                                    Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                                End If
                                ' End of Code to send the SMS
                                EmailStatus = UserMessageConstants.EmailSending
                                EmailDescription = UserMessageConstants.EmailDescriptionSending

                                Me.sendEmail(journalHistoryId, EmailStatus, EmailDescription)
                                Me.setEmailAndSmsStatusForAppointment(journalHistoryId, SmsStatus, SmsDescription, EmailStatus, EmailDescription)
                            End If

                            Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)

                        Else
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                        End If

                    End If
                Catch ex As ThreadAbortException
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                    End If
                    If isSaved = False Then
                        Me.mdlPopupAvailableAppointments.Show()
                        Me.mdlPopUpAmendAppointment.Show()
                    Else
                        objSchedulingBL.SetSchedulingPropertyStatus(journalId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), SessionManager.GetSelectedPropertySchedulingBo().AppointmentType)
                        Me.mdlPopUpAmendAppointment.Hide()
                        Me.mdlPopupAvailableAppointments.Hide()
                        Me.mdlSuccessPopup.Show()

                    End If

                End Try
            Else
                PropertyScheduledPopUp.Show()
            End If
        End If
    End Sub
#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            cancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Cancel Faults"

    Sub cancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As AppointmentCancellationBO = New AppointmentCancellationBO()
            resultDataSet = SessionManager.getConfirmedGasAppointment()

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim faultsList As StringBuilder = New StringBuilder()
                faultsList.Append(resultDataSet.FaultsList.ToString())
                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                Dim appointmentCancellationForSchemeBlockBo As New AppointmentCancellationForSchemeBlockBO()
                appointmentCancellationForSchemeBlockBo.Notes = txtBoxDescription.Text
                appointmentCancellationForSchemeBlockBo.userId = SessionManager.getAppServicingUserId()
                appointmentCancellationForSchemeBlockBo.AppointmentType = SessionManager.getAppointmentCancellationForSchemeBlock().AppointmentType
                appointmentCancellationForSchemeBlockBo.AppointmentId = SessionManager.getAppointmentCancellationForSchemeBlock().AppointmentId

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text
                objAppointmentCancellationBO.userId = SessionManager.getAppServicingUserId()

                Dim isCanceled As Integer
                'If (Not appointmentCancellationForSchemeBlockBo.AppointmentType.Contains("Property")) Then
                '    isCanceled = objSchedulingBL.saveGasServicingCancellationForSchemeBlock(appointmentCancellationForSchemeBlockBo)
                'Else
                isCanceled = objSchedulingBL.saveGasServicingCancellation(objAppointmentCancellationBO)
                'End If

                If IsDBNull(isCanceled) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False
                    pnlCustomerMessage.Visible = False
                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    SessionManager.removeConfirmedGasAppointment()
                    SessionManager.removeAppointmentCancellationForSchemeBlock()
                    mdlPopUpCancelFaults.Hide()
                    populateAppointmentArrangedGrid()
                    uiMessageHelper.Message = UserMessageConstants.FaultCancelled
                    'pushAppointmnetCancellation()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, False)
                End If

            End If

        End If

    End Sub

#End Region

#Region "set Push Noticification Status For Appointment"
    Private Sub setPushNoticificationStatusForAppointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
        Try
            objSchedulingBL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#Region "set Email And Sms Status For Appointment"
    Private Sub setEmailAndSmsStatusForAppointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
        Try
            objSchedulingBL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close success popup Click"

    Protected Sub imgBtnCancelSuccessPopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancelSuccessPopup.Click
        Try
            Response.Redirect(PathConstants.FuelServicingPath + "?" + PathConstants.Tab + "=" + PathConstants.AppointmentArranged)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal tenantmobile As String, ByRef smsStatus As String, ByRef smsDescription As String)
        Try

            tenantmobile = tenantmobile.Replace(" ", String.Empty)

            If tenantmobile.Length >= 10 Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(lblAppointmentStartTime.Text, lblAppointmentDate.Text)
                'Dim postData As String

                'POSTdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(tenantmobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)


                '  GeneralHelper.sendSMS(body, tenantmobile)
                'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                'Dim javascriptMethod As String = "sendSMS('" + tenantmobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(tenantmobile, body)

                smsStatus = UserMessageConstants.SmsSent
                smsDescription = UserMessageConstants.SmsDescriptionSent
            Else
                smsStatus = UserMessageConstants.SmsSendingFailed
                smsDescription = UserMessageConstants.AppointmentSavedSMSError + UserMessageConstants.InvalidMobile
                'uiMessageHelper.IsError = True
                'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            smsStatus = UserMessageConstants.SmsSendingFailed
            smsDescription = UserMessageConstants.AppointmentSavedSMSError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal journalHistoryId As Integer, ByRef emailStatus As String, ByRef emailDescription As String)

        Try
            Dim objScheduling As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            objScheduling.getAppointmentInfoForEmail(resultDataset, journalHistoryId)
            Dim infoDt As DataTable = resultDataset.Tables(0)
            If (infoDt.Rows.Count > 0) Then
                Dim recepientEmail As String = infoDt.Rows(0)(ApplicationConstants.EmailCol)
                Dim recepientName As String = infoDt.Rows(0)(ApplicationConstants.CustomerNameCol)
                Dim jobSheet As String = infoDt.Rows(0)(ApplicationConstants.JSGCol)
                Dim appointmentDate As String = infoDt.Rows(0)(ApplicationConstants.AppointmentDateCol)
                Dim appointmentTime As String = infoDt.Rows(0)(ApplicationConstants.AppointmentTimeCol)
                If Validation.isEmail(recepientEmail) Then
                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheet, appointmentTime, appointmentDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail
                    Dim mailMessage As New Mail.MailMessage
                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True
                    EmailHelper.sendEmail(mailMessage)
                    emailStatus = UserMessageConstants.EmailSent
                    emailDescription = UserMessageConstants.EmailDescriptionSent
                Else
                    Throw New Exception(UserMessageConstants.InvalidEmail)
                End If
            Else
                Throw New Exception(UserMessageConstants.TanencyTerminated)
            End If
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")

        Catch ex As Exception
            emailStatus = UserMessageConstants.EmailSendingFailed
            emailDescription = UserMessageConstants.AppointmentSavedEmailError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "btn View Letter Click"
    Protected Sub btnViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewLetter.Click
        Try
            If (ddlLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region

#Region "btn Resend Email/Sms Click"
    Protected Sub btnEmailSmsResend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEmailSmsResend.Click
        Try
            Dim cmdarg As String = btnEmailSmsResend.CommandArgument
            Dim EmailStatus As String = UserMessageConstants.EmailNotSent
            Dim SmsStatus As String = UserMessageConstants.SmsNotSent
            Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
            Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
            Dim TenancyId As Integer
            If Not cmdarg = "" Then
                Dim commandParams() As String = cmdarg.Split(";")
                Dim jhId As Integer = Convert.ToInt32(commandParams(0))
                'Dim TenancyId As Integer = Convert.ToInt32(commandParams(1))
                If lblStatusHeadingMessage.Text = UserMessageConstants.EmailHeading Then
                    EmailStatus = UserMessageConstants.EmailSending
                    EmailDescription = UserMessageConstants.EmailDescriptionSending
                    sendEmail(jhId, EmailStatus, EmailDescription)
                    Me.setEmailAndSmsStatusForAppointment(jhId, "", "", EmailStatus, EmailDescription)
                Else
                    If (String.IsNullOrEmpty(commandParams(1) = True)) Then
                        TenancyId = Nothing
                    Else
                        TenancyId = commandParams(1)
                    End If
                    Dim dstenantsInfo As New DataSet()
                    objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, TenancyId)
                    If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                        SmsStatus = UserMessageConstants.SmsSending
                        SmsDescription = UserMessageConstants.SmsDescriptionSending
                        Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                        Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                        Me.setEmailAndSmsStatusForAppointment(jhId, SmsStatus, SmsDescription, "", "")
                    End If
                End If

            End If
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            'Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnPushNoticificationResend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPushNoticificationResend.Click
        Try
            Dim cmdarg As String = btnPushNoticificationResend.CommandArgument
            Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
            Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
            If Not cmdarg = "" Then
                Dim commandParams() As String = cmdarg.Split(";")
                Dim journalHistoryId As Integer = 0
                journalHistoryId = Convert.ToInt32(commandParams(0))
                Dim operativeId As Integer = Convert.ToInt32(commandParams(1))
                Dim appointmentdate As DateTime = Convert.ToDateTime(commandParams(3))
                Dim appointmentstarttime As String = commandParams(4).Split(" ")(1).Split("-")(0)
                Dim appointmentendtime As String = commandParams(4).Split(" ")(1).Split("-")(1)
                Dim address As String = commandParams(2)
                If (journalHistoryId > 0 And appointmentdate = Today()) Then
                    PushNoticificationStatus = UserMessageConstants.PushNoticificationSending
                    PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSending
                    Dim propertyAddress As String = String.Empty
                    propertyAddress = address.Trim()

                    Dim message As String = String.Empty
                    message = GeneralHelper.generatePushNotificationMessage("New", appointmentdate, appointmentstarttime,
                                                            appointmentendtime, propertyAddress)
                    Dim pushnoticicesent As Boolean = GeneralHelper.pushNotificationAppliance(message, operativeId)
                    If pushnoticicesent = True Then
                        PushNoticificationStatus = UserMessageConstants.PushNoticificationSent
                        PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSent
                    Else
                        PushNoticificationStatus = UserMessageConstants.PushNoticificationSendingFailed
                        PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSendingFailed
                    End If

                End If

                Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)
            End If
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            'Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnPushNoticificationCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPushNoticificationCancel.Click
        Me.PushNoticificationPopUp.Hide()

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnEmailSmsCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEmailSmsCancel.Click
        Me.EmailSmsPopUp.Hide()

    End Sub
#End Region

#Region "img Btn close Property Scheduled Click"

    Protected Sub imgPropertyScheduleClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPropertyScheduleClose.Click
        Try
            populateAppointmentArrangedGrid()
            mdlPopupAvailableAppointments.Hide()
            mdlPopUpAmendAppointment.Hide()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "img Btn close Available Appointments Click"

    Protected Sub imgBtnCloseAvailableAppointments_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAvailableAppointments.Click
        Try
            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()
            If SessionManager.getUserEmployeeId() = objSchedulingBL.GetSchedulingPropertyLockedBy(selPropSchedulingBo.JournalId, selPropSchedulingBo.AppointmentType) Then
                objSchedulingBL.SetSchedulingPropertyStatus(selPropSchedulingBo.JournalId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), SessionManager.GetSelectedPropertySchedulingBo().AppointmentType)
            End If
            'objSchedulingBL.setPropertyschedulestatus(selPropSchedulingBo.PropertyId, 0, 0, SessionManager.getUserEmployeeId())
            mdlPopupAvailableAppointments.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Image Button Close Amend Appointment Popup"
    Protected Sub imgBtnCloseAmendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAmendApp.Click
        Dim selPropSchedulingBo As SelectedPropertySchedulingBO = SessionManager.GetSelectedPropertySchedulingBo()
        Me.mdlPopUpAmendAppointment.Hide()
        'Attach Script to show map.        
        showMapJavaScript()

        Me.mdlPopupAvailableAppointments.Show()
    End Sub
#End Region

#Region "lnkBtn App Details Click"

    Protected Sub lnkBtnAppDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkBtnAppDetails.Click
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)
            Dim dstenantsInfo As New DataSet()
            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                setTenantsInfo(dstenantsInfo, tblTenantInfoAmendPopup)
                Me.mdlPopupAvailableAppointments.Show()
                Me.mdlPopUpAmendAppointment.Show()
                Me.mdlPopUpAmenAptContactDetails.Show()
            Else
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.noTenantInformationFound, True)
                Me.mdlPopupAvailableAppointments.Show()
                Me.mdlPopUpAmendAppointment.Show()

            End If

            'Update - updPanelFuelScheduling(Update Panel) to show latest contant
            Dim updPanelFuelScheduling As UpdatePanel = CType(Parent.FindControl("updPanelFuelScheduling"), UpdatePanel)
            updPanelFuelScheduling.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If

        End Try

    End Sub

#End Region
#End Region

#Region "Available Appointments - Intelligent Scheduling Principal Popup - Events"

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the perviously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.DisplayCount = appointmentSlotsDtBo.DisplayCount + 5
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If
            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "chk Patch Checked Changed"
    Protected Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chkPatch As CheckBox = TryCast(sender, CheckBox)

            If chkPatch.Checked = True Then
                'If patch is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the perviously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.IsPatchSelected = chkPatch.Checked
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If
            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
            Me.updPanelAvailableOperatives.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "chk Expiry Date Checked Changed"
    Protected Sub chkExpiryDate_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim chkExpiryDate As CheckBox = TryCast(sender, CheckBox)

            If chkExpiryDate.Checked = True Then
                'If expiry date is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

            'get the previously created appointment slots
            appointmentSlotsDtBo = SessionManager.getAppointmentSlots()

            'increase the count of operatives by 
            appointmentSlotsDtBo.IsDateSelected = chkExpiryDate.Checked
            If Not txtfromDate.Text = Nothing Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim updAppointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(updAppointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                If txtfromDate.Text < Now.Date Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = UserMessageConstants.DateNotValid
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                    appointmentSlotsDtBo.dt.Clear()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                    Exit Sub
                Else
                    uiMessageHelper.IsError = False
                    uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)
                    appointmentSlotsDtBo.FromDate = txtfromDate.Text
                End If
            End If

            SessionManager.setAppointmentSlots(appointmentSlotsDtBo)
            'this 'll add five more operatives in the list appointments
            Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)
            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "grd Available Appointments Row Created"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAvailableAppointments_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAvailableAppointments.RowCreated
        'get the appointment slots from database
        Dim appointmentSlotsDtbo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
        appointmentSlotsDtbo = SessionManager.getAppointmentSlots()

        'get the property scheduling bo . this bo contains the property related data
        Dim propertySchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        propertySchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()

        'There are some properties which does not have any certificate expiry date for that this check is implemented
        'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
        Dim disableExpiryDate As Boolean = False
        If IsNothing(propertySchedulingBo.CertificateExpiryDate) Then
            disableExpiryDate = True
        End If

        'create the grid header
        'If e.Row.RowType = DataControlRowType.Header Then
        createGridHeader(appointmentSlotsDtbo.IsPatchSelected, appointmentSlotsDtbo.IsDateSelected, propertySchedulingBo.ExpiryInDays, disableExpiryDate)
        ' End If


    End Sub
#End Region

#Region "btn Select Click"
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'clear the add appointment popup controls
            Me.resetAmendAppointmentControls()
            'below mention sequence is expected in command argument parameters
            '0 index - operative id
            '1 index - operative name
            '2 index - appointment start time
            '3 index - appointment end time
            '4 index - appointment date                        
            Dim btnSelect As Button = TryCast(sender, Button)
            Dim appointmentInfo As String() = CType(btnSelect.CommandArgument, String).Split(";")
            'this function will set the appropriate values 
            Me.openAmendAppointmentPopup(appointmentInfo)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopUpAmendAppointment.Show()
            Me.mdlPopupAvailableAppointments.Show()
        End Try

    End Sub
#End Region

#Region "TextChanged event for  txtfromDate"

    Protected Sub txtfromDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try

            Dim startDate As DateTime = Date.Now

            If txtfromDate.Text <> "" Then
                Dim dateTime As Date
                Dim valid As Boolean = Date.TryParseExact(txtfromDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
                If valid = False Then
                    txtfromDate.Focus()
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = String.Format(UserMessageConstants.notValidDateFormat)
                    Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                    Me.setRefreshListButtonDisplay(False)
                    Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                    Exit Sub
                End If
                'If expiry date is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForSchedulingMap()
                'remove pins from map.
                removeMarkersJavaScript()
                startDate = Convert.ToDateTime(txtfromDate.Text)
            End If

            If startDate < Now.Date Then
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = UserMessageConstants.DateNotValid
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
                Me.setRefreshListButtonDisplay(False)
                Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
                Exit Sub
            Else
                uiMessageHelper.IsError = False
                uiMessageHelper.resetMessage(lblMessageOnPopUp, pnlMessageOnPopUp)


            End If



            Dim operativeDs As DataSet = New DataSet()
            Me.getAvailableOperatives(operativeDs)

            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            selPropSchedulingBo = SessionManager.GetSelectedPropertySchedulingBo

            'now create the appointment slots and bind data with available appointment grid
            Me.populateAppointments(selPropSchedulingBo, operativeDs, startDate)
            'Attach show map java script
            showMapJavaScript()
            'this 'll add five more operatives in the list appointments
            'Me.refreshTheAppointmentSlots(appointmentSlotsDtBo)

            mdlPopupAvailableAppointments.Show()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageOnPopUp, pnlMessageOnPopUp, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#End Region

#Region "Function"

#Region "Appointment Arranged Grid - Functions"
#Region "search Appointment Arranged"
    Public Sub searchAppointmentArranged(ByRef check56Days As Boolean, ByRef searchText As String, ByVal fuelType As Integer, ByRef statusType As String)
        Try

            Me.setSearchText(searchText)
            Me.setCheck56Days(check56Days)
            Me.setPageSortBo(objPageSortBo)
            Me.setFuelType(fuelType)
            Me.setStatusType(statusType)
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Cancel Reported Faults Click Event"

    Protected Sub btnCancelReportedFaults_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim objCancelAppointment = New AppointmentCancellationBO()
            Dim objCancelAppointmentForSchemeBlock = New AppointmentCancellationForSchemeBlockBO()
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim commandParams() As String = CType(imgBtn.CommandArgument, String).Split(";")

            Dim journalId As String = commandParams(0)
            Dim appointmentType As String = commandParams(1)
            Dim appointmentId As String = commandParams(2)
            Dim propertyStatus As String = commandParams(3)

            objCancelAppointment.FaultsList = journalId
            objCancelAppointmentForSchemeBlock.AppointmentType = appointmentType
            objCancelAppointmentForSchemeBlock.AppointmentId = appointmentId
            SessionManager.setConfirmedGasAppointment(objCancelAppointment)
            SessionManager.setAppointmentCancellationForSchemeBlock(objCancelAppointmentForSchemeBlock)

            pnlErrorMessage.Visible = False
            pnlCancelMessage.Visible = False
            pnlCustomerMessage.Visible = False
            pnlSaveButton.Visible = True
            txtBoxDescription.Text = ""
            txtBoxDescription.Enabled = True

            mdlPopUpCancelFaults.Show()
            If (propertyStatus.Contains("Cancelled")) Then
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

#End Region

#Region "populate Appointment ArrangedGrid "

    Public Sub populateAppointmentArrangedGrid()
        Dim totalCount As Integer = 0
        Dim status As String = String.Empty
        Dim schemeId As Integer = -1
        Dim patchId As Integer = -1

        If Request.QueryString(PathConstants.Status) IsNot Nothing AndAlso Request.QueryString(PathConstants.Status) <> String.Empty Then
            If Request.QueryString(PathConstants.Status).Equals(PathConstants.NoEntry) Then
                status = ApplicationConstants.noEntry
            ElseIf Request.QueryString(PathConstants.Status).Equals(PathConstants.Aborted) Then
                status = ApplicationConstants.aborted
            End If
            If Request.QueryString(PathConstants.SchemeId) IsNot Nothing AndAlso Request.QueryString(PathConstants.SchemeId) <> String.Empty Then
                schemeId = Convert.ToInt32(Request.QueryString(PathConstants.SchemeId))
            End If
            If Request.QueryString(PathConstants.PatchId) IsNot Nothing AndAlso Request.QueryString(PathConstants.PatchId) <> String.Empty Then
                patchId = Convert.ToInt32(Request.QueryString(PathConstants.PatchId))
            End If
        End If
        totalCount = objSchedulingBL.getAppointmentsArrangedList(resultDataSet, Me.getCheck56Days(), getSearchText(), objPageSortBo, patchId, schemeId, Me.getFuelType(), -1)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAppointmentArranged.Visible = False
        Else
            grdAppointmentArranged.Visible = True
            ViewState.Add(ViewStateConstants.AptaDataSet, resultDataSet)
            grdAppointmentArranged.VirtualItemCount = totalCount
            grdAppointmentArranged.DataSource = resultDataSet
            grdAppointmentArranged.DataBind()
        End If
        setVirtualItemCountViewState(totalCount)
        Me.setResultDataSetViewState(resultDataSet)
    End Sub
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
#End Region

#Region "set Tenant Info"

    Protected Sub setTenantInfo(ByVal customerId)


        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblApaEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblApaFisrtname.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblApaMobile.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblApaTel.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        'plhdrAptbaPopUp.Visible = True
        Me.mdlPopUpAppointmentArrangedPhone.Show()

    End Sub
#End Region

#Region "Sort Grid"
    Protected Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppointmentArrangedGrid()
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppointmentArranged.DataSource = dvSortedView
            grdAppointmentArranged.DataBind()
        End If

        ''''''''''''old Code''''''''''''
        'Dim resultDataSet As DataSet = New DataSet()
        'Dim resultDataTable As New DataTable

        ''get the dataset that was stored in view state
        'resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        ''find the table in dataset
        'resultDataTable = resultDataSet.Tables(0)

        'resultDataTable.DefaultView.Sort = Me.sortExpression + " " + Me.sortDirection
        'grdAppointmentArranged.DataSource = resultDataTable
        'grdAppointmentArranged.DataBind()

    End Sub
#End Region

#Region "set Search Text"
    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptaSearchString) = searchText
        End If
    End Sub
#End Region

#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptaSearchString), String)
        End If
    End Function
#End Region


#Region "set Check 56 Days"
    Public Sub setCheck56Days(ByVal checkBoxVal As Boolean)
        ViewState(ViewStateConstants.AptaCheck56Days) = checkBoxVal
    End Sub
#End Region

#Region "get Check 56 Days"
    Public Function getCheck56Days() As Boolean
        If (IsNothing(ViewState(ViewStateConstants.AptaCheck56Days))) Then
            Return True
        Else
            Return CType(ViewState(ViewStateConstants.AptaCheck56Days), Boolean)
        End If
    End Function
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Set Fuel Type"
    Protected Sub setFuelType(ByRef fuelType As Integer)
        ViewState(ViewStateConstants.FuelType) = fuelType
    End Sub
#End Region


#Region "Set Status Type"
    Protected Sub setStatusType(ByRef statusType As String)
        ViewState(ViewStateConstants.StatusType) = statusType
    End Sub
#End Region



#Region "Get Fuel Type"
    Protected Function getFuelType() As Integer
        If (IsNothing(ViewState(ViewStateConstants.FuelType))) Then
            Return -1
        Else
            Return CType(ViewState(ViewStateConstants.FuelType), Integer)
        End If
    End Function

#End Region

#Region "Get Status Type"
    Protected Function getStatusType() As String
        If (IsNothing(ViewState(ViewStateConstants.StatusType)) Or ViewState(ViewStateConstants.StatusType) = "All") Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.StatusType), String)
        End If
    End Function

#End Region


#End Region

#Region "Amend Appointment Popup - Functions"

#Region "Open Amend Appointment Popup"

    Public Sub openAmendAppointmentPopup(ByVal appointmentInfo As String())
        Try

            'below mention sequence is expected in appointmentInfo array
            '0 index - operative id
            '1 index - operative name
            '2 index - appointment start time
            '3 index - appointment end time
            '4 index - appointment date                    
            Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
            'retreive the information that has been already saved in session related to this selected property
            selPropSchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

            'fill up the information of appointment against selected property
            selPropSchedulingBo.OperativeId = CType(appointmentInfo(0), Integer)
            selPropSchedulingBo.OperativeName = appointmentInfo(1)
            selPropSchedulingBo.AppointmentStartTime = CType(appointmentInfo(2), Date)
            selPropSchedulingBo.AppointmentEndTime = CType(appointmentInfo(3), Date)
            selPropSchedulingBo.AppointmentDate = CType(appointmentInfo(4), Date)
            selPropSchedulingBo.AppointmentEndDate = CType(appointmentInfo(5), Date)

            'save this above information in session again
            SessionManager.setSelectedPropertySchedulingBo(selPropSchedulingBo)

            'fill up the address
            lblLocation.Text = selPropSchedulingBo.PropertyAddress
            'set the appointment date in label
            'This(fucntion) will convert the date in following format e.g Mon 12th Nov 2012
            lblAppointmentDate.Text = GeneralHelper.convertDateToUsCulture(selPropSchedulingBo.AppointmentDate)
            'set the appointment start time into 24 hrs format string
            lblAppointmentStartTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentStartTime)
            'set the appointment end time into 24 hrs format string
            lblAppointmentEndTime.Text = GeneralHelper.ConvertTimeTo24hrsFormat(selPropSchedulingBo.AppointmentEndTime)
            'set the operative name 
            lblOperative.Text = selPropSchedulingBo.OperativeName

            'declare the data set. This will be used to fetch the already saved data of appointment
            Dim dsAppointments As DataSet = New DataSet()

            'get & set the already saved data of appointment
            objSchedulingBL.getSelectedAppointmentDetails(dsAppointments, selPropSchedulingBo.AppointmentId)

            lblLocation.Text = selPropSchedulingBo.PropertyAddress
            If (ddlStatus.Items.Count <= 0) Then
                Me.LoadSatus(dsAppointments.Tables(0).Rows(0).Item(1))
                Me.LoadActions(dsAppointments.Tables(0).Rows(0).Item(1))
            Else
                Me.LoadSatus(dsAppointments.Tables(0).Rows(0).Item(1))
            End If
            txtNotes.Text = dsAppointments.Tables(0).Rows(0).Item(9).ToString()

            'To implement joint tenants information.
            lnkBtnAppDetails.Visible = GeneralHelper.isPropertyStatusLet(selPropSchedulingBo.PropertyStatus)
            lnkBtnAppDetails.CommandArgument = selPropSchedulingBo.TenancyId
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            Me.mdlPopupAvailableAppointments.Show()
            Me.mdlPopUpAmendAppointment.Show()

        End Try
    End Sub

#End Region

#Region "Load Satus"
    Protected Sub LoadSatus(Optional ByVal defaultValue As Integer = -1)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = ApplicationConstants.StatusId
        ddlStatus.DataTextField = ApplicationConstants.Title
        ddlStatus.DataBind()

        If (defaultValue <> -1) Then
            ddlStatus.SelectedValue = defaultValue
        End If

        'To Implement CR, Remove the status 'appointment to be arranged' from the drop down list when arranging or rearranging an appointment
        If ddlStatus.Items.Count > 0 AndAlso ddlStatus.Items.Contains(ddlStatus.Items.FindByText("Appointment to be arranged")) Then
            ddlStatus.Items.RemoveAt(ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Appointment to be arranged")))
        End If

    End Sub
#End Region

#Region "Load Actions"
    Protected Sub LoadActions(ByRef statusId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAction.DataSource = resultDataSet
        ddlAction.DataValueField = ApplicationConstants.ActionId
        ddlAction.DataTextField = ApplicationConstants.Title
        ddlAction.DataBind()
        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)


        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()

            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub LoadLetters(ByRef actionId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim objResourceBL As ResourcesBL = New ResourcesBL()
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlLetter.DataSource = resultDataSet
        ddlLetter.DataValueField = ApplicationConstants.LetterId
        ddlLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlLetter.DataBind()
        ddlLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Validate Amend Appointment"
    Protected Function validateAmendAppointment()
        Dim success As Boolean = True
        If Me.ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)
        End If

        Return success
    End Function
#End Region

#Region "Save Amend Appointment"
    Public Function saveAmendAppointment(ByRef pushNoticificationStatus As String, ByRef pushNoticificationDescription As String)
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        Dim selPropSechdulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
        Dim journalHistoryId As String

        'get the property and operative information from sesssion 
        selPropSechdulingBo = SessionManager.getSelectedPropertySchedulingBo()

        'Amend Appointment Information
        objScheduleAppointmentsBO.Duration = selPropSechdulingBo.Duration
        objScheduleAppointmentsBO.JournelId = selPropSechdulingBo.JournalId
        objScheduleAppointmentsBO.PropertyId = selPropSechdulingBo.PropertyId
        objScheduleAppointmentsBO.CustomerEmail = selPropSechdulingBo.CustomerEmail
        objScheduleAppointmentsBO.CustomerEmailStatus = selPropSechdulingBo.CustomerEmailStatus
        objScheduleAppointmentsBO.CreatedBy = SessionManager.getAppServicingUserId()
        objScheduleAppointmentsBO.InspectionTypeId = SessionManager.getApplianceServicingId()
        objScheduleAppointmentsBO.AppointmentDate = lblAppointmentDate.Text
        objScheduleAppointmentsBO.AppointmentEndDate = selPropSechdulingBo.AppointmentEndDate
        objScheduleAppointmentsBO.StatusId = ddlStatus.SelectedValue
        objScheduleAppointmentsBO.ActionId = ddlAction.SelectedValue
        objScheduleAppointmentsBO.LetterId = ddlLetter.SelectedValue
        objScheduleAppointmentsBO.Notes = txtNotes.Text
        objScheduleAppointmentsBO.DocumentPath = GeneralHelper.getDocumentUploadPath()
        If (System.DateTime.Parse(Me.lblAppointmentEndTime.Text).TimeOfDay > System.DateTime.Parse("12:00").TimeOfDay) Then
            objScheduleAppointmentsBO.Shift = "PM"
        Else
            objScheduleAppointmentsBO.Shift = "AM"
        End If
        objScheduleAppointmentsBO.AppStartTime = Me.lblAppointmentStartTime.Text
        objScheduleAppointmentsBO.AppEndTime = Me.lblAppointmentEndTime.Text
        objScheduleAppointmentsBO.AssignedTo = selPropSechdulingBo.OperativeId
        objScheduleAppointmentsBO.AppointmentId = selPropSechdulingBo.AppointmentId

        'Save Standard Letter
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objScheduleAppointmentsBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objScheduleAppointmentsBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objScheduleAppointmentsBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objScheduleAppointmentsBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objScheduleAppointmentsBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objScheduleAppointmentsBO.IsDocumentAttached = False
            End If
        End If
        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        journalHistoryId = objSchedulingBL.AmendAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

        If (journalHistoryId > 0 And objScheduleAppointmentsBO.AppointmentDate = Today()) Then
            pushNoticificationStatus = UserMessageConstants.PushNoticificationSending
            pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSending
            Dim propertyAddress As String = String.Empty
            propertyAddress = lblLocation.Text.Trim()

            Dim message As String = String.Empty
            message = GeneralHelper.generatePushNotificationMessage("Existing", objScheduleAppointmentsBO.AppointmentDate, objScheduleAppointmentsBO.AppStartTime,
                                                    objScheduleAppointmentsBO.AppEndTime, propertyAddress)
            Dim pushnoticicesent As Boolean = GeneralHelper.pushNotificationAppliance(message, objScheduleAppointmentsBO.AssignedTo)
            If pushnoticicesent = True Then
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSent
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSent
            Else
                pushNoticificationStatus = UserMessageConstants.PushNoticificationSendingFailed
                pushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSendingFailed
            End If
        End If

        Return journalHistoryId

    End Function

#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "set Contact Info"
    Protected Sub setContactInfo(ByVal customerId)


        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblFName.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblMobileNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblTelNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        'plhdrAptbaPopUp.Visible = True
        Me.mdlPopUpAmenAptContactDetails.Show()

    End Sub
#End Region

#Region "Reset Amend Appointment Controls"
    Protected Sub resetAmendAppointmentControls()

        Me.lblActivityPopupMessage.Text = String.Empty
        Me.LoadLetters(-1)
        Me.txtNotes.Text = String.Empty

        Dim dt As DataTable = New DataTable()
        Dim dv As DataView = New DataView(dt)
        dt = Me.getLetterDocList()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dt.Clear()
            Me.setLetterDocList(dt)
        End If

        Me.dataListLetterDoc.DataSource = dt
        Me.dataListLetterDoc.DataBind()

        SessionManager.removeActivityLetterDetail()

    End Sub
#End Region

#End Region

#Region "Available Appointments - Intelligent Scheduling Principal - Functions"

#Region "get Available Operatives"
    ''' <summary>
    ''' This function retrieves the available operatives, their appointments and their leaves
    ''' </summary>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>
    Private Sub getAvailableOperatives(ByRef operativeDs As DataSet)

        Dim objFaultAppointmentBl As SchedulingBL = New SchedulingBL()
        objFaultAppointmentBl.getAvailableOperatives(operativeDs)

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            SessionManager.setAvailableOperativesDs(operativeDs)
        End If
    End Sub
#End Region

#Region "Populate Appointment"
    ''' <summary>
    ''' This function calls the business logic layer and creates the appointment slots and 
    ''' then binds that datatable with grid
    ''' </summary>
    ''' <param name="propertySchedulingBo"></param>
    ''' <param name="operativeDs"></param>
    ''' <remarks></remarks>

    Private Sub populateAppointments(ByVal propertySchedulingBo As SelectedPropertySchedulingBO, ByVal operativeDs As DataSet, Optional ByVal startDate As DateTime = Nothing)
        Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()
        If Not startDate = Nothing Then
            appointmentSlotsDtBo.FromDate = startDate
        End If
        appointmentSlotsDtBo = objSchedulingBL.getAppointments(operativeDs.Tables(ApplicationConstants.OperativesDt), operativeDs.Tables(ApplicationConstants.LeavesDt), operativeDs.Tables(ApplicationConstants.AppointmentsDt), propertySchedulingBo, startDate)

        'Bind the appointment slots with asp.net control
        bindAppointmentSlotsWithControl(appointmentSlotsDtBo)

        'Save existing appointments for visible operatives to session to display pins on map.
        saveExistingAppointmentForVisibleOperatives(appointmentSlotsDtBo.dt, operativeDs.Tables(ApplicationConstants.AppointmentsDt))
    End Sub
#End Region

#Region "bind Appointment Slots With Control"
    ''' <summary>
    ''' Bind the appointment slots with asp.net control
    ''' </summary>
    ''' <param name="appointmentSlotsDtBo"></param>
    ''' <remarks></remarks>
    Private Sub bindAppointmentSlotsWithControl(ByVal appointmentSlotsDtBo As AppointmentSlotsDtBO)

        If (appointmentSlotsDtBo.dt.Rows.Count > 0) Then
            Me.setRefreshListButtonDisplay(True)
            Me.grdAvailableAppointments.DataSource = appointmentSlotsDtBo.dt
            Me.grdAvailableAppointments.DataBind()
            Dim btnRefreshList As Button = CType(grdAvailableAppointments.FooterRow.FindControl("btnRefreshList"), Button)
            btnRefreshList.Focus()
        Else
            Me.setRefreshListButtonDisplay(False)
            Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)
        End If

    End Sub
#End Region

#Region "refresh The Appointment Slots"
    ''' <summary>
    ''' This function refresh the appointment list and adds more operatives in the list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub refreshTheAppointmentSlots(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO)

        Dim propertySchedulingBo As SelectedPropertySchedulingBO

        'get the property scheduling bo
        propertySchedulingBo = SessionManager.GetSelectedPropertySchedulingBo()

        Dim operativeDs As DataSet = New DataSet()
        operativeDs = SessionManager.getAvailableOperativesDs()

        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and expiry date 
        objSchedulingBL.addMoreAppointments(operativeDs.Tables(ApplicationConstants.OperativesDt), operativeDs.Tables(ApplicationConstants.LeavesDt), operativeDs.Tables(ApplicationConstants.AppointmentsDt), propertySchedulingBo, appointmentSlotsDtBo)

        'save the appointment slots in session
        'First save a copy of object in session before applying the order function.
        SessionManager.setAppointmentSlots(appointmentSlotsDtBo)

        'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment         
        objSchedulingBL.orderAppointmentsByDistance(appointmentSlotsDtBo)

        'Bind the appointment slots with asp.net control
        bindAppointmentSlotsWithControl(appointmentSlotsDtBo)

        'Save existing appointments for visible operatives to session to display pins on map.
        saveExistingAppointmentForVisibleOperatives(appointmentSlotsDtBo.dt, operativeDs.Tables(ApplicationConstants.AppointmentsDt))

        'Register client side script to update markers
        updateMarkersJavaScript()
    End Sub
#End Region

#Region "Display Msg In Empty Grid"
    ''' <summary>
    ''' This function will be called if there are no appointments in the grid. This 'll create the empty row with message in it.
    ''' </summary>
    ''' <param name="appointmentSlotsDtBo"></param>
    ''' <param name="msg"></param>
    ''' <remarks></remarks>
    Private Sub displayMsgInEmptyGrid(ByRef appointmentSlotsDtBo As AppointmentSlotsDtBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        appointmentSlotsDtBo.dt.Rows.Add(appointmentSlotsDtBo.dt.NewRow())
        grdAvailableAppointments.DataSource = appointmentSlotsDtBo.dt
        grdAvailableAppointments.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        appointmentSlotsDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "avail-no-record-msg")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grdAvailableAppointments.Rows(0).Cells.Count
        grdAvailableAppointments.Rows(0).Cells.Clear()
        grdAvailableAppointments.Rows(0).Cells.Add(New TableCell())
        grdAvailableAppointments.Rows(0).Cells(0).ColumnSpan = columncount
        grdAvailableAppointments.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Grid Header"
    ''' <summary>
    ''' This function create the 2nd header of available appointments grid
    ''' </summary>
    ''' <param name="isPatchSelected"></param>
    ''' <param name="isDateSelected"></param>
    ''' <param name="expireInDays"></param>
    ''' <remarks></remarks>
    Private Sub createGridHeader(ByVal isPatchSelected As Boolean, ByVal isDateSelected As Boolean, ByVal expireInDays As String, Optional ByVal disableDateCheckBox As Boolean = False)

        'add patch checkbox

        chkPatch.Checked = isPatchSelected

        'add date checkbox
        chkDate.Checked = isDateSelected

        'add from date textbox

        'There are some properties which does not have any certificate expiry date for that this check is implemented
        'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
        If disableDateCheckBox = True Then
            chkDate.Enabled = False
        Else
            chkDate.Enabled = True
        End If
        'AddHandler chkDate.CheckedChanged, AddressOf chkExpiryDate_CheckedChanged

        'add default value under default label
        lblDefault.Text = expireInDays + " days"

    End Sub
#End Region

#Region "hide Show Refresh List Button"
    ''' <summary>
    ''' This function will be while assigning the datasource to grid. This 'll be called from aspx page.
    ''' this function checks the state of refresh list button and then deicides its visibility
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowRefreshListButton()
        If getRefreshListButtonDisplay() = False Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "hide Show Notes Button"
    ''' <summary>
    ''' hide Show Notes Button
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowAppointmentNotesButton(ByVal notes As String)
        If notes.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Set / Get Refresh List Button Display"

#Region "get Refresh List Button Display"
    Private Function getRefreshListButtonDisplay()
        Dim display As Boolean = False
        If ViewState(ViewStateConstants.RefreshListButtonDisplay) IsNot Nothing Then
            display = CType(ViewState(ViewStateConstants.RefreshListButtonDisplay), Boolean)
        End If
        Return display
    End Function
#End Region

#Region "set Refresh List Button Display"
    Private Sub setRefreshListButtonDisplay(ByVal display As Boolean)
        ViewState(ViewStateConstants.RefreshListButtonDisplay) = display
    End Sub
#End Region
#End Region

#End Region

#Region "Save existing appointments in session to show on map"

    Private Sub saveExistingAppointmentForVisibleOperatives(ByVal appointmentSlots As DataTable, ByVal existingAppointments As DataTable)

        'Get appointment of only visible operatives.
        Dim appointmentsForMap = (From extapt In existingAppointments.AsEnumerable()
                                    Join aptSlot In appointmentSlots.AsEnumerable() On extapt("OperativeId").ToString() Equals aptSlot("OperativeId").ToString()
                                    Select extapt)

        'Applied grouping to get only on appointment of any property.
        appointmentsForMap = appointmentsForMap.GroupBy(Function(row) {row.Field(Of String)("Address"), row.Field(Of String)("TownCity"), row.Field(Of String)("County")}).Select(Function(group) group.First())

        Dim appointmentsForMapDt As DataTable = Nothing

        If appointmentsForMap.Count() > 0 Then

            Dim existingAppointmentOnMap = SessionManager.getExistingAppointmentsForSchedulingMap()

            If Not IsNothing(existingAppointmentOnMap) Then
                'Only get new appointments/properties and exclude the appointments/properties already shown on map.
                appointmentsForMap = From aptForMap In appointmentsForMap
                                     From extAptOnMap In existingAppointmentOnMap.AsEnumerable().Where(Function(row) _
                                            aptForMap.Field(Of String)("Address") = row.Field(Of String)("Address") _
                                            AndAlso aptForMap.Field(Of String)("TownCity") = row.Field(Of String)("TownCity") _
                                            AndAlso aptForMap.Field(Of String)("County") = row.Field(Of String)("County")).DefaultIfEmpty()
                                     Where (IsNothing(extAptOnMap))
                                     Select aptForMap
            End If

            If appointmentsForMap.Count() > 0 Then
                'Join may cause repetition of rows so distinct is applied.
                appointmentsForMapDt = appointmentsForMap.Distinct().CopyToDataTable()
            End If
        End If

        SessionManager.setExistingAppointmentsForSchedulingMap(appointmentsForMapDt)

    End Sub

#End Region

#Region "Register client side Startup Script to show map"

    Public Sub showMapJavaScript()
        Dim selPropSchedulingBo As SelectedPropertySchedulingBO = SessionManager.GetSelectedPropertySchedulingBo()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "showMapAptA", "showMap('" + aptAMap.ClientID + "','" + selPropSchedulingBo.PropertyAddress.Replace("'", "\'") + "');", True)
    End Sub

#End Region

#Region "Register client side script to update markers on Google Map. on appointments refresh."

    Private Sub updateMarkersJavaScript()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "updateMarkersAptA", "addOriginMarkers();", True)
    End Sub

#End Region

#Region "Register client side script to remove existing markers from Map"

    Private Sub removeMarkersJavaScript()
        'Register client side script to remove markers on Google Map.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "deleteMarkersAptA", "deleteMarkers();", True)
    End Sub

#End Region


#Region "Rebind the Grid to prevent show empty lines for last page only"
    ''' <summary>
    ''' Rebind the Grid to prevent show empty lines for last page only
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reBindGridForLastPage()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Address", 1, 30)
        objPageSortBo = getPageSortBo()

        If grdAppointmentArranged.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdAppointmentArranged.PageCount Then
            grdAppointmentArranged.VirtualItemCount = getVirtualItemCountViewState()
            grdAppointmentArranged.PageIndex = objPageSortBo.PageNumber - 1
            grdAppointmentArranged.DataSource = getResultDataSetViewState()
            grdAppointmentArranged.DataBind()
        End If

    End Sub

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"
    ''' <summary>
    ''' Virtual Item Count Set
    ''' </summary>
    ''' <param name="VirtualItemCount"></param>
    ''' <remarks></remarks>
    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"
    ''' <summary>
    ''' Virtual Item Count Get
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"
    ''' <summary>
    ''' Virtual Item Count Remove
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region
#End Region

End Class