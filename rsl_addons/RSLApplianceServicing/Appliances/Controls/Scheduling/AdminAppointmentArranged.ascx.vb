﻿Imports System.Text
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Net.Mail
Imports System.Threading

Public Class AdminAppointmentArranged
    Inherits UserControlBase


#Region "Delegates"
    Public Delegate Sub AptbaIn56DaysDelegate(ByVal isDueWithIn56Days As Boolean)
#End Region

#Region "Attributes"

    'Calendar Variables
    Public varWeeklyDT As DataTable = New DataTable()
    'Appointment Arranged Variables
    Dim resultDataSet As DataSet = New DataSet()
    Dim resultDataTable As New DataTable
    Dim check56Days As Boolean = New Boolean
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Public Event onAptbaIn56DaysEvent As AptbaIn56DaysDelegate
    Dim dsAppointments As DataSet = New DataSet()
    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "Address", 1, 30)

    Private _isError As Boolean
    Public Property isError() As Boolean
        Get
            Return _isError
        End Get
        Set(ByVal value As Boolean)
            _isError = value
        End Set
    End Property

#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            uiMessageHelper.resetMessage(lblAmendMsg, msgAmendPanel)
            uiMessageHelper.resetMessage(lblActivityPopupMessage, pnlActivityPopupMessage)

            'Open Popup for Amend Existing Appointment from <AppointmentArranged>
            If (Not IsNothing(Request.QueryString("op")) AndAlso (SessionManager.getApaAmendAppointmentClicked() = False) AndAlso (Not IsPostBack)) Then
                Me.setCalendarInfo()
                Me.AmendAppointmentPopup()
                ' Me.mdlAmendPopUpCalendar.Show()
                'For Successful Amendment of existing Appointment from <AppointmentArranged>  
            ElseIf (Not IsNothing(Request.QueryString("chk")) And Request.QueryString("chk") = 1 And SessionManager.getAptbaIsSuccess() = True) Then
                'Me.populateAppointmentArrangedGrid()
                Me.searchAppointmentArranged(False, "Search", Me.getFuelType())
                Me.setCalendarInfo()
                'ApplicationConstants.amendIsSuccess = False
                SessionManager.setAptbaIsSuccess(False)
                uiMessageHelper.setMessage(lblAmendMsg, msgAmendPanel, UserMessageConstants.SaveAmendAppointmentSuccessfully, False)
            ElseIf (Not IsNothing(Request.QueryString("tId")) AndAlso SessionManager.getTenancyClicked() = False AndAlso (Not IsPostBack)) Then
                Try
                    'Me.searchAppointmentArranged(False, "Search")
                    'Me.setCalendarInfo()
                    Dim dstenantsInfo As New DataSet()

                    objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, CInt(Request.QueryString("tId")))

                    If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                        setTenantsInfo(dstenantsInfo, tblAmendCalanderTenantInfo)
                        Me.mdlPopupAmendCalanderTenantInfo.Show()
                    End If
                    SessionManager.setTenancyClicked(True)
                Catch ex As ThreadAbortException
                    uiMessageHelper.Message = ex.Message
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally

                    If uiMessageHelper.IsError Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
                    End If

                End Try
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Function"

#Region "search Appointment Arranged"
    Public Sub searchAppointmentArranged(ByRef check56Days As Boolean, ByRef searchText As String, ByVal fuelType As Integer)
        Try
            Me.setFuelType(fuelType)
            Me.setSearchText(searchText)
            Me.setCheck56Days(check56Days)
            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentArrangedGrid()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "populate Appointment ArrangedGrid "

    Public Sub populateAppointmentArrangedGrid()
        Dim totalCount As Integer = 0
        Dim schemeId As Integer = -1
        Dim patchId As Integer = -1
        Dim status As String = String.Empty
        If Request.QueryString(PathConstants.Status) IsNot Nothing AndAlso Request.QueryString(PathConstants.Status) <> String.Empty Then
            If Request.QueryString(PathConstants.Status).Equals(PathConstants.NoEntry) Then
                Status = ApplicationConstants.noEntry
            End If
            If Request.QueryString(PathConstants.SchemeId) IsNot Nothing AndAlso Request.QueryString(PathConstants.SchemeId) <> String.Empty Then
                schemeId = Convert.ToInt32(Request.QueryString(PathConstants.SchemeId))
            End If
            If Request.QueryString(PathConstants.PatchId) IsNot Nothing AndAlso Request.QueryString(PathConstants.PatchId) <> String.Empty Then
                patchId = Convert.ToInt32(Request.QueryString(PathConstants.PatchId))
            End If
        End If
        totalCount = objSchedulingBL.getAppointmentsArrangedList(resultDataSet, Me.getCheck56Days(), getSearchText(), objPageSortBo, patchId, schemeId, Me.getFuelType(), -1)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAppointmentArranged.Visible = False
        Else
            grdAppointmentArranged.Visible = True
            ViewState.Add(ViewStateConstants.AptaDataSet, resultDataSet)
            grdAppointmentArranged.VirtualItemCount = totalCount
            grdAppointmentArranged.DataSource = resultDataSet
            grdAppointmentArranged.DataBind()
        End If
    End Sub
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
    'End - Changed By Aamir Waheed on May 28, 2013.
#End Region

#Region "display Schedule Appointment"
    Protected Sub displayScheduleAppointment()
        'Variables Declaration 
        Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session

        'Get calander weekly data table from session. (If not exists it will return new DataTable)
        varWeeklyDT = SessionManager.getAmendCalanderWeeklyDataTable()

        If ((varWeeklyDT.Columns.Count <= 0 Or SessionManager.getApaAmendCalendarPageingClicked() = False) And (Not SessionManager.isAmendCalanderWeeklyDTSessionExists())) Then
            Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
            Dim strAppointment As StringBuilder = New StringBuilder()
            Dim dsEmployeeList As DataSet = New DataSet()
            Dim dsEmployeeLeaves As DataSet = New DataSet()
            Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
            Dim strEmployeeIds As StringBuilder = New StringBuilder()
            Dim rowCount As Integer
            Dim drLeaves() As DataRow
            Dim startDate As Date
            'Calendar code start here
            If (IsNothing(Request.QueryString("e"))) Then
                'ApplicationConstants.startDate = apptScheduleParam(6) 'Date.Today
                SessionManager.setApaStartDate(apptScheduleParam(6).Substring(0, 10))
            End If
            'Seek for Monday as starting and Sunday ending day of the Month
            If (SessionManager.getApaStartDate().ToString("dddd") <> "Monday") Then
                objCalendarUtilities.calculateApaMonday(SessionManager.getApaStartDate())
            End If
            If (Request.QueryString("e") <> "p") Then
                ' ApplicationConstants.endDate = DateAdd("d", 7, ApplicationConstants.startDate)
                SessionManager.setApaEndDate(DateAdd("d", 6, SessionManager.getApaStartDate()))
                'SessionManager.setApaEndDate(DateAdd("d", 7, SessionManager.getApaStartDate()))
            End If


            objSchedulingBL.getEngineersListForAppointment(dsEmployeeList)
            'Getting Appointments Information between specific dates
            objScheduleAppointmentsBO.StartDate = SessionManager.getApaStartDate()
            objScheduleAppointmentsBO.ReturnDate = SessionManager.getApaEndDate()
            objSchedulingBL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, dsAppointments)
            dsAppointments.Tables(0).DefaultView.ToTable(True, "APPOINTMENTID")
            If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Gas Engineer exist
                'Creating weekly column of Datatable

                'To reduce the error "Colmun " " already belong to data table.
                If (varWeeklyDT.Columns.Contains(" ")) Then
                    varWeeklyDT.Columns.Remove(" ")
                End If


                varWeeklyDT.Columns.Add(" ")
                'Creating Columns based on Engineers names
                For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                    objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString() + ":" + apptScheduleParam(7) + ":" + apptScheduleParam(5), varWeeklyDT)
                    strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")
                Next
                'Getting Employees Leaves Information
                objScheduleAppointmentsBO.StartDate = SessionManager.getApaStartDate()
                objScheduleAppointmentsBO.ReturnDate = SessionManager.getApaEndDate()
                objScheduleAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
                objSchedulingBL.getEngineerLeavesDetail(objScheduleAppointmentsBO, dsEmployeeLeaves)
                'End Getting Employees Leaves Information

                'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
                dsAppointments.Tables(0).Columns.Add("Distance")
                Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
                'Dim dr() As DataRow = dsAppointments.Tables(0).Select("(AppointmentDate>= '" + ApplicationConstants.startDate.ToString("MM/dd/yyyy") + "' and AppointmentDate<='" + ApplicationConstants.endDate.ToString("MM/dd/yyyy") + "')", "ASSIGNEDTO ASC")

                For drdsAppointments = 0 To dsAppointments.Tables(0).Rows.Count - 1
                    dsAppointments.Tables(0).Rows(drdsAppointments).Item("Distance") = 0 'objCalendarUtilities.CalculateDistance(apptScheduleParam(1), dsAppointments.Tables(0).Rows(drdsAppointments).Item(6).ToString())
                Next

                'Drawing Weekly Calendar
                startDate = SessionManager.getApaStartDate()
                For dayCount = 0 To 6
                    scheduledAppt(0) = DateAdd("d", dayCount, startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)

                    rowCount = 1
                    For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                        'Adding Leaves Information
                        drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                        If (drLeaves.Count > 0) Then
                            For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px;text-align:left'><img src='../../images/arrow_right.png' /><span style='margin-right:10px;margin-left:13px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></div><br/>")
                            Next
                        End If

                        For rdCount = 0 To dsAppointments.Tables(0).Rows.Count - 1

                            'Check the appointments of the engineers in Datarow
                            If (GeneralHelper.getUKCulturedDateTime(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate").ToString()).ToString("dddd") = ApplicationConstants.daysArray(dayCount) And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() = dsAppointments.Tables(0).Rows(rdCount).Item("ASSIGNEDTO").ToString()) Then 'System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(1)).ToString("dddd")
                                'If dsAppointments.Tables(0).Rows(rdCount).Item(7) = 3 Then
                                '    Dim dsAppointments.Tables(0).RowsEngInfo() As DataRow = dsEmployeeList.Tables(0).Select("APPOINTMENTID=" + dsAppointments.Tables(0).Rows(rdCount).Item(10).ToString())
                                '    strAppointment.Append("<a href='../../Views/Scheduling/AdminScheduling.aspx?op=" + drEngInfo(0).Item(0).ToString() + ":" + drEngInfo(0).Item(1) + ":" + drEngInfo(0).Item(2).ToString() + ":" + dr(rdCount).Item(5).ToString() + "'>")
                                'End If
                                'Checking Shift Type

                                'if appointment type is gas then it 'll have color according to status other wise appointment box 'll be yelloow
                                If dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") = "Gas" Then
                                    'Checking Appointment Id
                                    If (CInt(apptScheduleParam(5)) = dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTID")) Then
                                        'strAppointment.Append("<div class='detailAmendBox' id='currentAppointment' tabindex='-1' style='padding:4px'><img src='../../images/arrow_right.png' /><span style='margin-right:10px;margin-left:13px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME") + "-" + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME") + "<b>&nbsp;GAS</b></span><br />")
                                        strAppointment.Append("<div class='detailAmendBox' id='currentAppointment' tabindex='-1' style='padding:4px'><img src='../../images/arrow_right.png' /><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;GAS</b></span><br />")
                                        'Checking Distance selected from selection box
                                    ElseIf (dsAppointments.Tables(0).Rows(rdCount).Item("Distance") <= CInt(ddlAmendDistance.SelectedItem.Value)) Then
                                        strAppointment.Append("<div class='detailFilterBox' style='padding:4px'><img src='../../images/arrow_right.png' /><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;GAS</b></span><br />")
                                        'Check other appointments
                                    Else
                                        strAppointment.Append("<div class='detailBox' style='background-color:#FFF;padding:4px'><img src='../../images/arrow_right.png' /><span style='margin-right:10px;margin-left:13px;'>" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate") + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;GAS</b></span><br />")
                                    End If
                                Else
                                    'if appointment other than gas then appointment box 'll be yellow
                                    strAppointment.Append("<div class='detailBox' style='background-color:#ffef99;padding:4px'><img src='../../images/arrow_right.png'><span style='margin-right:10px;margin-left:13px;'>" + CType(CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate"), Date).Date + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTSTARTTIME"), Date).ToString("HH:mm") + "-" + CType(CType(dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentDate"), Date).Date + " " + dsAppointments.Tables(0).Rows(rdCount).Item("APPOINTMENTENDTIME"), Date).ToString("HH:mm") + "<b>&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") + "</b></span> <br />")
                                End If
                                'Rendering Mobile #
                                'If (dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() <> "" And dsAppointments.Tables(0).Rows(rdCount).Item("AppointmentType") = "Gas") Then
                                '    strAppointment.Append("<asp:ImageButton ID='imgbtnCalanderPhone" + rdCount + "' runat='server' ImageUrl='~/Images/img_cell.png' BorderWidth='0' CommandArgument='" + dsAppointments.Tables(0).Rows(rdCount).Item(15).ToString() + "'  />")
                                '    'strAppointment.Append("<img src='../../images/img_cell.png' /> <span style='margin-left:3px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() + "</span><br />")
                                'End If
                                strAppointment.Append("<div style='margin-left:25px;'>")
                                'Rending Customer Name
                                'If (dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() <> "") Then
                                '    strAppointment.Append("" + dsAppointments.Tables(0).Rows(rdCount).Item(2).ToString() + "<br/>")
                                'End If
                                'Rending Address
                                If (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "" AndAlso GeneralHelper.isPropertyStatusLet(dsAppointments.Tables(0).Rows(rdCount).Item("PropertyStatus").ToString())) Then
                                    strAppointment.Append("<a href='AdminScheduling.aspx?tId=" + dsAppointments.Tables(0).Rows(rdCount).Item("TenancyId").ToString() + "'><img src='../../Images/img_cell.png' style='border-width:0px' /> </a>" + dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + ", ")
                                    strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("PostCode").ToString() + "<br/>")
                                ElseIf (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "") Then
                                    strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + ", ")
                                    strAppointment.Append(dsAppointments.Tables(0).Rows(rdCount).Item("PostCode").ToString() + "<br/>")
                                End If
                                'Adding Distance to Appointment
                                'strAppointment.Append("<span style='margin-right:2px;'>Distance:&nbsp;&nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item("Distance").ToString() + " m</span><br/>")                                '
                                'strAppointment.Append("<span style='margin-right:2px;'>Next:&nbsp;&nbsp; 0 m <br/></span>")

                                strAppointment.Append("<span style='margin-right:2px;'><b>" + dsAppointments.Tables(0).Rows(rdCount).Item("Title") + "</b></span></div></div><br />")

                                'End If
                            End If

                        Next

                        'Changed the if else condition to if only to show appointments along with leaves.
                        'So commented out this End it and moved it to top.
                        'End If

                        If (strAppointment.ToString() <> "") Then
                            scheduledAppt(rowCount) = strAppointment.ToString()
                            strAppointment.Clear()
                        Else
                            scheduledAppt(rowCount) = ""
                        End If
                        rowCount = rowCount + 1
                    Next

                    varWeeklyDT.Rows.Add(scheduledAppt)

                Next
                'Else
                '    uiMessageHelper.setMessage(lblMsg, msgPanel, UserMessageConstants.GasEngNotFound, True)
            End If

            'Save calander weekly data table to session.
            SessionManager.setAmendCalanderWeeklyDataTable(varWeeklyDT)

        End If
        'ApplicationConstants.amendAppointmentClicked = False
        SessionManager.setApaAmendAppointmentClicked(False)
        lblMonth.Text = MonthName(SessionManager.getApaStartDate().Month) + " " + Year(SessionManager.getApaStartDate()).ToString()
        If (apptScheduleParam.Count > 0) Then
            lblAddress.Text = apptScheduleParam(0)
            lblExpiryDays.Text = apptScheduleParam(8)
        End If
    End Sub

#End Region

#Region "set Tenant Info"

    Protected Sub setTenantInfo(ByVal customerId)


        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblApaEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblApaFisrtname.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblApaMobile.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblApaTel.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        'plhdrAptbaPopUp.Visible = True
        Me.mdlPopUpAppointmentArrangedPhone.Show()

    End Sub
#End Region

#Region "img btn Calendar Details"

    Protected Sub imgbtnCalendarDetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim btn As ImageButton = CType(sender, ImageButton)
        Dim apptSchedultParam() As String = CType(btn.CommandArgument, String).Split(";")
        Dim pId As String = apptSchedultParam(4)
        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False
        Dim locked As Integer = objSchedulingBL.getArrangedPropertyschedulestatus(pId, isTimeExceeded, isScheduled, "Property")
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
        End If
        If criticalsection = 1 Then
            Try
                'Dim btn As ImageButton = CType(sender, ImageButton)
                'Dim apptSchedultParam() As String = CType(btn.CommandArgument, String).Split(";")
                SessionManager.setApaParam(apptSchedultParam) 'Storing Selected Property Information
                'ApplicationConstants.apptScheduleParam = apptSchedultParam
                SessionManager.setPropertyIdForEditLetter(apptSchedultParam(4))


                'Commented out varWeeklyDT and removed it from session so it can be created again.
                'varWeeklyDT = New DataTable()
                SessionManager.removeAmendCalanderWeeklyDataTable()

                'ApplicationConstants.amendAppointmentClicked = True
                SessionManager.setApaAmendAppointmentClicked(True)
                'System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "createFixedHeader();", True)

                Me.setCalendarInfo()
                SessionManager.setApaAmendCalendarPageingClicked(False)
                'ApplicationConstants.amendCalendarPageingClicked = False
                ''Alert: The certificate expiry date for this property is prior to the date of the appointment you have chosen.
                'Dim expiryDate As Date = CType(apptSchedultParam(3), Date)

                'Set Certificate Expiry Date in Session
                Dim startDate As New DateTime(1970, 1, 1)
                If Not String.IsNullOrEmpty(apptSchedultParam(3)) Then
                    SessionManager.setCertificateExpiryDate((DateTime.ParseExact(apptSchedultParam(3), "dd/MM/yyyy", Nothing) - startDate).TotalSeconds)
                Else
                    'If expiry date is not available set 0 seconds to database.
                    SessionManager.setCertificateExpiryDate(0)
                End If

                SessionManager.setTenancyClicked(False)
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try
        Else
            PropertyScheduledPopUp.Show()
        End If

    End Sub
#End Region

#Region "set Schedule Apppoint Popup"

    Public Sub setCalendarInfo()
        Me.displayScheduleAppointment()
        Me.mdlAmendPopUpCalendar.Show()

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdAppointmentArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentArranged.Sorting
        Try
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdAppointmentArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortGrid()

            ''''''''óld code''''''''
            'If IsNothing(ViewState(ViewStateConstants.SortDirection)) Then
            '    ViewState(ViewStateConstants.SortDirection) = e.SortDirection
            'End If
            'Me.sortExpression = e.SortExpression

            'Me.setSortDirection()
            'Me.populateAppointmentArrangedGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Sort Grid"
    Protected Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppointmentArrangedGrid()
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppointmentArranged.DataSource = dvSortedView
            grdAppointmentArranged.DataBind()
        End If

        ''''''''''''old Code''''''''''''
        'Dim resultDataSet As DataSet = New DataSet()
        'Dim resultDataTable As New DataTable

        ''get the dataset that was stored in view state
        'resultDataSet = ViewState(ViewStateConstants.AptaDataSet)

        ''find the table in dataset
        'resultDataTable = resultDataSet.Tables(0)

        'resultDataTable.DefaultView.Sort = Me.sortExpression + " " + Me.sortDirection
        'grdAppointmentArranged.DataSource = resultDataTable
        'grdAppointmentArranged.DataBind()

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentArranged.PageIndexChanging
        Try
            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppointmentArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentArrangedGrid()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "set Search Text"
    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptaSearchString) = searchText
        End If
    End Sub
#End Region

#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptaSearchString), String)
        End If
    End Function
#End Region

#Region "set Check 56 Days"
    Public Sub setCheck56Days(ByVal checkBoxVal As Boolean)
        ViewState(ViewStateConstants.AptaCheck56Days) = checkBoxVal
    End Sub
#End Region

#Region "set Check 56 Days"
    Public Function getCheck56Days() As Boolean
        If (IsNothing(ViewState(ViewStateConstants.AptaCheck56Days))) Then
            Return True
        Else
            Return CType(ViewState(ViewStateConstants.AptaCheck56Days), Boolean)
        End If
    End Function
#End Region

#Region "Load Inspection Types in DropDown"
    Public Sub loadInspectionTypes(ByRef dropDownRef As DropDownList, ByVal defaultValue As Integer)
        Dim objUsersBL As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        objUsersBL.getInspectionTypes(resultDataSet)
        dropDownRef.DataSource = resultDataSet
        dropDownRef.DataValueField = ApplicationConstants.InspectionId
        dropDownRef.DataTextField = ApplicationConstants.InspectionName
        dropDownRef.DataBind()
        ' dropDownRef.Items.Add(New ListItem("Select Type", ApplicationConstants.DropDownDefaultValue))
        dropDownRef.SelectedValue = defaultValue
    End Sub
#End Region

#Region "Load Satus"
    Protected Sub LoadSatus(ByVal defaultValue As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataSet)
        ddlStatus.DataSource = resultDataSet
        ddlStatus.DataValueField = ApplicationConstants.StatusId
        ddlStatus.DataTextField = ApplicationConstants.Title
        ddlStatus.DataBind()
        'ddlStatus.Items.Add(New ListItem("Select Status", ApplicationConstants.DropDownDefaultValue))
        ddlStatus.SelectedValue = defaultValue

        'To Implement CR, Remove the status 'appointment to be arranged' from the drop down list when arranging or rearranging an appointment
        'Action : Removed the status 'appointment to be arranged' from drop down list.

        If ddlStatus.Items.Count > 0 AndAlso ddlStatus.Items.Contains(ddlStatus.Items.FindByText("Appointment to be arranged")) Then
            ddlStatus.Items.RemoveAt(ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Appointment to be arranged")))
        End If

    End Sub
#End Region

#Region "Load Engineers"
    Protected Sub LoadEngineers()
        Dim addAppointmentParam() As String = SessionManager.getAmendApaParam() 'Getting Operative Name, Id from session

        Dim dsEmployeeList As DataSet = New DataSet()
        Dim newListItem As ListItem
        objSchedulingBL.getEngineersListForAppointment(dsEmployeeList)
        For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
            newListItem = New ListItem(dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), dsEmployeeList.Tables(0).Rows(dsColCount).Item(0))
            ddlOperative.Items.Add(newListItem)
        Next
        ddlOperative.SelectedValue = addAppointmentParam(0)

    End Sub
#End Region

#Region "Load Actions"
    Protected Sub LoadActions(ByRef statusId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim resultDataSet As DataSet = New DataSet()
        objStatusBL.getActionsByStatusId(statusId, resultDataSet)
        ddlAction.DataSource = resultDataSet
        ddlAction.DataValueField = ApplicationConstants.ActionId
        ddlAction.DataTextField = ApplicationConstants.Title
        ddlAction.DataBind()
        ddlAction.Items.Add(New ListItem("Select Action", ApplicationConstants.DropDownDefaultValue))
        ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Populate Letter Doc List Control"
    Protected Sub populateLetterDocListControl(ByRef type As String, ByVal letterDocValue As String)

        'If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
        '    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        'Else
        '    Dim dt As DataTable = New DataTable()
        '    Dim dr As DataRow

        '    dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
        '    dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
        '    dt.Columns.Add(ApplicationConstants.LetterDocDeleteColumn)

        '    If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

        '    Else
        '        dt = Me.getLetterDocList()
        '    End If

        '    dr = dt.NewRow()


        '    If (type = ApplicationConstants.LetterWord) Then
        '        'Initially This 'll be something like this 10232012073544
        '        Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

        '        ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
        '        dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
        '        dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
        '        dr(ApplicationConstants.LetterDocDeleteColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue

        '    ElseIf type = ApplicationConstants.DocumentWord Then

        '        Dim documentName As String = String.Empty
        '        documentName = Session(SessionConstants.DocumentName).ToString()
        '        Session(SessionConstants.DocumentName) = Nothing

        '        dr(ApplicationConstants.LetterDocNameColumn) = documentName
        '        dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
        '        dr(ApplicationConstants.LetterDocDeleteColumn) = ApplicationConstants.DocPrefix + documentName
        '    End If


        '    dt.Rows.Add(dr)
        '    Dim dv As DataView = New DataView(dt)

        '    Me.setLetterDocList(dt)
        '    dataListLetterDoc.DataSource = dv
        '    dataListLetterDoc.DataBind()
        'End If

        'if user didn't selected the letter from letter drop down
        If ddlLetter.SelectedValue = "-1" And type = ApplicationConstants.LetterWord Then
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)
        Else
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            dt.Columns.Add(ApplicationConstants.LetterDocNameColumn)
            dt.Columns.Add(ApplicationConstants.LetterDocValueColumn)
            dt.Columns.Add(ApplicationConstants.LetterIdColumn)

            If (IsNothing(ViewState(ViewStateConstants.LetterDocList))) Then

            Else
                dt = Me.getLetterDocList()
            End If

            dr = dt.NewRow()


            If (type = ApplicationConstants.LetterWord) Then
                'Initially This 'll be something like this 10232012073544
                Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")

                ViewState(ViewStateConstants.UniqueStringLength) = (ApplicationConstants.LetterPrefix + uniqueString).Length()
                dr(ApplicationConstants.LetterDocNameColumn) = ddlLetter.SelectedItem.Text
                'the below line will save the string something like this in datarow: SL_1023201207354423
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.LetterPrefix + uniqueString + letterDocValue
                dr(ApplicationConstants.LetterIdColumn) = letterDocValue

            ElseIf type = ApplicationConstants.DocumentWord Then

                Dim documentName As String = String.Empty
                documentName = Session(SessionConstants.DocumentUploadName).ToString()
                Session(SessionConstants.DocumentUploadName) = Nothing

                dr(ApplicationConstants.LetterDocNameColumn) = documentName
                dr(ApplicationConstants.LetterDocValueColumn) = ApplicationConstants.DocPrefix + documentName
                dr(ApplicationConstants.LetterIdColumn) = 0
            End If


            dt.Rows.Add(dr)
            Dim dv As DataView = New DataView(dt)

            Me.setLetterDocList(dt)
            dataListLetterDoc.DataSource = dv
            dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Load Letters"
    Protected Sub LoadLetters(ByRef actionId As Integer)
        Dim objStatusBL As StatusBL = New StatusBL()
        Dim objResourceBL As ResourcesBL = New ResourcesBL()
        Dim resultDataSet As DataSet = New DataSet()
        objResourceBL.getLettersByActionId(actionId, resultDataSet)
        ddlLetter.DataSource = resultDataSet
        ddlLetter.DataValueField = ApplicationConstants.LetterId
        ddlLetter.DataTextField = ApplicationConstants.LetterTitle
        ddlLetter.DataBind()
        ddlLetter.Items.Add(New ListItem("Select Letter", ApplicationConstants.DropDownDefaultValue))
        ddlLetter.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "get Letter Doc List"
    Protected Function getLetterDocList() As DataTable
        Return CType(ViewState(ViewStateConstants.LetterDocList), DataTable)
    End Function
#End Region

#Region "Set Letter Doc List"
    Protected Sub setLetterDocList(ByRef letterDocList As DataTable)
        ViewState(ViewStateConstants.LetterDocList) = letterDocList
    End Sub
#End Region

#Region "remove Item Letter Doc List "

    Protected Sub removeItemLetterDocList(ByVal itemId As String)
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn) <> itemId _
                Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                Me.setLetterDocList(dt)
            Else
                dt.Clear()
                Me.setLetterDocList(dt)
            End If



            Dim dv As DataView = New DataView(dt)
            Me.dataListLetterDoc.DataSource = dv
            Me.dataListLetterDoc.DataBind()
        End If
    End Sub
#End Region

#Region "Validate Amend Appointment"
    Protected Function validateAmendAppointment()
        Dim success As Boolean = True
        'If Me.ddlInspectionType.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
        '    success = False
        '    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionType, True)
        'Else
        If IsNothing(Me.txtAmendDate.Text) Or Me.txtAmendDate.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionDate, True)
        ElseIf System.DateTime.Parse(Me.ddlAppEndTime.SelectedItem.Value).TimeOfDay <= System.DateTime.Parse(Me.ddlAppStartTime.SelectedItem.Value).TimeOfDay Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentTime, True)

            'ElseIf Me.ddlStatus.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            '    success = False
            '    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionStatus, True)
        ElseIf Me.valideStartEndTime() > 0 Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.duplicateAppointmentTime, True)
        ElseIf Me.ddlAction.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            success = False
            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectAction, True)

            'To Implement CR, Notes should not be mandatory when adding a new appointment
            'Action : Commented out the ElseIf condition for extNotes
            'ElseIf IsNothing(Me.txtNotes.Text) Or Me.txtNotes.Text = String.Empty Then
            '    success = False
            '    uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionNotes, True)

        End If

        Return success
    End Function
#End Region

#Region "Populate Appointment Start/End Dropdowns"

    Private Sub PopulateAppointmentDropdown(ByVal startTime As String, ByVal endTime As String, ByVal appointmentDate As DateTime)
        'Variables Declaration
        Dim newListItem As ListItem

        For i = 0 To ApplicationConstants.appointmentTime.Count - 1
            'If (appointmentDate = txtAmendDate.Text Or txtAmendDate.Text <> "") Then
            'If ((Date.Compare(CDate(ApplicationConstants.appointmentTime(i)), CDate(startTime)) < 0) Or (Date.Compare(CDate(ApplicationConstants.appointmentTime(i)), CDate(endTime)) > 0)) Then
            newListItem = New ListItem(ApplicationConstants.appointmentTime(i), ApplicationConstants.appointmentTime(i))
            ddlAppStartTime.Items.Add(newListItem)
            'End If
            'If ((Date.Compare(CDate(ApplicationConstants.appointmentTime(i)), CDate(endTime)) > 0)) Then
            newListItem = New ListItem(ApplicationConstants.appointmentTime(i), ApplicationConstants.appointmentTime(i))
            ddlAppEndTime.Items.Add(newListItem)
            'End If

            'End If
        Next


        'To Implement CR, Can the drop down selection be reduced to start at 6:00 and ends at 20:00 hours
        'Action : Commented out the old newListItem, to stop adding 23:59 and added 20:00 in place
        'newListItem = New ListItem("23:59", "23:59")
        newListItem = New ListItem("20:00", "20:00")

        ddlAppEndTime.Items.Add(newListItem)

        setEndTimeSelectedindex(ddlAppStartTime, ddlAppEndTime)

        'ddlAppStartTime.SelectedIndex = 0
        'ddlAppEndTime.SelectedIndex = ddlAppStartTime.SelectedIndex + 1

    End Sub

#End Region

#Region "Open Amend Appointment Popup"

    Public Sub AmendAppointmentPopup()
        Try
            Dim startDate As Date = SessionManager.getApaStartDate()
            Me.hdnAmendExpiryDate.Value = SessionManager.getCertificateExpiryDate()
            Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session
            lblMonth.Text = MonthName(startDate.Month) + " " + Year(startDate).ToString()
            If (apptScheduleParam.Count > 0) Then
                lblAddress.Text = apptScheduleParam(0)
                lblExpiryDays.Text = apptScheduleParam(8)
            End If
            'Variable Declaration
            Dim dsAppointments As DataSet = New DataSet()

            'Code Starts from here
            objSchedulingBL.getSelectedAppointmentDetails(dsAppointments, CInt(apptScheduleParam(5)))
            lblLocation.Text = apptScheduleParam(0)

            SessionManager.setAmendApaParam(Request.QueryString("op").Split(":"))

            If (txtAmendDate.Text = "") Then
                txtAmendDate.Text = Date.Today
            End If
            Me.loadInspectionTypes(Me.ddlInspectionType, dsAppointments.Tables(0).Rows(0).Item(2))
            If (ddlOperative.Items.Count <= 0) Then
                Me.LoadEngineers()
            End If

            If (ddlStatus.Items.Count <= 0) Then
                Me.LoadSatus(dsAppointments.Tables(0).Rows(0).Item(1))
                Me.LoadActions(dsAppointments.Tables(0).Rows(0).Item(1))
            Else
                Me.LoadSatus(ddlStatus.SelectedItem.Value)
            End If

            'Show/Hide phoneicon (Link Button): Only show if property status is let other wise hide it.
            lnkBtnAppDetails.Visible = GeneralHelper.isPropertyStatusLet(apptScheduleParam(10))
            'CommandArdument: tenancy ID.
            lnkBtnAppDetails.CommandArgument = apptScheduleParam(2)

            txtNotes.Text = dsAppointments.Tables(0).Rows(0).Item(9).ToString()
            If (ddlAppStartTime.Items.Count <= 0 Or ddlAppEndTime.Items.Count <= 0) Then
                Me.PopulateAppointmentDropdown(dsAppointments.Tables(0).Rows(0).Item(6).ToString(), dsAppointments.Tables(0).Rows(0).Item(7).ToString(), dsAppointments.Tables(0).Rows(0).Item(5))
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            ' Me.mdlAmendPopUpCalendar.Show()
            Me.mdlPopUpAmendAppointment.Show()

        End Try
    End Sub

#End Region

#Region "Amend Appointment Information"
    Public Function AmendAppointment(ByRef pushNoticificationStatus As String, ByRef pushNoticificationDescription As String)
        Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session
        Dim addAppointmentParam() As String = SessionManager.getAmendApaParam() 'Getting Operative Name, Id from session
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        'Amend Appointment Information
        objScheduleAppointmentsBO.Duration = ApplicationConstants.DefaultSchedulingHour
        objScheduleAppointmentsBO.JournelId = addAppointmentParam(2)
        objScheduleAppointmentsBO.PropertyId = apptScheduleParam(4)
        objScheduleAppointmentsBO.CreatedBy = SessionManager.getAppServicingUserId()
        objScheduleAppointmentsBO.InspectionTypeId = ddlInspectionType.SelectedValue
        objScheduleAppointmentsBO.AppointmentDate = System.DateTime.Parse(txtAmendDate.Text) 'CType(txtAmendDate.Text, Date)
        objScheduleAppointmentsBO.AppointmentEndDate = objScheduleAppointmentsBO.AppointmentDate
        objScheduleAppointmentsBO.StatusId = ddlStatus.SelectedValue
        objScheduleAppointmentsBO.ActionId = ddlAction.SelectedValue
        objScheduleAppointmentsBO.LetterId = ddlLetter.SelectedValue
        objScheduleAppointmentsBO.Notes = txtNotes.Text
        objScheduleAppointmentsBO.DocumentPath = GeneralHelper.getDocumentUploadPath()
        If (System.DateTime.Parse(Me.ddlAppEndTime.SelectedItem.Value).TimeOfDay > System.DateTime.Parse("12:00").TimeOfDay) Then
            objScheduleAppointmentsBO.Shift = "PM"
        Else
            objScheduleAppointmentsBO.Shift = "AM"
        End If
        objScheduleAppointmentsBO.AppStartTime = ddlAppStartTime.SelectedItem.Value
        objScheduleAppointmentsBO.AppEndTime = ddlAppEndTime.SelectedItem.Value
        objScheduleAppointmentsBO.AssignedTo = ddlOperative.SelectedItem.Value
        objScheduleAppointmentsBO.AppointmentId = apptScheduleParam(5)

        'Save Standard Letter
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim letterQuery = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.LetterPrefix) _
            Select dataRow).ToList()

            If letterQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsLetterAttached = True
                Dim dtLetter As DataTable = New DataTable()
                dtLetter = letterQuery.CopyToDataTable()
                objScheduleAppointmentsBO.LetterList = New List(Of Integer)

                Dim row As DataRow
                For Each row In dtLetter.Rows
                    objScheduleAppointmentsBO.LetterList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn).ToString().Substring(ViewState(ViewStateConstants.UniqueStringLength)), Integer))
                Next
            Else
                objScheduleAppointmentsBO.IsLetterAttached = False
            End If

            'Save Attached Documents Information
            Dim docQuery = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.LetterDocValueColumn).Contains(ApplicationConstants.DocPrefix) _
                Select dataRow).ToList()

            If docQuery.Count > 0 Then
                objScheduleAppointmentsBO.IsDocumentAttached = True
                Dim dtDoc As DataTable = New DataTable()
                dtDoc = docQuery.CopyToDataTable()
                objScheduleAppointmentsBO.DocList = New List(Of String)
                Dim row As DataRow
                For Each row In dtDoc.Rows
                    objScheduleAppointmentsBO.DocList.Add(CType(row.Item(ApplicationConstants.LetterDocValueColumn), String))
                Next
            Else
                objScheduleAppointmentsBO.IsDocumentAttached = False
            End If
        End If
        Dim savedLetterDt As DataTable = New DataTable()
        savedLetterDt = SessionManager.getActivityLetterDetail()

        Return objSchedulingBL.AmendAppointmentInfo(objScheduleAppointmentsBO, savedLetterDt)

    End Function

#End Region

#Region "is Letter Already Exist"
    Protected Function isLetterAlreadyExist(ByVal letterId As String)
        Dim result As Boolean = False
        Dim dt As DataTable = New DataTable()
        dt = Me.getLetterDocList()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
            Where _
                dataRow.Field(Of String)(ApplicationConstants.LetterIdColumn) = letterId _
            Select dataRow).ToList()
            'Query the data table
            If query.Count > 0 Then
                result = True
            End If
        End If


        Return result
    End Function
#End Region

#Region "remmove Item Activity Letter Detail "
    Protected Sub remmoveItemActivityLetterDetail(ByVal letterId As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SessionManager.getActivityLetterDetail()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) <> letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                dt = query.CopyToDataTable()
                SessionManager.setActivityLetterDetail(dt)
            Else
                dt.Clear()
                SessionManager.setActivityLetterDetail(dt)
            End If
        End If
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Validate Appointment Start/End Time"
    Protected Function valideStartEndTime()
        'Getting Appointments Information between specific dates
        Dim appStartDate As Date = SessionManager.getApaStartDate()
        Dim appEndDate As Date = SessionManager.getApaEndDate()
        Dim addAppointmentParam() As String = SessionManager.getAmendApaParam() 'Getting Operative Name, Id from session
        Dim objScheduleAppointmentsBO As ScheduleAppointmentsBO = New ScheduleAppointmentsBO()
        objScheduleAppointmentsBO.StartDate = appStartDate
        objScheduleAppointmentsBO.ReturnDate = appEndDate
        objSchedulingBL.getScheduledAppointmentsDetail(objScheduleAppointmentsBO, dsAppointments)

        Dim startDate As New DateTime(1970, 1, 1)
        Dim startTimeInSec, endTimeInSec As Integer
        startTimeInSec = (System.DateTime.Parse(txtAmendDate.Text + " " + ddlAppStartTime.SelectedItem.Value) - startDate).TotalSeconds
        endTimeInSec = (System.DateTime.Parse(txtAmendDate.Text + " " + ddlAppEndTime.SelectedItem.Value) - startDate).TotalSeconds
        Dim strStartFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtAmendDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec<=" + startTimeInSec.ToString() + " and endTimeInSec>=" + startTimeInSec.ToString() + ")"
        Dim strendFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtAmendDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec<=" + endTimeInSec.ToString() + " and endTimeInSec>=" + endTimeInSec.ToString() + ")"
        Dim strBetweenFilter As String = "ValidateAppointmentDate= '" + System.DateTime.Parse(txtAmendDate.Text) + "' and ASSIGNEDTO=" + addAppointmentParam(0) + " and (startTimeInSec>=" + startTimeInSec.ToString() + " and endTimeInSec<=" + endTimeInSec.ToString() + ")"
        Return dsAppointments.Tables(0).Select(strStartFilter, "startTimeInSec ASC").Count _
            + dsAppointments.Tables(0).Select(strendFilter, "startTimeInSec ASC").Count _
            + dsAppointments.Tables(0).Select(strBetweenFilter, "startTimeInSec ASC").Count
    End Function
#End Region

#Region "Set End Time Drop Down Selected index - On Start Time Index change"

    ''' <summary>
    ''' Set end time 1 hour from start time.
    '''  1 hour from start time is not available set to the last item of ddlEndTime.
    ''' This function takes two dropdown by reference and change End Time dropdown in function.
    ''' </summary>
    ''' <param name="ddlStartTime"></param>
    ''' <param name="ddlEndTime"></param>
    ''' <remarks></remarks>
    Private Sub setEndTimeSelectedindex(ByRef ddlStartTime As DropDownList, ByRef ddlEndTime As DropDownList)
        'Set end time 1 hour from start time
        'If 1 hour from start time is not available set to the last item of ddlEndTime

        If ddlStartTime.SelectedIndex + 4 < ddlEndTime.Items.Count Then
            ddlEndTime.SelectedIndex = ddlStartTime.SelectedIndex + 4
        Else
            ddlEndTime.SelectedIndex = ddlEndTime.Items.Count - 1
        End If

    End Sub

#End Region

#End Region

#Region "Events"

#Region "img btn Aptba TenantInfo Click"

    Protected Sub imgbtnApaTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        'To implement joint tenants information.
        'Action: commented out previous function and applied new function.
        'Also added try catch block.
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                Me.setTenantsInfo(dstenantsInfo, tblTenantInfoApa)

                Me.mdlPopUpAppointmentArrangedPhone.Show()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If

            'Me.setTenantsInfoByTenancyID(tenancyID)

            'Dim customerId As Int32 = CType(btn.CommandArgument, Int32)
            'Me.setTenantInfo(customerId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            'ApplicationConstants.amendAppointmentClicked = True
            SessionManager.setApaAmendAppointmentClicked(True)
            Dim statusId As Integer = CType(ddlStatus.SelectedValue(), Integer)
            Me.LoadActions(statusId)
            Me.LoadLetters(-1)
            Me.ddlStatus.SelectedValue = statusId
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            Me.mdlPopUpAmendAppointment.Show()

            'ApplicationConstants.amendAppointmentClicked = True
            SessionManager.setApaAmendAppointmentClicked(True)
        End Try
    End Sub

#Region "ddl Action Selected Index Changed"
    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            Dim actionId As Integer = CType(ddlAction.SelectedValue(), Integer)
            Me.LoadLetters(actionId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            'Commented out mdlAmendPopUpCalendar and called set Calander Info 
            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            Me.mdlPopUpAmendAppointment.Show()

            'ApplicationConstants.amendAppointmentClicked = True
            SessionManager.setApaAmendAppointmentClicked(True)
        End Try
    End Sub
#End Region



#Region "img Btn Remove Letter Doc Click"

    Protected Sub imgBtnRemoveLetterDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim itemId As String = imgBtn.CommandArgument.ToString()

            Dim parts As Array = itemId.Split("%%")
            Me.removeItemLetterDocList(parts(0))
            Me.remmoveItemActivityLetterDetail(CType(parts(2), Integer))
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            'Commented out mdlAmendPopUpCalendar and called set Calander Info 
            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            mdlPopUpAmendAppointment.Show()

        End Try
    End Sub

#End Region

#Region "Btn Save Appointment Click"
    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAction.Click
        Dim isSaved As Boolean = False
        Dim apptScheduleParam() As String = SessionManager.getApaParam() 'Getting Property Information from session
        Dim startDate As Date = SessionManager.getApaStartDate()
        Dim EmailStatus As String = UserMessageConstants.EmailNotSent
        Dim SmsStatus As String = UserMessageConstants.SmsNotSent
        Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
        Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
        Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
        Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
        Dim pId As String = ""
        pId = apptScheduleParam(4)
        If Not IsNothing(pId) AndAlso Not pId = "" Then
            If SessionManager.getUserEmployeeId = objSchedulingBL.getPropertyLockedBy(pId, "Property") Then
                Try
                    lblMonth.Text = MonthName(startDate.Month) + " " + Year(startDate).ToString()
                    If (apptScheduleParam.Count > 0) Then
                        lblAddress.Text = apptScheduleParam(0)
                        lblExpiryDays.Text = apptScheduleParam(2)
                    End If
                    If (Me.validateAmendAppointment() = True) Then
                        Dim journalHistoryId As Integer = 0
                        journalHistoryId = Me.AmendAppointment(PushNoticificationStatus, PushNoticificationDescription)
                        If journalHistoryId > 0 Then

                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SaveAmendAppointmentSuccessfully, False)


                            isSaved = True
                            ' Start of code to send the SMS'
                            Dim tenancyID As Integer = Convert.ToInt32(apptScheduleParam(2))
                            Dim dstenantsInfo As New DataSet()
                            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

                            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                                SmsStatus = UserMessageConstants.SmsSending
                                SmsDescription = UserMessageConstants.SmsDescriptionSending
                                Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                                Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                            End If

                            EmailStatus = UserMessageConstants.EmailSending
                            EmailDescription = UserMessageConstants.EmailDescriptionSending
                            Me.sendEmail(journalHistoryId, EmailStatus, EmailDescription)
                            Me.setEmailAndSmsStatusForAppointment(journalHistoryId, SmsStatus, SmsDescription, EmailStatus, EmailDescription)
                            Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)
                            'To remove the record of existing letters from the session,
                            ' so these these letters can be re attached on re arrange.
                            SessionManager.removeActivityLetterDetail()

                        Else
                            uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                        End If

                    End If
                Catch ex As ThreadAbortException
                    uiMessageHelper.Message = ex.Message
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.Message = ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                Finally
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
                    End If
                    If isSaved = False Then

                        Me.setCalendarInfo()
                        'Me.mdlAmendPopUpCalendar.Show()

                        Me.mdlPopUpAmendAppointment.Show()

                    Else
                        Me.mdlPopUpAmendAppointment.Hide()
                        'ApplicationConstants.amendAppointmentClicked = False
                        SessionManager.setApaAmendAppointmentClicked(True)
                        ' ApplicationConstants.amendIsSuccess = True
                        SessionManager.setAptbaIsSuccess(True)
                        objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
                        'If the appointment is saved sucessfully Disable save button. and cancel button.
                        'Further more set calendar info from session using Me.setCalendarInfo() and also show popup.
                        btnSaveAction.Enabled = False
                        btnCancelAction.Enabled = False
                        Me.setCalendarInfo()
                        Me.mdlPopUpAmendAppointment.Show()

                        'Commented out, it is handelded in a different way now.
                        'Response.Redirect("AdminScheduling.aspx?chk=1")

                    End If

                End Try
            Else
                mdlPopUpAmendAppointment.Hide()
                SessionManager.setApaAmendAppointmentClicked(True)
                'populateAppointmentArrangedGrid()
                PropertyScheduledPopUp.Show()
            End If

        End If

    End Sub
#End Region
#Region "set Push Noticification Status For Appointment"
    Private Sub setPushNoticificationStatusForAppointment(ByVal journalHistoryId As Integer, ByVal pushnoticificationStatus As String, ByVal pushnoticificationDescription As String)
        Try
            objSchedulingBL.Savepushnoticificationstatusforappointment(journalHistoryId, pushnoticificationStatus, pushnoticificationDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
    Protected Sub btnViewLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewLetter.Click
        Try
            If (ddlLetter.SelectedValue = "-1") Then

                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.SelectActionLetter, True)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            Me.mdlPopUpAmendAppointment.Show()

        End Try

    End Sub

    Protected Sub ckBoxRefreshDataSet_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxRefreshDataSet.CheckedChanged
        Me.ckBoxRefreshDataSet.Checked = False
        Try
            If (isLetterAlreadyExist(ddlLetter.SelectedValue)) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.LetterAlreadyExist, True)
            Else
                Dim dt As DataTable = New DataTable()
                dt = SessionManager.getActivityLetterDetail()

                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.populateLetterDocListControl(ApplicationConstants.LetterWord, SessionManager.getActivityStandardLetterId())
                    SessionManager.removeActivityStandardLetterId()
                End If
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            Me.mdlPopUpAmendAppointment.Show()

        End Try

    End Sub

    Protected Sub btnCancelAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelAction.Click
        Try
            'ApplicationConstants.amendAppointmentClicked = True
            SessionManager.setApaAmendAppointmentClicked(True)
            mdlPopUpAmendAppointment.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
    Protected Sub imgbtnCancelCalendarpnl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnCancelCalendarpnl.Click
        Try
            Dim apptScheduleParam() As String = SessionManager.getApaParam()
            Dim pId As String = ""
            pId = apptScheduleParam(4)
            If SessionManager.getUserEmployeeId() = objSchedulingBL.getPropertyLockedBy(pId, "Property") Then
                objSchedulingBL.setPropertyschedulestatus(pId, ApplicationConstants.PropertyUnLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), "Property")
            End If
            'ApplicationConstants.amendAppointmentClicked = True
            SessionManager.setApaAmendAppointmentClicked(True)
            'ApplicationConstants.amendCalendarPageingClicked = True
            SessionManager.setApaAmendCalendarPageingClicked(True)
            mdlPopUpAmendAppointment.Hide()
            mdlAmendPopUpCalendar.Hide()

            'Re Populate Appointment Arranged Grid only on calender close.
            searchAppointmentArranged(SessionManager.getDueWithIn56Days(), String.Empty, Me.getFuelType())
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#Region "img Btn close Property Scheduled Click"

    Protected Sub imgPropertyScheduleClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPropertyScheduleClose.Click
        Try
            mdlPopUpAmendAppointment.Hide()
            mdlAmendPopUpCalendar.Hide()
            populateAppointmentArrangedGrid()


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "ddlAppStartTime Selected Index Changed"

    'To Implement CR, The 'Ends' time should automatically default to 1 hour from the selected 'Start' time


    Protected Sub ddlAppStartTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAppStartTime.SelectedIndexChanged

        Try
            setEndTimeSelectedindex(ddlAppStartTime, ddlAppEndTime)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            setCalendarInfo()
            'mdlAmendPopUpCalendar.Show()
            mdlPopUpAmendAppointment.Show()
        End Try

    End Sub

#End Region

    Protected Sub ddlAmendDistance_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAmendDistance.SelectedIndexChanged
        Try
            'Me.filterAppointmentsOnDistance()

            'Commented out varWeeklyDT as it is being handled in setCalanderInfo() function.
            'varWeeklyDT = New DataTable()
            Me.setCalendarInfo()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#Region "Close Amend Calander Tenant Info Popup"

    Protected Sub imgbtnCloseTenantInfoAmendCalander_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnCloseTenantInfoAmendCalander.Click
        Try
            Me.setCalendarInfo()
            SessionManager.setTenancyClicked(False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub


#End Region

#End Region

#Region "ck Box Document Upload Checked Changed"

    Protected Sub ckBoxDocumentUpload_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxDocumentUpload.CheckedChanged
        Me.ckBoxDocumentUpload.Checked = False
        Try
            If (IsNothing(SessionManager.getDocumentUploadName())) Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.ErrorDocumentUpload, True)
            Else
                Me.populateLetterDocListControl(ApplicationConstants.DocumentWord, String.Empty)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If

            Me.setCalendarInfo()
            'Me.mdlAmendPopUpCalendar.Show()

            Me.mdlPopUpAmendAppointment.Show()

        End Try
    End Sub

#End Region

#Region "lnkBtn App Details Click"

    Protected Sub lnkBtnAppDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkBtnAppDetails.Click

        'To implement joint tenants information.
        'Action: commented out previous function and applied new function.
        'Also added try catch block.
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            'Dim customerId As Int32 = CType(btn.CommandArgument, Int32)
            'Me.setContactInfo(customerId)

            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                setTenantsInfo(dstenantsInfo, tblTenantInfoAmendPopup)

                Me.setCalendarInfo()

                Me.mdlPopUpAmendAppointment.Show()

                Me.mdlPopUpAmenAptContactDetails.Show()

            Else
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.noTenantInformationFound, True)

                Me.setCalendarInfo()

                Me.mdlPopUpAmendAppointment.Show()

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If

        End Try


        'End - Changed By Aamir Waheed on June 3, 2013.
    End Sub

    Protected Sub setContactInfo(ByVal customerId)


        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblFName.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblMobileNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblTelNo.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        'plhdrAptbaPopUp.Visible = True
        Me.mdlPopUpAmenAptContactDetails.Show()

    End Sub

#End Region

#Region "Image Button Close Amend Appointment Popup"

    Protected Sub imgBtnCloseAmendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCloseAmendApp.Click
        Try
            'If the appointment is saved, remove the existing calendar from session.

            If Not btnSaveAction.Enabled Then
                SessionManager.removeAmendCalanderWeeklyDataTable()
            End If

            'Hide Add Appointment Popup and Update Calendar (conditional)
            Me.setCalendarInfo()
            Me.mdlPopUpAmendAppointment.Hide()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally

            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If

        End Try

    End Sub

#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal tenantmobile As String, ByRef smsStatus As String, ByRef smsDescription As String)
        Try

            tenantmobile = tenantmobile.Replace(" ", String.Empty)

            If tenantmobile.Length >= 10 Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(ddlAppStartTime.SelectedItem.Text, txtAmendDate.Text)
                'GeneralHelper.sendSMS(body, tenantmobile)
                'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                'Dim javascriptMethod As String = "sendSMS('" + tenantmobile + "','" + body + "','" + smsUrl + "')"
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                GeneralHelper.sendSmsUpdatedAPI(tenantmobile, body)
                smsStatus = UserMessageConstants.SmsSent
                smsDescription = UserMessageConstants.SmsDescriptionSent
            Else
                smsStatus = UserMessageConstants.SmsSendingFailed
                smsDescription = UserMessageConstants.AppointmentSavedSMSError + UserMessageConstants.InvalidMobile
                'uiMessageHelper.IsError = True
                'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            smsStatus = UserMessageConstants.SmsSendingFailed
            smsDescription = UserMessageConstants.AppointmentSavedSMSError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal journalHistoryId As Integer, ByRef emailStatus As String, ByRef emailDescription As String)

        Try
            Dim objScheduling As SchedulingBL = New SchedulingBL()
            Dim resultDataset As DataSet = New DataSet()
            objScheduling.getAppointmentInfoForEmail(resultDataset, journalHistoryId)
            Dim infoDt As DataTable = resultDataset.Tables(0)
            If (infoDt.Rows.Count > 0) Then
                Dim recepientEmail As String = infoDt.Rows(0)(ApplicationConstants.EmailCol)
                Dim recepientName As String = infoDt.Rows(0)(ApplicationConstants.CustomerNameCol)
                Dim jobSheet As String = infoDt.Rows(0)(ApplicationConstants.JSGCol)
                Dim appointmentDate As String = infoDt.Rows(0)(ApplicationConstants.AppointmentDateCol)
                Dim appointmentTime As String = infoDt.Rows(0)(ApplicationConstants.AppointmentTimeCol)

                If Validation.isEmail(recepientEmail) Then
                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheet, appointmentTime, appointmentDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)
                    emailStatus = UserMessageConstants.EmailSent
                    emailDescription = UserMessageConstants.EmailDescriptionSent
                Else
                    emailStatus = UserMessageConstants.EmailSendingFailed
                    emailDescription = UserMessageConstants.AppointmentSavedEmailError + UserMessageConstants.InvalidEmail
                    'uiMessageHelper.IsError = True
                    'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.InvalidEmail, True)
                End If
            Else
                Throw New Exception(UserMessageConstants.TanencyTerminated)
            End If

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            emailStatus = UserMessageConstants.EmailSendingFailed
            emailDescription = UserMessageConstants.AppointmentSavedEmailError + ex.Message
            'uiMessageHelper.IsError = True
            'uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region
#Region "btn Resend Email/Sms Click"
    Protected Sub btnEmailSmsResend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEmailSmsResend.Click
        Try
            Dim cmdarg As String = btnEmailSmsResend.CommandArgument
            Dim EmailStatus As String = UserMessageConstants.EmailNotSent
            Dim SmsStatus As String = UserMessageConstants.SmsNotSent
            Dim EmailDescription As String = UserMessageConstants.EmailDescriptionNotSent
            Dim SmsDescription As String = UserMessageConstants.SmsDescriptionNotSent
            Dim TenancyId As Integer = 0
            If Not cmdarg = "" Then
                Dim commandParams() As String = cmdarg.Split(";")
                Dim jhId As Integer = Convert.ToInt32(commandParams(0))
                'Dim TenancyId As Integer = Convert.ToInt32(commandParams(1))
                If lblStatusHeadingMessage.Text = UserMessageConstants.EmailHeading Then
                    EmailStatus = UserMessageConstants.EmailSending
                    EmailDescription = UserMessageConstants.EmailDescriptionSending
                    sendEmail(jhId, EmailStatus, EmailDescription)
                    Me.setEmailAndSmsStatusForAppointment(jhId, "", "", EmailStatus, EmailDescription)
                Else
                    If (String.IsNullOrEmpty(commandParams(1) = True)) Then
                        TenancyId = Nothing
                    Else
                        TenancyId = commandParams(1)
                    End If
                    Dim dstenantsInfo As New DataSet()
                    objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, TenancyId)
                    If dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0 Then
                        SmsStatus = UserMessageConstants.SmsSending
                        SmsDescription = UserMessageConstants.SmsDescriptionSending
                        Dim tenantmobile = dstenantsInfo.Tables(0).Rows(0)("mobile").ToString()
                        Me.sendSMS(tenantmobile, SmsStatus, SmsDescription)
                        Me.setEmailAndSmsStatusForAppointment(jhId, SmsStatus, SmsDescription, "", "")
                    End If
                End If

            End If
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            'Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnPushNoticificationResend_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPushNoticificationResend.Click
        Try
            Dim cmdarg As String = btnPushNoticificationResend.CommandArgument
            Dim PushNoticificationStatus As String = UserMessageConstants.PushNoticificationNotSent
            Dim PushNoticificationDescription As String = UserMessageConstants.PushNoticificationDescriptionNotSent
            If Not cmdarg = "" Then
                Dim commandParams() As String = cmdarg.Split(";")
                Dim journalHistoryId As Integer = 0
                journalHistoryId = Convert.ToInt32(commandParams(0))
                Dim operativeId As Integer = Convert.ToInt32(commandParams(1))
                Dim appointmentdate As DateTime = Convert.ToDateTime(commandParams(3))
                Dim appointmentstarttime As String = commandParams(4).Split(" ")(1).Split("-")(0)
                Dim appointmentendtime As String = commandParams(4).Split(" ")(1).Split("-")(1)
                Dim address As String = commandParams(2)
                If (journalHistoryId > 0 And appointmentdate = Today()) Then
                    PushNoticificationStatus = UserMessageConstants.PushNoticificationSending
                    PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSending
                    Dim propertyAddress As String = String.Empty
                    propertyAddress = address.Trim()

                    Dim message As String = String.Empty
                    message = GeneralHelper.generatePushNotificationMessage("New", appointmentdate, appointmentstarttime,
                                                            appointmentendtime, propertyAddress)
                    Dim pushnoticicesent As Boolean = GeneralHelper.pushNotificationAppliance(message, operativeId)
                    If pushnoticicesent = True Then
                        PushNoticificationStatus = UserMessageConstants.PushNoticificationSent
                        PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSent
                    Else
                        PushNoticificationStatus = UserMessageConstants.PushNoticificationSendingFailed
                        PushNoticificationDescription = UserMessageConstants.PushNoticificationDescriptionSendingFailed
                    End If

                End If

                Me.setPushNoticificationStatusForAppointment(journalHistoryId, PushNoticificationStatus, PushNoticificationDescription)
            End If
            Me.populateAppointmentArrangedGrid()

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.Message, True)
            End If
            'Me.mdlPopUpAmendAppointment.Show()
        End Try

    End Sub
#End Region
#Region "set Email And Sms Status For Appointment"
    Private Sub setEmailAndSmsStatusForAppointment(ByVal journalHistoryId As Integer, ByVal smsStatus As String, ByVal smsDescription As String, ByVal emailStatus As String, ByVal emailDescription As String)
        Try
            objSchedulingBL.Saveemailsmsstatusforappointment(journalHistoryId, smsStatus, smsDescription, emailStatus, emailDescription)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region
#Region "img btn Email Click"

    Protected Sub imgbtnEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim emailDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = emailDescription(0)
            lblStatusHeadingDescription.Text = emailDescription(3)
            If emailDescription(3) = UserMessageConstants.EmailNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf emailDescription(3) = UserMessageConstants.EmailSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            lblStatusHeadingMessage.Text = UserMessageConstants.EmailHeading
            If Not emailDescription(1) = "" And Not IsNothing(emailDescription(1)) Then
                btnEmailSmsResend.CommandArgument = emailDescription(1) + ";" + emailDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not emailDescription(0) = "" Then
                If emailDescription(0) = UserMessageConstants.EmailDescriptionSending Or emailDescription(0) = UserMessageConstants.EmailDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()
        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "img btn Sms Click"

    Protected Sub imgbtnSms_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim smsDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblStatusDescriptionMessage.Text = smsDescription(0)
            lblStatusHeadingMessage.Text = UserMessageConstants.SmsHeading
            lblStatusHeadingDescription.Text = smsDescription(3)
            If smsDescription(3) = UserMessageConstants.SmsNotSent Then
                lblStatusHeadingDescription.CssClass = "greyFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSending Then
                lblStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSendingFailed Then
                lblStatusHeadingDescription.CssClass = "redFont"
            ElseIf smsDescription(3) = UserMessageConstants.SmsSent Then
                lblStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not smsDescription(1) = "" And Not IsNothing(smsDescription(1)) Then
                btnEmailSmsResend.CommandArgument = smsDescription(1) + ";" + smsDescription(2)
            Else
                btnEmailSmsResend.CommandArgument = ""
            End If
            If Not smsDescription(0) = "" Then
                If smsDescription(0) = UserMessageConstants.SmsDescriptionSending Or smsDescription(0) = UserMessageConstants.SmsDescriptionSent Then
                    btnEmailSmsResend.Visible = False
                Else
                    btnEmailSmsResend.Visible = True
                End If
            Else
                btnEmailSmsResend.Visible = False
            End If
            Me.EmailSmsPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region
#Region "img btn Push Noticifications Click"

    Protected Sub imgbtnPushNoticification_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim pushnoticificationDescription() As String = CType(btn.CommandArgument, String).Split(";")
            lblPushNoticificationStatusDescriptionMessage.Text = "Push Noticifications are not applicable for Admin arranged appointments."
            lblPushNoticificationStatusHeadingMessage.Text = UserMessageConstants.PushNoticificationHeading
            lblPushNoticificationStatusHeadingDescription.Text = ""
            If pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationNotSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greyFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSending Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "yellowFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSendingFailed Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "redFont"
            ElseIf pushnoticificationDescription(6) = UserMessageConstants.PushNoticificationSent Then
                lblPushNoticificationStatusHeadingDescription.CssClass = "greenFont"
            End If
            If Not pushnoticificationDescription(1) = "" And Not IsNothing(pushnoticificationDescription(1)) Then
                btnPushNoticificationResend.CommandArgument = pushnoticificationDescription(1) + ";" + pushnoticificationDescription(2) + ";" + pushnoticificationDescription(3) + ";" + pushnoticificationDescription(4) + ";" + pushnoticificationDescription(5)
            Else
                btnPushNoticificationResend.CommandArgument = ""
            End If
            If Not pushnoticificationDescription(0) = "" Then
                If pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSending Or pushnoticificationDescription(0) = UserMessageConstants.PushNoticificationDescriptionSent Then
                    btnPushNoticificationResend.Visible = False
                Else
                    btnPushNoticificationResend.Visible = False
                End If
            Else
                btnPushNoticificationResend.Visible = False
            End If
            Me.PushNoticificationPopUp.Show()

        Catch ex As ThreadAbortException
            ExceptionPolicy.HandleException(ex, "Exception Policy")
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnPushNoticificationCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPushNoticificationCancel.Click
        Me.PushNoticificationPopUp.Hide()

    End Sub
#End Region
#Region "btn Resend Push Noticification Click"
    Protected Sub btnEmailSmsCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEmailSmsCancel.Click
        Me.EmailSmsPopUp.Hide()

    End Sub
#End Region

#Region "Button Cancel Reported Faults Click Event"

    Protected Sub btnCancelReportedFaults_Click(sender As Object, e As EventArgs)
        Try
            Dim objCancelAppointment = New AppointmentCancellationBO()
            Dim imgBtn As ImageButton = CType(sender, ImageButton)
            Dim journalId As String = imgBtn.CommandArgument.ToString()
            objCancelAppointment.FaultsList = journalId
            SessionManager.setConfirmedGasAppointment(objCancelAppointment)
            mdlPopUpCancelFaults.Show()
            If txtBoxDescription.Enabled = False Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            cancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Cancel Faults"

    Sub cancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As AppointmentCancellationBO = New AppointmentCancellationBO()
            resultDataSet = SessionManager.getConfirmedGasAppointment()

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim faultsList As StringBuilder = New StringBuilder()
                faultsList.Append(resultDataSet.FaultsList.ToString())
                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text
                objAppointmentCancellationBO.userId = SessionManager.getAppServicingUserId()

                If IsDBNull(objSchedulingBL.saveGasServicingCancellation(objAppointmentCancellationBO)) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False
                    pnlCustomerMessage.Visible = False
                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    SessionManager.removeConfirmedGasAppointment()
                    mdlPopUpCancelFaults.Hide()
                    populateAppointmentArrangedGrid()
                    uiMessageHelper.Message = UserMessageConstants.FaultCancelled
                    'pushAppointmnetCancellation()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, False)
                End If

            End If

        End If

    End Sub

#End Region

#Region "Set Fuel Type"
    Protected Sub setFuelType(ByRef fuelType As Integer)
        ViewState(ViewStateConstants.FuelType) = fuelType
    End Sub
#End Region


#Region "Set Status Type"
    Protected Sub setStatusType(ByRef statusType As String)
        ViewState(ViewStateConstants.StatusType) = statusType
    End Sub
#End Region



#Region "Get Fuel Type"
    Protected Function getFuelType() As Integer
        If (IsNothing(ViewState(ViewStateConstants.FuelType))) Then
            Return -1
        Else
            Return CType(ViewState(ViewStateConstants.FuelType), Integer)
        End If
    End Function

#End Region

#Region "Get Status Type"
    Protected Function getStatusType() As String
        If (IsNothing(ViewState(ViewStateConstants.StatusType)) Or ViewState(ViewStateConstants.StatusType) = "All") Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.StatusType), String)
        End If
    End Function

#End Region

End Class