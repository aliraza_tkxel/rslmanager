﻿Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Threading
Imports AS_Utilities


#Region "Delegates"
'Public Delegate Sub aptbaSearchDelegate(ByVal searchText As String)
#End Region
Partial Public Class AlternativeAppointmentToBeArranged
    Inherits UserControlBase

#Region "Attributes"
    Shared commandParams() As String
    'Calendar VariablesappointmentSlotsDtBo = objSchedulingBL.getAppoi
    Public addAppointmentWeeklyDT As DataTable = New DataTable()
    'Appointment to be Arranged Variables
    Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
    Dim objAlternativeSchedulingBL As AlternativeSchedulingBL = New AlternativeSchedulingBL()

    'public event MissedPaymentAlertCountDelegate MissedPaymentAlertCountEvent;    
    'Public Event onAptbaIn56DaysEvent As AptbaIn56DaysDelegate

    Dim objPageSortBo As PageSortBO = New PageSortBO("ASC", "LGSRDate", 1, 30)
#End Region

#Region "Events"
#Region "Page Load "

    'Private Property Path As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Appointment To Be Arranged Grid"
    Protected Sub imgArrowBtnClicked()
        'mdlPopupAvailableAppointments.Hide()
        ScriptManager.RegisterStartupScript(Page, Page.GetType, "radalert", "callAlert('') ", True)

    End Sub

#Region "img Btn Available Appointment Slots Click"
    ''' <summary>
    ''' This is event handler for arrow sign in appointment to be arranged grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub imgBtnAvailableAppointmentSlots_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim btnImage As ImageButton = CType(sender, ImageButton)
        commandParams = btnImage.CommandArgument.Split(";")
        Dim pId As String = commandParams(4)

        Dim commandArgumentParams() As String = commandParams
        Dim appointmentType As String = commandArgumentParams(21)
        Dim criticalsection As Integer = 0
        Dim isScheduled As Integer = ApplicationConstants.PropertyScheduled
        Dim isTimeExceeded As Boolean = False

        Dim locked As Integer = objSchedulingBL.GetSchedulingPropertyStatus(commandArgumentParams(5), isTimeExceeded, isScheduled, appointmentType)
        If locked = ApplicationConstants.PropertyUnLocked AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandArgumentParams(5), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        ElseIf locked = ApplicationConstants.PropertyLocked AndAlso isTimeExceeded AndAlso isScheduled = ApplicationConstants.PropertyUnScheduled Then
            criticalsection = 1
            objSchedulingBL.SetSchedulingPropertyStatus(commandArgumentParams(5), ApplicationConstants.PropertyLocked, ApplicationConstants.PropertyUnScheduled, SessionManager.getUserEmployeeId(), appointmentType)
        End If

        If criticalsection = 1 Then
            Try

                If Not commandArgumentParams(12) = "" And Not IsNothing(commandArgumentParams(12)) Then
                    Dim customerId As Integer = Convert.ToInt32(commandArgumentParams(12))
                    Dim riskVulnerability As String = getCustomerRiskVulnerability(customerId) 'index -12 : customerId
                    If Not riskVulnerability.Equals("") Then
                        Dim script As String = "callAlert('" + riskVulnerability + "')"
                        ScriptManager.RegisterStartupScript(Page, Page.GetType, "radalert", script, True)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try

                Dim resultDataSet As DataSet = New DataSet()
                objAlternativeSchedulingBL.GetAlternativeAppointmentsByPropertyId(pId, resultDataSet)

                If IsNothing(SessionManager.getApplianceServicingId()) Or SessionManager.getApplianceServicingId() = 0 Then
                    Dim userBl As UsersBL = New UsersBL()
                    Dim inspectionTypeDs As DataSet = New DataSet()
                    userBl.getInspectionTypesByDesc(inspectionTypeDs, "Appliance Servicing")
                    Dim applianceServicingId As Integer = inspectionTypeDs.Tables(0).Rows(0).Item("InspectionTypeId")
                    SessionManager.setApplianceServicingId(applianceServicingId)
                End If

                'set the property id in session so that it 'll be used to open the print letter window
                SessionManager.setPropertyIdForEditLetter(commandArgumentParams(4))

                'fill up the property scheduling bo
                Dim selPropSchedulingBo As SelectedPropertySchedulingBO = New SelectedPropertySchedulingBO()
                selPropSchedulingBo.PropertyAddress = commandArgumentParams(0)
                selPropSchedulingBo.PostCode = commandArgumentParams(1)
                selPropSchedulingBo.Boilers = commandArgumentParams(22)
                selPropSchedulingBo.AppointmentType = commandArgumentParams(21)
                'tenancy id can be null for void properties
                If (String.IsNullOrEmpty(commandArgumentParams(2)) = True) Then
                    selPropSchedulingBo.TenancyId = Nothing
                Else
                    selPropSchedulingBo.TenancyId = CType(commandArgumentParams(2), Integer)
                End If

                'There are some properties which does not have any certificate expiry date for that this check is implemented
                'this 'll make sure that expiry date checkbox is disabled and 'll not be implemented
                If (String.IsNullOrEmpty(commandArgumentParams(3)) = True) Then
                    selPropSchedulingBo.CertificateExpiryDate = Nothing
                Else
                    selPropSchedulingBo.CertificateExpiryDate = CType(commandArgumentParams(3), Date)
                End If
                selPropSchedulingBo.PropertyId = commandArgumentParams(4)

                selPropSchedulingBo.JournalId = commandArgumentParams(5)

                selPropSchedulingBo.ExpiryInDays = commandArgumentParams(6)
                selPropSchedulingBo.PropertyStatus = commandArgumentParams(8)
                selPropSchedulingBo.PatchId = CType(commandArgumentParams(9), Integer)
                selPropSchedulingBo.Telephone = commandArgumentParams(10)
                selPropSchedulingBo.CustomerName = commandArgumentParams(11)
                selPropSchedulingBo.CustomerEmail = commandArgumentParams(13)

                selPropSchedulingBo.HouseNumber = commandArgumentParams(14)
                selPropSchedulingBo.Address1 = commandArgumentParams(15)
                selPropSchedulingBo.Address2 = commandArgumentParams(16)
                selPropSchedulingBo.Address3 = commandArgumentParams(17)
                selPropSchedulingBo.TownCity = commandArgumentParams(18)
                selPropSchedulingBo.County = commandArgumentParams(19)
                selPropSchedulingBo.AppointmentId = commandArgumentParams(20)
                selPropSchedulingBo.AppointmentType = commandArgumentParams(21)

                selPropSchedulingBo.Duration = commandArgumentParams(24)

                selPropSchedulingBo.FuelType = commandArgumentParams(23)

                'Remove the proeprty scheduling bo from session if already exist  this is just to avoid the overlapping of sesssion values between different properties
                SessionManager.RemoveSelectedPropertySchedulingBo()
                SessionManager.RemovePropertyDurationDict()
                SessionManager.RemoveAlternativeSchedulingHeaderBoList()
                SessionManager.RemoveAppointmentTypeInfo()
                'now set the session with selected property values
                SessionManager.SetSelectedPropertySchedulingBo(selPropSchedulingBo)

                ToBeArrangedAvailableAppointments.IsFirstTimeInAlternative = 0

                Response.Redirect(PathConstants.ToBeArrangedAvailableAppointments)

            Catch ex As ThreadAbortException
                uiMessageHelper.Message = ex.Message

            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.Message = ex.Message

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If

            Finally
                If uiMessageHelper.IsError = True Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                End If
            End Try

        Else
            PropertyScheduledPopUp.Show()
        End If
    End Sub

#End Region

#Region "grd Appointment To Be Arranged Sorting"

    Protected Sub grdAppointmentToBeArranged_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAppointmentToBeArranged.Sorting
        Try
            'get the sort expression
            objPageSortBo = getPageSortBo()
            objPageSortBo.SortExpression = e.SortExpression

            'when user click on any column to sort the grid it does not maintain its page index.
            'If we try to set it, grid has no effect.So now whenever user 'll click on any column to sort, it 'll set first page as index           
            objPageSortBo.PageNumber = 1
            grdAppointmentToBeArranged.PageIndex = 0

            'set the sort direction. Grid view always gives Ascending as sort direction.
            'So we are saving the previous sort direction in view state              
            objPageSortBo.setSortDirection()
            setPageSortBo(objPageSortBo)
            Me.sortGrid()

            '''''''''''''''''''''''''''''''''''''''''' old code '''''''''''''''''''''' 
            'ViewState(ViewStateConstants.SortDirection) = e.SortDirection
            'Me.sortExpression = e.SortExpression
            'Me.setSortDirection()
            'Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "grd Appointment To Be Arranged Page Index Changing"

    Protected Sub grdAppointmentToBeArranged_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAppointmentToBeArranged.PageIndexChanging
        Try

            objPageSortBo = Me.getPageSortBo()
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            Me.grdAppointmentToBeArranged.PageIndex = e.NewPageIndex

            Me.setPageSortBo(objPageSortBo)
            Me.populateAppointmentToBeArrangedGrid()

            ''''''old code '''''''''''
            'Dim resultDataSet As DataSet = New DataSet()
            'resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
            'Me.grdAppointmentToBeArranged.PageIndex = e.NewPageIndex
            'Me.grdAppointmentToBeArranged.SelectedIndex = -1
            'Me.grdAppointmentToBeArranged.DataSource = resultDataSet
            'Me.grdAppointmentToBeArranged.DataBind()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

    Protected Sub grdAppointmentToBeArranged_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As System.Web.UI.WebControls.Image
            Dim objPageSortBo As PageSortBO = getPageSortBo()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New System.Web.UI.WebControls.Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "img btn Aptba TenantInfo Click"

    Protected Sub imgbtnAptbaTenantInfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Start - Changed By Aamir Waheed on May 28, 2013.
        'To implement joint tenants information.
        'Action: commented out previous function and applied new function.
        'Also added try catch block.
        Try
            Dim btn As ImageButton = CType(sender, ImageButton)
            Dim tenancyID As Integer = CType(btn.CommandArgument, Integer)
            'Me.setTenantsInfoByTenancyID(tenancyID)

            Dim dstenantsInfo As New DataSet()

            objSchedulingBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                setTenantsInfo(dstenantsInfo, tblTenantInfo)

                Me.mdlPopUpAppointmentToBeArrangedPhone.Show()

            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If

            'Dim customerId As Int32 = CType(btn.CommandArgument, Int32)
            'Me.setTenantInfo(customerId)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

        'End - Changed By Aamir Waheed on May 28, 2013.
    End Sub
#End Region

#Region "imgBtnRisk Click"
    Protected Sub imgBtnRisk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim objSchedulingBL As New SchedulingBL()
            Dim resultDataSet As New DataSet()
            Dim imgBtn As ImageButton = DirectCast(sender, ImageButton)
            Dim customerId As Integer = CType(imgBtn.CommandArgument, Integer)
            objSchedulingBL.getRiskList(resultDataSet, customerId)
            grdRisk.DataSource = resultDataSet
            grdRisk.DataBind()

            ModalPopupRiskDetails.Show()
        Catch ex As Exception
        Finally

        End Try


    End Sub

#End Region

#Region "img btn Appointment abort Click"

    Protected Sub imgbtnAptAbort_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim btnNotes As ImageButton = CType(sender, ImageButton)
            Dim appointmentId As Integer = CType(btnNotes.CommandArgument, Integer)
            'ucAppointmentNotes.loadAppointmentData(appointmentId)
            'mdlAppointmentNotesPopup.Show()

            Dim objSchedulingBL As New SchedulingBL()
            Dim resultDataSet As New DataSet()
            Dim imgBtn As ImageButton = DirectCast(sender, ImageButton)
            Dim journalId As Integer = CType(imgBtn.CommandArgument, Integer)
            objSchedulingBL.getAbortData(resultDataSet, journalId)
            grdAbort.DataSource = resultDataSet
            grdAbort.DataBind()

            ModalPopupAbortReason.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub
#End Region

#Region "Assign to contractor button Click"
    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim boilerTypeInfo As BoilerTypeInfo = New BoilerTypeInfo()
            Dim btnAssignToContractor As Button = TryCast(sender, Button)
            Dim btnAssignToContractorCommandArg() As String = btnAssignToContractor.CommandArgument.Split(";")
            boilerTypeInfo.AppointmentType = btnAssignToContractorCommandArg(2)
            boilerTypeInfo.BoilerCount = btnAssignToContractorCommandArg(3)
            SessionManager.removeBoilerTypeInfoForSchemeBlock()
            SessionManager.setPropertyIdForFuelScheduling(btnAssignToContractorCommandArg(0))
            SessionManager.setJournalIdForFuelScheduling(Integer.Parse(btnAssignToContractorCommandArg(1)))
            SessionManager.setBoilerTypeInfoForSchemeBlock(boilerTypeInfo)
            SessionManager.RemoveServicingTypeForOilAlternative()
            Dim servicingType As ServicingType = New ServicingType(False, True)
            SessionManager.SetServicingTypeForOilAlternativ(servicingType)
            ucAssignFuelServicingToContractor.PopulateControl()
            mdlpopupAssignToContractor.Show()

        Catch ex As Exception
        Finally
        End Try
    End Sub


#End Region

#Region "grdAppointmentToBeArranged row data bound action"
    Protected Sub grdAppointmentToBeArranged_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAppointmentToBeArranged.RowDataBound
        Try
            Dim drview As DataRowView = TryCast(e.Row.DataItem, DataRowView)
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hdnFieldRisk As HiddenField = TryCast(e.Row.FindControl("hdnFieldRiskCount"), HiddenField)
                If hdnFieldRisk.Value = "0" Then
                    TryCast(e.Row.FindControl("imgBtnRisk"), ImageButton).Visible = False
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region

#End Region

#End Region

#Region "Functions"

#Region "hide Show Abort Button"
    ''' <summary>
    ''' hide Show Notes Button
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function hideShowAppointmentNotesButton(ByVal status As String)
        If status = "Aborted" Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Appointment To Be Arranged Grid - Functions"

#Region "search Appointment To Be Arranged"
    Public Sub searchAppointmentToBeArranged(ByRef check56Days As Boolean, ByRef searchText As String, ByVal fuelType As Integer, ByRef statusType As String)
        Try
            Me.setSearchText(searchText)
            Me.setCheck56Days(check56Days)
            Me.setPageSortBo(objPageSortBo)
            Me.setFuelType(fuelType)
            Me.SetStatusType(statusType)
            Me.populateAppointmentToBeArrangedGrid()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "populate Appointment To Be ArrangedGrid "

    Public Sub populateAppointmentToBeArrangedGrid()

        Dim objSchedulingBL As SchedulingBL = New SchedulingBL()
        Dim resultDataSet As DataSet = New DataSet()
        Dim totalCount As Integer = 0

        totalCount = objAlternativeSchedulingBL.GetAlternativeAppointmentToBeArrangedList(resultDataSet, Me.getCheck56Days(), getSearchText(), Me.getPageSortBo(), Me.getFuelType(), GetStatusType(), -1, -1)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAppointmentToBeArranged.Visible = False
        Else
            grdAppointmentToBeArranged.Visible = True
            ViewState.Add(ViewStateConstants.AptbaDataSet, resultDataSet)
            grdAppointmentToBeArranged.VirtualItemCount = totalCount
            grdAppointmentToBeArranged.DataSource = resultDataSet
            grdAppointmentToBeArranged.DataBind()

        End If

    End Sub
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"
    ''' <summary>
    ''' 'To implement joint tenants information.
    ''' </summary>
    ''' <param name="dsTenantsInfo"></param>
    ''' <param name="tblTenantInfo"></param>
    ''' <remarks></remarks>
    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub
#End Region

#Region "set Tenant Info"

    Protected Sub setTenantInfo(ByVal customerId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim resultDataTable As New DataTable

        'get the dataset that was stored in view state
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)
        'find the table in dataset
        resultDataTable = resultDataSet.Tables(0)

        'query the data set using this will return the data row
        Dim customerQuery = (From dataRow In resultDataTable _
            Where _
                dataRow.Field(Of Integer)("CUSTOMERID") = customerId _
            Select dataRow).ToList()

        Dim a As Int32 = 0

        'Query the data table
        Dim datatable As DataTable = customerQuery.CopyToDataTable()


        'set the popup with values of tenant 
        'lblAptbaEmail.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("EMAIL")) = True, "", datatable.Rows(0).Item("EMAIL")), String)
        'lblAptbaFisrtname.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("CustomerName")) = True, "", datatable.Rows(0).Item("CustomerName")), String)
        'lblAptbaMobile.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("MOBILE")) = True, "", datatable.Rows(0).Item("MOBILE")), String)
        'lblAptbaTel.Text = CType(IIf(IsDBNull(datatable.Rows(0).Item("TEL")) = True, "", datatable.Rows(0).Item("TEL")), String)
        Me.mdlPopUpAppointmentToBeArrangedPhone.Show()

    End Sub
#End Region

#Region "Sort Grid"
    Protected Sub sortGrid()

        Dim resultDataSet As DataSet = New DataSet()
        Me.populateAppointmentToBeArrangedGrid()
        resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)

        Dim dt As DataTable = New DataTable()
        dt = resultDataSet.Tables(0)

        If IsNothing(dt) = False Then
            Dim dvSortedView As DataView = New DataView(dt)
            dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            grdAppointmentToBeArranged.DataSource = dvSortedView
            grdAppointmentToBeArranged.DataBind()
        End If
        '''''''''''''''''''''''''''''''''''''''''' old code '''''''''''''''''''''' 
        'Dim resultDataSet As DataSet = New DataSet()
        'Dim resultDataTable As New DataTable

        ''get the dataset that was stored in view state
        'resultDataSet = ViewState(ViewStateConstants.AptbaDataSet)

        ''find the table in dataset
        'resultDataTable = resultDataSet.Tables(0)

        'resultDataTable.DefaultView.Sort = Me.sortExpression + " " + Me.sortDirection
        'grdAppointmentToBeArranged.DataSource = resultDataTable
        'grdAppointmentToBeArranged.DataBind()
    End Sub
#End Region

#Region "set Search Text"
    Public Sub setSearchText(ByVal searchText As String)
        If (searchText <> "Search") Then
            ViewState(ViewStateConstants.AptbaSearchString) = searchText
        End If


    End Sub
#End Region

#Region "get Search Text"
    Public Function getSearchText() As String
        If (IsNothing(ViewState(ViewStateConstants.AptbaSearchString))) Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AptbaSearchString), String)
        End If
    End Function
#End Region

#Region "set Check 56 Days"
    Public Sub setCheck56Days(ByVal checkBoxVal As Boolean)
        ViewState(ViewStateConstants.AptbaCheck56Days) = checkBoxVal
    End Sub
#End Region

#Region "set Check 56 Days"
    Public Function getCheck56Days() As Boolean
        If (IsNothing(ViewState(ViewStateConstants.AptbaCheck56Days))) Then
            Return True
        Else
            Return CType(ViewState(ViewStateConstants.AptbaCheck56Days), Boolean)
        End If
    End Function
#End Region

#Region "reset Message"
    Public Sub resetMessage()
        lblMessage.Text = String.Empty
        pnlMessage.Visible = False
    End Sub
#End Region

#Region "Set Page Sort Bo"
    Protected Sub setPageSortBo(ByRef objPageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = objPageSortBo
    End Sub
#End Region

#Region "Get Page Sort Bo"
    Protected Function getPageSortBo() As PageSortBO
        Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
    End Function

#End Region

#Region "Set Fuel Type"
    Protected Sub setFuelType(ByRef fuelType As Integer)
        ViewState(ViewStateConstants.FuelType) = fuelType
    End Sub
#End Region

#Region "Set Status Type"
    Protected Sub SetStatusType(ByRef statusType As String)
        ViewState(ViewStateConstants.AlternativeStatusType) = statusType
    End Sub
#End Region

#Region "Get Fuel Type"
    Protected Function getFuelType() As Integer
        If (IsNothing(ViewState(ViewStateConstants.FuelType))) Then
            Return -1
        Else
            Return CType(ViewState(ViewStateConstants.FuelType), Integer)
        End If
    End Function

#End Region

#Region "Get Status Type"
    Protected Function GetStatusType() As String
        If (IsNothing(ViewState(ViewStateConstants.AlternativeStatusType)) Or ViewState(ViewStateConstants.AlternativeStatusType) = "All") Then
            Return ""
        Else
            Return CType(ViewState(ViewStateConstants.AlternativeStatusType), String)
        End If
    End Function

#End Region

#End Region

#Region "Risk & Vulnerability of Customer"
    Private Function getCustomerRiskVulnerability(ByVal customerId As Integer)

        Dim schedulingBL As SchedulingBL = New SchedulingBL()
        Dim riskVulnerabilityResult As DataSet = New DataSet()
        Return schedulingBL.getCustomerRiskVulnerability(riskVulnerabilityResult, customerId)

    End Function
#End Region

#Region "Save existing appointments in session to show on map"

    Private Sub saveExistingAppointmentForVisibleOperatives(ByVal appointmentSlots As DataTable, ByVal existingAppointments As DataTable)

        'Remove the appointments/properties from session

        'Get appointment of only visible operatives.
        Dim appointmentsForMap = (From extapt In existingAppointments.AsEnumerable()
                                    Join aptSlot In appointmentSlots.AsEnumerable() On extapt("OperativeId").ToString() Equals aptSlot("OperativeId").ToString()
                                    Select extapt)

        Dim appointmentsForMapDt As DataTable = Nothing

        If appointmentsForMap.Count() > 0 Then
            'Join may cause repetition of rows so distinct is applied.
            appointmentsForMapDt = appointmentsForMap.Distinct().CopyToDataTable()
        End If

        SessionManager.setExistingAppointmentsForSchedulingMap(appointmentsForMapDt)

    End Sub

#End Region

#Region "Register client side script to update markers on Google Map. on appointments refresh."

    Private Sub updateMarkersJavaScript()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "updateMarkerATBA", "addOriginMarkers();", True)
    End Sub

#End Region

#Region "Register client side script to remove existing markers from Map"

    Private Sub removeMarkersJavaScript()
        'Register client side script to remove markers on Google Map.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "deleteMarkersATBA", "deleteMarkers();", True)
    End Sub

#End Region

#End Region

End Class
