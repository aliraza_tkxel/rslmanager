﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AlternativeAppointmentArranged.ascx.vb"
    Inherits="Appliances.AlternativeAppointmentArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="propertyAddressTag" TagName="PropertyAddress" Src="~/Controls/Scheduling/PropertyAddress.ascx" %>
<%@ Register TagPrefix="appointmentNotesTag" TagName="AppointmentNotes" Src="~/Controls/Scheduling/AppointmentNotes.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<script type="text/javascript">
    document.onload = function () {
        var w;
        function uploadError(sender, args) {
            alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
        }

        function uploadComplete() {
            __doPostBack();
        }

        function callAlert(msg) {
            alert(msg);
        }
    }
        
</script>
<style type="text/css">
    .style1
    {
        width: 266px;
    }
    .style2
    {
        width: 24px;
    }
    .style5
    {
        width: 229px;
    }
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    
    .modalPopup
    {
        background-color: Blue; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
    .Noticification
    {
        height: 20px;
        width: 20px;
    }
    .greyFont
    {
        color: Gray;
    }
    .yellowFont
    {
        color: Yellow;
    }
    .redFont
    {
        color: Red;
    }
    .greenFont
    {
        color: Green;
    }
    .dashboard th
    {
        background: #fff;
        border-bottom: 4px solid #8b8687;
    }
    .dashboard th a
    {
        color: #000 !important;
    }
    .dashboard th img
    {
        float: right;
    }
    .dashboard, .dashboard td
    {
        padding: 5px 5px 5px 5px !important;
    }
</style>
<div style="overflow: auto;">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <cc1:PagingGridView ID="grdAppointmentArranged" runat="server" AutoGenerateColumns="False"
        AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
        CssClass="dashboard dashboard-border webgrid table table-responsive" EmptyDataText="No Records Found"
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="True" OnRowCreated="grdAppointmentArranged_RowCreated">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnApaTenantInfo" runat="server" CommandArgument='<%# Eval("TENANCYID") %>'
                        ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnApaTenantInfo_Click" BorderStyle="None"
                        Visible='<%# GeneralHelper.isPropertyStatusLet(Eval("PropertyStatus").ToString()) %>'
                        BorderWidth="0px" />
                    <asp:ImageButton ID="imgbtnAptNotes" runat="server" CommandArgument='<%# Eval("APPOINTMENTID") %>'
                        ImageUrl='<%# "~/Images/editCustomerNotes.png" %>' OnClick="imgbtnAptNotes_Click"
                        BorderStyle="None" Visible='<%# hideShowAppointmentNotesButton(Eval("Notes").ToString()) %>'
                        Width="18px" Height="18px" BorderWidth="0px" />
                </ItemTemplate>
                <ItemStyle Width="52px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" JSG No" SortExpression="JSGNUMBER">
                <ItemTemplate>
                    <asp:Label ID="lbljsgno" runat="server" Text='<%# "JSG"+ Eval("JSGNUMBER").ToString() %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Logged " SortExpression="LOGGEDDATE">
                <ItemTemplate>
                    <asp:Label ID="lblLogged" runat="server" Text='<%# Bind("LOGGEDDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                <ItemTemplate>
                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">--%>
            <asp:TemplateField HeaderText="Expiry Date" SortExpression="LGSRDate">
                <ItemTemplate>
                    <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Days" SortExpression="Days">
                <ItemTemplate>
                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("DAYS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fuel Type" SortExpression="Fuel">
                <ItemTemplate>
                    <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Engineer" SortExpression="ENGINEER">
                <ItemTemplate>
                    <asp:Label ID="lblEngineer" runat="server" Text='<%# Bind("ENGINEER") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appointment" SortExpression="AppointmentSortDate">
                <ItemTemplate>
                    <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("APPOINTMENT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <div style="text-align: right">
                        <asp:ImageButton ID="imgbtnEmailStatus" runat="server" CommandArgument='<%# Eval("EmailDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EmailStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("EmailImagePath") %>' OnClick="imgbtnEmail_Click" BorderStyle="None"
                            CssClass="Noticification" Visible='<%# Eval("AppointmentType").ToString() = "Property"  %>'
                            BorderWidth="0px" />
                        <asp:ImageButton ID="imgbtnSmsStatus" runat="server" CommandArgument='<%# Eval("SmsDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("SmsStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("SmsImagePath") %>' OnClick="imgbtnSms_Click" BorderStyle="None"
                            CssClass="Noticification" Visible='<%# Eval("AppointmentType").ToString() = "Property"  %>'
                            BorderWidth="0px" />
                        <asp:ImageButton ID="imgbtnPushNoticificationStatus" runat="server" CommandArgument='<%# Eval("PushNoticificationDescription").ToString()+";"+Eval("JournalHistoryId").ToString()+";"+Eval("ASSIGNEDTO").ToString()+";"+Eval("ADDRESS").ToString()+";"+Eval("ApDate").ToString()+";"+Eval("APPOINTMENT").ToString()+";"+Eval("PushNoticificationStatusDescription").ToString() %>'
                            ImageUrl='<%# Eval("PushNoticificationImagePath") %>' OnClick="imgbtnPushNoticification_Click"
                            BorderStyle="None" CssClass="Noticification" BorderWidth="0px" />
                        <asp:ImageButton ID="imgBtnAppointmentArrangedArrow" runat="server" OnClick="imgBtnAppointmentArrangedArrow_Click"
                            BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("PROPERTYID").ToString()+";"+Eval("APPOINTMENTID").ToString()+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString()+";"+Eval("PatchId").toString()+";"+Eval("Telephone").ToString()+";"+Eval("NAME").ToString()+";"+Eval("CustomerId").ToString() + ";" + Eval("EMAIL").ToString() + ";" + Eval("HouseNumber").ToString() + ";" + Eval("ADDRESS1").ToString() + ";" + Eval("ADDRESS2").ToString() + ";" + Eval("ADDRESS3").ToString() + ";" + Eval("TOWNCITY").ToString() + ";" + Eval("COUNTY").ToString()+ ";" + Eval("AppointmentType").ToString() + ";" + Eval("Duration").ToString() %>' />
                        <asp:ImageButton ID="btnCancelReportedFaults" runat="server" OnClick="btnCancelReportedFaults_Click"
                            BorderStyle="None" BorderWidth="0px" ImageUrl='<%# "~/Images/cross2.png" %>'
                            CommandArgument='<%# Eval("JournalId").ToString()+ ";" + Eval("AppointmentType").ToString() + ";" + Eval("APPOINTMENTID").ToString() + ";" + Eval("StatusTitle")%>'
                            ToolTip="Cancel Appointment" />
                    </div>
                </ItemTemplate>
                <ItemStyle Width="140px" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
    <%--Appointment Arrnaged Customer Contact Details-- START--%>
    <asp:Panel ID="pnlAppointmentArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfoApa" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnApaClose" runat="server" Style="position: absolute; top: -10px;
                        right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--Appointment Arrnaged Customer Contact Details-- END--%>
</div>
<asp:Label ID="lbldispActivity" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAppointmentArrangedPhone" runat="server" TargetControlID="lbldispActivity"
    PopupControlID="pnlAppointmentArrangedTenantInfo" Enabled="true" DropShadow="true"
    CancelControlID="imgBtnApaClose">
</asp:ModalPopupExtender>
<%--Available Appointments-- START--%>
<asp:Panel ID="EmailSmsPanel" runat="server" BackColor="White" Width="470px" Height="100px"
    Style="padding: 15px 5px; top: 20px; border: 1px solid black; top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgEmailSmsClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        <asp:Label ID="lblStatusHeadingMessage" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="lblStatusDescriptionMessage" runat="server" Text="" Style="padding-right: 10px;"></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
        <asp:Button ID="btnEmailSmsCancel" runat="server" Text="Cancel" Visible="true" BackColor="White" />
        <asp:Button ID="btnEmailSmsResend" runat="server" Text="Resend" Visible="false" BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="EmailSmsLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="EmailSmsPopUp" runat="server" TargetControlID="EmailSmsLabel"
    PopupControlID="EmailSmsPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgEmailSmsClose">
</asp:ModalPopupExtender>
<asp:Panel ID="PushNoticificationPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPushNoticificationClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        <asp:Label ID="lblPushNoticificationStatusHeadingMessage" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblPushNoticificationStatusHeadingDescription" runat="server" Text=""></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="lblPushNoticificationStatusDescriptionMessage" runat="server" Text=""></asp:Label>
    </div>
    <div class="add-apt-canel-save" style="padding-top: 20px;">
        <asp:Button ID="btnPushNoticificationCancel" runat="server" Text="Cancel" Visible="true"
            BackColor="White" />
        <asp:Button ID="btnPushNoticificationResend" runat="server" Text="Resend" Visible="false"
            BackColor="White" />
    </div>
</asp:Panel>
<asp:Label ID="PushNoticificationLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PushNoticificationPopUp" runat="server" TargetControlID="PushNoticificationLabel"
    PopupControlID="PushNoticificationPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt"
    CancelControlID="imgPushNoticificationClose">
</asp:ModalPopupExtender>
<%--Available Appointments-- END--%>

<%--Appointment Notes Popup Start--%>
<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right: 10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" Style="padding-right: 10px;
            font-weight: bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled."
            Style="padding-right: 10px;"></asp:Label>
    </div>
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlAppointmentNotesPopup" runat="server">
    <appointmentNotesTag:AppointmentNotes ID="ucAppointmentNotes" runat="server" MaxValue="10"
        MinValue="1" />
</asp:Panel>
<asp:Label ID="lblDisplayAppointmentNotesPopup" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="mdlAppointmentNotesPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplayAppointmentNotesPopup" PopupControlID="pnlAppointmentNotesPopup"
    DropShadow="true" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Appointment Notes Popup End--%>
<asp:Panel ID="pnlAmendApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <table id="tblTenantInfoAmendPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
        <tr>
            <td style="width: 0px; height: 0px;">
                <asp:ImageButton ID="imgbtnCloseAmendContactDetail" runat="server" Style="position: absolute;
                    top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Label ID="lblAmendApointmentContactDetails" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAmenAptContactDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblAmendApointmentContactDetails" PopupControlID="pnlAmendApointmentContactDetails"
    DropShadow="true" CancelControlID="imgbtnCloseAmendContactDetail" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<%--Amend Appointment Contact Details Popup-- END--%>
<asp:CheckBox ID="ckBoxRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
    Width="390px" Height="270px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Cancel</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="width: 100%; height: 490px; overflow: hidden;">
        <div style="height: 370px; padding-top: 10px !important; padding-left: 20px;">
            <table>
                <tr>
                    <td align="left" class="style1">
                        <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                            <asp:Label ID="lblErrorMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlDescription" runat="server">
                            <asp:Label ID="lblDescription" runat="server" Text="Enter the reason for cancellation below:"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlCancelMessage" runat="server" Visible="False" HorizontalAlign="Center">
                            <asp:Label ID="lblCancelFaultsText" runat="server" Text="The appointment has been cancelled."
                                Font-Bold="True"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlSaveButton" runat="server" HorizontalAlign="Right">
                            <asp:Button ID="btnSave" runat="server" Text="Save" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style1">
                        <asp:Panel ID="pnlCustomerMessage" runat="server" HorizontalAlign="Center" Visible="False">
                            <asp:LinkButton ID="lnkBtnClickHere" runat="server" Visible="True" OnClientClick="closeWindow()">Click here</asp:LinkButton>
                            &nbsp;<asp:Label ID="lblCustomerText" runat="server" Text="to return to the customers record"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
    DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
    BehaviorID="mdlPopUpCancelFaults">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>