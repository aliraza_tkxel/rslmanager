﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AlternativeAppointmentToBeArranged.ascx.vb"
    Inherits="Appliances.AlternativeAppointmentToBeArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="propertyAddressTag" TagName="PropertyAddress" Src="~/Controls/Scheduling/PropertyAddress.ascx" %>
<%@ Import Namespace="AS_Utilities" %>
<%@ Register TagName="AssignFuelServicingToContractor" TagPrefix="ucAssignFuelServicingToContractor"
    Src="~/Controls/FuelScheduling/AssignFuelServicingToContractor.ascx" %>
<%@ Register TagName="PopUpAlternateFuelConfirmation" TagPrefix="ucPopUpAlternateFuelConfirmation"
    Src="~/Controls/FuelScheduling/PopUpAlternateFuelConfirmation.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script type="text/javascript">
    var w;
    function uploadError(sender, args) {
        alert(args.get_fileName() + ' could not be uploaded. ' + args.get_errorMessage());
    }

    function uploadComplete() {
        __doPostBack();
    }

    function fireCheckboxEventForAddApp() {
        document.getElementById('<%= ckBoxAddRefreshDataSet.ClientID %>').checked = true;
        setTimeout('__doPostBack(\'<%= ckBoxAddRefreshDataSet.UniqueID %>\',\'\')', 0);
    }

    function callAlert(msg) {
        alert(msg);
    }
        
</script>
<style type="text/css">
    .modalBackground
    {
        background-color: #C1C1C1;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    .modalPopUpNotesBackground
    {
        background-color: Gray;
        filter: alpha(opacity=80);
        opacity: 0.8;
    }
    
    
    .modalPopup
    {
        background-color: White; /* position:absolute;*/
        border-style: solid;
        border-color: Black;
        border-width: 2px;
        width: 300px;
        height: 300px;
    }
    .availableAppointmentPopOut
    {
        height: 100%;
        width: 100%;
        background: #000;
        display: block;
        z-index: 9999;
        position: fixed;
        top: 0;
        left: 0;
    }
    .modalBackground, .modalBackground2
    {
        background-color: #455560;
        filter: alpha(opacity=40);
        opacity: 0.4;
    }
    .modalBackground
    {
        z-index: 1000;
    }
    .modalBackground2
    {
        z-index: 10001;
    }
    .dashboard th
    {
        background: #fff;
        border-bottom: 4px solid #8b8687;
    }
    .dashboard th a
    {
        color: #000 !important;
    }
    .dashboard th img
    {
        float: right;
    }
    
    .dashboard, .dashboard td
    {
        padding: 5px 5px 5px 5px !important;
    }
</style>
<div style="overflow: auto;">
    <%--<asp:Button ID="btnPopup" OnClick="btnPopup_OnClick" runat="server" Text="PopUp"
        Visible="True" />--%>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <cc1:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AutoGenerateColumns="False"
        AllowSorting="True" PageSize="30" Width="100%" Style="overflow: scroll" BorderWidth="0px"
        CssClass="dashboard dashboard-border webgrid table table-responsive" EmptyDataText="No Records Found"
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="True" OnRowCreated="grdAppointmentToBeArranged_RowCreated">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("TENANCYID") %>'
                        ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnAptbaTenantInfo_Click" BorderStyle="None"
                        Visible='<%# GeneralHelper.isPropertyStatusLet(Eval("PropertyStatus").ToString()) %>'
                        BorderWidth="0px" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="30px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblTENANCYID" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="180px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                <ItemTemplate>
                    <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("POSTCODE") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="95px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Boiler" SortExpression="BoilersCount">
                <ItemTemplate>
                    <asp:Label ID="lblBoiler" runat="server" Text='<%# Bind("BoilersCount") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="90px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Expiry Date" SortExpression="LGSRDate">
                <ItemTemplate>
                    <asp:Label ID="lblExpirydate" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Days" SortExpression="Days">
                <ItemTemplate>
                    <asp:Label ID="lblDays" runat="server" Text='<%# Bind("DAYS") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="80px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fuel Type" SortExpression="FUEL">
                <ItemTemplate>
                    <asp:Label ID="lblFuel" runat="server" Text='<%# Bind("FUEL") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="StatusTitle">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'>></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Risk" SortExpression="Risk">
                <ItemTemplate>
                    <asp:HiddenField ID="hdnFieldRiskCount" runat="server" Value='<%# Eval("risk").ToString()%>' />
                    <asp:ImageButton ID="imgBtnRisk" runat="server" OnClick="imgBtnRisk_Click" BorderStyle="None"
                        BorderWidth="0px" ImageUrl='<%# "~/Images/warning.png" %>' CommandArgument='<%# Eval("CustomerID").ToString()%>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="85px" />
            </asp:TemplateField>
            <%--Journal Id has been removed from here--%>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnApaTenantInfo" runat="server" OnClick="imgbtnAptAbort_Click"
                        CommandArgument='<%# Eval("JournalId") %>' ImageUrl='<%# "~/Images/abortedSmall.png" %>'
                        BorderStyle="None" Visible='<%# hideShowAppointmentNotesButton(Eval("StatusTitle").ToString()) %>'
                        BorderWidth="0px" />
                    <asp:Button ID="btnAssignToContractor" runat="server" CommandArgument='<%# Eval("propertyId").ToString()+";"+Eval("JournalId").ToString()+ ";"  + Eval("AppointmentType").ToString() + ";" + Eval("BoilersCount").ToString()%>'
                        CssClass="btn btn-xs btn-blue" OnClick="btnAssignToContractor_Click" Style="padding: 3px 10px !important;
                        margin: 0;" Text="Assign to Contractor" />
                    <asp:ImageButton ID="imgAppointmentToBeArrangedArrow" ImageAlign="Right" runat="server"
                        OnClick="imgBtnAvailableAppointmentSlots_Click" BorderStyle="None" BorderWidth="0px"
                        ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("ADDRESS").ToString()+";"+Eval("POSTCODE").ToString()+";"+Eval("TENANCYID").ToString()+";"+Eval("EXPIRYDATE").ToString()+";"+Eval("propertyId").ToString()+";"+Eval("JournalId").ToString()+";"+Eval("DAYS").ToString()+";"+"0"+";"+Eval("PropertyStatus").ToString()+";"+Eval("PatchId").toString()+";"+Eval("Telephone").ToString()+";" + Eval("NAME").ToString() + ";" + Eval("CustomerId").ToString() + ";" + Eval("EMAIL").ToString() + ";" + Eval("HouseNumber").ToString() + ";" + Eval("ADDRESS1").ToString() + ";" + Eval("ADDRESS2").ToString() + ";" + Eval("ADDRESS3").ToString() + ";" + Eval("TOWNCITY").ToString() + ";" + Eval("COUNTY").ToString() + ";" + Eval("APPOINTMENTID").ToString() + ";" + Eval("AppointmentType").ToString()+";"+Eval("BoilersCount").ToString()+";"+Eval("Fuel").ToString()+";"+Eval("Duration").ToString()  %>' />
                </ItemTemplate>
                <ItemStyle Width="185px" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
    <asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnAptbaClose" runat="server" Style="position: absolute;
                        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<asp:Label ID="lbldisplayAppointment" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
    TargetControlID="lbldisplayAppointment" PopupControlID="pnlAppointmentToBeArrangedTenantInfo"
    Enabled="true" DropShadow="true" CancelControlID="imgBtnAptbaClose">
</asp:ModalPopupExtender>

<asp:Panel ID="pnlAddApointmentContactDetails" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblAptbaTenantInfoAddPopup" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddAppointmentContactDetails" runat="server"
    DynamicServicePath="" Enabled="True" TargetControlID="lblAddAppointmentContactDetails"
    PopupControlID="pnlAddApointmentContactDetails" DropShadow="true" CancelControlID="imgBtnCloseTenantInfo"
    BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblAddAppointmentContactDetails" runat="server"></asp:Label>

<asp:Panel ID="PropertyScheduledPanel" runat="server" BackColor="White" Width="470px"
    Height="100px" Style="padding: 15px 5px; top: 20px; border: 1px solid black;
    top: 20px; position: absolute;">
    <div>
        <asp:ImageButton ID="imgPropertyScheduleClose" runat="server" Style="position: absolute;
            top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    </div>
    <div style="width: 97%; padding-left: 10px; padding-right: 10px;">
        <asp:Label ID="Schedulelabelheading" runat="server" Text="Information" Style="padding-right: 10px;
            font-weight: bold"></asp:Label>
    </div>
    <hr />
    <div style="width: 97%; padding-left: 10px; padding-top: 20px; padding-right: 10px;">
        <asp:Label ID="Label1" runat="server" Text="Please refresh the Page. This Property is already scheduled or is being scheduled."
            Style="padding-right: 10px;"></asp:Label>
    </div>
</asp:Panel>
<asp:Label ID="ScheduledLabel" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="PropertyScheduledPopUp" runat="server" TargetControlID="ScheduledLabel"
    PopupControlID="PropertyScheduledPanel" Enabled="true" DropShadow="true" BackgroundCssClass="modalAvial-Appt">
</asp:ModalPopupExtender>
<asp:CheckBox ID="ckBoxAddRefreshDataSet" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:CheckBox ID="ckBoxDocumentUpload" runat="server" AutoPostBack="True" Visible="true"
    CssClass="hiddenField" />
<asp:Panel runat="server" ID="pnlPropertyAttributesNotes" CssClass="left" BackColor="White"
    Width="470px" Style="max-height: 350px; padding: 15px 5px; top: 32px; border: 1px solid black;
    position: absolute; z-index: 100000;">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Notes</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnNotes" runat="server" Style="position: absolute; top: -12px;
        right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="height: 200px; overflow: auto; width: 100%;">
        <asp:Repeater runat="server" ID="rptPropertyNotes">
            <ItemTemplate>
                <div style='width: 100%; padding: 10px;'>
                    <asp:Label Text='<%# Eval("AttributeName") %>' ID="lblAttribute" Font-Bold="true"
                        runat="server" />
                    <br />
                    <asp:Label Text='<%# Eval("AttributeNotes") %>' ID="lblAttributeNotes" runat="server" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopupNotes" runat="server" DynamicServicePath="" Enabled="True"
    TargetControlID="lblPropertyAttributesNotes" PopupControlID="pnlPropertyAttributesNotes"
    DropShadow="true" CancelControlID="imgBtnNotes" BackgroundCssClass="modalBackground2">
</asp:ModalPopupExtender>
<asp:Label ID="lblPropertyAttributesNotes" runat="server" Text=""></asp:Label>
<!-- start abort details popup-->
<asp:Panel runat="server" ID="PanelAbortReason" CssClass="left" BackColor="White"
    Width="400px" Height="400px">
    <div style="font-weight: bold; padding: 10px">
        Following is the reason and notes for abort appointment.
    </div>
    <hr />
    <div style="height: 180px; overflow: auto; padding-top: 10px; padding-left: 20px">
        <asp:GridView ID="grdAbort" runat="server" AutoGenerateColumns="False" ShowHeader="true"
            GridLines="None">
            <%--  <Columns>
                <asp:Label Text="Abort Notes: "  runat="server" /><asp:BoundField DataField="AbortNotes" />
                <asp:Label Text="Abort Reason: "  runat="server" /><asp:BoundField DataField="AbortReason" />
            </Columns>--%>
            <Columns>
                <asp:TemplateField HeaderText="Abort Reason:" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblOperative" runat="server" Text='<%# Eval("AbortReason") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Abort Notes:" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblPatch" runat="server" Text='<%# Eval("AbortNotes") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: center; padding-top: 30px">
        <asp:Button ID="Button1" runat="server" Text="OK" Width="100px" />
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupAbortReason" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblAbortClose" PopupControlID="PanelAbortReason"
    DropShadow="true" CancelControlID="btnOk" BackgroundCssClass="modalBackground2">
</asp:ModalPopupExtender>
<asp:Label ID="lblAbortClose" runat="server" Text=""></asp:Label>
<!-- end Risk details popup-->
<!-- start Risk details popup-->
<asp:Panel runat="server" ID="pnlRiskDetails" CssClass="left" BackColor="White" Width="400px"
    Height="200px">
    <div style="font-weight: bold; padding: 10px">
        The customer has following risks recorded against their current details.
    </div>
    <hr />
    <div style="height: 80px; overflow: auto; padding-top: 10px; padding-left: 20px">
        <asp:GridView ID="grdRisk" runat="server" AutoGenerateColumns="False" ShowHeader="false"
            GridLines="None">
            <Columns>
                <asp:BoundField DataField="CATDESC" />
                <asp:BoundField DataField="SUBCATDESC" />
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: center; padding-top: 30px">
        <asp:Button ID="btnOk" runat="server" Text="OK" Width="100px" />
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupRiskDetails" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblRiskDetailsClose" PopupControlID="pnlRiskDetails"
    DropShadow="true" CancelControlID="btnOk" BackgroundCssClass="modalBackground2">
</asp:ModalPopupExtender>
<asp:Label ID="lblRiskDetailsClose" runat="server" Text=""></asp:Label>
<!-- end Risk details popup-->
<!--End Assign to contractor popup-->
<!--Start Assign to contractor popup-->
<asp:Button ID="btnHiddenRaisePO" UseSubmitBehavior="false" runat="server" Text=""
    Style="display: none;" />
<asp:ModalPopupExtender ID="mdlpopupAssignToContractor" runat="server" DynamicServicePath=""
    TargetControlID="lblAssignToContractorClose" PopupControlID="pnlAssignToContractor"
    DropShadow="true" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlAssignToContractor" runat="server" class="modalPopupSchedular"
    Style="min-width: 600px; max-width: 750px; border-width: 1px; border: 1px solid black">
    <ucAssignFuelServicingToContractor:AssignFuelServicingToContractor ID="ucAssignFuelServicingToContractor"
        runat="server" />
</asp:Panel>
<asp:Label ID="lblAssignToContractorClose" runat="server" Text=""></asp:Label>