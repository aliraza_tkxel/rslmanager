﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefectAppointmentsArranged.ascx.vb"
    Inherits="Appliances.DefectAppointmentsArranged" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Import Namespace="AS_Utilities" %>
<asp:UpdatePanel ID="updPnlDefectsGrid" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <cc1:PagingGridView runat="server" ID="grdAppointmentArranged" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
            ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True" PageSize="30"
            ShowHeaderWhenEmpty="false" AllowPaging="false">
            <Columns>
                <asp:TemplateField HeaderText="JSD:" SortExpression="DefectId">
                    <ItemTemplate>
                        <asp:Label ID="JournalId" Text='<%# Eval("JSD") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                    <ItemTemplate>
                        <asp:Label ID="lblAddress" Text='<%# Eval("Address") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                    <ItemTemplate>
                        <asp:Label ID="lblScheme" Text='<%# Eval("Scheme") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Block:" SortExpression="Block">
                    <ItemTemplate>
                        <asp:Label ID="lblBlock" Text='<%# Eval("Block") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                    <ItemTemplate>
                        <asp:Label ID="lblPostCode" Text='<%# Eval("Postcode") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appliance:" SortExpression="Appliance">
                    <ItemTemplate>
                        <asp:Label ID="lblAppliance" Text='<%# Eval("Appliance") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Defect:" SortExpression="Defect">
                    <ItemTemplate>
                        <asp:Label ID="lblDefect" Text='<%# Eval("Defect") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status:" SortExpression="DefectStatus">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" Text='<%# Eval("DefectStatus") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Engineer:" SortExpression="Engineer">
                    <ItemTemplate>
                        <asp:Label ID="lblEngineer" Text='<%# Eval("Engineer") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appointment:" SortExpression="AppointmentDateSort">
                    <ItemTemplate>
                        <asp:Label ID="lblAppointmentTimes" Text='<%# Eval("AppointmentTimes") %>' runat="server"></asp:Label><br />
                        <asp:Label ID="lblAppointmentDate" Text='<%# Eval("AppointmentDate") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgbtnShowDefectJobSheet" ImageUrl="~/Images/rightArrow.png"
                            AlternateText=">" runat="server" OnClick="showDefectJobSheet" BorderStyle="None"
                            CommandArgument='<%# Eval("DefectId") %>' Visible='' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle Font-Bold="true" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <HeaderStyle CssClass="grdDefectAppointmentsHeader" />
           <%-- <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="7" />
            <PagerStyle BackColor="White" />--%>
        </cc1:PagingGridView>
        <div align="center">
                <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                    vertical-align: middle; float:left; width:100%;  ">
                    <table style="width: 55%;">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                        Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                        Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    Page:&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                    of&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
    </ContentTemplate>
</asp:UpdatePanel>
