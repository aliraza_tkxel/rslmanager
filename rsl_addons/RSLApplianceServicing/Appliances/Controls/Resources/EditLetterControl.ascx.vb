﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading
Imports System.Net
Imports System.Net.Mail
Imports iTextSharp.text
Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.html

Public Class EditLetterControl
    Inherits UserControlBase

    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objActionBL As ActionBL = New ActionBL()
    Dim objLetterBL As LettersBL = New LettersBL()
    Dim objUsersBL As UsersBL = New UsersBL()
    Dim stdLetterId As Integer
    Dim found As Boolean, selectedValue As Integer

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsNothing(Request.QueryString("id")) Then
                Dim strStdLetterId As String = Request.QueryString("id")
                If Not (IsPostBack) Then
                    If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
                        Me.stdLetterId = Int32.Parse(strStdLetterId)
                        ViewState(ViewStateConstants.StandardLetterId) = Me.stdLetterId
                        Me.loadLetter()
                    Else
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoLetterId, True)
                    End If

                    Me.loadSignOffDdl(Me.ddlSignOff)
                    Me.loadTeamDdl(Me.ddlTeams)
                    Me.loadFromUserDdl(Me.ddlFrom)
                End If
            Else
                uiMessageHelper.setMessage(Me.lblMessage, Me.pnlMessage, UserMessageConstants.InvalidLetterId, True)
            End If

        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Event Handlers"

#Region "ddl Teams Selected Index Changed "

    Protected Sub ddlTeams_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTeams.SelectedIndexChanged
        Me.loadFromUserDdl(Me.ddlFrom)
    End Sub
#End Region

#Region "btn Add Saved Letter Click"

    Protected Sub btnAddSavedLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddSavedLetter.Click
        Try
            If (saveLetterInSession() = True) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterSuccess, False)
                'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alertscript", "javascript: closePopup('1');", True)
            ElseIf ((saveLetterInSession() = False)) Then
                If (uiMessageHelper.Message = "You have already added this letter.") Then
                    'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alertscript", "javascript: closePopup('1');", True)
                End If
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Print Standard Letter Click"

    Protected Sub btnPrintStandardLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrintStandardLetter.Click

        Dim resultDataSet As DataSet = New DataSet()
        Dim propertyId As String = SessionManager.getPropertyIdForEditLetter()
        objLetterBL.getCustomerAndPropertyRbRc(resultDataSet, propertyId, -1, -1)
        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            Response.Write("<script type=""text/javascript"">alert(""" + UserMessageConstants.TanencyRecordNotFound + """);</script")
            Exit Sub
        End If


        Dim printLetterBo As PrintLetterBO = New PrintLetterBO()
        If (validateLetter()) Then
            printLetterBo.LetterTitle = txtLetterTitle.Text
            printLetterBo.LetterBody = txtLetterTemplate.Text
            printLetterBo.SignOff = ddlSignOff.SelectedItem.Text
            printLetterBo.ResourceTeamName = ddlTeams.SelectedItem.Text
            printLetterBo.From = ddlFrom.SelectedItem.Text
            printLetterBo.EmployeeID = ddlFrom.SelectedItem.Value
            SessionManager.setPrintLetterBo(printLetterBo)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "openPrintLetterWindow()", True)
        End If
    End Sub

#End Region

#End Region

#Region "Methods"

#Region "Save Letter In Session"
    Protected Function saveLetterInSession()
        Dim success As Boolean = False
        If (validateLetter() = True) Then
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow
            Dim letterAleadyAdded As Boolean = False

            dt.Columns.Add(ApplicationConstants.LetterTextTitleColumn)
            dt.Columns.Add(ApplicationConstants.LetterBodyColumn)
            dt.Columns.Add(ApplicationConstants.StandardLetterId)
            dt.Columns.Add(ApplicationConstants.TeamIdColumn)
            dt.Columns.Add(ApplicationConstants.FromResourceIdColumn)
            dt.Columns.Add(ApplicationConstants.SignOffCodeColumn)
            dt.Columns.Add(ApplicationConstants.RentBalanceColumn)
            dt.Columns.Add(ApplicationConstants.RentChargeColumn)
            dt.Columns.Add(ApplicationConstants.TodayDateColumn)

            If (Not IsNothing(SessionManager.getActivityLetterDetail())) Then
                dt = SessionManager.getActivityLetterDetail()

                If (isLetterAlreadyAdded(dt) = True) Then
                    success = False
                    letterAleadyAdded = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.LetterAlreadyExist, True)
                End If
            End If

            If (letterAleadyAdded = False) Then
                dr = dt.NewRow()
                dr(ApplicationConstants.LetterTextTitleColumn) = txtLetterTitle.Text
                dr(ApplicationConstants.LetterBodyColumn) = txtLetterTemplate.Text
                dr(ApplicationConstants.StandardLetterId) = ViewState(ViewStateConstants.StandardLetterId)
                dr(ApplicationConstants.TeamIdColumn) = ddlTeams.SelectedValue
                dr(ApplicationConstants.FromResourceIdColumn) = ddlFrom.SelectedValue
                dr(ApplicationConstants.SignOffCodeColumn) = ddlSignOff.SelectedValue
                dr(ApplicationConstants.RentBalanceColumn) = "0"
                dr(ApplicationConstants.RentChargeColumn) = "0"
                dr(8) = Date.Now
                dt.Rows.Add(dr)
                SessionManager.setActivityLetterDetail(dt)
                SessionManager.setActivityStandardLetterId(ViewState(ViewStateConstants.StandardLetterId))
                success = True
            End If
        End If
        Return success
    End Function
#End Region

#Region "If Letter Already Added"
    Protected Function isLetterAlreadyAdded(ByRef dt As DataTable)
        Dim isLetterAlreadyExist As Boolean = False
        Dim letterId As Integer = Integer.Parse(ViewState(ViewStateConstants.StandardLetterId))

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            Dim query = (From dataRow In dt _
                Where _
                    dataRow.Field(Of String)(ApplicationConstants.StandardLetterId) = letterId _
                Select dataRow).ToList()

            'Query the data table
            If query.Count > 0 Then
                isLetterAlreadyExist = True
            Else
                isLetterAlreadyExist = False
            End If
        End If
        Return isLetterAlreadyExist
    End Function
#End Region

#Region "Validate Letter "
    Protected Function validateLetter() As Boolean
        Dim success As Boolean = True
        If (IsNothing(Me.txtLetterTitle.Text) Or Me.txtLetterTitle.Text = String.Empty) Then
            success = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTitle, True)
        ElseIf (IsNothing(Me.txtLetterTemplate.Text) Or Me.txtLetterTemplate.Text = String.Empty) Then
            success = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoBody, True)
        ElseIf (Me.ddlSignOff.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoSignOFfLookupCode, True)
        ElseIf (Me.ddlTeams.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTeamId, True)
        ElseIf (Me.ddlFrom.SelectedItem.Value = ApplicationConstants.DropDownDefaultValue) Then
            success = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoFromResourceId, True)
        End If
        Return success
    End Function
#End Region

#Region "Reset Controls"
    Protected Sub resetControls()
        Me.txtLetterTitle.Text = String.Empty
        Me.txtLetterTemplate.Text = String.Empty
        Me.ddlSignOff.SelectedValue = ApplicationConstants.DropDownDefaultValue
        ddlTeams.SelectedValue = ApplicationConstants.DropDownDefaultValue
        Me.ddlFrom.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub
#End Region

#Region "Load Letter"
    Protected Sub loadLetter()
        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLetterById(Me.stdLetterId, resultSet)

        If (resultSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            Me.txtLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")
            Me.txtLetterTemplate.Text = "<div>" + resultSet.Tables(0).Rows(0).Item("Body") + "</br></br></div>" '<p style='margin: 0cm 0cm 0pt;'>&nbsp;</p>
        End If
        
    End Sub
#End Region

#Region "load Sign Off Ddl"

    Protected Sub loadSignOffDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        found = False
        selectedValue = 0
        objLetterBL.getSignOffTypes(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New System.Web.UI.WebControls.ListItem("Select Sign Off Type", ApplicationConstants.DropDownDefaultValue))

        ' auto load salutation "Yours Sincerely" by default
        For i = 0 To ddlList.Count - 1
            If StrComp(ApplicationConstants.SignOff, ddlList.Item(i).Description) = 0 Then
                found = True
                selectedValue = ddlList.Item(i).ID
                Exit For
            End If
        Next

        If (ddlList.Count > 0 And found) Then
            ddl.SelectedValue = selectedValue
        Else
            ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
        End If
    End Sub
#End Region

#Region "load Team Ddl "
    Protected Sub loadTeamDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        found = False
        selectedValue = 0
        objLetterBL.getTeams(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New System.Web.UI.WebControls.ListItem("Select Team", ApplicationConstants.DropDownDefaultValue))

        '  auto load salutation "Customer Services" by default
        For i = 0 To ddlList.Count - 1
            If StrComp(ApplicationConstants.Team, ddlList.Item(i).Description) = 0 Then
                found = True
                selectedValue = ddlList.Item(i).ID
                Exit For
            End If
        Next

        If (ddlList.Count > 0 And found) Then
            ddl.SelectedValue = selectedValue
        Else
            ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
        End If

    End Sub
#End Region

#Region "load From User Ddl"
    Protected Sub loadFromUserDdl(ByRef ddl As DropDownList)
        Dim teamSelected As Int32 = CType(Me.ddlTeams.SelectedItem.Value, Int32)

        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        found = False
        selectedValue = 0
        objLetterBL.getFromUserByTeamId(teamSelected, ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New System.Web.UI.WebControls.ListItem("Select From", ApplicationConstants.DropDownDefaultValue))

        '  auto load salutation "logged in user" by default but he must exist in the "Customer Services" Team
        For i = 0 To ddlList.Count - 1
            If StrComp(SessionManager.getAppServicingUserId, ddlList.Item(i).ID) = 0 Then
                found = True
                selectedValue = ddlList.Item(i).ID
                Exit For
            End If
        Next

        If (ddlList.Count > 0 And found) Then
            ddl.SelectedValue = selectedValue
        Else
            ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
        End If

    End Sub
#End Region

#Region "btn Send Click"
    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If (validateLetter()) Then

                Dim letterAleadyAdded As Boolean = False
                Dim dt As DataTable = New DataTable()
                If (Not IsNothing(SessionManager.getActivityLetterDetail())) Then
                    dt = SessionManager.getActivityLetterDetail()

                    If (isLetterAlreadyAdded(dt) = True) Then
                        letterAleadyAdded = True
                    End If
                End If

                If (letterAleadyAdded = True) Then
                    txtSendTo.Text = SessionManager.getSelectedPropertySchedulingBo().CustomerEmail
                    Me.mdlPopUpSendEmail.Show()
                Else
                    If (Me.saveLetterInSession() = True) Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterSuccess, False)
                        txtSendTo.Text = SessionManager.getSelectedPropertySchedulingBo().CustomerEmail
                        Me.mdlPopUpSendEmail.Show()
                    End If
                End If
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Send Action Click"
    Protected Sub btnSendAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSendAction.Click
        Dim dt As DataTable = New DataTable()
        If (Not IsNothing(SessionManager.getActivityLetterDetail())) Then
            dt = SessionManager.getActivityLetterDetail()
            Dim EmailStatus As String = UserMessageConstants.EmailNotSent
            If Validation.isEmail(txtSendTo.Text) Then
                SendPDFEmail(dt, EmailStatus)
                Response.Write("<script>window.opener.fireCheckboxEventForAddApp()</" + "script>")
                Response.Write("<script>window.opener.fireLetterUploadCheckboxEvent()</" + "script>")
                Response.Write("<script>window.close();</" + "script>")
            Else
                uiMessageHelper.setMessage(lblSendEmailMessage, pnlSendEmailMessage, UserMessageConstants.InvalidEmail, True)
                Me.mdlPopUpSendEmail.Show()
            End If

        End If
    End Sub
#End Region

    Private Sub SendPDFEmail(ByVal dt As DataTable, ByVal emailStatus As String)

        Dim propertySchedulingBo = SessionManager.getSelectedPropertySchedulingBo()

        Using sw As New StringWriter()
            Using hw As New HtmlTextWriter(sw)

                '=========================================================================================
                Dim tenancyRefValue As String = propertySchedulingBo.TenancyId.ToString()
                Dim tenantNameValue As String = propertySchedulingBo.CustomerName
                Dim houseNumber As String = propertySchedulingBo.HouseNumber
                Dim addressLine1Value As String = propertySchedulingBo.Address1
                Dim addressLine2Value As String = propertySchedulingBo.Address2
                Dim townCityValue As String = propertySchedulingBo.TownCity
                Dim countyValue As String = propertySchedulingBo.County
                Dim postCodeValue As String = propertySchedulingBo.PostCode
                Dim letterTitleValue As String = txtLetterTitle.Text
                Dim letterBodyValue As String = txtLetterTemplate.Text
                Dim signOffValue As String = ddlSignOff.SelectedItem.Text
                Dim fromResourceValue As String = ddlFrom.SelectedItem.Text
                Dim teamValue As String = ddlTeams.SelectedItem.Text
                Dim fromEmailValue As String = objUsersBL.GetPropertyEmployeeEmail(ddlFrom.SelectedItem.Value)
                Dim fromDirectDialValue As String = objUsersBL.GetEmployeeWorkMobile(ddlFrom.SelectedItem.Value)
                Dim rentBalance As Double = dt.Rows(0).Item("RentBalance")
                Dim rentCharge As Double = dt.Rows(0).Item("RentCharge")
                Dim todayDate As Date = dt.Rows(0).Item("TodayDate")

                letterBodyValue = GeneralHelper.repalceRcRBTd(letterBodyValue, rentBalance, rentCharge, todayDate)

                'Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO("A3002320", New Date(), "Hussain Ali", "21", "Street 15", "Sector D, Askari X", "Lahore", "Punjab", "54400", "You have been awarded a new house", "Congratulations<br /> You have been given a new house. Congratulations !!", "Its been a real pleasure", "Altaf Hussain", "Askari X Management", "hussain@tkxel.com", "0332-8400486")
                Dim savedLetterPDFBO As SavedLetterPDFBO = New SavedLetterPDFBO(tenancyRefValue, todayDate, tenantNameValue, houseNumber, addressLine1Value, addressLine2Value, townCityValue, countyValue, postCodeValue, letterTitleValue, letterBodyValue, signOffValue, fromResourceValue, teamValue, fromEmailValue, fromDirectDialValue)

                Dim letterHTML As StringBuilder = New StringBuilder(String.Empty)
                letterHTML.Append("<html><body>")
                letterHTML.Append("<div id='print_content' style='position: absolute; font-family: Arial;font-size: 11pt;'>")
                letterHTML.Append("Tenancy Ref:").Append(savedLetterPDFBO.TenancyRef).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.TenantName).Append("<br>")

                letterHTML.Append(savedLetterPDFBO.HouseNumber).Append(", ").Append(savedLetterPDFBO.AddressLine1).Append("<br>")

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.AddressLine2) Then
                    letterHTML.Append(savedLetterPDFBO.AddressLine2).Append("<br>")
                End If
                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.TownCity) Then
                    letterHTML.Append(savedLetterPDFBO.TownCity).Append("<br>")
                End If

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.CountyValue) Then
                    letterHTML.Append(savedLetterPDFBO.CountyValue).Append("<br>")
                End If

                If Not String.IsNullOrWhiteSpace(savedLetterPDFBO.PostCode) Then
                    letterHTML.Append(savedLetterPDFBO.PostCode).Append("<br>")
                End If

                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.LetterDate.ToString("dd MMMM yyyy")).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append("Dear ").Append(savedLetterPDFBO.TenantName).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.LetterBody).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.SignOff).Append("<br>")
                letterHTML.Append("<br>")

                letterHTML.Append(savedLetterPDFBO.FromResource).Append("<br>")

                letterHTML.Append(savedLetterPDFBO.Team).Append("<br>")
                letterHTML.Append("Direct Dial: ").Append(savedLetterPDFBO.DirectDial).Append("<br>")
                letterHTML.Append("Email: ").Append(savedLetterPDFBO.DirectEmail).Append("<br>")

                letterHTML.Append("</div>")
                letterHTML.Append("</body></html>")

                '=========================================================================================
                Dim sr As New StringReader(letterHTML.ToString())

                Dim pdfDoc As New Document(iTextSharp.text.PageSize.A4, 56.692913386, 34.015748031, 85.039370079, 0) 'Margins are in points and 72 points equal 1 inch
                Dim htmlparser As New HTMLWorker(pdfDoc)
                Using memoryStream As New MemoryStream()
                    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, memoryStream)
                    pdfDoc.Open()
                    htmlparser.Parse(sr)
                    pdfDoc.Close()
                    Dim bytes As Byte() = memoryStream.ToArray()
                    memoryStream.Close()

                    If Validation.isEmail(txtSendTo.Text) Then
                        emailStatus = UserMessageConstants.EmailSending
                        Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                        Dim mailMessage As New Mail.MailMessage

                        mailMessage.Subject = subject
                        mailMessage.To.Add(New MailAddress(txtSendTo.Text, propertySchedulingBo.CustomerName))
                        mailMessage.Attachments.Add(New Attachment(New MemoryStream(bytes), "Letter1.pdf"))
                        mailMessage.IsBodyHtml = True

                        EmailHelper.sendEmail(mailMessage)
                        emailStatus = UserMessageConstants.EmailSent
                        SessionManager.setSendLetter(True)
                    Else
                        uiMessageHelper.setMessage(lblSendEmailMessage, pnlSendEmailMessage, UserMessageConstants.InvalidEmail, True)
                        'Throw New Exception(UserMessageConstants.InvalidEmail)
                    End If
                End Using
            End Using
        End Using
        propertySchedulingBo.CustomerEmailStatus = emailStatus
        SessionManager.setSelectedPropertySchedulingBo(propertySchedulingBo)
    End Sub

#End Region


End Class
