﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading


Public Class ViewLetter
    Inherits UserControlBase

    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objActionBL As ActionBL = New ActionBL()
    Dim objLetterBL As LettersBL = New LettersBL()
    Dim stdLetterId As Integer

    'define the delegate handler signature and the event that will be raised
    'to send the message
    'Public Delegate Sub customMessageHandler(ByVal sender As System.Object, ByVal e As MessageEventArgs)
    'Public Shared Event OnSend As customMessageHandler

    'Private Sub btnSendMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendMessage.Click
    '    Dim messageArgs As New MessageEventArgs
    '    messageArgs.message = "This message came from the source user control"
    '    Dim abc As Activities = New Activities()
    '    abc.updateLabel2()

    '    RaiseEvent OnSend(Me, messageArgs)
    'End Sub  'btnSendMessage_Click

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not (IsPostBack) Then

                Dim strStdLetterId As String = Request.QueryString("id")

                If (Int32.TryParse(strStdLetterId, stdLetterId)) Then
                    Me.stdLetterId = Int32.Parse(strStdLetterId)
                    Me.LoadLetter()
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoLetterId, True)
                End If


                Me.LoadSignOffDdl(Me.ddlSignOff)
                Me.LoadTeamDdl(Me.ddlTeams)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#Region "Event Handlers"

    Protected Sub ddlTeams_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTeams.SelectedIndexChanged
        Me.loadFromUserDdl(Me.ddlFrom)
    End Sub

    Protected Sub btnAddSavedLetter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddSavedLetter.Click

        Try
            Dim letterTitle As String = Me.txtLetterTitle.Text
            Dim letterBody As String = Me.txtLetterTemplate.Text

            Dim signOffLookupCode As Integer = Me.ddlSignOff.SelectedItem.Value
            Dim teamId As Integer = Me.ddlTeams.SelectedItem.Value
            Dim fromResourceId As Integer = Me.ddlFrom.SelectedItem.Value

            If (IsNothing(letterTitle) Or letterTitle = String.Empty) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTitle, True)
                Return
            ElseIf (IsNothing(letterBody) Or letterBody = String.Empty) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoBody, True)
                Return
            ElseIf (signOffLookupCode = ApplicationConstants.DropDownDefaultValue) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoSignOFfLookupCode, True)
                Return
            ElseIf (teamId = ApplicationConstants.DropDownDefaultValue) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoTeamId, True)
                Return
            ElseIf (fromResourceId = ApplicationConstants.DropDownDefaultValue) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoFromResourceId, True)
                Return
            End If

            Dim strStdLetterID As String = Request.QueryString("letterID")
            If (Int32.TryParse(strStdLetterID, stdLetterId)) Then
                Me.stdLetterId = Int32.Parse(strStdLetterID)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterNoLetterId, True)
            End If

            Dim savedLetterBO As SavedLetterBO = New SavedLetterBO(5, letterBody, letterTitle, Me.stdLetterId, teamId, fromResourceId, signOffLookupCode)
            Dim savedLetterOptionsBO As SavedLetterOptionsBO = New SavedLetterOptionsBO(-1, -1, Nothing)

            Dim resultSet As DataSet = New DataSet()

            objLetterBL.addCustomSavedLetter(savedLetterBO, savedLetterOptionsBO, resultSet)


            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterSuccess, False)

            Me.txtLetterTitle.Text = String.Empty
            Me.txtLetterTemplate.Text = String.Empty

            Me.LoadSignOffDdl(Me.ddlSignOff)
            Me.LoadTeamDdl(Me.ddlTeams)
            Me.loadFromUserDdl(Me.ddlFrom)
            Me.LoadLetter()

        Catch ex As Exception
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedLetterFailure, True)
        End Try


    End Sub

#End Region

#Region "Private methods"

    Protected Sub LoadLetter()
        Dim resultSet As DataSet = New DataSet()

        objLetterBL.getLetterById(Me.stdLetterId, resultSet)

        Me.txtLetterTitle.Text = resultSet.Tables(0).Rows(0).Item("Title")

        Me.txtLetterTemplate.Text = resultSet.Tables(0).Rows(0).Item("Body")

    End Sub

    Protected Sub LoadSignOffDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getSignOffTypes(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Sign Off Type", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub

    Protected Sub LoadTeamDdl(ByRef ddl As DropDownList)
        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getTeams(ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select Team", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue

    End Sub

    Protected Sub loadFromUserDdl(ByRef ddl As DropDownList)
        Dim teamSelected As Int32 = CType(Me.ddlTeams.SelectedItem.Value, Int32)

        Dim id As String = "ID"
        Dim codeName As String = "Description"
        Dim ddlList As List(Of DropDownBO) = New List(Of DropDownBO)()
        objLetterBL.getFromUserByTeamId(teamSelected, ddlList)

        ddl.DataSource = ddlList
        ddl.DataValueField = id
        ddl.DataTextField = codeName
        ddl.DataBind()

        ddl.Items.Add(New ListItem("Select From", ApplicationConstants.DropDownDefaultValue))
        ddl.SelectedValue = ApplicationConstants.DropDownDefaultValue
    End Sub

#End Region

End Class

'The following class provides the definition of the custom event arguments
'used as the event arguments for the message sent from this control
'This class simply inherits from System.EventArgs and adds a message property
'Public Class MessageEventArgs
'    Inherits EventArgs
'    Private mMessage As String
'    Public Property message() As String
'        Get
'            Return (mMessage)
'        End Get
'        Set(ByVal Value As String)
'            mMessage = Value
'        End Set
'    End Property

'End Class