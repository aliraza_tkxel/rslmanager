﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class StatusAndActions
    Inherits UserControlBase
#Region "Attributes"
    Dim objStatusBL As StatusBL = New StatusBL()
#End Region

#Region "Functions"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Me.getStatus()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub getStatus()

        Dim resultDataset As DataSet = New DataSet()
        objStatusBL.getStatus(resultDataset)
        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RecordNotFound, True)
        Else
            grdStatus.DataSource = resultDataset
            grdStatus.DataBind()

        End If
    End Sub

    Protected Sub btnEditStatus_Click(sender As Object, e As EventArgs)
        Try
            Dim statusId As String = CType(CType(sender, Button).CommandArgument, String)
            Session.Add(SessionConstants.StatusIdforEdit, statusId)
            Response.Redirect("~/Views/Resources/Status.aspx?id=" + statusId, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub grdStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdStatus.SelectedIndexChanged

    End Sub

#End Region



End Class