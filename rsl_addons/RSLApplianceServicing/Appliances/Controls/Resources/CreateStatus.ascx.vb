﻿Imports System
Imports AS_BusinessLogic
Imports AS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class CreateStatus
    Inherits UserControlBase
#Region "Attributes"


    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objUserBL As UsersBL = New UsersBL()
    Dim objResourceBL As ResourcesBL = New ResourcesBL()

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        If Not IsPostBack Then
            Me.LoadInspectionAndRankingDDL(False)
        End If



    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Try

            Dim inspectionType As Integer = 1  'ddlType.SelectedItem.Value
            Dim statusTitle As String = txtBoxTitle.Text
            Dim selectedRanking As Integer = CType(ddlRanking.SelectedItem.Text, Integer)
            If Not statusTitle = "" Then
                If statusTitle.Length <= 25 Then
                    objStatusBL.addStatus(inspectionType, statusTitle, selectedRanking)
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusSavedSuccessfuly, False)
                    Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
                    Dim tree As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
                    reloadTreeView()
                    updPanelStatus.Update()
                    txtBoxTitle.Text = String.Empty
                    LoadInspectionAndRankingDDL(False)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.statusLength, True)
                End If
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.statusTitle, True)

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click

        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim StatusControl As UserControl = CType(updPanelStatus.FindControl("sControl"), UserControl)
        StatusControl.Visible = False


    End Sub

    Public Sub LoadInspectionAndRankingDDL(ByVal checkEdit As Boolean)
        Dim resultDataSet As DataSet = New DataSet()
        Dim InspectionDataSet As DataSet = New DataSet()
        Try
            ddlRanking.Items.Clear()
            objStatusBL.getStatus(resultDataSet)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next
            If checkEdit = False Then
                ddlRanking.Items.Add(New ListItem((resultDataSet.Tables(0).Rows.Count + 1).ToString(), "-1"))
            End If
            ViewState(ViewStateConstants.RankingCount) = resultDataSet.Tables(0).Rows.Count + 1
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Dim StatusId As Integer = 0
        'SessionManager.getSessionVariable(SessionConstants.StatusIdforEdit, StatusId)      'ViewState(ViewStateConstants.StatusIdforEdit)
        Try
            StatusId = Session.Item(SessionConstants.StatusIdforEdit)
            Dim inspectionType As Integer = 1  'ddlType.SelectedItem.Value
            Dim statusTitle As String = txtBoxTitle.Text
            If Not statusTitle = "" Then
                Dim modifiedBy As Integer = SessionManager.getAppServicingUserId()
                Dim selectedRanking As Integer = CType(ddlRanking.SelectedItem.Text, Integer)
                objResourceBL.editStatus(StatusId, inspectionType, statusTitle, selectedRanking, modifiedBy)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.StatusUpdatedSuccessfuly, False)
                Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
                Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
                tree.PopulateNodesFromClient = True
                reloadTreeView()
                updPanelStatus.Update()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, "Status Title is required", True)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub reloadTreeView()
        Try
            Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
            Dim objStatus As Status = New Status()
            tree.Nodes(0).ChildNodes.Clear()
            tree.Height = 0
            objStatus.PopulateNodes(tree.Nodes(0))
            objStatus.PopulateActions(tree.Nodes(0))
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub


    Sub refreshView(ByRef node As TreeNode)
        CollapseAndExpandNode(node.Parent)
    End Sub


    Sub CollapseAndExpandNode(ByRef node As TreeNode)
        node.Collapse()
        node.Expand()
        SelectNode()
    End Sub

    Sub SelectNode()
        Dim node As TreeNode
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
        For Each node In tree.Nodes
            If node.ChildNodes.Count > 0 Then
                Dim node1 As TreeNode
                For Each node1 In node.ChildNodes
                    If node1.Text = Session("SelectedNodeText") Then
                        node1.Select()
                    End If
                    If node1.ChildNodes.Count > 0 Then
                        Dim node2 As TreeNode
                        For Each node2 In node1.ChildNodes
                            If node2.Text = Session("SelectedNodeText") Then
                                node2.Select()
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End Sub
End Class