﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AccessRights.ascx.vb"
    Inherits="Appliances.AccessRights" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server" CssClass="message"></asp:Label>
</asp:Panel>
<div class="editAccessRights">
    <h3 style="margin-top: -3px;">
        <asp:Label runat="server" ID="lblUserName"></asp:Label>
    </h3>
    <asp:Panel runat="server" ID="pnlAccessRight" Visible="true" CssClass="pnlAccessRight">
        <asp:TreeView runat="server" ID="trVwAccess" ShowLines="True" ForeColor="Black" ExpandDepth="1"
            ShowCheckBoxes="All" LineImagesFolder="~/TreeLineImages">
            <Nodes>
                <asp:TreeNode PopulateOnDemand="true" Text="Menu" ShowCheckBox="false"></asp:TreeNode>
            </Nodes>
        </asp:TreeView>
        <br />
        <div style="margin: 0 0 0 26px;">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
            <asp:Button ID="btnSaveRights" runat="server" Text="Save Access Rights" CommandArgument="" />
        </div>
    </asp:Panel>
</div>
