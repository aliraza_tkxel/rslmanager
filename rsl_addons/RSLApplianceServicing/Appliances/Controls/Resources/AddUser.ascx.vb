﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.IO
Imports System.Globalization
Imports System.Threading

Public Class AddUser
    Inherits UserControlBase

#Region "Attributes"

    Dim objUsersBL As UsersBL = New UsersBL()
    Public savingTradeIds As List(Of String) = New List(Of String)
    Public savingTradesList As List(Of String) = New List(Of String)
    Public selectedTrades As List(Of String) = New List(Of String)
    Public selectedTradeIds As List(Of String) = New List(Of String)

#End Region

#Region "Events"

#Region "Page Load event"
    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Try
            If Not IsPostBack Then
                Me.LoadTypeDll(ddlUserTypeOptions)
                Me.loadInspectionTypes()
                Me.loadTrades()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "AddUser Events"

#Region "Find Button Event"
    ''' <summary>
    ''' Find Button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        Dim resultDataSet As DataSet = New DataSet()
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim strtoFind As String = txtFind.Text
        If strtoFind.Contains(" ") Then
            firstName = strtoFind.Substring(0, strtoFind.IndexOf(" "))
            lastName = strtoFind.Substring(strtoFind.IndexOf(" ") + 1)
        Else
            firstName = strtoFind
            lastName = strtoFind
        End If

        Dim pnlAddUserParent = DirectCast(Parent.FindControl("pnlAddUserControl"), Panel)
        pnlAddUserParent.Visible = True

        Try
            objUsersBL.getQuickFindUsers(firstName, lastName, resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                txtFind.Text = resultDataSet.Tables(0).Rows(0).Item("FIRSTNAME").ToString() + "  " + resultDataSet.Tables(0).Rows(0).Item("LASTNAME").ToString()
                ViewState(ViewStateConstants.SaveEmployeeId) = resultDataSet.Tables(0).Rows(0).Item("EMPLOYEEID")
                ViewState(ViewStateConstants.SaveGSRENo) = resultDataSet.Tables(0).Rows(0).Item("GSRENo")
                txtFind.DataBind()
                SessionManager.setSelectedResourceEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EMPLOYEEID"))
                saveCoreAndOutOfHoursInSession()
                populateCoreWorkingHours()

            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoUserFind, True)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "File Check Changed Event"
    ''' <summary>
    ''' File Check Changed Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckBoxFileName_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ckBoxFileName.CheckedChanged
        setSignatureFileName()
    End Sub

#End Region

#End Region

#Region "Core Working Hours Events"

#Region "Bind Row Data"
    ''' <summary>
    ''' Bind Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdCoreWorkingHours_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCoreWorkingHours.RowDataBound
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim ddlFrom As DropDownList = DirectCast(e.Row.FindControl("ddlFrom"), DropDownList)
                Dim ddlTo As DropDownList = DirectCast(e.Row.FindControl("ddlTo"), DropDownList)
                Dim lblDay As Label = DirectCast(e.Row.FindControl("lblWeekDay"), Label)
                Dim hdnSelectedFrom As HiddenField = DirectCast(e.Row.FindControl("hdnStartTime"), HiddenField)
                Dim hdnSelectedTo As HiddenField = DirectCast(e.Row.FindControl("hdnEndTime"), HiddenField)
                Dim officialFrom As String = GeneralHelper.getCoreDayStartHour()
                Dim officialTo As String = GeneralHelper.getCoreDayEndHour()
                Dim resultDs As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()

                Dim coreDt As DataTable = resultDs.Tables(ApplicationConstants.EmpCoreHoursDataTable)

                ' Populate dropdowns ( Range 00:00 - 23:30 )
                GeneralHelper.populateCoreHoursWithThirtyMinDuration(ddlFrom)
                GeneralHelper.populateCoreHoursWithThirtyMinDuration(ddlTo)

                ' Set time to default office hours if there is no pre-selected values.
                If (hdnSelectedFrom.Value = Nothing) Then
                    If (coreDt.Rows.Count > 0) Then
                        ddlFrom.SelectedIndex = 0
                    Else
                        ddlFrom.SelectedValue = officialFrom
                    End If

                Else
                    ddlFrom.SelectedValue = hdnSelectedFrom.Value
                End If

                If ((lblDay.Text.Equals("Sunday") Or lblDay.Text.Equals("Saturday")) And String.IsNullOrEmpty(hdnSelectedFrom.Value)) Then
                    ddlFrom.SelectedIndex = 0
                End If

                If (hdnSelectedTo.Value = Nothing) Then

                    If (coreDt.Rows.Count > 0) Then
                        ddlTo.SelectedIndex = 0
                    Else
                        ddlTo.SelectedValue = officialTo
                    End If

                Else
                    ddlTo.SelectedValue = hdnSelectedTo.Value
                End If

                If ((lblDay.Text.Equals("Sunday") Or lblDay.Text.Equals("Saturday")) And String.IsNullOrEmpty(hdnSelectedTo.Value)) Then
                    ddlTo.SelectedIndex = 0
                End If

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Cancel Core button Event"
    ''' <summary>
    ''' Cancel Core button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCoreCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCoreCancel.Click

        Try
            populateCoreWorkingHours()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Save Core Working Hours button Event"
    ''' <summary>
    ''' Save Core Working Hours button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveCoreWorkingHours_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveCoreWorkingHours.Click



        Try
            Dim isSaved As Boolean = True

            validateCoreWorkingHours()

            If (lblHeader.Text.Equals(ApplicationConstants.AddUserHeader)) Then
                isSaved = saveBasicUserInfo()
            Else
                isSaved = editBasicUserInfo()
            End If


            If (isSaved) Then
                saveCoreWorkingHours()
                saveOutOfHours()

                If (lblHeader.Text.Equals(ApplicationConstants.AddUserHeader)) Then
                    loadAllPanels()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserAddedSuccessfully, False)
                Else
                    saveCoreAndOutOfHoursInSession()
                    editUser(getSelectedEmployeeId())
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedSuccessfully, False)
                End If

                Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
                updatePnlResources.Update()

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Out Of Hours Events"

#Region "Add button Event"
    ''' <summary>
    ''' Add button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click

        Try

            Dim objOutOfHoursSearchBO As OutOfHoursSearchBO = New OutOfHoursSearchBO()
            objOutOfHoursSearchBO.TypeId = Convert.ToInt32(ddlType.SelectedValue)
            objOutOfHoursSearchBO.Type = ddlType.SelectedItem.Text
            objOutOfHoursSearchBO.StartDate = txtFromDate.Text
            objOutOfHoursSearchBO.EndDate = txtToDate.Text
            objOutOfHoursSearchBO.FromTime = ddlFromTime.SelectedValue
            objOutOfHoursSearchBO.ToTime = ddlToTime.SelectedValue

            validateSelectedTime(objOutOfHoursSearchBO)
            addOutOfHours(objOutOfHoursSearchBO)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Bind Row Data"
    ''' <summary>
    ''' Bind Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdOutOfHoursManagement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOutOfHoursManagement.RowDataBound
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lblFrom As Label = DirectCast(e.Row.FindControl("lblFrom"), Label)
                Dim lblTo As Label = DirectCast(e.Row.FindControl("lblTo"), Label)
                Dim hdnStartDate As HiddenField = DirectCast(e.Row.FindControl("hdnStartDate"), HiddenField)
                Dim hdnEndDate As HiddenField = DirectCast(e.Row.FindControl("hdnEndDate"), HiddenField)

                If (Not IsNothing(hdnStartDate.Value)) Then
                    lblFrom.Text = DateTime.Parse(hdnStartDate.Value).ToString("ddd dd MMM yyyy HH:mm")
                End If

                If (Not IsNothing(hdnEndDate.Value)) Then
                    lblTo.Text = DateTime.Parse(hdnEndDate.Value).ToString("ddd dd MMM yyyy HH:mm")
                End If

            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Delete button click"
    ''' <summary>
    ''' Delete button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
            Dim lnkBtnDelete As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnDelete.NamingContainer, GridViewRow)
            Dim outOfHoursId As Integer = Convert.ToInt32(lnkBtnDelete.CommandArgument)
            Dim outOfHoursDeletedDt As DataTable = SessionManager.getOutOfHoursDeletedList()
            If (Not outOfHoursId = -1) Then
                outOfHoursDeletedDt.Rows.Add(outOfHoursId)
            End If

            resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Rows.RemoveAt(row.RowIndex)
            resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).AcceptChanges()
            SessionManager.setCoreAndOutOfHoursDataSet(resultDataset)
            SessionManager.setOutOfHoursDeletedList(outOfHoursDeletedDt)
            populateOutOfHoursManagement()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region



#Region "Add trade button"
    ''' <summary>
    ''' Delete button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub addTradeBtn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addTradeButton.Click
        Try

            Dim count As Integer
            Dim x As String
            If (grdTrade.Rows.Count > 0) Then
                For Each arr In grdTrade.Rows
                    x = grdTrade.Rows(count).Cells(0).Text
                    count = count + 1
                    Me.selectedTrades.Add(x)
                    If Me.tradeDropdownlist.SelectedItem.ToString().Equals(x) Then
                        Return
                    End If
                Next
            End If

            If (grdTrade.Rows.Count <= 0) Then
                x = Me.tradeDropdownlist.SelectedItem.ToString()
            End If

            Me.selectedTrades.Add(Me.tradeDropdownlist.SelectedItem.ToString())

            Dim selected As String = tradeDropdownlist.SelectedItem.ToString()

            Dim dataTable As New DataTable()

            dataTable.Columns.Add("TRADE")
            For Each arr In Me.selectedTrades
                Dim arrid2 As String() = {arr}
                dataTable.LoadDataRow(arrid2, True)
            Next


            grdTrade.DataSource = dataTable
            grdTrade.DataBind()


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region


#Region "Delete trade button"
    ''' <summary>
    ''' Delete button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub deleteTradeButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try


            Dim rowIndex As Integer
            Dim btn As ImageButton = sender



            Dim count As Integer
            Dim x As String
            If (grdTrade.Rows.Count > 0) Then
                For Each arr In grdTrade.Rows
                    x = grdTrade.Rows(count).Cells(0).Text
                    count = count + 1
                    Me.selectedTrades.Add(x)                   
                Next
            End If

            Me.selectedTrades.RemoveAt(btn.CommandArgument)


            

            Dim dataTable As New DataTable()

            dataTable.Columns.Add("TRADE")
            For Each arr In Me.selectedTrades
                Dim arrid2 As String() = {arr}
                dataTable.LoadDataRow(arrid2, True)
            Next


            grdTrade.DataSource = dataTable
            grdTrade.DataBind()


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Cancel button Event"
    ''' <summary>
    ''' Cancel button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click

        Try
            Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
            Dim pnlAddUser As Panel = CType(updatePnlResources.FindControl("pnlAddUserControl"), Panel)
            pnlAddUser.Visible = False
            updatePnlResources.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Save Button Event"
    ''' <summary>
    ''' Save Button Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click


        Try
            ' validateCoreWorkingHours()
            Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)

            Dim isSaved As Boolean = True
            isSaved = saveBasicUserInfo()

            If (isSaved) Then
                'saveCoreWorkingHours()
                saveOutOfHours()
                loadAllPanels()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserAddedSuccessfully, False)
                updatePnlResources.Update()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Edit click"
    ''' <summary>
    ''' Edit click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Try
            Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
            validateCoreWorkingHours()
            If (editBasicUserInfo()) Then
                saveCoreWorkingHours()
                saveOutOfHours()
                saveCoreAndOutOfHoursInSession()
                loadUsers()
                populateOutOfHoursManagement()
            End If
            updatePnlResources.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#End Region

#End Region

#Region "Functions"

#Region "Load All Panels"
    ''' <summary>
    ''' Load All Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub loadAllPanels()

        ' For new user employee id = -1
        SessionManager.removeSelectedResourceEmployeeId()
        SessionManager.setSelectedResourceEmployeeId(ApplicationConstants.NewEmployeeId)
        saveCoreAndOutOfHoursInSession()
        populateBasicInfoPanel()
        populateCoreWorkingHours()
        populateOutOfHoursManagement()
        Me.pnlAddUser.Visible = True
    End Sub

#End Region

#Region "Save Core and Out of Hours in Session"
    ''' <summary>
    ''' Save Core and Out of Hours in Session
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveCoreAndOutOfHoursInSession()

        SessionManager.removeCoreAndOutOfHoursDataSet()
        SessionManager.removeOutOfHoursDeletedList()

        Dim employeeId As Integer = SessionManager.getSelectedResourceEmployeeId()
        Dim resultDs As DataSet = New DataSet()
        Dim objResourcesBL As ResourcesBL = New ResourcesBL()
        objResourcesBL.getCoreAndOutOfHoursInfo(resultDs, employeeId)

        SessionManager.setCoreAndOutOfHoursDataSet(resultDs)

    End Sub

#End Region

#Region "AddUser Functions"

#Region "Edit Basic User Info"
    ''' <summary>
    ''' Edit Basic User Info
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function editBasicUserInfo()

        Dim resultDataSet As DataSet = New DataSet()
        Dim isEdited As Boolean
        Dim ModifiedBy As Integer = SessionManager.getAppServicingUserId()
        Dim userViewStatSchemeList As List(Of Integer) = New List(Of Integer)()
        Dim userViewStatPatchList As List(Of Integer) = New List(Of Integer)()
        Dim signatureFileName As String = txtSignature.Text
        Dim seletedUserTypeText As String = String.Empty
        Dim EmployeeId As Int32 = CType(ViewState(ViewStateConstants.SaveEmployeeId), Int32)


        Try
            Dim GSREno As String = CType(ViewState(ViewStateConstants.SaveGSRENo), String)
            Dim seletedUserTypeValue As Int32 = CType(ddlUserTypeOptions.SelectedValue, Int32)
            seletedUserTypeText = ddlUserTypeOptions.SelectedItem.Text
            Dim firstName As String = String.Empty
            Dim lastName As String = String.Empty
            Dim chkGasProperty As Boolean = False
            Dim chkBoxlstChecked As Boolean = False

            Dim name As String = txtFind.Text

            validateUserName()

            If EmployeeId = 0 Then
                Dim userInfoDataSet As DataSet = New DataSet()
                Try
                    firstName = name.Substring(0, name.IndexOf(" "))
                    lastName = name.Substring(name.IndexOf(" ") + 1)
                Catch ex As Exception
                    Throw New Exception(UserMessageConstants.NoEmployeFound)
                End Try
                objUsersBL.getEmployeeIdByName(firstName, lastName, userInfoDataSet)
                If userInfoDataSet.Tables(0).Rows.Count > 0 Then
                    GSREno = userInfoDataSet.Tables(0).Rows(0).Item("GSREno")
                    EmployeeId = userInfoDataSet.Tables(0).Rows(0).Item("EMPLOYEEID")
                Else
                    Throw New Exception(UserMessageConstants.NoEmployeFound)
                End If

            End If

            For Each chkBox As ListItem In chkBoxLst.Items
                If chkBox.Text.Equals("Gas") And chkBox.Selected = True Then
                    chkGasProperty = True
                End If
                If chkBox.Selected = True Then
                    chkBoxlstChecked = True
                End If
            Next
            If chkBoxlstChecked = True Then
                objUsersBL.updateUser(ModifiedBy, resultDataSet, EmployeeId, signatureFileName, userViewStatSchemeList, userViewStatPatchList, seletedUserTypeValue, chkBoxLst)
                loadUsers()
                lblMessage.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserEditedSuccessful, False)
            Else
                Throw New Exception(UserMessageConstants.chkBoxListProperty)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                isEdited = False
            Else
                'on success set the user type in session
                If EmployeeId = ModifiedBy Then
                    SessionManager.setUserType(seletedUserTypeText)
                End If
                isEdited = True
            End If
        End Try

        Return isEdited

    End Function

#End Region

#Region "Save Basic User Info"
    ''' <summary>
    ''' Save Basic User Info
    ''' </summary>
    ''' <remarks></remarks>
    Function saveBasicUserInfo()










        Dim resultDataSet As DataSet = New DataSet()
        'this variable will be used to populate the appliance user type in session
        Dim seletedUserTypeText As String = String.Empty
        Dim CreatedBy As Integer = SessionManager.getAppServicingUserId()
        Dim EmployeeId As Int32 = CType(ViewState(ViewStateConstants.SaveEmployeeId), Int32)
        Dim isSaved As Boolean

        Try

            Dim checkUserExists As Integer = 0
            Dim firstName As String = String.Empty
            Dim lastName As String = String.Empty
            Dim chkGasProperty As Boolean = False
            Dim chkBoxlstChecked As Boolean = False
            Dim GSREno As String = CType(ViewState(ViewStateConstants.SaveGSRENo), String)

            Dim name As String = txtFind.Text
            Dim signatureFileName As String = txtSignature.Text

            validateUserName()

            If EmployeeId = 0 Then
                Dim userInfoDataSet As DataSet = New DataSet()
                Try
                    firstName = name.Substring(0, name.IndexOf(" "))
                    lastName = name.Substring(name.IndexOf(" ") + 1)
                Catch ex As Exception
                    Throw New Exception(UserMessageConstants.NoEmployeFound)
                End Try
                objUsersBL.getEmployeeIdByName(firstName, lastName, userInfoDataSet)
                If userInfoDataSet.Tables(0).Rows.Count > 0 Then
                    GSREno = userInfoDataSet.Tables(0).Rows(0).Item("GSREno")
                    EmployeeId = userInfoDataSet.Tables(0).Rows(0).Item("EMPLOYEEID")
                Else
                    Throw New Exception(UserMessageConstants.NoEmployeFound)
                End If

            End If

            Dim seletedUserTypeValue As Int32 = CType(ddlUserTypeOptions.SelectedValue, Int32)
            seletedUserTypeText = ddlUserTypeOptions.SelectedItem.Text
            For Each chkBox As ListItem In chkBoxLst.Items
                If chkBox.Text.Equals("Gas") And chkBox.Selected = True Then
                    chkGasProperty = True
                End If
                If chkBox.Selected = True Then
                    chkBoxlstChecked = True
                End If
            Next
            If chkBoxlstChecked = True Then
                If chkGasProperty = True Then

                    If Not String.IsNullOrEmpty(GSREno) Then
                        checkUserExists = objUsersBL.saveUser(CreatedBy, resultDataSet, EmployeeId, seletedUserTypeValue, chkBoxLst, signatureFileName)
                        If (checkUserExists > 0) Then
                            Throw New Exception(UserMessageConstants.userAlreadyExists)
                        Else
                            lblMessage.Visible = True
                            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)
                        End If

                    Else
                        Throw New Exception(UserMessageConstants.UserGSREnotExists)
                    End If
                Else
                    checkUserExists = objUsersBL.saveUser(CreatedBy, resultDataSet, EmployeeId, seletedUserTypeValue, chkBoxLst, signatureFileName)
                    If (checkUserExists > 0) Then
                        Throw New Exception(UserMessageConstants.userAlreadyExists)
                    Else
                        lblMessage.Visible = True
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)
                    End If

                End If
            Else
                Throw New Exception(UserMessageConstants.chkBoxListProperty)
            End If

            loadUsers()


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
                isSaved = False
            Else
                'on success set the user type in session
                If EmployeeId = CreatedBy Then
                    SessionManager.setUserType(seletedUserTypeText)
                End If
                isSaved = True
            End If
        End Try

        Return isSaved

    End Function

#End Region

#Region "Get selected employeeid"
    ''' <summary>
    ''' Get selected employeeid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getSelectedEmployeeId()

        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim name As String = txtFind.Text
        Dim employeeId As Int32 = CType(ViewState(ViewStateConstants.SaveEmployeeId), Int32)

        If employeeId = 0 Then
            validateUserName()
            Dim userInfoDataSet As DataSet = New DataSet()
            Try
                firstName = name.Substring(0, name.IndexOf(" "))
                lastName = name.Substring(name.IndexOf(" ") + 1)
            Catch ex As Exception
                Throw New Exception(UserMessageConstants.NoEmployeFound)
            End Try
            objUsersBL.getEmployeeIdByName(firstName, lastName, userInfoDataSet)
            If userInfoDataSet.Tables(0).Rows.Count > 0 Then
                employeeId = userInfoDataSet.Tables(0).Rows(0).Item("EMPLOYEEID")
            Else
                Throw New Exception(UserMessageConstants.NoEmployeFound)
            End If

        End If

        Return employeeId

    End Function

#End Region

#Region "Validate User Name"
    ''' <summary>
    ''' Validate User Name
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateUserName()

        If txtFind.Text.Length <= 2 Or String.IsNullOrEmpty(txtFind.Text) Or String.IsNullOrWhiteSpace(txtFind.Text) Then
            Throw New Exception(UserMessageConstants.UserNameNotValid)
        End If

    End Sub

#End Region

#Region "Load All DDLs"
    ''' <summary>
    ''' Load All DDLs
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Protected Sub LoadTypeDll(ByVal ddl As DropDownList)

        Dim codeID As String = "UserTypeID"
        Dim codeName As String = "Description"
        Dim userTypeList As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        ddl.Items.Clear()
        Try
            objUsersBL.getUserTypes(0, userTypeList)
            ddl.DataSource = userTypeList
            ddl.DataValueField = codeID
            ddl.DataTextField = codeName
            ddl.DataBind()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
        'ddl.Items.Add(New ListItem("Please Select...", "-1"))
    End Sub

#End Region

#Region "Load Inspection Types in CheckBoxes"
    ''' <summary>
    ''' Load Inspection Types in CheckBoxes
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub loadInspectionTypes()
        Dim resultDataSet As DataSet = New DataSet()
        Try
            chkBoxLst.Items.Clear()

            objUsersBL.getInspectionTypes(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                chkBoxLst.DataSource = resultDataSet
                chkBoxLst.DataTextField = "Description"
                chkBoxLst.DataValueField = "InspectionTypeID"
                chkBoxLst.DataBind()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region


#Region "Load Trades"
    ''' <summary>
    ''' Load Inspection Types in CheckBoxes
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub loadTrades()
        Dim resultDataSet As DataSet = New DataSet()
        Try

            objUsersBL.getAllTrades(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                tradeDropdownlist.DataSource = resultDataSet
                tradeDropdownlist.DataTextField = "val"
                tradeDropdownlist.DataValueField = "id"
                tradeDropdownlist.DataBind()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region



#Region "Find User"
    ''' <summary>
    ''' Find User
    ''' </summary>
    ''' <param name="strtoFind"></param>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Public Sub getQuickFindUsers(ByVal strtoFind As String, ByRef resultDataSet As DataSet)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        If strtoFind.Contains(" ") Then
            firstName = strtoFind.Substring(0, strtoFind.IndexOf(" "))
            lastName = strtoFind.Substring(strtoFind.IndexOf(" ") + 1)
        Else
            firstName = strtoFind
            lastName = strtoFind
        End If
        objUsersBL.getQuickFindUsers(firstName, lastName, resultDataSet)
    End Sub

#End Region

#Region "Populate basic Info Panel"
    ''' <summary>
    ''' Populate basic Info Panel
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateBasicInfoPanel()
        txtFind.Enabled = True
        txtFind.Text = String.Empty
        ddlUserTypeOptions.Enabled = True
        chkBoxLst.Enabled = True
        txtSignature.Text = String.Empty
        btnFind.Enabled = True
        txtFind.Enabled = True
        ViewState(ViewStateConstants.SaveEmployeeId) = 0
        Me.LoadTypeDll(ddlUserTypeOptions)
        Me.loadInspectionTypes()
        Me.loadTrades()
    End Sub

#End Region

#Region "Edit User Click"
    ''' <summary>
    ''' Edit User Click
    ''' </summary>
    ''' <param name="employeeId"></param>
    ''' <remarks></remarks>
    Public Sub editUser(ByVal employeeId As Integer)

        Dim edittUserDataSet As DataSet = New DataSet()
        Dim allInspectionDataSet As DataSet = New DataSet()
        Dim edittUserPatchDataSet As DataSet = New DataSet()
        Dim edittUserInspectionDataSet As DataSet = New DataSet()
        Dim empTradesDataSet As DataSet = New DataSet()
        Dim chkItem As ListItem = New ListItem()
        Dim objAddUserPnl As Panel = CType(Me.FindControl("pnlAddUser"), Panel)
        Dim signatureFileName As String = txtSignature.Text
        Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)

        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Try

            SessionManager.removeSelectedResourceEmployeeId()
            SessionManager.setSelectedResourceEmployeeId(employeeId)
            saveCoreAndOutOfHoursInSession()

            objUsersBL.EditUser(employeeId, edittUserDataSet, edittUserPatchDataSet, edittUserInspectionDataSet)
            objUsersBL.GetEmpTrades(employeeId, empTradesDataSet)
            objUsersBL.getInspectionTypes(allInspectionDataSet)
            txtFind.Text = edittUserDataSet.Tables(0).Rows(0)("Name")
            ViewState(ViewStateConstants.SaveEmployeeId) = employeeId
            ViewState(ViewStateConstants.SaveGSRENo) = edittUserDataSet.Tables(0).Rows(0)("GSRENo")
            ddlUserTypeOptions.SelectedValue = edittUserDataSet.Tables(0).Rows(0)("UserTypeID")
            txtSignature.Text = edittUserDataSet.Tables(0).Rows(0)("SignaturePath")
            chkBoxLst.DataSource = allInspectionDataSet
            chkBoxLst.DataTextField = "Description"
            chkBoxLst.DataValueField = "InspectionTypeId"
            chkBoxLst.DataBind()

            Dim CheckInspectionTypes As CheckBoxList = New CheckBoxList()
            If edittUserInspectionDataSet.Tables(0).Rows.Count > 0 Then
                CheckInspectionTypes.DataSource = edittUserInspectionDataSet
                CheckInspectionTypes.DataTextField = "Description"
                CheckInspectionTypes.DataValueField = "InspectionTypeId"
                CheckInspectionTypes.DataBind()
                Dim counter As Integer = 0
                For Each chkItem In chkBoxLst.Items
                    Dim checkedItem As Integer = edittUserInspectionDataSet.Tables(0).Rows(counter).Item(0)
                    If chkItem.Value = checkedItem Then
                        chkItem.Selected = True
                        If counter < (edittUserInspectionDataSet.Tables(0).Rows.Count - 1) Then
                            counter = counter + 1
                        End If
                    End If

                Next
            End If

            If (empTradesDataSet.Tables(0).Rows.Count > 0) Then
                grdTrade.DataSource = empTradesDataSet.Tables(0)
                grdTrade.DataBind()
            End If

            btnEdit.Visible = True
            btnSave.Visible = False
            lblHeader.Text = ApplicationConstants.EditUserHeader
            btnFind.Enabled = False
            txtFind.Enabled = False

            populateCoreWorkingHours()
            populateOutOfHoursManagement()

            updatePnlResources.Update()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Load Users"
    ''' <summary>
    ''' Load Users
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub loadUsers()

        Dim resultDataset As DataSet = New DataSet()
        Try
            Dim updatePnlResources As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
            Dim grdUser As GridView = CType(updatePnlResources.FindControl("grdUsers"), GridView)
            objUsersBL.getUsers(resultDataset)
            If resultDataset.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Else
                grdUser.DataSource = resultDataset
                grdUser.DataBind()

            End If
            updatePnlResources.Update()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Set Signature File"
    ''' <summary>
    ''' Set Signature File
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub setSignatureFileName()
        txtSignature.Text = Session(SessionConstants.SignatureUploadName)
        If (Not String.IsNullOrEmpty(txtSignature.Text)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FileUploadedSuccessfully, False)
        End If
        Session(SessionConstants.SignatureUploadName) = String.Empty
        Dim pnlAddUserParent = DirectCast(Parent.FindControl("pnlAddUserControl"), Panel)
        pnlAddUserParent.Visible = True
    End Sub

#End Region

#End Region

#Region "Core Working Hours Functions"

#Region "Populate core working hours"
    ''' <summary>
    ''' Populate core working hours
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateCoreWorkingHours()

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()

        If (IsNothing(resultDataset)) Then
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If

        Dim coreDt As DataTable = resultDataset.Tables(ApplicationConstants.CoreHoursDataTable)
        grdCoreWorkingHours.DataSource = coreDt
        grdCoreWorkingHours.DataBind()

    End Sub

#End Region

#Region "Save Core Working Hours"
    ''' <summary>
    ''' Save Core Working Hours
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveCoreWorkingHours()

        validateCoreWorkingHours()

        '''''''''''''''''''''''''''''''''''
        ''''' Saving trades ''''''''''''''
        ''''''''''''''''''''''''''''''''''
        Dim count As Integer
        Dim x As String
        If (grdTrade.Rows.Count > 0) Then
            For Each arr In grdTrade.Rows
                x = grdTrade.Rows(count).Cells(0).Text
                count = count + 1
                Me.savingTradesList.Add(x)

            Next
        End If

        Dim resultDataTradeSet As DataSet = New DataSet()
        objUsersBL.getAllTrades(resultDataTradeSet)
        If resultDataTradeSet.Tables(0).Rows.Count > 0 Then
            For Each Row As DataRow In resultDataTradeSet.Tables(0).Rows
                If (Me.savingTradesList.Contains((Row("val")).ToString())) Then
                    ' Adding selected ids
                    Me.selectedTradeIds.Add(Row("id"))
                End If
            Next
        End If

        Dim tradeIdsString As String = ""
        For Each tId As Integer In Me.selectedTradeIds
            If tradeIdsString.Length <= 0 Then
                tradeIdsString = tradeIdsString + tId.ToString()
            Else
                tradeIdsString = tradeIdsString + "," + tId.ToString()
            End If

        Next

        If Me.selectedTradeIds.Count > 0 Then
            Dim saveTradesResutlSet As DataSet = New DataSet()
            Me.objUsersBL.saveSelectedTrades(Me.getSelectedEmployeeId(), tradeIdsString, SessionManager.getAppServicingUserId(), saveTradesResutlSet)
        End If

        '''''''''''''''''' Saving trades end '''''''''''''


        ''''''''''''''' Updating isoftec , gas engineer and is gas safe''''''''''''''''''''''

        Dim _isOftec As Integer
        Dim _isGassafe As Integer
        Dim _gsReNo As String

        If rdBtnGasSafe.SelectedValue = 0 Then
            _isGassafe = 0
        Else
            _isGassafe = 1
        End If

        If rdBtnOfTec.SelectedValue = 0 Then
            _isOftec = 0
        Else
            _isOftec = 1
        End If

        _gsReNo = txtRegEngineer.Text

        Dim saveJobDetailResutlSet As DataSet = New DataSet()
        Me.objUsersBL.saveJobDetails(_isGassafe, _isOftec, SessionManager.getAppServicingUserId(), Me.getSelectedEmployeeId, _gsReNo, saveJobDetailResutlSet)

        '''''''''''''' End updating isoftec , gas engineer and is gas safe ''''''''''''''''''''''''

        Dim timeFrom As String
        Dim timeTo As String
        Dim dayId As Integer
        Dim dt As DataTable = createCoreWorkingHoursDt()
        Dim objResourcesBL As ResourcesBL = New ResourcesBL()
        Dim editedBy As Integer = SessionManager.getAppServicingUserId()
        Dim employeeId As Integer = getSelectedEmployeeId()
        Dim isSaved As Boolean = False

        For Each row As GridViewRow In grdCoreWorkingHours.Rows

            timeFrom = DirectCast(row.FindControl("ddlFrom"), DropDownList).SelectedValue
            timeTo = DirectCast(row.FindControl("ddlTo"), DropDownList).SelectedValue
            dayId = Convert.ToInt32(DirectCast(row.FindControl("hdnWeekdayId"), HiddenField).Value)

            If (timeFrom.Equals("-1") Or timeTo.Equals("-1")) Then
                timeFrom = String.Empty
                timeTo = String.Empty
            End If

            Dim dr As DataRow
            dr = dt.NewRow()
            dr(ApplicationConstants.DayIdCol) = dayId
            dr(ApplicationConstants.StartTimeCol) = timeFrom
            dr(ApplicationConstants.EndTimeCol) = timeTo
            dt.Rows.Add(dr)

        Next

        isSaved = objResourcesBL.saveCoreWorkingHoursInfo(employeeId, editedBy, dt)

        If (isSaved = True) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedSuccessfully, False)
            SessionManager.setSelectedResourceEmployeeId(employeeId)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FailedToSave, True)
        End If

    End Sub

#End Region

#Region "Validate Core Working Hours"
    ''' <summary>
    ''' Validate Core Working Hours
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateCoreWorkingHours()

        Dim timeFrom As String
        Dim timeTo As String
        Dim weekDay As String

        For Each row As GridViewRow In grdCoreWorkingHours.Rows

            timeFrom = DirectCast(row.FindControl("ddlFrom"), DropDownList).SelectedItem.Text
            timeTo = DirectCast(row.FindControl("ddlTo"), DropDownList).SelectedItem.Text
            weekDay = DirectCast(row.FindControl("lblWeekDay"), Label).Text

            If (timeFrom.Equals("-") And Not timeTo.Equals("-")) Then
                Throw New Exception(UserMessageConstants.InvalidTimeFor + weekDay + " in Core Working Hours.")
            End If


            If (Not timeFrom.Equals("-") And timeTo.Equals("-")) Then
                Throw New Exception(UserMessageConstants.InvalidTimeFor + weekDay + " in Core Working Hours.")
            End If

            If (Not timeFrom.Equals("-") And Not timeTo.Equals("-")) Then

                Dim fromSelected As DateTime = DateTime.Parse(timeFrom)
                Dim toSelected As DateTime = DateTime.Parse(timeTo)

                If fromSelected.TimeOfDay > toSelected.TimeOfDay Then
                    Throw New Exception(UserMessageConstants.InvalidFromTime + weekDay + " in Core Working Hours.")
                End If

                If fromSelected.TimeOfDay.Equals(toSelected.TimeOfDay) Then
                    Throw New Exception(UserMessageConstants.InvalidTimeFor + weekDay + " in Core Working Hours.")
                End If

                If (isTimeReserveInOutOfHours(weekDay, fromSelected, toSelected)) Then
                    Throw New Exception(UserMessageConstants.InvalidSelectedTimeFor + weekDay + UserMessageConstants.InvalidSelectedTimeForReason + " in Core Working Hours.")
                End If

            End If


        Next


    End Sub

#End Region

#Region "Check time reserved in Out Of Hours Management"
    ''' <summary>
    ''' Check time reserved in Out Of Hours Management
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function isTimeReserveInOutOfHours(ByVal weekDay As String, ByVal fromTime As DateTime, ByVal toTime As DateTime)

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
        Dim outOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable)
        Dim isReserved As Boolean = False

        If (outOfHoursDt.Rows.Count > 0) Then
            Dim resultQuery = (From res In outOfHoursDt.AsEnumerable Where res.Item(ApplicationConstants.GeneralStartDateCol) > DateTime.Now() Select res)
            Dim futureOutOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Clone()
            If (resultQuery.Count > 0) Then
                futureOutOfHoursDt = resultQuery.CopyToDataTable()

                For Each row As DataRow In futureOutOfHoursDt.Rows

                    Dim reservedStartTime As DateTime
                    Dim reservedEndTime As DateTime

                    If (getReservedTimeForDay(row, weekDay, reservedStartTime, reservedEndTime)) Then

                        If ((fromTime.TimeOfDay > reservedStartTime.TimeOfDay And fromTime.TimeOfDay >= reservedEndTime.TimeOfDay) Or _
                            (toTime.TimeOfDay <= reservedStartTime.TimeOfDay And toTime.TimeOfDay < reservedEndTime.TimeOfDay)) Then
                        Else
                            isReserved = True
                            Exit For
                        End If

                    End If
                Next
            End If
        End If

        Return isReserved

    End Function

#End Region

#Region "Get reserved time for day"
    ''' <summary>
    ''' Get reserved time for day
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getReservedTimeForDay(ByVal row As DataRow, ByVal weekDay As String, ByRef reservedStartTime As DateTime, ByRef reservedEndTime As DateTime)

        Dim startDate As DateTime = row(ApplicationConstants.StartDateCol)
        Dim endDate As DateTime = row(ApplicationConstants.EndDateCol)
        Dim startTime As DateTime = DateTime.Parse(row(ApplicationConstants.StartTimeCol))
        Dim endTime As DateTime = DateTime.Parse(row(ApplicationConstants.EndTimeCol))
        Dim dayExisted As Boolean = False

        While startDate.Date <= endDate.Date
            If (startDate.Date.DayOfWeek.ToString().Equals(weekDay)) Then

                If (startDate.Date.Equals(endDate.Date)) Then
                    reservedStartTime = startTime
                    reservedEndTime = endTime
                Else
                    reservedStartTime = DateTime.Parse("00:00")
                    reservedEndTime = endTime
                End If
                dayExisted = True
                Exit While
            End If

            startDate = startDate.AddDays(1)
        End While

        Return dayExisted

    End Function

#End Region

#Region "Create Core Working Hours Datatable"
    ''' <summary>
    ''' Create Core Working Hours Datatable
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function createCoreWorkingHoursDt()

        Dim dt As DataTable = New DataTable()
        dt.Columns.Add(ApplicationConstants.DayIdCol, GetType(Int32))
        dt.Columns.Add(ApplicationConstants.StartTimeCol, GetType(String))
        dt.Columns.Add(ApplicationConstants.EndTimeCol, GetType(String))
        Return dt

    End Function

#End Region

#End Region

#Region "Out Of Hours Functions"

#Region "Save out of hours"
    ''' <summary>
    ''' Save out of hours
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveOutOfHours()

        Dim outOfHoursDeletedDt As DataTable = SessionManager.getOutOfHoursDeletedList()
        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
        Dim employeeId As Integer = SessionManager.getSelectedResourceEmployeeId()
        Dim editedBy As Integer = SessionManager.getAppServicingUserId()
        Dim isSaved As Boolean
        Dim query = (From res In resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).AsEnumerable Where res.Item(ApplicationConstants.OutOfHoursIdCol) = Convert.ToString(-1) Select res)
        Dim newItemsDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Clone()

        If (query.Count > 0) Then
            newItemsDt = query.CopyToDataTable()
        End If
        Dim deletedItemsDt As DataTable = outOfHoursDeletedDt

        Dim objResourcesBL As ResourcesBL = New ResourcesBL()
        isSaved = objResourcesBL.saveOutOfHoursInfo(employeeId, editedBy, newItemsDt, deletedItemsDt)

        If (isSaved = True) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SavedSuccessfully, False)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FailedToSave, True)
        End If

    End Sub

#End Region

#Region "Validate selected date time"
    ''' <summary>
    ''' Validate selected date time
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateSelectedTime(ByVal objOutOfHoursSearchBO As OutOfHoursSearchBO)
        checkDateFields(objOutOfHoursSearchBO)
        checkTimeInExistingOutOfHours(objOutOfHoursSearchBO)
        checkTimeInCoreWorkingHours(objOutOfHoursSearchBO)
    End Sub

#End Region

#Region "Add Out Of Hours"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub addOutOfHours(ByVal objOutOfHoursSearchBO As OutOfHoursSearchBO)

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()

        Dim dr = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).NewRow
        dr(ApplicationConstants.OutOfHoursIdCol) = -1
        dr(ApplicationConstants.TypeIdCol) = objOutOfHoursSearchBO.TypeId
        dr(ApplicationConstants.TypeCol) = objOutOfHoursSearchBO.Type
        dr(ApplicationConstants.StartDateCol) = Convert.ToDateTime(objOutOfHoursSearchBO.StartDate)
        dr(ApplicationConstants.EndDateCol) = Convert.ToDateTime(objOutOfHoursSearchBO.EndDate)
        dr(ApplicationConstants.StartTimeCol) = objOutOfHoursSearchBO.FromTime
        dr(ApplicationConstants.EndTimeCol) = objOutOfHoursSearchBO.ToTime
        dr(ApplicationConstants.GeneralStartDateCol) = DateTime.Parse(objOutOfHoursSearchBO.StartDate + " " + objOutOfHoursSearchBO.FromTime).ToString("yyyy-MM-dd HH:mm")
        dr(ApplicationConstants.GeneralEndDateCol) = DateTime.Parse(objOutOfHoursSearchBO.EndDate + " " + objOutOfHoursSearchBO.ToTime).ToString("yyyy-MM-dd HH:mm")
        resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Rows.InsertAt(dr, 0)

        resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).AcceptChanges()

        SessionManager.setCoreAndOutOfHoursDataSet(resultDataset)
        populateOutOfHoursManagement()


    End Sub

#End Region

#Region "Check time slot in core working hours"
    ''' <summary>
    ''' Check time slot in core working hours
    ''' </summary>
    ''' <param name="objOutOfHoursSearchBO"></param>
    ''' <remarks></remarks>
    Sub checkTimeInCoreWorkingHours(ByVal objOutOfHoursSearchBO As OutOfHoursSearchBO)

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
        Dim coreWorkingHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.EmpCoreHoursDataTable)

        Dim startDate As DateTime = Convert.ToDateTime(objOutOfHoursSearchBO.StartDate)
        Dim endDate As DateTime = Convert.ToDateTime(objOutOfHoursSearchBO.EndDate)
        Dim startTime As DateTime
        Dim endTime As DateTime
        Dim coreStartTime As DateTime
        Dim coreEndTime As DateTime
        Dim coreFullDayFree As Boolean = False
        Dim dayName As String = String.Empty

        Dim dayExisted As Boolean = False
        Dim traverseStartDate As DateTime = startDate

        While traverseStartDate.Date <= endDate.Date

            dayName = traverseStartDate.DayOfWeek.ToString()

            If (startDate.Date.Equals(endDate.Date)) Then
                startTime = Convert.ToDateTime(objOutOfHoursSearchBO.FromTime)
                endTime = Convert.ToDateTime(objOutOfHoursSearchBO.ToTime)
            Else

                If (traverseStartDate.Date.Equals(startDate.Date.Date)) Then
                    startTime = Convert.ToDateTime(objOutOfHoursSearchBO.FromTime)
                    endTime = DateTime.Parse("23:59")
                ElseIf (traverseStartDate.Date.Equals(endDate.Date)) Then
                    startTime = DateTime.Parse("00:00")
                    endTime = Convert.ToDateTime(objOutOfHoursSearchBO.ToTime)
                Else
                    startTime = DateTime.Parse("00:00")
                    endTime = DateTime.Parse("23:59")
                End If
            End If



            Dim resultQuery = (From res In coreWorkingHoursDt.AsEnumerable Where res.Item(ApplicationConstants.DayNameCol) = dayName Select res)

            If (resultQuery.Count > 0) Then
                Dim dt As DataTable = resultDataset.Tables(ApplicationConstants.CoreHoursDataTable).Clone()
                dt = resultQuery.CopyToDataTable()
                Dim row As DataRow = dt.Rows.Item(0)
                If (Not IsDBNull(row(ApplicationConstants.StartTimeCol)) _
                    And Not IsDBNull(row(ApplicationConstants.EndTimeCol)) _
                    And Not String.IsNullOrEmpty(row(ApplicationConstants.StartTimeCol)) _
                    And Not String.IsNullOrEmpty(row(ApplicationConstants.EndTimeCol))) Then
                    coreStartTime = Convert.ToDateTime(row(ApplicationConstants.StartTimeCol))
                    coreEndTime = Convert.ToDateTime(row(ApplicationConstants.EndTimeCol))
                Else
                    coreFullDayFree = True
                End If
            Else
                If (isWeekend(dayName)) Then
                    coreFullDayFree = True
                Else
                    coreStartTime = Convert.ToDateTime(GeneralHelper.getCoreDayStartHour())
                    coreEndTime = Convert.ToDateTime(GeneralHelper.getCoreDayEndHour())
                End If
            End If

            If (Not coreFullDayFree) Then

                If ((startTime.TimeOfDay > coreStartTime.TimeOfDay And startTime.TimeOfDay >= coreEndTime.TimeOfDay) Or _
                    (endTime.TimeOfDay <= coreStartTime.TimeOfDay And endTime.TimeOfDay < coreEndTime.TimeOfDay)) Then
                Else
                    Throw New Exception(UserMessageConstants.TimeReservedInCoreWorkingHours)
                End If

            End If

            traverseStartDate = traverseStartDate.AddDays(1)
        End While

    End Sub

#End Region

#Region "Check day is Weekend"
    ''' <summary>
    ''' Check day is Weekend
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function isWeekend(ByVal dayName As String) As Boolean

        If (dayName.Equals("Sunday") Or dayName.Equals("Saturday")) Then
            Return True
        End If
        Return False
    End Function


#End Region

#Region "Check time slot in existing out of hours"
    ''' <summary>
    ''' Check time slot in existing out of hours
    ''' </summary>
    ''' <param name="objOutOfHoursSearchBO"></param>
    ''' <remarks></remarks>
    Sub checkTimeInExistingOutOfHours(ByVal objOutOfHoursSearchBO As OutOfHoursSearchBO)

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
        Dim outOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable)

        If (outOfHoursDt.Rows.Count > 0) Then
            Dim resultQuery = (From res In outOfHoursDt.AsEnumerable Where res.Item(ApplicationConstants.GeneralStartDateCol) > DateTime.Now() Select res)
            Dim futureOutOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Clone()

            If (resultQuery.Count > 0) Then
                futureOutOfHoursDt = resultQuery.CopyToDataTable()
                'Dim checkSameDayResultQuery = (From result In futureOutOfHoursDt.AsEnumerable Where result.Item(ApplicationConstants.StartDateCol) = objOutOfHoursSearchBO.StartDate Select result)
                'Dim filteredOutOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable).Clone()

                'If (checkSameDayResultQuery.Count > 0) Then
                '    Throw New Exception(UserMessageConstants.ExtendedHourAlreadyExist)
                'Else
                For Each row As DataRow In futureOutOfHoursDt.Rows
                    Dim reservedStartTime As DateTime = DateTime.Parse(row(ApplicationConstants.StartDateCol) + " " + row(ApplicationConstants.StartTimeCol))
                    Dim reservedEndTime As DateTime = DateTime.Parse(row(ApplicationConstants.EndDateCol) + " " + row(ApplicationConstants.EndTimeCol))
                    Dim selectedStartTime As DateTime = DateTime.Parse(objOutOfHoursSearchBO.StartDate + " " + objOutOfHoursSearchBO.FromTime)
                    Dim selectedEndTime As DateTime = DateTime.Parse(objOutOfHoursSearchBO.EndDate + " " + objOutOfHoursSearchBO.ToTime)

                    If ((selectedStartTime > reservedStartTime And selectedStartTime >= reservedEndTime) Or _
                        (selectedEndTime <= reservedStartTime And selectedEndTime < reservedEndTime)) Then
                    Else
                        Throw New Exception(UserMessageConstants.InvalidSelectedOutOfHours)
                        Exit For
                    End If
                Next
                'End If
            End If

        End If

    End Sub

#End Region

#Region "Check date fields"
    ''' <summary>
    ''' Check date fields
    ''' </summary>
    ''' <remarks></remarks>
    Sub checkDateFields(ByVal objOutOfHoursSearchBO As OutOfHoursSearchBO)

        Dim dateTimeText As Date
        Dim startTime As DateTime
        Dim endTime As DateTime

        Dim isValidDate As Boolean = Date.TryParseExact(objOutOfHoursSearchBO.StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)
        If (Not isValidDate) Then
            Throw New Exception(UserMessageConstants.InvalidStartDate)
        End If

        isValidDate = Date.TryParseExact(objOutOfHoursSearchBO.EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)
        If (Not isValidDate) Then
            Throw New Exception(UserMessageConstants.InvalidEndDate)
        End If

        If (Not objOutOfHoursSearchBO.StartDate.Equals(objOutOfHoursSearchBO.EndDate)) Then
            Throw New Exception(UserMessageConstants.InvalidStartEndDate)
        End If

        If (ddlFromTime.SelectedIndex = 0) Then
            Throw New Exception(UserMessageConstants.InvalidStartTime)
        End If

        If (ddlToTime.SelectedIndex = 0) Then
            Throw New Exception(UserMessageConstants.InvalidEndTime)
        End If

        startTime = DateTime.Parse(txtFromDate.Text + " " + ddlFromTime.SelectedValue)
        endTime = DateTime.Parse(txtToDate.Text + " " + ddlToTime.SelectedValue)

        Dim result As Integer = DateTime.Compare(endTime, startTime)
        If (Not result > 0) Then
            Throw New Exception(UserMessageConstants.InvalidEndStartTime)
        End If

        result = DateTime.Compare(startTime, DateTime.Now)
        If (Not result > 0) Then
            Throw New Exception(UserMessageConstants.SelectFutureDate)
        End If

    End Sub

#End Region

#Region "Get Core Grid Data"
    ''' <summary>
    ''' Get Core Grid Data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getCoreGridData() As DataTable
        Dim timeFrom As String
        Dim timeTo As String
        Dim dayId As Integer
        Dim dayName As String

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()
        Dim coreWorkingHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.EmpCoreHoursDataTable).Clone()

        For Each row As GridViewRow In grdCoreWorkingHours.Rows

            timeFrom = DirectCast(row.FindControl("ddlFrom"), DropDownList).SelectedValue
            timeTo = DirectCast(row.FindControl("ddlTo"), DropDownList).SelectedValue
            dayId = Convert.ToInt32(DirectCast(row.FindControl("hdnWeekdayId"), HiddenField).Value)
            dayName = DirectCast(row.FindControl("ddlTo"), Label).Text

            If (timeFrom.Equals("-1") Or timeTo.Equals("-1")) Then
                timeFrom = String.Empty
                timeTo = String.Empty
            End If

            Dim dr As DataRow
            dr = coreWorkingHoursDt.NewRow()
            dr(ApplicationConstants.DayIdCol) = dayId
            dr(ApplicationConstants.StartTimeCol) = timeFrom
            dr(ApplicationConstants.EndTimeCol) = timeTo
            coreWorkingHoursDt.Rows.Add(dr)
            coreWorkingHoursDt.AcceptChanges()
        Next
        Return coreWorkingHoursDt

    End Function

#End Region

#Region "Reset out of hours controls"
    ''' <summary>
    ''' Reset out of hours controls
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetOutOfHoursControls()

        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()

        'Type
        ddlType.Items.Clear()
        ddlType.DataSource = resultDataset.Tables(ApplicationConstants.OutOfHoursTypeDataTable)
        ddlType.DataValueField = "TypeId"
        ddlType.DataTextField = "Type"
        ddlType.DataBind()

        'Date
        txtFromDate.Text = String.Empty
        txtToDate.Text = String.Empty

        'From/To dropdowns
        ddlFromTime.Items.Clear()
        ddlToTime.Items.Clear()
        GeneralHelper.populateCoreHoursWithThirtyMinDuration(ddlFromTime)
        GeneralHelper.populateCoreHoursWithThirtyMinDuration(ddlToTime)

    End Sub

#End Region

#Region "Reset grid"
    ''' <summary>
    ''' Reset grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetGrid()

        SessionManager.removeOutOfHoursDeletedList()
        Dim resultDs As DataSet = New DataSet()
        Dim employeeId As Integer = SessionManager.getSelectedResourceEmployeeId()
        Dim objResourcesBL As ResourcesBL = New ResourcesBL()
        objResourcesBL.getCoreAndOutOfHoursInfo(resultDs, employeeId)
        SessionManager.setCoreAndOutOfHoursDataSet(resultDs)

        'Grid
        Dim outOfHoursDt As DataTable = resultDs.Tables(ApplicationConstants.OutOfHoursDataTable)
        grdOutOfHoursManagement.DataSource = outOfHoursDt
        grdOutOfHoursManagement.DataBind()


    End Sub

#End Region

#Region "Populate Out of Hours Management"
    ''' <summary>
    ''' Populate Out of Hours Management
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateOutOfHoursManagement()

        resetOutOfHoursControls()
        Dim resultDataset As DataSet = SessionManager.getCoreAndOutOfHoursDataSet()

        If (IsNothing(resultDataset)) Then
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If

        Dim outOfHoursDt As DataTable = resultDataset.Tables(ApplicationConstants.OutOfHoursDataTable)
        grdOutOfHoursManagement.DataSource = outOfHoursDt
        grdOutOfHoursManagement.DataBind()

    End Sub

#End Region

#End Region

#End Region

End Class