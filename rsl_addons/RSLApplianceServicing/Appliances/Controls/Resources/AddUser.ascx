﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AddUser.ascx.vb" Inherits="Appliances.AddUser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script language="javascript" type="text/javascript">
// <![CDATA[


    function btnFind_onclick() {

    }

    function txtFind_onclick() {

    }
    function setEmployeeId() {
        alert('1');
        __doPostBack();
    }



// ]]>
</script>
<style type="text/css">
    .wideControl
    {
        padding: 5px;
    }
    
    #Resources td {
        padding: 0px !important;
        padding-left: 0px !important;      
    }
    
    .select_div select
    {
        width: 197px !important;
    }
    .dashboard th{
        background: #000;
        color: #fff;
    }
 .autocomplete_CompletionListElement
{
    font-family:Tahoma;
   font-size:12px;
    background-color: White;
    cursor: default;
    overflow-y: auto;
    overflow-x: hidden;    
    text-align: left !important;
    border: 1px solid #777;
    z-index:10000;
    max-height:300px;
    list-style:none;    
    width:150px !important;
}
.autocomplete_CompletionListitem
{
    padding-left:-10px !important;
    
    }
    </style>
<asp:UpdatePanel runat="server" ID="updPnlAddUser" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlAddUser" Style="float: right; margin-left: 1%; width: 100%;" align="left">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                &nbsp
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="portlet">
                <div class="header">
                    <asp:Label ID="lblHeader" CssClass="header-label" runat="server" Text="Add User"></asp:Label>
                </div>
                <div class="portlet-body">
                    <div id="tblAddUser" style="overflow:auto">
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Quick Find:<span class="Required">*</span>
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtFind" runat="server" Style="height: 20px; width: 190px;"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="autoCompleteExtenderFindUser" runat="server" EnableCaching="False"
                                        MinimumPrefixLength="1" ServiceMethod="GetCompletionList" EnableViewState="true" CompletionListCssClass="autocomplete_CompletionListElement" 
                                     CompletionListItemCssClass="autocomplete_CompletionListitem"
                                        ServicePath="~/SearchNames.asmx" UseContextKey="True" CompletionSetCount="12"                           
                                         TargetControlID="txtFind">
                                    </asp:AutoCompleteExtender>&nbsp;&nbsp;
                                    <asp:Button ID="btnFind" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important;" runat="server" Text="Find" />
                                    <asp:Panel ID="pnlErrorMsg" runat="server" Visible="false">
                                        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Type:<span class="Required">*</span>
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlUserTypeOptions" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    User Type:<span class="Required">*</span>
                                </div>
                                <div class="field">
                                    <asp:CheckBoxList ID="chkBoxLst" runat="server" CssClass="Required" TextAlign="Right"
                                        DataTextField="Description" DataValueField="Description" CellPadding="5" CellSpacing="5"
                                        RepeatColumns="2">
                                    </asp:CheckBoxList>
                                    <asp:Panel ID="pnlChkBoxList" runat="server" Visible="false">
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Signature:
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtSignature" runat="server" Enabled="false" Style="height: 20px;
                                        width: 190px;"></asp:TextBox>&nbsp;&nbsp;
                                    <input type="button" id="Button1" value="Upload" class="btn btn-xs btn-blue right" style="padding: 3px 15px !important;"
                                        onclick="openUploadWindow()" />
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Gas Safe Reg Engineer:
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtRegEngineer" runat="server" Enabled="true" Style="height: 20px;
                                        width: 190px;"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Trade : &nbsp
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="tradeDropdownlist" runat="server" CssClass="wideControl" Width="200px">
                                    </asp:DropDownList>&nbsp;&nbsp;
                                    <asp:Button ID="addTradeButton" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important;" OnClick="addTradeBtn_Click" runat="server" Text="Add" />
                                    <%--<input type="button" id="addTradeButton" value="Add" onclick="addTradeBtn_Click" style="height: 20px; width: 80px;"/>--%>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="width:100%">
                            <div class="select_div">
                                <div class="label style2">
                                    
                                </div>
                                <div class="field">
                                    <asp:UpdatePanel ID="updOperativesList" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="grdTrade" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                BorderStyle="Solid" BorderWidth="1px" GridLines="None" BorderColor="#BBBBBB"
                                                Font-Bold="False" CssClass="component-trade-list" Width="393px">
                                                <Columns>
                                                    <asp:BoundField DataField="TRADE" HeaderText="Trade" ShowHeader="False" />
                                                    <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                                        <ItemTemplate>
                                                            <%--<asp:Button ID="deleteTradeButton" onclick="deleteTradeButton_Click" runat="server" CommandArgument = '<%# Container.DisplayIndex %>' Text="X" />--%>
                                                            <asp:ImageButton Style="border: 0; padding-right: 10px;" ID="deleteTradeButton" ImageUrl="~/images/cross.png"
                                                                OnClick="deleteTradeButton_Click" Width="15px" Height="15px" CommandArgument='<%# Container.DisplayIndex %>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                        <ItemStyle Width="10px" BorderStyle="None" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label style2">
                                    Certificates:
                                </div>
                                <div class="field">
                                    <div>
                                        <span style="float: left; padding-top: 5px; padding-right: 10px;">
                                            Gas safe
                                        </span>
                                        <asp:RadioButtonList runat="server" ID="rdBtnGasSafe" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="1" Selected ="True"/>
                                            <asp:ListItem Text="No" Value="0" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div>
                                        <span style="float: left; padding-top: 5px; padding-right: 20px;">
                                            OFTEC
                                        </span>
                                        <asp:RadioButtonList runat="server" ID="rdBtnOfTec" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="1" Selected ="True"/>
                                            <asp:ListItem Text="No" Value="0" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <%-- CORE WORKING HOURS --%>
                    <asp:Panel runat="server" ID="pnlCoreWorkingHours" Style="float: right; margin-left: 2%; width: 96%; padding: 10px" align="left">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </asp:Panel>
                        <b>Core Working Hours </b>
                        
                        <asp:GridView ID="grdCoreWorkingHours" runat="server" AutoGenerateColumns="False"
                            BorderColor="White" BorderStyle="None" ShowHeader="false" BorderWidth="0px" GridLines="None">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeekDay" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                        :
                                        <asp:HiddenField ID="hdnWeekdayId" runat="server" Value='<%# Bind("DayId") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdnStartTime" runat="server" Value='<%# Bind("StartTime") %>'>
                                        </asp:HiddenField>
                                        <asp:HiddenField ID="hdnEndTime" runat="server" Value='<%# Bind("EndTime") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="176px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <span style="float: left; line-height: 25px;">
                                            From: &nbsp
                                        </span>
                                        <div class="list-select">
                                            <div class="field" style="margin:0">
                                                <asp:DropDownList ID="ddlFrom" runat="server" Style="padding: 5px; width:100px !important;" disabled="disabled">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <span style="float: left; line-height: 25px;">
                                            To: &nbsp
                                        </span>
                                        <div class="list-select">
                                            <div class="field" style="margin:0">
                                                <asp:DropDownList ID="ddlTo" runat="server" Style="padding: 5px; width:100px !important;" disabled="disabled">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="150px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div style="padding-bottom:10px;">
                            
                        </div>
                        <div style="text-align: right; padding-right: 28px;">
                            <asp:Button ID="btnSaveCoreWorkingHours" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important; display:none" runat="server" Text="Save Core Working Hours" />
                            <asp:Button ID="btnCoreCancel" CssClass="btn btn-xs btn-gray right" style="padding: 3px 15px !important; margin-right: 5px; display:none" runat="server" Text="Cancel" />
                        </div>
                    </asp:Panel>
                    <hr />
                    <%-- OUT OF HOURS MANAGEMENT --%>
                    <asp:Panel runat="server" ID="Panel2" Style="float: right; margin-left: 1%;
                        width: 96%; padding: 10px" align="left">
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </asp:Panel>
                        <b>Out of Hours Management: </b>
                        <div style="overflow:auto; padding-bottom:10px;">
                            <div class="form-control">
                                <div class="select_div" style="padding: 10px 0px;">
                                    <div class="label style2">
                                        Type : &nbsp
                                    </div>
                                    <div class="field">
                                        <asp:DropDownList ID="ddlType" runat="server" CssClass="wideControl" Width="200px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div" style="padding: 10px 0px;">
                                    <div class="label style2">
                                        Start date : &nbsp
                                    </div>
                                    <div class="field">
                                        <asp:TextBox ID="txtFromDate" name="txtFromDate" CssClass="wideControl" Width="150px"
                                            runat="server"></asp:TextBox>&nbsp
                                        <span>
                                            <asp:Image ID="imgFromDate" style="margin-bottom: -5px;" runat="server" src="../../Images/calendar.png" />
                                            <asp:CalendarExtender
                                                ID="calFromDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy" PopupButtonID="imgFromDate"
                                                PopupPosition="BottomLeft" TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div" style="padding: 10px 0px;">
                                    <div class="label style2">
                                        From : &nbsp
                                    </div>
                                    <div class="field">
                                        <asp:DropDownList ID="ddlFromTime" runat="server" CssClass="wideControl" Width="108px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div" style="padding: 10px 0px;">
                                    <div class="label style2">
                                        End date : &nbsp
                                    </div>
                                    <div class="field">
                                        <asp:TextBox ID="txtToDate" name="txtToDate" CssClass="wideControl" Width="150px"
                                            runat="server"></asp:TextBox>&nbsp
                                        <span>
                                            <asp:Image ID="imgToDate" style="margin-bottom: -5px;" runat="server" src="../../Images/calendar.png" /><asp:CalendarExtender
                                                ID="calToDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy" PopupButtonID="imgToDate"
                                                PopupPosition="BottomLeft" TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="select_div" style="padding: 10px 0px;">
                                    <div class="label style2">
                                        To : &nbsp
                                    </div>
                                    <div class="field">
                                        <asp:DropDownList ID="ddlToTime" runat="server" CssClass="wideControl" Width="108px">
                                        </asp:DropDownList> &nbsp;
                                        <asp:Button ID="btnAdd" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important; margin-right: 5px;" runat="server" Text="Add" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="max-height: 200px; overflow: auto;">
                            <asp:GridView ID="grdOutOfHoursManagement" runat="server" CssClass="dashboard webgrid table table-responsive"
                            HeaderStyle-CssClass="gridfaults-header"
                                ShowHeaderWhenEmpty="true" AutoGenerateColumns="False" BorderColor="#BBBBBB" GridLines="None" 
                                BorderStyle="Solid" BorderWidth="0px" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type:">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeekDay" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                            <asp:HiddenField ID="hdnOutOfHoursId" runat="server" Value='<%# Bind("OutOfHoursId") %>' />
                                            <asp:HiddenField ID="hdnStartDate" runat="server" Value='<%# Bind("GeneralStartDate") %>' />
                                            <asp:HiddenField ID="hdnEndDate" runat="server" Value='<%# Bind("GeneralEndDate") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" Width="25%" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From:">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFrom" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" Width="30%" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To:">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTo" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" Width="30%" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click" CommandArgument='<%# Bind("OutOfHoursId") %>'><img alt="Delete item"  src="../../Images/cross2.png" style="border:none;" />  </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <br />
                    </asp:Panel>
                    <div style="padding:0 10px 0 0">
                        <asp:Button ID="btnSave" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important;" runat="server" Text="Save" />
                        <asp:Button ID="btnEdit" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important;" runat="server" Text="Save" Visible="False" />
                        <asp:Button ID="btnCancel" CssClass="btn btn-xs btn-gray right" style="padding: 3px 15px !important;margin-right: 5px;" runat="server" Text="Cancel" />
                        <asp:CheckBox ID="ckBoxFileName" runat="server" AutoPostBack="True" Visible="true"
                            CssClass="hiddenField" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
