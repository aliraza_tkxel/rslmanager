﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Access.ascx.vb" Inherits="Appliances.Access" %>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
<asp:GridView ID="grdAccess" runat="server" AutoGenerateColumns="False" 
    Width="38px" style="margin-left: 3px;">
    <Columns>
        <asp:ButtonField ButtonType="Button" Text="Edit" HeaderText="Access" ControlStyle-BackColor="White">
        <ControlStyle BackColor="White"></ControlStyle>

            <HeaderStyle Width="5px" />

            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" 
            BackColor="White" />
        </asp:ButtonField>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
    </Columns>
    <HeaderStyle Width="20px" />
</asp:GridView>