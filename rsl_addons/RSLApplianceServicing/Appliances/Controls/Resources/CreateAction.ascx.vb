﻿Imports System
Imports AS_Utilities
Imports AS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class CreateAction
    Inherits UserControlBase
    Dim objStatusBL As StatusBL = New StatusBL()
    Dim objResourceBL As ResourcesBL = New ResourcesBL()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        'pnlActionControl.Visible = True
        'If Not IsPostBack Then
        'Me.LoadRankingDDL()
        'End If

    End Sub

    Protected Sub LoadRankingDDL()
        Dim resultDataSet As DataSet = New DataSet()
        Try
            'Dim trViewStatus As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
            'Dim ActionId As Integer = trViewStatus.SelectedNode.Value
            'Dim StatusId As Integer = trViewStatus.SelectedNode.Parent.Value
            objStatusBL.getActions(resultDataSet)
            Dim count As Integer = 0
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), ""))
                count = +1
            Next
            'ddlRanking.DataSource = resultDataSet
            'ddlRanking.DataTextField = "Ranking"
            'ddlRanking.DataValueField = "Ranking"
            'ddlRanking.DataBind()
            ddlRanking.Items.Add(New ListItem((resultDataSet.Tables(0).Rows.Count + 1).ToString(), "-1"))
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim Title As String = txtBoxTitle.Text
            Dim ranking As Integer = ddlRanking.SelectedItem.Text
            Dim treeView As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
            Dim StatusId As Integer = CType(Session.Item(SessionConstants.statusIdofAction), Integer)
            'Dim StatusId As Integer = ViewState(ViewStateConstants.StatusIdForAddAction)
            Dim CreatedBy As Integer = SessionManager.getAppServicingUserId() 'set the User id from Session
            If Not Title = "" Then
                objResourceBL.AddAction(StatusId, Title, ranking, CreatedBy)
                resetControl()
                Dim UpdPnlStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
                reloadTreeView()
                UpdPnlStatus.Update()
                Dim actionControl As UserControl = CType(UpdPnlStatus.FindControl("aControl"), UserControl)
                Dim pnlActionControl As Panel = CType(actionControl.FindControl("pnlActionControl"), Panel)
                'treeView.SelectedNode = treeView.Nodes
                txtBoxTitle.Text = String.Empty
                LoadRankingDDL(StatusId, False)
                pnlActionControl.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionSavedSuccessfuly, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, "Action Title is required", True)
            End If
            'pnlActionControl.Visible = False
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ActionError, True)
            End If
        End Try
    End Sub

    Protected Sub resetControl()
        'LoadRankingDDL()
        txtBoxTitle.Text = String.Empty

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Try
            Dim Title As String = txtBoxTitle.Text
            Dim ranking As Integer = CType((ddlRanking.SelectedItem.Value), Integer)
            Dim treeView As TreeView = CType(Me.Parent.FindControl("trVwStatus"), TreeView)
            Dim StatusId As Integer = CType(Session.Item(SessionConstants.StatIdforEditAction), Integer)
            Dim ActionId As Integer = CType(Session.Item(SessionConstants.ActioIdforEditAction), Integer)
            Dim modifiedBy As Integer = SessionManager.getAppServicingUserId()
            If Not Title = "" Then
                objResourceBL.editAction(ActionId, StatusId, Title, ranking, modifiedBy)
                Dim UpdPnlStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
                reloadTreeView()
                UpdPnlStatus.Update()
                Dim actionControl As UserControl = CType(UpdPnlStatus.FindControl("aControl"), UserControl)
                Dim pnlActionControl As Panel = CType(actionControl.FindControl("pnlActionControl"), Panel)
                'loadTreeSelectedNodes(StatusId, ActionId)
                LoadRankingDDL(StatusId, True)
                ddlRanking.SelectedIndex = ranking - 1
                pnlActionControl.Visible = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EditActionSuccessful, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, "Action Title is required", True)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub reloadTreeView()
        Try
            Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
            Dim objStatus As Status = New Status()
            tree.Nodes(0).ChildNodes.Clear()
            tree.Height = 0
            objStatus.PopulateNodes(tree.Nodes(0))
            objStatus.PopulateActions(tree.Nodes(0))
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub loadTreeSelectedNodes(ByVal StatusId As Integer, ByVal ActionId As Integer)
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
        Dim objStatus As Status = New Status()
        objStatus.PopulateNodes(tree.Nodes(0))
        objStatus.PopulateActions(tree.Nodes(0))
        'Me.Parent.trVwStatus.SelectedNode = Me.Parent.trVwStatus
        tree.ExpandDepth = ActionId
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        Dim ActionControl As UserControl = CType(updPanelStatus.FindControl("aControl"), UserControl)
        ActionControl.Visible = False
    End Sub

    Protected Sub LoadRankingDDL(ByVal statusId As Integer, ByVal checkEdit As Boolean)
        Dim resultDataSet As DataSet = New DataSet()
        Try

            ddlRanking.Items.Clear()
            objStatusBL.getActionRankingByStatusId(resultDataSet, statusId)
            Dim count As Integer = 1
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                ddlRanking.Items.Add(New ListItem(count.ToString(), count.ToString()))
                count = count + 1
            Next
            If checkEdit = False Then
                ddlRanking.Items.Add(New ListItem((resultDataSet.Tables(0).Rows.Count + 1).ToString(), "-1"))
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub


End Class