﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Users
    Inherits UserControlBase
#Region "Attributes"
    Public Delegate Sub addUserDelegate(ByVal abc As String)
    Dim objUsersBL As UsersBL = New UsersBL()
#End Region


#Region "Functions"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Try
            If Not IsPostBack Then
                Me.getUsers()
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub getUsers()

        Dim resultDataset As DataSet = New DataSet()
        objUsersBL.getUsers(resultDataset)
        If (resultDataset.Tables(0).Rows.Count = 0) Then
            lblMessage.Text = "NO record found"
        Else
            'ViewState.Add(ViewStateConstants.AptbaDataSet, resultDataset)
            grdUsers.DataSource = resultDataset
            grdUsers.DataBind()

        End If

    End Sub



    Protected Sub pgvUsers_RowEditing(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim lb As Label = New Label()
            Dim ddl As DropDownList = New DropDownList()
            Dim btnEdit As Button = New Button()
            Dim btnSave As Button = New Button()
            Dim hfl As HiddenField = New HiddenField()

            btnEdit = CType((grdUsers.Rows(index).FindControl("btnEdit")), Button)
            btnSave = CType((grdUsers.Rows(index).FindControl("btnSave")), Button)
            lb = CType((grdUsers.Rows(index).FindControl("lblUserType")), Label)
            ddl = CType((grdUsers.Rows(index).FindControl("ddlUserTypeOptions")), DropDownList)
            hfl = CType(grdUsers.Rows(index).FindControl("hflLookupId"), HiddenField)

            'Session(SessionConstants.EditResourceId) = btnEdit.CommandArgument;
            btnEdit.Visible = False
            btnSave.Visible = True
            lb.Visible = False
            PopulateUserType(index, ddl)
            ddl.EnableViewState = True
            ddl.Visible = True
            ddl.SelectedValue = hfl.Value
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub pgvUsers_RowUpdating(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim lb As Label = New Label()
            Dim ddl As DropDownList = New DropDownList()
            Dim btnEdit As Button = New Button()
            Dim btnSave As Button = New Button()
            Dim hfl As HiddenField = New HiddenField()

            btnEdit = CType((grdUsers.Rows(index).FindControl("btnEdit")), Button)
            btnSave = CType((grdUsers.Rows(index).FindControl("btnSave")), Button)
            lb = CType((grdUsers.Rows(index).FindControl("lblUserType")), Label)
            ddl = grdUsers.Rows(index).FindControl("ddlUserTypeOptions")
            ddl = CType(ddl, DropDownList)
            ViewState(ViewStateConstants.SaveEmployeeId) = btnSave.CommandArgument
            UpdateResource(CType(ddl.SelectedValue, Int32))
            btnEdit.Visible = True
            btnSave.Visible = False
            lb.Visible = True
            ddl.Visible = False
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub PopulateUserType(ByVal index As Int32, ByVal dll As DropDownList)
        'objUsersBL.PopulateUserType()
        Dim codeID As String = "UserTypeID"
        Dim codeName As String = "Description"
        Dim ddl As DropDownList = New DropDownList()
        Dim userTypeList As List(Of UserTypeBO) = New List(Of UserTypeBO)()
        objUsersBL.getUserTypes(index, userTypeList)
        ddl = CType((grdUsers.Rows(index).FindControl("ddlUserTypeOptions")), DropDownList)
        ddl.DataSource = userTypeList
        ddl.DataValueField = codeID
        ddl.DataTextField = codeName
        ddl.DataBind()
        ddl.Items.Add(New ListItem("Remove User", "-1"))

    End Sub

    Public Sub UpdateResource(ByVal value As Int32)
        Dim resultDataset As DataSet = New DataSet()
        Dim EmployeeId As Int32 = ViewState(ViewStateConstants.SaveEmployeeId)
        If (value > 0) Then
            objUsersBL.UpdateResource(EmployeeId, value, resultDataset)
        Else
            objUsersBL.deleteUser(EmployeeId, resultDataset)
        End If
        grdUsers.DataSource = resultDataset
        grdUsers.DataBind()
        updPnlUsers.Update()


    End Sub



    Protected Sub btnAddMoreOperatives_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddMoreOperatives.Click
        Try
            Dim objUpdPnl As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
            Dim objStatusPanel As Panel = CType(objUpdPnl.FindControl("pnlStatusAndLetters"), Panel)
            objStatusPanel.Visible = False
            Dim objaddUserPanel As Panel = CType(objUpdPnl.FindControl("pnlAddUserControl"), Panel)
            objaddUserPanel.Visible = True
            objUpdPnl.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub


    Protected Sub lnkBtnName_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim gvr As GridViewRow = (CType(sender, LinkButton)).Parent.Parent
            Dim index As Integer = gvr.RowIndex
            Dim LinkButtonName As LinkButton = New LinkButton()
            Dim ObjAddUser As AddUser = New AddUser()

            LinkButtonName = CType((grdUsers.Rows(index).FindControl("lnkBtnName")), LinkButton)
            Dim employeeId As Integer = LinkButtonName.CommandArgument


            Dim objUpdPnl As UpdatePanel = CType(Me.Parent.FindControl("updPnlResources"), UpdatePanel)
            Dim objStatusPanel As Panel = CType(objUpdPnl.FindControl("pnlStatusAndLetters"), Panel)
            objStatusPanel.Visible = False
            Dim objaddUserPanel As Panel = CType(objUpdPnl.FindControl("pnlAddUserControl"), Panel)
            objaddUserPanel.Visible = True
            objUpdPnl.Update()
            ObjAddUser.editUser(employeeId)

            objUpdPnl.Update()
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub
#End Region

End Class