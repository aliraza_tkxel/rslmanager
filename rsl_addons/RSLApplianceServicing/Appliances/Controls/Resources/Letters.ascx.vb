﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Letters
    Inherits UserControlBase

#Region "Functions"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        'If (postback) Then
        Me.getLetters()
        'End If

    End Sub
    Public Sub getLetters()
        Dim objLettersBL As LettersBL = New LettersBL()
        Dim resultDataset As DataSet = New DataSet()
        objLettersBL.getLetters(resultDataset)
        If (resultDataset.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoLettersFound, True)
        Else
            'ViewState.Add(ViewStateConstants.AptbaDataSet, resultDataset)
            grdLetters.DataSource = resultDataset
            grdLetters.DataBind()

        End If
    End Sub
#End Region


    Protected Sub btnEditLetter_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim letterID As String = CType(CType(sender, Button).CommandArgument, String)

            Response.Redirect("~/Views/Resources/CreateLetter.aspx?LetterId=" + letterID + "&src=Resources", False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnAddMoreOperatives_Click(ByVal sender As Object, ByVal e As EventArgs)

    End Sub
End Class