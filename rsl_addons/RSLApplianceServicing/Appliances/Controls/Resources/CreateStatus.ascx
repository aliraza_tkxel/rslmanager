﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CreateStatus.ascx.vb" Inherits="Appliances.CreateStatus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Panel runat="server" ID ="pnlStatusControl" Visible="true"  style="    padding-left: 100px;padding-top: 20px;">
    
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label> 
                </asp:Panel>
                <h3 style="padding-left: 8px;display:block;"><asp:Label runat="server" ID="lblNewStatus">Add New Status</asp:Label>
    <asp:Label runat="server" ID="lblEditStatus" Visible="false">Edit Status </asp:Label>
     </h3>
    <table style="width: 100%;">
        <tr>
            <td>Type:<span class="Required">*</span></td>
            <td style="padding-bottom:5px;">
                <%--<asp:DropDownList ID="ddlType" runat="server">
                </asp:DropDownList>--%>
                <asp:Label ID="lblStatus" runat="server">Gas Inpection</asp:Label>
                <br />
            </td>
        </tr>
        <tr>
            
            <td>Status Title:<span class="Required">*</span></td>
            <td><asp:TextBox ID="txtBoxTitle" runat="server" style="width: 153px;"></asp:TextBox>
                <br />
            <asp:Panel ID="pnlErrorMsg" runat="server" Visible="false"><asp:Label ID="lblErrorMsg" runat="server"></asp:Label></asp:Panel>
            <%-- <asp:RequiredFieldValidator runat="server" CssClass="Required" ID="validatortxtBoxTitle" ControlToValidate="txtBoxTitle" ErrorMessage ="Title is required"></asp:RequiredFieldValidator>  
            --%>
            </td>
        </tr>
        <tr>
            <td>Ranking:<span class="Required">*</span></td>
            <td>
                <asp:DropDownList ID="ddlRanking" runat="server" style="height: 25px;width: 160px;">
                </asp:DropDownList>
                <br />
            </td>
        </tr>
        <tr>
        <td></td>
        <td>
            <br />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="White" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" />
            <asp:Button ID="btnEdit" runat="server" Text="Edit" Visible="false" BackColor="White" />
            </td>
        </tr>
    </table>


</asp:Panel>