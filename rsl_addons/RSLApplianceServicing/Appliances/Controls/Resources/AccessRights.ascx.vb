﻿Imports System.Data
Imports System.Data.SqlClient
Imports AS_BusinessLogic
Imports AS_Utilities
Imports AS_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class AccessRights
    Inherits UserControlBase

    Dim objResourceBL As ResourcesBL = New ResourcesBL()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        trVwAccess.ExpandAll()
        'reloadTreeView()
        'refreshView(trVwAccess.Nodes(0))
    End Sub

    Protected Sub AccessTreeView_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles trVwAccess.SelectedNodeChanged

    End Sub

    Public Sub AccessTreeView_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwAccess.TreeNodePopulate
        Try
            If e.Node.ChildNodes.Count = 0 Then
                Select Case (e.Node.Depth)
                    Case 0
                        PopulatePages(e.Node)

                    Case 1
                        PopulateSubPages(e.Node)
                End Select
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub PopulatePages(ByVal node As TreeNode)
        Dim count As Integer = 1
        Dim resultDataSet As DataSet = New DataSet()
        Dim UserPageDataSet As DataSet = New DataSet()

        Try
            Dim employeeId As Integer = Session.Item(SessionConstants.AccessRightEmployeeId)
            objResourceBL.GetPageByEmployeeId(employeeId, UserPageDataSet)
            objResourceBL.getPages(resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                ViewState(ViewStateConstants.StatusDataTable) = resultDataSet
                If UserPageDataSet.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In resultDataSet.Tables(0).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        newNode.PopulateOnDemand = True
                        newNode.Text = row("PageName")
                        newNode.Value = row("PageId")
                        Dim PageId As Integer = newNode.Value
                        Dim customerQuery = (From dataRow In UserPageDataSet.Tables(0) _
                        Where _
                            dataRow.Field(Of Integer)("PageId") = PageId _
                        Select dataRow).ToList()
                        Dim a As Int32 = 0
                        'Query the data table
                        'Dim datatable As DataTable = customerQuery.CopyToDataTable()
                        If customerQuery.Count > 0 Then
                            newNode.Checked = True
                        Else
                            newNode.Checked = False
                        End If
                        newNode.ShowCheckBox = True
                        node.ChildNodes.Add(newNode)
                    Next
                Else
                    For Each row As DataRow In resultDataSet.Tables(0).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        newNode.PopulateOnDemand = True
                        newNode.Text = row("PageName")
                        newNode.Value = row("PageId")
                        newNode.ShowCheckBox = True
                        newNode.Checked = False
                        node.ChildNodes.Add(newNode)
                    Next
                End If
            End If


        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub PopulateSubPages(ByVal node As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Dim UserPageDataSet As DataSet = New DataSet()
        Try
            Dim employeeId As Integer = Session.Item(SessionConstants.AccessRightEmployeeId)
            objResourceBL.GetPageByEmployeeId(employeeId, UserPageDataSet)
            Dim ParentId As Integer = node.Value
            objResourceBL.getPagesbyParentId(ParentId, resultDataSet)
            If resultDataSet.Tables(0).Rows.Count > 0 Then
                If UserPageDataSet.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In resultDataSet.Tables(0).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        newNode.PopulateOnDemand = True
                        'newNode.ShowPlusMinus = False
                        newNode.Text = row("PageName")
                        newNode.Value = row("PageId")
                        Dim PageId As Integer = newNode.Value
                        Dim customerQuery = (From dataRow In UserPageDataSet.Tables(0) _
                        Where _
                            dataRow.Field(Of Integer)("PageId") = PageId _
                        Select dataRow).ToList()
                        Dim a As Int32 = 0
                        'Query the data table
                        'Dim datatable As DataTable = customerQuery.CopyToDataTable()
                        If customerQuery.Count > 0 Then
                            newNode.Checked = True
                        Else
                            newNode.Checked = False
                        End If
                        newNode.ShowCheckBox = True
                        node.ChildNodes.Add(newNode)
                    Next
                Else
                    For Each row As DataRow In resultDataSet.Tables(0).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        newNode.PopulateOnDemand = True
                        newNode.Text = row("PageName")
                        newNode.Value = row("PageId")
                        newNode.ShowCheckBox = True
                        node.ChildNodes.Add(newNode)
                    Next
                End If
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click

    End Sub

    Protected Sub btnSaveRights_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveRights.Click
        Dim CheckedNodesList As List(Of Integer) = New List(Of Integer)()
        Try
            If trVwAccess.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In trVwAccess.CheckedNodes
                    CheckedNodesList.Add(node.Value)
                Next
            End If
            Dim updPnlAccess As UpdatePanel = CType(Me.Parent.FindControl("updPnlAccess"), UpdatePanel)
            Dim employeeId As Integer = Session.Item(SessionConstants.AccessRightEmployeeId)
            'Session.Remove(SessionConstants.AccessRightEmployeeId)
            objResourceBL.saveUserAccessRight(employeeId, CheckedNodesList)

            '------------------------------------------------
            'Set user access pages in session. If the changes are for current user.

            If employeeId = SessionManager.getAppServicingUserId() Then

                SessionManager.removeUserPageAccess()

                Dim objUserBl As UsersBL = New UsersBL()
                Dim resultDataSet As DataSet = New DataSet()

                resultDataSet = objUserBl.getEmployeeById(employeeId)

                Dim userPageAccess As New Dictionary(Of String, Integer)
                Dim count As Integer = 0
                Dim dr As DataRow
                For Each dr In resultDataSet.Tables(0).Rows
                    userPageAccess.Add(dr.Item("PageName").ToString(), count)
                    count = count + 1
                Next

                SessionManager.setUserPageAccess(userPageAccess)

            End If

            '------------------------------------------------

            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveRightsSuccessfuly, False)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Public Sub reloadTreeView(ByVal node As TreeNode)
        Try
            'Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
            'Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
            Dim objStatus As Status = New Status()
            node.ChildNodes.Clear()
            'tree.Height = 0
            PopulatePages(node)
            PopulateSubPages(node)
        Catch ex As ThreadAbortException
            uiMessageHelper.Message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
            End If
        End Try
    End Sub

    Sub refreshView(ByRef node As TreeNode)
        CollapseAndExpandNode(node.Parent)
    End Sub


    Sub CollapseAndExpandNode(ByRef node As TreeNode)
        node.Collapse()
        node.Expand()
        SelectNode()
    End Sub

    Sub SelectNode()
        Dim node As TreeNode
        'Dim updPanelStatus As UpdatePanel = CType(Me.Parent.FindControl("updPnlStatus"), UpdatePanel)
        'Dim tree As TreeView = CType(updPanelStatus.FindControl("trVwStatus"), TreeView)
        For Each node In trVwAccess.Nodes
            If node.ChildNodes.Count > 0 Then
                Dim node1 As TreeNode
                For Each node1 In node.ChildNodes
                    If node1.Text = Session("SelectedNodeText") Then
                        node1.Select()
                    End If
                    If node1.ChildNodes.Count > 0 Then
                        Dim node2 As TreeNode
                        For Each node2 In node1.ChildNodes
                            If node2.Text = Session("SelectedNodeText") Then
                                node2.Select()
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub trVwAccess_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwAccess.TreeNodeCheckChanged
        'Try
        '    Dim parentNodeId As Integer = e.Node.Value
        '    PopulateSubPagesChecked(e.Node)
        'Catch ex As Exception
        '    uiMessageHelper.IsError = True
        '    uiMessageHelper.Message = ex.Message
        '    If uiMessageHelper.IsExceptionLogged = False Then
        '        ExceptionPolicy.HandleException(ex, "Exception Policy")
        '    End If
        'Finally
        '    If uiMessageHelper.IsError = True Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.Message, True)
        '    End If
        'End Try
    End Sub

    Protected Sub PopulateSubPagesChecked(ByVal node As TreeNode)
        Dim resultDataSet As DataSet = New DataSet()
        Dim ParentId As Integer = node.Value
        objResourceBL.getPagesbyParentId(ParentId, resultDataSet)
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In resultDataSet.Tables(0).Rows
                Dim newNode As TreeNode = New TreeNode()
                newNode.PopulateOnDemand = True
                newNode.Text = row("PageName")
                newNode.Value = row("PageId")
                newNode.ShowCheckBox = True
                newNode.Checked = True
                node.ChildNodes.Add(newNode)
            Next
        End If
    End Sub
End Class