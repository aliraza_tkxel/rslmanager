﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EditLetterControl.ascx.vb"
    Inherits="Appliances.EditLetterControl"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    ._lblMessage
    {
        float: left;
        margin-bottom: 10px;
        width: 100%;
    }
    .ajax__html_editor_extender_container
    {
        width: 100% !important;        
    }
    .ajax__html_editor_extender_container, .ajax__html_editor_extender_container *
    {
        font-family: Arial !important;
        font-size: 12pt !important;
    }
    .code_box
    {
        border: 2px solid #000;
        width: 300px;
        min-height: 155px;
    }
    .code_box p
    {
        margin-left: 10px;
    }
    .letter_buttons
    {
        float: right;
    }
    .Arial12pt, .Arial12pt *
    {
        font-family: Arial !important;
        font-size: 12pt !important;
    }
</style>
<asp:Panel ID="pnlLetterContent" runat="server">
    <div id="Resources" class="group">
        <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
            font-weight: bold; margin: 0 0 6px; font-size: 15px;">
            <font color="white">Edit Letter Template</font>
        </p>
        <div style="float: right; height: 558px; margin-left: 1%; width: 100%;" align="left">
            <asp:UpdatePanel runat="server" ID="pnlAddLetterTemplate" >
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server" Text="" CssClass="lblMessage"></asp:Label>
                    </asp:Panel>
                    <span style="float: left; width: 80px;">Letter Title: </span><span>
                        <asp:TextBox ID="txtLetterTitle" runat="server" ClientIDMode="Static" ReadOnly="True">            
                        </asp:TextBox>
                    </span>
                    <br />
                    <br />
                    <span>
                        <asp:TextBox ID="txtLetterTemplate" TextMode="MultiLine" Columns="60" Rows="15" runat="server"
                            Font-Bold="False" CssClass="Arial12pt" />
                        <asp:HtmlEditorExtender ID="richTxtComments" TargetControlID="txtLetterTemplate"
                            EnableSanitization="False" runat="server">
                            <Toolbar>
                                <ajaxToolkit:Bold />
                                <ajaxToolkit:Italic />
                                <ajaxToolkit:Underline />
                                <ajaxToolkit:StrikeThrough />   

                                <ajaxToolkit:JustifyLeft />
                                <ajaxToolkit:JustifyCenter />
                                <ajaxToolkit:JustifyRight />
                                <ajaxToolkit:JustifyFull />                             
                                
                                <ajaxToolkit:InsertOrderedList />
                                <ajaxToolkit:InsertUnorderedList />
                                <ajaxToolkit:Undo />
                                <ajaxToolkit:Redo />
                                <ajaxToolkit:InsertImage />
                            </Toolbar>
                        </asp:HtmlEditorExtender>
                    </span>
                    <br />
                    <div style="width: 100%; float: left;">
                        <div class="code_box" style="float: left">
                            <p>
                                Enter the following codes for the following:</p>
                            <p>
                                Rent Charge = <a href="javascript:void(0)" onclick="insertText('[RC]');">[RC]</a></p>
                            <p>
                                Current Rent Balance = <a href="javascript:void(0)" onclick="insertText('[RB]');">[RB]</a></p>
                            <p>
                                Today's Date = <a href="javascript:void(0)" onclick="insertText('[TD]');">[TD]</a></p>
                        </div>
                        <div style="width: 30px; float: left; height: 155px;">
                        </div>
                        <div class="code_box" style="width: 400px; float: right;">
                            <p>
                                <span style="width: 100%; float: left; margin-bottom: 13px;"><span style="width: 150px;
                                    float: left;">Select Sign Off: </span><span style="width: 150px; float: left;">
                                        <asp:DropDownList ID="ddlSignOff" class="styleselect" runat="server">
                                        </asp:DropDownList>
                                    </span></span><span style="width: 100%; float: left; margin-bottom: 13px;"><span
                                        style="width: 150px; float: left;">Select Team Name: </span><span style="width: 150px;
                                            float: left;">
                                            <asp:DropDownList ID="ddlTeams" class="styleselect" runat="server" OnSelectedIndexChanged="ddlTeams_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </span></span><span style="width: 100%; float: left; margin-bottom: 13px;"><span
                                            style="width: 150px; float: left;">Select From: </span><span style="width: 150px;
                                                float: left;">
                                                <asp:DropDownList ID="ddlFrom" class="styleselect" runat="server">
                                                    <asp:ListItem Value="-1">Please Select From</asp:ListItem>
                                                </asp:DropDownList>
                                            </span></span>
                            </p>
                        </div>
                    </div>
                    <br />
                    <div style="clear: both;">
                    </div>
                    <div class="letter_buttons" style="float: right; padding-top: 10px; text-align: right;
                        width: 100%;">
                        <asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="Send" BackColor="White" />
                        <%If (Not IsNothing(Request.QueryString("pg")) And Request.QueryString("pg") = 0) Then%>
                        <asp:Button ID="btnCancel" runat="server" Text="Close" OnClientClick="closePopup('0')"
                            BackColor="White" />
                        <%Else%>
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="closePopup('2')"
                            BackColor="White" />
                        <%End If%>
                        <asp:Button ID="btnPrintStandardLetter" runat="server" Text="Print" BackColor="White" />
                        <%--<input type="text" class="hiddenField" id="txtPrintStandardLetterHidden"  onchange="Clickheretoprint()" runat="server"/>--%>
                        <%--<asp:CheckBox ID="ckBoxPrintStandardLetter" runat="server" CssClas="hiddenField"  />--%>
                        <asp:Button ID="btnAddSavedLetter" runat="server" Text="Save Letter" BackColor="White" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnPrintStandardLetter" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Panel>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<asp:ModalPopupExtender ID="mdlPopUpSendEmail" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlSendEmail"
    DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnCancelAction"
    BehaviorID="mdlPopupAddAppointmentBehaviorId">
</asp:ModalPopupExtender>

<asp:Panel ID="pnlSendEmail" CssClass="left" runat="server" BackColor="White"
    Width="470px" Height="200px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
    position: absolute; display: none /*-bracket-: hack(; left: 430px !important;
    top: 100px; ); */">
    <div style="width: 100%; font-weight: bold; padding-left: 10px;">
        Send Email</div>
    <div style="clear: both; height: 1px;">
    </div>
    <hr />
    <asp:ImageButton ID="imgBtnCloseSendEmail" runat="server" Style="position: absolute;
        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <div style="overflow-y: scroll; height: 150px; padding-top: 5px !important;">
        <asp:UpdatePanel ID="updPanelSendEmailPopup" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="pnlSendEmailMessage" runat="server">
                    <asp:Label ID="lblSendEmailMessage" runat="server" Text="" CssClass="lblMessage"></asp:Label>
                </asp:Panel>
                <table>
                    <tr>
                        <td colspan="3" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="3">
                            <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                                <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style5">
                            Send To:
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtSendTo" runat="server" Width="246px"></asp:TextBox>
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="add-apt-canel-save">
            <asp:Button ID="btnCancelAction" runat="server" Text="Cancel" BackColor="White" />&nbsp;&nbsp;
            <asp:Button ID="btnSendAction" runat="server" Text="Send Email" BackColor="White" />
        </div>
    </div>
</asp:Panel>