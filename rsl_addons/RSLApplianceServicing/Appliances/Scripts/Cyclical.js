﻿var vat = [];  var totalvalue = [];
$(document).ready(function () {

    populateToBeAllocated("0", "0");
    PopulateVat();
//    $('#contractorDetail').modal('show');
//    $("#ddlVat").val(vat);
//    $("#ddlVat").prop("disabled", true);
//    $("#txtTotal").val(totalvalue);
//    $("#txtTotal").prop("disabled", true);
});
function populateToBeAllocated(attribute, scheme) {
    var rows_selected = [];
    var table = $('#dt-Services-to-be-Allocated').DataTable({
        "destroy": true,
        "dom": "Bfrtip",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "processing": true,
        "serverSide": true,
        "Sort": true,

        "ajax":
                {
                    "url": "CyclicalServices.aspx/PopulateToBeAllocated?attributeId=" + attribute + "&schemeId=" + scheme,
                    "contentType": "application/json",
                    "type": "GET",
                    "dataType": "JSON",
                    "cache": false,
                    "data": function (d) {
                        return d;
                    },
                    "dataSrc": function (json) {
                        json.draw = json.d.draw;
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        var return_data = json;
                        // console.log(json.draw);
                        return return_data.data;
                    }
                },
        "columns": [
                            {},
                            { "title": "Service", "data": "ServiceItemId", "sClass": "hidden" },
                            { "title": "ContractorId", "data": "ContractorId", "sClass": "hidden" },
                            { "title": "Scheme", "data": "SchemeName", "sClass": "tdScheme" },
                            { "title": "Block", "data": "BlockName", "sClass": "tdScheme" },
                            { "title": "PostCode", "data": "PostCode" },
                            { "title": "Cycle Commencement", "data": "Commencement" },
                            { "title": "Cycle Period", "data": "CyclePeriod" },
                            { "title": "Attribute", "data": "Attribute" },
                            { "title": "Contractor", "data": "Contractor" },
                             { "title": "Contract Start/End", "data": "ContractStart" },
                             { "title": "Status", "data": "Status" },
                             { "title": "VAT", "data": "VAT", "sClass": "hidden" },
                             { "title": "TotalValue", "data": "totalValue", "sClass": "hidden" }
                        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-center',
            'render': function (data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }
                ],
        'select': 'multi',
        'order': [1, 'desc'],
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        }

    });


    // Handle click on checkbox
    $('#dt-Services-to-be-Allocated tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#dt-Services-to-be-Allocated').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#dt-Services-to-be-Allocated tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#dt-Services-to-be-Allocated tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    $('div.dataTables_filter').append("<div style='float:right; padding-left:5px; padding-right:5px;'><select id='ddlScheme' style=' width:200px;'><option> </option></select></div>");
    $('div.dataTables_filter').append("<div style='float:right;padding-left:5px;padding-right:5px;'><select id='ddlAttribute' style=' width:200px;'> <option></option></select></div>");
    PopulateAttribute(attribute);
    PopulateScheme(scheme);

    //changeAttribute();
}

$('#btnContractor').click(function () {
    var ContractorArr = [];
    var AttributeArr = [];
    var ServiceIdArr = [];
    var ContractorIdArr = [];
    var VAT = [];
    var totalValue = [];
    $.each($("#dt-Services-to-be-Allocated tr.selected"), function () {
        AttributeArr.push($(this).find('td').eq(8).text());
        ContractorArr.push($(this).find('td').eq(9).text());
        ServiceIdArr.push($(this).find('td').eq(1).text());
        ContractorIdArr.push($(this).find('td').eq(2).text());
        VAT.push($(this).find('td').eq(12).text());
        totalValue.push($(this).find('td').eq(13).text());

    });
    vat = VAT;
    totalvalue = totalValue;
    if (ContractorArr.length > 0 && AttributeArr.length > 0) {
        if (checkSameContractor(ContractorArr, AttributeArr) == true) {
            loadPopUpData(ContractorArr, AttributeArr, ServiceIdArr, ContractorIdArr, VAT, totalValue);
            $('#contractorDetail').modal('show');
            $("#ddlVat").val(vat);
            //$("#ddlVat").prop("disabled", true);
            $("#txtTotal").val(totalvalue);
            //$("#txtTotal").prop("disabled", true);

        }
        else {
            showAlert("Please select the services with same attributes and same contractor.");

        }
    }
    else {
        showAlert("Please select at least one cyclical service.");
    }
});
//////////////////////Check Same contractor and Same Attribute selected /////////////////////////////////////////
function checkSameContractor(ContractorArr, AttributeArr) {
    var contractorArr = [];
    var contractor = ContractorArr[0];
    for (var i = 0; i < ContractorArr.length; ++i) {
        var value = ContractorArr[i];
        if (contractor == value) {
            contractorArr.push(value);
        }
    }
    var attributeArr = [];
    var attribute = AttributeArr[0];
    for (var i = 0; i < AttributeArr.length; ++i) {
        var attrValue = AttributeArr[i];
        if (attribute == attrValue) {
            attributeArr.push(attrValue);
        }
    }

    if (ContractorArr.length == contractorArr.length && AttributeArr.length == attributeArr.length) {
        return true;
    }
    else {
        return false;
    }
}

//////////////////////Check different states of checkboxes in grid view /////////////////////////////////////////
function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

//////////////////////////////Populate Attribute Drop Down List////////////////////////////////////////
function PopulateAttribute(attribute) {

    var attributeData = [];

    $.ajax({
        url: "CyclicalServices.aspx/populateAttribute",
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                attributeData.push(element)
            });
            console.log();
            loadddlAttribute(attributeData);
            if (attribute > 0) {
                $("#ddlAttribute").val(attribute);
                var mylist = document.getElementById("ddlAttribute");
                $("#select2-ddlAttribute-container").html(mylist.options[mylist.selectedIndex].text);
                console.log("set ddlAttribute value " + $("#ddlAttribute").val());
                //document.getElementById("#ddlAttribute").text
            }
        })
    });

}
function loadddlAttribute(attributeData) {

    $("#ddlAttribute").select2({
        placeholder: "Select Attribute",
        allowClear: true,
        data: attributeData
    }).on("change", function (e) {
        populateToBeAllocated($(this).val(), "0");
    });

}
//////////////////////////////Pop up Message Alert on success and failure////////////////////////////////////////

function showAlert(message) {
    dialog.alert({
        title: "Alert",
        message: message,
        button: "OK",
        animation: "fade"
    });
    return false;
}

//////////////////////////////Populate Scheme Drop Down List////////////////////////////////////////
function PopulateScheme(scheme) {
    var SchemeData = [];

    $.ajax({
        url: "CyclicalServices.aspx/populateScheme",
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                SchemeData.push(element)

            });
            // console.log(datax);
            loadddlScheme(SchemeData, scheme);
            if (scheme > 0) {
                $("#ddlScheme").val(scheme);
                var mylist = document.getElementById("ddlScheme");
                $("#select2-ddlScheme-container").html(mylist.options[mylist.selectedIndex].text);
                console.log("set ddlScheme value " + $("#ddlScheme").val());
                //document.getElementById("#ddlAttribute").text
            }

        })
    });

}
function loadddlScheme(SchemeData, scheme) {
    $("#ddlScheme").select2({
        placeholder: "Select Scheme",
        allowClear: true,
        data: SchemeData
    }).on("change", function (e) {
        populateToBeAllocated("0", $(this).val());
    });

    //    console.log("ddlScheme is " + scheme);
    //     $('#ddlScheme').select2('val', scheme);
}
//////////////////////////load Pop Up Data////////////////////////////////////////////
function loadPopUpData(ContractorArr, AttributeArr, ServiceIdArr, ContractorIdArr,VAT,TotalValue) {
    //Populate cost centre drop down list
    resetPopUp();
    PopulateCostCentre();
    var contractor = ContractorArr[0];
    var attribute = AttributeArr[0];
    var contractorId = ContractorIdArr[0];
    $("#hdnContractorId").val(contractorId);
    console.log(contractor);
    $("label[for='Contractor']").html(contractor);
    $("label[for='RequiredWorks']").html(attribute);
    //Populate Contractor contact drop down list
    PopulateContact(contractorId);
    var selectedIds = ServiceIdArr.join(",");
    populateAttributeTable(selectedIds)
    // Populate Vat
//    PopulateVat();
}

function resetPopUp() {
    $('#tblAttribute').DataTable().destroy();
   // populateToBeAllocated("0", "0");
    $("#dvtblWorksRequired").hide();
    $("label[for='Contractor']").html('');
    $("label[for='RequiredWorks']").html('');
    PopulateBudgetHead(0);
    PopulateExpenditure(0);
    //PopulateContact(0);
    PopulateVat();
    $("#select2-ddlBudget-container").html('');
    $("#select2-ddlExpenditure-container").html('');
    $("#select2-ddlCostCentre-container").html('');
    $("#select2-ddlContact-container").html('');
    $("#select2-ddlVat-container").html('');
    $("#ddlCostCentre").val('');
    $("#ddlBudget").val('');
    $("#ddlExpenditure").val('');
    $("#ddlContact").val('');
    $("#ddlVat").val('');
    $("#txtMoreDetail").val('');
}
//////////////////////////////Populate Cost Centre////////////////////////////////////////
function PopulateCostCentre() {
    var CostCentreData = [];

    $.ajax({
        url: "CyclicalServices.aspx/populateCostCentre",
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                CostCentreData.push(element)

            });
            loadddlCostCentre(CostCentreData);
        })
    });

}
function loadddlCostCentre(SchemeData) {
    $("#ddlCostCentre").select2({
        placeholder: "Select Cost Centre",
        allowClear: true,
        data: SchemeData
    }).on("change", function (e) {
        $('#ddlBudget').val('');
        $("#ddlExpenditure").val(null);
        PopulateBudgetHead($(this).val());
    });
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////Populate BudgetHead////////////////////////////////////////
function PopulateBudgetHead(CostCentreId) {
    var CostCentreData = [];

    $.ajax({
        url: "CyclicalServices.aspx/populateBudgetHead?costCentreId=" + CostCentreId,
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                CostCentreData.push(element)

            });

            loadddlBudgetHead(CostCentreData);
        })
    });

}
function loadddlBudgetHead(SchemeData) {
    $("#ddlBudget").select2({
        placeholder: "Select Budget Head",
        allowClear: true,
        data: SchemeData
    }).on("change", function (e) {
        $("#ddlExpenditure").val(null);
        PopulateExpenditure($(this).val());

    });


}
//////////////////////////////Populate Expenditure////////////////////////////////////////
function PopulateExpenditure(budgetHeadId) {
    var CostCentreData = [];
    //console.log("xxxxxx");
    var employeeId = $('#hdnSession').val();
    $.ajax({
        url: "CyclicalServices.aspx/populateExpenditure?budgetHeadId=" + budgetHeadId + "&employeeId=" + employeeId,
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                CostCentreData.push(element)

            });

            loadddlExpenditure(CostCentreData);
        })
    });

}
function loadddlExpenditure(SchemeData) {
    $("#ddlExpenditure").select2({
        placeholder: "Select Expenditure",
        allowClear: true,
        data: SchemeData
    });
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////Populate Contact////////////////////////////////////////
function PopulateContact(contractorId) {
    var ContactData = [];
    // var employeeId = $('#ContentPlaceHolder1_hdnSession').val();
    $.ajax({
        url: "CyclicalServices.aspx/populateContractorContact?contractorId=" + contractorId,
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                ContactData.push(element);
            });

            loadddlContact(ContactData);
        })
    });

}
function loadddlContact(ContactData) {
    $("#ddlContact").select2({
        placeholder: "Select Contact",
        allowClear: true,
        data: ContactData
    });
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////Populate VAT////////////////////////////////////////
function PopulateVat() {
    var VatData = [];
    // var employeeId = $('#ContentPlaceHolder1_hdnSession').val();
    $.ajax({
        url: "CyclicalServices.aspx/PopulateVat",
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            $.each(data.d, function (index, element) {
                VatData.push(element);
            });

            loadddlVat(VatData);
        })
    });

}
function loadddlVat(VatData) {
    $("#ddlVat").select2({
        placeholder: "Select VAT",
        allowClear: true,
        data: VatData
    }).on("change", function (e) {
        console.log($("#ddlVat option:selected").val())
        SetVat();
    });

}
//////////////////////////////////////////////////////////////////////

function populateAttributeTable(serviceItemIds) {
    var totalNetCost = 0;
    var table;
    $.fn.dataTable.ext.errMode = 'throw';
    table = $('#tblAttribute').DataTable({
        "destroy": true,
        "iDisplayLength": 100,
        "processing": true,
        "serverSide": true,
        "Sort": false,
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "ajax": {
            "url": "CyclicalServices.aspx/PopulateServices?serviceItemIds='" + serviceItemIds + "'",
            "contentType": "application/json",
            "type": "GET",
            "dataType": "JSON",
            "cache": false,
            "data": function (d) {
                console.log(d);
                return d;
            },
            "dataSrc": function (json) {
                $.each(json.d, function (index, element) {
                    var netCost = element["value"];
                    totalNetCost = totalNetCost + netCost;
                    //console.log(totalNetCost);
                });
                var cost = FormatCurrencyComma(totalNetCost);
                $("label[for='NetCost']").html(cost);
                SetVat();
                json.data = json.d;
                var return_data = json;
                // console.log(json.draw);
                return return_data.data;
            }
        },
        "columns": [
                            { "title": "Service", "data": "serviceItemId", "sClass": "hidden" },
                            { "title": "Scheme(s)/Block(s):", "data": "scheme", "sClass": "tdScheme" },
                            { "title": "Cycle Commencement:", "data": "commencement" },
                            { "title": "Cycle Value:", "data": "cycleValue", "sClass": "cycleValue" },
                            {},
                            { "title": "Cycle:", "data": "cycle" },
                            { "title": "Cycles:", "data": "cycles" },
                            { "title": "Value(&pound;)", "data": "value", render: $.fn.dataTable.render.number(',', '.', 2, '') },
                            { "title": "Cycle Days", "data": "cycleDays", "sClass": "hidden" },
                            { "title": "End Date", "data": "endDate", "sClass": "hidden" },
                            { "title": "Cycle Type", "data": "cycleType", "sClass": "hidden" },
                            { "title": "Cycle Period", "data": "cyclePeriod", "sClass": "hidden" }
                           
                        ],

        'columnDefs': [{
            'targets': 4,
            'searchable': false,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-left amend',
            'render': function () {
                return '<img src="../../Images/editv.png" class="hoverImage"/>'
            }
        }]
    });

    $('#tblAttribute tbody').on('click', '.amend', function () {
        var row = this.parentElement;
        if (!$('#tblAttribute').hasClass("editing")) {
            $('#tblAttribute').addClass("editing");

            var data = table.row(row).data();
            var $row = $(row);
            var serviceId = $row.find("td:nth-child(1)");
            var thisPosition = $row.find("td:nth-child(4)");
            var thisPositionText = thisPosition.text();
            var amendIcon = $row.find("td:nth-child(5)");
            thisPosition.empty().append("<input type='text' id='Position_" + serviceId.text() + "' class='changeCycleValue' style='width:100px;'/>");
            amendIcon.empty().append("<img src='../../Images/completed.png' class='hoverImage'/>");
            $("#Position_" + serviceId.text()).val(thisPositionText);
            $("#Position_" + serviceId.text()).focus();
            // $($this).removeClass("changeCycleValue");
        }
    });

    $('#tblAttribute tbody').on("blur", ".changeCycleValue", function () {
        var $this = $(this);
        console.log($this.val());
        var row = this.parentElement.parentElement;
        var data = table.row(row).data();
        var $row = $(row);
        var serviceId = $row.find("td:nth-child(1)");
        var thisPosition = $row.find("td:nth-child(4)");
        UpdateCycleValue($this.val(), serviceId.text(), serviceItemIds);
        console.log(serviceId.text());
        thisPosition.empty().html($this.val());
        $('#tblAttribute').removeClass("editing");
    });

    $('#btnClose').click(function () {
        resetPopUp();
        $('#contractorDetail').modal('hide');
        location.reload();
    });
   
}

$('#btnAdd').click(function () {

    if (validateTotal()) {
        var worksRequiredData = [];
        $.each($("#tblAttribute tbody tr"), function () {
            var net = 0, vat = 0, totalvalue = 0;
            //var data = this.data();
            //net = parseFloat(data["value"]);
            net = parseFloat($(this).find('td').eq(7).text().replace(',', ''));
            if ($("#ddlVat option:selected").val() == 0 || $("#ddlVat option:selected").val() == 3) {

                vat = parseFloat(0);
                totalvalue = net;
                var netCost = $("label[for='NetCost']").html();
                net = parseFloat(netCost.replace(/,/g, ''))

            }
            else if ($("#ddlVat option:selected").val() == 1) {

                var VATC = (net / 100) * 20
                VATC = round(round(VATC, 4), 2)
                vat = FormatCurrency(VATC);
                var gross = FormatCurrency(parseFloat(VATC + net));
                totalvalue = gross;
            }
            else if ($("#ddlVat option:selected").val() == 2) {

                var VATC = (net / 100) * 5
                VATC = round(round(VATC, 4), 2)
                vat = FormatCurrency(VATC);
                var gross = FormatCurrency(parseFloat(VATC + net));
                totalvalue = gross;
            }
            var worksRequired = $("label[for='RequiredWorks']").html()
            // var scheme = data["scheme"].replace(/'/g, '');
            var ServiceItemId = $(this).find('td').eq(0).text();

            var item = {}
            item["ServiceItemId"] = ServiceItemId;
            item["worksRequired"] = worksRequired + ": " + $(this).find('td').eq(1).text(); ;
            item["Net"] = net;
            item["Vat"] = vat;
            item["Gross"] = totalvalue;
            var jsonObject = JSON.stringify(item);
            worksRequiredData.push(jsonObject);
            //console.log(data["value"]);

        });
        ////////////////////////////Populate Required works Grid//////////////////////////////////////////
        PopulateWorksRequired("[" + worksRequiredData + "]");

    }
});



$('#btnGeneratePo').click(function () {
    if (validateProcessData()) {
        $(this).prop("disabled", true);
        var jsonAttributeObj = [];
        var jsonWorksRequiredObj = [];
        $.each($("#tblAttribute tbody tr"), function () {
            var item = {}
            item["ServiceItemId"] = $(this).find('td').eq(0).text();
            item["Commence"] = $(this).find('td').eq(2).text();
            item["CycleValue"] = $(this).find('td').eq(3).text();
            item["Cycles"] = $(this).find('td').eq(6).text();
            item["CycleDays"] = $(this).find('td').eq(8).text();
            item["EndDate"] = $(this).find('td').eq(9).text();
            item["cycleType"] = $(this).find('td').eq(10).text();
            item["cycleperiod"] = $(this).find('td').eq(11).text();
            console.log(item);
            jsonAttributeObj.push(item);
        });

        $.each($("#tblWorksRequired tbody tr"), function () {
            var item = {}
            item["ServiceItemId"] = $(this).find('td').eq(0).text();
            item["worksRequired"] = $(this).find('td').eq(1).text();
            item["Net"] = $(this).find('td').eq(2).text();
            item["Vat"] = $(this).find('td').eq(3).text();
            item["Gross"] = $(this).find('td').eq(4).text();
            //item["EndDate"] = $(this).find('td').eq(8).text();
            jsonWorksRequiredObj.push(item);
        });
        //        console.log(jsonAttributeObj);
        //        console.log(jsonWorksRequiredObj);

        var requestObj = {}
        var employeeId = $('#hdnSession').val();
        requestObj["contractorId"] = $("#hdnContractorId").val();
        requestObj["contactId"] = $("#ddlContact option:selected").val();
        requestObj["headId"] = $("#ddlBudget option:selected").val();
        requestObj["expenditureId"] = $("#ddlExpenditure option:selected").val();
        requestObj["vatId"] = $("#ddlVat option:selected").val();
        requestObj["moreDetail"] = $("#txtMoreDetail").val();
        requestObj["sendApprovalLink"] = $("input[name=rdbtnApproval]:checked").val();
        requestObj["assignedBy"] = employeeId;
        //var attributeJson = jQuery.parseJSON(jsonAttributeObj);
        requestObj["attribute"] = jsonAttributeObj;
        //var requiredWorksJson=jQuery.parseJSON(jsonWorksRequiredObj);
        requestObj["requiredWorks"] = jsonWorksRequiredObj;
        console.log(JSON.stringify(requestObj));
        var finalRequest = JSON.stringify(requestObj);
        //console.log(finalRequest);
        $.ajax({
            url: "CyclicalServices.aspx/GeneratePo",
            cache: false,
            method: "POST",
            data: '{"request":' + finalRequest + '}',
            contentType: "application/json",
            success: (function (data) {
                console.log(data.d);
                if (data.d == "Success") {
                    showAlert("Cyclical works has been assigned to Contractor successfully.");
                    populateToBeAllocated("0", "0");
                }
            })
        });

    }

});
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function UpdateCycleValue(cycleValue, serviceId, serviceItemIds) {
    $.ajax({
        url: "CyclicalServices.aspx/UpdateCycleValue?cycleValue=" + cycleValue + "&serviceItemId=" + serviceId,
        cache: false,
        method: "GET",
        contentType: "application/json",
        success: (function (data) {
            populateAttributeTable(serviceItemIds);

        })
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function FormatCurrencyComma(Figure) {
    NewFigure = FormatCurrency(Math.abs(Figure))
    NegativeCurrency = false
    if (parseFloat(Figure, 10) < 0) NegativeCurrency = true
    if ((Figure >= 1000 || Figure <= -1000)) {
        var iStart = NewFigure.indexOf(".");
        if (iStart < 0)
            iStart = NewFigure.length;

        iStart -= 3;
        while (iStart >= 1) {
            NewFigure = NewFigure.substring(0, iStart) + "," + NewFigure.substring(iStart, NewFigure.length)
            iStart -= 3;
        }
    }
    if (NegativeCurrency) NewFigure = "-" + NewFigure.toString()
    return NewFigure
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

function FormatCurrency(fPrice, decimals) {
    fPrice = Math.round(fPrice * Math.pow(10, 10)) / Math.pow(10, 10);
    if (decimals) {
        if (!isNaN(decimals)) indexCount = parseInt(decimals) + 1
        else indexCount = 3
    }
    else indexCount = 3 //default to two decimal places
    plusString = "0."
    for (i = 1; i < 11; i++) {
        if (indexCount == i) plusString += "5"
        else plusString += "0"
    }
    Negate = false
    if (parseFloat(fPrice) < 0) {
        fPrice = 0 - fPrice
        Negate = true
    }
    var sCurrency = "" + (parseFloat(fPrice) + parseFloat(plusString));
    var nPos = sCurrency.indexOf('.');
    if (nPos < 0 && indexCount > 0) {
        sCurrency += '.';
    }
    sCurrency = sCurrency.slice(0, nPos + indexCount);
    var nZero = indexCount - (sCurrency.length - nPos);
    for (var i = 0; i < nZero; i++)
        sCurrency += '0';
    if (Negate)
        return "-" + sCurrency;
    else
        return sCurrency;
}
//////////////////////////////////////////////////////////////////////////////////////
function SetVat() {
    if ($("#ddlVat option:selected").val() == 0 || $("#ddlVat option:selected").val() == 3) {
        $('#txtVat').attr('readonly', false);
        $("#txtVat").val(0.00);
        $('#txtVat').attr('readonly', true);
        var netCost = $("label[for='NetCost']").html();
        var total = parseFloat(netCost.replace(/,/g, ''))
        $('#txtTotal').val(total);
    }
    else if ($("#ddlVat option:selected").val() == 1) {
        $('#txtVat').attr('readonly', false);
        AddVAT(20)
    }
    else if ($("#ddlVat option:selected").val() == 2) {
        $('#txtVat').attr('readonly', false);
        AddVAT(5)
    }

}
function AddVAT(val) {

    var netCost = $("label[for='NetCost']").html();
    var total = parseFloat(netCost.replace(/,/g, ''))
    var VAT = (total / 100) * val
    VAT = round(round(VAT, 4), 2)
    var txtvat = FormatCurrency(VAT);
    $("#txtVat").val(txtvat);
    var gross = FormatCurrency(parseFloat(VAT + total));
    console.log(gross)
    $('#txtTotal').val(gross);
    $('#txtTotal').attr('readonly', true);
    $('#txtVat').attr('readonly', true);

}

function round(number, X) {
    // rounds number to X decimal places, defaults to 2
    X = (!X ? 2 : X);
    return Math.round(number * Math.pow(10, X)) / Math.pow(10, X);
}
//////////////////////////////////////////////////////////////////////////////////////////
function PopulateWorksRequired(tableData) {
    var testdata = jQuery.parseJSON(tableData);
    $("#dvtblWorksRequired").show();
    $('#tblWorksRequired').DataTable({
        "aaData": testdata,
        "destroy": true,
        "iDisplayLength": 100,
        "processing": false,
        "serverSide": false,
        "Sort": false,
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        //        "dataSrc": function () {
        //            return jQuery.parseJSON(tableData);
        //        },
        "aoColumns": [
            { "title": "Service ItemId", "data": "ServiceItemId", "sClass": "hidden" },
            { "title": "Works Required", "data": "worksRequired" },
            { "title": "Net", "data": "Net", render: $.fn.dataTable.render.number(',', '.', 2, '') },
            { "title": "Vat", "data": "Vat", render: $.fn.dataTable.render.number(',', '.', 2, '') },
            { "title": "Gross", "data": "Gross", render: $.fn.dataTable.render.number(',', '.', 2, '') }
            ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;
            var netCost = $("label[for='NetCost']").html();
            var vatcost = FormatCurrencyComma($("#txtVat").val());
            var grossCost = FormatCurrencyComma($('#txtTotal').val());
            $(api.column(2).footer()).html("Total:");
            $(api.column(2).footer()).html(netCost);
            $(api.column(3).footer()).html(vatcost);
            $(api.column(4).footer()).html(grossCost);
        }
    });
}

//////////////////////////////////////////////Validation////////////////////////////////////////////////////////////
function validateTotal() {
    if ($("#ddlVat option:selected").val() == "") {
        showAlert("Please select VAT.");
        return false;
    }
    return true;
}

function validateProcessData() {
    if ($("#ddlCostCentre option:selected").val() <= 0) {
        showAlert("Please select cost centre.");
        return false;
    }
    if ($("#ddlBudget option:selected").val() <= 0) {
        showAlert("Please select budget head.");
        return false;
    }
    if ($("#ddlExpenditure option:selected").val() <= 0) {
        showAlert("Please select expenditure.");
        return false;
    }
//    if ($("#ddlContact option:selected").val() <= 0) {
//        showAlert("Please select contractor contact.");
//        return false;
//    }
    return true;

}













