﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.Constants
{
    public class ApplicationConstants
    {
        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string UsersAccountDeactivated = "Your access to HR Module has been de-activiated. Please contact administrator.";
        public const string HRMenu = "ASB Management";
        /// <summary>
        /// DateFormat key. Saved in Web.config
        /// </summary>
        public const string KeyDateFormat = "DateFormat";
        /// <summary>
        /// DateFormat key for Model. Saved in Web.config
        /// </summary>
        public const string KeyDateFormatModel = "DateFormatForModel";
        /// <summary>
        /// Virtual Directory Key. This key is used to get virtual directory name from Web.config 
        /// </summary>
        public const string VirtualDirectory = "VirtualDirectory";
        public const string HRServices = "HR Services"; 
        public const string ContentType = "application/json";
        //TODO:
        public const string BaseURL = "https://testcrm.broadlandhousinggroup.org/RSLHRModuleApi/";
        //public const string BaseURL = "http://10.0.2.19/RSLHRModuleApi/";

        public const string UnitHours = "Hours";
        public const string UnitHrs = "hrs";
        public const string UnitDays = "days";
        public const int BaseWorkingHours = 8;
        public const string trainingId = "trainingId";
        #region >>> Pay Point Status <<<

        public const string submitted = "Submitted";
        public const string supported = "Supported";
        public const string authorized = "Authorised";
        public const string declined = "Declined";

        #endregion

        #region >>> Declaration of Interest Status <<<

        public const string doiSubmitted = "Submitted";
        public const string doiPending = "Pending";
        public const string doiDeclined = "Declined";
        public const string doiApproved = "Approved";
        public const string doiPendingReview = "Pending Review";        

        #endregion


        #region >>> Reports_Name  <<<

        public const string doi = "DeclarationOfInterests";
        public const string groupTraining = "GroupTraining";

        #endregion

    }
}
