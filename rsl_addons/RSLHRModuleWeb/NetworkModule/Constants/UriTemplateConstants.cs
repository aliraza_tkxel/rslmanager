﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.Constants
{
    public class UriTemplateConstants
    {

        public const string HRMenu = "api/web/Navigation/GetMenus";

        #region ### Employee ###

        public const string EmployeesList = "api/Web/Employees/GetEmployeesList";
        public const string GetEmployeesMainDetail = "api/Web/Employees/GetEmployeesMainDetail";

        #region >>> PersonalDetail <<<

        public const string GetEmployeePersonalDetail = "api/web/Employees/GetEmployeePersonalDetail?employeeId={0}";
        public const string UpdateEmployeePersonalDetail = "api/web/Employees/UpdateEmployeePersonalDetail";
        public const string GetEmployeeConatctDetail = "api/web/Employees/GetEmployeeConatctDetail?employeeId={0}";
        public const string UpdateEmployeeContactDetail = "api/web/Employees/UpdateEmployeeContactDetail";
        public const string UpdateEmployeeImage = "api/web/Employees/UpdateEmployeeImage";
        public const string GetEmployeeReferenceDocuments = "api/web/Post/GetEmployeeReferenceDocuments?employeeId={0}";
        public const string UpdateEmployeeReferenceDocument = "api/web/Post/UpdateEmployeeReferenceDocument";
        public const string GetDeclarationOfInterest = "api/web/Employees/GetDeclarationOfInterest?employeeId={0}";
        public const string GetDeclarationOfInterestById = "api/web/Employees/GetDeclarationOfInterestById?interestId={0}";
        public const string AddDeclarationOfInterest = "api/web/Employees/AddDeclarationOfInterest";
        public const string GetEmployeeGifts = "api/web/Employees/GetEmployeeGifts?employeeId={0}";
        public const string GetEmployeeGiftMaster = "api/web/Employees/GetEmployeeGiftMaster";
        public const string AddEmployeeGifts = "api/web/Employees/AddEmployeeGifts";
        public const string updateGiftStatus = "api/web/Employees/updateGiftStatus";
        public const string GetEmployeePaypointSubmission = "api/web/Employees/GetEmployeePaypointSubmission?employeeId={0}";
        public const string AddEmployeePaypointSubmission = "api/web/Employees/AddEmployeePaypointSubmission";
        public const string GetGradePointSalary = "api/web/Employees/GetGradePointSalary";
        public const string UploadDrivingLicenceImage = "api/web/DrivingLicence/UpdateDrivingLicenceImage";
        public const string GetDrivingLicence = "api/web/DrivingLicence/GetDrivingLicence?employeeId={0}";

        public const string AddUpdateDrivingLicence = "api/web/DrivingLicence/AddUpdateDrivingLicence";
        #endregion

        #region >>> Post <<<

        public const string GetEmployeesJobDetail = "api/web/Post/GetEmployeeJobDetails?employeeId={0}";
        public const string GetJobRolesForTeamId = "api/web/Post/GetJobRolesForTeamId?teamId={0}";
        public const string GetLoggedInUserTeam = "api/web/Post/GetLoggedInUserTeam?employeeId={0}";
        public const string GetTeamDirector = "api/web/Post/GetDirectorForTeamId?teamId={0}";
        public const string UpdateJobDetail = "api/web/Post/AmendJobDetail";
        public const string GetJobRoleHistory = "api/web/Post/GetEmployeeJobRoleHistory?employeeId={0}";
        public const string GetWorkingHours = "api/web/Post/GetEmployeeWorkingHours?employeeId={0}";
        public const string GetDaysTimings = "api/web/Post/GetEmployeeDaysTimings?employeeId={0}&wid={1}&day={2}";
        public const string UploadSalaryLetter = "api/web/Post/AddSalaryAmmendment";
        public const string RemoveSalaryAmendment = "api/web/Post/RemoveSalaryAmendment?salaryAmendmentId={0}";
        public const string UpdateEmployeeContractRoleDocument = "api/web/Post/UpdateEmployeeContractRoleDocument";
        public const string GetHolidayRule = "api/web/Post/GetHolidayRule?eid={0}";
        public const string GetGradePointLookup = "api/web/Post/GetGradePointLookup?eid={0}&isBRS={1}";
        public const string GetJobDetailNote = "api/web/Post/SaveJobDetailNote?employeeId={0}";
        public const string SaveJobDetailNote = "api/web/Post/SaveJobDetailNote";

        //TODO:
        public const string UpdateWorkingHours = "api/web/Post/AmendEmployeeWorkingHours";

        #endregion

        #region >>> Absence <<<

        public const string GetAnnualLeaveDetail = "api/web/Absence/GetAnnualLeaveDetail";
        public const string GetSicknessAbsence = "api/web/Absence/GetSicknessAbsenceWeb";
        public const string GetLeaveOfAbsence = "api/web/Absence/GetLeaveOfAbsenceWeb";
        public const string GetInternalMeetings = "api/web/Absence/GetInternalMeetings";
        public const string GetToilLeaves = "api/web/Absence/GetToilLeaves";

        public const string GetOwnSicknessAbsence = "api/web/Absence/GetOwnSicknessAbsence?absenceHistoryId={0}";
        public const string GetWorkingPattern = "api/web/Absence/GetWorkingPattern?employeeId={0}";
        public const string GetAnnualLeaveDetailByAbsenseHistoryId = "api/web/Absence/GetMyTeamLeavesRequestedWeb?employeeId={0}";
        public const string GetAbsenceLeaveDetailByAbsenseHistoryId = "api/web/Absence/GetAbsenceDetailByAbsenceHistoryId?absenceHistoryId={0}";
        

        public const string SaveSicknessAbsence = "api/web/Absence/RecordSickness";
        public const string SaveAnnualLeaveDetail = "api/web/Absence/RecordAnnualLeave";
        public const string SaveLeaveOfAbsence = "api/web/Absence/RecordLeaveOfAbsence";
        public const string SaveToilAbsence = "api/web/Absence/AddToilRecord";
        public const string AddInternalMeetingAbsence = "api/web/Absence/AddInternalMeetingRecord";
        public const string ChangeAbsenceStatus = "api/web/Absence/ChangeLeaveStatus";
        public const string ChangeSicknessStatus = "api/web/Absence/ChangeSicknessLeaveStatusWeb";
        public const string ChangeInternalLeaveStatus = "api/web/Absence/ChangeInternalLeaveStatus";

        #endregion

        #region >>> Health <<<

        public const string HealthList = "api/web/Health/GetDifficultiesAndDisablities?employeeId={0}";
        public const string AddAmendHealthRecord = "api/web/Health/AddAmendHealthRecord";
        public const string DeleteDisabilityHealthRecord = "api/web/Health/DeleteDisabilityHealthRecord";
        #endregion

        #region >>> Skills <<<
        public const string SkillsList = "api/web/Skills/GetEmployeesSkills";
        public const string AddAmendSkillsRecord = "api/web/Skills/AddAmendEmployeeSkills";
        public const string DeleteSkillsRecord = "api/web/Skills/DeleteEmployeeSkills";
        public const string AddAmendLanguageSkills = "api/web/Skills/AddLanguageSkills";
        #endregion

        #region >>> Benefits <<<
        public const string BenefitsList = "api/web/Benefits/GetEmployeeBenefits?employeeId={0}";
        public const string AddAmendBenefitsRecord = "api/web/Benefits/AddAmendBenefits";
        public const string BenefitsVehicle = "api/web/Benefits/GetBenefitVehicle?Id={0}";
        public const string BenefitsGeneral = "api/web/Benefits/GetBenefitGeneral";
        public const string BenefitsVehicles = "api/web/Benefits/GetBenefitVehicles";
        public const string BenefitsHealth = "api/web/Benefits/GetBenefitHealth";
        public const string BenefitsPension = "api/web/Benefits/GetBenefitPension";
        public const string BenefitsPRP = "api/web/Benefits/GetBenefitPRP";
        public const string BenefitsHealthHistory = "api/web/Benefits/GetBenefitsHealthHistory?BenefitHealthId={0}";
        public const string BenefitsSubscription = "api/web/Benefits/GetBenefitSubscription";
        public const string DeleteV5Doc = "api/web/Benefits/DeleteBenefitV5";
        public const string BenefitsFinancialYear = "api/web/Benefits/GetCurrentFinancialYear?Id={0}";
        #endregion

        #region >>> Documents <<<

        public const string DocumentsList = "api/web/Documents/GetEmployeeDocuments";
        public const string AddDocument = "api/web/Documents/AddEmployeeDocument";
        public const string DeleteDocument = "api/web/Documents/DeleteEmployeeDocument";

        #endregion
        
        #region >>> My Team<<<

        public const string teamList = "api/web/Reports/GetMyTeamList";
        public const string employeeByTeam= "api/web/Reports/GetEmployeeByTeam";
        public const string teamsByDirectorate = "api/web/Reports/GetTeamsByDirectorate?directorateId={0}";
        public const string employeesByteam = "api/web/Trainings/GetEmployeesByTeam?teamId={0}";

        #endregion

        #region >>> My Staff<<<

        public const string staffList = "api/web/Reports/GetMyStaffList";

        #endregion

        #region >>> Trainings <<<
        public const string EmployeesTrainings = "api/web/Trainings/GetEmployeesTrainings";
        public const string AddEmployeeTrainings = "api/web/Trainings/AddEmployeeTrainings";
        public const string UpdateEmployeeTraining = "api/web/Trainings/UpdateEmployeeTraining";
        public const string DeleteEmployeeTrainings = "api/web/Trainings/DeleteEmployeeTrainings";
        #endregion

        #region >>> Journal <<<
        public const string Journal = "api/web/Reports/GETEmployeeJournal";

        #endregion



        #endregion

        #region ### Reports ###

        public const string EstablishmentReport = "api/web/Reports/PopulateEstablishmentReport";
        public const string SicknessReport = "api/web/Reports/PopulateSicknessReport";
        public const string SicknessExport = "api/web/Reports/GetSicknessExport";
        public const string AnnualLeaveReport = "api/web/Reports/PopulateAnnualLeaveReport";
        public const string LeaverReport = "api/web/Reports/PopulateLeaverReport";
        public const string NewStarterReport = "api/web/Reports/PopulateNewStarterReport";
        public const string PayPointReport = "api/web/Employees/GetPaypointReport";
        public const string DOIReport = "api/web/Reports/GetDeclarationOfInterestsReport";
        public const string RemunerationReport = "api/web/Reports/PopulateRemunerationStatement";
        public const string RemunerationStatementDocument = "api/web/Reports/GetRemunerationStatementDocument?employeeId={0}";
        public const string MandatoryTraining = "api/web/Reports/GetMandatoryTrainings";
        public const string TrainingApprovalList = "api/web/Trainings/GetTrainingApprovalList";
        public const string ApproveDeclineGroupTrainings = "api/web/Trainings/ApproveDeclineGroupTrainings";
        public const string HealthListReport = "api/web/Reports/GetHealthList";
        public const string EmployeePaypointSubmission = "/api/web/Employees/GetEmployeePaypointSubmission?employeeId={0}";
        public const string PaypointById = "/api/web/Employees/GetPaypointById?paypointId={0}";
        public const string EmployeePaypointStatus = "/api/web/Employees/UpdatePaypoint";
        public const string UpdateDeclarationOfInterestStatus = "/api/web/Employees/UpdateDeclarationOfInterestStatus";
        public const string GiftApprovalList = "api/web/Reports/GiftApprovalList";
        public const string EmployeesByDirectorate = "api/web/Reports/EmployeesByDirectorate?directorateId={0}";

        #endregion

        #region ### Dashboard ###

        public const string GetMainDetailAlert = "api/Web/Dashboard/GetMainDetailAlert?teamId={0}";
        public const string GetAnnualLeaveAlert = "api/Web/Dashboard/GetAnnualLeaveAlert?teamId={0}";
        public const string GetFullTimeStaffAlert = "api/Web/Dashboard/GetFullTimeStaffAlert?teamId={0}";
        public const string GetSicknessAlert = "api/Web/Dashboard/GetSicknessAlert?teamId={0}";
        public const string GetNewStarterAlert = "api/Web/Dashboard/GetNewStarterAlert?teamId={0}";
        public const string GetHolidayAlert = "api/Web/Dashboard/GetHolidayAlert?teamId={0}";
        public const string GetPayPointAlert = "api/Web/Dashboard/GetPayPointAlert?teamId={0}";
        public const string GetMandatoryTrainingAlert = "api/Web/Dashboard/GetMandatoryTrainingAlert?teamId={0}";
        public const string GetHealthAlert = "api/Web/Dashboard/GetHealthAlert?teamId={0}";
        public const string GetVacanciesList = "api/Web/Dashboard/GetVacanciesList";
        public const string GetTeamLookUp = "api/Web/Dashboard/GetTeamLookUp";

        #endregion

        #region ### Admin ###

        #region >>> Teams <<<
        public const string GetAllTeams = "api/Web/Teams/GetTeamsList";
        public const string GetTeam = "api/web/Teams/GetTeam?teamId={0}";
        public const string DeleteTeam = "api/web/Teams/DeleteTeam?teamId={0}";
        public const string SaveTeam = "api/web/Teams/SaveTeam";
        #endregion

        #region >>> Access Control <<<
        public const string GetEmployee = "api/web/AccessControl/GetEmployee?employeeId={0}";
        public const string UpdateLogin = "api/web/AccessControl/UpdateLogin";
        public const string UnlockAccount = "api/web/AccessControl/UnlockAccount";
        #endregion

        #region >>> Group Training <<<

        public const string GetGroupTrainingsList = "api/web/Trainings/GetGroupTrainingsList";
        public const string GetGroupDetails = "api/web/Trainings/GetGroupDetails?groupId={0}";
        public const string GetEmployeesList = "api/web/Trainings/GetEmployeesList";
        public const string AddEmpployeeGroupTrainings = "api/web/Trainings/AddEmpployeeGroupTrainings";
        public const string UpdateGroupTrainingInfo = "api/web/Trainings/UpdateGroupTrainingInfo";

        #endregion

        #endregion

        #region ### Remitance ###
        public const string supplierData = "api/web/Remitance/GetSupplierData?supplierId={0}";
        public const string invoiceDetail = "api/web/Remitance/GetInvoiceDetail?supplierId={0}&processDate={1}&paymentTypeId={2}";
        #endregion

        #region ### Common ###

        public const string GetTeamsByDirectorates = "api/web/Trainings/GetTeamsByDirectorates?directorateId={0}";

        #endregion

    }
}
