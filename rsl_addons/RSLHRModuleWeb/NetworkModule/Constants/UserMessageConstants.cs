﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.Constants
{
    public class UserMessageConstants
    {
        public const string successMessage = "\"Your detail is updated successfully.\"";
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
        public const string GroupTrainingSuccessMsg = "\"Group Training is saved successfully.\"";
        public const string GroupTrainingUpdateSuccessMsg = "\"Group Training is updated successfully.\"";
    }
}
