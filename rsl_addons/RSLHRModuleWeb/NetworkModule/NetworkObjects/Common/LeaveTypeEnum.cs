﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkObjects
{
    public enum LeaveType
    {
        Annual,
        Sickness,
        LeaveOfAbsence,
        Toil,
        InternalMeeting
    }
}
