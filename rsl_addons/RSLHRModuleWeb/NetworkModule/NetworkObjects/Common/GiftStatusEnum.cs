﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkObjects
{
    public enum GiftStatusEnum
    {
        Rejected = 1,
        Approved = 2,
        Declared = 3,
        PendingApproval = 4
    }
}
