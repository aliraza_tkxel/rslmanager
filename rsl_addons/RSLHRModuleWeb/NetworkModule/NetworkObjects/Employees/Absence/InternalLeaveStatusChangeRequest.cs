﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class InternalLeaveStatusChangeRequest
    {
        public int employeeId { get; set; }
        public int absenceHistoryId { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public double duration { get; set; }
        public int actionBy { get; set; }
        public int actionId { get; set; }
        public string notes { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string title { get; set; }


        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
