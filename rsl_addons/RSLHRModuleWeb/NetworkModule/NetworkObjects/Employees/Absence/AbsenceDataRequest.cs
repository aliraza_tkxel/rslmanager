﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class AbsenceDataRequest
    {
        public int employeeId { get; set; }
        public string fiscalYearDate { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
