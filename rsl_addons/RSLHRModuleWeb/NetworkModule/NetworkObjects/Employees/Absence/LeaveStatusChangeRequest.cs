﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class LeaveStatusChangeRequest
    {
        public int actionBy { get; set; }
        public int absenceHistoryId { get; set; }
        public int actionId { get; set; }
        public string certNo { get; set; }
        public string drName { get; set; }
        public string notes { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
