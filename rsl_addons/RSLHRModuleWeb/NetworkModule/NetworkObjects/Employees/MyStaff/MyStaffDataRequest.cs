﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public  class MyStaffDataRequest
    {

        public int employeeId { get; set; }
        public bool isDirector { get; set; }

        public string sortBy { get; set; } = "fullName";
        public string sortOrder { get; set; } = "ASC";

        public PaginationDataRequest pagination { get; set; } = new PaginationDataRequest
        {
            pageNo = 1,
            pageSize = 12,
            totalPages = 4,
            totalRows = 3,
        };

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}
