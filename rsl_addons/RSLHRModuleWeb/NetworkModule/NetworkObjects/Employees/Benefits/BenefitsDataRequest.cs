﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class BenefitsDataRequest
    {
        public int benefitId { get; set; }
        public int employeeId { get; set; }
        public int vehicleId { get; set; }
        //Accomodation
        public decimal? accomodationRent { get; set; }
        public decimal? councilTax { get; set; }
        public Nullable<decimal> heating { get; set; }
        public decimal? lineRental { get; set; }
        //Company Car
        public string carMake { get; set; }
        public string model { get; set; }
        public decimal listPrice { get; set; }
        public decimal? contHireCharge { get; set; }
        public decimal? empContribution { get; set; }
        public decimal? excessContribution { get; set; }
        public DateTime? carStartDate { get; set; }
        public double? co2Emissions { get; set; }
        public string fuel { get; set; }
        public decimal? compEmpContribution { get; set; }
        public string drivingLicenceNo { get; set; }
        public string motCertNo { get; set; }
        public string insuranceNo { get; set; }
        public DateTime? insuranceRenewalDate { get; set; }
        public string drivingLicenceImage { get; set; }
        // Essential User
        public string engineSize { get; set; }
        public decimal? carAllowance { get; set; }
        public int? carLoan { get; set; }
        public decimal? ecuTopUp { get; set; }
        public DateTime? motRenewalDate { get; set; }
        public decimal? toolAllowance { get; set; }
        public decimal? firstAid { get; set; }
        public decimal? unionSubscription { get; set; }
        public decimal? lifeAssurance { get; set; }
        public int? enhancedHolidayEntitlement { get; set; }
        public decimal? carParkingFacilities { get; set; }
        public decimal? eyeCareAssistance { get; set; }
        public string employeeAssistanceProgramme { get; set; }
        public decimal? enhancedSickPay { get; set; }
        public decimal? learningAndDevelopment { get; set; }
        public decimal? professionalSubscriptions { get; set; }
        public decimal? childcareVouchers { get; set; }
        public decimal? fluAndHepBJabs { get; set; }
        //---------General------------------
        public decimal? professionalFees { get; set; }
        public decimal? telephoneAllowance { get; set; }
        public decimal? firstAidAllowance { get; set; }
        public decimal? callOutAllowance { get; set; }
        /////////////////////// BUPA Medical Insurance///////////////
        public int? medicalOrgId { get; set; }
        public decimal? taxableBenefit { get; set; }
        public decimal? annualPremium { get; set; }
        public string additionalMembers { get; set; }
        public string additionalMembers2 { get; set; }
        public string additionalMembers3 { get; set; }
        public string additionalMembers4 { get; set; }
        public string additionalMembers5 { get; set; }
        public string groupSchemeRef { get; set; }
        public string membershipNo { get; set; }
        /////////////////////// Pension/////////
        public string memNumber { get; set; }
        public int? scheme { get; set; }
        public string schemeNumber { get; set; }
        public double? salaryPercent { get; set; }
        public decimal? employeeContribution { get; set; }
        public decimal? employerContribution { get; set; }
        public decimal? avc { get; set; }
        public int? contractedOut { get; set; }
        public int? lastActionUser { get; set; }
        public string carRegistration { get; set; }
        public string insuranceCompany { get; set; }
        public decimal? insuranceReimbursement { get; set; }
        public string policySummaryPath { get; set; }
        public bool? dvlaOnline { get; set; }
        public DateTime? dvlaOnlineDate { get; set; }
        public string V5Path { get; set; }
        public string requestType { get; set; }
        
        public BenefitVehicleDataRequest vehicle { get; set; }
        public BenefitGeneralDataRequest general { get; set; }
        public BenefitHealthDataRequest health { get; set; }
        public BenefitPensionDataRequest pension { get; set; } = new BenefitPensionDataRequest();
        public BenefitPRPDataRequest prp { get; set; } = new BenefitPRPDataRequest();
        public BenefitSubscriptionDataRequest subscription { get; set; } = new BenefitSubscriptionDataRequest();
        public List<BenefitSubscriptionDataRequest> subscriptionlst { get; set; } = new List<BenefitSubscriptionDataRequest>();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}
