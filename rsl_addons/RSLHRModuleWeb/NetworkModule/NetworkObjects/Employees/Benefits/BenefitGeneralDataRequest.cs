﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class BenefitGeneralDataRequest
    {

        public int benefitGeneralId { get; set; }
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int benefitId { get; set; }
        public decimal? ECU { get; set; }
        public decimal? EnhancedFamilyLeave { get; set; }
        public decimal? PHI { get; set; }
        public decimal? Other { get; set; }
        public decimal? Overtime { get; set; }
        public decimal? callOutAllowance { get; set; }
        public decimal? birthdayLeave { get; set; }
        public decimal? carParking { get; set; }
        public decimal? childCareVouchers { get; set; }
        public decimal? employeeAssistance { get; set; }
        public decimal? enhancedHoliday { get; set; }
        public decimal? enhancedSickPay { get; set; }
        public decimal? eyeCareAssistance { get; set; }
        public decimal? firstAiderAllowance { get; set; }
        public decimal? fluHepBJab { get; set; }
        public decimal? learningAndDevelopment { get; set; }
        public decimal? lifeAssurance { get; set; }
        public decimal? personalDay { get; set; }
        public string VoluntaryDay { get; set; }
        public string birthdayLeaveDate { get; set; }
        public string carParkingDate { get; set; }
        public string childCareVouchersDate { get; set; }
        public string employeeAssistanceDate { get; set; }
        public string enhancedHolidayDate { get; set; }
        public string enhancedSickPayDate { get; set; }
        public string eyeCareAssistanceDate { get; set; }
        public string firstAiderAllowanceDate { get; set; }
        public string fluHelpBJabDate { get; set; }
        public string learningAndDevelopmentDate { get; set; }
        public string lifeAssuranceDate { get; set; }
        public string personalDayDate { get; set; }
        public string VoluntaryDayDate { get; set; }
        public bool isReadOnly { get; set; }
        public string callOutAllowanceDate { get; set; }
        public string fluHepBJabDate { get; set; }
        public string ECUDate { get; set; }
        public string EnhancedFamilyLeaveDate { get; set; }
        public string PHIDate { get; set; }
        public string OtherDate { get; set; }
        public string OvertimeDate { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}
