﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkObjects
{
  public class BenefitPensionDataRequest
    {

        public int benefitPensionId { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYear { get; set; }
        public decimal? accomodationRent { get; set; }
        public decimal? councilTax { get; set; }
        public decimal? lineRental { get; set; }
        public decimal? heating { get; set; }
        public double? salaryPercent { get; set; }
        public decimal? employeeContribution { get; set; }
        public decimal? employerContribution { get; set; }
        public decimal? avc { get; set; }
        public int? contractedOut { get; set; }
        public bool isReadOnly { get; set; }


    }
}
