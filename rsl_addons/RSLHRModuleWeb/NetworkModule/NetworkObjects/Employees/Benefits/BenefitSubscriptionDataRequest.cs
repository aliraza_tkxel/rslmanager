﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkObjects
{
   public class BenefitSubscriptionDataRequest
    {

        public int benefitSubscriptionId { get; set; }
        public int fiscalYear { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public string subscriptionTitle { get; set; }
        public decimal subscriptionCost { get; set; }


    }
}
