﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class BenefitVehicleDataRequest
    {
        public int benefitVehicleId { get; set; }
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int? Type { get; set; }
        public int benefitId { get; set; }
        public string carStartDate { get; set; }
        public string carEndDate { get; set; }
        public string carRegistration { get; set; }
        public string carMake { get; set; }
        public string model { get; set; }
        public string fuel { get; set; }
        public string engineSize { get; set; }
        public string motRenewalDate { get; set; }
        public string V5Path { get; set; }
        public string insuranceCompany { get; set; }
        public string insuranceRenewalDate { get; set; }
        public decimal? insuranceReimbursement { get; set; }
        public string policySummaryPath { get; set; }
        public bool? dvlaOnline { get; set; }
        public string dvlaOnlineDate { get; set; }
        public string additionalDriver { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}
