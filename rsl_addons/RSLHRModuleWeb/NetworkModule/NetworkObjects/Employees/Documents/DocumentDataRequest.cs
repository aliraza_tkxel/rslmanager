﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class DocumentDataRequest
    {
        public int documentId { get; set; }
        public int documentTypeId { get; set; }
        public int documentVisibilityId { get; set; }
        public string title { get; set; }
        public string documentDate { get; set; }
        public string documentExpiry { get; set; }
        public int employeeId { get; set; }
        public int createdBy { get; set; }
        public int updatedBy { get; set; }
        public string documentPath { get; set; }
        public string keywords { get; set; }
        public bool? employeeVisibility { get; set; } = true;
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
