﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class DocumentsListingDataRequest
    {
        public int employeeId { get; set; }
        public string sortBy { get; set; } = "documentId";
        public string sortOrder { get; set; } = "DESC";
        public int? documetTypeId { get; set; }
        public string title { get; set; }
        public string keyWords { get; set; }
        public bool employeeVisibility { get; set; } = true;
        public int loggedInUser { get; set; } 
        public PaginationDataRequest pagination { get; set; } = new PaginationDataRequest
        {
            pageNo = 1,
            pageSize = 12,
            totalPages = 4,
            totalRows = 3,
        };
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}
