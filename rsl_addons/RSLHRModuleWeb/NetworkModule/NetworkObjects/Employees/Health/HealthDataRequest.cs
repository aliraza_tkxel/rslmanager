﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class HealthDataRequest
    {
        public int employeeId { get; set; } = 1000;
        public int diffDisabilityId { get; set; }
        public int disablityId { get; set; }
        public string notes { get; set; }
        public int loggedBy { get; set; }
        public string loggedByString { get; set; }
        public string notifiedDate { get; set; }
        public bool isLongTerm { get; set; } 
        public string impactOnWork { get; set; }
        public bool? isOccupationalHealth { get; set; } 
        public string reviewDate { get; set; }
        public string documentTitle { get; set; }
        public string docPath { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
