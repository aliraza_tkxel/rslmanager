﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class JobDetailNoteDataRequest
    {
        public int jobDetailNoteID { get; set; }
        public int employeeId { get; set; }
        public DateTime _createdDate { get; set; }
        public string createdDate { get; set; }
        public int createdBy { get; set; }
        public string note { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
