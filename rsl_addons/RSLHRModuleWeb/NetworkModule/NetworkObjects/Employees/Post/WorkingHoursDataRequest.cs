﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class EmployeeWorkingHoursDataRequest
    {
        public int employeeId { get; set; }
        public string requestType { get; set; }
        public int? lastActionUser { get; set; }
        public decimal? total { get; set; }
        public string unit { get; set; }
        public List<WorkingHoursDataRequest> workingHours { get; set; } = new List<WorkingHoursDataRequest>();
        public List<WorkingHoursDataRequest> workingHoursHistory { get; set; } = new List<WorkingHoursDataRequest>();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class WorkingHoursDataRequest
    {
        public int employeeId { get; set; }
        public double mon { get; set; }
        public double tue { get; set; }
        public double wed { get; set; }
        public double thu { get; set; }
        public double fri { get; set; }
        public double sat { get; set; }
        public double sun { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int? createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public string createdByName { get; set; }
        public int? lastActionUser { get; set; }
        public double total { get; set; }
        public string DayTimings { get; set; }
    }
}
