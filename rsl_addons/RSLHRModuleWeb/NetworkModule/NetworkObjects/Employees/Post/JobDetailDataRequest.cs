﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class JobDetailDataRequest
    {
        public int jobDetailsId { get; set; }

        public int employeeId { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }

        public int leavingReason { get; set; }

        public string detailsOfRole { get; set; }

        public int? team { get; set; }

        public double? fte { get; set; }

        public int? lineManager { get; set; }
        public int? contractType { get; set; }

        public int? grade { get; set; }

        public decimal? salary { get; set; }

        public string taxOffice { get; set; }

        public string taxCode { get; set; }

        public string payRollNumber { get; set; }

        public double? maxHolidayEntitlementDays { get; set; }

        public double? maxHolidayEntitlementHours { get; set; }

        public double? holidayEntitlementDays { get; set; }

        public double? holidayEntitlementHours { get; set; }

        public string reviewDate { get; set; }

        public int? probationPeriod { get; set; }

        public int? partFullTime { get; set; }

        public double? hours { get; set; }

        public double? employeeLimit { get; set; }

        public int? noticePeriod { get; set; }

        public string dateOfNotice { get; set; }

        public string foreignNationalNumber { get; set; }

        public int? active { get; set; }

        public int? isDirector { get; set; }

        public int? isManager { get; set; }

        public int? gradePoint { get; set; }

        public int? officeLocation { get; set; }

        public int? holidayRule { get; set; }

        public int? holidayIndicator { get; set; }

        public double? carryForward { get; set; }
        public double? carryForwardHours { get; set; }

        public double? bankHoliday { get; set; }
        public double? autoHolidayAdjustment { get; set; }

        public double? totalLeave { get; set; }

        public double? totalLeaveHours { get; set; }

        public string alStartDate { get; set; }

        public int? jobRoleId { get; set; }

        public int? lastActionUser { get; set; }

        public string lastActiontime { get; set; }

        public string tupeInDate { get; set; }
        public string tupeOutDate { get; set; }
        public string payPointReviewDate { get; set; }
        public int fixedTermContract { get; set; }
        public int? isBRS { get; set; } = 1;

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class UploadRoleFileDataRequest
    {
        public int employeeId { get; set; }

        public string filePath { get; set; }
        public string rolePath { get; set; }
        public int updatedBy { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class SalaryAmmendmentDataRequest
    {
        public int employeeId { get; set; }

        public int salaryAmendmentId { get; set; }
        public string fileName { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class GetJobRolesForTeamDataRequest
    {
        public int teamId { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
