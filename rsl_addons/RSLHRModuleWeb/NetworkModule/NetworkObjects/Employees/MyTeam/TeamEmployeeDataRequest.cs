﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class TeamEmployeeDataRequest
    {

      public TeamEmployeeDataRequest() {


            pagination = new PaginationDataRequest();
        }
      public int  employeeId { get; set; }
      public int  teamId { get; set; }
      public string sortBy { get; set; } = "teamName";
      public string sortOrder { get; set; } = "ASC";
      public PaginationDataRequest pagination { get; set; }
      public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}
