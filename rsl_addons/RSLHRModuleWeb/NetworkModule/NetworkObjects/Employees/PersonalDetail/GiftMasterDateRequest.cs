﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class GiftMasterDateRequest
    {
        public int employeeId { get; set; }
        public int statusId { get; set; }
        public int typeId { get; set; }
        public string from { get; set; }
        public string to { get; set; }


        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
