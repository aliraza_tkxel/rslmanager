﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class ContactDetailDataRequest
    {
        public int contactId { get; set; }
        public int employeeId { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }

        public string city { get; set; }

        public string county { get; set; }

        public string postalCode { get; set; }

        public string homePhone { get; set; }

        public string homeEmail { get; set; }

        public string mobile { get; set; }

        public string mobilePersonal { get; set; }

        public string workMobile { get; set; }

        public string workDD { get; set; }

        public string extension { get; set; }

        public string workEmail { get; set; }

        public string nextOfKin { get; set; }

        public string emergencyContact { get; set; }

        public string emergencyTel { get; set; }
        public string emergencyRelationShip { get; set; }

        public string emergencyInfo { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
