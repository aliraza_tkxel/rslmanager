﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class DeclarationOfInterestDataRequest
    {
        public int page { get; set; }
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public int createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public bool? isActive { get; set; }

        public DOIPage1 page1 { get; set; }
        public DOIPage2 page2 { get; set; }
        public DOIPage3 page3 { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class DOIPage1
    {
        public string empName1 { get; set; }
        public string empRole1 { get; set; }
        public int? empRel1 { get; set; }
        public string empName2 { get; set; }
        public string empRole2 { get; set; }
        public int? empRel2 { get; set; }
        public string roleName { get; set; }
        public string roleRole { get; set; }
        public int? roleRel { get; set; }
        public string residingAddress1 { get; set; }
        public string residingAddress2 { get; set; }
        public string residingPostcode { get; set; }
        public string homeName1 { get; set; }
        public string homeAddress1 { get; set; }
        public int? homeRel1 { get; set; }
        public string homeName2 { get; set; }
        public string homeAddress2 { get; set; }
        public int? homeRel2 { get; set; }
        public string typeOfEmployment { get; set; }
        public string employer { get; set; }
        public bool? isSecondaryEmployment { get; set; }
    }

    public class DOIPage2
    {
        public string conOrganizationP6 { get; set; }
        public string conService { get; set; }
        public bool? isConApprovedByLeadership { get; set; }
        public string localAuthName1 { get; set; }
        public string localAuthOrganization1 { get; set; }
        public int? localAuthRel1 { get; set; }
        public string localAuthName2 { get; set; }
        public string localAuthOrganization2 { get; set; }
        public int? localAuthRel2 { get; set; }
        public bool? isDirectorP8 { get; set; }
        public string conOrganizationP8 { get; set; }
        public string conIndvidual { get; set; }
        public int? conRelP8 { get; set; }
        public bool? isDirectorP9 { get; set; }
        public string conOrganizationP9 { get; set; }
    }

    public class DOIPage3
    {
        public string trustee1 { get; set; }
        public string trustee2 { get; set; }
        public string trustee3 { get; set; }
        public string countyTown { get; set; }
        public string appointments { get; set; }
        public string beneficialInterest { get; set; }
        public bool? isInterested { get; set; }
        public string otherInterest { get; set; }
        public string documentPath { get; set; }
    }
}
