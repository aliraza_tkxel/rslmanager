﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class GiftDataRequest
    {
        public int fiscalYearId { get; set; }
        public string documentPath { get; set; }
        public int createdBy { get; set; }
        public int employeeId { get; set; }
        public List<Gift> gifts { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class Gift
    {
        public int giftId { get; set; }
        public string details { get; set; }
        public DateTime? dateGivenReceived { get; set; }
        public string offerToBy { get; set; }
        public double? approximateValue { get; set; }
        public bool? accepted { get; set; }
        public DateTime? notApprovedDate { get; set; }
        public DateTime? dateOfDeclaration { get; set; }
        public string notes { get; set; }
        public DateTime? createdDate { get; set; }
        public int? isGivenReceived { get; set; }
        public int employeeId { get; set; }
        public int fiscalYearId { get; set; }
        public int giftStatusId { get; set; }
        public DateTime? updatedDate { get; set; }
        public int? updatedBy { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public int? isAccepted { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
