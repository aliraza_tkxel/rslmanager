﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class PersonalDetailDataRequest
    {
        public int employeeId { get; set; }

        public int? title { get; set; }

        public string firstName { get; set; }

        public string middleName { get; set; }

        public string lastName { get; set; }

        public string aka { get; set; }

        public string dob { get; set; }

        public string gender { get; set; }

        public string niNumber { get; set; }

        public string dbsDate { get; set; }

        public string reference { get; set; }

        public int? maritalStatus { get; set; }

        public int? ethnicity { get; set; }

        public int? religion { get; set; }

        public int? sexualOrientation { get; set; }

        public string profile { get; set; }

        public string imagePath { get; set; }

        public string ModelDateFormate { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class UpdateEmployeeImageDataRequest
    {
        public int employeeId { get; set; }

        public string imageName { get; set; }

        public string profile { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class UpdateReferenceDocumentDataRequest
    {
        public int documentId { get; set; }
        public int employeeId { get; set; }
        public int createdBy { get; set; }
        public string documentPath { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
