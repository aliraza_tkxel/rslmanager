﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class GradePointSalaryDataRequest
    {
        public int employeeId { get; set; }
        public int grade { get; set; }
        public int point { get; set; }
        public int? fiscalYear { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
