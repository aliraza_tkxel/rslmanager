﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class LicenceDataRequest
    {
     
        public DirvingLicenceDataRequest drivingLicence { get; set; } = new DirvingLicenceDataRequest();
        public List<LicenceDisqualificationsDataRequest> disqualificationsList { get; set; } = new List<LicenceDisqualificationsDataRequest>();
        public LicenceDisqualificationsDataRequest disqualification { get; set; } = new LicenceDisqualificationsDataRequest();

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class DirvingLicenceDataRequest
    {
        public int drivingLicenceId { get; set; }
        public int? employeeId { get; set; }
       
        public string drivingLicenceType { get; set; }

        public string drivingLicenceImage { get; set; }
         
        
        public string drivingLicenceNumber { get; set; }
        
        public string drivingLicenceIssueNumber { get; set; }
        
        public DateTime? expiryDate { get; set; }

        public string expiry { get; set; }

        public string medicalHistory { get; set; }
        public int? createdBy { get; set; }

        public List<DrivingLicenceCategoriesDataRequest> categories { get; set; } = new List<DrivingLicenceCategoriesDataRequest>();
        public DrivingLicenceCategoriesDataRequest categoty = new DrivingLicenceCategoriesDataRequest();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class DrivingLicenceCategoriesDataRequest
    {
        
        public string category { get; set; }        
        public string code { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class LicenceDisqualificationsDataRequest
    {
        public int? employeeId { get; set; }
         
        public int? numberofPoints { get; set; }
        
        public string code { get; set; }
        
        public string reason { get; set; }
        
        public DateTime? issuedDate { get; set; }
        
        public DateTime? expiryDate { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class UploadDrivingLicenceImageDataRequest
    {
        public int employeeId { get; set; }

        public string filePath { get; set; }
        
        public int updatedBy { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

}
