﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class SalaryInformationDataRequest
    {
        public int paypointId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYearId { get; set; }

        public double? currentSalary { get; set; }
        public double? proposedSalary { get; set; }
        public int? grade { get; set; }
        public int? gradePoint { get; set; }
        public string dateProposed { get; set; }
        public string detailRationale { get; set; }

        public bool? approved { get; set; }
        public string approvedDate { get; set; }
        public int? approvedBy { get; set; }

        public bool? waiting { get; set; }
        public string waitingDate { get; set; }
        public int? waitingBy { get; set; }

        public bool? supported { get; set; }
        public string supportedDate { get; set; }
        public int? supportedBy { get; set; }

        public bool? authorized { get; set; }
        public string authorizedDate { get; set; }
        public int? authorizedBy { get; set; }

        public string reason { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
