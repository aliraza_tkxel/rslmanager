﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class SkillsDataRequest
    {


        public int qualificationId { get; set; }
        public int employeeId { get; set; }
        public int? qualLevelID { get; set; }
        public string levelDescription { get; set; }
        public string subject { get; set; }
        public string result { get; set; }
        public string qualificationDate { get; set; }

        public List<string> professionalQualification { get; set; }
        //public string languageSkills { get; set; }

        //public string spokenLanguage { get; set; }
        //public string spokenLanguageOther { get; set; }
        //public string mandatoryTraining { get; set; }
        //public string mandatoryTrainingExpiry { get; set; }
        //public string approvedTraining { get; set; }
        //public bool? remuneration { get; set; }
        //public decimal? remunerationCost { get; set; }

        public int? createdBy { get; set; }

        public int? updatedBy { get; set; }

        //public bool isSpoken { get; set; }


        //public bool isWritten { get; set; }


        public int? equivalentLevel { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }



    }
    public class LanguageSkillsDataRequest
    {

        public int? languageId { get; set; }
        public string language { get; set; }
        public string spokenWritten { get; set; }
        public bool isSpoken { get; set; }
        public bool isWritten { get; set; }
        public int? employeeId { get; set; }
        public int? createdBy { get; set; }
        public int? languageTypeId { get; set; }
        public string languageOther { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
    public class LanguageSkillsRequest
    {
        public List<LanguageSkillsDataRequest> languages { get; set; } = new List<LanguageSkillsDataRequest>();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
