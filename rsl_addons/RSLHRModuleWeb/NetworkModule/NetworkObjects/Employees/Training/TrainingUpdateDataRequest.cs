﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class TrainingUpdateDataRequest
    {
    public int  trainingId {get; set;}
    public int updatedBy {get; set;}
    public bool  isActive {get; set;}
    public string  status {get; set;}

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
