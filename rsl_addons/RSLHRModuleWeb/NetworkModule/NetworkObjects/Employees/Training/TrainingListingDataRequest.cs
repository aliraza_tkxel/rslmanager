﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class TrainingListingDataRequest
    {
        [Required]
        public string fromDate { get; set; }
        [Required]
        public string toDate { get; set; }
        public int? directorate { get; set; }
        public int? team { get; set; }
        public string searchText { get; set; }
        public string employeeType { get; set; }
        public int teamId { get; set; }
        public int employeeId { get; set; }
        public string sortBy { get; set; } = "trainingId";
        public string sortOrder { get; set; } = "ASC";
        public PaginationDataRequest pagination { get; set; } = new PaginationDataRequest
        {
            pageNo = 1,
            pageSize = 12,
            totalPages = 4,
            totalRows = 3,
        };

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}
