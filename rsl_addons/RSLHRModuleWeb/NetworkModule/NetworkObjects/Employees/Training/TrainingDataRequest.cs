﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
  public class TrainingDataRequest
    {
        public TrainingDataRequest() {

            approvedTraining = new List<ApprovedTrainingDataRequest>();

        }
        public int  trainingId {get; set;}
        public int  employeeId  {get; set;}
        public string justification  {get; set;}
        public string course {get; set;}
        public string startDate {get; set;}
        public string endDate {get; set;}
        public int    totalNumberOfDays {get; set;}
        public string providerName {get; set;}
        public string providerWebsite {get; set;}
        public string location {get; set;}
        public string postcode { get; set; }
        public string venue {get; set;}
        public decimal    totalCost {get; set;}
        public bool   isMandatoryTraining {get; set;}
        public string  expiry {get; set;}
        public string  additionalNotes {get; set;}
        public string  status {get; set;}
        public int     createdby {get; set;}

        public string approvedtraining { get; set; }
        public bool isRemuneration { get; set; }
        public decimal remunerationCost { get; set; }
        public int? professionalQualification { get; set; }
        public int? isSubmittedBy { get; set; }

        public List<ApprovedTrainingDataRequest> approvedTraining { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

public class ApprovedTrainingDataRequest
{

    public string approvedtraining {get; set;}
    public bool isRemuneration { get; set; }
    public decimal remunerationCost  { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
