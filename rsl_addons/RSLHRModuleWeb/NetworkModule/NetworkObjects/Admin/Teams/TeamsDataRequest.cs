﻿using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkManagers
{
    public class TeamsDataRequest
    {
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int status { get; set; }
        public int teamId { get; set; }
        public PaginationDataRequest pagination { get; set; } = new PaginationDataRequest();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
   
}
