﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkManagers
{
    public class AccessControlSaveRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string confirmation { get; set; }
        public int? isActive { get; set; }
        public int? loginId { get; set; }
        public int employeeId { get; set; }
        public bool? isLocked { get; set; }
        public int currentUserId { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
