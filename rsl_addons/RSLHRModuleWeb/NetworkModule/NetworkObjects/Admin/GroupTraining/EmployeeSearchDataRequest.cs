﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class EmployeeSearchDataRequest
    {
        public string searchText { get; set; }
        public int? status { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int? teamId { get; set; }
        public int? directorateId { get; set; }

        public PaginationDataRequest pagination { get; set; } = new PaginationDataRequest
        {
            pageNo = 1,
            pageSize = 12,
            totalPages = 4,
            totalRows = 3,
        };

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
