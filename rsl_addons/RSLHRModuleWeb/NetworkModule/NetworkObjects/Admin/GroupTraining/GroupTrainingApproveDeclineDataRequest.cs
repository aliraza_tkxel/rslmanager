﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class GroupTrainingApproveDeclineDataRequest
    {
        public string trainingIds { get; set; }
        public int[] trainingId { get; set; }
        public string employeeIds { get; set; }
        public int?[] employeeId { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public int updatedBy { get; set; }
        

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
