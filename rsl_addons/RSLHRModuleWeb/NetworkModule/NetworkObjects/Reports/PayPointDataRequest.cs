﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
   public class PayPointDataRequest
    {

        public int fiscalYearId { get; set; }
        public int? teamId { get; set; }
        public int? directorate { get; set; }
        public string searchText { get; set; }
        public int status { get; set; }
        public bool notSubmitted { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }

        public string fiscalYearStartDate { get; set; }
        public string fiscalYearEndDate { get; set; }
        public PaginationDataRequest pagination { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}
