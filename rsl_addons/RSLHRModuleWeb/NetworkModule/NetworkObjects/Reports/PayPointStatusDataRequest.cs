﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
  public class PayPointStatusDataRequest
    {

        public int paypointId { get; set; }
        public int lastActionUser { get; set; }
        public int statusId { get; set; }
        public string reason { get; set; }
        public string nextEligibleDate { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}
