﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
  public class NewStarterDataRequest
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int? directorate { get; set; }
        public int? team { get; set; }
        public string employeeType { get; set; }
        public string searchText { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public PaginationDataRequest pagination { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }



    }
}
