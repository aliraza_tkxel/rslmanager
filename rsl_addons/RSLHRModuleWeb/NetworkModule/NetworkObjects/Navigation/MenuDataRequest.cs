﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessModule.NetworkObjects
{
    public class MenuDataRequest
    {
        /// <summary>
        /// User Id.
        /// </summary>
        public int userId { get; set; }
        /// <summary>
        /// Module Name.
        /// </summary>
        public string moduleName { get; set; } = "HR";

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}
