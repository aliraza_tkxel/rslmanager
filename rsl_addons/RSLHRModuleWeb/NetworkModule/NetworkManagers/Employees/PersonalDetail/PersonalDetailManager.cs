﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class PersonalDetailManager : BaseManager
    {
        #region >>> Personal Detail <<<

        public string getPersonalDetail(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeePersonalDetail, request.employeeId));
        }

        public string savePersonalDetail(PersonalDetailDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeePersonalDetail);
        }

        #endregion

        #region >>> Contact Detail <<<

        public string getContactDetail(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeeConatctDetail, request.employeeId));
        }

        public string saveContactDetail(ContactDetailDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeeContactDetail);
        }

        #endregion

        #region >>> Declaration of Interest

        public string GetDeclarationOfInterest(GetPersonalDetailDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetDeclarationOfInterest, request.employeeId));
            return responseString;
        }

        public string SaveInterest(DeclarationOfInterestDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddDeclarationOfInterest);
        }

        #endregion

        #region >>> Gift & Hospitality

        public string giftMaster(GiftMasterDateRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetEmployeeGiftMaster);
        }

        public string getEmployeeGifts(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeeGifts, request.employeeId));
        }

        public string saveGift(GiftDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddEmployeeGifts);
        }

        public string updateGiftStatus(Gift request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.updateGiftStatus);
        }

        #endregion

        #region >>> Salary Information <<<

        public string getSalaryInformation(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeePaypointSubmission, request.employeeId));
        }

        public string SaveSalaryInformation(SalaryInformationDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddEmployeePaypointSubmission);
        }
        public string GetGradePointSalary(GradePointSalaryDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetGradePointSalary);
        }
        

        #endregion

        #region >>> Licence <<<

        public string SaveDrivingLicence(LicenceDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddUpdateDrivingLicence);
        }

        #endregion

        public string getEmployeeReferenceDocuments(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeeReferenceDocuments, request.employeeId));
        }

        public string updateEmployeeImage(UpdateEmployeeImageDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeeImage);
        }

        public string updateEmployeeReferenceDocument(UpdateReferenceDocumentDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeeReferenceDocument);
        }

        public string updateDrivingLicenceImage(UploadDrivingLicenceImageDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UploadDrivingLicenceImage);
        }
        public string GetDrivingLicence(GetPersonalDetailDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetDrivingLicence, request.employeeId));
            return responseString;
        }
    }
}
