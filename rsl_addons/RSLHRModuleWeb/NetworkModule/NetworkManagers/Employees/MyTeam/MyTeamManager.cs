﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class MyTeamManager : BaseManager
    {
        public string GetMyTeams(MyTeamListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.teamList);
        }

        public string GetEmployeeByTeam(TeamEmployeeDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.employeeByTeam);
        }
        public string GetTeamsByDirectorate(int directorateId)
        {
            return getResponse(string.Format(UriTemplateConstants.teamsByDirectorate, directorateId));
        }
        public string GetEmployeesByTeam(int teamId)
        {
            return getResponse(string.Format(UriTemplateConstants.employeesByteam, teamId));
        }
    }
}
