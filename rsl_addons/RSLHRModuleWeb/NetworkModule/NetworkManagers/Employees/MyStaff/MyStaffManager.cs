﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class MyStaffManager : BaseManager
    {
        public string GetMyStaff(MyStaffDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.staffList);
        }
    }
}
