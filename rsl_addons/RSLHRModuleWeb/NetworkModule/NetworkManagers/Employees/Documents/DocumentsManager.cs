﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class DocumentsManager : BaseManager
    {
        public string GetEmployeeDocuments(DocumentsListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DocumentsList);
        }

        public string AddDocumentRecord(DocumentDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddDocument);
        }

        public string deleteDocument(DocumentDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DeleteDocument);
        }
    }
}
