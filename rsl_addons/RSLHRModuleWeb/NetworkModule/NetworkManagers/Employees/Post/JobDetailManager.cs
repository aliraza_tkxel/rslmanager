﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class JobDetailManager : BaseManager
    {
        #region >>> Job Detail <<<

        public string getJobDetail(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployeesJobDetail, request.employeeId));
        }

        public string saveJobDetail(JobDetailDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateJobDetail);
        }

        public string getJobDetailNote(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetJobDetailNote, request.employeeId));
        }

        public string saveJobDetailNote(JobDetailNoteDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.SaveJobDetailNote);
        }

        #endregion

        public string getLoggedInUserTeam(int employeeId)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetLoggedInUserTeam, employeeId));
            return responseString;
        }

        #region >>> Working Hours <<<

        public string getWorkingHours(GetPersonalDetailDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetWorkingHours, request.employeeId));
            return responseString;
        }
        public string getDaysTimings(GetDayTimingDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetDaysTimings, request.employeeId,request.wId,request.day));
            return responseString;
        }
        public string saveWorkingHours(EmployeeWorkingHoursDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateWorkingHours);
        }

        #endregion

        #region >>> File Handling <<<

        public string updateEmployeeContractRoleDocument(UploadRoleFileDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeeContractRoleDocument);
        }

        public string uploadSalaryLetter(SalaryAmmendmentDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UploadSalaryLetter);
        }

        public string removeSalaryLetter(int id)
        {
            return getResponse(string.Format(UriTemplateConstants.RemoveSalaryAmendment, id));
        }

        #endregion

        public string getJobRolesForTeamId(GetJobRolesForTeamDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetJobRolesForTeamId, request.teamId));
            return responseString;
        }

        public string getJobRoleHistory(GetPersonalDetailDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetJobRoleHistory, request.employeeId));
            //responseString = "[   {                'createdDate': '2014-03-06T00:00:00',    'jobRole': 'HR Business Adviser',    'team': 'HR Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2014-06-11T00:00:00',    'jobRole': 'Receptionist',    'team': 'Executive Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2014-07-16T00:00:00',    'jobRole': 'Database Administrator',    'team': 'Digital Engagement ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2014-11-06T00:00:00',    'jobRole': 'Multiskilled Worker',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2014-11-07T00:00:00',    'jobRole': 'Multiskilled Worker',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2014-11-07T00:00:00',    'jobRole': 'Multiskilled Worker',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-09-30T00:00:00',    'jobRole': 'Governance Review',    'team': 'Board',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-09-30T00:00:00',    'jobRole': 'Governance Review',    'team': 'Board',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-09-30T00:00:00',    'jobRole': 'Governance Review',    'team': 'Board',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-09-30T00:00:00',    'jobRole': 'Governance Review',    'team': 'Board',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-02T00:00:00',    'jobRole': 'Business Support Advisor (1)',    'team': 'Business Support Unit',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-02T00:00:00',    'jobRole': 'Business Support Advisor (1)',    'team': 'Business Support Unit',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-20T00:00:00',    'jobRole': 'Customer Census Advisor',    'team': 'Customer Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-20T00:00:00',    'jobRole': 'Apprentice Customer Service Advisor',    'team': 'Customer Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-23T00:00:00',    'jobRole': 'Link Support Worker ',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-10-28T00:00:00',    'jobRole': 'Link Support Worker ',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-11-10T00:00:00',    'jobRole': 'Head of IT and Facilities ',    'team': 'IT and Facilities ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2015-11-13T00:00:00',    'jobRole': 'Carpenter / Joiner',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-02-01T00:00:00',    'jobRole': 'Maintenance Director',   'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-04-25T00:00:00',    'jobRole': 'Bank Worker ',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-04-25T00:00:00',    'jobRole': 'Agency Worker',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-04-25T00:00:00',    'jobRole': 'Bank Worker ',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-04-25T00:00:00',    'jobRole': 'Agency Worker',    'team': 'Training and Employment Services (Dibden Road)',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-05-09T00:00:00',    'jobRole': 'Maintenance Supervisor',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-05-09T00:00:00',    'jobRole': 'Maintenance Supervisor',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-05-24T00:00:00',    'jobRole': 'General Operative',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-02T00:00:00',    'jobRole': 'Head of Digital Engagement',    'team': 'Digital Engagement ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-02T00:00:00',    'jobRole': 'Company Secretary',    'team': 'Executive Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-02T00:00:00',    'jobRole': 'Head of Business Performance Management',    'team': 'Finance Services',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-06T00:00:00',    'jobRole': 'Head of Repairs Service',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-06T00:00:00',    'jobRole': 'Head of Repairs Service',    'team': 'Property ',    'createdby': 'FName113 LName113'  },  {                'createdDate': '2016-06-27T00:00:00',    'jobRole': 'Head of IT and Facilities ',    'team': 'IT and Facilities ',    'createdby': 'FName113 LName113'  }]";
            return responseString;
        }

        public string GetTeamDirector(GetJobRolesForTeamDataRequest request)
        {
            string responseString = string.Empty;

            responseString = getResponse(string.Format(UriTemplateConstants.GetTeamDirector, request.teamId));
            return responseString;
        }

        public string GetHolidayRule(HolidayRuleDataRequest request)
        {
            string responseString = string.Empty;

            responseString = getResponse(string.Format(UriTemplateConstants.GetHolidayRule, request.eid));
            return responseString;
        }

        public string GetGradePointLookup(GradePointLookupDataRequest request)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetGradePointLookup, request.eid, request.isBRS));
            return responseString;
        }
    }
}
