﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class AbsenceManager : BaseManager
    {

        public string GetSicknessAbsence(AbsenceDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetSicknessAbsence);
        }
        public string GetOwnSicknessAbsence(int absenceHistoryId)
        {
            return getResponse(string.Format(UriTemplateConstants.GetOwnSicknessAbsence, absenceHistoryId));
        }

        public string GetAnnualLeaveDetail(AbsenceDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetAnnualLeaveDetail);
        }

        public string GetAnnualLeaveDetailByAbsenseHistoryId(GetPersonalDetailDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetAnnualLeaveDetailByAbsenseHistoryId, request.employeeId));
        }

        public string GetAbsenceDetailByAbsenseHistoryId(int absencehistoryId)
        {
            return getResponse(string.Format(UriTemplateConstants.GetAbsenceLeaveDetailByAbsenseHistoryId, absencehistoryId));
        }

        public string GetLeaveOfAbsence(AbsenceDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetLeaveOfAbsence);
        }

        public string GetToilAbsence(AbsenceDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetToilLeaves);
        }

        public string GetInternalMeetingAbsence(AbsenceDataRequest request)
        {
            return postResponse(request.ToString(),UriTemplateConstants.GetInternalMeetings);
        }

        public string SaveLeave(SaveAbsenceDataRequest request, LeaveType type)
        {
            switch (type)
            {
                case LeaveType.Annual:
                    return postResponse(request.ToString(), UriTemplateConstants.SaveAnnualLeaveDetail);
                    break;
                case LeaveType.Sickness:
                    return postResponse(request.ToString(), UriTemplateConstants.SaveSicknessAbsence);
                    break;
                case LeaveType.LeaveOfAbsence:
                    return postResponse(request.ToString(), UriTemplateConstants.SaveLeaveOfAbsence);
                    break;
                case LeaveType.Toil:
                    return postResponse(request.ToString(), UriTemplateConstants.SaveToilAbsence);
                    break;
                case LeaveType.InternalMeeting:
                    return postResponse(request.ToString(), UriTemplateConstants.AddInternalMeetingAbsence);
                    break;
                default:
                    return "";
                    break;
            }
            
        }

        public string ChangeAbsenceStatus(LeaveStatusChangeRequest request, LeaveType type)
        {
            return postResponse(request.ToString(), UriTemplateConstants.ChangeAbsenceStatus);
        }

        public string ChangeSicknessStatus(LeaveSicknessStatusChangeRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.ChangeSicknessStatus);
        }

        public string ChangeInternalStatus(InternalLeaveStatusChangeRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.ChangeInternalLeaveStatus);
        }
    }
}
