﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using BusinessModule.NetworkObjects.Employees.Benefits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class BenefitsManager : BaseManager
    {
        public string getEmployeeBenefits(BenefitsDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.BenefitsList, request.employeeId));
        }
        public string  getBenefitVehicle(int Id)
        {

            return getResponse(string.Format(UriTemplateConstants.BenefitsVehicle, Id));

        }

        public string getCurrentFinancialYear(int Id)
        {

            return getResponse(string.Format(UriTemplateConstants.BenefitsFinancialYear,Id));

        }

        
        public string getBenefitGeneral(GeneralDataRequest request)
        {

           // return postResponse(request.ToString(), string.Format(UriTemplateConstants.BenefitsGeneral));
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsGeneral);

        }

        public string getBenefitVehicles(GeneralDataRequest request)
        {

            // return postResponse(request.ToString(), string.Format(UriTemplateConstants.BenefitsGeneral));
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsVehicles);

        }

        public string getBenefitHealth(GeneralDataRequest request)
        {

            // return postResponse(request.ToString(), string.Format(UriTemplateConstants.BenefitsGeneral));
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsHealth);

        }
        public string getBenefitPension(GeneralDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsPension);
        }
        public string getBenefitPRP(GeneralDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsPRP);
        }
        public string getBenefitsHealthHistory(int benefitHealthId)
        {

            return getResponse(string.Format(UriTemplateConstants.BenefitsHealthHistory, benefitHealthId));

        }
        public string getBenefitSubscription(GeneralDataRequest request)
        {

            // return postResponse(request.ToString(), string.Format(UriTemplateConstants.BenefitsGeneral));
            return postResponse(request.ToString(), UriTemplateConstants.BenefitsSubscription);

        }
        public string addAmendBenefitsRecord(BenefitsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddAmendBenefitsRecord);
        }
        public string deleteV5Doc(BenefitsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DeleteV5Doc);
        }

    }
}
