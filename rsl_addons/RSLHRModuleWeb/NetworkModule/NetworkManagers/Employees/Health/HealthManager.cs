﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class HealthManager : BaseManager
    {
        public string getDifficultiesAndDisablities(HealthDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.HealthList, request.employeeId));
        }

        public string addAmendHealthRecord(HealthDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddAmendHealthRecord);
        }

        public string deleteDisabilityHealthRecord(HealthDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DeleteDisabilityHealthRecord);
        }
    }
}
