﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace BusinessModule.NetworkManagers
{
    public class EmployeesManager : BaseManager
    {
        public string getEmployeesList(EmployeesDataRequest request)
        {
            if (request.requestList == "MainDetail")
            {
                return postResponse(request.ToString(), UriTemplateConstants.GetEmployeesMainDetail);
            }
            else
            {
                return postResponse(request.ToString(), UriTemplateConstants.EmployeesList);
            }
        }
    }
}
