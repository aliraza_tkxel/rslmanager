﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class TrainingManager : BaseManager
    {
        public string GetEmployeesTrainings(TrainingListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.EmployeesTrainings);
        }

        public string AddEmployeeTrainings(TrainingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddEmployeeTrainings);
        }

        public string UpdateEmployeeTrainings(TrainingUpdateDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateEmployeeTraining);
        }

        public string DeleteEmployeeTrainings(TrainingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DeleteEmployeeTrainings);
        }
        public string GetWorkingPattern(int employeeId)
        {
            return getResponse(string.Format(UriTemplateConstants.GetWorkingPattern, employeeId)); 
        }
    }
}
