﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class SkillsManager : BaseManager
    {
        public string getEmployeesSkills(SkillsListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.SkillsList);
        }

        public string addAmendSkillsRecord(SkillsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddAmendSkillsRecord);
        }

        public string deleteSkillsRecord(SkillsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DeleteSkillsRecord);
        }
        public string addAmendLanguageSkills(LanguageSkillsRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AddAmendLanguageSkills);
        }
    }
}
