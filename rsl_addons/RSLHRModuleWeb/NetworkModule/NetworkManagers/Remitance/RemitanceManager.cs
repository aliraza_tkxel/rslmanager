﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class RemitanceManager : BaseManager
    {
        public string getSupplierData(int supplierId)
        {
            return getResponse(string.Format(UriTemplateConstants.supplierData, supplierId));
        }

        public string getInvoiceDetail(int supplierId, string processDate, int paymentTypeId)
        {
            return getResponse(string.Format(UriTemplateConstants.invoiceDetail, supplierId, processDate, paymentTypeId));
        }
        
    }
}
