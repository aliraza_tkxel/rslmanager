﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class ReportsManager : BaseManager
    {
        public string getEstablishmentReport(EstablishmentDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.EstablishmentReport);
        }

        public string getSicknessReport(SicknessDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.SicknessReport);
        }

        public string getSicknessExport(SicknessDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.SicknessExport);
        }

        public string GetLeaverReport(LeaverDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.LeaverReport);
        }

        public string GetAnnualLeaveReport(AnnualLeaveDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.AnnualLeaveReport);
        }

        public string GetNewStarterReport(NewStarterDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.NewStarterReport);
        }

        public string GetPayPointReport(PayPointDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.PayPointReport);
        }

        public string GetDOIReport(DeclarationOfInterestsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.DOIReport);
        }

        public string GetEmployeePaypointSubmission(int employeeId)
        {
            return getResponse(string.Format(UriTemplateConstants.EmployeePaypointSubmission, employeeId));
        }

        public string GetPaypointById(int paypointId)
        {
            return getResponse(string.Format(UriTemplateConstants.PaypointById, paypointId));
        }

        public string GetRemunerationReport(RemunerationDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.RemunerationReport);
        }

        public string GetRemunerationStatementDocument(int employeeId)
        {
            return getResponse(string.Format(UriTemplateConstants.RemunerationStatementDocument, employeeId));
        }

        public string GetMandatoryTrainings(TrainingListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.MandatoryTraining);
        }

        public string GetGroupTrainings(GroupTrainingApprovalDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.TrainingApprovalList);
        }

        public string ApproveDeclineGroupTrainings(GroupTrainingApproveDeclineDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.ApproveDeclineGroupTrainings);
        }

        public string GetHealthList(HealthReportDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.HealthListReport);
        }

        public string UpdatePaypoint(PayPointStatusDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.EmployeePaypointStatus);
        }

        public string GetDeclarationOfInterestById(int interestId)
        {
            string responseString = string.Empty;
            responseString = getResponse(string.Format(UriTemplateConstants.GetDeclarationOfInterestById, interestId));
            return responseString;
        }

        public string UpdateDeclarationOfInterestStatus(DeclarationOfInterestStatusDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateDeclarationOfInterestStatus);
        }

        public string GetGifts(GiftDataApprovalRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GiftApprovalList);
        }

        public string GetEmployeeByDirectorate(int directorateId)
        {
            return getResponse(string.Format(UriTemplateConstants.EmployeesByDirectorate, directorateId));
        }
    }
}
