﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace BusinessModule.NetworkManagers
{
    public class AccessControlManager : BaseManager
    {
        public string getEmployeeProfile(AccessControlDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetEmployee, request.employeeId));
        }
        public string updateLogin(AccessControlSaveRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.UpdateLogin);
        }
        public string unlockAccount(int employeeId)
        {
            return postResponse(employeeId.ToString(), UriTemplateConstants.UnlockAccount);
        }
    }
}
