﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class GroupTrainingManager : BaseManager
    {
        public string GetGroupTrainingsList(GroupTrainingListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetGroupTrainingsList);
        }

        public string GetJobRolesForTeamId(GroupTrainingListingDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetGroupTrainingsList);
        }

        public string GetGroupDetails(int groupId)
        {
            return getResponse(string.Format(UriTemplateConstants.GetGroupDetails, groupId));
        }

        public string GetEmployeesList(EmployeeSearchDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetEmployeesList);
        }

        public string AddGroupTraining(GroupTrainingDataRequest request)
        {
            if (request.groupId > 0)
            {
                return postResponse(request.ToString(), UriTemplateConstants.UpdateGroupTrainingInfo);
            }
            return postResponse(request.ToString(), UriTemplateConstants.AddEmpployeeGroupTrainings);
        }
    }
}
