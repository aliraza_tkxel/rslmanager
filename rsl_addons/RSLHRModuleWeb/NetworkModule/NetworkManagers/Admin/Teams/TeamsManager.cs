﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace BusinessModule.NetworkManagers
{
    public class TeamsManager : BaseManager
    {
        public string getTeamsList(TeamsDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.GetAllTeams);
        }
        public string getTeamDetail(TeamsDataRequest request)
        {
            return getResponse(string.Format(UriTemplateConstants.GetTeam, request.teamId));
        }
        public string saveTeam(TeamSaveDataRequest request)
        {
            return postResponse(request.ToString(), UriTemplateConstants.SaveTeam);
        }
    }
}
