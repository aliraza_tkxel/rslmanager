﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class DashboardManager : BaseManager
    {
        public string GetAlertCount(string boxName = "", int teamId = 0)
        {
            var requestUrl = "";
            switch (boxName)
            {
                case "SicknessAbsence":
                    requestUrl = string.Format(UriTemplateConstants.GetSicknessAlert, teamId.ToString());
                    break;
                case "Health":
                    requestUrl = string.Format(UriTemplateConstants.GetHealthAlert, teamId.ToString());
                    break;
                case "NewStarters":
                    requestUrl = string.Format(UriTemplateConstants.GetNewStarterAlert, teamId.ToString());
                    break;
                case "AnnualLeave":
                    requestUrl = string.Format(UriTemplateConstants.GetAnnualLeaveAlert, teamId.ToString());
                    break;
                case "OverdueAppraisals":
                    requestUrl = string.Format(UriTemplateConstants.GetMainDetailAlert, teamId.ToString());
                    break;
                case "MainDetail":
                    requestUrl = string.Format(UriTemplateConstants.GetMainDetailAlert, teamId.ToString());
                    break;
                case "FullTimeStaff":
                    requestUrl = string.Format(UriTemplateConstants.GetFullTimeStaffAlert, teamId.ToString());
                    break;
                case "PayPoint":
                    requestUrl = string.Format(UriTemplateConstants.GetPayPointAlert, teamId.ToString());
                    break;
                case "DBSReminder":
                    requestUrl = string.Format(UriTemplateConstants.GetHolidayAlert, teamId.ToString());
                    break;
                case "MendatoryTraining":
                    requestUrl = string.Format(UriTemplateConstants.GetMandatoryTrainingAlert, teamId.ToString());
                    break;
                default:
                    requestUrl = "";
                    break;
            }

            return getResponse(requestUrl);
        }

        public string getVacancyList()
        {
            return getResponse(UriTemplateConstants.GetVacanciesList);
        }

        public string GetTeamLookUp()
        {
            return getResponse(UriTemplateConstants.GetTeamLookUp);
        }
    }
}
