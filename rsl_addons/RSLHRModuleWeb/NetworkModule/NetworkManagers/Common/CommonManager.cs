﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModule.NetworkManagers
{
    public class CommonManager : BaseManager
    {
        public string GetTeamIdForDirectorate(int directorateId)
        {
            return getResponse(string.Format(UriTemplateConstants.GetTeamsByDirectorates, directorateId));
        }
    }
}
