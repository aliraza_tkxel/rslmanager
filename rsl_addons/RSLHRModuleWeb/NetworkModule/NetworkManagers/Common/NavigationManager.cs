﻿using BusinessModule.Constants;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace BusinessModule.NetworkManagers
{
    public class NavigationManager : BaseManager
    {
        public string getTopNavigationMenu(MenuDataRequest menu)
        {
            return postResponse(menu.ToString(), UriTemplateConstants.HRMenu);

            //string responseString = string.Empty;
            //var httpContent = new StringContent(menu.ToString(), Encoding.UTF8, ApplicationConstants.ContentType);

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(ApplicationConstants.BaseURL);
            //    string url = client.BaseAddress + UriTemplateConstants.HRMenu;
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ApplicationConstants.ContentType));

            //    var response = client.PostAsync(url, httpContent).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        responseString = response.Content.ReadAsStringAsync().Result;
            //    }
            //}

            //responseString = "{ 'menues':[{'menuId':26,'menuName':'Employee','path':'Employees/Employees/EmployeesList'},{ 'menuId':29,'menuName':'Monitoring','subMenus':[{'menuId':29,'subMenuName':'Arrears List 2','path':'Customer/ArrearsList_2.asp'},{'menuId':29,'subMenuName':'Arrears Management Report','path':'Customer/ArrearsManagementReport.asp'},{'menuId':29,'subMenuName':'ASB List','path':'Customer/ASB.asp'},{'menuId':29,'subMenuName':'ASB Management','path':'Customer/../RSLASBModuleWeb/?UserId='},{'menuId':29,'subMenuName':'Bad Debt Report','path':'Customer/badDebtReport.asp'},{'menuId':29,'subMenuName':'Contact and Communication Need','path':'Customer/Contact_and_Communication_Need.asp'},{'menuId':29,'subMenuName':'Credit List','path':'Customer/TenantCreditList.asp'},{'menuId':29,'subMenuName':'Ended Tenancies','path':'Customer/EndTenancyList.asp'},{'menuId':29,'subMenuName':'Income Management','path':'Customer/../RSLArrearsManagement/Bridge.aspx'},{'menuId':29,'subMenuName':'New Tenancies','path':'Customer/NewTenancyList.asp'},{'menuId':29,'subMenuName':'Prev. Tenant Arrears','path':'Customer/ArrearsList_PrevTenant.asp'},{'menuId':29,'subMenuName':'Rent Error Report','path':'Customer/renterrorlist.asp'},{'menuId':29,'subMenuName':'Risk List','path':'Customer/RiskList.asp'},{'menuId':29,'subMenuName':'Satisfaction Report','path':'Customer/SatisfactionSurveyReport.asp'},{'menuId':29,'subMenuName':'Service Complaints List','path':'Customer/ServiceComplaintsList.asp'},{'menuId':29,'subMenuName':'Service Complaints Report','path':'Customer/serviceComplaintsReport.asp'},{'menuId':29,'subMenuName':'Tenant Support Referral Report','path':'Customer/../TenancySupport/Bridge.aspx?page=rptTenancySupport'},{'menuId':29,'subMenuName':'Universal Credit Report','path':'Customer/UniversalCreditReport.asp'},{'menuId':29,'subMenuName':'Vulnerability List','path':'Customer/VulnerabilityList.asp'}]},{'menuId':30,'menuName':'Exports','subMenus':[{'menuId':30,'subMenuName':'New Mailing List','path':'Customer/MailingList.asp'},{'menuId':30,'subMenuName':'Rent Letter','path':'Customer/MailingLists/RentLetterDevelopmentList.asp'},{'menuId':30,'subMenuName':'Rent Statement','path':'Customer/MailingLists/RentStatementDevelopmentList.asp'},{'menuId':30,'subMenuName':'Saved Mailing Lists','path':'Customer/MailingListCSV.asp'}]},{'menuId':32,'menuName':'Repairs','subMenus':[{'menuId':32,'subMenuName':'BRS Fault Locator Reports','path':'Customer/../Brsfaultlocator/bridge.aspx?report=yes'},{'menuId':32,'subMenuName':'BRS Repairs Dashboard','path':'Customer/../RepairsDashboard/Bridge.aspx'},{'menuId':32,'subMenuName':'Completed Repairs','path':'Customer/CompletedRepairsList.asp'},{'menuId':32,'subMenuName':'Fault Management','path':'Customer/../faultlocator/faultmanagement/fault_manager.aspx'},{'menuId':32,'subMenuName':'Post Inspection Report','path':'Customer/../faultlocator/secure/reports/postInspection_Report.aspx'},{'menuId':32,'subMenuName':'Pre Inspection Report','path':'Customer/../faultlocator/secure/reports/preInspection_Report.aspx'},{'menuId':32,'subMenuName':'Repair Management','path':'Customer/../faultlocator/secure/faultlocator/repair_list_management.aspx'},{'menuId':32,'subMenuName':'Reported Faults','path':'Customer/../faultlocator/secure/faultlocator/fault_locator.aspx'},{'menuId':32,'subMenuName':'Satisfaction Letters','path':'Customer/SatisfactionLettersList.asp'}]}],'hrModuleMenus':[{'moduleId':10,'moduleName':'Contractors','url':'/Contractors/CORM.asp'},{'moduleId':16,'moduleName':'Property','url':'/PropertyModule/Bridge.aspx'},{'moduleId':0,'moduleName':'Whiteboard','url':'/BHAIntranet/custompages/MyWhiteboard.aspx'},{'moduleId':9,'moduleName':'Customers','url':'/Customer/CustomerSearch.asp'},{'moduleId':3,'moduleName':'My Job','url':'/MyJob/erm.asp'},{'moduleId':1,'moduleName':'Business','url':'/BusinessManager/BusinessManager.asp'},{'moduleId':11,'moduleName':'Finance','url':'/Finance/Finance.asp'},{'moduleId':14,'moduleName':'Accounts','url':'/Accounts/Accounts.asp'},{'moduleId':6,'moduleName':'News','url':'/News/News.asp'},{'moduleId':7,'moduleName':'Portfolio','url':'/Portfolio/Portfolio.asp'},{'moduleId':12,'moduleName':'Suppliers','url':'/Suppliers/Suppliers.asp'},{'moduleId':4,'moduleName':'Logout','url':'/Logout/wash.asp'}]}";

            //return responseString;
        }
    }
}
