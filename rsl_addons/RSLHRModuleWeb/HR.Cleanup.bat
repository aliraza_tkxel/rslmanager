#@del /F /Q *.user
@PUSHD NetworkModule
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD


@PUSHD BHGHRModuleWeb
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Utilities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD


@PAUSE