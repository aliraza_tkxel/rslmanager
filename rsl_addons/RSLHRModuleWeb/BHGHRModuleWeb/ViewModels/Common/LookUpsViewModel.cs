﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class LookUpsViewModel
    {
        #region >>> Personal Detail <<<

        public List<LookUpFields> ethnicity { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> sexualOrientation { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> maritalStatus { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> religion { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> title { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> relationship { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> gradePointLookups { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> gradeLookups { get; set; } = new List<LookUpFields>();

        #endregion

        #region >>> Post <<<

        public List<LookUpFields> team { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> jobRole { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> holidayRule { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> lineManager { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> contractType { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> placeOfWork { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> grade { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> gradePoint { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> noticePeriod { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> probationPeriod { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> dayHour { get; set; }
        public List<LookUpFields> partFullTime { get; set; }

        #endregion

        public List<LookUpFields> absenceNature { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> absenceReason { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> leaveStatus { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> leaveActions { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> disabilityHealthProblem { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> learningDifficulty { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> qualificationLevel { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> equivalentLevel { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> medicalOrganizationList { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> medicalTypeList { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> schemesList { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> benefitTypeList { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> fiscalYearList { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> teamLookUps { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> employeeLookUps { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> directorateLookUps { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> managers{ get; set; } = new List<LookUpFields>();
        public List<LookUpFields> reasonLookUps { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> employeeType { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> documentType { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> documentVisibility { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> year { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> statusLookUps { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> fiscalYears { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> doiStatus { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> directorates { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> directors { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> paypointStatus { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> languageTypes { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> trainingStatus { get; set; } = new List<LookUpFields>();

        public List<LookUpFields> employees { get; set; } = new List<LookUpFields>();
        public List<LookUpFields> giftStatus { get; set; } = new List<LookUpFields>(); 
    }

    public class LookUpFields
    {
        public int lookUpId { get; set; }
        public string lookUpDescription { get; set; }
    }
}