﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class PaginationViewModel
    {
        public int pageSize { get; set; }
        public int pageNo { get; set; }
        public int totalRows { get; set; }
        public int totalPages { get; set; }
    }
}