﻿using BHGHRModuleWeb.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class SicknessReportViewModel
    {
        public List<SicknessViewModel> sicknessList { get; set; }

        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();

       
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }



    }
}