﻿using BHGHRModuleWeb.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class SicknessGridViewModel
    {
        public int reason { get; set; }
        public int? directorate { get; set; }
        public int? team { get; set; }
        public bool isLongTerm { get; set; }
        public string employeeType { get; set; }
        public SelectList employeeTypes { get; set; }

        [Display(Name = "From:")]
        public DateTime? fromDate { get; set; }

        [Display(Name = "To:")]
        public DateTime? toDate { get; set; }

        public SicknessReportViewModel sicknessList { get; set; }
        public SicknessExportReportViewModel sicknessExportList { get; set; }
        public LookUpsViewModel lookUps { get; set; }


        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> reasonLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.reasonLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                // list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                // list.Add(new SelectListItem() { Text = "Please Select Marital Status", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}