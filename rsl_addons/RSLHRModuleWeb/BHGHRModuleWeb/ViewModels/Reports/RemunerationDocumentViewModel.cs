﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class RemunerationDocumentViewModel
    {
        public int employeeId { get; set; }
		public string fullName { get; set; }
        public decimal? salary { get; set; }
        public decimal? employerContribution { get; set; }
        public decimal? phi { get; set; }
        public decimal? phcm { get; set; }
        public decimal? carAllowance { get; set; }
        public decimal?carParkingFacilities { get; set; }
        public decimal? eyeCareAssistance { get; set; }
        public decimal? employeeAssistanceProgramme { get; set; }
        public decimal? enhancedHolidayEntitlement { get; set; }
        public decimal? enhancedSickPay { get; set; }
        public decimal? learningAndDevelopment { get; set; }
        public decimal? professionalSubscriptions { get; set; }
        public decimal? childcareVouchers { get; set; }
        public decimal?  fluAndHepBJabs { get; set; }
        public decimal? totalRemunerationPackage { get; set; }
        public decimal? lifeAssurance { get; set; }
        public string fiscalStart { get; set; }
        public string fiscalEnd { get; set; }
    }
}