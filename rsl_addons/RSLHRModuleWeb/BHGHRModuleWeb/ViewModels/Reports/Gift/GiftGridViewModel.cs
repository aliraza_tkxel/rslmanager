﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class GiftGridViewModel
    {
        public int giftId { get; set; }
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public int? directorateId { get; set; }
        public string directorateName { get; set; }
        public string details { get; set; }
        public string receivedGivenDate { get; set; }
        public double? value { get; set; }
        public int? statusId { get; set; }
        public string statusName { get; set; }

        public DateTime? dateGivenReceived { get; set; }
    }
}