﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class GiftReportViewModel
    {
        [Display(Name = "From:")]
        public string fromDate { get; set; }
        [Display(Name = "To:")]
        public string toDate { get; set; }
        public int directorateId { get; set; }
        public int employeeId { get; set; }
        public int statusId { get; set; }
        public List<GiftGridViewModel> giftsAppovalList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> directorates
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorates.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> statuses
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.giftStatus.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Statuses", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> employees
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.employees.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Employees", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";
    }
}