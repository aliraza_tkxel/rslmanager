﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class LeaverViewModel
    {

        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string startDate { get; set; }
        public string leaveDate { get; set; }
        public string reason { get; set; }
       

    }
}