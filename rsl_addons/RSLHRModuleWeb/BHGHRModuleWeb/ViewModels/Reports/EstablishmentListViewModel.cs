﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels.Reports
{
    public class EstablishmentListViewModel
    {
        public List<EstablishmentViewModel> establishmentList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public string totalSalary { get; set; }
        public string totalStaff { get; set; }
        public int newStarter { get; set; }
        public int leaver { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }

    }
}