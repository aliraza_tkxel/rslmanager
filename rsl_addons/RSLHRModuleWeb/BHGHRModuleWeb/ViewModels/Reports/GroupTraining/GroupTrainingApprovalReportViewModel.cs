﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class GroupTrainingApprovalReportViewModel
    {
        public int directorateId { get; set; }
        public int teamId { get; set; }
        public int employeeId { get; set; }
        public int statusId { get; set; }
        public List<GroupTrainingApprovalViewModel> trainingApprovalList { get; set; }
        public LookUpsViewModel lookUps { get; set; }
        public int directorate { get; set; }
        public bool isSearch { get; set; }
        public bool HR { get; set; }
        public bool Director { get; set; }

        public List<SelectListItem> teams
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> directorates
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorates.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> statuses
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.trainingStatus.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please select", Value = "0", Selected = true });

                return list;
            }
            set { }
        }

        public List<SelectListItem> employees
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.employees.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Employees", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
    }
}