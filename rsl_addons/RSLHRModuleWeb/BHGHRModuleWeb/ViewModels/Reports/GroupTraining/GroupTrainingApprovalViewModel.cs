﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class GroupTrainingApprovalViewModel
    {
        public int employeeId { get; set; }
        public int trainingId { get; set; }
        public string employeeName { get; set; }
        public string directorateName { get; set; }
        public string teamName { get; set; }
        public string trainingCourseName { get; set; }
        public double? cost { get; set; }
        public bool? professionalQualification { get; set; }
        public bool isMandatory { get; set; }
        public string createdBy { get; set; }
        public string status { get; set; }
    }
}