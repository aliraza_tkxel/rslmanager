﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class AnnualLeaveViewModel
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public double carryForward { get; set; }
        public double contractedHours { get; set; }
        public decimal totalAllowance { get; set; }
        public decimal daysBooked { get; set; }
        public decimal daysRequested { get; set; }
        public decimal daysTaken { get; set; }
        public decimal balanceRemaining { get; set; }
        public string payRoleNum { get; set; }
        public string lineManager { get; set; }
        public string AlStartDate { get; set; }
        public string EmploymentStartDate { get; set; }
    }
}