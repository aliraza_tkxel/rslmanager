﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class HealthReportViewModel
    {
        public List<HealthViewModel> employeeList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}