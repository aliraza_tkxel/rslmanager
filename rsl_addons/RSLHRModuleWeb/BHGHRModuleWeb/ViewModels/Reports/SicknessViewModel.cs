﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{ 
    public class SicknessViewModel
    {
        
        
       public int employeeId  {get; set;}
       public string fullName {get; set;}
       public string directorate { get; set; }
       public string team { get; set; }
       public string  reason {get; set;}
       public string   startDate {get; set;}
       public string   anticipatedReturnDate {get; set;}
       public string   actualReturnDate {get; set;}
       public string lastDateOfAbsence { get; set; }
       public string     durationString {get; set;}
        public string duration { get; set; }
        public string    returnNotes {get; set;}
       public int    journalId {get; set;}
        
        }


    }
