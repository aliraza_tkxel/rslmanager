﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointViewModel
    {
        public int payPointId { get; set; }
        public int employeeId { get; set; }
        public string name { get; set; }
        public string team { get; set; }
        public string directorate { get; set; }
        public string gradePoint { get; set; }
        public string grade { get; set; }
        public string gradeDescription { get; set; }
        public string gradePointDescription { get; set; }
        public string reviewDate { get; set; }
        public string status { get; set; }
        public string statusDisplay { get; set; }
        public string submittedByName { get; set; }
        public int ceoId { get; set; } = 35;
        public int execDirectorId { get; set; } = 1222;
        public int isCeo { get; set; }
        public string imagePath { get; set; }
        public string reason { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}