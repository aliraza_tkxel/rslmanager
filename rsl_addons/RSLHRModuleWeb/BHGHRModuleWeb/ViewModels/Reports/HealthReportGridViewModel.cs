﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class HealthReportGridViewModel
    {
        public int? directorate { get; set; }
        public int? teamId { get; set; }
        public HealthReportViewModel healthReportListing { get; set; }

        [Display(Name = "From:")]
        public DateTime? fromDate { get; set; }
        [Display(Name = "To:")]
        public DateTime? toDate { get; set; }
        public string disabilityType { get; set; }
        public SelectList disabilityTypes { get; set; }
        public LookUpsViewModel lookUps { get; set; }
        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}