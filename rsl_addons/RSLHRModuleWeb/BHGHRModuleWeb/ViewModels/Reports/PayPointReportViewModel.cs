﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointReportViewModel
    {

        public int payPointStatus { get; set; }
        public bool notSubmitted { get; set; }
        public List<PayPointViewModel> payPoints { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }


    }
}