﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class NewStarterViewModel
    {
        public int employeeId { get; set; }

        public string fullName { get; set; }

        public string directorate { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public string dob { get; set; }
        public string salary { get; set; }
        public string grade { get; set; }
        public string startDate { get; set; }
        public string reviewDate { get; set; }
      



        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}