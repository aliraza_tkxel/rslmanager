﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Reports
{
    public class DeclarationOfInterestVM
    {
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public int? jobRoleId { get; set; }
        public string jobRoleName { get; set; }
        public int lineManagerId { get; set; }
        public string lineManagerName { get; set; }
        public int? teamId { get; set; }
        public string teamName { get; set; }
        public int? directorId { get; set; }
        public string directorName { get; set; }
        public int? directorateId { get; set; }
        public string directorateName { get; set; }
        public int? statusId { get; set; }
        public string statusName { get; set; }
        public DateTime? updatedDate { get; set; }
        public DateTime submittedDate { get; set; }
        public string documentPath { get; set; }
        public int? documentId { get; set; }
        public string comment { get; set; }
        public string reviewComments { get; set; }
        public DateTime? reviewDate { get; set; }
        public string statusDisplay
        {
            get
            {
                string status = string.Empty;

                if (statusName == ApplicationConstants.doiApproved)
                {
                    status = string.Format("{0} ({1})", statusName, updatedDate.Value.ToShortDateString());
                }
                else if (statusName == ApplicationConstants.doiSubmitted || statusName == ApplicationConstants.doiPending)
                {
                    status = ApplicationConstants.doiPending;
                }
                else if (statusName == ApplicationConstants.doiPendingReview)
                {
                    status = ApplicationConstants.doiPendingReview;
                }
                return status;
            }
        }

        public string submitted
        {
            get
            {
                return submittedDate.ToShortDateString();
            }
        }
        public string updated
        {
            get
            {
                if (updatedDate.HasValue)
                {
                    return updatedDate.Value.ToShortDateString();
                }
                return "";
            }
        }
    }
}