﻿using BHGHRModuleWeb.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class EstablishmentGridViewModel
    {
        public int? directorate { get; set; }
        public int? team { get; set; }
        public string employeeType { get; set; }
        public SelectList employeeTypes { get; set; }
        [Display(Name = "From:")]
        public DateTime? fromDate { get; set; }
        [Display(Name = "To:")]
        public DateTime? toDate { get; set; }
        public int fullTime { get; set; }
        public EstablishmentListViewModel establishmentList { get; set; }
        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> partFullTime
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.partFullTime.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All", Value = "0" });
                return list;
            }
            set { }
        }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}