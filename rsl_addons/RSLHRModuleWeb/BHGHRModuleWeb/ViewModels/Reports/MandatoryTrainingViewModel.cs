﻿using BHGHRModuleWeb.ViewModels.Admin.Training;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MandatoryTrainingViewModel
    {
        public int trainingId { get; set; }

        public int employeeId { get; set; }

        public string employeeName { get; set; }
        public string justification { get; set; }

        public string course { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }

        public int totalNumberOfDays { get; set; }

        public string providerName { get; set; }

        public string providerWebsite { get; set; }

        public string location { get; set; }

        public string venue { get; set; }

        public decimal totalCost { get; set; }

        public int? isSubmittedBy { get; set; }

        public bool isMandatoryTraining { get; set; }

        public string expiry { get; set; }


        public string additionalNotes { get; set; }

        public string status { get; set; }



        public int? createdby { get; set; }

        public string appTrainings { get; set; }
        //public string approvedTraining { get; set; }

        public bool isActive { get; set; }
        public string jobTitle { get; set; }
        public string directorateName { get; set; }
        public string teamName { get; set; }
        public string byName { get; set; }
    }


  
}