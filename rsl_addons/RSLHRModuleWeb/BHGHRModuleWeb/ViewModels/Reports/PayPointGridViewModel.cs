﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointGridViewModel
    {
       
        public int? teamId { get; set; }
        public int? directorate { get; set; }

        [Display(Name = "Year")]
        public int fiscalYearId { get; set; }

        [Display(Name = "Status")]
        public int status { get; set; }

        public bool notSubmitted { get; set; }
        public PayPointReportViewModel PayPointList { get; set; }

        public LookUpsViewModel lookUps { get; set; }


        public List<SelectListItem> fiscalYearLookUp { get; set; }
        //{
        //    get
        //    {
        //        var list = new List<SelectListItem>();
        //        var startDate = Convert.ToDateTime(lookUps.fiscalYears.FirstOrDefault().lookUpDescription);

        //        do
        //        {

        //            list.Add(new SelectListItem() { Text = startDate.ToShortDateString() + " - " + endDate.ToShortDateString(), Value = startDate.ToShortDateString() });

        //            startDate = startDate.AddYears(-1);
        //            endDate = endDate.AddYears(-1);

        //        } while (endDate.Year >= 2004);

        //        list.Insert(0, new SelectListItem()
        //        {
        //            Text = "Select Year",
        //            Value = "0"
        //        });

        //        //var list = new List<SelectListItem>();
        //        //list = lookUps.fiscalYears.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();

        //        return list;
        //    }
        //    set { }
        //}

        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> paypointStatusLookUp
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.paypointStatus.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();

                return list;
            }
            set { }
        }
        public int ceoId { get; set; }

        public int execDirectorId { get; set; }
        public override string ToString()
                {
                    var json = new JavaScriptSerializer().Serialize(this);
                    return json;
                }
    }
}