﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class DeclarationOfInterestsReportViewModel
    {
        public int? teamId { get; set; }
        public int? directorate { get; set; }
        [Display(Name = "Year")]
        public int fiscalYearId { get; set; }

        [Display(Name = "Status")]
        public int status { get; set; }

        public DeclarationOfInterestsGridViewModel declarationOfInterestsListing { get; set; }

        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> fiscalYearLookUp
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.fiscalYears.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();

                return list;
            }
            set { }
        }
        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> doiStatusLookUp
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.doiStatus.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                foreach (var item in list)
                {
                    if (item.Text == ApplicationConstants.doiSubmitted)
                    {
                        item.Text = ApplicationConstants.doiPending;
                    }
                }
                list.Add(new SelectListItem() { Text = "All", Value = "0" });
                return list;
            }
            set { }
        }

        public int lineManagerId { get; set; }

        public int userId { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}