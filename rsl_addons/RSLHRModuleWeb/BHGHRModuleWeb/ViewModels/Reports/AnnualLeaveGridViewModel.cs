﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class AnnualLeaveGridViewModel
    {
        public int? directorate { get; set; }
        public int? team { get; set; }

        [Display(Name = "Year")]
        public int Year { get; set; }
        public AnnualLeaveReportViewModel annualLeaveListing { get; set; }
        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> YearsLookUp
        {
            get
            {
                var list = new List<SelectListItem>();
                List<int> listYears = Enumerable.Range(1900, DateTime.Now.Year - 1900 + 30).ToList();
                list = listYears.Select(e => new SelectListItem() { Text = e.ToString(), Value = e.ToString() }).ToList();

                return list;
            }
            set { }
        }

        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> employeeLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.employeeLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Employees", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> yearLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.year.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}