﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointStatus
    {
        public int paypointId { get; set; }
        public int lastActionUser { get; set; }
        public int statusId { get; set; }
        public string reason { get; set; }
        public string nextEligibleDate { get; set; }
    }
}