﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{ 
    public class SicknessExportViewModel
    {
        
        
       public int employeeId  {get; set;}
       public string firstName { get; set;}
       public string surName { get; set; }
       public string full_partTime { get; set; }
       public string employeeStartDate { get; set; }
       public string jobTitle { get; set;}
       public string dept { get; set;}
       public string startDate { get; set;}
       public string endDate { get; set;}
       public string lastDateOfAbsence { get; set; }
       public string duration { get; set;}
        public string durationString { get; set; }
        public string reason { get; set;}
        
        }


    }
