﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class AnnualLeaveReportViewModel
    {
        public List<AnnualLeaveViewModel> reportListing { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}