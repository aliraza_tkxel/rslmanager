﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class MandatoryTrainGridViewModel
    {
        public int teamId { get; set; } = 0;
        public List<MandatoryTrainingViewModel> trainingList { get; set; } = new List<MandatoryTrainingViewModel>();
        public LookUpsViewModel lookUps { get; set; }
        public string employeeType { get; set; }
        public SelectList employeeTypes { get; set; }
        [Display(Name = "From:")]
        public DateTime? fromDate { get; set; }
        [Display(Name = "To:")]
        public DateTime? toDate { get; set; }
        public List<SelectListItem> teamLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Teams", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorateLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "All Directorates", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public int? directorate { get; set; }
        public int? team { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}