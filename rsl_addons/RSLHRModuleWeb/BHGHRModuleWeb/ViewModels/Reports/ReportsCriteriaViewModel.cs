﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class ReportsCriteriaViewModel
    {

        public int reportId { get; set; } = 0;

        public SelectList reports { get; set; }


        public EstablishmentViewModel establishment { get; set; }

    }
}