﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointPopUpViewModel
    {
        public int lineManager { get; set; }
        public string employeeName { get; set; }
        public string jobTitle { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public double? salary { get; set; }
        public string grade { get; set; }
        public string gradePoint { get; set; }
        public PayPointRequestViewModel payPoint { get; set; }
        public int isCeo { get; set; }
        public int ceoId { get; set; } = 35;
        public int execDirectorId { get; set; } = 1222;

        public List<SelectListItem> eligibleDateOptions
        {
            get
            {
                //1 April, 1 July, 1 October, 1 January 
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "1 April", Value = "01/04" });
                list.Add(new SelectListItem() { Text = "1 July", Value = "01/07" });
                list.Add(new SelectListItem() { Text = "1 October", Value = "01/10" });
                list.Add(new SelectListItem() { Text = "1 January", Value = "01/01" });
                return list;
            }
        }

        public List<SelectListItem> eligibleYearOptions
        {
            get
            {
                var currentYear = DateTime.Now.Year;
                var list = new List<SelectListItem>();
                for (int i = currentYear; i < currentYear+5; i++)
                {
                    list.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
                }

                return list;
            }
        }

    }
}