﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class HealthViewModel
    {
        public int disablityId { get; set; }
        public int diffDisabilityId { get; set; }
        public int employeeId { get; set; }
        public string description { get; set; }
        public string notes { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public int loggedBy { get; set; }
        public string loggedByString { get; set; }
        public string notifiedDate { get; set; }
        public string disabilityType { get; set; }
        public string fullName { get; set; }
    }
}