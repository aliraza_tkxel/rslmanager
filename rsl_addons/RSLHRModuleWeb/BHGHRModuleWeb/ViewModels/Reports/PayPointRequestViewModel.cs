﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointRequestViewModel
    {
        public int? paypointId { get; set; }
        public decimal? currentSalary { get; set; }
        public decimal? proposedSalary { get; set; }
        public int? grade { get; set; }
        public int? gradePoint { get; set; }
        public string gradeDescription { get; set; }
        public string gradePointDescription { get; set; }
        public string dateProposed { get; set; }
        public string detailRationale { get; set; }
        public bool? approved { get; set; }
        public bool? supported { get; set; }
        public bool? authorized { get; set; }
        public string approvedDate { get; set; }
        public string supportedDate { get; set; }
        public string authorizedDate { get; set; }
        public int? approvedBy { get; set; }
        public int? supportedBy { get; set; }
        public int? authorizedBy { get; set; }
        public int? employeeId { get; set; }
        public int? fiscalYearId { get; set; }

        public string approvedByName { get; set; }
        public string supportedByName { get; set; }
        public string authorizedByName { get; set; }

        public string paypointStatusDescription { get; set; }
        public string reason { get; set; }
    }
}