﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class OrganizationViewModel
    {
        public string name { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string townCity { get; set; }
        public string postCode { get; set; }
        public string county { get; set; }
    }
}