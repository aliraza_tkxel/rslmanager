﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class RemitanceViewModel
    {
        public List<InvoiceDetailViewModel> invoices { get; set; } = new List<InvoiceDetailViewModel>();
        public OrganizationViewModel organization { get; set; } = new OrganizationViewModel();
        public string paymentMethod { get; set; }
        public string receiverName { get; set; }
        public string processDate { get; set; }
        public DateTime currentDate { get { return DateTime.Now.Date; } }
    }
}