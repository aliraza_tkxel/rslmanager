﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class InvoiceDetailViewModel
    {
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public Double totalGross { get; set; }
        public Double purchaseCount { get; set; }
        public DateTime taxDate { get; set; }
        public string invoiceNumber { get; set; }
        public int orderId { get; set; }
    }
}