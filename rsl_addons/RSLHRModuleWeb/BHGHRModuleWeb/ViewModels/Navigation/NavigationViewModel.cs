﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class NavigationViewModel
    {
        public string CurrentModule { get; set; }
        /// <summary>
        /// Sub Menu Items
        /// </summary>
        public List<MenuViewModel> menues { get; set; }
        /// <summary>
        /// Sub Menu Items of Customer Module
        /// </summary>
        public List<ModuleItemViewModel> hrModuleMenus { get; set; }
    }

    public class ModuleItemViewModel
    {
        /// <summary>
        /// Module ID
        /// </summary>
        public int moduleId { get; set; }
        /// <summary>
        /// Module Name
        /// </summary>
        public string moduleName { get; set; }
        /// <summary>
        /// Module Url for Page redirection
        /// </summary>
        public string url { get; set; }
    }

    public class MenuViewModel
    {
        /// <summary>
        /// Menu Id
        /// </summary>
        public int menuId { get; set; }
        /// <summary>
        /// Menu name to be shown on Top Navigation
        /// </summary>
        public string menuName { get; set; }

        /// <summary>
        /// Menu path to be redirect that page
        /// </summary>
        public string path { get; set; }
        /// <summary>
        /// List os Sub Menus
        /// </summary>
        public List<SubMenuViewModel> subMenus { get; set; } = new List<SubMenuViewModel>();
    }

    public class SubMenuViewModel
    {
        /// <summary>
        /// Parent MenuItem id
        /// </summary>
        public int menuId { get; set; }
        /// <summary>
        /// Menu text
        /// </summary>
        public string subMenuName { get; set; }
        /// <summary>
        /// Redirect Path
        /// </summary>
        public string path { get; set; }
    }
}