﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class DashboardViewModel
    {
        public int teamId { get; set; }

        public List<TileViewModel> tiles { get; set; } = new List<TileViewModel>();

        public List<SelectListItem> teams
        {
            get
            {
                var list = new List<SelectListItem>();
                list = teamLookup.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Team", Value = "0", Selected = true });
                return list;
            }
        }
        public List<LookUpFields> teamLookup { get; set; }
        public List<VacancyViewModel> vacancies { get; set; }
        public List<DisciplineryViewModel> disciplinery { get; set; }
    }

    public class TileViewModel
    {
        public int tileId { get; set; }
        public string tileName { get; set; }
        public int count { get; set; }
        public string url { get; set; }
    }

    public class VacancyViewModel
    {
        public int jobId { get; set; }
        public string title { get; set; }
        public string teamName { get; set; }
        public string description { get; set; }
        public string jobType { get; set; }
        public string startDate { get; set; }
    }

    public class DisciplineryViewModel
    {
        public int disciplineryId { get; set; }
        public string name { get; set; }
        public string typeName { get; set; }
        public string removalDue { get; set; }
    }
}