﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class TeamListViewModel
    {
        public List<TeamViewModel> teams { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public LookUpsViewModel DMLookUps { get; set; }
    }
    public class TeamViewModel
    {
        public int teamId { get; set; }

        [Required(ErrorMessage = "Team Name is required.")]
        public string teamName { get; set; }
        public string directorate { get; set; }
        public string teamDirector { get; set; }
        public string teamManager { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Directorate")]
        public int? directorateId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Manager")]
        public int? managerId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Director")]
        public int? directorId { get; set; }
        [Required(ErrorMessage = "Main Function is required.")]
        public string mainFunction { get; set; }
        public int? active { get; set; }
        public string description { get; set; }
        public bool activeCheck { get; set; }

    }
}