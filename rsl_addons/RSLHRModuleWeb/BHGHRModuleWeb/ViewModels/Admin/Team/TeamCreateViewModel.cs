﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class TeamCreateViewModel
    {
        public TeamViewModel team { get; set; }

        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> directoratesList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorates.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Directorate Name", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> directorsList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directors.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Director Name", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> managersList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.managers.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Manager Name", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
    }
}