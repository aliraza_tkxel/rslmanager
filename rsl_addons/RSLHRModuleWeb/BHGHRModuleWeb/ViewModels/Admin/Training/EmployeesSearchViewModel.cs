﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class EmployeesSearchViewModel
    {
        public int teamId { get; set; } = 0;
        public int directorateId { get; set; } = 0;
        public string searchText { get; set; }
        public string employeeIds { get; set; }
        public List<SelectListItem> directorates { get; set; }
        public List<SelectListItem> teams { get; set; }
        public List<EmployeesViewModel> employees { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}