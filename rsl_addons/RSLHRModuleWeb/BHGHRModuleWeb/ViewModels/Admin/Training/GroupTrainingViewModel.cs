﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels.Admin.Training
{
    public class GroupTrainingViewModel
    {
        public GroupTrainingViewModel()
        {
            detail = new GroupTrainingDetailViewModel();
        }

        [Display(Name = "Team:")]
        public int? team { get; set; }

        [Display(Name = "Directorate:")]
        public int? directorate { get; set; }

        [Display(Name = "Training Course:")]
        public string trainingCourse { get; set; }

        public GroupTrainingDetailViewModel detail { get; set; }
        public GroupTrainingGridViewModel groupTrainingList { get; set; }
        public LookUpsViewModel lookUps { get; set; }

        public List<SelectListItem> teams
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.teamLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                 list.Insert(0, new SelectListItem() { Text = "Please Select Team", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> directorates
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.directorateLookUps.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                 list.Insert(0, new SelectListItem() { Text = "Please Select Directorate", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
    }
}