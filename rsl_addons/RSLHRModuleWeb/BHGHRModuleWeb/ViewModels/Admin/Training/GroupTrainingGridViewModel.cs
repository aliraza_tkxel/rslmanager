﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Admin.Training
{
    public class GroupTrainingGridViewModel
    {
        public GroupTrainingGridViewModel()
        {
            groupTrainings = new List<GroupTrainingListViewModel>();
            pagination= new PaginationViewModel();
        }
        public List<GroupTrainingListViewModel> groupTrainings { get; set; }
        public PaginationViewModel pagination { get; set; }
    }

    public class GroupTrainingListViewModel
    {
        public string recordedDate { get; set; }
        public string startDate { get; set; }
        public string courseName { get; set; }
        public string providerName { get; set; }
        public int attendees { get; set; }
        public string renewDate { get; set; }
        public string createdBy { get; set; }
        public int? groupId { get; set; }
        public bool? isMandatory { get; set; }
        public string recordedDateDisplay { get; set; }
        public string startDateDisplay { get; set; }
        public string renewDateDisplay { get; set; }
    }
}