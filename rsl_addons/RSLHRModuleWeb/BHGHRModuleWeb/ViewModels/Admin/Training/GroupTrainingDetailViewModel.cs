﻿using BHGHRModuleWeb.ViewModels.Employees.Training;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels.Admin.Training
{

    [Validator(typeof(GroupTrainingDetailValidator))]
    public class GroupTrainingDetailViewModel : GroupTrainingDetailValidator
    {
        public GroupTrainingDetailViewModel()
        {
            //approvedTraining = new List<ApprovedTraining>();
        }
        public int trainingId { get; set; }
        [Display(Name = "Training Need Justification:")]
        public string justification { get; set; }
        [Display(Name = "Training Course:")]
        public string course { get; set; }
        [Display(Name = "Start Date:")]
        public string startDate { get; set; }
        [Display(Name = "End Date:")]
        public string endDate { get; set; }
        [Display(Name = "Total Number Of Days:")]
        public int? totalNumberOfDays { get; set; }
        [Display(Name = "Provider Name:")]
        public string providerName { get; set; }
        [Display(Name = "Provider Website:")]
        public string providerWebsite { get; set; }
        [Display(Name = "Location:")]
        public string location { get; set; }

        [Display(Name = "Postcode:")]
        public string postcode { get; set; }
        [Display(Name = "Venue:")]
        public string venue { get; set; }

        [Display(Name = "Total Cost(inc VAT):")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Total Cost must be a valid number greater than 0")]
        public decimal? totalCost { get; set; }
        [Display(Name = "Submitted By:")]
        public int? isSubmittedBy { get; set; }
        [Display(Name = "Mandatory Training:")]
        public bool isMandatoryTraining { get; set; }
        [Display(Name = "Renew:")]
        public string expiry { get; set; }
        [Display(Name = "Additional Notes:")]
        public string additionalNotes { get; set; }
        public string status { get; set; }
        public int[] employeeId { get; set; }
        public string employeeIds
        {
            get;
            set;
        }
        public int? createdby { get; set; }
        public string appTrainings { get; set; }

        [Display(Name = "Approved Training")]
        public string approvedTraining { get; set; }
        public List<ApprovedTrainingViewModel> approvedTrainings { get; set; }

        [Display(Name = "Remuneration")]
        public bool? isRemuneration { get; set; }

        [Display(Name = "£")]
        [Range(0.01, double.MaxValue)]
        public decimal? remunerationCost { get; set; }
        //public List<ApprovedTraining> approvedTraining { get; set; }
        public bool isActive { get; set; }
        public int? groupId { get; set; }
        public int? professionalQualification { get; set; }
        public string createdDate { get; set; } = DateTime.Now.ToShortDateString();
        public List<EmployeeColloection> employeeDetails { get; set; }

        public List<SelectListItem> teams { get; set; }
        public List<SelectListItem> directorates { get; set; }
        public DateTime? startDateTime
        {
            get {

                return string.IsNullOrEmpty(startDate) ? (DateTime?)null : Convert.ToDateTime(DateTime.ParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

            }
        }
    }

    public class EmployeeColloection
    {
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public string teamName { get; set; }
    }

    public class ApprovedTrainingViewModel
    {
        [Display(Name = "Approved Training")]
        public string approvedTraining { get; set; }
        [Display(Name = "Remuneration")]
        public bool isRemuneration { get; set; }
        public decimal remunerationCost { get; set; }
    }
}