﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Admin.Training
{
    public class GroupTrainingUpdateViewModel
    {
        public int trainingId { get; set; }
        public int updatedBy { get; set; }
        public bool isActive { get; set; }
        public string status { get; set; }
    }
}