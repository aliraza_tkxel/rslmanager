﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class AccessControlViewModel
    {
        [Required(ErrorMessage = "User Name is required.")]
        public string userName { get; set; }
        public int? loginId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [RegularExpression(@"^(?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*", ErrorMessage = "The password must be at least 8 characters long , contain at least one lower case letter, one upper case letter and one digit.")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("password",ErrorMessage = "Both password fields should match.")]
        public string confirmation { get; set; }
        public DateTime? EXPIRES { get; set; }
        public int? isActive { get; set; }
        public bool? isLocked { get; set; }
        public int employeeId { get; set; }
        public int empId { get; set; }
        public string fullName { get { return firstName + " " + lastName; } }
    }
}