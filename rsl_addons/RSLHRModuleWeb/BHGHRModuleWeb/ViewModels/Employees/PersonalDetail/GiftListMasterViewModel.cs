﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class GiftListMasterViewModel
    {
        public int giftId { get; set; }
        public int employeeId { get; set; }
        public string details { get; set; }
        public DateTime? dateGivenReceived { get; set; }
        public decimal? approximateValue { get; set; }
        public string status { get; set; }

        public string strDateGivenReceived { get { return dateGivenReceived.HasValue ? dateGivenReceived.Value.ToShortDateString() : ""; }  }

        public string approxiValue
        {
            get
            {
                if (approximateValue.HasValue)
                {
                    return string.Format("£{0}", approximateValue.Value.ToString("0.00"));
                }
                return "£0.00";
            }
        }
    }
}