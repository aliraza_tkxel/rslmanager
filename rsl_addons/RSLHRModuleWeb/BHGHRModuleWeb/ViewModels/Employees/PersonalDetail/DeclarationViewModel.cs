﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class DeclarationViewModel
    {
        public int page { get; set; }
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public int createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public bool? isActive { get; set; }

        public DeclarationOfInterestViewModel declarationOfInterest { get; set; }

        public List<SelectListItem> relationships
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.relationship.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Relationship", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public LookUpsViewModel lookUps { get; set; }

    }

    public class DeclarationOfInterestViewModel
    {
        public int page { get; set; }
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public string employeeFullName { get; set; }
        public int createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public bool? isActive { get; set; }
        public string comment { get; set; }
        public DateTime? updatedOn { get; set; }
        public string reviewComments { get; set; }
        public DateTime? reviewDate { get; set; } = DateTime.Now;
        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";

        public DOIPage1ViewModel page1 { get; set; }
        public DOIPage2ViewModel page2 { get; set; }
        public DOIPage3ViewModel page3 { get; set; }
    }

    public class DOIPage1ViewModel
    {
        public string empName1 { get; set; }
        public string empRole1 { get; set; }
        public int? empRel1 { get; set; }
        public string empName2 { get; set; }
        public string empRole2 { get; set; }
        public int? empRel2 { get; set; }
        public string roleName { get; set; }
        public string roleRole { get; set; }
        public int? roleRel { get; set; }
        public string residingAddress1 { get; set; }
        public string residingAddress2 { get; set; }

        [RegularExpression("^([A-PR-UWYZa-pr-uwyz](([0-9](([0-9]|[A-HJKSTUWa-hjkstuw])?)?)|([A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRVWXYabehmnprvwxy])?)) [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2})")]
        public string residingPostcode { get; set; }
        public string homeName1 { get; set; }
        public string homeAddress1 { get; set; }
        public int? homeRel1 { get; set; }
        public string homeName2 { get; set; }
        public string homeAddress2 { get; set; }
        public int? homeRel2 { get; set; }
        public string typeOfEmployment { get; set; }
        public string employer { get; set; }
        public bool? isSecondaryEmployment { get; set; }

        public bool isEmployment { get; set; } = false;
    }

    public class DOIPage2ViewModel
    {
        public string conOrganizationP6 { get; set; }
        public string conService { get; set; }
        public bool? isConApprovedByLeadership { get; set; }
        public string localAuthName1 { get; set; }
        public string localAuthOrganization1 { get; set; }
        public int? localAuthRel1 { get; set; }
        public string localAuthName2 { get; set; }
        public string localAuthOrganization2 { get; set; }
        public int? localAuthRel2 { get; set; }
        public bool? isDirectorP8 { get; set; }
        public string conOrganizationP8 { get; set; }
        public string conIndvidual { get; set; }
        public int? conRelP8 { get; set; }
        public bool? isDirectorP9 { get; set; }
        public string conOrganizationP9 { get; set; }


        public bool isLeadership { get; set; } = false;
        public bool isTempDirectorP8 { get; set; } = false;
        public bool isTempDirectorP9 { get; set; } = false;
    }

    public class DOIPage3ViewModel
    {
        public string trustee1 { get; set; }
        public string trustee2 { get; set; }
        public string trustee3 { get; set; }
        public string countyTown { get; set; }
        public string appointments { get; set; }
        public string beneficialInterest { get; set; }
        public bool? isInterested { get; set; }
        public string otherInterest { get; set; }

        public string documentPath { get; set; }
        public bool isTempInterested { get; set; } = false;
    }
}