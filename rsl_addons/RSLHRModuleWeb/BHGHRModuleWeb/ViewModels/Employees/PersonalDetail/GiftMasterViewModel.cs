﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class GiftMasterViewModel
    {
        [Display(Name ="Type:")]
        public int typeId { get; set; }
        [Display(Name = "Status:")]
        public int statusId { get; set; }
        [Display(Name = "From:")]
        public string from { get; set; }
        [Display(Name = "To:")]
        public string to { get; set; }
        public int employeeId { get; set; }
        public bool isGift { get; set; }

        public List<GiftListMasterViewModel> giftList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public List<LookUpFields> giftStatuses { get; set; }

        public List<SelectListItem> types
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Insert(0, new SelectListItem() { Text = "All Tyes", Value = "0", Selected = true });
                list.Insert(1, new SelectListItem() { Text = "Received", Value = "1", Selected = true });
                list.Insert(2, new SelectListItem() { Text = "Given", Value = "2", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> statuses
        {
            get
            {
                var list = new List<SelectListItem>();
                list = giftStatuses.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "All Statuses", Value = "0", Selected = true });

                return list;
            }
            set { }
        }
    }
}