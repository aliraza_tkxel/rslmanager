﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class ContactDetailViewModel
    {
        public int contactId { get; set; }
        public int employeeId { get; set; }

        [Display(Name = "Address1:")]
        [Required]
        public string address1 { get; set; }

        [Display(Name = "Address2:")]
        [Required]
        public string address2 { get; set; }

        [Display(Name = "Address3:")]
        [Required]
        public string address3 { get; set; }

        [Display(Name = "Town/City:")]
        public string city { get; set; }

        [Display(Name = "County:")]
        public string county { get; set; }

        [Display(Name = "PostCode:")]
        [RegularExpression("^([A-PR-UWYZa-pr-uwyz](([0-9](([0-9]|[A-HJKSTUWa-hjkstuw])?)?)|([A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRVWXYabehmnprvwxy])?)) [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2})")]
        [Required]
        public string postalCode { get; set; }

        [Display(Name = "Home Tel:")]
        public string homePhone { get; set; }

        [Display(Name = "Home Email:")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}")]
        public string homeEmail { get; set; }

        [Display(Name = "Mobile (BCP):")]
        public string mobile { get; set; }

        [Display(Name = "Mobile (Personal):")]
        public string mobilePersonal { get; set; }

        [Display(Name = "Work Mobile:")]
        public string workMobile { get; set; }

        [Display(Name = "Work DD:")]
        public string workDD { get; set; }

        [Display(Name = "Extension:")]
        public string extension { get; set; }

        [Display(Name = "Work Email:")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}")]
        public string workEmail { get; set; }

        [Display(Name = "Next Of Kin:")]
        public string nextOfKin { get; set; }

        [Display(Name = "Emergency Contact:")]
        public string emergencyContact { get; set; }

        [Display(Name = "Emergency Tel:")]
        public string emergencyTel { get; set; }
        [Display(Name = "Emergency Relationship:")]
        public string emergencyRelationShip { get; set; }

        [Display(Name = "Emergency Info:")]
        public string emergencyInfo { get; set; }
    }
}