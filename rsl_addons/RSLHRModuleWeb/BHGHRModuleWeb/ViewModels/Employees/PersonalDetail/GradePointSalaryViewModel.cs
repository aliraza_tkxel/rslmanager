﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class GradePointSalaryViewModel
    {
        public int employeeId { get; set; }
        public int grade { get; set; }
        public int point { get; set; }
        public int? fiscalYear { get; set; }
    }
}