﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class PayPointSupportViewModel
    {
        public int lineManager { get; set; }
        public string employeeName { get; set; }
        public string jobTitle { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public double? salary { get; set; }
        public string grade { get; set; }
        public string gradePoint { get; set; }
        public PayPointRequestViewModel payPoint { get; set; }
        public int isCeo { get; set; }
        public int ceoId { get; set; } = 35;
        public int execDirectorId { get; set; } = 1222;
    }
}