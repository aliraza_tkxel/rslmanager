﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class ReferenceDocumentViewModel
    {
        public int employeeId { get; set; }
        public List<DocumentsViewModel> Documents { get; set; } = new List<DocumentsViewModel>();
        public string requestType { get; set; }
    }

    public class DocumentsViewModel
    {
        public int documentId { get; set; }
        public int documetTypeId { get; set; }
        public int employeeId { get; set; }
        public string createdDate { get; set; }
        public int createdBy { get; set; }
        public string documentPath { get; set; }
        public string createdByName { get; set; }

        public string documentName
        {
            get
            { 
                var arrPath = documentPath.Split('/');
                return arrPath[arrPath.Length-1];
            }
        }
    }
}