﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class DrivingLicenceResponseViewModel
    {
        public LicenceViewModel drivingLicence { get; set; } = new LicenceViewModel();
        public List<LicenceDisqualificationsViewModel> disqualificationsList { get; set; } = new List<LicenceDisqualificationsViewModel>();
        public LicenceDisqualificationsViewModel disqualification { get; set; } = new LicenceDisqualificationsViewModel();
        public string disqualifications { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class LicenceViewModel
    {
        public int drivingLicenceId { get; set; }

        public int? employeeId { get; set; }

        [Display(Name = "Driving Licence Type:")]
        [Required]
        public string drivingLicenceType { get; set; }
        
        public string drivingLicenceImage { get; set; }
        public string licenceImage { get; set; }
        public string imagePath
        {
            get
            {
                //  return Path.Combine(HttpContext.Current.Request.Url.Authority + "/" + "EmployeeDocuments" + "/" + employeeId.ToString() + "/", drivingLicenceImage == null ? "" : drivingLicenceImage);
                return Path.Combine(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/EmployeeDocuments/" + employeeId.ToString() + "/", licenceImage == null ? "" : licenceImage);
            }
        }
        public List<SelectListItem> LicenceType
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "Please Select", Value = "", Selected = true });
                list.Add(new SelectListItem() { Text = "Paper", Value = "Paper" });
                list.Add(new SelectListItem() { Text = "Card", Value = "Card" });
                list.Add(new SelectListItem() { Text = "Both", Value = "Both" });
                return list;
            }
        }

        [Display(Name = "Driving Licence Number:")]
        public string drivingLicenceNumber { get; set; }


        //[Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [StringLength(16, MinimumLength = 3, ErrorMessage = "The text entered exceeds the maximum length.")]
        [Display(Name = "Driving Licence Issue Number:")]
        [Required]
        public string drivingLicenceIssueNumber { get; set; }

        [Display(Name = "Driving Licence Expiry Date:")]
        [Required]
        public DateTime? expiryDate { get; set; }
        public string expiry { get; set; }

        [Display(Name = "Medical History:")]
        [Required]
        public string medicalHistory { get; set; }

        public int? createdBy { get; set; }

        public List<DrivingLicenceCategoriesViewModel> categories { get; set; } = new List<DrivingLicenceCategoriesViewModel>();
        public DrivingLicenceCategoriesViewModel categoty = new DrivingLicenceCategoriesViewModel();
        public string categorys { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class DrivingLicenceCategoriesViewModel
    {
        [Display(Name = "Category:")]
        public string category { get; set; }

        [Display(Name = "Code:")]
        public string code { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    public class LicenceDisqualificationsViewModel
    {
        public int? employeeId { get; set; }

        [Display(Name = "Number of Points:")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int? numberofPoints { get; set; }

        [Display(Name = "Code:")]
        public string code { get; set; }

        [Display(Name = "Reason:")]
        public string reason { get; set; }

        [Display(Name = "Issued Date:")]
        public DateTime? issuedDate { get; set; }
        public string issued { get { return issuedDate.HasValue ? issuedDate.Value.ToString("dd/MM/yyyy") : ""; } set { } }

        [Display(Name = "Expiry Date:")]
        public DateTime? expiryDate { get; set; }
        public string expiry { get { return expiryDate.HasValue ? expiryDate.Value.ToString("dd/MM/yyyy") : ""; } set { } }


        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
    public class UploadDrivingLicenceImageViewModel
    {
        public int employeeId { get; set; }

        public string filePath { get; set; }

        public string rolePath { get; set; }

        public int updatedBy { get; set; }

        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

}