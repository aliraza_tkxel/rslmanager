﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class GiftAndHospitalityViewModel
    {
        
        public int fiscalYearId { get; set; }
        public string documentPath { get; set; }
        public int createdBy { get; set; }
        public int employeeId { get; set; }

        public GiftViewModel given { get; set; }
        public GiftViewModel received { get; set; }
        public List<GiftViewModel> gifts { get; set; }
        public List<GiftViewModel> lstReceived // 1 for received
        {
            get
            {
                if (gifts != null && gifts.Count > 0)
                {
                    return gifts.Where(e => e.isGivenReceived == 1).ToList();
                }

                return new List<GiftViewModel>();
            }
        }
        public List<GiftViewModel> lstGiven // 2 for given
        {
            get
            {
                if (gifts != null && gifts.Count > 0)
                {
                    return gifts.Where(e => e.isGivenReceived == 2).ToList();
                }

                return new List<GiftViewModel>();
            }
        }
        public List<SelectListItem> offers
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "Please Select", Value = "null", Selected = true });
                list.Add(new SelectListItem() { Text = "Accepted", Value = "true" });
                list.Add(new SelectListItem() { Text = "Declined", Value = "false" });
                return list;
            }
        }
        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";

        public bool isTeam { get; set; }
    }

    public class GiftViewModel
    {
        public int giftId { get; set; }
        [Required]
        public string details { get; set; }

        [Required]
        [Display(Name = "Date Received:")]
        public DateTime? dateRevieved { get; set; }

        [Required]
        [Display(Name = "Date Given/Offered:")]
        public DateTime? dateGiven { get; set; }

        [Display(Name = "Offered by:")]
        public string offeredBy { get; set; }

        [Display(Name = "Given/Offered to:")]
        public string offeredTo { get; set; }

        [Required]
        [Display(Name = "Approx Value(£):")]
        public double? approximateValue { get; set; }

        [Display(Name = "Declined or accepted?")]
        public bool? accepted { get; set; }

        [Display(Name = "Line Manager approval")]
        public DateTime? notApprovedDate { get; set; }

        [Display(Name = "Date of declaration:")]
        public DateTime? dateOfDeclaration { get; set; }
        
        [Display(Name = "Notes:")]
        public string notes { get; set; }
        public DateTime? createdDate { get; set; }
        public int? isGivenReceived { get; set; }
        public string offerToBy { get; set; }
        public DateTime? dateGivenReceived { get; set; }
        public int employeeId { get; set; }
        public int fiscalYearId { get; set; }
        public string status { get; set; }
        [Display(Name = "Reason for  rejection:")]
        public string reason { get; set; }
        public int? isAccepted { get; set; }
        

        public string receivedDate
        {
            get
            {
                if (dateRevieved.HasValue)
                {
                    return dateRevieved.Value.ToString("dd/MM/yyyy");
                }
                return "";
            }
        }
        public string givenDate
        {
            get
            {
                if (dateGiven.HasValue)
                {
                    return dateGiven.Value.ToString("dd/MM/yyyy");
                }
                return "";
            }
        }
        public string dateNotApproved
        {
            get
            {
                if (notApprovedDate.HasValue)
                {
                    return notApprovedDate.Value.ToString("dd/MM/yyyy");
                }
                return "";
            }
        }
        public string declarationDate
        {
            get
            {
                if (dateOfDeclaration.HasValue)
                {
                    return dateOfDeclaration.Value.ToString("dd/MM/yyyy");
                }
                return "";
            }
        }
        public string dateCreated
        {
            get
            {
                if (createdDate.HasValue)
                {
                    return createdDate.Value.ToString("dd/MM/yyyy");
                }
                return "";
            }
        }

        public string approxiValue
        {
            get
            {
                if (approximateValue.HasValue)
                {
                    return string.Format("£{0}", approximateValue.Value.ToString("0.00"));
                }
                return "£0.00";
            }
        }
    }
}