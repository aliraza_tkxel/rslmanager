﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class PersonalDetailViewModel
    {
        public EmployeeDetailViewModel employeeDetail { get; set; }

        public List<SelectListItem> titles
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.title.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem()
                {
                    Text = "Please Select",
                    Value = "0"
                });

                return list;
            }
            set { }
        }
        public List<SelectListItem> genders
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "Please Select Gender", Value = "N/A", Selected = true });
                list.Add(new SelectListItem() { Text = "Male", Value = "Male" });
                list.Add(new SelectListItem() { Text = "Female", Value = "Female" });
                list.Add(new SelectListItem() { Text = "Prefer not to say", Value = "Prefer not to say" });
                return list;
            }
        }
        public List<SelectListItem> maritalStatuses
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.maritalStatus.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Marital Status", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> ethnicities
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.ethnicity.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Ethnicity", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> religions
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.religion.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Religion", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> sexualOrientations
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.sexualOrientation.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Sexual Orientation", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public LookUpsViewModel lookUps { get; set; }

        public bool isDirector { get; set; }
        public bool isGift { get; set; }
    }

    public class EmployeeDetailViewModel
    {
        public int employeeId { get; set; }

        [Display(Name = "Title:")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Title")]
        public int? title { get; set; }

        [Required]
        [Display(Name = "First Name:")]
        public string firstName { get; set; }

        [Display(Name = "Middle Name:")]
        public string middleName { get; set; }

        [Required]
        [Display(Name = "Last Name:")]
        public string lastName { get; set; }

        [Display(Name = "Preferred Name:")]
        public string aka { get; set; }

        [Required]
        [Display(Name = "Date of Birth:")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string dob { get; set; }

        [Display(Name = "Gender:")]
        public string gender { get; set; }

        [Display(Name = "NI Number:")]
        public string niNumber { get; set; }

        [Display(Name = "DBS Date:")]
        public string dbsDate { get; set; }

        [Display(Name = "DBS Reminder Date:")]
        public string dbsReminderDate { get; set; }

        [Display(Name = "Ref:")]
        public string reference { get; set; }

        [Display(Name = "Marital Status:")]
        public int? maritalStatus { get; set; }

        [Display(Name = "Ethnicity:")]
        public int? ethnicity { get; set; }

        [Display(Name = "Religion:")]
        public int? religion { get; set; }

        [Display(Name = "Sexual Orientation:")]

        public int? sexualOrientation { get; set; }

        [Display(Name = "Profile:")]
        public string profile { get; set; }

        public string imagePath { get; set; }

        public bool isFiscalYearGiftEntered { get; set; }

        public bool isManager { get; set; }

        public string paypointReviewDate { get; set; }
        public bool? isDoiCreated { get; set; } = false;

        public string statusDisplay { get; set; }
        public DateTime? actionDate { get; set; }

        public DateTime? doiCreationDate { get; set; }
        public DateTime? doiUpdationDate { get; set; }
        public DateTime? doiReviewedDate { get; set; }
        public int? doiStatusId { get; set; }
        public string doiStatusName { get; set; }
        public string doiButtonName { get; set; } = "Amend";
        public int trainingCount { get; set; }

        public string paypointStatusDisplay
        {
            get
            {
                if(actionDate.HasValue && !(string.IsNullOrEmpty(statusDisplay)))
                {
                    if (statusDisplay == "Waiting Submission")
                    {
                        return statusDisplay;
                    }
                    return statusDisplay + " on " + actionDate.Value.ToShortDateString();
                }
                return "";
            }
        }

        public string doiStatusDisplay
        {
            get
            {
                if (doiStatusName == "Pending Review")
                {
                    this.isDoiCreated = false;
                    this.doiButtonName = "Amend";
                    return "Submitted on " + doiCreationDate.Value.ToShortDateString();
                }
                else if (doiStatusName == "Submitted")
                {
                    this.isDoiCreated = true;
                    this.doiButtonName = "Reviewed";
                    return "Reviewed on " + doiReviewedDate.Value.ToShortDateString();
                }
                else if (doiStatusName == "Approved")
                {
                    this.isDoiCreated = true;
                    this.doiButtonName = "Approved";
                    return doiStatusName + " on " + doiUpdationDate.Value.ToShortDateString();
                }

                this.isDoiCreated = false;
                this.doiButtonName = "Amend";
                return "";
            }
        }

        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";


    }

    public class PersonalDetailResponseViewModel : ResponseViewModel
    {
        public EmployeeDetailViewModel personalDetail { get; set; }
    }
}