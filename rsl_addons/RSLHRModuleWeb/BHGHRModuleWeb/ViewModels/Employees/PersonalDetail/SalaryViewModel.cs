﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SalaryViewModel
    {
        public int employeeId { get; set; }
        public int? lineManager { get; set; }
        public int loginUserId { get; set; }
        public bool isLineManager { get { return lineManager == loginUserId; } }
        public string employeeName { get; set; }
        public string jobTitle { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode =true )]
        public string salary { get; set; }
        public string grade { get; set; }
        public string gradePoint { get; set; }
        public ProposedSalaryViewModel payPoint { get; set; } = new ProposedSalaryViewModel();

        public List<SelectListItem> grades
        {
            get
            {
                var list = new List<SelectListItem>();
                list = gradeLookups.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Grade", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> points
        {
            get
            {
                var list = new List<SelectListItem>();
                list = gradePointLookups.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Please Select Point", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<LookUpFields> gradePointLookups { get; set; }
        public List<LookUpFields> gradeLookups { get; set; }
    }

    public class ProposedSalaryViewModel
    {
        public int paypointId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYearId { get; set; }

        public double? currentSalary { get; set; }
        public double? proposedSalary { get; set; }

        public string currentSalaryDisplay
        {
            get
            {
                return currentSalary.HasValue ? currentSalary.Value.ToString("N2", System.Globalization.CultureInfo.GetCultureInfo("en-GB")) : "";
            }
        }
        public string proposedSalaryDisplay
        {
            get
            {
                return proposedSalary.HasValue ? proposedSalary.Value.ToString("N2", System.Globalization.CultureInfo.GetCultureInfo("en-GB")) : "";
            }
        }

        public int? grade { get; set; }
        public int? gradePoint { get; set; }
        public string gradeDescription { get; set; }
        public string gradePointDescription { get; set; }
        public string dateProposed { get; set; }
        public string detailRationale { get; set; }

        public bool? approved { get; set; }
        public string approvedDate { get; set; }
        public int? approvedBy { get; set; }
        public string approvedByName { get; set; }

        public bool? supported { get; set; }
        public string supportedDate { get; set; }
        public int? supportedBy { get; set; }
        public string supportedByName { get; set; }

        public bool? authorized { get; set; }
        public string authorizedDate { get; set; }
        public int? authorizedBy { get; set; }
        public string authorizedByName { get; set; }

        public int paypointStatusId { get; set; }
        public string paypointStatusDescription { get; set; }
        public string reason { get; set; }
        public bool isWaiting { get; set; }
        [Display(Name = "Reason:")]
        public string waitingReason { get; set; }

        public List<SelectListItem> eligibleDateOptions
        {
            get
            {
                //1 April, 1 July, 1 October, 1 January 
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "1 January " + DateTime.Now.Year, Value = "01/01/" + DateTime.Now.Year });
                list.Add(new SelectListItem() { Text = "1 April " + DateTime.Now.Year, Value = "01/04/" + DateTime.Now.Year });
                list.Add(new SelectListItem() { Text = "1 July " + DateTime.Now.Year, Value = "01/07/" + DateTime.Now.Year });
                list.Add(new SelectListItem() { Text = "1 October " + DateTime.Now.Year, Value = "01/10/" + DateTime.Now.Year });
                return list;
            }
        }
    }
}