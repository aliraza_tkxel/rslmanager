﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyTeamDetail
    {
        public string teamName { get; set; }
        public string fullName { get; set; }
        public string grade { get; set; }
        public double remainingLeave { get; set; }
        public string appraisalDate { get; set; }
        public string holRequest { get; set; }

        public int employeeId { get; set; }
        public string jobTitle { get; set; }
    }
}