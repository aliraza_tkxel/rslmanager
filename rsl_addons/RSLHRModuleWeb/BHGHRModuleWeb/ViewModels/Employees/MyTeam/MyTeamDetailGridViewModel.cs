﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyTeamDetailGridViewModel
    {
        public MyTeamDetailGridViewModel()
        {
            employeeList = new List<MyTeamDetail>();
        }
        public List<MyTeamDetail> employeeList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();

        public int teamId { get; set; }
        public int employeeId { get; set; }
    }
}