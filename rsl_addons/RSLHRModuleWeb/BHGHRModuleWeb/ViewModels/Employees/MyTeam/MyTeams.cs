﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyTeams
    {
        public int teamId { get; set; }
        public int employeeId { get; set; }
        public string teamName { get; set; }
        public string noOfEmployee { get; set; }
        public string leave { get; set; }
        public string leaveDays { get; set; }
        public string leaveHRS { get; set; }
        public string salary { get; set; }
        public double sicknessAbsence { get; set; }
    }
}