﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyTeamGridViewModel
    {
        public MyTeamGridViewModel()
        {
            teamList = new List<MyTeams>();
        }
        public List<MyTeams> teamList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}