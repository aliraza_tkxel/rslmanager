﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class JournalGridViewModel
    {
        public int employeeId { get; set; }
        public int  itemType { get; set; }
        public SelectList itemTypes
        {
            get
            {
                List<SelectViewModel> items = new List<SelectViewModel>
                {
                new SelectViewModel() { Name = "Personal Details", ID = "Personal Detail" },
                new SelectViewModel(){ Name = "Contact Info", ID="Contact Info" },
                new SelectViewModel(){ Name = "Declaration of Interests", ID="Declaration of Interests" },
                new SelectViewModel(){ Name = "Job Details", ID="Job Details" },
                new SelectViewModel(){ Name = "Health", ID="My Staff" },
                new SelectViewModel(){ Name = "Benefits", ID="Benefits" },
                new SelectViewModel(){ Name = "Skills and Quals", ID="Skills and Quals" },
                new SelectViewModel(){ Name = "Absence", ID="Absence" },
                new SelectViewModel(){ Name = "Training", ID="Training" },
                new SelectViewModel(){ Name = "Gift & Hospitality", ID="Gift & Hospitality" }
                };
                return new SelectList(items, "Name", "ID", "0");
            }
            set { }
        }
        [Display(Name = "From:")]
        public DateTime? fromDate { get; set; }
        [Display(Name = "To:")]
        public DateTime? toDate { get; set; }
        public JournalGridViewModel()
        {
            employeeList = new List<JournalViewModel>();
            pagination = new PaginationViewModel();
        }
        public List<JournalViewModel> employeeList { get; set; }
        public PaginationViewModel pagination { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

}
