﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class JournalViewModel
    {
        public int employeeId { get; set; }
        public string creationDate { get; set; }
        public string lastActionDate { get; set; }
        public string item { get; set; }
        public string nature { get; set; }
        public string title { get; set; }
        public string notes { get; set; }
        public string status { get; set; }
        public string redirectTab { get; set; }
        public string creationDateString { get; set; }
        public string lastActionDateString { get; set; }
        public string documentPath { get; set; }
        public string doiStatus { get; set; }
        public string itemId { get; set; }
        public override string ToString()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}