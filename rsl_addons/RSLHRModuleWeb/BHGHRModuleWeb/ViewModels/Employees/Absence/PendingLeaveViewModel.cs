﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class PendingLeaveViewModel
    {

        [Display(Name = "Start Date:")]
        public DateTime? startDate { get; set; }

        [Display(Name = "End Date:")]
        public DateTime? endDate { get; set; }

        [Display(Name = "Duration:")]
        public double? duration { get; set; }

        [Display(Name = "Unit:")]
        public string unit { get; set; }

        [Display(Name = "Nature:")]
        public int? natureId { get; set; }

        [Display(Name = "Description:")]
        public string leaveDescription { get; set; }

        [Display(Name = "Action:")]
        public int statusId { get; set; }

        [Display(Name = "status:")]
        public string status { get; set; }

        public string applicantName { get; set; }

        [Display(Name = "Holiday Type:")]
        public string holType { get; set; }

        [Display(Name = "Note:")]
        public string notes { get; set; }

        public int? absenceHistoryId { get; set; }

        public int? applicantId { get; set; }

        public int employeeId { get; set; }

        [Display(Name = "Title:")]
        public string title { get; set; }
        public string _notes { get; set; }
        public string _reason { get; set; }

        public string from { get {
                if (startDate.HasValue)
                {
                    return startDate.Value.ToString("HH:mm");
                }
                return "";
            } }

        public string to
        {
            get
            {
                if (endDate.HasValue)
                {
                    return endDate.Value.ToString("HH:mm");
                }
                return "";
            }
        }
    }
}