﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class AbsenceLeaveEditViewModel
    {
        public int employeeId { get; set; }

        [Display(Name = "Nature:")]
        public int nature { get; set; }

        [Display(Name = "Requested:")]
        public bool requested { get; set; }

        [Display(Name = "Start Date:")]
        public DateTime startDate { get; set; }

        public int holidayTypeStart { get; set; }

        public int holidayTypeEnd { get; set; }

        [Display(Name = "End Date:")]
        public DateTime endDate { get; set; }

        [Display(Name = "Duration:")]
        [Range(1, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public int duration { get; set; } = 1;

        [Display(Name = "Recorded By:")]
        public string recordedBy { get; set; }

        [Display(Name = "Notes:")]
        public string notes { get; set; }

        public List<SelectListItem> natures
        {
            get
            {
                if (leavesNatures != null && leavesNatures.Count > 0)
                {
                    return leavesNatures.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                }

                return new List<SelectListItem>();
            }
        }

        public string holtype { get; set; } = "F";
        public string start { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string end { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string bankHolidays { get; set; }
        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";
        public List<LookUpFields> leavesNatures { get; set; }
    }
}