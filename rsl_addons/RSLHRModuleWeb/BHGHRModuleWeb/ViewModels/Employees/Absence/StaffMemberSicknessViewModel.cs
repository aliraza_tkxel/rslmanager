﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class StaffMemberSicknessViewModel
    {
        public string staffMemberFName { get; set; }
        public string staffMemberLName { get; set; }
        public Nullable<int> staffMemberId { get; set; }
        public Nullable<int> lineManagerId { get; set; }
        public double? absentDays { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public DateTime? anticipatedReturnDate { get; set; }
        public double? duration { get; set; }
        public string nature { get; set; }
        public string reason { get; set; }
        public int? reasonId { get; set; }
        public string status { get; set; }
        public string holidayType { get; set; }
        public int? statusId { get; set; }
        public int? natureId { get; set; }
        public int? absenceHistoryId { get; set; }
        public string notes { get; set; }
        public string unit { get; set; }
    }
}