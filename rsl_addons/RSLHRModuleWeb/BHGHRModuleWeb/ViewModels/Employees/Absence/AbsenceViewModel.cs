﻿using BHGHRModuleWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class AbsenceViewModel
    {
        public int employeeId { get; set; }
        public string fiscalYearId { get; set; }
        public List<SelectListItem> fiscalYearLookUp { get; set; }
        public AnnualLeaveListViewModel annualList { get; set; }

        public AbsenceListViewModel absenceList { get; set; }

        public SicknessListViewModel sicknessList { get; set; }

        public ToilListViewModel toilList { get; set; }

        public MaternityPaternityListViewModel maternityPaternityList { get; set; } = new MaternityPaternityListViewModel();

        public InternalMeetingListViewModel internalMeetingsList { get; set; } = new InternalMeetingListViewModel();

        public bool isManager { get; set; }
    }

    public class MaternityPaternityListViewModel
    {
        public bool isTeam { get; set; }
        public List<AbsenteeViewModel> absentees { get; set; }
    }
}