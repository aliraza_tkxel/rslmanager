﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class PendingSicknessViewModel
    {
        public int employeeId { get; set; }
        public int? absenceHistoryId { get; set; }
        public int? staffMemberId { get; set; }

        [Display(Name = "Reason:")]
        public string reason { get; set; }

        [Display(Name = "Start Date:")]
        public DateTime? startDate { get; set; }

        [Display(Name = "Return Date:")]
        public DateTime? endDate { get; set; }

        [Display(Name = "Duration:")]
        public double? duration { get; set; }
        public string unit { get; set; }

        public int holidayTypeStart
        {
            get
            {
                if (holidayType != null)
                {
                    if (holidayType.Equals("M") || holidayType.Equals("F") || holidayType.Equals("F-F") || holidayType.Equals("F-M"))
                    {
                        return 0;
                    }
                    else if (holidayType.Equals("A") || holidayType.Equals("A-F") || holidayType.Equals("A-M"))
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            set
            {

            }
        }

        public int holidayTypeEnd
        {
            get
            {
                if (holidayType != null)
                {
                    if ( holidayType.Equals("F") || holidayType.Equals("F-F") || holidayType.Equals("A") || holidayType.Equals("A-F"))
                    {
                        return 0;
                    }
                    else if (holidayType.Equals("M") || holidayType.Equals("F-M")|| holidayType.Equals("A-M"))
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            set
            {

            }
        }

        [Display(Name = "Cert No:")]
        public string certNo { get; set; }

        [Display(Name = "Drs Name:")]
        public string drName { get; set; }

        [Display(Name = "Recorded By:")]
        public string recordedBy { get; set; }

        [Display(Name = "Return Notes:")]
        public string notes { get; set; }
       
        public string holtype { get; set; }

        public string staffMemberFName { get; set; }
        public string staffMemberLName { get; set; }
        public Nullable<int> lineManagerId { get; set; }
        public double? absentDays { get; set; }

        [Display(Name = "Anticipated Return Date:")]
        public DateTime? anticipatedReturnDate { get; set; }
        public string nature { get; set; }

        [Display(Name = "Reason:")]
        public int? reasonId { get; set; }
        public string status { get; set; }
        public string holidayType { get; set; }

        [Display(Name = "Action:")]
        public int? statusId { get; set; }
        public int? natureId { get; set; }

        public string start { get { return startDate.Value.ToString(ModelDateFormate); } set { } }
        public string end { get; set; }
        public string anticipated { get; set; }
        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";
        public string bankHolidays { get; set; }
        public List<LookUpFields> absenceReasons { get; set; }
        public List<LookUpFields> sicknessActions { get; set; }

        public List<SelectListItem> reasons
        {
            get
            {
                if (absenceReasons != null && absenceReasons.Count > 0)
                {
                    return absenceReasons.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                }

                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> actions
        {
            get
            {
                if (sicknessActions != null && sicknessActions.Count > 0)
                {
                    var responseList = new List<SelectListItem>();
                    var list = new SelectListItem() { Selected = true, Text = "Please Select", Value = "0" };
                    foreach (var item in sicknessActions)
                    {
                        var _item = new SelectListItem();
                        _item.Text = item.lookUpDescription;
                        _item.Value = item.lookUpId.ToString();
                        responseList.Add(_item);
                    }
                    responseList.Add(list);
                    return responseList;
                }

                return new List<SelectListItem>();
            }
        }
        public List<WorkingHoursHistoryViewModel> workingHours { get; set; } = new List<WorkingHoursHistoryViewModel>();
    }
}