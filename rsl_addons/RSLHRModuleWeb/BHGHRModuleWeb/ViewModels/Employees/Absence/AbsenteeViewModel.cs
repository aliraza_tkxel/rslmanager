﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class AbsenteeViewModel
    {
        public int? absenceHistoryId { get; set; }
        public double? absentDays { get; set; }
        public DateTime? endDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? lastActionDatetime { get; set; }
        public string nature { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string title { get; set; }
        public string holtype {
            get            
            {
                if (holidayType.Equals("M-M"))
                {
                    return "M";
                }
                else if (holidayType.Equals("A-A"))
                {
                    return "A";
                }
                else
                    return "F";
            }
        }
        public string start { get { return startDate.ToString("dd/MM/yyyy"); } }
        public string startLeaveDate { get { return startDate.ToString("dd/MM/yyyy"); } }
        public string endLeaveDate { get { return endDate ==null? "N/A" : ((DateTime)endDate).ToString("dd/MM/yyyy"); } }
        public string duration
        {
            get
            {
                if (absentDays.HasValue)
                {
                    if (String.IsNullOrEmpty(durationType))
                    {
                        if (String.IsNullOrEmpty(unit))
                        {
                            return string.Format("{0} day(s)", absentDays.Value.ToString());
                        }
                        return (unit.Equals(ApplicationConstants.UnitHours) || unit.Equals(ApplicationConstants.UnitHrs)) ?
                                        string.Format("{0} hour(s)", absentDays.Value.ToString()) :
                                        string.Format("{0} day(s)", absentDays.Value.ToString());
                    }
                    else
                    {
                        if (String.Equals(ApplicationConstants.UnitDays, durationType, StringComparison.OrdinalIgnoreCase))
                        {
                            return string.Format("{0} day(s)", absentDays.Value.ToString());
                        }

                        if ((String.Equals(ApplicationConstants.UnitHours, durationType, StringComparison.OrdinalIgnoreCase)) ||
                            (String.Equals(ApplicationConstants.UnitHrs, durationType, StringComparison.OrdinalIgnoreCase)))
                        {
                            return string.Format("{0} hour(s)", absentDays.Value.ToString());
                        }
                    }
                }
                return "";
            }
        }
        public string lastActionDate { get { return lastActionDatetime == null ? "N/A" : ((DateTime)lastActionDatetime).ToString("dd/MM/yyyy"); } }

        public string unit { get; set; }
        public string durationType { get; set; }

        public double? absentHours { get; set; }
        public string holidayType { get; set; }
    }
}