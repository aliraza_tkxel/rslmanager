﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class PendingInternalLeaveViewModel
    {
        [Display(Name = "Start Date:")]
        public DateTime? startDate { get; set; }

        [Display(Name = "End Date:")]
        public DateTime? endDate { get; set; }

        [Display(Name = "Duration:")]
        public double? duration { get; set; }

        [Display(Name = "Unit:")]
        public string unit { get; set; }

        [Display(Name = "Nature:")]
        public int? natureId { get; set; }

        [Display(Name = "Description:")]
        public string leaveDescription { get; set; }

        [Display(Name = "Action:")]
        public int statusId { get; set; }

        [Display(Name = "status:")]
        public string status { get; set; }

        public string applicantName { get; set; }

        [Display(Name = "Holiday Type:")]
        public string holType { get; set; }

        [Display(Name = "Note:")]
        public string notes { get; set; }

        public int? absenceHistoryId { get; set; }

        public int? applicantId { get; set; }

        public int employeeId { get; set; }

        [Display(Name = "Title:")]
        public string title { get; set; }
        public string _notes { get; set; }
        public string _reason { get; set; }

        public string from { get; set; }

        public string to { get; set; }

        public List<SelectListItem> statusLookUps { get; set; }
        public List<SelectListItem> natures { get; set; }
        public List<SelectListItem> reasons { get; set; }

        public bool isTeam { get; set; } = false;
        public string bankHolidays { get; set; }
        public List<LookUpFields> absenceReasons { get; set; }
        public List<LookUpFields> leavesNatures { get; set; }
        public List<WorkingHoursHistoryViewModel> workingHours { get; set; } = new List<WorkingHoursHistoryViewModel>();

        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";
    }
}