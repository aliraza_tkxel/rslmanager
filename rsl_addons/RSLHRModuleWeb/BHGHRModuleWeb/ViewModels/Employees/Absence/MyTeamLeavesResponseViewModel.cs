﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyTeamLeavesResponseViewModel
    {
        public MyTeamLeavesResponseViewModel()
        {
            absenceNature = absenceReason = leaveActions = new List<LookUpFields>();
        }

        public List<PendingLeaveViewModel> myTeamLeavesPending { get; set; }
        public PendingLeaveViewModel pendingLeaves { get; set; }
        public List<LookUpFields> leaveActions { get; set; }
        public List<LookUpFields> absenceNature { get; set; }
        public List<LookUpFields> absenceReason { get; set; }

        public List<SelectListItem> statusLookUps
        {
            get
            {
                var list = new List<SelectListItem>();
                list = leaveActions.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                return list;
            }
            set { }
        }

        public List<SelectListItem> natures
        {
            get
            {
                if (absenceNature != null && absenceNature.Count > 0)
                {
                    return absenceNature.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                }

                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> reasons
        {
            get
            {
                if (absenceReason != null && absenceReason.Count > 0)
                {
                    return absenceReason.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                }

                return new List<SelectListItem>();
            }
        }

        public int employeeId { get; set; }
        public bool isTeam { get; set; } = false;
        public string bankHolidays { get; set; }
        public List<LookUpFields> absenceReasons { get; set; }
        public List<LookUpFields> leavesNatures { get; set; }
        public List<WorkingHoursHistoryViewModel> workingHours { get; set; } = new List<WorkingHoursHistoryViewModel>();

        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";
    }
}