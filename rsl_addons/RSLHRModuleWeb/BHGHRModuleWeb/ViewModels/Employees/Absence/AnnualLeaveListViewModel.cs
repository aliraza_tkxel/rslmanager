﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class AnnualLeaveListViewModel
    {
        public int employeeId { get; set; }
        public bool isManager { get; set; }
        public AbsenceLeaveViewModel leaveDetail { get; set; } = new AbsenceLeaveViewModel();
        public List<AbsenteeViewModel> absentees { get; set; } = new List<AbsenteeViewModel>();
        public List<DateTime> bankHolidays { get; set; }
        public bool isTeam { get; set; }
        public bool isHr { get; set; }
        public int currentlyLoggedEmployeeId { get; set; }
        public string leaveYearStart { get; set; }
        public string leaveYearEnd { get; set; }

        public WorkingHoursViewModel workingPattren { get; set; }
    }

    public class AbsenceLeaveViewModel
    {
        public double totalAllowance { get; set; }
        public double leavesBooked { get; set; }
        public double leavesRequested { get; set; }
        public double leavesTaken { get; set; }
        public double leavesRemaining { get; set; }
        public string startDate { get; set; }
        public string unit { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public double timeOfInLieuOwed { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public double toil { get; set; }
        public double carryForward { get; set; }
        public double bankHoliday { get; set; }
        public double? holidayEntitlementDays { get; set; }
        public double? holidayEntitlementHours { get; set; }
    }
}