﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SicknessLeaveEditViewModel
    {
        public int employeeId { get; set; }
        public int? absenceHistoryId { get; set; }
        
        [Display(Name = "Reason:")]
        public int reason { get; set; }

        [Display(Name = "Start Date:")]
        public DateTime startDate { get; set; }

        [Display(Name = "Duration:")]
        public double duration { get; set; }

        public int holidayTypeStart { get; set; }

        [Display(Name = "Recorded By:")]
        public string recordedBy { get; set; }

        [Display(Name = "Notes:")]
        public string notes { get; set; }

        public List<SelectListItem> reasons
        {
            get
            {
                if (absenceReasons != null && absenceReasons.Count > 0)
                {
                    return absenceReasons.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                }

                return new List<SelectListItem>();
            }
        }

        public string start { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string end { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string holtype { get; set; } = "F";
        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";
        public string bankHolidays { get; set; }
        public List<LookUpFields> absenceReasons { get; set; }

        
    }
}