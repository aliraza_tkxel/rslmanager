﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class SicknessListViewModel
    {
        public List<StaffMemberSicknessViewModel> staffMembers { get; set; }

        public int employeeId { get; set; }
        public List<AbsenteeViewModel> sicknessAbsences { get; set; }
        public List<LookUpFields> absenceReasons { get; set; }
        public List<LookUpFields> sicknessActions { get; set; }
        public List<DateTime> bankHolidays { get; set; }
        public bool isTeam { get; set; }
        public bool isHR { get; set; }
        public double days
        {
            get
            {
                if (sicknessAbsences != null && sicknessAbsences.Count > 0)
                {
                    double totaldays = 0;
                    foreach (var item in sicknessAbsences)
                    {
                        if (item.status != "Cancelled")
                        {
                            totaldays = totaldays + double.Parse(item.absentDays.ToString());
                        }
                    }
                    return totaldays;
                }

                return 0;
            }
        }
    }
}