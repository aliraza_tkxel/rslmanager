﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class AbsenceListViewModel
    {
        public int employeeId { get; set; }
        public List<AbsenteeViewModel> absentees { get; set; }
        public List<LookUpFields> leavesNatures { get; set; }
        public List<DateTime> bankHolidays { get; set; }

        public bool isTeam { get; set; }

        public bool isHr { get; set; }
        public int currentlyLoggedEmployeeId { get; set; }

        public double days
        {
            get
            {
                if (absentees != null && absentees.Count > 0)
                {
                    double totaldays = 0;
                    foreach (var item in absentees)
                    {
                        if (item.status == "Approved")
                        {
                            totaldays = totaldays + double.Parse(item.absentDays.ToString());
                        }
                    }
                    return totaldays;
                }
                return 0;
            }
        }
    }
}