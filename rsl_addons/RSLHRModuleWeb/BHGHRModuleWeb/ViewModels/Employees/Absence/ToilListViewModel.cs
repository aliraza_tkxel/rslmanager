﻿using BHGHRModuleWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class ToilListViewModel
    {
        public int employeeId { get; set; }
        public bool isTeam { get; set; }
        public double timeOfInlieuOwed { get; set; }
        public double toilHours { get; set; }
        public double toilHoursTaken { get { return toilHours - timeOfInlieuOwed; } }

        public List<AbsenteeViewModel> toilLeaves { get; set; }
        public List<DateTime> bankHolidays { get; set; }
    }
}