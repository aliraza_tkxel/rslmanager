﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class InternalMeetingListViewModel
    {
        public bool isTeam { get; set; }

        public bool isHr { get; set; }
        public int currentlyLoggedEmployeeId { get; set; }

        public int employeeId { get; set; }
        public List<AbsenteeViewModel> absentees { get; set; }
    }
}