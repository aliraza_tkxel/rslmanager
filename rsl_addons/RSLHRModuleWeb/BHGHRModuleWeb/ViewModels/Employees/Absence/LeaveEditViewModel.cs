﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class LeaveEditViewModel
    {
        public int employeeId { get; set; }

        [Display(Name = "Requested:")]
        public bool requested { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Reason:")]

        public int reason { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Nature:")]

        public int nature { get; set; }
        public string natureName { get; set; }

        [Display(Name = "Start Date:")]
        public DateTime startDate { get; set; } = DateTime.Now.Date;

        [Display(Name = "Return Date:")]
        public DateTime endDate { get; set; } = DateTime.Now.Date;

        [Display(Name = "Duration:")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public double duration { get; set; } 
        [Display(Name = "Anticipated Return Date:")]
        public DateTime? anticipatedReturnDate { get; set; }
        public string anticipated { get; set; }
        public int holidayTypeStart { get; set; }

        public int holidayTypeEnd { get; set; }

        [Display(Name = "Recorded By:")]
        public string recordedBy { get; set; }

        [Display(Name = "Notes:")]
        [StringLength(200)]
        public string notes { get; set; }

        public string from { get; set; }
        public string to { get; set; }
        public string leaveType { get; set; }
        public string holtype { get; set; } = "F";
        public string start { get { return startDate.ToString("dd/MM/yyyy"); } set { } }
        public string end { get { return endDate.ToString("dd/MM/yyyy"); } set { } }
        public string bankHolidays { get; set; }
        public bool isTeam { get; set; } = false;
        public string unit { get; set; }

        [Display(Name = "Title:")]
        public string title { get; set; }
        
        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";

        public List<SelectListItem> reasons
        {
            get
            {
                if (absenceReasons != null && absenceReasons.Count > 0)
                {
                    var AbsenceReasons = absenceReasons.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                    AbsenceReasons.Add(new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                    return AbsenceReasons;
                }

                return new List<SelectListItem>();
            }
        }
        public List<SelectListItem> natures
        {
            get
            {
                if (leavesNatures != null && leavesNatures.Count > 0)
                {
                    var LeaveNatures= leavesNatures.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                    LeaveNatures.Add(new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                    LeaveNatures.RemoveAll(e => e.Text == "Paternity" || e.Text == "Maternity");
                    return LeaveNatures;
                }

                return new List<SelectListItem>();
            }
        }

        public List<LookUpFields> absenceReasons { get; set; }
        public List<LookUpFields> leavesNatures { get; set; }
        public List<WorkingHoursHistoryViewModel> workingHours { get; set; } = new List<WorkingHoursHistoryViewModel>();

    }
}