﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Employees.Absence
{
    public class ToilEditViewModel
    {
        public int employeeId { get; set; }

        [Display(Name = "Requested:")]
        public bool requested { get; set; }

        [Display(Name = "Start Date:")]
        public DateTime startDate { get; set; }

        public int holidayTypeStart { get; set; }

        public int holidayTypeEnd { get; set; }

        [Display(Name = "End Date:")]
        public DateTime endDate { get; set; } = DateTime.Now;

        [Display(Name = "Duration:")]
        [Range(1, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public double duration { get; set; } = 1;

        [Display(Name = "Recorded By:")]
        public string recordedBy { get; set; }

        [Display(Name = "Notes:")]
        public string notes { get; set; }

        public string holtype { get; set; } = "F";
        public string start { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string end { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string bankHolidays { get; set; }
        public string ModelDateFormate { get; set; } = "dd/MM/yyyy";
    }
}