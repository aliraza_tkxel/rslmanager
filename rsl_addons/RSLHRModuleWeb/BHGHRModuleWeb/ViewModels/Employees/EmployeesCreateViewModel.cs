﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class EmployeesCreateViewModel
    {
        public int employeeId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool isManager { get; set; }
        public string fullHeading { get { return string.Format("#{0:D7}: {1} {2}", employeeId, firstName, lastName); } }
        public int trainingCount { get; set; }
        public bool isDirector { get; set; }
        public int giftId { get; set; }
    }
}