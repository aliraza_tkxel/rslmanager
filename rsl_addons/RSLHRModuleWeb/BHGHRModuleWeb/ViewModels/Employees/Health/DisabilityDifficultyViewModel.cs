﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class DisabilityDifficultyViewModel
    {
        [Display(Name = "Disability/Health Problem:")]
        public SelectList disability {get; set;}
        [Display(Name = "Logged(By):")]
        public string logged { get; set; }
        [Display(Name = "Notified:")]
        public string notified { get; set; }
        [Display(Name = "Additional Notes:")]
        public string additionalNotes { get; set; }
        public string modelDateFormat { get; set; }
    }
}