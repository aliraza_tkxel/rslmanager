﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class SelectViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}