﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class HealthListViewModel
    {
        public List<HealthDetailViewModel> Disablities { get; set; }
        //public List<HealthDetailViewModel> learningDifficulties { get; set; }
        public HealthDetailViewModel healthDetail { get; set; }
        public LookUpsViewModel lookUps { get; set; }
        public List<SelectListItem> disabilityHealthProblem
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.disabilityHealthProblem.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please select", Value = 0.ToString() });
                return list;
            }
            set { }
        }
        //public List<SelectListItem> learningDifficulty
        //{
        //    get
        //    {
        //        var list = new List<SelectListItem>();
        //        list = lookUps.learningDifficulty.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
        //        return list;
        //    }
        //    set { }
        //}
    }
}