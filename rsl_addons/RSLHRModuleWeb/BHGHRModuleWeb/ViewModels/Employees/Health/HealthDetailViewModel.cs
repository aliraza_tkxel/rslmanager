﻿using Foolproof;
using System;
using System.ComponentModel.DataAnnotations;

namespace BHGHRModuleWeb.ViewModels
{
    public class HealthDetailViewModel
    {

        public int disablityId { get; set; }

        public int diffDisabilityId { get; set; }
        [Display(Name = "Details:")]
        [StringLength(200)]
        public string notes { get; set; }
        [Display(Name = "Impact on Work:")]
        [StringLength(200)]
        public string impactOnWork { get; set; }

        [Display(Name = "Logged:")]
        public string loggedByString { get; set; } = DateTime.Now.ToShortDateString();

        public int loggedBy { get; set; }

        [Display(Name = "Notified:")]
        public string notifiedDate { get; set; }

        [Display(Name = "Disability/Health Problem:")]
        public string description { get; set; }

        [Display(Name = "Address1")]
        public int  employeeId { get; set; }
        [Display(Name = "Long Term:")]
        public bool? isLongTerm { get; set; } = false;
        [Display(Name = "Occupational Health Referral:")]
        public bool? isOccupationalHealth { get; set; } = false;

        //[RequiredIf("isOccupationalHealth", true)]
        [RequiredIfTrue("isOccupationalHealth", ErrorMessage = "Review is required due to Occupational Health being equal to Yes")]
        [Display(Name = "Review:")]
        public string reviewDate { get; set; }
        [Display(Name = "Attach Document:")]
        public string documentTitle { get; set; }
        public string docPath { get; set; }
        public string docName { get; set; }
        public bool IsHR { get; set; }
        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";
    }
}