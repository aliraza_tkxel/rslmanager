﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class DeclarationOfInterestReviewStatusViewModel
    {
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public int lastActionUser { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }

        [Required]
        public string reviewComments { get; set; }
        [Required]
        public DateTime? reviewDate { get; set; }

    }
}