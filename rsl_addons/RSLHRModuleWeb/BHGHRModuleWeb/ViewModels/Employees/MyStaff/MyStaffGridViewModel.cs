﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyStaffGridViewModel
    {
      public  MyStaffGridViewModel() {
            employeeList =  new List<MyStaffViewModel>();
            myTeamHierarchyList = new List<MyStaffViewModel>();
            pagination = new PaginationViewModel();
        }

        public bool isDirector { get; set; }
        public List<MyStaffViewModel> myTeamHierarchyList { get; set; }
        
        public List<MyStaffViewModel> employeeList { get; set; }
        public PaginationViewModel pagination { get; set; }
    }
}