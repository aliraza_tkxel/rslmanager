﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.ViewModels
{
    public class MyStaffViewModel
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string jobTitle { get; set; }
        public decimal annualLeave { get; set; }
        public decimal remainingLeave { get; set; }
        public string payPointReview { get; set; }
        public string appraisal { get; set; }
        public string grade { get; set; }
        public string doiStatus { get; set; }
        public Nullable<int> interestId { get; set; }
        public string leaveType { get; set; }
        public string Pendingleave { get; set; }
        public int trainingApproval { get; set; }
        public string giftStatus { get; set; }
    }
}